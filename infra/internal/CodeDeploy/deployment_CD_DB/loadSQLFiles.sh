#!/bin/bash

command -v mysql >/dev/null 2>&1 || { echo >&2 "MySQL Client is required but not installed. Aborting..."; exit 1;}

INSTANCE_ID="`wget -qO- http://instance-data/latest/meta-data/instance-id`"
HOST="`aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" --output=text | grep "DBName" | awk '{print$5}'`"
USER=DBUSER
PASS=DBPASS

mysql -h $HOST -u$USER -p$PASS -e "DROP DATABASE IF EXISTS lrcd_db"
mysql -h $HOST -u$USER -p$PASS -e "CREATE DATABASE lrcd_db"
mysql -h $HOST -u $USER -p$PASS -Dlrcd_db < /home/ec2-user/deployment-sql/schema.sql
