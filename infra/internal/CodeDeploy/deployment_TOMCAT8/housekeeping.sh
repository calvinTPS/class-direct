#!/bin/bash

service tomcat8 stop

instanceid="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
DATE=`date +%Y%m%d`
env=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instanceid" "Name=key,Values=Environment" --output=text | cut -f5)
zip -r /tmp/tomcat8.$instanceid.$DATE.zip /var/log/tomcat8
aws s3 cp /tmp/tomcat8.$instanceid.$DATE.zip s3://lr-classdirect/$env/logs/
rm -rf /tmp/tomcat8.$instanceid.$DATE.zip

service tomcat8 start
