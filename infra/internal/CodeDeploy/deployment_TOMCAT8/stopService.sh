#!/bin/bash

service tomcat8 stop
service awslogs stop

#create export directory
mkdir /tmp/classdirect
chown tomcat:tomcat /tmp/classdirect
chmod 744 /tmp/classdirect

#cronjob installation here
echo "30 0 * * mon-fri /usr/share/housekeeping.sh" >> /etc/crontab
crontab -u root /etc/crontab
