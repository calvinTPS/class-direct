#!/bin/bash

aws configure set region eu-west-1
instanceid="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
env=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instanceid" "Name=key,Values=Environment" --output=text | cut -f5)

sudo sh /tmp/deployment-appconfig/stopService.sh

sudo cp -r /tmp/deployment-appconfig/tomcat8.conf /etc/tomcat8/
sudo chown root:tomcat /etc/tomcat8/tomcat8.conf
sudo chmod 744 /etc/tomcat8/tomcat8.conf

sudo cp -r /tmp/deployment-appconfig/server.xml /etc/tomcat8/
sudo chown root:tomcat /etc/tomcat8/server.xml
sudo chmod 744 /etc/tomcat8/server.xml

sudo cp -r /tmp/deployment-appconfig/tomcat-users.xml /etc/tomcat8/
sudo chown root:tomcat /etc/tomcat8/tomcat-users.xml
sudo chmod 744 /etc/tomcat8/tomcat-users.xml

sudo cp -r /tmp/deployment-appconfig/awslogs.conf /etc/awslogs/
sudo chown root:root /etc/awslogs/awslogs.conf
sudo chmod 744 /etc/awslogs/awslogs.conf
sed -i -e "s/ENV/$env/g" /etc/awslogs/awslogs.conf

sudo cp -r /tmp/deployment-appconfig/credential /usr/share/tomcat8/
sudo chown root:root /usr/share/tomcat8/credential
sudo chmod 744 /usr/share/tomcat8/credential

sudo cp -r /tmp/deployment-appconfig/housekeeping.sh /usr/share/
sudo chown root:root /usr/share/housekeeping.sh
sudo chmod 744 /usr/share/housekeeping.sh

sudo cp -r /tmp/deployment-appconfig/hosts /etc/
sudo chown root:root /etc/hosts
sudo chmod 744 /etc/hosts

sudo sh /tmp/deployment-appconfig/startService.sh
