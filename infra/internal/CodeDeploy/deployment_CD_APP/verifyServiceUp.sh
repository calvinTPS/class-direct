#!/bin/bash

tail -1f /var/log/tomcat8/catalina.out | while read line
do
    if echo $line | grep -q 'Server startup in'; then
        echo 'Server Started'
        response=$(curl --write-out %{http_code} --silent --output /dev/null http://localhost:8080/api/v1/ping)
        if [ "200" -eq "$response" ]; then
            echo "OK"
            pkill -f "tail -1f"
            exit 0
        else
            echo "Unexpected HTTP response: $response"
            pkill -f "tail -1f"
            exit -1
        fi
    fi
done
