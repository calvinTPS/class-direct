#!/bin/bash

service tomcat8 stop

rm -rf /usr/share/tomcat8/webapps/ROOT.war
rm -rf /usr/share/tomcat8/webapps/ROOT*
rm -rf /usr/share/tomcat8/webapps/mock-equasis-thetis.war
rm -rf /usr/share/tomcat8/webapps/mock-equasis-thetis*
rm -rf /usr/share/tomcat8/webapps/mock-sitekit*
