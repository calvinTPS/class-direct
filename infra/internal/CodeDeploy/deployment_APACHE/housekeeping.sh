#!/bin/bash

find /var/log/ -mindepth 1 -mtime +2 -delete
find /opt/codedeploy-agent/deployment-root/ -mindepth 1 -mtime +1 -delete
