#!/bin/bash

sudo sh /tmp/deployment-webconfig/installPackage.sh
sudo sh /tmp/deployment-webconfig/stopService.sh

function copy() {
    sudo cp -r $1 $2
    sudo chown $3:$3 $2
    sudo chmod 744 $2
}

copy /tmp/deployment-webconfig/httpd.conf /etc/httpd/conf/httpd.conf apache
copy /tmp/deployment-webconfig/cmro.conf /etc/httpd/conf/cmro.conf apache
copy /tmp/deployment-webconfig/oidc.conf /etc/httpd/conf/oidc.conf apache
copy /tmp/deployment-webconfig/server.conf /etc/httpd/conf/server.conf apache

copy /tmp/deployment-webconfig/00-base.conf /etc/httpd/conf.modules.d/00-base.conf apache
copy /tmp/deployment-webconfig/00-mpm.conf /etc/httpd/conf.modules.d/00-mpm.conf apache
copy /tmp/deployment-webconfig/00-optional.conf /etc/httpd/conf.modules.d/00-optional.conf apache
copy /tmp/deployment-webconfig/00-proxy.conf /etc/httpd/conf.modules.d/00-proxy.conf apache

copy /tmp/deployment-webconfig/credential /usr/share/credential root
copy /tmp/deployment-webconfig/housekeeping.sh /usr/share/housekeeping.sh root
copy /tmp/deployment-webconfig/exec_base64.py /usr/share/exec_base64.py root
copy /tmp/deployment-webconfig/exec_unbase64.py /usr/share/exec_unbase64.py root
copy /tmp/deployment-webconfig/awslogs.conf /etc/awslogs/awslogs.conf root
copy /tmp/deployment-webconfig/redirect.html /etc/httpd/error/redirect.html apache
copy /tmp/deployment-webconfig/httpd /etc/init.d/httpd root

sudo sh /tmp/deployment-webconfig/startService.sh
sudo sh /tmp/deployment-webconfig/verifyServiceUp.sh
