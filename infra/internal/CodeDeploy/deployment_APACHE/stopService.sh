#!/bin/bash -x

service httpd stop
service awslogs stop

mkdir "/etc/httpd/error"
chown apache:apache "/etc/httpd/error"
