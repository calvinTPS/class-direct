#!/usr/bin/python

import sys
import base64

while True:
    request = sys.stdin.readline().strip()
    try:
        encoded = base64.b64encode(request)
        sys.stdout.write(encoded + '\n')
        sys.stdout.flush()
    except:
        sys.stdout.write('NULL\n')
        sys.stdout.flush()
