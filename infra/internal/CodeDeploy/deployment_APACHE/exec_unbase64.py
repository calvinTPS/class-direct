#!/usr/bin/python

import sys
import base64

while True:
    request = sys.stdin.readline().strip()
    try:
        decoded = base64.b64decode(request)
        sys.stdout.write(decoded + '\n')
        sys.stdout.flush()
    except:
        sys.stdout.write('NULL\n')
        sys.stdout.flush()
