#!/bin/bash -x

yum install -y httpd24 httpd24-devel openssl-devel libcurl libcurl-devel pcre-devel mod24_ssl gcc
yum -y groupinstall 'Development Tools'

rm -rf /tmp/mod_oidc
mkdir /tmp/mod_oidc

download_extract() {
    echo "Download and extract $1"
    aws s3 cp s3://lr-classdirect/sources/$1 /tmp/
    tar -xf /tmp/$1 -C /tmp/mod_oidc
}

download_extract cjose-latest.tar.gz
download_extract jansson-2.9.tar.gz
download_extract hiredis-0.13.3.tar.gz
download_extract mod_auth_openidc-master.tar.gz
download_extract tomcat-connectors-1.2.42-src.tar.gz
download_extract mod_qos-11.36.tar.gz

echo "Installing jansson"
cd /tmp/mod_oidc/jansson-2.9
./configure
make
make install

echo "Installing cjose"
cd /tmp/mod_oidc/cjose-latest
./configure
make
make install

echo "Installing hiredis"
cd /tmp/mod_oidc/hiredis-0.13.3
make clean
make
make install

echo "Installing mod oidc"
cd /tmp/mod_oidc/mod_auth_openidc-master
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
./autogen.sh
./configure --with-hiredis
make
make install

echo "Installing mod_jk"
cd /tmp/mod_oidc/tomcat-connectors-1.2.42-src/native
./configure --with-apxs=/usr/bin/apxs
make
make install

echo "Installing mod_qos"
cd /tmp/mod_oidc/mod_qos-11.36/apache2
apxs -i -c mod_qos.c

#cronjob installation here
echo "30 0 * * mon-fri /usr/share/housekeeping.sh" >> /etc/crontab
crontab -u root /etc/crontab
