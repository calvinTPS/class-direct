#!/bin/bash

response=$(curl --write-out %{http_code} --silent --output /dev/null http://localhost/server-status)
if [ "200" -eq "$response" ]; then
    echo "OK"
    exit 0
else
    echo "Unexpected HTTP response: $response"
    exit -1
fi
