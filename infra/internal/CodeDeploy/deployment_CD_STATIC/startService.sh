#!/bin/bash

instanceid="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
env=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instanceid" "Name=key,Values=Environment" --output=text | cut -f5)
aws s3 sync /var/www/html s3://lr-classdirect/$env/cdn --delete
service httpd start
