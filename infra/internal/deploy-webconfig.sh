#!/bin/bash

[[ ! -f ~/.aws/credentials ]] &&  echo "Must have AWS CLI and credentials installed." && exit 1

[[ -z "$1" ]] && echo "Usage: $0 <test|demo|nfr> <key-id> <build-number> <path-to-credential.properties>" && exit 1

aws configure set region eu-west-1

env=$1
s3_path=${env^}
build=$3
credential_path=$4
key=$2

if [[ -z "$key" ]]; then
    key=bd611fe4-124d-4c21-99f3-e505bd11c7be
fi

echo "Clean up..."
rm -rf deployment-webconfig/ codedeploy-template/
mkdir deployment-webconfig

if [[ -z "$build" ]]; then
    mkdir codedeploy-template
    cp -r CodeDeploy/* codedeploy-template/
else
    rm -rf www
    url="http://10.210.69.3:8080/job/class-direct-frontend-nightly/$build/artifact/codedeploy-static-template.zip"
    echo "Downloading build from $url"
    curl --noproxy 10.210.69.3 -s -o codedeploy-template.zip $url
    unzip codedeploy-template.zip

    echo "CDFE Nightly $build" > CONFIG_BUILD.txt
    aws s3 cp CONFIG_BUILD.txt s3://lr-classdirect/$s3_path/cdn/CONFIG_BUILD.txt --metadata "Cache-Control=no-cache"
fi

cp codedeploy-template/deployment_APACHE/*.conf deployment-webconfig/
cp codedeploy-template/deployment_APACHE/$env-config/server.conf deployment-webconfig/
sed -i -e "s/ENV/$s3_path/g" deployment-webconfig/awslogs.conf

cp codedeploy-template/deployment_APACHE/*.sh deployment-webconfig/
cp codedeploy-template/deployment_APACHE/appspec.yml deployment-webconfig/
cp codedeploy-template/deployment_APACHE/httpd deployment-webconfig/
cp codedeploy-template/deployment_APACHE/*.html deployment-webconfig/
cp codedeploy-template/deployment_APACHE/*.py deployment-webconfig/
dos2unix deployment-webconfig/*

if [[ -z "$credential_path" ]]; then
    credential=codedeploy-template/deployment_APACHE/$env-config/credential.conf
else
    credential=$credential_path
fi

echo "Using credential: $credential"
cat $credential > deployment-webconfig/credential.tmp
aws kms encrypt --key-id $key --plaintext fileb://deployment-webconfig/credential.tmp --output text --query CiphertextBlob | base64 --decode > deployment-webconfig/credential
rm -rf deployment-webconfig/credential.tmp

echo "Creating new revision..."
aws deploy push --application-name ClassDirectWebConfig --s3-location s3://lr-classdirect/$s3_path/artifacts/deployment-webconfig.zip --source deployment-webconfig/ >/dev/null 2>&1

echo "Deploying new revision..."
#aws deploy create-deployment --application-name ClassDirectWebConfig --s3-location bucket=lr-classdirect,key=$s3_path/artifacts/deployment-webconfig.zip,bundleType=zip --deployment-group-name ClassDirectWeb$s3_path --file-exists-behavior OVERWRITE

echo "Done."
