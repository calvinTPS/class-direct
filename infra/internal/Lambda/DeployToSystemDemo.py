from __future__ import print_function

import json
import urllib
import boto3

s3 = boto3.client('s3')
codedeploy = boto3.client('codedeploy')


def lambda_handler(event, context):
    try:
        revision, params = {}, {}
        print('Received event: ' + json.dumps(event))

        key = urllib.unquote_plus(event['Records'][0]['s3']['object']['key'].encode('utf8'))
        etag = event['Records'][0]['s3']['object']['eTag']

        # get the artifact type
        tokens = key.split('.')
        artifactType = tokens[len(tokens) - 1]
        if(artifactType == 'gz') : artifactType = 'tgz'
        # get the bucket name
        bucket = event['Records'][0]['s3']['bucket']['name']
        # the S3 address of the deployment package
        revision['revisionType'] = 'S3'
        revision['s3Location'] = {'bucket' : bucket, 'key' : key, 'bundleType' : artifactType, 'eTag': etag}

        response = s3.head_object(Bucket = bucket, Key = key)
        deploymentGroupName = response['Metadata']['deploymentgroup-name']
        applicationName = response['Metadata']['application-name']

        if deploymentGroupName is None or applicationName is None:
            print("Deployment group or application name is invalid.")
        else:
            response = codedeploy.create_deployment(revision = revision,
                                                    applicationName = applicationName,
                                                    deploymentGroupName = deploymentGroupName,
                                                    deploymentConfigName = 'CodeDeployDefault.OneAtATime',
                                                    ignoreApplicationStopFailures = True,
                                                    description = 'Lambda triggered deployment')
            return response
    except Exception as e:
        print('Error deploying')
        print(e)
        raise e
