"""" ClassDirectMaintenance """
"""" This script will set ClassDirect environment to maintenance mode. """
"""" Set the following environment variable to use: """""
"""" 1. DBHostName - Target to RDS hostname. """
"""" 2. DBUserName - RDS username. """
"""" 3. DBPassWord - RDS password. """

"""" To execute, use following JSON format: """
"""" { """
""""    "active": false | true """
""""    "message": "<insert some message>" """
"""" } """

import boto3
import os.path
import os
import logging

import pymysql

logger = logging.getLogger()
logger.setLevel(logging.INFO)

rds = boto3.client('rds')
cf = boto3.client('cloudformation')


def lambda_handler(event, context):
    try:
        is_active = event['active']
        message = event['message']
        rdsAddress = os.environ['DBHostName']
        rdsUser = os.environ['DBUserName']
        rdsPassword = os.environ['DBPassWord']

        conn = pymysql.connect(host=rdsAddress, port=3306, user=rdsUser, passwd=rdsPassword, database='lrcd_db')
        try:
            with conn.cursor() as cursor:
                if not is_active:
                    logger.info('Setting system maintenance with message ' + message)
                    cursor.execute("INSERT INTO SYSTEM_MAINTENANCE(MESSAGE) VALUES (%s)", (message))
                else:
                    logger.info('Deactivating system maintenance.')
                    cursor.execute("UPDATE SYSTEM_MAINTENANCE SET IS_ACTIVE = 0, END_DATE = NOW() WHERE IS_ACTIVE = 1")
            conn.commit()
        finally:
            conn.close()
    except Exception as e:
        logger.error(e)
        raise e
