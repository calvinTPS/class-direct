from urllib2 import Request, urlopen, HTTPError
import json

def lambda_handler(event, context):
    for request in event:
        method = request['method']
        print('method: ' + request['method'])
        url = request['url']
        print('url: ' + request['url'])
        headers = request['headers']
        query = Request(url)
        for header in headers:
            print('header: ' + header['name'] + ' : ' + header['value'])
            query.add_header(header['name'], header['value'])

        if method != 'GET' and request['body']:
            print('data: ' + request['body'])
            query.add_data(request['body'])

        try:
            response = urlopen(query).read()
            print('Response: ' + json.dumps(response))
        except HTTPError, e:
            if e.getcode() == 500:
                content = e.read()
                print('Response: ' + str(content))
            else:
                raise
    return 'Done'
