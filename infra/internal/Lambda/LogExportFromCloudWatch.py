from datetime import datetime, timedelta
import boto3

def lambda_handler(event, context):
    # Daily log export

    # Expected event format:
    # {"LogGroup" : "string", "LogStreamsPrefix": "string"}

    logGroup = event['LogGroup']
    # The AWS function create_export_task can not filter log streams using regular expression
    # It filters only using prefix. Specify the prefix in the event, or use * to export all logstreams
    logStreamsPrefix = event['LogStreamsPrefix']
    print("Log group to export: " + logGroup)
    print("Log streams prefix: " + logStreamsPrefix)

    timeNow = datetime.now()
    timeDayBefore = timeNow - timedelta(days=1)

    client = boto3.client('logs')
    if(logStreamsPrefix == '*'):
        response = client.create_export_task(
            taskName = 'log-export',
            logGroupName = 'ClassDirect-Test-App',
            fromTime = calculate_miliseconds_from_utc(timeDayBefore),
            to = calculate_miliseconds_from_utc(timeNow),
            destination = 'cd-log-export',
            destinationPrefix = 'lambda-exported-logs-' + str(timeNow)
        )
    else:
        response = client.create_export_task(
            taskName = 'log-export',
            logGroupName = 'ClassDirect-Test-App',
            logStreamNamePrefix = logStreamsPrefix,
            fromTime = calculate_miliseconds_from_utc(timeDayBefore),
            to = calculate_miliseconds_from_utc(timeNow),
            destination = 'cd-log-export',
            destinationPrefix = 'lambda-exported-logs-' + str(timeNow)
        )
    return response

def calculate_miliseconds_from_utc(time):
    return int((time - datetime(1970, 1, 1)).total_seconds()) * 1000
