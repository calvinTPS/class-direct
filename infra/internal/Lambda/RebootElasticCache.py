from __future__ import print_function

import json
import boto3

print('Loading function')


def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))
    message = event['Records'][0]['Sns']['Message']
    print("From SNS: " + message)

    client = boto3.client('elasticache')
    response = client.describe_replication_groups(ReplicationGroupId='cdtest-redis')

    members = response["ReplicationGroups"][0]['NodeGroups'][0]['NodeGroupMembers']

    for member in members:
        print('Rebooting CacheClusterId=' + member['CacheClusterId'] + ' with node ' + member['CacheNodeId'])
        client.reboot_cache_cluster(CacheClusterId=member['CacheClusterId'], CacheNodeIdsToReboot=[member['CacheNodeId']])
    return message
