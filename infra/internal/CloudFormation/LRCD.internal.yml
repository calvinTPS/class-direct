AWSTemplateFormatVersion: "2010-09-09"
Description: "ClassDirect internal consists of Apache and Tomcat 8 on readily available VPC and base AMI on two AZs"
Resources:
  CDInstanceRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service: "ec2.amazonaws.com"
            Action: "sts:AssumeRole"
      ManagedPolicyArns:
        - !Ref LRCDPolicyArn
      RoleName: "CDInternal-InstanceRole"
  CDInstanceProfile:
    Type: "AWS::IAM::InstanceProfile"
    Properties:
      Path: "/"
      Roles:
        - !Ref CDInstanceRole
  CDAppServer:
    Type: "AWS::AutoScaling::LaunchConfiguration"
    Metadata:
      AWS::CloudFormation::Init:
        config:
          packages:
            yum:
              tomcat8: []
          files:
            /etc/awslogs/awscli.conf:
              content: !Sub |
                [plugins]
                cwlogs = cwlogs
                [default]
                region = ${AWS::Region}
          services:
            sysvinit:
              tomcat8:
                enabled: true
                ensureRunning: true
    Properties:
      InstanceType: !Ref AppInstanceType
      ImageId: !Ref BaseImageId
      IamInstanceProfile: !Ref CDInstanceProfile
      SecurityGroups:
        - "sg-e43da49c"
      KeyName: "lr-classdirect-test-key"
      UserData:
        'Fn:Base64': !Sub |
          #!/bin/bash
          yum update -y
          aws configure set region ${AWS::Region}
          wget -P /tmp/downloads/ --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http://www.oracle.com/; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-linux-x64.rpm"
          yum localinstall -y /tmp/downloads/jdk-8u144-linux-x64.rpm
          alternatives --set java /usr/java/jdk1.8.0_144/jre/bin/java
          /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource CDAppServer --region ${AWS::Region}
          instanceid="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
          env=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instanceid" "Name=key,Values=Environment" --output=text | cut -f5)
          aws s3 cp s3://lr-classdirect/$env/artifacts/deployment-appconfig.zip /tmp/deployment-appconfig.zip
          unzip /tmp/deployment-appconfig.zip -d /tmp/deployment-appconfig
          chmod +x /tmp/deployment-appconfig/*.sh
          sudo sh /tmp/deployment-appconfig/codedeploy.sh
          /opt/aws/bin/cfn-signal -e $? --stack ${AWS::StackName} --resource CDAppAutoScalingGroup --region ${AWS::Region}
  CDAppAutoScalingGroup:
    Type: "AWS::AutoScaling::AutoScalingGroup"
    DependsOn: "CDAppServer"
    Properties:
      Cooldown: "300"
      DesiredCapacity: !Ref AppDesiredCapacity
      HealthCheckGracePeriod: 120
      HealthCheckType: "EC2"
      LaunchConfigurationName: !Ref CDAppServer
      TargetGroupARNs:
        - !Ref AppTargetGroup
      MaxSize: "6"
      MinSize: "2"
      Tags:
        -
          Key: "Name"
          Value: !Ref AppInstanceName
          PropagateAtLaunch: "true"
        -
          Key: "Environment"
          Value: !Ref Environment
          PropagateAtLaunch: "true"
      VPCZoneIdentifier:
        - !Ref CDSubnetAZ1
        - !Ref CDSubnetAZ2
    CreationPolicy:
      ResourceSignal:
        Timeout: "PT15M"
  CDWebServer:
    Type: "AWS::AutoScaling::LaunchConfiguration"
    Metadata:
      AWS::CloudFormation::Init:
        config:
          packages:
            yum:
              httpd24: []
              httpd24-devel: []
              openssl-devel: []
              libcurl: []
              libcurl-devel: []
              pcre-devel: []
              mod24_ssl: []
          groups:
            apache: []
          users:
            apache:
              groups: "apache"
          files:
            /etc/awslogs/awscli.conf:
              content: !Sub |
                [plugins]
                cwlogs = cwlogs
                [default]
                region = ${AWS::Region}
          commands:
          services:
    Properties:
      InstanceType: !Ref WebInstanceType
      ImageId: !Ref BaseImageId
      IamInstanceProfile: !Ref CDInstanceProfile
      SecurityGroups:
        - "sg-9f8319e7"
      KeyName: "lr-classdirect-test-key"
      UserData:
        'Fn:Base64': !Sub |
          #!/bin/bash
          yum update -y
          aws configure set region ${AWS::Region}
          wget -P /tmp/downloads/ --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http://www.oracle.com/; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-linux-x64.rpm"
          yum localinstall -y /tmp/downloads/jdk-8u144-linux-x64.rpm
          alternatives --set java /usr/java/jdk1.8.0_144/jre/bin/java
          /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource CDWebServer --region ${AWS::Region}
          instanceid="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
          env=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instanceid" "Name=key,Values=Environment" --output=text | cut -f5)
          aws s3 cp s3://lr-classdirect/$env/artifacts/deployment-webconfig.zip /tmp/deployment-webconfig.zip
          unzip /tmp/deployment-webconfig.zip -d /tmp/deployment-webconfig
          chmod +x /tmp/deployment-webconfig/*.sh
          sudo sh /tmp/deployment-webconfig/codedeploy.sh
          /opt/aws/bin/cfn-signal -e $? --stack ${AWS::StackName} --resource CDWebAutoScalingGroup --region ${AWS::Region}
Parameters:
  Environment:
    Type: "String"
    Description: "Target environment for CD"
    AllowedValues:
      - "Test"
      - "Demo"
      - "Nfr"
  AppDesiredCapacity:
    Default: "2"
    Description: "Desired capacity for App, default is 2."
    Type: "String"
  AppTargetGroup:
    Default: "arn:aws:elasticloadbalancing:eu-west-1:908674476176:targetgroup/TEST-CD-APP/fc7e9fcf07e47691"
    Description: "ARN of target group for App"
    Type: "String"
  AppInstanceName:
    Default: "TEST-CD-APP"
    Description: "Name of instance."
    Type: "String"
  AppInstanceType:
    Default: "t2.micro"
    Description: "Type of EC2 instance for App layer"
    Type: "String"
    AllowedValues:
      - "t2.micro"
      - "t2.medium"
      - "t2.large"
      - "t2.xlarge"
      - "m4.large"
      - "m4.xlarge"
  WebInstanceType:
    Default: "t2.micro"
    Description: "Type of EC2 instance for Web layer"
    Type: "String"
    AllowedValues:
      - "t2.micro"
      - "t2.medium"
      - "t2.large"
      - "t2.xlarge"
      - "m4.large"
      - "m4.xlarge"
  WebInstanceType:
    Default: "t2.micro"
    Description: "Type of EC2 instance for Web layer"
    Type: "String"
    AllowedValues:
      - "t2.micro"
      - "t2.medium"
      - "t2.large"
      - "t2.xlarge"
      - "m4.large"
      - "m4.xlarge"
  CDSubnetAZ1:
    Default: "subnet-3dc5bf59"
    Description: "Subnet for first AZ"
    Type: "String"
  CDSubnetAZ2:
    Default: "subnet-46d3541e"
    Description: "Subnet for second AZ"
    Type: "String"
  BaseImageId:
    Default: "ami-9047b4e9"
    Description: "Base AMI with ENA enabled and standard packages installed."
    Type: "String"
  LRCDPolicyArn:
    Default: "arn:aws:iam::908674476176:policy/LRCDInternal-Policy"
    Description: "ARN to LRCD managed policy"
    Type: "String"
