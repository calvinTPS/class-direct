#!/bin/bash

[[ ! -f ~/.aws/credentials ]] &&  echo "Must have AWS CLI and credentials installed." && exit 1

[[ -z "$1" || -z "$2" || -z "$3" ]] && echo "Usage: deploy-app.sh <test|demo|nfr> <path-to-credential.properties> <key-id>" && exit 1

env=$1
s3_path=${env^}
credential_path=$2
key=$3

echo "Clean up..."
rm -rf deployment

mkdir deployment

cp CodeDeploy/deployment_TOMCAT8/awslogs.conf deployment/
cp CodeDeploy/deployment_TOMCAT8/appspec.yml deployment/
cp CodeDeploy/deployment_TOMCAT8/tomcat8.conf deployment/
cp CodeDeploy/deployment_TOMCAT8/hosts deployment/
cp CodeDeploy/deployment_TOMCAT8/*.xml deployment/

while IFS='' read -r line || [[ -n "$line" ]]; do
    name=${line%=*}
    value=${line#*=}
    if [[ ! -z $name && ! -z $value && $line != \#* ]]; then
        if [[ $name == reserve_app_config* ]]; then
            opts="$opts $value"
        else
            opts="$opts -D$name=$value"
        fi
    fi
done < CodeDeploy/deployment_TOMCAT8/$env-config/common.properties
sed -i -e "s#JAVAOPTS#$opts#g" deployment/tomcat8.conf

aws kms encrypt --key-id $key --plaintext fileb://$credential_path --output text --query CiphertextBlob | base64 --decode > deployment/credential

cp CodeDeploy/deployment_TOMCAT8/*.sh deployment
dos2unix deployment/*.sh >/dev/null 2>&1
dos2unix deployment/tomcat8.conf

echo "Creating new revision..."
aws deploy push --application-name ClassDirectAppConfig --s3-location s3://lr-classdirect/$s3_path/artifacts/deployment-appconfig.zip --source deployment/

echo "Deploying new revision..."
#aws deploy create-deployment --application-name ClassDirectAppConfig --s3-location bucket=lr-classdirect,key=$s3_path/artifacts/deployment-appconfig.zip,bundleType=zip --deployment-group-name ClassDirect$s3_path --file-exists-behavior OVERWRITE

echo "Done."
