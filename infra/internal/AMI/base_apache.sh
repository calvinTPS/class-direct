#!/bin/bash

yum update -y
aws configure set region eu-west-1

instanceid="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
env=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instanceid" "Name=key,Values=Environment" --output=text | cut -f5)
aws s3 cp s3://lr-classdirect/$env/artifacts/deployment-webconfig.zip /tmp/deployment-webconfig.zip
unzip /tmp/deployment-webconfig.zip -d /tmp/deployment-webconfig
chmod +x /tmp/deployment-webconfig/*.sh
sudo sh /tmp/deployment-webconfig/codedeploy.sh

aws cloudwatch put-metric-alarm --alarm-name ${env^^}-CD-WEB-DISK-$instanceid \
--alarm-description "DiskSpaceUtilization >= 90 for 30 minutes" --metric-name DiskSpaceUtilization \
--namespace "System/Linux" --statistic Average --period 900 --threshold 90 \
--comparison-operator GreaterThanThreshold --dimensions "Name=InstanceId,Value=$instanceid" "Name=MountPath,Value=/" "Name=Filesystem,Value=/dev/xvda1" \
--evaluation-periods 2 --alarm-actions "arn:aws:swf:eu-west-1:908674476176:action/actions/AWS_EC2.InstanceId.Terminate/1.0" \
--unit Percent
