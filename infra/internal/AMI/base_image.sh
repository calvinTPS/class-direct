#!/bin/bash

sudo su -
yum update -y
yum install -y wget mysql cronie crontabs awslogs ruby

echo "Download and install SSM agent."
yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm

echo "Download and install codedeploy agent."
wget https://aws-codedeploy-eu-west-1.s3.amazonaws.com/latest/install -P /tmp/
chmod +x /tmp/install
/tmp/install auto

echo "Download and install xray daemon"
curl https://s3.dualstack.us-east-2.amazonaws.com/aws-xray-assets.us-east-2/xray-daemon/aws-xray-daemon-2.x.rpm -o /tmp/xray.rpm
yum install -y /tmp/xray.rpm

echo "Download and install Cloudwatch Linux/System monitoring"
yum install perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https -y
curl -o /tmp/CloudWatchMonitoringScripts-1.2.1.zip http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip
unzip -o /tmp/CloudWatchMonitoringScripts-1.2.1.zip -d /usr/share && rm -rf /tmp/CloudWatchMonitoringScripts-1.2.1.zip

echo "Push periodically to CloudWatch"
cat > /etc/crontab <<EOL
*/5 * * * * /usr/share/aws-scripts-mon/mon-put-instance-data.pl --disk-space-util --disk-space-used --disk-path=/ --disk-space-avail --disk-space-units=megabytes --from-cron
*/5 * * * * /usr/share/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --swap-util --swap-used --from-cron
0 0 * * * /usr/share/aws-scripts-mon/mon-get-instance-stats.pl --recent-hours=12 --from-cron
EOL
crontab -u root /etc/crontab

echo "Overwrite region for awscli"
cat > /etc/awslogs/awscli.conf <<EOL
[plugins]
cwlogs = cwlogs
[default]
region = eu-west-1
EOL
service awslogs restart

sysctl net.core.somaxconn=5000
sysctl net.ipv4.tcp_max_syn_backlog=2048
