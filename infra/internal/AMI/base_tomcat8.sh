#!/bin/bash

yum update -y
aws configure set region eu-west-1
wget -P /tmp/downloads/ --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http://www.oracle.com/; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-linux-x64.rpm"
yum localinstall -y /tmp/downloads/jdk-8u144-linux-x64.rpm
alternatives --set java /usr/java/jdk1.8.0_144/jre/bin/java

yum install -y tomcat8
chkconfig tomcat8 on

instanceid="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`"
env=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instanceid" "Name=key,Values=Environment" --output=text | cut -f5)
aws s3 cp s3://lr-classdirect/$env/artifacts/deployment-appconfig.zip /tmp/deployment-appconfig.zip
unzip /tmp/deployment-appconfig.zip -d /tmp/deployment-appconfig
chmod +x /tmp/deployment-appconfig/*.sh
sudo sh /tmp/deployment-appconfig/codedeploy.sh

aws cloudwatch put-metric-alarm --alarm-name ${env^^}-CD-APP-DISK-$instanceid \
--alarm-description "DiskSpaceUtilization >= 90 for 30 minutes" --metric-name DiskSpaceUtilization \
--namespace "System/Linux" --statistic Average --period 900 --threshold 90 \
--comparison-operator GreaterThanThreshold --dimensions "Name=InstanceId,Value=$instanceid" "Name=MountPath,Value=/" "Name=Filesystem,Value=/dev/xvda1" \
--evaluation-periods 2 --alarm-actions "arn:aws:swf:eu-west-1:908674476176:action/actions/AWS_EC2.InstanceId.Terminate/1.0" \
--unit Percent
