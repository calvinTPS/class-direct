#!/bin/bash

# Exit on error
set -e

function program_is_installed {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  type $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  echo "$return_"
}

npm config set strict-ssl false
npm config list
npm install -g yarn gulp

echo "Generating Notification (Email) Pug files"
pushd static-file-generator
# # Install and clean
rm -rf dist
yarn install
npm rebuild node-sass
yarn run prod
mv ./dist/pug/* ../class-direct-mail/src/main/resources/

ls -l ../class-direct-mail/src/main/resources/*
popd

echo "Exporting Pug/Jade files for print export"
pushd reports-generator
# Use back the nodules_modules from above to save some time
yarn install
npm rebuild node-sass
yarn run export
popd

pushd ./class-direct-service/src/main/resources/
find . -name "*.pug" -exec bash -c 'mv "$1" "${1%.pug}".jade' - '{}' \;
ls -l *
popd

echo "Generating Pug files...DONE"
echo "Compiled pug files are located at /class-direct-mail/src/main/resources/"
