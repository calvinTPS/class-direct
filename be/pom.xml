<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.baesystemsai.lr.cd.be</groupId>
    <artifactId>parent</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>parent</name>
    <modules>
        <module>build-spring</module>
        <module>build-logging</module>
        <module>build-tools</module>
        <module>class-direct-rest</module>
        <module>class-direct-service</module>
        <module>class-direct-security</module>
        <module>class-direct-war</module>
        <module>build-camel</module>
        <module>class-direct-camel</module>
        <module>build-test</module>
        <module>build-retrofit</module>
        <module>domain-dao</module>
        <module>class-direct-mail</module>
        <module>domain-dto</module>
        <module>class-direct-it</module>
        <module>domain-exception</module>
        <module>build-hibernate</module>
        <module>class-direct-config</module>
        <module>mast-domain-dto</module>
        <module>cs10</module>
        <module>mock-equasis-thetis</module>
        <module>cs10-client</module>
        <module>mock-sitekit</module>
    </modules>

    <properties>
        <!-- ######################## VERSIONS ######################## -->

        <!-- Camel -->
        <camel.version>2.19.0</camel.version>

        <!-- Spring -->
        <spring.version>4.3.8.RELEASE</spring.version>
        <spring.data.version>1.11.3.RELEASE</spring.data.version>
        <spring.security.version>4.2.2.RELEASE</spring.security.version>
        <spring.redis.version>1.8.3.RELEASE</spring.redis.version>

        <!-- Hateoas -->
        <hateoas.version>0.23.0.RELEASE</hateoas.version>

        <!-- Slf4j -->
        <slf4j.version>1.7.25</slf4j.version>
        <log4j.version>2.8.2</log4j.version>

        <!-- Aspectj -->
        <aspectj.version>1.8.10</aspectj.version>

        <!-- Jcabi -->
        <jcabi.version>0.22.6</jcabi.version>

        <!-- Disruptor -->
        <disruptor.version>3.3.6</disruptor.version>

        <!-- Maven -->
        <maven.resources.version>2.7</maven.resources.version>
        <maven.enforcer.version>1.4.1</maven.enforcer.version>
        <maven-plugin-api.version>3.5.0</maven-plugin-api.version>

        <!-- Maven compiler -->
        <maven.compiler.version>3.3</maven.compiler.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <!-- Resources -->
        <main.resource.directory>src/main/resources</main.resource.directory>

        <!-- Encoding -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Jackson -->
        <jsr310.version>2.9.4</jsr310.version>
        <jackson.version>2.9.4</jackson.version>
        <jackson.json.view.version>0.16</jackson.json.view.version>
        <!-- Javax -->
        <javax.validation.version>1.1.0.Final</javax.validation.version>
        <javax.servlet.version>3.1.0</javax.servlet.version>

        <!-- BeanUtils -->
        <beanutils.version>1.9.2</beanutils.version>

        <!-- Lang3 -->
        <lang3.version>3.4</lang3.version>

        <!-- Openpojo -->
        <openpojo.version>0.8.5</openpojo.version>

        <!-- default connection profile -->
        <tomcat.url>http://localhost:8080/manager/text</tomcat.url>
        <tomcat.server>TomcatServer</tomcat.server>
        <tomcat.path>/class-direct</tomcat.path>

        <!-- mojo version -->
        <mojo.version>1.6.0</mojo.version>

        <!-- lombok version -->
        <lombok.version>1.16.16</lombok.version>

        <!-- junit -->
        <junit.version>4.12</junit.version>

        <!-- dbunit -->
        <dbunit.version>2.5.3</dbunit.version>

        <!-- Jacoco -->
        <jacoco.version>0.7.4.201502262128</jacoco.version>

        <!-- CXF -->
        <webjars-servlet.version>1.5</webjars-servlet.version>
        <webjars-swagger-ui.version>3.0.10</webjars-swagger-ui.version>
        <!-- java mail -->
        <java.mail.version>1.4.7</java.mail.version>

        <!-- Maven Surefire -->
        <maven.surefire.version>2.18.1</maven.surefire.version>

        <!-- Checkstyle -->
        <checkstyle.version>2.16</checkstyle.version>
        <puppycrawl.version>6.8</puppycrawl.version>
        <!-- PMD -->
        <pmd.version>3.5</pmd.version>
        <!-- FindBugs -->
        <findbugs.version>3.0.4</findbugs.version>

        <!-- mockito -->
        <mockito.version>2.0.2-beta</mockito.version>

        <!-- maven surefire plugin -->
        <surefire.report.version>2.19.1</surefire.report.version>

        <!-- Swagger -->
        <swagger-parser.version>1.0.16</swagger-parser.version>
        <swagger-model.version>1.5.6</swagger-model.version>

        <!-- java mail -->
        <java.mail.version>1.4</java.mail.version>

        <!-- cargo maven2 plugin -->
        <cargo.maven2.version>1.6.3</cargo.maven2.version>

        <!-- unirest -->
        <unirest.version>1.4.9</unirest.version>

        <!-- cargo settings -->
        <cargo.servlet.port>7000</cargo.servlet.port>
        <cargo.tomcat.ajp.port>7001</cargo.tomcat.ajp.port>

        <!-- Hibernate -->
        <hibernate.version>5.2.10.Final</hibernate.version>

        <!-- MYSql -->
        <mysql.version>5.1.36</mysql.version>
        <pool.hikari.version>2.6.1</pool.hikari.version>
        <melody.version>1.67.0</melody.version>

        <!-- Javassist -->
        <javassist.version>3.21.0-GA</javassist.version>

        <!-- Default profile properties -->
        <jacoco.report.skip>false</jacoco.report.skip>
        <!-- quick keyword for skipping validation for Checkstyle, PMD and Findbugs -->
        <!-- If true, will skip all validation -->
        <quick>false</quick>
        <tomcat.url>http://localhost:8080/manager/text</tomcat.url>
        <tomcat.server>TomcatServer</tomcat.server>
        <tomcat.path>/class-direct</tomcat.path>
        <jdbc.cd.driverClassName>org.h2.Driver</jdbc.cd.driverClassName>
        <jdbc.cd.Url>jdbc:h2:mem:</jdbc.cd.Url>
        <jdbc.cd.schema>cd_db</jdbc.cd.schema>
        <jdbc.cd.username>sa</jdbc.cd.username>
        <jdbc.cd.password/>
        <jdbc.cd.hibernateDialect>org.hibernate.dialect.H2Dialect</jdbc.cd.hibernateDialect>

        <retrofit.version>2.1.0</retrofit.version>
        <project.version>0.0.1-SNAPSHOT</project.version>
        <okhttp.version>3.3.0</okhttp.version>
        <powermock.version>1.6.6</powermock.version>

        <javax.xml.ws.version>2.2.11</javax.xml.ws.version>
        <commons.codec.version>1.10</commons.codec.version>

        <checkstyle.config.location>build-tools/src/main/resources/validation/sun_checks.xml
        </checkstyle.config.location>

        <!--  Reflection  -->
        <reflection.version>0.9.11</reflection.version>

        <json.version>20170516</json.version>

        <skip.owasp>true</skip.owasp>
        <owasp.format>XML</owasp.format>

        <!-- apache commons -->
        <commonsio.version>2.5</commonsio.version>
        <commonscsv.version>1.4</commonscsv.version>
        <commons.dbcp.version>2.1.1</commons.dbcp.version>
        <commons.pool.version>2.4.2</commons.pool.version>
        <commons.lang.version>3.5</commons.lang.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.amazonaws</groupId>
                <artifactId>aws-xray-recorder-sdk-bom</artifactId>
                <version>1.3.1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco.version}</version>
                    <configuration>
                        <excludes>
                            <exclude>**/*DO.*</exclude>
                            <exclude>**/*Do.*</exclude>
                            <exclude>**/*Test.*</exclude>
                            <exclude>**/*Exception*</exclude>
                            <exclude>**/*Servlet*</exclude>
                            <exclude>**/**Filter*</exclude>
                            <exclude>**/*CamelErrorHandler*</exclude>
                            <exclude>**/*DateTimeModule*</exclude>
                            <exclude>com/baesystems/ai/lr/dto/**/*</exclude>
                            <exclude>com/baesystems/ai/lr/enums/**/*</exclude>
                            <exclude>**/*Wrapper.*</exclude>
                        </excludes>
                    </configuration>
                    <executions>
                        <execution>
                            <id>prepare-agent</id>
                            <phase>validate</phase>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                            <configuration>
                                <propertyName>argLine</propertyName>
                            </configuration>
                        </execution>
                        <execution>
                            <id>jacoco-check</id>
                            <phase>test</phase>
                            <goals>
                                <goal>check</goal>
                            </goals>
                            <configuration>
                                <rules>
                                    <rule implementation="org.jacoco.maven.RuleConfiguration">
                                        <element>BUNDLE</element>
                                        <limits>
                                            <limit implementation="org.jacoco.report.check.Limit">
                                                <counter>LINE</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>0.80</minimum>
                                            </limit>
                                            <limit implementation="org.jacoco.report.check.Limit">
                                                <counter>CLASS</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>0.90</minimum>
                                            </limit>
                                            <limit implementation="org.jacoco.report.check.Limit">
                                                <counter>METHOD</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>0.80</minimum>
                                            </limit>
                                            <limit implementation="org.jacoco.report.check.Limit">
                                                <counter>COMPLEXITY</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>0.70</minimum>
                                            </limit>
                                            <limit implementation="org.jacoco.report.check.Limit">
                                                <counter>BRANCH</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>0.70</minimum>
                                            </limit>
                                            <limit implementation="org.jacoco.report.check.Limit">
                                                <counter>INSTRUCTION</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>0.80</minimum>
                                            </limit>
                                        </limits>
                                    </rule>
                                </rules>
                            </configuration>
                        </execution>
                        <execution>
                            <id>merge</id>
                            <phase>verify</phase>
                            <goals>
                                <goal>merge</goal>
                            </goals>
                            <configuration>
                                <skip>${jacoco.report.skip}</skip>
                                <destFile>${basedir}/target/jacoco-merged.exec</destFile>
                                <fileSets>
                                    <fileSet implementation="org.apache.maven.shared.model.fileset.FileSet">
                                        <directory>${project.build.directory}</directory>
                                        <includes>
                                            <include>*.exec</include>
                                        </includes>
                                    </fileSet>
                                </fileSets>
                            </configuration>
                        </execution>
                        <execution>
                            <id>default-report</id>
                            <phase>install</phase>
                            <goals>
                                <goal>report</goal>
                            </goals>
                            <configuration>
                                <skip>${jacoco.report.skip}</skip>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <!-- Checkstyle -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>${checkstyle.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>com.puppycrawl.tools</groupId>
                            <artifactId>checkstyle</artifactId>
                            <version>${puppycrawl.version}</version>
                        </dependency>
                        <dependency>
                            <groupId>com.baesystemsai.lr.cd.be</groupId>
                            <artifactId>build-tools</artifactId>
                            <version>${project.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <skip>${quick}</skip>
                        <consoleOutput>true</consoleOutput>
                        <failsOnViolation>true</failsOnViolation>
                        <violationSeverity>warning</violationSeverity>
                        <includeTestSourceDirectory>true</includeTestSourceDirectory>
                    </configuration>
                    <executions>
                        <execution>
                            <phase>verify</phase>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!-- PMD -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-pmd-plugin</artifactId>
                    <version>${pmd.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>com.baesystemsai.lr.cd.be</groupId>
                            <artifactId>build-tools</artifactId>
                            <version>${project.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <skip>${quick}</skip>
                        <consoleOutput>true</consoleOutput>
                        <verbose>true</verbose>
                        <failsOnViolation>true</failsOnViolation>
                        <includeTestSourceDirectory>true</includeTestSourceDirectory>
                        <linkXRef>false</linkXRef>
                        <rulesets>
                            <ruleset>validation/BAE_AI_PMD_Eclipse.xml</ruleset>
                        </rulesets>
                        <failurePriority>2</failurePriority>
                    </configuration>
                    <executions>
                        <execution>
                            <phase>verify</phase>
                            <goals>
                                <goal>check</goal>
                                <goal>cpd-check</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!-- FindBugs -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>findbugs-maven-plugin</artifactId>
                    <version>${findbugs.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>com.baesystemsai.lr.cd.be</groupId>
                            <artifactId>build-tools</artifactId>
                            <version>${project.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <skip>${quick}</skip>
                        <effort>Max</effort>
                        <threshold>Low</threshold>
                        <xmlOutput>true</xmlOutput>
                        <excludeFilterFile>validation/LR_FindBugs_Exclude.xml</excludeFilterFile>
                    </configuration>
                    <executions>
                        <execution>
                            <phase>verify</phase>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!-- OWASP Dependencies Check-->
                <plugin>
                    <groupId>org.owasp</groupId>
                    <artifactId>dependency-check-maven</artifactId>
                    <version>1.4.5</version>
                    <configuration>
                        <format>${owasp.format}</format>
                        <failOnError>false</failOnError>
                        <skip>${skip.owasp}</skip>
                        <suppressionFile>suppress_owasp.xml</suppressionFile>
                    </configuration>
                    <executions>
                        <execution>
                            <phase>verify</phase>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <configuration>
                    	<reuseForks>true</reuseForks>
                        <forkCount>2</forkCount>
                        <!-- Fix jacoco result missing by append back the $argLine variable injected by jacoco -->
                        <argLine>-DAsyncLogger.RingBufferSize=1024 ${argLine}</argLine>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
            <id>env_dev</id>
            <properties>
                <tomcat.url>http://localhost:8080/manager/text</tomcat.url>
                <tomcat.server>TomcatServer</tomcat.server>
                <tomcat.path>/class-direct</tomcat.path>
                <jacoco.report.skip>false</jacoco.report.skip>
                <quick>false</quick>
                <jdbc.cd.driverClassName>org.h2.Driver</jdbc.cd.driverClassName>
                <jdbc.cd.Url>jdbc:h2:mem:</jdbc.cd.Url>
                <jdbc.cd.schema>cd_db</jdbc.cd.schema>
                <jdbc.cd.username>sa</jdbc.cd.username>
                <jdbc.cd.password/>
                <jdbc.cd.hibernateDialect>org.hibernate.dialect.H2Dialect</jdbc.cd.hibernateDialect>
                <owasp.format>HTML</owasp.format>
            </properties>
        </profile>

        <profile>
            <id>env_ci</id>
            <properties>
                <tomcat.url>http://localhost:8080/manager/text</tomcat.url>
                <tomcat.server>TomcatServer</tomcat.server>
                <tomcat.path>/class-direct</tomcat.path>
                <jacoco.report.skip>false</jacoco.report.skip>
                <quick>false</quick>
                <jdbc.cd.driverClassName>org.h2.Driver</jdbc.cd.driverClassName>
                <jdbc.cd.Url>jdbc:h2:mem:</jdbc.cd.Url>
                <jdbc.cd.schema>cd_db</jdbc.cd.schema>
                <jdbc.cd.username>sa</jdbc.cd.username>
                <jdbc.cd.password/>
                <jdbc.cd.hibernateDialect>org.hibernate.dialect.H2Dialect</jdbc.cd.hibernateDialect>
                <skip.owasp>false</skip.owasp>
            </properties>
        </profile>
    </profiles>
</project>
