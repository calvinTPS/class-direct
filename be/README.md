# Class Direct back-end

## Project structure
The project should be separated in maven modules, each of those serve have
a specific purpose.

### build-*
Modules starting with build-* are helping during the build process. As an 
example *build-camel* will just regroup the all the dependencies related to 
camel.

*build-tools* will contain the diffrent xml styling files that need to be used.

### domain-*
Domain modules should be specific to the data model of Class Direct, example: 
DAOs and DTOs

### class-direct-*

Those modules will be the actual implementation of the project.
*class-direct-camel* will hold all the camel routes, *class-direct-service*
will contain all the services interfaces and implementations.


## Coding standards

### IDE

You'll find all the xml file related to coding standards in the *build-tools*
module, import the following files in your IDE:
1. Google_Checkstyle.xml
2. Google_Eclipse_Formatter.xml

### Build

The build of the project will fail if the source isn't following the rules
defined in the **checkstyle** an **formatter** files which mean the following:

* All methods and classes need to have a proper java doc with parameter 
and return value descriptions.
* packages need a package-info.java to describe the purpose of it

### Build and use compiled pug files
Run

`chmod +x ./dev-script.sh && ./dev-script.sh`

#### For development, run
Use option 'n'. Now start editing the files under `./static-file-generator/app/`. All images put here will be automatically converted & compressed to PNGs. You will be able to see your changes live in the browser.

#### For moving your pug file into your `class-direct-mail` module, use option 'N'`