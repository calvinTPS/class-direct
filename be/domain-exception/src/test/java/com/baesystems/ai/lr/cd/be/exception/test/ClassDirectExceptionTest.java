package com.baesystems.ai.lr.cd.be.exception.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.MastAPIException;

/**
 * Provides unit test for {@link ClassDirectException}.
 *
 * @author msidek
 * @author YWearn 2017-05-24
 */
public class ClassDirectExceptionTest {
  /**
   * The sample error message.
   */
  private static final String MESSAGE = "Test";

  /**
   * The sample error code.
   */
  private static final int ERROR_CODE = 404;

  /**
   * Tests on {@link ClassDirectException} for proper reporting
   * of error code and error message.
   */
  @Test
  public final void classDirectExceptionTest() {
    ClassDirectException exception = new ClassDirectException(MESSAGE);
    Assert.assertNotNull(exception);
    Assert.assertEquals(0, exception.getErrorCode());
    Assert.assertEquals(MESSAGE, exception.getMessage());

    exception = new ClassDirectException(ERROR_CODE, MESSAGE);
    Assert.assertNotNull(exception);
    Assert.assertEquals(ERROR_CODE, exception.getErrorCode());
    Assert.assertEquals(MESSAGE, exception.getMessage());

    exception = new ClassDirectException(new RuntimeException(MESSAGE));
    Assert.assertNotNull(exception);
    Assert.assertEquals(0, exception.getErrorCode());
    Assert.assertEquals(MESSAGE, exception.getMessage());

    //Report the mast error code and message in case of MastApiException.
    exception = new ClassDirectException(new MastAPIException(ERROR_CODE, MESSAGE));
    Assert.assertNotNull(exception);
    Assert.assertEquals(ERROR_CODE, exception.getErrorCode());
    Assert.assertTrue(StringUtils.endsWithIgnoreCase(exception.getMessage(), MESSAGE));
  }
}
