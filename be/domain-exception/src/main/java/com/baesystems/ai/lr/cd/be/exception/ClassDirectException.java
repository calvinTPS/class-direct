package com.baesystems.ai.lr.cd.be.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides an application exception to wrap application related error
 * with proper error code and description.
 *
 * @author VMandalapu
 * @author YWearn 2017-05-22
 */
public class ClassDirectException extends Exception {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = 6656034817706671249L;

  /**
   * The error description prefix in case of MAST API error.
   */
  private static final String MAST_ERROR_PREFIX = "MAST : ";

  /**
   * The specific code for given error.
   */
  @Getter
  @Setter
  private int errorCode;

  /**
   * The detail description for given error.
   */
  @Getter
  @Setter
  private String message;

  /**
   * Constructs an exception using given error message. The error code will be empty.
   *
   * @param messageObject the error description.
   */
  public ClassDirectException(final String messageObject) {
    super();
    this.message = messageObject;
  }

  /**
   * Constructs an exception using actual exception.
   * <p>
   * If the actual exception is of type {@link MastAPIException}, then
   * extracts the error code, error message (with appended prefix) into
   * this exception.
   * </p>
   *
   * @param exception to be set.
   *
   */
  public ClassDirectException(final Throwable exception) {
    super(exception);

    // Capture the MAST error code and message in case of re-throw of MastAPIException.
    if (exception instanceof MastAPIException) {
      final MastAPIException mastException = (MastAPIException) exception;
      this.errorCode = mastException.getErrorCode();
      this.message = MAST_ERROR_PREFIX + mastException.getMessage();
    } else {
      this.message = exception.getMessage();
    }
  }

  /**
   * Constructs an exception using error code and error message.
   *
   * @param errorCodeObject the error code.
   * @param messageObject the error message.
   */
  public ClassDirectException(final int errorCodeObject, final String messageObject) {
    super();
    this.errorCode = errorCodeObject;
    this.message = messageObject;
  }

}
