package com.baesystems.ai.lr.cd.be.exception;

import org.springframework.http.HttpStatus;

/**
 * Provides an application exception to wrap on asset query when there is
 * no filter condition specified as this will return large result set.
 *
 * @author Faizal Sidek
 * @author YWearn 2017-05-23
 */
public class TooManyResultsException extends ClassDirectException {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = 3811307490807168102L;

  /**
   * Constructs an exception using given error message with default
   * error code of {@link HttpStatus#BAD_REQUEST}.
   *
   * @param messageObject the error message.
   */
  public TooManyResultsException(final String messageObject) {
    super(HttpStatus.BAD_REQUEST.value(), messageObject);
  }

  /**
   * Constructs an exception using given error code and error message.
   *
   * @param errorCodeObject the error code.
   * @param messageObject the error message.
   */
  public TooManyResultsException(final int errorCodeObject, final String messageObject) {
    super(errorCodeObject, messageObject);
  }
}
