package com.baesystems.ai.lr.cd.be.exception;

/**
 * Provides an application exception to wrap on error
 * when performing encryption and decryption.
 *
 * @author RKaneysan
 * @author YWearn 2017-05-23
 */
public class ClassDirectSecurityException extends ClassDirectException {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = -6314883535522602025L;

  /**
   * Constructs an exception using given error message.
   * @param message the error message.
   */
  public ClassDirectSecurityException(final String message) {
    super(message);
  }

  /**
   * Constructs an exception wrapped on top of given actual exception.
   *
   * @param exception the actual exception.
   */
  public ClassDirectSecurityException(final Exception exception) {
    super(exception);
  }
}
