package com.baesystems.ai.lr.cd.be.processor;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.RuntimeCamelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectExceptionDTO;

/**
 * Provides default error handler for camel API which will return the
 * proper error code and error message in the response payload upon error.
 *
 * @author VMandalapu
 * @author YWearn 2017-05-23
 *
 */
public class CamelErrorHandler implements Processor {

  /**
   * The default error message when non application related exception occurs.
   */
  @Value("${internal.server.error}")
  private String internalServerError;

  /**
   * The application logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CamelErrorHandler.class);

  @Override
  public final void process(final Exchange exchange) {
    final Throwable exception = getException(exchange);
    if (exception != null) {
      processException(exchange, exception);
    }
  }

  /**
   * Handles exception from the underlying Camel route processes.
   * @param exchange the Camel container with all route related information.
   * @param exception the exception thrown from the route process.
   */
  private void processException(final Exchange exchange, final Throwable exception) {
    ClassDirectExceptionDTO errorMessage;
    LOGGER.error("process", exception);

    final Integer errorCodeDefault = HttpStatus.INTERNAL_SERVER_ERROR.value();
    Integer errorCode = null;

    if (exception instanceof ClassDirectException) {
      errorCode = ((ClassDirectException) exception).getErrorCode();
      errorMessage = new ClassDirectExceptionDTO(exception.getMessage(), errorCode);
    } else if (exception instanceof AccessDeniedException) {
      errorMessage = new ClassDirectExceptionDTO(exception.getMessage(), HttpStatus.FORBIDDEN.value());
    } else {
      errorMessage = new ClassDirectExceptionDTO(internalServerError, errorCodeDefault);
    }

    // translated so clear
    exchange.setException(null);
    exchange.setProperty(Exchange.EXCEPTION_CAUGHT, null);

    exchange.getOut().setHeaders(exchange.getIn().getHeaders());
    exchange.getOut().setBody(errorMessage);
    exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
    if (errorCode != null && errorCode != 0) {
      exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, errorCode);
    } else {
      exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, errorCodeDefault);
    }
  }


  /**
   * Extracts the actual application exception that wrapped around
   * {@link CamelException} or {@link RuntimeCamelException}.
   *
   * @param exchange the Camel container with all route related information.
   * @return value the actual application exception.
   */
  private Throwable getException(final Exchange exchange) {
    Throwable exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
    if (exception == null) {
      exception = exchange.getException();
    }

    if (exception != null) {
      while (exception.getCause() != null && exception != exception.getCause()
          && (exception instanceof CamelException || exception instanceof RuntimeCamelException)) {
        exception = exception.getCause();
      }
    }
    return exception;
  }

}
