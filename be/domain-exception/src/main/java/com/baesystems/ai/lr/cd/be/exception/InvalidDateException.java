package com.baesystems.ai.lr.cd.be.exception;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Provides an application exception to wrap on error
 * validates date and date time.
 *
 * @author NAvula
 * @author YWearn 2017-05-23
 */
public class InvalidDateException extends ClassDirectException {

  /**
  * The generated UUID.
  */
 private static final long serialVersionUID = -2313939065314332631L;

  /**
   * Constructs an exception using given error message and substitutes the variable
   * with the invalid date and time.
   *
   * @param message the error message.
   * @param dates the list of date time value.
   */
  public InvalidDateException(final String message,
      final LocalDateTime... dates) {
    super(String.format(message, dates));
  }

  /**
   * Constructs an exception using given error message and substitutes the variable
   * with the invalid date.
   * @param message the error message.
   * @param dates the list of date value.
   */
  public InvalidDateException(final String message,
      final LocalDate... dates) {
    super(String.format(message, dates));
  }



}
