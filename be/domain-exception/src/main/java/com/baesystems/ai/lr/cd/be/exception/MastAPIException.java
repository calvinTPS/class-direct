package com.baesystems.ai.lr.cd.be.exception;

import java.io.IOException;
import lombok.Getter;
import lombok.Setter;

/**
 * Provides an application exception to wrap on top of MAST API error
 * so error code and description from MAST API error can be captured.
 *
 * @author SBollu
 * @author YWearn 2017-05-22
 */
public class MastAPIException extends IOException {


  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = -1348014836383168570L;

  /**
  * The specific code for given error.
  */
  @Getter
  @Setter
  private int errorCode;

  /**
  * The detail description for given error.
  */
  @Getter
  @Setter
  private String message;

  /**
   * @param messageObject object
   */
  public MastAPIException(final String messageObject) {
    super();
    this.message = messageObject;
  }

  /**
   * Constructs an exception using error code and error message.
   *
   * @param errorCodeObject the failed MAST API error code.
   * @param messageObject the failed MAST API error description.
   */
  public MastAPIException(final int errorCodeObject, final String messageObject) {
    super();
    this.errorCode = errorCodeObject;
    this.message = messageObject;
  }

}
