package com.baesystems.ai.lr.cd.be.exception;

/**
 * Provides an application exception to wrap on accessing API without
 * proper authorisation / permission.
 *
 * @author sbollu
 * @author YWearn 2017-05-22
 */
public class UnauthorisedAccessException extends ClassDirectException {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = -3192373221894263946L;

  /**
   * The default error code for unauthorised access to APIs.
   */
  private static final Integer STATUS = 401;

  /**
   * Constructs an exception using given error message.
   *
   * @param message the error description.
   */
  public UnauthorisedAccessException(final String message) {
    super(STATUS, message);
  }

}
