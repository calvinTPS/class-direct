package com.baesystems.ai.lr.cd.be.exception;

/**
 * Provides an application exception to wrap on error
 * when a user tries to login with an inactive account.
 *
 * @author PFauchon 19/12/2016
 * @author YWearn 2017-05-23
 */
public class InactiveAccountException extends ClassDirectException {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = -3192373221894263946L;

  /**
   * The default status code for login with inactive account.
   */
  private static final Integer STATUS = 401;


  /**
   * Constructs an exception using given error message and
   * defaulted the error code as 401.
   *
   * @param messageObject the detailed error message
   */
  public InactiveAccountException(final String messageObject) {
    super(STATUS, messageObject);
  }

}
