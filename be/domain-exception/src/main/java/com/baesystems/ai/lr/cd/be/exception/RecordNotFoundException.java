package com.baesystems.ai.lr.cd.be.exception;


/**
 * Provides an application exception to wrap on API that do not
 * find the requested data.
 *
 * @author VMandalapu
 * @author YWearn 2015-05-23
 */
public class RecordNotFoundException extends ClassDirectException {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = 8964555189224671499L;

  /**
   * The default status code for record not found.
   */
  private static final Integer STATUS = 404;

  /**
   * Constructs an exception using given error message and substitute the value
   * replacement on the error message.
   *
   * @param message the error message with variable replacement.
   * @param id the variable replacement value.
   */
  public RecordNotFoundException(final String message, final long id) {
    super(STATUS, String.format(message, id));
  }

  /**
   * Constructs an exception using given error message and substitute the value
   * replacement on the error message.
   *
   * @param message the error message with variable replacement.
   * @param id the variable replacement value.
   */
  public RecordNotFoundException(final String message, final String id) {
    super(STATUS, String.format(message, id));
  }

  /**
   * Constructs an exception using given error message and substitute the value
   * replacement on the error message.
   *
   * @param message the error message with variable replacement.
   * @param id1 the first variable replacement value.
   * @param id2 the seconf variable replacement value.
   */
  public RecordNotFoundException(final String message, final String id1, final String id2) {
    super(STATUS, String.format(message, id1, id2));
  }

  /**
   * Constructs an exception using given error message.
   *
   * @param message the error message.
   */
  public RecordNotFoundException(final String message) {
    super(STATUS, message);
  }

}
