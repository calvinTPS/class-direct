package com.baesystems.ai.lr.cd.be.exception;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Provides an application exception to wrap on error
 * validates one or more emails.
 *
 * @author PFauchon 29/9/2016.
 * @author YWearn 2017-05-23
 */
public class InvalidEmailException extends ClassDirectException {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = -3040156318178335815L;

  /**
   * Constructs an exception using given error message and substitutes the variable
   * with the invalid email(s) value.
   *
   * @param messageObject the error message.
   * @param emails the list of emails that have been tested and one of them is invalid.
   */
  public InvalidEmailException(final String messageObject, final String... emails) {
    super(String.format(messageObject, Stream.of(emails).collect(Collectors.joining(", "))));
  }
}
