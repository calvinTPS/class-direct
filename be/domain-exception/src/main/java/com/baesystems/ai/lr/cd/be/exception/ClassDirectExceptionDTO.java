package com.baesystems.ai.lr.cd.be.exception;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides a model with internal error code and description to be
 * pass back to the ClassDirect API invoker upon error.
 *
 * @author VMandalapu
 * @author YWearn2017-05-23
 */
public class ClassDirectExceptionDTO implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -7308481925232697862L;

  /**
  * The detailed error message.
  */
  @Getter
  @Setter
  private String message;

  /**
  * The internal error code.
  */
  @Getter
  @Setter
  private int errorCode;

  /**
   * Constructs the model with only error message without error code.
   *
   * @param messageObject the error message.
   */
  public ClassDirectExceptionDTO(final String messageObject) {
    this.message = messageObject;
  }

  /**
   * Set the error message and the detailed message directly. This accepts the
   * stack trace/detailed message formatted as a string and is unlikely to be
   * used in most cases.
   * Constructs the model with the detailed error message and error code.
   *
   * @param messageObject the detailed error message.
   * @param errorCodeObject the error code.
   */
  public ClassDirectExceptionDTO(final String messageObject,
      final int errorCodeObject) {
    this.message = messageObject;
    this.errorCode = errorCodeObject;
  }

}
