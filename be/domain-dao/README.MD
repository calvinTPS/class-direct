# class-direct-domain-dao

This module has Retrofit services to liase with MAST endpoints

1. Include following spring context to boot:
	- src/main/resources/retrofit-context.xml
2. Configure MAST endpoint on PropertyConfigurer using these keys:
	- retrofit.mast.url=<mast url>
	E.g: retrofit.mast.url=http://localhost:8080/mast/api/v2/

Side Note:

Before compiling, make sure to install MAST domain dto using command:

mvn install:install-file -Dfile=libs/domain-dto-0.0.1-SNAPSHOT.jar -DgroupId=com.baesystemsai.lr.be -DartifactId=domain-dto -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -DpomFile=libs/pom.xml
