package com.baesystems.ai.lr.cd.be.hibernate.test;

import static com.baesystems.ai.lr.cd.be.hibernate.test.UserId.ALICE;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl.SubFleetDAOImpl;
import com.baesystems.ai.lr.cd.be.domain.repositories.SubFleet;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author syalavarthi
 *
 */
@RunWith(PowerMockRunner.class)
public class SubFleetDAOImplTest {
  /**
   * long id.
   */
  private static final Long ID = 1L;
  /**
   * long id2.
   */
  private static final Long ID2 = 2L;
  /**
   * long id3.
   */
  private static final Long ID3 = 3L;
  /**
   * long id4.
   */
  private static final Long ID4 = 4L;
  /**
   * mock subfleet dao.
   */
  @InjectMocks
  private final SubFleetDAO dao = new SubFleetDAOImpl();

  /**
   * mock entity manager.
   */
  @Mock
  private EntityManager entityManager;

  /**
   * mock typed query.
   */
  @Mock
  private List<SubFleet> typedQuery;

  /**
   * query.
   */
  @Mock
  private Query query;

  /**
   * message.
   */
  @Mock
  private String recordNotFoundMessage;

  /**
   * test subfleet dao.
   *
   * @throws RecordNotFoundException exception
   */
  @Test
  public final void test() throws RecordNotFoundException {
    SubFleet fleet = new SubFleet();
    fleet.setAssetId(ID);
    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    fleet.setUser(up);
    Assert.assertNotNull(entityManager);
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyString())).thenReturn(mockUserProfile());
    Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyString())).thenReturn(mockUserProfile());
    Mockito.when(this.entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when((query).setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
    Mockito.when((query).getResultList()).thenReturn(mockSubFleetList());

    Assert.assertEquals(ID, dao.getSubFleetById(ALICE.getId()).get(0));
    Assert.assertEquals(ALICE.getId(), fleet.getUser().getUserId());
    Assert.assertNotNull(((SubFleetDAOImpl) dao).getEntityManager());
  }

  /**
   * test subfleet dao.
   *
   * @throws RecordNotFoundException exception
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testNotFound() throws RecordNotFoundException {
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyLong())).thenReturn(null);
    Mockito.when(this.entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when((query).setParameter(Mockito.anyString(), Mockito.anyLong())).thenReturn(query);
    Mockito.when((query).getResultList()).thenReturn(mockSubFleetList());
    Assert.assertEquals(ID, dao.getSubFleetById(ALICE.getId()).get(0));
  }

  /**
   * test subfleet dao.
   *
   * @throws RecordNotFoundException exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testSubfleetNotFound() throws RecordNotFoundException {
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyString())).thenReturn(mockUserProfile());
    Mockito.when(this.entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when((query).setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
    Mockito.when((query).getResultList()).thenReturn(null);
    Assert.assertEquals(ID, dao.getSubFleetById(ALICE.getId()).get(0));
  }

  /**
   * test savesubfleet.
   *
   * @throws Exception exception.
   */
  @Test(expected = Exception.class)
  public final void testSaveSubFleet() throws Exception {
    List<Long> assetIds = new ArrayList<>();
    List<Long> assetIds1 = new ArrayList<>();
    List<Long> assetIds3 = new ArrayList<>();
    assetIds.add(ID);
    assetIds.add(ID2);
    assetIds3.add(ID3);
    assetIds3.add(ID4);
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyString())).thenReturn(mockUserProfile());
    Mockito.when(this.entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when((query).setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
    Mockito.when(this.entityManager.createQuery(Mockito.anyString()).executeUpdate()).thenReturn(1);

    entityManager.persist(mockSubFleet());
    final SubFleet subFleet1 = new SubFleet();
    Assert.assertNotNull(entityManager.contains(mockSubFleet()));
    Assert.assertNotNull(entityManager.contains(subFleet1));
    final StatusDto status = new StatusDto();
    status.setMessage("subfleet updated.");
    status.setStatus("200");
    Assert.assertEquals(status.getMessage(), dao.saveSubFleet(ALICE.getId(), assetIds, assetIds1).getMessage());
    Assert.assertEquals(status.getMessage(), dao.saveSubFleet(ALICE.getId(), assetIds, assetIds3).getMessage());
    Assert.assertNotNull(dao.saveSubFleet(ALICE.getId(), assetIds, assetIds3));
    Assert.assertEquals(status.getMessage(), dao.saveSubFleet(ALICE.getId(), assetIds1, assetIds3).getMessage());
    Assert.assertNotNull(dao.saveSubFleet(ALICE.getId(), assetIds1, assetIds3));
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyString())).thenReturn(null);
    Assert.assertEquals(status.getMessage(), dao.saveSubFleet(ALICE.getId(), assetIds1, assetIds).getMessage());
    Assert.assertNotNull(dao.saveSubFleet(ALICE.getId(), assetIds1, assetIds));

  }

  /**
   * Tests get sub fleet count.
   *
   * @throws RecordNotFoundException if the user not found
   */
  @Test
  public final void testGetSubFleetCount() throws RecordNotFoundException {
    final long subFleetSize = 1L;
    final UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    Assert.assertNotNull(entityManager);
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyString())).thenReturn(mockUserProfile());
    Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when(this.entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when((query).setParameter(Mockito.anyString(), Mockito.any())).thenReturn(query);
    Mockito.when((query).getSingleResult()).thenReturn(subFleetSize);

    Assert.assertEquals(subFleetSize, dao.getSubFleetCountByUserId((ALICE.getId())));
  }

  /**
   * Tests get sub fleet count with invalid user id.
   *
   * @throws RecordNotFoundException if the user not found
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testGetSubFleetCountWithInvalidUserId() throws RecordNotFoundException {
    final long subFleetSize = 1L;
    final UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    Assert.assertNotNull(entityManager);
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.anyString())).thenReturn(mockUserProfile());
    Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when(this.entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when((query).setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
    Mockito.when((query).getSingleResult()).thenReturn(subFleetSize);

    Assert.assertEquals(subFleetSize, dao.getSubFleetCountByUserId(null));
  }

  /**
   * Tests get sub fleet count with invalid user.
   *
   * @throws RecordNotFoundException if the user not found
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testGetSubFleetCountWithInvalidUser() throws RecordNotFoundException {
    final long subFleetSize = 1L;
    Assert.assertNotNull(entityManager);
    Mockito.when(this.entityManager.find(Mockito.any(), Mockito.eq(""))).thenReturn(null);
    Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when(this.entityManager.createQuery(Mockito.anyString())).thenReturn(query);
    Mockito.when((query).setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
    Mockito.when((query).getSingleResult()).thenReturn(subFleetSize);

    Assert.assertEquals(subFleetSize, dao.getSubFleetCountByUserId(""));
  }



  /**
   * @return list of subfleet.
   */
  private List<SubFleet> mockSubFleetList() {
    List<SubFleet> subFleetList = new ArrayList<>();
    SubFleet fleet = new SubFleet();
    fleet.setAssetId(ID);
    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    fleet.setUser(up);
    subFleetList.add(fleet);
    return subFleetList;
  }

  /**
   * @return user.
   */
  private UserProfiles mockUserProfile() {
    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    return up;
  }

  /**
   * @return subfleet.
   */
  private SubFleet mockSubFleet() {
    SubFleet fleet = new SubFleet();
    fleet.setAssetId(ID);
    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    fleet.setUser(up);
    return fleet;
  }
}
