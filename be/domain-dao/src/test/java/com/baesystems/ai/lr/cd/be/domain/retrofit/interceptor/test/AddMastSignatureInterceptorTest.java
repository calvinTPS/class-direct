package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpStatus;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import retrofit2.Call;
import retrofit2.Retrofit;

import com.baesystems.ai.lr.cd.be.domain.dao.RetrofitServiceFactory;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.security.MastSignature;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.be.domain.retrofit.RetrofitFactory;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.AddMastSignatureInterceptor;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.LoggingInterceptor;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.AddMastSignatureInterceptor}.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AddMastSignatureInterceptorTest {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AddMastSignatureInterceptorTest.class);

  /**
   * test user name.
   */
  private static final String TEST_USER = "test";

  /**
   * test group name.
   */
  private static final String TEST_GROUP = "admin";

  /**
   * Injected server.
   */
  @Autowired
  private MockWebServer server;

  /**
   * Injected retrofit service.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * Injected security dao.
   */
  @Autowired
  private SecurityDAO securityDAO;

  /**
   * Interceptor should intercept and append signature.
   *
   * @throws Exception when error.
   */
  @Test
  public final void shouldInterceptAndAppendSignature() throws Exception {
    server.enqueue(new MockResponse().setResponseCode(HttpStatus.SC_ACCEPTED).setBody("{}"));


    final Call<MultiAssetPageResourceDto> caller =
      assetRetrofitService.mastIhsQuery(new PrioritisedAbstractQueryDto(), "publishedVersion.name", "ASC");
    caller.execute().body();

    final RecordedRequest request = server.takeRequest();
    final String identity = request.getHeader("X-Client-Identity");
    assertNotNull(identity);
    assertEquals("cdf", identity);

    final String signature = request.getHeader("X-Message-Signature");
    assertNotNull(signature);
    verifySignature(signature, getUriKey(request));
  }

  /**
   * Injected interceptor.
   */
  @Autowired
  private AddMastSignatureInterceptor interceptor;

  /**
   * Should intercept request and append user and groups header.
   *
   * @throws Exception when error.
   */
  @Test
  public final void shouldInterceptAddAppendUserAndGroupHeader() throws Exception {
    server.enqueue(new MockResponse().setResponseCode(HttpStatus.SC_ACCEPTED).setBody("{}"));
    interceptor.setMastSecurityEnabled(false);

    final Call<MultiAssetPageResourceDto> caller =
      assetRetrofitService.mastIhsQuery(new PrioritisedAbstractQueryDto(), "publishedVersion.name", "ASC");
    caller.execute().body();

    final RecordedRequest request = server.takeRequest();
    final String userHeader = request.getHeader("user");
    assertNotNull(userHeader);
    assertEquals(TEST_USER, userHeader);

    final String groupsHeader = request.getHeader("groups");
    assertNotNull(groupsHeader);
    assertEquals(TEST_GROUP, groupsHeader);
  }

  /**
   * Generate uri key from request.
   *
   * @param recordedRequest request.
   * @return uri key.
   */
  private String getUriKey(final RecordedRequest recordedRequest) {
    final StringBuilder key = new StringBuilder(255);

    final String body = recordedRequest.getBody().readString(Charset.defaultCharset());
    String url = recordedRequest.getRequestLine();
    url = url.substring(url.indexOf("/mast"), url.lastIndexOf(" "));

    key.append(recordedRequest.getMethod().toUpperCase()).append(':')
      .append(url).append('[').append(body).append(']');
    LOGGER.debug("UriKey: {} ", key);
    return key.toString();
  }

  /**
   * Verify signature generated.
   *
   * @param signature checksum.
   * @param uriKey formatted request.
   * @throws Exception when error.
   */
  private void verifySignature(final String signature, final String uriKey) throws Exception {
    try {
      final MastSignature mastSignature = securityDAO.getActiveMastSignature();

      final PEMParser pemParser = new PEMParser(new StringReader(mastSignature.getPublicKey()));
      final SubjectPublicKeyInfo publicKeyInfo = (SubjectPublicKeyInfo) pemParser.readObject();
      final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
      final PublicKey publicKey = converter.getPublicKey(publicKeyInfo);

      final Signature verifier = Signature.getInstance(mastSignature.getAlgorithm());
      verifier.initVerify(publicKey);
      verifier.update(uriKey.getBytes("UTF-8"));
      final String checkSum = signature.substring(0, signature.indexOf("="));
      final byte[] checksum = Hex.decodeHex(checkSum.toCharArray());
      assertTrue(verifier.verify(checksum));
    } catch (IOException | NoSuchAlgorithmException
      | InvalidKeyException | SignatureException | DecoderException exception) {
      exception.printStackTrace();
    }
  }

  /**
   * Internal config.
   */
  @Configuration
  public static class Config {

    /**
     * Create new mock web server.
     * @return mock web server.
     */
    @Bean
    public MockWebServer mockWebServer() {
      return new MockWebServer();
    }

    /**
     * Create retrofit factory.
     *
     * @param mockWebServer injected mock web server.
     * @return retrofit factory.
     */
    @Bean
    public RetrofitFactory retrofitFactory(final MockWebServer mockWebServer) {
      final RetrofitFactory factory = new RetrofitFactory();
      factory.setBaseUrl(mockWebServer.url("mast/api/v1/").toString());
      List<Class<? extends Interceptor>> interceptorList = new ArrayList<>();
      interceptorList.add(AddMastSignatureInterceptor.class);
      interceptorList.add(LoggingInterceptor.class);
      factory.setInterceptorList(interceptorList);
      return factory;
    }

    /**
     * Create retrofit.
     *
     * @param retrofitFactory injected factory.
     * @return retrofit
     *
     */
    @Bean
    public Retrofit retrofit(final RetrofitFactory retrofitFactory) {
      return retrofitFactory.createRetrofit();
    }

    /**
     * Create one interceptor.
     *
     * @return interceptor.
     */
    @Bean
    public AddMastSignatureInterceptor interceptor() {
      final AddMastSignatureInterceptor interceptor = spy(new AddMastSignatureInterceptor());
      interceptor.setMastSecurityEnabled(true);
      interceptor.setTestUserName(TEST_USER);
      interceptor.setTestGroupName(TEST_GROUP);

      return interceptor;
    }

    /**
     * Add logging interceptor.
     *
     * @return logging interceptor.
     */
    @Bean
    public LoggingInterceptor loggingInterceptor() {
      return new LoggingInterceptor();
    }

    /**
     * Create retrofit service factory.
     *
     * @return retrofit service factory.
     *
     */
    @Bean
    public RetrofitServiceFactory retrofitServiceFactory() {
      return new RetrofitServiceFactory();
    }

    /**
     * Create service.
     *
     * @param factory injected.
     * @return new service.
     *
     */
    @Bean
    public AssetRetrofitService assetReferenceRetrofitService(final RetrofitServiceFactory factory) {
      return factory.createAssetRetrofitService();
    }

    /**
     * Mock security dao.
     *
     * @return security dao.
     * @throws Exception when error.
     */
    @Bean
    public SecurityDAO securityDAO() throws Exception {
      final SecurityDAO securityDAO = mock(SecurityDAO.class);

      final MastSignature mastSignature = new MastSignature();
      mastSignature.setId(1);
      mastSignature.setPrivateKey(
        "-----BEGIN RSA PRIVATE KEY-----\n" + "MIIEoQIBAAKCAQEAprEMyGhqY1FUgmE50dnaqWAMRucLvGnkVhu7H6PCTVVfKoDY\n"
          + "uK53y3FQDa7BBHrxN8vVIjeeTkErkjyHQbg7f2ZI5hNVgoPL4bg3pUlKhmW+cqK3\n"
          + "sNVE9NdAlUQkiAzKLBOw0f+MyGRlvd/agKa2YtfE+zdPkKtAP2Z7bSLV0HcShu22\n"
          + "xKzEEp4AE2ewXR3Qn17Nd+wsCpM4nqDsxuvjZd5/N/YFtJyPTIZlLhX204gi2AC0\n"
          + "6BTwmY0nNhyq8kUjG1Iqgs5xaN5xaSiW0UjjHymxjXJhCYwG7oG/5Q3mAwq/dOjH\n"
          + "qOQ/oSHguL7FKdrf6ZaY4tiJstpXQ5MdGDCNAwIBJQKCAQBjHSox+OVPz35bXGeR\n"
          + "iHQtW7RF1XyZixIlXJjGtGWxcQgnHC3VmCuNuP8quu9OxaQvA5N8IRIFBCe+vDS/\n"
          + "SvLxzh2B4ft+CS0ebYje0aHvDBBf1l9iOZ6fazsoUgfiMR5DuK5TWa2nlaRHYoHr\n"
          + "oWWG4SkEEwy24l2NezShwa+QtImsOIBkdvbYja5bO7SQs6ue5OWc3R4hGEC/JDQZ\n"
          + "vpYFHVwSR84I87dlbTzRzgilAgrCcNdGE1IWtsTV2k6JSUr+zZStehm8VoFHfEQ2\n"
          + "17/fZMrLEO6f9/A62gsgm3TBwDTmM15iE+rbdx9re4emRkgkAgMh2Y747GrEDAW/\n"
          + "SIMNAoGBAOUG26PShk7M4gZdsepPH1jmtu2PJ3dVaeFZs3O/Qz89aipeC+V7Sc+Y\n"
          + "F2GUUlOCXVTkSWHYQRgh54EkwFx9mAr5BvBFkYTwH/L8o4+nAmYv9BZd9XWCKuTV\n"
          + "Edv9YmRNjtLhE9Qtf1p8QxTezJECSgfmk99uoosEZVfTPgcbyDkJAoGBALpSylJJ\n"
          + "L9YfWVR5XP8aob/QVXUD3HZmOrvU6sRQuZCUN1FGznRBilEVfR1QLvHekBjyhS7U\n"
          + "J7ci5qDYPpvTmo5efR1IBth2LQIXNAjOCi6aaS7Z97dLy8hCGHQZjsj7RE3K26Yz\n"
          + "iTc/Z+h2onLtA16vLeK7SBndRHpsb2lpP9SrAoGBAK1RTESfUOGvzaPz4JWjqQUB\n"
          + "nzBQqD6hgI7cGRlSeBQuedrfYvLZ1v32jjwBimi1rmnBg6P9mQtX7XZ8rT8S75mZ\n"
          + "27y/BlbDjcyjgrHmK1Q/9vwrbaUIjym1/61lzesKQpG/FeyszxQLAlT7obnYOAX6\n"
          + "mWrQQ6d48r8AuVF8zt8NAoGAc9KZcWvDykrWqiHts1W+fiePEWND2uWaIbu0h98n\n"
          + "PjKe7VWHQVksd5e8egFpSj495f6KJAdW9U0L53GqYN17wEiS9oa4JbE+kpjNUZTV\n"
          + "5Z5BYlAkXS8dz4MISCuQHBHQg2Js3O+afEoCUjvocPQyhvdGCXtWVURa//43awoS\n"
          + "6/sCgYAHt7V/ICXcANY3FKgkO9uw7IbC8QJyVqM17dNBpjj4PVihBUBcbfQ0xy2n\n"
          + "/vnHYohOATiju0A7XyTawDv/sUhwVxG9wuBnrvev+oTkqMIFWMnqRiH6Wp6nmKkQ\n"
          + "FU6wtrZ0cIipQG0cN2tv7FHaYKfcNCjek38oyPdEPaEinkNgUw==\n" + "-----END RSA PRIVATE KEY-----\n");
      mastSignature.setPublicKey(
        "-----BEGIN PUBLIC KEY-----\n" + "MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAprEMyGhqY1FUgmE50dna\n"
          + "qWAMRucLvGnkVhu7H6PCTVVfKoDYuK53y3FQDa7BBHrxN8vVIjeeTkErkjyHQbg7\n"
          + "f2ZI5hNVgoPL4bg3pUlKhmW+cqK3sNVE9NdAlUQkiAzKLBOw0f+MyGRlvd/agKa2\n"
          + "YtfE+zdPkKtAP2Z7bSLV0HcShu22xKzEEp4AE2ewXR3Qn17Nd+wsCpM4nqDsxuvj\n"
          + "Zd5/N/YFtJyPTIZlLhX204gi2AC06BTwmY0nNhyq8kUjG1Iqgs5xaN5xaSiW0Ujj\n"
          + "HymxjXJhCYwG7oG/5Q3mAwq/dOjHqOQ/oSHguL7FKdrf6ZaY4tiJstpXQ5MdGDCN\n" + "AwIBJQ==\n"
          + "-----END PUBLIC KEY-----\n");
      mastSignature.setAlgorithm("SHA1WithRSA");
      mastSignature.setDescription("Default signature.");
      mastSignature.setEnabled(true);
      mastSignature.setMastId("cdf");
      when(securityDAO.getActiveMastSignature()).thenReturn(mastSignature);
      return securityDAO;
    }
  }
}
