package com.baesystems.ai.lr.cd.be.domain.dao.reference.test;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.PortRegistryService;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author vkovuru
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class PortRegistryServiceTest {
  /**
   * Logger log4j.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(PortRegistryServiceTest.class);
  /**
   * Injected assetReferenceService.
   *
   */
  @Autowired(required = false)
  private PortRegistryService portRegistryService;

  /**
   * @throws Exception exception
   */
  @Test
  public final void testPortRegistryService() throws Exception {
    LOGGER.debug("Successfully fetch asset types.");
    Assert.assertNotNull(portRegistryService);
  }

  /**
   * Inner config.
   */
  @Configuration
  @ImportResource("classpath:test-context.xml")
  public static class Config {

    /**
     * Security dao mock.
     *
     * @return mock dao.
     */
    @Bean
    public SecurityDAO securityDAO() {
      return Mockito.mock(SecurityDAO.class);
    }
  }
}
