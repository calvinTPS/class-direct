package com.baesystems.ai.lr.cd.be.domain.dao.reference.test;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.JobReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test unit for JobReferenceService.
 *
 * @author yng
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class JobReferenceRetrofitServiceTest {
  /**
   * Logger log4j.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(JobReferenceRetrofitServiceTest.class);

  /**
   * Injected jobReferenceService.
   *
   */
  @Autowired(required = false)
  private JobReferenceRetrofitService jobReferenceService;

  /**
   * JUnit test to test fetch.
   *
   * @throws Exception value.
   *
   */
  @Test
  public final void testJobReferenceService() throws Exception {
    LOGGER.debug("Successfully wired jobReferenceService.");
    Assert.assertNotNull(jobReferenceService);
  }

  /**
   * Inner config.
   */
  @Configuration
  @ImportResource("classpath:test-context.xml")
  public static class Config {

    /**
     * Security dao mock.
     *
     * @return mock dao.
     */
    @Bean
    public SecurityDAO securityDAO() {
      return Mockito.mock(SecurityDAO.class);
    }
  }
}
