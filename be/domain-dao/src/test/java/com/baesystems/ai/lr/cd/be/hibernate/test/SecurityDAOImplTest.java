package com.baesystems.ai.lr.cd.be.hibernate.test;

import static com.baesystems.ai.lr.cd.be.hibernate.test.UserId.ALICE;
import static com.baesystems.ai.lr.cd.be.hibernate.test.UserRoleId.EQUASIS;


import com.baesystems.ai.lr.cd.be.domain.dto.security.MastSignature;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl.SecurityDAOImpl;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserRoles;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author vkovuru
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SecurityDAOImplTest {

  /**
   * mock security dao.
   */
  @InjectMocks
  private final SecurityDAO dao = new SecurityDAOImpl();

  /**
   * mock entity manager.
   */
  @Mock
  private EntityManager entityManager;

  /**
   * mock typed query.
   */
  @Mock
  private TypedQuery<UserRoles> typedQuery;

  /**
   * mock typed query.
   */
  @Mock
  private TypedQuery<Roles> typedQueryRoles;

  /**
   * test security dao.
   */
  @Test
  public final void hasValidRoles() {

    List<String> roles = new ArrayList<>();

    roles.add(Role.EQUASIS.toString());

    Mockito
        .when(this.entityManager.createQuery("SELECT ur from UserRoles ur LEFT JOIN FETCH ur.role ro LEFT "
            + "JOIN FETCH ur.user u WHERE ro.roleName in :roles and u.userId= :userId ", UserRoles.class))
        .thenReturn(typedQuery);

    Mockito.when(this.typedQuery.setParameter(Mockito.anyString(), Mockito.anyListOf(String.class)))
        .thenReturn(typedQuery);

    Mockito.when(this.typedQuery.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(typedQuery);

    Mockito.when(this.typedQuery.getResultList()).thenReturn(mockUserRoles());

    Assert.assertTrue(dao.hasRole("user", roles));
  }


  /**
   * test security dao.
   */
  @Test
  public final void hasEmptyRoles() {

    List<String> roles = new ArrayList<>();
    roles.add(Role.EQUASIS.toString());

    Mockito
        .when(this.entityManager.createQuery("SELECT ur from UserRoles ur LEFT JOIN FETCH ur.role ro LEFT "
            + "JOIN FETCH ur.user u WHERE ro.roleName in :roles and u.userId= :userId ", UserRoles.class))
        .thenReturn(typedQuery);

    Mockito.when(this.typedQuery.setParameter(Mockito.anyString(), Mockito.anyListOf(String.class)))
        .thenReturn(typedQuery);

    Mockito.when(this.typedQuery.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(typedQuery);

    roles.add("NOVALUE");

    Assert.assertFalse(dao.hasRole("user", roles));

    Mockito.when(this.typedQuery.getResultList()).thenReturn(null);
  }


  /**
   * test security dao.
   */
  @Test
  public final void hasNullRoles() {
    List<String> roles = new ArrayList<>();
    roles.add(Role.EQUASIS.toString());
    Mockito
        .when(this.entityManager.createQuery("SELECT ur from UserRoles ur LEFT JOIN FETCH ur.role ro LEFT "
            + "JOIN FETCH ur.user u WHERE ro.roleName in :roles and u.userId= :userId ", UserRoles.class))
        .thenReturn(typedQuery);

    Mockito.when(this.typedQuery.setParameter(Mockito.anyString(), Mockito.anyListOf(String.class)))
        .thenReturn(typedQuery);
    Mockito.when(this.typedQuery.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(typedQuery);
    Mockito.when(this.typedQuery.getResultList()).thenReturn(null);
    roles.clear();
    roles.add(Role.EQUASIS.toString());

    Assert.assertFalse(dao.hasRole("user", roles));
  }

  /**
   * Should return active mast signature.
   * @throws Exception when error.
   */
  @Test
  public final void shouldReturnMastSignature() throws Exception {
    final MastSignature mastSignature = new MastSignature();
    mastSignature.setId(1);
    mastSignature.setPrivateKey(
      "-----BEGIN RSA PRIVATE KEY-----\n" + "MIIEoQIBAAKCAQEAprEMyGhqY1FUgmE50dnaqWAMRucLvGnkVhu7H6PCTVVfKoDY\n"
        + "uK53y3FQDa7BBHrxN8vVIjeeTkErkjyHQbg7f2ZI5hNVgoPL4bg3pUlKhmW+cqK3\n"
        + "sNVE9NdAlUQkiAzKLBOw0f+MyGRlvd/agKa2YtfE+zdPkKtAP2Z7bSLV0HcShu22\n"
        + "xKzEEp4AE2ewXR3Qn17Nd+wsCpM4nqDsxuvjZd5/N/YFtJyPTIZlLhX204gi2AC0\n"
        + "6BTwmY0nNhyq8kUjG1Iqgs5xaN5xaSiW0UjjHymxjXJhCYwG7oG/5Q3mAwq/dOjH\n"
        + "qOQ/oSHguL7FKdrf6ZaY4tiJstpXQ5MdGDCNAwIBJQKCAQBjHSox+OVPz35bXGeR\n"
        + "iHQtW7RF1XyZixIlXJjGtGWxcQgnHC3VmCuNuP8quu9OxaQvA5N8IRIFBCe+vDS/\n"
        + "SvLxzh2B4ft+CS0ebYje0aHvDBBf1l9iOZ6fazsoUgfiMR5DuK5TWa2nlaRHYoHr\n"
        + "oWWG4SkEEwy24l2NezShwa+QtImsOIBkdvbYja5bO7SQs6ue5OWc3R4hGEC/JDQZ\n"
        + "vpYFHVwSR84I87dlbTzRzgilAgrCcNdGE1IWtsTV2k6JSUr+zZStehm8VoFHfEQ2\n"
        + "17/fZMrLEO6f9/A62gsgm3TBwDTmM15iE+rbdx9re4emRkgkAgMh2Y747GrEDAW/\n"
        + "SIMNAoGBAOUG26PShk7M4gZdsepPH1jmtu2PJ3dVaeFZs3O/Qz89aipeC+V7Sc+Y\n"
        + "F2GUUlOCXVTkSWHYQRgh54EkwFx9mAr5BvBFkYTwH/L8o4+nAmYv9BZd9XWCKuTV\n"
        + "Edv9YmRNjtLhE9Qtf1p8QxTezJECSgfmk99uoosEZVfTPgcbyDkJAoGBALpSylJJ\n"
        + "L9YfWVR5XP8aob/QVXUD3HZmOrvU6sRQuZCUN1FGznRBilEVfR1QLvHekBjyhS7U\n"
        + "J7ci5qDYPpvTmo5efR1IBth2LQIXNAjOCi6aaS7Z97dLy8hCGHQZjsj7RE3K26Yz\n"
        + "iTc/Z+h2onLtA16vLeK7SBndRHpsb2lpP9SrAoGBAK1RTESfUOGvzaPz4JWjqQUB\n"
        + "nzBQqD6hgI7cGRlSeBQuedrfYvLZ1v32jjwBimi1rmnBg6P9mQtX7XZ8rT8S75mZ\n"
        + "27y/BlbDjcyjgrHmK1Q/9vwrbaUIjym1/61lzesKQpG/FeyszxQLAlT7obnYOAX6\n"
        + "mWrQQ6d48r8AuVF8zt8NAoGAc9KZcWvDykrWqiHts1W+fiePEWND2uWaIbu0h98n\n"
        + "PjKe7VWHQVksd5e8egFpSj495f6KJAdW9U0L53GqYN17wEiS9oa4JbE+kpjNUZTV\n"
        + "5Z5BYlAkXS8dz4MISCuQHBHQg2Js3O+afEoCUjvocPQyhvdGCXtWVURa//43awoS\n"
        + "6/sCgYAHt7V/ICXcANY3FKgkO9uw7IbC8QJyVqM17dNBpjj4PVihBUBcbfQ0xy2n\n"
        + "/vnHYohOATiju0A7XyTawDv/sUhwVxG9wuBnrvev+oTkqMIFWMnqRiH6Wp6nmKkQ\n"
        + "FU6wtrZ0cIipQG0cN2tv7FHaYKfcNCjek38oyPdEPaEinkNgUw==\n" + "-----END RSA PRIVATE KEY-----\n");
    mastSignature.setPublicKey(
      "-----BEGIN PUBLIC KEY-----\n" + "MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAprEMyGhqY1FUgmE50dna\n"
        + "qWAMRucLvGnkVhu7H6PCTVVfKoDYuK53y3FQDa7BBHrxN8vVIjeeTkErkjyHQbg7\n"
        + "f2ZI5hNVgoPL4bg3pUlKhmW+cqK3sNVE9NdAlUQkiAzKLBOw0f+MyGRlvd/agKa2\n"
        + "YtfE+zdPkKtAP2Z7bSLV0HcShu22xKzEEp4AE2ewXR3Qn17Nd+wsCpM4nqDsxuvj\n"
        + "Zd5/N/YFtJyPTIZlLhX204gi2AC06BTwmY0nNhyq8kUjG1Iqgs5xaN5xaSiW0Ujj\n"
        + "HymxjXJhCYwG7oG/5Q3mAwq/dOjHqOQ/oSHguL7FKdrf6ZaY4tiJstpXQ5MdGDCN\n" + "AwIBJQ==\n"
        + "-----END PUBLIC KEY-----\n");
    mastSignature.setAlgorithm("SHA1WithRSA");
    mastSignature.setDescription("Default signature.");
    mastSignature.setEnabled(true);
    mastSignature.setMastId("cdf");
    final TypedQuery<MastSignature> query = Mockito.mock(TypedQuery.class);
    Mockito.when(query.getResultList()).thenReturn(Collections.singletonList(mastSignature));
    Mockito.when(this.entityManager.createQuery(Mockito.anyString(), Mockito.eq(MastSignature.class)))
      .thenReturn(query);

    final MastSignature signature = dao.getActiveMastSignature();
    assertNotNull(signature);
    assertNotNull(signature.getAlgorithm());
    assertNotNull(signature.getId());
    assertNotNull(signature.getMastId());
    assertNotNull(signature.getPrivateKey());
    assertNotNull(signature.getPublicKey());
    assertTrue(signature.getEnabled());
  }

  /**
   * @return list of UserRoles.
   */
  private List<UserRoles> mockUserRoles() {

    List<UserRoles> roles = new ArrayList<>();
    Roles role = new Roles();
    role.setRoleId(EQUASIS.getId());
    role.setRoleName(EQUASIS.getName());

    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());

    UserRoles ur = new UserRoles();
    ur.setRole(role);
    ur.setUser(up);
    roles.add(ur);
    return roles;
  }
}


