package com.baesystems.ai.lr.cd.be.hibernate.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AwsGlacierAuditDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl.AwsGlacierAuditDaoImpl;
import com.baesystems.ai.lr.cd.be.domain.repositories.AwsGlacierAudit;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author fwijaya on 6/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AwsGlacierAuditDaoTest {
  /**
   * Archive id.
   */
  private static final String ARCHIVE_ID = "1";
  /**
   * Vault name.
   */
  private static final String VAULT_NAME = "ClassDirectTestArchive";
  /**
   * Mock typed query.
   */
  @Mock
  private TypedQuery<AwsGlacierAudit> typedQuery;
  /**
   * Injected dao.
   */
  @InjectMocks
  private AwsGlacierAuditDao awsGlacierAuditDao = new AwsGlacierAuditDaoImpl();
  /**
   * Mock entity manager.
   */
  @Mock
  private EntityManager entityManager;

  /**
   * Test getting aws audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void successfulAuditFind() throws ClassDirectException {
    when(typedQuery.getSingleResult()).thenReturn(new AwsGlacierAudit());
    when(typedQuery.setParameter(anyString(), anyString())).thenReturn(typedQuery);
    when(entityManager.createQuery(anyString(), eq(AwsGlacierAudit.class))).thenReturn(typedQuery);

    final AwsGlacierAudit awsGlacierAudit =
      awsGlacierAuditDao.findAuditByArchiveIdAndVaultName(ARCHIVE_ID, VAULT_NAME);
    assertNotNull(awsGlacierAudit);
  }

  /**
   * Test fail finding audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void failFindingAudit() throws ClassDirectException {
    doThrow(PersistenceException.class).when(typedQuery).getSingleResult();
    when(typedQuery.setParameter(anyString(), anyString())).thenReturn(typedQuery);
    when(entityManager.createQuery(anyString(), eq(AwsGlacierAudit.class))).thenReturn(typedQuery);
    awsGlacierAuditDao.findAuditByArchiveIdAndVaultName(ARCHIVE_ID, VAULT_NAME);
  }

  /**
   * Test successfully save audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void successfulAuditSave() throws ClassDirectException {
    final AwsGlacierAudit awsGlacierAudit =
      awsGlacierAuditDao.storeArchive(ARCHIVE_ID, VAULT_NAME, "", LocalDateTime.now(), "");
    assertNotNull(awsGlacierAudit);
    verify(entityManager, atLeastOnce()).persist(any(AwsGlacierAudit.class));
  }

  /**
   * Test fail saving audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void failSavingAudit() throws ClassDirectException {
    doThrow(PersistenceException.class).when(entityManager).persist(any(AwsGlacierAudit.class));
    final AwsGlacierAudit awsGlacierAudit =
      awsGlacierAuditDao.storeArchive(ARCHIVE_ID, VAULT_NAME, "", LocalDateTime.now(), "");
  }
}
