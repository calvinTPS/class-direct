package com.baesystems.ai.lr.cd.be.hibernate.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FlagsAssociationsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.FlagsAssociations;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Tests the DAO class {@link FlagsAssociationsDaoImpl}.
 * Coverage: class: 100%, method: 100%, line: 81%.
 *
 * @author fwijaya on 20/4/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao-context.xml")
public class FlagsAssociationsDaoTest {

  /**
   * Just a constant.
   */
  private static final int ZERO = 0;
  /**
   * Just a constant.
   */
  private static final int ONE = 1;
  /**
   * Flag code 1.
   */
  private static final String FLAG_CODE_1 = "ABC";
  /**
   * Secondary flag code 1.
   */
  private static final String SECONDARY_FLAG_CODE_1 = "DEF";
  /**
   * Secondary flag code 2.
   */
  private static final String SECONDARY_FLAG_CODE_2 = "GHI";
  /**
   * A random flag code.
   */
  private static final String RANDOM_FLAG_CODE = "You are ugly!";
  /**
   * The DAO.
   */
  @Autowired
  private FlagsAssociationsDao flagsAssociationsDao;
  /**
   * Test instance.
   */
  private FlagsAssociations flagsAssociations;

  /**
   * Setting up test instance before tests are run.
   */
  @Before
  public final void setup() {
    flagsAssociations = new FlagsAssociations();
    flagsAssociations.setFlagCode(FLAG_CODE_1);
    flagsAssociations.setSecondaryFlagCode(SECONDARY_FLAG_CODE_1);
  }

  /**
   * Tests adding an association, then getting it, and removing it.
   * This all is best done in one unit test because:
   * 1. You can't remove an association unless you have added it.
   * 2. After adding an association you should remove it.
   *
   * @throws ClassDirectException if any unchecked exception is thrown.
   */
  @Test
  public final void testAddAndRemoveAssociation() throws ClassDirectException {
    // Add flag association
    flagsAssociationsDao.addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    // Check that it has been added
    // Get the secondary flag code
    List<FlagsAssociations> flagsAssociationsLocal =
      flagsAssociationsDao.getFlagAssocisationsForFlagCode(FLAG_CODE_1);
    Assert.assertNotNull(flagsAssociationsLocal);
    Assert.assertEquals(flagsAssociationsLocal.get(ZERO).getSecondaryFlagCode(), SECONDARY_FLAG_CODE_1);
    // Get the flag code
    flagsAssociationsLocal = flagsAssociationsDao.getFlagAssocisationsForSecondaryFlagCode(SECONDARY_FLAG_CODE_1);
    Assert.assertNotNull(flagsAssociationsLocal);
    Assert.assertEquals(flagsAssociationsLocal.get(ZERO).getFlagCode(), FLAG_CODE_1);
    // Remove the newly added association
    flagsAssociationsDao.removeAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    // Test that it has been removed
    flagsAssociationsLocal = flagsAssociationsDao.getFlagAssocisationsForFlagCode(FLAG_CODE_1);
    Assert.assertNotNull(flagsAssociationsLocal);
    Assert.assertEquals(flagsAssociationsLocal.size(), ZERO);
  }

  /**
   * Tests adding too long flag code.
   *
   * @throws ClassDirectException if any unchecked exception is thrown.
   */
  @Test(expected = Exception.class)
  public final void testAddingLongFlagAssociations() throws ClassDirectException {
    flagsAssociationsDao.addAssociation(FLAG_CODE_1, RANDOM_FLAG_CODE);
  }

  /**
   * Tests getting associated flag.
   *
   * @throws ClassDirectException if any unchecked exception is thrown.
   */
  @Test
  public final void testGetAssociatedFlag() throws ClassDirectException {
    // This should return the secondary flag
    String associatedFlag = flagsAssociationsDao.getAssociatedFlag(flagsAssociations, FLAG_CODE_1);
    Assert.assertEquals(associatedFlag, SECONDARY_FLAG_CODE_1);
    // This should return the primary flag
    associatedFlag = flagsAssociationsDao.getAssociatedFlag(flagsAssociations, SECONDARY_FLAG_CODE_1);
    Assert.assertEquals(associatedFlag, FLAG_CODE_1);
    // This should return null
    associatedFlag = flagsAssociationsDao.getAssociatedFlag(flagsAssociations, RANDOM_FLAG_CODE);
    Assert.assertEquals(associatedFlag, null);
  }

  /**
   * Tests the following case: for a primary flag with two secondary flags, getting associated flag for
   * primary flag will return both the secondary flags.
   *
   * @throws ClassDirectException if any unchecked exception is thrown.
   */
  @Test
  public final void testGettingAssociatedFlagsForPrimaryFlag() throws ClassDirectException {
    // Add flag associations
    flagsAssociationsDao.addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    flagsAssociationsDao.addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_2);
    // Get the secondary flag codes
    final List<FlagsAssociations> flagsAssociationsLocal =
      flagsAssociationsDao.getFlagAssocisationsForFlagCode(FLAG_CODE_1);
    Assert.assertNotNull(flagsAssociationsLocal);
    final List<String> secondaryFlags = flagsAssociationsLocal.stream()
      .map(flagsAssociation -> flagsAssociationsDao.getAssociatedFlag(flagsAssociation, FLAG_CODE_1))
      .collect(Collectors.toList());
    Collections.sort(secondaryFlags);
    // Assert that we get both the secondary flags
    Assert.assertEquals(secondaryFlags.get(ZERO), SECONDARY_FLAG_CODE_1);
    Assert.assertEquals(secondaryFlags.get(ONE), SECONDARY_FLAG_CODE_2);
    // Remove flag associations
    flagsAssociationsDao.removeAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    flagsAssociationsDao.removeAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_2);
  }

  /**
   * Tests the following case: for a primary flag with two secondary flags, getting associated flag for
   * a secondary flag will return only the primary flag.
   *
   * @throws ClassDirectException if any unchecked exception is thrown.
   */
  @Test
  public final void testGettingAssociatedFlagsForSecondaryFlag() throws ClassDirectException {
    // Add flag associations
    flagsAssociationsDao.addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    flagsAssociationsDao.addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_2);
    // Get the primary flag codes
    final List<FlagsAssociations> flagsAssociationsLocal =
      flagsAssociationsDao.getFlagAssocisationsForSecondaryFlagCode(SECONDARY_FLAG_CODE_1);
    Assert.assertNotNull(flagsAssociationsLocal);
    // Assert that we get only the primary flag, not the sibling/other secondary flag
    Assert.assertEquals(flagsAssociationsLocal.size(), ONE);
    Assert.assertEquals(flagsAssociationsLocal.get(ZERO).getFlagCode(), FLAG_CODE_1);
    // Remove flag associations
    flagsAssociationsDao.removeAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    flagsAssociationsDao.removeAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_2);
  }
}
