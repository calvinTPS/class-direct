package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AvailabilityDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl.AvailabilityDaoImpl;
import com.baesystems.ai.lr.cd.be.domain.repositories.SystemMaintenance;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AvailabilityDao}.
 *
 * @author Faizal Sidek
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class AvailabilityDaoTest {

  /**
   * Injected dao.
   */
  @InjectMocks
  private AvailabilityDao availabilityDao = new AvailabilityDaoImpl();

  /**
   * Inject mock.
   */
  @Mock
  private EntityManager entityManager;

  /**
   * Inject typed query.
   */
  @Mock
  private TypedQuery<SystemMaintenance> systemMaintenanceTypedQuery;

  /**
   * Will return a real object.
   */
  @Test
  public final void shouldGetActiveSystemMaintenance() {
    when(systemMaintenanceTypedQuery.getSingleResult()).thenReturn(new SystemMaintenance());
    when(entityManager.createQuery(anyString(), eq(SystemMaintenance.class))).thenReturn(systemMaintenanceTypedQuery);

    final SystemMaintenance systemMaintenance = availabilityDao.getActiveSystemMaintenance();
    assertNotNull(systemMaintenance);
  }

  /**
   * Should return null when no maintenance.
   */
  @Test
  public final void shouldReturnNullWhenNoMaintenance() {
    when(systemMaintenanceTypedQuery.getSingleResult()).thenThrow(new NoResultException());
    when(entityManager.createQuery(anyString(), eq(SystemMaintenance.class))).thenReturn(systemMaintenanceTypedQuery);

    final SystemMaintenance systemMaintenance = availabilityDao.getActiveSystemMaintenance();
    assertNull(systemMaintenance);
  }
}
