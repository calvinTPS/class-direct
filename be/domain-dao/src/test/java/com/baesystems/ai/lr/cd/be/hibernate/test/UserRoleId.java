package com.baesystems.ai.lr.cd.be.hibernate.test;

/**
 * @author Faizal Sidek
 */
public enum UserRoleId {
  /**
   * Enum of user role.
   */
  EQUASIS(101L, "EQUASIS"), THESIS(102L, "THESIS"), EOR(103L, "EOR");

  /**
   * Role id.
   */
  private Long id;

  /**
   * Role name.
   */
  private String name;

  /**
   * Private constructor.
   *
   * @param roleId role id.
   * @param roleName role name.
   */
  UserRoleId(final Long roleId, final String roleName) {
    this.id = roleId;
    this.name = roleName;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return role id
   */
  public Long getId() {
    return id;
  }

  /**
   * Getter for {@link #name}.
   *
   * @return role name
   */
  public String getName() {
    return name;
  }
}
