package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.test;

import com.baesystems.ai.lr.cd.be.domain.dao.LRADRetrofitServiceFactory;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.InternalUserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.AzureAccessToken;
import com.baesystems.ai.lr.cd.be.domain.retrofit.LRADRetrofitFactory;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.LRADInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Interceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.spy;

/**
 * Unit test for {@link LRADInterceptor}.
 *
 * @author YWearn
 * @author fwijaya
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class LRADInterceptorTest {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(LRADInterceptorTest.class);

  /**
   * Azure Grant_type for authentication.
   */
  private static final String GRANT_TYPE = "client_credentials";
  /**
   * Azure client_id for authentication.
   */
  private static final String CLIENT_ID = "CLIENT123";
  /**
   * Azure client_secret for authentication.
   */
  private static final String CLIENT_SECRET = "SECRET123";
  /**
   * Azure api version for all request.
   */
  private static final String API_VERSION = "1.6";
  /**
   * Expected request body for getting authorisation token.
   */
  private static final String EXPECTED_AUTH_BODY =
    "grant_type=client_credentials" + "&client_id=CLIENT123&client_secret=SECRET123" + "&resource=&scope=";
  /**
   * Hour in seconds.
   */
  private static final long HOUR_IN_SEC = 3600L;
  /**
   * Hour in seconds.
   */
  private static final long ONE_SECOND = 1L;
  /**
   * Injected server.
   */
  @Autowired
  private MockWebServer server;

  /**
   * Injected retrofit service.
   */
  @Autowired
  private InternalUserRetrofitService userRetrofitService;

  /**
   * Injected AzureAuthInterceptor.
   */
  @Autowired
  private LRADInterceptor azureAuthInterceptor;

  /**
   * Ensure Azure access token is acquired before fire actual graph api.
   *
   * @throws Exception exception if any.
   */
  @Test
  public final void shouldInterceptAddAuthorisationHeader() throws Exception {
    final AzureAccessToken token = mockAzureToken();
    final ObjectMapper mapper = new ObjectMapper();
    final String authJson = mapper.writeValueAsString(token);

    // clear the token from previous test in case cached.
    azureAuthInterceptor.clearToken();

    final MockResponse authResp = new MockResponse();
    authResp.setHeader("Content-Type", "application/json; charset=utf-8");
    authResp.setResponseCode(HttpStatus.SC_OK);
    authResp.setBody(authJson);
    server.enqueue(authResp);
    // Second one for actual Azure
    server.enqueue(new MockResponse().setHeader("Content-Type", "application/json; charset=utf-8")
                     .setResponseCode(HttpStatus.SC_OK).setBody("[]"));

    final Call<List<LRIDUserDto>> caller = userRetrofitService.getInternalUsers("");
    try {
      caller.execute().body();
    } finally {
      final RecordedRequest authRequest = server.takeRequest(10, TimeUnit.SECONDS);
      validateAzureAuthRequest(authRequest);

      final RecordedRequest azureRequest = server.takeRequest(10, TimeUnit.SECONDS);
      validateAzureGraphAuthHeader(azureRequest, token);
    }
  }

  /**
   * To test throw exception when 401 return from Azure call. Note: Added Z as last method to run as
   * the server will be containing the mock request after exception.
   *
   * @throws Exception the IOException.
   */
  @Test(expected = IOException.class)
  public final void shouldZThrowIOExceptionWhenUnauthorise() throws Exception {
    final AzureAccessToken token = mockAzureToken();
    final ObjectMapper mapper = new ObjectMapper();
    final String authJson = mapper.writeValueAsString(token);

    // clear the token from previous test in case cached.
    azureAuthInterceptor.clearToken();

    // First on authorisation
    final MockResponse authResp = new MockResponse();
    authResp.setHeader("Content-Type", "application/json; charset=utf-8");
    authResp.setResponseCode(HttpStatus.SC_ACCEPTED);
    authResp.setBody(authJson);
    server.enqueue(authResp);

    // Second one for actual Azure
    server.enqueue(new MockResponse().setHeader("Content-Type", "application/json; charset=utf-8")
                     .setResponseCode(HttpStatus.SC_UNAUTHORIZED).setBody("[]"));

    try {
      final Call<List<LRIDUserDto>> caller = userRetrofitService.getInternalUsers("");
      caller.execute().body();
    } finally {
      // Clear out the server request
      server.takeRequest(ONE_SECOND, TimeUnit.SECONDS);
      server.takeRequest(ONE_SECOND, TimeUnit.SECONDS);
    }
  }

  /**
   * To test acquired a new token after it is expired.
   *
   * @throws Exception the exception when execute test.
   */
  @Test
  public final void shouldRefreshTokenWhenExpired() throws Exception {
    final ObjectMapper mapper = new ObjectMapper();
    String authJson = null;

    // clear the token from previous test in case cached.
    azureAuthInterceptor.clearToken();

    // First on authorisation
    final AzureAccessToken expiredToken = mockExpiredAzureToken();
    final MockResponse authExpiredNowResp = new MockResponse();
    authExpiredNowResp.setHeader("Content-Type", "application/json; charset=utf-8");
    authExpiredNowResp.setResponseCode(HttpStatus.SC_OK);
    authJson = mapper.writeValueAsString(expiredToken);
    authExpiredNowResp.setBody(authJson);
    server.enqueue(authExpiredNowResp);

    // Second one for actual Azure
    server.enqueue(new MockResponse().setHeader("Content-Type", "application/json; charset=utf-8")
                     .setResponseCode(HttpStatus.SC_OK).setBody("[]"));

    try {
      final Call<List<LRIDUserDto>> caller1 = userRetrofitService.getInternalUsers("");
      // First call, get expired soon token.
      caller1.execute().body();
    } finally {
      RecordedRequest authRequest = server.takeRequest(HOUR_IN_SEC, TimeUnit.SECONDS);
      validateAzureAuthRequest(authRequest);
      RecordedRequest azureRequest = server.takeRequest(HOUR_IN_SEC, TimeUnit.SECONDS);
      validateAzureGraphAuthHeader(azureRequest, expiredToken);
    }

    // Third one should refresh authorisation
    final AzureAccessToken token = mockExpiredAzureToken();
    final MockResponse authResp = new MockResponse();
    authResp.setHeader("Content-Type", "application/json; charset=utf-8");
    authResp.setResponseCode(HttpStatus.SC_OK);
    authJson = mapper.writeValueAsString(token);
    authResp.setBody(authJson);
    server.enqueue(authResp);

    // Fourth one should success for actual Azure
    server.enqueue(new MockResponse().setHeader("Content-Type", "application/json; charset=utf-8")
                     .setResponseCode(HttpStatus.SC_OK).setBody("[]"));

    // Second call get another new token.
    try {
      final Call<List<LRIDUserDto>> caller2 = userRetrofitService.getInternalUsers("");
      caller2.execute().body();
    } finally {
      final RecordedRequest authRequestRefresh = server.takeRequest();
      validateAzureAuthRequest(authRequestRefresh);
      final RecordedRequest azureRequest2 = server.takeRequest();
      validateAzureGraphAuthHeader(azureRequest2, token);
    }
  }

  /**
   * Validate the body of Azure authorization.
   *
   * @param request the http request to Azure authorization.
   */
  private void validateAzureAuthRequest(final RecordedRequest request) {
    Assert.assertNotNull(request);
    final String body = request.getBody().readUtf8();
    LOGGER.debug("Azure authorisation body: {}", body);
    Assert.assertEquals(EXPECTED_AUTH_BODY, body);
  }

  /**
   * Validate the azure request with authorization header.
   *
   * @param request the http request to Azure graph API.
   * @param token the mock Azure access token.
   */
  private void validateAzureGraphAuthHeader(final RecordedRequest request, final AzureAccessToken token) {
    Assert.assertNotNull(request);
    String authKey = request.getHeader("Authorization");
    LOGGER.debug("Azure Authorization Header: {}", authKey);
    Assert.assertEquals("Bearer " + token.getAccessToken(), authKey);
  }

  /**
   * Mock Expired Azure access token.
   *
   * @return the mock Azure access token.
   */
  private AzureAccessToken mockExpiredAzureToken() {
    final AzureAccessToken token = new AzureAccessToken();
    LocalDateTime ldt = LocalDateTime.now();

    final Long sameTime = ldt.atZone(ZoneId.systemDefault()).toEpochSecond();

    token.setAccessToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIseeee");
    token.setExpiresIn(HOUR_IN_SEC);
    token.setNotBefore(sameTime);
    token.setExpiresOn(sameTime - ONE_SECOND);

    return token;
  }

  /**
   * Mock Azure access token.
   *
   * @return the mock Azure access token.
   */
  private AzureAccessToken mockAzureToken() {
    final AzureAccessToken token = new AzureAccessToken();
    LocalDateTime ldt = LocalDateTime.now();

    final Long notBefore = ldt.atZone(ZoneId.systemDefault()).toEpochSecond();
    ldt.plusSeconds(HOUR_IN_SEC);
    final Long expiresOn = ldt.atZone(ZoneId.systemDefault()).toEpochSecond();

    token.setAccessToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1d");
    token.setExpiresIn(HOUR_IN_SEC);
    token.setNotBefore(notBefore);
    token.setExpiresOn(expiresOn);

    return token;
  }

  /**
   * Internal config.
   */
  @Configuration
  public static class Config {

    /**
     * Inject Azure related properties.
     *
     * @param mockWebServer the mock server.
     * @return the Spring configurer.
     */
    @Bean
    public PropertyPlaceholderConfigurer properties(final MockWebServer mockWebServer) {
      final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();

      Properties prop = new Properties();
      prop.setProperty("internal.lr.ad.oauth2.resource", "");
      prop.setProperty("internal.lr.ad.oauth2.scope", "");
      prop.setProperty("internal.lr.ad.oauth2.granttype", GRANT_TYPE);
      prop.setProperty("internal.lr.ad.oauth2.client.id", CLIENT_ID);
      prop.setProperty("internal.lr.ad.oauth2.client.secret", CLIENT_SECRET);
      prop.setProperty("graph.api.version", API_VERSION);
      prop.setProperty("internal.lr.ad.token.url", mockWebServer.url("dummydomain/oauth2/token/").toString());
      configurer.setProperties(prop);
      return configurer;
    }

    /**
     * Create new mock web server.
     *
     * @return mock web server.
     */
    @Bean
    public MockWebServer mockWebServer() {
      return new MockWebServer();
    }

    /**
     * Create Internal LR Ad retrofit factory.
     *
     * @param mockWebServer injected mock web server.
     * @return LRADRetrofitFactory factory.
     */
    @Bean
    public LRADRetrofitFactory lradRetrofitFactory(final MockWebServer mockWebServer) {
      final LRADRetrofitFactory factory = new LRADRetrofitFactory();
      factory.setLrADBaseUrl(mockWebServer.url("dummydomain/graph/").toString());
      List<Class<? extends Interceptor>> interceptorList = new ArrayList<>(1);
      interceptorList.add(LRADInterceptor.class);
      factory.setInterceptorList(interceptorList);
      return factory;
    }

    /**
     * Create retrofit.
     *
     * @param factory injected factory.
     * @return retrofit.
     */
    @Bean(name = "lrADRetrofit")
    public Retrofit lradRetrofit(final LRADRetrofitFactory factory) {
      return factory.createLRADRetrofit();
    }

    /**
     * Create retrofit service factory.
     *
     * @return retrofit service factory.
     */
    @Bean
    public LRADRetrofitServiceFactory lradRetrofitServiceFactory() {
      return new LRADRetrofitServiceFactory();
    }

    /**
     * Create service.
     *
     * @param factory injected.
     * @return new service.
     */
    @Bean
    public InternalUserRetrofitService userRetrofitService(final LRADRetrofitServiceFactory factory) {
      return factory.createInternalUserRetrofitService();
    }

    /**
     * Return the Azure Interceptor.
     *
     * @param mockWebServer the mock web server.
     * @return the Azure Interceptor.
     */
    @Bean
    public LRADInterceptor interceptor(final MockWebServer mockWebServer) {
      final LRADInterceptor interceptor = spy(new LRADInterceptor());
      interceptor.setTokenUrl(mockWebServer.url("dummydomain/oauth2/token/").toString());
      interceptor.setTokenClientId(CLIENT_ID);
      interceptor.setTokenGrantType(GRANT_TYPE);
      interceptor.setTokenClientSecret(CLIENT_SECRET);
      return interceptor;
    }
  }
}
