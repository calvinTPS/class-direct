package com.baesystems.ai.lr.cd.be.hibernate.test;

import static com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum.DUE_STATUS;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.NotificationsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.NotificationTypes;
import com.baesystems.ai.lr.cd.be.domain.repositories.Notifications;
import com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * Provides unit tests for notification {@link NotificationsDao}.
 *
 * @author RKaneysan.
 * @author syalavarthi 23-06-2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao-context.xml")
public class NotificationDaoImplTest {
  /**
   * The {@link NotificationsDao} from spring context.
   */
  @Autowired
  private NotificationsDao notificationsDao;
  /**
   * The notification successful update message.
   */
  @Value("${notification.successful.create}")
  private String notificationSuccessfulCreate;
  /**
   * The notification message.
   */
  @Value("${notification.exists}")
  private String notificationExistsMessage;

  /**
   * The notification message.
   */
  @Value("${notification.successful.delete}")
  private String notificationSuccessfulDelete;

  /**
   * The notification message.
   */
  @Value("${notification.not.found}")
  private String notificationNotFound;

  /**
   * Tests success scenario for {@link NotificationDao#getSubscribedUsers(List)} when the subscribed
   * users are not empty, check size is more than zero.
   */
  @Test
  public final void testGetSubscribedUsersSuccess() {
    List<NotificationTypesEnum> typeIds = new ArrayList<>();
    typeIds.add(DUE_STATUS);
    List<Notifications> notifications = notificationsDao.getSubscribedUsers(typeIds);
    Assert.assertNotNull(notifications);
    Assert.assertTrue(!notifications.isEmpty());
  }

  /**
   * Tests success scenario for {@link NotificationDao#getNotificationTypes}.
   */
  @Test
  public final void testGetNotificationTypesSuccess() {
    List<NotificationTypes> notificationTypes = notificationsDao.getNotificationTypes();
    Assert.assertFalse(notificationTypes.isEmpty());
  }

  /**
   * Tests success scenario for {@link NotificationDao#createNotification} to include favourite
   * assets.
   *
   * @throws RecordNotFoundException if there notification type record not found.
   */
  @Test
  public final void testCreateNotificationSuccess() throws RecordNotFoundException {
    StatusDto status = notificationsDao.createNotification("109", 1L, true);
    Assert.assertEquals(String.format(notificationSuccessfulCreate, "109", 1L), status.getMessage());
    List<NotificationTypesEnum> typeIds = new ArrayList<>();
    typeIds.add(NotificationTypesEnum.ASSET_LISTING);
    List<Notifications> notifications = notificationsDao.getSubscribedUsers(typeIds);
    Assert.assertNotNull(notifications);
    Assert.assertTrue(!notifications.isEmpty());
    notifications.forEach(notification -> {
      if (notification.getId().equals("109")) {
        Assert.assertEquals(notification.isFavouritesOnly(), true);
      }
    });
  }

  /**
   * Tests success scenario for {@link NotificationDao#createNotification} to include all assets.
   *
   * @throws RecordNotFoundException if there notification type record not found.
   */
  @Test
  public final void testCreateNotificationForAllAssets() throws RecordNotFoundException {
    StatusDto status = notificationsDao.createNotification("110", 1L, false);
    Assert.assertEquals(String.format(notificationSuccessfulCreate, "110", 1L), status.getMessage());
    List<NotificationTypesEnum> typeIds = new ArrayList<>();
    typeIds.add(NotificationTypesEnum.ASSET_LISTING);
    List<Notifications> notifications = notificationsDao.getSubscribedUsers(typeIds);
    Assert.assertNotNull(notifications);
    Assert.assertTrue(!notifications.isEmpty());
    notifications.forEach(notification -> {
      if (notification.getId().equals("110")) {
        Assert.assertEquals(notification.isFavouritesOnly(), false);
      }
    });
  }


  /**
   * Tests success scenario for {@link NotificationDao#createNotification}.
   *
   * @throws RecordNotFoundException exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testCreateNotificationFail() throws RecordNotFoundException {
    notificationsDao.createNotification("200", 1L, true);
  }

  /**
   * Tests success scenario for {@link NotificationDao#createNotification}.
   *
   * @throws RecordNotFoundException exception.
   */
  @Test
  public final void testCreateNotificationWithExistUser() throws RecordNotFoundException {
    notificationsDao.deleteNotification("104", 1L);
    StatusDto status = notificationsDao.createNotification("104", 1L, false);
    Assert.assertEquals(String.format(notificationSuccessfulCreate, "104", 1L), status.getMessage());
  }

  /**
   * Tests success scenario for {@link NotificationDao#deleteNotification}.
   *
   */
  @Test
  public final void testDeleteNotification() {
    StatusDto status = notificationsDao.deleteNotification("105", 2L);
    Assert.assertEquals(String.format(notificationSuccessfulDelete, "105", 2L), status.getMessage());
  }

  /**
   * Tests failure scenario scenario for {@link NotificationDao#deleteNotification} with no
   * notification record found.
   *
   */
  @Test
  public final void testDeleteNotificationWithNoRecord() {
    StatusDto status = notificationsDao.deleteNotification("109", 2L);
    Assert.assertNotNull(status.getMessage());
    Assert.assertEquals(String.format(notificationNotFound, "109", 2L), status.getMessage());
  }

  /**
   * Tests success scenario to get subscribed users
   * {@link NotificationsDao#getSubscribedUsers(List)} with valid subscribed users.
   *
   */
  @Test
  public final void testGetSubscribedUsersWithAssetListingSuccess() {
    List<NotificationTypesEnum> typeIds = new ArrayList<>();
    typeIds.add(NotificationTypesEnum.ASSET_LISTING);
    List<Notifications> notifications = notificationsDao.getSubscribedUsers(typeIds);
    Assert.assertNotNull(notifications);
    Assert.assertTrue(!notifications.isEmpty());

  }

  /**
   * Tests success scenario to get subscribed users
   * {@link NotificationsDao#getSubscribedUsers(List)} with no user subscribed for notification.
   *
   */
  @Test
  public final void testGetSubscribedUsersWithNoSubscribedUser() {
    StatusDto status = notificationsDao.deleteNotification("104", 1L);
    Assert.assertNotNull(status.getMessage());
    StatusDto status2 = notificationsDao.deleteNotification("109", 1L);
    Assert.assertNotNull(status2.getMessage());
    List<NotificationTypesEnum> typeIds = new ArrayList<>();
    typeIds.add(NotificationTypesEnum.ASSET_LISTING);
    List<Notifications> notifications = notificationsDao.getSubscribedUsers(typeIds);
    Assert.assertNotNull(notifications);
    Assert.assertTrue(notifications.isEmpty());

  }
}
