/**
 * This package is for retrofit unit test.
 *
 * @author msidek
 *
 */
package com.baesystems.ai.lr.cd.be.domain.retrofit.test;
