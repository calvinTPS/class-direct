package com.baesystems.ai.lr.cd.be.domain.utils.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;

import com.baesystems.ai.lr.cd.be.domain.utils.UserProfileDaoUtils;

/**
 * Test cases for UserProfileDaoUtils.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class UserProfileDaoUtilsTest {

  /**
   * Logger for User Profile Service Test.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileDaoUtilsTest.class);


  /**
   * Test Case for successful User retrieval.
   *
   * @throws Exception exception.
   */
  @Test
  public final void testBackSlash() throws Exception {
    final String output = "\\\\\\\\User \\%";
    final String input = "\\User %";
    LOGGER.info("Input : {}, Output : {}", input, output);
    String formattedSQL = UserProfileDaoUtils.getFormattedStringForSql(input);
    LOGGER.info("Formatted SQL : {}", formattedSQL);
    Assert.assertEquals(formattedSQL, output);
  }
}

