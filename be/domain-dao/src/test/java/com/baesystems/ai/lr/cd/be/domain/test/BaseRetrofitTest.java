package com.baesystems.ai.lr.cd.be.domain.test;

import com.baesystems.ai.lr.cd.be.domain.dao.RetrofitServiceFactory;
import com.baesystems.ai.lr.cd.be.domain.retrofit.RetrofitFactory;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.ErrorCodeInterceptor;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.PaginatedResponseInterceptor;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.ReferenceDataResponseInterceptor;

import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import retrofit2.Retrofit;

/**
 * Provides abstraction for retrofit unit test.
 *
 * @author msidek
 *
 */
public abstract class BaseRetrofitTest {
  /**
   * Success code.
   *
   */
  protected static final Integer SUCCESS_CODE = 200;

  /**
   * Injected {@link MockWebServer}.
   *
   */
  @Autowired
  private MockWebServer server;

  /**
   * Getter for {@link #server}.
   *
   * @return server
   */
  public final MockWebServer getServer() {
    return server;
  }

  /**
   * Enqueue response to mock server.
   *
   * @param filename response file.
   * @throws Exception when error.
   *
   */
  protected void enqueueResponseFromFile(final String filename) throws Exception {
    final String response = parseResponseFromClassPathFile(filename);

    server.enqueue(new MockResponse().setResponseCode(SUCCESS_CODE).setBody(response));
  }

  /**
   * Enqueue server error with 500.
   *
   * @throws Exception when error.
   *
   */
  protected void enqueueServerError() throws Exception {
    server.enqueue(new MockResponse().setResponseCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).setBody("[]"));
  }

  /**
   * Parse response json from classpath file.
   *
   * @param filename of file.
   * @return content.
   * @throws Exception when error.
   *
   */
  protected String parseResponseFromClassPathFile(final String filename) throws Exception {
    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(filename), writer, Charset.defaultCharset());

    return writer.toString();
  }

  /**
   * Spring config inner class.
   *
   * @author msidek
   *
   */
  public abstract static class BaseConfig {

    /**
     * Create mock web server.
     *
     * @return mock web server.
     *
     */
    @Bean
    public MockWebServer mockWebServer() {
      return new MockWebServer();
    }

    /**
     * Create retrofit factory.
     *
     * @param mockWebServer injected server.
     * @return factory.
     *
     */
    @Bean
    public RetrofitFactory retrofitFactory(final MockWebServer mockWebServer) {
      RetrofitFactory factory = new RetrofitFactory();
      factory.setBaseUrl(mockWebServer.url("/").toString());
      List<Class<? extends Interceptor>> interceptorList = new ArrayList<>(1);
      interceptorList.add(ReferenceDataResponseInterceptor.class);
      interceptorList.add(PaginatedResponseInterceptor.class);
      interceptorList.add(ErrorCodeInterceptor.class);
      factory.setInterceptorList(interceptorList);
      return factory;
    }

    /**
     * Create retrofit.
     *
     * @param retrofitFactory injected factory.
     * @return retrofit
     *
     */
    @Bean
    public Retrofit retrofit(final RetrofitFactory retrofitFactory) {
      return retrofitFactory.createRetrofit();
    }

    /**
     * Create response interceptor.
     *
     * @return interceptor.
     *
     */
    @Bean
    public ReferenceDataResponseInterceptor responseInterceptor() {
      return new ReferenceDataResponseInterceptor();
    }

    /**
     * Create interceptor for paginated response.
     *
     * @return interceptor.
     *
     */
    @Bean
    public PaginatedResponseInterceptor paginatedResponseInterceptor() {
      return new PaginatedResponseInterceptor();
    }

    /**
     * Create error code interceptor.
     *
     * @return interceptor.
     *
     */
    @Bean
    public ErrorCodeInterceptor errorCodeInterceptor() {
      return new ErrorCodeInterceptor();
    }

    /**
     * Create retrofit service factory.
     *
     * @return retrofit service factory.
     *
     */
    @Bean
    public RetrofitServiceFactory retrofitServiceFactory() {
      return new RetrofitServiceFactory();
    }
  }
}
