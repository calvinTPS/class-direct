/**
 * This package is for generic unit test.
 *
 * @author msidek
 *
 */
package com.baesystems.ai.lr.cd.be.domain.test;
