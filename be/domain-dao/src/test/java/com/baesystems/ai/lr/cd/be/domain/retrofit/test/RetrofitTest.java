package com.baesystems.ai.lr.cd.be.domain.retrofit.test;

import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.retrofit.RetrofitFactory;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.ReferenceDataResponseInterceptor;
import java.io.StringWriter;
import java.nio.charset.Charset;
import okhttp3.Interceptor.Chain;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import retrofit2.Retrofit;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

/**
 * Retrofit bean test.
 *
 * @author msidek
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Request.class, Chain.class, Response.class, ResponseBody.class})
@ContextConfiguration(classes = RetrofitTest.Config.class)
public class RetrofitTest {
  /**
   * HTTP success code.
   *
   */
  private static final Integer SUCCESS_CODE = 200;

  /**
   * Spring injected context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * Injected retrofit factory.
   *
   */
  @Autowired(required = false)
  private Retrofit retrofit;

  /**
   * Injected reference response interceptor.
   *
   */
  @Autowired
  private ReferenceDataResponseInterceptor responseInterceptor;

  /**
   * Unit test for reference data response interceptor.
   *
   * @throws Exception thrown.
   *
   */
  @Test
  public final void referenceDataResponseInterceptor() throws Exception {
    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("assettypes.reference.json"), writer,
        Charset.defaultCharset());

    final ResponseBody body = PowerMockito.mock(ResponseBody.class);
    doAnswer(new Answer<String>() {
      @Override
      public final String answer(final InvocationOnMock invocation) throws Throwable {
        return writer.toString();
      }
    }).when(body).string();

    Response response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(body);
    when(response.newBuilder()).thenReturn(new Response.Builder().request(PowerMockito.mock(Request.class))
        .protocol(Protocol.HTTP_1_1).code(SUCCESS_CODE));

    final Chain chain = PowerMockito.mock(Chain.class);
    when(chain.proceed(any(Request.class))).thenReturn(response);

    response = responseInterceptor.intercept(chain);
  }

  /**
   * Unit test for Reference Response interceptor.
   *
   * @throws Exception thrown.
   *
   */
  @Test
  public final void referenceDataResponseInterceptorNonReference() throws Exception {
    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("asset.json"), writer, Charset.defaultCharset());

    final ResponseBody body = PowerMockito.mock(ResponseBody.class);
    doAnswer(new Answer<String>() {
      @Override
      public final String answer(final InvocationOnMock invocation) throws Throwable {
        return writer.toString();
      }
    }).when(body).string();

    final Response response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(body);
    when(response.newBuilder()).thenReturn(new Response.Builder().request(PowerMockito.mock(Request.class))
        .protocol(Protocol.HTTP_1_1).code(SUCCESS_CODE));

    final Chain chain = PowerMockito.mock(Chain.class);
    when(chain.proceed(any(Request.class))).thenReturn(response);

    responseInterceptor.intercept(chain);
  }

  /**
   * Unit test for asset type.
   *
   */
  @Test
  public final void testAssetType() {
    final AssetCategoryHDto dto = new AssetCategoryHDto();

    final AssetTypeHDto assetType = new AssetTypeHDto();
    assetType.setCategoryDto(dto);
    Assert.assertEquals(dto, assetType.getCategoryDto());
    Assert.assertNotNull(assetType.toString());
  }

  /**
   * Static class as Spring config.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config {
    /**
     * Create properties configurer.
     *
     * @return new property placeholder.
     *
     */
    @Bean
    public PropertyPlaceholderConfigurer properties() {
      final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
      configurer.setLocations(new ClassPathResource("classdirect-config.properties"));

      return configurer;
    }

    /**
     * Create reference response interceptor.
     *
     * @return new reference data interceptor bean.
     *
     */
    @Bean
    public ReferenceDataResponseInterceptor referenceDataResponseInterceptor() {
      return new ReferenceDataResponseInterceptor();
    }

    /**
     * Create retrofit factory.
     *
     * @param baseUrl to be set.
     * @return new retrofit factory.
     *
     */
    @Bean
    public RetrofitFactory retrofitFactory(@Value("${retrofit.mast.url}") final String baseUrl) {
      RetrofitFactory factory = new RetrofitFactory();
      factory.setBaseUrl(baseUrl);

      return factory;
    }

    /**
     * Create retrofit factory.
     *
     * @param retrofitFactory bean.
     *
     * @return new retrofit factory.
     *
     */
    @Bean
    public Retrofit retrofit(final RetrofitFactory retrofitFactory) {
      return retrofitFactory.createRetrofit();
    }
  }
}
