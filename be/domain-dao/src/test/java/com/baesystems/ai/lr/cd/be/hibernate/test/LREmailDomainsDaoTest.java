package com.baesystems.ai.lr.cd.be.hibernate.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.LREmailDomainsDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl.LREmailDomainsDaoImpl;
import com.baesystems.ai.lr.cd.be.domain.repositories.LREmailDomains;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author fwijaya
 */
@RunWith(MockitoJUnitRunner.class)
public class LREmailDomainsDaoTest {
  /**
   * Mock typed query.
   */
  @Mock
  private TypedQuery<LREmailDomains> typedQuery;
  /**
   * Injected dao.
   */
  @InjectMocks
  private LREmailDomainsDao lrEmailDomainsDao = new LREmailDomainsDaoImpl();
  /**
   * Mock entity manager.
   */
  @Mock
  private EntityManager entityManager;

  /**
   * Test getting lr email domains.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void successfulAuditFind() throws ClassDirectException {
    when(typedQuery.getResultList()).thenReturn(new ArrayList<>());
    when(entityManager.createQuery(anyString())).thenReturn(typedQuery);
    final List<LREmailDomains> result = lrEmailDomainsDao.getLrEmailDomains();
    assertNotNull(result);
  }
}
