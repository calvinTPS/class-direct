package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.test;

import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.MastResponseCacheInterceptor;
import java.io.IOException;
import java.util.Collections;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
/**
 * @author sbollu
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(MockitoJUnitRunner.class)
@PrepareForTest(FileUtils.class)
@PowerMockIgnore("javax.management.*")
public class MastResponseCacheInterceptorTest {
  /**
   * Integer 1.
   */
  private static final long ONE = 1L;

  /**
   * Long.
   */
  private static final long EXPIRY_TIME = 300L;

  /**
   * cache size.
   */
  private static final long CACHE_SIZE = 1024L;

  /**
   * Primary class to be tested.
   */
  @InjectMocks
  private MastResponseCacheInterceptor interceptor = new MastResponseCacheInterceptor();

  /**
   * Intercepter should intercept but exclude if path is matching exclusion list.
   *
   * @throws IOException if retrofit error occurs.
   */
  @Test
  public final void shouldInterceptButExcludeTheRequest() throws IOException {
    final Interceptor.Chain chain = mock(Interceptor.Chain.class);
    final Request request = new Request.Builder().method("GET", null)
      .url("https://mast.classdirect-test.marine.mast.ids/mast/api/v2/reference-data-version").build();
    when(chain.request()).thenReturn(request);
    final okhttp3.Response mockResponse = new okhttp3.Response.Builder()
      .request(request)
      .protocol(Protocol.HTTP_1_1)
      .code(org.springframework.http.HttpStatus.OK.value())
      .build();
    final ArgumentCaptor<Request> captor = ArgumentCaptor.forClass(Request.class);
    when(chain.proceed(captor.capture())).thenReturn(mockResponse);

    interceptor.setExclusions(Collections.singletonList("(.+)reference-data-version(.*)"));
    interceptor.intercept(chain);
    verify(chain, atLeastOnce()).proceed(any());

    final Request usedRequest = captor.getValue();
    assertNull(usedRequest.header("Cache-Control"));
  }

  /**
   * Interceptor should intercept and append cache-control header.
   *
   * @throws IOException when retrofit error occurs.
   */
  @Test
  public final void shouldInterceptAndAppendCacheControlHeader() throws IOException {
    final String cacheControlValue = "public, max-age=" + EXPIRY_TIME;
    final Interceptor.Chain chain = mock(Interceptor.Chain.class);
    final Request request = new Request.Builder().method("GET", null)
      .url("https://mast.classdirect-test.marine.mast.ids/mast/api/v2/asset/query").build();
    when(chain.request()).thenReturn(request);
    final okhttp3.Response mockResponse = new okhttp3.Response.Builder()
      .request(request)
      .protocol(Protocol.HTTP_1_1)
      .code(org.springframework.http.HttpStatus.OK.value())
      .header("Pragma", "TestPragma")
      .header("Cache-Control", "no-cache")
      .build();
    final ArgumentCaptor<Request> captor = ArgumentCaptor.forClass(Request.class);
    when(chain.proceed(captor.capture())).thenReturn(mockResponse);

    interceptor.setExclusions(Collections.singletonList("(.+)reference-data-version(.*)"));
    interceptor.setCacheDuration(EXPIRY_TIME);

    final okhttp3.Response response = interceptor.intercept(chain);
    verify(chain, atLeastOnce()).proceed(any());
    final Request sentRequest = captor.getValue();
    assertNotNull(sentRequest.header("Cache-Control"));
    assertEquals(cacheControlValue, sentRequest.header("Cache-Control"));

    assertNotNull(response.header("Cache-Control"));
    assertEquals(cacheControlValue, response.header("Cache-Control"));
    assertNull(response.header("Pragma"));
  }

  /**
   * Interceptor will initialize builder during startup.
   */
  @Test
  public final void successfullyInitializeBuilder() {
    final OkHttpClient.Builder builder = new OkHttpClient.Builder();
    PowerMockito.mockStatic(FileUtils.class, new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        return null;
      }
    });
    interceptor.setCacheDirectory("test/directory");
    interceptor.initBuilder(builder);

    PowerMockito.verifyStatic(atLeastOnce());
  }

  /**
   * Interceptor should proceed even when FileUtils is throwing exception.
   */
  @Test
  public final void proceedBuilderEvenWhenFileUtilsThrowsException() {
    final OkHttpClient.Builder builder = new OkHttpClient.Builder();
    PowerMockito.mockStatic(FileUtils.class, new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        throw new IOException("Mock exception.");
      }
    });
    interceptor.setCacheDirectory("test/directory");
    interceptor.initBuilder(builder);

    PowerMockito.verifyStatic(atLeastOnce());
  }

}
