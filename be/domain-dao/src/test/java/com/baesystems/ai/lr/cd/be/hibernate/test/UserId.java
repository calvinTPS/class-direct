package com.baesystems.ai.lr.cd.be.hibernate.test;

/**
 * @author Faizal Sidek
 */
public enum UserId {
  /**
   * Reference to Alice user.
   */
  ALICE("d0835759-e2f5-4d0c-b445-c90a20d89466"), BOB("2618aed6-3d12-41e5-9f24-c29f2c7ea4c1");

  // ALICE("9ca667cd-5c0a-44e7-8a11-ba5dc806dabe"), BOB("ec414774-7a1b-4206-8ae8-98af0fb0696e");

  /**
   * Id of the user.
   */
  private String id;

  /**
   * Private constructor.
   *
   * @param userId id of user.
   */
  UserId(final String userId) {
    this.id = userId;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return id.
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for {@link #id}.
   *
   * @param userId id of the user.
   */
  public void setId(final String userId) {
    this.id = userId;
  }
}
