package com.baesystems.ai.lr.cd.be.domain.dao.reference.test;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test unit for AssetReferenceService.
 *
 * @author msidek
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AssetReferenceRetrofitServiceTest {
  /**
   * Logger log4j.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetReferenceRetrofitServiceTest.class);

  /**
   * Injected assetReferenceService.
   *
   */
  @Autowired(required = false)
  private AssetReferenceRetrofitService assetReferenceService;

  /**
   * JUnit test to test fetch asset type.
   *
   * @throws Exception exception thrown.
   *
   */
  @Test
  public final void successfullyFetchAssetTypes() throws Exception {
    LOGGER.debug("Successfully fetch asset types.");
    Assert.assertNotNull(assetReferenceService);
  }

  /**
   * Inner config.
   */
  @Configuration
  @ImportResource("classpath:test-context.xml")
  public static class Config {

    /**
     * Security dao mock.
     *
     * @return mock dao.
     */
    @Bean
    public SecurityDAO securityDAO() {
      return Mockito.mock(SecurityDAO.class);
    }
  }
}
