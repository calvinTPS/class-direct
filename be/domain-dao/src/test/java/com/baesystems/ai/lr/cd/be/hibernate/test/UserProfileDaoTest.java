package com.baesystems.ai.lr.cd.be.hibernate.test;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.LR_ADMIN;
import static com.baesystems.ai.lr.cd.be.hibernate.test.UserId.ALICE;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserCodeSearchDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserEORDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.TermsConditions;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.enums.OrderOptionEnum;
import com.baesystems.ai.lr.cd.be.enums.UserSortOptionEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.InactiveAccountException;
import com.baesystems.ai.lr.cd.be.exception.InvalidEmailException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;

/**
 * @author SBollu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao-context.xml")
public class UserProfileDaoTest {
  /**
   */
  @Autowired(required = false)
  private UserProfileDao userProfileDao;
  /**
   * Client user.
   */
  private static final String CLIENT_USER_ID = "101";
  /**
   * Another client user.
   */
  private static final String CLIENT2_USER_ID = "127";
  /**
   * Ship Builder user.
   */
  private static final String SHIP_USER_ID = "102";
  /**
   * Flag user.
   */
  private static final String FLAG_USER_ID = "103";
  /**
   * LR Admin user.
   */
  private static final String LR_ADMIN_USER_ID = "106";
  /**
   * LR Admin user.
   */
  private static final String LR_ADMIN_2_USER_ID = "107";
  /**
   * user's company name.
   */
  private static final String COMPANY_NAME = "Tschudi_Ship Management_AS";
  /**
   * Non-admin user.
   */
  private static final String NON_ADMIN_USER_ID = "102";
  /**
   * LR Support user.
   */
  private static final String LR_SUPPORT_USER_ID = "105";
  /**
   * Accepted T&C user.
   */
  private static final String ACCEPTED_TERMS_USER_ID = "105";
  /**
   * /** EOR user.
   */
  private static final String EOR_USER_ID = "108";
  /**
   * Client user.
   */
  private static final String NOTIFICATIONS_USER_ID = "104";
  /**
   * Active user.
   */
  private static final String ACTIVE_USER_ID = "104";
  /**
   */
  private static final String FAIL_USER_ID = "200";
  /**
   */
  private static final String FAIL_ADMIN_USER_ID = "110";
  /**
   * Last login not null user.
   */
  private static final String LAST_LOGIN_NOT_NULL_USER_ID = "124";
  /**
   * New User Id 1.
   */
  private static final String NEW_USER_ID = "cc5cb098-be31-4660-803a-b6c31fbd02b0";
  /**
   * New User Id 2.
   */
  private static final String NEW_USER_ID_2 = "cc5cb098-be31-4660-803a-b6c31fbd02b1";
  /**
   * New User Id 3.
   */
  private static final String NEW_USER_ID_3 = "cc5cb098-be31-4660-803a-b6c31fbd02b2";
  /**
   * New User Id 4.
   */
  private static final String NEW_USER_ID_4 = "cc5cb098-be31-4660-803a-b6c31fbd02b3";
  /**
   * New User Id 5.
   */
  private static final String NEW_USER_ID_5 = "cc5cb098-be31-4660-803a-b6c31fbd02b4";
  /**
   * New User Id 6.
   */
  private static final String NEW_USER_ID_6 = "cc5cb098-be31-4660-803a-b6c31fbd02b5";
  /**
   * New User Id 7.
   */
  private static final String NEW_USER_ID_7 = "cc5cb098-be31-4660-803a-b6c31fbd02b6";
  /**
   * New User Id 8.
   */
  private static final String NEW_USER_ID_8 = "cc5cb098-be31-4660-803a-b6c31fbd02b7";
  /**
   * New User Id 9.
   */
  private static final String NEW_USER_ID_9 = "cc5cb098-be31-4660-803a-b6c31fbd02b8";
  /**
   * New User Id 10.
   */
  private static final String NEW_USER_ID_10 = "cc5cb098-be31-4660-803a-b6c31fbd02b9";
  /**
   * New User Id 11.
   */
  private static final String NEW_USER_ID_11 = "cc5cb098-be31-4660-803a-b6c31fbd0210";
  /**
   */
  private static final String CLIENT_CODE_NOT_EQUAL_USER_ID = "112";
  /**
   */
  private static final String GET_USERS_FAIL_USER_ID = null;
  /**
   */
  private static final String NOT_NULL_USER_ID = "113";
  /**
   */
  private static final String STATUS_NULL_USER_ID = "106";
  /**
   */
  private static final String NOT_ACCEPT_TERMS_USER_ID = "103";
  /**
   */
  private static final String ROLE_TEST_USER_ID = "114";
  /**
   */
  private static final String TEST1_USER_ID = "115";
  /**
   */
  private static final String TEST2_USER_ID = "105";
  /**
   */
  private static final Long ACTIVE_STATUS_ID = 1L;
  /**
   */
  private static final Long DISABLED_STATUS_ID = 2L;
  /**
   */
  private static final Long ARCHIVED_STATUS_ID = 3L;
  /**
   */
  private static final Long NON_EXISTENT_STATUS = 5L;
  /**
   * Inactive user id.
   */
  private static final String INACTIVE_USER = "120";
  /**
   * Inactive user id used to test account status update.
   */
  private static final String INACTIVE_USER_FOR_STATUS_UPDATE = "122";
  /**
   * user as client admin and LR admin.
   */
  private static final String DUAL_ADMIN_ROLE_USER = "121";
  /**
   * Year.
   */
  private static final Integer DEFAULT_YEAR = 2019;
  /**
   * Month.
   */
  private static final Integer DEFAULT_MONTH = 12;
  /**
   * Date.
   */
  private static final Integer DEFAULT_DATE = 9;
  /**
   * ship builder code.
   */
  private static final String SHIP_BUILDER_CODE = "5678";
  /**
   * ship builder code.
   */
  private static final String CLIENT_CODE = "56789";
  /**
   * client type.
   */
  private static final String CLIENT_TYPE = "DOC_COMPANY_ROLE";
  /**
   * Year.
   */
  private static final Integer LATEST_YEAR = 2016;
  /**
   * Month.
   */
  private static final Integer LATEST_MONTH = 12;
  /**
   * Date.
   */
  private static final Integer LATEST_DATE = 29;
  /**
   * Default size per result.
   */
  public static final Integer DEFAULT_SIZE_PER_PAGE = Integer.MAX_VALUE;
  /**
   * Default starting page is set to 1.
   */
  public static final Integer DEFAULT_STARTING_PAGE = 1;
  /**
   * Default size.
   */
  private static final Integer MAX_SIZE = 15;
  /**
   * user email.
   */
  private static final String USER_EMAIL_1 = "puppy@bae.com";
  /**
   * user email.
   */
  private static final String USER_EMAIL_2 = "krish@bae.com";
  /**
   * user email.
   */
  private static final String USER_EMAIL_3 = "sesh@bae.com";
  /**
   * client code.
   */
  private static final String CLIENT_CODE2 = "4";
  /**
   * flag code.
   */
  private static final String FLAG_CODE = "CH2";
  /**
   * ship builder code.
   */
  private static final String SHIP_BUILDER_CODE2 = "1";
  /**
  *
  */
  private static final String CLIENT = "CLIENT";
  /**
  *
  */
  private static final String FLAG = "FLAG";
  /**
  *
  */
  private static final String SHIP_BUILDER = "SHIP_BUILDER";
  /**
   * hour.
   */
  private static final Integer LATEST_HOUR = 2;
  /**
   * hour.
   */
  private static final Integer LATEST_MIN = 20;

  /**
   * imo number.
   */
  private static final String IMO_NUM = "7653986";

  /**
   * imo number not available.
   */
  private static final String IMO_NUM_NOT_AVAILABLE = "765398";
  /**
   * user TC create message.
   */
  @Value("${userTC.successful.create}")
  private String userTCSuccessfulCreate;
  /**
   * user TC update message.
   */
  @Value("${userTC.successful.update}")
  private String userTCSuccessfulUpdate;
  /**
   * message for successful creation.
   */
  @Value("${user.successful.create}")
  private String userSuccessfulCreate;
  /**
   * message for existing user.
   */
  @Value("${user.exists}")
  private String userExists;
  /**
   * message for successful creation of EOR.
   */
  @Value("${userEOR.successful.create}")
  private String userEORSuccessfulCreate;
  /**
   * eor update message.
   */
  @Value("${userEOR.successful.update}")
  private String userEORSuccessfulUpdate;
  /**
   * user TC accept date exists message.
   */
  @Value("${userTC.acceptedDate.exists}")
  private String userTCacceptedDateExists;
  /**
   * user TC accept date not null message.
   */
  @Value("${userTC.acceptedDate.null}")
  private String userTCacceptedDateNotNull;

  /**
   * Delete Success Message.
   */
  @Value("${eor.successful.delete}")
  private String successfulDelete;

  /**
   * Test Case for UserProfile Data Access Object.
   */
  @Test
  public final void testUserProfileDao() {
    Assert.assertNotNull(userProfileDao);
  }

  /**
   * Test case for get all users.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testUserProfileDaoGetAllUsers() throws ClassDirectException {
    final List<UserProfiles> users = userProfileDao.getAllUsers();
    Assert.assertNotNull(users);
    Assert.assertFalse(users.isEmpty());
  }

  /**
   * Test case for get all users.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testUserProfileDaoGetAllUsersLargeResultSize() throws ClassDirectException {
    final List<UserProfiles> users = userProfileDao.getAllUsers();
    Assert.assertNotNull(users);
    Assert.assertFalse(users.isEmpty());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void verifyUserTestSuccess() throws Exception {
    final UserProfiles userProfiles = userProfileDao.getUser(ACTIVE_USER_ID);
    Assert.assertNotNull(userProfiles);
    Assert.assertNotNull(userProfiles.getUserId());
    Assert.assertNotNull(userProfiles.getStatus());
    Assert.assertFalse(userProfiles.getRoles().isEmpty());
  }

  /**
   * @throws RecordNotFoundException Object.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getUserTestFail() throws RecordNotFoundException {
    final UserProfiles userProfiles = userProfileDao.getUser(FAIL_USER_ID);
  }

  /**
   * @throws UnauthorisedAccessException Object.
   * @throws RecordNotFoundException Object.
   */
  @Test
  public final void getUsersLRAdminTestSuccess() throws UnauthorisedAccessException, RecordNotFoundException {
    final List<UserProfiles> users = userProfileDao.getUsers(LR_ADMIN_USER_ID);
    Assert.assertFalse(users.isEmpty());
  }

  /**
   * @throws UnauthorisedAccessException Object.
   * @throws RecordNotFoundException Object.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void getUsersTestFail() throws UnauthorisedAccessException, RecordNotFoundException {
    final List<UserProfiles> users = userProfileDao.getUsers(EOR_USER_ID);
  }

  /**
   * @throws UnauthorisedAccessException Object.
   * @throws RecordNotFoundException Object.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getUsersFail() throws UnauthorisedAccessException, RecordNotFoundException {
    final List<UserProfiles> users = userProfileDao.getUsers(GET_USERS_FAIL_USER_ID);
  }

  /**
   * @throws UnauthorisedAccessException Object.
   * @throws RecordNotFoundException Object.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void getUsersRoleFail() throws UnauthorisedAccessException, RecordNotFoundException {
    final List<UserProfiles> users = userProfileDao.getUsers(FAIL_ADMIN_USER_ID);
  }

  /**
   * @throws UnauthorisedAccessException Object.
   * @throws RecordNotFoundException Object.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void getUsersClientCodeFail() throws UnauthorisedAccessException, RecordNotFoundException {
    final List<UserProfiles> users = userProfileDao.getUsers(CLIENT_USER_ID);
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void updateUsersTestFail() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(DISABLED_STATUS_ID);
    status.setName("Disabled");
    userProfiles.setStatus(status);
    userProfiles.setEmail("ABC@bae");
    final UserProfiles user = userProfileDao.updateUser(CLIENT2_USER_ID, CLIENT2_USER_ID, userProfiles, "");
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void updateUsersTestFailEmptyUser() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    userProfiles.setRestrictAll(false);
    final UserProfiles user = userProfileDao.updateUser(CLIENT2_USER_ID, CLIENT2_USER_ID, userProfiles, "");
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void updateUsersTestFailBlankUser() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    final UserProfiles user = userProfileDao.updateUser(CLIENT2_USER_ID, CLIENT2_USER_ID, userProfiles, "");
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test
  public final void updateUsersLRAdminTestSuccess() throws ClassDirectException {
    final UserProfiles oldUser = userProfileDao.getUser(CLIENT_USER_ID);
    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(DISABLED_STATUS_ID);
    status.setName("Disabled");
    userProfiles.setRestrictAll(false);
    userProfiles.setStatus(status);
    userProfiles.setEmail("ABC@bae");
    userProfiles.setUserAccExpiryDate(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));

    final UserProfiles user = userProfileDao.updateUser(LR_ADMIN_USER_ID, CLIENT_USER_ID, userProfiles, "");
    Assert.assertEquals("status Id set to 2", DISABLED_STATUS_ID, user.getStatus().getId());
    Assert.assertEquals("Expected expiredDate", LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE),
        user.getUserAccExpiryDate());
    Assert.assertNotEquals("set Email", "ABC@bae", user.getEmail());
    Assert.assertEquals("set Email", user.getEmail(), oldUser.getEmail());
  }

  /**
   * Tests success scenario to update user with specific fields
   * {@link UserProfileDao#updateUser(String, String, UserProfiles, String)} should update only
   * flagCode, clientCode, shipBuilderCode, status, userAccExpiryDate.
   *
   * @throws ClassDirectException when error in updating user not authorized.
   */
  // @Test
  public final void updateUserSelectedFieldsSuccess() throws ClassDirectException {
    final UserProfiles oldUser = userProfileDao.getUser("135");

    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    final UserAccountStatus status = new UserAccountStatus();

    status.setId(DISABLED_STATUS_ID);
    status.setName("Disabled");
    userProfiles.setRestrictAll(false);
    userProfiles.setStatus(status);
    userProfiles.setEmail("ABC@bae");
    userProfiles.setUserAccExpiryDate(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    userProfiles.setClientCode("1234");
    userProfiles.setName("sesh");
    userProfiles.setCompanyName("bae");
    userProfiles.setDateOfModification(LocalDateTime.of(2017, 8, 1, 8, 13, 12));

    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleId(107L);
    role.setIsAdmin(true);
    role.setRoleName("LR_ADMIN");
    roles.add(role);
    userProfiles.setRoles(roles);

    final UserProfiles user = userProfileDao.updateUser(LR_ADMIN_USER_ID, "135", userProfiles, "");
    Assert.assertEquals("status Id set to 2", DISABLED_STATUS_ID, user.getStatus().getId());
    Assert.assertEquals("Expected expiredDate", LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE),
        user.getUserAccExpiryDate());
    Assert.assertNotEquals("set Email", "ABC@bae", user.getEmail());
    Assert.assertEquals("set Email", oldUser.getEmail(), user.getEmail());
    Assert.assertEquals("set name", user.getName(), oldUser.getName());
    Assert.assertEquals("set company name", user.getCompanyName(), oldUser.getCompanyName());
    boolean isAdmin = user.getRoles().stream().filter(userRole -> {
      return userRole.getRoleName().equalsIgnoreCase(LR_ADMIN.toString());
    }).findAny().isPresent();
    Assert.assertFalse(isAdmin);

  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test
  public final void updateUsersLRAdminStatusTest() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    userProfiles.setFlagCode(null);
    userProfiles.setShipBuilderCode(null);
    userProfiles.setClientCode(null);
    userProfiles.setEmail("");
    userProfiles.setStatus(null);

    final UserProfiles user = userProfileDao.updateUser(LR_ADMIN_USER_ID, NOT_NULL_USER_ID, userProfiles, "");
    Assert.assertEquals("status Id set to 1", ACTIVE_STATUS_ID, user.getStatus().getId());
    Assert.assertEquals("set Flagcode", null, user.getFlagCode());
    Assert.assertEquals("set ShipBuilderCode", null, user.getShipBuilderCode());
    Assert.assertEquals("set ClientCode", null, user.getClientCode());
    Assert.assertNotEquals("set Email", "", user.getEmail());
  }

  /**
   * Test setting client code to empty or with value.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void updateUsersLRAdminEditClientCode() throws ClassDirectException {
    final UserProfiles existingUserProfile = userProfileDao.getUser(CLIENT_USER_ID);
    Assert.assertNotNull(existingUserProfile.getClientCode());

    final UserProfiles emptyUserProfile = new UserProfiles();
    final UserProfiles newUserProfile =
        userProfileDao.updateUser(LR_ADMIN_USER_ID, CLIENT_USER_ID, emptyUserProfile, "clientCode");
    Assert.assertNull(newUserProfile.getClientCode());

    final UserProfiles userProfile2 = new UserProfiles();
    userProfile2.setClientCode(CLIENT_CODE);
    final UserProfiles newUserProfile2 = userProfileDao.updateUser(LR_ADMIN_USER_ID, CLIENT_USER_ID, userProfile2, "");

    Assert.assertEquals(CLIENT_CODE, newUserProfile2.getClientCode());
  }

  /**
   * Test deleting flag code.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void updateUsersLRAdminDeleteFlagCode() throws ClassDirectException {
    final UserProfiles existingUserProfile = userProfileDao.getUser(FLAG_USER_ID);
    Assert.assertNotNull(existingUserProfile.getFlagCode());

    final UserProfiles emptyUserProfile = new UserProfiles();
    final UserProfiles newUserProfile =
        userProfileDao.updateUser(LR_ADMIN_USER_ID, FLAG_USER_ID, emptyUserProfile, "flagCode");
    Assert.assertNull(newUserProfile.getFlagCode());
  }

  /**
   * Test deleting ship code.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void updateUsersLRAdminDeleteShipCode() throws ClassDirectException {
    final UserProfiles emptyUserProfile = new UserProfiles();
    final UserProfiles existingUserProfile = userProfileDao.getUser(SHIP_USER_ID);
    Assert.assertNotNull(existingUserProfile.getShipBuilderCode());

    final UserProfiles newUserProfile =
        userProfileDao.updateUser(LR_ADMIN_USER_ID, SHIP_USER_ID, emptyUserProfile, "shipBuilderCode");
    Assert.assertNull(newUserProfile.getShipBuilderCode());
  }

  /**
   * Test setting not deletable field from user profile.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void updateUsersLRAdminDeleteLastLoginFailure() throws ClassDirectException {
    final UserProfiles existingUserProfile = userProfileDao.getUser(LAST_LOGIN_NOT_NULL_USER_ID);
    Assert.assertNotNull(existingUserProfile.getLastLogin());

    final UserProfiles emptyUserProfile = new UserProfiles();
    final UserProfiles newUserProfile =
        userProfileDao.updateUser(LR_ADMIN_USER_ID, LAST_LOGIN_NOT_NULL_USER_ID, emptyUserProfile, "lastLogin");

    Assert.assertNotNull(newUserProfile.getLastLogin());
  }

  /**
   * Tests success scenario to update user with specific fields
   * {@link UserProfileDao#updateUser(String, String, UserProfiles, String)} should not update
   * email.
   *
   * @throws ClassDirectException when fails to update user.
   */
  @Test
  public final void updateUsersLRAdminNullTest() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    userProfiles.setEmail("");
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(ACTIVE_STATUS_ID);
    status.setName("Active");
    userProfiles.setStatus(status);
    final UserProfiles user = userProfileDao.updateUser(LR_ADMIN_USER_ID, STATUS_NULL_USER_ID, userProfiles, null);
    Assert.assertEquals("set Account status Active", ACTIVE_STATUS_ID, user.getStatus().getId());
    Assert.assertEquals("set Flagcode", null, user.getFlagCode());
    Assert.assertEquals("set ShipBuilderCode", null, user.getShipBuilderCode());
    Assert.assertEquals("set ClientCode", null, user.getClientCode());
    Assert.assertNotEquals("set Email", "", user.getEmail());
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test
  public final void updateUserIDTest() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    userProfiles.setUserId(STATUS_NULL_USER_ID);
    userProfiles.setEmail("");
    final UserProfiles user = userProfileDao.updateUser(LR_ADMIN_USER_ID, STATUS_NULL_USER_ID, userProfiles, null);
    Assert.assertEquals("test to set userId in update user method", STATUS_NULL_USER_ID, user.getUserId());
    Assert.assertEquals("set Flagcode", null, user.getFlagCode());
    Assert.assertEquals("set ShipBuilderCode", null, user.getShipBuilderCode());
    Assert.assertEquals("set ClientCode", null, user.getClientCode());
    Assert.assertNotEquals("set Email", "", user.getEmail());
  }

  /**
   * @throws ClassDirectException exception.
   */
  public final void updateExpiryDateByClientAdmin() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    userProfiles
        .setUserAccExpiryDate(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE).plusMonths(ACTIVE_STATUS_ID));

    final UserProfiles user = userProfileDao.updateUser(LR_SUPPORT_USER_ID, CLIENT_USER_ID, userProfiles, null);
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void updateExpiryDateByLoggedInUser() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserAccExpiryDate(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    final UserProfiles user = userProfileDao.updateUser(CLIENT_USER_ID, CLIENT_USER_ID, userProfiles, null);
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void updateUsersFail() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(ACTIVE_STATUS_ID);
    status.setName("Active");
    userProfiles.setStatus(status);
    userProfiles.setEmail("ABC@bae");

    final UserProfiles user =
        userProfileDao.updateUser(CLIENT_USER_ID, CLIENT_CODE_NOT_EQUAL_USER_ID, userProfiles, null);
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void updateUserConditionTest12() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(DISABLED_STATUS_ID);
    status.setName("Disabled");
    userProfiles.setStatus(status);
    final UserProfiles user = userProfileDao.updateUser(NON_ADMIN_USER_ID, NON_ADMIN_USER_ID, userProfiles, null);
  }

  /**
   * @throws ClassDirectException Object.
   */
  @Test
  public final void fechAllUserAccStatus() throws ClassDirectException {
    final List<UserAccountStatus> userStatus = new ArrayList<>();
    final UserAccountStatus status1 = new UserAccountStatus();
    status1.setId(ACTIVE_STATUS_ID);
    status1.setName("Active");
    userStatus.add(status1);
    final UserAccountStatus status2 = new UserAccountStatus();
    status2.setId(DISABLED_STATUS_ID);
    status2.setName("Disabled");
    userStatus.add(status2);
    final UserAccountStatus status3 = new UserAccountStatus();
    status3.setId(ARCHIVED_STATUS_ID);
    status3.setName("Archived");
    userStatus.add(status3);

    final Object[] expectedUserStatus = userStatus.toArray();
    final List<UserAccountStatus> status = userProfileDao.fetchAllUserAccStatus();
    final Object[] statusArray = status.toArray();
    Assert.assertArrayEquals(expectedUserStatus, statusArray);
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void updateUserStatusIdTest() throws ClassDirectException {
    final UserProfiles userProfiles = new UserProfiles();
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(NON_EXISTENT_STATUS);
    userProfiles.setStatus(status);
    final UserProfiles user = userProfileDao.updateUser(LR_ADMIN_USER_ID, TEST2_USER_ID, userProfiles, null);
  }

  /**
   * @throws UnauthorisedAccessException object.
   * @throws RecordNotFoundException object.
   */
  @Test
  public final void getUserconditionTest() throws UnauthorisedAccessException, RecordNotFoundException {
    final UserProfiles user = userProfileDao.getUser(LR_ADMIN_USER_ID);
    Assert.assertEquals("get userProfile with requested userID", LR_ADMIN_USER_ID, user.getUserId());
  }

  /**
   * @throws UnauthorisedAccessException object.
   * @throws RecordNotFoundException object.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getUserconditionTestEmpty() throws UnauthorisedAccessException, RecordNotFoundException {
    UserProfiles user = userProfileDao.getUser("");
  }

  /**
   * @throws UnauthorisedAccessException object.
   * @throws RecordNotFoundException object.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getUserconditionNull() throws UnauthorisedAccessException, RecordNotFoundException {
    UserProfiles user = userProfileDao.getUser(null);
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostTest() throws ClassDirectException {
    final UserProfileDto userProfileDto = new UserProfileDto();
    // included users with single role.
    List<String> includedUsers = new ArrayList<>();
    includedUsers.add(USER_EMAIL_1);
    includedUsers.add(USER_EMAIL_2);
    List<String> userRoles = new ArrayList<>();
    userRoles.add("EOR");
    userProfileDto.setUserId("");
    userProfileDto.setRoles(userRoles);
    userProfileDto.setStatus(Collections.emptyList());
    userProfileDto.setEmail("");
    userProfileDto.setIncludedUsers(includedUsers);
    userProfileDto.setExcludedUsers(new ArrayList<>());
    List<UserProfiles> userList = userProfileDao.getUsers(userProfileDto, null, null, null, null);
    Assert.assertFalse(userList.isEmpty());
    Assert.assertEquals(USER_EMAIL_1, userList.get(0).getEmail().toString());

    // excluded users with multiple role
    final UserProfileDto userProfileDto1 = new UserProfileDto();
    List<String> excludedUsers = new ArrayList<>();
    userRoles.add("CLIENT");
    excludedUsers.add(USER_EMAIL_1);
    excludedUsers.add(USER_EMAIL_2);
    userProfileDto1.setUserId("");
    userProfileDto1.setRoles(userRoles);
    userProfileDto1.setStatus(Collections.emptyList());
    userProfileDto1.setEmail("");
    userProfileDto1.setExcludedUsers(excludedUsers);
    final List<UserProfiles> userList1 = userProfileDao.getUsers(userProfileDto1, null, null, null, null);
    Assert.assertFalse(userList1.isEmpty());

    // excluded with userId.
    final UserProfileDto userProfileDto2 = new UserProfileDto();
    excludedUsers.add(USER_EMAIL_1);
    excludedUsers.add(USER_EMAIL_2);
    userProfileDto2.setUserId(EOR_USER_ID);
    userProfileDto2.setStatus(Collections.emptyList());
    userProfileDto2.setEmail("");
    final List<UserProfiles> userList2 = userProfileDao.getUsers(userProfileDto2, null, null, null, null);
    Assert.assertFalse(userList2.isEmpty());

    // codesearch with valid client code.
    UserProfileDto userProfileDto3 = new UserProfileDto();
    UserCodeSearchDto search = new UserCodeSearchDto();
    search.setType(CLIENT);
    search.setCode(CLIENT_CODE2);
    userProfileDto3.setUserId("");
    userProfileDto3.setStatus(Collections.emptyList());
    userProfileDto3.setEmail("");
    userProfileDto3.setCodeSearch(search);
    List<UserProfiles> userList3 = userProfileDao.getUsers(userProfileDto3, null, null, null, null);
    Assert.assertFalse(userList3.isEmpty());
    Assert.assertEquals(CLIENT_CODE2.toString(), userList3.get(0).getClientCode().toString());
    // test with empty code
    search.setCode("");
    userProfileDto3.setCodeSearch(search);
    userList3 = userProfileDao.getUsers(userProfileDto3, null, null, null, null);
    Assert.assertFalse(userList3.isEmpty());

    // code search with flagcode
    search.setType(FLAG);
    search.setCode(FLAG_CODE);
    userProfileDto3.setCodeSearch(search);
    userList3 = userProfileDao.getUsers(userProfileDto3, null, null, null, null);
    Assert.assertFalse(userList3.isEmpty());
    Assert.assertEquals(FLAG_CODE.toString(), userList3.get(0).getFlagCode().toString());

    // code search with invalid flagcode
    search.setType(FLAG);
    search.setCode("dd");
    userProfileDto3.setCodeSearch(search);
    userList3 = userProfileDao.getUsers(userProfileDto3, null, null, null, null);
    Assert.assertTrue(userList3.isEmpty());

    // code search with invalid shipbuildercode
    search.setType(SHIP_BUILDER);
    search.setCode(CLIENT_CODE.toString());
    userProfileDto3.setCodeSearch(search);
    userList3 = userProfileDao.getUsers(userProfileDto3, null, null, null, null);
    Assert.assertTrue(userList3.isEmpty());

    search.setCode(SHIP_BUILDER_CODE2);
    userProfileDto3.setCodeSearch(search);
    userList3 = userProfileDao.getUsers(userProfileDto3, null, null, null, null);
    Assert.assertFalse(userList3.isEmpty());
    Assert.assertEquals(SHIP_BUILDER_CODE2.toString(), userList3.get(0).getShipBuilderCode().toString());

    // filter by date of last login with min and max dates valid min and max dates.
    userProfileDto.setUserId("");
    userProfileDto.setRoles(null);
    userProfileDto.setStatus(Collections.emptyList());
    userProfileDto.setEmail("");
    userProfileDto.setIncludedUsers(null);
    userProfileDto.setExcludedUsers(new ArrayList<>());
    userProfileDto.setDateOfLastLoginMin("2016-01-04");
    userProfileDto.setDateOfLastLoginMax("2017-01-31");
    userList = userProfileDao.getUsers(userProfileDto, null, null, null, null);
    Assert.assertFalse(userList.isEmpty());
    Assert.assertTrue(userList.get(0).getLastLogin().isAfter(
        LocalDateTime.of(LocalDate.parse(userProfileDto.getDateOfLastLoginMin(), DateTimeFormatter.ISO_LOCAL_DATE),
            LocalTime.MIDNIGHT)));
    Assert.assertTrue(userList.get(userList.size() - 1).getLastLogin().isBefore(
        LocalDateTime.of(LocalDate.parse(userProfileDto.getDateOfLastLoginMax(), DateTimeFormatter.ISO_LOCAL_DATE),
            LocalTime.MIDNIGHT).plusDays(1)));

    // with min range
    userProfileDto.setDateOfLastLoginMin("2016-01-04");
    userProfileDto.setDateOfLastLoginMax(null);
    userList = userProfileDao.getUsers(userProfileDto, null, null, null, null);
    Assert.assertFalse(userList.isEmpty());
    Assert.assertTrue(userList.get(0).getLastLogin().isAfter(
        LocalDateTime.of(LocalDate.parse(userProfileDto.getDateOfLastLoginMin(), DateTimeFormatter.ISO_LOCAL_DATE),
            LocalTime.MIDNIGHT)));
    Assert.assertFalse(userList.get(userList.size() - 1).getLastLogin().isBefore(
        LocalDateTime.of(LocalDate.parse(userProfileDto.getDateOfLastLoginMin(), DateTimeFormatter.ISO_LOCAL_DATE),
            LocalTime.MIDNIGHT)));

    // with max range
    userProfileDto.setDateOfLastLoginMax("2016-01-03");
    userList = userProfileDao.getUsers(userProfileDto, null, null, null, null);
    Assert.assertTrue(userList.isEmpty());
  }

  /**
   * Ensures that returned users are ordered by ASCENDING email order.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostTestOrdering() throws ClassDirectException {
    // Scenario: No Criteria
    UserProfileDto dto = new UserProfileDto();
    List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    List<String> userEmails = userList.stream().map(user -> user.getEmail()).collect(Collectors.toList());

    Assert.assertNull(null, userEmails.get(0)); // alphabetically null emails come first.

    // Scenario: 1 Criteria
    UserProfileDto dto1 = new UserProfileDto();
    List<String> includedUsers1 = new ArrayList<>();
    includedUsers1.add(USER_EMAIL_1);
    includedUsers1.add(USER_EMAIL_2);
    includedUsers1.add(USER_EMAIL_3);
    dto1.setIncludedUsers(includedUsers1);

    List<UserProfiles> userList1 =
        userProfileDao.getUsers(dto1, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    List<String> userEmails1 = userList1.stream().map(user -> user.getEmail()).collect(Collectors.toList());
    Assert.assertEquals("krish@bae.com", userEmails1.get(0));
    Assert.assertEquals("puppy@bae.com", userEmails1.get(1));
    Assert.assertEquals("sesh@bae.com", userEmails1.get(2));

    // Scenario: > 1 Criteria
    UserProfileDto dto2 = new UserProfileDto();

    List<String> includedUsers2 = new ArrayList<>();
    includedUsers2.add(USER_EMAIL_2);
    includedUsers2.add(USER_EMAIL_3);
    dto2.setIncludedUsers(includedUsers2);

    List<String> roles2 = new ArrayList<>();
    roles2.add("CLIENT");
    dto2.setRoles(roles2);

    List<UserProfiles> userList2 =
        userProfileDao.getUsers(dto2, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    List<String> userEmails2 = userList2.stream().map(user -> user.getEmail()).collect(Collectors.toList());
    Assert.assertEquals("krish@bae.com", userEmails2.get(0));
    Assert.assertEquals("sesh@bae.com", userEmails2.get(1));
  }


  /**
   * Ensures that returned users are ordered by Ascending/Descending email order.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostTestOrderingByEmailSort() throws ClassDirectException {
    // Scenario: No Criteria
    UserProfileDto dto = new UserProfileDto();
    List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    List<String> userEmails = userList.stream().map(user -> user.getEmail()).collect(Collectors.toList());

    Assert.assertNull(null, userEmails.get(0)); // alphabetically null emails come first.

    // Scenario: 1 Criteria
    UserProfileDto dto1 = new UserProfileDto();
    List<String> includedUsers1 = new ArrayList<>();
    includedUsers1.add(USER_EMAIL_1);
    includedUsers1.add(USER_EMAIL_2);
    includedUsers1.add(USER_EMAIL_3);
    dto1.setIncludedUsers(includedUsers1);

    List<UserProfiles> userList1 = userProfileDao.getUsers(dto1, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE,
        UserSortOptionEnum.Email.toString(), OrderOptionEnum.ASC.toString());
    List<String> userEmails1 = userList1.stream().map(user -> user.getEmail()).collect(Collectors.toList());
    Assert.assertEquals("krish@bae.com", userEmails1.get(0));
    Assert.assertEquals("puppy@bae.com", userEmails1.get(1));
    Assert.assertEquals("sesh@bae.com", userEmails1.get(2));

    // Scenario: > 1 Criteria
    UserProfileDto dto2 = new UserProfileDto();

    List<String> includedUsers2 = new ArrayList<>();
    includedUsers2.add(USER_EMAIL_2);
    includedUsers2.add(USER_EMAIL_3);
    dto2.setIncludedUsers(includedUsers2);

    List<String> roles2 = new ArrayList<>();
    roles2.add("CLIENT");
    dto2.setRoles(roles2);

    List<UserProfiles> userList2 = userProfileDao.getUsers(dto2, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE,
        UserSortOptionEnum.Email.toString(), OrderOptionEnum.DESC.toString());
    List<String> userEmails2 = userList2.stream().map(user -> user.getEmail()).collect(Collectors.toList());
    Assert.assertEquals("sesh@bae.com", userEmails2.get(0));
    Assert.assertEquals("krish@bae.com", userEmails2.get(1));
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostTestforEorUser() throws ClassDirectException {
    UserProfileDto dto = new UserProfileDto();
    dto.setUserId("107");
    List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertFalse(userList.isEmpty());
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test(expected = ClassDirectException.class)
  public final void getUsersPostTestFailforEorUser() throws ClassDirectException {
    UserProfileDto dto = new UserProfileDto();
    Integer initial = 0;
    dto.setUserId("101");
    List<UserProfiles> userList = userProfileDao.getUsers(dto, initial, initial, null, null);
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostNullTest() throws ClassDirectException {
    final List<UserProfiles> userList =
        userProfileDao.getUsers(null, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertTrue(userList.size() > 0);
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostQueryNullTest() throws ClassDirectException {
    final UserProfileDto dto = new UserProfileDto();
    final List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertTrue(userList.size() > 0);
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostQueryUserFullNameTest() throws ClassDirectException {
    final UserProfileDto dto = new UserProfileDto();
    dto.setName("V");
    final List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertFalse(userList.isEmpty());
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostQueryUserCompanyNameTest() throws ClassDirectException {
    final UserProfileDto dto = new UserProfileDto();
    dto.setCompanyName(COMPANY_NAME);
    final List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertFalse(userList.isEmpty());
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostQueryPartialNullTest() throws ClassDirectException {
    final UserProfileDto dto = new UserProfileDto();
    dto.setUserId(null);
    dto.setRoles(null);
    dto.setStatus(null);
    dto.setEmail(null);
    dto.setIncludedUsers(new ArrayList<>());
    dto.setExcludedUsers(null);
    final List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertTrue(userList.size() > 0);
  }

  /**
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersPostQueryPartialNullTests() throws ClassDirectException {
    final UserProfileDto dto = new UserProfileDto();
    dto.setUserId(null);
    dto.setRoles(null);
    dto.setStatus(null);
    dto.setEmail("abc");
    dto.setIncludedUsers(new ArrayList<>());
    dto.setExcludedUsers(new ArrayList<>());
    final List<UserProfiles> userList =
        userProfileDao.getUsers(dto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertTrue(userList.size() > 0);
  }

  /**
   * DAO should return active user only.
   *
   * @throws RecordNotFoundException when no record found.
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  @Test
  public final void returnActiveUserGivenUserId() throws RecordNotFoundException, InactiveAccountException {
    final UserProfiles userProfiles = userProfileDao.getActiveUser(ACTIVE_USER_ID);
    Assert.assertNotNull(userProfiles);
    Assert.assertEquals(AccountStatusEnum.ACTIVE.getId(), userProfiles.getStatus().getId());
  }

  /**
   * DAO should throw record not found if user is inactive.
   *
   * @throws RecordNotFoundException when no user found.
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  @Test(expected = InactiveAccountException.class)
  public final void shouldThrowErrorIfUserIsNotActive() throws RecordNotFoundException, InactiveAccountException {
    userProfileDao.getActiveUser(INACTIVE_USER);
  }

  /**
   * @throws UnauthorisedAccessException when access restricted.
   * @throws RecordNotFoundException when record not found.
   */
  @Test
  public final void getRequestedUserLRAdminSuccess() throws UnauthorisedAccessException, RecordNotFoundException {
    final UserProfiles requestedUser = userProfileDao.getRequestedUser(CLIENT_USER_ID);
    Assert.assertNotNull(requestedUser);
  }

  /**
   * @throws UnauthorisedAccessException when access restricted.
   * @throws RecordNotFoundException when record not found.
   */
  @Test
  public final void getUsersAsDualAdminRole() throws UnauthorisedAccessException, RecordNotFoundException {
    final List<UserProfiles> users = userProfileDao.getUsers(DUAL_ADMIN_ROLE_USER);
    Assert.assertFalse(users.isEmpty());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void getUserNotifications() throws Exception {
    final UserProfiles userProfiles = userProfileDao.getUser(NOTIFICATIONS_USER_ID);
    Assert.assertNotNull(userProfiles);
    Assert.assertFalse(userProfiles.getNotifications().isEmpty());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void disabledExpiredUsers() throws Exception {
    final int result = userProfileDao.disableExpiredUsers();
    Assert.assertTrue(result >= 0);
  }

  /**
   * Tests success scenario to get expired eor users {@link UserProfileDao#getExpiredEorUsers()}.
   *
   * @throws ClassDirectException if error in querying database.
   */
  @Test
  public final void getExpiredEorUsers() throws ClassDirectException {
    final List<UserEOR> result = userProfileDao.getExpiredEorUsers();
    Assert.assertNotNull(result);
    Assert.assertFalse(result.isEmpty());
    Assert.assertTrue(result.size() >= 0);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createUserTestforEOR() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID_5);
    newUser.setEmail("sushma.bollu@baesystems.com");
    newUser.setRole(Role.EOR.toString());
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    final List<UserEORDto> eors = new ArrayList<>();
    final UserEORDto eor1 = new UserEORDto();
    eor1.setImoNumber("7653986");
    eor1.setTmReport(false);
    eor1.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor1);
    final UserEORDto eor2 = new UserEORDto();
    eor2.setImoNumber("7653987");
    eor2.setTmReport(true);
    eor2.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor2);
    newUser.setEor(eors);

    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("sushma.bollu@baesystems.com");
    Assert.assertEquals(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE), user.getUserAccExpiryDate());
    Assert.assertTrue(user.getEors().size() > 0);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createUserTestforShipBuilder() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID_2);
    newUser.setEmail("venkat.bollu@baesystems.com");
    newUser.setRole(Role.SHIP_BUILDER.toString());
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    newUser.setShipBuilderCode(SHIP_BUILDER_CODE);
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("venkat.bollu@baesystems.com");
    Assert.assertEquals(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE), user.getUserAccExpiryDate());
    Assert.assertEquals(SHIP_BUILDER_CODE, user.getShipBuilderCode());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createUserTestforFlagUser() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID_6);
    newUser.setEmail("john@baesystems.com");
    newUser.setRole(Role.FLAG.toString());
    newUser.setFlagCode("CFG");
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("john@baesystems.com");
    Assert.assertEquals("CFG", user.getFlagCode());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createUserTestforClientUser() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID_3);
    newUser.setEmail("sirisha@baesystems.com");
    newUser.setRole(Role.CLIENT.toString());
    newUser.setClientCode(CLIENT_CODE);
    newUser.setCustomerType(CLIENT_TYPE);
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("sirisha@baesystems.com");
    Assert.assertEquals(CLIENT_CODE, user.getClientCode());
    Assert.assertEquals(CLIENT_TYPE, user.getClientType());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createUserWithOutClientCodeTest() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID);
    newUser.setEmail("siri@baesystems.com");
    newUser.setRole(Role.CLIENT.toString());
    newUser.setCustomerType(CLIENT_TYPE);
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("siri@baesystems.com");
    Assert.assertNull(user.getClientCode());
    Assert.assertNull(user.getClientType());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test(expected = InvalidEmailException.class)
  public final void createUserTestforInvalidMail() throws Exception {
    final CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(LR_ADMIN_USER_ID);
    newUser.setEmail("abc");
    newUser.setShipBuilderCode(SHIP_BUILDER_CODE);
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * This method test for empty user id.
   *
   * @throws Exception unexpected exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserTestforEmptyUserId() throws Exception {
    final CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId("");
    newUser.setEmail("testuser2.classdirect@gmail.com");
    newUser.setRole(Role.FLAG.toString());
    newUser.setShipBuilderCode(SHIP_BUILDER_CODE);
    userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * This method test for null create new user dto.
   *
   * @throws Exception unexpected exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserTestforNullCreateNewUserDto() throws Exception {
    final CreateNewUserDto newUser = null;
    userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * This method test for empty user id.
   *
   * @throws Exception unexpected exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserTestforNullUserId() throws Exception {
    final CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setEmail("testuser2.classdirect@gmail.com");
    newUser.setRole(Role.FLAG.toString());
    newUser.setShipBuilderCode(SHIP_BUILDER_CODE);
    userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * This method test for duplicate user id.
   *
   * @throws Exception unexpected exception.
   */
  @Test(expected = EntityExistsException.class)
  public final void createUserTestforDuplicateUserId() throws Exception {
    final CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(LR_ADMIN_USER_ID);
    newUser.setEmail("testuser2.classdirect@gmail.com");
    newUser.setRole(Role.FLAG.toString());
    newUser.setFlagCode("ALA");
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test(expected = InvalidEmailException.class)
  public final void createUserTestforNullMail() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(LR_ADMIN_USER_ID);
    newUser.setEmail("");
    newUser.setShipBuilderCode(SHIP_BUILDER_CODE);
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void updateEORTestforAddNewEORtoUser() throws Exception {
    UserProfiles userBeforeAddEor = userProfileDao.getUser(EOR_USER_ID);
    Assert.assertTrue(userBeforeAddEor.getEors().isEmpty());
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    final List<UserEORDto> eors = new ArrayList<>();
    final UserEORDto eor1 = new UserEORDto();
    eor1.setImoNumber("7653986");
    eor1.setTmReport(false);
    eor1.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor1);
    final UserEORDto eor2 = new UserEORDto();
    eor2.setImoNumber("7653987");
    eor2.setTmReport(true);
    eor2.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor2);
    newUser.setEor(eors);
    StatusDto status = userProfileDao.updateEORs(LR_ADMIN_USER_ID, EOR_USER_ID, newUser);
    Assert.assertEquals(String.format(userEORSuccessfulCreate), status.getMessage());
    UserProfiles userAfterAddEor = userProfileDao.getUser(EOR_USER_ID);
    Assert.assertTrue(userAfterAddEor.getEors().size() == 2);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void updateEORTest() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    List<UserEORDto> eors = new ArrayList<>();
    UserEORDto eor1 = new UserEORDto();
    eor1.setImoNumber("7653986");
    eor1.setTmReport(true);
    eor1.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor1);
    UserEORDto eor2 = new UserEORDto();
    eor2.setImoNumber("7653987");
    eor2.setTmReport(false);
    eor2.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor2);
    newUser.setEor(eors);
    StatusDto status = userProfileDao.updateEORs(LR_ADMIN_USER_ID, ROLE_TEST_USER_ID, newUser);
    Assert.assertEquals(String.format(userEORSuccessfulUpdate), status.getMessage());
    UserProfiles user = userProfileDao.getUser(ALICE.getId());
    Assert.assertFalse(user.getEors().isEmpty());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getEORTestforNullValues() throws Exception {
    UserEOR eor = userProfileDao.getEOR(null, null);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void verifyUserByEmailTest() throws Exception {
    UserProfiles profile = userProfileDao.verifyUserByEmail("puppy1@bae.com");
    Assert.assertEquals("puppy1@bae.com", profile.getEmail());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void updateEORauthorizationTest() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    StatusDto profile = userProfileDao.updateEORs(ROLE_TEST_USER_ID, ROLE_TEST_USER_ID, newUser);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test(expected = IllegalArgumentException.class)
  public final void updateEORIlegalArgument() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    StatusDto profile = userProfileDao.updateEORs(LR_ADMIN_USER_ID, ALICE.getId(), newUser);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void getUserIsCDUserTest() throws Exception {
    final UserProfiles userProfiles = userProfileDao.getUser(LR_SUPPORT_USER_ID);
    Assert.assertNotNull(userProfiles);
    Assert.assertTrue(userProfiles.isCdUser());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void getUserTermsConditions() throws Exception {
    final UserProfiles userProfiles = userProfileDao.getUser(ACCEPTED_TERMS_USER_ID);
    Assert.assertNotNull(userProfiles);
    Assert.assertFalse(userProfiles.getUserTCs().isEmpty());
  }

  /**
   * Unit test to get latest terms&Conditions.
   *
   * @throws Exception if exception occured.
   */
  @Test
  public final void getLatestTermsConditionsTest() throws Exception {
    final TermsConditions tc = userProfileDao.getLatestTermsConditions();
    Assert.assertNotNull(tc);
    Assert.assertEquals(LocalDateTime.of(LATEST_YEAR, LATEST_MONTH, LATEST_DATE, LATEST_HOUR, LATEST_MIN),
        tc.getTcDate());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createNewTermsConditionsTest() throws Exception {
    final StatusDto status = userProfileDao.createAcceptedTermsandConditions(TEST1_USER_ID, ARCHIVED_STATUS_ID);
    Assert.assertEquals(String.format(userTCSuccessfulCreate), status.getMessage());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void updateTermsConditionsTest() throws Exception {
    final StatusDto status =
        userProfileDao.createAcceptedTermsandConditions(NOT_ACCEPT_TERMS_USER_ID, ARCHIVED_STATUS_ID);
    Assert.assertEquals(String.format(userTCSuccessfulUpdate), status.getMessage());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void createTermsConditionsforInvalidUser() throws Exception {
    final StatusDto status = userProfileDao.createAcceptedTermsandConditions("09bvcgdr45", ARCHIVED_STATUS_ID);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createTermsConditionsforAcceptedDateNotNull() throws Exception {
    final StatusDto status =
        userProfileDao.createAcceptedTermsandConditions(ACCEPTED_TERMS_USER_ID, ARCHIVED_STATUS_ID);
    Assert.assertEquals(String.format(userTCacceptedDateExists), status.getMessage());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void dateOfModificationTestforStatusChange() throws Exception {
    UserProfiles pendingStatusUser = userProfileDao.getUser(INACTIVE_USER_FOR_STATUS_UPDATE);
    Assert.assertNull(pendingStatusUser.getDateOfModification());
    UserProfiles newProfileValuestoSet = new UserProfiles();
    UserAccountStatus newStatus = new UserAccountStatus();
    newStatus.setId(AccountStatusEnum.ACTIVE.getId());
    newProfileValuestoSet.setStatus(newStatus);
    final UserProfiles upStatus =
        userProfileDao.updateUser(LR_ADMIN_USER_ID, INACTIVE_USER_FOR_STATUS_UPDATE, newProfileValuestoSet, null);
    Assert.assertNotNull(upStatus.getDateOfModification());
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test
  public final void dateOfModificationTestforNoStatusChange() throws Exception {
    UserProfiles newProfileValuestoSet = new UserProfiles();
    newProfileValuestoSet.setFlagCode("CH2");
    final UserProfiles upStatus =
        userProfileDao.updateUser(LR_ADMIN_USER_ID, TEST1_USER_ID, newProfileValuestoSet, null);
    Assert.assertEquals("CH2", upStatus.getFlagCode());
    Assert.assertNull(upStatus.getDateOfModification());
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test
  public final void getEorsByUserIdTest() throws ClassDirectException {
    Set<String> eorList = userProfileDao.getEorsByUserId(ROLE_TEST_USER_ID);
    Assert.assertTrue(!eorList.isEmpty());
    Assert.assertTrue(eorList.size() > 0);
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test
  public final void getEorsByUserIdTestforNoEorsUserId() throws ClassDirectException {
    Set<String> eorList = userProfileDao.getEorsByUserId(STATUS_NULL_USER_ID);
    Assert.assertTrue(eorList.isEmpty());
    Assert.assertTrue(eorList.size() == 0);
  }

  /**
   * @throws ClassDirectException exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getEorsByUserIdTestforUserIdNull() throws ClassDirectException {
    Set<String> eorList = userProfileDao.getEorsByUserId(null);
  }

  /**
   * method to test userscount from db with all scenarios.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersCountTestWithSpecialCharacterCompanyName() throws ClassDirectException {
    // checking with email
    final Integer count = 1;
    final UserProfileDto dto = new UserProfileDto();
    dto.setCompanyName(COMPANY_NAME);
    final Long usersCount = userProfileDao.getUsersCount(dto);
    Assert.assertNotNull(usersCount);
    Assert.assertEquals(count.intValue(), usersCount.intValue());
  }

  /**
   * method to test userscount from db with all scenarios.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersCountTest() throws ClassDirectException {
    final UserProfileDto dto = new UserProfileDto();
    final List<String> emptyList = Collections.emptyList();

    // checking with email
    Integer count = 1;
    List<String> includedUsers = new ArrayList<>();
    List<String> excludedUsers = new ArrayList<>();
    dto.setUserId("");
    dto.setRoles(new ArrayList<>());
    dto.setStatus(Collections.emptyList());
    dto.setEmail(USER_EMAIL_1);
    dto.setIncludedUsers(includedUsers);
    dto.setExcludedUsers(new ArrayList<>());
    Long usersCount = userProfileDao.getUsersCount(dto);
    Assert.assertNotNull(usersCount);
    Assert.assertEquals(count.intValue(), usersCount.intValue());

    final UserProfileDto dto1 = new UserProfileDto();
    dto1.setUserId("");
    dto1.setRoles(null);
    dto1.setStatus(Collections.emptyList());
    dto1.setEmail("");
    dto1.setIncludedUsers(emptyList);
    dto1.setExcludedUsers(emptyList);
    usersCount = userProfileDao.getUsersCount(dto1);
    Assert.assertNotNull(usersCount);

    final UserProfileDto dto2 = new UserProfileDto();
    dto2.setUserId(null);
    dto2.setRoles(null);
    dto2.setStatus(null);
    dto2.setEmail(null);
    dto2.setIncludedUsers(null);
    dto2.setExcludedUsers(null);
    usersCount = userProfileDao.getUsersCount(dto2);
    Assert.assertNotNull(usersCount);

    // checking with included users
    final UserProfileDto dto3 = new UserProfileDto();
    count++;
    List<String> userRoles = new ArrayList<>();
    userRoles.add("LR_ADMIN");
    userRoles.add("CLIENT");
    includedUsers.add(USER_EMAIL_2);
    includedUsers.add(USER_EMAIL_3);
    dto3.setUserId("");
    dto3.setRoles(userRoles);
    dto3.setStatus(Collections.singletonList(AccountStatusEnum.ACTIVE.getName()));
    dto3.setEmail("");
    dto3.setIncludedUsers(includedUsers);
    dto3.setExcludedUsers(emptyList);
    usersCount = userProfileDao.getUsersCount(dto3);
    Assert.assertNotNull(usersCount);
    Assert.assertEquals(count.intValue(), usersCount.intValue());

    // checking with excluded users
    final UserProfileDto dto4 = new UserProfileDto();
    excludedUsers.add(USER_EMAIL_1);
    dto4.setUserId(null);
    dto4.setRoles(null);
    dto4.setStatus(null);
    dto4.setEmail(null);
    dto4.setExcludedUsers(excludedUsers);
    dto4.setIncludedUsers(null);
    usersCount = userProfileDao.getUsersCount(dto4);
    Assert.assertNotNull(usersCount);
    Assert.assertTrue(usersCount.intValue() > MAX_SIZE);
    final UserProfileDto dto5 = new UserProfileDto();
    excludedUsers.add("");
    dto5.setUserId(ACTIVE_USER_ID);
    dto5.setRoles(userRoles);
    dto5.setStatus(null);
    dto5.setEmail(null);
    dto5.setExcludedUsers(excludedUsers);
    dto5.setIncludedUsers(null);
    usersCount = userProfileDao.getUsersCount(dto5);
    Assert.assertNotNull(usersCount);
    Assert.assertEquals(usersCount, new Long(0));

    // dto as null
    usersCount = userProfileDao.getUsersCount(null);
    Assert.assertNotNull(usersCount);
  }

  /**
   * method to test userscount from db with all scenarios.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getUsersCountTestWithMultipleStatuses() throws ClassDirectException {
    final UserProfileDto dto = new UserProfileDto();
    final List<String> emptyList = Collections.emptyList();

    // checking with email
    Integer count = 1;
    List<String> includedUsers = new ArrayList<>();
    List<String> excludedUsers = new ArrayList<>();
    dto.setUserId("");
    dto.setRoles(new ArrayList<>());
    dto.setStatus(Collections.emptyList());
    dto.setEmail(USER_EMAIL_1);
    dto.setIncludedUsers(includedUsers);
    dto.setExcludedUsers(new ArrayList<>());
    Long usersCount = userProfileDao.getUsersCount(dto);
    Assert.assertNotNull(usersCount);
    Assert.assertEquals(count.intValue(), usersCount.intValue());

    final UserProfileDto dto1 = new UserProfileDto();
    dto1.setUserId("");
    dto1.setRoles(null);
    final List<String> statusList = new ArrayList<>();
    statusList.add(AccountStatusEnum.ACTIVE.getName());
    statusList.add(AccountStatusEnum.ARCHIVED.getName());
    statusList.add(AccountStatusEnum.DISABLED.getName());
    dto1.setStatus(statusList);
    dto1.setEmail("");
    dto1.setIncludedUsers(emptyList);
    dto1.setExcludedUsers(emptyList);
    usersCount = userProfileDao.getUsersCount(dto1);
    Assert.assertNotNull(usersCount);

    final UserProfileDto dto2 = new UserProfileDto();
    dto2.setUserId(null);
    dto2.setRoles(null);
    final List<String> statusList2 = new ArrayList<>();
    statusList.add(AccountStatusEnum.ACTIVE.getName());
    statusList.add(AccountStatusEnum.ARCHIVED.getName());
    dto2.setStatus(statusList2);
    dto2.setEmail(null);
    dto2.setIncludedUsers(null);
    dto2.setExcludedUsers(null);
    usersCount = userProfileDao.getUsersCount(dto2);
    Assert.assertNotNull(usersCount);

    // checking with included users
    final UserProfileDto dto3 = new UserProfileDto();
    count++;
    List<String> userRoles = new ArrayList<>();
    userRoles.add("LR_ADMIN");
    userRoles.add("CLIENT");
    includedUsers.add(USER_EMAIL_2);
    includedUsers.add(USER_EMAIL_3);
    dto3.setUserId("");
    dto3.setRoles(userRoles);
    dto3.setStatus(Collections.singletonList(AccountStatusEnum.ACTIVE.getName()));
    dto3.setEmail("");
    dto3.setIncludedUsers(includedUsers);
    dto3.setExcludedUsers(emptyList);
    usersCount = userProfileDao.getUsersCount(dto3);
    Assert.assertNotNull(usersCount);
    Assert.assertEquals(count.intValue(), usersCount.intValue());

    // checking with excluded users
    final UserProfileDto dto4 = new UserProfileDto();
    excludedUsers.add(USER_EMAIL_1);
    dto4.setUserId(null);
    dto4.setRoles(null);
    dto4.setStatus(null);
    dto4.setEmail(null);
    dto4.setExcludedUsers(excludedUsers);
    dto4.setIncludedUsers(null);
    usersCount = userProfileDao.getUsersCount(dto4);
    Assert.assertNotNull(usersCount);
    Assert.assertTrue(usersCount.intValue() > MAX_SIZE);
    final UserProfileDto dto5 = new UserProfileDto();
    excludedUsers.add("");
    dto5.setUserId(ACTIVE_USER_ID);
    dto5.setRoles(userRoles);
    dto5.setStatus(null);
    dto5.setEmail(null);
    dto5.setExcludedUsers(excludedUsers);
    dto5.setIncludedUsers(null);
    usersCount = userProfileDao.getUsersCount(dto5);
    Assert.assertNotNull(usersCount);
    Assert.assertEquals(usersCount, new Long(0));

    // dto as null
    usersCount = userProfileDao.getUsersCount(null);
    Assert.assertNotNull(usersCount);
  }

  /**
   * Update Users by LRSupport.
   *
   * @throws Exception unexpected exception.
   */
  @Test
  public final void updateUsersLRSupportTestSuccess() throws Exception {
    final UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(DISABLED_STATUS_ID);
    status.setName("Disabled");
    userProfiles.setStatus(status);
    userProfiles.setEmail("ABC@bae");
    userProfiles.setUserAccExpiryDate(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));

    final UserProfiles user = userProfileDao.updateUser(LR_SUPPORT_USER_ID, CLIENT_USER_ID, userProfiles, "");
    Assert.assertEquals("status Id set to 2", DISABLED_STATUS_ID, user.getStatus().getId());
    Assert.assertNotEquals("set Email", "ABC@bae", user.getEmail());
    Assert.assertEquals("Expected expiredDate", LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE),
        user.getUserAccExpiryDate());
  }

  /**
   * Create User by LRSupport.
   *
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createEORUserTestforLRSupport() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID_7);
    newUser.setEmail("sushma.bollu_new@baesystems.com");
    newUser.setRole(Role.EOR.toString());
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    final List<UserEORDto> eors = new ArrayList<>();
    final UserEORDto eor1 = new UserEORDto();
    eor1.setImoNumber("7653986");
    eor1.setTmReport(false);
    eor1.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor1);
    final UserEORDto eor2 = new UserEORDto();
    eor2.setImoNumber("7653987");
    eor2.setTmReport(true);
    eor2.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor2);
    newUser.setEor(eors);

    final StatusDto status = userProfileDao.createUser(LR_SUPPORT_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("sushma.bollu_new@baesystems.com");
    Assert.assertEquals(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE), user.getUserAccExpiryDate());
    Assert.assertTrue(user.getEors().size() > 0);
  }

  /**
   * Update EOR's by LRSupport.
   *
   * @throws Exception unexpected exception.
   */
  @Test
  public final void lrSupportUpdateEORTestforAddNewEORtoUser() throws Exception {
    UserProfiles userBeforeAddEor = userProfileDao.getUser(TEST1_USER_ID);
    Assert.assertTrue(userBeforeAddEor.getEors().isEmpty());
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    final List<UserEORDto> eors = new ArrayList<>();
    final UserEORDto eor1 = new UserEORDto();
    eor1.setImoNumber("7653986");
    eor1.setTmReport(false);
    eor1.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor1);
    final UserEORDto eor2 = new UserEORDto();
    eor2.setImoNumber("7653987");
    eor2.setTmReport(true);
    eor2.setAssetExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    eors.add(eor2);
    newUser.setEor(eors);
    StatusDto status = userProfileDao.updateEORs(LR_SUPPORT_USER_ID, TEST1_USER_ID, newUser);
    Assert.assertEquals(String.format(userEORSuccessfulCreate), status.getMessage());
    UserProfiles userAfterAddEor = userProfileDao.getUser(TEST1_USER_ID);
    Assert.assertTrue(userAfterAddEor.getEors().size() == 2);
  }

  /**
   * Create LRAdmin User by LRSupport.
   *
   * @throws Exception unexpected exception.
   */
  @Test
  public final void createLRAdminUserTest() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID_9);
    newUser.setEmail("sushma.bollu_LRAdmin@baesystems.com");
    newUser.setRole(Role.LR_ADMIN.toString());
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    final StatusDto status = userProfileDao.createUser(LR_SUPPORT_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("sushma.bollu_LRAdmin@baesystems.com");
    Assert.assertEquals(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE), user.getUserAccExpiryDate());
  }

  /**
   * LRAdmin fails to create LRAdmin user.
   *
   * @throws Exception unexpected exception.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void createLRAdminUserTestFail() throws Exception {
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(NEW_USER_ID_11);
    newUser.setEmail("sushma.bollu_LR@baesystems.com");
    newUser.setRole(Role.LR_ADMIN.toString());
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    final StatusDto status = userProfileDao.createUser(LR_ADMIN_USER_ID, newUser);
    Assert.assertEquals(String.format(userSuccessfulCreate, newUser.getEmail()), status.getMessage());
    UserProfiles user = userProfileDao.verifyUserByEmail("sushma.bollu_LRAdmin@baesystems.com");
    Assert.assertEquals(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE), user.getUserAccExpiryDate());
  }

  /**
   * Test case for updating userprofile's full name and company name.
   *
   * @throws ClassDirectException unexpected exception.
   */
  @Test
  public final void updateUserFullNameAndCompanyName() throws ClassDirectException {
    UserProfiles newUserprofile = new UserProfiles();
    newUserprofile.setUserId(LR_ADMIN_2_USER_ID);
    newUserprofile.setName("Vicknesh Muthuveerapan");
    newUserprofile.setCompanyName(COMPANY_NAME);
    userProfileDao.updateUserFullNameAndCompanyName(newUserprofile);
    UserProfiles user = userProfileDao.getUser(newUserprofile.getUserId());
    Assert.assertEquals(newUserprofile.getName(), user.getName());
    Assert.assertEquals(newUserprofile.getCompanyName(), user.getCompanyName());
  }

  /**
   * Tests successful deletion of eor for userid and imonumber.
   *
   * @throws ClassDirectException if exception occurred.
   */
  @Test
  public final void removeEorTest() throws ClassDirectException {
    StatusDto eor = userProfileDao.removeEOR(TEST2_USER_ID, IMO_NUM);
    Assert.assertEquals(String.format(successfulDelete), eor.getMessage());
  }

  /**
   * Tests to throw exception when null values assigned to userId and imoNumber.
   *
   * @throws ClassDirectException if exception occurred.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void removeEorTestforNullValues() throws ClassDirectException {
    StatusDto eor = userProfileDao.removeEOR(null, null);
  }

  /**
   * Tests to throw exception for invalid imoNumber.
   *
   * @throws ClassDirectException if exception occurred.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void removeEorTestforNonExistValues() throws ClassDirectException {
    StatusDto eor = userProfileDao.removeEOR(TEST2_USER_ID, IMO_NUM_NOT_AVAILABLE);
  }

  /**
   * Tests to throw exception for null imoNumber.
   *
   * @throws ClassDirectException if exception occurred.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void removeEorTestforNullIMONumber() throws ClassDirectException {
    StatusDto eor = userProfileDao.removeEOR(TEST2_USER_ID, null);
  }
}
