package com.baesystems.ai.lr.cd.be.hibernate.test;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EOR;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;

/**
 * User Profile DAO - Recreate Archived User Test Cases.
 *
 * @author RKaneysan.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao-context.xml")
@PrepareForTest({SecurityUtils.class})
public class UserProfileDaoRecreateArchivedUserTest {

  /**
   * Year.
   */
  private static final Integer DEFAULT_YEAR = 2019;
  /**
   * Month.
   */
  private static final Integer DEFAULT_MONTH = 12;
  /**
   * Date.
   */
  private static final Integer DEFAULT_DATE = 9;

  /**
   * LR Admin user.
   */
  private static final String LR_ADMIN_USER_ID = "106";
  /**
   * The eor delete success message.
   */
  @Value("${eor.successful.delete}")
  private String successfulDelete;

  /**
   * Injected User Profile DAO Object.
   */
  @Autowired(required = false)
  private UserProfileDao userProfileDao;

  /**
   * This test case is to check if relevant archived user records are deleted and new records are
   * created.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testRecreateArchivedUser() throws ClassDirectException {
    // Original User record before recreation.
    final UserProfiles archivedUser = userProfileDao.getUser("128");
    final String userId = archivedUser.getUserId();
    final String userEmail = archivedUser.getEmail();

    UserProfiles userProfile = userProfileDao.getUser(userId);
    final boolean isClientUser = Optional.ofNullable(userProfile).map(UserProfiles::getRoles).get().stream()
        .anyMatch(ur -> ur.getRoleName().equals(Role.CLIENT.toString()));
    Assert.assertNotNull(userProfile);
    Assert.assertEquals(userProfile.getUserId(), userId);
    Assert.assertEquals(userProfile.getEmail(), userEmail);
    Assert.assertEquals(userProfile.getStatus().getName(), archivedUser.getStatus().getName());
    Assert.assertFalse(userProfile.getUserTCs().isEmpty());
    Assert.assertTrue(isClientUser);
    Assert.assertNull(userProfile.getFlagCode());
    Assert.assertFalse(userProfile.getEors().isEmpty());
    Assert.assertFalse(userProfile.getNotifications().isEmpty());

    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(userId);
    newUser.setEmail(userEmail);
    newUser.setRole(Role.FLAG.toString());
    newUser.setFlagCode("ALA");
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));

    userProfileDao.recreateArchivedUser(archivedUser, LR_ADMIN_USER_ID, newUser);

    // Current User Record after recreation.
    UserProfiles updatedUserProfile = userProfileDao.getUser(userId);
    final boolean isNowClientUser = Optional.ofNullable(updatedUserProfile).map(UserProfiles::getRoles).get().stream()
        .anyMatch(ur -> ur.getRoleName().equals(Role.CLIENT.toString()));
    final boolean isFlagUser = Optional.ofNullable(updatedUserProfile).map(UserProfiles::getRoles).get().stream()
        .anyMatch(ur -> ur.getRoleName().equals(Role.FLAG.toString()));
    Assert.assertNotNull(updatedUserProfile);
    Assert.assertEquals(updatedUserProfile.getUserId(), userId);
    Assert.assertEquals(updatedUserProfile.getEmail(), userEmail);
    Assert.assertEquals(updatedUserProfile.getStatus().getName(), AccountStatusEnum.ACTIVE.getName());
    Assert.assertTrue(updatedUserProfile.getUserTCs().isEmpty());
    Assert.assertFalse(isNowClientUser);
    Assert.assertTrue(isFlagUser);
    Assert.assertNotNull(updatedUserProfile.getFlagCode());
    Assert.assertEquals("ALA", updatedUserProfile.getFlagCode());
    Assert.assertTrue(updatedUserProfile.getEors().isEmpty());
    Assert.assertTrue(updatedUserProfile.getNotifications().isEmpty());
  }

  /**
   * This test case checks if relevant archived user records are rollback in case of
   * RuntimeException.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testRollbackDeleteArchivedUserWithRuntimeException() throws ClassDirectException {
    final UserProfiles archivedUser = userProfileDao.getUser("115");
    final String userId = archivedUser.getUserId();
    final String userEmail = archivedUser.getEmail();
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(userId);
    newUser.setEmail(userEmail);
    newUser.setRole("Client"); // Will cause RuntimeException as Role Enum constant can't find value
                               // for "Client".
    newUser.setClientCode("1000191");
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    try {
      userProfileDao.recreateArchivedUser(archivedUser, LR_ADMIN_USER_ID, newUser);
    } catch (final IllegalArgumentException exception) {
      // Check if rollback is executed properly.
      UserProfiles userProfile = userProfileDao.getUser(userId);
      final boolean isFlagUser = Optional.ofNullable(userProfile).map(UserProfiles::getRoles).get().stream()
          .anyMatch(ur -> ur.getRoleName().equals(Role.FLAG.toString()));
      final boolean isClientUser = Optional.ofNullable(userProfile).map(UserProfiles::getRoles).get().stream()
          .anyMatch(ur -> ur.getRoleName().equals(Role.CLIENT.toString()));
      Assert.assertNotNull(userProfile);
      Assert.assertEquals(userProfile.getUserId(), userId);
      Assert.assertEquals(userProfile.getEmail(), userEmail);
      Assert.assertEquals(userProfile.getStatus().getName(), archivedUser.getStatus().getName());
      Assert.assertFalse(isClientUser);
      Assert.assertTrue(isFlagUser);
      Assert.assertNotNull(userProfile.getFlagCode());
      Assert.assertEquals(archivedUser.getFlagCode(), userProfile.getFlagCode());
    }
  }

  /**
   * This test case checks if relevant archived user records are rollback in case of
   * ClassDirectException.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testRollbackDeleteArchivedUserWithClassDirectException() throws ClassDirectException {
    final UserProfiles archivedUser = userProfileDao.getUser("115");
    final String userId = archivedUser.getUserId();
    final String userEmail = archivedUser.getEmail();
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(userId);
    newUser.setEmail(userEmail);
    newUser.setRole(Role.CLIENT.toString());
    newUser.setClientCode("1000191");
    newUser.setAccountExpiry(LocalDate.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE));
    try {
      userProfileDao.recreateArchivedUser(archivedUser, "somethingWrong", newUser);
    } catch (final ClassDirectException exception) {
      // Check if rollback is executed properly.
      UserProfiles userProfile = userProfileDao.getUser(userId);
      final boolean isFlagUser = Optional.ofNullable(userProfile).map(UserProfiles::getRoles).get().stream()
          .anyMatch(ur -> ur.getRoleName().equals(Role.FLAG.toString()));
      final boolean isClientUser = Optional.ofNullable(userProfile).map(UserProfiles::getRoles).get().stream()
          .anyMatch(ur -> ur.getRoleName().equals(Role.CLIENT.toString()));
      Assert.assertNotNull(userProfile);
      Assert.assertEquals(userProfile.getUserId(), userId);
      Assert.assertEquals(userProfile.getEmail(), userEmail);
      Assert.assertEquals(userProfile.getStatus().getName(), archivedUser.getStatus().getName());
      Assert.assertFalse(isClientUser);
      Assert.assertTrue(isFlagUser);
      Assert.assertNotNull(userProfile.getFlagCode());
      Assert.assertEquals(archivedUser.getFlagCode(), userProfile.getFlagCode());
    }
  }

  /**
   * This test case checks if user who are not archived are not recreated.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testRecreateUnarchivedUserFail() throws ClassDirectException {
    final UserProfiles archivedUser = userProfileDao.getUser("109");
    final String userId = archivedUser.getUserId();
    final String userEmail = archivedUser.getEmail();

    UserProfiles userProfile = userProfileDao.getUser(userId);
    final boolean isLrAdminUser = Optional.ofNullable(userProfile).map(UserProfiles::getRoles).get().stream()
        .anyMatch(ur -> ur.getRoleName().equals(Role.LR_ADMIN.toString()));
    Assert.assertNotNull(userProfile);
    Assert.assertEquals(userProfile.getUserId(), userId);
    Assert.assertEquals(userProfile.getEmail(), userEmail);
    Assert.assertEquals(AccountStatusEnum.ACTIVE.getName(), userProfile.getStatus().getName());
    Assert.assertTrue(isLrAdminUser);
    try {
      userProfileDao.recreateArchivedUser(archivedUser, LR_ADMIN_USER_ID, new CreateNewUserDto());
    } catch (final ClassDirectException cdException) {
      // Check if rollback is executed properly.
      UserProfiles revertedUserProfile = userProfileDao.getUser(userId);
      final boolean isFlagUser = Optional.ofNullable(revertedUserProfile).map(UserProfiles::getRoles).get().stream()
          .anyMatch(ur -> ur.getRoleName().equals(Role.FLAG.toString()));
      final boolean isNowLrAdminUser = Optional.ofNullable(revertedUserProfile).map(UserProfiles::getRoles).get()
          .stream().anyMatch(ur -> ur.getRoleName().equals(Role.LR_ADMIN.toString()));
      Assert.assertNotNull(userProfile);
      Assert.assertEquals(userProfile.getUserId(), userId);
      Assert.assertEquals(userProfile.getEmail(), userEmail);
      Assert.assertEquals(userProfile.getStatus().getName(), AccountStatusEnum.ACTIVE.getName());
      Assert.assertTrue(isNowLrAdminUser);
      Assert.assertFalse(isFlagUser);
      Assert.assertNull(userProfile.getFlagCode());
    }
  }

  /**
   * Tests success scenario to remove user eor
   * {@link UserProfileDao#deleteUserEORRole(UserProfiles)}.
   * <p>
   * delete EOR role of user when all eor assets are deleted if user with EOR role and Client role.
   * </p>
   *
   * @throws ClassDirectException if imonumber/user id empty or eor not found with given user id or
   *         error in getting ero's.
   */
  @Test
  public final void testRemoveEorUserRoleSuccess() throws ClassDirectException {
    UserProfiles userBeforeEORDeletion = userProfileDao.getUser("130");
    StatusDto eor1 = userProfileDao.removeEOR("130", "7653986");
    Assert.assertEquals(String.format(successfulDelete), eor1.getMessage());
    StatusDto eor2 = userProfileDao.removeEOR("130", "7653987");
    Assert.assertEquals(String.format(successfulDelete), eor2.getMessage());

    UserProfiles userAfterEORDeletion = userProfileDao.getUser("130");
    userProfileDao.deleteUserEORRole(userAfterEORDeletion);

    UserProfiles userAfterEORRoleDeletion = userProfileDao.getUser("130");

    Assert.assertNotNull(userBeforeEORDeletion);
    Assert.assertNotNull(userBeforeEORDeletion.getEors());
    Assert.assertFalse(userBeforeEORDeletion.getEors().isEmpty());
    boolean isEOR = userBeforeEORDeletion.getRoles().stream().filter(role -> {
      return role.getRoleName().equalsIgnoreCase(EOR.toString());
    }).findAny().isPresent();
    Assert.assertTrue(isEOR);
    Assert.assertNotNull(userAfterEORRoleDeletion);
    Assert.assertNotNull(userAfterEORRoleDeletion.getEors());
    Assert.assertTrue(userAfterEORRoleDeletion.getEors().isEmpty());
    boolean isEOR2 = userAfterEORRoleDeletion.getRoles().stream().filter(role -> {
      return role.getRoleName().equalsIgnoreCase(EOR.toString());
    }).findAny().isPresent();
    Assert.assertFalse(isEOR2);

  }

  /**
   * Tests success scenario to remove user eor role
   * {@link UserProfileDao#deleteUserEORRole(UserProfiles)}.
   * <p>
   * keep EOR role of user when all eor assets are deleted if user with only EOR role.
   * </p>
   *
   * @throws ClassDirectException if imonumber/user id empty or eor not found with given user id or
   *         error in getting ero's.
   */
  @Test
  public final void testShouldNotDeleteUserEORRoleSuccess() throws ClassDirectException {
    UserProfiles userBeforeEORDeletion = userProfileDao.getUser("132");
    StatusDto eor1 = userProfileDao.removeEOR("132", "7653986");
    Assert.assertEquals(String.format(successfulDelete), eor1.getMessage());
    StatusDto eor2 = userProfileDao.removeEOR("132", "7653987");
    Assert.assertEquals(String.format(successfulDelete), eor2.getMessage());
    UserProfiles userAfterEORDeletion = userProfileDao.getUser("132");
    userProfileDao.deleteUserEORRole(userAfterEORDeletion);
    UserProfiles userAfterEORRoleDeletion = userProfileDao.getUser("132");
    Assert.assertNotNull(userBeforeEORDeletion);
    Assert.assertNotNull(userBeforeEORDeletion.getEors());
    Assert.assertFalse(userBeforeEORDeletion.getEors().isEmpty());
    boolean isEOR = userBeforeEORDeletion.getRoles().stream().filter(role -> {
      return role.getRoleName().equalsIgnoreCase(EOR.toString());
    }).findAny().isPresent();
    Assert.assertTrue(isEOR);
    Assert.assertNotNull(userAfterEORRoleDeletion);
    Assert.assertNotNull(userAfterEORRoleDeletion.getEors());
    Assert.assertTrue(userAfterEORRoleDeletion.getEors().isEmpty());
    boolean isEOR2 = userAfterEORRoleDeletion.getRoles().stream().filter(role -> {
      return role.getRoleName().equalsIgnoreCase(EOR.toString());
    }).findAny().isPresent();
    Assert.assertTrue(isEOR2);
  }
  /**
   * Tests to throw exception for invalid userId and imoNumber.
   *
   * @throws ClassDirectException if exception occurred.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void removeEorTestwithDifferentUserId() throws ClassDirectException {
    StatusDto eor = userProfileDao.removeEOR("107", "7653986");
  }

  /**
   * Tests success scenario to update user last login
   * {@link UserProfileDao#updateUserLastLogin(String, LocalDateTime)}.
   *
   * @throws ClassDirectException if any sql exception.
   */
  @Test
  public final void testUpdateUserLastLoginSuccess() throws ClassDirectException {
    int result = userProfileDao.updateUserLastLogin("135", LocalDateTime.of(2017, 8, 2, 1, 13, 13));
    Assert.assertNotNull(result);
    Assert.assertTrue(result > 0);
  }

  /**
   * Tests failure scenario to update user last login
   * {@link UserProfileDao#updateUserLastLogin(String, LocalDateTime)} invalid user.
   *
   * @throws ClassDirectException if any sql exception.
   */
  @Test
  public final void testUpdateUserLastLoginFail() throws ClassDirectException {
    int result = userProfileDao.updateUserLastLogin("189", LocalDateTime.of(2017, 8, 2, 1, 13, 13));
    Assert.assertNotNull(result);
    Assert.assertFalse(result > 0);
  }

}

