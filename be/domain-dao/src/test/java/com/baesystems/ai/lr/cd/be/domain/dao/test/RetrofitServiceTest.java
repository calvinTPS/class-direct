package com.baesystems.ai.lr.cd.be.domain.dao.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.baesystems.ai.lr.cd.be.domain.dao.RetrofitServiceFactory;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.customer.CustomerRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.FlagStateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.PortRegistryService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.PartyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.PaginatedResponseInterceptor;
import com.baesystems.ai.lr.cd.be.domain.test.BaseRetrofitTest;
import com.baesystems.ai.lr.cd.be.domain.test.MastResponseFiles;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsCompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.dto.query.PartyQueryDto;

import retrofit2.Call;

/**
 * Combined test unit for {@link AssetRetrofitService}.
 *
 * @author msidek
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RetrofitServiceTest.Config.class)
public class RetrofitServiceTest extends BaseRetrofitTest {

  /**
   * static id for asset.
   */
  private static final Long ASSET_ID = 3L;

  /**
   * client imo number.
   */
  private static final String CLIENT_IMO_NUMBER = "1000003";

  /**
   * static page page.
   */
  private static final Integer INITIAL_ID = 1;
  /**
   * Logger object.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(RetrofitServiceTest.class);

  /**
   * Injected {@link AssetRetrofitService}.
   *
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * Injected {@link AssetReferenceRetrofitService}.
   *
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceRetrofitService;

  /**
   * Injected {@link FlagStateRetrofitService}.
   *
   */
  @Autowired
  private FlagStateRetrofitService flagStateRetrofitService;


  /**
   * Injected {@link IhsRetrofitService}.
   *
   */
  @Autowired
  private IhsRetrofitService ihsRetrofitService;


  /**
   * Injected {@link CustomerRetrofitService}.
   *
   */
  @Autowired
  private CustomerRetrofitService customerRetrofitService;

  /**
   * Injected {@link PaginatedResponseInterceptor}.
   *
   */
  @Autowired
  private PaginatedResponseInterceptor paginatedResponseInterceptor;

  /**
   *
   */
  @Before
  public void setup() {
    List<String> paths = new ArrayList<>();
    paths.add("/asset/query");
    ReflectionTestUtils.setField(paginatedResponseInterceptor, "ignoredPaths", paths);
  }

  /**
   * Unit test for {@link AssetRetrofitService#assetQuery(AssetQueryHDto)}.
   *
   * @throws Exception when error.
   *
   */

  @Test
  public final void sendQueryForAsset() throws Exception {
    enqueueResponseFromFile("mast/asset_query/response/search_wildcard.json");

    Call<AssetPageResource> caller = assetRetrofitService.assetQuery(new AssetQueryHDto(), INITIAL_ID, INITIAL_ID);
    List<AssetHDto> assets = caller.execute().body().getContent();

    Assert.assertNotNull(assets);
    Assert.assertFalse(assets.isEmpty());

    assets.stream().forEach(asset -> LOGGER.debug(asset.toString()));
  }

  /**
   * Unit test for {@link AssetReferenceRetrofitService#getClassStatus()}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void classStatusQuery() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.CLASS_STATUS.getLocation());

    Call<List<ClassStatusHDto>> caller = assetReferenceRetrofitService.getClassStatus();
    List<ClassStatusHDto> classStatuses = caller.execute().body();
    Assert.assertNotNull(classStatuses);
    Assert.assertFalse(classStatuses.isEmpty());
    classStatuses.forEach(classStatus -> LOGGER.debug("Name: {}", classStatus.getName()));
  }

  /**
   * Unit test for {@link FlagStateRetrofitService#getAllFlags()}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void flagStateList() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.FLAG_STATE.getLocation());

    Call<List<FlagStateHDto>> caller = flagStateRetrofitService.getAllFlags();
    List<FlagStateHDto> flags = caller.execute().body();
    Assert.assertNotNull(flags);
    Assert.assertFalse(flags.isEmpty());

    LOGGER.debug("Size: {}", flags.size());
    flags.forEach(flag -> LOGGER.debug(flag.getName()));
  }

  /**
   * Static id for flag.
   *
   */
  private static final Long MALAYSIA_FLAG_ID = 117L;

  /**
   * Unit test for {@link FlagStateRetrofitService#getFlagById(Long)}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void flagByIdQuery() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.FLAG_STATE_MALAYSIA.getLocation());

    Call<FlagStateHDto> caller = flagStateRetrofitService.getFlagById(MALAYSIA_FLAG_ID);
    FlagStateHDto flag = caller.execute().body();
    Assert.assertNotNull(flag);
    LOGGER.debug("Name: {}", flag.getName());
  }

  /**
   * Unit test for {@link AssetRetrofitService#assetQuery(AssetQueryHDto)}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void assetById() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.ASSET_BY_ID.getLocation());

    Call<AssetHDto> caller = assetRetrofitService.getAssetById(ASSET_ID);
    AssetHDto assets = caller.execute().body();
    Assert.assertNotNull(assets);
    LOGGER.debug("Name: {}", assets.getId());
  }

  /**
   * Unit test for {@link IhsRetrofitService#getPrincipalDimensionsByImoNumber(ImoNumber)}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void principalDimensionsByImoNumber() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.IHS_PRINCIPAL_DIMENSIONS.getLocation());

    Call<IhsPrincipalDimensionsDto> caller = ihsRetrofitService.getPrincipalDimensionsByImoNumber("1000021");
    IhsPrincipalDimensionsDto principalDto = caller.execute().body();
    Assert.assertNotNull(principalDto);
    LOGGER.debug("Id: {}", principalDto.getId());
  }

  /**
   * Unit test for {@link IhsRetrofitService#getEquipmentDetailsByImoNumber(ImoNumber)}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void equipmentDetailsByImoNumber() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.IHS_EQUIPMENT_DETAILS.getLocation());

    Call<IhsEquipmentDetailsDto> caller = ihsRetrofitService.getEquipmentDetailsByImoNumber("1000021");
    IhsEquipmentDetailsDto equipmentDto = caller.execute().body();
    Assert.assertNotNull(equipmentDto);
    LOGGER.debug("Id: {}", equipmentDto.getId());
  }

  /**
   * Unit test for {@link IhsRetrofitService#getPersonnelContactByImoNumber(ImoNumber)}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void personnelAddressByImoNumber() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.IHS_PERSONNEL_ADDRESS.getLocation());

    Call<IhsPersonnelContactDto> caller = ihsRetrofitService.getPersonnelContactByImoNumber("1000021");
    IhsPersonnelContactDto ihsAssetDetailsDtoList = caller.execute().body();
    Assert.assertNotNull(ihsAssetDetailsDtoList);
    LOGGER.debug("Id: {}", ihsAssetDetailsDtoList.getId());
  }

  /**
   * Unit test for {@link IhsRetrofitService#getCompanyContactByImoNumber(ImoNumber)}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void companyContactByImoNumber() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.IHS_COMPANY_ADDRESS.getLocation());

    Call<IhsCompanyContactDto> caller = ihsRetrofitService.getCompanyContactByImoNumber("1000021");
    IhsCompanyContactDto companyDto = caller.execute().body();
    Assert.assertNotNull(companyDto);
    LOGGER.debug("Id: {}", companyDto.getId());
  }

  /**
   * Unit test for {@link IhsRetrofitService#getAssetDetailsByImoNumber(ImoNumber)}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void assetDetailsByImoNumber() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.IHS_ASSET_DETAILS.getLocation());

    Call<IhsAssetDetailsDto> caller = ihsRetrofitService.getAssetDetailsByImoNumber("1000021");
    IhsAssetDetailsDto ihsAssetDetailsDto = caller.execute().body();
    Assert.assertNotNull(ihsAssetDetailsDto);
    LOGGER.debug("Id: {}", ihsAssetDetailsDto.getId());
  }

  /**
   * Unit test for {@link CustomerRetrofitService#getClientInformationByClientImo(PartyQueryDto)}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void clientInformationByClientImo() throws Exception {
    enqueueResponseFromFile(MastResponseFiles.CUSTOMER_QUERY.getLocation());

    final PartyQueryDto query = new PartyQueryDto();
    query.setImoNumber(CLIENT_IMO_NUMBER);
    Call<List<PartyHDto>> caller = customerRetrofitService.getClientInformationByClientImo(null, null, query);
    List<PartyHDto> partyDtoList = caller.execute().body();
    Assert.assertNotNull(partyDtoList);
    final PartyHDto partyDto = partyDtoList.get(0);
    Assert.assertNotNull(partyDto);
    final String clientName = partyDto.getName();
    final String partyImoNumber = partyDto.getImoNumber();
    Assert.assertNotNull(clientName);
    Assert.assertNotNull(partyImoNumber);
    LOGGER.debug("Client Name: {}, Client Imo Number: {}", clientName, partyImoNumber);
  }

  /**
   * Spring config inner class.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseRetrofitTest.BaseConfig {
    /**
     * Create asset retrofit factory.
     *
     * @param factory bean factory.
     * @return service.
     *
     */
    @Bean
    public AssetRetrofitService assetRetrofitService(final RetrofitServiceFactory factory) {
      return factory.createAssetRetrofitService();
    }

    /**
     * Create {@link AssetReferenceRetrofitService}.
     *
     * @param factory injected.
     * @return {@link AssetReferenceRetrofitService}.
     *
     */
    @Bean
    public AssetReferenceRetrofitService assetReferenceRetrofitService(final RetrofitServiceFactory factory) {
      return factory.createAssetReferenceService();
    }

    /**
     * Create {@link RetrofitServiceFactory}.
     *
     * @param factory injected.
     * @return service.
     *
     */
    @Bean
    public PortRegistryService portRegistryService(final RetrofitServiceFactory factory) {
      return factory.createPortRegistryService();
    }

    /**
     * Create {@link FlagStateRetrofitService}.
     *
     * @param factory bean.
     * @return service.
     *
     */
    @Bean
    public FlagStateRetrofitService flagStateRetrofitService(final RetrofitServiceFactory factory) {
      return factory.createFlagStateRetrofitService();
    }

    /**
     * Create {@link IhsRetrofitService}.
     *
     * @param factory bean.
     * @return service.
     *
     */
    @Bean
    public IhsRetrofitService ihsRetrofitService(final RetrofitServiceFactory factory) {
      return factory.createIhsRetrofitService();
    }


    /**
     * Create {@link CustomerRetrofitService}.
     *
     * @param factory bean.
     * @return service.
     *
     */
    @Bean
    public CustomerRetrofitService customerRetrofitService(final RetrofitServiceFactory factory) {
      return factory.createCustomerRetrofitService();
    }
  }
}
