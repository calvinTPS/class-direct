package com.baesystems.ai.lr.cd.be.domain.profiling.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspect;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspectImpl;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingStat.MethodStat;
import com.baesystems.ai.lr.cd.be.enums.ResponseOptionEnum;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspectImpl}.
 *
 * @author YWearn
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ProfilingAspectImplTest {

  /**
   * Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProfilingAspectImplTest.class);
  /**
   * Mock method name.
   */
  private static final String MOCK_METHOD_NAME = "mockMethodStat()";
  /**
   * Mock method start time.
   */
  private static final LocalDateTime MOCK_START_TIME = LocalDateTime.of(2017, 1, 31, 3, 15, 05);
  /**
   * Mock method end time.
   */
  private static final LocalDateTime MOCK_END_TIME = LocalDateTime.of(2017, 1, 31, 3, 15, 26);
  /**
   * Expected logging format of method with valid start and end date.
   */
  private static final String EXPECTED_METHOD_STAT = " 21000 (ms) === "
      + "[E] com.baesystems.ai.lr.cd.be.domain.profiling.test.ProfilingAspectImplTest.mockMethodStat()"
      + ", ST: 2017-01-31T03:15:05, ET: 2017-01-31T03:15:26";
  /**
   * Expected logging format of method with null start and end date.
   */
  private static final String EXPECTED_NULL_METHOD_STAT = "    -1 (ms) === "
      + "[E] com.baesystems.ai.lr.cd.be.domain.profiling.test.ProfilingAspectImplTest.mockMethodStat()"
      + ", ST: NA, ET: NA";

  /**
   * Constants for mock method name.
   */
  private static final String METHOD_WITHOUT_PARAM = "testPrettyPrintMethodName";
  /**
   * Constants for mock method name.
   */
  private static final String EXPECTED_FORMAT_METHOD_NAME =
      "c.b.a.l.c.b.d.p.t.ProfilingAspectImplTest" + ".testPrettyPrintMethodName()";
  /**
   * Expected formatted string of method without parameters.
   */
  private static final String METHOD_WITH_PARAM = "dummyMethodWithParams";
  /**
   * Expected formatted string of method with parameters.
   */
  private static final String EXPECTED_FORMAT_METHOD_NAME_WITH_PARAM = "c.b.a.l.c.b.d.p.t.ProfilingAspectImplTest"
      + ".dummyMethodWithParams(String, int, ProfilingAspectImplTest, List)";
  /**
   * Indent constants.
   */
  private static final String INDENT = "===";

  /**
   * Timeout.
   */
  private static final long SLEEP_MILIS = 10L;

  /**
   * Class to be tested.
   */
  @Autowired
  private ProfilingAspect profilingAspect;

  /**
   * Test logging of method with valid start and end date.
   *
   * @throws NoSuchMethodException the exception.
   * @throws SecurityException the exception.
   * @throws IllegalAccessException the exception.
   * @throws IllegalArgumentException the exception.
   * @throws InvocationTargetException the exception.
   */
  @Test
  public final void testPrettyPrintMethodStat() throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {
    ProfilingAspectImpl profilingAspectImpl = (ProfilingAspectImpl) profilingAspect;

    final StringBuilder sb = new StringBuilder();

    final Method method = ProfilingAspectImpl.class.getDeclaredMethod("prettyPrintMethodStat", StringBuilder.class,
        MethodStat.class, String.class);
    method.setAccessible(true);
    method.invoke(profilingAspectImpl, sb, mockMethodStat(), INDENT);
    Assert.assertEquals(EXPECTED_METHOD_STAT, sb.toString());
  }

  /**
   * Test logging to handle null start and end date.
   *
   * @throws NoSuchMethodException the exception.
   * @throws SecurityException the exception.
   * @throws IllegalAccessException the exception.
   * @throws IllegalArgumentException the exception.
   * @throws InvocationTargetException the exception.
   */
  @Test
  public final void testPrettyPrintMethodStatWithNullDate() throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    ProfilingAspectImpl profilingAspectImpl = (ProfilingAspectImpl) profilingAspect;

    final StringBuilder sb = new StringBuilder();
    final MethodStat mstat = mockMethodStat();
    mstat.setStartTime(null);
    mstat.setEndTime(null);

    final Method method = ProfilingAspectImpl.class.getDeclaredMethod("prettyPrintMethodStat", StringBuilder.class,
        MethodStat.class, String.class);
    method.setAccessible(true);
    method.invoke(profilingAspectImpl, sb, mstat, INDENT);
    Assert.assertEquals(EXPECTED_NULL_METHOD_STAT, sb.toString());
  }

  /**
   * Test formatted method name without parameters.
   *
   * @throws NoSuchMethodException the exception.
   * @throws SecurityException the exception.
   * @throws IllegalAccessException the exception.
   * @throws IllegalArgumentException the exception.
   * @throws InvocationTargetException the exception.
   */
  @Test
  public final void testPrettyPrintMethodName() throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {
    final ProfilingAspectImpl profilingAspectImpl = (ProfilingAspectImpl) profilingAspect;
    final MethodSignature signature = mockMethodSignature();

    final Method method = ProfilingAspectImpl.class.getDeclaredMethod("prettyPrintMethodName", MethodSignature.class);
    method.setAccessible(true);
    final String prettyMethodName = (String) method.invoke(profilingAspectImpl, signature);
    Assert.assertEquals(EXPECTED_FORMAT_METHOD_NAME, prettyMethodName);
  }

  /**
   * Test formatted method name with parameters.
   *
   * @throws NoSuchMethodException the exception.
   * @throws SecurityException the exception.
   * @throws IllegalAccessException the exception.
   * @throws IllegalArgumentException the exception.
   * @throws InvocationTargetException the exception.
   */
  @Test
  public final void testPrettyPrintMethodNameWithParams() throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    final ProfilingAspectImpl profilingAspectImpl = (ProfilingAspectImpl) profilingAspect;
    final MethodSignature signature = mockMethodSignatureWithParams();

    final Method method = ProfilingAspectImpl.class.getDeclaredMethod("prettyPrintMethodName", MethodSignature.class);
    method.setAccessible(true);
    final String prettyMethodName = (String) method.invoke(profilingAspectImpl, signature);
    Assert.assertEquals(EXPECTED_FORMAT_METHOD_NAME_WITH_PARAM, prettyMethodName);
  }

  /**
   * Test case for nested method invocation.
   *
   * @throws Throwable the exception.
   */
  @Test
  public final void testNestedMethodInvocation() throws Throwable {
    final ProfilingAspectImpl profilingAspectImpl = (ProfilingAspectImpl) profilingAspect;
    final ProceedingJoinPoint joinPoint = mockProceedingJointPoint();
    final MethodStat testMethodStat = new MethodStat();
    testMethodStat.setMethodName(MOCK_METHOD_NAME);
    testMethodStat.setStartTime(MOCK_START_TIME);
    testMethodStat.setEndTime(MOCK_END_TIME);

    Mockito.when(joinPoint.proceed(Mockito.any())).then(new Answer<Object>() {
      @Override
      public Object answer(final InvocationOnMock invocation) throws Throwable {
        Thread.sleep(SLEEP_MILIS);
        final MethodStat mStat = profilingAspectImpl.injectMethodStatistic(testMethodStat.getMethodName(),
            ResponseOptionEnum.Cached, testMethodStat.getStartTime(), testMethodStat.getEndTime());
        return mStat;
      }
    });

    final MethodStat actualMStat = (MethodStat) profilingAspectImpl.profileAround(joinPoint);

    Assert.assertEquals(testMethodStat.getMethodName(), actualMStat.getMethodName());
    Assert.assertEquals(testMethodStat.getStartTime(), actualMStat.getStartTime());
    Assert.assertEquals(testMethodStat.getEndTime(), testMethodStat.getEndTime());
    Assert.assertEquals(testMethodStat.toString(), testMethodStat.toString());
  }

  /**
   * Empty method to pass into unit test.
   *
   * @param dummyStr the string object.
   * @param intVal the integer value.
   * @param objType the ProfilingAspectImplTest object.
   * @param strList the string list object.
   */
  public final void dummyMethodWithParams(final String dummyStr, final int intVal,
      final ProfilingAspectImplTest objType, final List<String> strList) {

  }

  /**
   * Return the mock ProceedingJointPoint.
   *
   * @return the mock ProceedingJointPoint.
   * @throws Throwable the exception.
   */
  protected final ProceedingJoinPoint mockProceedingJointPoint() throws Throwable {
    final ProceedingJoinPoint joinPoint = Mockito.mock(ProceedingJoinPoint.class);
    final MethodSignature signature = mockMethodSignature();
    Mockito.when(joinPoint.getArgs()).thenReturn(new Object[] {});
    Mockito.when(joinPoint.getSignature()).thenReturn(signature);

    return joinPoint;
  }

  /**
   * Return mock MethodSignature with empty parameters.
   *
   * @return the mock MethodSignature.
   * @throws NoSuchMethodException the exception.
   * @throws SecurityException the exception.
   */
  protected final MethodSignature mockMethodSignature() throws NoSuchMethodException, SecurityException {
    final MethodSignature signature = Mockito.mock(MethodSignature.class);
    final Method method = this.getClass().getMethod(METHOD_WITHOUT_PARAM);
    Mockito.when(signature.getDeclaringTypeName()).thenReturn(this.getClass().getName());
    Mockito.when(signature.getMethod()).thenReturn(method);
    return signature;
  }

  /**
   * Return mock MethodSignature with parameters.
   *
   * @return the mock MethodSignature.
   * @throws NoSuchMethodException the exception.
   * @throws SecurityException the exception.
   */
  protected final MethodSignature mockMethodSignatureWithParams() throws NoSuchMethodException, SecurityException {
    final MethodSignature signature = Mockito.mock(MethodSignature.class);
    final Method method = this.getClass().getMethod(METHOD_WITH_PARAM, String.class, int.class,
        ProfilingAspectImplTest.class, List.class);
    Mockito.when(signature.getDeclaringTypeName()).thenReturn(this.getClass().getName());
    Mockito.when(signature.getMethod()).thenReturn(method);
    return signature;
  }

  /**
   * Return mock MethodStat object.
   *
   * @return the mock MethodStat.
   */
  protected final MethodStat mockMethodStat() {
    final MethodStat mock = new MethodStat();
    mock.setMethodName(this.getClass().getName() + "." + MOCK_METHOD_NAME);
    mock.setResponseOpt(ResponseOptionEnum.External);
    mock.setStartTime(MOCK_START_TIME);
    mock.setEndTime(MOCK_END_TIME);
    return mock;
  }

  /**
   * Internal config.
   */
  @Configuration
  public static class Config {

    /**
     * Inject ProfilingAspect.
     *
     * @return the profilingAspect.
     */
    @Bean
    public ProfilingAspect profilingAspect() {
      final ProfilingAspect profilingAspect = new ProfilingAspectImpl();
      ((ProfilingAspectImpl) profilingAspect).setExcludedMethods(Collections.emptyList());
      return profilingAspect;
    }

  }
}
