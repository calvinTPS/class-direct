package com.baesystems.ai.lr.cd.be.hibernate.test;

import static com.baesystems.ai.lr.cd.be.hibernate.test.UserId.ALICE;
import static com.baesystems.ai.lr.cd.be.hibernate.test.UserId.BOB;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.Favourites;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;

/**
 * Test class to test Database.
 *
 * @author VMandalapu
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao-context.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class FavouritesRepositoryImplTest {

  /**
   * User id.
   */
  private static final String USER_ID = "101";

  /**
   * Constant Asset 1.
   */
  private static final int TWO_RECORD = 2;

  /**
   * Constant Asset 1.
   */
  private static final String ASSET_1 = "IHS1";

  /**
   * Constant Asset 3.
   */
  private static final String ASSET_3 = "LRV3";

  /**
   * Constant asset 7.
   */
  private static final Long ASSET_7 = 7L;

  /**
   * Invalid user id.
   */
  private static final String INVALID_USERID = "1234567890";

  /**
   * FavouritesRepository object.
   */
  @Autowired(required = false)
  private FavouritesRepository favouritesRepository;
  /**
   * UserProfileDao object.
   */
  @Autowired(required = false)
  private UserProfileDao userProfileDao;

  /**
   * Test object.
   */
  @Test
  public final void testFavouritesRepository() {
    Assert.assertNotNull(favouritesRepository);
  }

  /**
   * Test userProfiles.
   *
   * @throws RecordNotFoundException exception.
   */
  @Test
  public final void testUserProfiles() throws RecordNotFoundException {
    UserProfiles userProfiles = userProfileDao.getUser(BOB.getId());
    Assert.assertNotNull(userProfiles);
    Assert.assertNotNull(userProfiles.getUserId());
    Assert.assertNotNull(userProfiles.getClientCode());
    Assert.assertNotNull(userProfiles.getFlagCode());
    Assert.assertNotNull(userProfiles.getShipBuilderCode());
  }

  /**
   * No result found test on {@link FavouritesRepository#getUserProfiles(String)}.
   *
   * @throws RecordNotFoundException exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void userProfilesNoResultFound() throws RecordNotFoundException {
    UserProfiles userProfiles = userProfileDao.getUser(INVALID_USERID);
    Assert.assertNull(userProfiles);
  }

  /**
   * Test userFavourites.
   *
   * @throws RecordNotFoundException exception.
   */
  @Test
  public final void testUserFavourites() throws RecordNotFoundException {
    List<Favourites> favourites = favouritesRepository.getUserFavourites(ALICE.getId());
    Assert.assertNotNull(favourites);
    Assert.assertNotNull(favourites.get(0).getAssetId());
    Assert.assertNotNull(favourites.get(0).getUser().getUserId());
    Assert.assertEquals(ALICE.getId(), favourites.get(0).getUser().getUserId());
  }

  /**
   * Test verifyFavourites.
   */
  @Test
  public final void testVerifyFavourite() {
    Favourites favourite = favouritesRepository.verifyFavourite(BOB.getId(), ASSET_1);
    Assert.assertNotNull(favourite);
    Assert.assertNotNull(favourite.getAssetId());
    Assert.assertNotNull(favourite.getUser().getUserId());
    Assert.assertEquals(BOB.getId(), favourite.getUser().getUserId());
  }

  /**
   * Test createFavourite.
   *
   * @throws RecordNotFoundException exception.
   */
  @Test
  public final void testCreateFavourite() throws RecordNotFoundException {
    favouritesRepository.createUserFavourite(ALICE.getId(), ASSET_3);
  }

  /**
   * Test Delete User Favorite Asset.
   */
  @Test
  public final void testDeleteFavouriteSuccess() {
    favouritesRepository.deleteUserFavourite(ALICE.getId(), ASSET_3);
  }

  /**
   * Test Delete All User Favorite Assets.
   *
   * @throws RecordNotFoundException RecordNotFoundException.
   */
  @Test
  public final void testDeleteAllUserFavouriteSuccess() throws RecordNotFoundException {
    favouritesRepository.createUserFavourite(USER_ID, ASSET_1);
    favouritesRepository.createUserFavourite(USER_ID, ASSET_3);
    List<Favourites> favorites = favouritesRepository.getUserFavourites(USER_ID);
    Assert.assertFalse(favorites.isEmpty());
    Assert.assertEquals(favorites.size(), TWO_RECORD);
    favouritesRepository.deleteAllUserFavourites(USER_ID);
    List<Favourites> updatedFavorites = favouritesRepository.getUserFavourites(USER_ID);
    Assert.assertTrue(updatedFavorites.isEmpty());
  }

  /**
   * Positive test on {@link FavouritesRepository#isUserFavourite(String, Long)}.
   */
  @Test
  public final void successTestOnIsUserFavourite() {
    Boolean favourite = favouritesRepository.isUserFavourite(ALICE.getId(), ASSET_7);
    Assert.assertNotNull(favourite);
    Assert.assertTrue(favourite);
  }

  /**
   * User favourite not found on {@link FavouritesRepository#isUserFavourite(String, Long)}.
   */
  @Test
  public final void successTestOnIsUserFavouriteNotFound() {
    Boolean favourite = favouritesRepository.isUserFavourite(ALICE.getId(), AssetCodeUtil.getId(ASSET_1));
    Assert.assertNotNull(favourite);
    Assert.assertFalse(favourite);
  }

  /**
   * Tests success scenario of
   * {@link FavouritesRepository#createUserFavouriteList(String, FavouritesDto)}.
   *
   * @throws ClassDirectException if execution fails.
   */
  @Test
  public final void testCreateFavouriteList() throws ClassDirectException {
    FavouritesDto favouriteDto = new FavouritesDto();
    List<String> insertAssets = new ArrayList();
    insertAssets.add(ASSET_3);
    insertAssets.add("LRV9");
    insertAssets.add("IHS1000019");
    List<String> removeAssets = new ArrayList();
    removeAssets.add(ASSET_3);
    favouriteDto.setAdd(insertAssets);
    favouriteDto.setRemove(removeAssets);

    favouritesRepository.updateUserFavouriteList(ALICE.getId(), favouriteDto);

    List<Favourites> favorites = favouritesRepository.getUserFavourites(ALICE.getId());

    List<String> userFavouriteAssetCodes = favorites.stream().map(fav -> {
      return fav.getSource().concat(fav.getAssetId().toString());
    }).collect(Collectors.toList());

    Assert.assertTrue(userFavouriteAssetCodes.contains("LRV9"));
    Assert.assertTrue(userFavouriteAssetCodes.contains("IHS1000019"));
    Assert.assertFalse(userFavouriteAssetCodes.contains(ASSET_3));

  }


  /**
   * Tests success scenario of
   * {@link FavouritesRepository#createUserFavouriteList(String, FavouritesDto)}.
   *
   * @throws ClassDirectException if execution fails.
   */
  @Test
  public final void testCreateFavouriteListForEmptyAddList() throws ClassDirectException {
    FavouritesDto favouriteDto = new FavouritesDto();
    List<String> insertAssets = new ArrayList();
    List<String> removeAssets = new ArrayList();
    removeAssets.add("LRV9");
    favouriteDto.setAdd(insertAssets);
    favouriteDto.setRemove(removeAssets);

    favouritesRepository.updateUserFavouriteList(ALICE.getId(), favouriteDto);

    Assert.assertTrue(favouritesRepository.verifyFavourite(ALICE.getId(), "LRV9") == null);
  }

  /**
   * Tests success scenario of
   * {@link FavouritesRepository#createUserFavouriteList(String, FavouritesDto)} for null input
   * list.
   *
   * @throws ClassDirectException if execution fails.
   */
  @Test
  public final void testCreateFavouriteListForNullList() throws ClassDirectException {
    FavouritesDto favouriteDto = new FavouritesDto();
    favouriteDto.setAdd(null);
    favouriteDto.setRemove(null);

    favouritesRepository.updateUserFavouriteList(ALICE.getId(), favouriteDto);
  }

  /**
   * Tests success scenario of
   * {@link FavouritesRepository#createUserFavouriteList(String, FavouritesDto)} for empty input
   * list.
   *
   * @throws ClassDirectException if execution fails.
   */
  @Test
  public final void testCreateFavouriteListForEmptyList() throws ClassDirectException {
    FavouritesDto favouriteDto = new FavouritesDto();
    List<String> insertAssets = new ArrayList();
    List<String> removeAssets = new ArrayList();
    favouriteDto.setAdd(insertAssets);
    favouriteDto.setRemove(removeAssets);

    favouritesRepository.updateUserFavouriteList(ALICE.getId(), favouriteDto);
  }


  /**
   * Tests success scenario of
   * {@link FavouritesRepository#createUserFavouriteList(String, FavouritesDto)}.
   *
   * @throws ClassDirectException if execution fails.
   */
  @Test
  public final void testCreateFavouriteListForEmptyRemoveList() throws ClassDirectException {
    FavouritesDto favouriteDto = new FavouritesDto();
    List<String> insertAssets = new ArrayList();
    List<String> removeAssets = new ArrayList();
    insertAssets.add("LRV9");
    favouriteDto.setAdd(insertAssets);
    favouriteDto.setRemove(removeAssets);

    favouritesRepository.updateUserFavouriteList(ALICE.getId(), favouriteDto);

    Assert.assertTrue(favouritesRepository.verifyFavourite(ALICE.getId(), "LRV9") != null);
  }


  /**
   * Tests success scenario of
   * {@link FavouritesRepository#createUserFavouriteList(String, FavouritesDto)}.
   *
   * @throws ClassDirectException if execution fails.
   */
  @Test
  public final void testCreateFavouriteListForEmptyUserFavouriteList() throws ClassDirectException {
    FavouritesDto favouriteDto = new FavouritesDto();
    List<String> insertAssets = new ArrayList();
    insertAssets.add("LRV110023");
    insertAssets.add("LRV110079");
    insertAssets.add("IHS1000019");
    List<String> removeAssets = new ArrayList();
    removeAssets.add("IHS1000019");
    favouriteDto.setAdd(insertAssets);
    favouriteDto.setRemove(removeAssets);

    favouritesRepository.updateUserFavouriteList("122", favouriteDto);

    List<Favourites> favorites = favouritesRepository.getUserFavourites("122");

    List<String> userFavouriteAssetCodes = favorites.stream().map(fav -> {
      return fav.getSource().concat(fav.getAssetId().toString());
    }).collect(Collectors.toList());

    Assert.assertTrue(userFavouriteAssetCodes.contains("LRV110023"));
    Assert.assertTrue(userFavouriteAssetCodes.contains("LRV110079"));
    Assert.assertFalse(userFavouriteAssetCodes.contains("IHS1000019"));

  }

}
