package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.test;

import static org.mockito.Mockito.spy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.LRIDRetrofitServiceFactory;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.UserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspect;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspectImpl;
import com.baesystems.ai.lr.cd.be.domain.retrofit.LRIDRetrofitFactory;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.ProfilingInterceptor;

import okhttp3.Interceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.LRIDInterceptor}.
 *
 * @author YWearn
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ProfilingInterceptorTest {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProfilingInterceptorTest.class);
  /**
   * Ten seconds constant.
   */
  private static final long TEN_SECONDS = 10L;

  /**
   * Injected server.
   */
  @Autowired
  private MockWebServer server;

  /**
   * Injected retrofit service.
   */
  @Autowired
  private UserRetrofitService userRetrofitService;

  /**
   * Ensure ProfilingAspect is trigger without impacting existing request post.
   *
   * @throws Exception exception if any.
   */
  @Test
  public final void shouldInterceptAndTriggerProfilingAspect() throws Exception {
    // Enqueue dummy response.
    server.enqueue(new MockResponse().setHeader("Content-Type", "application/json; charset=utf-8")
        .setResponseCode(HttpStatus.SC_OK).setBody("[]"));

    final Call<List<LRIDUserDto>> caller = userRetrofitService.getAzureUsers("");
    caller.execute().body();

    // As long as it manager to trigger profiling without impacting existing function.
    final RecordedRequest request = server.takeRequest(TEN_SECONDS, TimeUnit.SECONDS);
    Assert.assertNotNull(request);
  }

  /**
   * Internal config.
   */
  @Configuration
  public static class Config {
    /**
     * Create new mock web server.
     *
     * @return mock web server.
     */
    @Bean
    public MockWebServer mockWebServer() {
      return new MockWebServer();
    }

    /**
     * Create LRIDRetrofitFactory factory.
     *
     * @param mockWebServer injected mock web server.
     * @return LRIDRetrofitFactory factory.
     */
    @Bean
    public LRIDRetrofitFactory lridRetrofitFactory(final MockWebServer mockWebServer) {
      final LRIDRetrofitFactory factory = new LRIDRetrofitFactory();
      factory.setLridBaseUrl(mockWebServer.url("dummydomain/graph/").toString());
      List<Class<? extends Interceptor>> interceptorList = new ArrayList<>(1);
      interceptorList.add(ProfilingInterceptor.class);
      factory.setInterceptorList(interceptorList);
      return factory;
    }

    /**
     * Create retrofit.
     *
     * @param lridRetrofitFactory injected factory.
     * @return retrofit.
     *
     */
    @Bean
    public Retrofit lridRetrofit(final LRIDRetrofitFactory lridRetrofitFactory) {
      return lridRetrofitFactory.createLRIDRetrofit();
    }

    /**
     * Create retrofit service factory.
     *
     * @return retrofit service factory.
     */
    @Bean
    public LRIDRetrofitServiceFactory lridRetrofitServiceFactory() {
      return new LRIDRetrofitServiceFactory();
    }

    /**
     * Create service.
     *
     * @param factory injected.
     * @return new service.
     */
    @Bean
    public UserRetrofitService userRetrofitService(final LRIDRetrofitServiceFactory factory) {
      return factory.createUserRetrofitService();
    }

    /**
     * Return the profiling aspect.
     *
     * @return the profiling aspect.
     */
    @Bean
    public ProfilingAspect profilingAspect() {
      ProfilingAspect profilingAspect = new ProfilingAspectImpl();
      return profilingAspect;
    }

    /**
     * Return the profiling interceptor.
     *
     * @param mockWebServer the mock webserver.
     * @return the profiling interceptor.
     */
    @Bean
    public ProfilingInterceptor interceptor(final MockWebServer mockWebServer) {
      final ProfilingInterceptor interceptor = spy(new ProfilingInterceptor());
      return interceptor;
    }

  }

}
