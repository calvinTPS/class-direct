package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.test;

import static com.baesystems.ai.lr.cd.be.domain.test.MastResponseFiles.ASSET_TYPE_LIGHT;
import static com.baesystems.ai.lr.cd.be.domain.test.MastResponseFiles.PORT_OF_REGISTRY;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.baesystems.ai.lr.cd.be.domain.dao.RetrofitServiceFactory;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.PortRegistryService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.PaginatedResponseInterceptor;
import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.ReferenceDataResponseInterceptor;
import com.baesystems.ai.lr.cd.be.domain.test.BaseRetrofitTest;
import com.baesystems.ai.lr.cd.be.exception.MastAPIException;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import retrofit2.Call;

/**
 * Unit test for
 * {@link com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.ReferenceDataResponseInterceptor}.
 *
 * @author msidek
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ReferenceDataInterceptorTest.Config.class)
public class ReferenceDataInterceptorTest extends BaseRetrofitTest {
  /**
   * Logger.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ReferenceDataInterceptorTest.class);

  /**
   * Retrofit service.
   *
   */
  @Autowired
  private AssetReferenceRetrofitService retrofitService;

  /**
   * Registry service.
   *
   */
  @Autowired
  private PortRegistryService registryService;
  /**
   * Injected {@link PaginatedResponseInterceptor}.
   *
   */
  @Autowired
  private PaginatedResponseInterceptor paginatedResponseInterceptor;

  /**
   *
   */
  @Before
  public void setup() {
    List<String> paths = new ArrayList<>();
    paths.add("asset/query");
    ReflectionTestUtils.setField(paginatedResponseInterceptor, "ignoredPaths", paths);
  }

  /**
   * Method {@link ReferenceDataResponseInterceptor#intercept(okhttp3.Interceptor.Chain)}
   * successfully intercepted response.
   *
   * @throws Exception when test failed.
   *
   */
  @Test
  public final void successfullyIntercept() throws Exception {
    LOGGER.debug("Test for {}", ReferenceDataResponseInterceptor.class.getSimpleName());
    enqueueResponseFromFile(ASSET_TYPE_LIGHT.getLocation());

    Call<List<AssetTypeHDto>> call = retrofitService.getAssetTypeDtos();
    List<AssetTypeHDto> assetTypes = call.execute().body();
    Assert.assertNotEquals(0, assetTypes.size());
    assetTypes.forEach(assetType -> LOGGER.debug(assetType.toString()));
  }

  /**
   * Method {@link ReferenceDataResponseInterceptor#intercept(okhttp3.Interceptor.Chain)} will
   * ignore the non-reference response.
   *
   * @throws Exception when test failed.
   *
   */
  @Test
  public final void ignoreInterceptOnNonReferenceResponse() throws Exception {
    enqueueResponseFromFile(PORT_OF_REGISTRY.getLocation());

    Call<List<PortOfRegistryDto>> call = registryService.getPortOfRegistryDtos();
    List<PortOfRegistryDto> ports = call.execute().body();
    ports.stream().forEach(port -> LOGGER.debug(port.getName()));
  }

  /**
   * Method errorcodeinterceptor will filters unsuccessful request.
   *
   * @throws Exception when error.
   *
   */
  @Test(expected = MastAPIException.class)
  public final void filterErrorCode() throws Exception {
    enqueueServerError();

    Call<List<PortOfRegistryDto>> call = registryService.getPortOfRegistryDtos();
    List<PortOfRegistryDto> ports = call.execute().body();
    Assert.assertNull(ports);
  }

  /**
   * Spring config inner class.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseRetrofitTest.BaseConfig {

    /**
     * Create {@link AssetReferenceRetrofitService}.
     *
     * @param factory injected.
     * @return {@link AssetReferenceRetrofitService}.
     *
     */
    @Bean
    public AssetReferenceRetrofitService assetReferenceRetrofitService(final RetrofitServiceFactory factory) {
      return factory.createAssetReferenceService();
    }

    /**
     * Create {@link RetrofitServiceFactory}.
     *
     * @param factory injected.
     * @return service.
     *
     */
    @Bean
    public PortRegistryService portRegistryService(final RetrofitServiceFactory factory) {
      return factory.createPortRegistryService();
    }
  }
}
