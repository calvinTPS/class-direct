package com.baesystems.ai.lr.cd.be.domain.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.be.domain.retrofit.RetrofitFactory;
import java.util.Map;
import okhttp3.Interceptor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ActiveProfiles;
import retrofit2.Retrofit;

/**
 * Provides unit tests for {@link RetrofitFactory}.
 *
 *
 * @author msidek
 * @author YWearn
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@ActiveProfiles("httpcache-disabled")
public class RetrofitFactoryTest {
  /**
   * The logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(RetrofitFactoryTest.class);

  /**
   * The Spring ApplicationContext.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The Retrofit service to test.
   */
  @Autowired(required = false)
  private Retrofit retrofit;

  /**
   * Tests the {@link RetrofitFactory} bean creation by Spring context.
   * <p>
   * The bean creation should not fail even if the httpcache interceptor
   * is disable by httpcache-disabled profile.
   * </p>
   */
  @Test
  public final void factoryBeanInjection() {
    Assert.assertNotNull(context.getBean(RetrofitFactory.class));
    Assert.assertNotNull(context.getBean("retrofit"));
    Assert.assertNotNull(retrofit);
    LOGGER.debug("Url: {}", retrofit.baseUrl().toString());

    final Map<String, Interceptor> interceptors = context.getBeansOfType(Interceptor.class);
    Assert.assertNotNull(interceptors);
    Assert.assertNotEquals(0, interceptors.size());
  }

  /**
   * Defines the Spring Context for unit test.
   */
  @Configuration
  @ImportResource("classpath:test-context.xml")
  public static class Config {

    /**
     * Returns the mocked {@link SecurityDao} implementation for unit test.
     *
     * @return mock the mocked {@link SecurityDao} implementation.
     */
    @Bean
    public SecurityDAO securityDAO() {
      return Mockito.mock(SecurityDAO.class);
    }
  }
}
