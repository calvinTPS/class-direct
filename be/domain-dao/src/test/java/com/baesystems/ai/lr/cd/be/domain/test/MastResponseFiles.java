package com.baesystems.ai.lr.cd.be.domain.test;

/**
 * Provides enumeration for test files.
 *
 * @author msidek
 *
 */
public enum MastResponseFiles {
  ASSET_TYPE_LIGHT("mast/reference/asset/asset_types_light.json"), PORT_OF_REGISTRY(
      "mast/reference/portofregistry/portofregistry.json"), CLASS_STATUS(
          "mast/reference/asset/class_status.json"), FLAG_STATE(
              "mast/flags/flag_states.json"), FLAG_STATE_LIGHT("mast/flags/flag_state_light.json"), FLAG_STATE_MALAYSIA(
                  "mast/flags/flag_state_malaysia.json"), ASSET_BY_ID(
                      "mast/reference/asset/asset_id.json"), IHS_COMPANY_ADDRESS(
                          "mast/ihs/ihs_company_address.json"), IHS_PERSONNEL_ADDRESS(
                              "mast/ihs/ihs_personnelAddress.json"), IHS_PRINCIPAL_DIMENSIONS(
                                  "mast/ihs/ihs_principal_dimensions.json"), IHS_EQUIPMENT_DETAILS(
                                      "mast/ihs/ihs_equipment_details.json"), IHS_ASSET_DETAILS(
                                          "mast/ihs/ihs_asset_details.json"), CUSTOMER_QUERY(
                                              "mast/customer/customer_query.json");

  private String location;

  private MastResponseFiles(final String file) {
    this.location = file;
  }

  public final String getLocation() {
    return location;
  }
}
