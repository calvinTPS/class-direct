package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import com.baesystems.ai.lr.cd.be.domain.repositories.FlagsAssociations;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

import java.util.List;

/**
 * Provides data access service to add, remove and get flags associations from database.
 *
 * @author fwijaya on 19/4/2017.
 */
public interface FlagsAssociationsDao {
  /**
   * Removes flags associations.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @return The number of entities deleted.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  int removeAssociation(final String flagCode, final String secondaryFlagCode) throws ClassDirectException;

  /**
   * Adds flags associations.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  void addAssociation(final String flagCode, final String secondaryFlagCode) throws ClassDirectException;

  /**
   * Returns flag associations for a flag code.
   *
   * @param flagCode Flag code.
   * @return Flag associations for a flag code.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  List<FlagsAssociations> getFlagAssocisationsForFlagCode(final String flagCode) throws ClassDirectException;

  /**
   * Returns flag associations for a secondary flag code.
   *
   * @param secondaryFlagCode Secondary flag code.
   * @return Flag associations for a secondary flag code.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  List<FlagsAssociations> getFlagAssocisationsForSecondaryFlagCode(final String secondaryFlagCode)
    throws ClassDirectException;

  /**
   * Returns the associated flag code for a given flag code.
   *
   * @param flagsAssociations The entity.
   * @param theFlagCode The flag code.
   * @return The associated flag code.
   */
  String getAssociatedFlag(final FlagsAssociations flagsAssociations, final String theFlagCode);
}
