package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

/**
 * Interceptor to append authorization header.
 *
 * @author Faizal Sidek
 */
public class LRIDInterceptor extends AzureInterceptorBase {
  /**
   * sitekit access token prefix.
   */
  @Value("${sitekit.oauth2.token.prefix}")
  private String prefix;

  /**
   * empty delimiter.
   */
  private static final String EMPTY_DELIMITTER = " ";

  @Override
  public final Response intercept(final Chain chain) throws IOException {
    final Request request = chain.request();
    refreshToken(null);
    final HttpUrl url = request.url().newBuilder().build();
    final Request.Builder builder = request.newBuilder().headers(request.headers())
      .addHeader("Authorization", prefix + EMPTY_DELIMITTER + getToken().getAccessToken())
      .method(request.method(), request.body()).url(url);
    return processResponse(chain.proceed(builder.build()));
  }
}
