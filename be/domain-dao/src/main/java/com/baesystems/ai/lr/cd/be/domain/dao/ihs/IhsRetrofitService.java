package com.baesystems.ai.lr.cd.be.domain.dao.ihs;


import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsCompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.dto.query.IhsSisterQueryDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Retrofit Service for IHS.
 */
public interface IhsRetrofitService {

  /**
   * Get IhsPrincipalDimensionsDto.
   *
   * @param imoNumber imoNumber from MAST.
   * @return Call<IhsPrincipalDimensionsDto> return dataType.
   */
  @GET("ihs/dimensions/{imoNumber}")
  Call<IhsPrincipalDimensionsDto> getPrincipalDimensionsByImoNumber(@Path("imoNumber") String imoNumber);

  /**
   * Get IhsEquipmentDetailsDto.
   *
   * @param imoNumber imoNumber from MAST.
   * @return Call<IhsEquipmentDetailsDto> return dataType.
   */
  @GET("ihs/equipment/{imoNumber}")
  Call<IhsEquipmentDetailsDto> getEquipmentDetailsByImoNumber(@Path("imoNumber") String imoNumber);

  /**
   * Get CompanyContactDto.
   *
   * @param imoNumber imoNumber from MAST.
   * @return Call<CompanyContactDto> return dataType.
   */
  @GET("ihs/companyAddress/{imoNumber}")
  Call<IhsCompanyContactDto> getCompanyContactByImoNumber(@Path("imoNumber") String imoNumber);

  /**
   * Get IhsPersonnelContactDto.
   *
   * @param imoNumber imoNumber from MAST.
   * @return Call<IhsPersonnelContactDto> return dataType.
   */
  @GET("ihs/personnelAddress/{imoNumber}")
  Call<IhsPersonnelContactDto> getPersonnelContactByImoNumber(@Path("imoNumber") String imoNumber);

  /**
   * Get IhsAssetDetailsDto .
   *
   * @param imoNumber imoNumber from MAST.
   * @return Call<IhsAssetDetailsDto > return dataType.
   */
  @GET("ihs/{imoNumber}")
  Call<IhsAssetDetailsDto> getAssetDetailsByImoNumber(@Path("imoNumber") String imoNumber);

  /**
   * Query for register book sister assets with sorting, order and pagination.
   *
   * @param dto query DTO.
   * @param page page number
   * @param size page size
   * @param sort sort by path.
   * @param order ASC or DESC.
   * @return list of multiAsset DTO.
   */
  @POST("ihs/query")
  Call<MultiAssetPageResourceDto> registerBookSistersIhsQuery(@Body IhsSisterQueryDto dto, @Query("page") Integer page,
      @Query("size") Integer size, @Query("sort") String sort, @Query("order") String order);

}
