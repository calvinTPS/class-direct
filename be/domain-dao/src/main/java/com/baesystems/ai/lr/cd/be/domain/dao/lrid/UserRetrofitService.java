package com.baesystems.ai.lr.cd.be.domain.dao.lrid;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserStatusDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

/**
 * @author VKolagutla
 *
 */
public interface UserRetrofitService {
  /**
   * @param filter filter.
   * @return list of user dto.
   */
  @GET("user")
  Call<List<LRIDUserDto>> getAzureUsers(@Query("filter") String filter);

  /**
   * Azure API to create new user in Azure.
   *
   * @param azureUser Azure user details.
   * @return Azure user detail with objectId and principleName populated from Azure
   */
  @POST("user")
  Call<LRIDUserDto> createAzureUser(@Body LRIDUserDto azureUser);

  /**
   * Azure API to approve user.
   *
   * @param userStatus Azure user status.
   * @param objectId objectId azure user.
   * @return status value.
   */
  @POST("approve/{objectId}")
  Call<StatusDto> approveAzureUser(@Body LRIDUserStatusDto userStatus, @Path("objectId") String objectId);
}
