package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Retrofit service to fetch reference data version.
 *
 * @author MSidek
 *
 */
public interface ReferenceDataVersionRetrofitService {

  /**
   * Get reference data version.
   *
   * @return caller.
   *
   */
  @GET("reference-data-version")
  Call<ReferenceDataVersionDto> getReferenceDataVersion();
}
