package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.certificate.ProductCatalogueHDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by PFauchon on 24/2/2017.
 */
public interface ProductReferenceRetrofitService {

  /**
   * Gets all the product catalogues.
   *
   * @return the product catalogues.
   */
  @GET("reference-data/product/product-catalogues")
  Call<List<ProductCatalogueHDto>> getProductCataloguesDto();

  /**
   * Gets all the product groups.
   * @return the list of product groups.
   */
  @GET("reference-data/product/product-groups")
  Call<List<ProductGroupDto>> getProductGroupsDto();

}
