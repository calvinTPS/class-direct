
package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import okhttp3.OkHttpClient.Builder;

/**
 * Callback to invoke custom change on retrofit builder after link the interceptor to retrofit
 * builder.
 *
 * @author YWearn
 *
 */
public interface AddInterceptorCallback {

  /**
   * @param builder builder.
   */
  void initBuilder(final Builder builder);

}
