package com.baesystems.ai.lr.cd.be.domain.profiling;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.cd.be.enums.ResponseOptionEnum;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

/**
 * Profiling statistic object.
 *
 * @author YWearn
 */
public class ProfilingStat {

  /**
   * First triggering point method name.
   */
  private String parentMethod;
  /**
   * Thread identifier.
   */
  private String threadIdentifier;
  /**
   * List of method statistics.
   */
  private List<MethodStat> methodStatList = new ArrayList<>();

  /**
   * @return the thread identifier.
   */
  public final String getThreadIdentifier() {
    return threadIdentifier;
  }

  /**
   * @param threadIdentifierStr the thread identifier.
   */
  public final void setThreadIdentifier(final String threadIdentifierStr) {
    this.threadIdentifier = threadIdentifierStr;
  }

  /**
   * @return the parent method name.
   */
  public final String getParentMethod() {
    return parentMethod;
  }

  /**
   * @param parentMethodStr the parent method name.
   */
  public final void setParentMethod(final String parentMethodStr) {
    this.parentMethod = parentMethodStr;
  }

  /**
   * @return the method statistic list.
   */
  public final List<MethodStat> getMethodStatList() {
    return methodStatList;
  }

  /**
   * @param methodStatListObj the method statistic list.
   */
  public final void setMethodStatList(final List<MethodStat> methodStatListObj) {
    this.methodStatList = methodStatListObj;
  }

  /**
   * Method statistic class.
   *
   * @author YWearn
   */
  public static class MethodStat {
    /**
     * Method name.
     */
    private String methodName;

    /**
     * enum.
     */
    private ResponseOptionEnum responseOpt;

    /**
     * Method start time.
     */
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startTime;
    /**
     * Method end time.
     */
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime endTime;

    /**
     * @return the method name.
     */
    public final String getMethodName() {
      return methodName;
    }

    /**
     * @param methodNameStr the method name.
     */
    public final void setMethodName(final String methodNameStr) {
      this.methodName = methodNameStr;
    }

    /**
     * @return the start time.
     */
    public final LocalDateTime getStartTime() {
      return startTime;
    }

    /**
     * @param startTimeObj the start time.
     */
    public final void setStartTime(final LocalDateTime startTimeObj) {
      this.startTime = startTimeObj;
    }

    /**
     * @return the end time.
     */
    public final LocalDateTime getEndTime() {
      return endTime;
    }

    /**
     * @param endTimeObj the end time.
     */
    public final void setEndTime(final LocalDateTime endTimeObj) {
      this.endTime = endTimeObj;
    }


    /**
     * @return the responseOpt
     */
    public final ResponseOptionEnum getResponseOpt() {
      return responseOpt;
    }

    /**
     * @param responseOptObj responseOptObj to set.
     */
    public final void setResponseOpt(final ResponseOptionEnum responseOptObj) {
      this.responseOpt = responseOptObj;
    }

    /**
     * To string method.
     */
    @Override
    public final String toString() {
      return "MethodStat [methodName=" + methodName + ", responseOpt=" + responseOpt + ",startTime=" + startTime
          + ", endTime=" + endTime + "]";
    }
  }
}

