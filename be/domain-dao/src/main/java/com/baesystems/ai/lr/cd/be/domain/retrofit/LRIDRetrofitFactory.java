package com.baesystems.ai.lr.cd.be.domain.retrofit;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Provides factory method to create Retrofit object
 * connect to LR SSO solution.
 *
 * @author VKolagutla
 * @author YWearn
 */
public class LRIDRetrofitFactory extends BaseRetrofitFactory {

  /**
   * The logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(LRIDRetrofitFactory.class);

  /**
   * The default Retrofit client connection timeout which is {@value #DEFAULT_TIMEOUT_IN_SECONDS}.
   */
  private static final Integer DEFAULT_TIMEOUT_IN_SECONDS = 300;

  /**
   * The read/write and general timeout in seconds for created Retrofit object.
   * Defaulted to {@value #DEFAULT_TIMEOUT_IN_SECONDS} if not specified in Spring Context.
   */
  private Integer timeout = DEFAULT_TIMEOUT_IN_SECONDS;

  /**
   * The LR SSO REST API base URL injected from Spring context.
   */
  private String lridBaseUrl;

  /**
   * The flag to indicates if the http client retry in case of connection error.
   */
  private boolean retryOnConnectionError = true;

  /**
   * Sets the Retrofit service default timeout in second(s).
   *
   * @param time the Retrofit read/write and general timeout in seconds.
   */
  public final void setTimeout(final Integer time) {
    this.timeout = time;
  }

  /**
   * Provides a new Retrofit client service to LR SSO REST API service.
   *
   * @return the Retrofit client service pointing to LR SSO defined in baseUrl.
   *
   */
  public final Retrofit createLRIDRetrofit() {
    LOGGER.info("Creating retrofit factory with lrid base url {}", lridBaseUrl);
    return new Retrofit.Builder().baseUrl(lridBaseUrl)
        .addConverterFactory(JacksonConverterFactory.create(createMapper())).client(createClient()).build();
  }

  /**
   * Creates a new instance of {@link OkHttpClient}.
   *
   * @return the {@link OkHttpClient} use by Retrofit service.
   *
   */
  private OkHttpClient createClient() {
    OkHttpClient client = null;
    final Builder builder = new OkHttpClient.Builder();
    builder.connectTimeout(timeout, TimeUnit.SECONDS);
    builder.readTimeout(timeout, TimeUnit.SECONDS);
    builder.writeTimeout(timeout, TimeUnit.SECONDS);
    // Enable the retry in live as notice intermittent connection error
    // but disable during unit test using retryOnConnectionError.
    // see https://github.com/square/okhttp/issues/2394
    // builder.retryOnConnectionFailure(retryOnConnectionError);

    if (super.getContext() != null) {
      addInterceptors(builder);
      client = builder.build();
    }

    return client;
  }

  /**
   * Creates a new JSON ObjectMapper to parse REST JSON request and response.
   *
   * @return the JSON ObjectMapper to parse REST JSON request and response.
   *
   */
  private ObjectMapper createMapper() {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.setDateFormat(new ISO8601DateFormat());
    return mapper;
  }

  /**
   * Gets the baseUrl that Retrofit service connect to.
   *
   * @return the baseUrl that Retrofit service connect to.
   */
  public final String getLridBaseUrl() {
    return lridBaseUrl;
  }

  /**
   * Sets the baseUrl that Retrofit service connect to.
   *
   * @param url the baseUrl that Retrofit service connect to.
   */
  public final void setLridBaseUrl(final String url) {
    this.lridBaseUrl = url;
  }

  /**
   * Sets the indicator to instructs http client automatically retry in case of connection error.
   * @param retryOnConnectionErrorVal the indicator to instruct the http client to retry in case of connection error.
   */
  public final void setRetryOnConnectionError(final boolean retryOnConnectionErrorVal) {
    retryOnConnectionError = retryOnConnectionErrorVal;
  }

}
