package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import java.time.LocalDateTime;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.TermsConditions;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.InactiveAccountException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import java.util.Set;

/**
 * Provides the database service for accessing, adding, deleting, updating user related information
 * in local database.
 *
 * @author SBollu
 */
public interface UserProfileDao {

  /**
   * Returns all Class-Direct users if requester user is authorized to access users' information from database.
   *
   * @param requesterId This is the parameter to getUsers method.
   * @return List of userProfiles.
   * @throws RecordNotFoundException when Record not found with @param userId.
   * @throws UnauthorisedAccessException if @param userId not authorized to access.
   */
  List<UserProfiles> getUsers(String requesterId) throws RecordNotFoundException, UnauthorisedAccessException;

  /**
   * Updates edit user's information given requester user is authorized to access edit user's information from database.
   *
   * @param requesterId logged in user who requested to edit.
   * @param edituserId user whose profile need to be edited.
   * @param newProfileValuestoSet new profile values need to be updated in database.
   * @param fieldsToDelete A string of comma-delimited field names to delete.
   * @return updated UserProfile.
   * @throws ClassDirectException exception.
   *
   */
  UserProfiles updateUser(String requesterId, String edituserId, UserProfiles newProfileValuestoSet,
      String fieldsToDelete) throws ClassDirectException;

  /**
   * Returns user profile of the requester user.
   *
   * @param userId This is the first parameter to updateUser method.
   * @return existing userProfile.
   * @throws RecordNotFoundException when Record not found with @param userId.
   */
  UserProfiles getUser(String userId) throws RecordNotFoundException;

  /**
   * Returns user with given user id if the user account status is active.
   *
   * @param userId id of the user.
   * @return user profile.
   * @throws RecordNotFoundException when no record returns.
   * @throws InactiveAccountException when the account isn't active.
   */
  UserProfiles getActiveUser(String userId) throws RecordNotFoundException, InactiveAccountException;

  /**
   * Fetch all user account status.
   *
   * @return all user account status.
   * @throws ClassDirectException classDirectException if there is any error occurred.
   */
  List<UserAccountStatus> fetchAllUserAccStatus() throws ClassDirectException;

  /**
   * @param query This is the parameter to getUsers method.
   * @param page page of user records.
   * @param size size of user records.
   * @param sort user records sorting field.
   * @param order user records order.
   * @return List of userProfiles.
   * @throws ClassDirectException exception.
   */
  List<UserProfiles> getUsers(UserProfileDto query, Integer page, Integer size, String sort, String order)
      throws ClassDirectException;

  /**
   * @param requestedId requestedId.
   * @return requested user profile.
   * @throws RecordNotFoundException when no record returns.
   */
  UserProfiles getRequestedUser(final String requestedId) throws RecordNotFoundException;

  /**
   * Disables expired users.
   *
   * @return number of successful updates for disabling expired user accounts.
   */
  int disableExpiredUsers();

  /**
   * Creates Class-Direct user in database.
   *
   * @param requesterId requesterId.
   * @param newprofile profile to createUser.
   * @return value.
   * @throws RecordNotFoundException when Record not found.
   * @throws ClassDirectException when exception occurs.
   */
  StatusDto createUser(final String requesterId, final CreateNewUserDto newprofile)
      throws RecordNotFoundException, ClassDirectException;

  /**
   * Returns user with given email from database.
   *
   * @param email email.
   * @return userProfiles.
   */
  UserProfiles verifyUserByEmail(final String email);

  /**
   * Updates EOR Record in database.
   *
   * @param requesterId requesterId.
   * @param userIdtoeditEor userId to which we need to add EOR records.
   * @param newEorstoSet newEorstoSet.
   * @return Updated Eor List.
   * @throws ClassDirectException when exception happens.
   */
  StatusDto updateEORs(String requesterId, String userIdtoeditEor, final CreateNewUserDto newEorstoSet)
      throws ClassDirectException;

  /**
   * Deletes the EOR from database.
   *
   * @param eorUserId the entity's attribute of user's eor.
   * @param eorIMONumber the entity's attribute of user's eor.
   * @return Success status Message.
   * @throws ClassDirectException if eor could not found with userId amd imonumber.
   */
  StatusDto removeEOR(final String eorUserId, final String eorIMONumber) throws ClassDirectException;

  /**
   * Returns the user EOR Record in Database.
   *
   * @param userId userId.
   * @param assetId assetId.
   * @return eor Record.
   * @throws RecordNotFoundException when no found.
   */
  UserEOR getEOR(String userId, String assetId) throws RecordNotFoundException;

  /**
   * Returns latest Terms and Conditions from Database.
   *
   * @return Latest terms and conditions Record.
   * @throws ClassDirectException when exception occurs.
   */
  TermsConditions getLatestTermsConditions() throws ClassDirectException;

  /**
   * Creates Terms and Conditions for user who have accepted the Terms and Conditions. In the case of an existing
   * record, the accepted date is updated to current date.
   *
   * @param userId userId.
   * @param tcId tcId.
   * @return value.
   * @throws ClassDirectException exception.
   */
  StatusDto createAcceptedTermsandConditions(final String userId, final Long tcId) throws ClassDirectException;

  /**
   * Returns EORs for user with given user id.
   *
   * @param userId userId.
   * @return list of eor IMOs.
   * @throws ClassDirectException exception.
   */
  Set<String> getEorsByUserId(final String userId) throws ClassDirectException;

  /**
   * Returns users count based on search criteria.
   *
   * @param query query.
   * @return value.
   * @throws ClassDirectException exception.
   */
  Long getUsersCount(UserProfileDto query) throws ClassDirectException;


  /**
   * Returns all users.
   *
   * @return List of userProfiles.
   * @throws ClassDirectException when unexpected error occurs.
   */
  List<UserProfiles> getAllUsers() throws ClassDirectException;

  /**
   * Updates user's information such as full name and company name in database.
   *
   * @param userprofile user profile of the user.
   * @return List of userProfiles.
   * @throws ClassDirectException when unexpected error occurs.
   */
  int updateUserFullNameAndCompanyName(final UserProfiles userprofile) throws ClassDirectException;

  /**
   * Recreates archived user in database and removes the archived user's cache. (For internal use only).
   *
   * @param archivedUser archived user in Class-Direct.
   * @param requesterId user whom trying to re-create the archived user.
   * @param newprofile new user information.
   * @return status of the archived user re-creation.
   * @throws RecordNotFoundException when Record not found.
   * @throws ClassDirectException when class direct not found.
   */
  StatusDto recreateArchivedUser(final UserProfiles archivedUser, final String requesterId,
      final CreateNewUserDto newprofile) throws RecordNotFoundException, ClassDirectException;

  /**
   * Gets list of user eor's of expired eor users.
   *
   * @return list of user id's of expired eor users
   * @throws ClassDirectException if error in querying database.
   */
  List<UserEOR> getExpiredEorUsers() throws ClassDirectException;

  /**
   * Deletes user eor role by given user id.
   *
   * @param eorUser the eor user who has Eor Assets.
   * @throws ClassDirectException if record not found with given user id.
   */
  void deleteUserEORRole(final UserProfiles eorUser) throws ClassDirectException;

  /**
   * Updates logged in user's last login date time.
   *
   * @param editUserId user whose profile to be edited.
   * @param lastLogin the last login date time to be updated.
   * @return updated user profile.
   * @throws ClassDirectException if any sql exception.
   */
  int updateUserLastLogin(String editUserId, LocalDateTime lastLogin)
      throws ClassDirectException;
}
