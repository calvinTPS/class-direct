package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import java.util.List;
import java.util.Map;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceGroupHDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Retrofit ServiceReference.
 *
 * @author VMandalapu
 *
 */
public interface ServiceReferenceRetrofitService {

  /**
   * Get list of AssetServices.
   *
   * @param assetId value
   * @return value
   */
  @GET("asset/{assetId}/service")
  Call<List<ScheduledServiceHDto>> getAssetServices(@Path("assetId") Long assetId);

  /**
   * Get list of serviceCreditStatus.
   *
   * @return object
   */
  @GET("reference-data/service/service-credit-statuses")
  Call<List<ServiceCreditStatusHDto>> getServiceCreditStatuses();

  /**
   * Get list of serviceCatalogues.
   *
   * @return catalogues.
   */
  @GET("reference-data/service/service-catalogues")
  Call<List<ServiceCatalogueHDto>> getServiceCatalogues();

  /**
   * Get list of serviceGroups.
   *
   * @return groups.
   */
  @GET("reference-data/service/service-groups")
  Call<List<ServiceGroupHDto>> getServiceGroups();

  /**
   * Get list of service types.
   *
   * @return list of service types.
   *
   */
  @GET("reference-data/service/service-types")
  Call<List<ReferenceDataDto>> getServicesTypes();

  /**
   * @param assetId long assetId.
   * @param options query.
   * @return list of services.
   */
  @GET("asset/{assetId}/service")
  Call<List<ScheduledServiceHDto>> getAssetServicesQuery(@Path("assetId") Long assetId,
      @QueryMap Map<String, String> options);
}
