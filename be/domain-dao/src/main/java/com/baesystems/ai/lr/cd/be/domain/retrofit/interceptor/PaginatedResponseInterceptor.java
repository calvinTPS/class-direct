package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Interceptor for paginated response.
 *
 * @author msidek
 */
public class PaginatedResponseInterceptor implements Interceptor {
  /**
   *
   */
  private List<String> ignoredPaths = null;

  /**
   * @return ignoredPaths.
   */
  public final List<String> getIgnoredPaths() {
    return ignoredPaths;
  }

  /**
   * @param ignoredPathsList list.
   */
  public final void setIgnoredPaths(final List<String> ignoredPathsList) {
    // path will be regularExpression,"$" Matches the end of the path.
    this.ignoredPaths = ignoredPathsList.stream().map(path -> path += "$").collect(Collectors.toList());
  }

  @Override
  public final Response intercept(final Chain chain) throws IOException {
    final Response response = chain.proceed(chain.request());
    final MediaType contentType = response.body().contentType();
    final String jsonResponse = response.body().string();
    Response.Builder builder = null;
    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode node = mapper.readTree(jsonResponse).get("content");
    if (null != node && ignoredPaths != null
        && !ignoredPaths.stream().anyMatch(path -> chain.request().url().encodedPath().matches(path))) {
      builder = response.newBuilder().body(ResponseBody.create(contentType, node.toString()));
    } else {
      builder = response.newBuilder().body(ResponseBody.create(contentType, jsonResponse));
    }

    return builder.build();
  }

}
