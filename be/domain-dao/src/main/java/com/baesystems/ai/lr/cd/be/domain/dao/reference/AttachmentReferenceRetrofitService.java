package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author syalavarthi
 *
 */
public interface AttachmentReferenceRetrofitService {

  /**
   * Get A list of attachment types.
   *
   * @return List of attachmemt types.
   */
  @GET("reference-data/attachment/attachment-types")
  Call<List<AttachmentTypeHDto>> getAttachmentTypes();

  /**
   * Get A list of attachment categories.
   *
   * @return List of attachmemt categories.
   */
  @GET("reference-data/attachment/attachment-categories")
  Call<List<AttachmentCategoryHDto>> getAttachmentCategories();
}


