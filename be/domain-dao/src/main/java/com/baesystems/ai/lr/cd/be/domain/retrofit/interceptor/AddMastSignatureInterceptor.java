package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import com.baesystems.ai.lr.cd.be.domain.dto.security.MastSignature;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okio.Buffer;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Intercept for each MAST request and append signature.
 *
 * @author Faizal Sidek
 */
public class AddMastSignatureInterceptor implements Interceptor {

  /**
   * Log object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AddMastSignatureInterceptor.class);

  /**
   * Pattern to match URL.
   */
  private static final String URL_PATTERN = "(http|https)://?[a-zA-Z-.0-9+&@#%?]+(:[0-9]+)?";

  /**
   * Injected security DAO.
   */
  @Autowired
  private SecurityDAO securityDAO;

  /**
   * Flag to determine mast security status.
   */
  private boolean mastSecurityEnabled = false;

  /**
   * Test user name.
   */
  private String testUserName;

  /**
   * Test group name.
   */
  private String testGroupName;

  /**
   * Setter for {@link #mastSecurityEnabled}.
   *
   * @param flag value to be set.
   */
  public final void setMastSecurityEnabled(final boolean flag) {
    this.mastSecurityEnabled = flag;
  }

  /**
   * Setter for {@link #testUserName}.
   *
   * @param userName value to be set.
   */
  public final void setTestUserName(final String userName) {
    this.testUserName = userName;
  }

  /**
   * Setter for {@link #testGroupName}.
   *
   * @param groupName value to be set.
   */
  public final void setTestGroupName(final String groupName) {
    this.testGroupName = groupName;
  }

  @Override
  public final Response intercept(final Chain chain) throws IOException {
    final Request request = chain.request();

    final Request.Builder builder;
    if (mastSecurityEnabled) {
      String encodedPath = request.url().encodedPath();
      encodedPath = encodedPath.replaceFirst("api/v", "trusted-system/api/v");
      final HttpUrl url = request.url().newBuilder().encodedPath(encodedPath).build();

      final MastSignature signature;
      try {
        signature = securityDAO.getActiveMastSignature();
      } catch (ClassDirectException exception) {
        throw new IOException("Signature not found", exception);
      }
      builder = request.newBuilder().headers(request.headers()).addHeader("X-Client-Identity", signature.getMastId())
          .url(url).method(request.method(), request.body());

      final String uriKey = getUriKey(request, url);
      final String checksum = signRequest(uriKey, signature);
      builder.addHeader("X-Message-Signature", checksum);
    } else {
      builder = request.newBuilder().headers(request.headers()).addHeader("user", testUserName)
          .addHeader("groups", testGroupName).method(request.method(), request.body()).url(request.url());
    }
    return chain.proceed(builder.build());
  }

  /**
   * Generate URI key from method, uri and body.
   *
   * @param request request object.
   * @param url request url.
   * @return formatted uri key.
   */
  private String getUriKey(final Request request, final HttpUrl url) {
    final String method = request.method().toUpperCase(Locale.ROOT);
    String uriAndContext = "";
    try {
      uriAndContext = URLDecoder.decode(url.toString(), StandardCharsets.UTF_8.displayName());
      final Matcher matcher = Pattern.compile(URL_PATTERN).matcher(uriAndContext);

      if (matcher.find()) {
        uriAndContext = uriAndContext.substring(matcher.end());
      } else {
        uriAndContext = uriAndContext.substring(uriAndContext.indexOf("/mast"));
      }
    } catch (UnsupportedEncodingException exception) {
      LOGGER.error("Invalid encoding exception.", exception);
    }

    final StringBuilder key = new StringBuilder(255);
    key.append(method).append(':').append(uriAndContext);

    if (request.body() != null) {
      try {
        final Buffer buffer = new Buffer();
        request.body().writeTo(buffer);
        final String body = buffer.readString(StandardCharsets.UTF_8);
        key.append('[').append(body).append(']');
      } catch (IOException exception) {
        LOGGER.error("Error appending body.", exception);
      }
    }

    return key.toString();
  }

  /**
   * Sign the request with private key.
   *
   * @param body formatted request body.
   * @param signature signature holder.
   * @return checksum.
   * @throws IOException when error.
   */
  private String signRequest(final String body, final MastSignature signature) throws IOException {
    try {
      final PEMParser parser = new PEMParser(new StringReader(signature.getPrivateKey()));
      final PEMKeyPair keyPair = (PEMKeyPair) parser.readObject();
      final PrivateKeyInfo keyInfo = keyPair.getPrivateKeyInfo();
      final PrivateKey privateKey = new JcaPEMKeyConverter().getPrivateKey(keyInfo);

      final Signature signer = Signature.getInstance(signature.getAlgorithm());
      signer.initSign(privateKey);
      signer.update(body.getBytes(StandardCharsets.UTF_8));
      byte[] signatureBytes = signer.sign();

      return new String(Hex.encode(signatureBytes), StandardCharsets.UTF_8) + "==";
    } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException exception) {
      LOGGER.error("Error signing request.", exception);
      throw new IOException("Signing error.", exception);
    }
  }
}
