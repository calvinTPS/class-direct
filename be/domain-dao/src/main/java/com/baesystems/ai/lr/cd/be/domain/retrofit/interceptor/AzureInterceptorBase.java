package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import com.baesystems.ai.lr.cd.be.domain.dto.security.AzureAccessToken;
import com.baesystems.ai.lr.cd.be.domain.utils.JSONUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;

/**
 * Intercepts calls to azure (sitekit or internal LR AD).
 *
 * @author fwijaya on 5/5/2017.
 */
public abstract class AzureInterceptorBase implements Interceptor {
  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AzureInterceptorBase.class);
  /**
   * Access token.
   */
  private AzureAccessToken token;
  /**
   * Token url.
   */
  private String tokenUrl;
  /**
   * Token grant type.
   */
  private String tokenGrantType;
  /**
   * Token client id.
   */
  private String tokenClientId;
  /**
   * Token client secret.
   */
  private String tokenClientSecret;

  /**
   * Processes http response.
   *
   * @param response http response.
   * @return The processed response.
   * @throws IOException exceptions.
   */
  protected final Response processResponse(final Response response) throws IOException {
    // Invalidate the token if received 401, which normally should not.
    // This should allow next call to get a new token.
    // HTTP 201 for create user success
    if (response.code() == HttpStatus.OK.value() || response.code() == HttpStatus.CREATED.value()) {
      okhttp3.MediaType contentType = response.body().contentType();
      final String jsonResponse = response.body().string();

      String resultStr = jsonResponse;
      final ObjectMapper mapper = new ObjectMapper();

      // check response body is valid json or not
      if (JSONUtils.isJSONValid(jsonResponse)) {
        // Safety check for value tag
        final JsonNode node = mapper.readTree(jsonResponse).get("value");

        if (node != null) {
          resultStr = node.toString();
        }
      } else {
        resultStr = JSONUtils.generateStatusDtoJson(response.code());
        contentType = okhttp3.MediaType.parse("application/json");
      }
      final Response.Builder respBuilder = response.newBuilder().body(ResponseBody.create(contentType, resultStr));
      return respBuilder.build();
    } else {
      if (response.code() == HttpStatus.UNAUTHORIZED.value()) {
        LOGGER.error("Received 401 from Azure with cached token. {}", token.toString());
        token = null;
      }
      throw new IOException("Get " + response.code() + " error when querying Azure: " + response.body().string());
    }
  }

  /**
   * Refreshes token.
   *
   * @param additionalProperties Additional properties to be sent in the request body.
   * @throws IOException exception.
   */
  protected final void refreshToken(final Map<String, String> additionalProperties) throws IOException {
    if (token != null && !isExpired(token.getExpiresOn())) {
      return;
    }
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    final MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("grant_type", tokenGrantType);
    map.add("client_id", tokenClientId);
    map.add("client_secret", tokenClientSecret);
    if (additionalProperties != null) {
      additionalProperties.keySet().stream().forEach(key -> map.add(key, additionalProperties.get(key)));
    }

    final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

    final ResponseEntity<AzureAccessToken> response =
      new RestTemplate().postForEntity(tokenUrl, request, AzureAccessToken.class);
    if (response.getStatusCode().is2xxSuccessful()) {
      token = response.getBody();
      LOGGER.debug("Refresh token happened.");
    } else {
      throw new IOException("Unable to get access token: {} " + response.getStatusCode().getReasonPhrase());
    }
  }

  /**
   * Checks whether the given timestamp is in the past.
   *
   * @param timestamp timestamp.
   * @return true if the timestamp is in the past.
   */
  private Boolean isExpired(final Long timestamp) {
    ZoneOffset offset = ZoneOffset.systemDefault().getRules().getOffset(LocalDateTime.now());
    return LocalDateTime.ofEpochSecond(timestamp, 0, offset).isBefore(LocalDateTime.now());
  }

  /**
   * Clear azure access token.
   */
  public final void clearToken() {
    token = null;
  }

  /**
   * Returns the token client secret.
   *
   * @return The token client secret.
   */
  public final String getTokenClientSecret() {
    return tokenClientSecret;
  }

  /**
   * Sets the token client secret.
   *
   * @param tokenClientSecretArg The token client secret.
   */
  public final void setTokenClientSecret(final String tokenClientSecretArg) {
    this.tokenClientSecret = tokenClientSecretArg;
  }

  /**
   * Returns the token client id.
   *
   * @return The token client id.
   */
  public final String getTokenClientId() {
    return tokenClientId;
  }

  /**
   * Sets the token client id.
   *
   * @param tokenClientIdArg The token client id.
   */
  public final void setTokenClientId(final String tokenClientIdArg) {
    this.tokenClientId = tokenClientIdArg;
  }

  /**
   * Returns the token grant type.
   *
   * @return The token grant type.
   */
  public final String getTokenGrantType() {
    return tokenGrantType;
  }

  /**
   * Sets the token grant type.
   *
   * @param tokenGrantTypeArg The token grant type.
   */
  public final void setTokenGrantType(final String tokenGrantTypeArg) {
    this.tokenGrantType = tokenGrantTypeArg;
  }

  /**
   * Returns the token URL.
   *
   * @return The token URL.
   */
  public final String getTokenUrl() {
    return tokenUrl;
  }

  /**
   * Sets the token URL.
   *
   * @param tokenUrlArg The token URL.
   */
  public final void setTokenUrl(final String tokenUrlArg) {
    this.tokenUrl = tokenUrlArg;
  }

  /**
   * Returns the token.
   *
   * @return The token.
   */
  public final AzureAccessToken getToken() {
    return token;
  }

  /**
   * Sets the token.
   *
   * @param tokenArg The token.
   */
  public final void setToken(final AzureAccessToken tokenArg) {
    this.token = tokenArg;
  }
}
