package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import com.baesystems.ai.lr.cd.be.domain.dto.security.MastSignature;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import java.util.List;
import org.springframework.cache.annotation.Cacheable;

/**
 * Security DAO.
 *
 * @author vkovuru
 */
public interface SecurityDAO {

  /**
   * @param userId userId.
   * @param roles list of roles.
   * @return boolean value.
   */
  boolean hasRole(final String userId, final List<String> roles);

  /**
   * Get active MAST signature.
   *
   * @return mast signature.
   * @throws ClassDirectException error.
   */
  @Cacheable(cacheNames = "signatures", key = "methodName")
  MastSignature getActiveMastSignature() throws ClassDirectException;
}
