package com.baesystems.ai.lr.cd.be.domain.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.certificate.CertificateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.customer.CustomerRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.employee.EmployeeRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AttachmentReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.CertificateReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.FlagStateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.JobReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.PortRegistryService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ProductReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ReferenceDataVersionRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskRetrofitService;

import retrofit2.Retrofit;

/**
 * Factory for Retrofit services.
 *
 * @author msidek
 */
public class RetrofitServiceFactory {

  /**
   * Retrofit builder.
   */
  @Autowired
  @Qualifier("retrofit")
  private Retrofit retrofit;

  /**
   * Create new AssetReferenceService.
   *
   * @return assetReferenceService
   */
  public final AssetReferenceRetrofitService createAssetReferenceService() {
    return retrofit.create(AssetReferenceRetrofitService.class);
  }

  /**
   * Create new PortRegistryService.
   *
   * @return PortRegistryService
   */
  public final PortRegistryService createPortRegistryService() {
    return retrofit.create(PortRegistryService.class);
  }

  /**
   * Create new ServiceReferenceRetrofitService.
   *
   * @return serviceReferenceRetrofitService
   */
  public final ServiceReferenceRetrofitService createServiceRetrofitService() {
    return retrofit.create(ServiceReferenceRetrofitService.class);
  }

  /**
   * @return asset service. Create new AssetRetrofitService.
   */
  public final AssetRetrofitService createAssetRetrofitService() {
    return retrofit.create(AssetRetrofitService.class);
  }

  /**
   * Create new {@link FlagStateRetrofitService}.
   *
   * @return service.
   */
  public final FlagStateRetrofitService createFlagStateRetrofitService() {
    return retrofit.create(FlagStateRetrofitService.class);
  }

  /**
   * Create new {@link JobRetrofitService}.
   *
   * @return service.
   */
  public final JobRetrofitService createJobRetrofitService() {
    return retrofit.create(JobRetrofitService.class);
  }

  /**
   * Create new {@link JobReferenceRetrofitService}.
   *
   * @return JobReferenceRetrofitService.
   */
  public final JobReferenceRetrofitService createJobReferenceRetrofitService() {
    return retrofit.create(JobReferenceRetrofitService.class);
  }

  /**
   * Create new {@link ReferenceDataVersionRetrofitService}.
   *
   * @return service.
   */
  public final ReferenceDataVersionRetrofitService createReferenceDataVersionRetrofitService() {
    return retrofit.create(ReferenceDataVersionRetrofitService.class);
  }

  /**
   * Create new {@link TaskRetrofitService}.
   *
   * @return task.
   */
  public final TaskRetrofitService createTaskRetrofitService() {
    return retrofit.create(TaskRetrofitService.class);
  }

  /**
   * Create new {@link TaskReferenceRetrofitService}.
   *
   * @return TaskReferenceRetrofitService.
   */
  public final TaskReferenceRetrofitService createTaskReferenceRetrofitService() {
    return retrofit.create(TaskReferenceRetrofitService.class);
  }

  /**
   * Create new {@link IhsRetrofitService}.
   *
   * @return ihs.
   */
  public final IhsRetrofitService createIhsRetrofitService() {
    return retrofit.create(IhsRetrofitService.class);
  }

  /**
   * Create new {@link CustomerRetrofitService}.
   *
   * @return Customer Information.
   */
  public final CustomerRetrofitService createCustomerRetrofitService() {
    return retrofit.create(CustomerRetrofitService.class);
  }

  /**
   * Create new {@link AttachmentRetrofitService}.
   *
   * @return AttachmentRetrofitService.
   */
  public final AttachmentRetrofitService createAttachmentRetrofitService() {
    return retrofit.create(AttachmentRetrofitService.class);
  }

  /**
   * Create new CertificateReferenceRetrofitService.
   *
   * @return CertificateReferenceRetrofitService.
   */
  public final CertificateReferenceRetrofitService createCertificateReferenceRetrofitService() {
    return retrofit.create(CertificateReferenceRetrofitService.class);
  }

  /**
   * Create new CertificateRetrofitService.
   *
   * @return CertificateRetrofitService.
   */
  public final CertificateRetrofitService createCertificateRetrofitService() {
    return retrofit.create(CertificateRetrofitService.class);
  }

  /**
   * Creates new ProductReferenceRetrofitService.
   *
   * @return the retrofit service.
   */
  public final ProductReferenceRetrofitService createProductReferenceRetrofitService() {
    return retrofit.create(ProductReferenceRetrofitService.class);
  }

  /**
   * Create new EmployeeRetrofitService.
   *
   * @return EmployeeRetrofitService.
   *
   */
  public final EmployeeRetrofitService createEmployeeRetrofitService() {
    return retrofit.create(EmployeeRetrofitService.class);
  }

  /**
   * Create new AttachmentReferenceRetrofitService.
   *
   * @return AttachmentReferenceRetrofitService.
   *
   */
  public final AttachmentReferenceRetrofitService createAttachmentReferenceRetrofitService() {
    return retrofit.create(AttachmentReferenceRetrofitService.class);
  }
}

