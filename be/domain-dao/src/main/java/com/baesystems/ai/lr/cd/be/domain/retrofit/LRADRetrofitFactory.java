package com.baesystems.ai.lr.cd.be.domain.retrofit;

import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.retrofit.propertysetter.BasePropertySetter;
import com.baesystems.ai.lr.cd.be.domain.retrofit.propertysetter.ObjectIdPropertySetter;
import com.baesystems.ai.lr.cd.be.domain.retrofit.propertysetter.SignInNamePropertySetter;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Provides factory method to create Retrofit object to call internal LR AD endpoints.
 *
 * @author fwijaya
 */
public class LRADRetrofitFactory extends BaseRetrofitFactory {

  /**
   * The logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(LRADRetrofitFactory.class);

  /**
   * The default Retrofit client connection timeout which is {@value #DEFAULT_TIMEOUT_IN_SECONDS}.
   */
  private static final Integer DEFAULT_TIMEOUT_IN_SECONDS = 300;

  /**
   * The read/write and general timeout in seconds for created Retrofit object.
   * Defaulted to {@value #DEFAULT_TIMEOUT_IN_SECONDS} if not specified in Spring Context.
   */
  private Integer timeout = DEFAULT_TIMEOUT_IN_SECONDS;

  /**
   * The internal LR AD REST API base URL injected from Spring context.
   */
  private String lrADBaseUrl;

  /**
   * Provides a new Retrofit client service to LR AD (internal users) REST API service.
   *
   * @return the Retrofit client service pointing to LR AD defined in baseUrl.
   */
  public final Retrofit createLRADRetrofit() {
    LOGGER.info("Creating retrofit factory with LR AD base url {}", lrADBaseUrl);
    return new Retrofit.Builder().baseUrl(lrADBaseUrl)
      .addConverterFactory(JacksonConverterFactory.create(createMapper())).client(createClient()).build();
  }

  /**
   * Creates a new instance of {@link OkHttpClient}.
   *
   * @return the {@link OkHttpClient} use by Retrofit service.
   */
  private OkHttpClient createClient() {
    OkHttpClient client = null;
    final Builder builder = new Builder();
    builder.connectTimeout(timeout, TimeUnit.SECONDS);
    builder.readTimeout(timeout, TimeUnit.SECONDS);
    builder.writeTimeout(timeout, TimeUnit.SECONDS);
    // Disable OkHttp3 auto-retry on network failure (Fail intermittent on unit test)
    // see https://github.com/square/okhttp/issues/2394
    // builder.retryOnConnectionFailure(false);

    if (super.getContext() != null) {
      addInterceptors(builder);
      client = builder.build();
    }

    return client;
  }

  /**
   * Creates a new JSON ObjectMapper to parse REST JSON request and response.
   *
   * @return the JSON ObjectMapper to parse REST JSON request and response.
   */
  private ObjectMapper createMapper() {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new SimpleModuleExt());
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.setDateFormat(new ISO8601DateFormat());
    return mapper;
  }

  /**
   * Module to modify default jackson mapper.
   */
  public static class SimpleModuleExt extends SimpleModule {

    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = -3469043895842195421L;

    /**
     * Adds a new deserializer to handle signInNames json field returned by LR AD.
     *
     * @param context The setup context.
     */
    @Override
    public final void setupModule(final SetupContext context) {
      super.setupModule(context);
      context.addBeanDeserializerModifier(new BeanDeserializerModifierExt());
    }
  }


  /**
   * Modifies json deserialization.
   */
  public static class BeanDeserializerModifierExt extends BeanDeserializerModifier {

    /**
     * Update deserialization builder.
     *
     * @param config Config.
     * @param beanDesc Bean description.
     * @param builder Builder.
     * @return the bean deserialization builder.
     */
    public final BeanDeserializerBuilder updateBuilder(final DeserializationConfig config,
                                                       final BeanDescription beanDesc,
                                                       final BeanDeserializerBuilder builder) {
      if (!beanDesc.getBeanClass().equals(LRIDUserDto.class)) {
        return builder;
      }
      final Iterator<SettableBeanProperty> iterator = builder.getProperties();
      final List<SettableBeanProperty> newPropertySetters = new ArrayList<>();
      while (iterator.hasNext()) {
        final SettableBeanProperty next = iterator.next();
        if (next.getName().equals(BasePropertySetter.SIGN_IN_NAME_PROPERTY)) {
          newPropertySetters.add(new SignInNamePropertySetter(next));
        } else if (next.getName().equals(BasePropertySetter.OBJECT_ID_PROPERTY)) {
          newPropertySetters.add(new ObjectIdPropertySetter(next));
        }
      }
      newPropertySetters.stream().forEach(propSetter -> builder.addOrReplaceProperty(propSetter, true));
      return builder;
    }
  }

  /**
   * Returns the timeout.
   *
   * @return The timeout.
   */
  public final Integer getTimeout() {
    return timeout;
  }

  /**
   * Sets the timeout.
   *
   * @param timeoutArg Timeout.
   */
  public final void setTimeout(final Integer timeoutArg) {
    this.timeout = timeoutArg;
  }

  /**
   * Returns the base URL.
   *
   * @return The base url.
   */
  public final String getLrADBaseUrl() {
    return lrADBaseUrl;
  }

  /**
   * Sets the base URL.
   *
   * @param lrADBaseUrlArg The base URL.
   */
  public final void setLrADBaseUrl(final String lrADBaseUrlArg) {
    this.lrADBaseUrl = lrADBaseUrlArg;
  }
}
