package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AvailabilityDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.SystemMaintenance;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * DAO for {@link SystemMaintenance}.
 *
 * @author Faizal Sidek
 */
@Repository
public class AvailabilityDaoImpl implements AvailabilityDao {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AvailabilityDao.class);

  /**
   * Entity manager.
   */
  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public final SystemMaintenance getActiveSystemMaintenance() {
    SystemMaintenance systemMaintenance = null;

    try {
      final TypedQuery<SystemMaintenance> query = entityManager
        .createQuery("SELECT maintenance from SystemMaintenance maintenance where maintenance.active = true",
          SystemMaintenance.class);
      systemMaintenance = query.getSingleResult();
    } catch (PersistenceException exception) {
      if (!(exception instanceof NoResultException)) {
        LOGGER.error("Exception when querying system maintenance.", exception);
      }
    }

    return systemMaintenance;
  }
}
