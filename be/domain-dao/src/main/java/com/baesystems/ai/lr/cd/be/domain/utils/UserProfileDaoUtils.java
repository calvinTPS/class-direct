package com.baesystems.ai.lr.cd.be.domain.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.google.common.collect.Sets;

/**
 * @author sbollu
 */
public final class UserProfileDaoUtils {

  /**
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileDaoUtils.class);

  /**
   * A list of deletable field names.
   */
  private static final List<String> DELETABLE_FIELDS =
      Collections.unmodifiableList(Arrays.asList("clientCode", "flagCode", "shipBuilderCode"));

  /**
   * The list of updatable field names.
   */
  private static final Set<String> UPDATABLE_FIELDS = Collections.unmodifiableSet(Sets.newHashSet("getClientCode",
      "getFlagCode", "getShipBuilderCode", "getUserAccExpiryDate", "getStatus", "getDateOfModification"));

  /**
   * List of special characters to be escaped for sql query parameters.
   */
  private static final String SQL_PARAM_SPECIAL_CHARACTERS = "%_";

  /**
   * First occurrence in regular expression (Regex) match.
   */
  private static final String REGEX_FIRST_OCCURRENCE = "$0";

  /**
   * Single backslash character in regex.
   */
  private static final String REGEX_BACKSLASH_CHARACTER = "\\\\";

  /**
   */
  private UserProfileDaoUtils() {

  }

  /**
   * Format input string to perform Structured Query Language query.
   *
   * @param inputString input string.
   * @return formatted string value.
   */
  public static String getFormattedStringForSql(final String inputString) {

    // Replace a single backslash with 4 backslash
    final String formattedSQL = inputString.replaceAll(REGEX_BACKSLASH_CHARACTER,
        REGEX_BACKSLASH_CHARACTER + REGEX_BACKSLASH_CHARACTER + REGEX_BACKSLASH_CHARACTER + REGEX_BACKSLASH_CHARACTER);

    // Escape special characters of MySQL with a backslash character.
    return Pattern.compile("[" + SQL_PARAM_SPECIAL_CHARACTERS + "]").matcher(formattedSQL)
        .replaceAll(REGEX_BACKSLASH_CHARACTER + REGEX_FIRST_OCCURRENCE);
  }

  /**
   * Copies over values from existing user profile to new user profile. Deletes values if values
   * exist in the {@code delete} list of comma-delimited field names.
   *
   * @param existingUserProfile The existing user profile.
   * @param newUserProfile The new user profile.
   * @param fieldsToDelete A comma-delimited string of field names to delete.
   * @param isAdminUser Whether or not the user is an admin user (LR Admin or LR Support).
   */
  public static void updateUserProfilesFields(final UserProfiles existingUserProfile, final UserProfiles newUserProfile,
      final String fieldsToDelete, final boolean isAdminUser) {

    final Optional<List<String>> fieldsToDeleteOpt =
        Optional.ofNullable(fieldsToDelete).map(fields -> fields.split(",")).map(Arrays::asList);

    final Class<UserProfiles> userProfileClass = UserProfiles.class;
    Stream.of(userProfileClass.getMethods()).filter(methodName -> methodName.getName().startsWith("get"))
        .filter(method -> {
          try {
            // Getting only methods where there's no value to replace with the DB data values.
            return (!UPDATABLE_FIELDS.contains(method.getName()) || method.invoke(newUserProfile) == null);
          } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException exception) {
            LOGGER.error("Can't call method " + method.getName(), exception);
            return false;
          }
        }).forEach(getMethod -> {
          // setting the user we're going to update with the missing values.
          final String setMethodName = getMethod.getName().replaceAll("get", "set");
          LOGGER.debug("GET [{}: return type: {}]", getMethod.getName(), getMethod.getReturnType());
          try {
            final Method setMethod = userProfileClass.getMethod(setMethodName, getMethod.getReturnType());
            LOGGER.debug("SET [{}: Parameters: {}]", setMethod.getName(), setMethod.getParameterTypes());

            // If delete list if not empty, delete values for fields in this list.
            if (isAdminUser) {
              Object valueToSet = getMethod.invoke(existingUserProfile);
              String fieldName = StringUtils.uncapitalize(setMethodName.replace("set", ""));
              if (fieldsToDeleteOpt.isPresent()) {
                if (fieldsToDeleteOpt.get().contains(fieldName) && DELETABLE_FIELDS.contains(fieldName)) {
                  valueToSet = null;
                }
              }
              setMethod.invoke(newUserProfile, valueToSet);
            }
          } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
              | InvocationTargetException exception) {
            LOGGER.error("Can't call method", exception);
          }
        });
  }


  /**
   * Set if a role should be admin and what kind of admin.
   *
   * @param roles list of roles a user has.
   * @return role of user.
   */
  public static Roles setUserRoleField(final Set<Roles> roles) {
    final Roles userRole = new Roles();
    userRole.setIslrSupport(false);
    userRole.setIslrAdmin(false);
    userRole.setIsAdmin(false);

    Comparator<Roles> comparator = (r1, r2) -> {
      Role enumR1 = Role.mapFromRoles(r1);
      Role enumR2 = Role.mapFromRoles(r2);
      return Integer.compare(enumR2.getRank(), enumR1.getRank());
    };

    roles.stream().sorted(comparator).findFirst().ifPresent(role -> {
      userRole.setRoleName(role.getRoleName());
      userRole.setRoleId(role.getRoleId());

      switch (Role.mapFromRoles(role)) {
        case LR_ADMIN:
          userRole.setIslrSupport(false);
          userRole.setIslrAdmin(true);
          userRole.setIsAdmin(true);
          break;
        case LR_SUPPORT:
          userRole.setIslrSupport(true);
          userRole.setIslrAdmin(false);
          userRole.setIsAdmin(true);
          break;
        default:
          userRole.setIslrSupport(false);
          userRole.setIslrAdmin(false);
          userRole.setIsAdmin(false);
          break;
      }
    });

    return userRole;
  }

}
