package com.baesystems.ai.lr.cd.be.domain.profiling;

import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.entities.Subsegment;
import com.amazonaws.xray.exceptions.SegmentNotFoundException;
import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingStat.MethodStat;
import com.baesystems.ai.lr.cd.be.enums.ResponseOptionEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Profiling Aspect for Service, DAO and Retrofit.
 *
 * @author YWearn
 */
@Aspect
public class ProfilingAspectImpl implements ProfilingAspect {

  /**
   * Logger object.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProfilingAspectImpl.class);
  /**
   * Dot constant.
   */
  private static final String DOT = ".";
  /**
   * Dot regex.
   */
  private static final String DOT_REGEX = "\\.";
  /**
   * Comma constant.
   */
  private static final String COMMA = ",";
  /**
   * Space constant.
   */
  private static final String SPACE = " ";
  /**
   * Open round bracket constant.
   */
  private static final String OPEN_ROUND_BRACKET = "(";
  /**
   * Closed round bracket constant.
   */
  private static final String CLOSE_ROUND_BRACKET = ")";
  /**
   * Open square bracket constant.
   */
  private static final String OPEN_SQUARE_BRACKET = "[";
  /**
   * Close square bracket constant.
   */
  private static final String CLOSE_SQUARE_BRACKET = "]";
  /**
   * Indent for first log line constant.
   */
  private static final String TITLE_IND = " ==== ";
  /**
   * Indent for first method.
   */
  private static final String MAIN_METHOD_IND = "**";
  /**
   * Indent for child method.
   */
  private static final String METHOD_IND = "--";
  /**
   * Not applicable constant.
   */
  private static final String NA = "NA";
  /**
   * Log Format in {ElapseTime} (ms) {Indent} {External/Internal} {Indent}
   * {CachedResponse/NetworkResponse} {MethodCall}, ST: {StartTime}, ET: {EndTime}.
   */
  private static final String METHOD_STAT_LOG_FORMAT = "%1$6d (ms) %2$s [%3$s] %4$.100s, ST: %5$s, ET: %6$s";

  /**
   * List of excluded methods to be profiled.
   */
  private List<String> excludedMethods;

  /**
   * Setter for {@link #excludedMethods}.
   *
   * @param methods value to be set.
   */
  public final void setExcludedMethods(final List<String> methods) {
    this.excludedMethods = methods;
  }

  /**
   * Constructor.
   */
  public ProfilingAspectImpl() {
    super();
    allMethod();
    isService();
    isDao();
  }

  /**
   * Pointcut holder for all methods.
   */
  @Pointcut("execution(* *(..))")
  private void allMethod() {

  }

  /**
   * Pointcut holder for service classes to avoid advice CacheAspect.
   */
  @Pointcut("within(com.baesystems.ai.lr.cd.service.asset..*)"
      + " || within(com.baesystems.ai.lr.cd.service.certificate..*)"
      + " || within(com.baesystems.ai.lr.cd.service.favourites..*)"
      + " || within(com.baesystems.ai.lr.cd.service.flagstate..*)"
      + " || within(com.baesystems.ai.lr.cd.service.job..*)"
      + " || within(com.baesystems.ai.lr.cd.service.notifications..*)"
      + " || within(com.baesystems.ai.lr.cd.service.port..*)"
      + " || within(com.baesystems.ai.lr.cd.service.security.SecurityService*)"
      + " || within(com.baesystems.ai.lr.cd.service.subfleet..*)"
      + " || within(com.baesystems.ai.lr.cd.service.survey..*)" + " || within(com.baesystems.ai.lr.cd.service.task..*)"
      + " || within(com.baesystems.ai.lr.cd.service.user..*)"
      + " || within(com.baesystems.ai.lr.cd.service.attachment..*)"
      + " || within(com.baesystems.ai.lr.cd.service.export..*)"
      + " || within(com.baesystems.ai.lr.cd.service.countryfiles..*)"
      + " || within(com.baesystems.ai.lr.cd.service.aws..*)"
      + " || within(com.baesystems.ai.lr.cd.service.userprofile..*)")
  private void isService() {

  }

  /**
   * Pointcut holder for DAO classes.
   */
  @Pointcut("within(com.baesystems.ai.lr.cd.be.domain.hibernate.dao..*)")
  private void isDao() {

  }

  /**
   * Threadlocal to capture the UUID from main thread.
   */
  public static final ThreadLocal<ProfilingStat> THREAD_CONTEXT = new ThreadLocal<>();

  /**
   * For retrofit interceptor to capture the performance statistics.
   *
   * @param methodName Method name.
   * @param optEnum Flag indicates ResponseType(cached,external,Internal).
   * @param startTime Method start time.
   * @param endTime Method end time.
   * @return MethodStat object contains profiling information.
   */
  @Override
  public final MethodStat injectMethodStatistic(final String methodName, final ResponseOptionEnum optEnum,
      final LocalDateTime startTime, final LocalDateTime endTime) {
    final MethodStat stat = new MethodStat();
    stat.setMethodName(methodName);
    stat.setResponseOpt(optEnum);
    stat.setStartTime(startTime);
    stat.setEndTime(endTime);

    if (THREAD_CONTEXT.get() == null) {
      final ProfilingStat stats = new ProfilingStat();
      stats.setThreadIdentifier(MDC.get(CDConstants.REQUEST_ID));
      stats.setParentMethod(methodName);
      THREAD_CONTEXT.set(stats);
      LOGGER.trace("Inject parent method statistic. | {}", stat);
    }
    THREAD_CONTEXT.get().getMethodStatList().add(stat);
    return stat;
  }

  /**
   * Print statistic to log file.
   */
  @Override
  public final void flushStat() {
    printToLog(THREAD_CONTEXT.get());
    printJsonToLog(THREAD_CONTEXT.get());
    THREAD_CONTEXT.remove();
  }

  /**
   * AspectJ advice for service and dao classes.
   *
   * @param joinPoint wrapper of the adviced method.
   * @return the result of the adviced method call.
   * @throws Throwable Any exceptions from the method.
   */
  @Around("allMethod() && (isService() || isDao())")
  public final Object profileAround(final ProceedingJoinPoint joinPoint) throws Throwable {
    Object result = null;
    boolean isFirstMethod = false;
    final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    final String methodName = prettyPrintMethodName(signature);

    final String classMethodId =
      signature.getMethod().getDeclaringClass().getName() + "." + signature.getMethod().getName();
    if (excludedMethods.contains(classMethodId)) {
      return joinPoint.proceed();
    }

    Subsegment subsegment = null;
    try {
      final String className = signature.getMethod().getDeclaringClass().getSimpleName();
      final String method = signature.getMethod().getName();
      LOGGER.trace("Adding segment for {}::{}", className, method);
      AWSXRay.getCurrentSegment();
      subsegment = AWSXRay.beginSubsegment(className + "::" + method);
    } catch (SegmentNotFoundException exception) {
      LOGGER.trace("No segment declared.");
    }

    MethodStat mStat = null;
    try {
      if (THREAD_CONTEXT.get() == null) {
        final ProfilingStat stats = new ProfilingStat();
        stats.setThreadIdentifier(MDC.get(CDConstants.REQUEST_ID));
        stats.setParentMethod(methodName);
        THREAD_CONTEXT.set(stats);
        isFirstMethod = true;
      }

      mStat = injectMethodStatistic(methodName, ResponseOptionEnum.Internal, LocalDateTime.now(), null);
      result = joinPoint.proceed(joinPoint.getArgs());
    } catch (Throwable exception) {
      if (subsegment != null) {
        subsegment.addException(exception);
      }
      // Re-throw the original exception
      throw exception;
    } finally {
      if (subsegment != null) {
        AWSXRay.endSubsegment();
      }
      if (mStat != null) {
        mStat.setEndTime(LocalDateTime.now());
      }
      if (isFirstMethod) {
        flushStat();
      }
    }
    return result;
  }

  /**
   * Print statistic to log file.
   *
   * @param stat the end to end statistic.
   */
  protected final void printToLog(final ProfilingStat stat) {
    final StringBuilder sb = new StringBuilder();
    sb.append(TITLE_IND).append(stat.getParentMethod()).append(SPACE).append(OPEN_SQUARE_BRACKET)
        .append(stat.getThreadIdentifier()).append(CLOSE_SQUARE_BRACKET).append(TITLE_IND);

    final List<MethodStat> mStatList = stat.getMethodStatList();
    final MethodStat parentMStat = mStatList.get(0);

    prettyPrintMethodStat(sb, parentMStat, MAIN_METHOD_IND);

    for (int i = 1; i < mStatList.size(); i++) {
      prettyPrintMethodStat(sb, mStatList.get(i), METHOD_IND);
    }
    sb.append(TITLE_IND);
    LOGGER.info(sb.toString());
  }

  /**
   * Print statistic to log file in json format.
   *
   * @param stat the end to end statistic.
   */
  protected final void printJsonToLog(final ProfilingStat stat) {
    try {
      final ObjectMapper mapper = new ObjectMapper();
      mapper.setDateFormat(new ISO8601DateFormat());
      final String jsonStr = mapper.writeValueAsString(stat);
      LOGGER.info("JSON ProfilingStat: {}", jsonStr);
    } catch (final JsonProcessingException e) {
      LOGGER.error("Error converting json for {} | ErrorMessage: {}", stat, e.getMessage(), e);
    }
  }

  /**
   * Format method statistic for logging.
   *
   * @param sb the StringBuilder.
   * @param stat the method statistic.
   * @param indent the line indent.
   */
  protected final void prettyPrintMethodStat(final StringBuilder sb, final MethodStat stat, final String indent) {

    if (stat != null) {
      final Optional<MethodStat> optStat = Optional.of(stat);

      final String startTimeStr = optStat.map(MethodStat::getStartTime)
          .map(timeObj -> timeObj.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)).orElse(NA);

      final String endTimeStr = optStat.map(MethodStat::getEndTime)
          .map(timeObj -> timeObj.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)).orElse(NA);

      final Long elapse = optStat.map(MethodStat::getEndTime).map(endTime -> {
        return optStat.map(MethodStat::getStartTime).map(startTime -> Duration.between(startTime, endTime).toMillis())
            .orElse(-1L);
      }).orElse(-1L);

      sb.append(String.format(METHOD_STAT_LOG_FORMAT, elapse, indent, stat.getResponseOpt().getFlagName(),
          optStat.map(MethodStat::getMethodName).orElse(NA), startTimeStr, endTimeStr));
    }
  }

  /**
   * Format the method name.
   *
   * @param signature the method signature.
   * @return the formatted method name.
   */
  @SuppressWarnings({"rawtypes"})
  protected final String prettyPrintMethodName(final MethodSignature signature) {
    // Class name generation
    final String clazzName = signature.getDeclaringTypeName();
    final String[] clazzNameAry = clazzName.split(DOT_REGEX, -1);

    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < clazzNameAry.length - 1; i++) {
      final String clazzNamePart = clazzNameAry[i];
      sb.append(clazzNamePart.charAt(0)).append(DOT);
    }
    sb.append(clazzNameAry[clazzNameAry.length - 1]);

    // Method generation
    final String methodName = signature.getMethod().getName();
    final List<Class> paramsClassList = Arrays.asList(signature.getMethod().getParameterTypes());
    final List<String> paramsTypeStr = paramsClassList.stream().map(Class::getName).map(className -> {
      return Arrays.asList(className.split(DOT_REGEX, -1)).stream().reduce((a, b) -> b).orElse(className);
    }).collect(Collectors.toList());
    final String params = String.join(COMMA + SPACE, paramsTypeStr);

    sb.append(DOT).append(methodName).append(OPEN_ROUND_BRACKET).append(params).append(CLOSE_ROUND_BRACKET);
    return sb.toString();
  }

}
