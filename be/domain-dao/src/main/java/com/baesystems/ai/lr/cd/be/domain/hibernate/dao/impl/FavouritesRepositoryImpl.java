package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.Favourites;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;

/**
 * @author VMandalapu
 * @author sbollu.
 */
@Repository
public class FavouritesRepositoryImpl implements FavouritesRepository {
  /**
   * Static logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FavouritesRepositoryImpl.class);

  /**
   * Constant for user id field name.
   */
  private static final String PARAM_USER_ID = "userId";

  /**
   * EntityManager Object.
   */
  @PersistenceContext
  private EntityManager entityManager;
  /**
   * UserProfileService Object.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  @Transactional(readOnly = true)
  @Override
  public final List<Favourites> getUserFavourites(final String userId) throws RecordNotFoundException {
    final List<Favourites> favourites = new ArrayList<>();
    final UserProfiles userProfiles = userProfileDao.getUser(userId);

    if (null != userProfiles) {
      favourites.addAll(
          entityManager.createQuery("select fv from Favourites fv where fv.user.userId=:userId", Favourites.class)
              .setParameter(PARAM_USER_ID, userProfiles.getUserId()).getResultList());
    }
    return favourites;
  }

  @Transactional(readOnly = true)
  @Override
  public final Boolean isUserFavourite(final String userId, final Long assetId) {
    Boolean isFavourite;

    LOGGER.debug("Searching for user favourite for user {} and asset {} ", userId, assetId);
    final List<Favourites> favourites = entityManager.createQuery(
        "select fav from Favourites fav where " + "fav.user.userId = :userId and " + "fav.assetId = :assetId",
        Favourites.class).setParameter(PARAM_USER_ID, userId).setParameter("assetId", assetId).getResultList();
    isFavourite = !favourites.isEmpty();

    return isFavourite;
  }

  /**
   * @return value.
   */
  public final EntityManager getEntityManager() {
    return entityManager;
  }

  /**
   * @param entityManagerObject value.
   */
  public final void setEntityManager(final EntityManager entityManagerObject) {
    this.entityManager = entityManagerObject;
  }

  @Transactional
  @Override
  public final void createUserFavourite(@NotNull final String userId, @NotNull final String assetCode)
      throws RecordNotFoundException {
    final long assetId = AssetCodeUtil.getId(assetCode);
    if ((!userId.isEmpty() && assetId != 0) && (verifyFavourite(userId, assetCode) == null)) {
      LOGGER.debug("Creating new user favourite with user id {} and asset id {}", userId, assetId);
      final UserProfiles userProfiles = userProfileDao.getUser(userId);

      final Favourites favourites = new Favourites();
      favourites.setUser(userProfiles);
      favourites.setAssetId(assetId);
      favourites.setSource(AssetCodeUtil.getAssetSource(assetCode));
      entityManager.persist(favourites);
    }
  }

  @Transactional
  @Override
  public final void deleteUserFavourite(@NotNull final String userId, @NotNull final String assetCode) {
    if (!userId.isEmpty() && !assetCode.isEmpty()) {
      LOGGER.debug("Deleting user favourite with user id {} and asset id {}.", userId, assetCode);
      final Favourites favourites = verifyFavourite(userId, assetCode);
      if (null != favourites) {
        entityManager.remove(favourites);
      }
    }
  }

  @Transactional(readOnly = true)
  @Override
  public final Favourites verifyFavourite(final String userId, final String assetCode) {
    Favourites favourites;
    final long assetId = AssetCodeUtil.getId(assetCode);
    final String assetSource = AssetCodeUtil.getAssetSource(assetCode);
    try {
      LOGGER.debug("Verifying favourite with user id {} and asset id {}.", userId, assetId);
      favourites = entityManager
          .createQuery("select fav from Favourites fav where fav.assetId = :assetId and fav.user.userId = :userId "
              + "and fav.source = :assetSource", Favourites.class)
          .setParameter("assetId", assetId).setParameter(PARAM_USER_ID, userId).setParameter("assetSource", assetSource)
          .getSingleResult();
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find any favourite with userId {} and assetId {}.", userId, assetId);
      favourites = null;
    }
    return favourites;
  }

  @Transactional
  @Override
  public final void deleteAllUserFavourites(final String userId) throws RecordNotFoundException {
    LOGGER.debug("Deleting all user favourites with user id {}.", userId);
    final List<Favourites> favourites = getUserFavourites(userId);
    for (Favourites favorite : favourites) {
      entityManager.remove(favorite);
    }
  }

  @Transactional
  @Override
  public void updateUserFavouriteList(final String userId, final FavouritesDto favourites) throws ClassDirectException {
    LOGGER.trace("Create FavouriteList for user id {}.", userId);

    if (favourites.getAdd() != null && !favourites.getAdd().isEmpty()) {
      List<Favourites> userFavourites = getUserFavourites(userId);
      if (userFavourites.isEmpty()) {
        updateFavourite(userId, favourites.getAdd());
      } else {
        List<String> userFavouriteAssetCodes = userFavourites.stream().map(fav -> {
          return fav.getSource().concat(fav.getAssetId().toString());
        }).collect(Collectors.toList());

        favourites.getAdd().removeAll(userFavouriteAssetCodes);

        updateFavourite(userId, favourites.getAdd());
      }
    }

    if (favourites.getRemove() != null && !favourites.getRemove().isEmpty()) {
      for (String assetCode : favourites.getRemove()) {
        deleteUserFavourite(userId, assetCode);
      }
    }
  }


  /**
   * Creates records in FAVOURITES table.
   *
   * @param userId the unique identifier of user.
   * @param addList the list of favorite assets to be inserted in FAVOURITES table.
   * @throws RecordNotFoundException if no user found with given userId.
   */
  private void updateFavourite(final String userId, final List<String> addList) throws RecordNotFoundException {
    final UserProfiles userProfiles = userProfileDao.getUser(userId);
    for (String assetCode : addList) {
      final Favourites favourite = new Favourites();
      favourite.setUser(userProfiles);
      favourite.setAssetId(AssetCodeUtil.getId(assetCode));
      favourite.setSource(AssetCodeUtil.getAssetSource(assetCode));
      entityManager.persist(favourite);

    }
  }
}
