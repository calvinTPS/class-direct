package com.baesystems.ai.lr.cd.be.domain.dao.attachments;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * @author syalavarthi
 *
 */
public interface AttachmentRetrofitService {

  /**
   * Get A list of job attachments.
   *
   * @param jobId jobId from MAST.
   * @param reportId reportId from MAST.
   * @return Call<List<SupplementaryInformationHDto>> return dataType.
   */
  @GET("job/{jobId}/report/{reportId}/attachment")
  Call<List<SupplementaryInformationHDto>> getAttachmentsByJobIdReportId(@Path("jobId") Long jobId,
      @Path("reportId") Long reportId);

  /**
   * Get A list of job attachments.
   *
   * @param jobId jobId.
   * @return List of attachmemts with param jobId.
   */
  @GET("job/{jobId}/attachment")
  Call<List<SupplementaryInformationHDto>> getAttachmentsByJobId(@Path("jobId") long jobId);

  /**
   * Get job attachment.
   *
   * @param jobId jobId from MAST.
   * @param attachmentId attachmentId from MAST.
   * @return Call<SupplementaryInformationHDto> return dataType.
   */
  @GET("job/{jobId}/attachment/{attachmentId}")
  Call<SupplementaryInformationHDto> getAttachmentByJobIdattAchmentId(@Path("jobId") Long jobId,
      @Path("attachmentId") Long attachmentId);

  /**
   * Get attachments by flagId.
   *
   * @param flagId flagId from MAST.
   * @return Call<List<SupplementaryInformationHDto>> return dataType.
   */
  @GET("reference-data/flag/{flagId}/attachment")
  Call<List<SupplementaryInformationHDto>> getAttachmentsByFlagId(@Path("flagId") Long flagId);

  /**
   * Get attachments by asset Id and mncnId.
   *
   * @param assetId the unique identidier for asset.
   * @param mncnId the unique identidier for major non confirmitive note.
   * @return Call<List<SupplementaryInformationHDto>> return dataType.
   */
  @GET("asset/{assetId}/mncn/{mncnId}/attachment")
  Call<List<SupplementaryInformationHDto>> getAttachmentsByAssetIdAndMncnId(@Path("assetId") Long assetId,
     @Path("mncnId") Long mncnId);
}
