/**
 * Package for annotations.
 *
 * @author Faizal Sidek
 *
 */
package com.baesystems.ai.lr.cd.be.domain.annotation;
