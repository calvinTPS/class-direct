package com.baesystems.ai.lr.cd.be.domain.dao.customer;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.customers.PartyHDto;
import com.baesystems.ai.lr.dto.query.PartyQueryDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Customer Retrofit Service to retrieve Client Name.
 */
public interface CustomerRetrofitService {

  /**
   * Retrieve Client Information By Client imo number.
   *
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @param query PartyQueryDto from MAST.
   * @return Call<List<PartyDto>> return List of Party DTOs.
   */
  @POST("customer/query")
  Call<List<PartyHDto>> getClientInformationByClientImo(@Query("page") Integer page, @Query("size") Integer size,
      @Body PartyQueryDto query);
}
