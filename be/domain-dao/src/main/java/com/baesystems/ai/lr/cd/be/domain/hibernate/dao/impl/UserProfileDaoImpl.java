package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import static java.lang.Boolean.TRUE;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserEORDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.TermsConditions;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserRoles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserTermsConditions;
import com.baesystems.ai.lr.cd.be.domain.utils.UserProfileDaoUtils;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.enums.OrderOptionEnum;
import com.baesystems.ai.lr.cd.be.enums.UserSortOptionEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.InactiveAccountException;
import com.baesystems.ai.lr.cd.be.exception.InvalidEmailException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;

/**
 * @author sbollu
 */
@Repository
public class UserProfileDaoImpl implements UserProfileDao {

  /**
   * Log4j logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileDaoImpl.class);

  /**
   * EntityManager Object.
   */
  @PersistenceContext
  private EntityManager entityManager;
  /**
   */
  @Value("${record.not.found.with.id}")
  private String userNotFoundMessage;
  /**
   *
   */
  @Value("${unauthorized.access}")
  private String unauthorizedAccess;
  /**
   *
   */
  @Value("${unauthorized.access.status}")
  private String unauthorizedStatusAccess;
  /**
   */
  @Value("${record.not.found.with.statusId}")
  private String statusNotFoundMessage;
  /**
   */
  @Value("${record.not.found.with.roleId}")
  private String roleNotFoundMessage;
  /**
   *
   */
  @Value("${unauthorized.access.user.account.expiry.date}")
  private String unauthorizedUserAccExpiryDateAccess;

  /**
  *
  */
  @Value("${unauthorized.access.user.Role.Create}")
  private String unauthorizedUserRoleCreation;

  /**
   * message for successful creation.
   */
  @Value("${user.successful.create}")
  private String userSuccessfulCreate;

  /**
   * message for successful creation of EOR.
   */
  @Value("${userEOR.successful.create}")
  private String userEORSuccessfulCreate;

  /**
   * Error message for invalid email.
   */
  @Value("${invalid.email}")
  private String invalidEmailErrorMessage;

  /**
   * email not found error message.
   */
  @Value("${user.email.not.found}")
  private String emailNotFound;

  /**
   * user id not found error message.
   */
  @Value("${user.id.empty}")
  private String userIdEmpty;

  /**
   * Message when a required active user exists but isn't active.
   */
  @Value("${unauthorized.access.account.not.active}")
  private String inactiveAccountErrorMessage;
  /**
   * eor not found error msg..
   */
  @Value("${user.EOR.not.found }")
  private String eorObjectNotFound;

  /**
   * eor not found error msg..
   */
  @Value("${user.EOR.not.found}")
  private String userEorNotFoundMessage;

  /**
   * eor update message.
   */
  @Value("${userEOR.successful.update}")
  private String userEORSuccessfulUpdate;

  /**
   * user TC create message.
   */
  @Value("${userTC.successful.create}")
  private String userTCSuccessfulCreate;

  /**
   * user TC update message.
   */
  @Value("${userTC.successful.update}")
  private String userTCSuccessfulUpdate;

  /**
   * user TC accept date exists message.
   */
  @Value("${userTC.acceptedDate.exists}")
  private String userTCacceptedDateExists;

  /**
   * user TC accept date not null message.
   */
  @Value("${userTC.acceptedDate.null}")
  private String userTCacceptedDateNotNull;

  /**
   * Eor delete success message.
   */
  @Value("${eor.successful.delete}")
  private String successfulDelete;

  /**
   * eor not found.
   */
  @Value("${eor.not.found.with.id}")
  private String eorNotFound;

  /**
   * The user with only EOR role message.
   */
  @Value("${user.only.eor.role}")
  private String userOnlyEorRole;

  /**
   * Exclamation character.
   */
  public static final char PERCENT_SIGN = '%';
  /**
   *
   */
  private static final String CLIENT = "CLIENT";
  /**
  *
  */
  private static final String FLAG = "FLAG";
  /**
  *
  */
  private static final String SHIP_BUILDER = "SHIP_BUILDER";

  /**
   *
   */
  private static final Long VALID_STATUS_THRESHOLD = AccountStatusEnum.ARCHIVED.getId();



  /**
   * Fetch users implementation.
   *
   * @return list of users.
   */
  @Transactional(readOnly = true)
  @Override
  public final List<UserProfiles> getAllUsers() {
    List<UserProfiles> users = null;
    LOGGER.debug("Fetching  all users");
    users = entityManager.createQuery("select up from UserProfiles up ", UserProfiles.class).getResultList();
    users.forEach(user -> {
      initializeUserProperties(user);
    });
    return users;
  }

  /**
   * Fetch users implementation.
   *
   * @return list of users.
   * @throws RecordNotFoundException when no result returned.
   * @throws UnauthorisedAccessException when access is restricted.
   */
  @Transactional(readOnly = true)
  @Override
  public final List<UserProfiles> getUsers(final String requesterId)
      throws RecordNotFoundException, UnauthorisedAccessException {
    List<UserProfiles> users = null;
    LOGGER.debug("Fetching  all users");
    final Roles currentUserRole = loggedInUserRole(requesterId);
    if (currentUserRole != null && currentUserRole.getIslrAdmin()) {
      users = entityManager.createQuery("select up from UserProfiles up ", UserProfiles.class).getResultList();
      users.forEach(user -> {
        initializeUserProperties(user);
      });
    } else {
      throw new UnauthorisedAccessException(unauthorizedAccess);
    }
    return users;
  }

  @Override
  @Transactional
  public final UserProfiles updateUser(final String requesterId, final String editUserId,
      final UserProfiles newProfileValuestoSet, final String fieldsToDelete) throws ClassDirectException {
    LOGGER.debug("Logged in user with user id {}", requesterId);
    LOGGER.debug("Editing user with user id {}", editUserId);

    UserProfiles userProfile = null;
    Optional<UserAccountStatus> editStatusOpt = null;
    Optional<LocalDate> editExpiryDateOpt = null;

    final Roles currentUserRole = loggedInUserRole(requesterId);
    boolean isAdminUser = currentUserRole != null && currentUserRole.getIsAdmin();
    final UserProfiles existingUserProfile = getUser(editUserId);

    if (newProfileValuestoSet != null && existingUserProfile != null) {
      newProfileValuestoSet.setUserId(editUserId);
      editStatusOpt = getStatus(existingUserProfile, newProfileValuestoSet);
      editExpiryDateOpt = getAccountExpiryDate(existingUserProfile, newProfileValuestoSet);
    }

    if (isAdminUser) {
      UserProfileDaoUtils.updateUserProfilesFields(existingUserProfile, newProfileValuestoSet, fieldsToDelete,
          isAdminUser);
      editStatusOpt.ifPresent(status -> newProfileValuestoSet.setDateOfModification(LocalDateTime.now()));
      editExpiryDateOpt.ifPresent(expiryDate -> newProfileValuestoSet.setUserAccExpiryDate(expiryDate));
      userProfile = entityManager.merge(newProfileValuestoSet);
    } else {
      throw new UnauthorisedAccessException(unauthorizedAccess);
    }
    return userProfile;
  }

  /**
   * @param userExist userExist.
   * @param newProfileValuestoSet newProfileValuestoSet.
   * @return status value.
   * @throws ClassDirectException exception.
   */
  private Optional<UserAccountStatus> getStatus(final UserProfiles userExist, final UserProfiles newProfileValuestoSet)
      throws ClassDirectException {
    Optional<Long> existingStatus = Optional.ofNullable(userExist.getStatus()).map(UserAccountStatus::getId);
    // check whether account status changed or not.
    Optional<UserAccountStatus> updatedStatusOpt = Optional.ofNullable(newProfileValuestoSet)
        .map(UserProfiles::getStatus).filter(newStatus -> existingStatus.isPresent()
            && newStatus.getId().longValue() != existingStatus.get().longValue());
    // check whether statusId is valid.
    if (updatedStatusOpt.isPresent()) {
      updatedStatusOpt.map(UserAccountStatus::getId).filter(sid -> sid <= VALID_STATUS_THRESHOLD)
          .orElseThrow(() -> new RecordNotFoundException(statusNotFoundMessage,
              updatedStatusOpt.map(UserAccountStatus::getId).get()));
    }
    return updatedStatusOpt;
  }

  /**
   * @param userExist userExist.
   * @param newProfileValuestoSet newProfileValuestoSet.
   * @return expiryDate.
   */
  private Optional<LocalDate> getAccountExpiryDate(final UserProfiles userExist,
      final UserProfiles newProfileValuestoSet) {

    Optional<LocalDate> existingDate = Optional.ofNullable(userExist).map(UserProfiles::getUserAccExpiryDate);

    Optional<LocalDate> editExpiryDate =
        Optional.ofNullable(newProfileValuestoSet).map(UserProfiles::getUserAccExpiryDate)
            .filter(newdate -> !existingDate.isPresent() || !newdate.isEqual(existingDate.get()));

    return editExpiryDate;
  }



  @Override
  @Transactional(readOnly = true)
  public final UserProfiles getUser(final String userId) throws RecordNotFoundException {
    UserProfiles userProfile = null;
    try {
      LOGGER.debug("Verifying user with user id {}", userId);
      if (!StringUtils.isEmpty(userId)) {
        userProfile =
            entityManager.createQuery("select up from UserProfiles up where up.userId = :userId", UserProfiles.class)
                .setParameter("userId", userId).getSingleResult();

        initializeUserProperties(userProfile);
      } else {
        throw new RecordNotFoundException(userNotFoundMessage, userId);
      }
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Cannot find any user with userId {}.", userId);
      throw new RecordNotFoundException(userNotFoundMessage, userId);
    }
    return userProfile;
  }

  @Override
  @Transactional
  public final UserProfiles getActiveUser(final String userId)
      throws RecordNotFoundException, InactiveAccountException {
    UserProfiles userProfiles;

    LOGGER.debug("Finding active user with user id {} ", userId);
    try {
      userProfiles = entityManager
          .createQuery("select user from UserProfiles user where user.userId = :userId", UserProfiles.class)
          .setParameter("userId", userId).getSingleResult();
      // if the account is not active, we throw another exception
      if (userProfiles.getStatus().getId().longValue() != AccountStatusEnum.ACTIVE.getId()) {
        throw new InactiveAccountException(
            String.format(inactiveAccountErrorMessage, userProfiles.getStatus().getName()));
      }

      initializeUserProperties(userProfiles);
    } catch (NoResultException | NonUniqueResultException exception) {
      throw new RecordNotFoundException("No active user found with user id " + userId);
    }

    return userProfiles;
  }

  @Transactional(readOnly = true)
  @Override
  public final List<UserAccountStatus> fetchAllUserAccStatus() throws ClassDirectException {
    LOGGER.debug("fetch all user account status");
    List<UserAccountStatus> status = null;
    try {
      status =
          entityManager.createQuery("select us from UserAccountStatus us ", UserAccountStatus.class).getResultList();
    } catch (final NoResultException exception) {
      LOGGER.debug("No User Account Status found");
      status = null;
    }
    return status;
  }

  @Override
  @Transactional(readOnly = true)
  public final List<UserProfiles> getUsers(final UserProfileDto query, final Integer page, final Integer size,
      final String sort, final String order) throws ClassDirectException {
    List<UserProfiles> usersList = null;
    try {
      final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      final CriteriaQuery<UserProfiles> criteriaQuery = criteriaBuilder.createQuery(UserProfiles.class);
      final Root<UserProfiles> users = criteriaQuery.from(UserProfiles.class);
      List<Predicate> criteria = new ArrayList<Predicate>();
      criteria = buildCriteriaQuery(criteriaBuilder, users, criteria, query);

      if (criteria.size() == 0) {
        criteriaQuery.select(users);
      } else if (criteria.size() == 1) {
        criteriaQuery.where(criteria.get(0));
      } else {
        criteriaQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
      }

      // Apply distinct and order by
      criteriaQuery.distinct(true);

      Order userprofileSortOrder;
      Path<UserProfiles> sortingOrder = users.get(UserSortOptionEnum.Email.getField());
      if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
        sortingOrder = users.get(UserSortOptionEnum.valueOf(sort).getField());
        if (OrderOptionEnum.valueOf(order).equals(OrderOptionEnum.ASC)) {
          userprofileSortOrder = criteriaBuilder.asc(sortingOrder);
        } else {
          userprofileSortOrder = criteriaBuilder.desc(sortingOrder);
        }
      } else {
        userprofileSortOrder = criteriaBuilder.asc(sortingOrder);
      }
      criteriaQuery.orderBy(userprofileSortOrder);

      TypedQuery<UserProfiles> tQuery = entityManager.createQuery(criteriaQuery);
      tQuery = setParametersToTypedQuery(tQuery, query);
      if (page != null && size != null) {
        if (page <= 0 || size <= 0) {
          throw new ClassDirectException("Page and size must not be smaller than one.");
        } else {
          tQuery.setFirstResult((page - 1) * size);
          tQuery.setMaxResults(size);
        }
      }

      usersList = tQuery.getResultList();
      usersList.forEach(user -> {
        initializeUserProperties(user);
      });
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Error while fetching userProfiles-getUsers post/query.");
      throw new ClassDirectException(resultException);
    }
    return usersList;
  }

  @Override
  @Transactional(readOnly = true)
  public final UserProfiles getRequestedUser(final String requestedId) throws RecordNotFoundException {
    return getUser(requestedId);
  }

  /**
   * @return value.
   */
  public final EntityManager getEntityManager() {
    return entityManager;
  }

  /**
   * @param entityManagerObject value.
   */

  public final void setEntityManager(final EntityManager entityManagerObject) {
    this.entityManager = entityManagerObject;
  }

  @Override
  @Transactional
  public final int disableExpiredUsers() {
    return this.entityManager
        .createQuery("update UserProfiles user set user.status.id = :new_status "
            + "where user.userAccExpiryDate < :current_date and user.status.id not in (:old_status) ")
        .setParameter("current_date", LocalDate.now()).setParameter("new_status", AccountStatusEnum.DISABLED.getId())
        .setParameter("old_status",
            Arrays.asList(new Long[] {AccountStatusEnum.DISABLED.getId(), AccountStatusEnum.ARCHIVED.getId()}))
        .executeUpdate();
  }

  @Transactional(rollbackFor = {RuntimeException.class, ClassDirectException.class})
  @Override
  public final StatusDto recreateArchivedUser(final UserProfiles archivedUser, final String requesterId,
      final CreateNewUserDto newprofile) throws ClassDirectException {

    final String userId = archivedUser.getUserId();
    LOGGER.debug("Delete Archived User id : {}.", userId);
    // Check if this is an existing user. If yes, check if the existing user is archived user.
    // Clean-up existing archived user before creating new record for the same user.
    LOGGER.debug("Removing database reference/information for archived user - user id : {}, status : {}.", userId,
        AccountStatusEnum.ARCHIVED.getName());

    final EntityManager entityMgr = this.entityManager;
    // Remove user's reference/information from database to allow new record creation.
    entityMgr.createQuery("delete from Favourites favour where favour.user.userId = :user_id")
        .setParameter("user_id", userId).executeUpdate();

    entityMgr.createQuery("delete from SubFleet subfleet where subfleet.user.userId = :user_id")
        .setParameter("user_id", userId).executeUpdate();

    entityMgr.createQuery("delete from UserEOR userEor where userEor.userId = :user_id").setParameter("user_id", userId)
        .executeUpdate();

    entityMgr.createQuery("delete from UserTermsConditions userTc where userTc.userId= :user_id ")
        .setParameter("user_id", userId).executeUpdate();

    entityMgr.createQuery("delete from Notifications notifications where notifications.userId= :user_id ")
        .setParameter("user_id", userId).executeUpdate();

    UserProfiles existingUser = entityMgr.find(archivedUser.getClass(), archivedUser.getUserId());
    entityMgr.remove(existingUser);

    return createUser(requesterId, newprofile);
  }

  @Transactional
  @Override
  public final StatusDto createUser(final String requesterId, final CreateNewUserDto newprofile)
      throws ClassDirectException {

    StatusDto status = null;

    // validate userId.
    if (newprofile == null || StringUtils.isEmpty(newprofile.getUserId())) {
      throw new ClassDirectException(userIdEmpty);
    }

    final Roles currentUserRole = loggedInUserRole(requesterId);
    if (Optional.ofNullable(currentUserRole).filter(isAdmin -> TRUE.equals(currentUserRole.getIsAdmin())).isPresent()) {

      final boolean lradminRole = Optional.ofNullable(newprofile.getRole())
          .filter(userRole -> Role.LR_ADMIN.toString().equalsIgnoreCase(userRole)).isPresent();

      if (TRUE.equals(currentUserRole.getIslrAdmin()) && lradminRole) {
        throw new UnauthorisedAccessException(String.format(unauthorizedUserRoleCreation, newprofile.getRole()));
      }

      // validate email
      if (StringUtils.isEmpty(newprofile.getEmail())) {
        throw new InvalidEmailException(emailNotFound, newprofile.getEmail());
      } else {
        if (!validateEmail(newprofile.getEmail())) {
          throw new InvalidEmailException(invalidEmailErrorMessage, newprofile.getEmail());
        }
      }

      final UserProfiles newProfiletoCreate = new UserProfiles();
      newProfiletoCreate.setUserId(newprofile.getUserId());
      newProfiletoCreate.setEmail(newprofile.getEmail());
      final Set<Roles> userRoles = new HashSet<>();
      final Roles role = new Roles();
      role.setRoleId(Role.valueOf(newprofile.getRole()).getId());
      userRoles.add(role);
      newProfiletoCreate.setRoles(userRoles);

      if (newprofile.getAccountExpiry() != null) {
        newProfiletoCreate.setUserAccExpiryDate(newprofile.getAccountExpiry());
      }
      if (newprofile.getShipBuilderCode() != null) {
        newProfiletoCreate.setShipBuilderCode(newprofile.getShipBuilderCode());
      }

      if (newprofile.getClientCode() != null) {
        newProfiletoCreate.setClientCode(newprofile.getClientCode());
        if (newprofile.getClientType() != null) {
          newProfiletoCreate.setClientType(newprofile.getClientType());
        }
      }

      if (!StringUtils.isEmpty(newprofile.getFlagCode())) {
        newProfiletoCreate.setFlagCode(newprofile.getFlagCode());
      }

      newProfiletoCreate.setRestrictAll(false);

      // Set account status to Active.
      final UserAccountStatus userStatus = new UserAccountStatus();
      userStatus.setId(AccountStatusEnum.ACTIVE.getId());
      newProfiletoCreate.setStatus(userStatus);
      entityManager.persist(newProfiletoCreate);

      if (newprofile.getEor() != null && newprofile.getEor().size() > 0) {
        for (UserEORDto eor : newprofile.getEor()) {
          // In the event that error occurs when creating new EOR records for user,
          // ClassDirectException will throw. Thus, terminating new user creation.
          // No status need to be retrieved as it will be overridden for each passing EOR records.
          createEORRecord(eor, newProfiletoCreate.getUserId());
        }
      }

      status = new StatusDto(userSuccessfulCreate, newprofile.getEmail());
    }
    return status;
  }

  @Transactional(readOnly = true)
  @Override
  public final UserProfiles verifyUserByEmail(final String email) {
    UserProfiles userProfile = null;
    try {
      LOGGER.debug("Verifying User with email {} and Status.", email);
      List<Long> statusCodes = new ArrayList<Long>();
      statusCodes.add(AccountStatusEnum.ACTIVE.getId());
      userProfile = entityManager
          .createQuery("select user from UserProfiles user where user.status.id in :statusId and user.email = :email",
              UserProfiles.class)
          .setParameter("statusId", statusCodes).setParameter("email", email).getSingleResult();
      initializeUserProperties(userProfile);
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find any User with email {}.", email);
    }
    return userProfile;
  }


  /**
   * Method to get role of LoggedinUser.
   *
   * @param requesterId requesterId.
   * @return roles of user.
   * @throws RecordNotFoundException when record not found.
   */
  private Roles loggedInUserRole(final String requesterId) throws RecordNotFoundException {
    Roles loggedInUserRole = null;
    LOGGER.debug("Fetching  all users");
    final UserProfiles currentUser = getUser(requesterId);
    if (!currentUser.getRoles().isEmpty()) {
      loggedInUserRole = UserProfileDaoUtils.setUserRoleField(currentUser.getRoles());
    }
    return loggedInUserRole;
  }

  /**
   * Method to insert UserEOR Records.
   *
   * @param eor to create UserEOR record.
   * @param userId userId.
   * @return value.
   * @throws ClassDirectException when exception occurs.
   */
  @Transactional
  private StatusDto createEORRecord(final UserEORDto eor, final String userId) throws ClassDirectException {
    StatusDto status = new StatusDto();
    try {
      LOGGER.debug("Creating new EOR Record with userId {}", userId);
      if (verifyEORRole(userId) == null) {
        createUserRoleEOR(userId);
      }

      final UserEOR newEORtoCreate = new UserEOR();
      newEORtoCreate.setUserId(userId);
      newEORtoCreate.setImoNumber(eor.getImoNumber());

      if (eor.getAssetExpiry() != null) {
        newEORtoCreate.setAssetExpiryDate(eor.getAssetExpiry());
      }
      if (eor.getTmReport() != null) {
        newEORtoCreate.setAccessTmReport(eor.getTmReport());
      }

      entityManager.persist(newEORtoCreate);
      status.setMessage(userEORSuccessfulCreate);
      status.setStatus("200");

    } catch (EntityExistsException | IllegalArgumentException exception) {
      LOGGER.debug("error while creating UserRoles or UserEOR with userId {}", userId);
      status.setStatus(null);
      throw new ClassDirectException(exception);
    }
    return status;
  }

  /**
   * Method to create EOR role in userRoles table.
   *
   * @param userId userId.
   * @return value
   */
  private StatusDto createUserRoleEOR(final String userId) {
    final StatusDto status = new StatusDto();
    UserProfiles user = new UserProfiles();
    user.setUserId(userId);

    final UserRoles eorRole = new UserRoles();
    eorRole.setUser(user);
    Roles usrrole = new Roles();
    usrrole.setRoleId(Role.EOR.getId());
    eorRole.setRole(usrrole);
    entityManager.persist(eorRole);
    status.setMessage(userEORSuccessfulCreate);
    status.setStatus("200");

    return status;
  }

  /**
   * Method to get verify user has EOR role or not.
   *
   * @param userId userId.
   * @return userRoles.
   */
  @Transactional(readOnly = true)
  private UserRoles verifyEORRole(final String userId) {
    UserRoles userRole = null;
    try {
      LOGGER.debug("Verifying UserRole with userId {} and RoleName.", userId);
      userRole = entityManager
          .createQuery("select usrRole from UserRoles usrRole where "
              + "usrRole.user.userId = :userId and usrRole.role.roleName = :roleName", UserRoles.class)
          .setParameter("userId", userId).setParameter("roleName", Role.EOR.toString()).getSingleResult();
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find any User with userId {} and RoleName {}.", userId, Role.EOR.toString());
    }
    return userRole;
  }

  /**
   * Method to validate email.
   *
   * @param email email.
   * @return value.
   */
  public final boolean validateEmail(final String email) {
    return email.matches(CDConstants.EMAIL_PATTERN);
  }

  @Override
  @Transactional(readOnly = true)
  public final UserEOR getEOR(final String userId, final String imonum) throws RecordNotFoundException {
    UserEOR userEOR = null;
    try {
      LOGGER.debug("Verifying EOR with user id {} and imo number {}", userId, imonum);
      if (!StringUtils.isEmpty(userId) && !StringUtils.isEmpty(imonum)) {
        userEOR = entityManager
            .createQuery("select usrEor from UserEOR usrEor where "
                + "usrEor.userId = :userId and usrEor.imoNumber = :imoNumber", UserEOR.class)
            .setParameter("userId", userId).setParameter("imoNumber", imonum).getSingleResult();
      } else {
        throw new RecordNotFoundException(userEorNotFoundMessage, userId, imonum);
      }
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find any userEOR with userId {} and imo number {}.", userId, imonum);
    }
    return userEOR;
  }

  @Transactional
  @Override
  public final StatusDto updateEORs(final String requesterId, final String userIdtoeditEor,
      final CreateNewUserDto newEorstoSet) throws ClassDirectException {
    StatusDto status = new StatusDto();

    Roles currentUserRole = loggedInUserRole(requesterId);
    final Boolean admin = currentUserRole != null && currentUserRole.getIsAdmin();
    if (admin) {
      if (newEorstoSet.getAccountExpiry() != null) {
        UserProfiles profiletoUpdate = new UserProfiles();
        profiletoUpdate.setUserAccExpiryDate(newEorstoSet.getAccountExpiry());
        updateUser(requesterId, userIdtoeditEor, profiletoUpdate, null);
      }
      if (newEorstoSet.getEor() != null && newEorstoSet.getEor().size() > 0) {
        for (UserEORDto eor : newEorstoSet.getEor()) {
          UserEOR usrEOR = getEOR(userIdtoeditEor, eor.getImoNumber());
          if (usrEOR == null) {
            status = createEORRecord(eor, userIdtoeditEor);
          } else {
            if (eor.getAssetExpiry() != null) {
              usrEOR.setAssetExpiryDate(eor.getAssetExpiry());
            }
            if (eor.getTmReport() != null) {
              usrEOR.setAccessTmReport(eor.getTmReport());
            }
            entityManager.merge(usrEOR);
            status.setMessage(userEORSuccessfulUpdate);
            status.setStatus("200");
          }
        }
      } else {
        LOGGER.debug("Can't find any userEOR with imo number {}.", userIdtoeditEor);
        throw new IllegalArgumentException(eorObjectNotFound);
      }
    } else {
      throw new UnauthorisedAccessException(unauthorizedAccess);
    }
    return status;
  }

  @Transactional
  @Override
  public final StatusDto removeEOR(final String eorUserId, final String eorIMONumber) throws ClassDirectException {
    StatusDto status = new StatusDto(successfulDelete);

    if (!StringUtils.isEmpty(eorUserId) && !StringUtils.isEmpty(eorIMONumber)) {
      LOGGER.debug("Deleting user Eor with user id {} and imo number {}.", eorUserId, eorIMONumber);
      final UserEOR eor = getEOR(eorUserId, eorIMONumber);
      if (eor != null) {
        try {
          entityManager.remove(eor);
          entityManager.flush();
          LOGGER.debug("user EOR removed imo number {} and user id {} ", eorIMONumber, eorUserId);
        } catch (final IllegalArgumentException exception) {
          LOGGER.error("Error while delete EOR", exception);
          throw new ClassDirectException(exception);
        }
      } else {
        throw new RecordNotFoundException(eorNotFound, eorUserId, eorIMONumber);
      }
    } else {
      throw new RecordNotFoundException(eorNotFound, eorUserId, eorIMONumber);
    }
    return status;
  }

  @Transactional
  @Override
  public final void deleteUserEORRole(final UserProfiles eorUser) throws ClassDirectException {
    // check whether user is only eor user or eor with any other role
    final List<String> roles = new ArrayList<>();
    Optional.ofNullable(eorUser.getRoles()).ifPresent(userRoles -> {
      roles.addAll(eorUser.getRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList()));
    });
    boolean isEorRole = SecurityUtils.isEORUser(roles);
    boolean isOnlyEOrRole = isEorRole && eorUser.getRoles().size() == 1;

    // When all Eor's assets removed, user EOR role must removed and user is eor and other
    // roles.
    if (!isOnlyEOrRole && (eorUser.getEors() == null || eorUser.getEors().isEmpty())) {
      LOGGER.debug("user with only EOR role and other roles", eorUser.getUserId());
      UserRoles eorRole = verifyEORRole(eorUser.getUserId());
      if (eorRole != null) {
        entityManager.remove(eorRole);
        entityManager.flush();
        LOGGER.debug("User {} with eor and other roles, user eor role deleted.", eorUser.getUserId());
      }
    }
  }

  /**
   * Method to initialize user.
   *
   * @param userProfile userprofile.
   */
  private void initializeUserProperties(final UserProfiles userProfile) {
    // User is definitely a CD user at this stage.
    userProfile.setCdUser(true);

    if (!userProfile.getRoles().isEmpty()) {
      Hibernate.initialize(userProfile.getRoles());
    }
    if (!userProfile.getNotifications().isEmpty()) {
      Hibernate.initialize(userProfile.getNotifications());
    }
    if (!userProfile.getEors().isEmpty()) {
      Hibernate.initialize(userProfile.getEors());
    }
    if (!userProfile.getUserTCs().isEmpty()) {
      Hibernate.initialize(userProfile.getUserTCs());
    }
  }

  @Override
  @Transactional
  public final List<UserEOR> getExpiredEorUsers() throws ClassDirectException {
    try {
      LOGGER.info("Getting all expired eor users.");
      return Optional.ofNullable(this.entityManager
          .createQuery("select distinct eor_user from UserEOR eor_user where eor_user.assetExpiryDate < :current_date")
          .setParameter("current_date", LocalDate.now()).getResultList()).orElse(Collections.emptyList());
    } catch (Exception exception) {
      LOGGER.error("error in getting all expired eor users.");
      throw new ClassDirectException(exception);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public final TermsConditions getLatestTermsConditions() throws ClassDirectException {
    TermsConditions tc = null;
    try {
      LOGGER.debug("Get latest Terms and Conditions");
      tc = entityManager.createQuery(
          "select tcs from TermsConditions tcs where tcs.tcDate = (select max(tcs.tcDate) from TermsConditions tcs)",
          TermsConditions.class).getSingleResult();
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find latest Terms and Conditions");
      throw new ClassDirectException(resultException);
    }
    return tc;
  }


  /**
   * Method to get user terms and conditions.
   *
   * @param userId userId.
   * @param tcId tcId.
   * @return UserTermsConditions.
   * @throws ClassDirectException when exception happens.
   */
  @Transactional(readOnly = true)
  private UserTermsConditions getTermsConditionsByUserId(final String userId, final Long tcId)
      throws ClassDirectException {
    UserTermsConditions userTC = null;
    try {
      LOGGER.debug("Verifying User TermsConditions with user id {} and tcId {}", userId, tcId);
      userTC =
          entityManager
              .createQuery("select tcs from UserTermsConditions tcs where tcs.userId = :userId and tcs.tcId = :tcId)",
                  UserTermsConditions.class)
              .setParameter("userId", userId).setParameter("tcId", tcId).getSingleResult();
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find any userEOR with userId {} and tcId {}.", userId, tcId);
    }
    return userTC;
  }

  @Override
  @Transactional
  public final StatusDto createAcceptedTermsandConditions(final String userId, final Long tcId)
      throws ClassDirectException {
    StatusDto status = new StatusDto();
    try {
      LOGGER.debug("createAcceptedTermsandConditions()");
      if (!StringUtils.isEmpty(userId) && tcId != null) {
        final UserProfiles userExist = getUser(userId);
        if (userExist != null) {
          final UserTermsConditions userTCExists = getTermsConditionsByUserId(userId, tcId);
          if (userTCExists == null) {
            LOGGER.debug("Create User TermsConditions with user id {} and tcId {}", userId, tcId);
            final UserTermsConditions newUserTCtoCreate = new UserTermsConditions();
            newUserTCtoCreate.setUserId(userId);
            newUserTCtoCreate.setTcId(tcId);
            newUserTCtoCreate.setAcceptedDate(LocalDateTime.now());

            entityManager.persist(newUserTCtoCreate);
            status.setMessage(userTCSuccessfulCreate);
            status.setStatus("200");

          } else {
            if (userTCExists.getAcceptedDate() == null) {
              LOGGER.debug("Update User TermsConditions with user id {} and tcId {}", userId, tcId);
              userTCExists.setAcceptedDate(LocalDateTime.now());
              entityManager.merge(userTCExists);
              status.setMessage(userTCSuccessfulUpdate);
              status.setStatus("200");

            } else {
              status.setMessage(userTCacceptedDateExists);
            }
          }
        }
      }
    } catch (EntityExistsException | IllegalArgumentException exception) {
      LOGGER.error("error while creating User TermsConditions with user id {} and tcId {}", userId, tcId);
      status.setStatus(null);
      throw new ClassDirectException(exception);
    }
    return status;
  }

  @Override
  @Transactional(readOnly = true)
  public final Set<String> getEorsByUserId(final String userId) throws ClassDirectException {
    Set<String> eorIMOs = new HashSet<String>();
    try {
      if (!StringUtils.isEmpty(userId)) {
        LOGGER.debug("Get eor IMOs with userId {}.", userId);
        List<UserEOR> eors =
            entityManager.createQuery("select usrEor from UserEOR usrEor where usrEor.userId = :userId", UserEOR.class)
                .setParameter("userId", userId).getResultList();
        if (!eors.isEmpty() && eors.size() > 0) {
          eorIMOs.addAll(eors.parallelStream().map(eor -> eor.getImoNumber()).collect(Collectors.toSet()));
        }
      } else {
        throw new RecordNotFoundException(userNotFoundMessage, userId);
      }

    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find any EorIMOs with userId {}.", userId);
      throw new ClassDirectException(resultException);
    }
    return eorIMOs;
  }

  @Override
  public final Long getUsersCount(final UserProfileDto query) throws ClassDirectException {
    Long usersCount = null;
    try {
      final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      List<Predicate> criteria = new ArrayList<Predicate>();

      CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
      final Root<UserProfiles> users = criteriaQuery.from(UserProfiles.class);

      criteria = buildCriteriaQuery(criteriaBuilder, users, criteria, query);

      criteriaQuery.select(criteriaBuilder.countDistinct(users));
      criteriaQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
      TypedQuery<Long> tQuery = entityManager.createQuery(criteriaQuery);
      tQuery = setParametersToTypedQuery(tQuery, query);
      usersCount = tQuery.getSingleResult();
      LOGGER.info("User count : {}", usersCount);

    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Error while fetching userProfiles-getUsers post/query.");
      throw new ClassDirectException(resultException);
    }
    return usersCount;
  }

  /**
   * add search conditions to criteria.
   *
   * @param <T> domainclass
   * @param criteriaBuilder criteriaBuilder.
   * @param users users.
   * @param criteria criteria.
   * @param query query.
   * @return value.
   * @throws ClassDirectException exception.
   */
  private <T> List<Predicate> buildCriteriaQuery(final CriteriaBuilder criteriaBuilder, final Root<T> users,
      final List<Predicate> criteria, final UserProfileDto query) throws ClassDirectException {
    if (query != null) {
      if (!StringUtils.isEmpty(query.getUserId())) {
        final ParameterExpression<String> p = criteriaBuilder.parameter(String.class, "userId");
        criteria.add(criteriaBuilder.like(users.get("userId"), p));
      }

      if (query.getStatus() != null && !query.getStatus().isEmpty()) {
        criteria.add(criteriaBuilder.and(users.join("status").get("name").as(String.class).in(query.getStatus())));
      }

      if (query.getRoles() != null && !query.getRoles().isEmpty()) {
        criteria.add(criteriaBuilder.and(users.join("roles").get("roleName").in(query.getRoles())));
      }

      // codeSearch based on client code, flag and ship builder codes.
      if (query.getCodeSearch() != null && !StringUtils.isEmpty(query.getCodeSearch().getType())
          && !StringUtils.isEmpty(query.getCodeSearch().getCode())) {
        if (query.getCodeSearch().getType().equals(CLIENT)) {
          criteria.add(criteriaBuilder.like(users.get("clientCode").as(String.class),
              "%" + query.getCodeSearch().getCode() + "%"));
        } else if (query.getCodeSearch().getType().equals(FLAG)) {
          criteria.add(criteriaBuilder.like(users.get("flagCode").as(String.class),
              "%" + query.getCodeSearch().getCode() + "%"));
        } else if (query.getCodeSearch().getType().equals(SHIP_BUILDER)) {
          criteria.add(criteriaBuilder.like(users.get("shipBuilderCode").as(String.class),
              "%" + query.getCodeSearch().getCode() + "%"));
        }
      }

      // filter by date of last login.
      // converting input string having date to LocalDateTime format and getting records.
      if (query.getDateOfLastLoginMin() != null) {
        criteria.add(criteriaBuilder.greaterThanOrEqualTo(users.<LocalDateTime>get("lastLogin"), LocalDateTime
            .of(LocalDate.parse(query.getDateOfLastLoginMin(), DateTimeFormatter.ISO_LOCAL_DATE), LocalTime.MIDNIGHT)));
      }

      if (query.getDateOfLastLoginMax() != null) {
        criteria.add(criteriaBuilder.lessThan(users.<LocalDateTime>get("lastLogin"), LocalDateTime
            .of(LocalDate.parse(query.getDateOfLastLoginMax(), DateTimeFormatter.ISO_LOCAL_DATE), LocalTime.MIDNIGHT)
            .plusDays(1)));
      }

      // first priority emailId if present search with email in db or else check with included
      // uses list or else check in
      // excluded users list.
      if (!StringUtils.isEmpty(query.getEmail())) {
        final ParameterExpression<String> r = criteriaBuilder.parameter(String.class, "email");
        criteria.add(criteriaBuilder.like(users.get("email"), r));
      } else if (query.getIncludedUsers() != null && !query.getIncludedUsers().isEmpty()) {
        criteria.add(criteriaBuilder.and(users.get("email").in(query.getIncludedUsers())));
      } else if (query.getExcludedUsers() != null && !query.getExcludedUsers().isEmpty()) {
        criteria.add(criteriaBuilder.not(users.get("email").in(query.getExcludedUsers())));
      }

      // filter by name of user.
      if (!StringUtils.isEmpty(query.getName())) {
        final ParameterExpression<String> r = criteriaBuilder.parameter(String.class, "name");
        criteria.add(criteriaBuilder.like(users.get("name"), r));
      }

      // filter by company name of user.
      if (!StringUtils.isEmpty(query.getCompanyName())) {
        final ParameterExpression<String> r = criteriaBuilder.parameter(String.class, "companyName");
        criteria.add(criteriaBuilder.like(users.get("companyName"), r));
      }

    }
    return criteria;
  }

  /**
   * set parameters to typed query.
   *
   * @param <T> domainclass.
   * @param tQuery typedQuery.
   * @param query query.
   * @return value.
   */
  private <T> TypedQuery<T> setParametersToTypedQuery(final TypedQuery<T> tQuery, final UserProfileDto query) {
    if (query != null) {

      if (!StringUtils.isEmpty(query.getUserId())) {
        tQuery.setParameter("userId", PERCENT_SIGN + query.getUserId() + PERCENT_SIGN);
      }

      if (!StringUtils.isEmpty(query.getEmail())) {
        final String formattedEmail = UserProfileDaoUtils.getFormattedStringForSql(query.getEmail());
        LOGGER.debug("Formatted user's email input parameter : {}", formattedEmail);
        tQuery.setParameter("email", PERCENT_SIGN + formattedEmail + PERCENT_SIGN);
      }

      if (!StringUtils.isEmpty(query.getName())) {
        final String formattedFullName = UserProfileDaoUtils.getFormattedStringForSql(query.getName());
        LOGGER.debug("Formatted user's full name input parameter : {}", formattedFullName);
        tQuery.setParameter("name", PERCENT_SIGN + formattedFullName + PERCENT_SIGN);
      }

      if (!StringUtils.isEmpty(query.getCompanyName())) {
        final String formattedCompanyName = UserProfileDaoUtils.getFormattedStringForSql(query.getCompanyName());
        LOGGER.debug("Formatted company name input parameter : {}", formattedCompanyName);
        tQuery.setParameter("companyName", PERCENT_SIGN + formattedCompanyName + PERCENT_SIGN);
      }
    }
    return tQuery;
  }

  @Override
  @Transactional
  public final int updateUserFullNameAndCompanyName(final UserProfiles newUserprofile) throws ClassDirectException {
    return this.entityManager
        .createQuery("update UserProfiles user set user.companyName = :new_company_name, user.name = :new_full_name "
            + "where user.userId = :userId ")
        .setParameter("new_company_name", Optional.ofNullable(newUserprofile.getCompanyName()).orElse(""))
        .setParameter("new_full_name", Optional.ofNullable(newUserprofile.getName()).orElse(""))
        .setParameter("userId", newUserprofile.getUserId()).executeUpdate();
  }

  @Override
  @Transactional
  public final int updateUserLastLogin(final String editUserId, final LocalDateTime lastLogin)
      throws ClassDirectException {
    return this.entityManager
        .createQuery("update UserProfiles user set user.lastLogin = :last_login where user.userId = :editUserId ")
        .setParameter("last_login", lastLogin).setParameter("editUserId", editUserId).executeUpdate();
  }

}
