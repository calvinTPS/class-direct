package com.baesystems.ai.lr.cd.be.domain.dao;

import com.baesystems.ai.lr.cd.be.domain.dao.lrid.InternalUserRetrofitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import retrofit2.Retrofit;

/**
 * @author VKolagutla
 * @author fwijaya
 *
 */
public class LRADRetrofitServiceFactory {

  /**
   * Retrofit builder.
   */
  @Autowired
  @Qualifier("lrADRetrofit")
  private Retrofit lrADRetrofit;

  /**
   * create new InternalUserRetrofitService.
   *
   * @return internalUserRetrofitService.
   */
  public final InternalUserRetrofitService createInternalUserRetrofitService() {
    return lrADRetrofit.create(InternalUserRetrofitService.class);
  }

}
