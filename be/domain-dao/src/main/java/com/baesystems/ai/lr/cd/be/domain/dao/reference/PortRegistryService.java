package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import java.util.List;

import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * @author vkovuru
 *
 */
public interface PortRegistryService {
  /**
   * @return Call<PortOfRegistryPageResourceDto>
   */
  @GET("port-of-registry")
  Call<List<PortOfRegistryDto>> getPortOfRegistryDtos();

   /**
   * @param portId portid
   * @return Call<PortOfRegistryDto>
   */
  @GET("port-of-registry/{id}")
  Call<PortOfRegistryDto> getPort(@Path("id") long portId);
}
