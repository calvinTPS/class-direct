package com.baesystems.ai.lr.cd.be.domain.retrofit.propertysetter;

import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;

import java.io.IOException;
import java.util.Locale;

/**
 * @author fwijaya on 11/5/2017.
 */
public class SignInNamePropertySetter extends BasePropertySetter {
  /**
   * Serial version unique ID.
   */
  private static final long serialVersionUID = 2941890601153727067L;
  /**
   * Json field name.
   */
  private static final String JSON_FIELD = "userPrincipalName";
  /**
   * LR company name.
   */
  private static final String COMPANY_NAME = "Lloyd's Register";

  /**
   * Constructs settable property object.
   *
   * @param methodPropertyArg the default settable property object.
   */
  public SignInNamePropertySetter(final SettableBeanProperty methodPropertyArg) {
    super(methodPropertyArg, JSON_FIELD);
  }

  /**
   * Parses the json and sets {@link LRIDUserDto#signInName} from userPrincipalName field of the json.
   *
   * @param parser json parser.
   * @param ctxt context.
   * @param instance {@link LRIDUserDto} instance;
   * @throws IOException
   */
  @Override
  public final void deserializeAndSet(final JsonParser parser, final DeserializationContext ctxt,
                                      final Object instance) throws IOException {
    final LRIDUserDto dto = (LRIDUserDto) instance;
    dto.setInternalUser(true);
    dto.setRcaOrganisationName(COMPANY_NAME);
    final JsonToken token = parser.getCurrentToken();
    if (token.equals(JsonToken.VALUE_STRING)) {
      final String signInName = parser.getValueAsString();
      if (signInName != null) {
        dto.setSignInName(signInName.toLowerCase(Locale.ENGLISH));
        dto.setUserPrincipalName(signInName.toLowerCase(Locale.ENGLISH));
      }
    }
  }
}
