package com.baesystems.ai.lr.cd.be.domain.retrofit.propertysetter;

import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;

import java.io.IOException;

/**
 * @author fwijaya on 11/5/2017.
 */
public class ObjectIdPropertySetter extends BasePropertySetter {
  /**
   * Serial version unique id.
   */
  private static final long serialVersionUID = 3520879789528515586L;
  /**
   * Json field name.
   */
  private static final String JSON_FIELD = "id";

  /**
   * Constructs settable property object.
   *
   * @param methodPropertyArg the default settable property object.
   */
  public ObjectIdPropertySetter(final SettableBeanProperty methodPropertyArg) {
    super(methodPropertyArg, JSON_FIELD);
  }

  /**
   * Parses the json and sets {@link LRIDUserDto#objectId} from id field of the json.
   *
   * @param parser json parser.
   * @param ctxt context.
   * @param instance {@link LRIDUserDto} instance;
   * @throws IOException
   */
  @Override
  public final void deserializeAndSet(final JsonParser parser, final DeserializationContext ctxt,
                                      final Object instance) throws IOException {
    final LRIDUserDto dto = (LRIDUserDto) instance;
    final JsonToken token = parser.getCurrentToken();
    if (token.equals(JsonToken.VALUE_STRING)) {
      final String objectId = parser.getValueAsString();
      dto.setObjectId(objectId);
    }
  }
}
