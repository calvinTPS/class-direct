package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Reference Data response interceptor.
 *
 * @author msidek
 *
 */
public class ReferenceDataResponseInterceptor implements Interceptor {

  @Override
  public final Response intercept(final Chain chain) throws IOException {
    final Response response = chain.proceed(chain.request());
    final MediaType contentType = response.body().contentType();
    final String jsonResponse = response.body().string();
    Response.Builder builder = null;

    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode node = mapper.readTree(jsonResponse).get("referenceData");
    final JsonNode value = Optional.ofNullable(node).map(JsonNode::elements).map(Iterator::next).map(JsonNode::elements)
        .map(Iterator::next).orElse(null);
    if (value != null) {
      builder = response.newBuilder().body(ResponseBody.create(contentType, value.toString()));
    } else {
      builder = response.newBuilder().body(ResponseBody.create(contentType, jsonResponse));
    }

    return builder.build();
  }

}
