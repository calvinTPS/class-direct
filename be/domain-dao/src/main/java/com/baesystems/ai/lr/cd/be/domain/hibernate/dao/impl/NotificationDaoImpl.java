package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.NotificationsDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.NotificationTypes;
import com.baesystems.ai.lr.cd.be.domain.repositories.Notifications;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author RKaneysan.
 */
@Repository
public class NotificationDaoImpl implements NotificationsDao {
  /**
   * Log4j logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationDaoImpl.class);
  /**
   * Persistence Context Object called Entity Manager.
   */
  @PersistenceContext
  private EntityManager entityManager;
  /**
   * Autowired UserProfileDao.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  /**
   */
  @Value("${notifications.types.not.found}")
  private String notificationTypesNotFoundMessage;

  /**
   * notification successful update message.
   */
  @Value("${notification.successful.create}")
  private String notificationSuccessfulCreate;

  /**
   * notification message.
   */
  @Value("${notification.exists}")
  private String notificationExistsMessage;

  /**
   * notification message.
   */
  @Value("${notification.successful.delete}")
  private String notificationSuccessfulDelete;

  /**
   * notification message.
   */
  @Value("${notification.not.found}")
  private String notificationNotFound;

  @Override
  @Transactional(readOnly = true)
  public final List<Notifications> getSubscribedUsers(final List<NotificationTypesEnum> typeIds) {
    final List<Notifications> notifications = entityManager
        .createQuery("select notfn from Notifications notfn where notfn.typeId in (:typeId) ", Notifications.class)
        .setParameter("typeId",
            typeIds.stream().collect(Collectors.mapping(NotificationTypesEnum::getId, Collectors.toList())))
        .getResultList();

    if (notifications != null && !notifications.isEmpty()) {
      final List<String> userIds = notifications.stream().map(Notifications::getUserId).collect(Collectors.toList());
      LOGGER.info("number of user's subscribed with notifications {}", userIds.size());

      final List<String> activeUsers = entityManager
          .createQuery(
              "select user from UserProfiles user where user.userId in :userIds and user.status.id != :disabledId ",
              UserProfiles.class)
          .setParameter("userIds", userIds).setParameter("disabledId", AccountStatusEnum.DISABLED.getId())
          .getResultList().stream().map(UserProfiles::getUserId).collect(Collectors.toList());

      return notifications.stream().filter(notification -> activeUsers.contains(notification.getUserId()))
          .collect(Collectors.toList());
    } else {
      String notificationTypes =
          typeIds.stream().map(NotificationTypesEnum::getName).distinct().collect(Collectors.joining(","));
      LOGGER.info("no user subscribed with notifications {}", notificationTypes);
      return Collections.emptyList();
    }
  }

  @Override
  @Transactional(readOnly = true)
  public final List<NotificationTypes> getNotificationTypes() {
    LOGGER.debug("fetch all notificationTypes");
    List<NotificationTypes> notificationTypes = null;
    try {
      notificationTypes =
          entityManager.createQuery("select nt from NotificationTypes nt ", NotificationTypes.class).getResultList();
    } catch (NoResultException exception) {
      LOGGER.debug(notificationTypesNotFoundMessage, exception);
    }
    return notificationTypes;
  }

  @Override
  @Transactional
  public final StatusDto createNotification(@NotNull final String requesterId, @NotNull final Long typeId,
      @NotNull final Boolean favouritesOnly) throws RecordNotFoundException {
    StatusDto status = null;
    if (!StringUtils.isEmpty(requesterId) && typeId != 0) {
      LOGGER.debug("Creating new user notification with user id {}, notification type id {} and favouritesOnly {}",
          requesterId, typeId, favouritesOnly);
      final UserProfiles userProfiles = userProfileDao.getUser(requesterId);
      if (userProfiles != null) {
        final Notifications notification = new Notifications();
        notification.setUserId(requesterId);
        notification.setTypeId(typeId);
        notification.setFavouritesOnly(favouritesOnly);
        entityManager.persist(notification);
        status = new StatusDto(notificationSuccessfulCreate, requesterId, typeId);
      }
    }
    return status;
  }

  @Override
  @Transactional
  public final StatusDto deleteNotification(@NotNull final String requesterId, @NotNull final Long typeId) {
    StatusDto status = null;
    if (!StringUtils.isEmpty(requesterId) && typeId != 0) {
      LOGGER.debug("Deleting user notification with user id {} and notification type id {}.", requesterId, typeId);
      final Notifications notification = verifyNotification(requesterId, typeId);
      if (notification != null && notification.getId() != null) {
        entityManager.remove(notification);
        status = new StatusDto(notificationSuccessfulDelete, requesterId, typeId);
      } else {
        status = new StatusDto(notificationNotFound, requesterId, typeId);
      }
    }
    return status;
  }

  @Transactional(readOnly = true)
  @Override
  public final Notifications verifyNotification(final String userId, final Long typeId) {
    Notifications notification = null;
    try {
      LOGGER.debug("Verifying notification with user id {} and notification type id {}.", userId, typeId);
      notification = entityManager.createQuery(
          "select notifi from Notifications notifi where notifi.typeId = :typeId and notifi.userId = :userId",
          Notifications.class).setParameter("typeId", typeId).setParameter("userId", userId).getSingleResult();
    } catch (NoResultException | NonUniqueResultException resultException) {
      LOGGER.debug("Can't find any notification with userId {} and notification type id {}.", userId, typeId);
    }
    return notification;
  }
}
