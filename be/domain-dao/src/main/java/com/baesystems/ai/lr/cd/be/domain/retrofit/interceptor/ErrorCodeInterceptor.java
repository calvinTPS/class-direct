package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;



import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.exception.MastAPIException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * This interceptor will filter failed request from remote server.
 *
 * @author msidek
 *
 */
public class ErrorCodeInterceptor implements Interceptor {
  /**
   * Logger object.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ErrorCodeInterceptor.class);

  @Override
  public final Response intercept(final Chain chain) throws IOException {
    final Response response = chain.proceed(chain.request());
    final MediaType contentType = response.body().contentType();
    final String jsonResponse = response.body().string();
    Response.Builder builder = null;

    if (response.isSuccessful()) {
      builder = response.newBuilder().body(ResponseBody.create(contentType, jsonResponse));
    } else {
      LOGGER.error("Response code is {}. Returning as empty json body.", response.code());
      // builder = response.newBuilder().body(ResponseBody.create(contentType, "[]"));
      throw new MastAPIException(response.code(), jsonResponse);
    }

    return builder.build();
  }
}
