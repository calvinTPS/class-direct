package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AwsGlacierAuditDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.AwsGlacierAudit;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.time.LocalDateTime;

/**
 * @author fwijaya on 6/3/2017.
 */
@Repository
public class AwsGlacierAuditDaoImpl implements AwsGlacierAuditDao {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AwsGlacierAuditDao.class);

  /**
   * Entity manager.
   */
  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Query for archive by id and vault.
   */
  private static final String FIND_ARCHIVE_BY_ID_AND_VAULT =
    "select aws from AwsGlacierAudit aws where aws.glacierArchiveId = :glacierArchiveId "
      + " and aws.glacierVaultName = :glacierVaultName";

  /**
   * Store information regarding the file archived in AWS Glacier into classdirect DB.
   *
   * @param glacierArchiveId AWS Glacier archive id.
   * @param glacierArchiveVault AWS Glacier vault name.
   * @param downloadRequestUrl Download url.
   * @param exportTime Export time.
   * @param userId User id.
   * @return the {@link AwsGlacierAudit} object saved to db.
   * @throws ClassDirectException exception.
   */
  @Override
  @Transactional
  public final AwsGlacierAudit storeArchive(final String glacierArchiveId, final String glacierArchiveVault,
    final String downloadRequestUrl, final LocalDateTime exportTime, final String userId)
    throws ClassDirectException {
    try {
      final AwsGlacierAudit awsGlacierAudit = new AwsGlacierAudit();
      awsGlacierAudit.setDownloadRequestUrl(downloadRequestUrl);
      awsGlacierAudit.setExportTime(exportTime);
      awsGlacierAudit.setGlacierArchiveId(glacierArchiveId);
      awsGlacierAudit.setGlacierVaultName(glacierArchiveVault);
      awsGlacierAudit.setUserId(userId);
      entityManager.persist(awsGlacierAudit);
      return awsGlacierAudit;
    } catch (PersistenceException exception) {
      LOGGER.error("Failed to save AwsGlacierAudit.", exception);
      throw new ClassDirectException(
        "Failed to save AwsGlacierAudit with code " + glacierArchiveId + " and vault " + "" + glacierArchiveVault);
    }
  }

  /**
   * Find {@link AwsGlacierAudit} by its archive id and vault name.
   *
   * @param glacierArchiveId archive id.
   * @param glacierArchiveVault vault name.
   * @return {@link AwsGlacierAudit}.
   * @throws ClassDirectException exception.
   */
  @Override
  @Transactional(readOnly = true)
  public final AwsGlacierAudit findAuditByArchiveIdAndVaultName(final String glacierArchiveId, final String
    glacierArchiveVault)
    throws ClassDirectException {
    try {
      return entityManager.createQuery(FIND_ARCHIVE_BY_ID_AND_VAULT, AwsGlacierAudit.class)
        .setParameter("glacierArchiveId", glacierArchiveId).setParameter("glacierVaultName", glacierArchiveVault)
        .getSingleResult();
    } catch (PersistenceException exception) {
      LOGGER.error("Error querying AWS_GLACIER_AUDIT table.", exception);
      throw new ClassDirectException(
        "Cannot find archive with id " + glacierArchiveId + " and vault " + glacierArchiveVault);
    }
  }
}
