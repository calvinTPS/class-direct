package com.baesystems.ai.lr.cd.be.domain.retrofit.propertysetter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.deser.NullValueProvider;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;

import java.io.IOException;
import java.lang.annotation.Annotation;

/**
 * Base class that implements common methods of {@link SettableBeanProperty}. Extending class should implement the
 * method #deserializeAndSet.
 *
 * @author fwijaya on 11/5/2017.
 */
public abstract class BasePropertySetter extends SettableBeanProperty {
  /**
   * Serial version.
   */
  private static final long serialVersionUID = -2640701349154341397L;
  /**
   * Property name.
   */
  public static final String SIGN_IN_NAME_PROPERTY = "signInName";
  /**
   * Property name.
   */
  public static final String OBJECT_ID_PROPERTY = "objectId";
  /**
   * Method property.
   */
  private SettableBeanProperty methodProperty;

  /**
   * Constructs settable property object.
   *
   * @param methodPropertyArg the default settable property object.
   * @param jsonProperty jsonProperty name.
   */
  public BasePropertySetter(final SettableBeanProperty methodPropertyArg, final String jsonProperty) {
    super(methodPropertyArg, new PropertyName(jsonProperty));
    this.methodProperty = methodPropertyArg;
  }

  /**
   * Create settable property object.
   *
   * @param deser The deserializer.
   * @return The settable object.
   */
  @Override
  public final SettableBeanProperty withValueDeserializer(final JsonDeserializer<?> deser) {
    return methodProperty.withValueDeserializer(deser);
  }

  /**
   * Create settable property object.
   *
   * @param newName The name.
   * @return The settable property object.
   */
  @Override
  public final SettableBeanProperty withName(final PropertyName newName) {
    return methodProperty.withName(newName);
  }

  /**
   * Returns the member of the default settable property object.
   *
   * @return
   */
  @Override
  public final AnnotatedMember getMember() {
    return methodProperty.getMember();
  }

  /**
   * Returns the annotations.
   *
   * @param acls Class.
   * @param <A> Type.
   * @return The annotations.
   */
  @Override
  public final <A extends Annotation> A getAnnotation(final Class<A> acls) {
    return methodProperty.getAnnotation(acls);
  }

  /**
   * Deserializes the json object.
   *
   * @param parser The json parser.
   * @param ctxt The context.
   * @param instance The LRIDUserDto instance.
   * @return The LRIDUserDto object.
   * @throws IOException Exceptions.
   */
  @Override
  public final Object deserializeSetAndReturn(final JsonParser parser, final DeserializationContext ctxt,
                                              final Object instance) throws IOException {
    return methodProperty.deserializeSetAndReturn(parser, ctxt, instance);
  }

  /**
   * Calls the setter.
   *
   * @param instance LRIDUserDto instance.
   * @param value Value to set.
   * @throws IOException Exceptions.
   */
  @Override
  public final void set(final Object instance, final Object value) throws IOException {
    methodProperty.set(instance, value);
  }

  /**
   * Calls the setter.
   *
   * @param instance LRIDUserDto instance.
   * @param value Value to set.
   * @return The LRIDUserDto object.
   * @throws IOException Exceptions.
   */
  @Override
  public final Object setAndReturn(final Object instance, final Object value) throws IOException {
    return methodProperty.setAndReturn(instance, value);
  }

  /**
   * Forwards method call to #methodProperty.
   *
   * @param nva NullValueProvider.
   * @return the settablebeanproperty.
   */
  @Override
  public final SettableBeanProperty withNullProvider(final NullValueProvider nva) {
    return methodProperty.withNullProvider(nva);
  }

  /**
   * Returns string deserializer.
   *
   * @return string deserializer.
   */
  @Override
  public final JsonDeserializer<Object> getValueDeserializer() {
    return (JsonDeserializer) new StringDeserializer();
  }
}
