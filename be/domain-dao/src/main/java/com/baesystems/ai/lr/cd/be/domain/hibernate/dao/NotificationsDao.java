package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.NotificationTypes;
import com.baesystems.ai.lr.cd.be.domain.repositories.Notifications;
import com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author RKaneysan
 */
public interface NotificationsDao {

  /**
   * This method will return list of Notifications.
   *
   * @param typeIds type id.
   * @return list of notifications.
   */
  List<Notifications> getSubscribedUsers(final List<NotificationTypesEnum> typeIds);

  /**
   * Method to get all notificationsTypes data.
   *
   * @return list of notificationTypes.
   */
  List<NotificationTypes> getNotificationTypes();

  /**
   * Method to Create Notification Record in Database.
   *
   * @param requesterId requesterId.
   * @param typeId typeId.
   * @param favouritesOnly The flag to indicate only favourite assets.
   * @return value.
   * @throws RecordNotFoundException when Record not found.
   */
  StatusDto createNotification(final String requesterId, final Long typeId, final Boolean favouritesOnly)
      throws RecordNotFoundException;

  /**
   * Method to delete Notification Record in Database.
   *
   * @param requesterId requesterId.
   * @param typeId typeId.
   * @return value.
   */
  StatusDto deleteNotification(final String requesterId, final Long typeId);

  /**
   * Method to check whether notification exists in database with given userId and typeId.
   *
   * @param userId userId.
   * @param typeId typeId.
   * @return notifications.
   */
  Notifications verifyNotification(final String userId, final Long typeId);

}
