package com.baesystems.ai.lr.cd.be.domain.retrofit;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.AddInterceptorCallback;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient.Builder;

/**
 * Provides common feature to inject interceptors and network interceptors
 * when create Retrofit object.
 *
 * @author YWearn
 */
public abstract class BaseRetrofitFactory implements ApplicationContextAware {

  /**
   * The logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(BaseRetrofitFactory.class);

  /**
   * The list of Interceptor implementation class from Spring context.
   */
  private List<Class<? extends Interceptor>> interceptorList;

  /**
   * The list of Interceptor implementation class (Network Interceptor) from Spring context.
   */
  private List<Class<? extends Interceptor>> networkInterceptorList;

  /**
   * The Spring ApplicationContext.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * Injects interceptors from Spring context into Retrofit builder.
   *
   * @param builder the retrofit builder use to create Retrofit object.
   */
  protected final void addInterceptors(final Builder builder) {
    if (interceptorList != null && interceptorList.size() > 0) {
      interceptorList.forEach(clazz -> {
        try {
          Optional.ofNullable(context.getBean(clazz)).ifPresent(interceptor -> {
            if (interceptor instanceof AddInterceptorCallback) {
              ((AddInterceptorCallback) interceptor).initBuilder(builder);
            }
            builder.addInterceptor(interceptor);
            LOGGER.info("Added interceptor : {}", clazz.getSimpleName());
          });
        } catch (final Exception e) {
          LOGGER.warn("Skipped interceptor : {}. Error Message: {}"
                        , clazz.getSimpleName(), e.getMessage());
        }
      });
    }
  }

  /**
   * Injects network interceptors from Spring context into Retrofit builder.
   *
   * @param builder the retrofit builder use to create Retrofit object.
   */
  protected final void addNetworkInterceptors(final Builder builder) {
    Optional.ofNullable(networkInterceptorList).ifPresent(list -> {
      list.forEach(clazz -> {
        try {
          Optional.ofNullable(context.getBean(clazz)).ifPresent(interceptor -> {
            if (interceptor instanceof AddInterceptorCallback) {
              ((AddInterceptorCallback) interceptor).initBuilder(builder);
            }
            builder.addNetworkInterceptor(interceptor);
            LOGGER.info("Added network interceptor : {}", clazz.getSimpleName());
          });
        } catch (final Exception e) {
          LOGGER.warn("Skipped network interceptor : {}. Error Message: {}"
                        , clazz.getSimpleName(), e.getMessage());
        }
      });
    });
  }

  /**
   * Injects Spring context upon initialization.
   */
  @Override
  public final void setApplicationContext(final ApplicationContext springContext) throws BeansException {
    this.context = springContext;
  }

  /**
   * Returns list of Interceptor implementation class.
   * @return the list of Interceptor implementation class.
   */
  public final List<Class<? extends Interceptor>> getInterceptorList() {
    return interceptorList;
  }

  /**
   * Sets list of Interceptor implementation class to be inject into retrofit builder.
   *
   * @param interceptorListObj the list of Interceptor implementation class.
   */
  public final void setInterceptorList(final List<Class<? extends Interceptor>> interceptorListObj) {
    this.interceptorList = interceptorListObj;
  }

  /**
   * Sets list of network Interceptor implementation class to be inject into retrofit builder.
   *
   * @param networkInterceptorListObj the list of network Interceptor implementation class.
   */
  public final void setNetworkInterceptorList(final List<Class<? extends Interceptor>> networkInterceptorListObj) {
    this.networkInterceptorList = networkInterceptorListObj;
  }

  /**
   * Returns list of network Interceptor implementation class.
   * @return the list of network Interceptor implementation class.
   */
  public final List<Class<? extends Interceptor>> getNetworkInterceptorList() {
    return networkInterceptorList;
  }


  /**
   * Returns Spring {@link ApplicationContext}.
   *
   * @return the Spring application context use to locate interceptor and
   *    network interceptor implementation object.
   */
  public final ApplicationContext getContext() {
    return context;
  }

}
