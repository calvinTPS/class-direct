package com.baesystems.ai.lr.cd.be.domain.profiling;

import java.time.LocalDateTime;

import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingStat.MethodStat;
import com.baesystems.ai.lr.cd.be.enums.ResponseOptionEnum;

/**
 * Profiling Aspect for Service, DAO and Retrofit.
 *
 * @author YWearn
 *
 */
public interface ProfilingAspect {

  /**
   * For retrofit interceptor to capture the performance statistics.
   *
   * @param methodName Method name.
   * @param optionEnum optionEnum.
   * @param startTime Method start time.
   * @param endTime Method end time.
   * @return MethodStat object contains profiling information.
   */

  MethodStat injectMethodStatistic(final String methodName, final ResponseOptionEnum optionEnum,
      final LocalDateTime startTime, final LocalDateTime endTime);

  /**
   * Print statistic to log file.
   */
  void flushStat();
}
