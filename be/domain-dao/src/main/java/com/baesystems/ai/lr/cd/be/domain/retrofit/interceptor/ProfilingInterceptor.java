package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspect;
import com.baesystems.ai.lr.cd.be.enums.ResponseOptionEnum;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * THe profiling interceptor for retrofit services, main on MAST api.
 *
 * @author YWearn
 */
public class ProfilingInterceptor implements Interceptor {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProfilingInterceptor.class);

  /**
   * THe {@link ProfilingAspect} to capture statistic.
   */
  @Autowired
  private ProfilingAspect profilingAspect;

  /**
   * Intercepts retrofit request and log performance statistic.
   */
  @Override
  public final Response intercept(final Chain chain) throws IOException {

    final Request request = chain.request();
    final String requestUrl = request.method() + " " + request.url();

    final LocalDateTime startTime = LocalDateTime.now();
    Response response = null;
    try {
      response = chain.proceed(request);

    } finally {
      // Checking from networkResponse is more reliable as there is scenario networkResponse and cacheResponse both
      // exist.
      final boolean network = Optional.ofNullable(response).map(Response::networkResponse).isPresent();

      if (network) {
        profilingAspect.injectMethodStatistic(requestUrl, ResponseOptionEnum.External, startTime, LocalDateTime.now());
      } else {
        profilingAspect.injectMethodStatistic(requestUrl, ResponseOptionEnum.Cached, startTime, LocalDateTime.now());
      }
    }
    return response;
  }

}
