package com.baesystems.ai.lr.cd.be.domain.dao.jobs;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobWithFlagsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobWithFlagsQueryDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * JobRetrofitService.
 *
 * @author yng
 *
 */
public interface JobRetrofitService {

  /**
   * Get A list of job reports.
   *
   * @param jobId jobId from MAST.
   * @return Call<List<ReportLightHDto>> return dataType.
   */
  @GET("job/{jobId}/report")
  Call<List<ReportHDto>> getReportsByJobId(@Path("jobId") long jobId);

  /**
   * Call to get single job from Mast.
   *
   * @param jobId jobId.
   * @return JobHDto
   */
  @GET("job/{jobId}")
  Call<JobHDto> getJobsByJobId(@Path("jobId") long jobId);

  /**
   * Retrieve jobs with TM and ESP indicators.
   *
   * @param query The query for jobs with flags.
   * @return List of jobs with flags.
   */
  @POST("job/with-flags/query")
  Call<List<JobWithFlagsHDto>> getJobsWithFlags(@Body JobWithFlagsQueryDto query);

  /**
   * @param jobId jobId.
   * @param reportId reportId.
   * @return List of reports with param jobId and param reportId.
   */
  @GET("job/{jobId}/report/{reportId}")
  Call<ReportDto> getSurveysByJobIdAndReportId(@Path("jobId") long jobId, @Path("reportId") long reportId);

  /**
   * Retrieve jobs by abstaract-query.
   *
   * @param query The query for jobs all fields from jobDto.
   * @return List of jobs.
   */
  @POST("job/abstract-query")
  Call<List<JobWithFlagsHDto>> getJobsByAbstarctQuery(@Body AbstractQueryDto query);
}
