package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO;
import com.baesystems.ai.lr.cd.be.domain.repositories.SubFleet;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author syalavarthi
 *
 */
@Repository
public class SubFleetDAOImpl implements SubFleetDAO {
  // private static final Logger LOGGER = LoggerFactory.getLogger(SubFleetDAOImpl.class);

  /**
   * EntityManager Object.
   */
  @PersistenceContext
  private EntityManager entityManager;
  /**
  *
  */
  @Value("${record.not.found}")
  private String recordNotFoundMessage;
  /**
  *
  */
  @Value("${record.not.found.with.id}")
  private String entityNotFoundMessage;

  /*
   * (non-Javadoc)
   *
   * @see com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO#getSubFleet(java.lang.Long)
   */
  @SuppressWarnings({"unchecked"})
  @Transactional
  @Override
  public final List<Long> getSubFleetById(final String selectedUserId) throws RecordNotFoundException {
    List<Long> assetIds = new ArrayList<>();

    if (selectedUserId != null) {
      List<SubFleet> subfleet = null;
      final UserProfiles user = entityManager.find(UserProfiles.class, selectedUserId);
      if (null != user) {
        subfleet =
          entityManager.createQuery("select sf from SubFleet sf LEFT JOIN FETCH sf.user usr where usr.userId=:userid")
            .setParameter("userid", selectedUserId).getResultList();
      } else {
        throw new RecordNotFoundException(entityNotFoundMessage, selectedUserId);
      }
      if (null == subfleet) {
        throw new RecordNotFoundException(recordNotFoundMessage);
      }
      assetIds.addAll(subfleet.stream().map(asset -> asset.getAssetId()).collect(Collectors.toList()));
    }
    return assetIds;
  }

  /**
   * @return value.
   */
  public final EntityManager getEntityManager() {
    return entityManager;
  }

  /**
   * @param entityManagerObject value.
   */
  public final void setEntityManager(final EntityManager entityManagerObject) {
    this.entityManager = entityManagerObject;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO#saveSubFleet(java.lang.Long,
   * java.util.List)
   */
  @Override
  @Transactional
  public final StatusDto saveSubFleet(final String selectedUserId, final List<Long> accessibleAssetIds,
      final List<Long> restrictedAssetIds) throws RecordNotFoundException, SQLException {
    final UserProfiles user = entityManager.find(UserProfiles.class, selectedUserId);
    final StatusDto status = new StatusDto();
    if (null != user) {
      entityManager.createQuery("DELETE FROM SubFleet sf WHERE sf.user.userId=:userid")
          .setParameter("userid", selectedUserId).executeUpdate();
      if (accessibleAssetIds.isEmpty() && !restrictedAssetIds.isEmpty()) {
        user.setRestrictAll(true);
      } else if (!accessibleAssetIds.isEmpty() && !restrictedAssetIds.isEmpty()) {
        user.setRestrictAll(false);
        accessibleAssetIds.forEach(assetId -> {
          final SubFleet subFleet = new SubFleet();
          subFleet.setAssetId(assetId);
          subFleet.setUser(user);
          entityManager.persist(subFleet);
        });
      } else if (!accessibleAssetIds.isEmpty() && restrictedAssetIds.isEmpty()) {
        user.setRestrictAll(false);
      }
      entityManager.merge(user);
      status.setMessage("subfleet updated.");
    } else {
      throw new RecordNotFoundException(entityNotFoundMessage, selectedUserId);
    }
    status.setStatus("200");
    return status;
  }

  /* (non-Javadoc)
   * @see com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO#getSubFleetCountByUserId(java.lang.String)
   */
  @Override
  public final long getSubFleetCountByUserId(final String userId) throws RecordNotFoundException {
    if (userId == null) {
      throw new RecordNotFoundException(entityNotFoundMessage, "UserProfiles.userId: NULL");
    }

    final UserProfiles user = entityManager.find(UserProfiles.class, userId);
    if (user != null) {
      final Long subFleetCount =
        (Long) entityManager
          .createQuery("select count(sf) from SubFleet sf where sf.user=:user")
          .setParameter("user", user).getSingleResult();
      return subFleetCount;
    } else {
      throw new RecordNotFoundException(entityNotFoundMessage, "UserProfiles.userId: " + userId);
    }
  }


}
