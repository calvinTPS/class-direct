package com.baesystems.ai.lr.cd.be.domain.dao.asset;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetEarliestEntitiesHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetModelHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BaseDtoPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RectificationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RepairHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.query.EarliestEntityQueryDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * AssetRetrofitService.
 *
 * @author yng
 */
public interface AssetRetrofitService {

  /**
   * Get A list of CoC.
   *
   * @param assetId assetId from MAST.
   * @return Call<CoCDto> return dataType.
   */
  @GET("asset/{assetId}/coc")
  Call<List<CoCHDto>> getCoCDto(@Path("assetId") long assetId);

  /**
   * Get A list of Actionable item.
   *
   * @param assetId assetId from MAST.
   * @return Call<ActionableItemPageResourceHDto> return dataType.
   */
  @GET("asset/{assetId}/actionable-item")
  Call<List<ActionableItemHDto>> getActionableItemDto(@Path("assetId") long assetId);

  /**
   * Get A list of Asset note.
   *
   * @param assetId assetId from MAST.
   * @return Call<AssetNotePageResourceHDto> return dataType.
   */
  @GET("asset/{assetId}/asset-note")
  Call<List<AssetNoteHDto>> getAssetNoteDto(@Path("assetId") long assetId);

  /**
   * Query for asset from MAST.
   *
   * @param query body.
   * @param page page.
   * @param size size.
   * @return caller.
   */
  @POST("asset/query")
  Call<AssetPageResource> assetQuery(@Body AssetQueryDto query, @Query("page") Integer page,
      @Query("size") Integer size);

  /**
   * Query for asset from MAST.
   *
   * @param query body.
   * @param page page.
   * @param size size.
   * @return caller.
   */
  @POST("asset/query")
  Call<BaseDtoPageResource> assetQueryAsBaseDto(@Body AssetQueryDto query, @Query("page") Integer page,
      @Query("size") Integer size);

  /**
   * @param assetId value
   * @return List<AssetHDto>
   */
  @GET("asset/{assetId}")
  Call<AssetHDto> getAssetById(@Path("assetId") long assetId);

  /**
   * @param assetId value
   * @return List<JobHDto>
   */
  @GET("asset/{assetId}/job")
  Call<List<JobHDto>> getJobsById(@Path("assetId") long assetId);

  /**
   * @param employeeId value.
   * @return caller.
   */
  @GET("employee/{employeeId}")
  Call<LrEmployeeDto> getEmployeeById(@Path("employeeId") long employeeId);

  /**
   * @param query body.
   * @param assetId id.
   * @return caller.
   */
  @POST("asset/{assetId}/coc/query")
  Call<List<CoCHDto>> getCoCQueryDto(@Body CodicilDefectQueryHDto query, @Path("assetId") long assetId);

  /**
   * @param query body
   * @param assetId id
   * @return caller.
   */
  @POST("asset/{assetId}/actionable-item/query")
  Call<List<ActionableItemHDto>> getActionableItemQueryDto(@Body CodicilDefectQueryHDto query,
      @Path("assetId") long assetId);

  /**
   * Get repair list by assetId and cocId.
   *
   * @param assetId id.
   * @param cocId id.
   * @return list repair data.
   */
  @GET("asset/{assetId}/coc/{cocId}/repair")
  Call<List<RepairHDto>> getCoCRepairDto(@Path("assetId") long assetId, @Path("cocId") long cocId);

  /**
   * Get single defect by assetId and defectId.
   *
   * @param assetId id.
   * @param defectId id.
   * @return defect object.
   */
  @GET("asset/{assetId}/defect/{defectId}")
  Call<DefectHDto> getAssetDefectDto(@Path("assetId") long assetId, @Path("defectId") long defectId);

  /**
   * Get single asset item by assetId and itemId.
   *
   * @param assetId id.
   * @param itemId id.
   * @return asset item object.
   */
  @GET("asset/{assetId}/item/{itemId}")
  Call<LazyItemHDto> getAssetItemDto(@Path("assetId") long assetId, @Path("itemId") long itemId);

  /**
   * Get defects by assetId.
   *
   * @param assetId id.
   * @return list of defects.
   */
  @GET("asset/{assetId}/defect")
  Call<List<DefectHDto>> getDefectDto(@Path("assetId") long assetId);

  /**
   * Get coc by defect id and asset id.
   *
   * @param assetId id.
   * @param defectId id.
   * @return list of coc.
   */
  @GET("asset/{assetId}/defect/{defectId}/coc")
  Call<List<CoCHDto>> getCoCbyDefectAssetId(@Path("assetId") long assetId, @Path("defectId") long defectId);

  /**
   * Get single CoC by assetId and cocid.
   *
   * @param assetId id.
   * @param cocId id.
   * @return CoC object.
   */
  @GET("asset/{assetId}/coc/{cocId}")
  Call<CoCHDto> getCoCByAssetIdCoCId(@Path("assetId") long assetId, @Path("cocId") long cocId);

  /**
   * Get single AI by assetId and actionableItemid.
   *
   * @param assetId id.
   * @param actionableItemId id.
   * @return AI object.
   */
  @GET("asset/{assetId}/actionable-item/{actionableItemId}")
  Call<ActionableItemHDto> getActionableItemByAssetIdActionableItemId(@Path("assetId") long assetId,
      @Path("actionableItemId") long actionableItemId);

  /**
   * Get single AN by assetId and assetNoteId.
   *
   * @param assetId id.
   * @param assetNoteId id.
   * @return AN object.
   */
  @GET("asset/{assetId}/asset-note/{assetNoteId}")
  Call<AssetNoteHDto> getAssetNoteByAssetIdAssetNoteId(@Path("assetId") long assetId,
      @Path("assetNoteId") long assetNoteId);

  /**
   * Get single defect with assetId and defectId.
   *
   * @param assetId id.
   * @param defectId id.
   * @return defect object.
   */
  @GET("asset/{assetId}/defect/{defectId}")
  Call<DefectHDto> getDefectByAssetIdDefectId(@Path("assetId") long assetId, @Path("defectId") long defectId);

  /**
   * Query for both MAST and IHS assets with sorting and order.
   *
   * @param dto query dto.
   * @param sort sort by path.
   * @param order ASC or DESC.
   * @return list of multi asset dto.
   */
  @POST("asset/mast-and-ihs-query")
  Call<MultiAssetPageResourceDto> mastIhsQuery(@Body PrioritisedAbstractQueryDto dto, @Query("sort") String sort,
      @Query("order") String order);

  /**
   * Query for both MAST and IHS.
   *
   * @param dto query dto.
   * @return list of multi asset dto.
   */
  @POST("asset/mast-and-ihs-query")
  Call<MultiAssetPageResourceDto> mastIhsQuery(@Body PrioritisedAbstractQueryDto dto);

  /**
   * Query for both MAST and IHS assets with sorting, order and pagination.
   *
   * @param dto query dto.
   * @param page page number
   * @param size page size
   * @param sort sort by path.
   * @param order ASC or DESC.
   * @return list of multi asset dto.
   */
  @POST("asset/mast-and-ihs-query")
  Call<MultiAssetPageResourceDto> mastIhsQuery(@Body PrioritisedAbstractQueryDto dto, @Query("page") Integer page,
      @Query("size") Integer size, @Query("sort") String sort, @Query("order") String order);

  /**
   * Query for both MAST and IHS assets with sorting, order and pagination by sorting Priority.
   * @see LRCD-3553. To sort the asset with the field from IHS,
   * the Mast request url should have additional parameter sortingPriority with value "IHS".
   *
   * @param dto query dto.
   * @param page page number
   * @param size page size
   * @param sort sort by path.
   * @param order ASC or DESC.
   * @param sortingPriority MAST or IHS.
   * @return list of multi asset dto.
   */
  @POST("asset/mast-and-ihs-query")
  Call<MultiAssetPageResourceDto> mastIhsQuery(@Body PrioritisedAbstractQueryDto dto, @Query("page") Integer page,
      @Query("size") Integer size, @Query("sort") String sort, @Query("order") String order,
      @Query("sortingPriority") String sortingPriority);

  /**
   * Query for both MAST and IHS assets with pagination but without sorting.
   *
   * @param dto query dto.
   * @param page page number
   * @param size page size
   * @return list of multi asset dto.
   */
  @POST("asset/mast-and-ihs-query")
  Call<MultiAssetPageResourceDto> mastIhsQueryUnsorted(@Body PrioritisedAbstractQueryDto dto, @Query("page")
        Integer page, @Query("size") Integer size);

  /**
   * Query to Mast to get Earliest Entity Due_Status.
   *
   * @param earliestEntityDto dto.
   * @return list of asset earliest entities dto.
   */
  @POST("asset/earliest-entities/query")
  Call<List<AssetEarliestEntitiesHDto>> assetEarliestEntities(@Body EarliestEntityQueryDto earliestEntityDto);

  /**
   * Get Deficiencies by assetId.
   *
   * @param assetId id.
   * @param deficiencyQueryHDto query.
   * @return list of faults.
   */
  @POST("asset/{assetId}/deficiency/query")
  Call<List<DeficiencyHDto>> getDeficiencies(@Path("assetId") long assetId,
      @Body DeficiencyQueryHDto deficiencyQueryHDto);

  /**
   * Get deficiency by id.
   *
   * @param assetId id.
   * @param deficiencyId deficiencyId.
   * @return DeficiencyDto dto.
   */
  @GET("asset/{assetId}/deficiency/{deficiencyId}")
  Call<DeficiencyHDto> getDeficiencyDto(@Path("assetId") long assetId, @Path("deficiencyId") long deficiencyId);

  /**
   * Get repairs by defectId.
   *
   * @param assetId id.
   * @param defectId defectId.
   * @return DeficiencyDto dto.
   */
  @GET("asset/{assetId}/defect/{defectId}/repair")
  Call<List<RepairHDto>> getRepairs(@Path("assetId") long assetId, @Path("defectId") long defectId);

  /**
   * Get rectification by deficiencyId.
   *
   * @param assetId id.
   * @param deficiencyId deficiencyId.
   * @return DeficiencyDto dto.
   */
  @GET("asset/{assetId}/deficiency/{deficiencyId}/rectification")
  Call<List<RectificationHDto>> getRectification(@Path("assetId") long assetId,
      @Path("deficiencyId") long deficiencyId);

  /**
   * Get rectification by deficiencyId and rectification id.
   *
   * @param assetId id.
   * @param deficiencyId deficiencyId.
   * @param rectificationId rectificationId.
   * @return DeficiencyDto dto.
   */
  @GET("asset/{assetId}/deficiency/{deficiencyId}/rectification/{rectificationId}")
  Call<RectificationHDto> getRectificationByRectificationId(@Path("assetId") long assetId,
      @Path("deficiencyId") long deficiencyId, @Path("rectificationId") long rectificationId);

  /**
   * Get statutory findings for deficiency.
   *
   * @param assetId id.
   * @param deficiencyId deficiencyId.
   * @return list of statutory findings.
   */
  @GET("asset/{assetId}/deficiency/{deficiencyId}/statutory-finding")
  Call<List<StatutoryFindingHDto>> getStatutoryFindingsForDeficiency(@Path("assetId") long assetId,
      @Path("deficiencyId") long deficiencyId);

  /**
   * Get statutory findings for asset.
   *
   * @param assetId id.
   * @param query query.
   * @return list of statutory findings.
   */
  @POST("asset/{assetId}/statutory-finding/query")
  Call<List<StatutoryFindingHDto>> getStatutoryFindingsForAsset(@Path("assetId") long assetId,
      @Body CodicilDefectQueryHDto query);

  /**
   * Get statutory finding by id.
   *
   * @param assetId id.
   * @param deficiencyId deficiencyId.
   * @param statutoryFindingId statutoryFindingId.
   * @return list of statutory findings.
   */
  @GET("asset/{assetId}/deficiency/{deficiencyId}/statutory-finding/{statutoryFindingId}")
  Call<StatutoryFindingHDto> getStatutoryFindingById(@Path("assetId") long assetId,
      @Path("deficiencyId") long deficiencyId, @Path("statutoryFindingId") long statutoryFindingId);

  /**
   * Get actionable item by asset id, service id.
   *
   * @param assetId id.
   * @param serviceId id.
   * @return list of actionable item.
   */
  @GET("asset/{assetId}/service/{serviceId}/actionable-item")
  Call<List<ActionableItemHDto>> getActionableItemByAssetIdByServiceId(@Path("assetId") long assetId,
      @Path("serviceId") long serviceId);


  /**
   * Gets list of task associated to an asset.
   *
   * @param assetId the internal asset id.
   * @return the list of task associated to an asset.
   */
  @GET("asset/{assetId}/task")
  Call<List<WorkItemLightHDto>> getTasksByAssetId(@Path("assetId") long assetId);


  /**
   * Get defects from MAST.
   *
   * @param assetId id.
   * @param query query.
   * @return list of defects.
   */
  @POST("asset/{assetId}/defect/query")
  Call<List<DefectHDto>> getDefectsByQuery(@Path("assetId") long assetId, @Body CodicilDefectQueryHDto query);

  /**
   * Get major non confirmitive notes by assetId.
   *
   * @param assetId id.
   * @return list of mncns.
   */
  @GET("asset/{assetId}/mncn")
  Call<List<MajorNCNHDto>> getMajorNCNdto(@Path("assetId") long assetId);

  /**
   * Get single major non confirmitive note by assetId and mncnId.
   *
   * @param assetId id.
   * @param mncnId the major non confirmitive note id.
   * @return AN object.
   */
  @GET("asset/{assetId}/mncn/{mncnId}")
  Call<MajorNCNHDto> getMajorNCNById(@Path("assetId") long assetId,
      @Path("mncnId") long mncnId);

  /**
   * Get asset items model by assetId and asset version id.
   *
   * @param assetId the asset unique value.
   * @param versionId the asset version id.
   * @return asset items model.
   */
  @GET("asset-model/{assetId}")
  Call<AssetModelHDto> getAssetItemsModel(@Path("assetId") long assetId, @Query("versionId") long versionId);
}
