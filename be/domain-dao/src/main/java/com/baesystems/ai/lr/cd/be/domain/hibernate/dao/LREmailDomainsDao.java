package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import com.baesystems.ai.lr.cd.be.domain.repositories.LREmailDomains;

import java.util.List;

/**
 * Provides data access service to get lr email domains from database.
 *
 * @author fwijaya on 28/4/2017.
 */
public interface LREmailDomainsDao {
  /**
   * Returns list of LR email domains.
   *
   * @return The LR email domains.
   */
  List<LREmailDomains> getLrEmailDomains();
}
