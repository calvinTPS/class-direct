package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.Favourites;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author VMandalapu
 * @author sbollu.
 *
 */
public interface FavouritesRepository {

  /**
   * To get User Favourites.
   *
   * @param userId id.
   * @return Favourites List.
   * @throws RecordNotFoundException exception.
   */
  List<Favourites> getUserFavourites(final String userId) throws RecordNotFoundException;

  /**
   * Check if the asset is favourite.
   *
   * @param userId id of user.
   * @param assetId asset id.
   * @return favourite flag.
   */
  Boolean isUserFavourite(final String userId, final Long assetId);

  /**
   * To create user favourite.
   *
   * @param userId id.
   * @param assetCode code.
   * @throws RecordNotFoundException exception.
   */
  void createUserFavourite(final String userId, final String assetCode) throws RecordNotFoundException;

  /**
   * To delete user favorite.
   *
   * @param userId user id.
   * @param assetCode assetCode.
   */
  void deleteUserFavourite(final String userId, final String assetCode);

  /**
   * To delete all user favorite.
   *
   * @param userId user id.
   * @throws RecordNotFoundException RecordNotFoundException.
   */
  void deleteAllUserFavourites(final String userId) throws RecordNotFoundException;

  /**
   * To verify favorite or not.
   *
   * @param userId user id.
   * @param assetCode assetCode.
   * @return Favorites favorite.
   */
  Favourites verifyFavourite(final String userId, final String assetCode);


  /**
   * Creates or remove user favorite from FAVOURITES table.
   *
   * @param userId the unique identifier of user.
   * @param favourites the object holds assetCodes to be removed and inserted.
   * @throws ClassDirectException if any execution fails.
   */
  void updateUserFavouriteList(final String userId, final FavouritesDto favourites) throws ClassDirectException;

}
