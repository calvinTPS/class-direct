package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BuilderHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ServiceCreditStatusRefHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Retrofit AssetReferenceService.
 *
 * @author msidek
 * @author YWearn 2017-05-03
 *
 */
public interface AssetReferenceRetrofitService {
  /**
   * Get list of asset types.
   *
   * @return list of asset types.
   *
   */
  @GET("reference-data/asset/asset-types")
  Call<List<AssetTypeHDto>> getAssetTypeDtos();

  /**
   * Get list of asset categories.
   *
   * @return list of asset categories.
   *
   */
  @GET("reference-data/asset/asset-categories")
  Call<List<AssetCategoryHDto>> getAssetCategories();

  /**
   * Get list of {@link ClassStatusHDto}.
   *
   * @return caller.
   *
   */
  @GET("reference-data/asset/class-statuses")
  Call<List<ClassStatusHDto>> getClassStatus();

  /**
   * Get list of codicil categories.
   *
   * @return list of codicil categories.
   *
   */
  @GET("reference-data/asset/codicil-categories")
  Call<List<CodicilCategoryHDto>> getCodicilCategories();

  /**
   * Get list of flagState.
   *
   * @return List<FlagStateHDto>
   */
  @GET("flag-state")
  Call<List<FlagStateHDto>> getFlagState();

  /**
   * Get list of LifecycleStatus.
   *
   * @return List<AssetLifeCycleStatusHDto>
   */
  @GET("reference-data/asset/lifecycle-statuses")
  Call<List<AssetLifeCycleStatusDto>> getAssetLifecycleStatuses();

  /**
   * Get list of builders.
   *
   * @return List<BuilderDto>
   */
  @GET("reference-data/asset/builder")
  Call<List<BuilderHDto>> getBuilders();

  /**
   * Get list of DueStatuses.
   *
   * @return List<DueStatusDto>
   */
  @GET("reference-data/miscellaneous/due-statuses")
  Call<List<DueStatusDto>> getDueStatuses();

  /**
   * Get list of repair actions.
   *
   * @return list of repair actions.
   */
  @GET("reference-data/repair/repair-actions")
  Call<List<RepairActionHDto>> getRepairActions();

  /**
   * Get list of defect statuses.
   *
   * @return list of defect statuses.
   */
  @GET("reference-data/defect/defect-statuses")
  Call<List<DefectStatusHDto>> getDefectStatuses();

  /**
   * Get list of codicil status.
   *
   * @return list of codicil status.
   */
  @GET("reference-data/service/codicil-statuses")
  Call<List<CodicilStatusHDto>> getCodicilStatuses();


  /**
   * Get list of repair type.
   *
   * @return list of repair type.
   */
  @GET("reference-data/repair/repair-types")
  Call<List<RepairTypeHDto>> getRepairTypes();

  /**
   * Get list of classification society.
   *
   * @return list of clarification society.
   */
  @GET("reference-data/asset/iacs-societies")
  Call<List<ClassificationSocietyHDto>> getClassificationSocieties();

  /**
   * Get list of class maintenance status.
   *
   * @return list of class maintenance status.
   */
  @GET("reference-data/asset/class-maintenance-statuses")
  Call<List<ClassMaintenanceStatusHDto>> getClassMaintenanceStatuses();

  /**
   * Get list of service credit status.
   *
   * @return list of service credit status.
   */
  @GET("reference-data/service/service-credit-statuses")
  Call<List<ServiceCreditStatusRefHDto>> getServiceCreditStatuses();

  /**
   * Get list of service Action taken.
   *
   * @return list of service credit status.
   */
  @GET("reference-data/asset/actions-taken")
  Call<List<ActionTakenHDto>> getActionTakenList();

  /**
   * Gets list of country information.
   *
   * @return the list of country information.
   */
  @GET("reference-data/asset/countries")
  Call<List<CountryHDto>> getCountryList();

  /**
   * Get list of party roles.
   *
   * @return list of party roles.
   */
  @GET("reference-data/party/party-roles")
  Call<List<PartyRoleHDto>> getPartyRoles();
  /**
   * Get list of major non confirmitives.
   *
   * @return list of major non confirmitives.
   */
  @GET("reference-data/asset/mncn-statuses")
  Call<List<MajorNCNStatusHDto>> getMajorNCNs();

}
