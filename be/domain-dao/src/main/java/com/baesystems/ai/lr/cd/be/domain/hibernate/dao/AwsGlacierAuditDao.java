package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import com.baesystems.ai.lr.cd.be.domain.repositories.AwsGlacierAudit;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

import java.time.LocalDateTime;

/**
 * @author fwijaya on 6/3/2017.
 */
public interface AwsGlacierAuditDao {

  /**
   * Store information regarding the file archived in AWS Glacier into classdirect DB.
   *
   * @param glacierArchiveId AWS Glacier archive id.
   * @param glacierArchiveVault AWS Glacier vault name.
   * @param downloadRequestUrl Download url.
   * @param exportTime Export time.
   * @param userId User id.
   * @return the {@link AwsGlacierAudit} object saved to db.
   * @throws ClassDirectException exception.
   */
  AwsGlacierAudit storeArchive(String glacierArchiveId, String glacierArchiveVault, String downloadRequestUrl,
    LocalDateTime exportTime, String userId)
    throws ClassDirectException;

  /**
   * Find {@link AwsGlacierAudit} by its archive id and vault name.
   *
   * @param glacierArchiveId archive id.
   * @param glacierArchiveVault vault name.
   * @return {@link AwsGlacierAudit}.
   * @throws ClassDirectException exception.
   */
  AwsGlacierAudit findAuditByArchiveIdAndVaultName(String glacierArchiveId, String glacierArchiveVault)
    throws ClassDirectException;
}
