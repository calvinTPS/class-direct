package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.entities.Subsegment;
import com.amazonaws.xray.exceptions.SegmentNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.MastAPIException;
import com.baesystems.ai.lr.cd.be.utils.ExceptionUtil;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Faizal Sidek
 */
public class LoggingInterceptor implements Interceptor {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(LoggingInterceptor.class);

  /**
   * Max output length.
   */
  private static final Integer MAX_LENGTH = 5000;

  @Override
  public final Response intercept(final Chain chain) throws IOException {
    final StringBuilder builder = new StringBuilder();
    final Map<String, Object> requestMap = new HashMap<>();
    final Request request = chain.request();
    final RequestBody body = request.body();

    final String requestUrl = request.method() + " " + request.url();
    final Integer indexStart = requestUrl.indexOf("/api/v");
    final Integer indexEnd = requestUrl.indexOf("?");
    final String servicePath;
    if (indexEnd > 0) {
      servicePath = requestUrl.substring(indexStart, indexEnd);
    } else {
      servicePath = requestUrl.substring(indexStart);
    }
    builder.append("Performed ").append(requestUrl).append(" | Headers: ");
    final StringBuilder requestHeaders = new StringBuilder();
    request.headers().names().forEach(name -> {
      builder.append(name).append(":").append(request.header(name)).append(";");
      requestHeaders.append(name).append(":").append(request.header(name)).append(";");
    });
    requestMap.put("requestheaders", requestHeaders.toString());

    String requestBody = "";
    if (null != body) {
      final Buffer buffer = new Buffer();
      body.writeTo(buffer);

      Charset charset = Charset.forName("UTF8");
      if (body.contentType() != null) {
        charset = body.contentType().charset(Charset.forName("UTF8"));
      }
      requestBody = buffer.readString(charset);
    }

    final long start = System.currentTimeMillis();
    final Response response;

    Subsegment subsegment = null;
    try {
      AWSXRay.getCurrentSegment();
      subsegment = AWSXRay.beginSubsegment("MAST::" + servicePath);
    } catch (SegmentNotFoundException exception) {
      //Safely ignored exception.
    }

    try {
      requestMap.put("url", requestUrl);
      requestMap.put("body", requestBody);
      response = chain.proceed(request);
      requestMap.put("status", response.code());
      final StringBuilder responseHeaders = new StringBuilder();
      response.headers().names().forEach(name -> {
        responseHeaders.append(name).append(":").append(response.header(name)).append(";");
        if (name.equalsIgnoreCase("X-Amzn-Trace-Id")) {
          requestMap.put("masttraceid", response.header(name));
        }
      });
      requestMap.put("responseheaders", responseHeaders.toString());
    } catch (IOException exception) {
      final long elapsed = System.currentTimeMillis() - start;
      // log the error code and message if fail in MAST API
      if (!requestBody.isEmpty()) {
        builder.append(" | RequestBody: ").append(requestBody);
      }
      final MastAPIException mex = ExceptionUtil.toMastException(exception);
      if (mex != null) {
        builder.append(" | Response Code: ").append(mex.getErrorCode()).append(" | Message: ").append(mex.getMessage());
        requestMap.put("message", mex.getMessage());
        requestMap.put("status", mex.getErrorCode());
      }
      builder.append(" | Request failed in ").append(elapsed).append(" ms");
      LOGGER.error(builder.toString());
      if (subsegment != null) {
        subsegment.addException(exception);
      }
      throw exception;
    } finally {
      if (subsegment != null) {
        subsegment.setAnnotations(requestMap);
        AWSXRay.endSubsegment();
      }
    }
    final long elapsed = System.currentTimeMillis() - start;

    final MediaType contentType = response.body().contentType();
    final String originalResponse = response.body().string();
    String responseBody = originalResponse;
    if (responseBody.length() > MAX_LENGTH) {
      responseBody = responseBody.substring(0, MAX_LENGTH) + "... (output omitted)";
    }

    builder.append(" | Response Code: ").append(response.code()).append(" | Message: ")
        .append(response.message()).append(" | Elapsed: ").append(elapsed).append(" ms");
    if (response.cacheResponse() != null) {
      builder.append(" | Cached:TRUE");
    } else {
      builder.append(" | Cached:FALSE");
    }
    if (!requestBody.isEmpty()) {
      builder.append(" | RequestBody: ").append(requestBody);
    }
    if (!responseBody.isEmpty()) {
      builder.append(" | Response Body: ").append(responseBody);
    }

    LOGGER.debug(builder.toString());
    return response.newBuilder().body(ResponseBody.create(contentType, originalResponse)).build();
  }

}
