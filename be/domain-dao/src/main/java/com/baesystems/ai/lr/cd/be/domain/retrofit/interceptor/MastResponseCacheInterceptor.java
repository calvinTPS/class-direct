package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import java.io.File;
import java.io.IOException;
import java.util.List;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Override the http cache header for MAST response to avoid repeated API call to MAST.
 *
 * @author YWearn
 *
 */
public class MastResponseCacheInterceptor implements Interceptor, AddInterceptorCallback {
  /**
   * Logger object.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MastResponseCacheInterceptor.class);

  /**
   * Default cache size (10 MB).
   */
  private static final Long DEFAULT_CACHE_SIZE = 10L * 1024L * 1024L;

  /**
   * cache duration.
   */
  private Long cacheDuration;
  /**
   * HTTP Cache directory.
   */
  private String cacheDirectory;

  /**
   * HTTP Cache size.
   */
  private Long cacheSize = DEFAULT_CACHE_SIZE;

  /**
   * List of url to be excluded.
   */
  private List<String> exclusions;

  /*
   * Intercept retrofit response,and implements httpcache for repeated retrofit request(GET).
   *
   * @see okhttp3.Interceptor#intercept(okhttp3.Interceptor.Chain)
   */
  @Override
  @ResponseBody
  public Response intercept(final Chain chain) throws IOException {
    final Request request = chain.request();

    final String requestUrl = request.method() + " " + request.url();

    if (exclusions != null && !exclusions.stream().anyMatch(path -> request.url().encodedPath().matches(path))) {
      final Request modRequest =
        request.newBuilder().addHeader("Cache-Control", "public, max-age=" + this.getCacheDuration()).build();

      final Response response = chain.proceed(modRequest);

      if (response.cacheResponse() != null) {
        LOGGER.info("Cached result from: {}", requestUrl);
      } else {
        LOGGER.info("Actual result from: {}", requestUrl);
      }

      final Response modResponse = response.newBuilder().removeHeader("Pragma").removeHeader("Cache-Control")
        .addHeader("Cache-Control", "public, max-age=" + this.getCacheDuration()).build();

      return modResponse;
    } else {
      LOGGER.debug("Exclude {} from request caching.", requestUrl);
      return chain.proceed(request.newBuilder().build());
    }
  }

  /*
   * Create cache Directory for httpCache and attach cacheDir to builder.
   *
   * @see com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor.AddInterceptorCallback#initBuilder(
   * okhttp3.OkHttpClient.Builder)
   */
  @Override
  public void initBuilder(final Builder builder) {
    LOGGER.info("Setup http cache on {} with max size: {}", this.getCacheDirectory(), this.getCacheSize());
    final File cacheDir = new File(this.getCacheDirectory());

    if (!cacheDir.exists()) {
      try {
        FileUtils.forceMkdir(cacheDir);
      } catch (final IOException e) {
        LOGGER.error("Error while creating http cache directory. " + e.getMessage(), e);
      }
    }

    LOGGER.info("Cache Directory attribute. CanWrite: {}, Exists: {}, isDirectory: {}", cacheDir.canWrite(),
        cacheDir.exists(), cacheDir.isDirectory());

    final Cache cache = new Cache(cacheDir, this.getCacheSize());
    builder.cache(cache);
  }

  /**
   * @return cacheDuration.
   */
  public final Long getCacheDuration() {
    return cacheDuration;
  }

  /**
   * @param cacheDurationValue cacheDurationValue.
   */
  public void setCacheDuration(final Long cacheDurationValue) {
    this.cacheDuration = cacheDurationValue;
  }



  /**
   * Getter for cache directory.
   *
   * @return cache directory.
   */
  public final String getCacheDirectory() {
    return cacheDirectory;
  }

  /**
   * Setter for cache directory.
   *
   * @param cacheDirectoryStr cache directory to be set.
   */
  public final void setCacheDirectory(final String cacheDirectoryStr) {
    this.cacheDirectory = cacheDirectoryStr;
  }

  /**
   * Getter for cache size.
   *
   * @return cache size.
   */
  public final Long getCacheSize() {
    return cacheSize;
  }

  /**
   * Setter for cache size.
   *
   * @param cacheSizeValue cache size to be set.
   */
  public final void setCacheSize(final Long cacheSizeValue) {
    this.cacheSize = cacheSizeValue;
  }

  /**
   * Setter for {@link #exclusions}.
   *
   * @param list value to be set.
   */
  public final void setExclusions(final List<String> list) {
    this.exclusions = list;
  }
}
