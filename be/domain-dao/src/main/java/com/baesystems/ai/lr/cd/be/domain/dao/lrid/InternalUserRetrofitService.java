package com.baesystems.ai.lr.cd.be.domain.dao.lrid;

import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

/**
 * Provides remote service to query, create and approve internal users (LR AD users).
 *
 * @author FirmanW
 */
public interface InternalUserRetrofitService {
  /**
   * Returns list of LR AD users.
   *
   * @param filter filter.
   * @return list of user dto.
   */
  @GET("users")
  Call<List<LRIDUserDto>> getInternalUsers(@Query("$filter") String filter);
}
