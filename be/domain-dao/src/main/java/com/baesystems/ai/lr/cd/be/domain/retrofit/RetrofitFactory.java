package com.baesystems.ai.lr.cd.be.domain.retrofit;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Provides factory method to create Retrofit object.
 *
 * @author msidek
 * @author YWearn
 */
public class RetrofitFactory extends BaseRetrofitFactory {
  /**
   * The logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(RetrofitFactory.class);

  /**
   * The default Retrofit client connection timeout which is {@value #DEFAULT_TIMEOUT_IN_SECONDS}.
   */
  private static final Integer DEFAULT_TIMEOUT_IN_SECONDS = 300;
  /**
   * The external REST API base URL injected from Spring context.
   */
  private String baseUrl;
  /**
   * The read/write and general timeout in seconds for created Retrofit object.
   * Defaulted to {@value #DEFAULT_TIMEOUT_IN_SECONDS} if not specified in Spring Context.
   */
  private Integer timeout = DEFAULT_TIMEOUT_IN_SECONDS;

  /**
   * The flag to indicates if the http client retry in case of connection error.
   */
  private boolean retryOnConnectionError = true;

  /**
   * Flag to ignore and accept self signed certificate.
   */
  private boolean ignoreSsl = false;

  /**
   * Sets the Retrofit service default timeout in second(s).
   *
   * @param time the Retrofit read/write and general timeout in seconds.
   */
  public final void setTimeout(final Integer time) {
    this.timeout = time;
  }

  /**
   * Provides a new Retrofit client service.
   *
   * @return the Retrofit client service.
   *
   */
  public final Retrofit createRetrofit() {
    LOGGER.info("Creating retrofit factory with base url {}", baseUrl);
    return new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(JacksonConverterFactory.create(createMapper()))
        .client(createClient()).build();
  }

  /**
   * Creates a new instance of OkHttpClient.
   *
   * @return the OkHttpClient use by Retrofit service.
   *
   */
  private OkHttpClient createClient() {
    final Builder builder = new OkHttpClient.Builder();
    builder.connectTimeout(timeout, TimeUnit.SECONDS);
    builder.readTimeout(timeout, TimeUnit.SECONDS);
    builder.writeTimeout(timeout, TimeUnit.SECONDS);
    // Enable the retry in live as notice intermittent connection error
    // but disable during unit test using retryOnConnectionError.
    // see https://github.com/square/okhttp/issues/2394
    builder.retryOnConnectionFailure(retryOnConnectionError);

    if (ignoreSsl) {
      LOGGER.info("Ignore all SSL certificate.");
      final TrustManager[] trustAllCerts = new TrustManager[] {new TrustAllManager()};
      try {
        final SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new SecureRandom());

        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
        builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
        builder.hostnameVerifier((s, sslSession) -> true);
      } catch (NoSuchAlgorithmException | KeyManagementException exception) {
        LOGGER.error("Unable to create custom SSL context.", exception);
      }
    }

    OkHttpClient client = null;
    if (super.getContext() != null) {
      addInterceptors(builder);
      addNetworkInterceptors(builder);
      client = builder.build();
    }

    return client;
  }


  /**
   * Creates a new JSON ObjectMapper to parse REST JSON request and response.
   *
   * @return the JSON ObjectMapper to parse REST JSON request and response.
   */
  private ObjectMapper createMapper() {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.setDateFormat(new ISO8601DateFormat());
    return mapper;
  }

  /**
   * Gets the baseUrl that Retrofit service connect to.
   *
   * @return the baseUrl that Retrofit service connect to.
   *
   */
  public final String getBaseUrl() {
    return baseUrl;
  }

  /**
   * Sets the baseUrl that Retrofit service connect to.
   *
   * @param url the baseUrl that Retrofit service connect to.
   *
   */
  public final void setBaseUrl(final String url) {
    this.baseUrl = url;
  }

  /**
   * Sets the indicator to instructs http client automatically retry in case of connection error.
   * @param retryOnConnectionErrorVal the indicator to instruct the http client to retry in case of connection error.
   */
  public final void setRetryOnConnectionError(final boolean retryOnConnectionErrorVal) {
    retryOnConnectionError = retryOnConnectionErrorVal;
  }

  /**
   * Setter for {@link #ignoreSsl}.
   *
   * @param ignore value to be set.
   */
  public final void setIgnoreSsl(final boolean ignore) {
    this.ignoreSsl = ignore;
  }

  /**
   * This Trust manager which trust all.
   */
  private static class TrustAllManager implements X509TrustManager {
    @Override
    public void checkClientTrusted(final X509Certificate[] x509Certificates, final String s)
      throws CertificateException {
    }

    @Override
    public void checkServerTrusted(final X509Certificate[] x509Certificates, final String s)
      throws CertificateException {
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
      return new X509Certificate[] {};
    }
  }
}
