package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FlagsAssociationsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.FlagsAssociations;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Provides data access service to add, remove and get flags associations from database.
 *
 * @author fwijaya on 19/4/2017.
 */
@Repository
public class FlagsAssociationsDaoImpl implements FlagsAssociationsDao {

  /**
   * Log4J Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FlagsAssociationsDaoImpl.class);

  /**
   * EntityManager Object.
   */
  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Removes flags associations.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @return The number of entities deleted.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  @Override
  @Transactional
  public final int removeAssociation(final String flagCode, final String secondaryFlagCode)
    throws ClassDirectException {
    LOGGER.debug("Remove association for flagCode: " + flagCode + " and secondaryFlagCode: " + secondaryFlagCode);
    try {
      return this.entityManager.createQuery("delete from FlagsAssociations fa where "
                                              + " fa.flagCode = :flagCode and fa.secondaryFlagCode = "
                                              + ":secondaryFlagCode")
        .setParameter("flagCode", flagCode).setParameter("secondaryFlagCode", secondaryFlagCode).executeUpdate();
    } catch (Exception exceptions) {
      throw new ClassDirectException(exceptions);
    }
  }

  /**
   * Adds flags associations.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  @Override
  @Transactional
  public final void addAssociation(final String flagCode, final String secondaryFlagCode)
    throws ClassDirectException {
    LOGGER.debug("Add association for flagCode: " + flagCode + " and secondaryFlagCode: " + secondaryFlagCode);
    final FlagsAssociations flagsAssociations = new FlagsAssociations();
    flagsAssociations.setFlagCode(flagCode);
    flagsAssociations.setSecondaryFlagCode(secondaryFlagCode);
    try {
      this.entityManager.persist(flagsAssociations);
    } catch (Exception exceptions) {
      throw new ClassDirectException(exceptions);
    }
  }

  /**
   * Returns flag associations for a flag code.
   *
   * @param flagCode Flag code.
   * @return Flag associations for a flag code.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  @Override
  @Transactional
  public final List<FlagsAssociations> getFlagAssocisationsForFlagCode(final String flagCode)
    throws ClassDirectException {
    LOGGER.debug("Get flags associations for flagCode: " + flagCode);
    try {
      return this.entityManager.createQuery("select fa from FlagsAssociations fa where fa.flagCode = :flagCode")
        .setParameter("flagCode", flagCode).getResultList();
    } catch (Exception exceptions) {
      throw new ClassDirectException(exceptions);
    }
  }

  /**
   * Returns flag associations for a secondary flag code.
   *
   * @param secondaryFlagCode Secondary flag code.
   * @return Flag associations for a secondary flag code.
   * @throws ClassDirectException if unchecked JPA exception occurs.
   */
  @Override
  @Transactional
  public final List<FlagsAssociations> getFlagAssocisationsForSecondaryFlagCode(final String secondaryFlagCode)
    throws ClassDirectException {
    LOGGER.debug("Get flags associations for secondary flags code: " + secondaryFlagCode);
    try {
      return this.entityManager
        .createQuery("select fa from FlagsAssociations fa where fa.secondaryFlagCode = :secondaryFlagCode")
        .setParameter("secondaryFlagCode", secondaryFlagCode).getResultList();
    } catch (Exception exceptions) {
      throw new ClassDirectException(exceptions);
    }
  }

  /**
   * Returns the associated flag code for a given flag code.
   *
   * @param flagsAssociations The entity.
   * @param theFlagCode The flag code.
   * @return The associated flag code.
   */
  @Override
  public final String getAssociatedFlag(final FlagsAssociations flagsAssociations, final String theFlagCode) {
    if (theFlagCode.equalsIgnoreCase(flagsAssociations.getFlagCode())) {
      return flagsAssociations.getSecondaryFlagCode();
    } else if (theFlagCode.equalsIgnoreCase(flagsAssociations.getSecondaryFlagCode())) {
      return flagsAssociations.getFlagCode();
    } else {
      return null;
    }
  }
}
