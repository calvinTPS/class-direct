package com.baesystems.ai.lr.cd.be.domain.dao;

import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.cd.be.domain.dao.lrid.UserRetrofitService;

import org.springframework.beans.factory.annotation.Qualifier;
import retrofit2.Retrofit;

/**
 * @author VKolagutla
 *
 */
public class LRIDRetrofitServiceFactory {

  /**
   * Retrofit builder.
   */
  @Autowired
  @Qualifier("lridRetrofit")
  private Retrofit lridRetrofit;

  /**
   * create new UserRetrofitService.
   *
   * @return userRetrofitService.
   */
  public final UserRetrofitService createUserRetrofitService() {
    return lridRetrofit.create(UserRetrofitService.class);
  }

}
