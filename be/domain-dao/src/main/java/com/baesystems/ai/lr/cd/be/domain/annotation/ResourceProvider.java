package com.baesystems.ai.lr.cd.be.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark the method as resource provider.
 *
 * @author msidek
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ResourceProvider {

  /**
   * Flag for is cache.
   *
   */
  boolean cache() default true;

  /**
   * Name of id fields.
   *
   */
  String[] fieldNames() default "id";
}
