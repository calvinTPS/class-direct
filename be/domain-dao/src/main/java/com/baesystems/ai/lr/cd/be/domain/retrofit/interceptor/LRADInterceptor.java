package com.baesystems.ai.lr.cd.be.domain.retrofit.interceptor;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Intercepts and appends authorization header and api-version query to retrofit calls to internal LR active
 * directory.
 *
 * @author fwijaya
 */
public class LRADInterceptor extends AzureInterceptorBase {

  /**
   * LR AD resource token.
   */
  @Value("${internal.lr.ad.oauth2.resource}")
  private String resource;
  /**
   * LR AD token scope.
   */
  @Value("${internal.lr.ad.oauth2.scope}")
  private String scope;
  /**
   * LR AD access token prefix.
   */
  private static final String PREFIX = "Bearer";

  /**
   * empty delimiter.
   */
  private static final String EMPTY_DELIMITTER = " ";

  /**
   * Intercepts and appends authorization header to retrofit calls to internal LR active directory.
   *
   * @param chain Interceptor chain.
   * @return The http response.
   * @throws IOException when IO exceptions occurred.
   */
  @Override
  public final Response intercept(final Chain chain) throws IOException {
    final Request request = chain.request();
    final Map<String, String> additionalProperties = new HashMap<>();
    additionalProperties.put("resource", resource);
    additionalProperties.put("scope", scope);
    refreshToken(additionalProperties);
    final HttpUrl url = request.url().newBuilder().build();
    final Request.Builder builder = request.newBuilder().headers(request.headers())
      .addHeader("Authorization", PREFIX + EMPTY_DELIMITTER + getToken().getAccessToken())
      .method(request.method(), request.body()).url(url);
    return processResponse(chain.proceed(builder.build()));
  }
}
