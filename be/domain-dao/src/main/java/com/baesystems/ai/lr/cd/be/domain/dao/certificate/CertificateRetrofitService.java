package com.baesystems.ai.lr.cd.be.domain.dao.certificate;

import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionPageResourceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificatePageResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;
import com.baesystems.ai.lr.dto.query.CertificateQueryDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * CertificateRetrofitService. Created by fwijaya on 23/1/2017.
 */
public interface CertificateRetrofitService {

  /**
   * Query certificates.
   *
   * @param dto dto.
   * @param page page.
   * @param size size.
   * @param sort sort.
   * @param order order.
   * @return certificate page resource object.
   */
  @POST("certificate/query")
  Call<CertificatePageResource> certificateQuery(@Body CertificateQueryDto dto, @Query("page") Integer page,
      @Query("size") Integer size, @Query("sort") String sort, @Query("order") String order);

  /**
   * Query certificate actions.
   *
   * @param id certificate id.
   * @return CertificateActionPageResourceHDto object.
   */
  @GET("certificate/{certificateId}/action")
  Call<CertificateActionPageResourceHDto> certificateActionQuery(@Path(value = "certificateId") Long id);

  /**
   * Query certificate attachments.
   *
   * @param jobId jobId.
   * @param certId certId.
   * @return SupplementaryInformationPageResourceDto object.
   */
  @GET("job/{jobId}/wip-certificate/{certificateId}/attachment")
  Call<SupplementaryInformationPageResourceDto> certificateAttachmentQuery(@Path(value = "jobId") Long jobId,
      @Path(value = "certificateId") Long certId);

}
