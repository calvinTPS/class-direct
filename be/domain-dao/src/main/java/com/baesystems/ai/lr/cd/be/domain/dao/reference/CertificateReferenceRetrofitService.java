package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateTemplateHDto;
import com.baesystems.ai.lr.dto.references.CertificateActionTakenDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Retrofit certificate reference.
 * Created by fwijaya on 24/1/2017.
 */
public interface CertificateReferenceRetrofitService {


  /**
   * Get certificate templates.
   *
   * @return A list of {@link CertificateTemplateHDto}.
   */
  @GET("reference-data/job/certificate-templates")
  Call<List<CertificateTemplateHDto>> getCertificateTemplates();

  /**
   * Get certificate types.
   *
   * @return A list of {@link CertificateTypeDto}.
   */
  @GET("reference-data/job/certificate-types")
  Call<List<CertificateTypeDto>> getCertificateTypes();

  /**
   * Get statuses.
   *
   * @return A list of {@link CertificateStatusDto}.
   */
  @GET("reference-data/job/certificate-statuses")
  Call<List<CertificateStatusDto>> getCertificateStatuses();

  /**
   * Get list of certificate action taken.
   *
   * @return A list of {@link CertificateActionTakenDto}.
   */
  @GET("reference-data/job/certificate-actions-taken")
  Call<List<CertificateActionTakenDto>> getCertificateActionTaken();
}
