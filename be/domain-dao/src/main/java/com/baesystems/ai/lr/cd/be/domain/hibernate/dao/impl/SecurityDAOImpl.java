package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import com.baesystems.ai.lr.cd.be.domain.dto.security.MastSignature;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserRoles;
import org.springframework.util.CollectionUtils;

/**
 * @author vkovuru
 *
 */
@Repository
public class SecurityDAOImpl implements SecurityDAO {

  /**
   * Comma delimiter.
   */
  public static final String COMMA_DELIMITER = ",";

  /**
   * EntityManager Object.
   */
  @PersistenceContext
  private EntityManager entityManager;

  /*
   * (non-Javadoc)
   *
   * @see com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityService#hasRole(java.lang.String,
   * java.lang.String)
   */
  @Transactional(readOnly = true)
  @Override
  public boolean hasRole(final String userId, final List<String> roles) {
    boolean hasRole = false;
    final TypedQuery<UserRoles> userRolesQuery = entityManager.createQuery(
        "SELECT ur from UserRoles ur LEFT JOIN FETCH ur.role ro LEFT "
            + "JOIN FETCH ur.user u WHERE ro.roleName in :roles and u.userId= :userId ",
        UserRoles.class);
    userRolesQuery.setParameter("roles", roles).setParameter("userId", userId);
    final List<UserRoles> userRoles = userRolesQuery.getResultList();
    if (null != userRoles && !userRoles.isEmpty()) {
      hasRole = userRoles.stream().map(userRole -> userRole.getRole().getRoleName())
          .collect(Collectors.toList()).containsAll(roles);
    } else {
      hasRole = false;
    }
    return hasRole;
  }

  @Transactional(readOnly = true)
  @Override
  public MastSignature getActiveMastSignature() throws ClassDirectException {
    final TypedQuery<MastSignature> query = entityManager
      .createQuery("select signature from MastSignature signature where signature.enabled = true", MastSignature.class);
    final List<MastSignature> signatures = query.getResultList();
    if (CollectionUtils.isEmpty(signatures)) {
      throw new RecordNotFoundException("No signatures found.");
    }

    return signatures.get(0);
  }

  /**
   * @return value.
   */
  public final EntityManager getEntityManager() {
    return entityManager;
  }

  /**
   * @param entityManagerObject value.
   */
  public final void setEntityManager(final EntityManager entityManagerObject) {
    this.entityManager = entityManagerObject;
  }
}
