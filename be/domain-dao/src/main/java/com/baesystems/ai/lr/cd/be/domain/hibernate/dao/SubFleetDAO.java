package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import java.sql.SQLException;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author syalavarthi
 *
 */
public interface SubFleetDAO {

  /**
   * @param selectedUserId id.
   * @return value.
   * @throws RecordNotFoundException exception.
   */
  List<Long> getSubFleetById(final String selectedUserId) throws RecordNotFoundException;

  /**
   * @param selectedUserId id.
   * @param accessibleAssetIds ids.
   * @param restrictedAssetIds ids.
   * @return value.
   * @throws RecordNotFoundException exception.
   * @throws SQLException exception.
   */
  StatusDto saveSubFleet(final String selectedUserId, final List<Long> accessibleAssetIds,
      final List<Long> restrictedAssetIds) throws RecordNotFoundException, SQLException;


  /**
   * Returns the total sub fleet whitelist for a given user.
   * @param userId the user identifier.
   * @return the total sub fleet whitelist count.
   * @throws RecordNotFoundException if the given userId is invalid or not found.
   */
  long getSubFleetCountByUserId(final String userId) throws RecordNotFoundException;

}
