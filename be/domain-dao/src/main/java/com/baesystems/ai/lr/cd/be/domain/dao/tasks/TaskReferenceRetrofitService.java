package com.baesystems.ai.lr.cd.be.domain.dao.tasks;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.CheckListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedValueGroupsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedWorkItemAttributeValueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author VKolagutla
 * @author sBollu
 *
 */
public interface TaskReferenceRetrofitService {

  /**
   * @return list of work item types.
   */
  @GET("reference-data/task/postponement-types")
  Call<List<PostponementTypeHDto>> getPostponementTypes();

  /**
   * @return list of checklist.
   */
  @GET("reference-data/task/checklists")
  Call<List<CheckListHDto>> getChecklists();

  /**
   * @return list of checklist groups.
   */
  @GET("reference-data/task/checklist-groups")
  Call<List<ChecklistGroupDto>> getChecklistGroups();

  /**
   * @return list of checklist subgroups.
   */
  @GET("reference-data/task/checklist-subgroups")
  Call<List<ChecklistSubgroupDto>> getChecklistSubgroups();

  /**
   * @return list of allowed value groups.
   */
  @GET("reference-data/task/allowed-value-groups")
  Call<List<AllowedValueGroupsHDto>> getAllowedValueGroups();

  /**
   * @return list of allowed attribute values.
   */
  @GET("reference-data/task/allowed-attribute-values")
  Call<List<AllowedWorkItemAttributeValueHDto>> getAllowedAttributeValues();

}
