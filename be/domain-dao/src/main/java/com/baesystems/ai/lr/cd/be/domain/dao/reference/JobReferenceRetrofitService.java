package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemTypeHDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Retrofit JobReferenceService.
 *
 * @author yng
 *
 */
public interface JobReferenceRetrofitService {
  /**
   * Get list of job status.
   *
   * @return list of job status.
   */
  @GET("reference-data/job/job-statuses")
  Call<List<JobStatusHDto>> getJobStatuses();

  /**
   * Get list of report type.
   *
   * @return list of report type.
   */
  @GET("reference-data/job/report-types")
  Call<List<ReportTypeHDto>> getReportTypes();

  /**
   * @return list of work item types.
   */
  @GET("reference-data/job/work-item-types")
  Call<List<WorkItemTypeHDto>> getWorkItemTypes();

  /**
   * @return list of resolution statuses.
   */
  @GET("reference-data/job/resolution-statuses")
  Call<List<JobResolutionStatusHDto>> getResolutionStatuses();

  /**
   * Returns list of job locations.
   *
   * @return locations list.
   */
  @GET("reference-data/job/locations")
  Call<List<LocationHDto>> getLocations();
}
