package com.baesystems.ai.lr.cd.be.domain.dao.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Flag state service.
 *
 * @author msidek
 *
 */
public interface FlagStateRetrofitService {

  /**
   * Fetch all flags.
   *
   * @return caller
   *
   */
  @GET("flag-state?page=0&size=1000")
  Call<List<FlagStateHDto>> getAllFlags();

  /**
   * Fetch flags by size.
   *
   * @param page number.
   * @param size of result per page.
   * @return caller.
   *
   */
  @GET("flag-state?page={page}&size={size}")
  Call<List<FlagStateHDto>> getFlags(@Query("page") Integer page, @Query("size") Integer size);

  /**
   * Fetch flags filtered by search term.
   *
   * @param query term.
   * @return list of flags.
   *
   */
  @GET("flag-state?page=0&size=1000&search={query}")
  Call<List<FlagStateHDto>> getFlags(@Query("query") String query);

  /**
   * Get flag identified by id.
   *
   * @param id of flag.
   * @return flag.
   *
   */
  @GET("flag-state/{flagStateId}")
  Call<FlagStateHDto> getFlagById(@Path("flagStateId") Long id);
}
