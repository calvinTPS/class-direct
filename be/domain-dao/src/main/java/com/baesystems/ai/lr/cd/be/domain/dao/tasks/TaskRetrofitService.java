package com.baesystems.ai.lr.cd.be.domain.dao.tasks;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Provides access to Mast API calls.
 *
 * @author VKolagutla
 * @author sbollu.
 *
 */
public interface TaskRetrofitService {

  /**
   * Returns list of tasks by query parameters provided in query body.
   *
   * @param query the post body.
   * @return list of tasks.
   */
  @POST("task/query")
  Call<List<WorkItemLightHDto>> getTask(@Body WorkItemQueryDto query);

  /**
   * Updates list of tasks.
   *
   * @param workItemLightListDto list DTO of tasks.
   * @return updated list of tasks.
   */
  @PUT("task")
  Call<WorkItemLightListDto> updateTaskList(@Body WorkItemLightListDto workItemLightListDto);

  /**
   * Returns hierarchical list of tasks by query parameters provided in query body.
   *
   * @param query the post body.
   * @param includeAttributesAndActions include attributes flag.
   * @return list DTO of asset work items.
   */
  @POST("work-item/query")
  Call<WorkItemPreviewListHDto> getHierarchicalTask(@Body WorkItemQueryDto query,
    @Query("includeAttributesAndActions") Boolean includeAttributesAndActions);

}
