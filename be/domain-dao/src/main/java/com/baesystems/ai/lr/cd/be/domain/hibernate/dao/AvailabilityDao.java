package com.baesystems.ai.lr.cd.be.domain.hibernate.dao;

import com.baesystems.ai.lr.cd.be.domain.repositories.SystemMaintenance;

/**
 * Dao to query Availability object.
 *
 * @author Faizal Sidek
 */
public interface AvailabilityDao {

  /**
   * Get active maintenance window. Returns null if no maintenance.
   *
   * @return maintenance or null.
   */
  SystemMaintenance getActiveSystemMaintenance();
}
