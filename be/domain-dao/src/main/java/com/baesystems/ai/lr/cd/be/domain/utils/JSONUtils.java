package com.baesystems.ai.lr.cd.be.domain.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author syalavarthi.
 *
 */
public final class JSONUtils {
  /**
   * Empty constructor.
   */
  private JSONUtils() {

  }

  /**
   * to check given string is valid json string or not.
   *
   * @param jsonString jsonString.
   * @return value.
   */
  public static boolean isJSONValid(final String jsonString) {
    try {
      new JSONObject(jsonString);
    } catch (JSONException ex) {
      try {
        new JSONArray(jsonString);
      } catch (JSONException ex1) {
        return false;
      }
    }
    return true;
  }

  /**
   * to generate json string using status dto.
   *
   * @param responseCode responseCode.
   * @return value
   * @throws JsonProcessingException exception.
   */
  public static String generateStatusDtoJson(final int responseCode) throws JsonProcessingException {

    // assuming statusDto is below:
    StatusDto status = new StatusDto();
    status.setStatus(String.valueOf(responseCode));

    ObjectMapper mapper = new ObjectMapper();
    String json = mapper.writeValueAsString(status);

    return json;
  }
}
