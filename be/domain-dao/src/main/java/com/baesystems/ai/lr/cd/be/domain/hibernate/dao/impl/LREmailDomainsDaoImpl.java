package com.baesystems.ai.lr.cd.be.domain.hibernate.dao.impl;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.LREmailDomainsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.LREmailDomains;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Provides data access service to get lr email domains from database.
 *
 * @author fwijaya on 28/4/2017.
 */
@Repository
public class LREmailDomainsDaoImpl implements LREmailDomainsDao {
  /**
   * Log4J Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(LREmailDomainsDaoImpl.class);

  /**
   * EntityManager Object.
   */
  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Returns list of LR email domains.
   *
   * @return The LR email domains.
   */
  @Override
  @Transactional
  public final List<LREmailDomains> getLrEmailDomains() {
    LOGGER.debug("Get LR email domains");
    final String getQuery = "select domains from LREmailDomains domains";
    return entityManager.createQuery(getQuery).getResultList();
  }
}
