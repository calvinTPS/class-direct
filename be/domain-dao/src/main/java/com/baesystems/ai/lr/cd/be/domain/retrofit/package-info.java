/**
 * Root package for Retrofit related config, classes, etc.
 *
 * @author Faizal Sidek
 *
 */
package com.baesystems.ai.lr.cd.be.domain.retrofit;
