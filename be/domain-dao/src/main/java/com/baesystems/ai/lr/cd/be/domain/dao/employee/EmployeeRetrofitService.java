package com.baesystems.ai.lr.cd.be.domain.dao.employee;

import java.util.List;

import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Customer Retrofit Service to retrieve employee Name.
 */
public interface EmployeeRetrofitService {

  /**
   * Gets employee list.
   *
   * @return employee list.
   */
  @GET("reference-data/employee/employee")
  Call<List<LrEmployeeDto>> getEmployee();

  /**
   * Gets office list.
   *
   * @return A list of {@link OfficeDto}.
   */
  @GET("reference-data/employee/office")
  Call<List<OfficeDto>> getOffice();
}
