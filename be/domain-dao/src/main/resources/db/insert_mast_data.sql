-- assigning client id 4 with asset 2, 3, 4, 5 and 6
INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (2, 1, 4, 3), (2, 1, 4, 2), (2, 1, 4, 14), (2, 1, 4, 4), (2, 1, 4, 6), (2, 1, 4, 10);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (3, 1, 4, 3), (3, 1, 4, 2), (3, 1, 4, 14), (3, 1, 4, 4), (3, 1, 4, 6), (3, 1, 4, 10);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (4, 1, 4, 3), (4, 1, 4, 2), (4, 1, 4, 14), (4, 1, 4, 4), (4, 1, 4, 6), (4, 1, 4, 10);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (5, 1, 4, 3), (5, 1, 4, 2), (5, 1, 4, 14), (5, 1, 4, 4), (5, 1, 4, 6), (5, 1, 4, 10);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (6, 1, 4, 3), (6, 1, 4, 2), (6, 1, 4, 14), (6, 1, 4, 4), (6, 1, 4, 6), (6, 1, 4, 10);

-- update above client with appropriate imo number
UPDATE mast_db.mast_cdh_party SET imo_number = 100100 WHERE id = 4;
UPDATE mast_db.mast_cdh_party SET imo_number = 100101 WHERE id = 5;

-- assigning flag state id 4 (flag code ALA) to asset 3, 4, 5, 6, 7, 8, 9
update mast_db.mast_asset_versionedasset set flag_state_id = 4 where id in (3,4,5,6,7,8,9);

-- assigning ship builder id 5 to asset 3,4,5,6,7,8
INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (3, 1, 5, 8);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (4, 1, 5, 8);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (5, 1, 5, 8);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (6, 1, 5, 8);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (7, 1, 5, 8);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (8, 1, 5, 8);

INSERT INTO mast_db.mast_asset_asset_party (asset_id, asset_version_id, party_id, party_role_id)
VALUES (9, 1, 5, 8);

