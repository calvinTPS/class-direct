
package org.lr.internal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DownloadShipDocsEBResult" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "downloadShipDocsEBResult"
})
@XmlRootElement(name = "DownloadShipDocsEBResponse")
public class DownloadShipDocsEBResponse {

    @XmlElement(name = "DownloadShipDocsEBResult")
    protected byte[] downloadShipDocsEBResult;

    /**
     * Gets the value of the downloadShipDocsEBResult property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDownloadShipDocsEBResult() {
        return downloadShipDocsEBResult;
    }

    /**
     * Sets the value of the downloadShipDocsEBResult property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDownloadShipDocsEBResult(byte[] value) {
        this.downloadShipDocsEBResult = value;
    }

}
