
package org.lr.internal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FORMno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SelectiveFilesOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "forMno",
    "selectiveFilesOnly"
})
@XmlRootElement(name = "DownloadShipDocs")
public class DownloadShipDocs {

    @XmlElement(name = "FORMno")
    protected String forMno;
    @XmlElement(name = "SelectiveFilesOnly")
    protected boolean selectiveFilesOnly;

    /**
     * Gets the value of the forMno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFORMno() {
        return forMno;
    }

    /**
     * Sets the value of the forMno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFORMno(String value) {
        this.forMno = value;
    }

    /**
     * Gets the value of the selectiveFilesOnly property.
     * 
     */
    public boolean isSelectiveFilesOnly() {
        return selectiveFilesOnly;
    }

    /**
     * Sets the value of the selectiveFilesOnly property.
     * 
     */
    public void setSelectiveFilesOnly(boolean value) {
        this.selectiveFilesOnly = value;
    }

}
