
package org.lr.internal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Obj_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Obj_Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LR_Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "objID",
    "objType",
    "lrNumber"
})
@XmlRootElement(name = "GetDocAttributes")
public class GetDocAttributes {

    @XmlElement(name = "Obj_ID")
    protected String objID;
    @XmlElement(name = "Obj_Type")
    protected String objType;
    @XmlElement(name = "LR_Number")
    protected String lrNumber;

    /**
     * Gets the value of the objID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjID() {
        return objID;
    }

    /**
     * Sets the value of the objID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjID(String value) {
        this.objID = value;
    }

    /**
     * Gets the value of the objType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjType() {
        return objType;
    }

    /**
     * Sets the value of the objType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjType(String value) {
        this.objType = value;
    }

    /**
     * Gets the value of the lrNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLRNumber() {
        return lrNumber;
    }

    /**
     * Sets the value of the lrNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLRNumber(String value) {
        this.lrNumber = value;
    }

}
