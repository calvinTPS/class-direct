
package org.lr.internal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isRoot" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="LR_Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Class_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Object_type" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ErrIndicator" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "isRoot",
    "lrNumber",
    "classID",
    "objectType",
    "errIndicator"
})
@XmlRootElement(name = "GetDocTree")
public class GetDocTree {

    protected boolean isRoot;
    @XmlElement(name = "LR_Number")
    protected String lrNumber;
    @XmlElement(name = "Class_ID")
    protected int classID;
    @XmlElement(name = "Object_type")
    protected int objectType;
    @XmlElement(name = "ErrIndicator")
    protected int errIndicator;

    /**
     * Gets the value of the isRoot property.
     * 
     */
    public boolean isIsRoot() {
        return isRoot;
    }

    /**
     * Sets the value of the isRoot property.
     * 
     */
    public void setIsRoot(boolean value) {
        this.isRoot = value;
    }

    /**
     * Gets the value of the lrNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLRNumber() {
        return lrNumber;
    }

    /**
     * Sets the value of the lrNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLRNumber(String value) {
        this.lrNumber = value;
    }

    /**
     * Gets the value of the classID property.
     * 
     */
    public int getClassID() {
        return classID;
    }

    /**
     * Sets the value of the classID property.
     * 
     */
    public void setClassID(int value) {
        this.classID = value;
    }

    /**
     * Gets the value of the objectType property.
     * 
     */
    public int getObjectType() {
        return objectType;
    }

    /**
     * Sets the value of the objectType property.
     * 
     */
    public void setObjectType(int value) {
        this.objectType = value;
    }

    /**
     * Gets the value of the errIndicator property.
     * 
     */
    public int getErrIndicator() {
        return errIndicator;
    }

    /**
     * Sets the value of the errIndicator property.
     * 
     */
    public void setErrIndicator(int value) {
        this.errIndicator = value;
    }

}
