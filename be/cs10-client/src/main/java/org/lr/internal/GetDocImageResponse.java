
package org.lr.internal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetDocImageResult" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDocImageResult"
})
@XmlRootElement(name = "GetDocImageResponse")
public class GetDocImageResponse {

    @XmlElement(name = "GetDocImageResult")
    protected byte[] getDocImageResult;

    /**
     * Gets the value of the getDocImageResult property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getGetDocImageResult() {
        return getDocImageResult;
    }

    /**
     * Sets the value of the getDocImageResult property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setGetDocImageResult(byte[] value) {
        this.getDocImageResult = value;
    }

}
