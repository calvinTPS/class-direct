
package org.lr.internal;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.lr.internal package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://localhost/eb_Service", "string");
    private final static QName _Base64Binary_QNAME = new QName("http://localhost/eb_Service", "base64Binary");
    private final static QName _AnyType_QNAME = new QName("http://localhost/eb_Service", "anyType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.lr.internal
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetICMSList }
     * 
     */
    public GetICMSList createGetICMSList() {
        return new GetICMSList();
    }

    /**
     * Create an instance of {@link GetICMSListResponse }
     * 
     */
    public GetICMSListResponse createGetICMSListResponse() {
        return new GetICMSListResponse();
    }

    /**
     * Create an instance of {@link GetICMSListEB }
     * 
     */
    public GetICMSListEB createGetICMSListEB() {
        return new GetICMSListEB();
    }

    /**
     * Create an instance of {@link GetICMSListEBResponse }
     * 
     */
    public GetICMSListEBResponse createGetICMSListEBResponse() {
        return new GetICMSListEBResponse();
    }

    /**
     * Create an instance of {@link GetCertList }
     * 
     */
    public GetCertList createGetCertList() {
        return new GetCertList();
    }

    /**
     * Create an instance of {@link GetCertListResponse }
     * 
     */
    public GetCertListResponse createGetCertListResponse() {
        return new GetCertListResponse();
    }

    /**
     * Create an instance of {@link GetCertListEB }
     * 
     */
    public GetCertListEB createGetCertListEB() {
        return new GetCertListEB();
    }

    /**
     * Create an instance of {@link GetCertListEBResponse }
     * 
     */
    public GetCertListEBResponse createGetCertListEBResponse() {
        return new GetCertListEBResponse();
    }

    /**
     * Create an instance of {@link GetCertListClientnFlagState }
     * 
     */
    public GetCertListClientnFlagState createGetCertListClientnFlagState() {
        return new GetCertListClientnFlagState();
    }

    /**
     * Create an instance of {@link GetCertListClientnFlagStateResponse }
     * 
     */
    public GetCertListClientnFlagStateResponse createGetCertListClientnFlagStateResponse() {
        return new GetCertListClientnFlagStateResponse();
    }

    /**
     * Create an instance of {@link GetCertListClientnFlagStateEB }
     * 
     */
    public GetCertListClientnFlagStateEB createGetCertListClientnFlagStateEB() {
        return new GetCertListClientnFlagStateEB();
    }

    /**
     * Create an instance of {@link GetCertListClientnFlagStateEBResponse }
     * 
     */
    public GetCertListClientnFlagStateEBResponse createGetCertListClientnFlagStateEBResponse() {
        return new GetCertListClientnFlagStateEBResponse();
    }

    /**
     * Create an instance of {@link DownloadShipCerts }
     * 
     */
    public DownloadShipCerts createDownloadShipCerts() {
        return new DownloadShipCerts();
    }

    /**
     * Create an instance of {@link DownloadShipCertsResponse }
     * 
     */
    public DownloadShipCertsResponse createDownloadShipCertsResponse() {
        return new DownloadShipCertsResponse();
    }

    /**
     * Create an instance of {@link DownloadShipDocs }
     * 
     */
    public DownloadShipDocs createDownloadShipDocs() {
        return new DownloadShipDocs();
    }

    /**
     * Create an instance of {@link DownloadShipDocsResponse }
     * 
     */
    public DownloadShipDocsResponse createDownloadShipDocsResponse() {
        return new DownloadShipDocsResponse();
    }

    /**
     * Create an instance of {@link DownloadShipDocsEB }
     * 
     */
    public DownloadShipDocsEB createDownloadShipDocsEB() {
        return new DownloadShipDocsEB();
    }

    /**
     * Create an instance of {@link DownloadShipDocsEBResponse }
     * 
     */
    public DownloadShipDocsEBResponse createDownloadShipDocsEBResponse() {
        return new DownloadShipDocsEBResponse();
    }

    /**
     * Create an instance of {@link ConnectURL }
     * 
     */
    public ConnectURL createConnectURL() {
        return new ConnectURL();
    }

    /**
     * Create an instance of {@link ConnectURLResponse }
     * 
     */
    public ConnectURLResponse createConnectURLResponse() {
        return new ConnectURLResponse();
    }

    /**
     * Create an instance of {@link FindBusinessSample }
     * 
     */
    public FindBusinessSample createFindBusinessSample() {
        return new FindBusinessSample();
    }

    /**
     * Create an instance of {@link FindBusinessSampleResponse }
     * 
     */
    public FindBusinessSampleResponse createFindBusinessSampleResponse() {
        return new FindBusinessSampleResponse();
    }

    /**
     * Create an instance of {@link DownloadClientDocs }
     * 
     */
    public DownloadClientDocs createDownloadClientDocs() {
        return new DownloadClientDocs();
    }

    /**
     * Create an instance of {@link DownloadClientDocsResponse }
     * 
     */
    public DownloadClientDocsResponse createDownloadClientDocsResponse() {
        return new DownloadClientDocsResponse();
    }

    /**
     * Create an instance of {@link GetDocAttributes }
     * 
     */
    public GetDocAttributes createGetDocAttributes() {
        return new GetDocAttributes();
    }

    /**
     * Create an instance of {@link GetDocAttributesResponse }
     * 
     */
    public GetDocAttributesResponse createGetDocAttributesResponse() {
        return new GetDocAttributesResponse();
    }

    /**
     * Create an instance of {@link GetDocAttributesEB }
     * 
     */
    public GetDocAttributesEB createGetDocAttributesEB() {
        return new GetDocAttributesEB();
    }

    /**
     * Create an instance of {@link GetDocAttributesEBResponse }
     * 
     */
    public GetDocAttributesEBResponse createGetDocAttributesEBResponse() {
        return new GetDocAttributesEBResponse();
    }

    /**
     * Create an instance of {@link GetDocImage }
     * 
     */
    public GetDocImage createGetDocImage() {
        return new GetDocImage();
    }

    /**
     * Create an instance of {@link GetDocImageResponse }
     * 
     */
    public GetDocImageResponse createGetDocImageResponse() {
        return new GetDocImageResponse();
    }

    /**
     * Create an instance of {@link GetDocImageEB }
     * 
     */
    public GetDocImageEB createGetDocImageEB() {
        return new GetDocImageEB();
    }

    /**
     * Create an instance of {@link GetDocImageEBResponse }
     * 
     */
    public GetDocImageEBResponse createGetDocImageEBResponse() {
        return new GetDocImageEBResponse();
    }

    /**
     * Create an instance of {@link GetConnectInfo }
     * 
     */
    public GetConnectInfo createGetConnectInfo() {
        return new GetConnectInfo();
    }

    /**
     * Create an instance of {@link GetConnectInfoResponse }
     * 
     */
    public GetConnectInfoResponse createGetConnectInfoResponse() {
        return new GetConnectInfoResponse();
    }

    /**
     * Create an instance of {@link GetDocTree }
     * 
     */
    public GetDocTree createGetDocTree() {
        return new GetDocTree();
    }

    /**
     * Create an instance of {@link GetDocTreeResponse }
     * 
     */
    public GetDocTreeResponse createGetDocTreeResponse() {
        return new GetDocTreeResponse();
    }

    /**
     * Create an instance of {@link GetDocTreeEB }
     * 
     */
    public GetDocTreeEB createGetDocTreeEB() {
        return new GetDocTreeEB();
    }

    /**
     * Create an instance of {@link GetDocTreeEBResponse }
     * 
     */
    public GetDocTreeEBResponse createGetDocTreeEBResponse() {
        return new GetDocTreeEBResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost/eb_Service", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost/eb_Service", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost/eb_Service", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

}
