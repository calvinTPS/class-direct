
package org.lr.internal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetICMSListEBResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getICMSListEBResult"
})
@XmlRootElement(name = "GetICMSListEBResponse")
public class GetICMSListEBResponse {

    @XmlElement(name = "GetICMSListEBResult")
    protected String getICMSListEBResult;

    /**
     * Gets the value of the getICMSListEBResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetICMSListEBResult() {
        return getICMSListEBResult;
    }

    /**
     * Sets the value of the getICMSListEBResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetICMSListEBResult(String value) {
        this.getICMSListEBResult = value;
    }

}
