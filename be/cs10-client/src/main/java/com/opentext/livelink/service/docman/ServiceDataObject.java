
package com.opentext.livelink.service.docman;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceDataObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceDataObject"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceDataObject")
@XmlSeeAlso({
    NodeRightUpdateInfo.class,
    GetNodesInContainerOptions.class,
    Node.class,
    Metadata.class,
    MultilingualMetadata.class,
    Version.class,
    MetadataLanguage.class,
    CollectionItem.class,
    ReportResult.class,
    CategoryInheritance.class,
    NodePosition.class,
    NodePageSpecification.class,
    NodePageResult.class,
    CopyOptions.class,
    MoveOptions.class,
    CompoundDocRelease.class,
    NodeAuditRecord.class,
    PagedNodeAuditData.class,
    NodeRights.class,
    NodeRight.class,
    CategoryItemsUpgradeInfo.class,
    AttributeGroup.class,
    AttributeGroupDefinition.class,
    NodeContainerInfo.class,
    NodeFeature.class,
    NodePermissions.class,
    NodeReferenceInfo.class,
    NodeReservationInfo.class,
    NodeVersionInfo.class,
    Attribute.class
})
public abstract class ServiceDataObject {


}
