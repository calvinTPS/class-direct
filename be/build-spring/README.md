# build-spring

This module keeps the dependencies related to Spring framework.
Import wherever it's needed using type *pom*

```xml
<dependency>
    <groupId>com.baesystemsai.lr.cd.be</groupId>
    <artifactId>build-spring</artifactId>
    <version>${project.version}</version>
    <type>pom</type>
</dependency>
```