package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * To be used on fields that contain other objects or lists of other objects that should be validated. This works like
 * the standard {@link javax.validation.Valid} annotation for fields, but also includes the option to specify a list of
 * potential target classes for fields where the type is fixed.
 *
 * It will validate as each potential target class on the assumption that the JSON parser will only be able to
 * de-serialise into one of them. If this is successful for more than one (e.g. one extends the other then errors may be
 * duplicated)
 *
 * Fields that use this annotation should not use {@link javax.validation.Valid} and ideally classes were this is used
 * will use it for all fields that need it and not use {@link javax.validation.Valid}. Sub classes and classes that
 * appear as fields in a class that uses this annotation should do the same. Mixing this annotation with
 * {@link javax.validation.Valid} annotations won't break anything it just risks potential duplication of the validation
 * and repeated error messages
 *
 * There is some risk here that the field names given in the JSON could match more than one class so care should be
 * taken when choosing field names for the classes in this list.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Validate
{
    /**
     * All classes that the field with this annotation should be validated as. These classes should also use this
     * annotation in stead of {@link javax.validation.Valid} if possible.
     */
    Class<?>[] AsClasses() default {};
}
