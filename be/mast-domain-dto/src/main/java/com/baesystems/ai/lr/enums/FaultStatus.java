package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.List;

public enum FaultStatus
{
    OPEN(1L),
    CLOSED(2L),
    CANCELLED(3L);

    private final Long value;

    FaultStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    /**
     * @return a list of FaultStatusType values that count as open
     */
    public static final List<Long> getFaultStatusesForOpen()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(OPEN.getValue());

        return statuses;
    }
}
