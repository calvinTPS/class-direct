package com.baesystems.ai.lr.enums;

public enum ProductType
{
    CLASSIFICATION(1L),
    MMS(2L),
    STATUTORY(3L);

    private final Long value;

    ProductType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
