package com.baesystems.ai.lr.dto.base;

import java.util.Date;

public class ReportContentElementDto extends AuditedDto
{
    private static final long serialVersionUID = -9126469560742271300L;

    private Date actionTakenDate;

    public Date getActionTakenDate()
    {
        return actionTakenDate;
    }

    public void setActionTakenDate(final Date actionTakenDate)
    {
        this.actionTakenDate = actionTakenDate;
    }
}
