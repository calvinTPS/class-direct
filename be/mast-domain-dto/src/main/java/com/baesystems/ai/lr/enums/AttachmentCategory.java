package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.List;

public enum AttachmentCategory
{
    CERTIFICATE(1L),
    THICKNESS_MEASUREMENT_REPORT_ARGONAUT(2L),
    THICKNESS_MEASUREMENT_REPORT_LONG(3L),
    THICKNESS_MEASUREMENT_REPORT_SHORT(4L),
    STATUTORY_RECORD(5L),
    EXECUTIVE_HULL_SUMMARY(6L),
    REPAIR(7L),
    MEETING_MINUTES(8L),
    PR_17(9L),
    SURVEY_REPORT(10L),
    TOC_COF_INSTRUCTIONS(11L),
    OTHER_CORRESPONDENCE(12L),
    OTHER(13L);


    private final Long value;

    AttachmentCategory(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }

    public static List<Long> getThicknessMeasurementCategories()
    {
        final List<Long> thicknessMeasurementIds = new ArrayList<Long>();

        thicknessMeasurementIds.add(THICKNESS_MEASUREMENT_REPORT_ARGONAUT.getValue());
        thicknessMeasurementIds.add(THICKNESS_MEASUREMENT_REPORT_LONG.getValue());
        thicknessMeasurementIds.add(THICKNESS_MEASUREMENT_REPORT_SHORT.getValue());

        return thicknessMeasurementIds;
    }
}
