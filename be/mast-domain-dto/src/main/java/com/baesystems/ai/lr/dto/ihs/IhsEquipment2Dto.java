package com.baesystems.ai.lr.dto.ihs;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsEquipment2Dto extends IhsBaseDto
{
    private static final long serialVersionUID = 4536068280072591992L;

    @Min(value = 0)
    private Double cableLength;

    private String cableGrade;

    @Size(message = "invalid length", max = 5)
    private String cableDiameter;

    public Double getCableLength()
    {
        return cableLength;
    }

    public void setCableLength(final Double cableLength)
    {
        this.cableLength = cableLength;
    }

    public String getCableGrade()
    {
        return cableGrade;
    }

    public void setCableGrade(final String cableGrade)
    {
        this.cableGrade = cableGrade;
    }

    public String getCableDiameter()
    {
        return cableDiameter;
    }

    public void setCableDiameter(final String cableDiameter)
    {
        this.cableDiameter = cableDiameter;
    }

}
