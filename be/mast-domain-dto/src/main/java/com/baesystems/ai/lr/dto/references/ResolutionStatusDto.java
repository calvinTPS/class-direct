package com.baesystems.ai.lr.dto.references;

public class ResolutionStatusDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -4128210802473390510L;

    private String code;

    public String getCode()
    {
        return this.code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }
}
