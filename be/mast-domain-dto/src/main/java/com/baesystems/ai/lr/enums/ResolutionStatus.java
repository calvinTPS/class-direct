package com.baesystems.ai.lr.enums;

public enum ResolutionStatus
{
    COMPLETE(1L),
    CONFIRMATORY_CHECK(2L),
    WAIVED(3L),
    NOT_APPLICABLE(4L),
    POSTPONED(6L);

    private final Long value;

    private ResolutionStatus(final Long value)
    {
        this.value = value;
    }

    public Long getValue()
    {
        return this.value;
    }
}
