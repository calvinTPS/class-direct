package com.baesystems.ai.lr.dto.query;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.JoinType;

import com.baesystems.ai.lr.dto.annotations.RequiredWithFields;
import com.baesystems.ai.lr.dto.annotations.Validate;
import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;

@RequiredWithFields.List({@RequiredWithFields(keyField = "joinType", fields = {"joinField"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "alias", fields = {"joinField"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "and", fields = {"field", "value", "relationship", "or", "not"}, shouldBePopulated = false),
                          @RequiredWithFields(keyField = "or", fields = {"field", "value", "relationship", "and", "not"}, shouldBePopulated = false),
                          @RequiredWithFields(keyField = "not", fields = {"field", "value", "relationship", "and", "or"}, shouldBePopulated = false),
                          @RequiredWithFields(keyField = "field", fields = {"and", "or", "not"}, shouldBePopulated = false),
                          @RequiredWithFields(keyField = "field", fields = {"value", "relationship"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "value", fields = {"and", "or", "not"}, shouldBePopulated = false),
                          @RequiredWithFields(keyField = "value", fields = {"field", "relationship"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "relationship", fields = {"and", "or", "not"}, shouldBePopulated = false),
                          @RequiredWithFields(keyField = "relationship", fields = {"field", "value"}, shouldBePopulated = true)
})
public class AbstractQueryDto implements Serializable, Dto
{
    private static final long serialVersionUID = 5668266565854162719L;

    private String joinField;

    private JoinType joinType;

    // to uniquely identify joined entities if the same thing needs to be joined more than once.
    private String alias;

    @Validate
    private List<AbstractQueryDto> and;

    @Validate
    private List<AbstractQueryDto> or;

    @Validate
    private AbstractQueryDto not;

    private String field;

    @Validate(AsClasses = {FieldDto.class, AbstractSubQueryDto.class})
    private Object value;

    private QueryRelationshipType relationship;

    // This is used on rare occasions where the same query is used on two repositories and some of the fields are not
    // present in both and only work if the repository is set up of it. Setting null will throw an error if the field is
    // missing on the DO (as normal). True will allow entities to be selected (provided they fulfil the other criteria).
    // False will not select entities unless they have the field on the DO. Calling inside a not will invert this.
    private Boolean selectWhenFieldIsMissing;

    public String getJoinField()
    {
        return this.joinField;
    }

    public void setJoinField(final String joinField)
    {
        this.joinField = joinField;
    }

    public JoinType getJoinType()
    {
        return this.joinType;
    }

    public void setJoinType(final JoinType joinType)
    {
        this.joinType = joinType;
    }

    public String getAlias()
    {
        return this.alias;
    }

    public void setAlias(final String alias)
    {
        this.alias = alias;
    }

    public List<AbstractQueryDto> getAnd()
    {
        return this.and;
    }

    public void setAnd(final List<AbstractQueryDto> and)
    {
        this.and = and;
    }

    public List<AbstractQueryDto> getOr()
    {
        return this.or;
    }

    public void setOr(final List<AbstractQueryDto> or)
    {
        this.or = or;
    }

    public AbstractQueryDto getNot()
    {
        return this.not;
    }

    public void setNot(final AbstractQueryDto not)
    {
        this.not = not;
    }

    public String getField()
    {
        return this.field;
    }

    public void setField(final String field)
    {
        this.field = field;
    }

    public Object getValue()
    {
        return this.value;
    }

    public void setValue(final Object value)
    {
        this.value = value;
    }

    public QueryRelationshipType getRelationship()
    {
        return this.relationship;
    }

    public void setRelationship(final QueryRelationshipType relationship)
    {
        this.relationship = relationship;
    }

    public Boolean getSelectWhenFieldIsMissing()
    {
        return this.selectWhenFieldIsMissing;
    }

    public void setSelectWhenFieldIsMissing(final Boolean selectWhenFieldIsMissing)
    {
        this.selectWhenFieldIsMissing = selectWhenFieldIsMissing;
    }
}
