package com.baesystems.ai.lr.dto;

import java.io.Serializable;
import java.util.List;

import com.baesystems.ai.lr.enums.JobBundleEntityType;

public final class JobBundleConflictReportDto implements Serializable
{
    private static final long serialVersionUID = 5009395806779335889L;

    // nulls to assign as defaults for final fields in constructors to avoid pmd errors.
    private static final JobBundleEntityType JOB_BUNDLE_TYPE_NULL = null;

    private static final Boolean BOOLEAN_NULL = null;

    private static final Long LONG_NULL = null;

    private static final List<ConflictedFieldSummaryDto> NULL_LIST = null;

    private final JobBundleEntityType jobBundleType;

    private String jobBundleFieldName;

    private final Long objectId;

    private final Long objectVersionId;

    private String objectDisplayKey;

    private final List<ConflictedFieldSummaryDto> conflictedFields;

    private final Boolean overwritten;

    public <T extends UpdateableJobBundleEntity> JobBundleConflictReportDto(final JobBundleEntityType field, final Long objectId,
            final Long objectVersionId, final ConflictResolutionOutcome<T> outcome)
    {
        if (outcome != null)
        {
            this.jobBundleType = field;
            this.objectId = objectId;
            this.objectVersionId = objectVersionId;
            this.conflictedFields = outcome.getConflictedFields();
            this.overwritten = outcome.getRetryUpdate();
        }
        else
        {
            this.jobBundleType = JOB_BUNDLE_TYPE_NULL;
            this.objectId = LONG_NULL;
            this.objectVersionId = LONG_NULL;
            this.conflictedFields = NULL_LIST;
            this.overwritten = BOOLEAN_NULL;
        }
    }

    public JobBundleEntityType getJobBundleType()
    {
        return this.jobBundleType;
    }

    public String getJobBundleFieldName()
    {
        return this.jobBundleFieldName;
    }

    public void setJobBundleFieldName(final String jobBundleFieldName)
    {
        this.jobBundleFieldName = jobBundleFieldName;
    }

    public Long getObjectId()
    {
        return this.objectId;
    }

    public Long getObjectVersionId()
    {
        return this.objectVersionId;
    }

    public List<ConflictedFieldSummaryDto> getConflictedFields()
    {
        return this.conflictedFields;
    }

    public Boolean getOverwritten()
    {
        return this.overwritten;
    }

    public String getObjectDisplayKey()
    {
        return this.objectDisplayKey;
    }

    public void setObjectDisplayKey(final String objectDisplayKey)
    {
        this.objectDisplayKey = objectDisplayKey;
    }
}
