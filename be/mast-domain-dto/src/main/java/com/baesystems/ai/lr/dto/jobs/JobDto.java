package com.baesystems.ai.lr.dto.jobs;

import static com.baesystems.ai.lr.dto.jobs.JobDto.ENTITY_NAME;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.FieldLessThanOrEqual;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
@FieldLessThanOrEqual.List({
                            @FieldLessThanOrEqual(first = "firstVisitDate", second = "lastVisitDate", useTime = false,
                                    message = "The last visit date cannot be before the first visit date")})
public class JobDto extends AuditedDto implements UpdateableJobBundleEntity
{
    private static final long serialVersionUID = 8289884782043777809L;
    public static final String ENTITY_NAME = "Job";

    private String stalenessHash;

    @Size(message = "invalid length", min = 1, max = 20)
    private String jobNumber;

    @ConflictAware
    @Size(message = "invalid length", max = 2500)
    private String note;

    @ConflictAware
    private Boolean requestByTelephoneIndicator;

    @NotNull
    @ConflictAware
    private Boolean scopeConfirmed;

    @ConflictAware
    private Date scopeConfirmedDate;

    @Valid
    @ConflictAware
    private LinkResource scopeConfirmedBy;

    @ConflictAware
    private Date requestedAttendanceDate;

    @ConflictAware
    @Size(message = "invalid length", max = 500)
    private String statusReason;

    @ConflictAware
    private Long workOrderNumber;

    @ConflictAware
    private Date writtenServiceRequestReceivedDate;

    @ConflictAware
    private Date writtenServiceResponseSentDate;

    @NotNull
    @Valid
    @ConflictAware
    private LinkVersionedResource asset;

    @Valid
    @ConflictAware
    private LinkResource aCase;

    @NotNull
    @Valid
    @ConflictAware
    private List<OfficeLinkDto> offices;

    @Valid
    @ConflictAware
    private List<EmployeeLinkDto> employees;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.JOB_STATUSES))
    private LinkResource jobStatus;

    @ConflictAware
    private Date createdOn;

    @ConflictAware
    private Date etaDate;

    @ConflictAware
    private Date etdDate;

    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.LOCATIONS))
    @Valid
    private LinkResource location;

    @ConflictAware
    @Size(message = "invalid length", max = 50)
    private String description;

    @ConflictAware
    private Date firstVisitDate;

    @ConflictAware
    private Date lastVisitDate;

    @ConflictAware
    private Date completedOn;

    @ConflictAware
    private String completedBy;

    @ConflictAware
    private Boolean zeroVisitJob;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.FLAGS))
    private LinkResource proposedFlagState;

    @ConflictAware
    private Date updatedOn;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LinkResource updatedOnBehalfOf;

    @ConflictAware
    private Boolean classGroupJob;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.JOB_CATEGORIES))
    private LinkResource jobCategory;

    @Valid
    @ConflictAware
    private LinkResource jobTeam;

    @ConflictAware
    private Boolean reopened;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CLASS_RECOMMENDATIONS))
    private LinkResource classRecommendation;

    @ConflictAware
    @Size(message = "invalid length", max = 2000)
    private String classRecommendationNarrative;

    @Valid
    @ConflictAware
    private Boolean finishCreditable;

    private String controlNumber;

    private Boolean migrated;

    @Size(message = "invalid length", max = 2000)
    private String reportedServiceCodes;

    public String getJobNumber()
    {
        return this.jobNumber;
    }

    public void setJobNumber(final String jobNumber)
    {
        this.jobNumber = jobNumber;
    }

    public String getNote()
    {
        return this.note;
    }

    public void setNote(final String note)
    {
        this.note = note;
    }

    public Boolean getRequestByTelephoneIndicator()
    {
        return this.requestByTelephoneIndicator;
    }

    public void setRequestByTelephoneIndicator(final Boolean requestByTelephoneIndicator)
    {
        this.requestByTelephoneIndicator = requestByTelephoneIndicator;
    }

    public Date getRequestedAttendanceDate()
    {
        return this.requestedAttendanceDate;
    }

    public void setRequestedAttendanceDate(final Date requestedAttendanceDate)
    {
        this.requestedAttendanceDate = requestedAttendanceDate;
    }

    public String getStatusReason()
    {
        return this.statusReason;
    }

    public void setStatusReason(final String statusReason)
    {
        this.statusReason = statusReason;
    }

    public Long getWorkOrderNumber()
    {
        return this.workOrderNumber;
    }

    public void setWorkOrderNumber(final Long workOrderNumber)
    {
        this.workOrderNumber = workOrderNumber;
    }

    public Date getWrittenServiceRequestReceivedDate()
    {
        return this.writtenServiceRequestReceivedDate;
    }

    public void setWrittenServiceRequestReceivedDate(final Date writtenServiceRequestReceivedDate)
    {
        this.writtenServiceRequestReceivedDate = writtenServiceRequestReceivedDate;
    }

    public Date getWrittenServiceResponseSentDate()
    {
        return this.writtenServiceResponseSentDate;
    }

    public void setWrittenServiceResponseSentDate(final Date writtenServiceResponseSentDate)
    {
        this.writtenServiceResponseSentDate = writtenServiceResponseSentDate;
    }

    public LinkVersionedResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkVersionedResource asset)
    {
        this.asset = asset;
    }

    public List<OfficeLinkDto> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<OfficeLinkDto> offices)
    {
        this.offices = offices;
    }

    public List<EmployeeLinkDto> getEmployees()
    {
        return this.employees;
    }

    public void setEmployees(final List<EmployeeLinkDto> employees)
    {
        this.employees = employees;
    }

    public LinkResource getJobStatus()
    {
        return this.jobStatus;
    }

    public void setJobStatus(final LinkResource jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    public Date getCreatedOn()
    {
        return this.createdOn;
    }

    public void setCreatedOn(final Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getEtaDate()
    {
        return this.etaDate;
    }

    public void setEtaDate(final Date etaDate)
    {
        this.etaDate = etaDate;
    }

    public Date getEtdDate()
    {
        return this.etdDate;
    }

    public void setEtdDate(final Date etdDate)
    {
        this.etdDate = etdDate;
    }

    public LinkResource getaCase()
    {
        return this.aCase;
    }

    public void setaCase(final LinkResource aCase)
    {
        this.aCase = aCase;
    }

    public LinkResource getLocation()
    {
        return this.location;
    }

    public void setLocation(final LinkResource location)
    {
        this.location = location;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Boolean getScopeConfirmed()
    {
        return this.scopeConfirmed;
    }

    public void setScopeConfirmed(final Boolean scopeConfirmed)
    {
        this.scopeConfirmed = scopeConfirmed;
    }

    public Date getScopeConfirmedDate()
    {
        return this.scopeConfirmedDate;
    }

    public void setScopeConfirmedDate(final Date scopeConfirmedDate)
    {
        this.scopeConfirmedDate = scopeConfirmedDate;
    }

    public LinkResource getScopeConfirmedBy()
    {
        return this.scopeConfirmedBy;
    }

    public void setScopeConfirmedBy(final LinkResource scopeConfirmedBy)
    {
        this.scopeConfirmedBy = scopeConfirmedBy;
    }

    public Date getFirstVisitDate()
    {
        return this.firstVisitDate;
    }

    public void setFirstVisitDate(final Date firstVisitDate)
    {
        this.firstVisitDate = firstVisitDate;
    }

    public Date getLastVisitDate()
    {
        return this.lastVisitDate;
    }

    public void setLastVisitDate(final Date lastVisitDate)
    {
        this.lastVisitDate = lastVisitDate;
    }

    public Date getCompletedOn()
    {
        return this.completedOn;
    }

    public void setCompletedOn(final Date completedOn)
    {
        this.completedOn = completedOn;
    }

    public String getCompletedBy()
    {
        return this.completedBy;
    }

    public void setCompletedBy(final String completedBy)
    {
        this.completedBy = completedBy;
    }

    public Boolean getZeroVisitJob()
    {
        return this.zeroVisitJob;
    }

    public void setZeroVisitJob(final Boolean zeroVisitJob)
    {
        this.zeroVisitJob = zeroVisitJob;
    }

    public LinkResource getProposedFlagState()
    {
        return this.proposedFlagState;
    }

    public void setProposedFlagState(final LinkResource proposedFlagState)
    {
        this.proposedFlagState = proposedFlagState;
    }

    public Date getUpdatedOn()
    {
        return this.updatedOn;
    }

    public void setUpdatedOn(final Date updatedOn)
    {
        this.updatedOn = updatedOn;
    }

    public LinkResource getUpdatedOnBehalfOf()
    {
        return this.updatedOnBehalfOf;
    }

    public void setUpdatedOnBehalfOf(final LinkResource updatedOnBehalfOf)
    {
        this.updatedOnBehalfOf = updatedOnBehalfOf;
    }

    public Boolean getClassGroupJob()
    {
        return this.classGroupJob;
    }

    public void setClassGroupJob(final Boolean classGroupJob)
    {
        this.classGroupJob = classGroupJob;
    }

    public LinkResource getJobCategory()
    {
        return this.jobCategory;
    }

    public void setJobCategory(final LinkResource jobCategory)
    {
        this.jobCategory = jobCategory;
    }

    public LinkResource getJobTeam()
    {
        return this.jobTeam;
    }

    public void setJobTeam(final LinkResource jobTeam)
    {
        this.jobTeam = jobTeam;
    }

    public Boolean getReopened()
    {
        return this.reopened;
    }

    public void setReopened(final Boolean reopened)
    {
        this.reopened = reopened;
    }

    public LinkResource getClassRecommendation()
    {
        return this.classRecommendation;
    }

    public void setClassRecommendation(final LinkResource classRecommendation)
    {
        this.classRecommendation = classRecommendation;
    }

    public String getClassRecommendationNarrative()
    {
        return this.classRecommendationNarrative;
    }

    public void setClassRecommendationNarrative(final String classRecommendationNarrative)
    {
        this.classRecommendationNarrative = classRecommendationNarrative;
    }

    public Boolean getFinishCreditable()
    {
        return this.finishCreditable;
    }

    public void setFinishCreditable(final Boolean finishCreditable)
    {
        this.finishCreditable = finishCreditable;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    public String getControlNumber()
    {
        return this.controlNumber;
    }

    public void setControlNumber(final String controlNumber)
    {
        this.controlNumber = controlNumber;
    }

    public Boolean getMigrated()
    {
        return this.migrated;
    }

    public void setMigrated(final Boolean migrated)
    {
        this.migrated = migrated;
    }

    public String getReportedServiceCodes()
    {
        return this.reportedServiceCodes;
    }

    public void setReportedServiceCodes(final String reportedServiceCodes)
    {
        this.reportedServiceCodes = reportedServiceCodes;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "JobDto [note=" + this.note
                + ", requestByTelephoneIndicator=" + this.requestByTelephoneIndicator
                + ", scopeConfirmed=" + this.scopeConfirmed
                + ", scopeConfirmedDate=" + this.getStringRepresentationOfDate(this.scopeConfirmedDate)
                + ", scopeConfirmedBy=" + this.getIdOrNull(this.scopeConfirmedBy)
                + ", requestedAttendanceDate=" + this.getStringRepresentationOfDate(this.requestedAttendanceDate)
                + ", statusReason=" + this.statusReason
                + ", workOrderNumber=" + this.workOrderNumber
                + ", writtenServiceRequestReceivedDate=" + this.getStringRepresentationOfDate(this.writtenServiceRequestReceivedDate)
                + ", writtenServiceResponseSentDate=" + this.getStringRepresentationOfDate(this.writtenServiceResponseSentDate)
                + ", asset=" + this.getIdOrNull(this.asset)
                + ", aCase=" + this.getIdOrNull(this.aCase)
                + ", offices=" + this.getStringRepresentationOfList(this.offices)
                + ", employees=" + this.getStringRepresentationOfList(this.employees)
                + ", jobStatus=" + this.getIdOrNull(this.jobStatus)
                + ", createdOn=" + this.getStringRepresentationOfDate(this.createdOn)
                + ", etaDate=" + this.getStringRepresentationOfDate(this.etaDate)
                + ", etdDate=" + this.getStringRepresentationOfDate(this.etdDate)
                + ", location=" + this.getIdOrNull(this.location)
                + ", description=" + this.description
                + ", firstVisitDate=" + this.getStringRepresentationOfDate(this.firstVisitDate)
                + ", lastVisitDate=" + this.getStringRepresentationOfDate(this.lastVisitDate)
                + ", completedOn=" + this.getStringRepresentationOfDate(this.completedOn)
                + ", completedBy=" + this.completedBy
                + ", zeroVisit=" + this.zeroVisitJob
                + ", proposedFlag=" + this.getIdOrNull(this.proposedFlagState)
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.updatedOn)
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.updatedOnBehalfOf)
                + ", classGroupJob=" + this.classGroupJob
                + ", jobCategory=" + this.getIdOrNull(this.jobCategory)
                + ", jobTeam=" + this.getIdOrNull(this.jobTeam)
                + ", reopened=" + this.reopened
                + ", classRecommendation=" + this.getIdOrNull(this.classRecommendation)
                + ", classRecommendationNarrative=" + this.classRecommendationNarrative
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
