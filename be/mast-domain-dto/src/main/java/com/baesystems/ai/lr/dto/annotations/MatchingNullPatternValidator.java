package com.baesystems.ai.lr.dto.annotations;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;

public class MatchingNullPatternValidator implements ConstraintValidator<MatchingNullPattern, Object>
{
    private String[] fieldNames; // List of field name to consider in the comparison. All other fields in the object are
                                 // ignored.

    /**
     * Get the field names from the annotation.
     */
    @Override
    public void initialize(final MatchingNullPattern constraintAnnotation)
    {
        fieldNames = constraintAnnotation.fields();
    }

    /**
     * Method for testing if if the null pattern (only considering given fields) of each object in a list matches.
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean isValid(final Object value, final ConstraintValidatorContext context)
    {
        boolean valid = true;

        try
        {
            if (value instanceof ArrayList)
            {
                valid = compareElements((ArrayList<Object>) value);
            }
            else
            {
                throw new UnsupportedOperationException("Matching null pattern validation can only be used with lists.");
            }

        }
        catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            // The method that this overrides doesn't throw these exceptions so it needs to throw a built in java
            // exception if there is a problem.
            throw new UnsupportedOperationException(exception);
        }

        return valid;
    }

    /**
     * Get the null pattern of a given field list for an object.
     *
     * @param obj the full object.
     * @return An array of booleans equal in length to the number of field names where each element is true if the field
     *         is null and false if not.
     */
    private Boolean[] getNullPattern(final Object obj) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        final Boolean[] nullPattern = new Boolean[fieldNames.length];

        int field = 0;

        for (final String fieldName : fieldNames)
        {
            nullPattern[field] = PropertyUtils.getProperty(obj, fieldName) == null;
            ++field;
        }

        return nullPattern;
    }

    /**
     * Compares the null pattern (using the given fields) of an object with a given template
     *
     * @param obj
     * @param nullPattern
     * @return false at when the first field that doesn't match is fount true if they all match.
     */
    private Boolean isNullPatternTheSame(final Object obj, final Boolean[] nullPattern)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        int field = 0;
        Boolean valid = true;

        for (final String fieldName : fieldNames)
        {
            final Boolean isNull = PropertyUtils.getProperty(obj, fieldName) == null;

            if (isNull != nullPattern[field])
            {
                valid = false;
                break;
            }
            ++field;
        }

        return valid;
    }

    /**
     * Method to compare the null patterns of all members of a list, using the given field names.
     *
     * @return false at the first failure true if they all match the first or the list is null, empty or only has one
     *         element.
     */
    private Boolean compareElements(final List<Object> list) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        Boolean valid = true;

        if (list != null && !list.isEmpty())
        {
            final Boolean[] nullPattern = this.getNullPattern(list.get(0));

            for (final Object obj : list)
            {
                if (!this.isNullPatternTheSame(obj, nullPattern))
                {
                    valid = false;
                    break;
                }
            }
        }
        return valid;
    }
}
