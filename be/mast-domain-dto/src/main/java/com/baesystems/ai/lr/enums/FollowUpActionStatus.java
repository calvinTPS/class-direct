package com.baesystems.ai.lr.enums;

public enum FollowUpActionStatus
{
    OPEN(1L),
    CLOSED(2L);

    private final Long value;

    FollowUpActionStatus(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }
}
