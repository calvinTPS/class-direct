package com.baesystems.ai.lr.dto.services;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class SurveyPageResourceDto extends BasePageResource<SurveyDto>
{
    @Valid
    private List<SurveyDto> content;

    @Override
    public List<SurveyDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<SurveyDto> content)
    {
        this.content = content;
    }
}
