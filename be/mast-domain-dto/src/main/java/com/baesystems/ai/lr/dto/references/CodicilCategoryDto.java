package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;

public class CodicilCategoryDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -7700260036236544321L;

    @NotNull
    @Valid
    private LinkResource codicilType;

    public final LinkResource getCodicilType()
    {
        return codicilType;
    }

    public final void setCodicilType(LinkResource codicilType)
    {
        this.codicilType = codicilType;
    }
}
