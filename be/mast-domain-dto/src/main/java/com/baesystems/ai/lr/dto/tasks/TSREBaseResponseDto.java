package com.baesystems.ai.lr.dto.tasks;

import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class TSREBaseResponseDto implements RuleEngineDto
{
    private static final long serialVersionUID = -4882268195741704568L;

    private String status;
    private String message;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TSREBaseResponseDto [getStatus()=");
        builder.append(getStatus());
        builder.append(", getMessage()=");
        builder.append(getMessage());
        builder.append("]");
        return builder.toString();
    }
}
