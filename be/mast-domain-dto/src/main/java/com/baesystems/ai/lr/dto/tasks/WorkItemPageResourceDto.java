package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class WorkItemPageResourceDto extends BasePageResource<WorkItemLightDto>
{
    @Valid
    private List<WorkItemLightDto> content;

    @Override
    public List<WorkItemLightDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<WorkItemLightDto> content)
    {
        this.content = content;
    }
}
