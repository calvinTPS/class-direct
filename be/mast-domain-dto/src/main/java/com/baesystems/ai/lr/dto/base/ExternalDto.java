package com.baesystems.ai.lr.dto.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ExternalDto implements IdDto, Serializable
{
    private static final long serialVersionUID = 7177674374161922994L;

    private Long id;

    @Override
    public Long getId()
    {
        return id;
    }

    @Override
    public void setId(final Long id)
    {
        this.id = id;
    }

    @JsonIgnore
    @Override
    public String getInternalId()
    {
        // no InternalID
        return null;
    }

    @Override
    @JsonIgnore
    public String getTrueInternalId()
    {
        return null;
    }

    @JsonIgnore
    @Override
    public void setInternalId(final String internalId)
    {
        // no internalID
    }
}
