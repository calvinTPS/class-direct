package com.baesystems.ai.lr.dto.query;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.annotations.RequiredWithFields;
import com.baesystems.ai.lr.dto.annotations.Validate;
import com.baesystems.ai.lr.dto.base.Dto;

/**
 * This class makes it possible to define two queries which each give priority to a different data source for apis that
 * draw their results from two data sources (mast and ihs) the results of both will be found than the intersection will
 * be taken so this class effectually ands the two queries together, all the results returned must match both
 * conditions.
 *
 * The fields mastPriorityQuery and ihsPriorityQuery are mutually excusive with all other query fields. If this class is
 * populated like a normal abstract query then it is up to the calling method to decide on the priority (currently the
 * only implementation that uses it is the mast-and-ihs-query api which assumes mast takes priorty by default)
 */
@RequiredWithFields.List({@RequiredWithFields(keyField = "joinType", fields = {"joinField"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "joinType", fields = {"mastPriorityQuery", "ihsPriorityQuery"}, shouldBePopulated = false),
                          @RequiredWithFields(keyField = "alias", fields = {"joinField"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "and",
                                  fields = {"field", "value", "relationship", "or", "not", "mastPriorityQuery", "ihsPriorityQuery"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "or",
                                  fields = {"field", "value", "relationship", "and", "not", "mastPriorityQuery", "ihsPriorityQuery"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "not",
                                  fields = {"field", "value", "relationship", "and", "or", "mastPriorityQuery", "ihsPriorityQuery"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "field", fields = {"and", "or", "not", "mastPriorityQuery", "ihsPriorityQuery"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "field", fields = {"value", "relationship"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "value", fields = {"and", "or", "not", "mastPriorityQuery", "ihsPriorityQuery"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "value", fields = {"field", "relationship"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "relationship", fields = {"and", "or", "not", "mastPriorityQuery", "ihsPriorityQuery"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "relationship", fields = {"field", "value"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "selectWhenFieldIsMissing", fields = {"mastPriorityQuery", "ihsPriorityQuery"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "mastPriorityQuery",
                                  fields = {"joinField", "joinType", "alias", "field", "value", "relationship", "and", "or", "not",
                                            "selectWhenFieldIsMissing"},
                                  shouldBePopulated = false),
                          @RequiredWithFields(keyField = "ihsPriorityQuery",
                                  fields = {"joinField", "joinType", "alias", "field", "value", "relationship", "and", "or", "not",
                                            "selectWhenFieldIsMissing"},
                                  shouldBePopulated = false)
})
public class PrioritisedAbstractQueryDto extends AbstractQueryDto implements Serializable, Dto
{
    private static final long serialVersionUID = 5136250409193702646L;

    @Validate
    private AbstractQueryDto mastPriorityQuery;

    @Validate
    private AbstractQueryDto ihsPriorityQuery;

    public AbstractQueryDto getMastPriorityQuery()
    {
        return mastPriorityQuery;
    }

    public void setMastPriorityQuery(final AbstractQueryDto mastPriorityQuery)
    {
        this.mastPriorityQuery = mastPriorityQuery;
    }

    public AbstractQueryDto getIhsPriorityQuery()
    {
        return ihsPriorityQuery;
    }

    public void setIhsPriorityQuery(final AbstractQueryDto ihsPriorityQuery)
    {
        this.ihsPriorityQuery = ihsPriorityQuery;
    }
}
