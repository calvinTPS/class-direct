package com.baesystems.ai.lr.dto.base;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Provide read-only visibility of the last updated info
 */
public abstract class AuditedDto extends DeletableDto
{
    private static final long serialVersionUID = -949312378093267282L;

    @JsonIgnore
    private String updatedBy;

    @JsonIgnore
    private Date updatedDate;

    @JsonProperty
    public String getUpdatedBy()
    {
        return updatedBy;
    }

    @JsonIgnore
    public void setUpdatedBy(final String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @JsonProperty
    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    @JsonIgnore
    public void setUpdatedDate(final Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }
}
