package com.baesystems.ai.lr.dto.validation;

import com.baesystems.ai.lr.dto.base.Dto;

public class AdditionalInfoErrorMessageDto extends ErrorMessageDto
{
    private static final long serialVersionUID = -3556963565588882240L;
    private Dto info;

    public Dto getInfo()
    {
        return info;
    }

    public void setInfo(final Dto info)
    {
        this.info = info;
    }
}
