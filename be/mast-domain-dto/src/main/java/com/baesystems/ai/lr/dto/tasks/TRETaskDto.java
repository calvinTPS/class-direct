package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class TRETaskDto implements RuleEngineDto
{
    private static final long serialVersionUID = 353094792790014408L;

    private String referenceCode;
    private List<TREAttributeDto> attributes;

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public List<TREAttributeDto> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(final List<TREAttributeDto> attributes)
    {
        this.attributes = attributes;
    }

    @Override
    public String toString()
    {
        return "TRETaskDto [referenceCode=" + referenceCode + ", attributes=" + attributes + "]";
    }

}
