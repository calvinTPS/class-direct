package com.baesystems.ai.lr.dto.references;

public class ReportValidationMessageDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 4558730574780996638L;

    private String referenceCode;
    private String messageText;

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public String getMessageText()
    {
        return this.messageText;
    }

    public void setMessageText(final String messageText)
    {
        this.messageText = messageText;
    }
}
