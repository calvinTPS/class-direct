package com.baesystems.ai.lr.dto.ihs;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsAssetDto extends IhsBaseDto
{
    private static final long serialVersionUID = 3700264812304200069L;

    @Size(message = "invalid length", max = 50)
    private String yardNumber;

    @Size(message = "invalid length", max = 50)
    private String builder;

    @Size(message = "invalid length", max = 9)
    private String builderCode;

    @Size(message = "invalid length", max = 50)
    private String name;

    @Size(message = "invalid length", max = 6)
    private Date dateOfBuild;

    @Valid
    private IhsAssetTypeDto ihsAssetType;

    @Size(message = "invalid length", max = 7)
    private String leadShip;

    @Size(message = "invalid length", max = 50)
    private String portName;

    private Long portMastId;

    @Size(message = "invalid length", max = 50)
    private String shipManager;

    @Size(message = "invalid length", max = 13)
    private String callSign;

    @Size(message = "invalid length", max = 30)
    private String docCompany;

    @Size(message = "invalid length", max = 3)
    private String flag;

    @Size(message = "invalid length", max = 50)
    private String flagName;

    @Size(message = "invalid length", max = 30)
    private String officialNo;

    @Size(message = "invalid length", max = 50)
    private String operator;

    @Size(message = "invalid length", max = 7)
    private String shipManagerCode;

    @Size(message = "invalid length", max = 7)
    private String operatorCode;

    @Size(message = "invalid length", max = 7)
    private String techManagerCode;

    @Size(message = "invalid length", max = 50)
    private String techManager;

    @Size(message = "invalid length", max = 7)
    private String docCode;

    @Size(message = "invalid length", max = 3)
    private String countryOfBuild;

    @Size(message = "invalid length", max = 50)
    private String countryOfBuildName;

    @Size(message = "invalid length", max = 50)
    private String status;

    @Size(message = "invalid length", max = 50)
    private String owner;

    @Size(message = "invalid length", max = 7)
    private String ownerCode;

    @Size(message = "invalid length", max = 50)
    private String gbo;

    @Size(message = "invalid length", max = 7)
    private String gboCode;

    @Size(message = "invalid length", max = 9)
    private String mmsi;

    private Double grossTonnage;

    @Valid
    private IhsCbsbDto ihsCbsb;

    private List<IhsOvnaDto> formerData;

    private Float breadth;

    private Double loa;

    private Double depth;

    private Date nconDate;

    private Double dwt;

    private String classList;

    public String getYardNumber()
    {
        return this.yardNumber;
    }

    public void setYardNumber(final String yardNumber)
    {
        this.yardNumber = yardNumber;
    }

    public String getBuilder()
    {
        return this.builder;
    }

    public void setBuilder(final String builder)
    {
        this.builder = builder;
    }

    public String getBuilderCode()
    {
        return this.builderCode;
    }

    public void setBuilderCode(final String builderCode)
    {
        this.builderCode = builderCode;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Date getDateOfBuild()
    {
        return this.dateOfBuild;
    }

    public void setDateOfBuild(final Date dateOfBuild)
    {
        this.dateOfBuild = dateOfBuild;
    }

    public IhsAssetTypeDto getIhsAssetType()
    {
        return this.ihsAssetType;
    }

    public void setIhsAssetType(final IhsAssetTypeDto ihsAssetType)
    {
        this.ihsAssetType = ihsAssetType;
    }

    public String getLeadShip()
    {
        return leadShip;
    }

    public void setLeadShip(final String leadShip)
    {
        this.leadShip = leadShip;
    }

    public String getPortName()
    {
        return this.portName;
    }

    public void setPortName(final String portName)
    {
        this.portName = portName;
    }

    public String getShipManager()
    {
        return this.shipManager;
    }

    public void setShipManager(final String shipManager)
    {
        this.shipManager = shipManager;
    }

    public String getCallSign()
    {
        return this.callSign;
    }

    public void setCallSign(final String callSign)
    {
        this.callSign = callSign;
    }

    public String getDocCompany()
    {
        return this.docCompany;
    }

    public void setDocCompany(final String docCompany)
    {
        this.docCompany = docCompany;
    }

    public String getFlag()
    {
        return this.flag;
    }

    public void setFlag(final String flag)
    {
        this.flag = flag;
    }

    public String getFlagName()
    {
        return this.flagName;
    }

    public void setFlagName(final String flagName)
    {
        this.flagName = flagName;
    }

    public String getOfficialNo()
    {
        return this.officialNo;
    }

    public void setOfficialNo(final String officialNo)
    {
        this.officialNo = officialNo;
    }

    public String getOperator()
    {
        return this.operator;
    }

    public void setOperator(final String operator)
    {
        this.operator = operator;
    }

    public String getShipManagerCode()
    {
        return this.shipManagerCode;
    }

    public void setShipManagerCode(final String shipManagerCode)
    {
        this.shipManagerCode = shipManagerCode;
    }

    public String getOperatorCode()
    {
        return this.operatorCode;
    }

    public void setOperatorCode(final String operatorCode)
    {
        this.operatorCode = operatorCode;
    }

    public String getTechManagerCode()
    {
        return this.techManagerCode;
    }

    public void setTechManagerCode(final String techManagerCode)
    {
        this.techManagerCode = techManagerCode;
    }

    public String getTechManager()
    {
        return this.techManager;
    }

    public void setTechManager(final String techManager)
    {
        this.techManager = techManager;
    }

    public String getDocCode()
    {
        return this.docCode;
    }

    public void setDocCode(final String docCode)
    {
        this.docCode = docCode;
    }

    public String getCountryOfBuild()
    {
        return this.countryOfBuild;
    }

    public void setCountryOfBuild(final String countryOfBuild)
    {
        this.countryOfBuild = countryOfBuild;
    }

    public String getStatus()
    {
        return this.status;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public IhsCbsbDto getIhsCbsb()
    {
        return this.ihsCbsb;
    }

    public void setIhsCbsb(final IhsCbsbDto ihsCbsb)
    {
        this.ihsCbsb = ihsCbsb;
    }

    public List<IhsOvnaDto> getFormerData()
    {
        return this.formerData;
    }

    public void setFormerData(final List<IhsOvnaDto> formerData)
    {
        this.formerData = formerData;
    }

    public String getOwner()
    {
        return this.owner;
    }

    public void setOwner(final String owner)
    {
        this.owner = owner;
    }

    public String getOwnerCode()
    {
        return this.ownerCode;
    }

    public void setOwnerCode(final String ownerCode)
    {
        this.ownerCode = ownerCode;
    }

    public String getGbo()
    {
        return this.gbo;
    }

    public void setGbo(final String gbo)
    {
        this.gbo = gbo;
    }

    public String getGboCode()
    {
        return this.gboCode;
    }

    public void setGboCode(final String gboCode)
    {
        this.gboCode = gboCode;
    }

    public String getMmsi()
    {
        return mmsi;
    }

    public void setMmsi(final String mmsi)
    {
        this.mmsi = mmsi;
    }

    public Double getGrossTonnage()
    {
        return grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public Float getBreadth()
    {
        return breadth;
    }

    public void setBreadth(final Float breadth)
    {
        this.breadth = breadth;
    }

    public Double getLoa()
    {
        return loa;
    }

    public void setLoa(final Double loa)
    {
        this.loa = loa;
    }

    public Double getDepth()
    {
        return depth;
    }

    public void setDepth(final Double depth)
    {
        this.depth = depth;
    }

    public Date getNconDate()
    {
        return nconDate;
    }

    public void setNconDate(final Date newConstructionContractDate)
    {
        this.nconDate = newConstructionContractDate;
    }

    public String getCountryOfBuildName()
    {
        return countryOfBuildName;
    }

    public void setCountryOfBuildName(final String countryOfBuildName)
    {
        this.countryOfBuildName = countryOfBuildName;
    }

    public Long getPortMastId()
    {
        return portMastId;
    }

    public void setPortMastId(final Long portMastId)
    {
        this.portMastId = portMastId;
    }

    public Double getDwt()
    {
        return dwt;
    }

    public void setDwt(final Double dwt)
    {
        this.dwt = dwt;
    }

    public String getClassList()
    {
        return classList;
    }

    public void setClassList(final String classList)
    {
        this.classList = classList;
    }
}
