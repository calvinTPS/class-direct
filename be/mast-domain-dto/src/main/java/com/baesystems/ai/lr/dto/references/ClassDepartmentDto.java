package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ClassDepartmentDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 8158925922837725527L;

    private String name;

    private String code;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return this.code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }
}
