package com.baesystems.ai.lr.enums;

public enum AttachmentSource
{
    EXTERNAL_SYSTEM("EXTERNAL SYSTEM");

    private final String value;

    AttachmentSource(final String value)
    {
        this.value = value;
    }

    public final String getValue()
    {
        return this.value;
    }
}
