package com.baesystems.ai.lr.enums;

public enum ItemRelationshipType
{
    IS_PART_OF(1L),
    IS_RELATED_TO(9L);

    private final Long value;

    private ItemRelationshipType(Long value)
    {
        this.value = value;
    }

    public Long getValue()
    {
        return this.value;
    }
}
