package com.baesystems.ai.lr.dto.services;

import static com.baesystems.ai.lr.dto.services.AbstractScheduledServiceDto.ENTITY_NAME;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@EntityName(ENTITY_NAME)
public abstract class AbstractScheduledServiceDto extends AuditedDto
        implements DueStatusUpdateableDto, UpdateableJobBundleEntity, HashedParentReportingDto
{
    public static final String ENTITY_NAME = "Service";
    private static final long serialVersionUID = 248381680076140946L;

    @Size(max = 50)
    private String name;

    private String stalenessHash;

    @NotNull
    private LinkResource serviceCatalogue;

    @Valid
    private LinkResource parentService;

    @Valid
    @NotNull
    private LinkResource asset;

    @Valid
    private LinkResource assetItem;

    private Integer cyclePeriodicity;

    private Date postponementDate;

    @Valid
    private LinkResource postponementType;

    private Date assignedDate;

    private Date assignedDateManual;

    private Date dueDate;

    private LinkResource dueStatus;

    private Date dueDateManual;

    private Date lowerRangeDate;

    private Date lowerRangeDateManual;

    private Date upperRangeDate;

    private Date upperRangeDateManual;

    @Valid
    private LinkResource serviceStatus;

    @Valid
    private LinkResource serviceCreditStatus;

    private Date completionDate;

    @NotNull
    private Boolean provisionalDates;

    private Date creditingDate;

    private Integer serviceCycleNumber;

    private Integer occurrenceNumber;

    @Size(message = "invalid length", max = 9)
    private String lastCreditedJob;

    @Size(message = "invalid length", max = 9)
    private String lastPartheldJob;

    private Boolean provisional;

    private Date sourceJobLastVisitDate;

    private Date sourceJobCreationDateTime;

    @Size(message = "invalid length", max = 150)
    private String swapReason;

    @JsonProperty("_dmID")
    private String dmInternalLookupID;

    public LinkResource getServiceCatalogue()
    {
        return this.serviceCatalogue;
    }

    public void setServiceCatalogue(final LinkResource serviceCatalogue)
    {
        this.serviceCatalogue = serviceCatalogue;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final LinkResource assetItem)
    {
        this.assetItem = assetItem;
    }

    public Integer getCyclePeriodicity()
    {
        return this.cyclePeriodicity;
    }

    public void setCyclePeriodicity(final Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    public Date getAssignedDate()
    {
        return this.assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public Date getAssignedDateManual()
    {
        return this.assignedDateManual;
    }

    public void setAssignedDateManual(final Date assignedDateManual)
    {
        this.assignedDateManual = assignedDateManual;
    }

    @Override
    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public LinkResource getDueStatus()
    {
        return this.dueStatus;
    }

    @Override
    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public Date getDueDateManual()
    {
        return this.dueDateManual;
    }

    public void setDueDateManual(final Date dueDateManual)
    {
        this.dueDateManual = dueDateManual;
    }

    public Date getLowerRangeDate()
    {
        return this.lowerRangeDate;
    }

    public void setLowerRangeDate(final Date lowerRangeDate)
    {
        this.lowerRangeDate = lowerRangeDate;
    }

    public Date getLowerRangeDateManual()
    {
        return this.lowerRangeDateManual;
    }

    public void setLowerRangeDateManual(final Date lowerRangeDateManual)
    {
        this.lowerRangeDateManual = lowerRangeDateManual;
    }

    public Date getUpperRangeDate()
    {
        return this.upperRangeDate;
    }

    public void setUpperRangeDate(final Date upperRangeDate)
    {
        this.upperRangeDate = upperRangeDate;
    }

    public Date getUpperRangeDateManual()
    {
        return this.upperRangeDateManual;
    }

    public void setUpperRangeDateManual(final Date upperRangeDateManual)
    {
        this.upperRangeDateManual = upperRangeDateManual;
    }

    public LinkResource getServiceStatus()
    {
        return this.serviceStatus;
    }

    public void setServiceStatus(final LinkResource serviceStatus)
    {
        this.serviceStatus = serviceStatus;
    }

    public LinkResource getServiceCreditStatus()
    {
        return this.serviceCreditStatus;
    }

    public void setServiceCreditStatus(final LinkResource serviceCreditStatus)
    {
        this.serviceCreditStatus = serviceCreditStatus;
    }

    public Date getCompletionDate()
    {
        return this.completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public Boolean getProvisionalDates()
    {
        return this.provisionalDates;
    }

    public void setProvisionalDates(final Boolean provisionalDates)
    {
        this.provisionalDates = provisionalDates;
    }

    public Date getCreditingDate()
    {
        return this.creditingDate;
    }

    public void setCreditingDate(final Date creditingDate)
    {
        this.creditingDate = creditingDate;
    }

    public Integer getServiceCycleNumber()
    {
        return this.serviceCycleNumber;
    }

    public void setServiceCycleNumber(final Integer serviceCycleNumber)
    {
        this.serviceCycleNumber = serviceCycleNumber;
    }

    public Integer getOccurrenceNumber()
    {
        return this.occurrenceNumber;
    }

    public void setOccurrenceNumber(final Integer occurrenceNumber)
    {
        this.occurrenceNumber = occurrenceNumber;
    }

    public String getLastCreditedJob()
    {
        return this.lastCreditedJob;
    }

    public void setLastCreditedJob(final String lastCreditedJob)
    {
        this.lastCreditedJob = lastCreditedJob;
    }

    public String getLastPartheldJob()
    {
        return this.lastPartheldJob;
    }

    public void setLastPartheldJob(final String lastPartheldJob)
    {
        this.lastPartheldJob = lastPartheldJob;
    }

    public Boolean getProvisional()
    {
        return this.provisional;
    }

    public void setProvisional(final Boolean provisional)
    {
        this.provisional = provisional;
    }

    public LinkResource getParentService()
    {
        return this.parentService;
    }

    public void setParentService(final LinkResource parentService)
    {
        this.parentService = parentService;
    }

    public Date getPostponementDate()
    {
        return this.postponementDate;
    }

    public void setPostponementDate(final Date postponementDate)
    {
        this.postponementDate = postponementDate;
    }

    public LinkResource getPostponementType()
    {
        return this.postponementType;
    }

    public void setPostponementType(final LinkResource postponementType)
    {
        this.postponementType = postponementType;
    }

    @Override
    public String getParentHash()
    {
        // This is present as the HashedParentReportingDto interface requires it, as in most other cases we have the
        // same DTO for WIP and non-WIP entities. However, services and surveys have different DTOs.
        return null;
    }

    public void setParentHash(final String parentHash)
    {
        // This has been deliberately left blank
    }

    @Override
    public Date getSourceJobLastVisitDate()
    {
        return this.sourceJobLastVisitDate;
    }

    @Override
    public void setSourceJobLastVisitDate(final Date sourceJobLastVisitDate)
    {
        this.sourceJobLastVisitDate = sourceJobLastVisitDate;
    }

    @Override
    public Date getSourceJobCreationDateTime()
    {
        return this.sourceJobCreationDateTime;
    }

    @Override
    public void setSourceJobCreationDateTime(final Date sourceJobCreationDateTime)
    {
        this.sourceJobCreationDateTime = sourceJobCreationDateTime;
    }

    public String getSwapReason()
    {
        return this.swapReason;
    }

    public void setSwapReason(final String swapReason)
    {
        this.swapReason = swapReason;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    public String getDmInternalLookupID()
    {
        return this.dmInternalLookupID;
    }

    public void setDmInternalLookupID(final String dmInternalLookupID)
    {
        this.dmInternalLookupID = dmInternalLookupID;
    }

    public abstract Boolean getActive();

    public abstract void setActive(Boolean active);

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        // Note: Deliberately don't include dueStatus in the hash as this data is derived from other fields.

        return "ScheduledServiceDto [serviceCatalogueId=" + this.getIdOrNull(this.serviceCatalogue)
                + ", parentService=" + this.getIdOrNull(this.parentService)
                + ", asset=" + this.getIdOrNull(this.asset)
                + ", assetItem=" + this.getIdOrNull(this.assetItem)
                + ", cyclePeriodicity=" + this.cyclePeriodicity
                + ", postponedDate=" + this.getStringRepresentationOfDate(this.getPostponementDate())
                + ", postponedType=" + this.getIdOrNull(this.getPostponementType())
                + ", assignedDate=" + this.getStringRepresentationOfDate(this.assignedDate)
                + ", assignedDateManual=" + this.getStringRepresentationOfDate(this.assignedDateManual)
                + ", dueDate=" + this.getStringRepresentationOfDate(this.dueDate)
                + ", dueDateManual=" + this.getStringRepresentationOfDate(this.dueDateManual)
                + ", lowerRangeDate=" + this.getStringRepresentationOfDate(this.lowerRangeDate)
                + ", lowerRangeDateManual=" + this.getStringRepresentationOfDate(this.lowerRangeDateManual)
                + ", upperRangeDate=" + this.getStringRepresentationOfDate(this.upperRangeDate)
                + ", upperRangeDateManual=" + this.getStringRepresentationOfDate(this.upperRangeDateManual)
                + ", serviceStatus=" + this.getIdOrNull(this.serviceStatus)
                + ", serviceCreditStatus=" + this.getIdOrNull(this.serviceCreditStatus)
                + ", completionDate=" + this.getStringRepresentationOfDate(this.completionDate)
                + ", provisionalDates=" + this.provisionalDates
                + ", creditingDate=" + this.getStringRepresentationOfDate(this.creditingDate)
                + ", serviceCycleNumber=" + this.serviceCycleNumber
                + ", occurrenceNumber=" + this.occurrenceNumber
                + ", lastCreditedJob=" + this.lastCreditedJob
                + ", lastPartheldJob=" + this.lastPartheldJob
                + ", provisional=" + this.provisional
                + ", active=" + (this.getActive() == null ? Boolean.TRUE : this.getActive())
                + ", getId()=" + this.getId()
                + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
