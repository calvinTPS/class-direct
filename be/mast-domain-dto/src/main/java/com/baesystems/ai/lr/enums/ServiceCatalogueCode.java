package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.List;

public enum ServiceCatalogueCode
{
    AMSC("AMSC"),
    SS("SS"),
    CSH("CSH"),
    BTMS("BTMS");

    private static final List<String> REQUIRED_SERVICE_CODES_FOR_HISTORIC_SERVICES = Arrays.asList(
            BTMS.getValue(),
            SS.getValue());

    private final String value;

    ServiceCatalogueCode(final String value)
    {
        this.value = value;
    }

    public final String getValue()
    {
        return this.value;
    }

    public static List<String> getRequiredServiceCodesForHistoricServices()
    {
        return REQUIRED_SERVICE_CODES_FOR_HISTORIC_SERVICES;
    }
}
