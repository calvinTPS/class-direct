package com.baesystems.ai.lr.dto.defects;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class RectificationPageResourceDto extends BasePageResource<RectificationDto>
{
    private List<RectificationDto> content;

    @Override
    public List<RectificationDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<RectificationDto> content)
    {
        this.content = content;
    }
}
