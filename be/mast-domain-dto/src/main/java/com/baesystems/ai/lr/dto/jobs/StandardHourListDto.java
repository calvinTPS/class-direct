package com.baesystems.ai.lr.dto.jobs;

import java.io.Serializable;
import java.util.List;

public class StandardHourListDto implements Serializable
{
    private static final long serialVersionUID = 2165647356220381244L;

    private List<StandardHourDto> standardHourList;

    public List<StandardHourDto> getStandardHourList()
    {
        return standardHourList;
    }

    public void setStandardHourList(final List<StandardHourDto> standardHourList)
    {
        this.standardHourList = standardHourList;
    }
}
