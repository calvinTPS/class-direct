package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import javax.validation.Valid;

public class WorkItemPreviewAssetItemDto extends WorkItemPreviewCommonDto<WorkItemPreviewAssetItemDto>
{
    private static final long serialVersionUID = -6166835888785844649L;

    @Valid
    private List<WorkItemLightDto> tasks;

    public List<WorkItemLightDto> getTasks()
    {
        return this.tasks;
    }

    public void setTasks(final List<WorkItemLightDto> tasks)
    {
        this.tasks = tasks;
    }
}
