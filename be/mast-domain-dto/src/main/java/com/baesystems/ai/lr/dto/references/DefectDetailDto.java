package com.baesystems.ai.lr.dto.references;

import java.util.List;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class DefectDetailDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 2073138219565518325L;

    @Size(message = "invalid length", max = 100)
    private String name;

    private List<DefectValueDto> values;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public List<DefectValueDto> getValues()
    {
        return values;
    }

    public void setValues(final List<DefectValueDto> values)
    {
        this.values = values;
    }
}
