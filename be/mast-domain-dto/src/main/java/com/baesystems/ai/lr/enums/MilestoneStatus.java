package com.baesystems.ai.lr.enums;

public enum MilestoneStatus
{
    OPEN(1L),
    COMPLETE(2L);

    private final Long value;

    MilestoneStatus(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }
}
