package com.baesystems.ai.lr.dto.security;

import com.baesystems.ai.lr.dto.base.Dto;

import java.util.List;


public class UserGroupListDto implements Dto
{
    private String username;
    private List<Long> groupIds;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(final String username)
    {
        this.username = username;
    }

    public List<Long> getGroupIds()
    {
        return groupIds;
    }

    public void setGroupIds(final List<Long> groupIds)
    {
        this.groupIds = groupIds;
    }
}
