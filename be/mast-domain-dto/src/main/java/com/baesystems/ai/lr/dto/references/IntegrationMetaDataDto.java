package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class IntegrationMetaDataDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 1868462885849524144L;

    private String key;

    private String value;

    public String getKey()
    {
        return key;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }
}
