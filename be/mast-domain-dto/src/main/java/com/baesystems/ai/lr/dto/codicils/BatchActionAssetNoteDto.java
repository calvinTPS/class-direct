package com.baesystems.ai.lr.dto.codicils;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.BaseBatchActionDto;

public class BatchActionAssetNoteDto extends BaseBatchActionDto
{
    private static final long serialVersionUID = 4999507030889611042L;

    @Valid
    @NotNull
    private AssetNoteDto assetNoteDto;

    /*
     * The Key value of this map is the AssetId against which each of the DTOs in the List have been saved. A Map is
     * used to assist FE count the saved DTOs against an asset particularly in the case where an ItemTypeID had been
     * specified for the batch-action. This Map is populated by BE.
     */
    private Map<Long, List<AssetNoteDto>> savedAssetNoteDtos;

    public AssetNoteDto getAssetNoteDto()
    {
        return this.assetNoteDto;
    }

    public void setAssetNoteDto(final AssetNoteDto assetNoteDto)
    {
        this.assetNoteDto = assetNoteDto;
    }

    public Map<Long, List<AssetNoteDto>> getSavedAssetNoteDtos()
    {
        return this.savedAssetNoteDtos;
    }

    public void setSavedAssetNoteDtos(final Map<Long, List<AssetNoteDto>> assetNoteDtos)
    {
        this.savedAssetNoteDtos = assetNoteDtos;
    }
}
