package com.baesystems.ai.lr.enums;

public enum PartyRole
{
    SHIP_MANAGER(3L),
    SHIP_OWNER(4L),
    SHIP_BUILDER(8L), // needs adding to ref data no ref data
    DOC_COMPANY(10L),
    TECH_MANAGER(14L);

    private final Long value;

    PartyRole(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
