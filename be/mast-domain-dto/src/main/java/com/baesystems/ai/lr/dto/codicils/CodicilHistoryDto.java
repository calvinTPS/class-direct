package com.baesystems.ai.lr.dto.codicils;

import java.io.Serializable;
import java.util.Date;

import com.baesystems.ai.lr.dto.base.Dto;

public class CodicilHistoryDto implements Dto, Serializable
{
    private static final long serialVersionUID = 1283744068396396740L;

    private String action;

    private Long jobId;

    private String jobNumber;

    private Date dueDate;

    private Long logEntry;

    public String getAction()
    {
        return action;
    }

    public void setAction(final String action)
    {
        this.action = action;
    }

    public Long getJobId()
    {
        return jobId;
    }

    public void setJobId(final Long jobId)
    {
        this.jobId = jobId;
    }

    public String getJobNumber()
    {
        return jobNumber;
    }

    public void setJobNumber(final String jobNumber)
    {
        this.jobNumber = jobNumber;
    }

    public Date getDueDate()
    {
        return dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Long getLogEntry()
    {
        return logEntry;
    }

    public void setLogEntry(final Long logEntry)
    {
        this.logEntry = logEntry;
    }
}
