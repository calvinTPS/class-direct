package com.baesystems.ai.lr.dto.tasks;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.dto.services.StalenessListDto;

import static com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto.ENTITY_NAME;

@EntityName(ENTITY_NAME)
public class WorkItemLightListDto extends StalenessListDto implements Dto, Serializable
{
    public static final String ENTITY_NAME = "Task";
    private static final long serialVersionUID = 154914170398655947L;

    @NotNull
    @Valid
    private List<WorkItemLightDto> tasks;

    public List<WorkItemLightDto> getTasks()
    {
        return this.tasks;
    }

    public void setTasks(final List<WorkItemLightDto> tasks)
    {
        this.tasks = tasks;
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
