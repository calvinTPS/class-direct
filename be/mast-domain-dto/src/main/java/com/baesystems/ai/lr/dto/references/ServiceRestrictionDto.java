package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ServiceRestrictionDto extends ImmutableReferenceDataDto
{

    private static final long serialVersionUID = -6890898511795379720L;

    @NotNull
    @Valid
    private LinkResource serviceRuleSet;

    @NotNull
    private String name;

    public LinkResource getServiceRuleSet()
    {
        return serviceRuleSet;
    }

    public void setServiceRuleSet(final LinkResource serviceRuleSet)
    {
        this.serviceRuleSet = serviceRuleSet;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
