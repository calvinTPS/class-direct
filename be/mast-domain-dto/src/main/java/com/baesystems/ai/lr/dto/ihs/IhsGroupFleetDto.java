package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsGroupFleetDto extends IhsBaseDto
{
    private static final long serialVersionUID = -7129815958921500826L;

    private String regdOwnerName;

    private String groupOwnerName;

    private String regdOwnerCode;

    private String groupOwnerCode;

    public String getRegdOwnerName()
    {
        return this.regdOwnerName;
    }

    public void setRegdOwnerName(final String regdOwnerName)
    {
        this.regdOwnerName = regdOwnerName;
    }

    public String getGroupOwnerName()
    {
        return this.groupOwnerName;
    }

    public void setGroupOwnerName(final String groupOwnerName)
    {
        this.groupOwnerName = groupOwnerName;
    }

    public String getRegdOwnerCode()
    {
        return this.regdOwnerCode;
    }

    public void setRegdOwnerCode(final String regdOwnerCode)
    {
        this.regdOwnerCode = regdOwnerCode;
    }

    public String getGroupOwnerCode()
    {
        return this.groupOwnerCode;
    }

    public void setGroupOwnerCode(final String groupOwnerCode)
    {
        this.groupOwnerCode = groupOwnerCode;
    }
}
