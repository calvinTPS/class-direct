package com.baesystems.ai.lr.dto.external.boomi.assets;

import com.baesystems.ai.lr.dto.base.ExternalDto;

public class AssetDto extends ExternalDto
{
    private static final long serialVersionUID = -3776418878276709257L;

    private String imoNumber;

    private String name;

    private ExternalReferenceDto assetType;

    private ExternalDto assetLifecycleStatus;

    private ExternalDto classStatus;

    private String buildDate;

    private ExternalDto classMaintenanceStatus;

    private Double grossTonnage;

    private ExternalReferenceDto flagState;

    private String hullIndicator;

    public String getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public ExternalReferenceDto getAssetType()
    {
        return this.assetType;
    }

    public void setAssetType(final ExternalReferenceDto assetType)
    {
        this.assetType = assetType;
    }

    public ExternalDto getAssetLifecycleStatus()
    {
        return this.assetLifecycleStatus;
    }

    public void setAssetLifecycleStatus(final ExternalDto assetLifecycleStatus)
    {
        this.assetLifecycleStatus = assetLifecycleStatus;
    }

    public ExternalDto getClassStatus()
    {
        return this.classStatus;
    }

    public void setClassStatus(final ExternalDto classStatus)
    {
        this.classStatus = classStatus;
    }

    public String getBuildDate()
    {
        return this.buildDate;
    }

    public void setBuildDate(final String buildDate)
    {
        this.buildDate = buildDate;
    }

    public ExternalDto getClassMaintenanceStatus()
    {
        return this.classMaintenanceStatus;
    }

    public void setClassMaintenanceStatus(final ExternalDto classMaintenanceStatus)
    {
        this.classMaintenanceStatus = classMaintenanceStatus;
    }

    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public ExternalReferenceDto getFlagState()
    {
        return this.flagState;
    }

    public void setFlagState(final ExternalReferenceDto flagState)
    {
        this.flagState = flagState;
    }

    public String getHullIndicator()
    {
        return this.hullIndicator;
    }

    public void setHullIndicator(final String hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }

    public static class ExternalReferenceDto extends ExternalDto
    {
        private static final long serialVersionUID = -2957154994430745997L;

        private String name;

        private String code;

        public String getName()
        {
            return name;
        }

        public void setName(final String name)
        {
            this.name = name;
        }

        public String getCode()
        {
            return code;
        }

        public void setCode(final String code)
        {
            this.code = code;
        }
    }
}
