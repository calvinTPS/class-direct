package com.baesystems.ai.lr.enums;

import java.util.Arrays;

/**
 * Enum for the each subset of reference datadata sets. This should match the list of subSets in the allowableValues tag
 * in rest-references-context.xml. If you want to add a new reference data type you'll need to define it here, in the
 * allowableValues list in rest-references-config.xml and define the behaviour in the ReferenceData enum.
 */
public enum ReferenceDataSubSet
{
    // asset
    ACTIONS_TAKEN(ReferenceDataSet.ASSET),
    ASSET_CATEGORIES(ReferenceDataSet.ASSET),
    ASSET_TYPES(ReferenceDataSet.ASSET),
    ASSET_TYPE_GROUPS(ReferenceDataSet.ASSET),
    ATTRIBUTE_TYPES(ReferenceDataSet.ASSET),
    ATTRIBUTE_TYPE_VALUES(ReferenceDataSet.ASSET),
    BUILDER(ReferenceDataSet.ASSET),
    CLASS_DEPARTMENTS(ReferenceDataSet.ASSET),
    CLASS_MAINTENANCE_STATUSES(ReferenceDataSet.ASSET),
    CLASS_NOTATIONS(ReferenceDataSet.ASSET),
    CLASS_STATUSES(ReferenceDataSet.ASSET),
    CODICIL_TEMPLATES(ReferenceDataSet.ASSET),
    CODICIL_CATEGORIES(ReferenceDataSet.ASSET),
    COUNTRIES(ReferenceDataSet.ASSET),
    IACS_SOCIETIES(ReferenceDataSet.ASSET),
    ITEM_ACTIONS(ReferenceDataSet.ASSET),
    ITEM_RELATIONSHIP_TYPES(ReferenceDataSet.ASSET),
    ITEM_TYPES(ReferenceDataSet.ASSET),
    ITEM_TYPE_RELATIONSHIPS(ReferenceDataSet.ASSET),
    LIFECYCLE_STATUSES(ReferenceDataSet.ASSET),
    MIGRATION_STATUSES(ReferenceDataSet.ASSET),
    MNCN_STATUSES(ReferenceDataSet.ASSET),
    RULE_SETS(ReferenceDataSet.ASSET),
    RULESET_CATEGORIES(ReferenceDataSet.ASSET),
    TEMPLATE_STATUSES(ReferenceDataSet.ASSET),

    // attachment
    ATTACHMENT_CATEGORIES(ReferenceDataSet.ATTACHMENT),
    CONFIDENTIALITY_TYPES(ReferenceDataSet.ATTACHMENT),
    ATTACHMENT_TYPES(ReferenceDataSet.ATTACHMENT),

    // case
    CASE_TYPES(ReferenceDataSet.CASE),
    CASE_STATUSES(ReferenceDataSet.CASE),
    MILESTONE_DUE_DATE_REFERENCES(ReferenceDataSet.CASE),
    MILESTONES(ReferenceDataSet.CASE),
    MILESTONE_STATUSES(ReferenceDataSet.CASE),
    OFFICE_ROLES(ReferenceDataSet.CASE),
    PRE_EIC_INSPECTION_STATUSES(ReferenceDataSet.CASE),
    RISK_ASSESSMENT_STATUSES(ReferenceDataSet.CASE),

    // defect
    DEFECT_CATEGORIES(ReferenceDataSet.DEFECT),
    DEFECT_DETAILS(ReferenceDataSet.DEFECT),
    DEFECT_STATUSES(ReferenceDataSet.DEFECT),
    FAULT_GROUPS(ReferenceDataSet.DEFECT),

    // employee
    EMPLOYEE(ReferenceDataSet.EMPLOYEE),
    EMPLOYEE_OFFICES(ReferenceDataSet.EMPLOYEE),
    LR_EMPLOYEE_ROLES(ReferenceDataSet.EMPLOYEE),
    EMPLOYEE_ROLES(ReferenceDataSet.EMPLOYEE),
    OFFICE(ReferenceDataSet.EMPLOYEE),
    TEAM(ReferenceDataSet.EMPLOYEE),
    TEAM_EMPLOYEE(ReferenceDataSet.EMPLOYEE),
    TEAM_OFFICE(ReferenceDataSet.EMPLOYEE),

    // fees
    FEE_ALLOWED_ANSWER(ReferenceDataSet.FEES),
    FEE_QUESTION(ReferenceDataSet.FEES),
    FEE_QUESTION_ANSWER(ReferenceDataSet.FEES),
    FEE_QUESTION_SERVICE(ReferenceDataSet.FEES),
    FEE_SURCHARGE(ReferenceDataSet.FEES),

    // flags
    FLAGS(ReferenceDataSet.FLAG),
    PORTS_OF_REGISTRY(ReferenceDataSet.FLAG),

    // job
    ALLOWED_WORK_ITEM_ATTRIBUTE_VALUES(ReferenceDataSet.JOB),
    CERTIFICATE_ACTIONS_TAKEN(ReferenceDataSet.JOB),
    CERTIFICATE_FLAG_SERVICES(ReferenceDataSet.JOB),
    CERTIFICATE_STATUSES(ReferenceDataSet.JOB),
    CERTIFICATE_TEMPLATES(ReferenceDataSet.JOB),
    CERTIFICATE_TYPES(ReferenceDataSet.JOB),
    CLASS_RECOMMENDATIONS(ReferenceDataSet.JOB),
    DEFICIENCY(ReferenceDataSet.JOB),
    DEFICIENCY_GROUP(ReferenceDataSet.JOB),
    DEFICIENCY_SEVERITIES(ReferenceDataSet.JOB),
    ENDORSEMENT_TYPES(ReferenceDataSet.JOB),
    FOLLOW_UP_ACTION_STATUS(ReferenceDataSet.JOB),
    JOB_CATEGORIES(ReferenceDataSet.JOB),
    JOB_STATUSES(ReferenceDataSet.JOB),
    LOCATIONS(ReferenceDataSet.JOB),
    REPORT_TYPES(ReferenceDataSet.JOB),
    REPORT_VALIDATION_MESSAGES(ReferenceDataSet.JOB),
    RESOLUTION_STATUSES(ReferenceDataSet.JOB),
    WORK_ITEM_ACTIONS(ReferenceDataSet.JOB),
    WORK_ITEM_SCOPE_STATUSES(ReferenceDataSet.JOB),
    WORK_ITEM_TYPES(ReferenceDataSet.JOB),

    // miscellaneous
    DUE_STATUSES(ReferenceDataSet.MISCELLANEOUS),
    INTEGRATION_META(ReferenceDataSet.MISCELLANEOUS),

    // party
    PARTY(ReferenceDataSet.PARTY),
    PARTY_ROLES(ReferenceDataSet.PARTY),

    // product
    PRODUCT_CATALOGUES(ReferenceDataSet.PRODUCT),
    PRODUCT_GROUPS(ReferenceDataSet.PRODUCT),
    PRODUCT_MODEL(ReferenceDataSet.PRODUCT),
    PRODUCT_TYPES(ReferenceDataSet.PRODUCT),

    // repair
    MATERIAL_TYPES(ReferenceDataSet.REPAIR),
    MATERIALS(ReferenceDataSet.REPAIR),
    REPAIR_ACTIONS(ReferenceDataSet.REPAIR),
    REPAIR_TYPES(ReferenceDataSet.REPAIR),

    // service
    ASSIGNED_SCHEDULING_TYPES(ReferenceDataSet.SERVICE),
    ASSOCIATED_SCHEDULING_TYPES(ReferenceDataSet.SERVICE),
    CODICIL_STATUSES(ReferenceDataSet.SERVICE),
    CODICIL_TYPES(ReferenceDataSet.SERVICE),
    HARMONISATION_TYPES(ReferenceDataSet.SERVICE),
    SCHEDULING_DUE_TYPES(ReferenceDataSet.SERVICE),
    SCHEDULING_REGIMES(ReferenceDataSet.SERVICE),
    SCHEDULING_TYPES(ReferenceDataSet.SERVICE),
    SERVICE_CATALOGUE_FLAG_ADMIN(ReferenceDataSet.SERVICE),
    SERVICE_CATALOGUES(ReferenceDataSet.SERVICE),
    SERVICE_CREDIT_STATUSES(ReferenceDataSet.SERVICE),
    SERVICE_GROUPS(ReferenceDataSet.SERVICE),
    SERVICE_RESTRICTION(ReferenceDataSet.SERVICE),
    SERVICE_RULESETS(ReferenceDataSet.SERVICE),
    SERVICE_STATUSES(ReferenceDataSet.SERVICE),
    SERVICE_TYPES(ReferenceDataSet.SERVICE),

    // task
    ALLOWED_ATTRIBUTE_VALUES(ReferenceDataSet.TASK),
    ALLOWED_VALUE_GROUPS(ReferenceDataSet.TASK),
    CHECKLIST_ADDITIONAL_INFO(ReferenceDataSet.TASK),
    CHECKLIST_GROUPS(ReferenceDataSet.TASK),
    CHECKLISTS(ReferenceDataSet.TASK),
    CHECKLIST_SERVICE_CATALOGUE(ReferenceDataSet.TASK),
    CHECKLIST_SUBGROUPS(ReferenceDataSet.TASK),
    CHECKLIST_NOTES(ReferenceDataSet.TASK),
    CHECKLIST_NOTES_SERVICE_CATALOGUE(ReferenceDataSet.TASK),
    CONDITIONAL_ATTRIBUTES(ReferenceDataSet.TASK),
    POSTPONEMENT_TYPES(ReferenceDataSet.TASK),
    READING_CATEGORY(ReferenceDataSet.TASK),

    // version
    VERSION(ReferenceDataSet.VERSION);

    private static final char KEBAB_SEPARATOR = '-';
    private static final char ENUM_SEPARATOR = '_';

    private final String value;
    private final String api;
    private final ReferenceDataSet set;

    ReferenceDataSubSet(final ReferenceDataSet set)
    {
        this.value = getCamelCase();
        this.api = getKebabCase();
        this.set = set;
    }

    public final String getValue()
    {
        return this.value;
    }

    public final String getApi()
    {
        return this.api;
    }

    public final ReferenceDataSet getSet()
    {
        return this.set;
    }

    public static final ReferenceDataSubSet getSubSetForName(final String value)
    {
        return Arrays.stream(values()).filter(set -> set.getValue().equals(value)).findAny().orElseGet(() -> null);
    }

    public static final ReferenceDataSubSet getSubSetForApi(final String api)
    {
        return Arrays.stream(values()).filter(set -> set.getApi().equals(api)).findAny().orElseGet(() -> null);
    }

    private String getCamelCase()
    {
        final StringBuilder builder = new StringBuilder();
        final char[] lowerCaseChars = this.toString().toLowerCase().toCharArray();
        boolean lastCharWasSeparator = false;
        for (int i = 0; i < lowerCaseChars.length; i++)
        {
            final char character = lowerCaseChars[i];
            if (character == ENUM_SEPARATOR)
            {
                lastCharWasSeparator = true;
            }
            else
            {
                builder.append(lastCharWasSeparator ? Character.toUpperCase(character) : character);
                lastCharWasSeparator = false;
            }
        }
        return builder.toString();
    }

    private String getKebabCase()
    {
        return this.toString().toLowerCase().replace(ENUM_SEPARATOR, KEBAB_SEPARATOR);
    }
}
