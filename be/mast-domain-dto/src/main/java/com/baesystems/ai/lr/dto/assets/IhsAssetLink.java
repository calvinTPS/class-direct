package com.baesystems.ai.lr.dto.assets;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.fasterxml.jackson.annotation.JsonProperty;

public class IhsAssetLink implements Serializable
{
    @ConflictAware
    @Size(message = "invalid length", max = 7, min = 7)
    private String id;

    @JsonProperty("_id")
    private String internalId;

    public IhsAssetLink()
    {
        super();
        this.internalId = UUID.randomUUID().toString();
    }

    public IhsAssetLink(final String id)
    {
        this();
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(final String id)
    {
        this.id = id;
    }

    public String getInternalId()
    {
        return this.id == null ? this.internalId : this.id;
    }

    public void setInternalId(final String internalId)
    {
        if (internalId != null)
        {
            this.internalId = this.id == null ? internalId : this.id;
        }
    }

    @Override
    public boolean equals(final Object otherObject)
    {
        boolean equals = false;

        if (otherObject instanceof IhsAssetLink)
        {
            final IhsAssetLink other = (IhsAssetLink) otherObject;

            equals = Objects.equals(other.getId(), this.id);
        }

        return equals;
    }

    @Override
    public int hashCode()
    {
        return this.id == null ? 0 : this.id.hashCode();
    }
}
