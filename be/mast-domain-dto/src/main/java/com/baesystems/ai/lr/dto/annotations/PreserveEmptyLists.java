package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Tells sanitiseQueryDto not to null out empty lists.
 */
@Documented
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RUNTIME)
public @interface PreserveEmptyLists
{

}
