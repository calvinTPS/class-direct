package com.baesystems.ai.lr.enums;

public enum JobCategory
{
    SURVEY(1L),
    MMS(2L);

    private final Long value;

    JobCategory(final Long value)
    {
        this.value = value;
    }

    public Long getValue()
    {
        return value;
    }
}
