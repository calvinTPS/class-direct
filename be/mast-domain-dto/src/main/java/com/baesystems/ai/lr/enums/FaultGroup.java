package com.baesystems.ai.lr.enums;

public enum FaultGroup
{
    CLASS(1L),
    STATUTORY(2L);

    private final Long value;

    FaultGroup(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return this.value;
    }
}
