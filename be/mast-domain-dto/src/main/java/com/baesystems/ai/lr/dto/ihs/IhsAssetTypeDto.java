package com.baesystems.ai.lr.dto.ihs;

import java.io.Serializable;

import javax.validation.constraints.Size;

public class IhsAssetTypeDto implements Serializable
{
    private static final long serialVersionUID = 3700264812304200069L;

    @Size(message = "invalid length", max = 7)
    private String stat5Code;

    @Size(message = "invalid length", max = 50)
    private String statDeCode;

    public String getStat5Code()
    {
        return stat5Code;
    }

    public void setStat5Code(String stat5Code)
    {
        this.stat5Code = stat5Code;
    }

    public String getStatDeCode()
    {
        return statDeCode;
    }

    public void setStatDeCode(String statDeCode)
    {
        this.statDeCode = statDeCode;
    }
}
