package com.baesystems.ai.lr.enums;

public enum JobBundleRepairAction
{
    FAIL,
    STRIP_IDS,
    BREAK_LINKS;
}
