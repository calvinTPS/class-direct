package com.baesystems.ai.lr.dto.assets;

import static com.baesystems.ai.lr.dto.assets.AssetItemActionDto.ENTITY_NAME;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class AssetItemActionDto extends AuditedDto implements StaleObject, UpdateableJobBundleEntity
{
    private static final long serialVersionUID = -6561849725225707727L;
    public static final String ENTITY_NAME = "Asset Item Action";

    private String stalenessHash;

    @NotNull
    @Valid
    @ConflictAware
    private LinkResource item;

    @NotNull
    @Valid
    @ConflictAware
    private LinkResource job;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.ITEM_ACTIONS))
    private LinkResource itemAction;

    public LinkResource getItem()
    {
        return this.item;
    }

    public void setItem(final LinkResource item)
    {
        this.item = item;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public LinkResource getItemAction()
    {
        return this.itemAction;
    }

    public void setItemAction(final LinkResource itemAction)
    {
        this.itemAction = itemAction;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "AssetItemActionDto [item=" + getIdOrNull(this.item)
                + ", job=" + getIdOrNull(this.job)
                + ", itemAction=" + getIdOrNull(this.itemAction) + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }

}
