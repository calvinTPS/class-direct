package com.baesystems.ai.lr.dto.fees;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import javax.validation.Valid;
import java.util.List;

public class FeesReportPageResourceDto extends BasePageResource<FeeReportDto>
{
    @Valid
    private List<FeeReportDto> content;

    @Override
    public List<FeeReportDto> getContent()
    {
        return this.content;
    }

    @Override
    public void setContent(final List<FeeReportDto> content)
    {
        this.content = content;
    }
}
