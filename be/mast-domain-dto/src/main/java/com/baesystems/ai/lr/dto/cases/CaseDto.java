package com.baesystems.ai.lr.dto.cases;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.FlexibleMandatoryFields;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * Case type (1L=AIC; 2=AIMC; 3=First Entry; 4=TOC; 5=TOMC; 6=Non Classed

 */
@FlexibleMandatoryFields.List({
        @FlexibleMandatoryFields(profiles = {1L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                2,
                3,
                4,
                5,
                7,
                8,
                9}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "proposedFlagState"
        }),
        @FlexibleMandatoryFields(profiles = {2L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                2,
                3,
                4,
                5,
                7,
                8,
                9}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "proposedFlagState"
        }),
        @FlexibleMandatoryFields(profiles = {3L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                2,
                3,
                4,
                5,
                7,
                8,
                9}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "estimatedBuildDate",
                "proposedFlagState"
        }),
        @FlexibleMandatoryFields(profiles = {4L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                2,
                3,
                4,
                5,
                7,
                8,
                9}, mandatoryFields = {
                "contractReferenceNumber",
                "preEicInspectionStatus",
                "riskAssessmentStatus",
                "losingSociety",
                "caseAcceptanceDate",
                "proposedFlagState"
        }),
        @FlexibleMandatoryFields(profiles = {5L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                2,
                3,
                4,
                5,
                7,
                8,
                9}, mandatoryFields = {
                "contractReferenceNumber",
                "losingSociety",
                "caseAcceptanceDate",
                "proposedFlagState",
                "offices",
                "surveyors"}),
        @FlexibleMandatoryFields(profiles = {6L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                2,
                3,
                4,
                5,
                7,
                8,
                9}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "proposedFlagState"
        }),
        // Closing
        @FlexibleMandatoryFields(profiles = {1L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "proposedFlagState"}),
        @FlexibleMandatoryFields(profiles = {2L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "proposedFlagState"}),
        @FlexibleMandatoryFields(profiles = {3L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "proposedFlagState"
        }),
        @FlexibleMandatoryFields(profiles = {4L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                "contractReferenceNumber",
                "preEicInspectionStatus",
                "riskAssessmentStatus",
                "losingSociety",
                "caseAcceptanceDate",
                "proposedFlagState"}),
        @FlexibleMandatoryFields(profiles = {5L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                "contractReferenceNumber",
                "losingSociety",
                "caseAcceptanceDate",
                "proposedFlagState"}),
        @FlexibleMandatoryFields(profiles = {6L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                "contractReferenceNumber",
                "caseAcceptanceDate",
                "proposedFlagState"})})


public class CaseDto extends AuditedDto implements StaleObject
{
    private static final long serialVersionUID = -6648664306570326177L;
    public static final String ENTITY_NAME = "Case";


    @Valid
    private LinkResource asset;

    @JsonIgnore
    private Long assetVersionId;

    @Valid
    private LinkResource proposedFlagState;

    @NotNull
    @Valid
    private LinkResource caseType;

    @NotNull
    @Valid
    private LinkResource caseStatus;

    @Valid
    private LinkResource previousCaseStatus;

    @Valid
    private LinkResource preEicInspectionStatus;

    @Valid
    private LinkResource riskAssessmentStatus;

    private Date tocAcceptanceDate;

    @Size(message = "invalid length", max = 30)
    private String contractReferenceNumber;

    @Valid
    private List<CaseSurveyorWithLinksDto> surveyors;

    @Valid
    private List<OfficeLinkDto> offices;

    @Valid
    private List<CustomerLinkDto> customers;

    private Date createdOn;

    private Date caseAcceptanceDate;

    private Date estimatedBuildDate;

    private Date caseClosedDate;

    @Valid
    private List<CaseMilestoneDto> milestones;

    @Size(message = "invalid length", max = 500)
    private String statusReason;

    private LinkResource losingSociety;

    private String stalenessHash;

    public LinkResource getAsset()
    {
        return asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getProposedFlagState()
    {
        return proposedFlagState;
    }

    public void setProposedFlagState(final LinkResource proposedFlagState)
    {
        this.proposedFlagState = proposedFlagState;
    }

    public LinkResource getCaseType()
    {
        return caseType;
    }

    public void setCaseType(final LinkResource caseType)
    {
        this.caseType = caseType;
    }

    public LinkResource getCaseStatus()
    {
        return caseStatus;
    }

    public void setCaseStatus(final LinkResource caseStatus)
    {
        this.caseStatus = caseStatus;
    }

    public LinkResource getPreviousCaseStatus()
    {
        return previousCaseStatus;
    }

    public void setPreviousCaseStatus(final LinkResource previousCaseStatus)
    {
        this.previousCaseStatus = previousCaseStatus;
    }

    public LinkResource getPreEicInspectionStatus()
    {
        return preEicInspectionStatus;
    }

    public void setPreEicInspectionStatus(final LinkResource preEicInspectionStatus)
    {
        this.preEicInspectionStatus = preEicInspectionStatus;
    }

    public LinkResource getRiskAssessmentStatus()
    {
        return riskAssessmentStatus;
    }

    public void setRiskAssessmentStatus(final LinkResource riskAssessmentStatus)
    {
        this.riskAssessmentStatus = riskAssessmentStatus;
    }

    public Date getTocAcceptanceDate()
    {
        return tocAcceptanceDate;
    }

    public void setTocAcceptanceDate(final Date tocAcceptanceDate)
    {
        this.tocAcceptanceDate = tocAcceptanceDate;
    }

    public String getContractReferenceNumber()
    {
        return contractReferenceNumber;
    }

    public void setContractReferenceNumber(final String contractReferenceNumber)
    {
        this.contractReferenceNumber = contractReferenceNumber;
    }

    public List<CaseSurveyorWithLinksDto> getSurveyors()
    {
        return surveyors;
    }

    public void setSurveyors(final List<CaseSurveyorWithLinksDto> surveyors)
    {
        this.surveyors = surveyors;
    }

    public List<OfficeLinkDto> getOffices()
    {
        return offices;
    }

    public void setOffices(final List<OfficeLinkDto> offices)
    {
        this.offices = offices;
    }

    public List<CustomerLinkDto> getCustomers()
    {
        return customers;
    }

    public void setCustomers(final List<CustomerLinkDto> customers)
    {
        this.customers = customers;
    }

    public Date getCreatedOn()
    {
        return createdOn;
    }

    public void setCreatedOn(final Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getCaseAcceptanceDate()
    {
        return caseAcceptanceDate;
    }

    public void setCaseAcceptanceDate(final Date caseAcceptanceDate)
    {
        this.caseAcceptanceDate = caseAcceptanceDate;
    }

    public Date getEstimatedBuildDate()
    {
        return estimatedBuildDate;
    }

    public void setEstimatedBuildDate(final Date estimatedBuildDate)
    {
        this.estimatedBuildDate = estimatedBuildDate;
    }

    public List<CaseMilestoneDto> getMilestones()
    {
        return milestones;
    }

    public void setMilestones(final List<CaseMilestoneDto> milestones)
    {
        this.milestones = milestones;
    }

    public String getStatusReason()
    {
        return statusReason;
    }

    public void setStatusReason(final String statusReason)
    {
        this.statusReason = statusReason;
    }

    public Date getCaseClosedDate()
    {
        return caseClosedDate;
    }

    public void setCaseClosedDate(final Date caseClosedDate)
    {
        this.caseClosedDate = caseClosedDate;
    }

    public LinkResource getLosingSociety()
    {
        return losingSociety;
    }

    public void setLosingSociety(final LinkResource losingSociety)
    {
        this.losingSociety = losingSociety;
    }

    @JsonProperty("assetVersionId")
    public Long getAssetVersionId()
    {
        return assetVersionId;
    }

    @JsonIgnore
    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CaseDto [proposedFlagState=" + getIdOrNull(this.proposedFlagState)
                + ", caseType=" + getIdOrNull(this.caseType)
                + ", caseStatus=" + getIdOrNull(this.caseStatus)
                + ", previousCaseStatus=" + getIdOrNull(this.previousCaseStatus)
                + ", preEicInspectionStatus=" + getIdOrNull(this.preEicInspectionStatus)
                + ", riskAssessmentStatus=" + getIdOrNull(this.riskAssessmentStatus)
                + ", tocAcceptanceDate;=" + this.tocAcceptanceDate
                + ", contractReferenceNumber=" + this.contractReferenceNumber
                + ", surveyors=" + this.getStringRepresentationOfList(this.surveyors)
                + ", offices=" + this.getStringRepresentationOfList(this.offices)
                + ", customers=" + this.getStringRepresentationOfList(this.customers)
                + ", createdOn=" + this.getStringRepresentationOfDate(this.createdOn)
                + ", caseAcceptanceDate=" + this.getStringRepresentationOfDate(this.caseAcceptanceDate)
                + ", caseClosedDate=" + this.getStringRepresentationOfDate(this.caseClosedDate)
                + ", milestones=" + this.getStringRepresentationOfList(this.milestones)
                + ", losingSociety=" + getIdOrNull(this.losingSociety)
                + ", statusReason=" + this.statusReason +"]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
