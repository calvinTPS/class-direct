package com.baesystems.ai.lr.dto.query;

import java.io.Serializable;

import javax.persistence.criteria.JoinType;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.RequiredWithFields;
import com.baesystems.ai.lr.dto.base.Dto;

@RequiredWithFields.List({@RequiredWithFields(keyField = "joinType", fields = {"joinField", "field"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "alias", fields = {"joinField", "field"}, shouldBePopulated = true),
                          @RequiredWithFields(keyField = "joinField", fields = {"field"}, shouldBePopulated = true),
})
public class FieldDto implements Serializable, Dto
{
    private static final long serialVersionUID = 3029655519341476731L;

    private String joinField;

    private JoinType joinType;

    private String alias;

    @NotNull
    private String field;

    public String getJoinField()
    {
        return this.joinField;
    }

    public void setJoinField(final String joinField)
    {
        this.joinField = joinField;
    }

    public JoinType getJoinType()
    {
        return this.joinType;
    }

    public void setJoinType(final JoinType joinType)
    {
        this.joinType = joinType;
    }

    public String getAlias()
    {
        return this.alias;
    }

    public void setAlias(final String alias)
    {
        this.alias = alias;
    }

    public String getField()
    {
        return this.field;
    }

    public void setField(final String field)
    {
        this.field = field;
    }
}
