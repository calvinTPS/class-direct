package com.baesystems.ai.lr.dto.query;

import java.util.List;

public class AttachmentQueryDto implements QueryDto
{
    private static final long serialVersionUID = 592550111722227541L;

    private List<Long> itemIds;
    private String attachmentTargetType;

    public List<Long> getItemIds()
    {
        return itemIds;
    }

    public void setItemIds(final List<Long> itemIds)
    {
        this.itemIds = itemIds;
    }

    public String getAttachmentTargetType()
    {
        return attachmentTargetType;
    }

    public void setAttachmentTargetType(final String attachmentTargetType)
    {
        this.attachmentTargetType = attachmentTargetType;
    }
}
