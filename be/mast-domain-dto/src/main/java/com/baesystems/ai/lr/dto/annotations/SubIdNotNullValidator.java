package com.baesystems.ai.lr.dto.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.baesystems.ai.lr.dto.base.IdDto;

public class SubIdNotNullValidator implements ConstraintValidator<SubIdNotNull, IdDto>
{

    @Override
    public void initialize(final SubIdNotNull constraintAnnotation)
    {
    }

    @Override
    public boolean isValid(final IdDto object, final ConstraintValidatorContext constraintContext)
    {
        return object == null || (object.getId() != null && object.getId() > 0);
    }

}
