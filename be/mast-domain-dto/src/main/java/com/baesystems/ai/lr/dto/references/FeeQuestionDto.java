package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class FeeQuestionDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 960768744800220753L;

    private String name;

    private String questionSource;

    private String comments;

    private List<LinkResource> feeQuestionServices;

    private LinkResource feeDefaultAnswer;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getQuestionSource()
    {
        return questionSource;
    }

    public void setQuestionSource(final String questionSource)
    {
        this.questionSource = questionSource;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(final String comments)
    {
        this.comments = comments;
    }

    public List<LinkResource> getFeeQuestionServices()
    {
        return feeQuestionServices;
    }

    public void setFeeQuestionServices(final List<LinkResource> feeQuestionServices)
    {
        this.feeQuestionServices = feeQuestionServices;
    }

    public LinkResource getFeeDefaultAnswer()
    {
        return feeDefaultAnswer;
    }

    public void setFeeDefaultAnswer(final LinkResource feeDefaultAnswer)
    {
        this.feeDefaultAnswer = feeDefaultAnswer;
    }
}
