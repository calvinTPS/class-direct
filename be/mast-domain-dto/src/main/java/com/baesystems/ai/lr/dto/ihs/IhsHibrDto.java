package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsHibrDto extends IhsBaseDto
{

    private static final long serialVersionUID = -760850423668635932L;

    private Double breadthMoulded;

    private Double breadthExtreme;

    public Double getBreadthMoulded()
    {
        return breadthMoulded;
    }

    public void setBreadthMoulded(final Double breadthMoulded)
    {
        this.breadthMoulded = breadthMoulded;
    }

    public Double getBreadthExtreme()
    {
        return breadthExtreme;
    }

    public void setBreadthExtreme(final Double breadthExtreme)
    {
        this.breadthExtreme = breadthExtreme;
    }
}
