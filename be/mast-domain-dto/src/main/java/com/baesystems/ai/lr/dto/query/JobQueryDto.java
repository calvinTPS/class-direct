package com.baesystems.ai.lr.dto.query;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

public class JobQueryDto extends BaseJobQueryDto
{
    private static final long serialVersionUID = 2645240319801702689L;

    @UsesLikeComparator
    private String search;

    public String getSearch()
    {
        return search;
    }

    public void setSearch(final String search)
    {
        this.search = search;
    }
}
