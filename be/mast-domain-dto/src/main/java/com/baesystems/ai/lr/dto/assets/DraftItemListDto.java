package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.MatchingNullPattern;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class DraftItemListDto extends BaseDto
{
    private static final long serialVersionUID = -5938979907798915558L;

    private Boolean copyAttributes;

    private Boolean placeNewItemBelowOriginal;

    @Valid
    @NotNull
    @MatchingNullPattern(fields = {"fromId", "itemTypeId"})
    private List<DraftItemDto> draftItemDtoList;

    public Boolean getCopyAttributes()
    {
        return copyAttributes;
    }

    public void setCopyAttributes(final Boolean copyAttributes)
    {
        this.copyAttributes = copyAttributes;
    }

    public Boolean getPlaceNewItemBelowOriginal()
    {
        return placeNewItemBelowOriginal;
    }

    public void setPlaceNewItemBelowOriginal(final Boolean placeNewItemBelowOriginal)
    {
        this.placeNewItemBelowOriginal = placeNewItemBelowOriginal;
    }

    public List<DraftItemDto> getDraftItemDtoList()
    {
        return draftItemDtoList;
    }

    public void setDraftItemDtoList(final List<DraftItemDto> draftItemDtoList)
    {
        this.draftItemDtoList = draftItemDtoList;
    }

}
