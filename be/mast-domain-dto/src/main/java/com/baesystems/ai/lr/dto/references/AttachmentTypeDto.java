package com.baesystems.ai.lr.dto.references;

public class AttachmentTypeDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 3016202029660405837L;

    private String fileExtension;

    public String getFileExtension()
    {
        return fileExtension;
    }

    public void setFileExtension(final String fileExtension)
    {
        this.fileExtension = fileExtension;
    }
}
