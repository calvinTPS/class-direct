package com.baesystems.ai.lr.dto.fees;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;

public class FeeReportDto extends BaseDto
{
    private static final long serialVersionUID = 8646392947894915633L;

    @Valid
    private LinkResource job;

    @JsonRawValue
    private String content;

    @NotNull
    private int approved;

    public LinkResource getJob()
    {
        return job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public void setApproved(final int approved)
    {
        this.approved = approved;
    }

    public int getApproved()
    {
        return this.approved;
    }

    @JsonSetter
    public void setContent(final JsonNode content)
    {
        this.content = content.toString();
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return this.content;
    }
}
