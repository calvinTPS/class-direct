package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.LinkResource;

public class ServiceCatalogueDto extends ServiceCatalogueLightDto
{
    private static final long serialVersionUID = -1791901422649527483L;

    private ProductCatalogueDto productCatalogue; // The full object is given here so that the frontend can sort on the
                                                  // ruleset and product family

    private List<LinkResource> flags;

    private List<ServiceCatalogueRelationshipDto> relationships;

    public ProductCatalogueDto getProductCatalogue()
    {
        return productCatalogue;
    }

    public void setProductCatalogue(final ProductCatalogueDto productCatalogue)
    {
        this.productCatalogue = productCatalogue;
    }

    public List<LinkResource> getFlags()
    {
        return flags;
    }

    public void setFlags(final List<LinkResource> flags)
    {
        this.flags = flags;
    }

    public List<ServiceCatalogueRelationshipDto> getRelationships()
    {
        return relationships;
    }

    public void setRelationships(final List<ServiceCatalogueRelationshipDto> relationships)
    {
        this.relationships = relationships;
    }
}
