package com.baesystems.ai.lr.dto.query;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.Validate;
import com.baesystems.ai.lr.dto.base.Dto;

public class AbstractSubQueryDto implements Serializable, Dto
{
    private static final long serialVersionUID = 7706624531809448504L;

    // the field on the parent object that has to be in the list generated by the result of this query.
    @NotNull
    private String fieldOnParentObject;

    // the field to select from the sub query (this has to be a single field tuples are not supported at the moment)
    @NotNull
    private String selecting;

    // an abstract query describing the sub query.
    @Validate
    @NotNull
    private AbstractQueryDto subQuery;

    public String getFieldOnParentObject()
    {
        return fieldOnParentObject;
    }

    public void setFieldOnParentObject(final String fieldOnParentObject)
    {
        this.fieldOnParentObject = fieldOnParentObject;
    }

    public String getSelecting()
    {
        return selecting;
    }

    public void setSelecting(final String selecting)
    {
        this.selecting = selecting;
    }

    public AbstractQueryDto getSubQuery()
    {
        return subQuery;
    }

    public void setSubQuery(final AbstractQueryDto subQuery)
    {
        this.subQuery = subQuery;
    }
}
