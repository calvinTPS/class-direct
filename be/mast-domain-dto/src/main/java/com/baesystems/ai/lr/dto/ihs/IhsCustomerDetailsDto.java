package com.baesystems.ai.lr.dto.ihs;

public class IhsCustomerDetailsDto
{
    private static final long serialVersionUID = -8312783587182959304L;

    private String name;

    private Long companyId;

    private String telephone;

    private String fax;

    private String addressLine1;

    private String addressLine2;

    private String addressLine3;

    private String city;

    private String postcode;

    private String country;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Long getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(final Long companyId)
    {
        this.companyId = companyId;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone(final String telephone)
    {
        this.telephone = telephone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(final String fax)
    {
        this.fax = fax;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(final String city)
    {
        this.city = city;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(final String postcode)
    {
        this.postcode = postcode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(final String country)
    {
        this.country = country;
    }
}
