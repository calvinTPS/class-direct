package com.baesystems.ai.lr.dto;

public class LinkVersionedRelationshipResource extends LinkVersionedResource
{
    private static final long serialVersionUID = 5362099929385556516L;

    private long relationshipId;

    public long getRelationshipId()
    {
        return relationshipId;
    }

    public void setRelationshipId(final long relationshipId)
    {
        this.relationshipId = relationshipId;
    }

    public LinkVersionedRelationshipResource(final Long id, final Long version, final long relationshipId)
    {
        super(id, version);
        this.relationshipId = relationshipId;
    }

    public LinkVersionedRelationshipResource()
    {
        super();
    }
}
