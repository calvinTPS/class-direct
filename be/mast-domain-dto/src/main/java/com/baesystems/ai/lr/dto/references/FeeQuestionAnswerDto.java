package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class FeeQuestionAnswerDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -3989460935021516909L;

    private Boolean isDefaultAnswer;

    @Valid
    @NotNull
    private LinkResource feeAnswer;

    @Valid
    @NotNull
    private LinkResource feeQuestion;

    public Boolean getIsDefaultAnswer()
    {
        return isDefaultAnswer;
    }

    public void setIsDefaultAnswer(final Boolean isDefaultAnswer)
    {
        this.isDefaultAnswer = isDefaultAnswer;
    }

    public LinkResource getFeeAnswer()
    {
        return feeAnswer;
    }

    public void setFeeAnswer(final LinkResource feeAnswer)
    {
        this.feeAnswer = feeAnswer;
    }

    public LinkResource getFeeQuestion()
    {
        return feeQuestion;
    }

    public void setFeeQuestion(final LinkResource feeQuestion)
    {
        this.feeQuestion = feeQuestion;
    }
}
