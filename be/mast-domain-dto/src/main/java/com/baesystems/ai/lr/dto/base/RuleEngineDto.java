package com.baesystems.ai.lr.dto.base;

import java.io.Serializable;

/**
 * Marker for Rule Engine specific data objects
 */
public interface RuleEngineDto extends Dto, Serializable
{
}
