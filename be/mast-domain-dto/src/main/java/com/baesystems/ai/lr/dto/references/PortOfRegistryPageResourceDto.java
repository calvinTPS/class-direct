package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class PortOfRegistryPageResourceDto extends BasePageResource<PortOfRegistryDto>
{
    private List<PortOfRegistryDto> content;

    @Override
    public List<PortOfRegistryDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<PortOfRegistryDto> content)
    {
        this.content = content;
    }

}
