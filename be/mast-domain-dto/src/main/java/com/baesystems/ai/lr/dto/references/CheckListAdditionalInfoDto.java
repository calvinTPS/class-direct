package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;

public class CheckListAdditionalInfoDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -1168012298248195487L;

    private LinkResource checklist;

    private LinkResource attributeDataType;

    public LinkResource getChecklist()
    {
        return checklist;
    }

    public void setChecklist(final LinkResource checklist)
    {
        this.checklist = checklist;
    }

    public LinkResource getAttributeDataType()
    {
        return attributeDataType;
    }

    public void setAttributeDataType(final LinkResource attributeDataType)
    {
        this.attributeDataType = attributeDataType;
    }

}
