package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.baesystems.ai.lr.enums.ReferenceDataSubSet;

/**
 * To be used on fields that are to be added to the conflict report if a conflict occurs between to objects. This should
 * be all fields that are part of the staleness hash. This also take several parameters that describe how the conflict
 * report should look if a conflict occurs on the given field.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConflictAware
{
    /**
     * Whether to use the field name which camel case converted to spaces as the display name. By default this will be
     * true so if nothing else is specified the spacified field name will be the display name. If other options are set
     * they will override this.
     */
    boolean autoSpaceName() default true;

    /**
     * Whether or not to capitalise the first letter of each word in the field name. True by default.
     */
    boolean correctFieldCase() default true;

    /**
     * Whether or not to capitalise the first letter of each word in the value name if the value is from reference data.
     * True by default.
     */
    boolean correctValueCase() default true;

    /**
     * Use this field to set a constant string that will appear as the field name in the conflict report is there is a
     * conflict on this field.
     */
    String staticFieldDisplayName() default "";

    /**
     * Use this field to set the display name for the field dynamically from the reference data based on the value of
     * the field using the instructions within the annotation. If there is an input value this will be used in
     * preference to the existing value as the user is more likely to recognise the value that they entered. If they
     * give a different result, they shouldn't really because users should only be able to change like for like.
     */
    DynamicDisplayName dynamicFieldDisplayName() default @DynamicDisplayName(targetType = ReferenceDataSubSet.ACTIONS_TAKEN, active = false);

    /**
     * Use this field to describe the source of reference data for any link resources etc that represent things like
     * statuses as the UI is supposed to display the name of the existing and input values for things that are chosen
     * from reference data. If this is applicable to this field then the conflict report service will get the requested
     * field from the data for both input and existing values.
     */
    DynamicDisplayName dynamicValueDisplayName() default @DynamicDisplayName(targetType = ReferenceDataSubSet.ACTIONS_TAKEN, active = false);
}
