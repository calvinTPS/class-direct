package com.baesystems.ai.lr.enums;

import java.util.Arrays;

public enum FeeOperation
{
    DO_NOTHING(1L), // No change to fee unit
    MULTIPLY_BY_FACTOR(2L), // Multiply the fee unit of the service by Operation Input
    ADD(3L), // Add the Operation Input to the Fee Unit
    USE_GIVEN_FEE(4L), // Use the Fee Unit of another Service, given by Service Code
    MULTIPLY_GIVEN_FEE(5L), // Use the Fee Unit of another Service, given by Service Code, and multiply it by the Operation Input
    SET_TO_VALUE(6L); // Set the Fee Unit to the Operation Input

    private final long value;

    FeeOperation(final long value)
    {
        this.value = value;
    }

    public long getValue()
    {
        return value;
    }

    public static FeeOperation getOperation(final long id)
    {
        return Arrays
                .stream(FeeOperation.values())
                .filter(operation -> operation.getValue() == id)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
