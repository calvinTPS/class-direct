package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = SubIdNotNullValidator.class)
@Documented
public @interface SubIdNotNull
{
    String message() default "must have a valid id";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
