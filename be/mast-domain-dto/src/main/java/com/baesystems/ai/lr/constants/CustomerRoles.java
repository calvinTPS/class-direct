package com.baesystems.ai.lr.constants;

public class CustomerRoles
{
    public static final String SHIP_BUILDER_ROLE = "8";
    public static final String SHIP_MANAGER_ROLE = "3";
    public static final String SHIP_OPERATOR_ROLE = "2";
    public static final String TECHNICAL_MANAGER_ROLE = "14";
    public static final String DOC_COMPANY_ROLE = "10";
    public static final String REGISTERED_OWNER_ROLE = "4";
    public static final String GROUP_BENEFICIAL_OWNER = "6";

    private CustomerRoles()
    {
        // no constructor
    }
}
