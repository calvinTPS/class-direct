package com.baesystems.ai.lr.dto.references;

public class WorkItemActionDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 9086962175800512168L;

    private String referenceCode;

    private String taskCategory;

    private Integer weighting;

    private Boolean creditAllowedX;

    private Boolean creditAllowedW;

    private Boolean creditAllowedE;

    private Boolean creditAllowedC;

    private Boolean setsCompletion;

    public String getTaskCategory()
    {
        return taskCategory;
    }

    public void setTaskCategory(final String taskCategory)
    {
        this.taskCategory = taskCategory;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public Integer getWeighting()
    {
        return weighting;
    }

    public void setWeighting(final Integer weighting)
    {
        this.weighting = weighting;
    }

    public Boolean getCreditAllowedX()
    {
        return this.creditAllowedX;
    }

    public void setCreditAllowedX(final Boolean creditAllowedX)
    {
        this.creditAllowedX = creditAllowedX;
    }

    public Boolean getCreditAllowedW()
    {
        return this.creditAllowedW;
    }

    public void setCreditAllowedW(final Boolean creditAllowedW)
    {
        this.creditAllowedW = creditAllowedW;
    }

    public Boolean getCreditAllowedC()
    {
        return this.creditAllowedC;
    }

    public void setCreditAllowedC(final Boolean creditAllowedC)
    {
        this.creditAllowedC = creditAllowedC;
    }

    public Boolean getCreditAllowedE()
    {
        return this.creditAllowedE;
    }

    public void setCreditAllowedE(final Boolean creditAllowedE)
    {
        this.creditAllowedE = creditAllowedE;
    }

    public Boolean getSetsCompletion()
    {
        return setsCompletion;
    }

    public void setSetsCompletion(final Boolean setsCompletion)
    {
        this.setsCompletion = setsCompletion;
    }
}
