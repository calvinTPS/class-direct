package com.baesystems.ai.lr.enums;

public enum SREServiceMethod
{
    NEXT_SERVICES("nextServices"),
    MANUAL_ENTRY_SERVICE("manualEntryService"),
    CERTIFICATE_EXPIRY_DATES("calculateCertificateExpiryDate");

    private final String methodName;

    private SREServiceMethod(final String methodName)
    {
        this.methodName = methodName;
    }

    public String getMethodName()
    {
        return methodName;
    }
}
