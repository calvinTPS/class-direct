package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.List;

public enum OfficeRole
{
    SDO(1L),
    CFO(2L),
    MMSO(5L),
    CLASS_GROUP(8L),
    EIC(10L),
    MDS(11L),
    MMS(16L);

    private final Long value;

    OfficeRole(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    public static List<Long> getMandatoryRolesForCase()
    {
        return Arrays.asList(SDO.getValue());
    }
}
