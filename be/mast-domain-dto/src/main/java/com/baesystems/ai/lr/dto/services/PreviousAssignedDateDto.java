package com.baesystems.ai.lr.dto.services;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.Dto;

public class PreviousAssignedDateDto implements Serializable, Dto
{
    private static final long serialVersionUID = -5789182596279307486L;

    @NotNull
    private Long serviceCatalogueId;

    private Long assetItemId;

    @NotNull
    private Date previousAssignedDate;

    public Long getServiceCatalogueId()
    {
        return serviceCatalogueId;
    }

    public void setServiceCatalogueId(final Long serviceCatalogueId)
    {
        this.serviceCatalogueId = serviceCatalogueId;
    }

    public Long getAssetItemId()
    {
        return assetItemId;
    }

    public void setAssetItemId(final Long assetItemId)
    {
        this.assetItemId = assetItemId;
    }

    public Date getPreviousAssignedDate()
    {
        return previousAssignedDate;
    }

    public void setPreviousAssignedDate(final Date previousAssignedDate)
    {
        this.previousAssignedDate = previousAssignedDate;
    }
}
