package com.baesystems.ai.lr.dto.base;

import java.util.Date;

/**
 * This interface is for any DTO used in conflict resolution for upversioning initiated by the reporting process.
 */
public interface HashedParentReportingDto
{
    String getParentHash();

    Date getSourceJobLastVisitDate();

    void setSourceJobLastVisitDate(Date sourceJobLastVisitDate);

    Date getSourceJobCreationDateTime();

    void setSourceJobCreationDateTime(Date sourceJobCreationDateTime);
}
