package com.baesystems.ai.lr.enums;

public enum ServiceCatalogueRelationshipType
{
    IS_DEPENDENT_ON(1L),
    IS_COUNTERPART_OF(2L);

    private final Long value;

    ServiceCatalogueRelationshipType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
