package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;

/**
 * This DTO is used for attribute value updates, so that they may be updated in a single call.
 */
public class ItemWithAttributesDto extends ItemLightDto
{
    private static final long serialVersionUID = -4267363168907923576L;

    @Valid
    private List<AttributeDto> attributes;

    public List<AttributeDto> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(final List<AttributeDto> attributes)
    {
        this.attributes = attributes;
    }
}
