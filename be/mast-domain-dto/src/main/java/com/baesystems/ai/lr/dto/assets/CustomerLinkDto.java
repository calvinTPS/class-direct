package com.baesystems.ai.lr.dto.assets;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;

@IgnoreIdsInConflicts
public class CustomerLinkDto extends BaseDto
{
    private static final long serialVersionUID = 5772816887977390746L;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.PARTY))
    private PartyDto customer;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.PARTY_ROLES))
    private LinkResource relationship;

    public PartyDto getCustomer()
    {
        return this.customer;
    }

    public void setCustomer(final PartyDto customer)
    {
        this.customer = customer;
    }

    public LinkResource getRelationship()
    {
        return this.relationship;
    }

    public void setRelationship(final LinkResource relationship)
    {
        this.relationship = relationship;
    }

}
