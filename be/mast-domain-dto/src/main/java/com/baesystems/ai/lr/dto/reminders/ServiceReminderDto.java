package com.baesystems.ai.lr.dto.reminders;

import java.util.Date;

public class ServiceReminderDto extends BaseReminderDto
{
    private static final long serialVersionUID = 4866395288525874758L;

    private String serviceCode;

    private String serviceName;

    private Date postponementDate;

    private Date upperRangeDate;

    public String getServiceCode()
    {
        return this.serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public String getServiceName()
    {
        return this.serviceName;
    }

    public void setServiceName(final String serviceName)
    {
        this.serviceName = serviceName;
    }

    public Date getPostponementDate()
    {
        return this.postponementDate;
    }

    public void setPostponementDate(final Date postponementDate)
    {
        this.postponementDate = postponementDate;
    }

    public Date getUpperRangeDate()
    {
        return this.upperRangeDate;
    }

    public void setUpperRangeDate(final Date upperRangeDate)
    {
        this.upperRangeDate = upperRangeDate;
    }
}
