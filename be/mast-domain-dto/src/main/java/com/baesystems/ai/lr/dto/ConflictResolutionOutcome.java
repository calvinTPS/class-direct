package com.baesystems.ai.lr.dto;

import java.util.List;

public class ConflictResolutionOutcome<T extends StaleObject>
{
    private T objectToPersist;

    private List<ConflictedFieldSummaryDto> conflictedFields;

    private boolean retryUpdate;

    // if true the action will be taken, but the conflict will not appear on the report.
    private boolean suppressed = false;

    public T getObjectToPersist()
    {
        return this.objectToPersist;
    }

    public void setObjectToPersist(final T objectToPersist)
    {
        this.objectToPersist = objectToPersist;
    }

    public List<ConflictedFieldSummaryDto> getConflictedFields()
    {
        return this.conflictedFields;
    }

    public void setConflictedFields(final List<ConflictedFieldSummaryDto> conflictedFields)
    {
        this.conflictedFields = conflictedFields;
    }

    public boolean getRetryUpdate()
    {
        return this.retryUpdate;
    }

    public void setRetryUpdate(final boolean retryUpdate)
    {
        this.retryUpdate = retryUpdate;
    }

    public boolean isSuppressed()
    {
        return suppressed;
    }

    public void suppress()
    {
        this.suppressed = true;
    }
}
