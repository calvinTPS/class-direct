package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class AssetTypeGroupDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -8800960867165380670L;

    @Valid
    private String name;

    public final String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

}
