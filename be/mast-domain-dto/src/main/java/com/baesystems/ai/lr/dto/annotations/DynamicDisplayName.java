package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.baesystems.ai.lr.enums.ReferenceDataSubSet;

/**
 * Annotation to be used with @ConflictAware to describe how to get the dynamic names of fields and values for job
 * bundle conflicts.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DynamicDisplayName
{
    /**
     * The path to the id field to be used on the source object (the dto that appears as input or existing value in the
     * conflict report). The path is relative to the current object so does not assume that the field that the
     * annotation is on is the beginning of the path. If this is not set it will be the assumed to be <fieldName>.id.
     */
    String idFieldOnSource() default "";

    /**
     * The path to the id field on the target reference data dto object. The starts from the object specified in
     * targetType.
     */
    String idFieldOnTarget() default "id";

    /**
     * The path to the name field on the target reference data dto object (the object whose string version will appear
     * as the value in the conflict report). The starts from the object specified in targetType.
     */
    String nameFieldOnTarget() default "name";

    /**
     * The source of the reference data.
     */
    ReferenceDataSubSet targetType();

    /**
     * To be used if the reference data needed is nested inside another reference data object. For example defect values
     * are nested inside defect details. Lists are dealt with and flattened automatically.
     */
    String flattenTarget() default "";

    /**
     * If the name returned from the reference data isn't exactly what needs to be displayed this can be used to display
     * a different value based on the value returned (this requires an exact match).
     */
    OverrideDisplayName[] overrideNames() default {};

    /**
     * To specify a formatter to insert the value into in case other words are required before or after the string. This
     * must include exactly one %s place holder for the value found from the reference data. By default it will return
     * the value taken from the data on it's own.
     */
    String format() default "%s";

    /**
     * This should not be used directly on a field annotation it is provided so that it can be set to false as a default
     * in other annotations where
     */
    boolean active() default true;
}
