package com.baesystems.ai.lr.dto.paging;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.base.Dto;

public class PageLinkDto implements Dto, Serializable
{
    private static final long serialVersionUID = 5070210221437891280L;

    private String rel;

    private String href;

    public String getRel()
    {
        return rel;
    }

    public void setRel(final String rel)
    {
        this.rel = rel;
    }

    public String getHref()
    {
        return href;
    }

    public void setHref(final String href)
    {
        this.href = href;
    }
}
