package com.baesystems.ai.lr.enums;

public enum ReportValidationMessageEnum
{
    ASSET_IS_CHECKED_OUT(1L),
    OPEN_DEFECT(2L),
    CLOSED_DEFECT(3L),
    SURVEYS(4L),
    NO_PARENT(5L),
    TEMP_COC(64L),
    OCCURRENCE_NUMBER(66L);

    private long value;

    ReportValidationMessageEnum(final long value)
    {
        this.value = value;
    }

    public long getValue()
    {
        return this.value;
    }
}
