package com.baesystems.ai.lr.dto.customers;

import com.baesystems.ai.lr.dto.base.MutableReferenceDataDto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PartyBaseDto extends MutableReferenceDataDto
{
    private static final long serialVersionUID = 5772816887977390746L;

    @NotNull
    @Size(message = "invalid length", max = 100)
    private String name;

    private String imoNumber;

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }
}
