package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.base.BaseDto;

@IgnoreIdsInConflicts
public class RepairItemDto extends BaseDto
{
    private static final long serialVersionUID = 2207746146624404895L;

    @NotNull
    @Valid
    @ConflictAware
    private DefectItemLinkDto item; // This is the id in the defect item table not the id of the item.

    @Valid
    @ConflictAware
    private LinkResource parent;

    @Valid
    private LinkResource wipRepairItem;

    public DefectItemLinkDto getItem()
    {
        return this.item;
    }

    public void setItem(final DefectItemLinkDto item)
    {
        this.item = item;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public LinkResource getWipRepairItem()
    {
        return this.wipRepairItem;
    }

    public void setWipRepairItem(final LinkResource wipRepairItem)
    {
        this.wipRepairItem = wipRepairItem;
    }
}
