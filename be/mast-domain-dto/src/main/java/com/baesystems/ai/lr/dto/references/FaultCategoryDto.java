package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;

public class FaultCategoryDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 6193987923324562488L;

    @Size(message = "invalid length", max = 1)
    private String categoryLetter;

    @Valid
    private LinkResource parent;

    @Valid
    private LinkResource faultGroup;

    @Valid
    private LinkResource productCatalogue;

    @Valid
    private LinkResource codicilCategory;

    public String getCategoryLetter()
    {
        return categoryLetter;
    }

    public void setCategoryLetter(final String categoryLetter)
    {
        this.categoryLetter = categoryLetter;
    }

    public LinkResource getParent()
    {
        return parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public LinkResource getFaultGroup()
    {
        return faultGroup;
    }

    public void setFaultGroup(final LinkResource faultGroup)
    {
        this.faultGroup = faultGroup;
    }

    public LinkResource getProductCatalogue()
    {
        return productCatalogue;
    }

    public void setProductCatalogue(final LinkResource productCatalogue)
    {
        this.productCatalogue = productCatalogue;
    }

    public LinkResource getCodicilCategory()
    {
        return codicilCategory;
    }

    public void setCodicilCategory (final LinkResource codicilCategory)
    {
        this.codicilCategory = codicilCategory;
    }
}
