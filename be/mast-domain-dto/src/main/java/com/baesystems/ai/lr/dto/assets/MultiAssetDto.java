package com.baesystems.ai.lr.dto.assets;

import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;

public class MultiAssetDto
{
    private String imoNumber;

    private IhsAssetDetailsDto ihsAsset;

    private AssetLightDto mastAsset;

    private Boolean flagMMS;

    public String getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public IhsAssetDetailsDto getIhsAsset()
    {
        return this.ihsAsset;
    }

    public void setIhsAsset(final IhsAssetDetailsDto ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public AssetLightDto getMastAsset()
    {
        return this.mastAsset;
    }

    public void setMastAsset(final AssetLightDto mastAsset)
    {
        this.mastAsset = mastAsset;
    }

    public Boolean getFlagMMS()
    {
        return flagMMS;
    }

    public void setFlagMMS(final Boolean flagMMS)
    {
        this.flagMMS = flagMMS;
    }

    @Override
    public boolean equals(final Object obj2)
    {
        boolean returnValue = false;

        if (MultiAssetDto.class.isInstance(obj2))
        {
            final MultiAssetDto multiAsset2 = MultiAssetDto.class.cast(obj2);
            returnValue = multiAsset2.getImoNumber() != null && this.imoNumber != null && this.imoNumber.equals(multiAsset2.getImoNumber());
        }

        return returnValue;
    }

    @Override
    public int hashCode()
    {
        int hashCode = super.hashCode();

        if (this.imoNumber != null)
        {
            hashCode = this.imoNumber.hashCode();
        }

        return hashCode;
    }
}
