package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;

public class ChecklistAssetTypeGroupDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 7755728077482734024L;

    @NotNull
    private LinkResource checklist;

    @NotNull
    private LinkResource assetTypeGroup;

    public LinkResource getChecklist()
    {
        return checklist;
    }

    public void setChecklist(final LinkResource checklist)
    {
        this.checklist = checklist;
    }

    public LinkResource getAssetTypeGroup()
    {
        return assetTypeGroup;
    }

    public void setAssetTypeGroup(final LinkResource assetTypeGroup)
    {
        this.assetTypeGroup = assetTypeGroup;
    }
}
