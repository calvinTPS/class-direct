package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class CodicilTypeDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -1141697433705951699L;

    @Size(message = "invalid length", max = 100)
    private String name;

    @Size(message = "invalid length", max = 3)
    private String code;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }
}
