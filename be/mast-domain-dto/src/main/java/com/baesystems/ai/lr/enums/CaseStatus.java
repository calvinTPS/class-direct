package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.List;

public enum CaseStatus
{
    UNCOMMITTED(1L),
    POPULATING(2L),
    ONHOLD(3L),
    JOB_PHASE(4L),
    VALIDATE_AND_UPDATE(5L),
    CLOSED(6L),
    CANCELLED(7L),
    DELETED(8L);

    private final Long value;

    CaseStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    /**
     * @return a list of CaseStatusType values for use when querying the database for duplicate cases
     */
    public static final List<Long> getDuplicateCaseStatusSearch()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(UNCOMMITTED.getValue());
        statuses.add(POPULATING.getValue());
        statuses.add(JOB_PHASE.getValue());
        statuses.add(VALIDATE_AND_UPDATE.getValue());
        statuses.add(ONHOLD.getValue());

        return statuses;
    }

    /**
     * @return a list of CaseStatusType values that count as open
     */
    public static final List<Long> getCaseStatusForOpen()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(POPULATING.getValue());
        statuses.add(JOB_PHASE.getValue());
        statuses.add(VALIDATE_AND_UPDATE.getValue());
        statuses.add(ONHOLD.getValue());

        return statuses;
    }

    /**
     * @return a list of CaseStatusType values for use when updating case milestones
     */
    public static final List<Long> getCaseStatusForUpdateCaseMilestone()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(POPULATING.getValue());
        statuses.add(JOB_PHASE.getValue());
        statuses.add(VALIDATE_AND_UPDATE.getValue());

        return statuses;
    }
    
    public static List<Long> openStatuses()
    {
        final List<Long> openStatusesList = new ArrayList<Long>();
        openStatusesList.add(CaseStatus.UNCOMMITTED.getValue());
        openStatusesList.add(CaseStatus.POPULATING.getValue());
        openStatusesList.add(CaseStatus.ONHOLD.getValue());
        openStatusesList.add(CaseStatus.JOB_PHASE.getValue());
        openStatusesList.add(CaseStatus.VALIDATE_AND_UPDATE.getValue());
        return openStatusesList;
    }
}
