package com.baesystems.ai.lr.dto.query;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

public class FaultQueryDto implements QueryDto
{
    private static final long serialVersionUID = -4605642967914330082L;

    @UsesLikeComparator
    private String title;

    private List<Long> categoryList;

    @UsesLikeComparator
    private String incidentDescription;

    private Date incidentDateMin;

    private Date incidentDateMax;

    private List<Long> confidentialityTypeList;

    private List<Integer> sequenceNumberList;

    private List<Long> statusList;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public List<Long> getCategoryList()
    {
        return categoryList;
    }

    public void setCategoryList(final List<Long> categoryList)
    {
        this.categoryList = categoryList;
    }

    public String getIncidentDescription()
    {
        return incidentDescription;
    }

    public void setIncidentDescription(final String incidentDescription)
    {
        this.incidentDescription = incidentDescription;
    }

    public Date getIncidentDateMin()
    {
        return incidentDateMin;
    }

    public void setIncidentDateMin(final Date incidentDateMin)
    {
        this.incidentDateMin = incidentDateMin;
    }

    public Date getIncidentDateMax()
    {
        return incidentDateMax;
    }

    public void setIncidentDateMax(final Date incidentDateMax)
    {
        this.incidentDateMax = incidentDateMax;
    }

    public List<Long> getConfidentialityTypeList()
    {
        return confidentialityTypeList;
    }

    public void setConfidentialityTypeList(final List<Long> confidentialityTypeList)
    {
        this.confidentialityTypeList = confidentialityTypeList;
    }

    public List<Integer> getSequenceNumberList()
    {
        return sequenceNumberList;
    }

    public void setSequenceNumberList(final List<Integer> sequenceNumberList)
    {
        this.sequenceNumberList = sequenceNumberList;
    }

    public List<Long> getStatusList()
    {
        return statusList;
    }

    public void setStatusList(final List<Long> statusList)
    {
        this.statusList = statusList;
    }

}
