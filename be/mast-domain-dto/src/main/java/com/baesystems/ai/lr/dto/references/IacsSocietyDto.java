package com.baesystems.ai.lr.dto.references;

public class IacsSocietyDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 2162125634454174010L;

    private String code;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }
}
