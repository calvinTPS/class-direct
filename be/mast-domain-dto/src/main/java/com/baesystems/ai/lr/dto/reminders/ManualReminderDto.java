package com.baesystems.ai.lr.dto.reminders;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class ManualReminderDto extends BaseDto
{
    private static final long serialVersionUID = -8958937266962904592L;

    @NotNull
    private String title;

    @NotNull
    @Valid
    private LinkResource employee;

    @NotNull
    private Integer severityId;

    @NotNull
    private Boolean watchFlag;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public LinkResource getEmployee()
    {
        return employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public Integer getSeverityId()
    {
        return severityId;
    }

    public void setSeverityId(final Integer severityId)
    {
        this.severityId = severityId;
    }

    public Boolean getWatchFlag()
    {
        return watchFlag;
    }

    public void setWatchFlag(final Boolean watchFlag)
    {
        this.watchFlag = watchFlag;
    }

}
