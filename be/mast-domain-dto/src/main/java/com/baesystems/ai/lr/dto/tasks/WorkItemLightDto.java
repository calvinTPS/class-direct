package com.baesystems.ai.lr.dto.tasks;

import static com.baesystems.ai.lr.dto.tasks.WorkItemLightDto.ENTITY_NAME;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class WorkItemLightDto extends BaseWorkItemDto implements UpdateableJobBundleEntity, HashedParentReportingDto
{
    public static final String ENTITY_NAME = "Task";
    private static final long serialVersionUID = 8252436811258196440L;

    private String stalenessHash;

    // todo generalise FlexibleMandatoryFieldsValidator for single mode column
    @Valid
    @ConflictAware
    private LinkResource assetItem;

    @Valid
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.SURVEY)
    private LinkResource survey;

    @Valid
    @ConflictAware
    private LinkResource scheduledService;

    @Valid
    @ConflictAware
    private LinkResource conditionalParent;

    @Valid
    @ConflictAware
    private LinkResource workItem;

    public LinkResource getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final LinkResource assetItem)
    {
        this.assetItem = assetItem;
    }

    public LinkResource getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final LinkResource survey)
    {
        this.survey = survey;
    }

    public LinkResource getScheduledService()
    {
        return this.scheduledService;
    }

    public void setScheduledService(final LinkResource scheduledService)
    {
        this.scheduledService = scheduledService;
    }

    public LinkResource getConditionalParent()
    {
        return this.conditionalParent;
    }

    public void setConditionalParent(final LinkResource conditionalParent)
    {
        this.conditionalParent = conditionalParent;
    }

    public LinkResource getWorkItem()
    {
        return this.workItem;
    }

    public void setWorkItem(final LinkResource workItem)
    {
        this.workItem = workItem;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        // Note: Deliberately don't include dueStatus in the hash as this data is derived from other fields.

        return "WorkItemLightDto [assetItem=" + this.getIdOrNull(this.assetItem)
                + ", survey=" + this.getIdOrNull(this.survey)
                + ", scheduledService=" + this.getIdOrNull(this.scheduledService)
                + ", conditionalParent=" + this.getIdOrNull(this.conditionalParent)
                + ", workItem=" + this.getIdOrNull(this.workItem)
                + ", getChecklist()=" + this.getIdOrNull(this.getChecklist())
                + ", getReferenceCode()=" + this.getReferenceCode()
                + ", getName()=" + this.getName()
                + ", getDescription()=" + this.getDescription()
                + ", getLongDescription()=" + this.getLongDescription()
                + ", getServiceCode()=" + this.getServiceCode()
                + ", getWorkItemType()=" + this.getIdOrNull(this.getWorkItemType())
                + ", getWorkItemAction()=" + this.getIdOrNull(this.getWorkItemAction())
                + ", getAttributeMandatory()=" + this.getAttributeMandatory()
                + ", getDueDate()=" + this.getStringRepresentationOfDate(this.getDueDate())
                + ", getDueDateManual()=" + this.getStringRepresentationOfDate(this.getDueDateManual())
                + ", getNotes()=" + this.getNotes()
                + ", getCodicil()=" + this.getIdOrNull(this.getCodicil())
                + ", getAttachmentRequired()=" + this.getAttachmentRequired()
                + ", getItemOrder()=" + this.getItemOrder()
                + ", getResolutionDate()=" + this.getStringRepresentationOfDate(this.getResolutionDate())
                + ", getResolutionStatus()=" + this.getIdOrNull(this.getResolutionStatus())
                + ", getClassSociety()=" + this.getIdOrNull(this.getClassSociety())
                + ", getAssignedTo()=" + this.getIdOrNull(this.getAssignedTo())
                + ", getResolvedBy()=" + this.getIdOrNull(this.getResolvedBy())
                + ", getTaskNumber()=" + this.getTaskNumber()
                // for the linked attributes
                + ", getAttributes()=" + this.getStringRepresentationOfList(this.getAttributes())
                // for the attribute values
                + ", getAttributes()=" + this.getStringRepresentationOfListOfStaleObjects(this.getAttributes())
                + ", getParent()=" + this.getIdOrNull(this.getParent())
                + ", getAssignedDate()=" + this.getStringRepresentationOfDate(this.getAssignedDate())
                + ", getAssignedDateManual()=" + this.getStringRepresentationOfDate(this.getAssignedDateManual())
                + ", getActionTakenDate()=" + this.getStringRepresentationOfDate(this.getActionTakenDate())
                + ", getPmsCreditDate()=" + this.getStringRepresentationOfDate(this.getPmsCreditDate())
                + ", getWorkItemScopeStatus()=" + this.getIdOrNull(this.getWorkItemScopeStatus())
                + ", getId()=" + this.getId()
                + ", getApprovedByJob()=" + this.getIdOrNull(this.getApprovedByJob())
                + ", getActive()=" + this.getActive() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
