package com.baesystems.ai.lr.dto;

public interface StaleObject
{
    String getStalenessHash();

    void setStalenessHash(String newStalenessHash);

    String getStringRepresentationForHash();

    String entityName();
}
