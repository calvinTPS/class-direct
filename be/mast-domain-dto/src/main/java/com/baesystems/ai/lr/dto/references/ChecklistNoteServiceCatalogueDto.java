package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ChecklistNoteServiceCatalogueDto extends ImmutableReferenceDataDto
{

    private static final long serialVersionUID = -8418176265295788564L;

    @NotNull
    private LinkResource checklistNote;

    @NotNull
    @Size(message = "invalid length", max = 5)
    private String serviceCode;

    public LinkResource getChecklistNote()
    {
        return checklistNote;
    }

    public void setChecklistNote(final LinkResource checklistNote)
    {
        this.checklistNote = checklistNote;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

}
