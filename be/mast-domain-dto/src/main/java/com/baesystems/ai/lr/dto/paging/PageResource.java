package com.baesystems.ai.lr.dto.paging;

import java.util.List;

public interface PageResource<T>
{
    public List<T> getContent();

    void setContent(final List<T> content);

    PaginationDto getPagination();

    void setPagination(final PaginationDto pagination);
}
