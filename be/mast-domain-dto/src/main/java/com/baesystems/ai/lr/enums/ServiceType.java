package com.baesystems.ai.lr.enums;

public enum ServiceType
{
    // From table MAST_REF_ServiceType 08-Jun2016
    ALTERNATE(1L),
    ANNUAL(2L),
    DOCKING(3L),
    INITIAL(4L),
    INTERIM(5L),
    INTERMEDIATE(6L),
    MISCELLANEOUS(7L),
    PERIODIC(8L),
    RENEWAL(9L),
    STANDALONE(10L),
    ADDITIONAL(11L);

    private final Long value;

    ServiceType(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return this.value;
    }
}
