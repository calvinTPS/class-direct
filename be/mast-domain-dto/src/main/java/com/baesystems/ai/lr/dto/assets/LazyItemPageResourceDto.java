package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class LazyItemPageResourceDto extends BasePageResource<LazyItemDto>
{
    private List<LazyItemDto> content;

    @Override
    public List<LazyItemDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<LazyItemDto> content)
    {
        this.content = content;
    }
}
