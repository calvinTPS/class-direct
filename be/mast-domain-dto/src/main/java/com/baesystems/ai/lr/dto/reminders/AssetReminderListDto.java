package com.baesystems.ai.lr.dto.reminders;

import java.io.Serializable;
import java.util.List;

public class AssetReminderListDto implements Serializable
{
    private static final long serialVersionUID = -1456593991690133295L;

    private List<ServiceReminderDto> serviceReminders;

    private List<TaskReminderDto> taskReminders;

    private List<CoCReminderDto> cocReminders;

    private List<ManualReminderDto> manualReminders;

    public List<ServiceReminderDto> getServiceReminders()
    {
        return this.serviceReminders;
    }

    public void setServiceReminders(final List<ServiceReminderDto> serviceReminders)
    {
        this.serviceReminders = serviceReminders;
    }

    public List<TaskReminderDto> getTaskReminders()
    {
        return this.taskReminders;
    }

    public void setTaskReminders(final List<TaskReminderDto> taskReminders)
    {
        this.taskReminders = taskReminders;
    }

    public List<CoCReminderDto> getCocReminders()
    {
        return this.cocReminders;
    }

    public void setCocReminders(final List<CoCReminderDto> cocReminders)
    {
        this.cocReminders = cocReminders;
    }

    public List<ManualReminderDto> getManualReminders()
    {
        return this.manualReminders;
    }

    public void setManualReminders(final List<ManualReminderDto> manualReminders)
    {
        this.manualReminders = manualReminders;
    }
}
