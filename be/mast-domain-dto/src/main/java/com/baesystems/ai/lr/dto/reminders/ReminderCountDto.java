package com.baesystems.ai.lr.dto.reminders;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.assets.AssetTileDto;

public class ReminderCountDto implements Serializable
{
    private static final long serialVersionUID = -2785420318691585631L;

    private AssetTileDto asset;

    private Long serviceReminderCount;

    private Long taskReminderCount;

    private Long cocReminderCount;

    private Long manualReminderCount;

    public AssetTileDto getAsset()
    {
        return this.asset;
    }

    public void setAsset(final AssetTileDto asset)
    {
        this.asset = asset;
    }

    public Long getServiceReminderCount()
    {
        return this.serviceReminderCount;
    }

    public void setServiceReminderCount(final Long serviceReminderCount)
    {
        this.serviceReminderCount = serviceReminderCount;
    }

    public Long getTaskReminderCount()
    {
        return this.taskReminderCount;
    }

    public void setTaskReminderCount(final Long taskReminderCount)
    {
        this.taskReminderCount = taskReminderCount;
    }

    public Long getCocReminderCount()
    {
        return this.cocReminderCount;
    }

    public void setCocReminderCount(final Long cocReminderCount)
    {
        this.cocReminderCount = cocReminderCount;
    }

    public Long getManualReminderCount()
    {
        return this.manualReminderCount;
    }

    public void setManualReminderCount(final Long manualReminderCount)
    {
        this.manualReminderCount = manualReminderCount;
    }
}
