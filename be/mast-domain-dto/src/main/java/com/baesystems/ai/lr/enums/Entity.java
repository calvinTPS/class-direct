package com.baesystems.ai.lr.enums;

public enum Entity
{
    ACTIONABLE_ITEM("Actionable Item"),
    ASSET("Asset"),
    ASSET_NOTE("Asset Note"),
    CASE("Case"),
    CASE_MILESTONE("Case Milestone"),
    CERTIFICATE("Certificate"),
    COC("Condition of Class"),
    DEFECT("Defect"),
    DEFICIENCY("Deficiency"),
    FOLLOW_UP_ACTION("Follow Up Action"),
    JOB("Job"),
    MAJOR_NON_CONFORMITY("Major Non Conformity"),
    PRODUCT("Product"),
    REPAIR("Repair"),
    REPORT("Report"),
    SERVICE("Service"),
    STATUTORY_FINDING("Statutory Finding"),
    SURVEY("Survey"),
    TASK("Task");

    private final String value;

    Entity(final String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
