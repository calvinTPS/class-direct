package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ServiceCreditStatus
{
    NOT_STARTED(1L),
    PART_HELD(2L),
    POSTPONED(3L),
    COMPLETE(4L),
    FINISHED(5L);

    private final Long value;

    ServiceCreditStatus(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }

    public static List<Long> getUncredited()
    {
        final List<Long> uncreditedIds = new ArrayList<Long>();

        uncreditedIds.add(NOT_STARTED.value());

        return uncreditedIds;
    }

    public static List<Long> getOpen()
    {
        final List<Long> openIds = new ArrayList<Long>();

        openIds.add(NOT_STARTED.value());
        openIds.add(PART_HELD.value());
        openIds.add(POSTPONED.value());

        return openIds;
    }

    public static List<Long> getOpenStarted()
    {
        final List<Long> openCreditedIds = new ArrayList<Long>();

        openCreditedIds.add(PART_HELD.value());
        openCreditedIds.add(POSTPONED.value());

        return openCreditedIds;
    }

    public static List<Long> getClosed()
    {
        final List<Long> closedIds = new ArrayList<Long>();

        closedIds.add(COMPLETE.value());
        closedIds.add(FINISHED.value());

        return closedIds;
    }

    public static List<Long> getESPStatuses()
    {
        final List<Long> espIds = new ArrayList<Long>();

        espIds.add(COMPLETE.value());
        espIds.add(PART_HELD.value());

        return espIds;
    }

    /**
     * Returns the enum that matches the passed id, or NOT_STARTED by default.
     *
     * @param id
     * @return ServiceCreditStatus
     */
    public static ServiceCreditStatus getTypeForId(final Long id)
    {
        return Arrays
                .stream(ServiceCreditStatus.values())
                .filter(type -> type.value().equals(id))
                .findFirst()
                .orElse(NOT_STARTED);
    }
}
