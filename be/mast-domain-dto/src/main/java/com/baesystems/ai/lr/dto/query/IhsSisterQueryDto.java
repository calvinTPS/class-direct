package com.baesystems.ai.lr.dto.query;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.annotations.Validate;

public class IhsSisterQueryDto implements QueryDto
{
    private static final long serialVersionUID = -947043968698780757L;

    @NotNull
    @Size(message = "invalid length", min = 1, max = 7)
    private String leadImoNumber;

    private List<String> imoNumber; // Restrict results to any in this list

    @Validate
    private AbstractQueryDto additionalFilter;

    public String getLeadImoNumber()
    {
        return leadImoNumber;
    }

    public void setLeadImoNumber(final String leadImoNumber)
    {
        this.leadImoNumber = leadImoNumber;
    }

    public List<String> getImoNumber()
    {
        return imoNumber;
    }

    public void setImoNumber(final List<String> imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public AbstractQueryDto getAdditionalFilter()
    {
        return additionalFilter;
    }

    public void setAdditionalFilter(final AbstractQueryDto additionalFilter)
    {
        this.additionalFilter = additionalFilter;
    }
}
