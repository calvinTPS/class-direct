package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.baesystems.ai.lr.enums.JobBundleEntityType;

/**
 * This annotation is used when syncing the job bundle to make sue that that links exist in the database before syncing
 * the object itself. If the links are null then the object will be synced without checking. However, if they are
 * present but the linked object does not exist then this object will not be synced and the user will see a conflict
 * saying that it has been deleted.
 *
 * This should only apply to links that refer to other job bundle related entities. If there are links to things outsid
 * the job that are not correct then the sync will fail.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LinkedEntityMustExist
{
    JobBundleEntityType type();

    boolean allowDeleted() default false;

    VersionFormula deriveVersionFrom() default @VersionFormula(dtoKey = "", targetDOField = "", targetDOVersionIdField = "");
}
