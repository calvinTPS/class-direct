package com.baesystems.ai.lr.dto.codicils;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.util.List;

public class ActionableItemPageResourceDto extends BasePageResource<ActionableItemDto>
{
    private List<ActionableItemDto> content;

    @Override
    public List<ActionableItemDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<ActionableItemDto> content)
    {
        this.content = content;
    }
}
