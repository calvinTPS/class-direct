package com.baesystems.ai.lr.dto.jobs;

import static com.baesystems.ai.lr.dto.jobs.PR17Dto.ENTITY_NAME;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.annotations.OnNotFoundException;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.baesystems.ai.lr.enums.JobBundleRepairAction;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class PR17Dto extends AuditedDto implements UpdateableJobBundleEntity
{
    public static final String ENTITY_NAME = "PR17";

    private static final long serialVersionUID = 1775376715353547152L;

    private String stalenessHash;

    @Valid
    @NotNull
    @ConflictAware
    private LinkResource job;

    @Valid
    @LinkedEntityMustExist(type = JobBundleEntityType.SURVEY)
    private SurveyDto survey;

    @ConflictAware
    private Long companyImo;

    @ConflictAware
    @Size(message = "invalid length", max = 500)
    private String companyName;

    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.IACS_SOCIETIES))
    private LinkResource iacsDocIssuer;

    @ConflictAware
    @Size(message = "invalid length", max = 50)
    private String otherDocIssuer;

    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.IACS_SOCIETIES))
    private LinkResource iacsSmcIssuer;

    @ConflictAware
    @Size(message = "invalid length", max = 50)
    private String otherSmcIssuer;

    @ConflictAware
    @NotNull
    private Long issuingSurveyor;

    @NotNull
    @Valid
    @ConflictAware(
            dynamicFieldDisplayName = @DynamicDisplayName(idFieldOnSource = "deficiencies.deficiency.id",
                    idFieldOnTarget = "deficiencies.id",
                    targetType = ReferenceDataSubSet.DEFICIENCY_GROUP),
            dynamicValueDisplayName = @DynamicDisplayName(idFieldOnSource = "deficiencies.deficiency.id",
                    nameFieldOnTarget = "displayOrder",
                    targetType = ReferenceDataSubSet.DEFICIENCY))
    @OnNotFoundException(action = JobBundleRepairAction.STRIP_IDS)
    private List<PR17DeficiencyDto> deficiencies;

    @ConflictAware
    private Boolean smcAuditorSame;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.IACS_SOCIETIES))
    private LinkResource iacsSmcAuditor;

    @ConflictAware
    @Size(message = "invalid length", max = 50)
    private String otherSmcAuditor;

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public Long getCompanyImo()
    {
        return this.companyImo;
    }

    public void setCompanyImo(final Long companyImo)
    {
        this.companyImo = companyImo;
    }

    public String getCompanyName()
    {
        return this.companyName;
    }

    public void setCompanyName(final String companyName)
    {
        this.companyName = companyName;
    }

    public LinkResource getIacsDocIssuer()
    {
        return this.iacsDocIssuer;
    }

    public void setIacsDocIssuer(final LinkResource iacsDocIssuer)
    {
        this.iacsDocIssuer = iacsDocIssuer;
    }

    public String getOtherDocIssuer()
    {
        return this.otherDocIssuer;
    }

    public void setOtherDocIssuer(final String otherDocIssuer)
    {
        this.otherDocIssuer = otherDocIssuer;
    }

    public LinkResource getIacsSmcIssuer()
    {
        return this.iacsSmcIssuer;
    }

    public void setIacsSmcIssuer(final LinkResource iacsSmcIssuer)
    {
        this.iacsSmcIssuer = iacsSmcIssuer;
    }

    public String getOtherSmcIssuer()
    {
        return this.otherSmcIssuer;
    }

    public void setOtherSmcIssuer(final String otherSmcIssuer)
    {
        this.otherSmcIssuer = otherSmcIssuer;
    }

    public Long getIssuingSurveyor()
    {
        return this.issuingSurveyor;
    }

    public void setIssuingSurveyor(final Long issuingSurveyor)
    {
        this.issuingSurveyor = issuingSurveyor;
    }

    public List<PR17DeficiencyDto> getDeficiencies()
    {
        return this.deficiencies;
    }

    public void setDeficiencies(final List<PR17DeficiencyDto> deficiencies)
    {
        this.deficiencies = deficiencies;
    }

    public SurveyDto getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }

    public Boolean getSmcAuditorSame()
    {
        return this.smcAuditorSame;
    }

    public void setSmcAuditorSame(final Boolean smcAuditorSame)
    {
        this.smcAuditorSame = smcAuditorSame;
    }

    public LinkResource getIacsSmcAuditor()
    {
        return this.iacsSmcAuditor;
    }

    public void setIacsSmcAuditor(final LinkResource iacsSmcAuditor)
    {
        this.iacsSmcAuditor = iacsSmcAuditor;
    }

    public String getOtherSmcAuditor()
    {
        return this.otherSmcAuditor;
    }

    public void setOtherSmcAuditor(final String otherSmcAuditor)
    {
        this.otherSmcAuditor = otherSmcAuditor;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "PR17Dto [job=" + this.getIdOrNull(this.job)
                + ", companyImo=" + this.companyImo
                + ", companyName=" + this.companyName
                + ", iacsDocIssuer=" + this.getIdOrNull(this.iacsDocIssuer)
                + ", otherDocIssuer=" + this.otherDocIssuer
                + ", iacsSmcIssuer=" + this.getIdOrNull(this.iacsSmcIssuer)
                + ", otherSmcIssuer=" + this.otherSmcIssuer
                + ", issuingSurveyor=" + this.issuingSurveyor
                // for linked deficiencies
                + ", deficiencies=" + this.getStringRepresentationOfList(this.deficiencies)
                // for the content of the deficiencies
                + ", deficiencies=" + this.getStringRepresentationOfListOfStaleObjects(this.deficiencies)
                + ", smcAuditorSame=" + this.smcAuditorSame
                + ", iacsSmcAuditor=" + this.getIdOrNull(this.iacsSmcAuditor)
                + ", otherSmcAuditor=" + this.otherSmcAuditor + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
