package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import com.baesystems.ai.lr.dto.annotations.MutuallyExclusive;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.validation.DraftItemRelationshipDto;

@MutuallyExclusive.List(
        {@MutuallyExclusive(
                fields = {"fromId", "itemTypeId"},
                allowAllNull = false,
                message = "Fields fromId and itemTypeId are mutually exclusive")})
public class DraftItemDto extends BaseDto
{
    private static final long serialVersionUID = 5606776255495907825L;

    private String name;
    // The parent ID where the item is being copied to
    private Long toParentId;
    private Long toAssetId;

    // The ID of the item which is being copied from, can be omitted if itemTypeId is being used
    private Long fromId;
    private Long fromAssetId;
    private Long itemTypeId;
    private Long displayOrder;

    private List<DraftItemRelationshipDto> itemRelationshipDto;

    private String uniqueItemId;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Long getToParentId()
    {
        return toParentId;
    }

    public void setToParentId(final Long toParentId)
    {
        this.toParentId = toParentId;
    }

    public Long getFromId()
    {
        return fromId;
    }

    public void setFromId(final Long fromId)
    {
        this.fromId = fromId;
    }

    public Long getItemTypeId()
    {
        return itemTypeId;
    }

    public void setItemTypeId(final Long itemTypeId)
    {
        this.itemTypeId = itemTypeId;
    }

    public List<DraftItemRelationshipDto> getItemRelationshipDto()
    {
        return itemRelationshipDto;
    }

    public void setItemRelationshipDto(final List<DraftItemRelationshipDto> itemRelationshipDto)
    {
        this.itemRelationshipDto = itemRelationshipDto;
    }

    public Long getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Long getFromAssetId()
    {
        return fromAssetId;
    }

    public void setFromAssetId(final Long fromAssetId)
    {
        this.fromAssetId = fromAssetId;
    }

    public Long getToAssetId()
    {
        return toAssetId;
    }

    public void setToAssetId(final Long toAssetId)
    {
        this.toAssetId = toAssetId;
    }

    public String getUniqueItemId()
    {
        return uniqueItemId;
    }

    public void setUniqueItemId(final String uniqueItemId)
    {
        this.uniqueItemId = uniqueItemId;
    }
}
