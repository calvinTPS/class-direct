package com.baesystems.ai.lr.dto.base;

import java.util.Date;

import com.baesystems.ai.lr.dto.LinkResource;

public interface DueStatusUpdateableDto
{
    Date getDueDate();

    void setDueStatus(LinkResource dueStataus);
}
