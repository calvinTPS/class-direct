package com.baesystems.ai.lr.enums;

public enum AttributeCopyRule
{
    MUST(1L),
    MUST_NOT(2L),
    OPTIONAL(3L);

    private final Long value;

    AttributeCopyRule(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
