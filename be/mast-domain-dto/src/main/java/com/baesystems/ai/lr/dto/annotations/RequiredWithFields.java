package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = RequiredWithFieldsValidator.class)
@Documented
public @interface RequiredWithFields
{
    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The list of field names that are mandatory or exclusive with the key field
     */
    String[] fields();

    /**
     * @return The key field
     */
    String keyField();

    /**
     * @return Whether this list of field should or should not be null
     */
    boolean shouldBePopulated();

    /**
     */
    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List
    {
        RequiredWithFields[] value();
    }
}
