package com.baesystems.ai.lr.dto.query;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.baesystems.ai.lr.dto.annotations.FieldLessThanOrEqual;
import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;
import com.baesystems.ai.lr.enums.Operator;

@FieldLessThanOrEqual.List({
                            @FieldLessThanOrEqual(first = "grossTonnageMin", second = "grossTonnageMax", useTime = false, message = "Minimum gross tonnage exceeds maximum"),
                            @FieldLessThanOrEqual(first = "buildDateMin", second = "buildDateMax", useTime = false, message = "Earliest build date cannot be after latest build date"),
                            @FieldLessThanOrEqual(first = "deadWeightMin", second = "deadWeightMax", useTime = false, message = "Minimum dead weight exceeds maximum"),
                            @FieldLessThanOrEqual(first = "effectiveDateMin", second = "effectiveDateMax", useTime = false, message = "Earliest effective date cannot be after latest effective date")
})
public class AssetQueryDto implements QueryDto
{
    private static final long serialVersionUID = -8537946058324922688L;

    @UsesLikeComparator
    private String search;

    private List<Long> lifecycleStatusId;

    private List<String> imoNumber;

    private List<Long> assetTypeId;

    @UsesLikeComparator
    private String yardNumber;

    private Date buildDateMin;

    private Date buildDateMax;

    private Date effectiveDateMin;

    private Date effectiveDateMax;

    private List<Long> classStatusId;

    private List<Long> classDepartmentId;

    private List<Long> linkedAssetId;

    private List<Long> flagStateId;

    @Max(value = 9999999)
    @Min(value = 0)
    private Double grossTonnageMin;

    @Max(value = 9999999)
    @Min(value = 0)
    private Double grossTonnageMax;

    @Max(value = 9999999)
    @Min(value = 0)
    private Double deadWeightMin;

    @Max(value = 9999999)
    @Min(value = 0)
    private Double deadWeightMax;

    private List<Long> partyId;

    private List<Long> partyRoleId;

    private String builder;

    private List<Long> categoryId;

    private List<Long> idList;

    private Long itemTypeId;

    private Long attributeTypeId;

    private Operator operator;

    private String attributeValue;

    private Long officeId;

    @UsesLikeComparator
    private String leadImoNumber;

    public String getSearch()
    {
        return search;
    }

    public void setSearch(final String search)
    {
        this.search = search;
    }

    public List<Long> getLifecycleStatusId()
    {
        return lifecycleStatusId;
    }

    public void setLifecycleStatusId(final List<Long> lifecycleStatusId)
    {
        this.lifecycleStatusId = lifecycleStatusId;
    }

    public List<String> getImoNumber()
    {
        return imoNumber;
    }

    public void setImoNumber(final List<String> imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public List<Long> getAssetTypeId()
    {
        return assetTypeId;
    }

    public void setAssetTypeId(final List<Long> assetTypeId)
    {
        this.assetTypeId = assetTypeId;
    }

    public String getYardNumber()
    {
        return yardNumber;
    }

    public void setYardNumber(final String yardNumber)
    {
        this.yardNumber = yardNumber;
    }

    public Date getBuildDateMin()
    {
        return buildDateMin;
    }

    public void setBuildDateMin(final Date buildDateMin)
    {
        this.buildDateMin = buildDateMin;
    }

    public Date getBuildDateMax()
    {
        return buildDateMax;
    }

    public void setBuildDateMax(final Date buildDateMax)
    {
        this.buildDateMax = buildDateMax;
    }

    public Date getEffectiveDateMin()
    {
        return effectiveDateMin;
    }

    public void setEffectiveDateMin(final Date effectiveDateMin)
    {
        this.effectiveDateMin = effectiveDateMin;
    }

    public Date getEffectiveDateMax()
    {
        return effectiveDateMax;
    }

    public void setEffectiveDateMax(final Date effectiveDateMax)
    {
        this.effectiveDateMax = effectiveDateMax;
    }

    public List<Long> getClassStatusId()
    {
        return classStatusId;
    }

    public void setClassStatusId(final List<Long> classStatusId)
    {
        this.classStatusId = classStatusId;
    }

    public List<Long> getClassDepartmentId()
    {
        return classDepartmentId;
    }

    public void setClassDepartmentId(final List<Long> classDepartmentId)
    {
        this.classDepartmentId = classDepartmentId;
    }

    public List<Long> getLinkedAssetId()
    {
        return linkedAssetId;
    }

    public void setLinkedAssetId(final List<Long> linkedAssetId)
    {
        this.linkedAssetId = linkedAssetId;
    }

    public List<Long> getFlagStateId()
    {
        return flagStateId;
    }

    public void setFlagStateId(final List<Long> flagStateId)
    {
        this.flagStateId = flagStateId;
    }

    public Double getGrossTonnageMin()
    {
        return grossTonnageMin;
    }

    public void setGrossTonnageMin(final Double grossTonnageMin)
    {
        this.grossTonnageMin = grossTonnageMin;
    }

    public Double getGrossTonnageMax()
    {
        return grossTonnageMax;
    }

    public void setGrossTonnageMax(final Double grossTonnageMax)
    {
        this.grossTonnageMax = grossTonnageMax;
    }

    public Double getDeadWeightMin()
    {
        return deadWeightMin;
    }

    public void setDeadWeightMin(final Double deadWeightMin)
    {
        this.deadWeightMin = deadWeightMin;
    }

    public Double getDeadWeightMax()
    {
        return deadWeightMax;
    }

    public void setDeadWeightMax(final Double deadWeightMax)
    {
        this.deadWeightMax = deadWeightMax;
    }

    public List<Long> getPartyId()
    {
        return partyId;
    }

    public void setPartyId(final List<Long> partyId)
    {
        this.partyId = partyId;
    }

    public List<Long> getPartyRoleId()
    {
        return partyRoleId;
    }

    public void setPartyRoleId(final List<Long> partyRoleId)
    {
        this.partyRoleId = partyRoleId;
    }

    public String getBuilder()
    {
        return builder;
    }

    public void setBuilder(final String builder)
    {
        this.builder = builder;
    }

    public List<Long> getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(final List<Long> categoryId)
    {
        this.categoryId = categoryId;
    }

    public List<Long> getIdList()
    {
        return idList;
    }

    public void setIdList(final List<Long> idList)
    {
        this.idList = idList;
    }

    public Long getItemTypeId()
    {
        return itemTypeId;
    }

    public void setItemTypeId(final Long itemTypeId)
    {
        this.itemTypeId = itemTypeId;
    }

    public Long getAttributeTypeId()
    {
        return attributeTypeId;
    }

    public void setAttributeTypeId(final Long attributeTypeId)
    {
        this.attributeTypeId = attributeTypeId;
    }

    public Operator getOperator()
    {
        return operator;
    }

    public void setOperator(final Operator operator)
    {
        this.operator = operator;
    }

    public String getAttributeValue()
    {
        return attributeValue;
    }

    public void setAttributeValue(final String attributeValue)
    {
        this.attributeValue = attributeValue;
    }

    public Long getOfficeId()
    {
        return officeId;
    }

    public void setOfficeId(final Long officeId)
    {
        this.officeId = officeId;
    }

    public String getLeadImoNumber()
    {
        return leadImoNumber;
    }

    public void setLeadImoNumber(final String leadImoNumber)
    {
        this.leadImoNumber = leadImoNumber;
    }
}
