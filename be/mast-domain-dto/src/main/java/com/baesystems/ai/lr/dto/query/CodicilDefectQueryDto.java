package com.baesystems.ai.lr.dto.query;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

/**
 * Common dto class used for querying Codicil type entities (Asset Notes, Actionable Items, CoCs) and also Defects.
 */
public class CodicilDefectQueryDto implements QueryDto
{
    private static final long serialVersionUID = 7457519316161753246L;

    @UsesLikeComparator
    private String searchString;

    private List<Long> categoryList;

    private List<Long> statusList;

    private List<Long> itemTypeList;

    private List<Long> confidentialityTypeList;

    private Date dueDateMin;

    private Date dueDateMax;

    private Boolean itemName;

    public String getSearchString()
    {
        return this.searchString;
    }

    public void setSearchString(final String searchString)
    {
        this.searchString = searchString;
    }

    public List<Long> getCategoryList()
    {
        return this.categoryList;
    }

    public void setCategoryList(final List<Long> categoryList)
    {
        this.categoryList = categoryList;
    }

    public List<Long> getStatusList()
    {
        return this.statusList;
    }

    public void setStatusList(final List<Long> statusList)
    {
        this.statusList = statusList;
    }

    public List<Long> getItemTypeList()
    {
        return this.itemTypeList;
    }

    public void setItemTypeList(final List<Long> itemTypeList)
    {
        this.itemTypeList = itemTypeList;
    }

    public List<Long> getConfidentialityTypeList()
    {
        return this.confidentialityTypeList;
    }

    public void setConfidentialityTypeList(final List<Long> confidentialityTypeList)
    {
        this.confidentialityTypeList = confidentialityTypeList;
    }

    public Date getDueDateMin()
    {
        return this.dueDateMin;
    }

    public void setDueDateMin(final Date dueDateMin)
    {
        this.dueDateMin = dueDateMin;
    }

    public Date getDueDateMax()
    {
        return this.dueDateMax;
    }

    public void setDueDateMax(final Date dueDateMax)
    {
        this.dueDateMax = dueDateMax;
    }

    public Boolean getItemName()
    {
        return this.itemName;
    }

    public void setItemName(final Boolean itemName)
    {
        this.itemName = itemName;
    }

    @Override
    public boolean guaranteesNoResults()
    {
        return collectionIsEmpty(this.categoryList)
                || collectionIsEmpty(this.statusList)
                || collectionIsEmpty(this.itemTypeList)
                || collectionIsEmpty(this.confidentialityTypeList);
    }

    private static boolean collectionIsEmpty(final Collection<?> collection)
    {
        return collection != null && collection.isEmpty();
    }
}
