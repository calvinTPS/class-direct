package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public enum AttachmentTargetType
{
    CASE("aCase.id", null, null, null, null, "/case/{caseId}/attachment"),
    MILESTONE("milestone.id", null, null, null, null, "/case/{caseId}/milestone/{milestoneId}/attachment"),

    ASSET("asset.id", null, null, null, null, "/asset/{assetId}/attachment"),
    ITEM("item.id", null, null, null, null, "/asset/{assetId}/item/{itemId}/attachment"),
    REMINDER("reminder.id", null, null, null, null, "/asset/{assetId}/reminder/{reminderId}/attachment"),

    SERVICE("service.id", null, null, null, null, "/asset/{assetId}/service/{serviceId}/attachment"),
    SURVEY("survey.id", null, null, null, "survey.serviceCatalogue.code", "/job/{jobId}/survey/{surveyId}/attachment"),
    TASK("workItem.id", null, null, null, null, "/asset/{assetId}/task/{taskId}/attachment"),
    WIP_TASK("wipWorkItem.id", null, null, null, "wipWorkItem.referenceCode", "/job/{jobId}/wip-task/{taskId}/attachment"),

    ASSET_NOTE("assetNote.id", null, null, null, null, "/asset/{assetId}/asset-note/{assetNoteId}/attachment"),
    WIP_ASSET_NOTE("wipAssetNote.id", null, null, null, "wipAssetNote.referenceCode", "/job/{jobId}/wip-asset-note/{assetNoteId}/attachment"),
    COC("coC.id", null, null, null, null, "/asset/{assetId}/coc/{cocId}/attachment"),
    WIP_COC("wipCoC.id", null, null, null, "wipCoC.referenceCode", "/job/{jobId}/wip-coc/{cocId}/attachment"),
    ACTIONABLE_ITEM("actionableItem.id", null, null, null, null, "/asset/{assetId}/actionable-item/{actionableItemId}/attachment"),
    WIP_ACTIONABLE_ITEM("wipActionableItem.id", null, null, null, "wipActionableItem.referenceCode",
            "/job/{jobId}/wip-actionable-item/{actionableItemId}/attachment"),

    // This exists because we need to be able to manage/process 'O' type MNCNs when we are handling attachments,
    // even though there's no direct URLs for them.
    OLD_MNCN("mncnId", "mncnVersionId", "mncn.versionType", VersionType.O, null, (String) null),
    MNCN("mncnId", "mncnVersionId", "mncn.versionType", VersionType.P, null, "/asset/{assetId}/mncn/{mncnId}/attachment"),
    WIP_MNCN("mncnId", "mncnVersionId", "mncn.versionType", VersionType.J, "mncnId", "/job/{jobId}/wip-mncn/{mncnId}/attachment"),

    STATUTORY_FINDING("statutoryFinding.id", null, null, null, null,
            "/asset/{assetId}/deficiency/{deficiencyId}/statutory-finding/{statutoryFindingId}/attachment"),
    WIP_STATUTORY_FINDING("wipStatutoryFinding.id", null, null, null, "wipStatutoryFinding.referenceCode",
            "/job/{jobId}/wip-deficiency/{deficiencyId}/wip-statutory-finding/{statutoryFindingId}/attachment"),

    DEFECT("defect.id", null, null, null, null, "/asset/{assetId}/defect/{defectId}/attachment"),
    WIP_DEFECT("wipDefect.id", null, null, null, "wipDefect.id", "/job/{jobId}/wip-defect/{defectId}/attachment"),
    REPAIR("repair.id", null, null, null, null, "/asset/{assetId}/defect/{defectId}/repair/{repairId}/attachment",
            "/asset/{assetId}/coc/{cocId}/repair/{repairId}/attachment"),
    WIP_REPAIR("wipRepair.id", null, null, null, "wipRepair.id", "/job/{jobId}/wip-defect/{defectId}/wip-repair/{repairId}/attachment",
            "/job/{jobId}/wip-coc/{cocId}/wip-repair/{repairId}/attachment"),
    DEFICIENCY("deficiency.id", null, null, null, null, "/asset/{assetId}/deficiency/{deficiencyId}/attachment"),
    WIP_DEFICIENCY("wipDeficiency.id", null, null, null, "wipDeficiency.id", "/job/{jobId}/wip-deficiency/{deficiencyId}/attachment"),
    RECTIFICATION("rectification.id", null, null, null, null,
            "/asset/{assetId}/deficiency/{deficiencyId}/rectification/{rectificationId}/attachment"),
    WIP_RECTIFICATION("wipRectification.id", null, null, null, "wipRectification.id",
            "/job/{jobId}/wip-deficiency/{deficiencyId}/wip-rectification/{rectificationId}/attachment"),

    // This exists because we need to be able to manage/process 'O' type certificates when we are handling attachments,
    // even though there's no direct URLs for them.
    OLD_CERTIFICATE("certificateId", "certificateVersionId", "certificate.versionType", VersionType.O, null, (String) null),
    CERTIFICATE("certificateId", "certificateVersionId", "certificate.versionType", VersionType.P, null,
            "/asset/{assetId}/certificate/{certificateId}/attachment"),
    WIP_CERTIFICATE("certificateId", "certificateVersionId", "certificate.versionType", VersionType.J, "certificateId",
            "/job/{jobId}/wip-certificate/{certificateId}/attachment"),

    JOB("job.id", null, null, null, null, "/job/{jobId}/attachment"),
    PR_SEVENTEEN("pr17.id", null, null, null, "pr17.id", "/job/{jobId}/pr-seventeen/{prSeventeenId}/attachment"),

    REPORT("report.id", null, null, null, null, "/job/{jobId}/report/{reportId}/attachment"),

    FLAG("flag.id", null, null, null, null, "/reference-data/flag/{flagId}/attachment");

    private final String idFieldName;

    private final String versionIdFieldName;

    private final String discriminatorColumn;

    private final Object discriminatorValue;

    private final String jobRelatedIdentifierColumn;

    private final List<String> camelServletContextPaths;

    private static final Map<String, AttachmentTargetType> SERVLET_CONTEXT_PATH_MAP = setServletContextPath();
    private static final List<AttachmentTargetType> TYPES_THAT_MUST_CHECK_ALL_FIELDS = Arrays.asList(JOB);
    private static final List<AttachmentTargetType> JOB_RELATED_ATTACHMENT_TYPES = createJobRelatedAttachmentTypeList();

    static
    {
        Arrays.stream(AttachmentTargetType.values())
                .filter(type -> type.isJobRelated())
                .collect(Collectors.toList());
    }

    private static final Map<String, AttachmentTargetType> FILED_NAME_TYPE_MAP = setTypeForFieldName();

    AttachmentTargetType(final String idFieldName, final String versionIdFieldName, final String discriminatorColumn, final Object discriminatorValue,
            final String jobRelatedIdentifierColumn, final String... camelServletContextPaths)
    {
        this.camelServletContextPaths = new ArrayList<>();

        for (final String camelServletContextPath : camelServletContextPaths)
        { // there may be more than one url per type (repairs for example)
            this.camelServletContextPaths.add(camelServletContextPath);
        }

        this.discriminatorColumn = discriminatorColumn;
        this.discriminatorValue = discriminatorValue;
        this.idFieldName = idFieldName;
        this.versionIdFieldName = versionIdFieldName;
        this.jobRelatedIdentifierColumn = jobRelatedIdentifierColumn;
    }

    public List<String> getCamelServletContextPaths()
    {
        return camelServletContextPaths;
    }

    public String getIdFieldName()
    {
        return this.idFieldName;
    }

    public String getVersionIdFieldName()
    {
        return this.versionIdFieldName;
    }

    public String getDiscriminatorColumn()
    {
        return discriminatorColumn;
    }

    public Object getDiscriminatorValue()
    {
        return discriminatorValue;
    }

    public String getJobRelatedIdentifierColumn()
    {
        return jobRelatedIdentifierColumn;
    }

    public boolean isVersioned()
    {
        return this.versionIdFieldName != null;
    }

    /**
     * if this is not in the list of types what require the mapper to scan all of the fields before deciding on the type
     * then return true as the loop can stop.
     *
     * Currently the only type where this happens is JOB as other entities can have a job id so that all attachments
     * associated with a job related entity can be loaded easily.
     *
     * Using the example of JOB, because all of the attachments on job related entities (such as wip codicils) have the
     * job id saved in the job_id column finding a non-null value in this column does not guarantee that the attachment
     * is attached to the job as it may be attached to a job related entity such as a codicil. So returning false her
     * tells the mapper that it must keep checking fields until it finds another non-null (if the attachment is related
     * to another job related entity) or it has scanned all of the relevant fields if the attachment is related to the
     * job. In all other cases the first non-null guarantees the result, because the given type will be the only type
     * that is expected to have a value in that field.
     */
    public boolean firstMatchGuaranteesType()
    {
        return !TYPES_THAT_MUST_CHECK_ALL_FIELDS.contains(this);
    }

    /**
     * Whether or not the attachment the entity is linked to is related to the job or not so that we know whether to
     * save the job id when creating the attachment.
     *
     * This is determined by whether or not a column is specified for the identifier to identify the attachment in the
     * list of job related attachments.
     *
     * Job itself is not included in this list because it already has a job id.
     */
    public boolean isJobRelated()
    {
        return jobRelatedIdentifierColumn != null;
    }

    public static AttachmentTargetType getTypeFromPath(final String path)
    {
        return SERVLET_CONTEXT_PATH_MAP.get(path);
    }

    public static List<AttachmentTargetType> getJobRelatedAttachmentTypes()
    {
        return JOB_RELATED_ATTACHMENT_TYPES;
    }

    public static AttachmentTargetType getTypeByLinkedFieldName(final String fieldName)
    {
        return FILED_NAME_TYPE_MAP.get(fieldName);
    }

    /**
     * Get the field name in the DO that links to the entity for this attachment type. If the entity is versioned then
     * we need to use the discriminator column as this is on the object while the id is just a long in the attachmentDO.
     */
    public String getFieldName()
    {
        return (this.getDiscriminatorColumn() == null ? this.getIdFieldName() : this.getDiscriminatorColumn()).replaceAll("\\..*", "");
    }

    /**
     * use the urls as key in the map so that the type can be found from the url quickly. This is a many to one
     * relationship.
     */
    private static Map<String, AttachmentTargetType> setServletContextPath()
    {
        final Map<String, AttachmentTargetType> servletContextPathMap = new HashMap<String, AttachmentTargetType>();

        for (final AttachmentTargetType type : AttachmentTargetType.values())
        { // go though all the types
            for (final String servletContextPath : type.getCamelServletContextPaths())
            { // add keys for all the urls that point to that type
                servletContextPathMap.put(servletContextPath, type);
            }
        }

        return servletContextPathMap;
    }

    /**
     * Loop over all of the attachment types to create a map of the field name corresponding to the relevant linked
     * entity to the attachment type. For the sake of getting a one-to-one mapping versioned things will use the
     * published type. Methods that needed to know the exact type would need to provide more information than just the
     * field name so couldn't use this map anyway.
     */
    private static Map<String, AttachmentTargetType> setTypeForFieldName()
    {
        final Map<String, AttachmentTargetType> fieldNameTypeMap = new HashMap<String, AttachmentTargetType>();

        for (final AttachmentTargetType type : AttachmentTargetType.values())
        { // go though all the types and add keys for the DO field names (of the linked objects) that point to that type
            if (!type.isVersioned() || type.getDiscriminatorValue().equals(VersionType.P))
            { // for the sake of getting a one-to-one mapping versioned things will use the published type
                fieldNameTypeMap.put(type.getFieldName(), type);
            }
        }

        return fieldNameTypeMap;
    }

    /**
     * Return a list of types that are related to the job.
     */
    private static List<AttachmentTargetType> createJobRelatedAttachmentTypeList()
    {
        return Arrays.stream(AttachmentTargetType.values())
                .filter(type -> type.isJobRelated())
                .collect(Collectors.toList());
    }
}
