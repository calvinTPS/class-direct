package com.baesystems.ai.lr.dto.tasks;

import java.io.Serializable;

public class TSRETaskIdRequestDto implements Serializable
{
    private static final long serialVersionUID = -4028796598172113329L;

    private String taskIds;

    public String getTaskIds()
    {
        return taskIds;
    }

    public void setTaskIds(final String taskIds)
    {
        this.taskIds = taskIds;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("{\"taskIds\":\"");
        builder.append(getTaskIds());
        builder.append("\"}");
        return builder.toString();
    }
}
