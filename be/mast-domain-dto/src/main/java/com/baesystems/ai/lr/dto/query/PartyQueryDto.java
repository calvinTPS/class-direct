
package com.baesystems.ai.lr.dto.query;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

public class PartyQueryDto implements QueryDto
{
    private static final long serialVersionUID = 4442160639869640333L;

    @UsesLikeComparator
    private String imoNumber;

    @UsesLikeComparator
    private String name;

    private Boolean shipBuilder;

    public String getImoNumber()
    {
        return imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Boolean getShipBuilder()
    {
        return shipBuilder;
    }

    public void setShipBuilder(final Boolean shipBuilder)
    {
        this.shipBuilder = shipBuilder;
    }
}
