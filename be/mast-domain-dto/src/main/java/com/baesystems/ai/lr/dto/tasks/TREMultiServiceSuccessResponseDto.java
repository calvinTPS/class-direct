package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

public class TREMultiServiceSuccessResponseDto extends TREBaseResponseDto
{
    private static final long serialVersionUID = -4956015627349153429L;

    private List<TREServiceDto> services;

    public List<TREServiceDto> getServices()
    {
        return services;
    }

    public void setServices(final List<TREServiceDto> services)
    {
        this.services = services;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TREMultiServiceSuccessResponseDto [getServices()=");
        builder.append(getServices());
        builder.append(", getStatus()=");
        builder.append(getStatus());
        builder.append(", getMessage()=");
        builder.append(getMessage());
        builder.append("]");
        return builder.toString();
    }
}
