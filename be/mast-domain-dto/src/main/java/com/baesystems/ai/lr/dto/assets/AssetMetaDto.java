package com.baesystems.ai.lr.dto.assets;

import com.baesystems.ai.lr.dto.base.BaseDto;

import java.util.Date;

public class AssetMetaDto extends BaseDto
{
    private static final long serialVersionUID = -43386412187057888L;

    private Long publishedVersionId;
    private Long draftVersionId;
    private String checkedOutBy;
    private String name;
    private String source;
    private String imoNumber;
    private Date updatedDate;

    public Long getPublishedVersionId()
    {
        return publishedVersionId;
    }

    public void setPublishedVersionId(final Long publishedVersionId)
    {
        this.publishedVersionId = publishedVersionId;
    }

    public Long getDraftVersionId()
    {
        return draftVersionId;
    }

    public void setDraftVersionId(final Long draftVersionId)
    {
        this.draftVersionId = draftVersionId;
    }

    public String getCheckedOutBy()
    {
        return checkedOutBy;
    }

    public void setCheckedOutBy(final String checkedOutBy)
    {
        this.checkedOutBy = checkedOutBy;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(final String source)
    {
        this.source = source;
    }

    public String getImoNumber()
    {
        return imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(final Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }
}
