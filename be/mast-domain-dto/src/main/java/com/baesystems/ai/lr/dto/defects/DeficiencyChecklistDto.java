package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.CodeLinkResource;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@IgnoreIdsInConflicts
public class DeficiencyChecklistDto extends BaseDto
{
    private static final long serialVersionUID = 5986185215336767281L;

    @Valid
    private CodeLinkResource workItem;

    @Valid
    private CodeLinkResource wipWorkItem;

    private Long checklistId;

    @Valid
    private LinkResource deficiency;

    @Valid
    private NamedLinkResource parent;

    // This is effectively the 'child', when this DTO is used in non-wip form
    @JsonIgnore
    private LinkResource wipDeficiencyChecklist;

    public CodeLinkResource getWorkItem()
    {
        return this.workItem;
    }

    public void setWorkItem(final CodeLinkResource workItem)
    {
        this.workItem = workItem;
    }

    public CodeLinkResource getWipWorkItem()
    {
        return this.wipWorkItem;
    }

    public void setWipWorkItem(final CodeLinkResource wipWorkItem)
    {
        this.wipWorkItem = wipWorkItem;
    }

    public LinkResource getDeficiency()
    {
        return this.deficiency;
    }

    public void setDeficiency(final LinkResource deficiency)
    {
        this.deficiency = deficiency;
    }

    public NamedLinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final NamedLinkResource parent)
    {
        this.parent = parent;
    }

    public LinkResource getWipDeficiencyChecklist()
    {
        return this.wipDeficiencyChecklist;
    }

    public void setWipDeficiencyChecklist(final LinkResource wipDeficiencyChecklist)
    {
        this.wipDeficiencyChecklist = wipDeficiencyChecklist;
    }

    public Long getChecklistId()
    {
        return this.checklistId;
    }

    public void setChecklistId(final Long checklistId)
    {
        this.checklistId = checklistId;
    }
}
