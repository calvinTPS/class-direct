package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.BaseDto;

import java.util.Date;

public class IhsNconDto extends BaseDto
{
    private static final long serialVersionUID = -4834240234234L;

    private Date nconDate;

    public Date getNconDate()
    {
        return nconDate;
    }

    public void setNconDate(final Date nconDate)
    {
        this.nconDate = nconDate;
    }
}
