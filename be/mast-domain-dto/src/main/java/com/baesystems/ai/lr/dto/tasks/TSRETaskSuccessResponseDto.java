package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

public class TSRETaskSuccessResponseDto extends TSREBaseResponseDto
{
    private static final long serialVersionUID = -2484649545408835348L;

    private List<TSRETaskDto> tasks;

    public List<TSRETaskDto> getTasks()
    {
        return tasks;
    }

    public void setTasks(final List<TSRETaskDto> tasks)
    {
        this.tasks = tasks;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TSRETaskSuccessResponseDto [getTasks()=");
        builder.append(getTasks());
        builder.append(", getStatus()=");
        builder.append(getStatus());
        builder.append(", getMessage()=");
        builder.append(getMessage());
        builder.append("]");
        return builder.toString();
    }
}
