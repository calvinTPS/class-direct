package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;

public class CertificateTemplateDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 5202643030091407981L;

    private String variant;

    @Size(message = "invalid length", max = 30)
    private String formNumber;

    private Double version;

    private Integer displayOrder;

    private LinkResource productCatalogue;

    private LinkResource serviceRuleset;

    private Boolean multiple;

    private Boolean expireable;

    private Boolean endorseable;

    public String getVariant()
    {
        return variant;
    }

    public void setVariant(final String variant)
    {
        this.variant = variant;
    }

    public String getFormNumber()
    {
        return formNumber;
    }

    public void setFormNumber(final String formNumber)
    {
        this.formNumber = formNumber;
    }

    public Double getVersion()
    {
        return version;
    }

    public void setVersion(final Double version)
    {
        this.version = version;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public LinkResource getProductCatalogue()
    {
        return productCatalogue;
    }

    public void setProductCatalogue(final LinkResource productCatalogue)
    {
        this.productCatalogue = productCatalogue;
    }

    public LinkResource getServiceRuleset()
    {
        return serviceRuleset;
    }

    public void setServiceRuleset(final LinkResource serviceRuleset)
    {
        this.serviceRuleset = serviceRuleset;
    }

    public Boolean getMultiple()
    {
        return multiple;
    }

    public void setMultiple(final Boolean multiple)
    {
        this.multiple = multiple;
    }

    public Boolean getExpireable()
    {
        return expireable;
    }

    public void setExpireable(final Boolean expireable)
    {
        this.expireable = expireable;
    }

    public Boolean getEndorseable()
    {
        return endorseable;
    }

    public void setEndorseable(final Boolean endorseable)
    {
        this.endorseable = endorseable;
    }
}
