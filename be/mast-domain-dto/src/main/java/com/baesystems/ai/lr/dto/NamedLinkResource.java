package com.baesystems.ai.lr.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NamedLinkResource extends LinkResource
{
    private static final long serialVersionUID = 1914884139860649971L;

    public NamedLinkResource()
    {
        super();
    }

    public NamedLinkResource(final Long id)
    {
        this();
        this.setId(id);
    }

    // TODO Every time MastDTO Update we should check on this from CD side.
    // This @JsonIgnore completely exclude's name from process of de-serialization.So name is showing
    // as null ,even though there is a valid name in the DB {@link #LRCD-2765}
    // @JsonIgnore
    private String name;

    @JsonProperty
    public String getName()
    {
        return name;
    }

    @JsonIgnore
    public void setName(final String name)
    {
        this.name = name;
    }
}
