package com.baesystems.ai.lr.dto.attachments.external;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ExternalDto;

public class SupplementaryInformationDto extends ExternalDto
{
    private static final long serialVersionUID = 7488872460395590127L;

    @NotNull
    @Size(message = "invalid length", max = 255)
    private String title;

    @NotNull
    @Size(message = "invalid length", max = 500)
    private String attachmentUrl;

    @NotNull
    @Valid
    private LinkResource attachmentType;

    @NotNull
    @Valid
    private LinkResource attachmentCategory;

    @NotNull
    private Long documentVersion;

    private String updatedDateWithTime;

    private String creationDateWithTime;

    private String attachmentDateWithTime;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getAttachmentUrl()
    {
        return attachmentUrl;
    }

    public void setAttachmentUrl(final String attachmentUrl)
    {
        this.attachmentUrl = attachmentUrl;
    }

    public LinkResource getAttachmentType()
    {
        return attachmentType;
    }

    public void setAttachmentType(final LinkResource attachmentType)
    {
        this.attachmentType = attachmentType;
    }

    public LinkResource getAttachmentCategory()
    {
        return attachmentCategory;
    }

    public void setAttachmentCategory(final LinkResource attachmentCategory)
    {
        this.attachmentCategory = attachmentCategory;
    }

    public Long getDocumentVersion()
    {
        return documentVersion;
    }

    public void setDocumentVersion(final Long documentVersion)
    {
        this.documentVersion = documentVersion;
    }

    public String getUpdatedDateWithTime()
    {
        return updatedDateWithTime;
    }

    public void setUpdatedDateWithTime(final String updatedDateWithTime)
    {
        this.updatedDateWithTime = updatedDateWithTime;
    }

    public String getCreationDateWithTime()
    {
        return creationDateWithTime;
    }

    public void setCreationDateWithTime(final String creationDateWithTime)
    {
        this.creationDateWithTime = creationDateWithTime;
    }

    public String getAttachmentDateWithTime()
    {
        return attachmentDateWithTime;
    }

    public void setAttachmentDateWithTime(final String attachmentDateWithTime)
    {
        this.attachmentDateWithTime = attachmentDateWithTime;
    }
}
