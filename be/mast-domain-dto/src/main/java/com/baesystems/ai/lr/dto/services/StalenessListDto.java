package com.baesystems.ai.lr.dto.services;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class StalenessListDto
{
    public static final String MULTIPLE_STALE_OBJECTS = "The following entities cannot be updated as there are newer versions someone else has saved. \"%s\" "
            + "They all have the following entity type: \"%s\"";

    private Boolean staleElements = Boolean.FALSE;
    private String stalenessMessage;

    @JsonProperty
    public Boolean getStaleElements()
    {
        return this.staleElements;
    }

    @JsonProperty
    public String getStalenessMessage()
    {
        return this.stalenessMessage;
    }

    @JsonIgnore
    public void setStalenessState(final List<String> entityNames)
    {
        this.staleElements = entityNames != null && !entityNames.isEmpty();
        if (this.staleElements)
        {
            this.stalenessMessage = String.format(MULTIPLE_STALE_OBJECTS, String.join(",", entityNames), entityName());
        }
    }

    protected abstract String entityName();

}
