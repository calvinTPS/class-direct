package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class LocationDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -1408170384405316092L;

    private String name;
    
    private String code;

    private LinkResource country;

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return this.code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public LinkResource getCountry()
    {
        return this.country;
    }

    public void setCountry(final LinkResource country)
    {
        this.country = country;
    }


}
