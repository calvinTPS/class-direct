package com.baesystems.ai.lr.enums;

public enum ClassStatus
{
    IN_CLASS_PENDING_APPROVAL(2L),
    IN_CLASS(3L),
    IN_CLASS_LAID_UP(4L),
    CLASS_SUSPENDED(5L),
    CANCELLED(6L),
    CLASS_WITHDRAWN(7L),
    UNKNOWN_MIGRATED_CLASS_STATUS(8L),
    UNKNOWN_CLASS_STATUS(9L),
    NOT_LR_CLASSED(10L),
    TOC_CONTEMPLATED(14L),
    AIC_CONTEMPALTED(15L),
    RECLASSIFICATION_CONTEMPLATED(16L),
    HELD(17L);

    private final Long value;

    ClassStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
