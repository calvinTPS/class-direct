package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class AssetTypeDto extends ImmutableReferenceDataDto
{

    private static final long serialVersionUID = 4491536951011962273L;

    @Valid
    private String code;

    @Valid
    private String name;

    @Valid
    private LinkResource category;

    @Valid
    private Long parentId;

    @Valid
    private Integer levelIndication;

    @Valid
    private LinkResource defaultClassDepartment;

    @Valid
    private LinkResource assetTypeGroup;

    @Valid
    public final String getCode()
    {
        return code;
    }

    public final void setCode(final String code)
    {
        this.code = code;
    }

    public final String getName()
    {
        return name;
    }

    public final void setName(final String name)
    {
        this.name = name;
    }

    public LinkResource getCategory()
    {
        return category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public final Long getParentId()
    {
        return parentId;
    }

    public final void setParentId(final Long parentId)
    {
        this.parentId = parentId;
    }

    public final Integer getLevelIndication()
    {
        return levelIndication;
    }

    public final void setLevelIndication(final Integer levelIndication)
    {
        this.levelIndication = levelIndication;
    }

    public LinkResource getDefaultClassDepartment()
    {
        return defaultClassDepartment;
    }

    public void setDefaultClassDepartment(final LinkResource defaultClassDepartment)
    {
        this.defaultClassDepartment = defaultClassDepartment;
    }

    public LinkResource getAssetTypeGroup()
    {
        return assetTypeGroup;
    }

    public void setAssetTypeGroup(final LinkResource assetTypeGroup)
    {
        this.assetTypeGroup = assetTypeGroup;
    }
}
