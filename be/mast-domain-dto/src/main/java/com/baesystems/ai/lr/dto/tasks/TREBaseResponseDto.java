package com.baesystems.ai.lr.dto.tasks;

import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class TREBaseResponseDto implements RuleEngineDto
{
    private static final long serialVersionUID = 2009536157988239809L;

    private String status;
    private String message;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TREBaseResponseDto [getStatus()=");
        builder.append(getStatus());
        builder.append(", getMessage()=");
        builder.append(getMessage());
        builder.append("]");
        return builder.toString();
    }
}
