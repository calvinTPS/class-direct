package com.baesystems.ai.lr.dto.defects;

import static com.baesystems.ai.lr.dto.defects.DefectLightDto.ENTITY_NAME;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.OnNotFoundException;
import com.baesystems.ai.lr.dto.annotations.OverrideDisplayName;
import com.baesystems.ai.lr.enums.JobBundleRepairAction;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class DefectLightDto extends FaultDto implements UpdateableJobBundleEntity
{
    private static final long serialVersionUID = 4241397782209250604L;

    public static final String ENTITY_NAME = "Defect";

    @ConflictAware(staticFieldDisplayName = "Affected Section (from)")
    private Integer firstFrameNumber;

    @ConflictAware(staticFieldDisplayName = "Affected Section (to)")
    private Integer lastFrameNumber;

    @ConflictAware(staticFieldDisplayName = "Prompt and Thorough Repair Required")
    private Boolean promptThoroughRepair;

    @Valid
    @ConflictAware(
            dynamicFieldDisplayName = @DynamicDisplayName(idFieldOnSource = "values.defectValue.id",
                    targetType = ReferenceDataSubSet.DEFECT_DETAILS,
                    idFieldOnTarget = "values.id",
                    overrideNames = {@OverrideDisplayName(referenceDataName = "Onboard Location", displayName = "In Way of")}),
            dynamicValueDisplayName = @DynamicDisplayName(idFieldOnSource = "values.defectValue.id",
                    targetType = ReferenceDataSubSet.DEFECT_DETAILS,
                    flattenTarget = "values"))
    @OnNotFoundException(action = JobBundleRepairAction.STRIP_IDS)
    private List<DefectDefectValueDto> values;

    @Valid
    @ConflictAware
    @OnNotFoundException(action = JobBundleRepairAction.STRIP_IDS)
    private List<DefectItemDto> items;

    private String stalenessHash;

    public Integer getFirstFrameNumber()
    {
        return this.firstFrameNumber;
    }

    public void setFirstFrameNumber(final Integer firstFrameNumber)
    {
        this.firstFrameNumber = firstFrameNumber;
    }

    public Integer getLastFrameNumber()
    {
        return this.lastFrameNumber;
    }

    public void setLastFrameNumber(final Integer lastFrameNumber)
    {
        this.lastFrameNumber = lastFrameNumber;
    }

    public Boolean getPromptThoroughRepair()
    {
        return this.promptThoroughRepair;
    }

    public void setPromptThoroughRepair(final Boolean promptThoroughRepair)
    {
        this.promptThoroughRepair = promptThoroughRepair;
    }

    public List<DefectDefectValueDto> getValues()
    {
        return this.values;
    }

    public void setValues(final List<DefectDefectValueDto> values)
    {
        this.values = values;
    }

    public List<DefectItemDto> getItems()
    {
        return this.items;
    }

    public void setItems(final List<DefectItemDto> items)
    {
        this.items = items;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String stalenessHash)
    {
        this.stalenessHash = stalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "DefectDto [jobs=" + this.getStringRepresentationOfList(this.getJobs())
                + ", getSequenceNumber()=" + this.getSequenceNumber()
                + ", getFirstFrameNumber()=" + this.getFirstFrameNumber()
                + ", getLastFrameNumber()=" + this.getLastFrameNumber()
                + ", getIncidentDate()=" + this.getStringRepresentationOfDate(this.getIncidentDate())
                + ", getIncidentDescription()=" + this.getIncidentDescription()
                + ", getTitle()=" + this.getTitle()
                + ", getCategory()=" + this.getIdOrNull(this.getCategory())
                + ", getStatus()=" + this.getIdOrNull(this.getStatus())
                + ", getAsset()=" + this.getIdOrNull(this.getAsset())
                // for linked values
                + ", getValues()=" + this.getStringRepresentationOfList(this.getValues())
                // for the relevant fields on the values themselves
                + ", getValues()=" + this.getStringRepresentationOfListOfStaleObjects(this.getValues())
                // for the linked items
                + ", getItems()=" + this.getStringRepresentationOfList(this.getItems())
                // for the relevant fields on the items themselves
                + ", getValues()=" + this.getStringRepresentationOfListOfStaleObjects(this.getItems())
                + ", getJob()=" + this.getIdOrNull(this.getJob())
                + ", getParent()=" + this.getIdOrNull(this.getParent())
                + ", getPromptThoroughRepair()=" + this.getPromptThoroughRepair()
                + ", getConfidentialityType()=" + this.getIdOrNull(this.getConfidentialityType())
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
