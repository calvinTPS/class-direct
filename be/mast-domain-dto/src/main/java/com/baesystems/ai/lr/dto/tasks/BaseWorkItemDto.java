package com.baesystems.ai.lr.dto.tasks;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.dto.base.ReportContentElementDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class BaseWorkItemDto extends ReportContentElementDto implements DueStatusUpdateableDto, HashedParentReportingDto
{
    private static final long serialVersionUID = 3255354513654790758L;

    @ConflictAware
    private String referenceCode;

    @ConflictAware
    private String name;

    @ConflictAware
    private String description;

    @ConflictAware
    private String longDescription;

    @ConflictAware
    private String serviceCode;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.WORK_ITEM_TYPES))
    private LinkResource workItemType;

    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(nameFieldOnTarget = "referenceCode", targetType = ReferenceDataSubSet.CHECKLISTS))
    private LinkResource checklist;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(nameFieldOnTarget = "referenceCode", targetType = ReferenceDataSubSet.WORK_ITEM_ACTIONS))
    private WorkItemActionDto workItemAction;

    @Valid
    @ConflictAware
    private List<WorkItemAttributeDto> attributes;

    @ConflictAware
    private Boolean attributeMandatory;

    @ConflictAware
    private Date dueDate;

    @ConflictAware
    private Date dueDateManual;

    @Size(message = "invalid length", max = 2000)
    @ConflictAware
    private String notes;

    @Valid
    @ConflictAware
    private LinkResource codicil;

    @ConflictAware
    private Boolean attachmentRequired;

    @ConflictAware
    private Integer itemOrder;

    @ConflictAware
    private Date resolutionDate;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.RESOLUTION_STATUSES))
    private LinkResource resolutionStatus;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.IACS_SOCIETIES))
    private LinkResource classSociety;

    @SubIdNotNull
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LrEmployeeDto assignedTo;

    @ConflictAware
    private Date assignedDate;

    @ConflictAware
    private Date assignedDateManual;

    @SubIdNotNull
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LrEmployeeDto resolvedBy;

    @Size(max = 20)
    @ConflictAware
    private String taskNumber;

    @Valid
    @ConflictAware
    private NamedLinkResource parent;

    @Valid
    private LinkResource dueStatus;

    @Valid
    private NamedLinkResource creditedByJob;

    private Date postponementDate;

    @Valid
    private LinkResource postponementType;

    @Valid
    private LinkResource postponedOnJob;

    private Boolean pmsApplicable;

    @ConflictAware
    private Date pmsCreditDate;

    private Boolean pmsCredited;

    @Valid
    @ConflictAware
    private LinkResource workItemScopeStatus;

    @ConflictAware
    private LinkResource approvedByJob;

    @ConflictAware
    private Date expiryDate;

    private String parentHash;

    private Date sourceJobLastVisitDate;

    private Date sourceJobCreationDateTime;

    @JsonProperty("_dmID")
    private String dmInternalLookupID;

    private Long attachmentCount;

    @ConflictAware
    private Boolean active;

    public String getReferenceCode()
    {
        return this.referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getLongDescription()
    {
        return this.longDescription;
    }

    public void setLongDescription(final String longDescription)
    {
        this.longDescription = longDescription;
    }

    public String getServiceCode()
    {
        return this.serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public LinkResource getWorkItemType()
    {
        return this.workItemType;
    }

    public void setWorkItemType(final LinkResource workItemType)
    {
        this.workItemType = workItemType;
    }

    public WorkItemActionDto getWorkItemAction()
    {
        return this.workItemAction;
    }

    public void setWorkItemAction(final WorkItemActionDto workItemAction)
    {
        this.workItemAction = workItemAction;
    }

    public Boolean getAttributeMandatory()
    {
        return this.attributeMandatory;
    }

    public void setAttributeMandatory(final Boolean attributeMandatory)
    {
        this.attributeMandatory = attributeMandatory;
    }

    @Override
    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Date getDueDateManual()
    {
        return this.dueDateManual;
    }

    public void setDueDateManual(final Date dueDateManual)
    {
        this.dueDateManual = dueDateManual;
    }

    public String getNotes()
    {
        return this.notes;
    }

    public void setNotes(final String notes)
    {
        this.notes = notes;
    }

    public LinkResource getCodicil()
    {
        return this.codicil;
    }

    public void setCodicil(final LinkResource codicil)
    {
        this.codicil = codicil;
    }

    public Boolean getAttachmentRequired()
    {
        return this.attachmentRequired;
    }

    public void setAttachmentRequired(final Boolean attachmentRequired)
    {
        this.attachmentRequired = attachmentRequired;
    }

    public Integer getItemOrder()
    {
        return this.itemOrder;
    }

    public void setItemOrder(final Integer itemOrder)
    {
        this.itemOrder = itemOrder;
    }

    public Date getResolutionDate()
    {
        return this.resolutionDate;
    }

    public void setResolutionDate(final Date resolutionDate)
    {
        this.resolutionDate = resolutionDate;
    }

    public LinkResource getResolutionStatus()
    {
        return this.resolutionStatus;
    }

    public void setResolutionStatus(final LinkResource resolutionStatus)
    {
        this.resolutionStatus = resolutionStatus;
    }

    public LinkResource getClassSociety()
    {
        return this.classSociety;
    }

    public void setClassSociety(final LinkResource classSociety)
    {
        this.classSociety = classSociety;
    }

    public LrEmployeeDto getAssignedTo()
    {
        return this.assignedTo;
    }

    public void setAssignedTo(final LrEmployeeDto assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    public LrEmployeeDto getResolvedBy()
    {
        return this.resolvedBy;
    }

    public void setResolvedBy(final LrEmployeeDto resolvedBy)
    {
        this.resolvedBy = resolvedBy;
    }

    public String getTaskNumber()
    {
        return this.taskNumber;
    }

    public void setTaskNumber(final String taskNumber)
    {
        this.taskNumber = taskNumber;
    }

    public List<WorkItemAttributeDto> getAttributes()
    {
        return this.attributes;
    }

    public void setAttributes(final List<WorkItemAttributeDto> attributes)
    {
        this.attributes = attributes;
    }

    public NamedLinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final NamedLinkResource parent)
    {
        this.parent = parent;
    }

    public Date getAssignedDate()
    {
        return this.assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public Date getAssignedDateManual()
    {
        return this.assignedDateManual;
    }

    public void setAssignedDateManual(final Date assignedDateManual)
    {
        this.assignedDateManual = assignedDateManual;
    }

    public LinkResource getDueStatus()
    {
        return this.dueStatus;
    }

    @Override
    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public NamedLinkResource getCreditedByJob()
    {
        return this.creditedByJob;
    }

    public void setCreditedByJob(final NamedLinkResource creditedByJob)
    {
        this.creditedByJob = creditedByJob;
    }

    public Date getPostponementDate()
    {
        return this.postponementDate;
    }

    public void setPostponementDate(final Date postponementDate)
    {
        this.postponementDate = postponementDate;
    }

    public LinkResource getPostponementType()
    {
        return this.postponementType;
    }

    public void setPostponementType(final LinkResource postponementType)
    {
        this.postponementType = postponementType;
    }

    public LinkResource getPostponedOnJob()
    {
        return this.postponedOnJob;
    }

    public void setPostponedOnJob(final LinkResource postponedOnJob)
    {
        this.postponedOnJob = postponedOnJob;
    }

    public Boolean getPmsApplicable()
    {
        return this.pmsApplicable;
    }

    public void setPmsApplicable(final Boolean pmsApplicable)
    {
        this.pmsApplicable = pmsApplicable;
    }

    public Date getPmsCreditDate()
    {
        return this.pmsCreditDate;
    }

    public void setPmsCreditDate(final Date pmsCreditDate)
    {
        this.pmsCreditDate = pmsCreditDate;
    }

    public Boolean getPmsCredited()
    {
        return this.pmsCredited;
    }

    public void setPmsCredited(final Boolean pmsCredited)
    {
        this.pmsCredited = pmsCredited;
    }

    public LinkResource getChecklist()
    {
        return this.checklist;
    }

    public void setChecklist(final LinkResource checklist)
    {
        this.checklist = checklist;
    }

    public LinkResource getWorkItemScopeStatus()
    {
        return this.workItemScopeStatus;
    }

    public void setWorkItemScopeStatus(final LinkResource workItemScopeStatus)
    {
        this.workItemScopeStatus = workItemScopeStatus;
    }

    public LinkResource getApprovedByJob()
    {
        return this.approvedByJob;
    }

    public void setApprovedByJob(final LinkResource approvedByJob)
    {
        this.approvedByJob = approvedByJob;
    }

    public Date getExpiryDate()
    {
        return this.expiryDate;
    }

    public void setExpiryDate(final Date expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    @Override
    public String getParentHash()
    {
        return this.parentHash;
    }

    public void setParentHash(final String parentHash)
    {
        this.parentHash = parentHash;
    }

    @Override
    public Date getSourceJobLastVisitDate()
    {
        return this.sourceJobLastVisitDate;
    }

    @Override
    public void setSourceJobLastVisitDate(final Date sourceJobLastVisitDate)
    {
        this.sourceJobLastVisitDate = sourceJobLastVisitDate;
    }

    @Override
    public Date getSourceJobCreationDateTime()
    {
        return this.sourceJobCreationDateTime;
    }

    @Override
    public void setSourceJobCreationDateTime(final Date sourceJobCreationDateTime)
    {
        this.sourceJobCreationDateTime = sourceJobCreationDateTime;
    }

    public String getDmInternalLookupID()
    {
        return this.dmInternalLookupID;
    }

    public void setDmInternalLookupID(final String dmInternalLookupID)
    {
        this.dmInternalLookupID = dmInternalLookupID;
    }

    public Long getAttachmentCount()
    {
        return this.attachmentCount;
    }

    public void setAttachmentCount(final Long attachmentCount)
    {
        this.attachmentCount = attachmentCount;
    }

    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(final Boolean active)
    {
        this.active = active;
    }
}
