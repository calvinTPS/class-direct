package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class TeamEmployeeDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -4323423452875443024L;

    private LinkResource team;

    private LinkResource employee;

    public LinkResource getTeam()
    {
        return team;
    }

    public void setTeam(final LinkResource team)
    {
        this.team = team;
    }

    public LinkResource getEmployee()
    {
        return employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }
}
