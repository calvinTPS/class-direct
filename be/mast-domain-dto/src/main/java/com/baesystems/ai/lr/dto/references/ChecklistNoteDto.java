package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ChecklistNoteDto extends ImmutableReferenceDataDto
{

    private static final long serialVersionUID = 6340464259993330903L;

    @NotNull
    private LinkResource checklistSubgroup;

    @NotNull
    @Size(message ="invalid length" , max = 5000)
    private String name;

    public LinkResource getChecklistSubgroup()
    {
        return checklistSubgroup;
    }

    public void setChecklistSubgroup(final LinkResource checklistSubgroup)
    {
        this.checklistSubgroup = checklistSubgroup;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

}
