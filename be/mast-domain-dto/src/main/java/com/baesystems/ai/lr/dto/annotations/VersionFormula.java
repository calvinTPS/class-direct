package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface VersionFormula
{
    String dtoKey();

    String targetDOField();

    String targetDOVersionIdField();
}
