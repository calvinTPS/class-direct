package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.List;

public enum ConfidentialityType
{
    LR_ONLY(1L),
    LR_AND_CUSTOMER(2L),
    ALL(3L);

    private final Long value;

    ConfidentialityType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }

    public static final List<Long> getAllValues()
    {
        return Arrays.asList(
                LR_ONLY.getValue(),
                LR_AND_CUSTOMER.getValue(),
                ALL.getValue());
    }
}
