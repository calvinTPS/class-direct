package com.baesystems.ai.lr.dto.defects;

import static com.baesystems.ai.lr.dto.defects.RepairDto.ENTITY_NAME;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class RepairDto extends FixDto implements UpdateableJobBundleEntity
{
    public static final String ENTITY_NAME = "Repair";
    private static final long serialVersionUID = 2207746146624404895L;

    @Valid
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_DEFECT)
    private LinkResource defect;

    @Valid
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_COC)
    private LinkResource codicil;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.REPAIR_TYPES))
    private List<LinkResource> repairTypes;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.REPAIR_ACTIONS))
    private LinkResource repairAction;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.MATERIAL_TYPES))
    private List<LinkResource> materialsUsed;

    @NotNull
    @Valid
    @ConflictAware
    private List<RepairItemDto> repairs;

    @NotNull
    @ConflictAware
    private Boolean promptThoroughRepair;

    @NotNull
    @ConflictAware
    @Size(message = "invalid length", max = 2000)
    private String description;

    @Size(message = "invalid length", max = 2000)
    private String narrative;

    @ConflictAware
    @Size(message = "invalid length", max = 5000)
    private String repairTypeDescription;

    @NotNull
    @ConflictAware
    private Boolean confirmed;

    private String stalenessHash;

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public LinkResource getCodicil()
    {
        return this.codicil;
    }

    public void setCodicil(final LinkResource codicil)
    {
        this.codicil = codicil;
    }

    public List<LinkResource> getRepairTypes()
    {
        return this.repairTypes;
    }

    public void setRepairTypes(final List<LinkResource> repairTypes)
    {
        this.repairTypes = repairTypes;
    }

    public LinkResource getRepairAction()
    {
        return this.repairAction;
    }

    public void setRepairAction(final LinkResource repairAction)
    {
        this.repairAction = repairAction;
    }

    public List<LinkResource> getMaterialsUsed()
    {
        return this.materialsUsed;
    }

    public void setMaterialsUsed(final List<LinkResource> materialsUsed)
    {
        this.materialsUsed = materialsUsed;
    }

    public List<RepairItemDto> getRepairs()
    {
        return this.repairs;
    }

    public void setRepairs(final List<RepairItemDto> repairs)
    {
        this.repairs = repairs;
    }

    public Boolean getPromptThoroughRepair()
    {
        return this.promptThoroughRepair;
    }

    public void setPromptThoroughRepair(final Boolean promptThoroughRepair)
    {
        this.promptThoroughRepair = promptThoroughRepair;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getNarrative()
    {
        return this.description;
    }

    public void setNarrative(final String narrative)
    {
        this.narrative = narrative;
    }

    public String getRepairTypeDescription()
    {
        return this.repairTypeDescription;
    }

    public void setRepairTypeDescription(final String repairTypeDescription)
    {
        this.repairTypeDescription = repairTypeDescription;
    }

    public Boolean getConfirmed()
    {
        return this.confirmed;
    }

    public void setConfirmed(final Boolean confirmed)
    {
        this.confirmed = confirmed;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "RepairDto [promptThoroughRepair=" + this.getPromptThoroughRepair()
                + ", description=" + this.getDescription()
                + ", repairTypeDescription=" + this.getRepairTypeDescription()
                + ", defect=" + this.getIdOrNull(this.getDefect())
                + ", repairTypes=" + this.getStringRepresentationOfList(this.repairTypes)
                + ", repairAction=" + this.getIdOrNull(this.repairAction)
                + ", materialsUsed=" + this.getStringRepresentationOfList(this.materialsUsed)
                + ", repairs=" + this.getStringRepresentationOfList(this.repairs)
                + (this.repairs == null ? ""
                        : this.repairs.stream().map(repair -> repair.getItem().getId().toString() + "," + repair.getItem().getItem().getId())
                                .collect(Collectors.joining(", ")))
                + ", parent=" + this.getIdOrNull(this.getParent())
                + ", codicil=" + this.getIdOrNull(this.getCodicil())
                + ", confirmed=" + this.getConfirmed()
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
