package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class MultiAssetPageResourceDto extends BasePageResource<MultiAssetDto>
{
    @Valid
    private List<MultiAssetDto> content;

    @Override
    public List<MultiAssetDto> getContent()
    {
        return this.content;
    }

    @Override
    public void setContent(final List<MultiAssetDto> content)
    {
        this.content = content;
    }

}
