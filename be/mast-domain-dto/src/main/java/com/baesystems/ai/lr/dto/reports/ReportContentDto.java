package com.baesystems.ai.lr.dto.reports;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.certificates.CertificateDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.ncns.MajorNCNDto;
import com.baesystems.ai.lr.dto.services.SurveyWithTaskListDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;

public class ReportContentDto implements Serializable, Dto
{
    private static final long serialVersionUID = -4982569443309854864L;

    private String assetName;

    private String imoNumber;

    private LinkResource currentFlag;

    private LinkResource registeredPort;

    private LinkResource jobLocation;

    private Date firstVisitDate;

    private Date lastVisitDate;

    private LinkResource proposedFlag;

    private List<AssetNoteDto> wipAssetNotes;

    private List<CoCDto> wipCocs;

    private List<ActionableItemDto> wipActionableItems;

    private List<MajorNCNDto> wipMNCNs;

    private List<SurveyWithTaskListDto> surveys;

    private List<WorkItemDto> tasks;

    private List<CertificateDto> certificates;

    private List<StatutoryFindingDto> statutoryFindings;

    private List<CertificateActionDto> certificateActions;

    public String getAssetName()
    {
        return assetName;
    }

    public void setAssetName(final String assetName)
    {
        this.assetName = assetName;
    }

    public String getImoNumber()
    {
        return imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public LinkResource getCurrentFlag()
    {
        return currentFlag;
    }

    public void setCurrentFlag(final LinkResource currentFlag)
    {
        this.currentFlag = currentFlag;
    }

    public LinkResource getRegisteredPort()
    {
        return registeredPort;
    }

    public void setRegisteredPort(final LinkResource registeredPort)
    {
        this.registeredPort = registeredPort;
    }

    public LinkResource getJobLocation()
    {
        return jobLocation;
    }

    public void setJobLocation(final LinkResource jobLocation)
    {
        this.jobLocation = jobLocation;
    }

    public Date getFirstVisitDate()
    {
        return firstVisitDate;
    }

    public void setFirstVisitDate(final Date firstVisitDate)
    {
        this.firstVisitDate = firstVisitDate;
    }

    public Date getLastVisitDate()
    {
        return lastVisitDate;
    }

    public void setLastVisitDate(final Date lastVisitDate)
    {
        this.lastVisitDate = lastVisitDate;
    }

    public LinkResource getProposedFlag()
    {
        return proposedFlag;
    }

    public void setProposedFlag(final LinkResource proposedFlag)
    {
        this.proposedFlag = proposedFlag;
    }

    public List<AssetNoteDto> getWipAssetNotes()
    {
        return wipAssetNotes;
    }

    public void setWipAssetNotes(final List<AssetNoteDto> wipAssetNotes)
    {
        this.wipAssetNotes = wipAssetNotes;
    }

    public List<CoCDto> getWipCocs()
    {
        return wipCocs;
    }

    public void setWipCocs(final List<CoCDto> wipCocs)
    {
        this.wipCocs = wipCocs;
    }

    public List<ActionableItemDto> getWipActionableItems()
    {
        return wipActionableItems;
    }

    public void setWipActionableItems(final List<ActionableItemDto> wipActionableItems)
    {
        this.wipActionableItems = wipActionableItems;
    }

    public List<MajorNCNDto> getWipMNCNs()
    {
        return wipMNCNs;
    }

    public void setWipMNCNs(final List<MajorNCNDto> wipMNCNs)
    {
        this.wipMNCNs = wipMNCNs;
    }

    public List<SurveyWithTaskListDto> getSurveys()
    {
        return surveys;
    }

    public void setSurveys(final List<SurveyWithTaskListDto> surveys)
    {
        this.surveys = surveys;
    }

    public List<WorkItemDto> getTasks()
    {
        return tasks;
    }

    public void setTasks(final List<WorkItemDto> tasks)
    {
        this.tasks = tasks;
    }

    public List<CertificateDto> getCertificates()
    {
        return certificates;
    }

    public void setCertificates(final List<CertificateDto> certificates)
    {
        this.certificates = certificates;
    }

    public List<StatutoryFindingDto> getStatutoryFindings()
    {
        return statutoryFindings;
    }

    public void setStatutoryFindings(final List<StatutoryFindingDto> statutoryFindings)
    {
        this.statutoryFindings = statutoryFindings;
    }

    public List<CertificateActionDto> getCertificateActions()
    {
        return certificateActions;
    }

    public void setCertificateActions(final List<CertificateActionDto> certificateActions)
    {
        this.certificateActions = certificateActions;
    }
}
