package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class TREServiceDto implements RuleEngineDto
{
    private static final long serialVersionUID = 3853624940389944954L;

    /* This ID may be null when populated via TREServiceMethod.TASKS_FOR_SERVICE */
    private Long serviceIdentifier;

    private List<TREAssetItemDto> assetItems;

    public List<TREAssetItemDto> getAssetItems()
    {
        return assetItems;
    }

    public void setAssetItems(final List<TREAssetItemDto> assetItems)
    {
        this.assetItems = assetItems;
    }

    public Long getServiceIdentifier()
    {
        return serviceIdentifier;
    }

    public void setServiceIdentifier(final Long serviceIdentifier)
    {
        this.serviceIdentifier = serviceIdentifier;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TREServiceDto [getAssetItems()=");
        builder.append(getAssetItems());
        builder.append(", getServiceIdentifier()=");
        builder.append(getServiceIdentifier());
        builder.append("]");
        return builder.toString();
    }
}
