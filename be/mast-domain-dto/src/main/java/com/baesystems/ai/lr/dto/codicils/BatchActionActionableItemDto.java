package com.baesystems.ai.lr.dto.codicils;

import com.baesystems.ai.lr.dto.base.BaseBatchActionDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public class BatchActionActionableItemDto extends BaseBatchActionDto
{
    private static final long serialVersionUID = -4222445730741683607L;

    @Valid
    @NotNull
    private ActionableItemDto actionableItemDto;

    /*
       The Key value of this map is the AssetId against which each of the DTOs in the List have been saved.
       A Map is used to assist FE count the saved DTOs against an asset particularly in the case where
       an ItemTypeID had been specified for the batch-action. This Map is populated by BE.
    */
    private Map<Long, List<ActionableItemDto>> savedActionableItemDtos;

    public ActionableItemDto getActionableItemDto()
    {
        return this.actionableItemDto;
    }

    public void setActionableItemDto(final ActionableItemDto actionableItemDto)
    {
        this.actionableItemDto = actionableItemDto;
    }

    public Map<Long, List<ActionableItemDto>> getSavedActionableItemDtos()
    {
        return this.savedActionableItemDtos;
    }

    public void setSavedActionableItemDtos(final Map<Long, List<ActionableItemDto>> actionableItemDtos)
    {
        this.savedActionableItemDtos = actionableItemDtos;
    }
}
