package com.baesystems.ai.lr.dto.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.baesystems.ai.lr.dto.base.IdDto;

public class ValidIdValidator implements ConstraintValidator<ValidId, IdDto>
{

    @Override
    public void initialize(final ValidId constraintAnnotation)
    {
    }

    @Override
    public boolean isValid(final IdDto object, final ConstraintValidatorContext constraintContext)
    {
        return object.getId() != null && object.getId() > 0;
    }

}
