package com.baesystems.ai.lr.dto.defects;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class DeficiencyPageResourceDto extends BasePageResource<DeficiencyDto>
{
    private List<DeficiencyDto> content;

    @Override
    public List<DeficiencyDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<DeficiencyDto> content)
    {
        this.content = content;
    }

}
