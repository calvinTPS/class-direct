package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class CertificateTypeDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 4355074968975436227L;

    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
