package com.baesystems.ai.lr.dto;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.base.IdDto;

public interface JobBundleEntity extends StaleObject, IdDto, Serializable
{

}
