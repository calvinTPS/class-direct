package com.baesystems.ai.lr.dto.query;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.annotations.PreserveEmptyLists;
import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

public class WorkItemQueryDto implements QueryDto
{
    private static final long serialVersionUID = 7993937122049302942L;

    private Long assetId;

    private Long assetVersionId;

    @PreserveEmptyLists
    private List<Long> surveyId;

    @PreserveEmptyLists
    private List<Long> scheduledServiceId;

    @PreserveEmptyLists
    private List<Long> itemId;

    private Boolean credited;

    private Date dueDateMin;

    private Date dueDateMax;

    private Long creditStatusId;

    @UsesLikeComparator
    private String taskName;

    private Long taskTypeId;

    private Long taskGroupId;

    @PreserveEmptyLists
    private List<Long> workItemScopeStatusId;

    private List<Long> hiddenCreditStatusIdsForContinuousService;

    private Boolean showCurrentInstancesOnly;

    private Boolean pmsable;

    public List<Long> getItemId()
    {
        return this.itemId;
    }

    public void setItemId(final List<Long> itemId)
    {
        this.itemId = itemId;
    }

    public List<Long> getSurveyId()
    {
        return this.surveyId;
    }

    public void setSurveyId(final List<Long> surveyId)
    {
        this.surveyId = surveyId;
    }

    public List<Long> getScheduledServiceId()
    {
        return this.scheduledServiceId;
    }

    public void setScheduledServiceId(final List<Long> scheduledServiceId)
    {
        this.scheduledServiceId = scheduledServiceId;
    }

    public Boolean getCredited()
    {
        return this.credited;
    }

    public void setCredited(final Boolean credited)
    {
        this.credited = credited;
    }

    public Date getDueDateMin()
    {
        return dueDateMin;
    }

    public void setDueDateMin(final Date dueDateMin)
    {
        this.dueDateMin = dueDateMin;
    }

    public Date getDueDateMax()
    {
        return dueDateMax;
    }

    public void setDueDateMax(final Date dueDateMax)
    {
        this.dueDateMax = dueDateMax;
    }

    public Long getCreditStatusId()
    {
        return creditStatusId;
    }

    public void setCreditStatusId(final Long creditStatusId)
    {
        this.creditStatusId = creditStatusId;
    }

    public String getTaskName()
    {
        return taskName;
    }

    public void setTaskName(final String taskName)
    {
        this.taskName = taskName;
    }

    public Long getTaskTypeId()
    {
        return taskTypeId;
    }

    public void setTaskTypeId(final Long taskTypeId)
    {
        this.taskTypeId = taskTypeId;
    }

    public Long getTaskGroupId()
    {
        return taskGroupId;
    }

    public void setTaskGroupId(final Long taskGroupId)
    {
        this.taskGroupId = taskGroupId;
    }

    public List<Long> getWorkItemScopeStatusId()
    {
        return workItemScopeStatusId;
    }

    public void setWorkItemScopeStatusId(final List<Long> workItemScopeStatusId)
    {
        this.workItemScopeStatusId = workItemScopeStatusId;
    }

    public List<Long> getHiddenCreditStatusIdsForContinuousService()
    {
        return hiddenCreditStatusIdsForContinuousService;
    }

    public void setHiddenCreditStatusIdsForContinuousService(final List<Long> hiddenCreditStatusIdsForContinuousService)
    {
        this.hiddenCreditStatusIdsForContinuousService = hiddenCreditStatusIdsForContinuousService;
    }

    public Boolean getShowCurrentInstancesOnly()
    {
        return showCurrentInstancesOnly;
    }

    public void setShowCurrentInstancesOnly(final Boolean showCurrentInstancesOnly)
    {
        this.showCurrentInstancesOnly = showCurrentInstancesOnly;
    }

    public Long getAssetId()
    {
        return assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Boolean getPmsable()
    {
        return this.pmsable;
    }

    public void setPmsable(final Boolean pmsable)
    {
        this.pmsable = pmsable;
    }

    public Long getAssetVersionId()
    {
        return assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    @Override
    public boolean guaranteesNoResults()
    {
        return collectionIsEmpty(surveyId)
                || collectionIsEmpty(scheduledServiceId)
                || collectionIsEmpty(itemId)
                || collectionIsEmpty(workItemScopeStatusId);
    }

    private static boolean collectionIsEmpty(final Collection<?> collection)
    {
        return collection != null && collection.isEmpty();
    }
}
