package com.baesystems.ai.lr.dto.tasks;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.Dto;

public class WorkItemPreviewListDto<T extends WorkItemPreviewCommonDto<T>> implements Serializable, Dto
{
    private static final long serialVersionUID = 4821828530767400495L;

    @NotNull
    @Valid
    private List<WorkItemPreviewScheduledServiceDto<T>> services;

    public List<WorkItemPreviewScheduledServiceDto<T>> getServices()
    {
        return services;
    }

    public void setServices(final List<WorkItemPreviewScheduledServiceDto<T>> services)
    {
        this.services = services;
    }
}
