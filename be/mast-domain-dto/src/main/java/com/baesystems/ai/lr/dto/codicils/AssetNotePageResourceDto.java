package com.baesystems.ai.lr.dto.codicils;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class AssetNotePageResourceDto extends BasePageResource<AssetNoteDto>
{
    @Valid
    private List<AssetNoteDto> content;

    @Override
    public List<AssetNoteDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<AssetNoteDto> content)
    {
        this.content = content;
    }

}
