package com.baesystems.ai.lr.dto.certificates;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.annotations.VersionFormula;
import com.baesystems.ai.lr.dto.base.VersionedDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class CertificateActionDto extends VersionedDto implements UpdateableJobBundleEntity
{
    public static final String ENTITY_NAME = "Certificate Action";
    private static final long serialVersionUID = -57353453435252522L;

    private String stalenessHash;

    @Valid
    @NotNull
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_CERTIFICATE,
            deriveVersionFrom = @VersionFormula(dtoKey = "raisedOnJob.id", targetDOField = "job.id", targetDOVersionIdField = "versionId"))
    private LinkVersionedResource certificate;

    @ConflictAware
    private Date actionDate;

    @Valid
    @NotNull
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CERTIFICATE_ACTIONS_TAKEN))
    private LinkResource actionTaken;

    @ConflictAware
    private String location;

    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LinkResource employee;

    @Valid
    @NotNull
    @ConflictAware
    private LinkResource raisedOnJob;

    @Valid
    @NotNull
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.SURVEY)
    private LinkResource service;

    public LinkVersionedResource getCertificate()
    {
        return this.certificate;
    }

    public void setCertificate(final LinkVersionedResource certificate)
    {
        this.certificate = certificate;
    }

    public Date getActionDate()
    {
        return this.actionDate;
    }

    public void setActionDate(final Date actionDate)
    {
        this.actionDate = actionDate;
    }

    public LinkResource getActionTaken()
    {
        return this.actionTaken;
    }

    public void setActionTaken(final LinkResource actionTaken)
    {
        this.actionTaken = actionTaken;
    }

    public String getLocation()
    {
        return this.location;
    }

    public void setLocation(final String location)
    {
        this.location = location;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public LinkResource getService()
    {
        return this.service;
    }

    public void setService(final LinkResource service)
    {
        this.service = service;
    }

    public LinkResource getRaisedOnJob()
    {
        return this.raisedOnJob;
    }

    public void setRaisedOnJob(final LinkResource raisedOnJob)
    {
        this.raisedOnJob = raisedOnJob;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CertificateActionDto [getCertificate()" + this.getStringRepresentationOfLinkVersionedResource(this.certificate)
                + "getActionDate()=" + this.getStringRepresentationOfDate(this.actionDate)
                + "getActionTaken()=" + this.getIdOrNull(this.actionTaken)
                + "getLocation()=" + this.location
                + "getEmployee()=" + this.getIdOrNull(this.employee)
                + "getService()=" + this.getIdOrNull(this.service)
                + "getRaisedOnJob()=" + this.getIdOrNull(this.raisedOnJob)
                + "getId()=" + this.getId()
                + "getVersionId()=" + this.getVersionId()
                + "getVersionType()=" + this.getVersionType()
                + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
