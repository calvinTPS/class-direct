package com.baesystems.ai.lr.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CodeLinkResource extends NamedLinkResource
{
    private static final long serialVersionUID = -6460529256639989139L;

    @JsonIgnore
    private String code;

    public CodeLinkResource()
    {
        super();
    }

    public CodeLinkResource(final Long id)
    {
        this();
        this.setId(id);
    }

    @JsonProperty
    public String getCode()
    {
        return code;
    }

    @JsonIgnore
    public void setCode(final String code)
    {
        this.code = code;
    }

}
