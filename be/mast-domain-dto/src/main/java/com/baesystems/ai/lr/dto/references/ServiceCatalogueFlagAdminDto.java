package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ServiceCatalogueFlagAdminDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 4354981541993997758L;

    private LinkResource flagState;

    private LinkResource serviceCatalogue;

    public LinkResource getFlagState()
    {
        return flagState;
    }

    public void setFlagState(final LinkResource flagState)
    {
        this.flagState = flagState;
    }

    public LinkResource getServiceCatalogue()
    {
        return serviceCatalogue;
    }

    public void setServiceCatalogue(final LinkResource serviceCatalogue)
    {
        this.serviceCatalogue = serviceCatalogue;
    }
}
