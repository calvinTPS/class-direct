package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;

public class AllowedAssetAttributeValueDto extends ReferenceDataDto {

    private static final long serialVersionUID = -8320755767903257658L;

    private LinkResource attributeType;

    private LinkResource itemType;

    private Long descriptionOrder;

    public LinkResource getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(final LinkResource attributeType) {
        this.attributeType = attributeType;
    }

    public LinkResource getItemType() {
        return itemType;
    }

    public void setItemType(final LinkResource itemType) {
        this.itemType = itemType;
    }

    public Long getDescriptionOrder() {
        return descriptionOrder;
    }

    public void setDescriptionOrder(final Long descriptionOrder) {
        this.descriptionOrder = descriptionOrder;
    }
}
