package com.baesystems.ai.lr.dto.warnings;

import static com.baesystems.ai.lr.dto.warnings.JobWarningDto.ENTITY_NAME;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class JobWarningDto extends AuditedDto implements UpdateableJobBundleEntity
{
    private static final long serialVersionUID = 664589240439713887L;

    public static final String ENTITY_NAME = "JobWarning";

    private String stalenessHash;

    @Valid
    @NotNull
    private LinkResource job;

    private Date creationDate;

    @Valid
    @NotNull
    private int warningNumber;

    @Size(max = 500, message = "invalid length")
    private String description;

    @Override
    public String getStalenessHash()
    {
        return stalenessHash;
    }

    @Override
    public void setStalenessHash(final String stalenessHash)
    {
        this.stalenessHash = stalenessHash;
    }

    public LinkResource getJob()
    {
        return job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public int getWarningNumber()
    {
        return warningNumber;
    }

    public void setWarningNumber(final int warningNumber)
    {
        this.warningNumber = warningNumber;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "JobWarningDto[stalenessHash=" + this.stalenessHash
                + ", job=" + this.getIdOrNull(this.job)
                + ", creationDate=" + this.getStringRepresentationOfDate(this.creationDate)
                + ", warningNumber=" + this.warningNumber
                + ", description=" + this.description
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
