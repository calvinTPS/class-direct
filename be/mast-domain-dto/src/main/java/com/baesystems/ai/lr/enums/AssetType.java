package com.baesystems.ai.lr.enums;

public enum AssetType
{
    NAVAL_OR_NAVAL_AUXILIARY(126L);

    private final Long value;

    AssetType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
