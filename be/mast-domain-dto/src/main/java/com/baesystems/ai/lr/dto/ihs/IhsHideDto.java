package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsHideDto extends IhsBaseDto
{

    private static final long serialVersionUID = 8147130127828545630L;

    private Double depthMoulded;

    public Double getDepthMoulded()
    {
        return depthMoulded;
    }

    public void setDepthMoulded(final Double depthMoulded)
    {
        this.depthMoulded = depthMoulded;
    }
}
