package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum IHSReplicaCableGrade
{
    /*
    IHS cable grade to desired value mapping.
     */
    NULL(null, ""),
    SQL_SERVER_NULL("<null>", ""),
    ES("ES", "ESQ"),
    FS("FS", "FSWR"),
    SQ("SQ", "SQ"),
    WI("WI", "WI"),
    U1("U1", "U1"),
    U2("U2", "U2"),
    U3("U3", "U3"),
    TWO_A("2A", "U2(a)"),
    THREE_A("3A", "U3(a)"),
    MS("MS", "MS"),
    ONE_B("1B", "U1(b)"),
    NN("NN", "N/A"),
    UU("UU", "U/K"),
    YY("YY", "Yes"),
    NO_CONVERSION("NoConversion", "NoConversion");

    private final String replicaValue;

    private String convertedValue;

    private static final Map<String, IHSReplicaCableGrade> CABLE_GRADE_MAP = Arrays
            .stream(IHSReplicaCableGrade.values())
            .collect(Collectors.toMap(IHSReplicaCableGrade::getReplicaValue, Function.identity()));

    IHSReplicaCableGrade(final String IHSGrade, final String convertedGrade)
    {
        this.replicaValue = IHSGrade;
        this.convertedValue = convertedGrade;
    }

    public static IHSReplicaCableGrade getEnum(final String replicaCableGrade)
    {
        return CABLE_GRADE_MAP.getOrDefault(replicaCableGrade, NO_CONVERSION);
    }

    public final String getReplicaValue()
    {
        return this.replicaValue;
    }

    public final String getConvertedValue()
    {
        return this.convertedValue;
    }

    private void setConvertedValue(final String convertedValue)
    {
        this.convertedValue = convertedValue;
    }

}
