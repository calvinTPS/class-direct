package com.baesystems.ai.lr.dto.validation;

import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class DraftItemRelationshipDto extends BaseDto
{
    private static final long serialVersionUID = 1565123169696891960L;

    private Long originalItemId;
    private Long relationshipTypeId;

    // If you want to terminate the recursion, empty list here please, NOT null
    private List<DraftItemRelationshipDto> itemRelationshipDto;

    public Long getOriginalItemId()
    {
        return originalItemId;
    }

    public void setOriginalItemId(final Long originalItemId)
    {
        this.originalItemId = originalItemId;
    }

    public Long getRelationshipTypeId()
    {
        return relationshipTypeId;
    }

    public void setRelationshipTypeId(Long relationshipTypeId)
    {
        this.relationshipTypeId = relationshipTypeId;
    }

    public List<DraftItemRelationshipDto> getItemRelationshipDto()
    {
        return itemRelationshipDto;
    }

    public void setItemRelationshipDto(final List<DraftItemRelationshipDto> itemRelationshipDto)
    {
        this.itemRelationshipDto = itemRelationshipDto;
    }
}
