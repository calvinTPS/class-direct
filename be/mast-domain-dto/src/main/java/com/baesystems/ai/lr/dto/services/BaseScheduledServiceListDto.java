package com.baesystems.ai.lr.dto.services;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.Dto;

public class BaseScheduledServiceListDto<T extends AbstractScheduledServiceDto> implements Serializable, Dto
{
    private static final long serialVersionUID = 2947839802965876099L;

    @NotNull
    @Valid
    @Size(min = 1)
    private List<T> scheduledServices;

    public List<T> getScheduledServices()
    {
        return this.scheduledServices;
    }

    public void setScheduledServices(final List<T> scheduledServices)
    {
        this.scheduledServices = scheduledServices;
    }
}
