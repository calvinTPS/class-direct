package com.baesystems.ai.lr.dto.customers;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.references.OfficeDto;

public class PartyDto extends PartyBaseDto
{
    private static final long serialVersionUID = 5772816887977390746L;

    private Long cdhCustomerId;

    @Size(message = "invalid length", max = 240)
    private String jdeReferenceNumber;

    @Size(message = "invalid length", max = 30)
    private String phoneNumber;

    @Size(message = "invalid length", max = 30)
    private String faxNumber;

    @SubIdNotNull
    @Valid
    private OfficeDto office;

    @Size(message = "invalid length", max = 110)
    private String addressLine1;

    @Size(message = "invalid length", max = 50)
    private String addressLine2;

    @Size(message = "invalid length", max = 65)
    private String addressLine3;

    @Size(message = "invalid length", max = 40)
    private String city;

    @Size(message = "invalid length", max = 30)
    private String postcode;

    private String country;

    @Size(message = "invalid length", max = 100)
    private String emailAddress;

    @Size(message = "invalid length", max = 200)
    private String website;

    public Long getCdhCustomerId()
    {
        return cdhCustomerId;
    }

    public void setCdhCustomerId(final Long cdhCustomerId)
    {
        this.cdhCustomerId = cdhCustomerId;
    }

    public String getJdeReferenceNumber()
    {
        return jdeReferenceNumber;
    }

    public void setJdeReferenceNumber(final String jdeReferenceNumber)
    {
        this.jdeReferenceNumber = jdeReferenceNumber;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber()
    {
        return faxNumber;
    }

    public void setFaxNumber(final String faxNumber)
    {
        this.faxNumber = faxNumber;
    }

    public OfficeDto getOffice()
    {
        return office;
    }

    public void setOffice(final OfficeDto office)
    {
        this.office = office;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(final String city)
    {
        this.city = city;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(final String postcode)
    {
        this.postcode = postcode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(final String country)
    {
        this.country = country;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getWebsite()
    {
        return website;
    }

    public void setWebsite(final String website)
    {
        this.website = website;
    }
}
