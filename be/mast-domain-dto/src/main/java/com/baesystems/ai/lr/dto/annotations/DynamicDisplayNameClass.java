package com.baesystems.ai.lr.dto.annotations;

import com.baesystems.ai.lr.enums.ReferenceDataSubSet;

public class DynamicDisplayNameClass
{
    private final DynamicDisplayName annotation;

    private final String defaultFieldName;

    public DynamicDisplayNameClass(final DynamicDisplayName annotation, final String defaultFieldName)
    {
        this.annotation = annotation;
        this.defaultFieldName = defaultFieldName + ".id";
    }

    public String idFieldOnSource()
    {
        // if this is an empty string (ie is the default) then use the field name
        return this.annotation.idFieldOnSource().isEmpty() ? this.defaultFieldName : this.annotation.idFieldOnSource();
    }

    public String idFieldOnTarget()
    {
        return this.annotation.idFieldOnTarget();
    }

    public String nameFieldOnTarget()
    {
        return this.annotation.nameFieldOnTarget();
    }

    public ReferenceDataSubSet targetType()
    {
        return this.annotation.targetType();
    }

    public String flattenTarget()
    {
        return this.annotation.flattenTarget();
    }

    public OverrideDisplayName[] overrideNames()
    {
        return this.annotation.overrideNames();
    }

    public String format()
    {
        return this.annotation.format();
    }

    public boolean active()
    {
        return this.annotation.active();
    }
}
