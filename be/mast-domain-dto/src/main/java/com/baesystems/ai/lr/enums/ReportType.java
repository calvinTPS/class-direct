package com.baesystems.ai.lr.enums;

import java.util.HashMap;
import java.util.Map;

public enum ReportType
{
    UNKNOWN(0L),
    DAR(4L),
    FAR(2L),
    DSR(3L),
    FSR(1L);

    // to avoid magic number warnings
    private static final Integer FIRST = 1;
    private static final Integer SECOND = 2;
    private static final Integer THIRD = 3;
    private static final Integer FOURTH = 4;

    private final Long value;

    private static Map<Long, ReportType> idMap = new HashMap<Long, ReportType>();

    private static Map<Long, Integer> reportOrder = new HashMap<Long, Integer>();
    private static Map<Integer, Long> orderReport = new HashMap<Integer, Long>();

    static
    {
        for (final ReportType val : ReportType.values())
        {
            idMap.put(val.value(), val);
        }

        orderReport.put(FIRST, DAR.value());
        orderReport.put(SECOND, FAR.value());
        orderReport.put(THIRD, DSR.value());
        orderReport.put(FOURTH, FSR.value());

        orderReport.entrySet().stream()
                .forEach(orderReportEntry -> reportOrder.put(orderReportEntry.getValue(), orderReportEntry.getKey()));
    }

    ReportType(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }

    /**
     * @return get the enum that has the value type id.
     */
    public static ReportType findById(final Long typeId)
    {
        final ReportType type = idMap.get(typeId);
        return type == null ? UNKNOWN : type;
    }

    /**
     * @param typeId
     * @param referenceType
     * @return true if the report with reportTypeId is before referenceType in the reporting sequence
     */
    public static Boolean isBefore(final Long typeId, final ReportType referenceType)
    {
        return reportOrder.get(typeId) < reportOrder.get(referenceType.value());
    }

    /**
     * @param typeId
     * @param referenceType
     * @return true if the report with reportTypeId is after referenceType in the reporting sequence
     */
    public static Boolean isAfter(final Long typeId, final ReportType referenceType)
    {
        return reportOrder.get(typeId) > reportOrder.get(referenceType.value());
    }

    /**
     * @param currentTypeId
     * @return the id of the type that comes before the current type whose id is passed in in the ordered report
     *         sequence.
     */
    public static Long getTypeBeforeCurrent(final Long currentTypeId)
    {
        final Integer correntPosition = reportOrder.get(currentTypeId);
        Long beforeTypeId = UNKNOWN.value();

        if (correntPosition != null && correntPosition > FIRST)
        {
            beforeTypeId = orderReport.get(correntPosition - 1);
        }

        return beforeTypeId;
    }
}
