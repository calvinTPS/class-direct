package com.baesystems.ai.lr.dto.assets;

import javax.validation.Valid;

public class AssetDto extends AssetLightDto
{

    private static final long serialVersionUID = 7507491534404025751L;

    @Valid
    private AssetModelDto assetModel;

    public AssetModelDto getAssetModel()
    {
        return assetModel;
    }

    public void setAssetModel(final AssetModelDto assetModel)
    {
        this.assetModel = assetModel;
    }
}
