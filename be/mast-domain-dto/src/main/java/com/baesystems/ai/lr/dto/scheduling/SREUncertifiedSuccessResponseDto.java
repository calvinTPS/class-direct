package com.baesystems.ai.lr.dto.scheduling;

public class SREUncertifiedSuccessResponseDto extends SREBaseResponseDto
{
    private static final long serialVersionUID = -3187162141474655473L;

    private SREServiceDto service;

    private Integer hullIndicator;

    public SREServiceDto getService()
    {
        return service;
    }

    public void setService(final SREServiceDto service)
    {
        this.service = service;
    }

    public Integer getHullIndicator()
    {
        return hullIndicator;
    }

    public void setHullIndicator(final Integer hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("SREUncertifiedSuccessResponseDto [getService()=");
        builder.append(getService());
        builder.append(", getHullIndicator()=");
        builder.append(getHullIndicator());
        builder.append(", getMessage()=");
        builder.append(getMessage());
        builder.append(", getStatus()=");
        builder.append(getStatus());
        builder.append("]");
        return builder.toString();
    }
}
