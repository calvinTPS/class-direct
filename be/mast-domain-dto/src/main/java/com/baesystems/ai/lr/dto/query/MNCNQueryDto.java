package com.baesystems.ai.lr.dto.query;

import java.util.List;

public class MNCNQueryDto implements QueryDto
{
    private static final long serialVersionUID = -3961243372019727652L;

    private List<Long> statusList;

    private Long assetId;

    private Long jobId;

    public List<Long> getStatusList()
    {
        return statusList;
    }

    public void setStatusList(final List<Long> statusList)
    {
        this.statusList = statusList;
    }

    public Long getAssetId()
    {
        return this.assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getJobId()
    {
        return this.jobId;
    }

    public void setJobId(final Long jobId)
    {
        this.jobId = jobId;
    }

    public void setQueryPathFields(final Long jobId, final Long assetId)
    {
        this.jobId = jobId;
        this.assetId = assetId;
    }
}
