package com.baesystems.ai.lr.enums;

public enum Relationship
{ // enum for internal use
    EQ(0),
    GT(1),
    LT(-1);

    private int value;

    Relationship(final int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}
