package com.baesystems.ai.lr.dto.jobs;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@IgnoreIdsInConflicts
public class PR17DeficiencyDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = 3319050347177105504L;

    @NotNull
    @Valid
    @ConflictAware(
            dynamicFieldDisplayName = @DynamicDisplayName(idFieldOnTarget = "deficiencies.id", targetType = ReferenceDataSubSet.DEFICIENCY_GROUP),
            dynamicValueDisplayName = @DynamicDisplayName(nameFieldOnTarget = "displayOrder", targetType = ReferenceDataSubSet.DEFICIENCY))
    private LinkResource deficiency;

    @ConflictAware(
            dynamicFieldDisplayName = @DynamicDisplayName(idFieldOnSource = "deficiency.id",
                    idFieldOnTarget = "deficiencies.id",
                    targetType = ReferenceDataSubSet.DEFICIENCY_GROUP,
                    format = "%s Comments"))
    @Size(message = "invalid length", max = 500)
    private String description;

    private String stalenessHash;

    public LinkResource getDeficiency()
    {
        return this.deficiency;
    }

    public void setDeficiency(final LinkResource deficiency)
    {
        this.deficiency = deficiency;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "PR17DeficiencyDto [deficiency=" + getIdOrNull(getDeficiency())
                + "description=" + getDescription()
                + "]";
    }

}
