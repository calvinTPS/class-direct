package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsSsch2Dto extends IhsBaseDto
{
    private static final long serialVersionUID = -2212835568628441098L;

    private Integer grossTonnage;

    private Float breadth;

    public Integer getGrossTonnage()
    {
        return grossTonnage;
    }

    public void setGrossTonnage(final Integer grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public Float getBreadth()
    {
        return breadth;
    }

    public void setBreadth(final Float breadth)
    {
        this.breadth = breadth;
    }
}
