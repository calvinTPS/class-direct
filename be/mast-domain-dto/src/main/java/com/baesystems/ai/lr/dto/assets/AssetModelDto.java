package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class AssetModelDto extends BaseDto
{
    private static final long serialVersionUID = -8598864946256797928L;

    @Valid
    private List<ItemDto> items; //This shouldn't really be an array as there can only be one, but leave it for now

    public List<ItemDto> getItems()
    {
        return items;
    }

    public void setItems(final List<ItemDto> items)
    {
        this.items = items;
    }
}
