package com.baesystems.ai.lr.dto.jobs;

public class JobWithFlagsDto extends JobDto
{
    private static final long serialVersionUID = 3479305338557832913L;

    private Boolean flagTM;

    private Boolean flagESP;

    public Boolean getFlagTM()
    {
        return this.flagTM;
    }

    public void setFlagTM(final Boolean flagTM)
    {
        this.flagTM = flagTM;
    }

    public Boolean getFlagESP()
    {
        return this.flagESP;
    }

    public void setFlagESP(final Boolean flagESP)
    {
        this.flagESP = flagESP;
    }
}
