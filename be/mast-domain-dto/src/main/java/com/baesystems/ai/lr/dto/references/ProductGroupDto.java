package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ProductGroupDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 3788317500709381860L;

    @Size(message = "invalid length", max = 45)
    private String name;

    @Size(message = "invalid length", max = 45)
    private String description;

    private Integer displayOrder;

    private LinkResource productType;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public LinkResource getProductType()
    {
        return this.productType;
    }

    public void setProductType(final LinkResource productType)
    {
        this.productType = productType;
    }
}
