package com.baesystems.ai.lr.dto;

public interface Versioned
{
    Long getVersionId();
}
