package com.baesystems.ai.lr.dto.base;

public interface LongNarrativeDto
{
    String getNarrative();

    void setNarrative(String narrative);

    String getLongNarrative();

    void setLongNarrative(String longNarrative);
}
