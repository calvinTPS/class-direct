package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

public class CountryDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -758157203912795330L;

    @Size(message = "invalid length", max = 3)
    private String code;

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }
}
