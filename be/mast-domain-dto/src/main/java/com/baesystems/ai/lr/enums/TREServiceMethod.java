package com.baesystems.ai.lr.enums;

public enum TREServiceMethod
{
    TASKS_FOR_ASSET("tasksForAsset"),
    TASKS_FOR_ASSET_SERVICE("tasksForAssetService");

    private final String methodName;

    private TREServiceMethod(final String methodName)
    {
        this.methodName = methodName;
    }

    public String getMethodName()
    {
        return methodName;
    }
}
