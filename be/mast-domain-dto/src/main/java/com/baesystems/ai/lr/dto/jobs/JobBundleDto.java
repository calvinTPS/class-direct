package com.baesystems.ai.lr.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.AssetItemActionDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.certificates.CertificateDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.defects.RectificationDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.ncns.MajorNCNDto;
import com.baesystems.ai.lr.dto.references.ChecklistAssetTypeGroupDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyWithTaskListDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * This DTO is to be used to send all the required data for Job, such that a client may work "offline".
 */
@JsonIgnoreProperties("warning")
@SuppressWarnings("PMD.ExcessivePublicCount")
public class JobBundleDto extends BaseDto
{
    private static final long serialVersionUID = -5132954694615676811L;

    // Job Specific info
    private JobDto job;

    // Asset specific info
    private AssetLightDto asset;
    private AssetModelDto assetModel;
    private AssetLightDto publishedAsset;

    // codicils
    private List<ActionableItemDto> actionableItem;
    private List<CoCDto> coC;
    private List<AssetNoteDto> assetNote;
    private List<StatutoryFindingDto> statutoryFinding;

    // wip codicils
    private List<ActionableItemDto> wipActionableItem;
    private List<CoCDto> wipCoC;
    private List<AssetNoteDto> wipAssetNote;
    private List<StatutoryFindingDto> wipStatutoryFinding;

    // defects and repairs
    private List<DefectDto> defect;
    private List<RepairDto> repair;

    // wip defects and wip repairs
    private List<DefectDto> wipDefect;
    private List<RepairDto> wipRepair;

    // deficiencies and rectification
    private List<DeficiencyDto> deficiency;
    private List<RectificationDto> rectification;

    // wip deficiencies and wip rectification
    private List<DeficiencyDto> wipDeficiency;
    private List<RectificationDto> wipRectification;

    // products
    private List<ProductDto> product;

    // services and tasks
    private List<ScheduledServiceDto> service;
    private List<ScheduledServiceDto> historicService;
    private List<WorkItemLightDto> task;
    private List<WorkItemLightDto> historicChecklistTask;

    // wip services and wip tasks
    private List<SurveyWithTaskListDto> survey;
    private List<WorkItemLightDto> wipTask;

    // asset related certificates and Actions
    private List<CertificateDto> assetRelatedCertificate;
    private List<CertificateActionDto> assetRelatedCertificateAction;

    // job related certificates and Actions
    private List<CertificateDto> jobRelatedCertificate;
    private List<CertificateActionDto> jobRelatedCertificateAction;

    // mncns
    private List<MajorNCNDto> mncn;
    private List<MajorNCNDto> wipMncn;

    // reports
    private List<ReportDto> report;

    // pr17s
    private List<PR17Dto> prSeventeen;

    // Attachments
    private List<SupplementaryInformationDto> attachment;

    // Item actions
    private List<AssetItemActionDto> itemAction;

    // standard hours
    private List<StandardHourDto> standardHour;

    // Checklist Asset Type Group Mapping
    private List<ChecklistAssetTypeGroupDto> checklistAssetTypeGroup;

    public JobDto getJob()
    {
        return this.job;
    }

    public void setJob(final JobDto job)
    {
        this.job = job;
    }

    public AssetLightDto getAsset()
    {
        return this.asset;
    }

    public void setAsset(final AssetLightDto asset)
    {
        this.asset = asset;
    }

    public AssetModelDto getAssetModel()
    {
        return this.assetModel;
    }

    public void setAssetModel(final AssetModelDto assetModel)
    {
        this.assetModel = assetModel;
    }

    public List<ActionableItemDto> getActionableItem()
    {
        return this.actionableItem;
    }

    public void setActionableItem(final List<ActionableItemDto> actionableItem)
    {
        this.actionableItem = actionableItem;
    }

    public List<CoCDto> getCoC()
    {
        return this.coC;
    }

    public void setCoC(final List<CoCDto> coC)
    {
        this.coC = coC;
    }

    public List<AssetNoteDto> getAssetNote()
    {
        return this.assetNote;
    }

    public void setAssetNote(final List<AssetNoteDto> assetNote)
    {
        this.assetNote = assetNote;
    }

    public List<StatutoryFindingDto> getStatutoryFinding()
    {
        return this.statutoryFinding;
    }

    public void setStatutoryFinding(final List<StatutoryFindingDto> statutoryFinding)
    {
        this.statutoryFinding = statutoryFinding;
    }

    public List<ActionableItemDto> getWipActionableItem()
    {
        return this.wipActionableItem;
    }

    public void setWipActionableItem(final List<ActionableItemDto> wipActionableItem)
    {
        this.wipActionableItem = wipActionableItem;
    }

    public List<CoCDto> getWipCoC()
    {
        return this.wipCoC;
    }

    public void setWipCoC(final List<CoCDto> wipCoC)
    {
        this.wipCoC = wipCoC;
    }

    public List<AssetNoteDto> getWipAssetNote()
    {
        return this.wipAssetNote;
    }

    public void setWipAssetNote(final List<AssetNoteDto> wipAssetNote)
    {
        this.wipAssetNote = wipAssetNote;
    }

    public List<StatutoryFindingDto> getWipStatutoryFinding()
    {
        return this.wipStatutoryFinding;
    }

    public void setWipStatutoryFinding(final List<StatutoryFindingDto> wipStatutoryFinding)
    {
        this.wipStatutoryFinding = wipStatutoryFinding;
    }

    public List<DefectDto> getDefect()
    {
        return this.defect;
    }

    public void setDefect(final List<DefectDto> defect)
    {
        this.defect = defect;
    }

    public List<RepairDto> getRepair()
    {
        return this.repair;
    }

    public void setRepair(final List<RepairDto> repair)
    {
        this.repair = repair;
    }

    public List<DefectDto> getWipDefect()
    {
        return this.wipDefect;
    }

    public void setWipDefect(final List<DefectDto> wipDefect)
    {
        this.wipDefect = wipDefect;
    }

    public List<RepairDto> getWipRepair()
    {
        return this.wipRepair;
    }

    public void setWipRepair(final List<RepairDto> wipRepair)
    {
        this.wipRepair = wipRepair;
    }

    public List<DeficiencyDto> getDeficiency()
    {
        return this.deficiency;
    }

    public void setDeficiency(final List<DeficiencyDto> deficiency)
    {
        this.deficiency = deficiency;
    }

    public List<RectificationDto> getRectification()
    {
        return this.rectification;
    }

    public void setRectification(final List<RectificationDto> rectification)
    {
        this.rectification = rectification;
    }

    public List<DeficiencyDto> getWipDeficiency()
    {
        return this.wipDeficiency;
    }

    public void setWipDeficiency(final List<DeficiencyDto> wipDeficiency)
    {
        this.wipDeficiency = wipDeficiency;
    }

    public List<RectificationDto> getWipRectification()
    {
        return this.wipRectification;
    }

    public void setWipRectification(final List<RectificationDto> wipRectification)
    {
        this.wipRectification = wipRectification;
    }

    public List<ProductDto> getProduct()
    {
        return this.product;
    }

    public void setProduct(final List<ProductDto> product)
    {
        this.product = product;
    }

    public List<ScheduledServiceDto> getService()
    {
        return this.service;
    }

    public void setService(final List<ScheduledServiceDto> service)
    {
        this.service = service;
    }

    public List<WorkItemLightDto> getTask()
    {
        return this.task;
    }

    public void setTask(final List<WorkItemLightDto> task)
    {
        this.task = task;
    }

    public List<WorkItemLightDto> getHistoricChecklistTask()
    {
        return historicChecklistTask;
    }

    public void setHistoricChecklistTask(final List<WorkItemLightDto> historicChecklistTask)
    {
        this.historicChecklistTask = historicChecklistTask;
    }

    public List<SurveyWithTaskListDto> getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final List<SurveyWithTaskListDto> survey)
    {
        this.survey = survey;
    }

    public List<WorkItemLightDto> getWipTask()
    {
        return this.wipTask;
    }

    public void setWipTask(final List<WorkItemLightDto> wipTask)
    {
        this.wipTask = wipTask;
    }

    public List<CertificateDto> getAssetRelatedCertificate()
    {
        return this.assetRelatedCertificate;
    }

    public void setAssetRelatedCertificate(final List<CertificateDto> assetRelatedCertificate)
    {
        this.assetRelatedCertificate = assetRelatedCertificate;
    }

    public List<CertificateActionDto> getAssetRelatedCertificateAction()
    {
        return this.assetRelatedCertificateAction;
    }

    public void setAssetRelatedCertificateAction(final List<CertificateActionDto> assetRelatedCertificateAction)
    {
        this.assetRelatedCertificateAction = assetRelatedCertificateAction;
    }

    public List<CertificateDto> getJobRelatedCertificate()
    {
        return this.jobRelatedCertificate;
    }

    public void setJobRelatedCertificate(final List<CertificateDto> jobRelatedCertificate)
    {
        this.jobRelatedCertificate = jobRelatedCertificate;
    }

    public List<CertificateActionDto> getJobRelatedCertificateAction()
    {
        return this.jobRelatedCertificateAction;
    }

    public void setJobRelatedCertificateAction(final List<CertificateActionDto> jobRelatedCertificateAction)
    {
        this.jobRelatedCertificateAction = jobRelatedCertificateAction;
    }

    public List<MajorNCNDto> getMncn()
    {
        return this.mncn;
    }

    public void setMncn(final List<MajorNCNDto> mncn)
    {
        this.mncn = mncn;
    }

    public List<MajorNCNDto> getWipMncn()
    {
        return this.wipMncn;
    }

    public void setWipMncn(final List<MajorNCNDto> wipMncn)
    {
        this.wipMncn = wipMncn;
    }

    public List<ReportDto> getReport()
    {
        return this.report;
    }

    public void setReport(final List<ReportDto> report)
    {
        this.report = report;
    }

    public List<PR17Dto> getPrSeventeen()
    {
        return this.prSeventeen;
    }

    public void setPrSeventeen(final List<PR17Dto> prSeventeen)
    {
        this.prSeventeen = prSeventeen;
    }

    public List<SupplementaryInformationDto> getAttachment()
    {
        return this.attachment;
    }

    public void setAttachment(final List<SupplementaryInformationDto> attachment)
    {
        this.attachment = attachment;
    }

    public List<AssetItemActionDto> getItemAction()
    {
        return this.itemAction;
    }

    public void setItemAction(final List<AssetItemActionDto> itemAction)
    {
        this.itemAction = itemAction;
    }

    public AssetLightDto getPublishedAsset()
    {
        return publishedAsset;
    }

    public void setPublishedAsset(final AssetLightDto publishedAsset)
    {
        this.publishedAsset = publishedAsset;
    }

    public List<StandardHourDto> getStandardHour()
    {
        return standardHour;
    }

    public void setStandardHour(final List<StandardHourDto> standardHour)
    {
        this.standardHour = standardHour;
    }

    public List<ChecklistAssetTypeGroupDto> getChecklistAssetTypeGroup()
    {
        return checklistAssetTypeGroup;
    }

    public void setChecklistAssetTypeGroup(final List<ChecklistAssetTypeGroupDto> checklistAssetTypeGroup)
    {
        this.checklistAssetTypeGroup = checklistAssetTypeGroup;
    }

    public List<ScheduledServiceDto> getHistoricService()
    {
        return historicService;
    }

    public void setHistoricService(final List<ScheduledServiceDto> historicService)
    {
        this.historicService = historicService;
    }
}
