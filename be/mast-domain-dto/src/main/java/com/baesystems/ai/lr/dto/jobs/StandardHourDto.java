package com.baesystems.ai.lr.dto.jobs;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.AuditedDto;

public class StandardHourDto extends AuditedDto
{
    private static final long serialVersionUID = 7223972872878062162L;

    @Valid
    @NotNull
    private LinkResource job;

    @Size(message = "invalid length", max = 10)
    private String serviceCode;

    private Double standardHours;

    public LinkResource getJob()
    {
        return job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public Double getStandardHours()
    {
        return standardHours;
    }

    public void setStandardHours(final Double standardHours)
    {
        this.standardHours = standardHours;
    }
}
