package com.baesystems.ai.lr.dto.references;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.services.StalenessListDto;

import static com.baesystems.ai.lr.dto.references.ProductListDto.ENTITY_NAME;

@EntityName(ENTITY_NAME)
public class ProductListDto extends StalenessListDto implements Serializable
{
    public static final String ENTITY_NAME = "Product";
    private static final long serialVersionUID = 6583972251905124705L;

    @NotNull
    private List<ProductDto> productList;

    public List<ProductDto> getProductList()
    {
        return this.productList;
    }

    public void setProductList(final List<ProductDto> productList)
    {
        this.productList = productList;
    }

    @Override
    protected String entityName()
    {
        return ENTITY_NAME;
    }
}
