package com.baesystems.ai.lr.dto.query;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.annotations.PreserveEmptyLists;
import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

/**
 * Dto for combining the various fields for searching reminders.
 */
@PreserveEmptyLists
public class ReminderQueryDto implements QueryDto
{
    private static final long serialVersionUID = 7878012592844636282L;

    private List<Integer> severities;

    private Date lowerReviewDateLimit;

    private Date upperReviewDateLimit;

    private List<Long> departmentSectionIds;

    private List<Long> officeIds;

    private List<Long> officeRoleIds;

    private List<Long> partyId;

    private List<Long> partyRoleId;

    @UsesLikeComparator
    private String assetIdNameOrImo;

    private Boolean isWatched;

    public List<Integer> getSeverities()
    {
        return this.severities;
    }

    public void setSeverities(final List<Integer> severities)
    {
        this.severities = severities;
    }

    public Date getLowerReviewDateLimit()
    {
        return this.lowerReviewDateLimit;
    }

    public void setLowerReviewDateLimit(final Date lowerReviewDateLimit)
    {
        this.lowerReviewDateLimit = lowerReviewDateLimit;
    }

    public Date getUpperReviewDateLimit()
    {
        return this.upperReviewDateLimit;
    }

    public void setUpperReviewDateLimit(final Date upperReviewDateLimit)
    {
        this.upperReviewDateLimit = upperReviewDateLimit;
    }

    public List<Long> getDepartmentSectionIds()
    {
        return this.departmentSectionIds;
    }

    public void setDepartmentSectionIds(final List<Long> departmentSectionIds)
    {
        this.departmentSectionIds = departmentSectionIds;
    }

    public List<Long> getOfficeIds()
    {
        return this.officeIds;
    }

    public void setOfficeIds(final List<Long> officeIds)
    {
        this.officeIds = officeIds;
    }

    public String getAssetIdNameOrImo()
    {
        return assetIdNameOrImo;
    }

    public void setAssetIdNameOrImo(final String assetIdNameOrImo)
    {
        this.assetIdNameOrImo = assetIdNameOrImo;
    }

    public Boolean getIsWatched()
    {
        return isWatched;
    }

    public void setIsWatched(final Boolean isWatched)
    {
        this.isWatched = isWatched;
    }

    public List<Long> getPartyId()
    {
        return partyId;
    }

    public void setPartyId(final List<Long> partyId)
    {
        this.partyId = partyId;
    }

    public List<Long> getPartyRoleId()
    {
        return partyRoleId;
    }

    public void setPartyRoleId(final List<Long> partyRoleId)
    {
        this.partyRoleId = partyRoleId;
    }

    public List<Long> getOfficeRoleIds()
    {
        return officeRoleIds;
    }

    public void setOfficeRoleIds(final List<Long> officeRoleIds)
    {
        this.officeRoleIds = officeRoleIds;
    }
}
