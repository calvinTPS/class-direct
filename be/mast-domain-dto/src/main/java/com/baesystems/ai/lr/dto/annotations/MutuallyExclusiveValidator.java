package com.baesystems.ai.lr.dto.annotations;

import java.lang.reflect.InvocationTargetException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;

public class MutuallyExclusiveValidator implements ConstraintValidator<MutuallyExclusive, Object>
{
    private String[] fieldNames;
    private boolean allowAllNull;

    /**
     * Get the field names from the annotation.
     */
    @Override
    public void initialize(final MutuallyExclusive constraintAnnotation)
    {
        fieldNames = constraintAnnotation.fields();
        allowAllNull = constraintAnnotation.allowAllNull();
    }

    /**
     * Method for testing if the no more than one of the field is not null.
     */
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context)
    {
        Integer nonNullCount = 0;
        boolean valid = false;

        try
        {
            for (final String fieldName : fieldNames)
            {
                if (PropertyUtils.getProperty(value, fieldName) != null)
                {
                    ++nonNullCount;
                }
            }

            valid = allowAllNull ? nonNullCount <= 1 : nonNullCount == 1;
        }
        catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            // The method that this overrides doesn't throw these exceptions so it needs to throw a built in java
            // exception if there is a problem.
            final UnsupportedOperationException newException = new UnsupportedOperationException();
            newException.initCause(exception);
            throw newException;
        }

        return valid;
    }
}
