package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;
import com.baesystems.ai.lr.dto.LinkResource;

public class CertificateFlagServiceDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 1242883591361675776L;

    @Size(message = "invalid length", max = 10)
    private String serviceCode;

    private LinkResource certificateTemplate;

    private LinkResource flagAdministration;

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public LinkResource getCertificateTemplate()
    {
        return certificateTemplate;
    }

    public void setCertificateTemplate(final LinkResource certificateTemplate)
    {
        this.certificateTemplate = certificateTemplate;
    }

    public LinkResource getFlagAdministration()
    {
        return flagAdministration;
    }

    public void setFlagAdministration(final LinkResource flagAdministration)
    {
        this.flagAdministration = flagAdministration;
    }
}
