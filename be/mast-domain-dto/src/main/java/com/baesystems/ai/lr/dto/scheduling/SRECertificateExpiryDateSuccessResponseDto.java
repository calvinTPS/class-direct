package com.baesystems.ai.lr.dto.scheduling;

import java.util.Date;

public class SRECertificateExpiryDateSuccessResponseDto extends SREBaseResponseDto
{
    private static final long serialVersionUID = 4279276959819703257L;

    private Long creditedServiceId;
    private Date expiryDate;

    public Long getCreditedServiceId()
    {
        return this.creditedServiceId;
    }

    public void setCreditedServiceId(final Long creditedServiceId)
    {
        this.creditedServiceId = creditedServiceId;
    }

    public Date getExpiryDate()
    {
        return this.expiryDate;
    }

    public void setExpiryDate(final Date expiryDate)
    {
        this.expiryDate = expiryDate;
    }
}
