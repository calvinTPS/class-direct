package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class ReferenceDataVersionDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -7977194839937596657L;

    private Long version;

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(final Long version)
    {
        this.version = version;
    }
}
