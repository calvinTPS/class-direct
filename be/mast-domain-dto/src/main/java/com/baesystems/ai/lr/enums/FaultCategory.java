package com.baesystems.ai.lr.enums;

/*
 * MAST_Ref_FaultCategory contains 48 rows; this is partial enum, containing IDs for parents only
 * Add other values if required
 */
public enum FaultCategory
{
    HULL(2L),
    MACHINERY(4L);

    private final Long value;

    FaultCategory(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
