package com.baesystems.ai.lr.dto;

import com.baesystems.ai.lr.dto.annotations.ValidId;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@ValidId
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkResource extends BaseDto
{
    private static final long serialVersionUID = 1914884139860649971L;

    public LinkResource()
    {
        super();
    }

    public LinkResource(final Long id)
    {
        this();
        this.setId(id);
    }
}
