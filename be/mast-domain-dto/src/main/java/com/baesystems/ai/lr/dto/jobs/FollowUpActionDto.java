package com.baesystems.ai.lr.dto.jobs;

import static com.baesystems.ai.lr.dto.jobs.FollowUpActionDto.ENTITY_NAME;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@EntityName(ENTITY_NAME)
public class FollowUpActionDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = 4158767297567245418L;
    public static final String ENTITY_NAME = "Follow Up Action";

    private String stalenessHash;

    @Size(message = "invalid length", max = 70)
    private String title;

    @Size(message = "invalid length", max = 2000)
    private String description;

    @NotNull
    @Valid
    private LinkResource raisedBy;

    @Valid
    private LinkResource completedBy;

    private Boolean addedManually;

    @NotNull
    @Valid
    private LinkResource followUpActionStatus;

    private Date completionDate;

    private Date targetDate;

    @Size(message = "invalid length", max = 2000)
    private String endorserComment;

    @NotNull
    @Valid
    private LinkResource asset;

    @NotNull
    @Valid
    private LinkResource report;

    @Valid
    private LinkResource survey;

    @JsonProperty("_dmID")
    private String dmInternalLookupID;

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public LinkResource getRaisedBy()
    {
        return this.raisedBy;
    }

    public void setRaisedBy(final LinkResource raisedBy)
    {
        this.raisedBy = raisedBy;
    }

    public LinkResource getCompletedBy()
    {
        return this.completedBy;
    }

    public void setCompletedBy(final LinkResource completedBy)
    {
        this.completedBy = completedBy;
    }

    public Boolean getAddedManually()
    {
        return this.addedManually;
    }

    public void setAddedManually(final Boolean addedManually)
    {
        this.addedManually = addedManually;
    }

    public LinkResource getFollowUpActionStatus()
    {
        return this.followUpActionStatus;
    }

    public void setFollowUpActionStatus(final LinkResource followUpActionStatus)
    {
        this.followUpActionStatus = followUpActionStatus;
    }

    public Date getCompletionDate()
    {
        return this.completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public Date getTargetDate()
    {
        return this.targetDate;
    }

    public void setTargetDate(final Date targetDate)
    {
        this.targetDate = targetDate;
    }

    public String getEndorserComment()
    {
        return this.endorserComment;
    }

    public void setEndorserComment(final String endorserComment)
    {
        this.endorserComment = endorserComment;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getReport()
    {
        return this.report;
    }

    public void setReport(final LinkResource report)
    {
        this.report = report;
    }

    public LinkResource getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final LinkResource survey)
    {
        this.survey = survey;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "FollowUpActionDto [followUpActionStatus=" + getIdOrNull(this.followUpActionStatus)
                + ", getTitle()=" + this.getTitle()
                + ", getDescription()=" + this.getDescription()
                + ", getRaisedBy()=" + this.getIdOrNull(this.getRaisedBy())
                + ", getCompletedBy()=" + this.getIdOrNull(this.getCompletedBy())
                + ", getAddedManually()=" + this.getAddedManually()
                + ", getFollowUpActionStatus(=)" + this.getIdOrNull(this.getFollowUpActionStatus())
                + ", getCompletionDate()=" + this.getStringRepresentationOfDate(this.getCompletionDate())
                + ", getTargetDate()=" + this.getStringRepresentationOfDate(this.getTargetDate())
                + ", getEndorserComment()=" + this.getEndorserComment()
                + ", getAsset()=" + this.getIdOrNull(this.getAsset())
                + ", getReport()=" + this.getIdOrNull(this.getReport())
                + ", getSurvey()=" + this.getIdOrNull(this.getSurvey())
                + ", getId()=" + this.getId() + "]";

    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }

    public String getDmInternalLookupID()
    {
        return this.dmInternalLookupID;
    }

    public void setDmInternalLookupID(final String dmInternalLookupID)
    {
        this.dmInternalLookupID = dmInternalLookupID;
    }
}
