package com.baesystems.ai.lr.dto.reminders;

import java.io.Serializable;
import java.util.Date;

import com.baesystems.ai.lr.dto.LinkResource;

public class BaseReminderDto implements Serializable
{
    private static final long serialVersionUID = 3470879958428225614L;

    private LinkResource severity;

    private LinkResource dueStatus;

    private Date dueDate;

    public LinkResource getSeverity()
    {
        return this.severity;
    }

    public void setSeverity(final LinkResource severity)
    {
        this.severity = severity;
    }

    public LinkResource getDueStatus()
    {
        return this.dueStatus;
    }

    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }
}
