package com.baesystems.ai.lr.dto.references;

import java.util.List;
import java.util.Map;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class ReferenceDataMapDto
{
    private Map<String, Map<String, List<? extends BaseReferenceDataDto>>> referenceData;

    public Map<String, Map<String, List<? extends BaseReferenceDataDto>>> getReferenceData()
    {
        return referenceData;
    }

    public void setReferenceData(final Map<String, Map<String, List<? extends BaseReferenceDataDto>>> referenceData)
    {
        this.referenceData = referenceData;
    }
}
