package com.baesystems.ai.lr.dto.security;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class RoleDto extends BaseDto
{
    private static final long serialVersionUID = -399783177751836844L;

    @NotNull
    @Size(message = "invalid length", max = 100)
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
