package com.baesystems.ai.lr.dto.reports;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.Dto;

import java.io.Serializable;
import java.util.Map;

public class ReportValidationEntryDto implements Dto, Serializable
{
    private static final long serialVersionUID = 570258852066364689L;

    private LinkResource message;
    private Map<String, String> placeholders;

    public LinkResource getMessage()
    {
        return this.message;
    }

    public void setMessage(final LinkResource message)
    {
        this.message = message;
    }

    public Map<String, String> getPlaceholders()
    {
        return this.placeholders;
    }

    public void setPlaceholders(final Map<String, String> placeholders)
    {
        this.placeholders = placeholders;
    }
}
