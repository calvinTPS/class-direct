package com.baesystems.ai.lr.dto.certificates;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.util.List;

public class CertificateActionPageResourceDto extends BasePageResource<CertificateActionDto>
{
    private List<CertificateActionDto> content;

    @Override public List<CertificateActionDto> getContent()
    {
        return this.content;
    }

    @Override public void setContent(final List<CertificateActionDto> content)
    {
        this.content = content;
    }
}
