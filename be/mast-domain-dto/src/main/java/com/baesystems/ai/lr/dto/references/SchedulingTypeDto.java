package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;


public class SchedulingTypeDto extends ReferenceDataDto
{

    private static final long serialVersionUID = -2656221543735111912L;

    private LinkResource associatedSchedulingType;

    private LinkResource schedulingDueType;

    public LinkResource getAssociatedSchedulingType()
    {
        return associatedSchedulingType;
    }

    public void setAssociatedSchedulingType(final LinkResource associatedSchedulingType)
    {
        this.associatedSchedulingType = associatedSchedulingType;
    }

    public LinkResource getSchedulingDueType()
    {
        return schedulingDueType;
    }

    public void setSchedulingDueType(final LinkResource schedulingDueType)
    {
        this.schedulingDueType = schedulingDueType;
    }
}
