package com.baesystems.ai.lr.enums;

import java.util.Arrays;

public enum RelationshipType
{
    IS_PART_OF(1L),
    IS_RELATED_TO(9L);

    private final Long value;

    RelationshipType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    /**
     * Returns the enum the matches the passed id, or IS_RELATED_TO by default.
     *
     * @param id
     * @return
     */
    public static RelationshipType getTypeForId(final Long id)
    {
        return Arrays
                .stream(RelationshipType.values())
                .filter(relType -> relType.getValue().equals(id))
                .findFirst()
                .orElse(IS_RELATED_TO);
    }
}
