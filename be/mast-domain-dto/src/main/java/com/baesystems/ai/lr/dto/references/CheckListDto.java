package com.baesystems.ai.lr.dto.references;

import java.util.Date;

import com.baesystems.ai.lr.dto.LinkResource;

public class CheckListDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -3191267523021468314L;

    private LinkResource checklistGroup;

    private LinkResource checklistSubgroup;

    private String longDescription;

    private String shortDescription;

    private String referenceCode;

    private Integer displayOrder;

    private Boolean priority;

    private Boolean multiple;

    private Integer businessWeighting;

    private Date lowerBuildDate;

    private Date upperBuildDate;

    private Date lowerKeelDate;

    private Date upperKeelDate;

    private Double lowerDeadweight;

    private Double upperDeadweight;

    public LinkResource getChecklistGroup()
    {
        return checklistGroup;
    }

    public void setChecklistGroup(final LinkResource checklistGroup)
    {
        this.checklistGroup = checklistGroup;
    }

    public LinkResource getChecklistSubgroup()
    {
        return checklistSubgroup;
    }

    public void setChecklistSubgroup(final LinkResource checklistSubgroup)
    {
        this.checklistSubgroup = checklistSubgroup;
    }

    public String getLongDescription()
    {
        return longDescription;
    }

    public void setLongDescription(final String longDescription)
    {
        this.longDescription = longDescription;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setShortDescription(final String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public Boolean getPriority()
    {
        return priority;
    }

    public void setPriority(final Boolean priority)
    {
        this.priority = priority;
    }

    public Boolean getMultiple()
    {
        return multiple;
    }

    public void setMultiple(final Boolean multiple)
    {
        this.multiple = multiple;
    }

    public Integer getBusinessWeighting()
    {
        return businessWeighting;
    }

    public void setBusinessWeighting(final Integer businessWeighting)
    {
        this.businessWeighting = businessWeighting;
    }

    public Date getLowerBuildDate()
    {
        return lowerBuildDate;
    }

    public void setLowerBuildDate(final Date lowerBuildDate)
    {
        this.lowerBuildDate = lowerBuildDate;
    }

    public Date getUpperBuildDate()
    {
        return upperBuildDate;
    }

    public void setUpperBuildDate(final Date upperBuildDate)
    {
        this.upperBuildDate = upperBuildDate;
    }

    public Date getLowerKeelDate()
    {
        return lowerKeelDate;
    }

    public void setLowerKeelDate(final Date lowerKeelDate)
    {
        this.lowerKeelDate = lowerKeelDate;
    }

    public Date getUpperKeelDate()
    {
        return upperKeelDate;
    }

    public void setUpperKeelDate(final Date upperKeelDate)
    {
        this.upperKeelDate = upperKeelDate;
    }

    public Double getLowerDeadweight()
    {
        return lowerDeadweight;
    }

    public void setLowerDeadweight(final Double lowerDeadweight)
    {
        this.lowerDeadweight = lowerDeadweight;
    }

    public Double getUpperDeadweight()
    {
        return upperDeadweight;
    }

    public void setUpperDeadweight(final Double upperDeadweight)
    {
        this.upperDeadweight = upperDeadweight;
    }
}
