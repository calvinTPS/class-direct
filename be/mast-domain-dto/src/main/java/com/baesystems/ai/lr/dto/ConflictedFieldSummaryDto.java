package com.baesystems.ai.lr.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ConflictedFieldSummaryDto implements Serializable
{
    private static final long serialVersionUID = 15395538990769872L;

    // nulls to assign as defaults for final fields in constructors to avoid pmd errors.
    private static final Object NULL = null;

    private static final List<ConflictedFieldSummaryDto> NULL_LIST = null;

    private final Object inputParentObject;

    private String path;

    private String fieldName;

    private final Long parentObjectId;

    private final Long parentObjectVersionId;

    private final String existingObjectOwnerId;

    private Object existingValue;

    private Object inputValue;

    private String existingValueDisplayName;

    private String inputValueDisplayName;

    private List<ConflictedFieldSummaryDto> conflicts;

    public ConflictedFieldSummaryDto(final Object inputParentObject, final String fieldName, final Long parentObjectId,
            final Long parentObjectVersionId, final String existingObjectOwnerId, final Object existingValue, final Object inputValue)
    {
        this.inputParentObject = inputParentObject;
        this.fieldName = fieldName;
        this.parentObjectId = parentObjectId;
        this.parentObjectVersionId = parentObjectVersionId;
        this.existingObjectOwnerId = existingObjectOwnerId;
        this.existingValue = existingValue;
        this.inputValue = inputValue;
        this.conflicts = NULL_LIST;
    }

    public ConflictedFieldSummaryDto(final String fieldName, final Long parentObjectId, final Long parentObjectVersionId,
            final String existingObjectOwnerId, final Object existingValue, final Object inputValue)
    {
        this(NULL, fieldName, parentObjectId, parentObjectVersionId, existingObjectOwnerId, existingValue, inputValue);
    }

    public ConflictedFieldSummaryDto(final String fieldName, final Long parentObjectId, final Long parentObjectVersionId,
            final String existingObjectOwnerId, final List<ConflictedFieldSummaryDto> conflicts)
    {
        this.inputParentObject = NULL;
        this.fieldName = fieldName;
        this.parentObjectId = parentObjectId;
        this.parentObjectVersionId = parentObjectVersionId;
        this.existingObjectOwnerId = existingObjectOwnerId;
        this.existingValue = NULL;
        this.inputValue = NULL;
        this.conflicts = conflicts;
    }

    public void initialisePath()
    {
        final StringBuilder stringBuilder = new StringBuilder();

        if (this.fieldName != null && !this.fieldName.isEmpty())
        {
            stringBuilder.append(this.fieldName);
            if (this.parentObjectId != null)
            {
                stringBuilder.append('[')
                        .append(this.parentObjectId)
                        .append(']');
            }
        }

        this.path = stringBuilder.toString();
    }

    public void prependPath(final String inputFieldName)
    {
        final StringBuilder stringBuilder = new StringBuilder();

        if (inputFieldName != null && !inputFieldName.isEmpty())
        {
            stringBuilder.append(inputFieldName);
        }
        if (this.path != null && !this.path.isEmpty())
        {
            if (stringBuilder.length() > 0)
            {
                stringBuilder.append('.');
            }
            stringBuilder.append(this.path);
        }

        this.path = stringBuilder.toString();
    }

    public String getPath()
    {
        return this.path;
    }

    public void setPath(final String path)
    {
        this.path = path;
    }

    public String getFieldName()
    {
        return this.fieldName;
    }

    public void setFieldName(final String fieldName)
    {
        this.fieldName = fieldName;
    }

    public Object getExistingValue()
    {
        return this.existingValue;
    }

    public void setExistingValue(final Object existingValue)
    {
        this.existingValue = existingValue;
    }

    public Object getInputValue()
    {
        return this.inputValue;
    }

    public void setInputValue(final Object inputValue)
    {
        this.inputValue = inputValue;
    }

    public String getExistingValueDisplayName()
    {
        return this.existingValueDisplayName;
    }

    public void setExistingValueDisplayName(final String existingValueDisplayName)
    {
        this.existingValueDisplayName = existingValueDisplayName;
    }

    public String getInputValueDisplayName()
    {
        return this.inputValueDisplayName;
    }

    public void setInputValueDisplayName(final String inputValueDisplayName)
    {
        this.inputValueDisplayName = inputValueDisplayName;
    }

    @JsonIgnore
    public List<ConflictedFieldSummaryDto> getConflicts()
    {
        return this.conflicts;
    }

    @JsonIgnore
    public void setConflicts(final List<ConflictedFieldSummaryDto> conflicts)
    {
        this.conflicts = conflicts;
    }

    @JsonIgnore
    public Object getInputParentObject()
    {
        return this.inputParentObject;
    }

    public Long getParentObjectId()
    {
        return this.parentObjectId;
    }

    public Long getParentObjectVersionId()
    {
        return this.parentObjectVersionId;
    }

    public String getExistingObjectOwnerId()
    {
        return this.existingObjectOwnerId;
    }
}
