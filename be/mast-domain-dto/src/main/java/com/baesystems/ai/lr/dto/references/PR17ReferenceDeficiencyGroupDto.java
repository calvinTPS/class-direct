package com.baesystems.ai.lr.dto.references;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PR17ReferenceDeficiencyGroupDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 8375938761141599208L;

    @NotNull
    private Long displayOrder;

    private List<PR17ReferenceDeficiencyDto> deficiencies;

    public Long getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    @JsonIgnore
    public List<PR17ReferenceDeficiencyDto> getDeficiencies()
    {
        return this.deficiencies;
    }

    @JsonIgnore
    public void setDeficiencies(final List<PR17ReferenceDeficiencyDto> deficiencies)
    {
        this.deficiencies = deficiencies;
    }
}
