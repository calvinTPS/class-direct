package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.annotations.OverrideDisplayName;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@IgnoreIdsInConflicts
public class DefectDefectValueDto extends AuditedDto implements StaleObject
{
    private static final long serialVersionUID = 5757240290554132918L;

    @NotNull
    @SubIdNotNull
    @Valid
    @ConflictAware(
            dynamicFieldDisplayName = @DynamicDisplayName(
                    idFieldOnTarget = "values.id",
                    targetType = ReferenceDataSubSet.DEFECT_DETAILS,
                    overrideNames = {@OverrideDisplayName(referenceDataName = "Onboard Location", displayName = "In Way of")}),
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.DEFECT_DETAILS,
                    flattenTarget = "values"))
    private DefectValueDto defectValue;

    @ConflictAware(
            dynamicFieldDisplayName = @DynamicDisplayName(idFieldOnSource = "defectValue.id",
                    idFieldOnTarget = "values.id",
                    targetType = ReferenceDataSubSet.DEFECT_DETAILS,
                    format = "%s Other Details"))
    @Size(message = "invalid length", max = 50)
    private String otherDetails;

    @Valid
    private LinkResource parent;

    private String stalenessHash;

    public DefectValueDto getDefectValue()
    {
        return this.defectValue;
    }

    public void setDefectValue(final DefectValueDto defectValue)
    {
        this.defectValue = defectValue;
    }

    public String getOtherDetails()
    {
        return this.otherDetails;
    }

    public void setOtherDetails(final String otherDetails)
    {
        this.otherDetails = otherDetails;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;

    }

    /**
     * This is needed as defect values are saved with the defect, but parts of this object are important on the defect
     * so if they have changed then we should consider the defect as changed as well.
     */
    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "DefectDefectValueDto [otherDetails=" + getOtherDetails()
                + "defectValue=" + getIdOrNull(getDefectValue())
                + "]";
    }
}
