package com.baesystems.ai.lr.dto.ihs;

import java.util.Date;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

/*
 * IhsOvnaDto contains former asset details.
 */
public class IhsOvnaDto extends IhsBaseDto
{

    private static final long serialVersionUID = 4911347127088689195L;

    private Integer seqNo;

    private Integer g01Efd;

    private Character g01Cc;

    private String g01Srce;

    private Character g01Ver;

    private Character g01HullSec;

    private Character g01LnchdAs;

    private Character g01CompAs;

    private Character g01OrigName;

    // Former Asset name.
    private String g01Name;

    private Integer g01RbSeq;

    private String lrnoName;

    private Integer grtL;

    private Date dob;

    private Double shipTyp;

    private Integer invGrt;

    private Character currNInd;

    private Character origNInd;

    private String flagCode;

    private Character shpActSt;

    private String keyName;

    public Integer getSeqNo()
    {
        return seqNo;
    }

    public void setSeqNo(final Integer seqNo)
    {
        this.seqNo = seqNo;
    }

    public Integer getG01Efd()
    {
        return g01Efd;
    }

    public void setG01Efd(final Integer g01Efd)
    {
        this.g01Efd = g01Efd;
    }

    public Character getG01Cc()
    {
        return g01Cc;
    }

    public void setG01Cc(final Character g01Cc)
    {
        this.g01Cc = g01Cc;
    }

    public String getG01Srce()
    {
        return g01Srce;
    }

    public void setG01Srce(final String g01Srce)
    {
        this.g01Srce = g01Srce;
    }

    public Character getG01Ver()
    {
        return g01Ver;
    }

    public void setG01Ver(final Character g01Ver)
    {
        this.g01Ver = g01Ver;
    }

    public Character getG01HullSec()
    {
        return g01HullSec;
    }

    public void setG01HullSec(final Character g01HullSec)
    {
        this.g01HullSec = g01HullSec;
    }

    public Character getG01LnchdAs()
    {
        return g01LnchdAs;
    }

    public void setG01LnchdAs(final Character g01LnchdAs)
    {
        this.g01LnchdAs = g01LnchdAs;
    }

    public Character getG01CompAs()
    {
        return g01CompAs;
    }

    public void setG01CompAs(final Character g01CompAs)
    {
        this.g01CompAs = g01CompAs;
    }

    public Character getG01OrigName()
    {
        return g01OrigName;
    }

    public void setG01OrigName(final Character g01OrigName)
    {
        this.g01OrigName = g01OrigName;
    }

    /*
     * Returns former name of the Asset.
     */
    public String getG01Name()
    {
        return g01Name;
    }

    public void setG01Name(final String g01Name)
    {
        this.g01Name = g01Name;
    }

    public Integer getG01RbSeq()
    {
        return g01RbSeq;
    }

    public void setG01RbSeq(final Integer g01RbSeq)
    {
        this.g01RbSeq = g01RbSeq;
    }

    public String getLrnoName()
    {
        return lrnoName;
    }

    public void setLrnoName(final String lrnoName)
    {
        this.lrnoName = lrnoName;
    }

    public Integer getGrtL()
    {
        return grtL;
    }

    public void setGrtL(final Integer grtL)
    {
        this.grtL = grtL;
    }

    public Date getDob()
    {
        return dob;
    }

    public void setDob(final Date dob)
    {
        this.dob = dob;
    }

    public Double getShipTyp()
    {
        return shipTyp;
    }

    public void setShipTyp(final Double shipTyp)
    {
        this.shipTyp = shipTyp;
    }

    public Integer getInvGrt()
    {
        return invGrt;
    }

    public void setInvGrt(final Integer invGrt)
    {
        this.invGrt = invGrt;
    }

    public Character getCurrNInd()
    {
        return currNInd;
    }

    public void setCurrNInd(final Character currNInd)
    {
        this.currNInd = currNInd;
    }

    public Character getOrigNInd()
    {
        return origNInd;
    }

    public void setOrigNInd(final Character origNInd)
    {
        this.origNInd = origNInd;
    }

    public String getFlagCode()
    {
        return flagCode;
    }

    public void setFlagCode(final String flagCode)
    {
        this.flagCode = flagCode;
    }

    public Character getShpActSt()
    {
        return shpActSt;
    }

    public void setShpActSt(final Character shpActSt)
    {
        this.shpActSt = shpActSt;
    }

    public String getKeyName()
    {
        return keyName;
    }

    public void setKeyName(final String keyName)
    {
        this.keyName = keyName;
    }

}
