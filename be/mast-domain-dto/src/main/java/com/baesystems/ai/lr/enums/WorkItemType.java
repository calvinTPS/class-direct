package com.baesystems.ai.lr.enums;

public enum WorkItemType
{
    TASK(1L),
    CHECKLIST(2L);

    private final Long value;

    WorkItemType(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return this.value;
    }
}
