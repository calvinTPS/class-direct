package com.baesystems.ai.lr.constants;

public interface HttpStatus
{
    int UNPROCESSABLE_ENTITY = 422;
    int BAD_REQUEST = 400;
    int CONFLICT = 409;
    int INTERNAL_SERVER_ERROR = 500;
    int NOT_FOUND = 404;
    int UNAUTHORIZED = 401;
}
