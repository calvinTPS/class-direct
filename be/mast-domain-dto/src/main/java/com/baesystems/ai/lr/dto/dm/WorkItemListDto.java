package com.baesystems.ai.lr.dto.dm;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

public class WorkItemListDto implements Serializable
{
    private static final long serialVersionUID = -1312451676513164874L;

    @Valid
    private List<WorkItemLightDto> workItems;

    public List<WorkItemLightDto> getWorkItems()
    {
        return workItems;
    }

    public void setWorkItems(final List<WorkItemLightDto> workItems)
    {
        this.workItems = workItems;
    }

}
