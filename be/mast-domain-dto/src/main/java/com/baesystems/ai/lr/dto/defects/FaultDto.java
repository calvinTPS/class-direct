package com.baesystems.ai.lr.dto.defects;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.dto.jobs.JobTracker;
import com.baesystems.ai.lr.dto.references.FaultCategoryDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;

public class FaultDto extends AuditedDto implements JobTracker, HashedParentReportingDto
{
    private static final long serialVersionUID = 137536376723295889L;

    @ConflictAware
    private Integer sequenceNumber;

    @ConflictAware(staticFieldDisplayName = "Occurred")
    private Date incidentDate;

    @Size(message = "invalid length", max = 2000)
    @ConflictAware
    private String incidentDescription;

    @NotNull
    @Size(message = "invalid length", max = 50)
    @ConflictAware
    private String title;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CONFIDENTIALITY_TYPES))
    private LinkResource confidentialityType;

    @Valid
    @ConflictAware
    private LinkResource job;

    @Valid
    @ConflictAware
    private List<JobFaultDto> jobs;

    @Valid
    @ConflictAware
    private LinkResource asset;

    private Integer fixCount;

    private Integer courseOfActionCount;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.DEFECT_CATEGORIES))
    private FaultCategoryDto category;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.DEFECT_STATUSES))
    private LinkResource status;

    @ConflictAware
    private LinkResource parent;

    @Valid
    private LinkResource raisedOnJob;

    @Valid
    private LinkResource closedOnJob;

    @ConflictAware
    private Date updatedOn;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LinkResource updatedOnBehalfOf;

    private String parentHash;

    private Date sourceJobLastVisitDate;

    private Date sourceJobCreationDateTime;

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getIncidentDate()
    {
        return this.incidentDate;
    }

    public void setIncidentDate(final Date incidentDate)
    {
        this.incidentDate = incidentDate;
    }

    public String getIncidentDescription()
    {
        return this.incidentDescription;
    }

    public void setIncidentDescription(final String incidentDescription)
    {
        this.incidentDescription = incidentDescription;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public Integer getCourseOfActionCount()
    {
        return this.courseOfActionCount;
    }

    public void setCourseOfActionCount(final Integer courseOfActionCount)
    {
        this.courseOfActionCount = courseOfActionCount;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public FaultCategoryDto getCategory()
    {
        return this.category;
    }

    public void setCategory(final FaultCategoryDto category)
    {
        this.category = category;
    }

    public LinkResource getStatus()
    {
        return this.status;
    }

    public void setStatus(final LinkResource status)
    {
        this.status = status;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public Integer getFixCount()
    {
        return this.fixCount;
    }

    public void setFixCount(final Integer fixCount)
    {
        this.fixCount = fixCount;
    }

    @Override
    public LinkResource getRaisedOnJob()
    {
        return this.raisedOnJob;
    }

    @Override
    public void setRaisedOnJob(final LinkResource raisedOnJob)
    {
        this.raisedOnJob = raisedOnJob;
    }

    @Override
    public LinkResource getClosedOnJob()
    {
        return this.closedOnJob;
    }

    @Override
    public void setClosedOnJob(final LinkResource closedOnJob)
    {
        this.closedOnJob = closedOnJob;
    }

    public Date getUpdatedOn()
    {
        return this.updatedOn;
    }

    public void setUpdatedOn(final Date updatedOn)
    {
        this.updatedOn = updatedOn;
    }

    public LinkResource getUpdatedOnBehalfOf()
    {
        return this.updatedOnBehalfOf;
    }

    public void setUpdatedOnBehalfOf(final LinkResource updatedOnBehalfOf)
    {
        this.updatedOnBehalfOf = updatedOnBehalfOf;
    }

    public List<JobFaultDto> getJobs()
    {
        return this.jobs;
    }

    public void setJobs(final List<JobFaultDto> jobs)
    {
        this.jobs = jobs;
    }

    @Override
    public String getParentHash()
    {
        return this.parentHash;
    }

    public void setParentHash(final String parentHash)
    {
        this.parentHash = parentHash;
    }

    @Override
    public Date getSourceJobLastVisitDate()
    {
        return this.sourceJobLastVisitDate;
    }

    @Override
    public void setSourceJobLastVisitDate(final Date sourceJobLastVisitDate)
    {
        this.sourceJobLastVisitDate = sourceJobLastVisitDate;
    }

    @Override
    public Date getSourceJobCreationDateTime()
    {
        return this.sourceJobCreationDateTime;
    }

    @Override
    public void setSourceJobCreationDateTime(final Date sourceJobCreationDateTime)
    {
        this.sourceJobCreationDateTime = sourceJobCreationDateTime;
    }
}
