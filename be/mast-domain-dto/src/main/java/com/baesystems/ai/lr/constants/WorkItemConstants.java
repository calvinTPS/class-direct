package com.baesystems.ai.lr.constants;

public interface WorkItemConstants
{
    // This cutoff is specified in LRD-10820 AC2.
    Integer PMS_CREDIT_CUTOFF_MONTHS = 18;
}
