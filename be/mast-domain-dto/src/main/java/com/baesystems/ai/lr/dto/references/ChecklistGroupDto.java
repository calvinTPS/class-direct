package com.baesystems.ai.lr.dto.references;

import java.util.List;

public class ChecklistGroupDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 7502656116084398041L;

    private List<ChecklistSubgroupDto> subGroups;

    private Integer displayOrder;

    public List<ChecklistSubgroupDto> getSubGroups()
    {
        return subGroups;
    }

    public void setSubGroups(final List<ChecklistSubgroupDto> subgroups)
    {
        this.subGroups = subgroups;
    }

    public Integer getDisplayOrder()
    {
      return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
      this.displayOrder = displayOrder;
    }
}
