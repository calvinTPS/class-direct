package com.baesystems.ai.lr.dto;

import java.util.Date;

public interface UpdateableJobBundleEntity extends JobBundleEntity
{
    String getUpdatedBy();

    void setUpdatedBy(String updatedBy);

    Date getUpdatedDate();

    boolean isDeleted();

    void setDeleted(boolean deleted);
}
