package com.baesystems.ai.lr.dto.services;

import java.util.List;

public class SurveyWithTaskListDto extends SurveyDto
{
    private static final long serialVersionUID = 408838940223883090L;

    private List<Long> taskIds;

    public List<Long> getTaskIds()
    {
        return this.taskIds;
    }

    public void setTaskIds(final List<Long> taskIds)
    {
        this.taskIds = taskIds;
    }
}
