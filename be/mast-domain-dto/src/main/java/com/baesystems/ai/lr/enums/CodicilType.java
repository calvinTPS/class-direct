package com.baesystems.ai.lr.enums;

public enum CodicilType
{
    // SD (4) and SDD (7) removed as these will be treated as types of fault, not codicil.
    COC(1L),
    AI(2L),
    AN(3L),
    SF(5L),
    NCN(6L),
    OBS(8L);

    private final Long value;

    CodicilType(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return this.value;
    }

    public boolean imposedDateAltersStatus()
    {
        return AN.equals(this) || AI.equals(this);
    }
}
