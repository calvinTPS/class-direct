package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class TeamDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -5439202041123488024L;

    private String name;

    private LinkResource officeRole;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public LinkResource getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkResource officeRole)
    {
        this.officeRole = officeRole;
    }
}
