package com.baesystems.ai.lr.dto.reminders;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.Dto;

public class AssetReviewDateDto implements Dto, Serializable
{
    private static final long serialVersionUID = -6520691741609744200L;

    @NotNull
    private Date reviewDate;

    public Date getReviewDate()
    {
        return reviewDate;
    }

    public void setReviewDate(final Date reviewDate)
    {
        this.reviewDate = reviewDate;
    }
}
