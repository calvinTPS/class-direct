package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * The purpose of this class is to notify the reference data loading mechanism that
 * a field on the DTO doesn't map directly to a field on the DO by name. The normal
 * assumption is the DTO <-> DO mapping will be on field name. This specifies that the
 * field(s) in this annotation are those on the DO which we should be checking when
 * working out if any sub-entities have changed.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface ReferenceDataMappingColumnOverride
{
    String[] fields();
}
