package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ReadingCategoryDto extends ImmutableReferenceDataDto
{

    private static final long serialVersionUID = 6211679122149907129L;

    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

}
