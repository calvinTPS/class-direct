package com.baesystems.ai.lr.dto.codicils;

import static com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto.ENTITY_NAME;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.AdditionalNullChecks;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
@AdditionalNullChecks(fields = {"dueDate"})
public class StatutoryFindingDto extends CodicilDto implements DueStatusUpdateableDto, HashedParentReportingDto
{
    public static final String ENTITY_NAME = "Statutory Finding";
    private static final long serialVersionUID = 548468746258537151L;

    private String stalenessHash;

    @Valid
    @ConflictAware
    private LinkResource asset;

    @Valid
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_DEFICIENCY)
    private LinkResource defect;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_CATEGORIES))
    private LinkResource category;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CONFIDENTIALITY_TYPES))
    private LinkResource confidentialityType;

    @ConflictAware
    private Integer sequenceNumber;

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public LinkResource getCategory()
    {
        return this.category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        // Note: Deliberately don't include dueStatus in the hash as this data is derived from other fields.

        return "StatutoryFindingDto [imposedDate=" + this.getStringRepresentationOfDate(this.getImposedDate())
                + ", dueDate=" + this.getStringRepresentationOfDate(this.getDueDate())
                + ", status=" + this.getIdOrNull(this.getStatus())
                + ", getId()=" + this.getId()
                + ", title=" + this.getTitle()
                + ", asset=" + this.getAsset()
                + ", job=" + this.getIdOrNull(this.getJob())
                + ", jobScoptConfirmed=" + this.getJobScopeConfirmed()
                + ", title=" + this.getTitle()
                + ", description=" + this.getDescription()
                + ", narrative=" + this.getNarrative()
                + ", sequenceNumber=" + this.getSequenceNumber()
                + ", defect=" + this.getIdOrNull(this.defect)
                + ", category=" + this.getIdOrNull(this.category)
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", confidentialityType=" + getIdOrNull(getConfidentialityType())
                + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
