package com.baesystems.ai.lr.dto.jobs;

import com.baesystems.ai.lr.dto.LinkResource;

public interface JobTracker
{
    LinkResource getRaisedOnJob();

    void setRaisedOnJob(LinkResource raisedOnJob);

    LinkResource getClosedOnJob();

    void setClosedOnJob(LinkResource closedOnJob);
}
