package com.baesystems.ai.lr.dto.assets;

import static com.baesystems.ai.lr.dto.assets.AssetLightDto.ENTITY_NAME;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.Versioned;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.RegisterBookField;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@EntityName(ENTITY_NAME)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetLightDto extends AssetTileDto implements UpdateableJobBundleEntity, Versioned, HashedParentReportingDto
{
    private static final long serialVersionUID = -4552907118794216757L;
    public static final String ENTITY_NAME = "Asset";

    @NotNull
    @ConflictAware
    private Boolean isLead;

    @ConflictAware
    @Size(message = "invalid length", max = 7, min = 7)
    private String leadImo;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CLASS_MAINTENANCE_STATUSES))
    private LinkResource classMaintenanceStatus;

    @Size(message = "invalid length", max = 1000)
    @ConflictAware
    private String classNotation;

    @Size(message = "invalid length", max = 1000)
    @ConflictAware
    private String machineryClassNotation;

    @ConflictAware
    private Date estimatedBuildDate;

    @ConflictAware
    private Date keelLayingDate;

    @Size(message = "invalid length", max = 100)
    @ConflictAware
    private String builder;

    @Size(message = "invalid length", max = 15)
    @ConflictAware
    private String yardNumber;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.RULE_SETS))
    private LinkResource ruleSet;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.RULE_SETS))
    private LinkResource previousRuleSet;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.SERVICE_RULESETS))
    private LinkResource productRuleSet;

    @ConflictAware
    private Date harmonisationDate;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.PORTS_OF_REGISTRY))
    @RegisterBookField
    private LinkResource registeredPort;

    @ConflictAware
    private Date dateOfRegistry;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.LIFECYCLE_STATUSES))
    private LinkResource assetLifecycleStatus;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.IACS_SOCIETIES))
    private LinkResource coClassificationSociety;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(idFieldOnSource = "customers.customer.id", targetType = ReferenceDataSubSet.PARTY))
    @RegisterBookField(ignoreFields = {"id"})
    private List<CustomerLinkDto> customers;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(idFieldOnSource = "offices.office.id", targetType = ReferenceDataSubSet.OFFICE))
    private List<OfficeLinkDto> offices;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.COUNTRIES))
    private LinkResource countryOfBuild;

    @Size(message = "invalid length", max = 500)
    @ConflictAware
    private String descriptiveNote;

    @Size(message = "invalid length", max = 100)
    @ConflictAware
    @RegisterBookField
    private String callSign;

    @NotNull
    @ConflictAware
    private Integer hullIndicator;

    @Valid
    private LinkResource classDepartment;

    @Valid
    @ConflictAware
    private LinkResource linkedAsset;

    @ConflictAware
    private Date effectiveDate;

    @Min(value = 0)
    @Max(value = 9999999)
    @ConflictAware
    private Double deadWeight;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.SERVICE_RESTRICTION))
    private LinkResource serviceRestriction;

    @ConflictAware
    private Long parentPublishVersionId;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.SERVICE_RULESETS))
    private LinkResource serviceRuleSet;

    @ConflictAware
    private Boolean pmsApplicable;

    private List<BaseAssetDto> allVersions;

    @Size(message = "invalid length", max = 20)
    @ConflictAware
    private String source;

    @Size(message = "invalid length", max = 9)
    @ConflictAware
    @RegisterBookField
    private String mmsiNumber;

    @ConflictAware
    private Date newConstructionContractDate;

    @ConflictAware
    private Integer cfoProvenanceIndicator;

    @Size(message = "invalid length", max = 30)
    @ConflictAware
    @RegisterBookField
    private String officialNumber;

    @ConflictAware
    private Float breadth;

    @ConflictAware
    private Double loa;

    @ConflictAware
    private Double depth;

    private Date sourceJobLastVisitDate;

    private Date sourceJobCreationDateTime;

    @RegisterBookField
    private Integer partyUpdatedCounter;

    private String parentHash;

    @JsonIgnore
    private String updatedBy;

    @JsonIgnore
    private Date updatedDate;

    private boolean deleted;

    public Boolean getIsLead()
    {
        return this.isLead;
    }

    public void setIsLead(final Boolean isLead)
    {
        this.isLead = isLead;
    }

    public String getLeadImo()
    {
        return this.leadImo;
    }

    public void setLeadImo(final String leadImo)
    {
        this.leadImo = leadImo;
    }

    public String getClassNotation()
    {
        return this.classNotation;
    }

    public void setClassNotation(final String classNotation)
    {
        this.classNotation = classNotation;
    }

    public Date getEstimatedBuildDate()
    {
        return this.estimatedBuildDate;
    }

    public void setEstimatedBuildDate(final Date estimatedBuildDate)
    {
        this.estimatedBuildDate = estimatedBuildDate;
    }

    public Date getKeelLayingDate()
    {
        return this.keelLayingDate;
    }

    public void setKeelLayingDate(final Date keelLayingDate)
    {
        this.keelLayingDate = keelLayingDate;
    }

    public final String getBuilder()
    {
        return this.builder;
    }

    public final void setBuilder(final String builder)
    {
        this.builder = builder;
    }

    public String getYardNumber()
    {
        return this.yardNumber;
    }

    public void setYardNumber(final String yardNumber)
    {
        this.yardNumber = yardNumber;
    }

    public LinkResource getRuleSet()
    {
        return this.ruleSet;
    }

    public void setRuleSet(final LinkResource ruleSet)
    {
        this.ruleSet = ruleSet;
    }

    public LinkResource getPreviousRuleSet()
    {
        return this.previousRuleSet;
    }

    public void setPreviousRuleSet(final LinkResource previousRuleSet)
    {
        this.previousRuleSet = previousRuleSet;
    }

    public LinkResource getProductRuleSet()
    {
        return this.productRuleSet;
    }

    public void setProductRuleSet(final LinkResource productRuleSet)
    {
        this.productRuleSet = productRuleSet;
    }

    public Date getHarmonisationDate()
    {
        return this.harmonisationDate;
    }

    public void setHarmonisationDate(final Date harmonisationDate)
    {
        this.harmonisationDate = harmonisationDate;
    }

    public LinkResource getRegisteredPort()
    {
        return this.registeredPort;
    }

    public void setRegisteredPort(final LinkResource registeredPort)
    {
        this.registeredPort = registeredPort;
    }

    public Date getDateOfRegistry()
    {
        return this.dateOfRegistry;
    }

    public void setDateOfRegistry(final Date dateOfRegistry)
    {
        this.dateOfRegistry = dateOfRegistry;
    }

    public LinkResource getAssetLifecycleStatus()
    {
        return this.assetLifecycleStatus;
    }

    public void setAssetLifecycleStatus(final LinkResource assetLifecycleStatus)
    {
        this.assetLifecycleStatus = assetLifecycleStatus;
    }

    public LinkResource getCoClassificationSociety()
    {
        return this.coClassificationSociety;
    }

    public void setCoClassificationSociety(final LinkResource coClassificationSociety)
    {
        this.coClassificationSociety = coClassificationSociety;
    }

    public LinkResource getClassMaintenanceStatus()
    {
        return this.classMaintenanceStatus;
    }

    public void setClassMaintenanceStatus(final LinkResource classMaintenanceStatus)
    {
        this.classMaintenanceStatus = classMaintenanceStatus;
    }

    public List<CustomerLinkDto> getCustomers()
    {
        return this.customers;
    }

    public void setCustomers(final List<CustomerLinkDto> customers)
    {
        this.customers = customers;
    }

    public List<OfficeLinkDto> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<OfficeLinkDto> offices)
    {
        this.offices = offices;
    }

    public LinkResource getCountryOfBuild()
    {
        return this.countryOfBuild;
    }

    public void setCountryOfBuild(final LinkResource countryOfBuild)
    {
        this.countryOfBuild = countryOfBuild;
    }

    public String getDescriptiveNote()
    {
        return this.descriptiveNote;
    }

    public void setDescriptiveNote(final String description)
    {
        this.descriptiveNote = description;
    }

    public String getCallSign()
    {
        return this.callSign;
    }

    public void setCallSign(final String callSign)
    {
        this.callSign = callSign;
    }

    public String getMachineryClassNotation()
    {
        return this.machineryClassNotation;
    }

    public void setMachineryClassNotation(final String machineryClassNotation)
    {
        this.machineryClassNotation = machineryClassNotation;
    }

    public Integer getHullIndicator()
    {
        return this.hullIndicator;
    }

    public void setHullIndicator(final Integer hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }

    public Long getParentPublishVersionId()
    {
        return this.parentPublishVersionId;
    }

    public void setParentPublishVersionId(final Long parentPublishVersionId)
    {
        this.parentPublishVersionId = parentPublishVersionId;
    }

    public LinkResource getClassDepartment()
    {
        return this.classDepartment;
    }

    public void setClassDepartment(final LinkResource classDepartment)
    {
        this.classDepartment = classDepartment;
    }

    public LinkResource getLinkedAsset()
    {
        return this.linkedAsset;
    }

    public void setLinkedAsset(final LinkResource linkedAsset)
    {
        this.linkedAsset = linkedAsset;
    }

    public Date getEffectiveDate()
    {
        return this.effectiveDate;
    }

    public void setEffectiveDate(final Date effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    public Double getDeadWeight()
    {
        return this.deadWeight;
    }

    public void setDeadWeight(final Double deadWeight)
    {
        this.deadWeight = deadWeight;
    }

    public LinkResource getServiceRestriction()
    {
        return this.serviceRestriction;
    }

    public void setServiceRestriction(final LinkResource serviceRestriction)
    {
        this.serviceRestriction = serviceRestriction;
    }

    public LinkResource getServiceRuleSet()
    {
        return this.serviceRuleSet;
    }

    public void setServiceRuleSet(final LinkResource serviceRuleSet)
    {
        this.serviceRuleSet = serviceRuleSet;
    }

    public Boolean getPmsApplicable()
    {
        return this.pmsApplicable;
    }

    public void setPmsApplicable(final Boolean pmsApplicable)
    {
        this.pmsApplicable = pmsApplicable;
    }

    public String getSource()
    {
        return this.source;
    }

    public void setSource(final String source)
    {
        this.source = source;
    }

    public List<BaseAssetDto> getAllVersions()
    {
        return this.allVersions;
    }

    public void setAllVersions(final List<BaseAssetDto> allVersions)
    {
        this.allVersions = allVersions;
    }

    public String getMmsiNumber()
    {
        return this.mmsiNumber;
    }

    public void setMmsiNumber(final String mmsiNumber)
    {
        this.mmsiNumber = mmsiNumber;
    }

    public Date getNewConstructionContractDate()
    {
        return this.newConstructionContractDate;
    }

    public void setNewConstructionContractDate(final Date newConstructionContractDate)
    {
        this.newConstructionContractDate = newConstructionContractDate;
    }

    public Integer getCfoProvenanceIndicator()
    {
        return this.cfoProvenanceIndicator;
    }

    public void setCfoProvenanceIndicator(final Integer cfoProvenanceIndicator)
    {
        this.cfoProvenanceIndicator = cfoProvenanceIndicator;
    }

    public String getOfficialNumber()
    {
        return this.officialNumber;
    }

    public void setOfficialNumber(final String officialNumber)
    {
        this.officialNumber = officialNumber;
    }

    public Float getBreadth()
    {
        return this.breadth;
    }

    public void setBreadth(final Float breadth)
    {
        this.breadth = breadth;
    }

    public Double getLoa()
    {
        return this.loa;
    }

    public void setLoa(final Double loa)
    {
        this.loa = loa;
    }

    public Double getDepth()
    {
        return this.depth;
    }

    public void setDepth(final Double depth)
    {
        this.depth = depth;
    }

    public Integer getPartyUpdatedCounter()
    {
        return this.partyUpdatedCounter;
    }

    public void setPartyUpdatedCounter(final Integer partyUpdatedCounter)
    {
        this.partyUpdatedCounter = partyUpdatedCounter;
    }

    @Override
    public String getUpdatedBy()
    {
        return this.updatedBy;
    }

    @Override
    public void setUpdatedBy(final String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdatedDate()
    {
        return this.updatedDate;
    }

    @Override
    public Date getSourceJobLastVisitDate()
    {
        return this.sourceJobLastVisitDate;
    }

    @Override
    public void setSourceJobLastVisitDate(final Date sourceJobLastVisitDate)
    {
        this.sourceJobLastVisitDate = sourceJobLastVisitDate;
    }

    @Override
    public Date getSourceJobCreationDateTime()
    {
        return this.sourceJobCreationDateTime;
    }

    @Override
    public void setSourceJobCreationDateTime(final Date sourceJobCreationDateTime)
    {
        this.sourceJobCreationDateTime = sourceJobCreationDateTime;
    }

    @Override
    public String getParentHash()
    {
        return this.parentHash;
    }

    public void setParentHash(final String parentHash)
    {
        this.parentHash = parentHash;
    }

    @Override
    public boolean isDeleted()
    {
        return this.deleted;
    }

    @Override
    public void setDeleted(final boolean deleted)
    {
        this.deleted = deleted;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "AssetLightDto [name=" + super.getName()
                + ", isLead=" + this.isLead
                + ", leadImo=" + this.leadImo
                + ", classMaintenanceStatus=" + this.getIdOrNull(this.classMaintenanceStatus)
                + ", classNotation=" + this.classNotation
                + ", machineryClassNotation=" + this.machineryClassNotation
                + ", buildDate=" + this.getStringRepresentationOfDate(super.getBuildDate())
                + ", estimatedBuildDate=" + this.getStringRepresentationOfDate(this.estimatedBuildDate)
                + ", keelLayingDate=" + this.getStringRepresentationOfDate(this.keelLayingDate)
                + ", grossTonnage=" + super.getGrossTonnage()
                + ", assetType=" + this.getIdOrNull(super.getAssetType())
                + ", assetCategory=" + this.getIdOrNull(super.getAssetCategory())
                + ", classStatus=" + this.getIdOrNull(super.getClassStatus())
                + ", builder=" + this.builder
                + ", yardNumber=" + this.yardNumber
                + ", ihsAsset=" + (super.getIhsAsset() == null ? null : super.getIhsAsset())
                + ", ruleSet=" + this.getIdOrNull(this.ruleSet)
                + ", previousRuleSet=" + this.getIdOrNull(this.previousRuleSet)
                + ", productRuleSet=" + this.getIdOrNull(this.productRuleSet)
                + ", harmonisationDate=" + this.getStringRepresentationOfDate(this.harmonisationDate)
                + ", flagState=" + this.getIdOrNull(super.getFlagState())
                + ", registeredPort=" + this.getIdOrNull(this.registeredPort)
                + ", dateOfRegistry=" + this.getStringRepresentationOfDate(this.dateOfRegistry)
                + ", assetLifecycleStatus=" + this.getIdOrNull(this.assetLifecycleStatus)
                + ", coClassificationSociety=" + this.getIdOrNull(this.coClassificationSociety)
                + ", customers=" + this.getStringRepresentationOfList(this.customers)
                + ", offices=" + this.getStringRepresentationOfList(this.offices)
                + ", countryOfBuild=" + this.getIdOrNull(this.countryOfBuild)
                + ", description=" + this.descriptiveNote
                + ", callSign=" + this.callSign
                + ", hullIndicator=" + this.hullIndicator
                + ", parentPublishVersionId=" + this.parentPublishVersionId
                + ", linkedAsset=" + this.getIdOrNull(this.linkedAsset)
                + ", effectiveDate=" + this.getStringRepresentationOfDate(this.effectiveDate)
                + ", deadWeight=" + this.deadWeight
                + ", serviceRestriction=" + this.getIdOrNull(this.serviceRestriction)
                + ", serviceRuleSet=" + this.getIdOrNull(this.serviceRuleSet)
                + ", classDepartment=" + this.getIdOrNull(this.classDepartment)
                + ", pmsApplicable=" + this.pmsApplicable
                + ", mmsiNumber=" + this.mmsiNumber
                + ", newConstructionContractDate=" + getStringRepresentationOfDate(this.newConstructionContractDate)
                + ", cfoProvenanceIndicator=" + this.cfoProvenanceIndicator
                + ", source=" + this.source
                + ", breadth=" + this.breadth
                + ", loa=" + this.loa
                + ", depth=" + this.depth
                + ", officialNumber=" + this.officialNumber + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
