package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCatalogueDto extends ProductCatalogueLightDto
{
    private static final long serialVersionUID = -2765352806519567604L;

    @Valid
    private LinkResource productGroup;

    @Valid
    private LinkResource productType;

    public LinkResource getProductGroup()
    {
        return productGroup;
    }

    public void setProductGroup(final LinkResource productGroup)
    {
        this.productGroup = productGroup;
    }

    public LinkResource getProductType()
    {
        return productType;
    }

    public void setProductType(final LinkResource productType)
    {
        this.productType = productType;
    }
}
