package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;

import javax.validation.constraints.NotNull;

public class ChecklistServiceCatalogueDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -60312243393235127L;

    @NotNull
    private LinkResource checklist;

    @NotNull
    private String serviceCode;

    private Double lowerGrossTonnage;

    private Double upperGrossTonnage;

    public LinkResource getChecklist()
    {
        return checklist;
    }

    public void setChecklist(final LinkResource checklist)
    {
        this.checklist = checklist;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public Double getLowerGrossTonnage()
    {
        return lowerGrossTonnage;
    }

    public void setLowerGrossTonnage(final Double lowerGrossTonnage)
    {
        this.lowerGrossTonnage = lowerGrossTonnage;
    }

    public Double getUpperGrossTonnage()
    {
        return upperGrossTonnage;
    }

    public void setUpperGrossTonnage(final Double upperGrossTonnage)
    {
        this.upperGrossTonnage = upperGrossTonnage;
    }
}
