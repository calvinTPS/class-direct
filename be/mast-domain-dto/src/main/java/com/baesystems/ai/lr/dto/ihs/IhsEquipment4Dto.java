package com.baesystems.ai.lr.dto.ihs;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsEquipment4Dto extends IhsBaseDto
{
    private static final long serialVersionUID = -5274483044150909977L;

    @Size(message = "invalid length", max = 1)
    private String equipmentLetter;

    public String getEquipmentLetter()
    {
        return equipmentLetter;
    }

    public void setEquipmentLetter(final String equipmentLetter)
    {
        this.equipmentLetter = equipmentLetter;
    }

}
