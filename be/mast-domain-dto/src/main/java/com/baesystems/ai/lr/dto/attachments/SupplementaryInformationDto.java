package com.baesystems.ai.lr.dto.attachments;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("PMD.ExcessivePublicCount")
public class SupplementaryInformationDto extends AuditedDto implements UpdateableJobBundleEntity
{
    private static final long serialVersionUID = -3219159044886874926L;

    @Size(message = "invalid length", max = 255)
    private String title;

    @Size(message = "invalid length", max = 2500)
    private String note;

    @Size(message = "invalid length", max = 500)
    private String attachmentUrl;

    @Size(message = "invalid length", max = 100)
    private String assetLocationViewpoint;

    @Valid
    private LinkResource attachmentType;

    @Valid
    private LinkResource attachmentCategory;

    @Valid
    private LinkResource confidentialityType;

    @Valid
    private LinkResource location;

    @Valid
    private LinkResource author;

    @Size(message = "invalid length", max = 100)
    private String source;

    private Long documentVersion;

    private Date creationDate;

    private Date attachmentDate;

    private AttachmentEntityLink entityLink;

    private Long parentRecordId;

    private String updatedDateWithTime;

    private String creationDateWithTime;

    private String attachmentDateWithTime;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getNote()
    {
        return note;
    }

    public void setNote(final String note)
    {
        this.note = note;
    }

    public String getAttachmentUrl()
    {
        return attachmentUrl;
    }

    public void setAttachmentUrl(final String attachmentUrl)
    {
        this.attachmentUrl = attachmentUrl;
    }

    public String getAssetLocationViewpoint()
    {
        return assetLocationViewpoint;
    }

    public void setAssetLocationViewpoint(final String assetLocationViewpoint)
    {
        this.assetLocationViewpoint = assetLocationViewpoint;
    }

    public LinkResource getAttachmentType()
    {
        return attachmentType;
    }

    public void setAttachmentType(final LinkResource attachmentType)
    {
        this.attachmentType = attachmentType;
    }

    public LinkResource getConfidentialityType()
    {
        return confidentialityType;
    }

    public LinkResource getAttachmentCategory()
    {
        return attachmentCategory;
    }

    public void setAttachmentCategory(final LinkResource attachmentCategory)
    {
        this.attachmentCategory = attachmentCategory;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public LinkResource getLocation()
    {
        return location;
    }

    public void setLocation(final LinkResource location)
    {
        this.location = location;
    }

    public LinkResource getAuthor()
    {
        return author;
    }

    public void setAuthor(final LinkResource author)
    {
        this.author = author;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public Date getAttachmentDate()
    {
        return attachmentDate;
    }

    public void setAttachmentDate(final Date attachmentDate)
    {
        this.attachmentDate = attachmentDate;
    }

    public Long getDocumentVersion()
    {
        return documentVersion;
    }

    public void setDocumentVersion(final Long documentVersion)
    {
        this.documentVersion = documentVersion;
    }

    public AttachmentEntityLink getEntityLink()
    {
        return entityLink;
    }

    public void setEntityLink(final AttachmentEntityLink entityLink)
    {
        this.entityLink = entityLink;
    }

    public Long getParentRecordId()
    {
        return parentRecordId;
    }

    public void setParentRecordId(final Long parentRecordId)
    {
        this.parentRecordId = parentRecordId;
    }

    public String getUpdatedDateWithTime()
    {
        return updatedDateWithTime;
    }

    public void setUpdatedDateWithTime(final String updatedDateWithTime)
    {
        this.updatedDateWithTime = updatedDateWithTime;
    }

    public String getCreationDateWithTime()
    {
        return creationDateWithTime;
    }

    public void setCreationDateWithTime(final String creationDateWithTime)
    {
        this.creationDateWithTime = creationDateWithTime;
    }

    public String getAttachmentDateWithTime()
    {
        return attachmentDateWithTime;
    }

    public void setAttachmentDateWithTime(final String attachmentDateWithTime)
    {
        this.attachmentDateWithTime = attachmentDateWithTime;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(final String source)
    {
        this.source = source;
    }

    // There is no conflict resolution for attachments so this will not be used, but these methods are need so that it
    // can be passed to the sync service
    @Override
    @JsonIgnore
    public String getStalenessHash()
    {
        return null;
    }

    @Override
    @JsonIgnore
    public void setStalenessHash(final String newStalenessHash)
    {
        // do nothing
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return null;
    }

    public static class AttachmentEntityLink
    {
        private LinkVersionedResource link;

        private String type;

        private String identifier;

        public LinkVersionedResource getLink()
        {
            return link;
        }

        public void setLink(final LinkVersionedResource link)
        {
            this.link = link;
        }

        public String getType()
        {
            return type;
        }

        public void setType(final String type)
        {
            this.type = type;
        }

        public String getIdentifier()
        {
            return identifier;
        }

        public void setIdentifier(final String identifier)
        {
            this.identifier = identifier;
        }
    }
}
