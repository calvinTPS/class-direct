package com.baesystems.ai.lr.dto.reminders;

import java.util.Date;

public class TaskReminderDto extends BaseReminderDto
{
    private static final long serialVersionUID = 6698484611686932720L;

    private String serviceCode;

    private String taskName;

    private Date postponementDate;

    public String getServiceCode()
    {
        return this.serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public String getTaskName()
    {
        return this.taskName;
    }

    public void setTaskName(final String taskName)
    {
        this.taskName = taskName;
    }

    public Date getPostponementDate()
    {
        return this.postponementDate;
    }

    public void setPostponementDate(final Date postponementDate)
    {
        this.postponementDate = postponementDate;
    }
}
