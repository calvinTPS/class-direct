package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ServiceCatalogueLightDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -3224181422510117184L;

    private String name;

    private String code;

    private LinkResource serviceType;

    private LinkResource surveyType;

    private LinkResource defectCategory;

    private LinkResource serviceRuleset;

    private LinkResource serviceGroup;

    private LinkResource schedulingType;

    private Boolean continuousIndicator;

    private LinkResource harmonisationType;

    private Integer displayOrder;

    private Integer cyclePeriodicity;

    private Boolean cyclePeriodicityEditable;

    private Integer lowerRangeDateOffsetMonths;

    private Integer upperRangeDateOffsetMonths;

    private Integer approvedPostponementMonths;

    private Integer conditionalPostponementMonths;

    private String description;

    private String categoryLetter;

    private Boolean multipleIndicator;

    private LinkResource schedulingRegime;

    private Boolean privateIndicator;

    private Boolean coastalIndicator;

    private LinkResource workItemType;

    private Boolean partHeldApplicable;

    private Boolean espIndicator;

    private Boolean nameEditable;

    private Boolean autoCertificate;

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return this.code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public LinkResource getServiceType()
    {
        return this.serviceType;
    }

    public void setServiceType(final LinkResource serviceType)
    {
        this.serviceType = serviceType;
    }

    public LinkResource getSurveyType()
    {
        return this.surveyType;
    }

    public void setSurveyType(final LinkResource surveyType)
    {
        this.surveyType = surveyType;
    }

    public LinkResource getDefectCategory()
    {
        return this.defectCategory;
    }

    public void setDefectCategory(final LinkResource defectCategory)
    {
        this.defectCategory = defectCategory;
    }

    public LinkResource getServiceRuleset()
    {
        return this.serviceRuleset;
    }

    public void setServiceRuleset(final LinkResource serviceRuleset)
    {
        this.serviceRuleset = serviceRuleset;
    }

    public LinkResource getServiceGroup()
    {
        return this.serviceGroup;
    }

    public void setServiceGroup(final LinkResource serviceGroup)
    {
        this.serviceGroup = serviceGroup;
    }

    public LinkResource getSchedulingType()
    {
        return this.schedulingType;
    }

    public void setSchedulingType(final LinkResource schedulingType)
    {
        this.schedulingType = schedulingType;
    }

    public Boolean getContinuousIndicator()
    {
        return this.continuousIndicator;
    }

    public void setContinuousIndicator(final Boolean continuousIndicator)
    {
        this.continuousIndicator = continuousIndicator;
    }

    public LinkResource getHarmonisationType()
    {
        return this.harmonisationType;
    }

    public void setHarmonisationType(final LinkResource harmonisationType)
    {
        this.harmonisationType = harmonisationType;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public Integer getCyclePeriodicity()
    {
        return this.cyclePeriodicity;
    }

    public void setCyclePeriodicity(final Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    public Boolean getCyclePeriodicityEditable()
    {
        return this.cyclePeriodicityEditable;
    }

    public void setCyclePeriodicityEditable(final Boolean cyclePeriodicityEditable)
    {
        this.cyclePeriodicityEditable = cyclePeriodicityEditable;
    }

    public Integer getLowerRangeDateOffsetMonths()
    {
        return this.lowerRangeDateOffsetMonths;
    }

    public void setLowerRangeDateOffsetMonths(final Integer lowerRangeDateOffsetMonths)
    {
        this.lowerRangeDateOffsetMonths = lowerRangeDateOffsetMonths;
    }

    public Integer getUpperRangeDateOffsetMonths()
    {
        return this.upperRangeDateOffsetMonths;
    }

    public void setUpperRangeDateOffsetMonths(final Integer upperRangeDateOffsetMonths)
    {
        this.upperRangeDateOffsetMonths = upperRangeDateOffsetMonths;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getCategoryLetter()
    {
        return this.categoryLetter;
    }

    public void setCategoryLetter(final String categoryLetter)
    {
        this.categoryLetter = categoryLetter;
    }

    public Boolean getMultipleIndicator()
    {
        return this.multipleIndicator;
    }

    public void setMultipleIndicator(final Boolean multipleIndicator)
    {
        this.multipleIndicator = multipleIndicator;
    }

    public LinkResource getSchedulingRegime()
    {
        return this.schedulingRegime;
    }

    public void setSchedulingRegime(final LinkResource schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }

    public Boolean getPrivateIndicator()
    {
        return this.privateIndicator;
    }

    public void setPrivateIndicator(final Boolean privateIndicator)
    {
        this.privateIndicator = privateIndicator;
    }

    public Boolean getCoastalIndicator()
    {
        return this.coastalIndicator;
    }

    public void setCoastalIndicator(final Boolean coastalIndicator)
    {
        this.coastalIndicator = coastalIndicator;
    }

    public Integer getApprovedPostponementMonths()
    {
        return this.approvedPostponementMonths;
    }

    public void setApprovedPostponementMonths(final Integer approvedPostponementMonths)
    {
        this.approvedPostponementMonths = approvedPostponementMonths;
    }

    public Integer getConditionalPostponementMonths()
    {
        return this.conditionalPostponementMonths;
    }

    public void setConditionalPostponementMonths(final Integer conditionalPostponementMonths)
    {
        this.conditionalPostponementMonths = conditionalPostponementMonths;
    }

    public LinkResource getWorkItemType()
    {
        return this.workItemType;
    }

    public void setWorkItemType(final LinkResource workItemType)
    {
        this.workItemType = workItemType;
    }

    public Boolean getPartHeldApplicable()
    {
        return partHeldApplicable;
    }

    public void setPartHeldApplicable(final Boolean partHeldApplicable)
    {
        this.partHeldApplicable = partHeldApplicable;
    }

    public Boolean getEspIndicator()
    {
        return espIndicator;
    }

    public void setEspIndicator(final Boolean espIndicator)
    {
        this.espIndicator = espIndicator;
    }

    public Boolean getNameEditable()
    {
        return nameEditable;
    }

    public void setNameEditable(final Boolean nameEditable)
    {
        this.nameEditable = nameEditable;
    }

    public Boolean getAutoCertificate()
    {
        return autoCertificate;
    }

    public void setAutoCertificate(final Boolean autoCertificate)
    {
        this.autoCertificate = autoCertificate;
    }
}
