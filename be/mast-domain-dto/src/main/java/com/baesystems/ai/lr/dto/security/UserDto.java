package com.baesystems.ai.lr.dto.security;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class UserDto extends BaseDto
{
    private static final long serialVersionUID = 8957544789826737609L;

    @NotNull
    private String networkUserId;

    private String name;

    @NotNull
    private List<String> groupList;

    @NotNull
    private List<String> permissionList;

    private String emailAddress;

    private boolean deleted;

    public String getNetworkUserId()
    {
        return networkUserId;
    }

    public void setNetworkUserId(final String networkUserId)
    {
        this.networkUserId = networkUserId;
    }

    public List<String> getGroupList()
    {
        return groupList;
    }

    public void setGroupList(final List<String> groupList)
    {
        this.groupList = groupList;
    }

    public List<String> getPermissionList()
    {
        return permissionList;
    }

    public void setPermissionList(final List<String> permissionList)
    {
        this.permissionList = permissionList;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public boolean getDeleted()
    {
        return deleted;
    }

    public void setDeleted(final boolean deleted)
    {
        this.deleted = deleted;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
