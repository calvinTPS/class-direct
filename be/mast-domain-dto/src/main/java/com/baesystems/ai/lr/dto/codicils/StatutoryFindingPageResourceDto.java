package com.baesystems.ai.lr.dto.codicils;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class StatutoryFindingPageResourceDto extends BasePageResource<StatutoryFindingDto>
{
    @Valid
    private List<StatutoryFindingDto> content;

    @Override
    public List<StatutoryFindingDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<StatutoryFindingDto> content)
    {
        this.content = content;
    }
}
