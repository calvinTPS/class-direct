package com.baesystems.ai.lr.dto.query;

import java.util.List;

public class CertificateActionQueryDto implements QueryDto
{
    private static final long serialVersionUID = -8718934152777484517L;

    private List<Long> certificateId;

    private Long jobId;

    public List<Long> getCertificateId()
    {
        return certificateId;
    }

    public void setCertificateId(final List<Long> certificateId)
    {
        this.certificateId = certificateId;
    }

    public Long getJobId()
    {
        return this.jobId;
    }

    public void setJobId(final Long jobId)
    {
        this.jobId = jobId;
    }
}
