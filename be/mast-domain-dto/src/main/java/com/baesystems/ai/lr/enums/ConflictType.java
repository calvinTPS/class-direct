package com.baesystems.ai.lr.enums;

/**
 * This enum is used in the ReportEntityConflictService to denote the entity type being checked.
 */
public enum ConflictType
{
    COC,
    ASSET_NOTE,
    ACTIONABLE_ITEM,
    STATUTORY_FINDING,
    DEFECT,
    DEFICIENCY,
    REPAIR,
    RECTIFICATION,
    SURVEY,
    WORK_ITEM;
}
