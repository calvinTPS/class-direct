package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class LrEmployeeRoleDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -8788978251299711763L;

    private LinkResource role;

    private LinkResource employee;

    public LinkResource getRole()
    {
        return role;
    }

    public void setRole(final LinkResource role)
    {
        this.role = role;
    }

    public LinkResource getEmployee()
    {
        return employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }
}
