package com.baesystems.ai.lr.dto.base;

/**
 * Reference data DTOs that wrap data that may be edited by the user
 * e.g. customer details, should extend this class. This will allow existing
 * data to be updated if an ID is specified or new reference data created
 * if ID is null.
 */
public class MutableReferenceDataDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 32913123054423654L;
}
