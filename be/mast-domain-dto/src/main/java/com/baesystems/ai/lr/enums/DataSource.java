package com.baesystems.ai.lr.enums;

public enum DataSource
{
    MAST("MAST"),
    IHS("IHS");

    private String value;

    DataSource(final String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
