package com.baesystems.ai.lr.dto.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.time.DateFormatUtils;

import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseDto implements IdDto, Serializable
{
    private static final long serialVersionUID = -7700260036236546617L;

    @JsonProperty("_id")
    private String internalId;

    @ConflictAware
    private Long id;

    public BaseDto()
    {
        this.internalId = UUID.randomUUID().toString();
    }

    @Override
    public final Long getId()
    {
        return this.id;
    }

    @Override
    public final void setId(final Long id)
    {
        this.id = id;
    }

    @Override
    public String getInternalId()
    {
        return this.id == null ? this.internalId : this.id.toString();
    }

    @Override
    public void setInternalId(final String internalId)
    {
        if (internalId != null)
        {
            this.internalId = this.id == null ? internalId : this.id.toString();
        }
    }

    @Override
    @JsonIgnore
    public String getTrueInternalId()
    {
        return this.internalId;
    }

    /**
     * Objects point to the same reference and therefore the same object -> result = true, method returns true<br>
     * Objects are not the same object -> result = false
     * <p>
     * Other objects is not a BaseDTO -> result is unchanged i.e.false, method returns false<br>
     * ID of other object equals ID of this object -> result = true, method returns true<br>
     * ID of other object does NOT equal ID of this object -> result is unchanged i.e. false, method returns false
     * <p>
     * Therefore, equals only if exact same object, or if other is a BaseDto and has the same ID.
     */
    @Override
    public boolean equals(final Object other)
    {
        boolean result = this == other;

        if (!result && other instanceof BaseDto)
        {
            final BaseDto baseDto = (BaseDto) other;

            if (baseDto.getId() != null && baseDto.getId().equals(this.getId()))
            {
                result = true;
            }
        }

        return result;
    }

    @Override
    public int hashCode()
    {
        final int result;

        if (this.getId() != null)
        {
            result = this.getId().hashCode();
        }
        else
        {
            result = super.hashCode();
        }

        return result;
    }

    protected String getStringRepresentationOfList(final List<? extends BaseDto> listOfDtos)
    {
        final StringBuilder builder = new StringBuilder();

        if (listOfDtos == null)
        {
            builder.append("NULL");
        }
        else if (listOfDtos.isEmpty())
        {
            builder.append("[]");
        }
        else
        {
            final List<BaseDto> sortedList = new ArrayList<>(listOfDtos);
            sortedList
                    .stream()
                    .sorted(Comparator.comparing(dto -> dto.getId() == null ? -1 : dto.getId()))
                    .forEach(dto -> builder.append(dto.getId()));
        }

        return builder.toString();
    }

    protected String getStringRepresentationOfListOfStaleObjects(final List<? extends StaleObject> listOfDtos)
    {
        final StringBuilder builder = new StringBuilder();

        if (listOfDtos == null)
        {
            builder.append("NULL");
        }
        else if (listOfDtos.isEmpty())
        {
            builder.append("[]");
        }
        else
        {
            final List<StaleObject> sortedList = new ArrayList<>(listOfDtos);
            sortedList
                    .stream()
                    .sorted(Comparator.comparing(StaleObject::getStringRepresentationForHash))
                    .forEach(dto -> builder.append(dto.getStringRepresentationForHash()));
        }

        return builder.toString();
    }

    protected String getStringRepresentationOfDate(final Date dateToFormat)
    {
        String result = null;

        if (dateToFormat != null)
        {
            result = DateFormatUtils.ISO_DATE_FORMAT.format(dateToFormat);
        }

        return result;
    }

    protected String getStringRepresentationOfLinkVersionedResource(final LinkVersionedResource linkResource)
    {
        return linkResource == null ? "NULL" : String.format("%d,%d", linkResource.getId(), linkResource.getVersion());
    }

    protected <T extends BaseDto> Long getIdOrNull(final T dto)
    {
        return dto == null ? null : dto.getId();
    }

    public String entityName()
    {
        return "";
    }

    protected <T extends StaleObject> String getStringRepresentationOfStaleObject(final T object)
    {
        return object == null ? "NULL" : object.getStringRepresentationForHash();
    }
}
