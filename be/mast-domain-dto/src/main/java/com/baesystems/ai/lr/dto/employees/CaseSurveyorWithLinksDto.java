package com.baesystems.ai.lr.dto.employees;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class CaseSurveyorWithLinksDto extends BaseDto
{
    private static final long serialVersionUID = 1746089424926458018L;

    @NotNull
    private LinkResource surveyor;

    @NotNull
    @Valid
    private LinkResource employeeRole;

    public LinkResource getSurveyor()
    {
        return surveyor;
    }

    public void setSurveyor(final LinkResource surveyor)
    {
        this.surveyor = surveyor;
    }

    public LinkResource getEmployeeRole()
    {
        return employeeRole;
    }

    public void setEmployeeRole(final LinkResource employeeRole)
    {
        this.employeeRole = employeeRole;
    }
}
