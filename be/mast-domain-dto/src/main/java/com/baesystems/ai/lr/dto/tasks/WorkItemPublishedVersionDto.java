package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class WorkItemPublishedVersionDto extends BaseDto
{
    private static final long serialVersionUID = -179135795926946166L;

    private Long publishedVersionId;

    private List<WorkItemLightDto> workItemDtos;

    public Long getPublishedVersionId()
    {
        return publishedVersionId;
    }

    public void setPublishedVersionId(final Long publishedVersionId)
    {
        this.publishedVersionId = publishedVersionId;
    }

    public List<WorkItemLightDto> getWorkItemDtos()
    {
        return workItemDtos;
    }

    public void setWorkItemDtos(final List<WorkItemLightDto> workItemDtos)
    {
        this.workItemDtos = workItemDtos;
    }
}
