package com.baesystems.ai.lr.enums;

public enum RBACGroups
{
    ADMIN("Admin", 1L),
    SURVEYOR("SURVEYOR", 2L),
    AUTHORISING_SURVEYOR("AUTHORISINGSURVEYOR", 3L),
    SDO("SDO", 4L),
    CFO("CFO", 5L),
    MMSO("MMSO", 6L),
    DCE_TECHNICAL("DCE-TECHNICAL", 7L),
    CLASS_GROUP_MMS("CLASSGROUP-MMS", 8L),
    CLASS_GROUP_FLEET_DATABASE("CLASSGROUP-FLEETDATABASE", 9L),
    CLASS_GROUP_ADMIN("CLASSGROUP-ADMIN", 10L),
    CLASS_GROUP_SENIOR_ADMIN("CLASSGROUP-SENIORADMIN", 11L),
    CLASS_GROUP_TECHNICAL("CLASSGROUP-TECHNICAL", 12L),
    EIC_ADMIN("EIC-ADMIN", 13L),
    EIC_SENIOR_ADMIN("EIC-SENIOR-ADMIN", 14L),
    EIC_TECHNICAL("EIC-TECHNICAL", 15L),
    MDS_SYSTEM_ADMIN("MDS-SYSTEMADMIN", 16L),
    MDS_SUPER_USER("MDS-SUPERUSER", 17L);

    private String RBACGroupDisplay;

    private Long RBACGroupId;

    RBACGroups(final String userGroupDisplay, final Long userGroupId)
    {
        this.RBACGroupDisplay = userGroupDisplay;
        this.RBACGroupId = userGroupId;
    }

    public String getUserGroupDisplay()
    {
        return this.RBACGroupDisplay;
    }

    public Long getUserGroupId()
    {
        return this.RBACGroupId;
    }
}
