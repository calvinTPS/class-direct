package com.baesystems.ai.lr.dto.tasks;

import java.util.Date;
import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class TSRETaskDto implements RuleEngineDto
{
    private static final long serialVersionUID = 4771506979116559176L;

    private Long id;

    private Date dueDate;

    private Date assignedDate;

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Date getAssignedDate()
    {
        return this.assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TSRETaskDto [getId()=");
        builder.append(getId());
        builder.append(", getDueDate()=");
        builder.append(getDueDate());
        builder.append(", getAssignedDate()=");
        builder.append(getAssignedDate());
        builder.append("]");
        return builder.toString();
    }
}
