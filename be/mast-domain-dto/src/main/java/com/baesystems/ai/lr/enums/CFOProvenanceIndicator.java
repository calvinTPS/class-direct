package com.baesystems.ai.lr.enums;

import java.util.Arrays;

public enum CFOProvenanceIndicator
{
    // These are used in the CFO integration. They need to be preserved in priority order; higher values
    // take precedence when performing an integration
    NO_PROVENANCE(0),
    JDE(1),
    BY_DESIGN(2);

    private final Integer value;

    CFOProvenanceIndicator(final Integer value)
    {
        this.value = value;
    }

    public final Integer value()
    {
        return value;
    }

    public static CFOProvenanceIndicator getForValue(final Integer value)
    {
        return Arrays.asList(CFOProvenanceIndicator.values()).stream().filter(indicator -> indicator.value().equals(value)).findAny().orElseGet(() -> NO_PROVENANCE);
    }


}
