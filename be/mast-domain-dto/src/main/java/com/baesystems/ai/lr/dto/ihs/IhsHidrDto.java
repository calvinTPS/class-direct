package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsHidrDto extends IhsBaseDto
{
    private static final long serialVersionUID = -9129817030858497238L;

    private Double draughtMax;

    private Integer deadWeight;

    public Double getDraughtMax()
    {
        return draughtMax;
    }

    public void setDraughtMax(final Double draughtMax)
    {
        this.draughtMax = draughtMax;
    }

    public Integer getDeadWeight()
    {
        return deadWeight;
    }

    public void setDeadWeight(final Integer deadWeight)
    {
        this.deadWeight = deadWeight;
    }
}
