package com.baesystems.ai.lr.enums;

public enum TREServiceQueryParameter
{
    ASSET_ID("assetId"),
    SERVICE_ID("assetServiceId"),
    ASSET_VERSION("assetVersion");

    private final String parameterName;

    private TREServiceQueryParameter(final String parameterName)
    {
        this.parameterName = parameterName;
    }

    public String getParameterName()
    {
        return this.parameterName;
    }
}
