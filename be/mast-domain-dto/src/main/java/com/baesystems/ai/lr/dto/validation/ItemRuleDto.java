package com.baesystems.ai.lr.dto.validation;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ItemRuleDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -6909874087893631234L;

    private Long relationshipTypeId;

    private Long toItemType;

    private Long fromItemType;

    private Integer minOccurs;

    private Integer maxOccurs;

    private Integer assetCategoryId;

    public Long getRelationshipTypeId()
    {
        return relationshipTypeId;
    }

    public void setRelationshipTypeId(final Long relationshipTypeId)
    {
        this.relationshipTypeId = relationshipTypeId;
    }

    public Long getToItemType()
    {
        return toItemType;
    }

    public void setToItemType(final Long toItemType)
    {
        this.toItemType = toItemType;
    }

    public Long getFromItemType()
    {
        return fromItemType;
    }

    public void setFromItemType(final Long fromItemType)
    {
        this.fromItemType = fromItemType;
    }

    public Integer getMinOccurs()
    {
        return minOccurs;
    }

    public void setMinOccurs(final Integer minOccurs)
    {
        this.minOccurs = minOccurs;
    }

    public Integer getMaxOccurs()
    {
        return maxOccurs;
    }

    public void setMaxOccurs(final Integer maxOccurs)
    {
        this.maxOccurs = maxOccurs;
    }

    public Integer getAssetCategoryId()
    {
        return assetCategoryId;
    }

    public void setAssetCategoryId(final Integer assetCategoryId)
    {
        this.assetCategoryId = assetCategoryId;
    }
}
