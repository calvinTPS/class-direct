package com.baesystems.ai.lr.dto.query;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EarliestEntityQueryDto implements QueryDto
{
    private static final long serialVersionUID = -7456864616165360835L;

    @NotNull
    @Size(min = 1)
    private List<Long> assetIdList;

    @NotNull
    @Size(min = 1)
    private List<Long> serviceStatusIdList;

    private List<String> excludedServiceCodeList;

    @NotNull
    @Size(min = 1)
    private List<Long> taskStatusIdList;

    @NotNull
    @Size(min = 1)
    private List<Long> actionableItemStatusIdList;

    @NotNull
    @Size(min = 1)
    private List<Long> cocStatusIdList;

    @NotNull
    @Size(min = 1)
    private List<Long> statutoryFindingStatusIdList;

    public List<Long> getAssetIdList()
    {
        return this.assetIdList;
    }

    public void setAssetIdList(final List<Long> assetIdList)
    {
        this.assetIdList = assetIdList;
    }

    public List<Long> getServiceStatusIdList()
    {
        return this.serviceStatusIdList;
    }

    public void setServiceStatusIdList(final List<Long> serviceStatusIdList)
    {
        this.serviceStatusIdList = serviceStatusIdList;
    }

    public List<String> getExcludedServiceCodeList()
    {
        return this.excludedServiceCodeList;
    }

    public void setExcludedServiceCodeList(final List<String> excludedServiceCodeList)
    {
        this.excludedServiceCodeList = excludedServiceCodeList;
    }

    public List<Long> getTaskStatusIdList()
    {
        return this.taskStatusIdList;
    }

    public void setTaskStatusIdList(final List<Long> taskStatusIdList)
    {
        this.taskStatusIdList = taskStatusIdList;
    }

    public List<Long> getActionableItemStatusIdList()
    {
        return this.actionableItemStatusIdList;
    }

    public void setActionableItemStatusIdList(final List<Long> actionableItemStatusIdList)
    {
        this.actionableItemStatusIdList = actionableItemStatusIdList;
    }

    public List<Long> getCocStatusIdList()
    {
        return this.cocStatusIdList;
    }

    public void setCocStatusIdList(final List<Long> cocStatusIdList)
    {
        this.cocStatusIdList = cocStatusIdList;
    }

    public List<Long> getStatutoryFindingStatusIdList()
    {
        return statutoryFindingStatusIdList;
    }

    public void setStatutoryFindingStatusIdList(final List<Long> statutoryFindingStatusIdList)
    {
        this.statutoryFindingStatusIdList = statutoryFindingStatusIdList;
    }
}
