package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsPrincipalDimensionsDto extends IhsBaseDto
{

    private static final long serialVersionUID = 3073204305402513031L;

    private Double lengthOverall;

    private String lengthBtwPerpendiculars;

    private Double breadthMoulded;

    private Double breadthExtreme;

    private Double draughtMax;

    private Integer deadWeight;

    private Double depthMoulded;

    private Integer grossTonnage;

    private Integer netTonnage;

    private String decks;

    private String propulsion;

    public Double getLengthOverall()
    {
        return lengthOverall;
    }

    public void setLengthOverall(final Double lengthOverall)
    {
        this.lengthOverall = lengthOverall;
    }

    public String getLengthBtwPerpendiculars()
    {
        return lengthBtwPerpendiculars;
    }

    public void setLengthBtwPerpendiculars(final String lengthBtwPerpendiculars)
    {
        this.lengthBtwPerpendiculars = lengthBtwPerpendiculars;
    }

    public Double getBreadthMoulded()
    {
        return breadthMoulded;
    }

    public void setBreadthMoulded(final Double breadthMoulded)
    {
        this.breadthMoulded = breadthMoulded;
    }

    public Double getBreadthExtreme()
    {
        return breadthExtreme;
    }

    public void setBreadthExtreme(final Double breadthExtreme)
    {
        this.breadthExtreme = breadthExtreme;
    }

    public Double getDraughtMax()
    {
        return draughtMax;
    }

    public void setDraughtMax(final Double draughtMax)
    {
        this.draughtMax = draughtMax;
    }

    public Integer getDeadWeight()
    {
        return deadWeight;
    }

    public void setDeadWeight(final Integer deadWeight)
    {
        this.deadWeight = deadWeight;
    }

    public Double getDepthMoulded()
    {
        return depthMoulded;
    }

    public void setDepthMoulded(final Double depthMoulded)
    {
        this.depthMoulded = depthMoulded;
    }

    public Integer getGrossTonnage()
    {
        return grossTonnage;
    }

    public void setGrossTonnage(final Integer grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public Integer getNetTonnage()
    {
        return netTonnage;
    }

    public void setNetTonnage(final Integer netTonnage)
    {
        this.netTonnage = netTonnage;
    }

    public String getDecks()
    {
        return decks;
    }

    public void setDecks(final String decks)
    {
        this.decks = decks;
    }

    public String getPropulsion()
    {
        return propulsion;
    }

    public void setPropulsion(final String propulsion)
    {
        this.propulsion = propulsion;
    }

}
