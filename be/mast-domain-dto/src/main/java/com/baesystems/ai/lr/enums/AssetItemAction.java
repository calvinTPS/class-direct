package com.baesystems.ai.lr.enums;

public enum AssetItemAction
{
    DECOMMISSIONED(1L),
    REMOVED(2L);

    private final Long value;

    AssetItemAction(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
