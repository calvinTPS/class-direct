package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;


public class ServiceGroupDto extends ReferenceDataDto

{

    private static final long serialVersionUID = -1287097137981484909L;


    private LinkResource associatedSchedulingType;

    public LinkResource getAssociatedSchedulingType()
    {
        return associatedSchedulingType;
    }

    public void setAssociatedSchedulingType(final LinkResource associatedSchedulingType)
    {
        this.associatedSchedulingType = associatedSchedulingType;
    }
}
