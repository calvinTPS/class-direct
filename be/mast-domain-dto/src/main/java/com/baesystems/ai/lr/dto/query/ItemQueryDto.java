package com.baesystems.ai.lr.dto.query;

import java.util.List;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

public class ItemQueryDto implements QueryDto
{
    private static final long serialVersionUID = 3166612617517208558L;

    private List<Long> itemId;

    private List<Long> originalItemId;

    private List<Long> itemTypeId;

    private List<Long> attributeId;

    @UsesLikeComparator
    private String itemName;

    @UsesLikeComparator
    private String itemTypeName;

    @UsesLikeComparator
    private String attributeName;

    @UsesLikeComparator
    private String attributeValue;

    private List<Long> attributeTypeId;

    public List<Long> getItemId()
    {
        return itemId;
    }

    public void setItemId(final List<Long> itemId)
    {
        this.itemId = itemId;
    }

    public List<Long> getOriginalItemId()
    {
        return originalItemId;
    }

    public void setOriginalItemId(final List<Long> originalItemId)
    {
        this.originalItemId = originalItemId;
    }

    public List<Long> getItemTypeId()
    {
        return itemTypeId;
    }

    public void setItemTypeId(final List<Long> itemTypeId)
    {
        this.itemTypeId = itemTypeId;
    }

    public List<Long> getAttributeId()
    {
        return attributeId;
    }

    public void setAttributeId(final List<Long> attributeId)
    {
        this.attributeId = attributeId;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(final String itemName)
    {
        this.itemName = itemName;
    }

    public String getItemTypeName()
    {
        return itemTypeName;
    }

    public void setItemTypeName(final String itemTypeName)
    {
        this.itemTypeName = itemTypeName;
    }

    public String getAttributeName()
    {
        return attributeName;
    }

    public void setAttributeName(final String attributeName)
    {
        this.attributeName = attributeName;
    }

    public String getAttributeValue()
    {
        return attributeValue;
    }

    public void setAttributeValue(final String attributeValue)
    {
        this.attributeValue = attributeValue;
    }

    public List<Long> getAttributeTypeId()
    {
        return attributeTypeId;
    }

    public void setAttributeTypeId(final List<Long> attributeTypeId)
    {
        this.attributeTypeId = attributeTypeId;
    }
}
