package com.baesystems.ai.lr.dto.codicils;

import static com.baesystems.ai.lr.dto.codicils.ActionableItemDto.ENTITY_NAME;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.FieldLessThanOrEqual;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
@FieldLessThanOrEqual.List({
                            @FieldLessThanOrEqual(first = "imposedDate", second = "dueDate", useTime = false,
                                    message = "The due date cannot be before the imposed date")})
public class ActionableItemDto extends CodicilDto implements DueStatusUpdateableDto, HashedParentReportingDto
{
    private static final long serialVersionUID = -7222445739741683607L;
    public static final String ENTITY_NAME = "Actionable Item";

    private String stalenessHash;

    @NotNull
    @ConflictAware
    private Boolean requireApproval;

    @ConflictAware
    @Size(message = "invalid length", max = 2000)
    private String surveyorGuidance;

    @ConflictAware
    private Boolean editableBySurveyor;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_CATEGORIES))
    private LinkResource category;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CONFIDENTIALITY_TYPES))
    private LinkResource confidentialityType;

    @Valid
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_DEFECT)
    private LinkResource defect;

    @Valid
    @ConflictAware
    private LinkResource asset;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LinkResource employee;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_TEMPLATES))
    private LinkResource template;

    @ConflictAware
    private Integer sequenceNumber;

    @ConflictAware
    private Date dateOfCrediting;

    @Valid
    @ConflictAware
    private List<LinkResource> scheduledServices;

    public Boolean getRequireApproval()
    {
        return this.requireApproval;
    }

    public void setRequireApproval(final Boolean requireApproval)
    {
        this.requireApproval = requireApproval;
    }

    public String getSurveyorGuidance()
    {
        return this.surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public Boolean getEditableBySurveyor()
    {
        return this.editableBySurveyor;
    }

    public void setEditableBySurveyor(final Boolean editableBySurveyor)
    {
        this.editableBySurveyor = editableBySurveyor;
    }

    public LinkResource getCategory()
    {
        return this.category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public LinkResource getTemplate()
    {
        return this.template;
    }

    public void setTemplate(final LinkResource template)
    {
        this.template = template;
    }

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getDateOfCrediting()
    {
        return this.dateOfCrediting;
    }

    public void setDateOfCrediting(final Date dateOfCrediting)
    {
        this.dateOfCrediting = dateOfCrediting;
    }

    public List<LinkResource> getScheduledServices()
    {
        return this.scheduledServices;
    }

    public void setScheduledServices(final List<LinkResource> scheduledServices)
    {
        this.scheduledServices = scheduledServices;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "ActionableItemDto [title=" + this.getTitle()
                + ", description=" + this.getDescription()
                + ", narrative=" + this.getNarrative()
                + ", imposedDate=" + this.getStringRepresentationOfDate(this.getImposedDate())
                + ", dueDate=" + this.getStringRepresentationOfDate(this.getDueDate())
                + ", requireApproval=" + this.getRequireApproval()
                + ", surveyorGuidance=" + this.getSurveyorGuidance()
                + ", editableBySurveyor=" + this.getEditableBySurveyor()
                + ", category=" + this.getIdOrNull(this.category)
                + ", confidentialityType=" + this.getIdOrNull(this.confidentialityType)
                + ", job=" + this.getIdOrNull(this.getJob())
                + ", status=" + this.getIdOrNull(this.getStatus())
                + ", defect=" + this.getIdOrNull(this.defect)
                + ", asset=" + this.getIdOrNull(this.asset)
                + ", assetItem=" + this.getIdOrNull(this.getAssetItem())
                + ", employee=" + this.getIdOrNull(this.employee)
                + ", template=" + this.getIdOrNull(this.template)
                + ", parent=" + this.getIdOrNull(this.getParent())
                + ", jobScopeConfirmed=" + this.getJobScopeConfirmed()
                + ", sequenceNumber=" + this.getSequenceNumber()
                + ", dateOfCrediting=" + this.getStringRepresentationOfDate(this.dateOfCrediting)
                + ", creditedBy=" + this.getIdOrNull(getCreditedBy())
                + ", scheduledServices=" + this.getStringRepresentationOfList(this.scheduledServices)
                + ", raisedOnJob=" + this.getIdOrNull(this.getRaisedOnJob())
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
