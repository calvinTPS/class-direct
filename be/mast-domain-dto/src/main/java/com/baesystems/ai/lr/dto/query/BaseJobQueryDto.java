package com.baesystems.ai.lr.dto.query;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Size;

public class BaseJobQueryDto implements QueryDto
{
    private static final long serialVersionUID = 536501995749649737L;

    @Size(min = 1)
    private List<Long> jobStatusId;

    @Size(min = 1)
    private List<Long> employeeId;

    @Size(min = 1)
    private List<Long> officeId;

    @Size(min = 1)
    private List<Long> assetId;

    @Size(min = 1)
    private List<Long> caseId;

    private Date startDateMin;

    private Date startDateMax;

    public List<Long> getJobStatusId()
    {
        return jobStatusId;
    }

    public void setJobStatusId(final List<Long> jobStatusId)
    {
        this.jobStatusId = jobStatusId;
    }

    public List<Long> getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(final List<Long> employeeId)
    {
        this.employeeId = employeeId;
    }

    public List<Long> getOfficeId()
    {
        return officeId;
    }

    public void setOfficeId(final List<Long> officeId)
    {
        this.officeId = officeId;
    }

    public List<Long> getCaseId()
    {
        return caseId;
    }

    public void setCaseId(final List<Long> caseId)
    {
        this.caseId = caseId;
    }

    public List<Long> getAssetId()
    {
        return assetId;
    }

    public void setAssetId(final List<Long> assetId)
    {
        this.assetId = assetId;
    }

    public Date getStartDateMin()
    {
        return startDateMin;
    }

    public void setStartDateMin(final Date startDateMin)
    {
        this.startDateMin = startDateMin;
    }

    public Date getStartDateMax()
    {
        return startDateMax;
    }

    public void setStartDateMax(final Date startDateMax)
    {
        this.startDateMax = startDateMax;
    }
}
