package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.List;

/**
 * CodicilCategory enum matching MAST_REF_codicilCategory DB table. 04 January 2017
 */
public enum CodicilCategory
{
    AN_CLASS(1L),
    AN_STATUTORY(2L),
    AN_EXTERNAL(3L),
    AI_CLASS(4L),
    AI_STATUTORY(5L),
    AI_EXTERNAL(6L),
    COC_HULL(7L),
    COC_MACHINERY(8L),
    COC_REFRIGERATION(9L),
    COC_LIFTING_APPLIANCE(10L),
    COC_ELECTRO_TECHNICAL(11L),
    COC_ASSET(12L),
    COC_UNKNOWN_CODICIL_CATEGORY_COC(13L),
    COC_UNKNOWN_MIGRATED_CODICIL_CATEGORY_COC(14L),
    AN_UNKNOWN_CODICIL_CATEGORY_ASSET_NOTE(15L),
    AN_UNKNOWN_MIGRATED_CODICIL_CATEGORY_ASSET_NOTE(16L),
    COC_INHERITED(17L),
    COC_NON_DEFECT(18L),
    SF_ANTI_FOULING_SYSTEMS(19L),
    SF_BALLAST_WATER_MANAGEMENT(20L),
    SF_CHEMICAL_CODE(21L),
    SF_INTERNAT_CHEM_CODE(22L),
    SF_GAS_CODE(23L),
    SF_INTERNAT_GAS_CODE(24L),
    SF_LOAD_LINE(25L),
    SF_LIFTING_APPLIANCES(26L),
    SF_MARPOL_ANNEX_I_OIL(27L),
    SF_MARPOL_ANNEX_II_NOXIOUS_LIQUIDS(28L),
    SF_MARPOL_ANNEX_IV_SEWAGE(29L),
    SF_MARPOL_ANNEX_V_GARBAGE(30L),
    SF_MARPOL_ANNEX_VI_AIR(31L),
    SF_BULK_CARGOES_CODE(32L),
    SF_STABILITY(33L),
    SF_DANGEROUS_GOODS(34L),
    SF_DANGEROUS_GOODS_REG_18(35L),
    SF_PASSENGER_DANGEROUS_GOODS(36L),
    SF_PASSENGER_SAFETY(37L),
    SF_CARGO_SHIP_SAFETY_RADIO(38L),
    SF_CARGO_SHIP_SAFETY_CONSTRUCTION(39L),
    SF_CARGO_SHIP_SAFETY_EQUIPMENT(40L),
    SF_TONNAGE(41L),
    SF_CREW_ACCOMMODATION(42L),
    SF_PORT_STATE_CONTROL(43L),
    SF_MOBILE_OFFSHORE_DRILLING_UNIT_CODE_MODU(44L),
    SF_CP_COMBINED(45L),
    SF_CP_WORKBOATS(46L),
    SF_CP_SAILING(47L),
    SF_MEGAYACHT(48L),
    SF_CANADIAN_SAFETY(49L),
    SF_CANADIAN_POLLUTION(50L),
    SF_ADNR(51L);

    private final Long value;

    CodicilCategory(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }

    public static final List<CodicilCategory> getCoCCategories()
    {
        return Arrays.asList(
                COC_HULL,
                COC_MACHINERY,
                COC_REFRIGERATION,
                COC_LIFTING_APPLIANCE,
                COC_ELECTRO_TECHNICAL,
                COC_ASSET,
                COC_UNKNOWN_CODICIL_CATEGORY_COC,
                COC_UNKNOWN_MIGRATED_CODICIL_CATEGORY_COC,
                COC_INHERITED,
                COC_NON_DEFECT);
    }

    public static final List<CodicilCategory> getAICategories()
    {
        return Arrays.asList(
                AI_CLASS,
                AI_STATUTORY,
                AI_EXTERNAL);
    }

    public static final List<CodicilCategory> getANCategories()
    {
        return Arrays.asList(
                AN_CLASS,
                AN_STATUTORY,
                AN_EXTERNAL,
                AN_UNKNOWN_CODICIL_CATEGORY_ASSET_NOTE,
                AN_UNKNOWN_MIGRATED_CODICIL_CATEGORY_ASSET_NOTE);
    }

    public static final List<CodicilCategory> getSFCategories()
    {
        return Arrays.asList(
                SF_ANTI_FOULING_SYSTEMS,
                SF_BALLAST_WATER_MANAGEMENT,
                SF_CHEMICAL_CODE,
                SF_INTERNAT_CHEM_CODE,
                SF_GAS_CODE,
                SF_INTERNAT_GAS_CODE,
                SF_LOAD_LINE,
                SF_LIFTING_APPLIANCES,
                SF_MARPOL_ANNEX_I_OIL,
                SF_MARPOL_ANNEX_II_NOXIOUS_LIQUIDS,
                SF_MARPOL_ANNEX_IV_SEWAGE,
                SF_MARPOL_ANNEX_V_GARBAGE,
                SF_MARPOL_ANNEX_VI_AIR,
                SF_BULK_CARGOES_CODE,
                SF_STABILITY,
                SF_DANGEROUS_GOODS,
                SF_DANGEROUS_GOODS_REG_18,
                SF_PASSENGER_DANGEROUS_GOODS,
                SF_PASSENGER_SAFETY,
                SF_CARGO_SHIP_SAFETY_RADIO,
                SF_CARGO_SHIP_SAFETY_CONSTRUCTION,
                SF_CARGO_SHIP_SAFETY_EQUIPMENT,
                SF_TONNAGE,
                SF_CREW_ACCOMMODATION,
                SF_PORT_STATE_CONTROL,
                SF_MOBILE_OFFSHORE_DRILLING_UNIT_CODE_MODU,
                SF_CP_COMBINED,
                SF_CP_WORKBOATS,
                SF_CP_SAILING,
                SF_MEGAYACHT,
                SF_CANADIAN_SAFETY,
                SF_CANADIAN_POLLUTION,
                SF_ADNR);
    }
}
