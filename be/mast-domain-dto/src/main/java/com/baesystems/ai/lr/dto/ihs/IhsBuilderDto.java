package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsBuilderDto extends IhsBaseDto
{
    private static final long serialVersionUID = -8743255819726534537L;

    private String imoNumber;

    private String name;

    public String getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
