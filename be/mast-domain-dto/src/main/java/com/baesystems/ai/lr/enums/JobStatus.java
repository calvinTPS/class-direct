package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum JobStatus
{
    UNKNOWN(0L),
    SDO_ASSIGNED(1L),
    RESOURCE_ASSIGNED(2L),
    UNDER_SURVEY(3L),
    UNDER_REPORTING(4L),
    AWAITING_TECHNICAL_REVIEWER_ASSIGNMENT(5L),
    UNDER_TECHNICAL_REVIEW(6L),
    AWAITING_ENDORSER_ASSIGNMENT(7L),
    UNDER_ENDORSEMENT(8L),
    CANCELLED(9L),
    ABORTED(10L),
    CLOSED(11L);

    private static final Map<Long, JobStatus> JOB_STATUS_TYPES = new HashMap<>();

    static
    {
        for (final JobStatus statusType : JobStatus.values())
        {
            JOB_STATUS_TYPES.put(statusType.getValue(), statusType);
        }
    }

    private final Long value;

    JobStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    /**
     * Returns a JobStatusType for a given Long value or UNKNOWN if the specified Long is null or not mapped.
     *
     * @param statusTypeValue
     * @return the JobStatusType for the specified Long or UNKNOWN if there is no mapping.
     */
    public static JobStatus getJobStatusType(final Long statusTypeValue)
    {
        JobStatus returnValue = UNKNOWN;
        if (statusTypeValue != null)
        {
            returnValue = JOB_STATUS_TYPES.get(statusTypeValue);
            if (returnValue == null)
            {
                returnValue = UNKNOWN;
            }
        }
        return returnValue;
    }

    public static List<Long> getValidSyncStatuses()
    {
        final List<Long> validStatuses = new ArrayList<Long>();
        validStatuses.add(SDO_ASSIGNED.value);
        validStatuses.add(RESOURCE_ASSIGNED.value);
        validStatuses.add(UNDER_SURVEY.value);
        validStatuses.add(UNDER_REPORTING.value);

        return validStatuses;
    }

    public static List<Long> openStatuses()
    {
        final List<Long> openStatusesList = new ArrayList<Long>();
        openStatusesList.add(JobStatus.SDO_ASSIGNED.getValue());
        openStatusesList.add(JobStatus.RESOURCE_ASSIGNED.getValue());
        openStatusesList.add(JobStatus.UNDER_SURVEY.getValue());
        openStatusesList.add(JobStatus.UNDER_REPORTING.getValue());
        openStatusesList.add(JobStatus.AWAITING_TECHNICAL_REVIEWER_ASSIGNMENT.getValue());
        openStatusesList.add(JobStatus.UNDER_TECHNICAL_REVIEW.getValue());
        openStatusesList.add(JobStatus.AWAITING_ENDORSER_ASSIGNMENT.getValue());
        openStatusesList.add(JobStatus.UNDER_ENDORSEMENT.getValue());
        return openStatusesList;
    }

}
