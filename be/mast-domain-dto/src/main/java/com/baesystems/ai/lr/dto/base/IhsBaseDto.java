package com.baesystems.ai.lr.dto.base;

import java.io.Serializable;
import java.util.UUID;

import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.fasterxml.jackson.annotation.JsonProperty;

public class IhsBaseDto implements Serializable
{
    private static final long serialVersionUID = -4093447789345937297L;

    @JsonProperty("_id")
    private String internalId;

    @ConflictAware
    private String id;

    public IhsBaseDto()
    {
        this.internalId = UUID.randomUUID().toString();
    }

    public final String getId()
    {
        return this.id;
    }

    public final void setId(final String id)
    {
        this.id = id;
    }

    public String getInternalId()
    {
        return this.id == null ? this.internalId : this.id.toString();
    }

    public void setInternalId(final String internalId)
    {
        if (internalId != null)
        {
            this.internalId = this.id == null ? internalId : this.id.toString();
        }
    }

    @Override
    public boolean equals(final Object other)
    {
        boolean result = true;

        if (!(other instanceof IhsBaseDto))
        {
            result = false;
        }
        else
        {
            final IhsBaseDto baseDto = (IhsBaseDto) other;

            if (baseDto.getId() != null && !baseDto.getId().equals(this.getId()))
            {
                result = false;
            }
        }

        return result;
    }

    @Override
    public int hashCode()
    {
        final int result;

        if (this.getId() != null)
        {
            result = this.getId().hashCode();
        }
        else
        {
            result = super.hashCode();
        }

        return result;
    }
}
