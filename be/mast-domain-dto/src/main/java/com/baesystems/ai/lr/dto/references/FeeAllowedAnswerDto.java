package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class FeeAllowedAnswerDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 8241837820689461195L;

    private String value;

    private LinkResource feeOperation;

    private Float operationInput;

    private String serviceCode;

    public String getValue()
    {
        return value;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }

    public LinkResource getFeeOperation()
    {
        return feeOperation;
    }

    public void setFeeOperation(final LinkResource feeOperation)
    {
        this.feeOperation = feeOperation;
    }

    public Float getOperationInput()
    {
        return operationInput;
    }

    public void setOperationInput(final Float operationInput)
    {
        this.operationInput = operationInput;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }
}
