package com.baesystems.ai.lr.dto.ncns;

import static com.baesystems.ai.lr.dto.ncns.MajorNCNDto.ENTITY_NAME;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.dto.base.VersionedDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class MajorNCNDto extends VersionedDto implements UpdateableJobBundleEntity, HashedParentReportingDto
{
    private static final long serialVersionUID = 3549869749849815501L;

    public static final String ENTITY_NAME = "Major Non Conformity";

    private String stalenessHash;

    @NotNull
    @ConflictAware
    @Size(message = "Size must not exceed 50 characters", max = 50)
    private String ismClause;

    @ConflictAware
    @Size(message = "Size must not exceed 50", max = 50)
    private String mncnNumber;

    @Valid
    @NotNull
    @ConflictAware
    private LinkResource job;

    private LinkResource asset;

    @NotNull
    @ConflictAware
    @Size(message = "Size must not exceed 50 characters", max = 50)
    private String areaUnderReview;

    @NotNull
    @ConflictAware
    @Size(message = "Size must not exceed 1000 characters", max = 1000)
    private String description;

    @NotNull
    @ConflictAware
    @Size(message = "Size must not exceed 2000 characters", max = 2000)
    private String objectiveEvidence;

    @ConflictAware
    @Size(message = "Size must not exceed 2000 characters", max = 2000)
    private String correctiveAction;

    @NotNull
    @ConflictAware
    private Date dueDate;

    @ConflictAware
    private Date revisedDate;

    @ConflictAware
    @Size(message = "Size must not exceed 500 characters", max = 500)
    private String revisedReason;

    @ConflictAware
    private Date closureDate;

    @ConflictAware
    @Size(message = "Size must not exceed 2000 characters", max = 2000)
    private String closureReason;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CONFIDENTIALITY_TYPES))
    private LinkResource confidentialityType;

    @Valid
    @NotNull
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.MNCN_STATUSES))
    private LinkResource status;

    @ConflictAware
    private Boolean jobScopeConfirmed;

    private String parentHash;

    private Date sourceJobLastVisitDate;

    private Date sourceJobCreationDateTime;

    @Size(message = "Size must not exceed 100 characters", max = 100)
    @ConflictAware
    private String docCompanyName;

    @Size(message = "Size must not exceed 100 characters", max = 100)
    @ConflictAware
    private String docCompanyCountry;

    @ConflictAware
    private String docCompanyImo;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.ACTIONS_TAKEN))
    private LinkResource actionTaken;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LinkResource creditedBy;

    private Date updatedOn;

    public String getIsmClause()
    {
        return this.ismClause;
    }

    public void setIsmClause(final String ismClause)
    {
        this.ismClause = ismClause;
    }

    public String getAreaUnderReview()
    {
        return this.areaUnderReview;
    }

    public void setAreaUnderReview(final String areaUnderReview)
    {
        this.areaUnderReview = areaUnderReview;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getObjectiveEvidence()
    {
        return this.objectiveEvidence;
    }

    public void setObjectiveEvidence(final String objectiveEvidence)
    {
        this.objectiveEvidence = objectiveEvidence;
    }

    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public String getMncnNumber()
    {
        return this.mncnNumber;
    }

    public void setMncnNumber(final String mncnNumber)
    {
        this.mncnNumber = mncnNumber;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getStatus()
    {
        return this.status;
    }

    public void setStatus(final LinkResource status)
    {
        this.status = status;
    }

    public Date getRevisedDate()
    {
        return this.revisedDate;
    }

    public void setRevisedDate(final Date revisedDate)
    {
        this.revisedDate = revisedDate;
    }

    public String getRevisedReason()
    {
        return this.revisedReason;
    }

    public void setRevisedReason(final String revisedReason)
    {
        this.revisedReason = revisedReason;
    }

    public Date getClosureDate()
    {
        return this.closureDate;
    }

    public void setClosureDate(final Date closureDate)
    {
        this.closureDate = closureDate;
    }

    public String getClosureReason()
    {
        return this.closureReason;
    }

    public void setClosureReason(final String closureReason)
    {
        this.closureReason = closureReason;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public String getCorrectiveAction()
    {
        return this.correctiveAction;
    }

    public void setCorrectiveAction(final String correctiveAction)
    {
        this.correctiveAction = correctiveAction;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public String getParentHash()
    {
        return this.parentHash;
    }

    public void setParentHash(final String parentHash)
    {
        this.parentHash = parentHash;
    }

    @Override
    public Date getSourceJobLastVisitDate()
    {
        return this.sourceJobLastVisitDate;
    }

    @Override
    public void setSourceJobLastVisitDate(final Date sourceJobLastVisitDate)
    {
        this.sourceJobLastVisitDate = sourceJobLastVisitDate;
    }

    @Override
    public Date getSourceJobCreationDateTime()
    {
        return this.sourceJobCreationDateTime;
    }

    @Override
    public void setSourceJobCreationDateTime(final Date sourceJobCreationDateTime)
    {
        this.sourceJobCreationDateTime = sourceJobCreationDateTime;
    }

    @Override
    public void setStalenessHash(final String stalenessHash)
    {
        this.stalenessHash = stalenessHash;
    }

    public Boolean getJobScopeConfirmed()
    {
        return this.jobScopeConfirmed;
    }

    public void setJobScopeConfirmed(final Boolean jobScopeConfirmed)
    {
        this.jobScopeConfirmed = jobScopeConfirmed;
    }

    public String getDocCompanyName()
    {
        return this.docCompanyName;
    }

    public void setDocCompanyName(final String docCompanyName)
    {
        this.docCompanyName = docCompanyName;
    }

    public String getDocCompanyCountry()
    {
        return this.docCompanyCountry;
    }

    public void setDocCompanyCountry(final String docCompanyCountry)
    {
        this.docCompanyCountry = docCompanyCountry;
    }

    public String getDocCompanyImo()
    {
        return this.docCompanyImo;
    }

    public void setDocCompanyImo(final String docCompanyImo)
    {
        this.docCompanyImo = docCompanyImo;
    }

    public LinkResource getActionTaken()
    {
        return this.actionTaken;
    }

    public void setActionTaken(final LinkResource actionTaken)
    {
        this.actionTaken = actionTaken;
    }

    public LinkResource getCreditedBy()
    {
        return this.creditedBy;
    }

    public void setCreditedBy(final LinkResource creditedBy)
    {
        this.creditedBy = creditedBy;
    }

    public Date getUpdatedOn()
    {
        return this.updatedOn;
    }

    public void setUpdatedOn(final Date updatedOn)
    {
        this.updatedOn = updatedOn;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "MajorNCNDto [ismClause=" + this.ismClause
                + ", actionTaken=" + getIdOrNull(this.actionTaken)
                + ", mncnNumber=" + this.mncnNumber
                + ", job=" + this.getIdOrNull(this.job)
                + ", areaUnderReview=" + this.areaUnderReview
                + ", description=" + this.description
                + ", objectiveEvidence=" + this.objectiveEvidence
                + ", correctiveAction=" + this.correctiveAction
                + ", dueDate=" + this.getStringRepresentationOfDate(this.dueDate)
                + ", revisedDate=" + this.getStringRepresentationOfDate(this.revisedDate)
                + ", revisedReason=" + this.revisedReason
                + ", closureDate=" + this.closureDate
                + ", closureReason=" + this.closureReason
                + ", confidentialityType=" + this.getIdOrNull(this.confidentialityType)
                + ", jobScopeConfirmed=" + this.jobScopeConfirmed
                + ", status=" + this.getIdOrNull(this.status)
                + ", docCompanyName=" + this.docCompanyName
                + ", docCompanyCountry=" + this.docCompanyCountry
                + ", docCompanyImo=" + this.docCompanyImo + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
