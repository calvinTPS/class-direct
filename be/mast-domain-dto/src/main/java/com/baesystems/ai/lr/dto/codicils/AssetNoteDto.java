package com.baesystems.ai.lr.dto.codicils;

import static com.baesystems.ai.lr.dto.codicils.AssetNoteDto.ENTITY_NAME;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class AssetNoteDto extends CodicilDto implements DueStatusUpdateableDto
{
    private static final long serialVersionUID = 6721935950386166905L;
    public static final String ENTITY_NAME = "Asset Note";

    private String stalenessHash;

    private String templateName;

    @ConflictAware
    @Size(message = "invalid length", max = 45)
    private String affectedItems;

    @Valid
    private LinkResource asset;

    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LinkResource employee;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_TEMPLATES))
    private LinkResource template;

    @Valid
    @ConflictAware
    private Boolean inheritedFlag;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_CATEGORIES))
    private LinkResource category;

    @ConflictAware
    @Size(message = "invalid length", max = 2000)
    private String surveyorGuidance;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CONFIDENTIALITY_TYPES))
    private LinkResource confidentialityType;

    @NotNull
    @ConflictAware
    private boolean requireApproval;

    @ConflictAware
    private Integer sequenceNumber;

    public String getTemplateName()
    {
        return this.templateName;
    }

    public void setTemplateName(final String templateName)
    {
        this.templateName = templateName;
    }

    public String getAffectedItems()
    {
        return this.affectedItems;
    }

    public void setAffectedItems(final String affectedItems)
    {
        this.affectedItems = affectedItems;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public Boolean getInheritedFlag()
    {
        return this.inheritedFlag;
    }

    public void setInheritedFlag(final Boolean inheritedFlag)
    {
        this.inheritedFlag = inheritedFlag;
    }

    public LinkResource getTemplate()
    {
        return this.template;
    }

    public void setTemplate(final LinkResource template)
    {
        this.template = template;
    }

    public LinkResource getCategory()
    {
        return this.category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public String getSurveyorGuidance()
    {
        return this.surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public boolean isRequireApproval()
    {
        return this.requireApproval;
    }

    public void setRequireApproval(final boolean requireApproval)
    {
        this.requireApproval = requireApproval;
    }

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "AssetNoteDto [templateName=" + this.templateName
                + ", imposedDate=" + this.getStringRepresentationOfDate(this.getImposedDate())
                + ", dueDate=" + this.getStringRepresentationOfDate(this.getDueDate())
                + ", description=" + this.getDescription()
                + ", narrative=" + this.getNarrative()
                + ", affectedItems=" + this.getAffectedItems()
                + ", status=" + this.getIdOrNull(this.getStatus())
                + ", asset=" + this.getIdOrNull(this.asset)
                + ", assetItem=" + this.getIdOrNull(this.getAssetItem())
                + ", parent=" + this.getIdOrNull(this.getParent())
                + ", job=" + this.getIdOrNull(this.getJob())
                + ", employee=" + this.getIdOrNull(this.employee)
                + ", template=" + this.getIdOrNull(this.template)
                + ", inheritedFlag=" + this.getInheritedFlag()
                + ", title=" + this.getTitle()
                + ", category=" + this.getIdOrNull(this.category)
                + ", surveyorGuidance=" + this.surveyorGuidance
                + ", confidentialityType=" + this.getIdOrNull(this.confidentialityType)
                + ", requireApproval=" + this.isRequireApproval()
                + ", jobScopeConfirmed=" + this.getJobScopeConfirmed()
                + ", sequenceNumber=" + this.getSequenceNumber()
                + ", raisedOnJob=" + this.getIdOrNull(this.getRaisedOnJob())
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
