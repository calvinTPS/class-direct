package com.baesystems.ai.lr.enums;

/*
 * MAST_Ref_AssetItemType contains 650 rows; this is partial enum, containing IDs we refer to within the code
 * Add other values if required
 */
public enum AssetItemType
{
    ESP(610L);

    private final Long value;

    AssetItemType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
