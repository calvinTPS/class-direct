package com.baesystems.ai.lr.dto.base;

import com.baesystems.ai.lr.dto.annotations.ValidId;

/**
 * Reference data DTOs that wrap data that may NOT be edited by the
 * user should extend this class. Strictly speaking this class will
 * not prevent modification of existing reference data but it will prevent
 * the creation of new reference data records due to @ValidId.
 *
 * The @ValidId was originally on BaseReferenceDataDto however some
 * reference data objects needed to become updatable by the user but
 * were being rejected by validation rules.
 */
@ValidId
public class ImmutableReferenceDataDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -74224324241414L;
}
