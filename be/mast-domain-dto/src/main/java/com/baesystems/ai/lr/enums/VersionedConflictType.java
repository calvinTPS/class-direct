package com.baesystems.ai.lr.enums;

/**
 * This enum is used in the ReportEntityConflictService to denote the VERISONED entity type being checked.
 */
public enum VersionedConflictType
{
    MNCN,
    CERTIFICATE,
    ASSET;
}
