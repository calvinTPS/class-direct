package com.baesystems.ai.lr.dto.annotations;

import java.lang.reflect.InvocationTargetException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;

public class AdditionalNullChecksValidator implements ConstraintValidator<AdditionalNullChecks, Object>
{
    private static final String NULL_FIELD_VIOLATION = "The field %s is mandatory for class %s";

    private String[] fieldNames;

    /**
     * Get the field names from the annotation.
     */
    @Override
    public void initialize(final AdditionalNullChecks constraintAnnotation)
    {
        this.fieldNames = constraintAnnotation.fields();
    }

    /**
     * @return true if none of the fields in the list fields on the annotation at the top of the class are null, false
     *         if any of them are null.
     */
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context)
    {
        boolean valid = true;

        for (final String field : this.fieldNames)
        {
            try
            {
                if (PropertyUtils.getProperty(value, field) == null)
                {
                    valid = false;
                    context.buildConstraintViolationWithTemplate(
                            String.format(NULL_FIELD_VIOLATION, field, value.getClass().getSimpleName()))
                            .addConstraintViolation();
                }
            }
            catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
            {
                // The method that this overrides doesn't throw these exceptions so it needs to throw a built in java
                // exception if there is a problem.
                throw new UnsupportedOperationException(exception);
            }
        }

        return valid;
    }
}
