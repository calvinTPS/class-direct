package com.baesystems.ai.lr.dto.certificates;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.dto.base.VersionedDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CertificateDto extends VersionedDto implements UpdateableJobBundleEntity, HashedParentReportingDto
{
    public static final String ENTITY_NAME = "Certificate";
    private static final long serialVersionUID = -447851452971444204L;

    private String stalenessHash;

    @ConflictAware
    private Boolean approved;

    @ConflictAware
    @Size(message = "invalid length", max = 25)
    private String certificateNumber;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CERTIFICATE_STATUSES))
    private LinkResource certificateStatus;

    @NotNull
    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CERTIFICATE_TYPES))
    private LinkResource certificateType;

    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CERTIFICATE_TEMPLATES))
    private LinkResource certificateTemplate;

    @ConflictAware
    @Size(message = "invalid length", max = 10)
    private String issueNumber;

    @ConflictAware
    private Date expiryDate;

    @ConflictAware
    private Date extendedDate;

    @ConflictAware
    private Date issueDate;

    @ConflictAware
    private String issuedAt;

    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    @SubIdNotNull
    private LinkResource employee;

    @NotNull
    @Valid
    private LinkResource job;

    @Valid
    @NotNull
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.SURVEY, allowDeleted = true)
    private LinkResource service;

    @Valid
    @NotNull
    @ConflictAware
    private LinkResource raisedOnJob;

    @Valid
    @ConflictAware
    private LinkResource editedOnJob;

    private Boolean raisedManually;

    @ConflictAware
    private String name;

    private String parentHash;

    private Date sourceJobLastVisitDate;

    private Date sourceJobCreationDateTime;

    @Valid
    @ConflictAware
    private LinkResource certificateFlagState;

    public String getCertificateNumber()
    {
        return this.certificateNumber;
    }

    public void setCertificateNumber(final String certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public LinkResource getCertificateStatus()
    {
        return this.certificateStatus;
    }

    public void setCertificateStatus(final LinkResource certificateStatus)
    {
        this.certificateStatus = certificateStatus;
    }

    public LinkResource getCertificateType()
    {
        return this.certificateType;
    }

    public void setCertificateType(final LinkResource certificateType)
    {
        this.certificateType = certificateType;
    }

    public LinkResource getCertificateTemplate()
    {
        return this.certificateTemplate;
    }

    public void setCertificateTemplate(final LinkResource certificateTemplate)
    {
        this.certificateTemplate = certificateTemplate;
    }

    public Date getExpiryDate()
    {
        return this.expiryDate;
    }

    public void setExpiryDate(final Date expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public Date getExtendedDate()
    {
        return this.extendedDate;
    }

    public void setExtendedDate(final Date extendedDate)
    {
        this.extendedDate = extendedDate;
    }

    public Date getIssueDate()
    {
        return this.issueDate;
    }

    public void setIssueDate(final Date issueDate)
    {
        this.issueDate = issueDate;
    }

    public String getIssuedAt()
    {
        return this.issuedAt;
    }

    public void setIssuedAt(final String issuedAt)
    {
        this.issuedAt = issuedAt;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public LinkResource getService()
    {
        return this.service;
    }

    public void setService(final LinkResource service)
    {
        this.service = service;
    }

    public LinkResource getRaisedOnJob()
    {
        return this.raisedOnJob;
    }

    public void setRaisedOnJob(final LinkResource raisedOnJob)
    {
        this.raisedOnJob = raisedOnJob;
    }

    public LinkResource getEditedOnJob()
    {
        return this.editedOnJob;
    }

    public void setEditedOnJob(final LinkResource editedOnJob)
    {
        this.editedOnJob = editedOnJob;
    }

    public Boolean getRaisedManually()
    {
        return this.raisedManually;
    }

    public void setRaisedManually(final Boolean raisedManually)
    {
        this.raisedManually = raisedManually;
    }

    public String getIssueNumber()
    {
        return this.issueNumber;
    }

    public void setIssueNumber(final String issueNumber)
    {
        this.issueNumber = issueNumber;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    @JsonProperty
    public Boolean getApproved()
    {
        return this.approved;
    }

    @JsonIgnore
    public void setApproved(final Boolean approved)
    {
        this.approved = approved;
    }

    @Override
    public String getParentHash()
    {
        return this.parentHash;
    }

    public void setParentHash(final String parentHash)
    {
        this.parentHash = parentHash;
    }

    @Override
    public Date getSourceJobLastVisitDate()
    {
        return this.sourceJobLastVisitDate;
    }

    @Override
    public void setSourceJobLastVisitDate(final Date sourceJobLastVisitDate)
    {
        this.sourceJobLastVisitDate = sourceJobLastVisitDate;
    }

    @Override
    public Date getSourceJobCreationDateTime()
    {
        return this.sourceJobCreationDateTime;
    }

    @Override
    public void setSourceJobCreationDateTime(final Date sourceJobCreationDateTime)
    {
        this.sourceJobCreationDateTime = sourceJobCreationDateTime;
    }

    public LinkResource getCertificateFlagState()
    {
        return this.certificateFlagState;
    }

    public void setCertificateFlagState(final LinkResource certificateFlagState)
    {
        this.certificateFlagState = certificateFlagState;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CertificateDto [getApproved()=" + getApproved()
                + "getCertificateNumber()=" + getCertificateNumber()
                + "getCertificateStatus()=" + getIdOrNull(getCertificateStatus())
                + "getCertificateType()=" + getIdOrNull(getCertificateType())
                + "getCertificateTemplate()=" + getIdOrNull(getCertificateTemplate())
                + "getExpiryDate()=" + getStringRepresentationOfDate(getExpiryDate())
                + "getExtendedDate()=" + getStringRepresentationOfDate(getExtendedDate())
                + "getIssueDate()=" + getStringRepresentationOfDate(getIssueDate())
                + "getIssuedAt()=" + getIssuedAt()
                + "getEmployee()=" + getIdOrNull(getEmployee())
                + "getService()=" + getIdOrNull(getService())
                + "getRaisedOnJob()=" + getIdOrNull(getRaisedOnJob())
                + "getEditedOnJob()=" + getIdOrNull(getEditedOnJob())
                + "getId()=" + getId()
                + "getVersionId()=" + getVersionId()
                + "getVersionType()=" + getVersionType()
                + "getIssueNumber()=" + getIssueNumber()
                + "getName()=" + getName()
                + "getCertificateFlagState()=" + getIdOrNull(getCertificateFlagState())
                + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }

}
