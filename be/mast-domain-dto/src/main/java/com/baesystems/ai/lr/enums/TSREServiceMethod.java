package com.baesystems.ai.lr.enums;

public enum TSREServiceMethod
{
    SCHEDULE_NEXT_TASKS("scheduleServiceNextTasks"),
    SCHEDULE_TASKS("scheduleTasks");

    private final String methodName;

    private TSREServiceMethod(final String methodName)
    {
        this.methodName = methodName;
    }

    public String getMethodName()
    {
        return methodName;
    }
}
