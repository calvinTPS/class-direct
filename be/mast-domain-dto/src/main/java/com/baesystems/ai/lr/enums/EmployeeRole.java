package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.List;

public enum EmployeeRole
{
    BUSINESS_SUPPORT(1L),
    SURVEYOR_ASSESSOR_INSPECTOR(2L),
    MANAGEMENT(3L),
    SALES_CLIENT_RELATIONSHIP_MANAGEMENT(4L),
    SPECIALIST_LEADERSHIP(5L),
    AUTHORISING_SURVEYOR(6L),
    EIC_CASE_SURVEYOR(7L),
    EIC_ADMIN(8L),
    SDO(9L),
    EIC_MANAGER(10L),
    LEAD_SURVEYOR(11L),
    SDO_COORDINATOR(12L);

    private final Long value;

    EmployeeRole(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    public static List<Long> getMandatoryRolesForCase()
    {
        final List<Long> mandatoryRoles = new ArrayList<Long>();

        mandatoryRoles.add(EIC_ADMIN.getValue());
        mandatoryRoles.add(MANAGEMENT.getValue());

        return mandatoryRoles;
    }
}
