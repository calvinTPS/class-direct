package com.baesystems.ai.lr.dto.cases;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class CaseMilestoneListDto extends BaseDto
{
    private static final long serialVersionUID = 1585009422622839784L;

    @Valid
    @NotNull
    private List<CaseMilestoneDto> caseMilestoneList;

    public List<CaseMilestoneDto> getCaseMilestoneList()
    {
        return caseMilestoneList;
    }

    public void setCaseMilestoneList(final List<CaseMilestoneDto> caseMilestoneList)
    {
        this.caseMilestoneList = caseMilestoneList;
    }
}
