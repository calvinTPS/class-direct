package com.baesystems.ai.lr.dto.base;

public interface IdDto extends Dto
{
    Long getId();

    void setId(final Long id);

    String getInternalId();

    void setInternalId(final String internalId);

    String getTrueInternalId();

}
