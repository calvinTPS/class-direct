package com.baesystems.ai.lr.enums;

public enum RouteType
{
    PUT,
    POST,
    DELETE;
}
