package com.baesystems.ai.lr.dto.annotations;

import java.lang.reflect.InvocationTargetException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;

public class RequiredWithFieldsValidator implements ConstraintValidator<RequiredWithFields, Object>
{
    private static final String FIELD_SHOULD_NOT_BE_NULL = "Field '%s' cannot be null when the field '%s' is populated";
    private static final String FIELD_SHOULD_BE_NULL = "Field '%s' must be null when the field '%s' is populated";

    private String[] fieldNames;
    private String keyFieldName;
    private boolean shouldBePopulated;

    /**
     * Get the field names from the annotation.
     */
    @Override
    public void initialize(final RequiredWithFields constraintAnnotation)
    {
        this.fieldNames = constraintAnnotation.fields();
        this.keyFieldName = constraintAnnotation.keyField();
        this.shouldBePopulated = constraintAnnotation.shouldBePopulated();
    }

    /**
     * Method for testing that a list of fields are either populated or not, depending on the value of
     * shouldBePopulated, given that that key field is populated.
     */
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context)
    {
        boolean valid = true;

        try
        {
            if (PropertyUtils.getProperty(value, keyFieldName) != null)
            {
                for (final String field : fieldNames)
                {
                    if (PropertyUtils.getProperty(value, field) == null && shouldBePopulated)
                    {
                        valid = false;
                        context.buildConstraintViolationWithTemplate(
                                String.format(FIELD_SHOULD_NOT_BE_NULL, field, keyFieldName))
                                .addConstraintViolation();
                    }
                    else if (PropertyUtils.getProperty(value, field) != null && !shouldBePopulated)
                    {
                        valid = false;
                        context.buildConstraintViolationWithTemplate(
                                String.format(FIELD_SHOULD_BE_NULL, field, keyFieldName))
                                .addConstraintViolation();
                    }
                }
            }

        }
        catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            // The method that this overrides doesn't throw these exceptions so it needs to throw a built in java
            // exception if there is a problem.
            final UnsupportedOperationException newException = new UnsupportedOperationException();
            newException.initCause(exception);
            throw newException;
        }

        return valid;
    }
}
