package com.baesystems.ai.lr.dto.references;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ItemTypeDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -5602101211457248064L;

    @Valid
    private LinkResource itemClass;

    @Size(message = "invalid length", max = 100)
    private String name;

    private String description;

    private Date startDate;

    private Date endDate;

    private String section;

    public LinkResource getItemClass()
    {
        return itemClass;
    }

    public void setItemClass(final LinkResource itemClass)
    {
        this.itemClass = itemClass;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
   {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(final Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(final Date endDate)
    {
        this.endDate = endDate;
    }

    public String getSection()
    {
        return section;
    }

    public void setSection(final String section)
    {
        this.section = section;
    }
}
