package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsHileDto extends IhsBaseDto
{
    private static final long serialVersionUID = 3367064303625786385L;

    private String lengthBtwPerpendiculars;

    public String getLengthBtwPerpendiculars()
    {
        return lengthBtwPerpendiculars;
    }

    public void setLengthBtwPerpendiculars(final String lengthBtwPerpendiculars)
    {
        this.lengthBtwPerpendiculars = lengthBtwPerpendiculars;
    }
}
