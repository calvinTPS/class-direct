package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

// todo // FIXME: 22/11/2017 - I have no idea why this class exists, it's sibling looks identical
public class WorkItemModelItemDto extends WorkItemPreviewCommonDto<WorkItemModelItemDto>
{
    private static final long serialVersionUID = -5733298535097323305L;

    private List<WorkItemLightDto> tasks;

    public List<WorkItemLightDto> getTasks()
    {
        return tasks;
    }

    public void setTasks(final List<WorkItemLightDto> tasks)
    {
        this.tasks = tasks;
    }

}
