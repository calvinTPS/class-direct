package com.baesystems.ai.lr.dto.jobs;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

public class PR17ListDto implements Serializable
{
    private static final long serialVersionUID = 2943588433766640618L;

    @NotNull
    private List<PR17Dto> pr17List;

    public List<PR17Dto> getPr17List()
    {
        return pr17List;
    }

    public void setPr17List(final List<PR17Dto> pr17List)
    {
        this.pr17List = pr17List;
    }
}
