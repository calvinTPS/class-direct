package com.baesystems.ai.lr.dto.references;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class DefectValueDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 3519586990228540293L;

    @Size(message = "invalid length", max = 100)
    private String name;

    @Valid
    private ReferenceDataDto severity;

    @Valid
    private List<FaultCategoryDto> defectCategories;

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public ReferenceDataDto getSeverity()
    {
        return this.severity;
    }

    public void setSeverity(final ReferenceDataDto severity)
    {
        this.severity = severity;
    }

    public List<FaultCategoryDto> getDefectCategories()
    {
        return this.defectCategories;
    }

    public void setDefectCategories(final List<FaultCategoryDto> defectCategories)
    {
        this.defectCategories = defectCategories;
    }
}
