package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * To be used on fields of the AssetLightClass that are part of the RegisterBook.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RegisterBookField
{
    String[] ignoreFields() default "";
}
