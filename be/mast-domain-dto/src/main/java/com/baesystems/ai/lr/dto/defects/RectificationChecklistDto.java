package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@IgnoreIdsInConflicts
public class RectificationChecklistDto extends BaseDto
{
    private static final long serialVersionUID = -2332400826791660350L;

    @NotNull
    @Valid
    @ConflictAware
    private DeficiencyChecklistLinkDto deficiencyChecklist;

    @Valid
    @ConflictAware
    private LinkResource parent;

    // This is effectively the 'child', when this DTO is used in non-wip form
    @JsonIgnore
    private LinkResource wipRectificationChecklist;

    public DeficiencyChecklistLinkDto getDeficiencyChecklist()
    {
        return this.deficiencyChecklist;
    }

    public void setDeficiencyChecklist(final DeficiencyChecklistLinkDto deficiencyChecklist)
    {
        this.deficiencyChecklist = deficiencyChecklist;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public LinkResource getWipRectificationChecklist()
    {
        return this.wipRectificationChecklist;
    }

    public void setWipRectificationChecklist(final LinkResource wipRectificationChecklist)
    {
        this.wipRectificationChecklist = wipRectificationChecklist;
    }
}
