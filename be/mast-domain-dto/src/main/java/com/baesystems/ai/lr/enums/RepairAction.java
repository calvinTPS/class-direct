package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.List;

public enum RepairAction
{
    PERMANENT(1L),
    TEMPORARY(2L),
    MODIFICATION(3L);

    private final Long value;

    RepairAction(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }

    /**
     * @return a list of RepairAction values for use when querying the database for the number of repairs associated
     *         with a defect.
     */
    public static final List<Long> getRepairsForDefect()
    {
        final ArrayList<Long> actions = new ArrayList<Long>();

        actions.add(PERMANENT.value());
        actions.add(TEMPORARY.value());

        return actions;
    }
}
