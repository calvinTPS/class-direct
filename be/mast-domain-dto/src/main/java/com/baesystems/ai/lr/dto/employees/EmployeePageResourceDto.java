package com.baesystems.ai.lr.dto.employees;

import com.baesystems.ai.lr.dto.paging.BasePageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

import java.util.List;

public class EmployeePageResourceDto extends BasePageResource<LrEmployeeDto>
{
    private List<LrEmployeeDto> content;

    @Override
    public List<LrEmployeeDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<LrEmployeeDto> content)
    {
        this.content = content;
    }
}
