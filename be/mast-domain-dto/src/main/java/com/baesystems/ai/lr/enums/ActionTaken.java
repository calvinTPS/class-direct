package com.baesystems.ai.lr.enums;

/**
 * Enumeration matching MAST_REF_ActionTaken DB table.
 */
public enum ActionTaken
{
    RAISED(1L),
    CLOSED(2L),
    AMENDED(3L);

    private final Long value;

    ActionTaken(final Long value)
    {
        this.value = value;
    }

    public Long getValue()
    {
        return this.value;
    }
}
