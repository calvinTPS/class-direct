package com.baesystems.ai.lr.dto.base;

/**
 * Base class for all reference data DTOs.
 *
 * Specific reference data classes should not extend this class directly
 * but one of its immediate descendants dependent upon whether the data
 * wrapped can be edited by the user (rather than through the traditional
 * reference data drop/upload).
 */
public class BaseReferenceDataDto extends BaseDto
{
    private static final long serialVersionUID = -7700260036236546617L;

    private Boolean deleted;

    private Long lastUpdateVersion;

    public Boolean getDeleted()
    {
        return deleted;
    }

    public void setDeleted(final Boolean deleted)
    {
        this.deleted = deleted;
    }

    public Long getLastUpdateVersion()
    {
        return this.lastUpdateVersion;
    }

    public void setLastUpdateVersion(final Long lastUpdateVersion)
    {
        this.lastUpdateVersion = lastUpdateVersion;
    }
}
