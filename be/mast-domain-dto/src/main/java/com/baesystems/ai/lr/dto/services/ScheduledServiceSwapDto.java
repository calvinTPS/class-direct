package com.baesystems.ai.lr.dto.services;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.Dto;

public class ScheduledServiceSwapDto implements Serializable, Dto
{
    private static final long serialVersionUID = -4923600357587239139L;

    @NotNull
    private LinkResource newServiceCatalogue;

    @NotNull
    @Size(message = "invalid length", max = 150)
    private String swapReason;

    public LinkResource getNewServiceCatalogue()
    {
        return newServiceCatalogue;
    }

    public void setNewServiceCatalogue(final LinkResource newServiceCatalogue)
    {
        this.newServiceCatalogue = newServiceCatalogue;
    }

    public String getSwapReason()
    {
        return swapReason;
    }

    public void setSwapReason(final String swapReason)
    {
        this.swapReason = swapReason;
    }
}
