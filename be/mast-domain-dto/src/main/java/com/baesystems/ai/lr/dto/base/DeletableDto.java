package com.baesystems.ai.lr.dto.base;

import com.baesystems.ai.lr.dto.annotations.ConflictAware;

public class DeletableDto extends BaseDto
{
    private static final long serialVersionUID = -7382431817051371773L;

    @ConflictAware
    private boolean deleted;

    public boolean isDeleted()
    {
        return this.deleted;
    }

    public void setDeleted(final boolean deleted)
    {
        this.deleted = deleted;
    }
}
