package com.baesystems.ai.lr.dto.tasks;

import java.util.Map;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class WorkItemAssetMapDto extends BaseDto
{
    private static final long serialVersionUID = -8346279237494196240L;

    private Map<Long, WorkItemPublishedVersionDto> assetWorkItemsMap;

    public Map<Long, WorkItemPublishedVersionDto> getAssetWorkItemsMap()
    {
        return assetWorkItemsMap;
    }

    public void setAssetWorkItemsMap(final Map<Long, WorkItemPublishedVersionDto> assetWorkItemsMap)
    {
        this.assetWorkItemsMap = assetWorkItemsMap;
    }
}
