package com.baesystems.ai.lr.dto.references;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class MilestoneDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -7711513796464926581L;

    @Size(message = "invalid length", max = 256)
    private String description;

    private Long displayOrder;

    @Valid
    private LinkResource caseType;

    @Valid
    private LinkResource dueDateReference;

    @Valid
    private LinkResource changeCaseStatusTo;

    @Valid
    private List<LinkResource> predecessorMilestones;

    @Valid
    private Boolean mandatory;

    @Valid
    private Boolean includeOnSummaryReport;

    @Valid
    private Integer identifier;

    @Valid
    private String shortDescription;

    @Valid
    private String changeClassStatus;

    @Valid
    private ReferenceDataDto owner;

    @Valid
    private String comments;

    @Valid
    private Integer workingDaysOffset;

    @Valid
    private List<LinkResource> childMilestones;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Long getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public LinkResource getCaseType()
    {
        return caseType;
    }

    public void setCaseType(final LinkResource caseType)
    {
        this.caseType = caseType;
    }

    public List<LinkResource> getPredecessorMilestones()
    {
        return predecessorMilestones;
    }

    public void setPredecessorMilestones(final List<LinkResource> predecessorMilestones)
    {
        this.predecessorMilestones = predecessorMilestones;
    }

    public LinkResource getDueDateReference()
    {
        return dueDateReference;
    }

    public void setDueDateReference(final LinkResource dueDateReference)
    {
        this.dueDateReference = dueDateReference;
    }

    public LinkResource getChangeCaseStatusTo()
    {
        return changeCaseStatusTo;
    }

    public void setChangeCaseStatusTo(final LinkResource changeCaseStatusTo)
    {
        this.changeCaseStatusTo = changeCaseStatusTo;
    }

    public Boolean getMandatory()
    {
        return mandatory;
    }

    public void setMandatory(final Boolean mandatory)
    {
        this.mandatory = mandatory;
    }

    public Boolean getIncludeOnSummaryReport()
    {
        return includeOnSummaryReport;
    }

    public void setIncludeOnSummaryReport(final Boolean includeOnSummaryReport)
    {
        this.includeOnSummaryReport = includeOnSummaryReport;
    }

    public Integer getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(final Integer identifier)
    {
        this.identifier = identifier;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setShortDescription(final String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    public String getChangeClassStatus()
    {
        return changeClassStatus;
    }

    public void setChangeClassStatus(final String changeClassStatus)
    {
        this.changeClassStatus = changeClassStatus;
    }

    public ReferenceDataDto getOwner()
    {
        return owner;
    }

    public void setOwner(final ReferenceDataDto owner)
    {
        this.owner = owner;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(final String comments)
    {
        this.comments = comments;
    }

    public Integer getWorkingDaysOffset()
    {
        return workingDaysOffset;
    }

    public void setWorkingDaysOffset(final Integer workingDaysOffset)
    {
        this.workingDaysOffset = workingDaysOffset;
    }

    public List<LinkResource> getChildMilestones()
    {
        return childMilestones;
    }

    public void setChildMilestones(final List<LinkResource> childMilestones)
    {
        this.childMilestones = childMilestones;
    }
}
