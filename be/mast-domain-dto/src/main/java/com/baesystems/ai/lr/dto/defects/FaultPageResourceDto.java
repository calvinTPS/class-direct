package com.baesystems.ai.lr.dto.defects;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class FaultPageResourceDto extends BasePageResource<FaultDto>
{
    private List<FaultDto> content;

    @Override
    public List<FaultDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<FaultDto> content)
    {
        this.content = content;
    }

}
