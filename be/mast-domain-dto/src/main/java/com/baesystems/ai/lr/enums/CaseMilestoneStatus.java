package com.baesystems.ai.lr.enums;

public enum CaseMilestoneStatus
{
    OPEN(1L),
    COMPLETE(2L);

    private final Long value;

    CaseMilestoneStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
