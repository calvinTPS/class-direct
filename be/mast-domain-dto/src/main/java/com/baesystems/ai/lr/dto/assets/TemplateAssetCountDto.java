package com.baesystems.ai.lr.dto.assets;

import java.util.Map;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class TemplateAssetCountDto extends BaseDto
{
    private static final long serialVersionUID = -306971763243734397L;

    /* Key = Template ID, Value = Asset Count */
    private Map<Long, Integer> counts;

    public Map<Long, Integer> getCounts()
    {
        return counts;
    }

    public void setCounts(final Map<Long, Integer> counts)
    {
        this.counts = counts;
    }
}
