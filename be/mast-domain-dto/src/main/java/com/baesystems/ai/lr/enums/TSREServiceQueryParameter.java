package com.baesystems.ai.lr.enums;

public enum TSREServiceQueryParameter
{
    SERVICE_ID("serviceId"),
    TASK_IDS("taskIds");

    private final String parameterName;

    private TSREServiceQueryParameter(final String parameterName)
    {
        this.parameterName = parameterName;
    }

    public String getParameterName()
    {
        return this.parameterName;
    }
}
