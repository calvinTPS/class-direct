package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class TeamOfficeDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -9420343487388024L;

    private LinkResource office;

    private LinkResource team;

    public LinkResource getOffice()
    {
        return office;
    }

    public void setOffice(final LinkResource office)
    {
        this.office = office;
    }

    public LinkResource getTeam()
    {
        return team;
    }

    public void setTeam(final LinkResource team)
    {
        this.team = team;
    }
}
