package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation for fields on reference data DOs that indicate that the list
 * of embedded DOs also need to be upversioned so the cache doesn't go stale.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface CascadeReferenceDataUpversion
{

}
