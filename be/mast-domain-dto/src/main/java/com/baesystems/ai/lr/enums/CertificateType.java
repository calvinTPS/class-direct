package com.baesystems.ai.lr.enums;

public enum CertificateType
{
    FULL(1L),
    CONDITIONAL(2L),
    INTERIM(3L);

    private final Long value;

    CertificateType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
