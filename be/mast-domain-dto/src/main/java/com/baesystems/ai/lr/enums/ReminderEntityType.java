package com.baesystems.ai.lr.enums;

public enum ReminderEntityType
{
    SERVICE(1L),
    TASK(2L),
    COC(3L);

    private final Long value;

    ReminderEntityType(final Long value)
    {
        this.value = value;
    }

    public Long getValue()
    {
        return this.value;
    }
}
