package com.baesystems.ai.lr.dto.customers;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.util.List;


public class PartyPageResourceDto extends BasePageResource<PartyDto>
{
    private List<PartyDto> content;

    @Override
    public List<PartyDto> getContent()
    {
        return this.content;
    }

    @Override
    public void setContent(final List<PartyDto> content)
    {
        this.content = content;
    }
}
