package com.baesystems.ai.lr.enums;

public enum FeeReportStatus
{
    INCOMPLETE(0),
    COMPLETE(1),
    COMPLETEWITHCHANGES(2);

    private final int value;

    private FeeReportStatus(int value)
    {
        this.value = value;
    }

    public final int getValue()
    {
        return value;
    }

    public static boolean contains(int test)
    {

        for (FeeReportStatus c : FeeReportStatus.values())
        {
            if (c.value == test)
            {
                return true;
            }
        }
        return false;
    }
}
