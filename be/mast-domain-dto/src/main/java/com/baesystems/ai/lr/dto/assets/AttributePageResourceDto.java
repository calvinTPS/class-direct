package com.baesystems.ai.lr.dto.assets;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.util.List;

public class AttributePageResourceDto extends BasePageResource<AttributeDto>
{
    private List<AttributeDto> content;

    @Override
    public List<AttributeDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<AttributeDto> content)
    {
        this.content = content;
    }
}
