package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.CodeLinkResource;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;

@IgnoreIdsInConflicts
public class DeficiencyChecklistLinkDto extends BaseDto
{
    private static final long serialVersionUID = 8777359168920099884L;

    @Valid
    private CodeLinkResource workItem;

    @Valid
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_TASK)
    private CodeLinkResource wipWorkItem;

    private Long checklistId;

    public CodeLinkResource getWorkItem()
    {
        return this.workItem;
    }

    public void setWorkItem(final CodeLinkResource workItem)
    {
        this.workItem = workItem;
    }

    public CodeLinkResource getWipWorkItem()
    {
        return this.wipWorkItem;
    }

    public void setWipWorkItem(final CodeLinkResource wipWorkItem)
    {
        this.wipWorkItem = wipWorkItem;
    }

    public Long getChecklistId()
    {
        return this.checklistId;
    }

    public void setChecklistId(final Long checklistId)
    {
        this.checklistId = checklistId;
    }
}
