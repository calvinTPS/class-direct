package com.baesystems.ai.lr.dto.ihs;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.base.Dto;

/*
 * This dto contains YARD information. YARD = decodeA + decodeB.
 */
public class IhsCbsbDto implements Dto, Serializable
{
    private static final long serialVersionUID = -7972526385470048958L;

    private String decodeA;

    private String decodeB;

    public String getDecodeA()
    {
        return this.decodeA;
    }

    public void setDecodeA(final String decodeA)
    {
        this.decodeA = decodeA;
    }

    public String getDecodeB()
    {
        return this.decodeB;
    }

    public void setDecodeB(final String decodeB)
    {
        this.decodeB = decodeB;
    }

}
