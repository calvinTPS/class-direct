package com.baesystems.ai.lr.enums;

public enum ReadingCategory
{
    REPORTED(1L),
    REPORTED_PREVIOUS(2L),
    COMPARATIVE(3L);

    private final Long value;

    private ReadingCategory(final Long value)
    {
        this.value = value;
    }

    public Long getValue()
    {
        return this.value;
    }

}
