package com.baesystems.ai.lr.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class FollowUpActionPageResourceDto extends BasePageResource<FollowUpActionDto>
{
    private List<FollowUpActionDto> content;

    @Override
    public List<FollowUpActionDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<FollowUpActionDto> content)
    {
        this.content = content;
    }
}
