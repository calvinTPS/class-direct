package com.baesystems.ai.lr.dto.assets;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class AttributeDto extends BaseDto
{
    private static final long serialVersionUID = -7717416265213624554L;

    @Size(message = "invalid length", max = 250)
    private String value;

    @Valid
    private LinkResource lov;

    @NotNull
    @Valid
    private LinkResource attributeType;

    public LinkResource getAttributeType()
    {
        return attributeType;
    }

    public void setAttributeType(final LinkResource attributeType)
    {
        this.attributeType = attributeType;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }

    public LinkResource getLov()
    {
        return lov;
    }

    public void setLov(final LinkResource lov)
    {
        this.lov = lov;
    }
}
