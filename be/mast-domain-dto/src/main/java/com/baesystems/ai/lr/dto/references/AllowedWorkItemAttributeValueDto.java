package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class AllowedWorkItemAttributeValueDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 4951251818871426915L;

    private String value;

    private Integer displayOrder;

    private Boolean isFollowOnTrigger;

    private LinkResource allowedValueGroup;

    private LinkResource workItemConditionalAttribute;

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }

    public Boolean getIsFollowOnTrigger()
    {
        return isFollowOnTrigger;
    }

    public void setIsFollowOnTrigger(final Boolean isFollowOnTrigger)
    {
        this.isFollowOnTrigger = isFollowOnTrigger;
    }

    public LinkResource getAllowedValueGroup()
    {
        return allowedValueGroup;
    }

    public void setAllowedValueGroup(final LinkResource allowedValueGroup)
    {
        this.allowedValueGroup = allowedValueGroup;
    }

    public LinkResource getWorkItemConditionalAttribute()
    {
        return workItemConditionalAttribute;
    }

    public void setWorkItemConditionalAttribute(final LinkResource workItemConditionalAttribute)
    {
        this.workItemConditionalAttribute = workItemConditionalAttribute;
    }
}
