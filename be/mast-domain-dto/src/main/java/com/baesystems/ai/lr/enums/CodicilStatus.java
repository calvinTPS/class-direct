package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * CodicilStatusType enum matching MAST_REF_CodicilStatus DB table.
 */
public enum CodicilStatus
{
    UNKNOWN(0L),
    AI_DRAFT(1L),
    AI_INACTIVE(2L),
    AI_OPEN(3L),
    AI_CHANGE_RECOMMENDED(4L),
    AI_CLOSED(5L),
    AI_CONTINUES_SATISFACTORY(6L),
    AI_CANCELLED(7L),
    AN_DRAFT(8L),
    AN_INACTIVE(9L),
    AN_OPEN(10L),
    AN_CHANGE_RECOMMENDED(11L),
    AN_CLOSED(12L),
    AN_CANCELLED(13L),
    COC_OPEN(14L),
    COC_CLOSED(15L),
    COC_CANCELLED(16L),
    SF_OPEN(23L),
    SF_CLOSED(24L);

    private final Long value;

    private static final Map<Long, CodicilStatus> CODICIL_STATUS_TYPE_MAP = Arrays
            .stream(CodicilStatus.values())
            .collect(Collectors.toMap(CodicilStatus::getValue, Function.identity()));

    CodicilStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }

    /**
     * @return a list of CodicilStatusType values that count as open
     */
    public static final List<Long> getCodicilStatusesForOpen()
    {
        return Arrays.asList(
                AI_OPEN.getValue(),
                AN_OPEN.getValue(),
                COC_OPEN.getValue(),
                AN_INACTIVE.getValue(),
                AI_INACTIVE.getValue(),
                AN_CHANGE_RECOMMENDED.getValue(),
                AI_CHANGE_RECOMMENDED.getValue(),
                SF_OPEN.getValue());
    }

    /**
     * @return a list of CodicilStatusType values that are valid for courses of action for a CoC
     */
    public static final List<Long> getCoCStatusesForCourseOfAction()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(COC_OPEN.getValue());

        return statuses;
    }

    /**
     * @return a list of CodicilStatusType values that are valid for courses of action for an Actionable Item
     */
    public static final List<Long> getActionableItemStatusesForCourseOfAction()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(AI_OPEN.getValue());
        statuses.add(AI_INACTIVE.getValue());

        return statuses;
    }

    /**
     * Returns the OPEN enum for the specified Codicil type or UNKNOWN
     *
     * @param codicilType
     * @return OPEN or UNKNOWN if OPEN is an invalid status for the specified CodicilType.
     */
    public static CodicilStatus getOpen(final CodicilType codicilType)
    {
        CodicilStatus open;
        switch (codicilType)
        {
            case AI:
                open = CodicilStatus.AI_OPEN;
                break;
            case AN:
                open = CodicilStatus.AN_OPEN;
                break;
            case COC:
                open = CodicilStatus.COC_OPEN;
                break;
            case SF:
                open = CodicilStatus.SF_OPEN;
                break;
            default:
                open = CodicilStatus.UNKNOWN;
                break;
        }
        return open;
    }

    /**
     * Returns the CLOSED enum for the specified Codicil type or UNKNOWN
     *
     * @param codicilType
     * @return CLOSED or UNKNOWN if OPEN is an invalid status for the specified CodicilType.
     */
    public static CodicilStatus getClosed(final CodicilType codicilType)
    {
        CodicilStatus open;
        switch (codicilType)
        {
            case AI:
                open = CodicilStatus.AI_CLOSED;
                break;
            case AN:
                open = CodicilStatus.AN_CLOSED;
                break;
            case COC:
                open = CodicilStatus.COC_CLOSED;
                break;
            case SF:
                open = CodicilStatus.SF_CLOSED;
                break;
            default:
                open = CodicilStatus.UNKNOWN;
                break;
        }
        return open;
    }

    /**
     * Returns the DRAFT Enum for the specified Codicil or UNKNOWN.
     *
     * @param codicilType
     * @return DRAFT or UNKNOWN if DRAFT is invalid for the specified CodicilType
     */
    public static CodicilStatus getDraft(final CodicilType codicilType)
    {
        CodicilStatus draft;
        switch (codicilType)
        {
            case AI:
                draft = CodicilStatus.AI_DRAFT;
                break;
            case AN:
                draft = CodicilStatus.AN_DRAFT;
                break;
            case COC:
            default:
                draft = CodicilStatus.UNKNOWN;
                break;
        }
        return draft;

    }

    /**
     * Returns the INACTIVE Enum for the specified CodicilType or UNKNOWN
     *
     * @param codicilType
     * @return INACTIVE or UNKNOWN if INACTIVE is an invalid status for the specified CodicilType.
     */
    public static CodicilStatus getInactive(final CodicilType codicilType)
    {
        CodicilStatus inactive;
        switch (codicilType)
        {
            case AI:
                inactive = CodicilStatus.AI_INACTIVE;
                break;
            case AN:
                inactive = CodicilStatus.AN_INACTIVE;
                break;
            case COC:
            default:
                inactive = CodicilStatus.UNKNOWN;
                break;
        }
        return inactive;
    }

    /**
     * Returns the enum for the specified value or UNKNOWN no mapping exists.
     *
     * @param value
     * @return the Enum mapped to value or UNKNOWN if no such mapping exists.
     */
    public static CodicilStatus getEnum(final Long value)
    {
        CodicilStatus codicilStatusType = UNKNOWN;
        if (value != null)
        {
            codicilStatusType = CODICIL_STATUS_TYPE_MAP.getOrDefault(value, UNKNOWN);
        }
        return codicilStatusType;
    }

    public static List<Long> getOpenAndInactiveForAssetNote()
    {
        return Arrays.asList(
                AN_OPEN.getValue(),
                AN_INACTIVE.getValue());
    }

    public static List<Long> getOpenAndInactiveForActionableItem()
    {
        return Arrays.asList(
                AI_OPEN.getValue(),
                AI_INACTIVE.getValue());
    }

    /**
     * @return a list of CodicilStatusType values that are valid for courses of action for a Statutory Finding
     */
    public static final List<Long> getStatutoryFindingStatusesForCourseOfAction()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(SF_OPEN.getValue());

        return statuses;
    }
}
