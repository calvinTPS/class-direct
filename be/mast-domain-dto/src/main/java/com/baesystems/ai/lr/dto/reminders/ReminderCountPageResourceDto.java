package com.baesystems.ai.lr.dto.reminders;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class ReminderCountPageResourceDto extends BasePageResource<ReminderCountDto>
{

    @Valid
    List<ReminderCountDto> content;

    @Override
    public List<ReminderCountDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<ReminderCountDto> content)
    {
        this.content = content;
    }
}
