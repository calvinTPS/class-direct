package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.List;

public enum AssetLifecycleStatus
{
    UNDER_CONSTRUCTION(1L),
    IN_SERVICE(3L),
    LAID_UP(4L),
    UNDER_REPAIR(5L),
    IN_CASUALTY(6L),
    CONVERTING(7L),
    DECOMMISSIONED(9L),
    CANCELLED(11L),
    SUNK(12L);

    private final Long value;

    AssetLifecycleStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    public static final List<Long> getStatusesForTemplateCount()
    {
        final List<Long> statuses = new ArrayList<Long>();

        statuses.add(UNDER_CONSTRUCTION.getValue());
        statuses.add(IN_SERVICE.getValue());
        statuses.add(LAID_UP.getValue());
        statuses.add(UNDER_REPAIR.getValue());
        statuses.add(IN_CASUALTY.getValue());
        statuses.add(CONVERTING.getValue());

        return statuses;
    }
}
