package com.baesystems.ai.lr.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.Dto;

public class IdListDto implements Dto, Serializable
{
    private static final long serialVersionUID = -4831522152134048706L;

    @NotNull
    private List<Long> ids;

    public List<Long> getIds()
    {
        return ids;
    }

    public void setIds(final List<Long> ids)
    {
        this.ids = ids;
    }
}
