package com.baesystems.ai.lr.dto.query;

import java.util.List;

public class RepairQueryDto implements QueryDto
{
    private static final long serialVersionUID = 1991025821266709832L;

    private List<Long> cocId;
    private List<Long> defectId;

    public List<Long> getCocId()
    {
        return cocId;
    }

    public void setCocId(final List<Long> cocId)
    {
        this.cocId = cocId;
    }

    public List<Long> getDefectId()
    {
        return defectId;
    }

    public void setDefectId(final List<Long> defectId)
    {
        this.defectId = defectId;
    }
}
