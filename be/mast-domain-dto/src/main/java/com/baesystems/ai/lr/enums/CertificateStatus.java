package com.baesystems.ai.lr.enums;

public enum CertificateStatus
{
    ISSUED(1L),
    WITHDRAWN(2L),
    EXPIRED(3L);

    private final Long value;

    CertificateStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
