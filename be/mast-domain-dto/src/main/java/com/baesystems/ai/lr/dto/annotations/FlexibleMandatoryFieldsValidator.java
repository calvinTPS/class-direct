package com.baesystems.ai.lr.dto.annotations;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;

import com.baesystems.ai.lr.dto.LinkResource;

public class FlexibleMandatoryFieldsValidator implements ConstraintValidator<FlexibleMandatoryFields, Object>
{
    private static final String MANDATORY_FIELD_VIOLATION = "The field %s is mandatory when %s is in state with id %d";

    private String[] mandatoryFieldNames;
    private String modeFieldName;
    private String secondaryModeFieldName;
    private List<Long> profileIds;
    private List<Long> secondaryProfileIds;

    private List<Long> getprofileIdList(final long[] profileIdsArray)
    {
        final List<Long> profileIdsInternal = new ArrayList<Long>();

        if (profileIdsArray != null)
        {
            for (final long id : profileIdsArray)
            {
                profileIdsInternal.add(id);
            }
        }

        return profileIdsInternal;
    }

    /**
     * Get the field names from the annotation.
     */
    @Override
    public void initialize(final FlexibleMandatoryFields constraintAnnotation)
    {
        this.mandatoryFieldNames = constraintAnnotation.mandatoryFields();
        this.modeFieldName = constraintAnnotation.modeField();
        this.profileIds = getprofileIdList(constraintAnnotation.profiles());
        this.secondaryModeFieldName = constraintAnnotation.secondaryModeField();
        this.secondaryProfileIds = getprofileIdList(constraintAnnotation.secondaryProfiles());
    }

    /**
     * Method for testing if the first named field is less than or equal to the second named field. Validation must be
     * added at the class level and can take a list of field pairs, which must be of the same type and must be
     * comparable (Double, Float, Long, Integer and Date are accepted).
     *
     * @return true if firstObj <= secondObj false if not
     */
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context)
    {
        Object modeField = null;
        Object secondaryModeField = null;
        boolean valid = true;

        try
        {
            modeField = PropertyUtils.getProperty(value, modeFieldName);
            secondaryModeField = PropertyUtils.getProperty(value, secondaryModeFieldName);

            if (modeField != null && (profileIds.contains(modeField) || profileIds.contains((LinkResource.class).cast(modeField).getId())))
            {
                if (secondaryModeField != null && (secondaryProfileIds.contains(secondaryModeField) || profileIds
                        .contains((LinkResource.class).cast(secondaryModeField).getId())))
                {
                    for (final String mandatoryField : mandatoryFieldNames)
                    {
                        if (PropertyUtils.getProperty(value, mandatoryField) == null)
                        {
                            valid = false;
                            context.buildConstraintViolationWithTemplate(
                                    String.format(MANDATORY_FIELD_VIOLATION, mandatoryField, modeFieldName,
                                            profileIds.contains(modeField) ? modeField
                                                    : (LinkResource.class).cast(modeField).getId()))
                                    .addConstraintViolation();
                        }
                    }
                }
            }
        }
        catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            // The method that this overrides doesn't throw these exceptions so it needs to throw a built in java
            // exception if there is a problem.
            throw new UnsupportedOperationException(exception);
        }

        return valid;
    }
}
