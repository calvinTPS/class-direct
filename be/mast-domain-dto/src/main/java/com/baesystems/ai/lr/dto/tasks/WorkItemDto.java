package com.baesystems.ai.lr.dto.tasks;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;

import javax.validation.Valid;

public class WorkItemDto extends BaseWorkItemDto implements HashedParentReportingDto
{
    private static final long serialVersionUID = 7031502991694251802L;

    @Valid
    private ItemLightDto assetItem;

    @Valid
    private SurveyDto survey;

    @Valid
    private ScheduledServiceDto scheduledService;

    @Valid
    private WorkItemDto conditionalParent;

    @Valid
    private WorkItemDto workItem;

    private LinkResource asset;

    public ItemLightDto getAssetItem()
    {
        return assetItem;
    }

    public void setAssetItem(final ItemLightDto assetItem)
    {
        this.assetItem = assetItem;
    }

    public SurveyDto getSurvey()
    {
        return survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }

    public ScheduledServiceDto getScheduledService()
    {
        return scheduledService;
    }

    public void setScheduledService(final ScheduledServiceDto scheduledService)
    {
        this.scheduledService = scheduledService;
    }

    public WorkItemDto getConditionalParent()
    {
        return conditionalParent;
    }

    public void setConditionalParent(final WorkItemDto conditionalParent)
    {
        this.conditionalParent = conditionalParent;
    }

    public WorkItemDto getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final WorkItemDto workItem)
    {
        this.workItem = workItem;
    }

    public LinkResource getAsset()
    {
        return asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }
}
