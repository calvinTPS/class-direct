package com.baesystems.ai.lr.enums;

public enum StreamliningRoutesMNCN
{
    STREAMLINE_JOB("Job"),
    STREAMLINE_ASSET("Asset"),
    STREAMLINE_NONE("");

    private String value;

    StreamliningRoutesMNCN(final String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
