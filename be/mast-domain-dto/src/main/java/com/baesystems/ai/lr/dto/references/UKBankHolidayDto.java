package com.baesystems.ai.lr.dto.references;

import java.util.Date;

import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class UKBankHolidayDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = 4057570935738516648L;

    private Date bankHoliday;

    public Date getBankHoliday()
    {
        return bankHoliday;
    }

    public void setBankHoliday(Date bankHoliday)
    {
        this.bankHoliday = bankHoliday;
    }
}
