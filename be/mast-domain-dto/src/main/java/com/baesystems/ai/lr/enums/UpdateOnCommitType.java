package com.baesystems.ai.lr.enums;

/**
 * enum to distinguish different types of stores used so that ids can be saved from multiple listeners to be used in
 * updates on transaction commits.
 */
public enum UpdateOnCommitType
{
    ASSET_REMINDER,
    SURVEY_PERCENTAGE_HELD,
    SURVEY_PERCENTAGE_HELD_BY_JOB,
    JOB_BUNDLE_JOB_RELATED,
    JOB_BUNDLE_ASSET_RELATED,
    ATTACHMENT_DELETION;
}
