package com.baesystems.ai.lr.dto.jobs;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class JobWithFlagsPageResourceDto extends BasePageResource<JobWithFlagsDto>
{
    @Valid
    private List<JobWithFlagsDto> content;

    @Override
    public List<JobWithFlagsDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(List<JobWithFlagsDto> content)
    {
        this.content = content;
    }
}
