package com.baesystems.ai.lr.dto.reports;

import com.baesystems.ai.lr.dto.base.Dto;

import java.io.Serializable;
import java.util.List;

/**
 * Provides a list of validation results against report submission, such that the user
 * may address all problems without repeated attempts
 * <p>
 * Processed on the Client against error message ref data
 * <p>
 */
public class ReportValidationDto implements Dto, Serializable
{
    private static final long serialVersionUID = 6748430359865354306L;

    private boolean valid;
    private List<ReportValidationEntryDto> messages;
    private String otherError;

    public boolean isValid()
    {
        return valid;
    }

    public void setValid(final boolean valid)
    {
        this.valid = valid;
    }

    public List<ReportValidationEntryDto> getMessages()
    {
        return messages;
    }

    public void setMessages(final List<ReportValidationEntryDto> messages)
    {
        this.messages = messages;
    }

    public String getOtherError()
    {
        return otherError;
    }

    public void setOtherError(final String otherError)
    {
        this.otherError = otherError;
    }
}
