package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class WorkItemPreviewCommonDto<T extends WorkItemPreviewCommonDto<?>> extends BaseDto
{
    private static final long serialVersionUID = 1966739550180319224L;

    @NotNull
    private String name;

    private String uniqueItemId;

    @Valid
    private List<T> items;

    private Long displayOrder;

    private Long attachmentCount;

    public final String getName()
    {
        return this.name;
    }

    public final void setName(final String name)
    {
        this.name = name;
    }

    public List<T> getItems()
    {
        return items;
    }

    public void setItems(final List<T> items)
    {
        this.items = items;
    }

    public Long getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public Long getAttachmentCount()
    {
        return attachmentCount;
    }

    public void setAttachmentCount(final Long attachmentCount)
    {
        this.attachmentCount = attachmentCount;
    }

    public String getUniqueItemId()
    {
        return uniqueItemId;
    }

    public void setUniqueItemId(final String uniqueItemId)
    {
        this.uniqueItemId = uniqueItemId;
    }
}
