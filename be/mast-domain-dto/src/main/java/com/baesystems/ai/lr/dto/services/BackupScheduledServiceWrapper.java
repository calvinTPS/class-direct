package com.baesystems.ai.lr.dto.services;

public class BackupScheduledServiceWrapper
{
    private final Long backupServiceId;
    private final Long originalServiceId;
    private final String hashOfOriginalServiceState;

    public BackupScheduledServiceWrapper(final Long backupServiceId, final Long originalServiceId, final String hashOfOriginalServiceState)
    {
        this.backupServiceId = backupServiceId;
        this.originalServiceId = originalServiceId;
        this.hashOfOriginalServiceState = hashOfOriginalServiceState;
    }

    public Long getBackupServiceId()
    {
        return backupServiceId;
    }

    public Long getOriginalServiceId()
    {
        return originalServiceId;
    }

    public String getHashOfOriginalServiceState()
    {
        return hashOfOriginalServiceState;
    }
}
