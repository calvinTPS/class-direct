package com.baesystems.ai.lr.dto.jobs;


public class JobWithRegisterBookChangesDto extends JobDto
{
    private static final long serialVersionUID = 7391569476838881553L;

    private String registerBookAssetName;

    private Long registerBookAssetRegisteredPortId;

    private Long registerBookAssetFlagStateId;

    private Double registerBookAssetLoa;

    private Double registerBookAssetGrossTonnage;

    private String registerBookAssetCallSign;

    private String registerBookAssetOfficialNumber;

    public String getRegisterBookAssetName()
    {
        return this.registerBookAssetName;
    }

    public void setRegisterBookAssetName(final String registerBookAssetName)
    {
        this.registerBookAssetName = registerBookAssetName;
    }

    public Long getRegisterBookAssetRegisteredPortId()
    {
        return this.registerBookAssetRegisteredPortId;
    }

    public void setRegisterBookAssetRegisteredPortId(final Long registerBookAssetRegisteredPortId)
    {
        this.registerBookAssetRegisteredPortId = registerBookAssetRegisteredPortId;
    }

    public Long getRegisterBookAssetFlagStateId()
    {
        return this.registerBookAssetFlagStateId;
    }

    public void setRegisterBookAssetFlagStateId(final Long registerBookAssetFlagStateId)
    {
        this.registerBookAssetFlagStateId = registerBookAssetFlagStateId;
    }

    public Double getRegisterBookAssetLoa()
    {
        return this.registerBookAssetLoa;
    }

    public void setRegisterBookAssetLoa(final Double registerBookAssetLoa)
    {
        this.registerBookAssetLoa = registerBookAssetLoa;
    }

    public Double getRegisterBookAssetGrossTonnage()
    {
        return this.registerBookAssetGrossTonnage;
    }

    public void setRegisterBookAssetGrossTonnage(final Double registerBookAssetGrossTonnage)
    {
        this.registerBookAssetGrossTonnage = registerBookAssetGrossTonnage;
    }

    public String getRegisterBookAssetCallSign()
    {
        return this.registerBookAssetCallSign;
    }

    public void setRegisterBookAssetCallSign(final String registerBookAssetCallSign)
    {
        this.registerBookAssetCallSign = registerBookAssetCallSign;
    }

    public String getRegisterBookAssetOfficialNumber()
    {
        return this.registerBookAssetOfficialNumber;
    }

    public void setRegisterBookAssetOfficialNumber(final String registerBookAssetOfficialNumber)
    {
        this.registerBookAssetOfficialNumber = registerBookAssetOfficialNumber;
    }
}
