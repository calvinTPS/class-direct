package com.baesystems.ai.lr.dto.cases;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.IhsAssetLink;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class CaseValidationDto extends BaseDto implements Serializable
{
    private static final long serialVersionUID = -3628404079802004345L;

    @Valid
    private IhsAssetLink ihsAsset;

    @Valid
    private LinkResource idsAsset;

    @Valid
    private List<LinkResource> cases;

    public IhsAssetLink getIhsAsset()
    {
        return this.ihsAsset;
    }

    public void setIhsAsset(final IhsAssetLink ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public LinkResource getIdsAsset()
    {
        return this.idsAsset;
    }

    public void setIdsAsset(final LinkResource idsAsset)
    {
        this.idsAsset = idsAsset;
    }

    public List<LinkResource> getCases()
    {
        if (cases == null)
        {
            this.cases = new ArrayList<LinkResource>();
        }
        return this.cases;
    }

    public void setCases(final List<LinkResource> cases)
    {
        this.cases = cases;
    }

}
