package com.baesystems.ai.lr.dto.assets;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.RegisterBookField;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseAssetDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = -2379690910656090638L;

    @JsonProperty("assetVersionId")
    private Long versionId;

    @NotNull
    @Size(message = "invalid length", max = 50)
    @ConflictAware
    @RegisterBookField
    private String name;

    @NotNull
    @ConflictAware
    private Date buildDate;

    @Min(value = 0)
    @RegisterBookField
    @ConflictAware
    private Double grossTonnage;

    @NotNull
    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.ASSET_TYPES))
    private AssetTypeDto assetType;

    @NotNull
    @Valid
    @ConflictAware(
            dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.ASSET_CATEGORIES))
    private LinkResource assetCategory;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CLASS_STATUSES))
    private LinkResource classStatus;

    @Valid
    @ConflictAware
    private IhsAssetLink ihsAsset;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.FLAGS))
    private LinkResource flagState;

    private String stalenessHash;

    private String classStatusReason;

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Date getBuildDate()
    {
        return this.buildDate;
    }

    public void setBuildDate(final Date buildDate)
    {
        this.buildDate = buildDate;
    }

    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public AssetTypeDto getAssetType()
    {
        return this.assetType;
    }

    public void setAssetType(final AssetTypeDto assetType)
    {
        this.assetType = assetType;
    }

    public LinkResource getAssetCategory()
    {
        return this.assetCategory;
    }

    public void setAssetCategory(final LinkResource assetCategory)
    {
        this.assetCategory = assetCategory;
    }

    public LinkResource getClassStatus()
    {
        return this.classStatus;
    }

    public void setClassStatus(final LinkResource classStatus)
    {
        this.classStatus = classStatus;
    }

    public IhsAssetLink getIhsAsset()
    {
        return this.ihsAsset;
    }

    public void setIhsAsset(final IhsAssetLink ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public LinkResource getFlagState()
    {
        return this.flagState;
    }

    public void setFlagState(final LinkResource flagState)
    {
        this.flagState = flagState;
    }

    public Long getVersionId()
    {
        return this.versionId;
    }

    public void setVersionId(final Long versionId)
    {
        this.versionId = versionId;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    public String getClassStatusReason()
    {
        return this.classStatusReason;
    }

    public void setClassStatusReason(final String classStatusReason)
    {
        this.classStatusReason = classStatusReason;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "AssetTileDto [name=" + this.getName()
                + ", buildDate=" + this.getStringRepresentationOfDate(this.getBuildDate())
                + ", grossTonnage=" + this.getGrossTonnage()
                + ", assetType=" + this.getIdOrNull(this.getAssetType())
                + ", assetCategory=" + this.getIdOrNull(this.getAssetCategory())
                + ", classStatus=" + this.getIdOrNull(this.getClassStatus())
                + ", ihsAsset=" + (this.getIhsAsset() == null ? null : this.getIhsAsset().getId())
                + ", flagState=" + this.getIdOrNull(this.getFlagState())
                + ", classStatusReason=" + this.getClassStatusReason() + "]";
    }
}
