package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class WorkItemPreviewScheduledServiceDto<T extends WorkItemPreviewCommonDto<T>> extends BaseDto
{
    private static final long serialVersionUID = -8654502749235604350L;

    @Valid
    @NotNull
    private LinkResource serviceCatalogue;

    @Valid
    @NotNull
    private LinkResource productType;

    private Integer occurrenceNumber;

    @Valid
    @NotNull
    private List<T> items;

    public LinkResource getServiceCatalogue()
    {
        return this.serviceCatalogue;
    }

    public void setServiceCatalogue(final LinkResource serviceCatalogue)
    {
        this.serviceCatalogue = serviceCatalogue;
    }

    public LinkResource getProductType()
    {
        return this.productType;
    }

    public void setProductType(final LinkResource productType)
    {
        this.productType = productType;
    }

    public List<T> getItems()
    {
        return this.items;
    }

    public void setItems(final List<T> items)
    {
        this.items = items;
    }

    public Integer getOccurrenceNumber()
    {
        return occurrenceNumber;
    }

    public void setOccurrenceNumber(final Integer occurrenceNumber)
    {
        this.occurrenceNumber = occurrenceNumber;
    }
}
