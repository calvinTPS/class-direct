package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.base.BaseDto;

@IgnoreIdsInConflicts
public class DefectItemLinkDto extends BaseDto
{
    private static final long serialVersionUID = 1914884139860649971L;

    @Valid
    @NotNull
    @ConflictAware
    private LinkResource item;

    public LinkResource getItem()
    {
        return this.item;
    }

    public void setItem(final LinkResource item)
    {
        this.item = item;
    }

}
