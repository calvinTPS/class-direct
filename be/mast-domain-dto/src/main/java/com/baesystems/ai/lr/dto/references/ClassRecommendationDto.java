package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class ClassRecommendationDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -5628040548482762751L;

    private LinkResource reportType;

    private String narrativeTitle;

    private String narrativeText;

    private String farNarrativeText;

    private Boolean surveyorEditable;

    private LinkResource jobCategory;

    private Boolean isDefault;

    public LinkResource getReportType()
    {
        return reportType;
    }

    public void setReportType(final LinkResource reportType)
    {
        this.reportType = reportType;
    }

    public String getNarrativeTitle()
    {
        return narrativeTitle;
    }

    public void setNarrativeTitle(final String narrativeTitle)
    {
        this.narrativeTitle = narrativeTitle;
    }

    public String getNarrativeText()
    {
        return narrativeText;
    }

    public void setNarrativeText(final String narrativeText)
    {
        this.narrativeText = narrativeText;
    }

    public String getFarNarrativeText()
    {
        return farNarrativeText;
    }

    public void setFarNarrativeText(final String farNarrativeText)
    {
        this.farNarrativeText = farNarrativeText;
    }

    public Boolean getSurveyorEditable()
    {
        return surveyorEditable;
    }

    public void setSurveyorEditable(final Boolean surveyorEditable)
    {
        this.surveyorEditable = surveyorEditable;
    }

    public LinkResource getJobCategory()
    {
        return jobCategory;
    }

    public void setJobCategory(final LinkResource jobCategory)
    {
        this.jobCategory = jobCategory;
    }

    public Boolean getIsDefault()
    {
        return isDefault;
    }

    public void setIsDefault(final Boolean isDefault)
    {
        this.isDefault = isDefault;
    }
}
