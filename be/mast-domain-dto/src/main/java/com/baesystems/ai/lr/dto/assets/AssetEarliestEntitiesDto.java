package com.baesystems.ai.lr.dto.assets;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;

public class AssetEarliestEntitiesDto implements Dto, Serializable
{
    private static final long serialVersionUID = 7547592821880799681L;

    public AssetEarliestEntitiesDto()
    {
    }

    public AssetEarliestEntitiesDto(final Long assetId)
    {
        this.assetId = assetId;
    }

    private Long assetId;

    private ScheduledServiceDto earliestServiceByUpperRangeDateOrDueDate;

    private ScheduledServiceDto earliestServiceByLowerRangeDate;

    private ScheduledServiceDto earliestServiceByPostponementDate;

    private WorkItemDto earliestNonPMSTaskByDueDate;

    private WorkItemDto earliestNonPMSTaskByPostponementDate;

    private WorkItemDto earliestPMSTaskByDueDate;

    private WorkItemDto earliestPMSTaskByPostponementDate;

    private CoCDto earliestCoCByDueDate;

    private ActionableItemDto earliestStatutoryActionableItemByDueDate;

    private ActionableItemDto earliestNonStatutoryActionableItemByDueDate;

    private StatutoryFindingDto earliestStatutoryFindingByDueDate;

    public Long getAssetId()
    {
        return this.assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public ScheduledServiceDto getEarliestServiceByUpperRangeDateOrDueDate()
    {
        return this.earliestServiceByUpperRangeDateOrDueDate;
    }

    public void setEarliestServiceByUpperRangeDateOrDueDate(final ScheduledServiceDto earliestServiceByUpperRangeDateOrDueDate)
    {
        this.earliestServiceByUpperRangeDateOrDueDate = earliestServiceByUpperRangeDateOrDueDate;
    }

    public ScheduledServiceDto getEarliestServiceByLowerRangeDate()
    {
        return this.earliestServiceByLowerRangeDate;
    }

    public void setEarliestServiceByLowerRangeDate(final ScheduledServiceDto earliestServiceByLowerRangeDate)
    {
        this.earliestServiceByLowerRangeDate = earliestServiceByLowerRangeDate;
    }

    public ScheduledServiceDto getEarliestServiceByPostponementDate()
    {
        return this.earliestServiceByPostponementDate;
    }

    public void setEarliestServiceByPostponementDate(final ScheduledServiceDto earliestServiceByPostponementDate)
    {
        this.earliestServiceByPostponementDate = earliestServiceByPostponementDate;
    }

    public WorkItemDto getEarliestNonPMSTaskByDueDate()
    {
        return this.earliestNonPMSTaskByDueDate;
    }

    public void setEarliestNonPMSTaskByDueDate(final WorkItemDto earliestNonPMSTaskByDueDate)
    {
        this.earliestNonPMSTaskByDueDate = earliestNonPMSTaskByDueDate;
    }

    public WorkItemDto getEarliestNonPMSTaskByPostponementDate()
    {
        return this.earliestNonPMSTaskByPostponementDate;
    }

    public void setEarliestNonPMSTaskByPostponementDate(final WorkItemDto earliestNonPMSTaskByPostponementDate)
    {
        this.earliestNonPMSTaskByPostponementDate = earliestNonPMSTaskByPostponementDate;
    }

    public WorkItemDto getEarliestPMSTaskByDueDate()
    {
        return this.earliestPMSTaskByDueDate;
    }

    public void setEarliestPMSTaskByDueDate(final WorkItemDto earliestPMSTaskByDueDate)
    {
        this.earliestPMSTaskByDueDate = earliestPMSTaskByDueDate;
    }

    public WorkItemDto getEarliestPMSTaskByPostponementDate()
    {
        return this.earliestPMSTaskByPostponementDate;
    }

    public void setEarliestPMSTaskByPostponementDate(final WorkItemDto earliestPMSTaskByPostponementDate)
    {
        this.earliestPMSTaskByPostponementDate = earliestPMSTaskByPostponementDate;
    }

    public CoCDto getEarliestCoCByDueDate()
    {
        return this.earliestCoCByDueDate;
    }

    public void setEarliestCoCByDueDate(final CoCDto earliestCoCByDueDate)
    {
        this.earliestCoCByDueDate = earliestCoCByDueDate;
    }

    public ActionableItemDto getEarliestStatutoryActionableItemByDueDate()
    {
        return this.earliestStatutoryActionableItemByDueDate;
    }

    public void setEarliestStatutoryActionableItemByDueDate(final ActionableItemDto earliestStatutoryActionableItemByDueDate)
    {
        this.earliestStatutoryActionableItemByDueDate = earliestStatutoryActionableItemByDueDate;
    }

    public ActionableItemDto getEarliestNonStatutoryActionableItemByDueDate()
    {
        return this.earliestNonStatutoryActionableItemByDueDate;
    }

    public void setEarliestNonStatutoryActionableItemByDueDate(final ActionableItemDto earliestNonStatutoryActionableItemByDueDate)
    {
        this.earliestNonStatutoryActionableItemByDueDate = earliestNonStatutoryActionableItemByDueDate;
    }

    public StatutoryFindingDto getEarliestStatutoryFindingByDueDate()
    {
        return earliestStatutoryFindingByDueDate;
    }

    public void setEarliestStatutoryFindingByDueDate(final StatutoryFindingDto earliestStatutoryFindingByDueDate)
    {
        this.earliestStatutoryFindingByDueDate = earliestStatutoryFindingByDueDate;
    }
}
