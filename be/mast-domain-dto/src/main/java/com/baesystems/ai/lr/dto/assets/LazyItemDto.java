package com.baesystems.ai.lr.dto.assets;

import com.baesystems.ai.lr.dto.LinkVersionedRelationshipResource;

public class LazyItemDto extends ItemBaseDto<LinkVersionedRelationshipResource>
{
    private static final long serialVersionUID = -7745944933815168599L;
}
