package com.baesystems.ai.lr.enums;

public enum DateSelectionRule
{
    UPPER_RANGE_DATE("upperRangeDate"),
    DUE_DATE("dueDate"),
    LOWER_RANGE_DATE("lowerRangeDate"),
    POSTPONEMENT_DATE("postponementDate");

    private final String value;

    DateSelectionRule(final String value)
    {
        this.value = value;
    }

    public final String getValue()
    {
        return this.value;
    }
}
