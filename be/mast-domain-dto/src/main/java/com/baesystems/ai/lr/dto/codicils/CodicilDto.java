package com.baesystems.ai.lr.dto.codicils;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.baesystems.ai.lr.dto.base.LongNarrativeDto;
import com.baesystems.ai.lr.dto.base.ReportContentElementDto;
import com.baesystems.ai.lr.dto.jobs.JobTracker;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;

public abstract class CodicilDto extends ReportContentElementDto
        implements UpdateableJobBundleEntity, DueStatusUpdateableDto, JobTracker, HashedParentReportingDto, LongNarrativeDto
{
    private static final long serialVersionUID = 34258180855215370L;

    @ConflictAware
    @Size(message = "invalid length", min = 1, max = 50)
    private String title;

    @NotNull
    @ConflictAware
    @Size(message = "invalid length", min = 1, max = 2000)
    private String description;

    @ConflictAware
    @Size(message = "invalid length", max = 2000)
    private String narrative;

    @JsonIgnore
    private String longNarrative;

    @ConflictAware
    @NotNull
    private Date imposedDate;

    @ConflictAware
    private Date dueDate;

    @ConflictAware
    private Boolean jobScopeConfirmed;

    @Valid
    private LinkResource dueStatus;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_STATUSES))
    private LinkResource status;

    @ConflictAware
    private LinkResource parent;

    @Valid
    @ConflictAware
    private LinkResource raisedOnJob;

    @Valid
    private LinkResource closedOnJob;

    private String referenceCode;

    @ConflictAware
    private Date updatedOn;

    @Valid
    @ConflictAware
    private LinkResource updatedOnBehalfOf;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LinkResource creditedBy;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.ACTIONS_TAKEN))
    private LinkResource actionTaken;

    private String parentHash;

    private Date sourceJobLastVisitDate;

    private Date sourceJobCreationDateTime;

    @Valid
    @ConflictAware
    private LinkResource job;

    @Valid
    @ConflictAware
    private NamedLinkResource assetItem;

    @JsonRawValue
    private String history;

    @JsonProperty("_dmID")
    private String dmInternalLookupID;

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    @Override
    public String getNarrative()
    {
        return this.narrative;
    }

    @Override
    public void setNarrative(final String narrative)
    {
        this.narrative = narrative;
    }

    @Override
    @JsonIgnore
    public String getLongNarrative()
    {
        return this.longNarrative;
    }

    @Override
    @JsonIgnore
    public void setLongNarrative(final String longNarrative)
    {
        this.longNarrative = longNarrative;
    }

    public Date getImposedDate()
    {
        return this.imposedDate;
    }

    public void setImposedDate(final Date imposedDate)
    {
        this.imposedDate = imposedDate;
    }

    @Override
    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Boolean getJobScopeConfirmed()
    {
        return this.jobScopeConfirmed;
    }

    public void setJobScopeConfirmed(final Boolean jobScopeConfirmed)
    {
        this.jobScopeConfirmed = jobScopeConfirmed;
    }

    public LinkResource getDueStatus()
    {
        return this.dueStatus;
    }

    @Override
    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public LinkResource getStatus()
    {
        return this.status;
    }

    public void setStatus(final LinkResource status)
    {
        this.status = status;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    @Override
    public LinkResource getRaisedOnJob()
    {
        return this.raisedOnJob;
    }

    @Override
    public void setRaisedOnJob(final LinkResource raisedOnJob)
    {
        this.raisedOnJob = raisedOnJob;
    }

    @Override
    public LinkResource getClosedOnJob()
    {
        return this.closedOnJob;
    }

    @Override
    public void setClosedOnJob(final LinkResource closedOnJob)
    {
        this.closedOnJob = closedOnJob;
    }

    public String getReferenceCode()
    {
        return this.referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public Date getUpdatedOn()
    {
        return this.updatedOn;
    }

    public void setUpdatedOn(final Date updatedOn)
    {
        this.updatedOn = updatedOn;
    }

    public LinkResource getUpdatedOnBehalfOf()
    {
        return this.updatedOnBehalfOf;
    }

    public void setUpdatedOnBehalfOf(final LinkResource updatedOnBehalfOf)
    {
        this.updatedOnBehalfOf = updatedOnBehalfOf;
    }

    public LinkResource getCreditedBy()
    {
        return this.creditedBy;
    }

    public void setCreditedBy(final LinkResource creditedBy)
    {
        this.creditedBy = creditedBy;
    }

    public LinkResource getActionTaken()
    {
        return this.actionTaken;
    }

    public void setActionTaken(final LinkResource actionTaken)
    {
        this.actionTaken = actionTaken;
    }

    @Override
    public String getParentHash()
    {
        return this.parentHash;
    }

    public void setParentHash(final String parentHash)
    {
        this.parentHash = parentHash;
    }

    @Override
    public Date getSourceJobLastVisitDate()
    {
        return this.sourceJobLastVisitDate;
    }

    @Override
    public void setSourceJobLastVisitDate(final Date sourceJobLastVisitDate)
    {
        this.sourceJobLastVisitDate = sourceJobLastVisitDate;
    }

    @Override
    public Date getSourceJobCreationDateTime()
    {
        return this.sourceJobCreationDateTime;
    }

    @Override
    public void setSourceJobCreationDateTime(final Date sourceJobCreationDateTime)
    {
        this.sourceJobCreationDateTime = sourceJobCreationDateTime;
    }

    public NamedLinkResource getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final NamedLinkResource assetItem)
    {
        this.assetItem = assetItem;
    }

    @JsonSetter
    public void setHistory(final JsonNode history)
    {
        this.history = history.toString();
    }

    public void setHistory(final String history)
    {
        this.history = history;
    }

    public String getHistory()
    {
        return this.history;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public String getDmInternalLookupID()
    {
        return this.dmInternalLookupID;
    }

    public void setDmInternalLookupID(final String dmInternalLookupID)
    {
        this.dmInternalLookupID = dmInternalLookupID;
    }
}
