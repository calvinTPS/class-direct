package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.baesystems.ai.lr.enums.JobBundleRepairAction;

/**
 * Annotation to tell the job bundle conflict resolution service what to do with the fields of a dto if there is a
 * record not found exception on something that is linked to it. Most time this cannot be fixed so the default is to
 * fail which is the same as if the annotation was not on the field at all.
 *
 * However in the case of join tables stripping the ids to post a new row will often help and is safe to do as other
 * joins will be deleted so the user will always see what they expect only the internal ids will have changed.
 *
 * When a parent item is missing it may be necessary to break the link, for example if a survey is related to a deleted
 * service. Updating the survey without the service link will save the survey then when the FAR is submitted an new
 * service will be created.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OnNotFoundException
{
    JobBundleRepairAction action() default JobBundleRepairAction.FAIL;
}
