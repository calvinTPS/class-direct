package com.baesystems.ai.lr.dto.services;

import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.HashedParentReportingDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ScheduledServiceDto extends AbstractScheduledServiceDto
        implements DueStatusUpdateableDto, UpdateableJobBundleEntity, HashedParentReportingDto
{
    private static final long serialVersionUID = 3565933128926406022L;

    @JsonIgnore
    private Boolean active;

    @Override
    @JsonProperty
    public Boolean getActive()
    {
        return this.active;
    }

    @Override
    @JsonIgnore
    public void setActive(final Boolean active)
    {
        this.active = active;
    }
}
