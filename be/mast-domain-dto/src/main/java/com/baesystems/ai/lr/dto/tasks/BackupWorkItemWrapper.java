package com.baesystems.ai.lr.dto.tasks;

public class BackupWorkItemWrapper
{
    private final Long backupWorkItemId;
    private final Long originalWorkItemId;
    private final String hashOfOriginalWorkItemState;

    public BackupWorkItemWrapper(final Long backupWorkItemId, final Long originalWorkItemId, final String hashOfOriginalWorkItemState)
    {
        this.backupWorkItemId = backupWorkItemId;
        this.originalWorkItemId = originalWorkItemId;
        this.hashOfOriginalWorkItemState = hashOfOriginalWorkItemState;
    }

    public Long getBackupWorkItemId()
    {
        return backupWorkItemId;
    }

    public Long getOriginalWorkItemId()
    {
        return originalWorkItemId;
    }

    public String getHashOfOriginalWorkItemState()
    {
        return hashOfOriginalWorkItemState;
    }
}
