package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.LinkResource;

public class WorkItemConditionalAttributeDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -448940813171868929L;

    private String serviceCode;

    private String referenceCode;

    private ItemTypeDto itemType;

    private WorkItemActionDto workItemAction;

    private LinkResource attributeDataType;

    private List<AllowedWorkItemAttributeValueDto> allowedAttributeValues;

    private LinkResource conditionalValue;

    private LinkResource allowedValueGroup;

    private LinkResource parentAttribute;

    private LinkResource readingCategory;

    private String comparativeAttributeCode;

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public ItemTypeDto getItemType()
    {
        return itemType;
    }

    public void setItemType(final ItemTypeDto itemType)
    {
        this.itemType = itemType;
    }

    public WorkItemActionDto getWorkItemAction()
    {
        return workItemAction;
    }

    public void setWorkItemAction(final WorkItemActionDto workItemAction)
    {
        this.workItemAction = workItemAction;
    }

    public LinkResource getAttributeDataType()
    {
        return attributeDataType;
    }

    public void setAttributeDataType(final LinkResource attributeDataType)
    {
        this.attributeDataType = attributeDataType;
    }

    public List<AllowedWorkItemAttributeValueDto> getAllowedAttributeValues()
    {
        return allowedAttributeValues;
    }

    public void setAllowedAttributeValues(final List<AllowedWorkItemAttributeValueDto> allowedAttributeValues)
    {
        this.allowedAttributeValues = allowedAttributeValues;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public LinkResource getConditionalValue()
    {
        return conditionalValue;
    }

    public void setConditionalValue(final LinkResource conditionalValue)
    {
        this.conditionalValue = conditionalValue;
    }

    public LinkResource getAllowedValueGroup()
    {
        return allowedValueGroup;
    }

    public void setAllowedValueGroup(final LinkResource allowedValueGroup)
    {
        this.allowedValueGroup = allowedValueGroup;
    }

    public LinkResource getParentAttribute()
    {
        return this.parentAttribute;
    }

    public void setParentAttribute(final LinkResource parentAttribute)
    {
        this.parentAttribute = parentAttribute;
    }

    public LinkResource getReadingCategory()
    {
        return readingCategory;
    }

    public void setReadingCategory(final LinkResource readingCategory)
    {
        this.readingCategory = readingCategory;
    }

    public String getComparativeAttributeCode()
    {
        return comparativeAttributeCode;
    }

    public void setComparativeAttributeCode(final String comparativeAttributeCode)
    {
        this.comparativeAttributeCode = comparativeAttributeCode;
    }
}
