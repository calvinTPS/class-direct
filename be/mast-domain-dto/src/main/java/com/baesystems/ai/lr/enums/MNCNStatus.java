package com.baesystems.ai.lr.enums;

public enum MNCNStatus
{
    OPEN(1L),
    OPEN_DOWNGRADED(2L),
    CLOSED(3L);

    MNCNStatus(final Long value)
    {
        this.value = value;
    }

    private Long value;

    public Long getValue()
    {
        return this.value;
    }
}
