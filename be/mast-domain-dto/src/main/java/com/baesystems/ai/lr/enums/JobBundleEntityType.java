package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.List;

public enum JobBundleEntityType
{
    JOB("job", "direct:updateJob", null, null, null, "id",
            Arrays.asList("\"Job \"", "id")),
    WIP_ACTIONABLE_ITEM("wipActionableItem", "direct:updateWIPActionableItem", "direct:createWIPActionableItem", "direct:deleteWIPActionableItem",
            "parent.id", "job.id",
            Arrays.asList("\"Actionable Item \"", "referenceCode")),
    WIP_COC("wipCoC", "direct:updateWIPCoC", "direct:saveWIPCoC", "direct:deleteWIPCoC", "parent.id", "job.id",
            Arrays.asList("\"Condition of Class \"", "referenceCode")),
    WIP_ASSET_NOTE("wipAssetNote", "direct:updateWIPAssetNote", "direct:saveWIPAssetNote", "direct:deleteWIPAssetNote", "parent.id", "job.id",
            Arrays.asList("\"Asset Note \"", "referenceCode")),
    WIP_STATUTORY_FINDING("wipStatutoryFinding", "direct:updateWIPStatutoryFinding", "direct:createWIPStatutoryFindingOnWIPDeficiency",
            "direct:deleteWIPStatutoryFinding", "parent.id", "job.id",
            Arrays.asList("\"Statutory Finding \"", "referenceCode")),
    WIP_DEFECT("wipDefect", "direct:updateWIPDefect", "direct:createWIPDefect", "direct:deleteWIPDefect", "parent.id", "job.id",
            Arrays.asList("\"Defect \"", "sequenceNumber")),
    WIP_DEFECT_REPAIR("wipRepair", "direct:updateWIPDefectRepair", "direct:createWIPDefectRepair", "direct:deleteWIPRepair", "parent.id",
            "defect.job.id",
            Arrays.asList("\"Repair \"", "id")),
    WIP_COC_REPAIR("wipRepair", "direct:updateWIPCoCRepair", "direct:createWIPRepairOnACoC", "direct:deleteWIPRepair", "parent.id", "codicil.job.id",
            Arrays.asList("\"Repair \"", "id")),
    WIP_DEFICIENCY("wipDeficiency", "direct:updateWIPDeficiency", "direct:createWIPDeficiency", "direct:deleteWIPDeficiency", "parent.id", "job.id",
            Arrays.asList("\"Deficiency \"", "sequenceNumber")),
    WIP_RECTIFICATION("wipRectification", "direct:updateWIPDeficiencyRectification", "direct:createWIPDeficiencyRectification",
            "direct:updateWIPDeficiencyRectification", "parent.id", "defect.job.id",
            Arrays.asList("\"Rectification \"", "id")),
    SURVEY("survey", "direct:updateSurvey", "direct:createSurvey", "direct:deleteSurvey", "scheduledService.id", "job.id",
            Arrays.asList("\"Service \"", "name", "\", \"", "serviceCatalogue.code", "occurrenceNumber")),
    WIP_TASK("wipTask", "direct:updateWIPWorkItem", "direct:createWIPWorkItem", "direct:deleteWIPTask", "parent.id", "survey.job.id",
            Arrays.asList("\"Task \"", "taskNumber")),
    WIP_CHECKLIST("wipTask", "direct:updateWIPWorkItem", "direct:createWIPWorkItem", "direct:deleteWIPTask", "parent.id", "survey.job.id",
            Arrays.asList("\"Checklist \"", "survey.serviceCatalogue.code", "\" \"", "referenceCode")),
    WIP_CERTIFICATE("jobRelatedCertificate", "direct:updateCertificate", "direct:createCertificate", "direct:deleteCertificate", null, "job.id",
            Arrays.asList("\"Certificate \"", "id")),
    WIP_CERTIFICATE_ACTION("jobRelatedCertificateAction", "direct:updateCertificateAction", "direct:createCertificateAction",
            "direct:deleteCertificateAction", null, "certificate.job.id",
            Arrays.asList("\"Certificate Action \"", "id")),
    WIP_MNCN("wipMncn", "direct:updateMajorNCN", "direct:createMajorNCN", "direct:deleteMajorNCN", null, "job.id",
            Arrays.asList("\"Major Non-Conformity \"", "mncnNumber")),
    PR17("prSeventeen", "direct:updatePR", "direct:createPR", "direct:deletePR", null, "job.id",
            Arrays.asList("\"PR17 \"")),
    ATTACHMENT("attachment", "direct:updateAttachment", "direct:saveAttachment", "direct:deleteAttachment", "parentRecordId", "job.id",
            Arrays.asList("\"Attachment \"", "id")),
    REPORT("report", "direct:updateReport", "direct:saveReport", null, null, "job.id",
            Arrays.asList("\"Report \"", "reportType.name", "reportVersion")),
    ASSET("asset", "direct:updateAsset", null, null, null, "job.id",
            Arrays.asList("\"Asset \"", "name")),
    ASSET_ITEM_ACTIONS("itemAction", "direct:updateItemAction", "direct:createItemActions", "direct:deleteItemAction", null, "job.id",
            Arrays.asList("\"Asset Item Action \"", "id"));

    private String fieldName;
    private String putUri;
    private String postUri;
    private String deleteUri;
    private String parent; // the path to the parent id on the Dto
    private String jobId; // the path to the job id on the DO
    private List<String> conflictKey; // list of fields/Strings to concatenate for the display id (See
                                      // BaseJobBundleEntityRepositoryImpl.getIdKey for more information)

    JobBundleEntityType(final String fieldName, final String putUri, final String postUri, final String deleteUri, final String parent,
            final String jobId, final List<String> conflictKey)
    {
        this.fieldName = fieldName;
        this.putUri = putUri;
        this.postUri = postUri;
        this.deleteUri = deleteUri;
        this.parent = parent;
        this.jobId = jobId;
        this.conflictKey = conflictKey;
    }

    public String getFieldName()
    {
        return this.fieldName;
    }

    public String getPutUri()
    {
        return this.putUri;
    }

    public String getPostUri()
    {
        return this.postUri;
    }

    public String getDeleteUri()
    {
        return this.deleteUri;
    }

    public String getParent()
    {
        return parent;
    }

    public String getJobId()
    {
        return jobId;
    }

    public List<String> getConflictKey()
    {
        return this.conflictKey;
    }
}
