package com.baesystems.ai.lr.dto.assets;

import com.baesystems.ai.lr.dto.LinkResource;

public class ItemMetaDto
{
    private Long id;
    private LinkResource asset;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkResource getAsset()
    {
        return asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

}
