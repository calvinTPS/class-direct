package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * To be used at the class level of dtos that represent links so that the conflict service knows that the ids can be
 * ignored when creating the conflict report. If this is present and two elements in a list do not match on their ids
 * then the service will try to find a match using the other conflict aware fields.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreIdsInConflicts
{

}
