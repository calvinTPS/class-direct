package com.baesystems.ai.lr.dto.defects;

import static com.baesystems.ai.lr.dto.defects.DeficiencyLightDto.ENTITY_NAME;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;

@EntityName(ENTITY_NAME)
public class DeficiencyDto extends DeficiencyLightDto
{
    private static final long serialVersionUID = 999757624122048500L;

    @Valid
    @LinkedEntityMustExist(type = JobBundleEntityType.SURVEY)
    private SurveyDto survey;

    public SurveyDto getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }
}
