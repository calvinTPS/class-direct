package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class AssetItemActionPageResourceDto extends BasePageResource<AssetItemActionDto>
{

    @Valid
    private List<AssetItemActionDto> content;

    @Override
    public List<AssetItemActionDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<AssetItemActionDto> content)
    {
        this.content = content;
    }

}
