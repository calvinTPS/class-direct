package com.baesystems.ai.lr.dto.defects;

import static com.baesystems.ai.lr.dto.defects.DeficiencyLightDto.ENTITY_NAME;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class DeficiencyLightDto extends FaultDto implements UpdateableJobBundleEntity
{
    private static final long serialVersionUID = 8651472505617251091L;

    public static final String ENTITY_NAME = "Deficiency";

    private String stalenessHash;

    @ConflictAware
    private Boolean flagStateConsulted;

    @ConflictAware
    private String narrative;

    @Valid
    @ConflictAware
    private List<DeficiencyChecklistDto> deficiencyChecklists;

    @Valid
    @ConflictAware
    private List<LinkResource> rectifications;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.DEFICIENCY_SEVERITIES))
    private LinkResource deficiencySeverity;

    public Boolean getFlagStateConsulted()
    {
        return this.flagStateConsulted;
    }

    public void setFlagStateConsulted(final Boolean flagStateConsulted)
    {
        this.flagStateConsulted = flagStateConsulted;
    }

    public String getNarrative()
    {
        return this.narrative;
    }

    public void setNarrative(final String narrative)
    {
        this.narrative = narrative;
    }

    public List<DeficiencyChecklistDto> getDeficiencyChecklists()
    {
        return this.deficiencyChecklists;
    }

    public void setDeficiencyChecklists(final List<DeficiencyChecklistDto> deficiencyChecklists)
    {
        this.deficiencyChecklists = deficiencyChecklists;
    }

    public List<LinkResource> getRectifications()
    {
        return this.rectifications;
    }

    public void setRectifications(final List<LinkResource> rectifications)
    {
        this.rectifications = rectifications;
    }

    public LinkResource getDeficiencySeverity()
    {
        return this.deficiencySeverity;
    }

    public void setDeficiencySeverity(final LinkResource deficiencySeverity)
    {
        this.deficiencySeverity = deficiencySeverity;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "DeficiencyDto [jobs=" + this.getStringRepresentationOfList(this.getJobs())
                + ", getSequenceNumber()=" + this.getSequenceNumber()
                + ", getIncidentDate()=" + this.getStringRepresentationOfDate(this.getIncidentDate())
                + ", getIncidentDescription()=" + this.getIncidentDescription()
                + ", getNarrative()=" + this.getNarrative()
                + ", getTitle()=" + this.getTitle()
                + ", getAsset()=" + this.getIdOrNull(this.getAsset())
                + ", getCourseOfActionCount()=" + this.getCourseOfActionCount()
                + ", getJob()=" + this.getIdOrNull(this.getJob())
                + ", getCategory()=" + this.getCategory()
                + ", getStatus()=" + this.getStatus()
                + ", getFlagStateConsulted()=" + this.getFlagStateConsulted()
                + ", getConfidentialityType()=" + this.getIdOrNull(this.getConfidentialityType())
                + ", getDeficiencyChecklists()=" + this.getStringRepresentationOfList(this.getDeficiencyChecklists())
                + ", getRectifications()=" + this.getStringRepresentationOfList(this.getRectifications())
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", getId()=" + this.getId()
                + ", getDeficiencySeverity()=" + this.getIdOrNull(this.getDeficiencySeverity()) + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
