package com.baesystems.ai.lr.dto.query;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

public class JobWithFlagsQueryDto extends BaseJobQueryDto
{
    private static final long serialVersionUID = 1271493315271918499L;

    @UsesLikeComparator
    private Long search;

    private Boolean tmJob;

    private Boolean espJob;

    private Long assetFlagStateId;

    private Long jobProposedFlagStateId;

    // Filters for assetPartyId and assetPartyRoleId are only applied to MMS jobs at query level
    private Long assetPartyId;

    private Long assetPartyRoleId;

    public Long getSearch()
    {
        return search;
    }

    public void setSearch(final Long search)
    {
        this.search = search;
    }

    public Boolean getTmJob()
    {
        return tmJob;
    }

    public void setTmJob(final Boolean tmJob)
    {
        this.tmJob = tmJob;
    }

    public Boolean getEspJob()
    {
        return espJob;
    }

    public void setEspJob(final Boolean espJob)
    {
        this.espJob = espJob;
    }

    public Long getAssetFlagStateId()
    {
        return assetFlagStateId;
    }

    public void setAssetFlagStateId(final Long assetFlagStateId)
    {
        this.assetFlagStateId = assetFlagStateId;
    }

    public Long getJobProposedFlagStateId()
    {
        return jobProposedFlagStateId;
    }

    public void setJobProposedFlagStateId(final Long jobProposedFlagStateId)
    {
        this.jobProposedFlagStateId = jobProposedFlagStateId;
    }

    public Long getAssetPartyId()
    {
        return assetPartyId;
    }

    public void setAssetPartyId(final Long assetPartyId)
    {
        this.assetPartyId = assetPartyId;
    }

    public Long getAssetPartyRoleId()
    {
        return assetPartyRoleId;
    }

    public void setAssetPartyRoleId(final Long assetPartyRoleId)
    {
        this.assetPartyRoleId = assetPartyRoleId;
    }
}
