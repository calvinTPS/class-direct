package com.baesystems.ai.lr.constants;

/**
 * Constants class for MAST centric HTTP headers for REST API consumers
 */
public interface MastHeaders
{
    String IDS_CLIENT_IDENTITY = "X-Client-Identity"; // Client identification, serves as the "key" to the client's stored public key
    String IDS_CLIENT_VERSION = "client-version"; // Client version
    String IDS_CLIENT_SIGN = "X-Message-Signature"; // Checksum ([METHOD]:[CORE-API_URL)] and payload, signed with the client's private key)
    String USER = "user"; // Header used for RBAC
    String GROUPS = "groups"; // Header used for RBAC
    String CORRELATION_ID_HEADER_NAME = "X-Correlation-Id"; //Header for FE generated correlation id.
}
