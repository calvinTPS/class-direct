package com.baesystems.ai.lr.enums;

public enum WorkItemScopeStatus
{
    JOB_OUT_OF_SCOPE(1L),
    JOB_IN_PENDING(2L),
    JOB_IN_CONFIRMED(3L),
    JOB_OUT_PENDING(4L),
    JOB_OUT_CONFIRMED(5L);

    private final Long value;

    WorkItemScopeStatus(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return this.value;
    }
}
