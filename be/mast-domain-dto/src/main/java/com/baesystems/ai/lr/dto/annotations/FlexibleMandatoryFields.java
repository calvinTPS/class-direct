package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = FlexibleMandatoryFieldsValidator.class)
@Documented
public @interface FlexibleMandatoryFields
{
    String message()

    default "mandatory fields do not match profile";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The list of field names that are mandatory
     */
    String[] mandatoryFields();

    /**
     * @return The field that is used to decide on the profile
     */
    String modeField();

    /**
     * @return The id of the status that the mode field must be in to use this profile
     */
    long[] profiles();

    String secondaryModeField();

    long[] secondaryProfiles();

    /**
     */
    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List
    {
        FlexibleMandatoryFields[] value();
    }
}
