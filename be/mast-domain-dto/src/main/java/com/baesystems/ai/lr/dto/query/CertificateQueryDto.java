package com.baesystems.ai.lr.dto.query;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Size;

public class CertificateQueryDto implements QueryDto
{
    private static final long serialVersionUID = 3038108185778109762L;

    private Long assetId;

    private Long jobId;

    @Size(min = 1)
    private List<String> certificateName;

    @Size(min = 1)
    private List<String> certificateNumber;

    @Size(min = 1)
    private List<String> formNumber;

    @Size(min = 1)
    private List<Double> version;

    private List<Long> surveyId;

    private String issuedAt;

    private Date issueDateMin;

    private Date issueDateMax;

    private Date expiryDateMin;

    private Date expiryDateMax;

    private Date extendedDateMin;

    private Date extendedDateMax;

    private List<Long> status;

    private Boolean includeExpired;

    private Boolean combinedExpired;

    private Boolean expiryMaxIsCurrentDate;

    private Boolean expiryMinIsCurrentDate;

    public Long getAssetId()
    {
        return this.assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getJobId()
    {
        return this.jobId;
    }

    public void setJobId(final Long jobId)
    {
        this.jobId = jobId;
    }

    public List<String> getCertificateName()
    {
        return this.certificateName;
    }

    public void setCertificateName(final List<String> certificateName)
    {
        this.certificateName = certificateName;
    }

    public List<String> getCertificateNumber()
    {
        return this.certificateNumber;
    }

    public void setCertificateNumber(final List<String> certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public List<String> getFormNumber()
    {
        return this.formNumber;
    }

    public void setFormNumber(final List<String> formNumber)
    {
        this.formNumber = formNumber;
    }

    public List<Double> getVersion()
    {
        return this.version;
    }

    public void setVersion(final List<Double> version)
    {
        this.version = version;
    }

    public String getIssuedAt()
    {
        return this.issuedAt;
    }

    public void setIssuedAt(final String issuedAt)
    {
        this.issuedAt = issuedAt;
    }

    public Date getIssueDateMin()
    {
        return this.issueDateMin;
    }

    public void setIssueDateMin(final Date issueDateMin)
    {
        this.issueDateMin = issueDateMin;
    }

    public Date getIssueDateMax()
    {
        return this.issueDateMax;
    }

    public void setIssueDateMax(final Date issueDateMax)
    {
        this.issueDateMax = issueDateMax;
    }

    public Date getExpiryDateMin()
    {
        return this.expiryDateMin;
    }

    public void setExpiryDateMin(final Date expiryDateMin)
    {
        this.expiryDateMin = expiryDateMin;
    }

    public Date getExpiryDateMax()
    {
        return this.expiryDateMax;
    }

    public void setExpiryDateMax(final Date expiryDateMax)
    {
        this.expiryDateMax = expiryDateMax;
    }

    public Date getExtendedDateMin()
    {
        return this.extendedDateMin;
    }

    public void setExtendedDateMin(final Date extendedDateMin)
    {
        this.extendedDateMin = extendedDateMin;
    }

    public Date getExtendedDateMax()
    {
        return this.extendedDateMax;
    }

    public void setExtendedDateMax(final Date extendedDateMax)
    {
        this.extendedDateMax = extendedDateMax;
    }

    public List<Long> getStatus()
    {
        return this.status;
    }

    public void setStatus(final List<Long> status)
    {
        this.status = status;
    }

    public List<Long> getSurveyId()
    {
        return surveyId;
    }

    public void setSurveyId(final List<Long> surveyId)
    {
        this.surveyId = surveyId;
    }

    @JsonIgnore
    public Boolean getIncludeExpired()
    {
        return includeExpired;
    }

    @JsonIgnore
    public void setIncludeExpired(final Boolean includeExpired)
    {
        this.includeExpired = includeExpired;
    }

    @JsonIgnore
    public Boolean getCombinedExpired()
    {
        return combinedExpired;
    }

    @JsonIgnore
    public void setCombinedExpired(final Boolean combinedExpired)
    {
        this.combinedExpired = combinedExpired;
    }

    @JsonIgnore
    public Boolean getExpiryMaxIsCurrentDate()
    {
        return expiryMaxIsCurrentDate;
    }

    @JsonIgnore
    public void setExpiryMaxIsCurrentDate(final Boolean expiryMaxIsCurrentDate)
    {
        this.expiryMaxIsCurrentDate = expiryMaxIsCurrentDate;
    }

    @JsonIgnore
    public Boolean getExpiryMinIsCurrentDate()
    {
        return expiryMinIsCurrentDate;
    }

    @JsonIgnore
    public void setExpiryMinIsCurrentDate(final Boolean expiryMinIsCurrentDate)
    {
        this.expiryMinIsCurrentDate = expiryMinIsCurrentDate;
    }
}
