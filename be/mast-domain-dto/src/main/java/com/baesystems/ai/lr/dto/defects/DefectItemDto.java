package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.IgnoreIdsInConflicts;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@IgnoreIdsInConflicts
public class DefectItemDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = 2207746146624404895L;

    @NotNull
    @SubIdNotNull
    @Valid
    @ConflictAware
    private ItemLightDto item;

    @Valid
    private LinkResource defect;

    @Valid
    private LinkResource parent;

    @JsonIgnore
    private LinkResource wipDefectItem;

    private String stalenessHash;

    public ItemLightDto getItem()
    {
        return this.item;
    }

    public void setItem(final ItemLightDto item)
    {
        this.item = item;
    }

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    @JsonIgnore
    public LinkResource getWipDefectItem()
    {
        return this.wipDefectItem;
    }

    @JsonIgnore
    public void setWipDefectItem(final LinkResource wipDefectItem)
    {
        this.wipDefectItem = wipDefectItem;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;

    }

    /**
     * This is needed in case the item is change on an existing link. The reference back to the defect is not relevant
     * here as this Dto only appears as a field on the defect dto.
     */
    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "DefectDefectValueDto [defectItem=" + getIdOrNull(getItem()) + "]";
    }
}
