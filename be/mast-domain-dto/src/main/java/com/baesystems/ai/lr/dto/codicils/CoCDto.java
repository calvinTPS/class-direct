package com.baesystems.ai.lr.dto.codicils;

import static com.baesystems.ai.lr.dto.codicils.CoCDto.ENTITY_NAME;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class CoCDto extends CodicilDto implements DueStatusUpdateableDto
{
    private static final long serialVersionUID = -4350347374072610940L;
    public static final String ENTITY_NAME = "Condition of Class";

    private String stalenessHash;

    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_TEMPLATES))
    private LinkResource template;

    @ConflictAware
    @Size(message = "invalid length", max = 45)
    private String affectedItems;

    @Valid
    @ConflictAware
    private LinkResource asset;

    @Valid
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_DEFECT)
    private LinkResource defect;

    @NotNull
    @ConflictAware
    private Boolean inheritedFlag;

    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.EMPLOYEE))
    private LrEmployeeDto employee;

    @ConflictAware
    private Boolean editableBySurveyor;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CONFIDENTIALITY_TYPES))
    private LinkResource confidentialityType;

    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.CODICIL_CATEGORIES))
    private LinkResource category;

    @ConflictAware
    @Size(message = "invalid length", max = 2000)
    private String surveyorGuidance;

    @ConflictAware
    private Boolean requireApproval;

    @ConflictAware
    private Integer sequenceNumber;

    @ConflictAware
    private Date dateOfCrediting;

    @Valid
    @LinkedEntityMustExist(type = JobBundleEntityType.SURVEY)
    private SurveyDto survey;

    @ConflictAware
    private Boolean temporary;

    public Boolean getEditableBySurveyor()
    {
        return this.editableBySurveyor;
    }

    public void setEditableBySurveyor(final Boolean editableBySurveyor)
    {
        this.editableBySurveyor = editableBySurveyor;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public LinkResource getCategory()
    {
        return this.category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public String getSurveyorGuidance()
    {
        return this.surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public LinkResource getTemplate()
    {
        return this.template;
    }

    public void setTemplate(final LinkResource template)
    {
        this.template = template;
    }

    public String getAffectedItems()
    {
        return this.affectedItems;
    }

    public void setAffectedItems(final String affectedItems)
    {
        this.affectedItems = affectedItems;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public Boolean getInheritedFlag()
    {
        return this.inheritedFlag;
    }

    public void setInheritedFlag(final Boolean inheritedFlag)
    {
        this.inheritedFlag = inheritedFlag;
    }

    public LrEmployeeDto getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LrEmployeeDto employee)
    {
        this.employee = employee;
    }

    public Boolean getRequireApproval()
    {
        return this.requireApproval;
    }

    public void setRequireApproval(final Boolean requireApproval)
    {
        this.requireApproval = requireApproval;
    }

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getDateOfCrediting()
    {
        return this.dateOfCrediting;
    }

    public void setDateOfCrediting(final Date dateOfCrediting)
    {
        this.dateOfCrediting = dateOfCrediting;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    public SurveyDto getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }

    public Boolean getTemporary()
    {
        return this.temporary;
    }

    public void setTemporary(final Boolean temporary)
    {
        this.temporary = temporary;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;

    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CoCDto [template=" + this.getIdOrNull(this.template)
                + ", title=" + this.getTitle()
                + ", imposedDate=" + this.getStringRepresentationOfDate(this.getImposedDate())
                + ", dueDate=" + this.getStringRepresentationOfDate(this.getDueDate())
                + ", description=" + this.getDescription()
                + ", narrative=" + this.getNarrative()
                + ", affectedItems=" + this.getAffectedItems()
                + ", status=" + this.getIdOrNull(this.getStatus())
                + ", assetItem=" + this.getIdOrNull(this.getAssetItem())
                + ", job=" + this.getIdOrNull(this.getJob())
                + ", asset=" + this.getIdOrNull(this.asset)
                + ", defect=" + this.getIdOrNull(this.defect)
                + ", inheritedFlag=" + this.getInheritedFlag()
                + ", temporary=" + this.getTemporary()
                + ", actionTaken=" + this.getActionTaken()
                + ", employee=" + this.getIdOrNull(this.employee)
                + ", parent=" + this.getIdOrNull(this.getParent())
                + ", editableBySurveyor=" + this.editableBySurveyor
                + ", confidentialityType=" + this.getIdOrNull(this.confidentialityType)
                + ", category=" + this.getIdOrNull(this.category)
                + ", surveyorGuidance=" + this.surveyorGuidance
                + ", requireApproval=" + this.requireApproval
                + ", jobScopeConfirmed=" + this.getJobScopeConfirmed()
                + ", sequenceNumber=" + this.getSequenceNumber()
                + ", dateOfCrediting=" + this.getStringRepresentationOfDate(this.dateOfCrediting)
                + ", creditedBy=" + this.getIdOrNull(getCreditedBy())
                + ", raisedOnJob=" + this.getIdOrNull(this.getRaisedOnJob())
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }

}
