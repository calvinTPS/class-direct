package com.baesystems.ai.lr.dto.references;

public class CertificateActionTakenDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 960132635896936371L;

    private Boolean issueDateMutable;

    public Boolean getIssueDateMutable()
    {
        return issueDateMutable;
    }

    public void setIssueDateMutable(final Boolean issueDateMutable)
    {
        this.issueDateMutable = issueDateMutable;
    }
}
