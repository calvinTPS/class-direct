package com.baesystems.ai.lr.dto.reminders;

public class CoCReminderDto extends BaseReminderDto
{
    private static final long serialVersionUID = -2155934736900287277L;

    private Long cocId;

    private String referenceCode;

    private String cocTitle;

    public Long getCocId()
    {
        return this.cocId;
    }

    public void setCocId(final Long cocId)
    {
        this.cocId = cocId;
    }

    public String getCocTitle()
    {
        return this.cocTitle;
    }

    public void setCocTitle(final String cocTitle)
    {
        this.cocTitle = cocTitle;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }
}
