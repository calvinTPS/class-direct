package com.baesystems.ai.lr.dto.assets;

import java.util.Date;

public class AssetTileDto extends BaseAssetDto
{
    private static final long serialVersionUID = -8579599848414749063L;

    private Boolean isWatched;

    private Integer jobCount;

    private Integer caseCount;

    private Date reviewDate;

    private Boolean targetShip;

    public Boolean getIsWatched()
    {
        return this.isWatched;
    }

    public void setIsWatched(final Boolean isWatched)
    {
        this.isWatched = isWatched;
    }

    public Integer getJobCount()
    {
        return this.jobCount;
    }

    public void setJobCount(final Integer jobCount)
    {
        this.jobCount = jobCount;
    }

    public Integer getCaseCount()
    {
        return this.caseCount;
    }

    public void setCaseCount(final Integer caseCount)
    {
        this.caseCount = caseCount;
    }

    public Date getReviewDate()
    {
        return this.reviewDate;
    }

    public void setReviewDate(final Date reviewDate)
    {
        this.reviewDate = reviewDate;
    }

    public Boolean getTargetShip()
    {
        return targetShip;
    }

    public void setTargetShip(final Boolean targetShip)
    {
        this.targetShip = targetShip;
    }
}
