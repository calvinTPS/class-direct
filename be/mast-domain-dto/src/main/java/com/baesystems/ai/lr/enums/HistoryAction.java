package com.baesystems.ai.lr.enums;

import java.util.Arrays;

public enum HistoryAction
{
    RAISED(1, "Raised"),
    CLOSED(2, "Closed"),
    REVISED(3, "Revised");

    private final long id;
    private final String value;

    HistoryAction(final long id, final String value)
    {
        this.id = id;
        this.value = value;
    }

    public long getId()
    {
        return id;
    }

    public String getValue()
    {
        return value;
    }

    public static HistoryAction enumFor(final long id)
    {
        return Arrays.stream(values())
                .filter(hEnum -> hEnum.id == id)
                .findFirst().orElse(null);
    }

}
