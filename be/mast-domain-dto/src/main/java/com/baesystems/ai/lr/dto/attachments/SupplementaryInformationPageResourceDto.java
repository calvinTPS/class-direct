package com.baesystems.ai.lr.dto.attachments;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class SupplementaryInformationPageResourceDto extends BasePageResource<SupplementaryInformationDto>
{
    @Valid
    private List<SupplementaryInformationDto> content;

    @Override
    public List<SupplementaryInformationDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<SupplementaryInformationDto> content)
    {
        this.content = content;
    }

}
