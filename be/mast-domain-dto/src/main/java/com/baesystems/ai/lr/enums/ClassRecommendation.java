package com.baesystems.ai.lr.enums;

public enum ClassRecommendation
{
    STANDARD(1L),
    NON_STANDARD(2L),
    REINSTATEMENT_CLASS_SUSPENDED(3L),
    TOC_AIC_COMPLETED(4L),
    TOC_AIC_NOT_COMPLETED(5L),
    MAINTENANCE_LAID_UP(6L),
    REACTIVATION_LAID_UP(7L),
    TOW_FITNESS(8L);

    private final Long value;

    ClassRecommendation(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
