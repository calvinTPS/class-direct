package com.baesystems.ai.lr.enums;

public enum FollowUpActionTemplates
{
    PR17(1L),
    EIC(2L),
    NEW_CLASS_DEFECTS(3L),
    HULL_AND_OR_MACHINERY_ALTERATION(4L),
    CUSTOMER_DATA_CHANGE(5L),
    NOT_IN_CLASS(6L),
    NAVAL_VESSEL(7L),
    NON_STANDARD_RECOMMENDATION(8L),
    OUTSIDE_RANGE_DATES(9L),
    CHANGE_OF_FLAG(10L),
    RE_OPENED_JOB(11L),
    FSR_REJECTED(12L),
    SERVICE(13L),
    AN_RECOMMENDATION(14L),
    AI_RECOMMENDATION(15L);

    private final Long value;

    FollowUpActionTemplates(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }
}
