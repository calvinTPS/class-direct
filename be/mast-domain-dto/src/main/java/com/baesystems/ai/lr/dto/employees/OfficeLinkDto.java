package com.baesystems.ai.lr.dto.employees;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.DynamicDisplayName;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.enums.ReferenceDataSubSet;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OfficeLinkDto extends BaseDto
{
    private static final long serialVersionUID = 4681535730575213262L;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.OFFICE))
    private LinkResource office;

    @NotNull
    @Valid
    @ConflictAware(dynamicValueDisplayName = @DynamicDisplayName(targetType = ReferenceDataSubSet.OFFICE_ROLES))
    private LinkResource officeRole;

    private Long clientImo;

    private String clientName;

    private String contractNumber;

    private Date contractStartDate;

    private Date contractEndDate;

    public LinkResource getOffice()
    {
        return this.office;
    }

    public void setOffice(final LinkResource office)
    {
        this.office = office;
    }

    public LinkResource getOfficeRole()
    {
        return this.officeRole;
    }

    public void setOfficeRole(final LinkResource officeRole)
    {
        this.officeRole = officeRole;
    }

    public Long getClientImo()
    {
        return this.clientImo;
    }

    public void setClientImo(final Long clientImo)
    {
        this.clientImo = clientImo;
    }

    public String getClientName()
    {
        return this.clientName;
    }

    public void setClientName(final String clientName)
    {
        this.clientName = clientName;
    }

    public String getContractNumber()
    {
        return this.contractNumber;
    }

    public void setContractNumber(final String contractNumber)
    {
        this.contractNumber = contractNumber;
    }

    public Date getContractStartDate()
    {
        return this.contractStartDate;
    }

    public void setContractStartDate(final Date contractStartDate)
    {
        this.contractStartDate = contractStartDate;
    }

    public Date getContractEndDate()
    {
        return this.contractEndDate;
    }

    public void setContractEndDate(final Date contractEndDate)
    {
        this.contractEndDate = contractEndDate;
    }
}
