package com.baesystems.ai.lr.constants;

public interface MastConstants
{
    String TRUSTED_SYSTEM_PREFIX = "trusted-system";
    String ADFS_GROUP_PROD = "PROD-MAST";
    String ADFS_GROUP_TEST = "TEST-MAST";
    String CDF = "cdf";
    String MAST_CLIENT = "mast-client-v2";
}
