package com.baesystems.ai.lr.dto.dm;

import java.util.Date;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class WorkItemDueDateDto extends BaseDto
{
    private static final long serialVersionUID = -1769678018595245769L;

    private Long assetId;
    private Long assetItemId;
    private String imoNumber;

    @Size(max = 5)
    private String parentMasterlistNumber;

    @Size(max = 5)
    private String masterlistNumber;

    private Integer cycle;
    private Date dueDate;
    private Integer adjCycle;
    private Date adjDueDate;

    @Size(max = 3)
    private String actionFlag;

    private Date assignedDate;

    private Date assignedDateManual;

    @Size(max = 10)
    private String serviceCode;

    public Long getAssetId()
    {
        return this.assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getAssetItemId()
    {
        return this.assetItemId;
    }

    public void setAssetItemId(final Long assetItemId)
    {
        this.assetItemId = assetItemId;
    }

    public String getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public String getParentMasterlistNumber()
    {
        return this.parentMasterlistNumber;
    }

    public void setParentMasterlistNumber(final String parentMasterlistNumber)
    {
        this.parentMasterlistNumber = parentMasterlistNumber;
    }

    public String getMasterlistNumber()
    {
        return this.masterlistNumber;
    }

    public void setMasterlistNumber(final String masterlistNumber)
    {
        this.masterlistNumber = masterlistNumber;
    }

    public Integer getCycle()
    {
        return this.cycle;
    }

    public void setCycle(final Integer cycle)
    {
        this.cycle = cycle;
    }

    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Integer getAdjCycle()
    {
        return this.adjCycle;
    }

    public void setAdjCycle(final Integer adjCycle)
    {
        this.adjCycle = adjCycle;
    }

    public Date getAdjDueDate()
    {
        return this.adjDueDate;
    }

    public void setAdjDueDate(final Date adjDueDate)
    {
        this.adjDueDate = adjDueDate;
    }

    public String getActionFlag()
    {
        return this.actionFlag;
    }

    public void setActionFlag(final String actionFlag)
    {
        this.actionFlag = actionFlag;
    }

    public Date getAssignedDate()
    {
        return assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public Date getAssignedDateManual()
    {
        return assignedDateManual;
    }

    public void setAssignedDateManual(final Date assignedDateManual)
    {
        this.assignedDateManual = assignedDateManual;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

}
