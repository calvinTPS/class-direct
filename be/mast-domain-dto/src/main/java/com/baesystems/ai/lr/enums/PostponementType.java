package com.baesystems.ai.lr.enums;

import java.util.Arrays;

public enum PostponementType
{
    CONDITIONALLY_APPROVED(1L),
    APPROVED(2L);

    private final Long value;

    private PostponementType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }

    /**
     * Returns the enum that matches the passed id, or CONDITIONALLY_APPROVED by default.
     *
     * @param id
     * @return
     */
    public static PostponementType getTypeForId(final Long id)
    {
        return Arrays
                .stream(PostponementType.values())
                .filter(status -> status.getValue().equals(id))
                .findFirst()
                .orElse(CONDITIONALLY_APPROVED);
    }
}
