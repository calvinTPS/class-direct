package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class FlagStatePageResourceDto extends BasePageResource<FlagStateDto>
{
    private List<FlagStateDto> content;

    @Override
    public List<FlagStateDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<FlagStateDto> content)
    {
        this.content = content;
    }

}
