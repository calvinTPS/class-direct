package com.baesystems.ai.lr.enums;
public enum DueStatus
{
    NOT_DUE(1L),
    DUE(2L),
    OVERDUE(3L);

    private final Long value;

    DueStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
