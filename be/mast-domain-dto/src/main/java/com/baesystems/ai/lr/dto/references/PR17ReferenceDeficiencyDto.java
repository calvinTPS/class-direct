package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;

public class PR17ReferenceDeficiencyDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -2397903004644238707L;

    @Valid
    private LinkResource deficiencyGroup;

    @NotNull
    private Long displayOrder;

    public LinkResource getDeficiencyGroup()
    {
        return deficiencyGroup;
    }

    public void setDeficiencyGroup(final LinkResource deficiencyGroup)
    {
        this.deficiencyGroup = deficiencyGroup;
    }

    public Long getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder)
    {
        this.displayOrder = displayOrder;
    }

}
