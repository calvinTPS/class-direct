package com.baesystems.ai.lr.dto.ncns;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.util.List;

public class MajorNCNPageResourceDto extends BasePageResource<MajorNCNDto>
{
    private List<MajorNCNDto> content;

    @Override
    public List<MajorNCNDto> getContent()
    {
        return this.content;
    }

    @Override
    public void setContent(final List<MajorNCNDto> content)
    {
        this.content = content;
    }
}
