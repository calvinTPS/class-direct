package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsHitlDto extends IhsBaseDto
{
    private static final long serialVersionUID = -1443333557922813103L;

    private Integer netTonnage;

    public Integer getNetTonnage()
    {
        return netTonnage;
    }

    public void setNetTonnage(final Integer netTonnage)
    {
        this.netTonnage = netTonnage;
    }
}
