package com.baesystems.ai.lr.dto.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to specify a string to replace a given reference data name with. For the replacement to work the reference
 * data name must match exactly.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OverrideDisplayName
{
    /**
     * The name in the reference data to replace
     */
    String referenceDataName();

    /**
     * The string to replace it with.
     */
    String displayName();
}
