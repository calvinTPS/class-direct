package com.baesystems.ai.lr.dto.services;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.Dto;

import static com.baesystems.ai.lr.dto.services.SurveyListDto.ENTITY_NAME;

@EntityName(ENTITY_NAME)
public class SurveyListDto extends StalenessListDto implements Dto, Serializable
{
    public static final String ENTITY_NAME = "Survey";
    private static final long serialVersionUID = 3108037396746737893L;

    @NotNull
    @Valid
    private List<SurveyDto> surveys;

    public List<SurveyDto> getSurveys()
    {
        return this.surveys;
    }

    public void setSurveys(final List<SurveyDto> surveys)
    {
        this.surveys = surveys;
    }

    @Override
    protected String entityName()
    {
        return ENTITY_NAME;
    }
}
