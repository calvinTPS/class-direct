package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ServiceStatus
{
    NOT_STARTED(1L),
    PART_HELD(2L),
    HELD(3L),
    COMPLETE(4L);

    private final Long value;

    private ServiceStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }

    /**
     * Returns the enum that matches the passed id, or NOT_STARTED by default.
     *
     * @param id
     * @return
     */
    public static ServiceStatus getTypeForId(final Long id)
    {
        return Arrays
                .stream(ServiceStatus.values())
                .filter(status -> status.getValue().equals(id))
                .findFirst()
                .orElse(NOT_STARTED);
    }

    /**
     * @return a list of ServiceStatus values that count as open
     */
    public static final List<Long> getServiceStatusForNonCompleted()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(NOT_STARTED.getValue());
        statuses.add(PART_HELD.getValue());
        statuses.add(HELD.getValue());

        return statuses;
    }
}
