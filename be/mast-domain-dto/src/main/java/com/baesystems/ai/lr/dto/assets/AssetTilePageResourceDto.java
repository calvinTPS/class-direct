package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class AssetTilePageResourceDto extends BasePageResource<AssetTileDto>
{
    @Valid
    private List<AssetTileDto> content;

    @Override
    public List<AssetTileDto> getContent()
    {
        return this.content;
    }

    @Override
    public void setContent(final List<AssetTileDto> content)
    {
        this.content = content;
    }

}
