package com.baesystems.ai.lr.dto.jobs;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class JobPageResourceDto extends BasePageResource<JobDto>
{
    @Valid
    private List<JobDto> content;

    @Override
    public List<JobDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<JobDto> content)
    {
        this.content = content;
    }

}
