package com.baesystems.ai.lr.dto.base;

import com.baesystems.ai.lr.dto.Versioned;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;

public class VersionedDto extends AuditedDto implements Versioned
{
    private static final long serialVersionUID = -7182490646368283264L;

    @ConflictAware
    private Long versionId;

    @ConflictAware
    private String versionType;

    @Override
    public Long getVersionId()
    {
        return this.versionId;
    }

    public void setVersionId(final Long versionId)
    {
        this.versionId = versionId;
    }

    public String getVersionType()
    {
        return this.versionType;
    }

    public void setVersionType(final String versionType)
    {
        this.versionType = versionType;
    }

}
