package com.baesystems.ai.lr.dto.jobs;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.base.Dto;

public class JobBundleUpdatedDateDto implements Dto, Serializable
{
    private static final long serialVersionUID = 8441613359230820417L;

    private String bundleLastUpdatedDate;

    public String getBundleLastUpdatedDate()
    {
        return bundleLastUpdatedDate;
    }

    public void setBundleLastUpdatedDate(final String bundleLastUpdatedDate)
    {
        this.bundleLastUpdatedDate = bundleLastUpdatedDate;
    }
}
