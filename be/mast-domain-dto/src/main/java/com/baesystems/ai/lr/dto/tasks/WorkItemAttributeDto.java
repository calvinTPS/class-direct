package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class WorkItemAttributeDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = -5290824531522351772L;

    private String description;

    @ConflictAware
    private String attributeValueString;

    @Valid
    private LinkResource workItem;

    @Valid
    private LinkResource attributeDataType;

    @Valid
    private WorkItemConditionalAttributeDto workItemConditionalAttribute;

    @Valid
    private List<LinkResource> allowedAttributeValues;

    @Valid
    private LinkResource followedOnFrom;

    @Valid
    private LinkResource parent;

    @Valid
    private LinkResource checklistAdditionalInfo;

    private Integer entryNumber;

    private String previousAttributeValueString;

    private String stalenessHash;

    private Boolean active;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public LinkResource getAttributeDataType()
    {
        return attributeDataType;
    }

    public void setAttributeDataType(final LinkResource attributeDataType)
    {
        this.attributeDataType = attributeDataType;
    }

    public List<LinkResource> getAllowedAttributeValues()
    {
        return allowedAttributeValues;
    }

    public void setAllowedAttributeValues(final List<LinkResource> allowedAttributeValues)
    {
        this.allowedAttributeValues = allowedAttributeValues;
    }

    public String getAttributeValueString()
    {
        return attributeValueString;
    }

    public void setAttributeValueString(final String attributeValueString)
    {
        this.attributeValueString = attributeValueString;
    }

    public WorkItemConditionalAttributeDto getWorkItemConditionalAttribute()
    {
        return workItemConditionalAttribute;
    }

    public void setWorkItemConditionalAttribute(final WorkItemConditionalAttributeDto workItemConditionalAttribute)
    {
        this.workItemConditionalAttribute = workItemConditionalAttribute;
    }

    public LinkResource getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final LinkResource workItem)
    {
        this.workItem = workItem;
    }

    public LinkResource getFollowedOnFrom()
    {
        return followedOnFrom;
    }

    public void setFollowedOnFrom(final LinkResource followedOnFrom)
    {
        this.followedOnFrom = followedOnFrom;
    }

    public LinkResource getParent()
    {
        return parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public LinkResource getChecklistAdditionalInfo()
    {
        return checklistAdditionalInfo;
    }

    public void setChecklistAdditionalInfo(final LinkResource checklistAdditionalInfo)
    {
        this.checklistAdditionalInfo = checklistAdditionalInfo;
    }

    public Integer getEntryNumber()
    {
        return this.entryNumber;
    }

    public void setEntryNumber(final Integer entryNumber)
    {
        this.entryNumber = entryNumber;
    }

    public String getPreviousAttributeValueString()
    {
        return previousAttributeValueString;
    }

    public void setPreviousAttributeValueString(final String previousAttributeValueString)
    {
        this.previousAttributeValueString = previousAttributeValueString;
    }

    public Boolean getActive()
    {
        return active;
    }

    public void setActive(final Boolean active)
    {
        this.active = active;
    }

    @Override
    public String getStalenessHash()
    {
        return stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        stalenessHash = newStalenessHash;

    }

    /**
     * Work item attributes are saved as part of the work item, but the values are material parts of the work item so we
     * should detect changes in them and class this as a change to the work item. This method is provided as a means of
     * getting the important fields from the attribute to add to the hash on the work item.
     */
    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "WorkItemAttributeDto [attributeValueString=" + getAttributeValueString() + "]";
    }
}
