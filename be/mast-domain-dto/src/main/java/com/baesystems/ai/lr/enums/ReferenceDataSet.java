package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Enum for the base reference data sets. This should match the list of sets in
 * the allowableValues tag in rest-references-context.xml
 */
public enum ReferenceDataSet
{
    ASSET("asset"),
    ATTACHMENT("attachment"),
    CASE("case"),
    DEFECT("defect"),
    EMPLOYEE("employee"),
    FEES("fees"),
    FLAG("flag"),
    JOB("job"),
    MISCELLANEOUS("miscellaneous"),
    PARTY("party"),
    PRODUCT("product"),
    REPAIR("repair"),
    SERVICE("service"),
    TASK("task"),
    VERSION("version");

    private final String value;

    ReferenceDataSet(final String value)
    {
        this.value = value;
    }

    public final String getValue()
    {
        return this.value;
    }

    /*
     * The API for root sets is the same as the name.
     */
    public final String getApi()
    {
        return getValue();
    }

    public static final ReferenceDataSet getSetForName(final String name)
    {
        return Arrays.stream(values()).filter(set -> set.getValue().equals(name)).findAny().orElseGet(() -> null);
    }

    public static final ReferenceDataSet getSetForApi(final String api)
    {
        return getSetForName(api);
    }

    public static final List<String> getApiList()
    {
        return Arrays.stream(values()).map(ReferenceDataSet::getApi).collect(Collectors.toList());
    }
}
