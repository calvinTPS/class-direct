package com.baesystems.ai.lr.dto.ihs;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.base.IhsBaseDto;

public class IhsAssetDetailsDto extends IhsBaseDto
{

    private static final long serialVersionUID = 7292783580182959304L;

    @Valid
    private IhsAssetDto ihsAsset;

    @Valid
    private IhsGroupFleetDto ihsGroupFleet;

    @Valid
    private IhsHistDto ihsHist;

    @Valid
    private IhsCustomerDetailsDto registeredOwner;

    @Valid
    private IhsCustomerDetailsDto shipManager;

    @Valid
    private IhsCustomerDetailsDto docCompany;

    @Valid
    private IhsCustomerDetailsDto technicalManager;

    public IhsAssetDto getIhsAsset()
    {
        return ihsAsset;
    }

    public void setIhsAsset(final IhsAssetDto ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public IhsGroupFleetDto getIhsGroupFleet()
    {
        return ihsGroupFleet;
    }

    public void setIhsGroupFleet(final IhsGroupFleetDto ihsGroupFleet)
    {
        this.ihsGroupFleet = ihsGroupFleet;
    }

    public IhsHistDto getIhsHist()
    {
        return ihsHist;
    }

    public void setIhsHist(final IhsHistDto ihsHist)
    {
        this.ihsHist = ihsHist;
    }

    public IhsCustomerDetailsDto getRegisteredOwner()
    {
        return registeredOwner;
    }

    public void setRegisteredOwner(final IhsCustomerDetailsDto registeredOwner)
    {
        this.registeredOwner = registeredOwner;
    }

    public IhsCustomerDetailsDto getShipManager()
    {
        return shipManager;
    }

    public void setShipManager(final IhsCustomerDetailsDto shipManager)
    {
        this.shipManager = shipManager;
    }

    public IhsCustomerDetailsDto getDocCompany()
    {
        return docCompany;
    }

    public void setDocCompany(final IhsCustomerDetailsDto docCompany)
    {
        this.docCompany = docCompany;
    }

    public IhsCustomerDetailsDto getTechnicalManager()
    {
        return technicalManager;
    }

    public void setTechnicalManager(final IhsCustomerDetailsDto technicalManager)
    {
        this.technicalManager = technicalManager;
    }
}
