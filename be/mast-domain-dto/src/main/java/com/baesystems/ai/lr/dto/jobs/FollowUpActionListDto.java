package com.baesystems.ai.lr.dto.jobs;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.dto.services.StalenessListDto;

import static com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto.ENTITY_NAME;

@EntityName(ENTITY_NAME)
public class FollowUpActionListDto extends StalenessListDto implements Dto, Serializable
{
    public static final String ENTITY_NAME = "Vetting Action";
    private static final long serialVersionUID = -7903525565897255408L;

    @NotNull
    @Valid
    private List<FollowUpActionDto> followUpActions;

    public List<FollowUpActionDto> getFollowUpActions()
    {
        return this.followUpActions;
    }

    public void setFollowUpActions(final List<FollowUpActionDto> followUpActions)
    {
        this.followUpActions = followUpActions;
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
