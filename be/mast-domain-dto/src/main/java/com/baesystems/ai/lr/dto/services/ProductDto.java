package com.baesystems.ai.lr.dto.services;

import static com.baesystems.ai.lr.dto.services.ProductDto.ENTITY_NAME;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.AuditedDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@EntityName(ENTITY_NAME)
public class ProductDto extends AuditedDto implements UpdateableJobBundleEntity
{
    public static final String ENTITY_NAME = "Product";
    private static final long serialVersionUID = -3562734472471817574L;

    private String stalenessHash;

    @NotNull
    private LinkResource productCatalogue;

    @NotNull
    @Size(message = "invalid length", max = 45)
    private String name;

    @Size(message = "invalid length", max = 45)
    private String description;

    @NotNull
    private Integer displayOrder;

    @NotNull
    @Valid
    private LinkResource productGroup;

    @NotNull
    @Valid
    private LinkResource productType;

    @Valid
    private LinkResource schedulingRegime;

    @JsonProperty
    private Boolean active;

    public LinkResource getProductCatalogue()
    {
        return this.productCatalogue;
    }

    public void setProductCatalogue(final LinkResource productCatalogue)
    {
        this.productCatalogue = productCatalogue;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public LinkResource getProductGroup()
    {
        return this.productGroup;
    }

    public void setProductGroup(final LinkResource productGroup)
    {
        this.productGroup = productGroup;
    }

    public LinkResource getProductType()
    {
        return this.productType;
    }

    public void setProductType(final LinkResource productType)
    {
        this.productType = productType;
    }

    public LinkResource getSchedulingRegime()
    {
        return this.schedulingRegime;
    }

    public void setSchedulingRegime(final LinkResource schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }

    @JsonProperty
    public Boolean getActive()
    {
        return active;
    }

    @JsonIgnore
    public void setActive(final Boolean active)
    {
        this.active = active;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "ProductDto ["
                + "schedulingRegime=" + this.getIdOrNull(this.schedulingRegime)
                + ", productCatalogue=" + this.getIdOrNull(this.productCatalogue)
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
