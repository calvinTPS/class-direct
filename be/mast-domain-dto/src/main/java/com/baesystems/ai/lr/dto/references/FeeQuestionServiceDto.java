package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.ImmutableReferenceDataDto;

public class FeeQuestionServiceDto extends ImmutableReferenceDataDto
{
    private static final long serialVersionUID = -8871937428879236957L;

    private LinkResource feeQuestion;

    private String serviceCode;

    public LinkResource getFeeQuestion()
    {
        return feeQuestion;
    }

    public void setFeeQuestion(final LinkResource feeQuestion)
    {
        this.feeQuestion = feeQuestion;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }
}
