package com.baesystems.ai.lr.dto.external.boomi.assets;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.util.List;

public class AssetPageResourceDto extends BasePageResource<AssetDto>
{
    private List<AssetDto> content;

    @Override
    public List<AssetDto> getContent()
    {
        return this.content;
    }

    @Override
    public void setContent(final List<AssetDto> content)
    {
        this.content = content;
    }
}
