package com.baesystems.ai.lr.dto.codicils;

import java.io.Serializable;
import java.util.List;

import com.baesystems.ai.lr.dto.base.Dto;

public class CodicilHistoryContentDto implements Dto, Serializable
{
    private static final long serialVersionUID = -8740129685701297121L;

    private List<CodicilHistoryDto> codicilHistories;

    public List<CodicilHistoryDto> getCodicilHistory()
    {
        return codicilHistories;
    }

    public void setCodicilHistory(final List<CodicilHistoryDto> codicilHistory)
    {
        this.codicilHistories = codicilHistory;
    }
}
