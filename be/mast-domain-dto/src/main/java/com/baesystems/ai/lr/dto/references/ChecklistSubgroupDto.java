package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;

public class ChecklistSubgroupDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 3475600516197584363L;

    private LinkResource checklistGroup;

    private Integer displayOrder;

    public LinkResource getChecklistGroup()
    {
        return this.checklistGroup;
    }

    public void setChecklistGroup(final LinkResource checklistGroup)
    {
        this.checklistGroup = checklistGroup;
    }

    public Integer getDisplayOrder()
    {
      return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
      this.displayOrder = displayOrder;
    }
}
