package com.baesystems.ai.lr.dto.defects;

import static com.baesystems.ai.lr.dto.defects.RectificationDto.ENTITY_NAME;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.UpdateableJobBundleEntity;
import com.baesystems.ai.lr.dto.annotations.ConflictAware;
import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.annotations.LinkedEntityMustExist;
import com.baesystems.ai.lr.enums.JobBundleEntityType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@EntityName(ENTITY_NAME)
public class RectificationDto extends FixDto implements UpdateableJobBundleEntity
{
    private static final long serialVersionUID = 2207746146624404895L;

    public static final String ENTITY_NAME = "Rectification";

    @Valid
    @ConflictAware
    @LinkedEntityMustExist(type = JobBundleEntityType.WIP_DEFICIENCY)
    private LinkResource defect;

    @Valid
    @ConflictAware
    private LinkResource codicil;

    @NotNull
    @ConflictAware
    @Size(message = "invalid length", min = 1, max = 2000)
    private String narrative;

    @NotNull
    @Valid
    @ConflictAware
    private LinkResource repairAction;

    @Valid
    @ConflictAware
    private List<RectificationChecklistDto> rectificationChecklists;

    private String stalenessHash;

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public LinkResource getCodicil()
    {
        return this.codicil;
    }

    public void setCodicil(final LinkResource codicil)
    {
        this.codicil = codicil;
    }

    public String getNarrative()
    {
        return this.narrative;
    }

    public void setNarrative(final String narrative)
    {
        this.narrative = narrative;
    }

    public LinkResource getRepairAction()
    {
        return this.repairAction;
    }

    public void setRepairAction(final LinkResource repairAction)
    {
        this.repairAction = repairAction;
    }

    public List<RectificationChecklistDto> getRectificationChecklists()
    {
        return this.rectificationChecklists;
    }

    public void setRectificationChecklists(final List<RectificationChecklistDto> rectificationChecklists)
    {
        this.rectificationChecklists = rectificationChecklists;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "RepairDto [narrative=" + this.getNarrative()
                + ", defect=" + this.getIdOrNull(this.getDefect())
                + ", survey=" + this.getSurvey()
                + ", repairAction=" + this.getRepairAction()
                + ", parent=" + this.getIdOrNull(this.getParent())
                + ", codicil=" + this.getIdOrNull(this.getCodicil())
                + ", checklists=" + this.getStringRepresentationOfList(this.getRectificationChecklists())
                + ", updatedOn=" + this.getStringRepresentationOfDate(this.getUpdatedOn())
                + ", updatedOnBehalfOf=" + this.getIdOrNull(this.getUpdatedOnBehalfOf())
                + ", getId()=" + this.getId() + "]";
    }

    @Override
    public String entityName()
    {
        return ENTITY_NAME;
    }
}
