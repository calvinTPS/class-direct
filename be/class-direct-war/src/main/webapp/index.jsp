<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	String contextPath = request.getContextPath().equals("/") ? "" : request.getContextPath();
    String redirectURL = "webjars/swagger-ui/3.0.10/index.html?url=" + contextPath + "/api-docs/cdContext";
    response.sendRedirect(redirectURL);
%>
