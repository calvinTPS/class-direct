package com.baesystems.ai.lr.cd.be.cs10.test;

import com.baesystems.ai.lr.cd.be.cs10.CS10ClientFactory;
import com.baesystems.ai.lr.cd.be.cs10.CS10Service;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10File;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10FileVersion;
import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import com.baesystems.ai.lr.cd.be.cs10.impl.CS10ServiceImpl;
import com.opentext.livelink.service.core.Attachment;
import com.opentext.livelink.service.core.Authentication;
import com.opentext.livelink.service.docman.DocumentManagement;
import com.opentext.livelink.service.docman.Node;
import com.opentext.livelink.service.docman.NodeVersionInfo;
import com.opentext.livelink.service.docman.Version;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lr.internal.ShipDMServiceSoap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.cs10.CS10Service}.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class CS10ServiceTest {

  /**
   * Default token.
   */
  private static final String DEFAULT_TOKEN =
    "QmFzZTY0IGlzIGEgZ2VuZXJpYyB0ZXJtIGZvciBhIG51bWJlciBvZiBzaW1pbGFyIGVuY29W5nIHNjaGVtZXMgdGhhdCBlbmNvZGUgYmluYXJ==";

  /**
   * Default node id.
   */
  private static final Integer DEFAULT_NODE_ID = 1;

  /**
   * Default version.
   */
  private static final Integer DEFAULT_VERSION = 1;

  /**
   * Sample file content.
   */
  private static final String FILE_CONTENT_SAMPLE = "123412312312312312321";

  /**
   * CS10 service.
   */
  @Autowired
  private CS10Service cs10Service;

  /**
   * Factory.
   */
  @Autowired
  private CS10ClientFactory factory;

  /**
   * Default username.
   */
  @Value("${cs10.service.auth.username}")
  private String defaultUsername;

  /**
   * Default password.
   */
  @Value("${cs10.service.auth.password}")
  private String defaultPassword;

  /**
   * Should return generated token.
   * @throws CS10Exception when error.
   */
  @Test
  public final void successfullyGenerateAuthToken() throws CS10Exception {
    final Authentication authentication = mock(Authentication.class);
    when(authentication.authenticateUser(anyString(), anyString())).thenReturn(DEFAULT_TOKEN);
    when(factory.createAuthenticationService()).thenReturn(authentication);

    final String token = cs10Service.getAuthenticationToken(defaultUsername, defaultPassword);
    assertNotNull(token);
    verify(authentication, atLeastOnce()).authenticateUser(eq(defaultUsername), eq(defaultPassword));
  }

  /**
   * Will throw {@link CS10Exception} during web service exception.
   *
   * @throws CS10Exception when error.
   */
  @Test(expected = CS10Exception.class)
  public final void exceptionCatchDuringTokenGeneration() throws CS10Exception {
    final Authentication authentication = mock(Authentication.class);
    when(authentication.authenticateUser(anyString(), anyString())).thenThrow(new WebServiceException());
    when(factory.createAuthenticationService()).thenReturn(authentication);

    cs10Service.getAuthenticationToken(defaultUsername, defaultPassword);
  }

  /**
   * Service will returns file contents in base64 format.
   *
   * @throws CS10Exception when error.
   */
  @Test
  public final void successfullyGetFileContents() throws CS10Exception {
    final DocumentManagement documentManagement = mock(DocumentManagement.class);

    mockStandardToken();
    final Attachment attachment = new Attachment();
    attachment.setContents(FILE_CONTENT_SAMPLE.getBytes());
    when(documentManagement.getVersionContents(anyInt(), anyInt(), any())).thenReturn(attachment);

    final Version version = new Version();
    version.setID(DEFAULT_NODE_ID);
    version.setMimeType("application/pdf");
    version.setModifyDate(createDefaultCalendar());
    when(documentManagement.getVersion(anyInt(), anyInt(), any())).thenReturn(version);

    final ShipDMServiceSoap dmServiceSoap = mock(ShipDMServiceSoap.class);
    when(dmServiceSoap.downloadClientDocs(anyString(), anyBoolean())).thenReturn(FILE_CONTENT_SAMPLE.getBytes());
    when(factory.createDocumentManagementService()).thenReturn(documentManagement);
    when(factory.createShipDMService()).thenReturn(dmServiceSoap);

    final CS10File file = cs10Service.getFileContents(DEFAULT_NODE_ID, DEFAULT_VERSION, Boolean.FALSE);
    final byte[] contents = file.getContents();
    assertNotNull(contents);
    assertNotEquals(0, contents.length);
    verify(documentManagement, atLeastOnce()).getVersionContents(eq(DEFAULT_NODE_ID), eq(DEFAULT_VERSION), any());
    verify(documentManagement, atLeastOnce()).getVersion(eq(DEFAULT_NODE_ID), eq(DEFAULT_VERSION), any());
  }

  /**
   * Service will catch and throw {@link CS10Exception} during error.
   *
   * @throws CS10Exception when error.
   */
  @Test(expected = CS10Exception.class)
  public final void exceptionCatchDuringGetFileContents() throws CS10Exception {
    mockStandardToken();
    final DocumentManagement documentManagement = mock(DocumentManagement.class);
    when(documentManagement.getVersionContents(anyInt(), anyInt(), any())).thenThrow(new WebServiceException());
    when(factory.createDocumentManagementService()).thenReturn(documentManagement);

    cs10Service.getFileContents(DEFAULT_NODE_ID, DEFAULT_VERSION, Boolean.FALSE);
  }

  /**
   * Successfully get file download link.
   *
   * @throws CS10Exception when error.
   */
  @Test(expected = CS10Exception.class)
  public final void successfullyGetFileDownloadLink() throws CS10Exception {
    cs10Service.getFileDownloadLink(DEFAULT_NODE_ID, DEFAULT_VERSION);
  }

  /**
   * Service will returns list of file versions.
   *
   * @throws CS10Exception when error.
   */
  @Test
  public final void getListOfFileVersions() throws CS10Exception {
    mockStandardToken();

    final NodeVersionInfo versionInfo = new NodeVersionInfo();
    versionInfo.getVersions().addAll(Collections.singleton(createVersion()));
    final Node node = new Node();
    node.setVersionInfo(versionInfo);
    final DocumentManagement documentManagement = mock(DocumentManagement.class);
    when(documentManagement.getNode(eq(DEFAULT_NODE_ID), any())).thenReturn(node);
    when(factory.createDocumentManagementService()).thenReturn(documentManagement);

    final List<CS10FileVersion> versions = cs10Service.getFileVersions(DEFAULT_NODE_ID);
    assertNotNull(versions);
    assertFalse(versions.isEmpty());
    verify(documentManagement, atLeastOnce()).getNode(eq(DEFAULT_NODE_ID), any());
  }

  /**
   * Service will return no version found error.
   *
   * @throws CS10Exception when error.
   */
  @Test(expected = CS10Exception.class)
  public final void noVersionFound() throws CS10Exception {
    mockStandardToken();
    final Node node = new Node();
    final DocumentManagement documentManagement = mock(DocumentManagement.class);
    when(documentManagement.getNode(eq(DEFAULT_NODE_ID), any())).thenReturn(node);
    when(factory.createDocumentManagementService()).thenReturn(documentManagement);

    cs10Service.getFileVersions(DEFAULT_NODE_ID);
  }

  /**
   * Service will catch the exception.
   *
   * @throws CS10Exception when error.
   */
  @Test(expected = CS10Exception.class)
  public final void catchExceptionWhenGettingFileVersions() throws CS10Exception {
    mockStandardToken();
    final DocumentManagement documentManagement = mock(DocumentManagement.class);
    when(documentManagement.getNode(eq(DEFAULT_NODE_ID), any())).thenThrow(new WebServiceException());
    when(factory.createDocumentManagementService()).thenReturn(documentManagement);

    cs10Service.getFileVersions(DEFAULT_NODE_ID);
  }

  /**
   * Successfully create new document.
   *
   * @throws CS10Exception when error.
   * @throws IOException when error.
   */
  @Test
  public final void successfullyCreateDocument() throws CS10Exception, IOException {
    mockStandardToken();

    final DocumentManagement documentManagement = mock(DocumentManagement.class);
    when(documentManagement.createSimpleDocument(eq(DEFAULT_NODE_ID), anyString(), any(), any()))
      .thenReturn(DEFAULT_NODE_ID);
    when(factory.createDocumentManagementService()).thenReturn(documentManagement);

    final InputStream inputStream = getClass().getClassLoader().getResourceAsStream("sample_file.pdf");
    final File tempFile = File.createTempFile(String.valueOf(inputStream.hashCode()), ".tmp");
    tempFile.deleteOnExit();

    Integer nodeId = cs10Service.createDocuments(DEFAULT_NODE_ID, tempFile);
    assertNotNull(nodeId);
    assertEquals(DEFAULT_NODE_ID, nodeId);
    verify(documentManagement, atMost(1)).createSimpleDocument(anyInt(), anyString(), any(), any());
  }

  /**
   * Successfully refresh authentication token.
   *
   * @throws CS10Exception when error.
   */
  @Test
  public final void successfullyRefreshAuthToken() throws CS10Exception {
    final String refreshedToken = new StringBuilder(DEFAULT_TOKEN).reverse().toString();

    final Authentication authentication = mock(Authentication.class);
    when(authentication.authenticateUser(anyString(), anyString())).thenReturn(DEFAULT_TOKEN);
    when(authentication.refreshToken(any())).thenReturn(refreshedToken);
    when(factory.createAuthenticationService()).thenReturn(authentication);

    String token = cs10Service.getAuthenticationToken(defaultUsername, defaultPassword);
    assertEquals(DEFAULT_TOKEN, token);
    token = cs10Service.refreshAuthToken(token);
    assertEquals(refreshedToken, token);
    verify(authentication, atLeastOnce()).authenticateUser(anyString(), anyString());
    verify(authentication, atLeastOnce()).refreshToken(any());
  }

  /**
   * Create version object.
   * @return object.
   */
  private Version createVersion() {
    final Version version = new Version();
    version.setCreateDate(createDefaultCalendar());
    version.setModifyDate(createDefaultCalendar());
    version.setFileCreateDate(createDefaultCalendar());
    version.setFileDataSize(FILE_CONTENT_SAMPLE.getBytes().length);
    version.setFilename(FILE_CONTENT_SAMPLE);
    version.setFileType("log");
    version.setVerMajor(DEFAULT_VERSION);
    version.setVerMinor(DEFAULT_VERSION);
    version.setName(FILE_CONTENT_SAMPLE);
    version.setNodeID(DEFAULT_NODE_ID);
    return version;
  }

  /**
   * Default calendar.
   *
   * @return calendar.
   */
  private XMLGregorianCalendar createDefaultCalendar() {
    final XMLGregorianCalendar calendar = mock(XMLGregorianCalendar.class);
    when(calendar.getYear()).thenReturn(LocalDate.now().getYear());
    when(calendar.getMonth()).thenReturn(LocalDate.now().getMonthValue());
    when(calendar.getDay()).thenReturn(LocalDate.now().getDayOfMonth());

    return calendar;
  }

  /**
   * Mock standard CS10 token.
   *
   * @throws CS10Exception when error.
   */
  private void mockStandardToken() throws CS10Exception {
    final Authentication authentication = mock(Authentication.class);
    when(authentication.authenticateUser(anyString(), anyString())).thenReturn(DEFAULT_TOKEN);
    when(factory.createAuthenticationService()).thenReturn(authentication);
  }

  /**
   * Inner config.
   */
  @Configuration
  public static class Config {

    /**
     * Properties reader.
     *
     * @return properties.
     */
    @Bean
    public PropertyPlaceholderConfigurer configurer() {
      final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
      configurer.setLocations(new ClassPathResource("cs10.properties")
                                , new ClassPathResource("test-credential.properties"));
      return configurer;
    }

    /**
     * Create mock client factory.
     *
     * @throws MalformedURLException when error.
     * @return factory.
     */
    @Bean(name = "CS10ClientFactory")
    public CS10ClientFactory clientFactory() throws MalformedURLException {
      return mock(CS10ClientFactory.class);
    }

    /**
     * Create CS10 service.
     *
     * @return service.
     */
    @Bean
    public CS10Service cs10Service() {
      return new CS10ServiceImpl();
    }
  }
}
