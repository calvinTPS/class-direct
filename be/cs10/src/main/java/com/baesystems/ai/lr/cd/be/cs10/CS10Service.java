package com.baesystems.ai.lr.cd.be.cs10;

import com.baesystems.ai.lr.cd.be.cs10.dto.CS10File;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10FileVersion;
import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import java.io.File;
import java.util.List;

/**
 * Service to interface with CS10 services.
 *
 * @author Faizal Sidek
 */
public interface CS10Service {

  /**
   * Get authentication token.
   *
   * @param username username.
   * @param password password.
   * @return auth token.
   * @throws CS10Exception when error.
   */
  String getAuthenticationToken(String username, String password) throws CS10Exception;

  /**
   * Refresh authentication token.
   *
   * @param oldToken old token.
   * @return new token.
   * @throws CS10Exception when error.
   */
  String refreshAuthToken(String oldToken) throws CS10Exception;

  /**
   * Returns file contents in Base64 encoded.
   *
   * @param nodeId document node id.
   * @param versionNumber document version number.
   * @param asZipPdf if true will download the file as zipped pdf format.
   * @return file contents in base64 encoded.
   * @throws CS10Exception when error.
   */
  CS10File getFileContents(Integer nodeId, Integer versionNumber, Boolean asZipPdf) throws CS10Exception;

  /**
   * Returns latest version of file content.
   *
   * @param nodeId document id.
   * @param asZipPdf if true will download as zipped pdf format.
   * @return cs10 file wrapper.
   * @throws CS10Exception when error.
   */
  CS10File getLatestFile(Integer nodeId, Boolean asZipPdf) throws CS10Exception;

  /**
   * Returns file download link.
   *
   * @param nodeId document node id.
   * @param versionNumber document version number.
   * @return file contents in base64 encoded.
   * @throws CS10Exception when error.
   */
  String getFileDownloadLink(Integer nodeId, Integer versionNumber) throws CS10Exception;

  /**
   * Get file versions.
   *
   * @param nodeId node id.
   * @return list of file versions.
   * @throws CS10Exception when error.
   */
  List<CS10FileVersion> getFileVersions(Integer nodeId) throws CS10Exception;

  /**
   * Get specific file version.
   *
   * @param nodeId node id.
   * @param versionNumber version number.
   * @return cs10 file.
   * @throws CS10Exception when error.
   */
  CS10FileVersion getFileVersion(Integer nodeId, Integer versionNumber) throws CS10Exception;

  /**
   * Create document in CS10.
   *
   * @param parentNodeId parent node
   * @param fileContents file.
   * @return node id.
   * @throws CS10Exception when error.
   */
  Integer createDocuments(Integer parentNodeId, File fileContents) throws CS10Exception;
}
