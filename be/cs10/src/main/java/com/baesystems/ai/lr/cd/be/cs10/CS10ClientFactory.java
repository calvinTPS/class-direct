package com.baesystems.ai.lr.cd.be.cs10;

import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import com.opentext.livelink.service.core.Authentication;
import com.opentext.livelink.service.core.Authentication_Service;
import com.opentext.livelink.service.docman.DocumentManagement;
import com.opentext.livelink.service.docman.DocumentManagement_Service;
import java.net.MalformedURLException;
import java.net.URL;
import org.lr.internal.ShipDMService;
import org.lr.internal.ShipDMServiceSoap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Factory bean for CS10 Client.
 *
 * @author Faizal Sidek
 */
@Service("CS10ClientFactory")
public class CS10ClientFactory {

  /**
   * CS10 Service URL.
   */
  @Value("${cs10.service.authentication.url}")
  private String cs10AuthenticationWsdlUrl;

  /**
   * CS10 DocMan url.
   */
  @Value("${cs10.service.docmgmt.url}")
  private String cs10DocumentManagementWsdlUrl;

  /**
   * URL to CS10 zipped document service.
   */
  @Value("${cs10.service.dmservice.url}")
  private String shipDmServiceWsdlUrl;

  /**
   * Initialize CS10 client.
   *
   * @throws CS10Exception when error.
   */
  public Authentication createAuthenticationService() throws CS10Exception {
    try {
      final Authentication_Service service = new Authentication_Service(new URL(cs10AuthenticationWsdlUrl));
      final Authentication authentication = service.getPort(Authentication.class);
      return service.getBasicHttpBindingAuthentication();
    } catch (MalformedURLException exception) {
      throw new CS10Exception(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
    }
  }

  /**
   * Initialize document management service.
   *
   * @return document management service.
   * @throws CS10Exception when error.
   */
  public DocumentManagement createDocumentManagementService() throws CS10Exception {
    try {
      return new DocumentManagement_Service(new URL(cs10DocumentManagementWsdlUrl))
        .getBasicHttpBindingDocumentManagement();
    } catch (MalformedURLException exception) {
      throw new CS10Exception(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
    }
  }

  /**
   * Initialize ShipDMService service.
   *
   * @return ship dm service.
   * @throws CS10Exception when initialization exception occurs.
   */
  public ShipDMServiceSoap createShipDMService() throws CS10Exception {
    try {
      return new ShipDMService(new URL(shipDmServiceWsdlUrl)).getShipDMServiceSoap();
    } catch (MalformedURLException exception) {
      throw new CS10Exception(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
    }
  }
}
