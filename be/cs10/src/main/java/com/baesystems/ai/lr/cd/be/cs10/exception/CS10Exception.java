package com.baesystems.ai.lr.cd.be.cs10.exception;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import org.springframework.http.HttpStatus;

/**
 * CS10 base exception.
 *
 * @author Faizal Sidek
 */
public class CS10Exception extends ClassDirectException {

  /**
   * Default CS10 error message.
   */
  private static final String DEFAULT_CS10_ERROR_MESSAGE = "Error connecting to CS10 server.";

  /**
   * Base constructor.
   *
   * @param errorCodeObject error code.
   * @param messageObject message.
   */
  public CS10Exception(final int errorCodeObject, final String messageObject) {
    super(errorCodeObject, messageObject);
  }

  public CS10Exception() {
    super(HttpStatus.INTERNAL_SERVER_ERROR.value(), DEFAULT_CS10_ERROR_MESSAGE);
  }
}
