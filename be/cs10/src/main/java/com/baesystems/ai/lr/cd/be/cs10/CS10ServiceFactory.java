package com.baesystems.ai.lr.cd.be.cs10;

import com.baesystems.ai.lr.cd.be.cs10.impl.CS10ServiceImpl;
import com.baesystems.ai.lr.cd.be.cs10.impl.MockCS10ServiceImpl;
import org.springframework.beans.factory.annotation.Value;

/**
 * Factory to create {@link CS10Service}.
 *
 * @author Faizal Sidek
 */
public class CS10ServiceFactory {

  /**
   * Flag to check for CS10 integration.
   */
  @Value("${cs10.service.enable}")
  private Boolean enableCS10Integration;

  /**
   * Factory method to create CS10 service.
   *
   * @return cs10 service.
   */
  public final CS10Service createCS10Service() {
    final CS10Service cs10Service;

    if (enableCS10Integration) {
      cs10Service = new CS10ServiceImpl();
    } else {
      cs10Service = new MockCS10ServiceImpl();
    }

    return cs10Service;
  }
}
