package com.baesystems.ai.lr.cd.be.cs10.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Wrapper for CS10 file download.
 *
 * @author Faizal Sidek
 */
public class CS10File implements Serializable {

  /**
   * Serial uuid.
   */
  private static final long serialVersionUID = -4184391698342269290L;

  /**
   * Node id.
   */
  private Integer nodeId;

  /**
   * Parent node id.
   */
  private Integer parentId;

  /**
   * File version number.
   */
  private Integer versionNumber;

  /**
   * Last modified date.
   */
  private LocalDateTime lastModified;

  /**
   * File name.
   */
  private String filename;

  /**
   * File size.
   */
  private Integer fileSize;

  /**
   * File content in byte array.
   */
  private byte[] contents;

  /**
   * Mime type.
   */
  private String mimeType;

  /**
   * Getter for {@link #nodeId}.
   *
   * @return value of nodeId.
   */
  public final Integer getNodeId() {
    return nodeId;
  }

  /**
   * Setter for {@link #nodeId}.
   *
   * @param id value to be set.
   */
  public final void setNodeId(final Integer id) {
    this.nodeId = id;
  }

  /**
   * Getter for {@link #parentId}.
   *
   * @return value of parentId.
   */
  public final Integer getParentId() {
    return parentId;
  }

  /**
   * Setter for {@link #parentId}.
   *
   * @param id value to be set.
   */
  public final void setParentId(final Integer id) {
    this.parentId = id;
  }

  /**
   * Getter for {@link #versionNumber}.
   *
   * @return value of versionNumber.
   */
  public final Integer getVersionNumber() {
    return versionNumber;
  }

  /**
   * Setter for {@link #versionNumber}.
   *
   * @param number value to be set.
   */
  public final void setVersionNumber(final Integer number) {
    this.versionNumber = number;
  }

  /**
   * Getter for {@link #lastModified}.
   *
   * @return value of lastModified.
   */
  public final LocalDateTime getLastModified() {
    return lastModified;
  }

  /**
   * Setter for {@link #lastModified}.
   *
   * @param modified value to be set.
   */
  public final void setLastModified(final LocalDateTime modified) {
    this.lastModified = modified;
  }

  /**
   * Getter for {@link #filename}.
   *
   * @return value of filename.
   */
  public final String getFilename() {
    return filename;
  }

  /**
   * Setter for {@link #filename}.
   *
   * @param name value to be set.
   */
  public final void setFilename(final String name) {
    this.filename = name;
  }

  /**
   * Getter for {@link #fileSize}.
   *
   * @return value of fileSize.
   */
  public final Integer getFileSize() {
    return fileSize;
  }

  /**
   * Setter for {@link #fileSize}.
   *
   * @param size value to be set.
   */
  public final void setFileSize(final Integer size) {
    this.fileSize = size;
  }

  /**
   * Getter for {@link #contents}.
   *
   * @return value of contents.
   */
  public final byte[] getContents() {
    return contents;
  }

  /**
   * Setter for {@link #contents}.
   *
   * @param file value to be set.
   */
  public final void setContents(final byte[] file) {
    this.contents = file;
  }

  /**
   * Getter for {@link #mimeType}.
   *
   * @return value of mimeType.
   */
  public final String getMimeType() {
    return mimeType;
  }

  /**
   * Setter for {@link #mimeType}.
   *
   * @param type value to be set.
   */
  public final void setMimeType(final String type) {
    this.mimeType = type;
  }
}
