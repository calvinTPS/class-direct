package com.baesystems.ai.lr.cd.be.cs10.exception;

/**
 * Timeout exception when downloading files.
 *
 * @author Faizal Sidek
 */
public class DownloadTimeoutException extends CS10Exception {
}
