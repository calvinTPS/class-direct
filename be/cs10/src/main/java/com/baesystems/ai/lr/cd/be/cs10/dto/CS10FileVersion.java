package com.baesystems.ai.lr.cd.be.cs10.dto;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * CS10 file version object.
 *
 * @author Faizal Sidek
 */
public class CS10FileVersion implements Serializable {

  /**
   * Serial id.
   */
  private static final long serialVersionUID = -6789737620290628004L;

  /**
   * Created date.
   */
  private LocalDate createdDate;

  /**
   * Modified date.
   */
  private LocalDate modifiedDate;

  /**
   * File create date.
   */
  private LocalDate fileCreateDate;

  /**
   * File data size.
   */
  private Integer fileDataSize;

  /**
   * File name.
   */
  private String filename;

  /**
   * File type.
   */
  private String fileType;

  /**
   * Node id.
   */
  private Integer nodeId;

  /**
   * Version name.
   */
  private String name;

  /**
   * Major version.
   */
  private Integer majorVersion;

  /**
   * Minor version.
   */
  private Integer minorVersion;

  /**
   * Getter for {@link #modifiedDate}.
   *
   * @return value of modifiedDate.
   */
  public final LocalDate getModifiedDate() {
    return modifiedDate;
  }

  /**
   * Setter for {@link #modifiedDate}.
   *
   * @param date value to be set.
   */
  public final void setModifiedDate(final LocalDate date) {
    this.modifiedDate = date;
  }

  /**
   * Getter for {@link #createdDate}.
   *
   * @return value of createdDate.
   */
  public final LocalDate getCreatedDate() {
    return createdDate;
  }

  /**
   * Setter for {@link #createdDate}.
   *
   * @param date value to be set.
   */
  public final void setCreatedDate(final LocalDate date) {
    this.createdDate = date;
  }

  /**
   * Getter for {@link #fileCreateDate}.
   *
   * @return value of fileCreateDate.
   */
  public final LocalDate getFileCreateDate() {
    return fileCreateDate;
  }

  /**
   * Setter for {@link #fileCreateDate}.
   *
   * @param date value to be set.
   */
  public final void setFileCreateDate(final LocalDate date) {
    this.fileCreateDate = date;
  }

  /**
   * Getter for {@link #fileDataSize}.
   *
   * @return value of fileDataSize.
   */
  public final Integer getFileDataSize() {
    return fileDataSize;
  }

  /**
   * Setter for {@link #fileDataSize}.
   *
   * @param size value to be set.
   */
  public final void setFileDataSize(final Integer size) {
    this.fileDataSize = size;
  }

  /**
   * Getter for {@link #filename}.
   *
   * @return value of filename.
   */
  public final String getFilename() {
    return filename;
  }

  /**
   * Setter for {@link #filename}.
   *
   * @param thisFileName value to be set.
   */
  public final void setFilename(final String thisFileName) {
    this.filename = thisFileName;
  }

  /**
   * Getter for {@link #fileType}.
   *
   * @return value of fileType.
   */
  public final String getFileType() {
    return fileType;
  }

  /**
   * Setter for {@link #fileType}.
   *
   * @param type value to be set.
   */
  public final void setFileType(final String type) {
    this.fileType = type;
  }

  /**
   * Getter for {@link #nodeId}.
   *
   * @return value of nodeId.
   */
  public final Integer getNodeId() {
    return nodeId;
  }

  /**
   * Setter for {@link #nodeId}.
   *
   * @param id value to be set.
   */
  public final void setNodeId(final Integer id) {
    this.nodeId = id;
  }

  /**
   * Getter for {@link #name}.
   *
   * @return value of name.
   */
  public final String getName() {
    return name;
  }

  /**
   * Setter for {@link #name}.
   *
   * @param versionName value to be set.
   */
  public final void setName(final String versionName) {
    this.name = versionName;
  }

  /**
   * Getter for {@link #majorVersion}.
   *
   * @return value of majorVersion.
   */
  public final Integer getMajorVersion() {
    return majorVersion;
  }

  /**
   * Setter for {@link #majorVersion}.
   *
   * @param version value to be set.
   */
  public final void setMajorVersion(final Integer version) {
    this.majorVersion = version;
  }

  /**
   * Getter for {@link #minorVersion}.
   *
   * @return value of minorVersion.
   */
  public final Integer getMinorVersion() {
    return minorVersion;
  }

  /**
   * Setter for {@link #minorVersion}.
   *
   * @param version value to be set.
   */
  public final void setMinorVersion(final Integer version) {
    this.minorVersion = version;
  }
}
