package com.baesystems.ai.lr.cd.be.cs10.impl;

import com.baesystems.ai.lr.cd.be.cs10.CS10ClientFactory;
import com.baesystems.ai.lr.cd.be.cs10.CS10Service;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10File;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10FileVersion;
import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import com.baesystems.ai.lr.cd.be.cs10.exception.DownloadTimeoutException;
import com.opentext.ecm.api.OTAuthentication;
import com.opentext.livelink.service.core.Attachment;
import com.opentext.livelink.service.core.Authentication;
import com.opentext.livelink.service.docman.DocumentManagement;
import com.opentext.livelink.service.docman.Node;
import com.opentext.livelink.service.docman.Version;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceException;
import org.apache.commons.io.IOUtils;
import org.lr.internal.ShipDMServiceSoap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Implementation of {@link CS10Service}.
 *
 * @author Faizal Sidek
 */
public class CS10ServiceImpl implements CS10Service {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CS10Service.class);

  /**
   * Zip file mime type.
   */
  private static final String ZIP_MIME_TYPE = "application/octet-stream";

  /**
   * Default timeout for download operation.
   */
  private static final Integer MAX_TIME_OUT = 120;

  /**
   * Extension for zip file.
   */
  private static final String ZIP_EXTENSION = ".zip";

  /**
   * Date time format.
   */
  private static final String DATE_TIME_FORMAT = "YYYYMMdd_HH_mm_ss";

  /**
   * CS10 username.
   */
  @Value("${cs10.service.auth.username}")
  private String username;

  /**
   * CS10 password.
   */
  @Value("${cs10.service.auth.password}")
  private String password;

  /**
   * Mutable token.
   */
  private String token;

  /**
   * Client factory.
   */
  @Autowired
  private CS10ClientFactory clientFactory;

  @Override
  public final String getAuthenticationToken(final String user, final String pass) throws CS10Exception {
    try {
      final Authentication authenticationService = clientFactory.createAuthenticationService();
      return authenticationService.authenticateUser(user, pass);
    } catch (WebServiceException exception) {
      LOGGER.error("Exception executing web service.", exception);
      throw new CS10Exception();
    }
  }

  @Override
  public final CS10File getFileContents(final Integer nodeId, final Integer versionNumber, final Boolean asZipPdf)
    throws CS10Exception {
    try {
      LOGGER.trace("Downloading file with node id {} and version number {} ", nodeId, versionNumber);
      final DocumentManagement documentManagement = clientFactory.createDocumentManagementService();
      final ShipDMServiceSoap dmService = clientFactory.createShipDMService();
      final Instant startDocVersion = Instant.now();
      final Version versionInfo = documentManagement.getVersion(nodeId, versionNumber, getAuthenticationHolder());
      LOGGER.debug("CS10 - fetched single file version within {} ms.",
          Duration.between(startDocVersion, Instant.now()).toMillis());

      final ExecutorService executorService = Executors.newSingleThreadExecutor();
      final Future<CS10File> future;
      if (asZipPdf) {
        future = executorService.submit(() -> {
          final String fileName;
          if (versionInfo.getFilename() != null && !versionInfo.getFilename().isEmpty()) {
            fileName =
              versionInfo.getFilename().substring(0, versionInfo.getFilename().lastIndexOf("."));
          } else {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
            fileName = dateFormat.format(new Date());
          }
          final String formNo = generateFormNo(nodeId, fileName, versionNumber);
          LOGGER.trace("Downloading zipped file with form no {} ", formNo);
          final CS10File cs10File = new CS10File();
          final Instant startDmService = Instant.now();
          final byte[] contents = dmService.downloadClientDocs(formNo, false);
          LOGGER.debug("DmService - download document within {} ms.",
              Duration.between(startDmService, Instant.now()).toMillis());
          cs10File.setContents(contents);
          cs10File.setFileSize(contents.length);
          cs10File.setFilename(fileName + ZIP_EXTENSION);
          cs10File.setMimeType(ZIP_MIME_TYPE);

          return cs10File;
        });
      } else {
        LOGGER.trace("Downloading file as original copy.");
        future = executorService.submit(() -> {
          final CS10File cs10File = new CS10File();
          final Instant startCS10Download = Instant.now();
          final Attachment attachment =
            documentManagement.getVersionContents(nodeId, versionNumber, getAuthenticationHolder());
          LOGGER.debug("CS10 - download document within {} ms.",
              Duration.between(startCS10Download, Instant.now()).toMillis());
          cs10File.setContents(attachment.getContents());
          cs10File.setFileSize(attachment.getContents().length);
          cs10File.setMimeType(versionInfo.getMimeType());
          cs10File.setFilename(versionInfo.getFilename());
          return cs10File;
        });
      }

      final CS10File file = future.get(MAX_TIME_OUT, TimeUnit.SECONDS);
      file.setNodeId(nodeId);
      file.setVersionNumber(versionNumber);
      file.setParentId(versionInfo.getID());
      final XMLGregorianCalendar modifiedDate = versionInfo.getModifyDate();
      file.setLastModified(LocalDateTime
        .of(modifiedDate.getYear(), modifiedDate.getMonth(), modifiedDate.getDay(), modifiedDate.getHour(),
          modifiedDate.getMinute(), modifiedDate.getSecond()));

      LOGGER.trace("Returning file with name: {}, size: {}, type: {}, last modified: {}", file.getFilename(),
        file.getFileSize(), file.getMimeType(), file.getLastModified().toString());
      return file;
    } catch (WebServiceException | InterruptedException | ExecutionException exception) {
      LOGGER.error("Exception executing CS10 service.", exception);
      throw new CS10Exception();
    } catch (TimeoutException timeoutException) {
      LOGGER.error("File download operation timed out after " + MAX_TIME_OUT + " seconds.");
      throw new DownloadTimeoutException();
    }
  }

  @Override
  public final CS10File getLatestFile(final Integer nodeId, final Boolean asZipPdf) throws CS10Exception {
    LOGGER.trace("Downloading latest file with node id {} ", nodeId);
    final List<CS10FileVersion> versions = getFileVersions(nodeId);
    final CS10FileVersion latest = versions.get(versions.size() - 1);

    Integer version = latest.getMajorVersion();
    if (version == null) {
      version = latest.getMinorVersion();
    }

    return getFileContents(nodeId, version, asZipPdf);
  }

  @Override
  public final String getFileDownloadLink(final Integer nodeId, final Integer versionNumber) throws CS10Exception {
    throw new CS10Exception(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Features not available.");
  }

  @Override
  public final List<CS10FileVersion> getFileVersions(final Integer nodeId) throws CS10Exception {
    final Instant start = Instant.now();
    try {
      LOGGER.trace("Getting latest version of node id {} ", nodeId);
      final DocumentManagement documentManagement = clientFactory.createDocumentManagementService();
      final Node node = documentManagement.getNode(nodeId, getAuthenticationHolder());

      if (node != null && node.getVersionInfo() != null && node.getVersionInfo().getVersions() != null && !node
        .getVersionInfo().getVersions().isEmpty()) {
        final List<CS10FileVersion> versions = new ArrayList<>();
        node.getVersionInfo().getVersions().forEach(version -> {
          final CS10FileVersion fileVersion = new CS10FileVersion();

          final XMLGregorianCalendar createdDate = version.getCreateDate();
          fileVersion.setCreatedDate(LocalDate.of(createdDate.getYear(), createdDate.getMonth(), createdDate.getDay()));

          final XMLGregorianCalendar modifiedDate = version.getModifyDate();
          fileVersion
            .setModifiedDate(LocalDate.of(modifiedDate.getYear(), modifiedDate.getMonth(), modifiedDate.getDay()));

          final XMLGregorianCalendar fileCreatedDate = version.getFileCreateDate();
          fileVersion.setFileCreateDate(
            LocalDate.of(fileCreatedDate.getYear(), fileCreatedDate.getMonth(), fileCreatedDate.getDay()));

          fileVersion.setFileDataSize((int) version.getFileDataSize());
          fileVersion.setFilename(version.getFilename());
          fileVersion.setFileType(version.getFileType());
          fileVersion.setMajorVersion(version.getVerMajor());
          fileVersion.setMinorVersion(version.getVerMinor());
          fileVersion.setName(version.getName());
          fileVersion.setNodeId(version.getID());
          versions.add(fileVersion);
        });
        LOGGER.trace("Found {} versions for node {}.", versions.size(), nodeId);
        return versions;
      } else {
        throw new CS10Exception(HttpStatus.NOT_FOUND.value(), "Node with id " + nodeId + " not found.");
      }
    } catch (WebServiceException exception) {
      throw new CS10Exception(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
    } finally {
      LOGGER.debug("CS10 - fetched all file versions within {} ms.",
          Duration.between(start, Instant.now()).toMillis());
    }
  }

  @Override
  public final CS10FileVersion getFileVersion(Integer nodeId, Integer versionNumber)
      throws CS10Exception {
    final Instant start = Instant.now();
    final CS10FileVersion fileVersion = new CS10FileVersion();
    try {
      final DocumentManagement documentManagement = clientFactory.createDocumentManagementService();

      final Version cs10Version =
          documentManagement.getVersion(nodeId, versionNumber, getAuthenticationHolder());
      if (cs10Version != null) {
        fileVersion.setFilename(cs10Version.getFilename());
        fileVersion.setFileType(cs10Version.getFileType());
      } else {
        fileVersion.setFilename("");
        fileVersion.setFileType("");
      }
    } finally {
      LOGGER.debug("CS10 - fetched single file version within {} ms.",
          Duration.between(start, Instant.now()).toMillis());
    }
    return fileVersion;
  }

  @Override
  public final Integer createDocuments(final Integer parentNodeId, final File fileContents) throws CS10Exception {
    try {
      final DocumentManagement documentManagement = clientFactory.createDocumentManagementService();

      final GregorianCalendar now = new GregorianCalendar();
      now.setTime(new Date());

      final byte[] contents = IOUtils.toByteArray(new FileInputStream(fileContents));
      final Attachment attachment = new Attachment();
      attachment.setContents(contents);
      attachment.setFileName(fileContents.getName());
      attachment.setFileSize(contents.length);
      attachment.setCreatedDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(now));

      return documentManagement
        .createSimpleDocument(parentNodeId, fileContents.getName(), attachment, getAuthenticationHolder());
    } catch (IOException | DatatypeConfigurationException | WebServiceException exception) {
      throw new CS10Exception(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
    }
  }

  @Override
  public final String refreshAuthToken(final String oldToken) throws CS10Exception {
    final Authentication authentication = clientFactory.createAuthenticationService();
    return authentication.refreshToken(getAuthenticationHolder());
  }

  /**
   * Get and cache authentication holder.
   *
   * @return authentication holder.
   * @throws CS10Exception when error.
   */
  private Holder<OTAuthentication> getAuthenticationHolder() throws CS10Exception {
    if (token == null) {
      token = getAuthenticationToken(username, password);
    }
    final OTAuthentication authentication = new OTAuthentication();
    authentication.setAuthenticationToken(token);
    return new Holder<>(authentication);
  }

  /**
   * Periodically refresh token.
   *
   * @throws CS10Exception when error.
   */
  @Scheduled(fixedDelayString = "${cs10.service.auth.refresh.delay}")
  public final void refreshToken() throws CS10Exception {
    if (token != null && !token.isEmpty()) {
      token = refreshAuthToken(token);
    }
  }

  /**
   * Generate form no for dm service.
   *
   * @param nodeId      file node id.
   * @param fileName    file name.
   * @param fileVersion file version number.
   * @return formatted form no.
   */
  private String generateFormNo(final Integer nodeId, final String fileName, final Integer fileVersion) {
    return nodeId + "|" + fileName + "|" + fileVersion;
  }
}
