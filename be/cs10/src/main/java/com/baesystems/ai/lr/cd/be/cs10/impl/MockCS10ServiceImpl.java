package com.baesystems.ai.lr.cd.be.cs10.impl;

import com.baesystems.ai.lr.cd.be.cs10.CS10Service;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10File;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10FileVersion;
import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mock CS10 service.
 *
 * @author Faizal Sidek
 */
public class MockCS10ServiceImpl implements CS10Service {

  /**
   * Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MockCS10ServiceImpl.class);

  @Override
  public final String getAuthenticationToken(final String username, final String password) throws CS10Exception {
    return null;
  }

  @Override
  public final String refreshAuthToken(final String oldToken) throws CS10Exception {
    return null;
  }

  @Override
  public final CS10File getFileContents(final Integer nodeId, final Integer versionNumber, final Boolean asZipPdf)
    throws CS10Exception {
    try {
      final CS10File mockCS10File = new CS10File();
      mockCS10File.setFilename("sample_file.pdf");
      mockCS10File.setMimeType("application/pdf");

      final InputStream fileContent = getClass().getClassLoader().getResourceAsStream("sample_file.pdf");
      final byte[] contents = IOUtils.toByteArray(fileContent);
      mockCS10File.setContents(contents);

      mockCS10File.setFileSize(contents.length);
      mockCS10File.setLastModified(LocalDateTime.now());
      mockCS10File.setNodeId(nodeId);
      mockCS10File.setVersionNumber(versionNumber);

      return mockCS10File;
    } catch (IOException exception) {
      LOGGER.error("Failed to get file contents.", exception);
      throw new CS10Exception();
    }
  }

  @Override
  public final CS10File getLatestFile(final Integer nodeId, final Boolean asZipPdf) throws CS10Exception {
    return getFileContents(nodeId, nodeId, asZipPdf);
  }

  @Override
  public final String getFileDownloadLink(final Integer nodeId, final Integer versionNumber) throws CS10Exception {
    return null;
  }

  @Override
  public final List<CS10FileVersion> getFileVersions(final Integer nodeId) throws CS10Exception {
    return null;
  }

  @Override
  public final CS10FileVersion getFileVersion(Integer nodeId, Integer versionNumber)
      throws CS10Exception {
    return null;
  }

  @Override
  public final Integer createDocuments(final Integer parentNodeId, final File fileContents) throws CS10Exception {
    return null;
  }
}
