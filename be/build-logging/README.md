# build-logging

This module keeps the dependencies related to logging.
Import wherever it's needed using type *pom*

```xml
<dependency>
    <groupId>com.baesystemsai.lr.cd.be</groupId>
    <artifactId>build-logging</artifactId>
    <version>${project.version}</version>
    <type>pom</type>
</dependency>
```