package com.baesystems.ai.lr.cd.mail.test;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.mail.impl.MailServiceImpl;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Test class for Mail Service.
 *
 * @author yng
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/class-direct-mail-test-context.xml"})
public class MailServiceTest {

  /**
   * Injected sender.
   */
  @Autowired
  private JavaMailSender mailSender;

  /**
   * @throws JadeException jadeException.
   * @throws IOException ioException.
   */
  @Test
  public void mailTest() throws JadeException, IOException {
    Mockito.doReturn(Mockito.mock(MimeMessage.class)).when(mailSender).createMimeMessage();
    Mockito.doNothing().when(mailSender).send(Mockito.any(MimeMessage.class));

    SimpleMailMessage templateMessage = Mockito.mock(SimpleMailMessage.class);
    final MailServiceImpl mailService = new MailServiceImpl();
    mailService.setMailSender(mailSender);
    mailService.setTemplateMessage(templateMessage);
    mailService.setSenderAddress("test@test.com");

    // generate test data
    Map<String, Object> model = new TreeMap<String, Object>();
    model.put("requester", "abc1");
    model.put("associatedClientName", "abc2");
    model.put("IMONumber", "abc3");
    model.put("assetName", "abc4");
    model.put("assetType", "abc5");
    model.put("flag", "abc6");
    model.put("assetClassStatus", "abc7");
    model.put("assetLifecycleStatus", "abc8");
    model.put("portOfSurvey", "abc9");
    model.put("SDOOffice", "abc10");
    model.put("SDOPhone", "abc11");
    model.put("SDOAddress", "abc12");
    model.put("SDOEmailAddress", "abc13");
    model.put("estimatedDateOfArrival", "abc14");
    model.put("estimatedTimeOfArrival", "abc15");
    model.put("estimatedDateOfDeparture", "abc16");
    model.put("estimatedTimeOfDeparture", "abc17");
    model.put("surveyStartDate", "abc18");
    model.put("portAgentName", "abc19");
    model.put("portAgentPhoneNumber", "abc20");
    model.put("portAgentEmailAddress", "abc21");
    model.put("berthOrAnchorage", "abc22");
    model.put("shipContactDetails", "abc23");
    model.put("additionalComments", "abc24");
    model.put("products", "");

    mailService.sendMailWithTemplate("abc@ourcompany.com", "mail", "template", "mail_template.pug", model);

  }

  /**
   * test SendMail() method.
   */
  @Test
  public void testSendMail() {
    MailSender mailSender = Mockito.mock(MailSender.class);
    SimpleMailMessage templateMessage = Mockito.mock(SimpleMailMessage.class);
    final MailServiceImpl mailService = new MailServiceImpl();
    mailService.setMailSender(mailSender);
    mailService.setTemplateMessage(templateMessage);

    mailService.sendMail("abc@ourcompany.com", "mail", "template", "body");
  }

}
