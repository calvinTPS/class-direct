package com.baesystems.ai.lr.cd.mail.impl;

import com.baesystems.ai.lr.cd.mail.MailService;

import de.neuland.jade4j.JadeConfiguration;
import de.neuland.jade4j.exceptions.JadeException;
import de.neuland.jade4j.template.ClasspathTemplateLoader;
import de.neuland.jade4j.template.JadeTemplate;
import de.neuland.jade4j.template.TemplateLoader;

import java.io.IOException;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * Implementation for mail service.
 *
 * @author Faizal Sidek
 */
public class MailServiceImpl implements MailService {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

  /**
   * The maximum attempt when email fail.
   */
  private static final int MAX_ATTEMPT = 3;

  /**
   * The interval to wait for next email retry.
   */
  private static final long RETRY_INTERVAL_MILISECONDS = 60000L;

  /**
   * Mail sender.
   */
  private MailSender mailSender = null;

  /**
   * Default mail message.
   */
  private SimpleMailMessage templateMessage;

  /**
   * Address of sender.
   */
  private String senderAddress;

  /**
   * Sender name.
   */
  private String senderName;

  /**
   * Setter for {@link #mailSender}.
   *
   * @param sender value to be set.
   */
  public final void setMailSender(final MailSender sender) {
    this.mailSender = sender;
  }

  /**
   * Setter for {@link #templateMessage}.
   *
   * @param message value to be set.
   */
  public final void setTemplateMessage(final SimpleMailMessage message) {
    this.templateMessage = message;
  }

  /**
   * Setter for {@link #senderAddress}.
   *
   * @param address value to be set.
   */
  public final void setSenderAddress(final String address) {
    this.senderAddress = address;
  }

  /**
   * Setter for {@link #senderName}.
   *
   * @param name value to be set.
   */
  public final void setSenderName(final String name) {
    this.senderName = name;
  }

  @Override
  public final boolean sendMail(final String to, final String cc, final String subject, final String body) {
    SimpleMailMessage msg = new SimpleMailMessage(this.templateMessage);
    msg = setBasicInput(msg, to, cc, subject);

    msg.setText(body);
    mailSender.send(msg);

    LOGGER.debug("[sendMail] To: " + to + ", cc: " + cc + ", subject: " + subject);
    return true;
  }

  @Override
  public final boolean sendMailWithTemplate(final String to, final String cc, final String subject,
    final String template, final Map<String, Object> model) throws JadeException, IOException {
    boolean emailFlag = false;
    final TemplateLoader loader = new ClasspathTemplateLoader();
    final JadeConfiguration configuration = new JadeConfiguration();
    configuration.setTemplateLoader(loader);
    String body = " ";
    // test
    LOGGER.debug("Checking template for mail send" + subject);
    try {
      final JadeTemplate jadeTemplate = configuration.getTemplate(template);
      body = configuration.renderTemplate(jadeTemplate, model);
    } catch (final JadeException jadeException) {
      LOGGER.error("Error in rednrering template" + jadeException);
    }

    // test
    LOGGER.debug("Template is ready for sending mail" + subject);
    for (int i=1; i<=MAX_ATTEMPT; i++) {
      try {
        LOGGER.debug("inside mail block" + subject);
        final MimeMessage message = ((JavaMailSender) mailSender).createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(to);
        helper.setText(body, true);
        helper.setFrom(new InternetAddress(senderAddress, senderName));
        if (null != cc && !cc.isEmpty()) {
          helper.setCc(cc.split(";"));
        }
        helper.setSubject(subject);

        ((JavaMailSender) mailSender).send(message);
        LOGGER.debug("[sendMailWithTemplate] Attempt: {} of {}, To: {}, cc: {}, subject: {}, model: {}", i
            , MAX_ATTEMPT, to, cc, subject, model);
        emailFlag = true;
        break;
      } catch (final MailException | MessagingException exception) {
        LOGGER.error("[sendMailWithTemplate] Unable to send email. | Attempt: " + i + " of " + MAX_ATTEMPT + ", To: "
            + to + ", cc: " + cc + ", subject: " + subject + ", model: " + model, exception);
        //Skip the wait for last attempt
        if (i != MAX_ATTEMPT) {
          try {
            Thread.sleep( i * RETRY_INTERVAL_MILISECONDS );
          } catch (final InterruptedException e) {
            LOGGER.error("[sendMailWithTemplate] Error on waiting for retry.", e);
          }
        }
      }
    }

    return emailFlag;
  }

  /**
   * Create basic message.
   *
   * @param msg message.
   * @param to target.
   * @param cc cc target.
   * @param subject subject.
   * @return message.
   */
  private SimpleMailMessage setBasicInput(final SimpleMailMessage msg, final String to, final String cc,
    final String subject) {
    if (null != cc && !cc.isEmpty()) {
      msg.setCc(cc.split("\\s*;\\s*"));
    }
    msg.setTo(to.split("\\s*;\\s*"));
    msg.setSubject(subject);
    return msg;
  }

}
