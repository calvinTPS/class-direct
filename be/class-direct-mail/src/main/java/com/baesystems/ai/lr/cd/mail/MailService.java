package com.baesystems.ai.lr.cd.mail;

import java.io.IOException;
import java.util.Map;

import de.neuland.jade4j.exceptions.JadeException;

public interface MailService {
  boolean sendMail(String to, String cc, String subject, String body);

  boolean sendMailWithTemplate(String to, String cc, String subject,
      String template, Map<String, Object> model)
          throws JadeException, IOException;
}
