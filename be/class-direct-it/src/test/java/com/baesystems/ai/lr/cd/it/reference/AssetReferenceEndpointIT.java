package com.baesystems.ai.lr.cd.it.reference;

import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.it.BaseIntegrationTests;
import com.baesystems.ai.lr.cd.it.asset.AssetQueryIntegrationIT;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test for endpoint <code>/reference-data/asset/*</code> endpoints.
 *
 * @author msidek
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class AssetReferenceEndpointIT extends BaseIntegrationTests {
  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetQueryIntegrationIT.class);

  /**
   * Unit test for /reference-data/asset/asset-types.
   *
   * @throws Exception when error.
   */
  @Test
  public final void testForSearchAllAssetTypes() throws Exception {
    LOGGER.debug("Search all asset types test.");

    List<AssetTypeHDto> assetTypes = doGetList("reference-data/asset/asset-types", AssetTypeHDto.class);
    assertNotNull(assetTypes);
    assertNotEquals(0, assetTypes.size());

    List<AssetTypeHDto> hydratedAssetTypes =
      assetTypes.stream().filter(assetType -> assetType.getCategoryDto() != null).collect(Collectors.toList());
    assertNotNull(hydratedAssetTypes);
    assertFalse(hydratedAssetTypes.isEmpty());
  }

  /**
   * Unit test for /reference-data/asset/class-status.
   *
   * @throws Exception when error.
   */
  @Test
  public final void testForSearchAllClassStatuses() throws Exception {
    LOGGER.debug("Search all class statuses test.");

    List<ClassStatusHDto> classStatuses = doGetList("reference-data/asset/class-statuses", ClassStatusHDto.class);
    assertNotNull(classStatuses);
    assertNotEquals(0, classStatuses.size());
  }
}
