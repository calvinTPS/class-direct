package com.baesystems.ai.lr.cd.it.asset;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.it.BaseIntegrationTests;

/**
 * Integration test for /asset/query.
 *
 * @author msidek
 */
public class AssetQueryIntegrationIT extends BaseIntegrationTests {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetQueryIntegrationIT.class);

  /**
   * Test for search all assets.
   *
   * @throws Exception when error.
   */
  @Test
  public final void searchAllAssets() throws Exception {
    LOGGER.debug("Testing query /asset/query");

    AssetQueryHDto assetQuery = new AssetQueryBuilder().search("*").build();
    AssetPageResource pageResource = doPost("assets/query", assetQuery, AssetPageResource.class);
    Assert.assertNotNull(pageResource);
    Assert.assertNotEquals(0, pageResource.getContent().size());
  }

  /**
   * Search pagination test.
   *
   * @throws Exception when error.
   */
  @Test
  public final void searchPagination() throws Exception {
    LOGGER.debug("Search pagination test.");

    AssetQueryHDto assetQuery = new AssetQueryBuilder().search("*").build();

    AssetPageResource pageResource = doPost("assets/query", assetQuery, AssetPageResource.class);
    List<AssetHDto> allAssets = pageResource.getContent();
    Assert.assertNotEquals(0, allAssets.size());
    pageResource.getContent().forEach(asset -> LOGGER.debug(asset.toString()));
  }
}
