package com.baesystems.ai.lr.cd.it.asset;

import java.util.ArrayList;
import java.util.Date;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;

/**
 * {@link AssetQueryHDto} builder.
 *
 * @author msidek
 *
 */
public class AssetQueryBuilder {
  /**
   * asset query.
   *
   */
  private AssetQueryHDto assetQuery;

  /**
   * Constructor.
   *
   */
  public AssetQueryBuilder() {
    assetQuery = new AssetQueryHDto();
  }

  /**
   * Add search term.
   *
   * @param term of search.
   * @return builder.
   *
   */
  public final AssetQueryBuilder search(final String term) {
    assetQuery.setSearch(term);
    return this;
  }

  /**
   * Add lifecycle status id.
   *
   * @param lifecycleStatusId id.
   * @return builder.
   *
   */
  public final AssetQueryBuilder addLifecycleStatusId(final Long lifecycleStatusId) {
    if (assetQuery.getLifecycleStatusId() == null) {
      assetQuery.setLifecycleStatusId(new ArrayList<>());
    }
    assetQuery.getLifecycleStatusId().add(lifecycleStatusId);

    return this;
  }

  /**
   * Add IMO number.
   *
   * @param imoNumber imo number.
   * @return builder.
   *
   */
  public final AssetQueryBuilder addImoNumber(final String imoNumber) {
    if (null == assetQuery.getImoNumber()) {
      assetQuery.setImoNumber(new ArrayList<>());
    }
    assetQuery.getImoNumber().add(imoNumber);

    return this;
  }

  /**
   * Add asset type id.
   *
   * @param assetTypeId id of asset type.
   * @return builder.
   *
   */
  public final AssetQueryBuilder addAssetTypeId(final Long assetTypeId) {
    if (null == assetQuery.getAssetTypeId()) {
      assetQuery.setAssetTypeId(new ArrayList<>());
    }
    assetQuery.getAssetTypeId().add(assetTypeId);

    return this;
  }

  /**
   * Set yard number.
   *
   * @param yardNumber number of yard.
   * @return builder.
   */
  public final AssetQueryBuilder yardNumber(final String yardNumber) {
    assetQuery.setYardNumber(yardNumber);

    return this;
  }

  /**
   * Set ship build date.
   *
   * @param startDate range start.
   * @return builder
   *
   */
  public final AssetQueryBuilder buildDateStart(final Date startDate) {
    assetQuery.setBuildDateMin(startDate);

    return this;
  }

  /**
   * Set ship build date.
   *
   * @param endDate range end.
   * @return builder
   *
   */
  public final AssetQueryBuilder buildDateEnd(final Date endDate) {
    assetQuery.setBuildDateMax(endDate);

    return this;
  }

  /**
   * Add class status id.
   *
   * @param classStatusId id of class status.
   * @return builder.
   *
   */
  public final AssetQueryBuilder addClassStatusId(final Long classStatusId) {
    if (null == assetQuery.getClassStatusId()) {
      assetQuery.setClassStatusId(new ArrayList<>());
    }
    assetQuery.getClassStatusId().add(classStatusId);

    return this;
  }

  /**
   * Add flag state id.
   *
   * @param flagStateId id of flag state.
   * @return builder.
   *
   */
  public final AssetQueryBuilder addFlagStateId(final Long flagStateId) {
    if (null == assetQuery.getFlagStateId()) {
      assetQuery.setFlagStateId(new ArrayList<>());
    }
    assetQuery.getFlagStateId().add(flagStateId);

    return this;
  }

  /**
   * Set favorite flag.
   *
   * @param favorite flag of isfavourite.
   * @return builder
   *
   */
  public final AssetQueryBuilder isFavorite(final Boolean favorite) {
    assetQuery.setIsFavourite(favorite);
    return this;
  }

  /**
   * Builder asset query object.
   *
   * @return asset query.
   *
   */
  public final AssetQueryHDto build() {
    return assetQuery;
  }
}
