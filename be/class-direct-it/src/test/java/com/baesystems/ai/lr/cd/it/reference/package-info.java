/**
 * Contains test cases for <code>/reference-data</code> endpoints.
 *
 */
package com.baesystems.ai.lr.cd.it.reference;
