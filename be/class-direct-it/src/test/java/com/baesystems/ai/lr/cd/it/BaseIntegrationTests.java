package com.baesystems.ai.lr.cd.it;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.junit.Assert;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base integration test suite.
 *
 * @author Faizal Sidek
 */
public abstract class BaseIntegrationTests {
  /**
   * Base logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(BaseIntegrationTests.class);

  /**
   * Setting default time out to 3 minutes.
   */
  private static final Long DEFAULT_TIMEOUT = Long.valueOf(60 * 3 * 1000);

  /**
   * Default url.
   */
  private String url;

  /**
   * Json mapper.
   */
  private ObjectMapper mapper;

  /**
   * Setting up base test.
   */
  @Before
  public final void setup() {
    final ResourceBundle rs = ResourceBundle.getBundle("class-direct-it");
    url = rs.getString("integration.test.url") + "/class-direct/api/v1/";

    Unirest.setTimeouts(DEFAULT_TIMEOUT, DEFAULT_TIMEOUT);
    mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
      @Override
      public String writeValue(final Object value) {
        try {
          return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
          throw new RuntimeException(e);
        }
      }

      @Override
      public <T> T readValue(final String value, final Class<T> valueType) {
        try {
          return mapper.readValue(value, valueType);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    });
  }

  /**
   * Perform POST on endpoint.
   *
   * @param endpoint url relative to base url.
   * @param body     parameter input.
   * @param clazz    expected return type.
   * @param <T>      generic type.
   * @return parsed object.
   * @throws UnirestException when error.
   * @throws IOException      when error.
   */
  protected final <T> T doPost(final String endpoint, final Object body, final Class<T> clazz)
    throws UnirestException, IOException {
    final HttpResponse<String> response = Unirest.post(url + endpoint).header("userId", "101").body(body).asString();
    final String output = response.getBody();

    LOGGER.debug("Output: {}", output);
    return mapper.readValue(output, clazz);
  }

  /**
   * Perform POST on endpoint.
   *
   * @param endpoint url relative to base url.
   * @param queries  query parameters.
   * @param body     parameter input.
   * @param clazz    expected return type.
   * @param <T>      generic type.
   * @return parsed object.
   * @throws UnirestException when error.
   * @throws IOException      when error.
   */
  protected final <T> T doPost(final String endpoint, final Map<String, Object> queries, final Object body,
    final Class<T> clazz) throws UnirestException, IOException {
    final String output =
      Unirest.post(url + endpoint).header("userId", "101").queryString(queries).body(body).asString().getBody();
    LOGGER.debug("Output: " + output);
    return mapper.readValue(output, clazz);
  }

  /**
   * Perform GET on endpoint.
   *
   * @param endpoint url.
   * @param clazz    expected return type
   * @param <T>      generic type.
   * @return list of objects
   * @throws UnirestException when error
   * @throws IOException      when error
   */
  protected final <T> List<T> doGetList(final String endpoint, final Class<T> clazz)
    throws UnirestException, IOException {
    final HttpResponse<String> response = Unirest.get(url + endpoint).header("userId", "101").asString();
    final String output = response.getBody();

    final JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
    return mapper.readValue(output, type);
  }

  /**
   * Perform GET on endpoint.
   *
   * @param endpoint url.
   * @param clazz    expected return type
   * @param <T>      generic type.
   * @return single object
   * @throws UnirestException when error
   * @throws IOException      when error
   */
  protected final <T> T doGet(final String endpoint, final Class<T> clazz) throws UnirestException, IOException {
    final HttpResponse<String> response = Unirest.get(url + endpoint).header("userId", "101").asString();
    final String output = response.getBody();

    return mapper.readValue(output, clazz);
  }

  /**
   * Perform PUT operation.
   * @param endpoint path
   * @param body param
   * @throws UnirestException when error.
   * @throws IOException when error
   */
  protected final void doPut(final String endpoint, final Object body) throws UnirestException, IOException {
    final HttpResponse<String> response = Unirest.put(url + endpoint).header("userId", "101").asString();

    final String responseBody = response.getBody();
    LOGGER.debug("Response body: {}", responseBody);
    Assert.assertNotNull(responseBody);
  }

  /**
   * Getter for {@link #url}.
   *
   * @return value of url.
   */
  public final String getUrl() {
    return url;
  }
}
