package com.baesystems.ai.lr.cd.be.utils;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;

/**
 * Filter class.
 *
 * @author VMandalapu
 */
public class CDRedirectFilter implements Filter {

  /**
   * Logger Implementation.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CDRedirectFilter.class);

  /**
   * Index file name.
   */
  private static final String INDEX_FILE = "/index.html";

  /**
   * Root path.
   */
  private static final String ROOT_PATH = "/";
  /**
   * FilterConfig declaration.
   */
  private FilterConfig filterConfig = null;

  @Override
  public final void destroy() {}

  @Override
  public final void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
      throws IOException, ServletException {

    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;
    String servletPath = httpRequest.getServletPath();

    final Matcher resourceMatcher =
        Pattern.compile(filterConfig.getInitParameter("resource.path")).matcher(servletPath);
    final Matcher exclusionPathMatcher =
        Pattern.compile(filterConfig.getInitParameter("exclusion.path")).matcher(servletPath);

    ServletContext context = request.getServletContext();
    String equasisThetisURL = (String) httpRequest.getAttribute(CDConstants.EQUASIS_THETIS_URL);
    if (equasisThetisURL != null) {
      LOGGER.debug("Equasis / Thetis URL : {}", equasisThetisURL);
      httpResponse.sendRedirect(equasisThetisURL);
    } else if (exclusionPathMatcher.find()) {
      httpResponse.addHeader(CDConstants.REQUEST_ID, MDC.get(CDConstants.REQUEST_ID));
      chain.doFilter(request, httpResponse);
    } else if (resourceMatcher.find()) {
      String resource = ROOT_PATH + resourceMatcher.group(1);
      LOGGER.debug("Forwarding to resource {} ", resource);
      context.getRequestDispatcher(resource).forward(request, httpResponse);
    } else {
      LOGGER.debug("Forwarding to index.");
      context.getRequestDispatcher(INDEX_FILE).forward(request, httpResponse);
    }

  }

  @Override
  public final void init(final FilterConfig config) throws ServletException {
    this.filterConfig = config;
  }
}
