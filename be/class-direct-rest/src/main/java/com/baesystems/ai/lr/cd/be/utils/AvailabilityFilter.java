package com.baesystems.ai.lr.cd.be.utils;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectExceptionDTO;
import com.baesystems.ai.lr.cd.core.SpringContextUtil;
import com.baesystems.ai.lr.cd.service.security.AvailabilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;

/**
 * Checks for availability before forwarding the requests and Provides unique identifier in logs
 * using MDC.
 *
 * @author Faizal Sidek
 * @author syalavarthi 19-05-2017.
 */
public class AvailabilityFilter implements Filter {

  @Override
  public final void init(final FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public final void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
      throws IOException, ServletException {
    final AvailabilityService service = SpringContextUtil.getBean("AvailabilityService", AvailabilityService.class);
    if (service.isServiceAvailable()) {
      chain.doFilter(request, response);
    } else {
      final ClassDirectExceptionDTO exceptionDTO =
          new ClassDirectExceptionDTO("System under maintenance", HttpStatus.GATEWAY_TIMEOUT.value());
      final ObjectMapper mapper = new ObjectMapper();

      response.getWriter().write(mapper.writeValueAsString(exceptionDTO));
      ((HttpServletResponse) response).setStatus(HttpStatus.GATEWAY_TIMEOUT.value());
    }
  }

  @Override
  public final void destroy() {
  }
}
