package com.baesystems.ai.lr.cd.be.utils;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.core.SpringContextUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;

/**
 * Filter class for Thetis user login.
 *
 * @author RKaneysan
 *
 */
public class ThetisSecurityFilter implements Filter {

  /**
   * Logger Implementation.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ThetisSecurityFilter.class);

  /**
   * Root path.
   */
  private static final String ROOT_PATH = "/";

  /**
   * Asset Hub path.
   */
  private static final String ASSET_HUB_PATH = "assets/";

  @Override
  public void init(FilterConfig filterCfg) throws ServletException {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    final String imoNumber = request.getParameter("lrno");
    final String user = THETIS.toString();

    AssetService assetService = SpringContextUtil.getBean("AssetService", AssetService.class);
    HttpServletResponse httpResponse = (HttpServletResponse) response;
    String errorMsg = null;

    // To check if IMO Number (International Maritime Organization Number) is blank or null.
    if (!StringUtils.isEmpty(imoNumber)) {
      try {
        HeaderMapRequestWrapper requestWrapper = new HeaderMapRequestWrapper((HttpServletRequest) request);
        StringBuilder redirectURL = new StringBuilder(ROOT_PATH);

        /*
         * If asset code is not empty, page is routed to "Asset Hub" page. Otherwise, route to
         * "root" path. An asset code can be empty if the asset is not registered under Lloyd's
         * Register.
         */
        final AssetHDto assetHdto = assetService.getAssetByImoNumber(imoNumber);
        Optional.ofNullable(assetHdto).ifPresent(asset -> {
          Optional.of(asset.getId()).ifPresent(assetId -> {
            LOGGER.debug("Asset Id : {}", assetId);
            requestWrapper.setAttribute(CDConstants.EQUASIS_THETIS_ASSET_ID, assetId);
          });
          Optional.of(asset.getCode()).ifPresent(assetCode -> {
            LOGGER.debug("Asset Code : {}", assetCode);
            redirectURL.append(ASSET_HUB_PATH).append(assetCode);
          });
        });

        requestWrapper.setAttribute(CDConstants.ROLE, user);
        LOGGER.debug("User: {}. Redirect to {}", user, redirectURL.toString());
        requestWrapper.setAttribute(CDConstants.EQUASIS_THETIS_URL, redirectURL.toString());
        chain.doFilter(requestWrapper, httpResponse);
      } catch (final ClassDirectException exception) {
        errorMsg = exception.getMessage();
        LOGGER.error("Error occured when redirect to asset hub page for user {}.", user, exception);
      }
    } else {
      errorMsg = "International Maritime Organization Number is empty for user " + user + ". ";
      LOGGER.error(errorMsg.toString());
    }

    if (!StringUtils.isEmpty(errorMsg)) {
      errorMsg = errorMsg + "Sorry, something's gone wrong with this link.\n"
          + "Please use your back button to return to the previous page and try again.\n"
          + "If you continue to have problems accessing this page, "
          + "please contact classdirect@lr.org for support.\n";
      LOGGER.error("Unable to Authenticate {} user. Error message {}", user, errorMsg.toString());
      final ServletOutputStream httpStream = httpResponse.getOutputStream();
      httpStream.println(errorMsg.toString());
      httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
      httpStream.close();
    }
  }

  @Override
  public void destroy() {}

}
