package com.baesystems.ai.lr.cd.be.utils;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.security.ClassDirectSecurityService;
import com.baesystems.ai.lr.cd.core.SpringContextUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;

/**
 * Filter class for Equasis user login.
 *
 * @author RKaneysan
 *
 */
public class EquasisSecurityFilter implements Filter {

  /**
   * Logger Implementation.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(EquasisSecurityFilter.class);

  /**
   * Root path.
   */
  private static final String ROOT_PATH = "/";

  /**
   * Asset Hub path.
   */
  private static final String ASSET_HUB_PATH = "assets/";

  @Override
  public void init(FilterConfig filterCfg) throws ServletException {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    String path = ((HttpServletRequest) request).getServletPath();
    String imoNumber = request.getParameter("IMO");
    String time = request.getParameter("d");
    String hash = request.getParameter("k");
    final String user = EQUASIS.toString();
    String errorMsg = "";
    Long assetId = null;

    ClassDirectSecurityService classDirectSecurity =
        SpringContextUtil.getBean("ClassDirectSecurityService", ClassDirectSecurityService.class);
    AssetService assetService = SpringContextUtil.getBean("AssetService", AssetService.class);
    HttpServletResponse httpResponse = (HttpServletResponse) response;

    path = path.replace(ROOT_PATH, "");

    LOGGER.debug("Path: {}", path);

    // To avoid improper translation of hash value (special character can be translated wrongly).
    if (!StringUtils.isEmpty(imoNumber) && !StringUtils.isEmpty(time) && !StringUtils.isEmpty(hash)) {
      try {
        classDirectSecurity.authenticateHash(imoNumber, time, hash);
        classDirectSecurity.validateTime(time);
        assetId = assetService.getAssetIdByImoNumber(imoNumber);
        HttpServletRequest req = (HttpServletRequest) request;
        HeaderMapRequestWrapper requestWrapper = new HeaderMapRequestWrapper(req);
        requestWrapper.setAttribute(CDConstants.ROLE, user);
        requestWrapper.setAttribute(CDConstants.EQUASIS_THETIS_ASSET_ID, assetId);
        StringBuilder redirectURL = new StringBuilder(ROOT_PATH);

        /*
         * Asset id can be empty if the asset is not registered in Lloyds Register. If asset id is
         * empty, then route to servletPath. Otherwise, route to "Asset Hub" page.
         */
        if (!StringUtils.isEmpty(assetId.toString())) {
          redirectURL.append(ASSET_HUB_PATH).append(AssetCodeUtil.getMastCode(assetId));
        }

        LOGGER.debug("Redirect to {}", redirectURL.toString());
        requestWrapper.setAttribute(CDConstants.EQUASIS_THETIS_URL, redirectURL.toString());
        chain.doFilter(requestWrapper, httpResponse);
      } catch (final ClassDirectException exception) {
        errorMsg = exception.getMessage();
      }

    } else {
      errorMsg = "Invalid input parameters.";
    }

    if (!StringUtils.isEmpty(errorMsg)) {
      LOGGER.info("Unable to Authenticate {} user. Error message {}", user, errorMsg);
      errorMsg = "Sorry, something's gone wrong with this link.\n"
          + "Please use your back button to return to the previous page and try again.\n"
          + "If you continue to have problems accessing this page, please contact classdirect@lr.org for support.\n";
      httpResponse.getOutputStream().println(errorMsg);
      httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
      httpResponse.getOutputStream().close();
    }

  }

  @Override
  public void destroy() {}

}
