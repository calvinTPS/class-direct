package com.baesystems.ai.lr.cd.be.utils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.baesystems.ai.lr.cd.service.aws.AWSHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Servlet listener to decrypt and parse properties file as system properties.
 *
 * @author Faizal Sidek
 */
public class EncryptedPropertiesListener implements ServletContextListener {
  /**
   * System properties name.
   */
  public static final String CONFIG_NAME = "kms.encrypted.config";

  /**
   * Kms region.
   */
  public static final String KMS_REGION = "kms.region";

  /**
   * KMS access key.
   */
  private static final String KMS_ACCESS_KEY = "kms.access.key";

  /**
   * KMS secret key.
   */
  private static final String KMS_SECRET_KEY = "kms.secret.key";

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedPropertiesListener.class);

  /**
   * Default constructor to initialize the helper.
   *
   */
  public EncryptedPropertiesListener() {
    awsHelper = new AWSHelper();
  }

  /**
   * Tiny wrapper to create client.
   */
  private AWSHelper awsHelper;

  /**
   * Setter for {@link #awsHelper}.
   *
   * @param helper value to be set.
   */
  public final void setAwsHelper(final AWSHelper helper) {
    this.awsHelper = helper;
  }

  @Override
  public final void contextInitialized(final ServletContextEvent servletContextEvent) {
    final String configFilePath = System.getProperty(CONFIG_NAME);
    final String kmsRegion = System.getProperty(KMS_REGION);
    if (!(isEmpty(configFilePath) && isEmpty(kmsRegion))) {
      final String kmsAccessKey = System.getProperty(KMS_ACCESS_KEY);
      final String kmsAccessSecret = System.getProperty(KMS_SECRET_KEY);

      LOGGER.info("Loading properties from file: " + configFilePath);
      try {
        final FileInputStream inputStream = new FileInputStream(new File(configFilePath));
        final ByteBuffer buffer = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
        final AWSKMS kms;
        if (!(isEmpty(kmsAccessKey) && isEmpty(kmsAccessSecret))) {
          kms = awsHelper.createKmsClient(new BasicAWSCredentials(kmsAccessKey, kmsAccessSecret), kmsRegion);
        } else {
          kms = awsHelper.createKmsClient(null, kmsRegion);
        }

        final DecryptRequest decryptRequest = new DecryptRequest().withCiphertextBlob(buffer);
        final ByteBuffer decrypted = kms.decrypt(decryptRequest).getPlaintext();
        final StringReader reader = new StringReader(new String(decrypted.array()));
        final Properties properties = new Properties();
        properties.load(reader);

        final Properties current = System.getProperties();
        properties.stringPropertyNames().forEach(name -> {
          LOGGER.debug("Set/Override {} ", name);
          current.setProperty(name, properties.getProperty(name));
        });
        System.setProperties(current);
        LOGGER.info("Properties reloaded.");
      } catch (Exception exception) {
        LOGGER.error("Error processing file.", exception);
        throw new RuntimeException("Unable to start CD App.");
      }
    }
  }

  @Override
  public void contextDestroyed(final ServletContextEvent servletContextEvent) {
  }
}
