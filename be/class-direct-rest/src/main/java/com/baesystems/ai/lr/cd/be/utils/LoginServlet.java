package com.baesystems.ai.lr.cd.be.utils;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;

/**
 * This filter meant to handle logout and re-route to (Azure) login page.
 */
public class LoginServlet extends HttpServlet {

  /**
   * Default version UID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);

  /**
   * Expired Cookie Max Age.
   */
  private static final int EXPIRE_COOKIE_MAX_AGE = 0;

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
    throws ServletException, IOException {
    doPost(request, response);
  }

  @Override
  protected void doPost(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
      throws ServletException, IOException {
    Optional<Cookie> cookieStream = null;
    String target = httpRequest.getParameter("logout");
    if (httpRequest.getCookies() != null) {
      cookieStream =
          Stream.of(httpRequest.getCookies()).filter(cookie -> cookie.getName().equals(CDConstants.ROLE)).findAny();

      LOGGER.debug("Role Cookie Exists: {}", cookieStream.isPresent());
      if (cookieStream.isPresent() && cookieStream.get().getValue() != null) {
        Cookie cookie = cookieStream.get();
        if (!cookie.getValue().isEmpty()) {
          cookie.setValue(null);
          cookie.setMaxAge(EXPIRE_COOKIE_MAX_AGE);
          LOGGER.debug("Cookie Name: {}, Cookie Max-Age: {}, Cookie Path: {}, Cookie Value: {}", cookie.getName(),
              cookie.getMaxAge(), cookie.getPath(), cookie.getValue());
          httpResponse.addCookie(cookie);
        }
      }
    }

    httpResponse.sendRedirect("/login?logout=" + target);
  }
}

