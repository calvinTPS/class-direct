package com.baesystems.ai.lr.cd.be.utils;

import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.AWSXRayRecorderBuilder;
import com.amazonaws.xray.plugins.EC2Plugin;
import com.amazonaws.xray.strategy.sampling.LocalizedSamplingStrategy;
import java.net.URL;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Amazon X-Ray sampling listener.
 *
 * This listener will limit X-Ray sampling to certain API calls.
 * @author Faizal Sidek
 */
public class DefaultXraySamplingListener implements ServletContextListener {

  @Override
  public final void contextInitialized(final ServletContextEvent sce) {
    final AWSXRayRecorderBuilder builder = AWSXRayRecorderBuilder.standard().withPlugin(new EC2Plugin());

    final URL ruleFile = getClass().getClassLoader().getResource("/default-sampling.json");
    builder.withSamplingStrategy(new LocalizedSamplingStrategy(ruleFile));
    AWSXRay.setGlobalRecorder(builder.build());
  }

  @Override
  public final void contextDestroyed(final ServletContextEvent sce) {
  }
}
