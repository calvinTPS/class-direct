package com.baesystems.ai.lr.cd.be.utils;

import com.amazonaws.xray.entities.Segment;
import com.amazonaws.xray.javax.servlet.AWSXRayServletFilter;
import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspect;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingStat;
import com.baesystems.ai.lr.cd.be.enums.ResponseOptionEnum;
import com.baesystems.ai.lr.cd.core.SpringContextUtil;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.MDC;
import org.springframework.beans.BeansException;

/**
 * Custom servlet filter to record additional headers.
 *
 * @author Faizal Sidek
 */
public class CustomAWSXRayServletFilter extends AWSXRayServletFilter {

  @Override
  public final Segment preFilter(final ServletRequest request, final ServletResponse response) {
    final Segment segment = super.preFilter(request, response);
    Map<String, Object> existingAnnotations = segment.getAnnotations();
    final Map<String, Object> annotations;
    if (existingAnnotations == null) {
      annotations = new HashMap<>();
    } else {
      annotations = new HashMap<>(existingAnnotations);
    }
    annotations.put("UUID", MDC.get(CDConstants.REQUEST_ID));
    segment.setAnnotations(annotations);
    return segment;
  }

  @Override
  public final void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
    throws IOException, ServletException {
    try {
      final ProfilingAspect profiling = SpringContextUtil.getBean(ProfilingAspect.class);
      Optional<ProfilingStat.MethodStat> statOpt = null;
      statOpt = Optional.ofNullable(profiling)
        .map(pa -> pa.injectMethodStatistic(((HttpServletRequest) request).getRequestURL().toString(),
          ResponseOptionEnum.Internal, LocalDateTime.now(), null));

      try {
        final String randomId = RandomStringUtils.randomAlphanumeric(CDConstants.MAX_UUID_LENGTH).toUpperCase();
        MDC.put(CDConstants.REQUEST_ID, randomId);

        super.doFilter(request, response, chain);
        if (response != null) {
          ((HttpServletResponse) response).addHeader("UUID", MDC.get(CDConstants.REQUEST_ID));
        }
      } finally {
        statOpt.ifPresent(stat -> {
          stat.setEndTime(LocalDateTime.now());
          Optional.ofNullable(profiling).ifPresent(ProfilingAspect::flushStat);
        });
        MDC.remove(CDConstants.REQUEST_ID);
      }
    } catch (BeansException exception) {
      super.doFilter(request, response, chain);
    }
  }
}
