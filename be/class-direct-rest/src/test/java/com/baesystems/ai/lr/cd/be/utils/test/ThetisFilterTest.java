package com.baesystems.ai.lr.cd.be.utils.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.ThetisSecurityFilter;
import com.baesystems.ai.lr.cd.core.SpringContextUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.utils.ThetisSecurityFilter}.
 *
 * @author RKaneysan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ThetisFilterTest {
  /**
   * Log object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ThetisFilterTest.class);

  /**
   * Asset Service.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Mast asset id 1.
   */
  private static final String MAST_ASSET_IMO_NUMBER = "1000019";

  /**
   * Test Case for Thetis Filter with valid imo number.
   *
   * @throws IOException IOException.
   * @throws ServletException ServletException.
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testValidImoNumber() throws ClassDirectException, IOException, ServletException {
    // create the objects to be mocked.
    mockMastAsset(MAST_ASSET_IMO_NUMBER);
    final ThetisSecurityFilter thetisFilter = new ThetisSecurityFilter();
    MockHttpServletRequest mockRequest = new MockHttpServletRequest();
    // mock the getParameter(lrno) response
    mockRequest.setParameter("lrno", MAST_ASSET_IMO_NUMBER);
    LOGGER.debug("Thetis Filter Test - LR NO: {} ", mockRequest.getParameter("lrno"));
    MockHttpServletResponse mockResponse = new MockHttpServletResponse();
    FilterChain mockChain = Mockito.mock(FilterChain.class);
    thetisFilter.doFilter(mockRequest, mockResponse, mockChain);

    // verify if "do filter" of "filter chain" is called at least once
    verify(mockChain, atLeastOnce()).doFilter(any(), any());

    // "http servlet response" status code is not forbidden (403).
    assertFalse(HttpServletResponse.SC_FORBIDDEN == mockResponse.getStatus());
  }

  /**
   * Test Case for Thetis Filter without any imo number.
   *
   * @throws IOException IOException.
   * @throws ServletException ServletException.
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testWithoutImoNumber() throws IOException, ServletException, ClassDirectException {
    // create the objects to be mocked.
    mockMastAsset(MAST_ASSET_IMO_NUMBER);
    final ThetisSecurityFilter thetisFilter = new ThetisSecurityFilter();
    MockHttpServletRequest mockRequest = new MockHttpServletRequest();
    MockHttpServletResponse mockResponse = new MockHttpServletResponse();
    FilterChain mockChain = Mockito.mock(FilterChain.class);
    // mock the getParameter(lrno) response
    LOGGER.debug("Thetis Filter Test - LR NO: {} ", mockRequest.getParameter("lrno"));
    thetisFilter.doFilter(mockRequest, mockResponse, mockChain);

    // verify if "do filter" of "filter chain" is called at least once
    verify(mockChain, never()).doFilter(any(), any());

    // "http servlet response" status code is not forbidden (403).
    assertEquals(HttpServletResponse.SC_FORBIDDEN, mockResponse.getStatus());
  }

  /**
   * Mock single asset.
   *
   * @param imoNumber asset id.
   * @return asset asset hdto.
   * @throws ClassDirectException ClassDirectException.
   */
  private AssetHDto mockMastAsset(final String imoNumber) throws ClassDirectException {
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setCode("LRV1");
    doReturn(asset).when(assetService).getAssetByImoNumber(eq(imoNumber));
    return asset;
  }

  /**
   * Inner configuration.
   */
  @Configuration
  public static class Config {

    /**
     * Mock asset service.
     *
     * @return mock service.
     */
    @Bean(name = "AssetService")
    public AssetService assetService() {
      return mock(AssetService.class);
    }

    /**
     * Initialize spring context util.
     *
     * @return context util.
     */
    @Bean
    public SpringContextUtil springContextUtil() {
      return new SpringContextUtil();
    }
  }
}
