package com.baesystems.ai.lr.cd.be.utils.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspect;
import com.baesystems.ai.lr.cd.be.utils.AvailabilityFilter;
import com.baesystems.ai.lr.cd.core.SpringContextUtil;
import com.baesystems.ai.lr.cd.service.security.AvailabilityService;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.utils.AvailabilityFilter}.
 *
 * @author Faizal Sidek
 * @author syalavarthi 22-05-2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AvailabilityFilterTest {

  /**
   * The {@link AvailabilityService} from Spring context.
   */
  @Autowired
  private AvailabilityService availabilityService;

  /**
   * Tests success scenario
   * {@link AvailabilityFilter#doFilter(ServletRequest, javax.servlet.ServletResponse, FilterChain)}}
   * allow request when system is available.
   *
   * @throws IOException if fail to perform object conversion.
   * @throws ServletException if fail to invoke filter chain.
   */
  @Test
  public final void shouldAllowRequestWhenAvailable() throws IOException, ServletException {
    when(availabilityService.isServiceAvailable()).thenReturn(true);

    final FilterChain filterChain = mock(FilterChain.class);
    // create the objects to be mocked
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
    // mock the getRequestURL() response
    when(httpServletRequest.getRequestURL()).thenReturn(new StringBuffer("/assets/{assetCode}"));
    final AvailabilityFilter availabilityFilter = new AvailabilityFilter();
    availabilityFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
    verify(filterChain, atLeastOnce()).doFilter(any(), any());
  }

  /**
   * Tests negative scenario
   * {@link AvailabilityFilter#doFilter(ServletRequest, javax.servlet.ServletResponse, FilterChain)}}
   * return 503 when not available.
   *
   * @throws IOException if fail to perform object conversion.
   * @throws ServletException if fail to invoke filter chain.
   */
  @Test
  public final void shouldBlockRequestWhenNotAvailable() throws IOException, ServletException {
    final MockHttpServletResponse response = new MockHttpServletResponse();
    when(availabilityService.isServiceAvailable()).thenReturn(false);

    final AvailabilityFilter availabilityFilter = new AvailabilityFilter();
    availabilityFilter.doFilter(mock(ServletRequest.class), response, mock(FilterChain.class));
    assertEquals(HttpStatus.GATEWAY_TIMEOUT.value(), response.getStatus());
  }

  /**
   * Inner configuration.
   */
  @Configuration
  public static class Config {

    /**
     * Mock availability service.
     *
     * @return mock service.
     */
    @Bean(name = "AvailabilityService")
    public AvailabilityService availabilityService() {
      return mock(AvailabilityService.class);
    }

    /**
     * Mock profiling aspect.
     *
     * @return mock service.
     */
    @Bean(name = "ProfilingAspect")
    public ProfilingAspect profilingAspect() {
      return mock(ProfilingAspect.class);
    }

    /**
     * Initialize spring context util.
     *
     * @return context util.
     */
    @Bean
    public SpringContextUtil springContextUtil() {
      return new SpringContextUtil();
    }
  }
}
