package com.baesystems.ai.lr.cd.be.utils.test;

import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.DecryptResult;
import com.baesystems.ai.lr.cd.be.utils.EncryptedPropertiesListener;
import com.baesystems.ai.lr.cd.service.aws.AWSHelper;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.servlet.ServletContextEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link EncryptedPropertiesListener}.
 *
 * @author Faizal Sidek
 */
@RunWith(MockitoJUnitRunner.class)
public class EncryptedPropertiesListenerTest {

  /**
   * Listener to be tested.
   */
  private EncryptedPropertiesListener listener = new EncryptedPropertiesListener();

  /**
   * Mock helper.
   */
  @Mock
  private AWSHelper helper;

  /**
   * Sample properties file contents.
   */
  private final StringBuilder sampleProperties = new StringBuilder()
    .append("retrofit.mast.url=http://localhost:8080/mast/api/v2/\n")
    .append("jdbc.cd.driverClassName=com.mysql.jdbc.Driver\n")
    .append("jdbc.cd.Url=jdbc:mysql://localhost:3306/lrcd_db\n")
    .append("jdbc.cd.schema=lrcd_db\n")
    .append("jdbc.cd.username=testUser");

  /**
   * Initialize unit test.
   * @throws IOException when unable to create file.
   */
  @Before
  public final void initialize() throws IOException {
    final File file = File.createTempFile("test", "test.txt");
    System.setProperty(EncryptedPropertiesListener.CONFIG_NAME, file.getAbsolutePath());
    System.setProperty(EncryptedPropertiesListener.KMS_REGION, "eu-west-1");
    listener.setAwsHelper(helper);
  }

  /**
   * Listener should set the properties from file.
   */
  @Test
  public final void initializeWithListenerEnabled() {
    final AWSKMS kmsClient = mock(AWSKMS.class);
    final DecryptResult decryptResult = mock(DecryptResult.class);
    when(decryptResult.getPlaintext()).thenReturn(ByteBuffer.wrap(sampleProperties.toString().getBytes()));

    when(kmsClient.decrypt(any(DecryptRequest.class))).thenReturn(decryptResult);
    when(helper.createKmsClient(any(), anyString())).thenReturn(kmsClient);

    listener.contextInitialized(mock(ServletContextEvent.class));

    assertNotNull(System.getProperty("jdbc.cd.username"));
    assertEquals("testUser", System.getProperty("jdbc.cd.username"));
  }

  /**
   * Listener will override current properties value.
   */
  @Test
  public final void listenerWillOverrideCurrentProperties() {
    final AWSKMS kmsClient = mock(AWSKMS.class);
    final DecryptResult decryptResult = mock(DecryptResult.class);
    when(decryptResult.getPlaintext()).thenReturn(ByteBuffer.wrap(sampleProperties.toString().getBytes()));

    when(kmsClient.decrypt(any(DecryptRequest.class))).thenReturn(decryptResult);
    when(helper.createKmsClient(any(), anyString())).thenReturn(kmsClient);

    System.setProperty("jdbc.cd.username", "treidan");
    listener.contextInitialized(mock(ServletContextEvent.class));

    assertNotNull(System.getProperty("jdbc.cd.username"));
    assertEquals("testUser", System.getProperty("jdbc.cd.username"));
  }

  /**
   * Listener will throw runtime exception when block is having exception.
   */
  @Test(expected = RuntimeException.class)
  public final void listenerWillThrowRuntimeExceptionDuringError() {
    listener.contextInitialized(mock(ServletContextEvent.class));
  }
}
