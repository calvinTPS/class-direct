# class-direct-service

### How to use Resource injection ###

1. Create a new Java bean extending __MAST__ bean dao in _domain-dao_ module.
2. Identify _LinkResource_ to be injected from __MAST__ bean dao, create actual field with actual _LinkResource_ object on bean.
3. Annotate that field with _@LinkedResource_ annotation, and specify _referenceId_ with field name from MAST bean dao.
4. Create a __Retrofit__ service to fetch the dao from __MAST__, if any create another service layer and delegate to __Retrofit__ service.
5. Annotate the method to fetch the injected DAO with _@ResourceProvider_, specify the _value_ with target DAO type.
	_Note: the value is optional for non-collection return type._
6. To inject the _LinkResource_, use __Resources.inject(Object)__ after the DAO has be initialized.


### How to create Spring-based unit test ###
1. Create a unit test class in the package, preferably root service package with test. For example:
Service to be tested package: com.baesystems.ai.lr.cd.service.asset
Test package would be: com.baesystems.ai.lr.cd.service.asset.__test__

2. Create an inner class, named it as Config. Extends class BaseMockConfiguration and finally annotate the class with _@Configuration_ annotation. 

Example:
```Java
import org.springframework.context.annotation.Configuration;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

public class AssetServiceTest {
	@Configuration
	public static class Config extends BaseMockConfiguration {
	
	}
}
```

3. Annotate the test class with _@RunWith_ and _@ContextConfiguration_ annotations.
Example:
```Java
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.context.annotation.Configuration;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

@RunWith(SpringJunit4ClassRunner.class)
@ContextConfiguration(classes = AssetServiceTest.Config.class)
public class AssetServiceTest {
	@Configuration
	public static class Config extends BaseMockConfiguration {
	
	}
}
```

4. Finally, override the BaseMockConfiguration with actual class/service that you want to test. Remember to annotate the overrode method with _@Bean_.
Example:
```Java
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.context.annotation.Configuration;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

@RunWith(SpringJunit4ClassRunner.class)
@ContextConfiguration(classes = AssetServiceTest.Config.class)
public class AssetServiceTest {
	@Configuration
	public static class Config extends BaseMockConfiguration {
		@Bean
    	@Override
    	public AssetReferenceService assetReferenceService() {
      		return new AssetReferenceServiceImpl();
    	}
	}
}
```


Holds all the services and their implementations. **__Keep tidy and organized__**