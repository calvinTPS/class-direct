package com.baesystems.ai.lr.cd.service.userprofile.helper;

import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * @author fwijaya on 16/5/2017.
 */
public interface UpdateUserCallback {

  /**
   * Update user profile based on information from sso.
   *
   * @param userProfile user profile.
   * @param ssoProfile SSO data.
   * @throws ClassDirectException exceptions.
   */
  void update(UserProfiles userProfile, LRIDUserDto ssoProfile) throws ClassDirectException;
}
