package com.baesystems.ai.lr.cd.service.asset.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BuilderHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ServiceCreditStatusRefHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;

/**
 * Provides services to get asset related reference data.
 *
 * @author Faizal Sidek
 * @author syalavarthi 15-06-2017.
 *
 */
public interface AssetReferenceService {

  /**
   * Returns list of {@link AssetTypeHDto} asset types.
   *
   * @return list of asset types.
   *
   */
  List<AssetTypeHDto> getAssetTypes();

  /**
   * Returns asset type {@link AssetTypeHDto} by asset type id.
   *
   * @param id the unique key of asset type
   * @return asset type
   *
   */
  AssetTypeHDto getAssetType(Long id);

  /**
   * Returns list of {@link AssetCategoryHDto} asset categories.
   *
   * @return list of asset categories
   *
   */
  List<AssetCategoryHDto> getAssetCategories();

  /**
   * Returns asset category {@link AssetCategoryHDto} by asset category id.
   *
   * @param id the unique key of asset category.
   * @return asset category.
   *
   */
  AssetCategoryHDto getAssetCategory(Long id);

  /**
   * Returns list of {@link ClassStatusHDto} class statuses.
   *
   * @return list of class statuses.
   *
   */
  List<ClassStatusHDto> getClassStatuses();

  /**
   * Returns specific class status {@link ClassStatusHDto} by class status id.
   *
   * @param id the unique key of class status.
   * @return class status.
   *
   */
  ClassStatusHDto getClassStatus(Long id);

  /**
   * Returns list of {@link CodicilCategoryHDto} codicil categories.
   *
   * @return list of codicil categories
   *
   */
  List<CodicilCategoryHDto> getCodicilCategories();

  /**
   * Returns specific codicil category {@link CodicilCategoryHDto} by codicil category id.
   *
   * @param id the unique key of codicil category.
   * @return codicil category.
   *
   */
  CodicilCategoryHDto getCodicilCategory(Long id);

  /**
   * Returns list of {@link FlagStateHDto} flag states.
   *
   * @return list of flag states.
   *
   */
  List<FlagStateHDto> getFlagStates();

  /**
   * Returns specific flag state {@link FlagStateHDto} by flag state id.
   *
   * @param flagStateId the unique key of flag state.
   * @return flag state.
   *
   */
  FlagStateHDto getFlagState(long flagStateId);

  /**
   * Returns list of {@link AssetLifeCycleStatusDto} asset life cycle statuses.
   *
   * @return list of asset life cycle statuses.
   *
   */
  List<AssetLifeCycleStatusDto> getAssetLifecycleStatuses();

  /**
   * Returns specific asset life cycle status {@link AssetLifeCycleStatusDto} by asset life cycle
   * status id.
   *
   * @param id the unique key of asset life cycle status.
   * @return asset life cycle status.
   *
   */
  AssetLifeCycleStatusDto getAssetLifecycleStatus(Long id);

  /**
   * Returns list of {@link BuilderHDto} builders.
   *
   * @return list of builders.
   *
   */
  List<BuilderHDto> getBuilders();

  /**
   * Returns specific builder {@link BuilderHDto} by builder id.
   *
   * @param id the unique key of builder.
   * @return builder.
   *
   */
  BuilderHDto getBuilderById(Long id);

  /**
   * Returns list of {@link DueStatusDto} due statuses.
   *
   * @return list of due statuses.
   *
   */
  List<DueStatusDto> getDueStatuses();

  /**
   * Returns specific due status {@link DueStatusDto} by due status id.
   *
   * @param id the unique key of due status.
   * @return due status.
   *
   */
  DueStatusDto getDueStatusesById(Long id);

  /**
   * Returns list of {@link RepairActionHDto} repair actions.
   *
   * @return list of repair actions.
   *
   */
  List<RepairActionHDto> getRepairActions();

  /**
   * Returns specific repair action {@link RepairActionHDto} by repair action id.
   *
   * @param id the unique key of repair action.
   * @return repair action.
   *
   */
  RepairActionHDto getRepairAction(Long id);

  /**
   * Returns list of {@link DefectStatusHDto} defect statuses.
   *
   * @return list of defect statuses.
   *
   */
  List<DefectStatusHDto> getDefectStatuses();

  /**
   * Returns specific defect status {@link DefectStatusHDto} by defect status id.
   *
   * @param id the unique key of defect status.
   * @return defect status.
   *
   */
  DefectStatusHDto getDefectStatus(Long id);

  /**
   * Returns list of {@link CodicilStatusHDto} codicil statuses.
   *
   * @return list of codicil statuses.
   *
   */
  List<CodicilStatusHDto> getCodicilStatuses();

  /**
   * Returns specific codicil status {@link CodicilStatusHDto} by codicil status id.
   *
   * @param id the unique key of codicil status.
   * @return codicil status.
   *
   */
  CodicilStatusHDto getCodicilStatus(Long id);

  /**
   * Returns list of {@link RepairTypeHDto} repair types.
   *
   * @return list of repair types.
   *
   */
  List<RepairTypeHDto> getRepairTypes();

  /**
   * Returns specific repair type {@link RepairTypeHDto} by repair type id.
   *
   * @param id the unique key of repair type.
   * @return repair type.
   *
   */
  RepairTypeHDto getRepairType(Long id);

  /**
   * Returns list of {@link ClassificationSocietyHDto} classification societies.
   *
   * @return list of classification societies.
   *
   */
  List<ClassificationSocietyHDto> getClassificationSocieties();

  /**
   * Returns specific classification society {@link ClassificationSocietyHDto} by classification
   * society id.
   *
   * @param id the unique key of classification society.
   * @return classification society.
   *
   */
  ClassificationSocietyHDto getClassificationSociety(Long id);

  /**
   * Returns list of {@link ClassMaintenanceStatusHDto} class maintenance statuses.
   *
   * @return list of class maintenance statuses.
   *
   */
  List<ClassMaintenanceStatusHDto> getClassMaintenanceStatuses();

  /**
   * Returns specific class maintenance status {@link ClassMaintenanceStatusHDto} by class
   * maintenance status id.
   *
   * @param id the unique key of class maintenance status.
   * @return class maintenance status.
   *
   */
  ClassMaintenanceStatusHDto getClassMaintenanceStatus(Long id);

  /**
   * Returns list of {@link ServiceCreditStatusRefHDto} service credit statuses.
   *
   * @return list of service credit statuses.
   *
   */
  List<ServiceCreditStatusRefHDto> getServiceCreditStatuses();

  /**
   * Returns specific service credit status {@link ServiceCreditStatusRefHDto} by service credit
   * status id.
   *
   * @param id the unique key of service credit status.
   * @return service credit status.
   *
   */
  ServiceCreditStatusRefHDto getServiceCreditStatus(Long id);

  /**
   * Returns list of {@link ActionTakenHDto} action taken.
   *
   * @return list of action taken.
   *
   */
  List<ActionTakenHDto> getActionTakenList();

  /**
   * Returns specific action taken {@link ActionTakenHDto} by action taken id.
   *
   * @param id the unique key of action taken.
   * @return action taken.
   *
   */
  ActionTakenHDto getActionTaken(Long id);


  /**
   * Returns list of {@link PartyRoleHDto} customer role.
   *
   * @return list of party roles.
   */
  List<PartyRoleHDto> getPartyRoles();

  /**
   * Returns specific party role {@link PartyRoleHDto} by unique key of party role.
   *
   * @param id the unique identifier of party role.
   * @return party role.
   */
  PartyRoleHDto getPartyRole(Long id);
  /**
   * Returns list of {@link MajorNCNStatusHDto} major non confirmitive notes.
   *
   * @return list of major non confirmitive notes.
   *
   */
  List<MajorNCNStatusHDto> getMncnStatusList();

  /**
   * Returns major non confirmitive note {@link MajorNCNStatusHDto} by major non confirmitive note id.
   *
   * @param id the unique key of major non confirmitive note.
   * @return major non confirmitive note.
   *
   */
  MajorNCNStatusHDto getMncnStatus(Long id);

}
