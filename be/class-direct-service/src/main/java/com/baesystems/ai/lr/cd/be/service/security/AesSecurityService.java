package com.baesystems.ai.lr.cd.be.service.security;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.MasqueradeUserDto;

/**
 * AES Encryption Service.
 *
 * @author RKaneysan
 *
 */
public interface AesSecurityService {

  /**
   * Encrypt role name and asset id.
   *
   * @param roleName role name.
   * @param assetId asset id.
   * @return encrypted cookie.
   */
  String encrypt(String roleName, Long assetId);

  /**
   * Decrypt role name from encrypted value.
   *
   * @param encryptedValue encrypted value.
   * @return role name.
   */
  String decryptRoleName(String encryptedValue);

  /**
   * Decrypt asset id from encrypted value.
   *
   * @param encryptedValue encrypted value.
   * @return asset id.
   */
  Long decryptAssetId(String encryptedValue);

  /**
   * Encrypt masquerade user id.
   *
   * @param dto masqueradingUserDto.
   * @return encrypted user id.
   */
  @PreAuthorize(value = "hasAnyRole('LR_ADMIN', 'LR_SUPPORT')")
  StringResponse encryptMasqueradeUserId(final MasqueradeUserDto dto);

  /**
   * Decrypter.
   *
   * @param encryptedValue encrypted value.
   * @return decrypted value.
   */
  String decryptValue(final String encryptedValue);
}
