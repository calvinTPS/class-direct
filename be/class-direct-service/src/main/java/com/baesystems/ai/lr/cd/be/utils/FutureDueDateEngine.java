package com.baesystems.ai.lr.cd.be.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.domain.dto.export.RepeatOfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.enums.ProductType;
import com.baesystems.ai.lr.enums.ServiceType;

/**
 * Calculate future due date.
 *
 * @author yng
 *
 */
public final class FutureDueDateEngine {

  /**
   * Logger for AssetServiceImpl class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FutureDueDateEngine.class);

  /**
   * Constructor.
   */
  private FutureDueDateEngine() {

  }


  /**
   * Calculate future date.
   *
   * @param serviceList list object reference service.
   * @param dateMaxLimit max limit for calculation.
   * @return list of future date services.
   * @throws CloneNotSupportedException value.
   */
  public static List<RepeatOfDto> calculate(final List<ScheduledServiceHDto> serviceList,
      final Date dateMaxLimit) throws CloneNotSupportedException {
    LOGGER.info("Entering calculate");

    // create grouped list
    final Map<Long, Map<Long, Map<Long, List<ScheduledServiceHDto>>>> groupedList =
        createGroupList(serviceList);

    final List<RepeatOfDto> repeatedService = new ArrayList<>();

    // loop the data
    for (final Map.Entry<Long, Map<Long, Map<Long, List<ScheduledServiceHDto>>>> groupedListEntry : groupedList
        .entrySet()) {
      final Map<Long, Map<Long, List<ScheduledServiceHDto>>> keyByProductType =
          groupedListEntry.getValue();

      for (final Map.Entry<Long, Map<Long, List<ScheduledServiceHDto>>> keyByProductTypeEntry : keyByProductType
          .entrySet()) {
        final Long productTypeId = keyByProductTypeEntry.getKey();
        final Map<Long, List<ScheduledServiceHDto>> keyByServiceType = keyByProductTypeEntry.getValue();

        if (productTypeId.equals(ProductType.CLASSIFICATION.getValue())) {
          LOGGER.info("Entering classification...");
          // calculate logic as per productType = Classification
          repeatedService.addAll(calculateForProductTypeClassfication(keyByServiceType, dateMaxLimit));
        } else if (productTypeId.equals(ProductType.MMS.getValue())) {
          LOGGER.info("Entering MMS...");
          // calculate logic as per productType = MMS
          repeatedService.addAll(calculateForProductTypeMMS(keyByServiceType, dateMaxLimit));
        } else if (productTypeId.equals(ProductType.STATUTORY.getValue())) {
          LOGGER.info("Entering statutory...");
          // calculate logic as per productType = Statutory
          repeatedService.addAll(calculateForProductTypeStatutory(keyByServiceType, dateMaxLimit));
        }

      }
    }

    LOGGER.info("Exiting calculate");
    return repeatedService;
  }

  /**
   * Calculation for production type classification.
   *
   * @param serviceList service list.
   * @param dateMaxLimit max date value.
   * @return list of service.
   * @throws CloneNotSupportedException value.
   */
  public static List<RepeatOfDto> calculateForProductTypeClassfication(
      final Map<Long, List<ScheduledServiceHDto>> serviceList, final Date dateMaxLimit)
      throws CloneNotSupportedException {
    LOGGER.info("Entering calc classification");

    final List<RepeatOfDto> futureDueDateList = new ArrayList<>();
    final List<Date> renewalDueDateList = new ArrayList<>();

    // Step: Get future due date for serviceType = annual
    if (serviceList.get(ServiceType.ANNUAL.value()) != null) {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.ANNUAL.value());
      futureDueDateList.addAll(calculateRule1(services, dateMaxLimit));
    }

    // Step: Get future due date for serviceType = renewal
    Optional.ofNullable(serviceList.get(ServiceType.RENEWAL.value())).ifPresent(serviceTypeId -> {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.RENEWAL.value());
      List<Date> renewDate = services.stream().map(service -> service.getDueDate()).collect(Collectors.toList());
      // add current due for renewal as it affect calculation future calculation for intermediate
      renewalDueDateList.addAll(renewDate);
    });

    // Step: Get due date for serviceType = intermediate
    if (serviceList.get(ServiceType.INTERMEDIATE.value()) != null) {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.INTERMEDIATE.value());
      futureDueDateList.addAll(calculateRule2(services, renewalDueDateList, dateMaxLimit));
    }

    // Step: Get due date for serviceType = standalone
    if (serviceList.get(ServiceType.STANDALONE.value()) != null) {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.STANDALONE.value());
      futureDueDateList.addAll(calculateRule1(services, dateMaxLimit));
    }

    LOGGER.info("Exiting calc classification");
    return futureDueDateList;
  }

  /**
   * Calculation for production type MMS.
   *
   * @param serviceList service list.
   * @param dateMaxLimit max date value.
   * @return list of service.
   * @throws CloneNotSupportedException value.
   */
  public static List<RepeatOfDto> calculateForProductTypeMMS(final Map<Long, List<ScheduledServiceHDto>> serviceList,
      final Date dateMaxLimit) throws CloneNotSupportedException {
    LOGGER.info("Entering calc MMS");

    final List<RepeatOfDto> futureDueDateList = new ArrayList<>();
    List<Date> renewalDueDateList = new ArrayList<>();

    // Step: Get future due date for serviceType = renewal
    if (serviceList.get(ServiceType.RENEWAL.value()) != null) {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.RENEWAL.value());
      final List<RepeatOfDto> result = calculateRule1(services, dateMaxLimit);
      futureDueDateList.addAll(result);
      renewalDueDateList =
          result.stream().map(object -> object.getDueDate()).collect(Collectors.toList());
      List<Date> renewDate = services.stream().map(service -> service.getDueDate()).collect(Collectors.toList());
      // add current due for renewal as it affect calculation future calculation for periodic
      renewalDueDateList.addAll(renewDate);
    }

    // Step: Get future due date for serviceType = periodic
    if (serviceList.get(ServiceType.PERIODIC.value()) != null) {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.PERIODIC.value());
      futureDueDateList.addAll(calculateRule2(services, renewalDueDateList, dateMaxLimit));
    }

    LOGGER.info("Exiting calc MMS");
    return futureDueDateList;
  }

  /**
   * Calculation for production type statutory.
   *
   * @param serviceList service list.
   * @param dateMaxLimit max date value.
   * @return list of service.
   * @throws CloneNotSupportedException value.
   */
  public static List<RepeatOfDto> calculateForProductTypeStatutory(
      final Map<Long, List<ScheduledServiceHDto>> serviceList, final Date dateMaxLimit)
      throws CloneNotSupportedException {
    LOGGER.info("Entering calc statutory");

    final List<RepeatOfDto> futureDueDateList = new ArrayList<>();
    final List<Date> renewalDueDateList = new ArrayList<>();
    final List<Date> renewalDueDateListWithDateCorrection = new ArrayList<>();
    List<Date> intermediateDueDateList = new ArrayList<>();

    // Step: Get future due date for serviceType = renewal
    Optional.ofNullable(serviceList.get(ServiceType.RENEWAL.value())).ifPresent(serviceTypeId -> {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.RENEWAL.value());

      // add current due for renewal as it affect calculation future calculation for intermediate
      for (ScheduledServiceHDto service : services) {
        renewalDueDateList.add(service.getDueDate());

        // this correction is use for condition in RSD + 1 day in calculating future date for annual
        LocalDate dueDateLocalDate = DateUtils.getLocalDateFromDate(service.getDueDate());
        dueDateLocalDate = dueDateLocalDate.plusDays(1L);
        renewalDueDateListWithDateCorrection.add(DateUtils.getLocalDateToDate(dueDateLocalDate));
      }
    });

    // Step: Get future due date for serviceType = intermediate
    if (serviceList.get(ServiceType.INTERMEDIATE.value()) != null) {
      final List<ScheduledServiceHDto> services = serviceList.get(ServiceType.INTERMEDIATE.value());
      final List<RepeatOfDto> result =
          calculateRule2(services, renewalDueDateList, dateMaxLimit);
      futureDueDateList.addAll(result);

      intermediateDueDateList =
          result.stream().map(object -> object.getDueDate()).collect(Collectors.toList());
    }

    // Step: Get future due date for serviceType = annual
    if (serviceList.get(ServiceType.ANNUAL.value()) != null) {
      final List<ScheduledServiceHDto> service = serviceList.get(ServiceType.ANNUAL.value());
      final List<RepeatOfDto> result = calculateRule1(service, dateMaxLimit);

      final List<RepeatOfDto> filteredResult = new ArrayList<>();

      for (final RepeatOfDto serv : result) {
        if (!renewalDueDateListWithDateCorrection.contains(serv.getDueDate())
            && !intermediateDueDateList.contains(serv.getDueDate())) {
          filteredResult.add(serv);
        }
      }

      futureDueDateList.addAll(filteredResult);
    }

    LOGGER.info("Exiting calc statutory");
    return futureDueDateList;
  }

  /**
   * Calculation formula.
   *
   * Calculation for formula: dd = Y + (N x CP)
   *
   * Nomenclature dd = Future due dates
   * N = Number cycle periodicities beyond the current due date
   * CP = The service’s cycle periodicity set in MAST
   * Y = Current due date in MAST
   * RSD = Any current or future due date of the Associated Renewal
   * survey in same service group as the statutory annual
   * ISD = Any current or future due date of the Associated intermediate
   * survey in the same service group as the statutory annual
   *
   * @param serviceList list of services for future due date calculation.
   * @param dateMaxLimit date limit for future due date calculation.
   * @return list of future date list.
   * @throws CloneNotSupportedException value.
   */
  public static List<RepeatOfDto> calculateRule1(final List<ScheduledServiceHDto> serviceList,
      final Date dateMaxLimit) throws CloneNotSupportedException {
    final List<RepeatOfDto> futureDueDateList = new ArrayList<>();
    for (ScheduledServiceHDto service : serviceList) {
      if (service.getDueDate() != null && service.getCyclePeriodicity() != null && service.getCyclePeriodicity() > 0
          && dateMaxLimit != null) {
        // convert max limit date to local date
        final LocalDate maxLimitLocalDate = DateUtils.getLocalDateFromDate(dateMaxLimit);

        // convert due date to local date
        final LocalDate dueDateLocalDate = DateUtils.getLocalDateFromDate(service.getDueDate());

        // calculate the future due date
        LocalDate currentDueDate = dueDateLocalDate.plusMonths(service.getCyclePeriodicity());

        while (currentDueDate.isBefore(maxLimitLocalDate) || currentDueDate.isEqual(maxLimitLocalDate)) {
          futureDueDateList.add(createNewDueDateObject(service, currentDueDate));

          currentDueDate = currentDueDate.plusMonths(service.getCyclePeriodicity());
        }
      }
    }
    return futureDueDateList;
  }

  /**
   * Calculation formula.
   *
   * Calculation for formula: dd = RSD + CP
   *
   * Nomenclature dd = Future due dates
   * N = Number cycle periodicities beyond the current due date
   * CP = The service’s cycle periodicity set in MAST
   * Y = Current due date in MAST
   * RSD = Any current or future due date of the Associated Renewal
   * survey in same service group as the statutory annual
   * ISD = Any current or future due date of the Associated intermediate
   * survey in the same service group as the statutory annual
   *
   * @param serviceList list of services for future due date calculation.
   * @param renewalDueDateList renewal due date list.
   * @param dateMaxLimit date limit for future due date calculation.
   * @return list of future date list.
   * @throws CloneNotSupportedException value.
   */
  public static List<RepeatOfDto> calculateRule2(final List<ScheduledServiceHDto> serviceList,
      final List<Date> renewalDueDateList, final Date dateMaxLimit) throws CloneNotSupportedException {

    final List<RepeatOfDto> futureDueDateList = new ArrayList<>();
    for (ScheduledServiceHDto service : serviceList) {
      if (service.getDueDate() != null && service.getCyclePeriodicity() != null && service.getCyclePeriodicity() > 0
          && dateMaxLimit != null) {

        // convert max limit date to local date
        final LocalDate maxLimitLocalDate = DateUtils.getLocalDateFromDate(dateMaxLimit);

        for (final Date date : renewalDueDateList) {
          // convert current renewal date to local date
          final LocalDate localDate = DateUtils.getLocalDateFromDate(date);

          // calculate the future due date
          final LocalDate currentDueDate = localDate.plusMonths(service.getCyclePeriodicity());

          // check if due date is > dateMaxLimit
          if (currentDueDate.isBefore(maxLimitLocalDate)
              || currentDueDate.isEqual(maxLimitLocalDate)) {
            futureDueDateList.add(createNewDueDateObject(service, currentDueDate));
          }
        }
      }
    }
    return futureDueDateList;
  }

  /**
   * Create new service object with future due date.
   *
   * @param service original service object.
   * @param dueDate future due date.
   * @return repeatOf service object with updated due date.
   * @throws CloneNotSupportedException value.
   */
  public static RepeatOfDto createNewDueDateObject(final ScheduledServiceHDto service,
      final LocalDate dueDate) throws CloneNotSupportedException {

    // create a repeatOf object.
    final RepeatOfDto localService = new RepeatOfDto();

    // set due date
    localService.setDueDate(DateUtils.getLocalDateToDate(dueDate));

    // check for range date.
    final Integer lowerRangeOffset = Optional.ofNullable(service.getServiceCatalogueH())
        .map(ServiceCatalogueHDto::getLowerRangeDateOffsetMonths).orElse(0);
    final Integer upperRangeOffset = Optional.ofNullable(service.getServiceCatalogueH())
        .map(ServiceCatalogueHDto::getUpperRangeDateOffsetMonths).orElse(0);

    // only perform calculation if range != 0
    if (lowerRangeOffset != 0) {
      final LocalDate lowerRangeLocalDate = dueDate.minusMonths(lowerRangeOffset).plusDays(1);
      localService.setLowerRangeDate(DateUtils.getLocalDateToDate(lowerRangeLocalDate));
    } else {
      localService.setLowerRangeDate(null);
    }

    // only perform calculation if range != 0
    if (upperRangeOffset != 0) {
      final LocalDate upperRangeLocalDate = dueDate.plusMonths(upperRangeOffset).minusDays(1);
      localService.setUpperRangeDate(DateUtils.getLocalDateToDate(upperRangeLocalDate));
    } else {
      localService.setUpperRangeDate(null);
    }

    // set repeat of = objectId
    localService.setRepeatOf(service.getId());


    // this object is to perform due status update on dueStatusH
    final ScheduledServiceHDto serviceForDueStatusCalc = service.clone();
    serviceForDueStatusCalc.setDueDate(localService.getDueDate());
    serviceForDueStatusCalc.setLowerRangeDate(localService.getLowerRangeDate());
    serviceForDueStatusCalc.setUpperRangeDate(localService.getUpperRangeDate());

    localService.setDueStatusH(DueStatus.NOT_DUE);
    localService.setDueStatus(new LinkResource(localService.getDueStatusH().getId()));

    return localService;
  }

  /**
   * Create grouping by serviceGroup->productGroup->serviceType.
   *
   * @param services list.
   * @return grouped list.
   */
  public static Map<Long, Map<Long, Map<Long, List<ScheduledServiceHDto>>>> createGroupList(
      final List<ScheduledServiceHDto> services) {
    LOGGER.info("Entering grouping");
    final Map<Long, Map<Long, Map<Long, List<ScheduledServiceHDto>>>> serviceGroup = new TreeMap<>();

    for (final ScheduledServiceHDto service : services) {

      // null check for intermediate hierarchy
      final Long serviceGroupId = Optional.ofNullable(service.getServiceCatalogueH())
          .map(ServiceCatalogueHDto::getServiceGroup).map(LinkResource::getId).orElse(null);
      final Long productTypeId = Optional.ofNullable(service.getServiceCatalogueH())
          .map(ServiceCatalogueHDto::getProductCatalogue).map(ProductCatalogueDto::getProductType)
          .map(LinkResource::getId).orElse(null);
      final Long serviceTypeId = Optional.ofNullable(service.getServiceCatalogueH())
          .map(ServiceCatalogueHDto::getServiceType).map(LinkResource::getId).orElse(null);

      // skip grouping if serviceGroup/productType/serviceType is null
      if (serviceGroupId == null || productTypeId == null || serviceTypeId == null) {
        continue;
      }

      // check the key if present:
      // if not exist: create key pair
      // if exist: add value
      if (serviceGroup.get(serviceGroupId) == null) {
        serviceGroup.put(serviceGroupId, new TreeMap<>());
        serviceGroup.get(serviceGroupId).put(productTypeId, new TreeMap<>());
      } else if (serviceGroup.get(serviceGroupId).get(productTypeId) == null) {
        serviceGroup.get(serviceGroupId).put(productTypeId, new TreeMap<>());
      }
      // LRCD-3393:Inconsistency in appearance of second cycle of machinery items in service schedule
      if (serviceGroup.get(serviceGroupId).get(productTypeId).get(serviceTypeId) == null) {
        serviceGroup.get(serviceGroupId).get(productTypeId).put(serviceTypeId, new ArrayList<ScheduledServiceHDto>());
      }
        serviceGroup.get(serviceGroupId).get(productTypeId).get(serviceTypeId).add(service);
    }

    LOGGER.info("Exiting grouping");
    return serviceGroup;
  }
}
