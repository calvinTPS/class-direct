package com.baesystems.ai.lr.cd.service.attachment.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;

/**
 * Provides services to get list of attachment
 * types, single attachment type, list of attachment categories and single attachment category.
 *
 * @author syalavarthi.
 *
 */
public interface AttachmentReferenceService {
  /**
   * Gets all attachment types.
   *
   * @return list of attachment types.
   */
  List<AttachmentTypeHDto> getAttachmentTypes();

  /**
   * Get attachment type by attachment type id.
   *
   * @param id the primary key for attachment type.
   * @return attachment type.
   */
  AttachmentTypeHDto getAttachmentType(final Long id);

  /**
   * Gets all attachment category.
   *
   * @return list of attachment category.
   */
  List<AttachmentCategoryHDto> getAttachmentCategories();

  /**
   * Get attachment category by attachment category id.
   *
   * @param id the primary key for attachment category.
   * @return attachment category.
   */
  AttachmentCategoryHDto getAttachmentCategory(final Long id);
}
