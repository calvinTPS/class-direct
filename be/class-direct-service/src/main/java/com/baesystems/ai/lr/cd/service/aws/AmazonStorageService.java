package com.baesystems.ai.lr.cd.service.aws;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Service to interface with AWS SDK.
 *
 * @author Faizal Sidek
 */
public interface AmazonStorageService {

  /**
   * Upload new file to AWS S3.
   *
   * @param region region name according to {@link com.amazonaws.regions.Regions}.
   * @param bucketName bucket name.
   * @param key file path.
   * @param sourceFilePath path to source on local file system.
   * @param userId the user id.
   * @return file token in Base64 encoded JSON.
   * @throws ClassDirectException when upload error.
   */
  String uploadFile(String region, String bucketName, String key, String sourceFilePath, String userId)
      throws ClassDirectException;

  /**
   * Upload new file to AWS S3 with default region to
   * {@link com.amazonaws.regions.Regions#EU_WEST_1}.
   *
   * @param bucketName bucket name.
   * @param key file path.
   * @param sourceFilePath path to source on local file system.
   * @param userId the user id.
   * @return file token in Base64 encoded JSON.
   * @throws ClassDirectException when upload error.
   */
  String uploadFile(String bucketName, String key, String sourceFilePath, String userId) throws ClassDirectException;

  /**
   * Download file from AWS S3.
   *
   * @param region     S3 region according to {@link com.amazonaws.regions.Regions}.
   * @param bucketName S3 bucket name.
   * @param fileKey    file path.
   * @return binary contents.
   * @throws ClassDirectException when download error.
   */
  byte[] downloadFile(String region, String bucketName, String fileKey) throws ClassDirectException;

  /**
   * Archive the file to glacier.
   * Refer to LRCD-1875.
   *
   * @param vaultName   glacier vault name.
   * @param description archive description.
   * @param fileName    file name.
   * @param contents    file contents.
   * @return upload id.
   * @throws ClassDirectException when upload failed.
   */
  String archiveFile(String vaultName, String description, String fileName, byte[] contents)
    throws ClassDirectException;
}
