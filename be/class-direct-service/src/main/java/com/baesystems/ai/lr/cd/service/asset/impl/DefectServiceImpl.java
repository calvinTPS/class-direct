package com.baesystems.ai.lr.cd.service.asset.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.DefectService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.jcabi.aspects.Loggable;


/**
 * Provides services to get list of deficiencies for asset and get single defect.
 *
 * @author yng
 * @author syalavarthi 24-05-2017.
 *
 */
@Service(value = "DefectService")
@Loggable(Loggable.DEBUG)
public class DefectServiceImpl implements DefectService {
  /**
   * The {@value #LOGGER} Logger for DefectServiceImpl.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(DefectServiceImpl.class);

  /**
   * The {@link AssetRetrofitService} from spring context.
   *
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * The {@link JobRetrofitService} from spring context.
   *
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  @RestrictedAsset
  @Override
  public final DefectHDto getDefectByAssetIdDefectId(@AssetID final Long assetId, final long defectId)
      throws ClassDirectException {
    DefectHDto defectResponse = null;

    try {
      final DefectHDto defect = assetServiceDelegate.getDefectByAssetIdDefectId(assetId, defectId).execute().body();

      final List<Callable<Void>> task = new ArrayList<>();
      task.add(() -> {
        // get jobs data
        if (defect.getJobs() != null) {
          defect.setJobsH(new ArrayList<>());
          defect.getJobs().forEach(defectJob -> {
            try {
              final JobHDto job = jobServiceDelegate.getJobsByJobId(defectJob.getJob().getId()).execute().body();
              defect.getJobsH().add(job);
            } catch (final IOException exception) {
              LOGGER.error("Error query getDefectByAssetIdDefectId - getJobsByJobId({}): Id not found.",
                  defectJob.getJob().getId(), exception);
            }
          });
        }
        return null;
      });

      task.add(() -> {
        // get item data
        if (defect.getItems() != null) {
          defect.setItemsH(new ArrayList<>());
          defect.getItems().forEach(defectItem -> {
            try {
              final LazyItemHDto item =
                  assetServiceDelegate.getAssetItemDto(assetId, defectItem.getItem().getId()).execute().body();
              defect.getItemsH().add(item);
            } catch (final IOException exception) {
              LOGGER.error("Error query getDefectByAssetIdDefectId - getItem({}): Id not found.",
                  defectItem.getItem().getId(), exception);
            }
          });
        }
        return null;
      });

      task.add(() -> {
        // get coc data
        try {
          final List<CoCHDto> associateCoC =
              assetServiceDelegate.getCoCbyDefectAssetId(assetId, defect.getId()).execute().body();
          associateCoC.forEach(Resources::inject);
          defect.setCocH(associateCoC);
        } catch (final IOException exception) {
          LOGGER.error("Error query getDefectByAssetIdDefectId - getCoCbyDefectAssetId({}): Id not found.",
              defect.getId(), exception);
        }
        return null;
      });

      final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
      try {
        for (final Future<Void> future : executor.invokeAll(task)) {
          future.get();
        }
      } catch (InterruptedException | ExecutionException exception) {
        throw new ClassDirectException(exception);
      } finally {
        executor.shutdown();
      }

      Resources.inject(defect);

      defectResponse = defect;

    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getDefectByAssetIdDefectId.", ioException);
      throw new ClassDirectException(ioException);
    }

    return defectResponse;
  }

  @Override
  public final PageResource<DefectHDto> getDefectsByQuery(final Integer page, final Integer size, final Long assetId,
      final CodicilDefectQueryHDto query) throws ClassDirectException {
    PageResource<DefectHDto> pageResource = null;
    pageResource = PaginationUtil.paginate(getDefectsByQuery(assetId, query), page, size);

    return pageResource;
  }

  /**
   * Gets defects by calling MAST defect query API, postprocessing and hydrating the content.
   *
   * @param assetId the primary key of asset.
   * @param query the body with search parameters.
   * @return list of defects.
   * @throws ClassDirectException if mast API fail or executor failure.
   */
  public final List<DefectHDto> getDefectsByQuery(final Long assetId, final CodicilDefectQueryHDto query)
      throws ClassDirectException {

    final List<DefectHDto> hydratedDefects = new ArrayList<>();
    final ExecutorService executorService = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    final List<Callable<DefectHDto>> callables = new ArrayList<>();
    try {
      // call MAST defect query to get list of defects
      final List<DefectHDto> defects = assetServiceDelegate.getDefectsByQuery(assetId, query).execute().body();

      defects.forEach(defect -> {
        callables.add(hydrateDefectData(defect, assetId));
      });

      final List<Future<DefectHDto>> taskList = executorService.invokeAll(callables);
      for (final Future<DefectHDto> task : taskList) {
        hydratedDefects.add(task.get());
      }

    } catch (final IOException | InterruptedException | ExecutionException exception) {
      throw new ClassDirectException(exception);
    } finally {
      executorService.shutdown();
    }

    return hydratedDefects;
  }

  /**
   * Hydrates job data for input defect.
   *
   * @param defect the defect to be hydrated.
   * @param assetId the primary key of asset.
   * @return hydrated defect.
   */
  private Callable<DefectHDto> hydrateDefectData(final DefectHDto defect, final long assetId) {
    final Map<Long, JobHDto> jobMap = new ConcurrentHashMap<>();

    final Callable<DefectHDto> callable = () -> {

      // hydrate job data in job field
      Optional.ofNullable(defect.getJob()).ifPresent(job -> {
        defect.setJobH(queryJobDetail(job.getId(), jobMap));
      });

      // hydrate job data in raisedOnJob field
      Optional.ofNullable(defect.getRaisedOnJob()).ifPresent(job -> {
        defect.setRaisedOnJobH(queryJobDetail(job.getId(), jobMap));
      });

      // hydrate job data in closedOnJob field
      Optional.ofNullable(defect.getRaisedOnJob()).ifPresent(job -> {
        defect.setClosedOnJobH(queryJobDetail(job.getId(), jobMap));
      });

      Resources.inject(defect);

      // hydrate item data in item field
      if (defect.getItems() != null) {
        defect.setItemsH(new ArrayList<>());
        defect.getItems().forEach(defectItem -> {
          try {
            final LazyItemHDto item =
                assetServiceDelegate.getAssetItemDto(assetId, defectItem.getItem().getId()).execute().body();
            defect.getItemsH().add(item);
          } catch (final IOException exception) {
            LOGGER.error("Error query hydrateDefectData - getItem({}): Id not found.", defectItem.getItem().getId(),
                exception);
          }
        });
      }

      return defect;
    };
    return callable;
  }


  /**
   * Gets job by job id. JobMap is parse in saving queried job to reduce the time of API calling.
   *
   * @param jobId the primary key of job.
   * @param jobMap the collections of jobs.
   * @return job.
   */
  public final JobHDto queryJobDetail(final Long jobId, final Map<Long, JobHDto> jobMap) {
    JobHDto jobDto = null;
    try {
      if (!jobMap.containsKey(jobId)) {
        jobDto = jobServiceDelegate.getJobsByJobId(jobId).execute().body();
        Resources.inject(jobDto);
      } else {
        jobDto = jobMap.get(jobId);
      }

      if (jobDto != null) {
        jobMap.put(jobId, jobDto);
      }
    } catch (final IOException exception) {
      LOGGER.error("Error query queryJobDetail: Job Id {} not found.", jobId, exception);
    }

    return jobDto;
  }
}
