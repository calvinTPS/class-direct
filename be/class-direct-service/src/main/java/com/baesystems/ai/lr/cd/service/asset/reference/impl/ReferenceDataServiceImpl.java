package com.baesystems.ai.lr.cd.service.asset.reference.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.ReferenceDataVersionRetrofitService;
import com.baesystems.ai.lr.cd.service.asset.reference.ReferenceDataService;
import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;

import retrofit2.Call;

/**
 * Provides service to get latest version of reference data {@link ReferenceDataService}.
 *
 * @author MSidek
 *
 */
@Service
public class ReferenceDataServiceImpl implements ReferenceDataService {
  /**
   * The Logger for ReferenceDataService {@link ReferenceDataService}.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ReferenceDataServiceImpl.class);

  /**
   * The {@link ReferenceDataVersionRetrofitService} from spring context.
   *
   */
  @Autowired
  private ReferenceDataVersionRetrofitService referenceDataVersionDelegate;

  @Override
  public final Long getCurrentVersion() {
    Long version = -1L;

    try {
      final Call<ReferenceDataVersionDto> caller = referenceDataVersionDelegate.getReferenceDataVersion();
      final ReferenceDataVersionDto dto = caller.execute().body();
      version = dto.getVersion();
    } catch (IOException exception) {
      LOGGER.error("Exception during executing delegate.", exception);
    }

    return version;
  }
}
