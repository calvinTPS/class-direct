package com.baesystems.ai.lr.cd.service.export;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides service to fetches attachments for major non confirmitive note.
 *
 * @author syalavarthi.
 */
public interface MNCNAttachmentService {

  /**
   * Gets attachments for major non confirmitives by asset id and mncn id.
   *
   * @param assetId the unique identifier for asset.
   * @param mncnId the unique identifier for major non confirmitive note.
   * @return List of SupplementaryInformationHDtos.
   * @throws ClassDirectException when error in getting mncn by mncn id or when no mncn file found for id.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  List<SupplementaryInformationHDto> getAttachmentsMncn(Long assetId, Long mncnId)
      throws ClassDirectException;
}
