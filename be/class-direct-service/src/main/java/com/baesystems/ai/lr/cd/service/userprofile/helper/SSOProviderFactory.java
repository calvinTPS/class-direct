package com.baesystems.ai.lr.cd.service.userprofile.helper;

import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;

import java.util.List;

/**
 * @author fwijaya on 16/5/2017.
 */
public interface SSOProviderFactory {
  /**
   * Return SSO user providers.
   *
   * @param userProfiles List of user profiles.
   * @return {@link SSOUserProvider}.
   */
  List<SSOUserProvider> getProviders(List<UserProfiles> userProfiles);

  /**
   * Return SSO user provider.
   *
   * @param email user email.
   * @return SSO user provider.
   */
  SSOUserProvider getProvider(String email);
}
