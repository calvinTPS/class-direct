package com.baesystems.ai.lr.cd.service.asset;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides services to get list of deficiencies for asset and get single defect.
 *
 * @author yng
 * @author syalavarthi 24-04-2017.
 *
 */
public interface DefectService {
  /**
   * Gets Single defect by id.
   *
   * @param assetId the primary key of asset.
   * @param defectId the primary key of defect.
   * @return defect.
   * @throws ClassDirectException if mast api fail or any error in execution.
   */
  DefectHDto getDefectByAssetIdDefectId(Long assetId, long defectId) throws ClassDirectException;

  /**
   * Gets paginated defects for asset.
   *
   * @param page the page number.
   * @param size the page size.
   * @param assetId the primary key of asset.
   * @param query the body with search parameters.
   * @return list of paginated defects.
   * @throws ClassDirectException if mast API call or executor fail.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  PageResource<DefectHDto> getDefectsByQuery(final Integer page, final Integer size, final Long assetId,
      final CodicilDefectQueryHDto query) throws ClassDirectException;
}
