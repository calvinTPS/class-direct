package com.baesystems.ai.lr.cd.service.asset.reference.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BuilderHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ServiceCreditStatusRefHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;

import retrofit2.Call;

/**
 * Provides services to get asset related reference data.
 *
 * @author Faizal Sidek
 * @author syalavarthi 15-06-2017.
 *
 */
@Service("assetReferenceService")
public class AssetReferenceServiceImpl implements AssetReferenceService, InitializingBean {

  /**
   * The logger {@value #LOGGER} for AssetReferenceService.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetReferenceService.class);
  /**
   * The log message {@value #LOG_MESSAGE} when no element found with requested id.
   */
  private static final String LOG_MESSAGE = "Can't find any element matched id {}.";

  /**
   * The {@link AssetReferenceRetrofitService} from spring context.
   *
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceDelegate;

  @ResourceProvider
  @Override
  public final List<AssetTypeHDto> getAssetTypes() {
    List<AssetTypeHDto> assetTypes = new ArrayList<>();

    try {
      final Call<List<AssetTypeHDto>> caller = assetReferenceDelegate.getAssetTypeDtos();
      assetTypes = caller.execute().body();
      assetTypes.forEach(Resources::inject);
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }

    return assetTypes;
  }

  @ResourceProvider
  @Override
  public final List<AssetCategoryHDto> getAssetCategories() {
    List<AssetCategoryHDto> categories = new ArrayList<>();

    final Call<List<AssetCategoryHDto>> caller = assetReferenceDelegate.getAssetCategories();
    try {
      LOGGER.debug("Finding asset category from MAST.");
      categories = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Retrofit execution failed.", ioe);
    }

    return categories;
  }

  @ResourceProvider
  @Override
  public final AssetTypeHDto getAssetType(final Long id) {
    AssetTypeHDto assetType = null;

    try {
      final List<AssetTypeHDto> assetTypes = self.getAssetTypes();
      assetType = assetTypes.stream().filter(type -> type.getId().equals(id)).findFirst().get();
      Resources.inject(assetType);
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, id);
    }

    return assetType;
  }

  @ResourceProvider
  @Override
  public final AssetCategoryHDto getAssetCategory(final Long id) {
    AssetCategoryHDto category = null;

    final List<AssetCategoryHDto> assetCategories = self.getAssetCategories();
    try {
      category = assetCategories.stream().filter(assetCategory -> assetCategory.getId().equals(id)).findFirst().get();
      Resources.inject(category);
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug("Found no element matching with id {}", id);
    }

    return category;
  }

  @ResourceProvider
  @Override
  public final List<ClassStatusHDto> getClassStatuses() {
    List<ClassStatusHDto> classStatuses = new ArrayList<>();

    final Call<List<ClassStatusHDto>> caller = assetReferenceDelegate.getClassStatus();
    try {
      classStatuses = caller.execute().body();
      if (classStatuses != null) {
        classStatuses.removeIf(status -> status.getDeleted() != null && status.getDeleted().equals(true));
      }
    } catch (final IOException ioe) {
      LOGGER.error("Retrofit execution failed.", ioe);
    }

    return classStatuses;
  }

  @ResourceProvider
  @Override
  public final ClassStatusHDto getClassStatus(final Long id) {
    ClassStatusHDto classStatus = null;

    try {
      final List<ClassStatusHDto> classStatuses = self.getClassStatuses();
      classStatus = classStatuses.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, id);
    }

    return classStatus;
  }

  @ResourceProvider
  @Override
  public final List<CodicilCategoryHDto> getCodicilCategories() {
    List<CodicilCategoryHDto> categories = new ArrayList<>();

    final Call<List<CodicilCategoryHDto>> caller = assetReferenceDelegate.getCodicilCategories();
    try {
      categories = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Retrofit execution failed.", ioe);
    }
    categories.forEach(Resources::inject);

    return categories;
  }

  @ResourceProvider
  @Override
  public final CodicilCategoryHDto getCodicilCategory(final Long id) {
    CodicilCategoryHDto category = null;

    final List<CodicilCategoryHDto> codicilCategories = self.getCodicilCategories();
    try {
      category =
          codicilCategories.stream().filter(codicilCategory -> codicilCategory.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug("Found no element matching with id {}", id);
    }

    return category;
  }

  @ResourceProvider
  @Override
  public final List<FlagStateHDto> getFlagStates() {
    List<FlagStateHDto> dtos = new ArrayList<>();

    final Call<List<FlagStateHDto>> call = assetReferenceDelegate.getFlagState();
    try {
      dtos = call.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    dtos.forEach(Resources::inject);
    return dtos;

  }

  @ResourceProvider
  @Override
  public final FlagStateHDto getFlagState(final long flagState) {
    FlagStateHDto flagStateHDto = null;
    try {
      final List<FlagStateHDto> flagStateHDtos = self.getFlagStates();
      flagStateHDto = flagStateHDtos.stream().filter(type -> type.getId().equals(flagState)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, noElementFound);
    }
    return flagStateHDto;
  }


  @ResourceProvider
  @Override
  public final List<AssetLifeCycleStatusDto> getAssetLifecycleStatuses() {
    List<AssetLifeCycleStatusDto> dtos = new ArrayList<>();

    final Call<List<AssetLifeCycleStatusDto>> call = assetReferenceDelegate.getAssetLifecycleStatuses();
    try {
      dtos = call.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed for AssetLifeCycleStatus.", ioe);
    }

    return dtos;
  }

  @ResourceProvider
  @Override
  public final AssetLifeCycleStatusDto getAssetLifecycleStatus(final Long id) {
    AssetLifeCycleStatusDto assetLifeCycleStatusDto = null;
    try {
      final List<AssetLifeCycleStatusDto> assetLifeCycleStatusDtos = self.getAssetLifecycleStatuses();
      assetLifeCycleStatusDto =
          assetLifeCycleStatusDtos.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, noElementFound);
    }
    return assetLifeCycleStatusDto;
  }

  @Override
  @ResourceProvider
  public final List<BuilderHDto> getBuilders() {
    List<BuilderHDto> builderHDtos = new ArrayList<>();
    final Call<List<BuilderHDto>> builderCaller = assetReferenceDelegate.getBuilders();
    try {
      builderHDtos = builderCaller.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for Builders.", exception);
    }
    return builderHDtos;
  }

  @ResourceProvider
  @Override
  public final BuilderHDto getBuilderById(final Long id) {
    BuilderHDto builder = null;
    try {
      final List<BuilderHDto> builderHDtos = self.getBuilders();
      builder = builderHDtos.stream().filter(builderObj -> builderObj.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException exception) {
      LOGGER.debug(LOG_MESSAGE, exception);
    }
    return builder;
  }

  @Override
  @ResourceProvider
  public final List<DueStatusDto> getDueStatuses() {
    List<DueStatusDto> dueStatusDtos = new ArrayList<>();
    final Call<List<DueStatusDto>> dueStatus = assetReferenceDelegate.getDueStatuses();
    try {
      dueStatusDtos = dueStatus.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for Builders.", exception);
    }
    return dueStatusDtos;
  }

  @ResourceProvider
  @Override
  public final DueStatusDto getDueStatusesById(final Long id) {
    DueStatusDto dueStatus = null;
    try {
      final List<DueStatusDto> dueStatusDtos = self.getDueStatuses();
      dueStatus = dueStatusDtos.stream().filter(dueStatusObj -> dueStatusObj.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException exception) {
      LOGGER.debug(LOG_MESSAGE, exception);
    }
    return dueStatus;
  }

  @Override
  @ResourceProvider
  public final List<RepairActionHDto> getRepairActions() {
    List<RepairActionHDto> repairActionHDto = new ArrayList<>();
    final Call<List<RepairActionHDto>> repairActions = assetReferenceDelegate.getRepairActions();
    try {
      repairActionHDto = repairActions.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getRepairActions.", exception);
    }
    return repairActionHDto;
  }

  @ResourceProvider
  @Override
  public final RepairActionHDto getRepairAction(final Long id) {
    RepairActionHDto repairAction = null;

    final List<RepairActionHDto> repairActions = self.getRepairActions();
    final Optional<RepairActionHDto> repairActionOpt =
        repairActions.stream().filter(repairActionObj -> repairActionObj.getId().equals(id)).findFirst();

    if (repairActionOpt.isPresent()) {
      repairAction = repairActionOpt.get();
    }

    return repairAction;
  }

  @Override
  @ResourceProvider
  public final List<DefectStatusHDto> getDefectStatuses() {
    List<DefectStatusHDto> defectStatusHDto = new ArrayList<>();
    final Call<List<DefectStatusHDto>> defectStatuses = assetReferenceDelegate.getDefectStatuses();
    try {
      defectStatusHDto = defectStatuses.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getDefectStatuses.", exception);
    }
    return defectStatusHDto;
  }

  @ResourceProvider
  @Override
  public final DefectStatusHDto getDefectStatus(final Long id) {
    DefectStatusHDto defectStatus = null;

    final List<DefectStatusHDto> defectStatuses = self.getDefectStatuses();
    final Optional<DefectStatusHDto> defectStatusOpt =
        defectStatuses.stream().filter(defectStatusObj -> defectStatusObj.getId().equals(id)).findFirst();

    if (defectStatusOpt.isPresent()) {
      defectStatus = defectStatusOpt.get();
    }

    return defectStatus;
  }

  @Override
  @ResourceProvider
  public final List<CodicilStatusHDto> getCodicilStatuses() {
    List<CodicilStatusHDto> codicilStatusHDto = new ArrayList<>();
    final Call<List<CodicilStatusHDto>> defectStatuses = assetReferenceDelegate.getCodicilStatuses();
    try {
      codicilStatusHDto = defectStatuses.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getDefectStatuses.", exception);
    }
    return codicilStatusHDto;
  }

  @ResourceProvider
  @Override
  public final CodicilStatusHDto getCodicilStatus(final Long id) {
    CodicilStatusHDto codicilStatus = null;

    final List<CodicilStatusHDto> codicilStatuses = self.getCodicilStatuses();
    final Optional<CodicilStatusHDto> codicilStatusOpt =
        codicilStatuses.stream().filter(defectStatusObj -> defectStatusObj.getId().equals(id)).findFirst();

    if (codicilStatusOpt.isPresent()) {
      codicilStatus = codicilStatusOpt.get();
    }

    return codicilStatus;
  }

  @Override
  @ResourceProvider
  public final List<RepairTypeHDto> getRepairTypes() {
    List<RepairTypeHDto> repairTypeHDto = new ArrayList<>();
    final Call<List<RepairTypeHDto>> repairTypes = assetReferenceDelegate.getRepairTypes();
    try {
      repairTypeHDto = repairTypes.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getRepairActions.", exception);
    }
    return repairTypeHDto;
  }

  @ResourceProvider
  @Override
  public final RepairTypeHDto getRepairType(final Long id) {
    RepairTypeHDto repairType = null;

    final List<RepairTypeHDto> repairTypes = self.getRepairTypes();
    final Optional<RepairTypeHDto> repairTypeOpt =
        repairTypes.stream().filter(repairTypeObj -> repairTypeObj.getId().equals(id)).findFirst();

    if (repairTypeOpt.isPresent()) {
      repairType = repairTypeOpt.get();
    }

    return repairType;
  }

  @Override
  @ResourceProvider
  public final List<ClassificationSocietyHDto> getClassificationSocieties() {
    List<ClassificationSocietyHDto> classificationSocietyList = new ArrayList<>();
    final Call<List<ClassificationSocietyHDto>> classificationSocieties =
        assetReferenceDelegate.getClassificationSocieties();
    try {
      classificationSocietyList = classificationSocieties.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getClassificationSocieties.", exception);
    }
    return classificationSocietyList;
  }

  @ResourceProvider
  @Override
  public final ClassificationSocietyHDto getClassificationSociety(final Long id) {
    ClassificationSocietyHDto classificationSociety = null;

    final List<ClassificationSocietyHDto> classificationSocieties = self.getClassificationSocieties();
    final Optional<ClassificationSocietyHDto> classificationSocietyOpt = classificationSocieties.stream()
        .filter(classificationSocietyObj -> classificationSocietyObj.getId().equals(id)).findFirst();

    if (classificationSocietyOpt.isPresent()) {
      classificationSociety = classificationSocietyOpt.get();
    }

    return classificationSociety;
  }

  @Override
  @ResourceProvider
  public final List<ClassMaintenanceStatusHDto> getClassMaintenanceStatuses() {
    List<ClassMaintenanceStatusHDto> classMaintenanceStatusList = new ArrayList<>();
    final Call<List<ClassMaintenanceStatusHDto>> classMaintenanceStatuses =
        assetReferenceDelegate.getClassMaintenanceStatuses();
    try {
      classMaintenanceStatusList = classMaintenanceStatuses.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getClassMaintenanceStatuses.", exception);
    }
    return classMaintenanceStatusList;
  }

  @ResourceProvider
  @Override
  public final ClassMaintenanceStatusHDto getClassMaintenanceStatus(final Long id) {
    ClassMaintenanceStatusHDto classMaintenanceStatus = null;

    final List<ClassMaintenanceStatusHDto> classMaintenanceStatuses = self.getClassMaintenanceStatuses();
    final Optional<ClassMaintenanceStatusHDto> classMaintenanceStatusOpt = classMaintenanceStatuses.stream()
        .filter(classMaintenanceStatusObj -> classMaintenanceStatusObj.getId().equals(id)).findFirst();

    if (classMaintenanceStatusOpt.isPresent()) {
      classMaintenanceStatus = classMaintenanceStatusOpt.get();
    }

    return classMaintenanceStatus;
  }

  @Override
  @ResourceProvider
  public final List<ServiceCreditStatusRefHDto> getServiceCreditStatuses() {
    List<ServiceCreditStatusRefHDto> serviceCreditStatusList = new ArrayList<>();
    final Call<List<ServiceCreditStatusRefHDto>> serviceCreditStatuses =
        assetReferenceDelegate.getServiceCreditStatuses();
    try {
      serviceCreditStatusList = serviceCreditStatuses.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getServiceCreditStatuses.", exception);
    }
    return serviceCreditStatusList;
  }

  @ResourceProvider
  @Override
  public final ServiceCreditStatusRefHDto getServiceCreditStatus(final Long id) {
    ServiceCreditStatusRefHDto serviceCreditStatus = null;

    final List<ServiceCreditStatusRefHDto> serviceCreditStatuses = self.getServiceCreditStatuses();

    serviceCreditStatus = serviceCreditStatuses.stream()
        .filter(serviceCreditStatusObj -> serviceCreditStatusObj.getId().equals(id)).findFirst().orElse(null);

    return serviceCreditStatus;
  }

  @Override
  @ResourceProvider
  public final List<ActionTakenHDto> getActionTakenList() {
    List<ActionTakenHDto> actionTakenList = new ArrayList<>();
    final Call<List<ActionTakenHDto>> actionTaken = assetReferenceDelegate.getActionTakenList();
    try {
      actionTakenList = actionTaken.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getActionTaken.", exception);
    }
    return actionTakenList;
  }

  @ResourceProvider
  @Override
  public final ActionTakenHDto getActionTaken(final Long id) {
    ActionTakenHDto actionTaken = null;

    final List<ActionTakenHDto> actionTakenList = self.getActionTakenList();

    actionTaken =
        actionTakenList.stream().filter(actionTakenObj -> actionTakenObj.getId().equals(id)).findFirst().orElse(null);

    return actionTaken;
  }


  @Override
  @ResourceProvider
  public final List<PartyRoleHDto> getPartyRoles() {
    List<PartyRoleHDto> partyRoleList = new ArrayList<>();
    final Call<List<PartyRoleHDto>> partyRoles = assetReferenceDelegate.getPartyRoles();
    try {
      partyRoleList = partyRoles.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getPartyRoles.", exception);
    }
    return partyRoleList;
  }

  @ResourceProvider
  @Override
  public final PartyRoleHDto getPartyRole(final Long id) {
    PartyRoleHDto partyRole = null;

    final List<PartyRoleHDto> partyRoles = self.getPartyRoles();

    partyRole = partyRoles.stream().filter(partyRoleObj -> partyRoleObj.getId().equals(id)).findFirst().orElse(null);

    return partyRole;
  }

  @ResourceProvider
  @Override
  public final List<MajorNCNStatusHDto> getMncnStatusList() {
    List<MajorNCNStatusHDto> mncnList = new ArrayList<>();
    final Call<List<MajorNCNStatusHDto>> mncns = assetReferenceDelegate.getMajorNCNs();
    try {
      mncnList = mncns.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getMncnStatusList.", exception);
    }
    return mncnList;
  }

  @ResourceProvider
  @Override
  public final MajorNCNStatusHDto getMncnStatus(final Long id) {
    MajorNCNStatusHDto majorNcn = null;

    final List<MajorNCNStatusHDto> mncnList = self.getMncnStatusList();

    majorNcn = mncnList.stream().filter(mncnObj -> mncnObj.getId().equals(id)).findFirst().orElse(null);

    return majorNcn;
  }

  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   *
   */
  private AssetReferenceService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(AssetReferenceService.class);
  }

}
