package com.baesystems.ai.lr.cd.service.cache;


import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * Provides cache setup configuration.
 *
 * @author yng
 *
 */
public class CacheFactory {

  /**
   * Logger for {@link CacheFactory}.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CacheFactory.class);

  /**
   * The {@link RedisTemplate} from Spring context.
   */
  @Autowired
  private RedisTemplate redisTemplate;

  /**
   * The {@link EhCacheManagerFactoryBean} from Spring context.
   */
  @Autowired
  private EhCacheManagerFactoryBean ehCacheManagerFactoryBean;

  /**
   * The default expiration from Spring properties file.
   */
  @Value("${cache.redis.defaultexpiration}")
  private String defaultExpiration;

  /**
   * The assets expiration from Spring properties file.
   */
  @Value("${cache.redis.assets.expire}")
  private String assetsExpiration;

  /**
   * The assetsByUser expiration from Spring properties file.
   */
  @Value("${cache.redis.assetsbyuser.expire}")
  private String assetsByUserExpiration;

  /**
   * The assetsByUser expiration from Spring properties file.
   */
  @Value("${cache.redis.userprofile.expire}")
  private String userProfileExpiration;

  /**
   * The assetsExport cache expiration from Spring properties file.
   */
  @Value("${cache.redis.assetsexport.expire}")
  private String assetsExportExpiration;

  /**
   * The redis hostname from Spring properties file.
   */
  @Value("${cache.redis.hostname}")
  private String redisHostname;

  /**
   * The redis port from Spring properties file.
   */
  @Value("${cache.redis.port}")
  private String redisPort;


  /**
   * Creates cache manager. <br>
   * If Redis is connected successfully, the cache manager will setup as Redis cache manager,
   * otherwise EhCache cache manager.
   *
   * @return cacheManager object.
   */
  public final CacheManager createCacheManager() {
    LOGGER.info("Initializing cache manager ...");

    CacheManager cacheManager = null;
    // check if redis server is available
    try {
      final Jedis jedis = new Jedis(redisHostname, Integer.parseInt(redisPort));
      jedis.ping();
      if (jedis.isConnected()) {
        LOGGER.info("Using redis cache manager.");
        cacheManager = new RedisCacheManager(redisTemplate);

        final Map<String, Long> expires = new TreeMap<>();
        expires.put(CacheConstants.CACHE_ASSETS, Long.parseLong(assetsExpiration));
        expires.put(CacheConstants.CACHE_ASSETS_BY_USER, Long.parseLong(assetsByUserExpiration));
        expires.put(CacheConstants.CACHE_USERS, Long.parseLong(userProfileExpiration));
        expires.put(CacheConstants.CACHE_ASSETS_EXPORT, Long.parseLong(assetsExportExpiration));

        ((RedisCacheManager) cacheManager).setExpires(expires);
        ((RedisCacheManager) cacheManager).setDefaultExpiration(Long.parseLong(defaultExpiration));
      } else {
        LOGGER.info("Redis is not connected. Connection issue occured.");
        LOGGER.info("Revert back to use EhCache.");
        cacheManager = new EhCacheCacheManager(ehCacheManagerFactoryBean.getObject());
      }
    } catch (final JedisConnectionException exception) {
      LOGGER.info("Redis server is not available.");
      LOGGER.info("Using EhCache cache manager.");

      cacheManager = new EhCacheCacheManager(ehCacheManagerFactoryBean.getObject());

    }
    return cacheManager;
  }
}
