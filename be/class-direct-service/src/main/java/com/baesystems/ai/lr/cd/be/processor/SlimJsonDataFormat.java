package com.baesystems.ai.lr.cd.be.processor;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.monitorjbl.json.JsonView;
import org.apache.camel.Exchange;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Stream;

import static com.monitorjbl.json.Match.match;

/**
 * Class to filter JSON output based on http request Headers
 * Created by PFauchon on 18/11/2016.
 */
@SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.SignatureDeclareThrowsException"})
public class SlimJsonDataFormat extends JacksonDataFormat {
  /**
   * Logger for AssetServiceImpl class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(SlimJsonDataFormat.class);
  /**
   * Class used for pagination purpose as it requires special treatment before filtering.
   */
  private static final String PAGINATION_CLASS_NAME = "com.baesystems.ai.lr.dto.paging.PaginationDto";

  /**
   * name of the pagination field in the json.
   */
  private static final String PAGINATION_FIELD_PREFIX = "pagination.";

  /**
   * Name of the content field in the json.
   */
  private static final String CONTENT_FIELD_PREFIX = "content.";

  /**
   * Name of the header field for the list of fields.
   */
  private static final String FIELDS_HEADER = "Fields";

  /**
   * Default exclusion rule.
   */
  private static final String EXCLUSION_RULE = "*";

  /**
   * Separator in between fields in the header.
   */
  private static final String CSV_SEPARATOR = ",";

  /**
   * Method to transform into JSON.
   *
   * @param exchange the camel exchange object
   * @param graph    the object we want to transform
   * @param stream   the outgoing stream
   * @throws Exception if things goes wrong
   */
  @Override
  public final void marshal(final Exchange exchange, final Object graph, final OutputStream stream) throws Exception {


    //checking if we have headers
    final String fieldsHeader = exchange.getIn().getHeader(FIELDS_HEADER, String.class);

    if (!StringUtils.isEmpty(fieldsHeader) && graph != null) {
      LOGGER.debug("Field filtering required, Fields: {}", fieldsHeader);

      Class clazz = null;

      //will be CSV
      String[] fields = fieldsHeader.split(CSV_SEPARATOR);

      //String cleaning
      for (int i = 0; i < fields.length; i++) {
        fields[i] = new StringBuffer(fields[i]).toString().trim();
      }

      //if we're having a paginated resource, we need to manually add the fields manually and preprend the fields with
      // content
      if (graph instanceof PageResource) {
        clazz = graph.getClass();
        LOGGER.debug("Paginated resource, need to keep pagination and change required fields based on content field");


        //preprend all required fields with content. as it's inside the pagination dto
        for (int i = 0; i < fields.length; i++) {
          fields[i] = new StringBuffer(CONTENT_FIELD_PREFIX).append(fields[i]).toString();
        }

        //Getting all the fields from the PaginationDTO, using reflection instead of hardcoding in case DTO changes.
        final Class paginationClass = Class.forName(PAGINATION_CLASS_NAME);
        final String[] paginationFields = Stream.of(paginationClass.getDeclaredFields())
            .map(field -> new StringBuffer(PAGINATION_FIELD_PREFIX).append(field.getName()).toString())
            .toArray(size -> new String[size]);

        fields = ArrayUtils.addAll(fields, paginationFields);

      } else if (graph instanceof List) {
        //If we have a list we need to get the class of the items inside it.
        final List list = (List) graph;
        if (!list.isEmpty()) {
          clazz = list.get(0).getClass();
        }

      } else {
        clazz = graph.getClass();
      }


      if (clazz != null) {
        //Exclude everything, keep what we need
        final String json = getObjectMapper().writeValueAsString(JsonView.with(graph)
            .onClass(clazz, match()
                .exclude(EXCLUSION_RULE)
                .include(fields)
            ));

        //writing the output
        stream.write(json.getBytes(StandardCharsets.UTF_8));

      } else {
        super.marshal(exchange, graph, stream);
      }
    } else {
      //use default behavior
      super.marshal(exchange, graph, stream);
    }

  }

  /**
   * Init method.
   *
   * @throws Exception if things goe haywire
   */
  @Override
  protected final void doStart() throws Exception {
    super.doStart();

    getObjectMapper().setDateFormat(new ISO8601DateFormat());
  }
}
