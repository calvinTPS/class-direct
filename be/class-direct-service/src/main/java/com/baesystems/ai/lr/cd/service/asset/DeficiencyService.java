package com.baesystems.ai.lr.cd.service.asset;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyQueryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides deficiency service to get paginated deficiency list, list of deficiencies and single
 * deficiency associated rectifications and statutory findings.
 *
 * @author syalavarthi.
 *
 */
public interface DeficiencyService {

  /**
   * Gets paginated deficiencies list for asset with query.
   *
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @param assetId the asset unique identifier.
   * @param query the query body with search parameters.
   * @return deficiencies list.
   * @throws ClassDirectException if mast api call fail or any error in execution flow.
   */
  PageResource<DeficiencyHDto> getDeficienciesPageResource(final Integer page, final Integer size, final Long assetId,
      final DeficiencyQueryHDto query) throws ClassDirectException;

  /**
   * Gets deficiencies list for asset with query.
   *
   * @param assetId the asset unique identifier.
   * @param query the query body with search parameters.
   * @return deficiencies list.
   * @throws ClassDirectException if mast api call fail or any error in execution flow.
   */
  List<DeficiencyHDto> getDeficiencies(final Long assetId, final DeficiencyQueryHDto query) throws ClassDirectException;

  /**
   * Gets deficiency associated rectifications and statutory findings by unique identifier deficiency id.
   *
   * @param assetId the asset unique identifier.
   * @param deficiencyId the deficiency unique identifier.
   * @return defect object.
   * @throws ClassDirectException if mast api call fail.
   */
  DeficiencyHDto getDeficiencyByAssetIdDeficiencyId(Long assetId, long deficiencyId) throws ClassDirectException;
}
