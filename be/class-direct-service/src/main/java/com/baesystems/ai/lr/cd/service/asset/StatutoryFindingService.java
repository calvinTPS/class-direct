package com.baesystems.ai.lr.cd.service.asset;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides the base model interface for the Statutory Finding Service. Gets paginated statutory
 * findings, statutory findings list and get single statutory finding.
 *
 * @author syalavarthi.
 *
 */
public interface StatutoryFindingService extends DueStatusCalculator<StatutoryFindingHDto> {
  /**
   * Gets paginated Statutory findings for asset with different search parameters.
   *
   * @param page the page number.
   * @param size the page size.
   * @param assetId the primary key of the asset.
   * @param query the search criteria to get statutory findings.
   * @return statutory findings page resource.
   * @throws ClassDirectException if there is error mast API to get statutory findings.
   */
  PageResource<StatutoryFindingHDto> getStatutorysPageResource(final Integer page, final Integer size,
      final Long assetId, final CodicilDefectQueryHDto query) throws ClassDirectException;

  /**
   * Gets Statutory findings list by asset id and query with different search parameters.
   *
   * @param assetId the primary key of the asset.
   * @param query the search criteria to get statutory findings.
   * @return statutory findings list.
   * @throws ClassDirectException if there is error mast API to get statutory findings.
   */
  List<StatutoryFindingHDto> getStatutories(final Long assetId, final CodicilDefectQueryHDto query)
      throws ClassDirectException;

  /**
   * Gets single StatutoryFinding.
   *
   * @param assetId the primary key of the asset.
   * @param deficiencyId the primary key of the deficiency.
   * @param statutoryFindingId the primary key of the statutoryFinding.
   * @return the single statutory finding.
   * @throws ClassDirectException if there is error mast API to get statutory finding.
   */
  StatutoryFindingHDto getStatutoryFindingById(final Long assetId, final Long deficiencyId,
      final Long statutoryFindingId) throws ClassDirectException;

  /**
   * Gets Statutory findings list by asset id and query with due datemax (1 year + 7 days) and min date(past 7 days)
   *  with statutories 23(Open) statuses for due notifications.
   *
   * @param assetId the primary key of the asset.
   * @param query the search criteria to get statutory findings.
   * @return statutory findings list.
   * @throws ClassDirectException if there is error mast API to get statutory findings.
   */
  List<StatutoryFindingHDto> getStatutoriesForDueNotification(final Long assetId, final CodicilDefectQueryHDto query)
      throws ClassDirectException;
}
