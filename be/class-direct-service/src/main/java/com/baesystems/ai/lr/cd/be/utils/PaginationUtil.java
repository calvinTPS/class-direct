package com.baesystems.ai.lr.cd.be.utils;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.domain.dto.user.PaginationValidationDto;
import com.baesystems.ai.lr.dto.paging.BasePageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;

/**
 * Helper class for pagination.
 *
 * @author msidek
 *
 */
public final class PaginationUtil {

  /**
   * Hidden constructor.
   *
   */
  private PaginationUtil() {
    super();
  }

  /**
   * Logger object.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(PaginationUtil.class);

  /**
   * Default size per result.
   *
   */
  public static final Integer DEFAULT_SIZE_PER_PAGE = Integer.MAX_VALUE;

  /**
   * Default starting page is set to 1.
   *
   */
  public static final Integer DEFAULT_STARTING_PAGE = 1;

  /**
   * Create pagination with specified number of elements and page results.
   *
   * @param pageResults list of results.
   * @param page page number.
   * @param <T> type of DTO.
   * @param size size of result per page.
   * @param totalElements totalElements.
   * @return paged result.
   *
   */
  public static <T> BasePageResource<T> paginate(final List<T> pageResults, final Integer page, final Integer size,
      final Integer totalElements) {
    PaginationValidationDto pageInfo = validatePageAndSize(page, size);
    Integer inputSize = pageInfo.getSize();
    Integer inputPage = pageInfo.getPage();
    final Integer resultSize = inputSize;
    Integer resultPage = inputPage;
    resultPage = resultPage - 1;
    Integer totalPages = 0;
    if ((totalElements / resultSize) > 0) {
      totalPages = totalElements / resultSize;
      if ((totalElements % resultSize) > 0) {
        totalPages = totalPages + 1;
      }
    }

    final PaginationDto pagination = new PaginationDto();
    pagination.setSize(inputSize);
    pagination.setNumber(inputPage);
    if (resultPage == 0) {
      if (inputPage.equals(totalPages)) {
        pagination.setFirst(true);
        pagination.setLast(true);
      } else {
        pagination.setFirst(true);
        pagination.setLast(false);
      }
    } else if (inputPage.equals(totalPages)) {
      pagination.setFirst(false);
      pagination.setLast(true);
    } else {
      pagination.setFirst(false);
      pagination.setLast(false);
    }
    pagination.setTotalPages(totalPages);
    pagination.setTotalElements(Long.valueOf(totalElements));

    final BasePageResource<T> pageResource = new BasePageResource<T>() {
      private List<T> content;

      @Override
      public List<T> getContent() {
        return content;
      }

      @Override
      public void setContent(final List<T> list) {
        this.content = list;
      }
    };

    pagination.setNumberOfElements(pageResults.size());

    pageResource.setContent(pageResults);
    pageResource.setPagination(pagination);

    return pageResource;
  }

  /**
   * Create pagination with specified page and size.
   *
   * @param results list of results.
   * @param page page number.
   * @param <T> type of DTO.
   * @param size size of result per page.
   * @return paged result.
   *
   */
  public static <T> BasePageResource<T> paginate(final List<T> results, final Integer page, final Integer size) {
    PaginationValidationDto pageInfo = validatePageAndSize(page, size);

    Integer resultSize = pageInfo.getSize();
    final Integer resultPage = pageInfo.getPage();

    Integer totalElements = results.size();
    final Integer skip = resultSize * (resultPage - 1);
    LOGGER.debug("Collect by skipping {} with {}", skip, resultSize);
    final List<T> pageResult = results.stream().skip(skip).limit(resultSize).collect(Collectors.toList());
    return paginate(pageResult, resultPage, resultSize, totalElements);
  }

  /**
   * validation for page and size.
   *
   * @param page page.
   * @param size size.
   * @return value.
   */
  public static PaginationValidationDto validatePageAndSize(final Integer page, final Integer size) {
    Integer inputPage = null;
    Integer inputSize = null;
    PaginationValidationDto pagination = new PaginationValidationDto();

    if (size == null) {
      inputSize = DEFAULT_SIZE_PER_PAGE;
      inputPage = DEFAULT_STARTING_PAGE;
    } else {
      inputSize = size;
      inputPage = page;
    }

    if (page == null) {
      inputPage = DEFAULT_STARTING_PAGE;
    } else {
      inputPage = page;
    }

    if (inputPage <= 0) {
      throw new IllegalArgumentException("Page must not be smaller than one.");
    }

    if (inputSize <= 0) {
      throw new IllegalArgumentException("Size must not be smaller than one.");
    }
    pagination.setPage(inputPage);
    pagination.setSize(inputSize);
    return pagination;
  }
}
