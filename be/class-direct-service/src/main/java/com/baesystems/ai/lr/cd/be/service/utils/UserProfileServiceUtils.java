package com.baesystems.ai.lr.cd.be.service.utils;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides utility for user export service.
 *
 * @author syalavarthi.
 */
public final class UserProfileServiceUtils {

  /**
   * Returns UserProfileServiceUtils.
   */
  private UserProfileServiceUtils() {

  }

  /**
   * The logger for {@link UserProfileServiceUtils}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileServiceUtils.class);
  /**
   * The {@value #NEW_LINE_SEPARATOR} Delimiter used in CSV file for new line.
   */
  public static final String NEW_LINE_SEPARATOR = "\n";
  /**
   * The {@value #EMPTY_DELIMITER} Delimiter used in CSV file for empty space.
   */
  public static final Object EMPTY_DELIMITER = "";
  /**
   * The {@value #COMMA_DELIMITER} Delimiter used in CSV file.
   */
  public static final Object COMMA_DELIMITER = ",";
  /**
   * The {@value UserProfileServiceUtils#OR_DELIMITER} delimiter is used to query azure api format
   * the email's.
   */
  public static final String OR_DELIMITER = " or ";
  /**
   * The {@value UserProfileServiceUtils#ENCODING} is used in csv generation to encode content.
   */
  public static final String ENCODING = "UTF-8";
  /**
   * The {@value #SSO_FIELDS} Single-Signed On fields in User profile object.
   */
  private static final String[] SSO_FIELDS = new String[] {"ssoUser", "cdUser", "firstName", "lastName", "companyName",
      "name", "companyPosition", "company", "internalUser"};

  /**
   * Compares and sorts given objects.
   */
  private static Comparator<Object> fieldComparator = new Comparator<Object>() {
    @Override
    public int compare(final Object object1, final Object object2) {
      if (object1 == null && object2 == null) {
        return 0;
      } else if (object1 == null) {
        return -1;
      } else if (object2 == null) {
        return 1;
      } else {
        return ((Comparable) object1).compareTo(object2);
      }
    }
  };

  /**
   * Compares each field in fields of old object against new object to check if there is any
   * changes.
   *
   * @param fields list of fields to be compared.
   * @param oldObject the old object to be compared.
   * @param newObject the new object to be compared.
   * @return true if any of the fields varies in old and new object.
   */
  public static boolean isAnyFieldChanged(final String[] fields, final Object oldObject, final Object newObject) {
    return Stream.of(fields).map(field -> new BeanComparator(field, fieldComparator))
        .filter(bc -> bc.compare(oldObject, newObject) != 0).findAny().isPresent();
  }

  /**
   * Updates signed-in user information from cache into the latest user object.
   *
   * @param latestUser the new user profile object fetched from database.
   * @param cachedUser the old user profile object fetched from database.
   */
  public static void updateSignedInUserInformationFromCacheAndMast(final UserProfiles latestUser,
      final UserProfiles cachedUser) {
    // Update Signed-in User information and Company Name from Cache if there's any change in given
    // fields.
    Stream.of(SSO_FIELDS).map(field -> new BeanComparator(field, fieldComparator))
        .filter(bc -> bc.compare(cachedUser, latestUser) != 0).forEach(beanCompare -> {
          String propertyName = beanCompare.getProperty();
          try {
            Object propertyValue = PropertyUtils.getProperty(cachedUser, propertyName);
            PropertyUtils.setProperty(latestUser, propertyName, propertyValue);
          } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception) {
            LOGGER.error("Error occurred during copying property : {}", propertyName, exception);
          }
        });
  }

  /**
   * Formats email's to send azure.
   * <p>
   * formats each email using the string 'alternativeSignInNamesInfo/any(x:x/value eq '%email')' and
   * separates each email with string 'or'.
   * </p>
   *
   * @param emailIds the email id's to be formatted.
   * @param emailFilter the email filter.
   * @return formatted email string.
   */
  public static String getFormattedEmail(final List<String> emailIds, final String emailFilter) {
    String formattedEmails =
        emailIds.stream().map(email -> String.format(emailFilter, email)).collect(Collectors.joining(OR_DELIMITER));
    return formattedEmails;
  }

  /**
   * Writes contents to file.
   *
   * @param users the list users to display in report.
   * @param fileWriter the writer writes content to file.
   * @param fileHeader the csv fileHeader.
   * @param legalArtifact the text in csv.
   * @throws ClassDirectException if error in generating csv.
   */
  public static void generateCsv(final List<UserProfiles> users, final Writer fileWriter, final String[] fileHeader,
      final String legalArtifact) throws ClassDirectException {
    CSVPrinter csvFilePrinter = null;
    try {
      // Create the CSVFormat object with "\n" as a record delimiter
      CSVFormat csvFileFormat = CSVFormat.RFC4180.withFirstRecordAsHeader().withIgnoreEmptyLines(true).withTrim();
      // initialize CSVPrinter object
      csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
      csvFilePrinter.printRecord(fileHeader);

      for (UserProfiles user : users) {
        List<Object> userDataRecord = new ArrayList<Object>();

        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getUserId()));
        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getFirstName()));
        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getLastName()));
        // last login
        if (user.getLastLogin() != null) {
          userDataRecord.add(user.getLastLogin().format(ISO_LOCAL_DATE_TIME));
        } else {
          userDataRecord.add(EMPTY_DELIMITER);
        }
        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getClientCode()));
        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getShipBuilderCode()));
        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getFlagCode()));
        // client name
        if (user.getCompany() != null && user.getCompany().getName() != null) {
          userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getCompany().getName()));
        } else {
          userDataRecord.add(EMPTY_DELIMITER);
        }
        // user roles
        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
          String roles = user.getRoles().stream().map(role -> {
            return CsvUtils.sanitiseFirstCharacter(role.getRoleName());
          }).collect(Collectors.joining((CharSequence) COMMA_DELIMITER));
          userDataRecord.add(roles);
        } else {
          userDataRecord.add(EMPTY_DELIMITER);
        }
        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getEmail()));
        // Date of modification
        if (user.getDateOfModification() != null) {
          userDataRecord.add(user.getDateOfModification().format(ISO_LOCAL_DATE_TIME));
        } else {
          userDataRecord.add(EMPTY_DELIMITER);
        }
        userDataRecord.add(CsvUtils.sanitiseFirstCharacter(user.getStatus().getName()));
        csvFilePrinter.printRecord(userDataRecord);

      }
      csvFilePrinter.printRecord(legalArtifact);
      LOGGER.debug("CSV file was created successfully.");
    } catch (final IOException exception) {
      LOGGER.error("Error in CsvFileWriter " + exception);
      throw new ClassDirectException(exception);
    } finally {
      try {
        fileWriter.flush();
        fileWriter.close();
        if (csvFilePrinter != null) {
          csvFilePrinter.close();
        }
      } catch (IOException exception) {
        LOGGER.error("Error while flushing/closing fileWriter " + exception);
      }
    }
  }

  /**
   * Writes contents to file.
   *
   * @param users the users list to display in report.
   * @param file the csv file.
   * @param fileHeader the csv file fileHeader.
   * @param legalArtifact the text in csv.
   * @return csv file.
   * @throws ClassDirectException if API call from MAST, or PDF template compilation fail.
   * @throws IOException if error generating csv.
   */
  public static File generateCSV(final List<UserProfiles> users, final File file, final String[] fileHeader,
      final String legalArtifact) throws IOException, ClassDirectException {
    OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file), ENCODING);
    UserProfileServiceUtils.generateCsv(users, fileWriter, fileHeader, legalArtifact);
    return file;
  }

  /**
   * Retrieves client type for given User Profile.
   *
   * @param matchRoles desired roles.
   * @param user the user object of given user.
   * @return client type.
   */
  public static Optional<Role> getMatchedRoles(final Role[] matchRoles, final UserProfiles user) {
    return Optional.ofNullable(user).map(UserProfiles::getRoles).flatMap(roles -> roles.stream().map(ur -> {
      for (Role role : matchRoles) {
        if (ur.getRoleName().equals(role.toString())) {
          return role;
        }
      }
      return null;
    }).filter(Objects::nonNull).findFirst());
  }

  /**
   * Checks if user is client.
   *
   * @param user user profile object.
   * @return true if user is client user.
   */
  public static Boolean isClientUser(final UserProfiles user) {
    return user.getClientCode() != null;
  }

  /**
   * Checks if user is flag user.
   *
   * @param user user profile.
   * @return true if user is flag user.
   */
  public static Boolean isFlagUser(final UserProfiles user) {
    return user.getFlagCode() != null;
  }

  /**
   * Checks if user is builder.
   *
   * @param user user profile.
   * @return true if ship builder user.
   */
  public static Boolean isBuilderUser(final UserProfiles user) {
    return user.getShipBuilderCode() != null;
  }
}
