package com.baesystems.ai.lr.cd.service.certificate.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dao.certificate.CertificateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionPageResourceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificatePageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateStatusDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.certificate.CertificateReferenceService;
import com.baesystems.ai.lr.cd.service.certificate.CertificateService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.enums.ProductType;
import com.jcabi.aspects.Loggable;

import retrofit2.Call;

/**
 * Provides methods to get certificates, certificate attachments, inject endorsed date to
 * certificates and filter statutory certificates.
 *
 * @author fwijaya on 20/1/2017.
 * @author sbollu
 *
 */
@Service(value = "CertificateService")
@Loggable(Loggable.DEBUG)
public class CertificateServiceImpl implements CertificateService {

  /**
   * The Constant for "Endorsed" action taken certificate.
   */
  public static final String ENDORSED_ACTION_TAKEN = "Endorsed";
  /**
   * The Logger object to display logs for {@link CertificateServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CertificateServiceImpl.class);
  /**
   * The Default initial size used in default pagination.
   */
  private static final int INITIAL_SIZE = 0;
  /**
   * The {@link CertificateRetrofitService} from Spring context.
   */
  @Autowired
  private CertificateRetrofitService certService;

  /**
   * The {@link CertificateReferenceService} from Spring context.
   */
  @Autowired
  private CertificateReferenceService certRefService;


  /**
   * Returns paginated list of certificates for given asset.
   *
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @param order the sorting order eg: ascending or descending.
   * @param sort the sort field.
   * @param query the query body.
   * @return paginated list of certificates.
   * @throws ClassDirectException if execution or API call fails..
   */
  @Override
  public final CertificatePageResource getCertificateQueryPageResource(final Integer page, final Integer size,
      final String sort, final String order, final CertificateQueryHDto query) throws ClassDirectException {
    CertificatePageResource certificates = getDefaultPaginatedResource();

    boolean equasisThetisUser = SecurityUtils.isEquasisThetisUser();
    try {
      if (query != null) {
        List<Long> validStatus = certRefService.getCertificateStatuses().parallelStream()
            .filter(status -> status != null && !status.isDeleted()).map(CertificateStatusDto::getId)
            .collect(Collectors.toList());
        query.setStatus(validStatus);
        final Call<CertificatePageResource> call;
        if (equasisThetisUser) {
          // for equisis/thesis
          call = certService.certificateQuery(query, null, null, sort, order);
        } else {
          call = certService.certificateQuery(query, page, size, sort, order);
        }
        PageResource<CertificateHDto> resource = call.execute().body();

        final Optional<List<CertificateHDto>> certListOpt =
            Optional.ofNullable(resource).map(PageResource<CertificateHDto>::getContent);


        if (certListOpt.isPresent()) {
          final List<Callable<Void>> tasks = certListOpt.get().parallelStream().map(certDto -> {
            Callable<Void> updateTask = () -> {
              try {
                Resources.inject(certDto);
                Optional.ofNullable(certDto.getEmployee()).map(LinkResource::getId).ifPresent(id -> {
                  LrEmployeeDto employee = certRefService.getSurveyor(id);
                  certDto.setSurveyor(employee);
                });

                injectEndorsedDate(certDto);

                // injecting the certificate if needed
                Optional.ofNullable(certDto.getCertificateTemplateHDto()).ifPresent(Resources::inject);

              } catch (final Exception e) {
                LOGGER.error("Error enriching certificate : " + certDto.getId(), e);
              }
              return null;
            };
            return updateTask;
          }).collect(Collectors.toList());
          final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
          try {
            executor.invokeAll(tasks);
          } catch (InterruptedException e) {
            LOGGER.error("Exception when executing certificate enrichment: ", e);
            throw new ClassDirectException(e);
          } finally {
            executor.shutdown();
          }
          if (equasisThetisUser) {
            resource = PaginationUtil.paginate(filterStatutory(resource.getContent()), page, size);
          }
          certificates.setContent(resource.getContent());
          certificates.setPagination(resource.getPagination());
        }
      }
    } catch (IOException e) {
      LOGGER.error("Exception when executing delegate: ", e);
      throw new ClassDirectException(e);
    }
    return certificates;
  }

  /**
   * Injects endorsed date field to CertificateHDto.
   *
   * @param certificateHDto the certificateHDto object.
   */
  private void injectEndorsedDate(final CertificateHDto certificateHDto) {
    final Long certificateId = certificateHDto.getId();
    final Call<CertificateActionPageResourceHDto> call = certService.certificateActionQuery(certificateId);
    try {
      final CertificateActionPageResourceHDto actionPageResourceDto = call.execute().body();
      Optional.ofNullable(actionPageResourceDto).map(CertificateActionPageResourceHDto::getContent).get()
          .parallelStream().filter(dto -> {
            Resources.inject(dto);
            return dto.getActionTakenDto() != null && dto.getActionTakenDto().getName().equals(ENDORSED_ACTION_TAKEN)
                && dto.getActionDate() != null;
          }).map(CertificateActionHDto::getActionDate).max(Date::compareTo).ifPresent(endorsedDate -> {
            certificateHDto.setEndorsedDate(endorsedDate);
          });
    } catch (final IOException e) {
      LOGGER.error("Exception when getting actions for certificate: " + e);
    } catch (final NoSuchElementException nsee) {
      LOGGER.warn("No actions found for certificate: {}", certificateId);
    }
  }

  /**
   * Filters only statutory certificates for equisis/thesis user.
   *
   * @param certificates the list of certificates to be filtered.
   * @return list of filtered statutory certificates.
   */
  private List<CertificateHDto> filterStatutory(final List<CertificateHDto> certificates) {
    List<CertificateHDto> filteredList = certificates.parallelStream()
        .filter(certDto -> certDto.getCertificateTemplateHDto() != null
            && certDto.getCertificateTemplateHDto().getProductCatalogue() != null
            && certDto.getCertificateTemplateHDto().getProductCatalogueH().getProductType().getId()
                .equals(ProductType.STATUTORY.getValue()))
        .collect(Collectors.toList());

    return filteredList;
  }

  /**
   * Gets default paginated resource.
   *
   * @return {@link @CertificatePageResource}.
   */
  private CertificatePageResource getDefaultPaginatedResource() {
    final CertificatePageResource pageResource = new CertificatePageResource();
    pageResource.setContent(Collections.emptyList());
    final PaginationDto pagination = new PaginationDto();
    pagination.setSize(INITIAL_SIZE);
    pagination.setNumber(INITIAL_SIZE);
    pagination.setFirst(false);
    pagination.setLast(false);
    pagination.setTotalPages(INITIAL_SIZE);
    pagination.setTotalElements(0L);
    pagination.setNumberOfElements(INITIAL_SIZE);
    pageResource.setPagination(pagination);
    return pageResource;
  }

  @Override
  public final SupplementaryInformationPageResourceDto getCertificateAttachment(@NotNull final Long jobId,
      @NotNull final Long certId) throws ClassDirectException {
    SupplementaryInformationPageResourceDto certAttachDto = null;

    try {
      certAttachDto = certService.certificateAttachmentQuery(jobId, certId).execute().body();
    } catch (IOException exp) {
      LOGGER.error("Exception when getting attachments for certificate: " + exp);
      throw new ClassDirectException(exp);
    }

    return certAttachDto;
  }

}
