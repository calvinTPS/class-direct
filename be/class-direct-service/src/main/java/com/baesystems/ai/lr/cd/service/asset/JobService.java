package com.baesystems.ai.lr.cd.service.asset;


import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.AttachmentsCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobWithFlagsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides jobs information like reports associated with job(FAR, FSR, TM and ESP), attachments for
 * job and visibility of jobs based on user role.
 *
 * @author yng
 * @author syalavarthi.
 *
 */
public interface JobService {
  /**
   * Returns survey reports resource for an asset.
   *
   * @param page the total pages in result.
   * @param size result size.
   * @param assetId assetId from MAST.
   * @return the survey reports list.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should always success.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS,THETIS')")
  PageResource<ReportHDto> getSurveyReports(final Integer page, final Integer size, final Long assetId)
      throws ClassDirectException;


  /**
   * Returns List of Jobs from Mast for an asset.
   *
   * @param assetId assetId.
   * @param isSurveyReports the flag to identity api is calling from survey reports.
   * @return the List of Jobs with same assetId.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should always success.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS,THETIS')")
  List<JobCDDto> getJobsByAssetIdCall(final Long assetId, final Boolean isSurveyReports) throws ClassDirectException;

  /**
   * Returns Job from Mast.
   *
   * @param jobId jobId.
   * @return the job as result.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should always success.
   */
  JobCDDto getJob(Long jobId) throws ClassDirectException;

  /**
   * Returns completed jobs by assetId which lies in the given range from mast.
   *
   * @param userId userId.
   * @param fromDate fromDate.
   * @param toDate toDate.
   * @return the List of completed jobs from mast.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should always success.
   */
  List<JobWithFlagsHDto> getCompletedJobs(final String userId, final Date fromDate, final Date toDate)
      throws ClassDirectException;

  /**
   * Returns list of reports for a job.
   *
   * @param jobId jobId.
   * @return the list of reports as result.
   * @throws IOException if there is error from MAST API call otherwise should always success.
   */
  List<ReportCDDto> getReports(final Long jobId) throws IOException;

  /**
   * Returns list of attachments for a job.
   *
   * @param jobId jobId.
   * @return the attachments.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should always success.
   */
  AttachmentsCDDto getAttachments(final Long jobId) throws ClassDirectException;

  /**
   * Validates jobs list based on user role and filter jobs.
   *
   * @param userId the requested user's unique value.
   * @param assetId the asset unique value.
   * @param jobsList the list of jobs to check with users accessibility.
   * @return the list of jobs valid for requested user.
   * @throws ClassDirectException if ther is any error in get user or ger asset or check for mms service apis.
   */
  List<JobWithFlagsHDto> filterJobsBasedOnUserRole(final String userId, final Long assetId,
      final List<JobWithFlagsHDto> jobsList) throws ClassDirectException;

}
