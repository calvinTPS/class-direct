package com.baesystems.ai.lr.cd.service.cache.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.service.cache.CacheService;
import com.jcabi.aspects.Loggable;

import redis.clients.jedis.Jedis;

/**
 * Provides the cache flushing in service level.
 * <br>
 * <b>This is only created for tester to clear the cache during development purpose. This method
 * must be disabled or deleted in production.</b>
 *
 * @author yng
 *
 */
@Service(value = "CacheService")
@Loggable(Loggable.DEBUG)
public class CacheServiceImpl implements CacheService {

  /**
   * The redis host name from Spring properties file.
   */
  @Value("${cache.redis.hostname}")
  private String redisHostname;

  /**
   * The redis port from Spring properties file.
   */
  @Value("${cache.redis.port}")
  private String redisPort;

  /**
   * Provides cache flush function.
   */
  @Override
  public final StringResponse flushCache() {
    final Jedis jedis = new Jedis(redisHostname, Integer.parseInt(redisPort));

    jedis.ping();

    String message;

    if (jedis.isConnected()) {
      final String status = jedis.flushAll();
      message = "Flush successful: " + status;
    } else {
      message = "Unable to connect to redis server.";
    }

    jedis.close();
    return new StringResponse(message);
  }


}
