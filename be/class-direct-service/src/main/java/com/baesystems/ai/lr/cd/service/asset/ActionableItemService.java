package com.baesystems.ai.lr.cd.service.asset;

import java.util.List;
import java.util.function.Predicate;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides the interface for actionable item service.
 *
 * @author yng
 *
 */
public interface ActionableItemService extends DueStatusCalculator<ActionableItemHDto> {
  /**
   * Returns the paginated list of actionable item.
   *
   * @param page the page number.
   * @param size the max amount of data to be display per page.
   * @param assetId the asset Id.
   * @return the paginated list of actionable item.
   * @throws ClassDirectException if API fail call.
   */
  PageResource<ActionableItemHDto> getActionableItem(final Integer page, final Integer size, final Long assetId)
      throws ClassDirectException;

  /**
   * Returns list of actionable items of an asset.
   *
   * @param assetId the asset model object.
   * @param filter the predicate applicable for an equasis thesis user.
   * @return the list of actionable items.
   * @throws ClassDirectException if mast api call fail.
   */
  List<ActionableItemHDto> getActionableItems(final long assetId,
      final Predicate<ActionableItemHDto> filter) throws ClassDirectException;

  /**
   * Returns single actionable item.
   *
   * @param assetId the asset Id.
   * @param actionableItemId the actionable item Id.
   * @return the actionable item object.
   * @throws ClassDirectException if API call fail.
   */
  ActionableItemHDto getActionableItemByAssetIdActionableItemId(final Long assetId, final long actionableItemId)
      throws ClassDirectException;

  /**
   * Returns list of actionable items of an asset by query with due date min(past 7 days) and max(1 year+7 days) and
   * with actionable item statuses 3(Open), 4(Change Recommended) and 6(Continues Satisfactory) for asset due status
   *  notifications.
   *
   * @param assetId the asset model object.
   * @param query the input query parameter to fileter actionable items.
   * @return the list of actionable items.
   * @throws ClassDirectException if mast api call fail.
   */
  List<ActionableItemHDto> getActionableItemsByQuery(final long assetId, final CodicilDefectQueryHDto query)
      throws ClassDirectException;
}
