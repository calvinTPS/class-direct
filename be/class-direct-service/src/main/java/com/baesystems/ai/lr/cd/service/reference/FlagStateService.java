package com.baesystems.ai.lr.cd.service.reference;

import java.io.IOException;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides service to get flag details by id, user flag details and list of all flag details.
 *
 * @author msidek
 * @author syalavarthi 23-05-2017.
 *
 */
public interface FlagStateService {

  /**
   * Gets flag by id.
   *
   * @param id the primary key of flag.
   * @return flag details.
   * @throws ClassDirectException if mast api call fails.
   *
   */
  FlagStateHDto getFlagById(Long id) throws ClassDirectException;

  /**
   * Gets user flags.
   *
   * <p>
   * user flags fetch based on user role.
   * </p>
   *
   * <ol>
   * <li>1.ADMIN_ROLE</li>
   * <p>
   * requested user with any of the roles LR_ADMIN, LR_INTERNAL and EXTERNAL_NON_CLIENT <br>
   * can access all flags (all reference data flags).
   * </p>
   * <li>2.FLAG_ROLE</li>
   * <p>
   * can access only flag associated with user (flag code of user).
   * </p>
   * <li>3.OTHER_ROLES</li>
   * <p>
   * can access flags associated with user assets (each asset have flag associated with it).
   * </p>
   * </ol>
   *
   * @param userId the primary key of user.
   * @return flags.
   * @throws IOException if error fetching assets associated with user.
   * @throws ClassDirectException if error when get user or get flag reference data or any error
   *         occur in the flow.
   */
  List<FlagStateHDto> getUserFlagStates(final String userId) throws IOException, ClassDirectException;

  /**
   * Gets all flags from MAST.
   *
   * @return flags.
   * @throws ClassDirectException if mast api fails.
   */
  List<FlagStateHDto> getFlagStates() throws ClassDirectException;
}
