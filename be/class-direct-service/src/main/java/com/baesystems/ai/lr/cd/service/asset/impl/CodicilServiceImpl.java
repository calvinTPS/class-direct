package com.baesystems.ai.lr.cd.service.asset.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilStatusesHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.CodicilService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.dto.codicils.CodicilDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.jcabi.aspects.Loggable;

/**
 * Provides local service to get list of codicil associated with asset.
 *
 * @author yng
 * @author sbollu
 *
 */
@Service(value = "CodicilService")
@Loggable(Loggable.DEBUG)
public class CodicilServiceImpl implements CodicilService {
  /**
   * The logger to display logs for {@link CodicilService}.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CodicilServiceImpl.class);


  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private CoCService coCDelegate;

  /**
   * The {@link ActionableItemService} from Spring context.
   */
  @Autowired
  private ActionableItemService actionableItemDelegate;
  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * Returns list of paginated codicils associated to an asset.
   *
   * @param page the number of pages to be returned..
   * @param size the size of elements per page to be returned.
   * @param assetId the asset unique identifier.
   * @return list of codicil.
   * @throws ClassDirectException if API call to fetch COC's and actionable items fails or execution
   *         fails.
   */
  @RestrictedAsset
  @Override
  public final PageResource<Object> getCodicil(final Integer page, final Integer size, @AssetID final Long assetId)
      throws ClassDirectException {
    LOGGER.debug("Querying codicils with page {} and size {}.", page, size);
    PageResource<Object> pageResource = null;

    pageResource = PaginationUtil.paginate(getCodicil(assetId), page, size);
    return pageResource;


  }

  /**
   * Returns list of codicil objects associated to an asset.
   *
   * @param assetId the asset unique identifier.
   * @return list of codicil objects.
   * @throws ClassDirectException if API call to fetch COC's and actionable items fails or execution
   *         fails.
   */
  public final List<Object> getCodicil(final long assetId) throws ClassDirectException {
    final List<Object> consolidatedContent = new ArrayList<Object>();
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    try {
      // get CoC
      final Callable<List<? extends CodicilDto>> cocResponse = () -> {
        final List<? extends CodicilDto> cocList = coCDelegate.getCoC(assetId);
        LOGGER.debug("Coc size for asset {} is {}", assetId, cocList.size());
        return cocList;
      };

      // get Actionable Item
      final Callable<List<? extends CodicilDto>> actionableItemResponse = () -> {
        final List<? extends CodicilDto> aiList =
            actionableItemDelegate.getActionableItem(null, null, assetId).getContent();
        LOGGER.debug("Actionable item size for asset {} is {}", assetId, aiList.size());
        return aiList;
      };

      final List<Callable<List<? extends CodicilDto>>> callables = Arrays.asList(cocResponse, actionableItemResponse);

      final List<Future<List<? extends CodicilDto>>> futureList = executor.invokeAll(callables);

      for (Future<List<? extends CodicilDto>> singleFuture : futureList) {
        consolidatedContent.addAll(singleFuture.get());
      }

      // sort data by due date
      if (!consolidatedContent.isEmpty()) {
        Collections.sort(consolidatedContent, new CodicilComparator());
      }
    } catch (final Exception exception) {
      LOGGER.error("Error while executing getCodicil: {} ", exception);
    } finally {
      executor.shutdown();
    }
    return consolidatedContent;
  }


  /**
   * Provides methods to compare date objects and sorting of date objects by ascending order.
   *
   * @author yng
   */
  static class CodicilComparator implements Comparator<Object>, Serializable {
    /**
     * The serialVersionUID of subclass.
     */
    private static final long serialVersionUID = 5877972149246462882L;

    @Override
    public int compare(final Object object1, final Object object2) {
      // ascending sort
      return getDate(object1).compareTo(getDate(object2));
    }

    /**
     * Returns date object as COC date,if object is instance of COC or actionable item date, if
     * object is instance of actionable item.
     *
     * @param object the object from list.
     * @return date object.
     */
    private Date getDate(final Object object) {
      Date date = null;

      if (object instanceof CoCHDto) {
        date = ((CoCHDto) object).getDueDate();
      }

      if (object instanceof ActionableItemHDto) {
        date = ((ActionableItemHDto) object).getDueDate();
      }

      return date;
    }
  }

  @Override
  public final CodicilStatusesHDto getCodicilStatuses() {

    CodicilStatusesHDto statuses = new CodicilStatusesHDto();

    // get all codicil statuses
    List<CodicilStatusHDto> codicilStatuses = assetReferenceService.getCodicilStatuses();

    // get defect statuses
    List<DefectStatusHDto> defectStatuses = assetReferenceService.getDefectStatuses();

    // get mncn statuses
    List<MajorNCNStatusHDto> mncnStatuses = assetReferenceService.getMncnStatusList();

    if (codicilStatuses != null && !codicilStatuses.isEmpty()) {
      // get codicil statuses
      List<String> cocStatuses = codicilStatuses.stream().filter(status -> !status.getDeleted())
          .filter(status -> status.getTypeId().equals(CodicilType.COC.getId())).map(CodicilStatusHDto::getName)
          .collect(Collectors.toList());
      statuses.setCocStatuses(cocStatuses);
      // get actionable item statuses
      List<String> aiStatuses = codicilStatuses.stream().filter(status -> !status.getDeleted())
          .filter(status -> status.getTypeId().equals(CodicilType.AI.getId())).map(CodicilStatusHDto::getName)
          .collect(Collectors.toList());
      statuses.setAiStatuses(aiStatuses);

      // get asset notes statuses
      List<String> anStatuses = codicilStatuses.stream().filter(status -> !status.getDeleted())
          .filter(status -> status.getTypeId().equals(CodicilType.AN.getId())).map(CodicilStatusHDto::getName)
          .collect(Collectors.toList());
      statuses.setAnStatuses(anStatuses);

      // get statutory findings statuses
      List<String> sfStatuses = codicilStatuses.stream().filter(status -> !status.getDeleted())
          .filter(status -> status.getTypeId().equals(CodicilType.SF.getId())).map(CodicilStatusHDto::getName)
          .collect(Collectors.toList());
      statuses.setSfStatuses(sfStatuses);
    }
    if (defectStatuses != null && !defectStatuses.isEmpty()) {

      // get decfect/ deficiency statuses
      List<String> falutStatuses = defectStatuses.stream().filter(status -> !status.getDeleted())
          .map(DefectStatusHDto::getName).collect(Collectors.toList());
      statuses.setDefectStatuses(falutStatuses);
      statuses.setDeficiencyStatuses(falutStatuses);
    }

    if (mncnStatuses != null && !mncnStatuses.isEmpty()) {
      // get mncns statuses
      List<String> majorNcnStatuses = mncnStatuses.stream().filter(status -> !status.getDeleted())
          .map(MajorNCNStatusHDto::getName).collect(Collectors.toList());
      statuses.setMncnStatuses(majorNcnStatuses);
    }

    return statuses;
  }

}
