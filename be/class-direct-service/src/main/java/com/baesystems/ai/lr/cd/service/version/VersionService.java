package com.baesystems.ai.lr.cd.service.version;

import com.baesystems.ai.lr.cd.be.domain.dto.version.VersionDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides local service to get the build number.
 *
 * @author fwijaya on 15/6/2017.
 */
public interface VersionService {

  /**
   * Returns the version/build number.
   *
   * @return The build number.
   * @throws ClassDirectException When no build number is found in the manifest.
   */
  VersionDto getVersion() throws ClassDirectException;
}
