package com.baesystems.ai.lr.cd.service.userprofile.impl;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.CLIENT;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.FLAG;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.SHIP_BUILDER;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;
import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ARCHIVED;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.CREATE_USER;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.CREATE_USER_WITH_EOR;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.CREATE_USER_WITH_TM;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.RECREATE_ARCHIVED_USER;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.UPDATE_USER;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.UPDATE_USER_EOR_CHANGED;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.UPDATE_USER_EXPIRYDATE_CHANGED;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.UPDATE_USER_LAST_LOGIN;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.UPDATE_USER_STATUS_CHANGED;
import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.UPDATE_USER_TM_CHANGED;
import static com.baesystems.ai.lr.cd.be.enums.AzureUserStatusEnum.APPROVAL_APPROVED;
import static com.baesystems.ai.lr.cd.be.enums.AzureUserStatusEnum.APPROVAL_NONE;
import static com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils.isAnyFieldChanged;
import static com.baesystems.ai.lr.cd.be.utils.ExecutorUtils.newFixedThreadPool;
import static com.baesystems.ai.lr.cd.be.utils.PaginationUtil.DEFAULT_SIZE_PER_PAGE;
import static com.baesystems.ai.lr.cd.be.utils.PaginationUtil.DEFAULT_STARTING_PAGE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static org.springframework.util.StringUtils.isEmpty;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.customer.CustomerRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.UserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.PartyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.ExportSummaryDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.LastLoginRangeDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.PartyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.LREmailDomainsDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.LREmailDomains;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.TermsConditions;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.baesystems.ai.lr.cd.service.favourites.FavouritesService;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.cd.service.userprofile.helper.SSOProviderFactory;
import com.baesystems.ai.lr.cd.service.userprofile.helper.SSOUserProvider;
import com.baesystems.ai.lr.dto.paging.PageResource;

import lombok.Setter;
import retrofit2.Call;

/**
 * @author SBollu
 *
 */
@Service("userProfileService")
public class UserProfileServiceImpl implements UserProfileService, InitializingBean {
  /**
   * Self call for UserProfileService.
   */
  private UserProfileService self;

  /**
   * Injected app context.
   */
  @Autowired
  private ApplicationContext applicationContext;

  /**
   * Logger for UserProfileServiceImpl class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileServiceImpl.class);

  /**
   * Inject cache manager.
   */
  @Autowired
  private CacheManager cacheManager;

  /**
   */
  @Autowired
  private UserProfileDao userProfileDao;

  /**
   * userRetrofitService.
   */
  @Autowired
  private UserRetrofitService userRetrofitService;

  /**
   * customerRetrofitService.
   */
  @Autowired
  private CustomerRetrofitService customerRetrofitService;

  /**
   * Flag State Service.
   */
  @Autowired
  private FlagStateService flagStateService;

  /**
   * Favorites Service.
   */
  @Autowired
  private FavouritesService favouritesService;

  /**
   * SubFleet Service.
   */
  @Autowired
  private SubFleetService subfleetService;

  /**
   * blank constant.
   */
  private static final String SPACE = " ";

  /**
   * The empty string constant.
   */
  private static final String EMPTY = "";

  /**
   * Audit Service for user management audit.
   */
  @Autowired
  private AuditService auditService;
  /**
   * error code.
   */
  private static final int ERROR_CODE = 400;

  /**
   * Equasis / Thesis role id.
   */
  private static final Long EQUASIS_THETIS_ROLE_ID = -1L;
  /**
   * logger prefix for lr_account.
   */
  private static final String PREFIX = "LR_ACCOUNT";

  /**
   * Fields with user's imo number.
   */
  private static final String[] USER_CODE_FIELDS = new String[] {"flagCode", "clientCode", "shipBuilderCode"};

  /**
   * message for existing user.
   */
  @Value("${user.exists}")
  private String userExists;

  /**
   * user id not found error message.
   */
  @Value("${user.id.empty}")
  private String userIdEmpty;

  /**
   * LR Email domains DAO.
   */
  @Autowired
  private LREmailDomainsDao lrEmailDomainsDao;

  /**
   * A list of LR email domains.
   */
  @Setter
  private List<String> lREmailDomains;

  /**
   * SSO provider factory.
   */
  @Autowired
  private SSOProviderFactory ssoProviderFactory;

  /**
   * afterPropertiesSet() method to initialize the bean.
   */
  @Override
  public final void afterPropertiesSet() throws Exception {
    self = applicationContext.getBean("userProfileService", UserProfileService.class);
    final List<LREmailDomains> result = lrEmailDomainsDao.getLrEmailDomains();
    lREmailDomains = result.stream().map(domain -> domain.getDomain()).collect(Collectors.toList());
  }

  /**
   * This method aims to update users' information to Azure in batch.
   *
   * @param userProfiles list of user profiles.
   * @throws ClassDirectException ClassDirectException.
   */
  private void updateSSOUser(final List<UserProfiles> userProfiles) throws ClassDirectException {
    for (SSOUserProvider provider : ssoProviderFactory.getProviders(userProfiles)) {
      provider.fetchUsers(userProfiles, this::updateUserProfilesUsingExternalAPI);
    }
  }

  @Override
  public final List<UserProfiles> getUsers(final String requesterId) throws ClassDirectException {
    final List<UserProfiles> resultList = userProfileDao.getUsers(requesterId);
    setInternalUserFlag(resultList);
    cacheNewSsoUser(resultList);
    return resultList;
  }

  /**
   * Caches all given Class-Direct users whom are yet to be cached.
   *
   * @param users original list of User Profiles.
   * @throws ClassDirectException ClassDirectException.
   */
  private void cacheNewSsoUser(final List<UserProfiles> users) throws ClassDirectException {
    final Cache cache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    LOGGER.trace("CacheNewSsoUser : Caching new users from DB into User Cache : {}", cache.getName());
    LOGGER.debug("{} users retrieved from DB", users.size());

    final List<UserProfiles> nonSsoUsers = users.stream().map(user -> {
      final ValueWrapper cacheUserWrap = cache.get(CacheConstants.PREFIX_USER_ID + user.getUserId());
      if (cacheUserWrap != null && cacheUserWrap.get() != null) {
        BeanUtils.copyProperties(cacheUserWrap.get(), user);
      }
      return user;
    }).filter(user -> !user.isSsoUser()).collect(Collectors.toList());

    if (nonSsoUsers.size() > 0) {
      updateSSOUser(nonSsoUsers);
      LOGGER.debug("{} users updated in Azure", nonSsoUsers.size());

      nonSsoUsers.stream().filter(user -> user.isSsoUser()).forEach(newUser -> {
        final String userId = newUser.getUserId();
        cache.put(CacheConstants.PREFIX_USER_ID + userId, newUser);
        LOGGER.debug("Azure User Ids cached: {}", userId);
      });
    }
  }

  @Override
  public final UserProfiles updateUser(final String requesterId, final String editUserId,
      final UserProfiles userProfiles, final String deleteFields) throws ClassDirectException {

    final UserProfiles requester = self.getUser(requesterId);
    final UserProfiles oriUserProfile;
    // Prevent second call to database if the requester user and edit user are the same.
    if (editUserId.equals(requesterId)) {
      oriUserProfile = requester;
    } else {
      oriUserProfile = userProfileDao.getUser(editUserId);
    }
    UserProfiles userProfile = userProfileDao.updateUser(requesterId, editUserId, userProfiles, deleteFields);
    auditService.audit(requester, oriUserProfile, oriUserProfile, userProfile, UPDATE_USER, UPDATE_USER_EOR_CHANGED,
        UPDATE_USER_TM_CHANGED, UPDATE_USER_STATUS_CHANGED, UPDATE_USER_EXPIRYDATE_CHANGED);
    // Remove favorites and sub-fleet in database and cache when flag code, client code or ship
    // builder code is changed.
    if (isAnyFieldChanged(USER_CODE_FIELDS, oriUserProfile, userProfile)) {
      favouritesService.deleteAllUserFavourites(editUserId);
      final SubFleetDto subfleet = new SubFleetDto();
      subfleet.setAccessibleAssetIds(emptyList());
      subfleet.setRestrictedAssetIds(emptyList());
      try {
        subfleetService.saveSubFleet(editUserId, subfleet);
      } catch (final SQLException sqlException) {
        LOGGER.debug("Unable to delete subfleets associated with user id: {}", editUserId);
        throw new ClassDirectException(sqlException);
      }
    }

    // check if user account status is archive or otherwise.
    boolean isUserArchived = ofNullable(userProfile.getStatus())
        .filter(userAccountStatus -> ARCHIVED.getName().equals(userAccountStatus.getName())).isPresent();

    if (isUserArchived && !isLREmailDomain(userProfile.getEmail())) {
      // delete user from active group via Azure API if user is not LR internal user
      deleteUserFromApprovedGroup(userProfile);
    }
    setInternalUserFlag(userProfile);
    return userProfile;
  }

  /**
   * Updates user's information such as user's full name and company name in database.
   *
   * @param newUserProfiles new user profile.
   * @throws ClassDirectException ClassDirectException.
   */
  private void updateUserFullNameAndCompanyName(final UserProfiles newUserProfiles) throws ClassDirectException {
    final int result = userProfileDao.updateUserFullNameAndCompanyName(newUserProfiles);
    final String userId = newUserProfiles.getUserId();
    UserProfiles newProfileChanged = null;
    try {
      newProfileChanged = userProfileDao.getUser(userId);
    } catch (final RecordNotFoundException exception) {
      // only for audit purpose is ok it will be empty during new user creation journey.
      LOGGER.debug("Record Not Found Exception for user with user id : {}", userId);
    }
    auditService.audit(null, newProfileChanged, newUserProfiles, newProfileChanged, UPDATE_USER);
    LOGGER.info("User full name and Company Name update status: {}", result);
  }

  /**
   * Removes user from "CDF(Customer Decision Framework)/Approved" User Group in Azure.
   *
   * @param userprofile userprofile.
   */
  private void deleteUserFromApprovedGroup(final UserProfiles userprofile) {
    final String userId = userprofile.getUserId();
    try {
      LRIDUserStatusDto azureUserStatus = new LRIDUserStatusDto();
      azureUserStatus.setState(APPROVAL_NONE.name());
      final Call<StatusDto> statusCall = userRetrofitService.approveAzureUser(azureUserStatus, userprofile.getUserId());
      final StatusDto statusDto = statusCall.execute().body();
      LOGGER.info(PREFIX + " has removed Azure user {} from CDF user group. Status : {}", userId,
          ofNullable(statusDto).map(StatusDto::getMessage).orElse(EMPTY));
    } catch (final IOException exception) {
      LOGGER.error(PREFIX + " failed to remove Azure user {} from CDF user group.", userId, exception);
    }
  }


  @Override
  public final UserProfiles getUser(final String requesterId) throws ClassDirectException {
    UserProfiles user = null;
    if (isEmpty(requesterId)) {
      // Retrieve authentication user roles from spring security context.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      // filter out Equasis/Thetis users.
      final List<SimpleGrantedAuthority> equasisThetisUsers = token.getAuthorities().stream().filter(authority -> {
        return authority.getAuthority().equalsIgnoreCase(EQUASIS.toString())
            || authority.getAuthority().equalsIgnoreCase(THETIS.toString());
      }).collect(Collectors.toList());

      if (!equasisThetisUsers.isEmpty()) {
        for (final SimpleGrantedAuthority equisisThetisUser : equasisThetisUsers) {
          // Check and set Equasis user.
          if (equisisThetisUser.getAuthority().equalsIgnoreCase(EQUASIS.toString())) {
            user = setEquasisThetisUser(EQUASIS);
          }
          // Check and set Thetis user.
          if (equisisThetisUser.getAuthority().equalsIgnoreCase(THETIS.toString())) {
            user = setEquasisThetisUser(THETIS);
          }
        }
      }
    } else {
      // Fetch user's information from database when that requested user is not Equasis or Thetis.
      user = userProfileDao.getUser(requesterId);
      if (!isEmpty(user.getEmail())) {
        LRIDUserDto ssoUser = getSSOUser(user.getEmail());
        if (ssoUser != null) {
          updateUserProfilesUsingExternalAPI(user, ssoUser);
        }
      }

    }
    return user;
  }

  @Override
  public final List<UserAccountStatus> fetchAllUserAccStatus() throws ClassDirectException {
    return userProfileDao.fetchAllUserAccStatus();
  }

  @Override
  public final PageResource<UserProfiles> getUsersById(final String requesterId, final Integer page, final Integer size)
      throws ClassDirectException {
    LOGGER.debug("Query users with page number {} and page size {}.", page, size);
    return PaginationUtil.paginate(getUsers(requesterId), page, size);
  }

  @Override
  public final PageResource<UserProfiles> getUsersByDto(final UserProfileDto query, final Integer page,
      final Integer size, final String sort, final String order) throws ClassDirectException {
    Long userCount = userProfileDao.getUsersCount(query);
    LOGGER.debug("Query users with page number {} and page size {}. {} user(s) is found.", page, size, userCount);
    return PaginationUtil.paginate(getUsers(query, page, size, sort, order), page, size, userCount.intValue());
  }

  @Override
  public final List<UserProfiles> getUsers(final UserProfileDto query, final Integer page, final Integer size,
      final String sort, final String order) throws ClassDirectException {
    UserProfiles ssoUserProfile = null;
    List<UserProfiles> userProfiles = null;
    userProfiles = userProfileDao.getUsers(query, page, size, sort, order);
    setInternalUserFlag(userProfiles);

    LOGGER.debug("{} users found for query", userProfiles.size());
    if (!userProfiles.isEmpty()) {
      cacheNewSsoUser(userProfiles);
    } else {
      // When user is not found in Class-Direct, then check if there is a need to query SSO.
      if (ofNullable(query).map(UserProfileDto::getSearchSSOIfNotExist).orElse(false)) {
        if (!isEmpty(query.getEmail())) {
          final LRIDUserDto ssoUserDto = this.getSSOUser(StringUtils.lowerCase(query.getEmail()));
          if (ssoUserDto != null) {
            ssoUserProfile = new UserProfiles();
            ssoUserProfile.setEmail(StringUtils.lowerCase(query.getEmail()));
            userProfiles.add(ssoUserProfile);
            updateUserProfilesUsingExternalAPI(ssoUserProfile, ssoUserDto);
          } else {
            LOGGER.debug("User with email address {} is not found in SSO.", query.getEmail());
          }
        } else {
          LOGGER.debug("User email is not specified.");
        }
      }
    }

    return userProfiles;
  }

  /**
   * Updates user profile using Single Sign On and MAST information using external API such as
   * Azure/Sitekit and MAST (Customer Query).
   *
   * @param userProfile userProfile.
   * @param lridDto lridDto.
   * @throws ClassDirectException ClassDirectException.
   */
  private void updateUserProfilesUsingExternalAPI(final UserProfiles userProfile, final LRIDUserDto lridDto)
      throws ClassDirectException {

    if (isEmpty(userProfile.getUserId())) {
      userProfile.setUserId(lridDto.getObjectId());
    }

    final String oldCompanyName = ofNullable(userProfile.getCompanyName()).orElse("");
    final String oldFullName = ofNullable(userProfile.getName()).orElse("");

    userProfile.setLastName(lridDto.getSurname());
    userProfile.setFirstName(lridDto.getGivenName());

    final StringBuilder fullName = new StringBuilder();
    final boolean isFirstNamePresent = !isEmpty(userProfile.getFirstName());
    if (isFirstNamePresent) {
      fullName.append(userProfile.getFirstName());
    }
    if (!isEmpty(userProfile.getLastName())) {
      if (isFirstNamePresent) {
        fullName.append(SPACE);
      }
      fullName.append(userProfile.getLastName());
    }
    userProfile.setName(fullName.toString());

    userProfile.setCompany(ofNullable(userProfile.getCompany()).orElseGet(Company::new));
    final Company company = userProfile.getCompany();
    LOGGER.debug("{} is a SSO user, updating details", userProfile.getUserId());

    // Client user, Company name and Contact information - Customer Query (MAST API).
    // Ship Builder user, Company name - Customer Query & IHS CompanyAddress Query(MAST API) and
    // Contact information - Azure API.
    // Flag user, Company name - Flag name - Flag Service and Contact information - Azure API.
    // Other users, Company name and contact information - Azure API.
    final Role[] neededRole = new Role[] {CLIENT, FLAG, SHIP_BUILDER};
    final Optional<Role> userRoleOpt = UserProfileServiceUtils.getMatchedRoles(neededRole, userProfile);
    final boolean match = userRoleOpt.isPresent();

    // Use MAST API to update company information for users if user is CLIENT, FLAG or SHIP_BUILDER.
    // Otherwise, use
    // Azure/Sitekit.
    if (match) {
      final Role role = userRoleOpt.get();
      if (role == FLAG) {
        final String flagCode = userProfile.getFlagCode();
        if (flagCode != null) {
          try {
            flagStateService.getFlagStates().parallelStream().filter(flag -> flag.getFlagCode().equals(flagCode))
                .findFirst().ifPresent(flagDto -> {
                  company.setName(flagDto.getName());
                  userProfile.setCompany(company);
                  userProfile.setCompanyName(company.getName());
                });
          } catch (final ClassDirectException cdException) {
            LOGGER.error("Error retrieving flag information for flag code :{}", flagCode, cdException);
          }
        } else {
          LOGGER.error("User's Flag code is empty.");
        }
      } else {
        final PartyQueryHDto query = new PartyQueryHDto();
        if (role == SHIP_BUILDER) {
          query.setShipBuilder(true);
          query.setImoNumber(userProfile.getShipBuilderCode());
        } else {
          query.setShipBuilder(false);
          query.setImoNumber(userProfile.getClientCode());
        }
        ofNullable(query.getImoNumber()).ifPresent(clientImo -> {
          query.setImoNumber(clientImo.toString());
          try {
            ofNullable(retrieveClientInformation(null, null, query)).flatMap(partyDto -> partyDto.stream().findFirst())
                .ifPresent(partyDto -> {
                  // Hydrate the country
                  Resources.inject(partyDto);
                  final String countryName = ofNullable(partyDto.getCountry()).orElse(EMPTY);
                  // company details for ship builder is from Customer Query.
                  if (role == CLIENT) {
                    company.setAddressLine1(partyDto.getAddressLine1());
                    company.setAddressLine2(partyDto.getAddressLine2());
                    company.setAddressLine3(partyDto.getAddressLine3());
                    company.setPostCode(partyDto.getPostcode());
                    company.setCity(partyDto.getCity());
                    company.setCountry(countryName);
                    company.setTelephone(partyDto.getPhoneNumber());
                  }
                  company.setName(partyDto.getName());
                  userProfile.setCompany(company);
                  userProfile.setCompanyName(company.getName());
                });
          } catch (final ClassDirectException classDirectException) {
            LOGGER.error("Error retrieving client information for imo number :{}", query.getImoNumber(),
                classDirectException);
          }
        });
      }
    } else {
      company.setName(lridDto.getRcaOrganisationName());
      userProfile.setCompanyName(company.getName());
    }

    // Update Company Information from Azure if new user from Azure or User with CLIENT role.
    // nor SHIP BUILDER.
    if (!match || Role.FLAG == userRoleOpt.get() || Role.SHIP_BUILDER == userRoleOpt.get()) {
      company.setAddressLine1(lridDto.getRcaAddressLine1());
      company.setAddressLine2(lridDto.getRcaAddressLine2());
      company.setAddressLine3(lridDto.getRcaAddressLine3());
      company.setCity(lridDto.getRcaCity());
      company.setCountry(lridDto.getRcaCountry());
      company.setState(lridDto.getRcaStateProvinceRegion());
      company.setTelephone(lridDto.getTelephoneNumber());
      company.setPostCode(lridDto.getRcaZipPostalCode());
    }

    LOGGER.debug("New - company name :{}, user's full name : {}", userProfile.getCompanyName(), userProfile.getName());
    if (!oldCompanyName.equals(userProfile.getCompanyName()) || !oldFullName.equals(userProfile.getName())) {
      updateUserFullNameAndCompanyName(userProfile);
    }

    userProfile.setCompanyPosition(lridDto.getOrganisationalRole());
    userProfile.setSsoUser(true);
    userProfile.setInternalUser(isLREmailDomain(userProfile.getEmail()));
  }

  /**
   * get single SSOUser.
   *
   * @param email email.
   * @throws ClassDirectException exceptions.
   * @return list of lrid users.
   * @throws ClassDirectException exceptions.
   */
  public final LRIDUserDto getSSOUser(final String email) throws ClassDirectException {
    List<LRIDUserDto> azureUserDtoList = ssoProviderFactory.getProvider(email).fetchUsers(email);
    if (azureUserDtoList != null && !azureUserDtoList.isEmpty()) {
      LOGGER.info("Found {} users from SSO with email {}", azureUserDtoList.size(), email);
      for (LRIDUserDto dto : azureUserDtoList) {
        if (dto.getSignInName().equalsIgnoreCase(email)) {
          return dto;
        }
      }
    }
    return null;
  }

  @Override
  public final UserProfiles getRequestedUser(final String requestedId) throws ClassDirectException {
    UserProfiles cdUser = userProfileDao.getRequestedUser(requestedId);
    if (!isEmpty(cdUser.getEmail())) {
      LRIDUserDto ssoUser = getSSOUser(cdUser.getEmail());
      setInternalUserFlag(cdUser);
      if (ssoUser != null) {
        updateUserProfilesUsingExternalAPI(cdUser, ssoUser);
      }
    }
    return cdUser;
  }

  /**
   * Sets the Equasis_Thetis role for currentUser whom are either Equasis or Thetis.
   *
   * @param role role of the user.
   * @return user profile with their respective user role.
   */
  private UserProfiles setEquasisThetisUser(final Role role) {
    final UserProfiles user = new UserProfiles();
    final Set<Roles> userRoles = new HashSet<Roles>();
    final Roles userRole = new Roles();
    userRole.setRoleId(EQUASIS_THETIS_ROLE_ID);
    userRole.setRoleName(role.toString());
    userRoles.add(userRole);
    user.setRoles(userRoles);
    return user;
  }

  @Override
  public final void disableExpiredUserAccounts() {
    int updateResult = userProfileDao.disableExpiredUsers();
    LOGGER.info("{} expired user account(s) disabled.", updateResult);
  }

  @Override
  public final StatusDto createUser(final String requesterId, final CreateNewUserDto newprofile)
      throws ClassDirectException {
    StatusDto status = null;
    try {
      // validate userId.
      if (newprofile == null || isEmpty(newprofile.getUserId())) {
        throw new ClassDirectException(userIdEmpty);
      }

      final String userId = newprofile.getUserId();

      // call azure API to approve user.
      // By pass calling sitekit API if the user is internal LR user
      if (!isLREmailDomain(newprofile.getEmail())) {
        addUserToApprovedGroup(newprofile);
      }
      UserProfiles requesterUser = self.getUser(requesterId);
      final UserProfiles targetUser = new UserProfiles();
      targetUser.setUserId(newprofile.getUserId());
      targetUser.setEmail(newprofile.getEmail());

      try {
        // Check if new user or existing archived user or existing active/disabled user.
        final UserProfiles archivedUser = userProfileDao.getUser(userId);
        // Check if existing user is archived user.
        final boolean isArchivedUser = ofNullable(archivedUser).map(UserProfiles::getStatus)
            .map(UserAccountStatus::getId).filter(statusId -> ARCHIVED.getId().equals(statusId)).isPresent();

        if (isArchivedUser) {
          // recreate existing archived user if the email of new user matches.
          status = self.recreateArchivedUser(archivedUser, requesterId, newprofile);
          // retrieve the updated user information for audit logging.
          final UserProfiles newlyCreatedUser = userProfileDao.getUser(userId);
          auditService.audit(requesterUser, targetUser, archivedUser, newlyCreatedUser, RECREATE_ARCHIVED_USER);
        } else {
          status = new StatusDto(userExists, newprofile.getEmail());
          throw new ClassDirectException(ERROR_CODE, status.getMessage());
        }
      } catch (final RecordNotFoundException recordNotFoundException) {
        // In the event that there's no record found in Class-Direct, create new user.
        status = userProfileDao.createUser(requesterId, newprofile);
        auditService.audit(requesterUser, targetUser, null, newprofile, CREATE_USER);
      }
      auditService.audit(requesterUser, targetUser, null, newprofile, CREATE_USER_WITH_EOR, CREATE_USER_WITH_TM);
    } catch (final DataIntegrityViolationException exception) {
      LOGGER.error("Error while create user", exception);
      throw new ClassDirectException(exception);
    }
    return status;
  }


  /**
   * Add user to "CDF(Customer Decision Framework)/Approved" User Group in Azure..
   *
   * @param userprofile userprofile.
   * @throws ClassDirectException exception.
   */
  private void addUserToApprovedGroup(final CreateNewUserDto userprofile) throws ClassDirectException {
    try {
      LRIDUserStatusDto azureUserStatus = new LRIDUserStatusDto();
      azureUserStatus.setState(APPROVAL_APPROVED.name());
      final Call<StatusDto> statusCall = userRetrofitService.approveAzureUser(azureUserStatus, userprofile.getUserId());
      final StatusDto statusDto = statusCall.execute().body();
      LOGGER.info(PREFIX + " user approved and added to cdf user group {}: " + userprofile.getUserId(), statusDto);
    } catch (final IOException exception) {
      LOGGER.error(PREFIX + " Fail to approve azure user {}: " + userprofile.getUserId(), exception);
      throw new ClassDirectException(PREFIX + " Fail to approve azure user {}: " + userprofile.getUserId());
    }
  }

  @Override
  public final StatusDto updateEOR(final String requesterId, final String userId,
      final CreateNewUserDto createNewUserDto) throws ClassDirectException {
    final UserProfiles requesterUser = self.getUser(requesterId);
    final UserProfiles targetUser;
    if (userId.equals(requesterId)) {
      targetUser = requesterUser;
    } else {
      targetUser = userProfileDao.getUser(userId);
    }

    final UserProfiles oldUser = targetUser;
    StatusDto status = userProfileDao.updateEORs(requesterId, userId, createNewUserDto);

    // Retrieve updated user for logging purpose.
    final UserProfiles newUser = userProfileDao.getUser(userId);
    auditService.audit(requesterUser, targetUser, oldUser, newUser, UPDATE_USER_EOR_CHANGED, UPDATE_USER_TM_CHANGED);
    return status;
  }

  @Override
  public final StatusDto deleteEOR(final String requesterId, final String userId, final String imoNumber)
      throws ClassDirectException {
    final UserProfiles requesterUser = self.getUser(requesterId);
    final UserProfiles oldUser;
    // Prevent second call to database if the requester user and edit user are the same.
    if (userId.equals(requesterId)) {
      oldUser = requesterUser;
    } else {
      oldUser = userProfileDao.getUser(userId);
    }
    StatusDto status = userProfileDao.removeEOR(userId, imoNumber);
    final UserProfiles targetUser = userProfileDao.getUser(userId);
    userProfileDao.deleteUserEORRole(targetUser);
    auditService.audit(requesterUser, targetUser, oldUser, targetUser, UPDATE_USER_EOR_CHANGED);
    return status;
  }

  @Override
  public final void removeExpiredEorUserAccounts() throws ClassDirectException {

    final List<UserEOR> eorUsersTobeExpired = userProfileDao.getExpiredEorUsers();
    LOGGER.info("Size of Expired EOR User Account's {}", eorUsersTobeExpired.size());

    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    final UserProfiles loggedInUser = token.getDetails();

    Optional.ofNullable(loggedInUser).ifPresent(user -> {

      for (UserEOR userEor : eorUsersTobeExpired) {
        try {
          self.deleteEOR(user.getUserId(), userEor.getUserId(), String.valueOf(userEor.getImoNumber()));
        } catch (final ClassDirectException exception) {
          LOGGER.error("Error in deleting expired user eor with user id {}", userEor.getUserId(),
              exception.getMessage());
        }
      }

    });
  }

  @Override
  public final TermsConditions getLatestTermsConditions() throws ClassDirectException {
    return userProfileDao.getLatestTermsConditions();
  }

  @Override
  public final StatusDto createAcceptedTermsandConditions(final String userId, final Long tcId)
      throws ClassDirectException {
    return userProfileDao.createAcceptedTermsandConditions(userId, tcId);
  }

  @Override
  public final ExportSummaryDto exportSummary(final UserProfileDto userProfileDto) throws ClassDirectException {
    ExportSummaryDto exportSummary = new ExportSummaryDto();
    // get all users
    List<UserProfiles> users =
        userProfileDao.getUsers(userProfileDto, DEFAULT_STARTING_PAGE, DEFAULT_SIZE_PER_PAGE, null, null);
    if (!users.isEmpty()) {
      List<String> filteredRoles = new ArrayList<>();
      SortedSet<String> roles = Collections.synchronizedSortedSet(new TreeSet<String>());
      SortedSet<String> loginRange = Collections.synchronizedSortedSet(new TreeSet<String>());
      ExecutorService executor = newFixedThreadPool(Runtime.getRuntime().availableProcessors());
      final List<Callable<Void>> tasks = users.parallelStream().map(user -> {
        Callable<Void> updateTask = () -> {
          if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            roles.addAll(user.getRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList()));
          }
          if (user.getLastLogin() != null) {
            loginRange.add(user.getLastLogin().format(ISO_LOCAL_DATE_TIME));
          }
          return null;
        };
        return updateTask;
      }).collect(Collectors.toList());

      try {
        executor.invokeAll(tasks);
        filteredRoles.addAll(roles);
        exportSummary.setAccountTypes(filteredRoles);
        LastLoginRangeDto loginDetails = new LastLoginRangeDto();
        if (!loginRange.isEmpty()) {
          loginDetails.setFrom(loginRange.first());
          loginDetails.setTo(loginRange.last());
        }
        exportSummary.setLastLoginDate(loginDetails);

      } catch (InterruptedException exception) {
        LOGGER.error("Error executing JobService - invoking threadpool .", exception);
      } finally {
        executor.shutdown();
      }

    }
    return exportSummary;
  }

  @Override
  public final Set<String> getEorsByUserId(final String userId) throws ClassDirectException {
    return userProfileDao.getEorsByUserId(userId);
  }

  @Override
  public final UserEOR getEOR(final String userId, final String imoNumber) throws RecordNotFoundException {
    return userProfileDao.getEOR(userId, imoNumber);
  }

  @Override
  public final List<PartyHDto> retrieveClientInformation(final Integer page, final Integer size,
      final PartyQueryHDto query) throws ClassDirectException {
    List<PartyHDto> partyDtos = null;
    try {
      final Call<List<PartyHDto>> caller = customerRetrofitService.getClientInformationByClientImo(page, size, query);
      partyDtos = caller.execute().body();
    } catch (final IOException ioException) {
      LOGGER.error("Error occured during Client Information retrieval", ioException);
    }
    return partyDtos;
  }

  @Override
  public final void updateAllUsersInformations() {
    final String className = this.getClass().getSimpleName();
    final Cache cache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    try {
      final List<UserProfiles> users = userProfileDao.getAllUsers();
      // Single-Sign On information of the user.
      updateSSOUser(users);
      // Update user's Company Name and full name in both database and cache.
      final List<Callable<Void>> callableUpdateUserTaskList =
          users.parallelStream().filter(usr -> usr.isSsoUser()).map(user -> {
            final Callable<Void> callableUpdateUserTask = () -> {
              final String userId = user.getUserId();
              LOGGER.debug("Updating user - user full name : {}, user id: {}", user.getName(), userId);
              updateUser(userId, userId, user, null);
              cache.evict(CacheConstants.PREFIX_USER_ID + userId);
              cache.put(CacheConstants.PREFIX_USER_ID + userId, user);
              return null;
            };
            return callableUpdateUserTask;
          }).collect(Collectors.toList());

      final ExecutorService executor = newFixedThreadPool(this.getClass().getSimpleName());
      try {
        executor.invokeAll(callableUpdateUserTaskList);
      } catch (final InterruptedException interruptedException) {
        LOGGER.error(interruptedException.getMessage());
      } finally {
        executor.shutdown();
      }
    } catch (final ClassDirectException exception) {
      LOGGER.error("Error occurred when updating all user", className, exception);
    }
  }

  @Override
  public final StatusDto recreateArchivedUser(final UserProfiles archivedUser, final String requesterId,
      final CreateNewUserDto newprofile) throws ClassDirectException {
    return userProfileDao.recreateArchivedUser(archivedUser, requesterId, newprofile);
  }

  /**
   * Tests whether an email address' domain belongs to LR internal.
   *
   * @param email The email address.
   * @return true if the domain belongs to LR internal.
   */
  private boolean isLREmailDomain(final String email) {
    if (email == null || email.isEmpty()) {
      return false;
    }
    return lREmailDomains.contains(email.substring(email.indexOf('@') + 1).toLowerCase(Locale.ENGLISH));
  }

  /**
   * Sets the internal user flag of {@link UserProfiles} dto.
   *
   * @param userProfiles The dto.
   */
  private void setInternalUserFlag(final UserProfiles userProfiles) {
    final boolean isInternalUser = isLREmailDomain(userProfiles.getEmail());
    userProfiles.setInternalUser(isInternalUser);
  }

  /**
   * Sets the internal user flag of {@link UserProfiles} dto.
   *
   * @param userProfiles The dto list.
   */
  private void setInternalUserFlag(final List<UserProfiles> userProfiles) {
    userProfiles.stream().forEach(user -> setInternalUserFlag(user));
  }

  @Override
  public final void updateUserLastLogin(final String editUserId, final LocalDateTime lastLogin)
      throws ClassDirectException {
    try {
      final UserProfiles targetUser = self.getUser(editUserId);
      userProfileDao.updateUserLastLogin(editUserId, lastLogin);
      auditService.audit(targetUser, targetUser, targetUser.getLastLogin(), lastLogin, UPDATE_USER_LAST_LOGIN);
    } catch (final Exception exception) {
      LOGGER.error("Error in updating user's last login {} with user id {} ", lastLogin, editUserId,
          exception.getMessage());
      throw new ClassDirectException(exception);
    }
  }
}
