package com.baesystems.ai.lr.cd.service.userprofile;

import java.time.LocalDateTime;
import java.util.List;

import java.util.Set;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.PartyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.ExportSummaryDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.PartyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.TermsConditions;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides local service for accessing, adding, deleting, updating user related information in
 * Class-Direct application.
 *
 * @author SBollu
 */
public interface UserProfileService {

  /**
   * Returns all Class-Direct users if requester user is authorized to access users' information.
   *
   * @param requesterId logged in user who requested to access all users.
   * @return Class-Direct users.
   * @throws ClassDirectException when user with @param requesterId is not found in Class-Direct or
   *         user is not authorized to access Class-Direct users' information or error occurred
   *         during SSO update.
   */
  List<UserProfiles> getUsers(String requesterId) throws ClassDirectException;

  /**
   * Updates edit user's information given requester user is authorized to access edit user's
   * information.
   *
   * @param requesterId logged in user who requested to edit.
   * @param editUserId user whose profile to be edited.
   * @param userProfiles edit user's new user profile object.
   * @param deleteFields list of comma-delimited fields to be deleted from the user profile.
   * @return updated user profile.
   * @throws ClassDirectException exception.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  @CacheEvict(cacheNames = CacheConstants.CACHE_USERS,
      key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#editUserId)")
  UserProfiles updateUser(String requesterId, String editUserId, UserProfiles userProfiles, String deleteFields)
      throws ClassDirectException;

  /**
   * Returns user profile of the requester user.
   *
   * @param requesterId logged in user who requested to edit.
   * @return existing user profile.
   * @throws ClassDirectException when error occurs during the retrieval of requester user's user
   *         profile.
   */
  @Cacheable(cacheNames = CacheConstants.CACHE_USERS,
      key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#requesterId)", condition = "#requesterId != null")
  UserProfiles getUser(String requesterId) throws ClassDirectException;

  /**
   * @return List of userAccStatus.
   * @throws ClassDirectException on any type of exception.
   */
  List<UserAccountStatus> fetchAllUserAccStatus() throws ClassDirectException;

  /**
   * Returns Class-Direct users with requester user's credentials given that user is authorized to
   * access the Class-Direct users.
   *
   * @param requesterId This is the first parameter to getUsers method.
   * @param page page number of user records.
   * @param size page size of user records.
   * @return PageResource object with the users' informations.
   * @throws ClassDirectException when error occurred during the retrieval of user's information.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  PageResource<UserProfiles> getUsersById(String requesterId, Integer page, Integer size) throws ClassDirectException;

  /**
   * Returns Page Resource object with Class-Direct users' information that matches user query with
   * page number, page size, sorting and order requested by user.
   *
   * @param query user query.
   * @param page page of user records.
   * @param size size of user records.
   * @param sort user records sorting field.
   * @param order user records order.
   * @return page resource for user List.
   * @throws ClassDirectException object.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  PageResource<UserProfiles> getUsersByDto(UserProfileDto query, Integer page, Integer size, String sort, String order)
      throws ClassDirectException;

  /**
   * Returns Class-Direct users that matches user query with page number, page size, sorting and
   * order requested by the user.
   *
   * @param query user query.
   * @param page page number of user records.
   * @param size page size of user records.
   * @param sort user records sorting field.
   * @param order user records order.
   * @return Class-Direct users.
   * @throws ClassDirectException object.
   */
  List<UserProfiles> getUsers(UserProfileDto query, Integer page, Integer size, String sort, String order)
      throws ClassDirectException;

  /**
   * Returns requested user profile.
   *
   * @param requestedId requested user id.
   * @return requested user profile.
   * @throws ClassDirectException when error occurs.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  @Cacheable(cacheNames = CacheConstants.CACHE_USERS,
      key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#requestedId)", condition = "#requestedId != null")
  UserProfiles getRequestedUser(final String requestedId) throws ClassDirectException;

  /**
   * Disables users whose expire date is passed current date.
   */
  void disableExpiredUserAccounts();

  /**
   * Creates new user in Class-Direct.
   *
   * @param requesterId logged in user who requested to edit.
   * @param newprofile new user object to create the new user.
   * @return status of user creation.
   * @throws RecordNotFoundException when user record is not found.
   * @throws ClassDirectException when exception occurs.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  StatusDto createUser(final String requesterId, final CreateNewUserDto newprofile)
      throws RecordNotFoundException, ClassDirectException;

  /**
   * Recreates archived user in database and removes the archived user's cache. (For internal use
   * only).
   *
   * @param archivedUser archived user.
   * @param requesterId logged in user who requested to edit.
   * @param newprofile new user object to create the new user.
   * @return status of the archived user recreation.
   * @throws RecordNotFoundException when user record is not found.
   * @throws ClassDirectException when exception occurs.
   */
  @Caching(evict = {
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#archivedUser.getUserId()).concat('"
              + CacheConstants.POSTFIX_SUBFLEET_IDS + "')"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#archivedUser.getUserId()).concat('"
              + CacheConstants.POSTFIX_RESTRICTED_ASSET_IDS + "')"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#archivedUser.getUserId()).concat('"
              + CacheConstants.POSTFIX_ACCESSIBLE_ASSET_IDS + "')"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_USERS,
          key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#archivedUser.getUserId())")})
  StatusDto recreateArchivedUser(final UserProfiles archivedUser, final String requesterId,
      final CreateNewUserDto newprofile) throws RecordNotFoundException, ClassDirectException;

  /**
   * Updates Exhibition Of Record user information for given user with the authority of requester
   * user.
   *
   * @param requesterId logged in user who requested to edit.
   * @param userId user id.
   * @param createNewUserDto user object to update EOR informations.
   * @return status of EOR record update.
   * @throws ClassDirectException when exception occurs.
   *
   */
  @CacheEvict(cacheNames = CacheConstants.CACHE_USERS, key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#userId)")
  StatusDto updateEOR(final String requesterId, final String userId, final CreateNewUserDto createNewUserDto)
      throws ClassDirectException;

  /**
   * Deletes the records of Exhibition Of Record (EOR) user for user with given imo number.
   *
   * @param requesterId logged in user who requested to edit.
   * @param userId user id whom EOR records to be deleted.
   * @param imoNumber EOR user's imo number.
   * @return status of EOR deletion.
   * @throws RecordNotFoundException if no user records for EOR user with imo number is found.
   * @throws ClassDirectException exception while delete EOR.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  @CacheEvict(cacheNames = CacheConstants.CACHE_USERS, key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#userId)")
  StatusDto deleteEOR(final String requesterId, final String userId, final String imoNumber)
      throws RecordNotFoundException, ClassDirectException;

  /**
   * Removes EOR user records whose expire date is passed current date.
   *
   * @throws ClassDirectException when database operation is interrupted.
   */
  void removeExpiredEorUserAccounts() throws ClassDirectException;

  /**
   * Returns latest Terms and Conditions Record from Database.
   *
   * @return latest Terms and Conditions record.
   * @throws ClassDirectException when exception occurs.
   */
  TermsConditions getLatestTermsConditions() throws ClassDirectException;

  /**
   * Creates Terms and Conditions for user who have accepted the Terms and Conditions. In the case
   * of an existing record, the accepted date is updated to current date.
   *
   * @param userId user id.
   * @param tcId Terms and Conditions id.
   * @return status of the creation/update process.
   * @throws ClassDirectException if there is error occurred during Terms and Conditions record
   *         creation and update.
   */
  @CacheEvict(cacheNames = CacheConstants.CACHE_USERS, key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#userId)")
  StatusDto createAcceptedTermsandConditions(final String userId, final Long tcId) throws ClassDirectException;

  /**
   * Returns user export summary for selected users including information such as roles, first and
   * last login date of the user.
   *
   * @param userProfileDto user query to filter users.
   * @return user export summary.
   * @throws ClassDirectException when exception occurs.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  ExportSummaryDto exportSummary(final UserProfileDto userProfileDto) throws ClassDirectException;

  /**
   * Returns EORs for user with given user id.
   *
   * @param userId userId.
   * @return list of eor IMOs.
   * @throws ClassDirectException exception.
   */
  Set<String> getEorsByUserId(final String userId) throws ClassDirectException;

  /**
   * Returns client information based on party query filter. page the number of pages to be
   * returned.
   *
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @param query party query to filter client information from MAST.
   * @return party dtos.
   * @throws ClassDirectException exception.
   */
  List<PartyHDto> retrieveClientInformation(final Integer page, final Integer size, final PartyQueryHDto query)
      throws ClassDirectException;

  /**
   * Returns the user EOR with given user id and imo number.
   *
   * @param userId userId.
   * @param imoNumber imoNumber.
   * @return eor Record.
   * @throws RecordNotFoundException when no found.
   */
  UserEOR getEOR(String userId, String imoNumber) throws RecordNotFoundException;

  /**
   * Updates all users' information with latest Company and Single-Sign On information in both cache
   * and database.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  void updateAllUsersInformations();

  /**
   * Updates logged in user's last login date time.
   *
   * @param editUserId user whose profile to be edited.
   * @param lastLogin the user last login to be updated.
   * @throws ClassDirectException if any sql exception or error in getting user or any error
   *         execution flow.
   */
  @CacheEvict(cacheNames = CacheConstants.CACHE_USERS,
      key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#editUserId)")
  void updateUserLastLogin(String editUserId, LocalDateTime lastLogin) throws ClassDirectException;
}
