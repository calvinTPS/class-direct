package com.baesystems.ai.lr.cd.service.flagstate.impl;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FlagsAssociationsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.FlagsAssociations;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides the local service for removing, adding and getting flags associations.
 *
 * @author fwijaya on 20/4/2017.
 */
@Service("FlagsAssociationsService")
public class FlagsAssociationsServiceImpl implements FlagsAssociationsService {
  /**
   * Log4J Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FlagsAssociationsServiceImpl.class);

  /**
   * The flags associations dao.
   */
  @Autowired
  private FlagsAssociationsDao flagsAssociationsDao;
  /**
   * Removes association.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @return The number of entities deleted.
   * @throws ClassDirectException if unchecked JPA exception occured.
   */
  @Override
  @Transactional
  public final int removeAssociation(final String flagCode, final String secondaryFlagCode)
    throws ClassDirectException {
    LOGGER.debug("Remove association for flagCode: " + flagCode + " and secondaryFlagCode: " + secondaryFlagCode);
    return this.flagsAssociationsDao.removeAssociation(flagCode, secondaryFlagCode);
  }

  /**
   * Adds new association.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @throws ClassDirectException if unchecked JPA exception occured.
   */
  @Override
  @Transactional
  public final void addAssociation(final String flagCode, final String secondaryFlagCode)
    throws ClassDirectException {
    LOGGER.debug("Add association for flagCode: " + flagCode + " and secondaryFlagCode: " + secondaryFlagCode);
    this.flagsAssociationsDao.addAssociation(flagCode, secondaryFlagCode);
  }

  /**
   * Returns flag associations for a flag code.
   *
   * @param flagCode Flag code.
   * @return List of flag associations for flag code.
   * @throws ClassDirectException if unchecked JPA exception occured.
   */
  @Override
  @Transactional
  public final List<String> getAssociatedFlags(final String flagCode) throws ClassDirectException {
    LOGGER.debug("Get flags associations for flagCode: " + flagCode);
    // First try treating the flag code as primary flag code and getting the list of associated secondary flags
    // If the resulting list is empty, try treating it as secondary flag
    List<FlagsAssociations> flagsAssociations =
      this.flagsAssociationsDao.getFlagAssocisationsForFlagCode(flagCode);
    if (flagsAssociations.isEmpty()) {
      flagsAssociations = this.flagsAssociationsDao.getFlagAssocisationsForSecondaryFlagCode(flagCode);
    }
    return flagsAssociations.stream()
      .map(flagAssociation -> flagsAssociationsDao.getAssociatedFlag(flagAssociation, flagCode))
      .collect(Collectors.toList());
  }

}
