package com.baesystems.ai.lr.cd.be.utils;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

import lombok.Getter;

/**
 * Stores the value from property file and splits the String value into Map<String, String>.
 *
 * @author VKolagutla
 *
 */
@Component
public class AssetEmblemUtils implements ApplicationContextAware, InitializingBean {

  /**
   * {@value #LOGGER} is the Logger display logs.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetEmblemUtils.class);

  /**
   * The spring application context.
   */
  private ApplicationContext context;

  /**
   * The constant string Padding.
   */
  private static final String PAD_CHARACTER = "1";

  /**
   * The constant number Asset Code Length.
   */
  private static final int ASSET_CODE_LENGTH = 3;

  /**
   * The constant String .png.
   */
  private static final String PNG = ".png";

  /**
   * The constant String Comma.
   */
  private static final String COMMA = ",";

  /**
   * The constant String PIPE.
   */
  private static final String PIPE = "\\|";

  /**
   * The constant String UNDERSQUARE.
   */
  private static final String UNDERSQUARE = "_";


  /**
   * The Actual injector.
   */
  private static AssetEmblemUtils util;

  /**
   * Get the emblems from properties file.
   */
  @Value("${emblems}")
  private String emblem;


  /**
   * The lookupmap to store emblem images with key value pair.
   */
  @Getter
  private final Map<String, String> lookupmap = new HashMap<>();

  /**
   * Get the emblem image with key assetCode from the Map lookupmap.
   *
   * @param assetCode to get emblem image.
   * @return emblem image.
   */
  public final String getEmblem(final String assetCode) {
    final String formatedAssetCode = formatAssetCode(assetCode);
    final String result = lookupmap.get(formatedAssetCode);
    return result;
  }


  /**
   * Format the assetCode, if the code length is less than three then padding with 1. EX:- A -> A11,
   * A1 -> A11, A11 -> A11. A2 -> A21, A23 -> A23.
   *
   * @param assetCode to format assetCode.
   * @return formatted assetCode.
   */
  private String formatAssetCode(final String assetCode) {
    String formatedAssetCode = "";
    if (assetCode.length() < ASSET_CODE_LENGTH) {
      formatedAssetCode = StringUtils.rightPad(assetCode, ASSET_CODE_LENGTH, PAD_CHARACTER);
    } else {
      formatedAssetCode = assetCode.substring(0, ASSET_CODE_LENGTH);
    }
    return formatedAssetCode;
  }

  /**
   * @param utilObj obj.
   */
  public static final void setAssetEmblemUtil(final AssetEmblemUtils utilObj) {
    AssetEmblemUtils.util = utilObj;
  }

  /**
   * Get emblem image with assetCode.
   *
   * @param assetCode assetCode.
   * @return emblem.
   */
  public static final String getEmblemImage(final String assetCode) {
    return util.getEmblem(assetCode);
  }

  /**
   * Splits the Property into key and value pair, splits with "_" and again with ",".
   *
   * Ex:- A11_liquified-gas.png|A12,A14_chemical-other-liquids.png {A11 = A11_liquified-gas.png, A12
   * = A12,A14_chemical-other-liquids.png, A14 = A12,A14_chemical-other-liquids.png}
   *
   * @throws ClassDirectException when emblem is empty.
   */
  public final void initEmblemLookup() throws ClassDirectException {

    if (StringUtils.isEmpty(emblem)) {
      LOGGER.error("expected format is A11_liquified-gas.png|A12,A14_chemical-other-liquids.png {}", emblem);
      throw new ClassDirectException("Emblem is not set.");
    }

    final String[] elements = emblem.split(PIPE, -1);
    for (String element : elements) {
      final String[] splitWithUnderSquare = element.split(UNDERSQUARE);
      for (String afterSplitWithUnderSquare : splitWithUnderSquare) {
        final String[] splitWithComma = afterSplitWithUnderSquare.split(COMMA, -1);
        for (String singleElement : splitWithComma) {
          if (!singleElement.contains(PNG)) {
            lookupmap.put(singleElement, element);
          }
        }
      }
    }
  }


  @Override
  public final void afterPropertiesSet() throws Exception {
    final AssetEmblemUtils utils = context.getBean(AssetEmblemUtils.class);
    utils.initEmblemLookup();
    AssetEmblemUtils.setAssetEmblemUtil(utils);
  }

  /**
   * Set application context.
   *
   * @param ctx application context.
   */
  @Override
  public final void setApplicationContext(final ApplicationContext ctx) throws BeansException {
    LOGGER.debug("Setting up application context.");
    context = ctx;
  }

}
