package com.baesystems.ai.lr.cd.service.notifications;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.NotificationTypes;
import com.baesystems.ai.lr.cd.be.domain.repositories.Notifications;
import com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * Provides notification service gets list of subscribed users and sends notifications which
 * includes asset listing, due status and jobs.
 *
 * @author RKaneysan.
 * @author syalavarthi 07-06-2017.
 */
public interface NotificationsService {

  /**
   * Returns list of subscribed users with all their subscribed notifications (asset listing, due
   * status (asset status update), and job notifications).
   *
   * @param typeIds notification type id.
   * @return list of subscribed users for given notification type.
   */
  List<Notifications> getSubscribedUsers(List<NotificationTypesEnum> typeIds);

  /**
   * Triggers due status notification and sends email to subscribed users
   * <p>
   * Sends Due Status notification(s) for Scheduled Services, Condition of Classes, Actionable Items
   * and Statutory Findings to Subscribed Users for their favorite assets or fleet. This method is only
   * accessible by personnel with "LR Admin" role and "LR_SUPPORT".
   * </p>
   * .
   *@return task uuid.
   */
  @PreAuthorize("hasAnyRole('LR_ADMIN','LR_SUPPORT')")
  String sendDueStatusNotification();

  /**
   * Triggers due status notification asynchronously.
   *
   * <p>
   *   This will trigger {@link #sendDueStatusNotification()} on separate thread, monitored by separate UUID
   *   and x-ray trace.
   * </p>
   * @throws ClassDirectException thrown when runtime exception happen.
   * @return task uuid.
   */
  @PreAuthorize("hasAnyRole('LR_ADMIN','LR_SUPPORT')")
  String sendDueStatusNotificationAsync() throws ClassDirectException;

  /**
   * Trigger asset listing notification and sends email to subscribed users.
   * <p>
   * Sends Asset Listing Notifications to subscribed users. This method is only accessible by
   * personnel with "LR Admin" role and "LR_SUPPORT".
   * </p>
   *@return task uuid.
   */
  @PreAuthorize("hasAnyRole('LR_ADMIN','LR_SUPPORT')")
  String sendAssetListingNotification();

  /**
   * Trigger asset listing notification.
   *
   * @throws ClassDirectException when error happen.
   * @return task uuid.
   */
  @PreAuthorize("hasAnyRole('LR_ADMIN','LR_SUPPORT')")
  String sendAssetListingNotificationAsync() throws ClassDirectException;

  /**
   * Triggers job notifications and sends email to subscribed users.
   * <p>
   * Sends Final Survey Report notification to subscribed users for their favorite assets or fleet. This
   * method is only accessible by personnel with "LR Admin" role and "LR_SUPPORT".
   * </p>
   *@return task uuid.
   */
  @PreAuthorize("hasAnyRole('LR_ADMIN','LR_SUPPORT')")
  String sendJobNotifications();

  /**
   * Triggers job notification asynchronously.
   *
   * @throws ClassDirectException when exception happen.
   * @return task uuid.
   */
  @PreAuthorize("hasAnyRole('LR_ADMIN','LR_SUPPORT')")
  String sendJobNotificationsAsync() throws ClassDirectException;

  /**
   * Returns all notification types.
   *
   * @return List of Notification types.
   */
  List<NotificationTypes> getNotificationTypes();

  /**
   * Creates notification reference for given user with user id and notifications type.
   * Returns @RecordNotFoundException if the user is not found in Class-Direct.
   *
   * @param requesterId subscriber user id.
   * @param typeId notification type id.
   * @param favouritesOnly The flag to indicate only favourite assets.
   * @return status of notification creation if successful or otherwise.
   * @throws RecordNotFoundException when Record not found.
   * @throws ClassDirectException when Record not found to delete notification.
   */
  @PreAuthorize("hasAnyRole('LR_ADMIN','LR_SUPPORT','CLIENT','FLAG','SHIP_BUILDER')")
  @CacheEvict(cacheNames = CacheConstants.CACHE_USERS,
      key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#requesterId)")
  StatusDto createNotification(final String requesterId, final Long typeId, final Boolean favouritesOnly)
      throws RecordNotFoundException, ClassDirectException;

  /**
   * Deletes notification reference for given user with user id and notifications type.
   *
   * @param requesterId subscriber user id.
   * @param typeId notification type id.
   * @return status of notification deletion if successful or otherwise.
   * @throws ClassDirectException exception.
   */
  @CacheEvict(cacheNames = CacheConstants.CACHE_USERS,
      key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#requesterId)")
  StatusDto deleteNotification(final String requesterId, final Long typeId) throws ClassDirectException;
}
