package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.export.TaskListExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemModelItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.TaskListExportService;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * @author VKolagutla
 *
 */
@Service(value = "TaskListExportService")
public class TaskListExportServiceImpl implements TaskListExportService {

  /**
   * Inject task service.
   */
  @Autowired
  private TaskService taskService;

  /**
   * Inject asset service.
   */
  @Autowired
  private AssetService assetService;

  /**
   * amazonStorageService.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * Inject service service.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;
  /**
   * logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(TaskListExportServiceImpl.class);

  /**
   * File generation working directory.
   */
  @Value("${tasklist.export.temp.dir}")
  private String tempDir;

  /**
   * File generation s3 bucket name.
   */
  @Value("${s3.export.bucket.path}")
  private String bucketName;

  /**
   * File name word splitter.
   */
  private static final String WORD_SPLITTER = "_";

  /**
   * Date formatter appended to working directory.
   */
  private static final DateTimeFormatter DIR_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS");

  /**
   * key variable to parse to pug file.
   */
  private static final String TEMPLATE_HELPER = "helper";

  /**
   * key variable for tasklist to parse to pug file.
   */
  private static final String TASKLIST_INFO = "tasksList";

  /**
   * key variable for asset to parse to pug file.
   */
  private static final String ASSET_INFO = "asset";

  /**
   * key variable for service to parse to pug file.
   */
  private static final String SERVICE = "service";

  /**
   * Pug template.
   */
  private static final String PUG_TEMPLATE_SIMPLE = "tasklist_export_template.jade";


  /**
   * Simple pmstasklist pdf file prefix.
   */
  private static final String SIMPLE_PDF_PREFIX = new StringBuilder().append("tasklist").append(WORD_SPLITTER)
      .append("listing").append(WORD_SPLITTER).append("export").append(WORD_SPLITTER).toString();

  /**
   * Date formatter appended to zip file.
   */
  private static final DateTimeFormatter FILE_DATE_FORMAT = DateTimeFormatter.ofPattern("ddMMyyyyHHmm");

  /**
   * s3 supported file separator.
   */
  private static final String S3_FILE_SEPARATOR = "/";

  /**
   * s3 export directory.
   */
  private static final String S3_TASKLIST_EXPORT_DIR = "tasklist_export";

  @Override
  public final StringResponse downloadTaskListInfo(final TaskListExportDto query) throws ClassDirectException {

    final long elapsedTime = System.currentTimeMillis();
    final String assetCode = query.getAssetCode();
    final Long assetId = AssetCodeUtil.getId(assetCode);

    try {

      // prepare download directory by <user_id>_<timestamp>
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      final UserProfiles user = token.getDetails();

      final LocalDateTime current = LocalDateTime.now();

      final String workingDir =
          tempDir + File.separator + user.getUserId() + WORD_SPLITTER + current.format(DIR_DATE_FORMAT);

      final File createDir = new File(workingDir);
      if (!createDir.exists()) {
        FileUtils.forceMkdir(createDir);
      }

      // put tasklist information
      final Map<String, Object> model = new TreeMap<>();

      final AssetHDto asset = assetService.assetByCode(user.getUserId(), assetCode);

      // get service information
      List<ScheduledServiceHDto> services = serviceReferenceService.getServicesForAsset(assetId);
      services = services.stream().filter(service -> service.getId().equals(query.getServiceId()))
          .collect(Collectors.toList());
      if (!services.isEmpty()) {
        model.put(SERVICE, services.get(0));
      }

      // get taskList inforamtion
      final WorkItemPreviewListHDto tasksList =
          taskService.getHierarchicalTasksByService(query.getServiceId(), asset.getId());
      if (tasksList.getServicesH() != null && !tasksList.getServicesH().isEmpty()) {
        tasksList.getServicesH().forEach(service -> {
          if (service.getItemsH() != null && !service.getItemsH().isEmpty()) {
            List<WorkItemModelItemHDto> itemsList = sortTasklistByItemDisplayOrder(service.getItemsH());
            service.setItemsH(itemsList);
          }
        });
      }

      // get helper class
      model.put(TEMPLATE_HELPER, new TemplateHelper());

      // get asset information
      model.put(ASSET_INFO, asset);

      // get tasklist information
      model.put(TASKLIST_INFO, tasksList);
      // to print current date in pdf
      model.put("currentDate", new Date());

      final String pdfFilename = generatePdf(model, workingDir, current);
      final StringBuilder outputStreamPathFilename = new StringBuilder();
      final String filePath =
          outputStreamPathFilename.append(workingDir).append(File.separator).append(pdfFilename).toString();

      try {
        final String path =
            user.getUserId() + S3_FILE_SEPARATOR + S3_TASKLIST_EXPORT_DIR + S3_FILE_SEPARATOR + pdfFilename;

        final String s3Token =
            amazonStorageService.uploadFile(bucketName, path, PdfUtils.convertToLinuxPathDelimiter(filePath),
                user.getUserId());

        return new StringResponse(s3Token, pdfFilename);

      } finally {
        FileUtils.deleteDirectory(new File(workingDir));
      }

    } catch (final JadeException | IOException exception) {
      throw new ClassDirectException(
          "Export-exception occured in downloadAssetInfo for asset [" + assetCode + "] : " + exception);
    } finally {
      // benchmark time
      LOGGER.info("TaskListExport ended for Asset : {}. | Elapsed time (ms): {}", assetCode,
          System.currentTimeMillis() - elapsedTime);
    }
  }

  /**
   * Generate pdf file.
   *
   * @param model pug model.
   * @param workingDir working directory.
   * @param date time.
   * @return generated filename.
   * @throws ClassDirectException value.
   */
  public final String generatePdf(final Map<String, Object> model, final String workingDir, final LocalDateTime date)
      throws ClassDirectException {
    final StringBuilder pdfFile = new StringBuilder();
    pdfFile.append(SIMPLE_PDF_PREFIX).append(date.format(FILE_DATE_FORMAT)).append(".pdf");

    printToPdf(model, workingDir, pdfFile.toString(), PUG_TEMPLATE_SIMPLE);

    LOGGER.info("TaskListExport-pdf created");
    return pdfFile.toString();
  }

  /**
   * Sort task list by items display order.
   *
   * @param itemsList itemsList.
   * @return List<WorkItemModelItemHDto>.
   */
  private List<WorkItemModelItemHDto> sortTasklistByItemDisplayOrder(final List<WorkItemModelItemHDto> itemsList) {
    Collections.sort(itemsList, (itemDto1, itemDto2) -> {
      final int result;
      result = itemDto1.getDisplayOrder().compareTo(itemDto2.getDisplayOrder());
      return result;
    });
    itemsList.forEach(items -> {
      if (items.getItemsH() != null && !items.getItemsH().isEmpty()) {
        sortTasklistByItemDisplayOrder(items.getItemsH());
      }
    });
    return itemsList;
  }

  /**
   * Print to pdf base on model provided.
   *
   * @param model pug model.
   * @param workingDir working directory.
   * @param filename pdf file name.
   * @param template pug template.
   * @throws ClassDirectException value.
   */
  public final void printToPdf(final Map<String, Object> model, final String workingDir, final String filename,
      final String template) throws ClassDirectException {
    try {
      PdfUtils.createPdfFromTemplate(model, workingDir, filename, template);
      PdfUtils.createWatermark(workingDir + File.separator + filename);
    } catch (SAXException | IOException | DocumentException | ParserConfigurationException exception) {
      throw new ClassDirectException("Failed to create pdf file: " + exception);
    }

  }

}
