package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.PmsTaskListExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * @author VKolagutla
 *
 */
public interface PmsTaskListExportService {

  /**
   * Generate PMS export PDF for given asset.
   *
   * @param query query.
   * @return file path.
   * @throws ClassDirectException exception if any.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadPmsTaskListInfo(PmsTaskListExportDto query) throws ClassDirectException;

}
