package com.baesystems.ai.lr.cd.service.export.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.parboiled.common.StringUtils;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;

/**
 * Utility method for pug/jade template.
 *
 * @author yng
 *
 */
public class TemplateHelper {

  /**
   * Date formatter.
   *
   * @param date value.
   * @return formatted date.
   */
  public final String formatDate(final Date date) {
    final DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
    return formatter.format(date);
  }

  /**
   * Print Date formatter.
   *
   * @param date value.
   * @return formatted date.
   */
  public final String formatPrintDate(final Date date) {
    final DateFormat formatter = new SimpleDateFormat("E, MMM dd, yyyy");
    return formatter.format(date);
  }

  /**
   * Left zero padding for job Id.
   *
   * @param id value.
   * @return padded id.
   */
  public final String leftZeroPaddingForJobId(final Long id) {
    return String.format("%07d", id);
  }

  /**
   * Left zero padding and comma separated for job id.
   *
   * @param jobs list.
   * @return padded concatenated ids.
   */
  public final String leftZeroPaddingForJobId(final List<JobHDto> jobs) {
    final List<String> idList = jobs.parallelStream().map(JobHDto::getJobNumber).collect(Collectors.toList());
    return StringUtils.join(idList, ", ");
  }

  /**
   * convert the input double value to long to remove decimals and return as string.
   *
   * @param inputValue double
   * @return string value without decimals.
   */
  public final String removeDecimals(final Double inputValue) {
     String returnValue;
     if (inputValue != null) {
          long convertedValue = Math.round(inputValue);
          returnValue = String.valueOf(convertedValue);
      } else {
          returnValue = "-";
      }
      return returnValue;
  }
}

