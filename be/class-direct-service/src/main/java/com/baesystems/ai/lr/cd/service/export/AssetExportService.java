package com.baesystems.ai.lr.cd.service.export;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Generates asset export PDF and return an encoded identifier that can use to trigger file
 * download.
 *
 * @author yng
 * @author YWearn 2017-05-17
 *
 */
public interface AssetExportService {

  /**
   * Generates the assets PDF file in the S3 storage and return an encoded identifier string which
   * the invoker can used to trigger download.
   *
   * @param query the export configuration that defined which assets to be export into PDF.
   * @param user the user profile.
   * @return the encoded identifier string that invoker can used to trigger download.
   * @throws ClassDirectException if any error on external API to get the requested asset(s)
   *         details.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadAssetInfo(AssetExportDto query, UserProfiles user) throws ClassDirectException;

  /**
   * Generates the assets PDF/CSV file in the S3 storage and return an encoded identifier token
   * which the invoker can used to trigger download.
   *
   * @param userId The login user identifier.
   * @param query The export configuration that defined which assets to be export into PDF.
   * @return The encoded identifier string that invoker can used to trigger download.
   * @throws ClassDirectException If any error on external API to fetch the requested asset(s)
   *         details.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  ExportStatusDto exportAssetAsync(final String userId, final AssetExportDto query) throws ClassDirectException;


  /**
   * Updates the export job status.
   *
   * Notes: Used internal to update the export status into cache through Spring cache abstraction.
   *
   * @param userId The login user identifier.
   * @param status The export job status model.
   * @return The export job status model.
   */
  @CachePut(
      cacheNames = CacheConstants.CACHE_ASSETS_EXPORT,
      key = "'" + CacheConstants.PREFIX_USER + "'.concat(#userId).concat('"
          + CacheConstants.INFIX_EXPORT + "').concat(#status.jobId)",
      condition = "#userId != null"
  )
  ExportStatusDto updateExportStatus(final String userId, final ExportStatusDto status);

  /**
   * Fetches the export job status for a given user and unique job id.
   *
   * @param userId The login user identifier.
   * @param jobId The export job unique identifier.
   * @return The export job status model.
   * @throws ClassDirectException If the given user id and job id not found.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  @Cacheable (
      cacheNames = CacheConstants.CACHE_ASSETS_EXPORT,
      key = "'" + CacheConstants.PREFIX_USER + "'.concat(#userId).concat('"
          + CacheConstants.INFIX_EXPORT + "').concat(#jobId)",
      condition = "#userId != null"
  )
  ExportStatusDto fetchExportStatus(final String userId, final String jobId) throws ClassDirectException;

    /**
     * Fetch the asset export status.
     *
     * @param userId
     *            The login user identifier.
     * @param jobId
     *            The export job unique identifier.
     * @return The export job status model.
     * @throws ClassDirectException
     *             If the export status code other than 200 and 202.
     */
    ExportStatusDto fetchAssetExportStatus(final String userId, final String jobId) throws ClassDirectException;

}
