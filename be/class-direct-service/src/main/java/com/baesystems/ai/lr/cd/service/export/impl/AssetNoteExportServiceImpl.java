package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.export.AssetNoteExportService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Provides the asset note export service for generating, and downloading asset note report.
 *
 * @author yng
 */
@Service(value = "AssetNoteExportService")
@Loggable(Loggable.DEBUG)
public class AssetNoteExportServiceImpl extends BasePdfExport<ExportPdfDto> implements AssetNoteExportService {
  /**
   * The logger instantiated for {@link AssetNoteExportServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetNoteExportServiceImpl.class);

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link AssetNoteService} from Spring context.
   */
  @Autowired
  private AssetNoteService assetNoteService;

  /**
   * The report name prefix for asset note.
   */
  private static final String PDF_PREFIX_ASSET_NOTE = "AN";

  /**
   * The pug template file name for asset note.
   */
  private static final String TEMPLATE_ASSET_NOTE = "an_listing_export_template.jade";

  /**
   * The directory name in Amazon S3 where the generated report to be uploaded.
   * This variable in only use at {@link BasePdfExport}.
   * <br>
   * For more information see {@link AssetNoteExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "codicil_export";

  /**
   * This field is use as variable in pug template that store list of asset note object.
   */
  private static final String ANS = "ans";

  /**
   * This field is use as variable in pug template that store asset object.
   */
  private static final String ASSET = "asset";

  /**
   * This field is use {@link BasePdfExport} as a part of file name generation.
   */
  private static final String FILE_SECTION = "fileSection";

  /**
   * This field is use to check if the pdf is single page or multi page.
   */
  private static final String ONLY_ONE_PAGE = "onlyOnePage";

  /**
   * The file generation working directory.
   */
  @Value("${codicil.export.temp.dir}")
  private String tempDir;

  @Override
  protected final String generateFile(final Map<String, Object> model, final ExportPdfDto query,
      final UserProfiles user, final String workingDirAN, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Retrieving AN object...");

      // create file section name
      model.put(FILE_SECTION, query.getCode());

      // get asset information
      final AssetHDto asset = assetService.assetByCode(user.getUserId(), query.getCode());
      if (asset == null) {
        throw new ClassDirectException("Asset not found.");
      } else {
        model.put(ASSET, asset);
      }

      // get AI by asset id.
      final PageResource<AssetNoteHDto> noteResource = assetNoteService.getAssetNote(null, null, asset.getId());
      List<AssetNoteHDto> notes = noteResource.getContent();

      // filter notes by ids
      if (notes != null && !notes.isEmpty()) {
        if (query.getItemsToExport() != null && !query.getItemsToExport().isEmpty()) {
          notes = notes.stream().filter(item -> query.getItemsToExport().contains(item.getId()))
              .collect(Collectors.toList());
        }

        // sort by due date and ID
        notes.sort(ServiceUtils.assetNotesDefaultComparator());
      }

      model.put(ANS, notes);

      String filename = generatePdf(PDF_PREFIX_ASSET_NOTE, query.getCode(), current,
          TEMPLATE_ASSET_NOTE, workingDirAN, model);

      int pageCountForAN = ServiceUtils.getPageCount(workingDirAN, filename);
     //if the pdf has only one page then we will recall the page to put ONLY_ONE_PAGE flag in the model
      if (pageCountForAN == 1) {
        model.put(ONLY_ONE_PAGE, "true");
        filename =
            generatePdf(PDF_PREFIX_ASSET_NOTE, query.getCode(), current, TEMPLATE_ASSET_NOTE, workingDirAN, model);
      }

      return filename;
    } catch (JadeException | IOException | ParserConfigurationException | SAXException | DocumentException exception) {
      throw new ClassDirectException(exception);
    }
  }

  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return this.tempDir;
  }

}
