package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.export.TaskListExportDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * @author VKolagutla
 *
 */
public interface TaskListExportService {

  /**
   * @param query TaskListExportDto.
   * @return file path.
   * @throws ClassDirectException exception if any.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadTaskListInfo(TaskListExportDto query) throws ClassDirectException;

}
