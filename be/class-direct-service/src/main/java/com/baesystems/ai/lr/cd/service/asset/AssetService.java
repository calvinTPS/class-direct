package com.baesystems.ai.lr.cd.service.asset;

import java.io.IOException;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.IhsAssetClientDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.UserPersonalInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ServiceScheduleDto;
import com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.enums.OrderOptionEnum;
import com.baesystems.ai.lr.cd.be.enums.SortOptionEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.dto.paging.PageResource;


/**
 * Interface to define methods of all Asset transactions.
 *
 * @author VMandalapu
 */
public interface AssetService {
  /**
   * @param userId id.
   * @param assetQuery query.
   * @param page page.
   * @param size size.
   * @return AssetPageResource pageResource.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  AssetPageResource getAssets(final String userId, final AssetQueryHDto assetQuery, final Integer page,
      final Integer size) throws ClassDirectException, IOException;

  /**
   * Get assets filtered by {@link AssetQueryHDto}.
   *
   * @param userId value.
   * @param page page number.
   * @param size result size.
   * @param sort sort by option.
   * @param order order ascending or descending.
   * @param query filter.
   * @return page resource for {@link AssetHDto}.
   * @throws ClassDirectException when exception.
   * @throws IOException exception.
   */
  @PreAuthorize("authenticated")
  AssetPageResource getAssetsQueryPageResource(String userId, Integer page, Integer size, String order, String sort,
      AssetQueryHDto query) throws ClassDirectException, IOException;

  /**
   * Lookup asset by asset code.
   *
   * @param userId user id.
   * @param assetCode asset code.
   * @return single asset.
   * @throws ClassDirectException when no record found.
   */
  @Cacheable(
      cacheNames = CacheConstants.CACHE_ASSETS_BY_USER, key = "'" + CacheConstants.PREFIX_USER
          + "'.concat(#userId).concat('" + CacheConstants.INFIX_ASSETS + "').concat(#assetCode)",
      condition = "#userId != null")
  AssetHDto assetByCode(final String userId, final String assetCode) throws ClassDirectException;


  /**
   * Lookup Eor asset by asset code.
   *
   * @param userId user id.
   * @param assetCode asset code.
   * @return single eor asset.
   * @throws ClassDirectException when no record found.
   */
  AssetHDto eorAssetByCode(final String userId, final String assetCode) throws ClassDirectException;


  /**
   * Returns list of paginated assets has same lead IMO(Technical sister assets).
   *
   * @param userId the unique identification of user.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @param leadImo the IMO number to fetch technical sisters of asset.
   * @param assetCode the assetCode to fetch technical sisters of asset.
   * @return paginated list of technical sister assets.
   * @throws ClassDirectException if any execution fail or any invalid input parameters.
   * @throws IOException if API call fails.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  AssetPageResource getTechnicalSister(final String leadImo, final String userId, final String assetCode,
      final Integer page, final Integer size) throws ClassDirectException, IOException;

  /**
   * Gets count of registry book sisters by leadImo.
   *
   * @param leadShip the leadShip IMO number to fetch register sisters of asset.
   * @param userId the unique identification of user.
   * @param assetCode the assetCode to fetch register book sisters of asset.
   * @return count.
   * @throws ClassDirectException if mast API call fails.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  Long getRegisterBookSistersCount(final String leadShip, final String userId, final String assetCode)
      throws ClassDirectException;

  /**
   * Gets count of technical sisters by leadImo.
   *
   * @param leadImo the IMO number to fetch technical sisters of asset.
   * @param userId the unique identification of user.
   * @param assetCode the assetCode to fetch technical sisters of asset.
   * @return count.
   * @throws ClassDirectException if mast api call fails.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  Long getTechnicalSistersCount(final String leadImo, final String userId, final String assetCode)
      throws ClassDirectException;

  /**
   * Returns list of paginated assets(both IHS and mast) has same lead IMO(Register book sister
   * assets).
   *
   * @param userId the unique identification of user.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @param leadShip the leadShip IMO number to fetch register sisters of asset.
   * @param assetCode the assetCode to fetch register book sisters of asset.
   * @return paginated list of technical sister assets.
   * @throws ClassDirectException if any execution fail or any invalid input parameters.
   * @throws IOException if API call fails.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  AssetPageResource getRegisterBookSister(final String leadShip, final String userId, final String assetCode,
      final Integer page, final Integer size) throws ClassDirectException, IOException;

  /**
   * @param userId userid.
   * @param assetCode assetid.
   * @return value.
   * @throws ClassDirectException exception.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  @CacheEvict(cacheNames = CacheConstants.CACHE_ASSETS_BY_USER, key = "'" + CacheConstants.PREFIX_USER
      + "'.concat(#userId).concat('" + CacheConstants.INFIX_ASSETS + "').concat(#assetCode)")
  StatusDto createFavourite(final String userId, final String assetCode) throws ClassDirectException;

  /**
   * @param userId userid.
   * @param assetCode assetid.
   * @return value.
   * @throws ClassDirectException exception.
   */
  @CacheEvict(cacheNames = CacheConstants.CACHE_ASSETS_BY_USER, key = "'" + CacheConstants.PREFIX_USER
      + "'.concat(#userId).concat('" + CacheConstants.INFIX_ASSETS + "').concat(#assetCode)")
  StatusDto deleteFavourite(final String userId, final String assetCode) throws ClassDirectException;

  /**
   * @param assetId id.
   * @param queryHDto intype.
   * @return value.
   * @throws IOException exception.
   * @throws ClassDirectException exception.
   */
  CodicilActionableHDto getCodicils(final Long assetId, final CodicilDefectQueryHDto queryHDto)
      throws IOException, ClassDirectException;

  /**
   * Validate imonumber if asset exist based on imo number. If record exists, returns asset id.
   * Otherwise, return null.
   *
   * @param imoNumber imo number input parameter.
   * @return Asset id.
   * @throws ClassDirectException Class-Direct Exception.
   */
  Long getAssetIdByImoNumber(String imoNumber) throws ClassDirectException;

  /**
   * Validate imonumber if asset exist based on imo number. If record exists, returns asset.
   * Otherwise, return null.
   *
   * @param imoNumber imo number input parameter.
   * @return Asset id.
   * @throws ClassDirectException Class-Direct Exception.
   */
  AssetHDto getAssetByImoNumber(String imoNumber) throws ClassDirectException;

  /**
   * get services for asset.
   *
   * @param page number.
   * @param size size.
   * @param assetId id.
   * @param query query.
   * @return list of services.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  PageResource<ScheduledServiceHDto> getServices(final Integer page, final Integer size, final Long assetId,
      final ServiceQueryDto query) throws ClassDirectException, IOException;

  /**
   * Calculate asset overall status.
   *
   * @param asset dto.
   * @return due status.
   */
  DueStatus calculateOverAllStatus(final AssetHDto asset);

  /**
   * Add filter based on user type.
   *
   * @param userProfiles user.
   * @param assetQuery query.
   * @return mast query builder.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  MastQueryBuilder populateAssetQuery(final UserProfiles userProfiles, final MastQueryBuilder assetQuery)
      throws IOException, ClassDirectException;

  /**
   * Provide detailed information for an Asset such as "Registry Information", "Principal Dimensions
   * ", "Equipment Information", and "Ownership and Management Details".
   *
   * @param imoNumber imo Number.
   * @param userId user Id.
   * @param isEor isEor.
   * @return AssetDetailsDto Asset Details Dto.
   * @throws ClassDirectException ClassDirect Exception.
   */
  AssetDetailsDto getAssetDetailsByAssetId(final Long imoNumber, final String userId, final Boolean isEor)
      throws ClassDirectException;

  /**
   * Perform mast-ihs query for logged in user.
   *
   * @param query query dto object.
   * @param sort sort option, nullable.
   * @param order order option, nullable.
   * @return list of asset hdto or empty list.
   * @throws ClassDirectException when unexpected error occurs.
   */
  List<AssetHDto> mastIhsQuery(MastQueryBuilder query, SortOptionEnum sort, OrderOptionEnum order)
      throws ClassDirectException;

  /**
   * Perform mast-ihs query.
   *
   * @param query query dto object.
   * @param sort sort option, nullable.
   * @param order order option, nullable.
   * @param hydrateAssets Whether or not to hydrate assets.
   * @return list of asset hdto or empty list.
   * @throws ClassDirectException when unexpected error occurs.
   */
  List<AssetHDto> executeMastIhsQuery(MastQueryBuilder query, SortOptionEnum sort, OrderOptionEnum order,
      Boolean hydrateAssets) throws ClassDirectException;

  /**
   * Hydrate asset.
   *
   * @param asset object.
   * @return hydrated asset object.
   * @throws ClassDirectException value.
   */
  @Cacheable(cacheNames = CacheConstants.CACHE_ASSETS,
      key = "'" + CacheConstants.PREFIX_ASSET + "'.concat(#asset.code)")
  AssetHDto hydrateAssetData(AssetHDto asset) throws ClassDirectException;

  /**
   * Perform mast-ihs query with pagination options for logged in user.
   *
   * @param query query object.
   * @param page page number.
   * @param size page size.
   * @param sort sort options.
   * @param order order options.
   * @return asset page resource.
   * @throws ClassDirectException when error.
   */
  AssetPageResource mastIhsQuery(MastQueryBuilder query, Integer page, Integer size, SortOptionEnum sort,
      OrderOptionEnum order) throws ClassDirectException;

  /**
   * Delegate execution to mast-ihs query with pagination.
   *
   * @param query query builder.
   * @param page page number.
   * @param size page size.
   * @param sort sort option.
   * @param order order option.
   * @return asset page resource.
   * @throws ClassDirectException when error.
   */
  AssetPageResource executeMastIhsQuery(MastQueryBuilder query, Integer page, Integer size, SortOptionEnum sort,
      OrderOptionEnum order) throws ClassDirectException;

  /**
   * to get list of customers by imoNumber.
   *
   * @param imoNumber imoNumber.
   * @return value.
   * @throws ClassDirectException exception.
   */
  IhsAssetClientDto getCustomersByImoNumber(final String imoNumber) throws ClassDirectException;

  /**
   * AssetQuery to MastQuery converter.
   *
   * @param assetQuery object.
   * @return mast query object.
   */
  MastQueryBuilder convert(final AssetQueryHDto assetQuery);

  /**
   * Populate for given AssetHDto.
   *
   * @param name Contact Name.
   * @param code Contact Code.
   * @return EquipmentDetailsDto.
   * @throws ClassDirectException ClassDirectException.
   */
  UserPersonalInformationDto getPersonalInformation(final String name, final String code) throws ClassDirectException;

  /**
   * Add user filter on query.
   *
   * @param query builder.
   * @param userProfile The user profile.
   * @throws RecordNotFoundException when no user found.
   */
  void filterByUser(final MastQueryBuilder query, final UserProfiles userProfile) throws RecordNotFoundException;

  /**
   * get services for asset (support future due date calculation).
   *
   * @param assetId id.
   * @param query query.
   * @return list of services.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   * @throws CloneNotSupportedException exception.
   */
  ServiceScheduleDto getServicesWithFutureDueDateSupport(final Long assetId, final ServiceQueryDto query)
      throws ClassDirectException, IOException, CloneNotSupportedException;

  /**
   * Gets assets by query builder.
   *
   * @param userId the user unique id.
   * @param assetQuery the query with search filters.
   * @param assetQueryBuilder the reusable asset query builder.
   * @param page the number of pages to be returned.
   * @param size the page size to be returned.
   * @return AssetPageResource paginated assets.
   * @throws ClassDirectException error if mast api fails or error in execution flow.
   * @throws IOException error if get subfleet api fails.
   */
  AssetPageResource getAssetByQueryBuilder(final String userId, final AssetQueryHDto assetQuery,
      final MastQueryBuilder assetQueryBuilder, final Integer page, final Integer size)
      throws ClassDirectException, IOException;

  /**
   * Provides detailed information for an Asset such as "Registry Information", "Principal
   * Dimensions ", "Equipment Information", and "Ownership and Management Details" by asset.
   *
   * @param asset the asset model.
   * @param userId the user Id.
   * @return Asset Details.
   * @throws ClassDirectException if any error in execution flow/ mast api call fail.
   */
  AssetDetailsDto getAssetDetailsByAsset(final AssetHDto asset, final String userId) throws ClassDirectException;

  /**
   * Apply's user filter to asset query.
   *
   * @param query the asset query DTO.
   * @param user the user profile object.
   * @throws ClassDirectException if any error in executing asset query.
   */
  void filterMastAssetByUser(final AssetQueryHDto query, final UserProfiles user) throws ClassDirectException;


  /**
   * Creates or remove user favorite from FAVOURITES table.
   *
   * @param userId the unique identifier of user.
   * @param favourites the object holds assetCodes to be removed and inserted.
   * @return status value.
   * @throws ClassDirectException if any execution fails.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  String updateFavouriteList(final String userId, final FavouritesDto favourites) throws ClassDirectException;

  /**
   * Gets assets by paginating with fixed page size for a given asset query or mast query builder used in notification.
   *
   * @param userId the unique identifier of user.
   * @param assetQuery assetQuery the query with search filters.
   * @param queryBuilder the reusable asset query builder.
   * @return the list of assets.
   * @throws ClassDirectException if any error in executing mast and ihs query.
   */
  List<AssetHDto> getAssetsByPagination(final String userId, final AssetQueryHDto assetQuery,
      final MastQueryBuilder queryBuilder) throws ClassDirectException;
}
