package com.baesystems.ai.lr.cd.be.service.utils;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.Setter;

/**
 * Provides service model used in survey email.
 *
 * @author syalavarthi
 *
 */
public class ServiceDemo {

  /**
   * serviceName.
   */
  @Getter
  @Setter
  private String serviceName;
  /**
   * The dueDate in string format.
   */
  @Getter
  @Setter
  private String dueDate;
  /**
   * the due date in date format.
   */
  @Getter
  @Setter
  @SuppressFBWarnings(value = {"EI_EXPOSE_REP2", "EI_EXPOSE_REP"}, justification = "not modifying the date instance")
  private Date origionalDueDate;
  /**
   * assignedDate.
   */
  @Getter
  @Setter
  private String assignedDate;
  /**
   * rangeDates.
   */
  @Getter
  @Setter
  private List<String> rangeDates;
  /**
   * postponementDate.
   */
  @Getter
  @Setter
  private String postponementDate;
  /**
   * dueStatus.
   */
  @Getter
  @Setter
  private String dueStatus;
  /**
   * productId.
   */
  @Getter
  @Setter
  private Long productId;
  /**
   * productName.
   */
  @Getter
  @Setter
  private String productName;
  /**
   * productDisplayOrder.
   */
  @Getter
  @Setter
  private Integer productDisplayOrder;
  /**
   * occurrenceNumber.
   */
  @Getter
  @Setter
  private Integer occurrenceNumber;
  /**
   * serviceDueDate.
   */
  @Getter
  @Setter
  @SuppressFBWarnings(value = {"EI_EXPOSE_REP"}, justification = "not modifying the date instance")
  private Date serviceDueDate;
  /**
   * catalogueDisplayOrder.
   */
  @Getter
  @Setter
  private Integer catalogueDisplayOrder;
  /**
   * productTypeId.
   */
  @Getter
  @Setter
  private Long productTypeId;
  /**
   * productTypeName.
   */
  @Getter
  @Setter
  private String productTypeName;
  /**
   * Service Title(service code +service occurrence number +service name).
   */
  @Getter
  @Setter
  private String title;
  /**
   * Constructs service demo with serviceName, dueDate, assignedDate, rangeDates, postponementDate,
   * dueStatus, prodId, prodName, productDisplayOrder,occurrenceNumber, serviceDueDate,
   * catalogueDisplayOrder.
   *
   * @param service the service object.
   * @param dueDateP the service due date in string format.
   * @param origionalDueDateP the service due date.
   * @param assignedDateP the service assigned date.
   * @param rangeDatesP the service lower range and upper range dates.
   * @param postponementDateP the service postponementDate.
   * @param productTypeNameP the product type name.
   * @param titleP Service Title.
   */
  @SuppressFBWarnings(value = {"EI_EXPOSE_REP2"}, justification = "not modifying the date instance")
  public ServiceDemo(final ScheduledServiceHDto service, final String dueDateP, final Date origionalDueDateP,
      final String assignedDateP, final List<String> rangeDatesP, final String postponementDateP,
      final String productTypeNameP, final String titleP) {
    this.serviceName = service.getServiceCatalogueName();
    this.dueDate = dueDateP;
    this.origionalDueDate = origionalDueDateP;
    this.assignedDate = assignedDateP;
    this.rangeDates = rangeDatesP;
    this.postponementDate = postponementDateP;
    this.dueStatus = service.getDueStatusH().getIcon();
    this.productId = service.getServiceCatalogueH().getProductCatalogue().getId();
    this.productName = service.getServiceCatalogueH().getProductCatalogue().getName();
    this.productDisplayOrder = service.getServiceCatalogueH().getProductCatalogue().getDisplayOrder();
    this.occurrenceNumber = service.getOccurrenceNumber();
    this.serviceDueDate = service.getDueDate();
    this.catalogueDisplayOrder = service.getServiceCatalogueH().getDisplayOrder();
    this.productTypeId = service.getServiceCatalogueH().getProductGroupH().getProductType().getId();
    this.productTypeName = productTypeNameP;
    this.title = titleP;
  }

}


