package com.baesystems.ai.lr.cd.be.utils;

import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.baesystems.ai.lr.cd.be.domain.dto.IDueStatusDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * @author VKolagutla
 *
 */
public final class AssetOverAllStatusUtils {

  /**
   * Private constructor AssetOverAllStatusUtils.
   */
  private AssetOverAllStatusUtils() {
    super();
  }

  /**
   * Logger for AssetOverAllStatusUtils class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetOverAllStatusUtils.class);

  /**
   * Provides conversion of MAST DTO object to CD DTO object and trigger the due status calculation.
   *
   * @param mastObj the mast DTO object.
   * @param cdObj the correspondent CD DTO object with hydrated fields.
   * @param dueStatusCalculator the DTO object due status calculation implementation.
   * @param <T> the CD DTO class.
   * @return the instance of CD DTO with properties value copied from MAST DTO and due status populated.
   * @throws ClassDirectException if error within properties value copy from MAST to CD DTO.
   */
  public static <T> T mastToCdDto(final Object mastObj, final T cdObj, final DueStatusCalculator<T> dueStatusCalculator)
      throws ClassDirectException {
    try {
      if (mastObj != null && cdObj != null) {
        BeanUtils.copyProperties(mastObj, cdObj);
        final DueStatus dueStatus = dueStatusCalculator.calculateDueStatus(cdObj);
        final IDueStatusDto dueStatusDto = (IDueStatusDto) cdObj;
        dueStatusDto.setDueStatusH(dueStatus);
        return cdObj;
      }
    } catch (Exception e) {
      throw new ClassDirectException("Exception in copying mast to cd: " + e.getMessage());
    }
    return null;
  }

  /**
   * @param dueStatusDto dueStatus.
   * @return dueStatus.
   */
  public static DueStatus getDueStatus(final IDueStatusDto dueStatusDto) {
      return Optional.ofNullable(dueStatusDto).map(IDueStatusDto::getDueStatusH).orElse(null);
  }
}
