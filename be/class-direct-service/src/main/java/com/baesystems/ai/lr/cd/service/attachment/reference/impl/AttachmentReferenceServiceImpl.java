package com.baesystems.ai.lr.cd.service.attachment.reference.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AttachmentReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;
import com.baesystems.ai.lr.cd.service.attachment.reference.AttachmentReferenceService;

import retrofit2.Call;

/**
 * Provides services to get list of attachment types, single attachment type, list of attachment
 * categories and single attachment category.
 *
 * @author syalavarthi.
 *
 */
@Service("attachmentReferenceService")
public class AttachmentReferenceServiceImpl implements AttachmentReferenceService, InitializingBean {
  /**
   * The logger for AttachmentReferenceService.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentReferenceService.class);
  /**
   * {@value #LOG_MESSAGE} is the Logger message.
   */
  private static final String LOG_MESSAGE = "Can't find any element matched id {}.";
  /**
   * The {@link AttachmentReferenceRetrofitService} from Spring context.
   */
  @Autowired
  private AttachmentReferenceRetrofitService attachmentsRefRetrofit;

  @ResourceProvider
  @Override
  public final List<AttachmentTypeHDto> getAttachmentTypes() {
    List<AttachmentTypeHDto> attachmentTypes = new ArrayList<>();

    final Call<List<AttachmentTypeHDto>> caller = attachmentsRefRetrofit.getAttachmentTypes();
    try {
      attachmentTypes = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Retrofit execution failed.", ioe);
    }

    return attachmentTypes;
  }

  @ResourceProvider
  @Override
  public final AttachmentTypeHDto getAttachmentType(final Long id) {
    AttachmentTypeHDto attachmentType = null;

    try {
      final List<AttachmentTypeHDto> attachmentTypes = self.getAttachmentTypes();
      attachmentType = attachmentTypes.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.error(LOG_MESSAGE, id);
    }

    return attachmentType;
  }

  @ResourceProvider
  @Override
  public final List<AttachmentCategoryHDto> getAttachmentCategories() {
    List<AttachmentCategoryHDto> attachmentCategorys = new ArrayList<>();

    final Call<List<AttachmentCategoryHDto>> caller = attachmentsRefRetrofit.getAttachmentCategories();
    try {
      attachmentCategorys = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Retrofit execution failed.", ioe);
    }

    return attachmentCategorys;
  }

  @ResourceProvider
  @Override
  public final AttachmentCategoryHDto getAttachmentCategory(final Long id) {
    AttachmentCategoryHDto attachmentCategory = null;

    try {
      final List<AttachmentCategoryHDto> attachmentCategorys = self.getAttachmentCategories();
      attachmentCategory = attachmentCategorys.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.error(LOG_MESSAGE, id);
    }

    return attachmentCategory;
  }

  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   *
   */
  private AttachmentReferenceService self = null;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(AttachmentReferenceService.class);
  }
}
