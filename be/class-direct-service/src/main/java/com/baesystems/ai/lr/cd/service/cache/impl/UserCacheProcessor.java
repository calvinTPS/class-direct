package com.baesystems.ai.lr.cd.service.cache.impl;

import java.util.Map;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.baesystems.ai.lr.cd.service.cache.CacheOperationProcessor;

/**
 * Cache Operation Processor for User.
 *
 * @author RKaneysan.
 */
public class UserCacheProcessor implements CacheOperationProcessor {

  /**
   * Default serial version UID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserCacheProcessor.class);

  /**
   */
  @Value("${record.not.found.with.id}")
  private String userNotFoundMessage;

  /**
   * Injected User Profile Dao.
   */
  @Autowired
  private transient UserProfileDao userProfileDao;

  /**
   * Cache preProcessor for User Profile Service.
   *
   * @param cache spring Cache.
   * @param cacheKey spring Cache Key.
   * @param context container to hold values to be updated.
   */
  @Override
  public final void preProcessor(@NotNull final Cache cache, @NotNull final Object cacheKey,
      @NotNull final Map<String, Object> context) {
    LOGGER.trace("Pre-Processor : Cache Key - {}", cacheKey);
    Optional.ofNullable(cache.get(cacheKey)).ifPresent(cacheValue -> {
      LOGGER.debug("Cache - key : {}, value : {}", cacheKey, cacheValue.get());
      context.put((String) cacheKey, cacheValue.get());
    });
  }

  /**
   * Cache postProcessor for User Profile Service.
   *
   * @param cache spring Cache.
   * @param cacheKey spring Cache Key.
   * @param context container to hold values to be updated.
   */
  @Override
  public final void postProcessor(@NotNull final Cache cache, @NotNull final Object cacheKey,
      @NotNull final Map<String, Object> context) {
    LOGGER.trace("Post-Processor : Cache Key - {}", cacheKey);
    final UserProfiles cacheUserProfile = (UserProfiles) context.get(cacheKey);
    if (cacheUserProfile != null) {
      final String userId = cacheUserProfile.getUserId();
      try {
        final UserProfiles latestUserProfile = userProfileDao.getUser(userId);
        final String[] externalFields = new String[] {"lastLogin"};
        final boolean isExternalFieldChange =
            UserProfileServiceUtils.isAnyFieldChanged(externalFields, latestUserProfile, cacheUserProfile);
        if (!isExternalFieldChange) {
          UserProfileServiceUtils.updateSignedInUserInformationFromCacheAndMast(latestUserProfile, cacheUserProfile);
          cache.put(cacheKey, latestUserProfile);
          LOGGER.debug("Post-Processor: Re-Cached - Cache Key : {}", cacheKey);
        }
      } catch (final RecordNotFoundException recordNotFoundException) {
        LOGGER.error(userNotFoundMessage, userId, recordNotFoundException);
      }
    }
  }

}

