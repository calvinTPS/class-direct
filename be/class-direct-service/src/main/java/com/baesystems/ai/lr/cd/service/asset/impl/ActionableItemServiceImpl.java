package com.baesystems.ai.lr.cd.service.asset.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.DefectService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.enums.CodicilCategory;
import com.jcabi.aspects.Loggable;

/**
 * Provides service for querying actionable item.
 *
 * @author yng
 *
 */
@Service(value = "ActionableItemService")
@Loggable(Loggable.DEBUG)
public class ActionableItemServiceImpl implements ActionableItemService {
  /**
   * The logger instantiated for {@link ActionableItemServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ActionableItemServiceImpl.class);

  /**
   * The {@link DefectService} from Spring context.
   */
  @Autowired
  private DefectService defectService;

  /**
   * The {@link AssetRetrofitService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The due soon threshold default value in month.
   */
  private static final Long DUE_SOON_TH = 3L;

  /**
   * The imminent threshold default value in month.
   */
  private static final Long IMMINENT_TH = 1L;

  @RestrictedAsset
  @Override
  public final PageResource<ActionableItemHDto> getActionableItem(final Integer page, final Integer size,
      @AssetID final Long assetId) throws ClassDirectException {
    PageResource<ActionableItemHDto> pageResource = null;

    if (SecurityUtils.isEquasisThetisUser()) {
      Predicate<ActionableItemHDto> filter = actionableItem -> {
        return CodicilCategory.AI_STATUTORY.getValue().equals(actionableItem.getCategory().getId());
      };
      pageResource = PaginationUtil.paginate(getActionableItems(assetId, filter), page, size);
    } else {
      pageResource = PaginationUtil.paginate(getActionableItems(assetId, null), page, size);
    }

    return pageResource;
  }

  @Override
  public final List<ActionableItemHDto> getActionableItems(final long assetId,
      final Predicate<ActionableItemHDto> filter) throws ClassDirectException {
    final List<ActionableItemHDto> contentH = new ArrayList<ActionableItemHDto>();

    List<ActionableItemHDto> response = null;
    try {
      response = assetServiceDelegate.getActionableItemDto(assetId).execute().body();
    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getActionableItem.", ioException);
      throw new ClassDirectException(
          "Fail to fetch actionable item for asset " + assetId + " : " + ioException.getMessage());
    }

    if (response != null && !response.isEmpty()) {
      final Predicate<ActionableItemHDto> aiFilter = Optional.ofNullable(filter).orElse((ai) -> {
        return true;
      });
      final List<Callable<ActionableItemHDto>> enrichTaskList = response.parallelStream().filter(aiFilter).map(ai -> {
        Callable<ActionableItemHDto> enrichTask = () -> {
          if (ai.getAssetItem() != null) {
            try {
              final LazyItemHDto assetItem =
                  assetServiceDelegate.getAssetItemDto(assetId, ai.getAssetItem().getId()).execute().body();
              ai.setAssetItemH(assetItem);
            } catch (final Exception e) {
              LOGGER.error("Asset item with id:{} not found.", ai.getAssetItem().getId(), e);
            }
          }

          Resources.inject(ai);
          return ai;
        };
        return enrichTask;
      }).collect(Collectors.toList());

      final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
      try {
        List<Future<ActionableItemHDto>> aiFutureList = executor.invokeAll(enrichTaskList);
        for (Future<ActionableItemHDto> aiFuture : aiFutureList) {
          contentH.add(aiFuture.get());
        }
      } catch (final InterruptedException | ExecutionException e) {
        LOGGER.error("Error while enrich Actionable Item for asset id : {}", assetId, e);
        throw new ClassDirectException("Fail to actionable item for asset " + assetId + " : " + e.getMessage());
      } finally {
        executor.shutdown();
      }
    }
    return contentH;
  }

  @Override
  public final List<ActionableItemHDto> getActionableItemsByQuery(final long assetId,
      final CodicilDefectQueryHDto query) throws ClassDirectException {

    List<ActionableItemHDto> response = null;
    try {
      response = assetServiceDelegate.getActionableItemQueryDto(query, assetId).execute().body();
    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getActionableItemsByQuery.", ioException);
      throw new ClassDirectException(
          "Fail to fetch actionable item for asset " + assetId + " : " + ioException.getMessage());
    }

    if (response != null && !response.isEmpty()) {
      response.forEach(item -> {
        Resources.inject(item);
      });
    }
    return response;
  }

  @Override
  public final DueStatus calculateDueStatus(final ActionableItemHDto actionableItemInput) {
    return calculateDueStatus(actionableItemInput, LocalDate.now());
  }

  // Calculate dueStatus for ActionableItems.
  @Override
  public final DueStatus calculateDueStatus(final ActionableItemHDto actionableItem, final LocalDate date) {

    DueStatus status = DueStatus.NOT_DUE;
    if (actionableItem.getDueDate() != null) {

      final LocalDate dueDate = DateUtils.getLocalDateFromDate(actionableItem.getDueDate());

      if (dueDate.isBefore(date)) {
        status = DueStatus.OVER_DUE;
      } else if (date.plusMonths(IMMINENT_TH).isAfter(dueDate)) {
        status = DueStatus.IMMINENT;
      } else if (actionableItem.getCategory() != null
          && !CodicilCategory.AI_STATUTORY.getValue().equals(actionableItem.getCategory().getId())
          && date.plusMonths(DUE_SOON_TH).isAfter(dueDate)) {
        status = DueStatus.DUE_SOON;
      } else if ((actionableItem.getCategory() != null)
          && (CodicilCategory.AI_STATUTORY.getValue().equals(actionableItem.getCategory().getId()))
          && (date.plusYears(1L).isAfter(dueDate))) {
        status = DueStatus.DUE_SOON;
      }
    }
    return status;
  }

  @RestrictedAsset
  @Override
  public final ActionableItemHDto getActionableItemByAssetIdActionableItemId(@AssetID final Long assetId,
      final long actionableItemId) throws ClassDirectException {
    ActionableItemHDto actionableItemResponse = null;

    try {
      final ActionableItemHDto actionableItem =
          assetServiceDelegate.getActionableItemByAssetIdActionableItemId(assetId, actionableItemId).execute().body();

      // get asset item data
      if (actionableItem.getAssetItem() != null) {
        try {
          final LazyItemHDto assetItem =
              assetServiceDelegate.getAssetItemDto(assetId, actionableItem.getAssetItem().getId()).execute().body();
          actionableItem.setAssetItemH(assetItem);
        } catch (final IOException exception) {
          LOGGER.error("Error executing getAssetItemDto({}): Id not found.", actionableItem.getAssetItem().getId(),
              exception);
        }
      }

      // get defect data is only class defect
      if (actionableItem.getDefect() != null) {
        try {
          final DefectHDto defect =
              defectService.getDefectByAssetIdDefectId(assetId, actionableItem.getDefect().getId());
          actionableItem.setDefectH(defect);
        } catch (final ClassDirectException exception) {
          LOGGER.error("Error executing getDefectByAssetIdDefectId({}): Id not found.",
              actionableItem.getDefect().getId(), exception);
        }
      }
      // hydrate job data
      try {
        if (actionableItem.getJob() != null) {
          JobHDto jobDto = jobServiceDelegate.getJobsByJobId(actionableItem.getJob().getId()).execute().body();
          actionableItem.setJobH(jobDto);
        }
      } catch (final Exception exception) {
        LOGGER.error("Error fetching job for actionable item {} . | Error Message: {}", actionableItem.getId(),
            exception.getMessage());
      }
      Resources.inject(actionableItem);

      actionableItemResponse = actionableItem;

    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getActionableItem.", ioException);
      throw new ClassDirectException(ioException);
    }

    return actionableItemResponse;
  }

}
