package com.baesystems.ai.lr.cd.service.audit.impl;

import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ACTIVE;
import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ARCHIVED;
import static com.monitorjbl.json.Match.match;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.parboiled.common.StringUtils;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dto.task.AuditPmsAbleTaskDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserEORDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum;
import com.baesystems.ai.lr.cd.be.processor.CustomJsonSerializerModule;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.baesystems.ai.lr.cd.service.audit.AuditHelper;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.monitorjbl.json.JsonView;

/**
 * Provides service for user audit management.
 *
 * @author VKolagutla
 * @author syalavarthi 14-06-2017.
 *
 */
@Service("auditService")
public class AuditServiceImpl implements AuditService {

  /**
   * The {@value #LOGGER} logger for AuditServiceImpl.
   */
  private static final Logger LOGGER = LogManager.getLogger(AuditServiceImpl.class);

  /**
   * The {@value #EMPTY_STRING} to be returned from {@link AuditHelper#getUpdateField(Object)} when
   * object is null.
   */
  private static final String EMPTY_STRING = "";

  /**
   * The {@value #JOINING} comma delimiter.
   */
  private static final String JOINING = ",";

  /**
   * The {@value #IMO} is used in create or update eor audit scenario to display imo number.
   */
  private static final String IMO = "IMO: ";

  /**
   * The {@value #TM_FLAG} is used in create or update eor audit scenario to display tm flag.
   */
  private static final String TM_FLAG = ", TM Flag: ";

  /**
   * The {@value #RESTRICT_ALL} is used to display in audit scenario if user moves all his accesible
   * assets to restricted and vice versa.
   */
  private static final String RESTRICT_ALL = " RESTRICT ALL: ";

  /**
   * The default exclusion rule.
   */
  private static final String EXCLUSION_RULE = "*";
  /**
   * The {@value #LAST_LOGIN} is used in update last login audit scenario to display.
   */
  private static final String LAST_LOGIN = "Last Login : ";

  /**
   * The auditScenarios.
   */
  private Map<AuditScenarioEnum, AuditHelper> auditScenarios = new HashMap<>();
  /**
   * The default object MAPPER.
   */
  private static final ObjectMapper MAPPER;

  static {
    MAPPER = new ObjectMapper();
    MAPPER.registerModule(new CustomJsonSerializerModule());
    MAPPER.setDateFormat(new ISO8601DateFormat());
  }

  /**
   * The audit service scenarios.
   */
  public AuditServiceImpl() {

    auditScenarios.put(AuditScenarioEnum.CREATE_USER, getCreateNewUserAuditHelper());
    auditScenarios.put(AuditScenarioEnum.CREATE_USER_WITH_EOR, getCreateUserWithEORAuditHelper());
    auditScenarios.put(AuditScenarioEnum.CREATE_USER_WITH_TM, getCreateUserWithTMPermAuditHelper());
    auditScenarios.put(AuditScenarioEnum.RECREATE_ARCHIVED_USER, getRecreateArchivedUserAuditHelper());
    auditScenarios.put(AuditScenarioEnum.UPDATE_USER, getUpdateUserAuditHelper());
    auditScenarios.put(AuditScenarioEnum.UPDATE_USER_EOR_CHANGED, getUpdateUserWithEORAuditHelper());
    auditScenarios.put(AuditScenarioEnum.UPDATE_USER_TM_CHANGED, geUpdateUserWithTMPermAuditHelper());
    auditScenarios.put(AuditScenarioEnum.UPDATE_USER_STATUS_CHANGED, getUpdateUserStatusChangedAuditHelper());
    auditScenarios.put(AuditScenarioEnum.UPDATE_USER_EXPIRYDATE_CHANGED, getUpdateUserExpiryDateChangedAuditHelper());
    auditScenarios.put(AuditScenarioEnum.SAVE_SUBFLEET, getSubfleetAuditHelper());
    auditScenarios.put(AuditScenarioEnum.SAVE_SUBFLEET_ALL, getSubfleetAllAuditHelper());
    auditScenarios.put(AuditScenarioEnum.UPDATE_USER_LAST_LOGIN, getUpdateUserLastLoginAuditHelper());
    auditScenarios.put(AuditScenarioEnum.UPDATE_MPMS_TASKS, getUpdateMpmsTasksAuditHelper());

  }

  /**
   * Verifies re create userProfile with archived user profile audit scenario
   * {@link AuditScenarioEnum#RECREATE_ARCHIVED_USER}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<UserProfiles> getRecreateArchivedUserAuditHelper() {
    return new AuditHelper<UserProfiles>() {

      @Override
      public boolean isMatchAuditScenario(final UserProfiles oldProfile, final UserProfiles newProfile) {
        final boolean isOldProfileStatusArchived = Optional.ofNullable(oldProfile).map(UserProfiles::getStatus)
            .filter(status -> ARCHIVED.getId().equals(oldProfile.getStatus().getId())).isPresent();
        final boolean isNewProfileStatusActive = Optional.ofNullable(newProfile).map(UserProfiles::getStatus)
            .filter(status -> ACTIVE.getId().equals(oldProfile.getStatus().getId())).isPresent();

        return (isOldProfileStatusArchived && isNewProfileStatusActive);
      }

      @Override
      public final String getUpdateField(final UserProfiles object) {
        if (object != null) {
          try {
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
          } catch (JsonProcessingException e) {
            LOGGER.error("Fail to convert UserProfiles to json.", e);
          }
        }
        return EMPTY_STRING;
      }
    };
  }

  /**
   * Verifies user last login updatd audit scenario
   * {@link AuditScenarioEnum#UPDATE_USER_LAST_LOGIN}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<LocalDateTime> getUpdateUserLastLoginAuditHelper() {
    return new AuditHelper<LocalDateTime>() {

      @Override
      public boolean isMatchAuditScenario(final LocalDateTime oldValue, final LocalDateTime newValue) {
        Optional<LocalDateTime> optOldValue = Optional.ofNullable(oldValue);
        Optional<LocalDateTime> optNewValue = Optional.ofNullable(newValue);
        if (optOldValue.isPresent()) {
          return !optOldValue.get().equals(optNewValue.orElse(null));
        } else if (optNewValue.isPresent()) {
          return !optNewValue.get().equals(optOldValue.orElse(null));
        }
        return false;
      }

      @Override
      public final String getUpdateField(final LocalDateTime lastLogin) {
        return LAST_LOGIN + lastLogin;
      }
    };
  }

  /**
   * Verifies user create audit scenario {@link AuditScenarioEnum#CREATE_USER}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<CreateNewUserDto> getCreateNewUserAuditHelper() {
    return new AuditHelper<CreateNewUserDto>() {

      @Override
      public boolean isMatchAuditScenario(final CreateNewUserDto oldProfile, final CreateNewUserDto newProfile) {
        return (oldProfile == null && newProfile != null);
      }

      @Override
      public final String getUpdateField(final CreateNewUserDto object) {
        if (object != null) {
          try {
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
          } catch (JsonProcessingException e) {
            LOGGER.error("Fail to convert CreateNewUserDto to json.", e);
          }
        }
        return EMPTY_STRING;
      }
    };
  }

  /**
   * Verifies user created with eor audit scenario {@link AuditScenarioEnum#CREATE_USER_WITH_EOR}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<CreateNewUserDto> getCreateUserWithEORAuditHelper() {
    return new AuditHelper<CreateNewUserDto>() {

      @Override
      public boolean isMatchAuditScenario(final CreateNewUserDto oldProfile, final CreateNewUserDto newProfile) {
        if (oldProfile == null
            && !Optional.ofNullable(newProfile).map(CreateNewUserDto::getEor).map(List::isEmpty).orElse(true)) {
          return true;
        }
        return false;
      }

      @Override
      @SuppressWarnings("unchecked")
      public final String getUpdateField(final CreateNewUserDto object) {
        if (object != null) {
          try {
            List<UserEORDto> eorList =
                Optional.ofNullable(object).map(CreateNewUserDto::getEor).orElse(java.util.Collections.EMPTY_LIST);
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(eorList);
          } catch (JsonProcessingException e) {
            LOGGER.error("Fail to convert UserEORDto to json.", e);
          }
        }
        return EMPTY_STRING;
      }

    };
  }

  /**
   * Verifies user created with EOR with TMflag audit scenario
   * {@link AuditScenarioEnum#CREATE_USER_WITH_TM}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<CreateNewUserDto> getCreateUserWithTMPermAuditHelper() {
    return new AuditHelper<CreateNewUserDto>() {

      @SuppressWarnings("unchecked")
      @Override
      public boolean isMatchAuditScenario(final CreateNewUserDto oldProfile, final CreateNewUserDto newProfile) {
        boolean matchFlag = false;

        if (oldProfile == null) {
          List<UserEORDto> eorList =
              Optional.ofNullable(newProfile).map(CreateNewUserDto::getEor).orElse(Collections.EMPTY_LIST);
          final String eorWithTmStr = getEorTmString(eorList);
          matchFlag = StringUtils.isNotEmpty(eorWithTmStr);
        }
        return matchFlag;
      }

      @Override
      @SuppressWarnings("unchecked")
      public final String getUpdateField(final CreateNewUserDto object) {
        List<UserEORDto> eorList =
            Optional.ofNullable(object).map(CreateNewUserDto::getEor).orElse(Collections.EMPTY_LIST);
        final String eorWithTmStr = getEorTmString(eorList);
        return eorWithTmStr;
      }

      private String getEorTmString(final List<UserEORDto> eorList) {
        return eorList.stream().filter(UserEORDto::getTmReport)
            .map(eor -> IMO + eor.getImoNumber() + TM_FLAG + eor.getTmReport()).collect(Collectors.joining(JOINING));
      }

    };
  }


  /**
   * Verifies user updated with any of the fields : flagCode, clientCode,
   * shipBuilderCode,restrictAll,lastLogin, name, companyName in update user audit scenario
   * {@link AuditScenarioEnum#UPDATE_USER}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<UserProfiles> getUpdateUserAuditHelper() {
    final String[] fieldsToBeVerified =
        new String[] {"flagCode", "clientCode", "shipBuilderCode", "restrictAll", "name", "companyName"};
    return new AuditHelper<UserProfiles>() {

      @Override
      public boolean isMatchAuditScenario(final UserProfiles oldProfile, final UserProfiles newProfile) {
        // to check user profile changes multiple scenarios
        boolean isUserProfileChanged = false;

        final Optional<UserProfiles> oldProfileOpt = Optional.ofNullable(oldProfile);
        final Optional<UserProfiles> newProfileOpt = Optional.ofNullable(newProfile);


        if (oldProfileOpt.isPresent() && newProfileOpt.isPresent()) {
          isUserProfileChanged = UserProfileServiceUtils.isAnyFieldChanged(fieldsToBeVerified, oldProfile, newProfile);
        }
        return isUserProfileChanged;
      }

      @Override
      public String getUpdateField(final UserProfiles object) {

        if (object != null) {
          try {
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(JsonView.with(object)
                .onClass(UserProfiles.class, match().exclude(EXCLUSION_RULE).include(fieldsToBeVerified)));
          } catch (JsonProcessingException e) {
            LOGGER.error("Fail to convert UserProfiles to json.", e);
          }
        }
        return EMPTY_STRING;
      };
    };
  }

  /**
   * Verifies user updated with EOR audit scenario
   * {@link AuditScenarioEnum#UPDATE_USER_EOR_CHANGED}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<UserProfiles> getUpdateUserWithEORAuditHelper() {
    return new AuditHelper<UserProfiles>() {

      @SuppressWarnings("unchecked")
      @Override
      public boolean isMatchAuditScenario(final UserProfiles oldProfile, final UserProfiles newProfile) {

        String oldEorStr =
            getEorString(Optional.ofNullable(oldProfile).map(UserProfiles::getEors).orElse(Collections.EMPTY_SET));
        String newEorStr =
            getEorString(Optional.ofNullable(newProfile).map(UserProfiles::getEors).orElse(Collections.EMPTY_SET));

        if (!oldEorStr.equals(newEorStr)) {
          return true;
        }
        return false;
      }

      @Override
      @SuppressWarnings("unchecked")
      public final String getUpdateField(final UserProfiles object) {
        return getEorString(Optional.ofNullable(object).map(UserProfiles::getEors).orElse(Collections.EMPTY_SET));
      }

      private String getEorString(final Set<UserEOR> eorSet) {
        return eorSet.stream().map(eor -> {
          try {
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(eor);
          } catch (JsonProcessingException e) {
            LOGGER.error("Fail to convert UserEOR to json.", e);
            return EMPTY_STRING;
          }
        }).sorted().collect(Collectors.joining(JOINING));
      }

    };
  }

  /**
   * Verifies user updated with TM flag change scenario
   * {@link AuditScenarioEnum#UPDATE_USER_TM_CHANGED}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<UserProfiles> geUpdateUserWithTMPermAuditHelper() {
    return new AuditHelper<UserProfiles>() {

      @SuppressWarnings("unchecked")
      @Override
      public boolean isMatchAuditScenario(final UserProfiles oldProfile, final UserProfiles newProfile) {

        String oldEorTMStr =
            getEorTmString(Optional.ofNullable(oldProfile).map(UserProfiles::getEors).orElse(Collections.EMPTY_SET));
        String newEorTmStr =
            getEorTmString(Optional.ofNullable(newProfile).map(UserProfiles::getEors).orElse(Collections.EMPTY_SET));

        if (!oldEorTMStr.equals(newEorTmStr)) {
          return true;
        }

        return false;
      }

      @Override
      @SuppressWarnings("unchecked")
      public final String getUpdateField(final UserProfiles object) {
        Set<UserEOR> eorSet = Optional.ofNullable(object).map(UserProfiles::getEors).orElse(Collections.EMPTY_SET);
        final String eorWithTmStr = getEorTmString(eorSet);
        return eorWithTmStr;
      }

      private String getEorTmString(final Set<UserEOR> eorSet) {
        return eorSet.stream().filter(UserEOR::getAccessTmReport)
            .map(eor -> IMO + eor.getImoNumber() + TM_FLAG + eor.getAccessTmReport()).sorted()
            .collect(Collectors.joining(JOINING));
      }

    };
  }

  /**
   * Verifies user updated with status change audit scenario
   * {@link AuditScenarioEnum#UPDATE_USER_STATUS_CHANGED}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<UserProfiles> getUpdateUserStatusChangedAuditHelper() {
    return new AuditHelper<UserProfiles>() {

      @Override
      public boolean isMatchAuditScenario(final UserProfiles oldProfile, final UserProfiles newProfile) {

        final String oldStatus = getStatus(oldProfile);
        final String newStatus = getStatus(newProfile);

        if (!oldStatus.equalsIgnoreCase(newStatus)) {
          return true;
        }

        return false;
      }

      @Override
      public final String getUpdateField(final UserProfiles object) {
        return getStatus(object);
      }

      private String getStatus(final UserProfiles userProfile) {
        return Optional.ofNullable(userProfile).map(UserProfiles::getStatus).map(UserAccountStatus::getName)
            .orElse(EMPTY_STRING);
      }
    };
  }

  /**
   * Verifies user updated with expiry date change audit scenario
   * {@link AuditScenarioEnum#UPDATE_USER_EXPIRYDATE_CHANGED}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<UserProfiles> getUpdateUserExpiryDateChangedAuditHelper() {
    return new AuditHelper<UserProfiles>() {


      @Override
      public boolean isMatchAuditScenario(final UserProfiles oldProfile, final UserProfiles newProfile) {

        final Optional<LocalDate> oldExpiryDate =
            Optional.ofNullable(oldProfile).map(UserProfiles::getUserAccExpiryDate);
        final Optional<LocalDate> newExpiryDate =
            Optional.ofNullable(newProfile).map(UserProfiles::getUserAccExpiryDate);

        if (oldExpiryDate.isPresent()) {
          return !oldExpiryDate.get().equals(newExpiryDate.orElse(null));
        } else if (newExpiryDate.isPresent()) {
          return !newExpiryDate.get().equals(oldExpiryDate.orElse(null));
        }
        return false;
      }

      @Override
      public final String getUpdateField(final UserProfiles object) {
        return getStatus(object);
      }

      public final String getStatus(final UserProfiles object) {
        return Optional.ofNullable(object).map(UserProfiles::getUserAccExpiryDate).map(LocalDate::toString)
            .orElse(EMPTY_STRING);
      }
    };
  }

  /**
   * Verifies user updated with subfleet change audit scenario
   * {@link AuditScenarioEnum#SAVE_SUBFLEET}.
   *
   * @return audit helper for create scenario.
   */
  @SuppressWarnings("unchecked")
  protected static final AuditHelper<List<Long>> getSubfleetAuditHelper() {
    return new AuditHelper<List<Long>>() {

      @Override
      public boolean isMatchAuditScenario(final List<Long> oldSubFleet, final List<Long> newSubFleet) {

        String oldAccessibleIds = sortedLongString(Optional.ofNullable(oldSubFleet).orElse(Collections.EMPTY_LIST));
        String newAccessibleIds = sortedLongString(Optional.ofNullable(newSubFleet).orElse(Collections.EMPTY_LIST));

        if (!oldAccessibleIds.equalsIgnoreCase(newAccessibleIds)) {
          return true;
        }
        return false;
      }

      @Override
      public final String getUpdateField(final List<Long> object) {
        return sortedLongString(object);
      }

      private String sortedLongString(final List<Long> longList) {
        return longList.stream().sorted().map(String::valueOf).collect(Collectors.joining(JOINING));
      }
    };
  }

  /**
   * Verifies user updated with subfleet change if user moves all assets to restricted or all assets
   * to accessiable audit scenario {@link AuditScenarioEnum#SAVE_SUBFLEET_ALL}.
   *
   * @return audit helper for create scenario.
   */
  @SuppressWarnings("unchecked")
  protected static final AuditHelper<UserProfiles> getSubfleetAllAuditHelper() {
    return new AuditHelper<UserProfiles>() {

      @Override
      public boolean isMatchAuditScenario(final UserProfiles oldProfile, final UserProfiles newProfile) {
        boolean isUserProfileChanged = false;

        final Optional<UserProfiles> oldProfileOpt = Optional.ofNullable(oldProfile);
        final Optional<UserProfiles> newProfileOpt = Optional.ofNullable(newProfile);

        if (oldProfileOpt.isPresent() && newProfileOpt.isPresent()
            && !(oldProfile.getRestrictAll().equals(newProfile.getRestrictAll()))) {
          isUserProfileChanged = true;
        }
        return isUserProfileChanged;
      }

      @Override
      public final String getUpdateField(final UserProfiles object) {
        if (Optional.ofNullable(object).isPresent()) {
          return getRestrictAll(object);
        } else {
          return null;
        }
      }

      public final String getRestrictAll(final UserProfiles object) {
        return RESTRICT_ALL + object.getRestrictAll().toString();
      }
    };
  }

  /**
   * Verifies tasks pms credited date update audit scenario
   * {@link AuditScenarioEnum#UPDATE_MPMS_TASKS}.
   *
   * @return audit helper for create scenario.
   */
  protected static final AuditHelper<AuditPmsAbleTaskDto> getUpdateMpmsTasksAuditHelper() {
    return new AuditHelper<AuditPmsAbleTaskDto>() {

      @Override
      public boolean isMatchAuditScenario(final AuditPmsAbleTaskDto oldValue, final AuditPmsAbleTaskDto newValue) {
        boolean isCreditedDateChanged = false;

        Optional<Date> optOldValue = Optional.ofNullable(oldValue).map(AuditPmsAbleTaskDto::getDateCredited);
        Optional<Date> optNewValue = Optional.ofNullable(newValue).map(AuditPmsAbleTaskDto::getDateCredited);

        if (!optOldValue.isPresent() && optNewValue.isPresent()) {
          isCreditedDateChanged = true;
        }
        return isCreditedDateChanged;
      }

      @Override
      public final String getUpdateField(final AuditPmsAbleTaskDto object) {
        if (object != null) {
          try {
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
          } catch (JsonProcessingException e) {
            LOGGER.error("Fail to convert AuditPmsAbleTaskDto to json.", e);
          }
        }
        return EMPTY_STRING;
      }
    };
  }

  @Override
  @SuppressWarnings({"rawtypes", "unchecked"})
  public final void audit(final UserProfiles requesterUser, final UserProfiles targetUser, final Object oldValue,
      final Object newValue, final AuditScenarioEnum... scenarios) {
    if (scenarios != null) {
      final Optional<UserProfiles> requesterUserOpt = Optional.ofNullable(requesterUser);
      final String requesterId = requesterUserOpt.map(UserProfiles::getUserId).orElse(EMPTY_STRING);
      final String requesterEmail = requesterUserOpt.map(UserProfiles::getEmail).orElse(EMPTY_STRING);

      final Optional<UserProfiles> targetUserOpt = Optional.ofNullable(targetUser);
      final String targetId = targetUserOpt.map(UserProfiles::getUserId).orElse(EMPTY_STRING);
      final String targetEmail = targetUserOpt.map(UserProfiles::getEmail).orElse(EMPTY_STRING);

      Stream.of(scenarios).forEach(scenario -> {
        LOGGER.debug("Audit Scenarios {}", scenario);
        Optional.ofNullable(auditScenarios.get(scenario)).ifPresent(helper -> {
          if (helper.isMatchAuditScenario(oldValue, newValue)) {

            LOGGER.log(Level.getLevel("INFO"),
                "RequesterId: {} | RequesterEmail: {} | TargetUserId: {} | TargetUserEmail: {} | Action: {} | "
                    + "DateTime: {} | OldValue: {} | NewValue: {} ",
                requesterId, requesterEmail, targetId, targetEmail, scenario.getValue(), LocalDateTime.now(),
                helper.getUpdateField(oldValue), helper.getUpdateField(newValue));
          }
        });
      });
    }
  }
}
