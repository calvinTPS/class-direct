/**
 *
 */
package com.baesystems.ai.lr.cd.service.favourites;

import java.util.List;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * Provides service to get favourite assets and delete favorite assets for user.
 *
 * @author NAvula
 * @author syalavarthi 23-05-2017.
 *
 */
public interface FavouritesService {

  /**
   * Retrieves favourite assets for given user.
   *
   * @param userId the user id.
   * @return the list of asset id's.
   * @throws RecordNotFoundException if the requested user not available.
   */
  List<String> getFavouritesForUser(String userId) throws RecordNotFoundException;

  /**
   * Deletes all favourite of given user.
   *
   * @param userId the user id.
   * @throws ClassDirectException if any error occur in deleting user favorite.
   */
  void deleteAllUserFavourites(final String userId) throws ClassDirectException;
}
