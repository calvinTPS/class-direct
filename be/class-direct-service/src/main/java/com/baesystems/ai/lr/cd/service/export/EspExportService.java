package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ESPReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides service methods for ESP report generation.
 *
 * @author yng
 * @author SBollu
 *
 */
public interface EspExportService {

  /**
   * Generates ESP report in PDF format for download.
   *
   * @param query post body.
   * @return the encrypted string token which contain file path location of generated PDF report in server.
   * @throws ClassDirectException if API call, or PDF template compilation fails.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadPdf(ESPReportExportDto query) throws ClassDirectException;
}
