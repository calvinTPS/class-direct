package com.baesystems.ai.lr.cd.service.asset.impl;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.criteria.JoinType;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.enums.CustomerRolesEnum;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AbstractSubQueryDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.baesystems.ai.lr.enums.ServiceStatus;

/**
 * Reusable fluent query builder util.
 *
 * @author Faizal Sidek
 * @author YWearn 2017-07-18
 * @author syalavarthi 2018-02-07.
 */
public final class MastQueryBuilder {

  /**
   * List of mandatory queries.
   */
  private final List<AbstractQueryDto> mandatory = new ArrayList<>();

  /**
   * List of optional queries.
   */
  private final List<AbstractQueryDto> optional = new ArrayList<>();

  /**
   * List of optional queries.
   */
  private List<AbstractQueryDto> ihsPriorityMandatory = new ArrayList<>();

  /**
   * List of optional queries.
   */
  private List<AbstractQueryDto> ihsPriorityOptional = new ArrayList<>();

  /**
   * IHS Asset status to identify active asset .
   */
  private static final String[] IHS_ASSET_STATUS =
      new String[] {"9", "C", "E", "F", "H", "J", "K", "L", "M", "O", "P", "R", "S", "T", "U", "V"};
  /**
   * Asset life cycle status to identify active asset .
   */
  private static final Integer[] ASSET_LIFECYCLE_STATUS = new Integer[] {1, 3, 4, 5, 6, 7};
  /**
   * The codicil allowable confidentiality type ids.
   */
  @Value("#{'${codicil.confidentiality.type.id}'.split(',')}")
  private List<Long> confidentialityTypeIds;

  /**
   * Privatized this constructor for builder pattern.
   */

  public MastQueryBuilder() {

  }

  /**
   * Create query by asset type id.
   *
   * @param cond condition.
   * @param values value.
   * @return predicate.
   */
  public static Predicate assetTypeId(final QueryRelationshipType cond, final Long... values) {
    return () -> newQuery("publishedVersion.assetType", "id", cond, values);
  }

  /**
   * Query by flag state ids.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate flagStateId(final QueryRelationshipType cond, final Long... values) {
    return () -> newQuery("publishedVersion.flagState", "id", cond, values);
  }

  /**
   * Filter result by flag codes.
   *
   * @param cond relationship type.
   * @param flagCodes flag codes.
   * @return predicate.
   */
  public static Predicate flagStateCode(final QueryRelationshipType cond, final String... flagCodes) {
    return () -> newQuery("publishedVersion", "flagState.flagCode", cond, flagCodes);
  }

  /**
   * Query range of gross tonnage.
   *
   * @param grossTonnageMin min gross tonnage.
   * @param grossTonnageMax max gross tonnage.
   * @return predicate.
   */
  public static Predicate grossTonnage(final Double grossTonnageMin, final Double grossTonnageMax) {
    return () -> {
      AbstractQueryDto query = new AbstractQueryDto();

      if (grossTonnageMin != null && grossTonnageMax != null) {
        query = between("publishedVersion.grossTonnage", grossTonnageMin, grossTonnageMax);
      } else if (grossTonnageMin != null) {
        query = newQuery("publishedVersion.grossTonnage", QueryRelationshipType.GE, grossTonnageMin);
      } else if (grossTonnageMax != null) {
        query = newQuery("publishedVersion.grossTonnage", QueryRelationshipType.LE, grossTonnageMax);
      }

      return query;
    };
  }

  /**
   * Non LR classed class status id.
   */
  private static final Long NON_LR_CLASSED_CLASS_STATUS_ID = 10L;

  /**
   * Query based on list of class status ids.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate classStatusId(final QueryRelationshipType cond, final Long... values) {

    return () -> {
      final boolean hasNonLRClassed =
          Stream.of(values).filter(id -> id.equals(NON_LR_CLASSED_CLASS_STATUS_ID)).findAny().isPresent();
      final AbstractQueryDto query;
      if (hasNonLRClassed) {
        query = new AbstractQueryDto();
        query.setOr(new ArrayList<>());
        query.getOr().add(newQuery("publishedVersion", "hiddenId", QueryRelationshipType.GT, 0));
        query.getOr().add(newQuery("publishedVersion.classStatus", "id", cond, values));
      } else {
        query = newQuery("publishedVersion.classStatus", "id", cond, values);
      }
      return query;
    };
  }

  /**
   * Query based on build date range.
   *
   * @param buildDateMin from.
   * @param buildDateMax to.
   * @return predicate.
   */
  public static Predicate buildDate(final Date buildDateMin, final Date buildDateMax) {
    final Predicate predicate;
    if (buildDateMin.equals(buildDateMax)) {
      final Calendar buildDateMinCal = Calendar.getInstance();
      buildDateMinCal.setTime(buildDateMin);
      buildDateMinCal.add(Calendar.SECOND, -1);
      buildDateMinCal.add(Calendar.MILLISECOND, buildDateMinCal.getActualMaximum(Calendar.MILLISECOND));

      final Calendar buildDateMaxCal = Calendar.getInstance();
      buildDateMaxCal.setTime(buildDateMax);
      buildDateMaxCal.add(Calendar.DATE, 1);
      buildDateMaxCal.add(Calendar.SECOND, -1);
      buildDateMaxCal.add(Calendar.MILLISECOND, buildDateMaxCal.getActualMaximum(Calendar.MILLISECOND));

      predicate = () -> between("publishedVersion.buildDate", buildDateMinCal.getTime(), buildDateMaxCal.getTime());
    } else {
      predicate = () -> between("publishedVersion.buildDate", buildDateMin, buildDateMax);
    }

    return predicate;
  }

  /**
   * Query based on asset id.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate assetId(final QueryRelationshipType cond, final Long... values) {
    return () -> newQuery("publishedVersion", "id", cond, values);
  }

  /**
   * Query based on IHS asset id.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate ihsAssetId(final QueryRelationshipType cond, final Long... values) {
    return () -> newQuery("publishedVersion", "hiddenId", cond, values);
  }

  /**
   * Query based on IHS and MAST asset id.
   *
   * @param values values.
   * @return predicate.
   */
  public static Predicate assetIdAndIhsAssetId(final Long... values) {
    return () -> MastQueryBuilder.create().optional(assetId(QueryRelationshipType.IN, values))
        .optional(ihsAssetId(QueryRelationshipType.IN, values)).build();
  }

  /**
   * Query by client id.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate clientCode(final QueryRelationshipType cond, final String... values) {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());
      final List<String> paths =
          Stream.of(CustomerRolesEnum.values()).filter(role -> role != CustomerRolesEnum.SHIP_BUILDER)
              .map(CustomerRolesEnum::getPath).collect(Collectors.toList());
      paths.forEach(path -> query.getOr().add(newQuery(path, JoinType.LEFT, "imoNumber", cond, false, values)));

      return query;
    };
  }

  /**
   * Query by client id and eors.
   *
   * @param eorImos the list of imo for eor assets.
   * @param clientCode the client identifier
   * @return the predicate to construct the mast ihs query to match client code or eor imos.
   */
  public static Predicate clientCodeAndEor(final List<String> eorImos, final String clientCode) {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      final AbstractQueryDto clientCodeQuery = new AbstractQueryDto();
      clientCodeQuery.setOr(new ArrayList<>());
      final List<String> paths =
          Stream.of(CustomerRolesEnum.values()).filter(role -> role != CustomerRolesEnum.SHIP_BUILDER)
              .map(CustomerRolesEnum::getPath).collect(Collectors.toList());
      paths.forEach(path -> clientCodeQuery.getOr()
          .add(newQuery(path, JoinType.LEFT, "imoNumber", QueryRelationshipType.EQ, false, clientCode)));

      final AbstractQueryDto imoQuery =
          newQuery("publishedVersion", "imoNumber", QueryRelationshipType.IN, eorImos.toArray(new String[] {}));

      query.setOr(new ArrayList<>(2));
      query.getOr().add(clientCodeQuery);
      query.getOr().add(imoQuery);

      return query;
    };
  }

  /**
   * Queries by services with MMS Services only.
   *
   * @param mmsServiceCatalogueIds the list of service catalogue id with mms.
   * @return the predicate to filter by mms service catalogues.
   */
  public static Predicate serviceWithMMSService(final List<Long> mmsServiceCatalogueIds) {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setJoinField("services");
      query.setJoinType(JoinType.INNER);
      query.setAnd(new ArrayList<>());
      query.getAnd().add(newQuery("serviceCatalogueReadOnlyDomain.id", QueryRelationshipType.IN,
          mmsServiceCatalogueIds.toArray(new Long[] {})));
      query.getAnd().add(newQuery("serviceStatus.id", QueryRelationshipType.NE, ServiceStatus.COMPLETE.getValue()));
      query.setSelectWhenFieldIsMissing(false);
      return query;
    };
  }

  /**
   * Filter by client code and type.
   *
   * @param cond relationship.
   * @param clients map of client type and code.
   * @return predicate.
   */
  public static Predicate clientCode(final QueryRelationshipType cond, final Map<String, String> clients) {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());
      clients.forEach((type, code) -> {
        final CustomerRolesEnum clientType = CustomerRolesEnum.valueOf(type);
        query.getOr().add(newQuery(clientType.getPath(), JoinType.LEFT, "imoNumber", cond, false, code));
      });
      return query;
    };
  }

  /**
   * Query by client name.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate clientName(final QueryRelationshipType cond, final String... values) {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());
      query.getOr().add(newQuery("publishedVersion.shipManagerInfo", JoinType.LEFT, "name", cond, false, values));
      query.getOr().add(newQuery("publishedVersion.operatorInfo", JoinType.LEFT, "name", cond, false, values));
      query.getOr().add(newQuery("publishedVersion.techManagerInfo", JoinType.LEFT, "name", cond, false, values));
      query.getOr().add(newQuery("publishedVersion.docCompanyInfo", JoinType.LEFT, "name", cond, false, values));
      query.getOr().add(newQuery("publishedVersion.registeredOwnerInfo", JoinType.LEFT, "name", cond, false, values));
      query.getOr().add(newQuery("publishedVersion.groupOwnerInfo", JoinType.LEFT, "name", cond, false, values));

      return query;
    };
  }

  /**
   * Search by client name with specific client type.
   *
   * @param cond condition.
   * @param clientType client type.
   * @param clientName client name.
   * @return predicate.
   */
  public static Predicate clientName(final QueryRelationshipType cond, final String clientType,
      final String clientName) {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());
      final CustomerRolesEnum type = CustomerRolesEnum.valueOf(clientType);
      query.getOr().add(newQuery(type.getPath(), JoinType.LEFT, "name", cond, false, clientName));

      return query;
    };
  }

  /**
   * Query by yard number.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate yardNumber(final QueryRelationshipType cond, final String... values) {
    return () -> newQuery("publishedVersion.yardNumber", cond, values);
  }


  /**
   * Query by leadImo.
   *
   * @param values values.
   * @return predicate.
   */
  public static Predicate leadImo(final String values) {
    return () -> newQuery("publishedVersion", "leadImo", QueryRelationshipType.EQ, values);
  }

  /**
   * Query based on asset id.
   *
   * @param cond condition.
   * @param selectWhenFieldIsMissing whether to search both databases.
   * @param values values.
   * @return predicate.
   */
  public static Predicate assetId(final QueryRelationshipType cond, final Boolean selectWhenFieldIsMissing,
      final Long... values) {
    return () -> newQuery("publishedVersion", JoinType.INNER, "id", cond, selectWhenFieldIsMissing, values);
  }

  /**
   * Query by asset type Stat5 code.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate assetTypeCode(final QueryRelationshipType cond, final String... values) {
    return () -> newQuery("publishedVersion", "assetType.code", cond, values);
  }

  /**
   *  Existing approach: There is separate join for Date field and mms & surveyType & Codicil Type
   *  field to Service and Codicil table, which worsen query performance since joining to same table multiple times
   *
   *  Current approach: The surveyType , codicilType and MMS conditions has been moved
   *  to the subquery which joins the date condition to service and codicil table respectively,
   *  which improve the query performance
   *
   *  The query generated for surveyType, codicilType and MMS filter as below.
   * <ul>
   *  <li>Generate service query, if any of the conditions below satisfy.
   *   <ul>
   *     <li>ServiceCatalogueId's is not empty
   *     <li>IsMMSService is true
   *     <li>CodicilTypeId is empty
   *   </ul>
   *  <li>Generate codicil query, if any of the conditions below satisfy.
   *   </ul>
   *     <li>CodicilTypeId is not empty
   *     <li>ServiceCatalogueId's is empty and IsMMSService is false
   *   <ul>
   * </ul>
   *
   * @param assetQuery the asset query dto to filter.
   * @param mmsServiceCatalogueIds the list of mms service catalogue id's to filter.
   * @param codicilTypeIds the list of codicil type id's to filter.
   * @param statusIds the list of codicil status id's to filter.
   * @param confidentialityTypeIds the list of codicil confidential Type Id's to filter.
   *
   * @return predicate.
   */
  public static Predicate buildServiceCodicilQuery(final AssetQueryHDto assetQuery,
      final List<Long> mmsServiceCatalogueIds, final List<Long> codicilTypeIds,
      final List<Long> statusIds, final List<Long> confidentialityTypeIds) {
    return () -> {
      final AbstractQueryDto or = new AbstractQueryDto();
      or.setOr(new ArrayList<>());

      boolean isCodicilQueryRequired = false;
      boolean isServiceQueryRequired = false;

      if (CollectionUtils.isNotEmpty(assetQuery.getServiceCatalogueIds())
          || Boolean.TRUE.equals(assetQuery.getIsMMSService())) {
        isServiceQueryRequired = true;
      }

      if (CollectionUtils.isNotEmpty(assetQuery.getCodicilTypeId())) {
        isCodicilQueryRequired = true;
      }

      if (isServiceQueryRequired && isCodicilQueryRequired) {
        or.getOr().add(buildServiceQuery(assetQuery, mmsServiceCatalogueIds));
        or.getOr().add(buildCodicilQuery(assetQuery, codicilTypeIds, statusIds, confidentialityTypeIds));
      } else if (isServiceQueryRequired) {
        or.getOr().add(buildServiceQuery(assetQuery, mmsServiceCatalogueIds));
      } else if (isCodicilQueryRequired) {
        or.getOr().add(buildCodicilQuery(assetQuery, codicilTypeIds, statusIds, confidentialityTypeIds));
      } else {
        // If both of the service and codicil filter not selected ,then build service and codicil
        // query with only date.
        or.getOr().add(buildServiceQuery(assetQuery, mmsServiceCatalogueIds));
        or.getOr().add(buildCodicilQuery(assetQuery, codicilTypeIds, statusIds, confidentialityTypeIds));
      }

      return or;
    };
  }

  /**
   * Build Service Query on below criteria.
   * <ul>
   *  <li>Filter by Survey Type and MMS with due & proposed date.
   *  <li>Filter by Survey Type with due & proposed date.
   *  <li>Filter by MMS with due date & proposed date.
   *  <li>Filter by due date & proposed date.
   * </ul>
   *
   * @param assetQuery the asset query dto to filter.
   * @param mmsServiceCatalogueIds the list of mms service catalogue id's to filter.
   *
   * @return AbstractQueryDto the query build for service related filter.
   */
  private static AbstractQueryDto buildServiceQuery(final AssetQueryHDto assetQuery,
      final List<Long> mmsServiceCatalogueIds) {
    final AbstractQueryDto queryByService = new AbstractQueryDto();
    queryByService.setField("id");
    queryByService.setRelationship(QueryRelationshipType.IN);
    queryByService.setSelectWhenFieldIsMissing(false);


    final AbstractSubQueryDto serviceSubQuery = new AbstractSubQueryDto();
    serviceSubQuery.setSelecting("asset.id");
    serviceSubQuery.setFieldOnParentObject("services");
    serviceSubQuery.setSubQuery(new AbstractQueryDto());
    serviceSubQuery.getSubQuery().setAnd(new ArrayList<>());

    serviceSubQueryWithDate(serviceSubQuery, assetQuery.getCodicilDueDateMin(), assetQuery.getCodicilDueDateMax());

    if (CollectionUtils.isNotEmpty(assetQuery.getServiceCatalogueIds())) {
      serviceSubQuery.getSubQuery().getAnd().add(newQuery("serviceCatalogue.id", QueryRelationshipType.IN,
          assetQuery.getServiceCatalogueIds().toArray(new Long[] {})));
    }

    if (Boolean.TRUE.equals(assetQuery.getIsMMSService())) {
      serviceSubQuery.getSubQuery().getAnd().add(newQuery("serviceCatalogueReadOnlyDomain.id", QueryRelationshipType.IN,
          mmsServiceCatalogueIds.toArray(new Long[] {})));
    }

    queryByService.setValue(serviceSubQuery);

    return queryByService;
  }

  /**
   * Build Codicil Query on below criteria.
   * <ul>
   *  <li>Filter by Codicil Type with due date.
   *  <li>Filter by due date only.
   * </ul>
   *
   * @param assetQuery the asset query dto to filter.
   * @param codicilTypeIds the list of codicil type id's to filter.
   * @param statusIds the list of codicil status id's to filter.
   * @param confidentialityTypeIds the list of codicil confidential Type Id's to filter.
   *
   * @return AbstractQueryDto the query build for codicil related filter.
   */
  private static AbstractQueryDto buildCodicilQuery(final AssetQueryHDto assetQuery, final List<Long> codicilTypeIds,
      final List<Long> statusIds, final List<Long> confidentialityTypeIds) {
    final AbstractQueryDto queryByCodicils = new AbstractQueryDto();
    queryByCodicils.setField("id");
    queryByCodicils.setRelationship(QueryRelationshipType.IN);
    queryByCodicils.setSelectWhenFieldIsMissing(false);

    final AbstractSubQueryDto codicilsSubQuery = new AbstractSubQueryDto();
    codicilsSubQuery.setSelecting("asset.id");
    codicilsSubQuery.setFieldOnParentObject("codicils");
    codicilsSubQuery.setSubQuery(new AbstractQueryDto());
    codicilsSubQuery.getSubQuery().setAnd(new ArrayList<>());

    codicilsSubQuery.getSubQuery().getAnd()
        .add(newQuery("dueDate", QueryRelationshipType.GE, assetQuery.getCodicilDueDateMin()));
    codicilsSubQuery.getSubQuery().getAnd()
        .add(newQuery("dueDate", QueryRelationshipType.LE, assetQuery.getCodicilDueDateMax()));

    if (CollectionUtils.isNotEmpty(assetQuery.getCodicilTypeId())) {
      codicilsSubQuery.getSubQuery().getAnd()
          .add(newQuery("typeId", QueryRelationshipType.IN, codicilTypeIds.toArray(new Long[] {})));

      codicilsSubQuery.getSubQuery().getAnd()
          .add(newQuery("status", QueryRelationshipType.IN, statusIds.toArray(new Long[] {})));

      codicilsSubQuery.getSubQuery().getAnd().add(
          newQuery("confidentialityType.id", QueryRelationshipType.IN, confidentialityTypeIds.toArray(new Long[] {})));
    }

    queryByCodicils.setValue(codicilsSubQuery);

    return queryByCodicils;
  }

  /**
   * Build Service subQuery with below conditions.
   * <li>active service.
   * <li>
   * <li>service status except 4(completed).
   * <li>
   * <li>postponementDate
   * <li>
   * <li>dueDate.
   * <li>
   * <p>
   * Note: postponementDate and due date comes under OR conditions. the service with due date or
   * postponementDate with specified date range will be retrieved.
   *
   * @param serviceSubQuery the AbstractSubQueryDto.
   * @param dueDateMin the minimum date.
   * @param dueDateMax the maximum date.
   *
   */
  private static void serviceSubQueryWithDate(final AbstractSubQueryDto serviceSubQuery, final Date dueDateMin,
      final Date dueDateMax) {

    serviceSubQuery.getSubQuery().getAnd().add(newQuery("active", QueryRelationshipType.EQ, true));
    serviceSubQuery.getSubQuery().getAnd()
        .add(newQuery("serviceStatus.id", QueryRelationshipType.NE, ServiceStatus.COMPLETE.getValue()));

    final AbstractQueryDto subQueryOr = new AbstractQueryDto();
    subQueryOr.setOr(new ArrayList<>());

    final AbstractQueryDto queryByServicesPostponementDate = new AbstractQueryDto();
    queryByServicesPostponementDate.setAnd(new ArrayList<>());
    queryByServicesPostponementDate.getAnd().add(newQuery("postponementDate", QueryRelationshipType.GE, dueDateMin));
    queryByServicesPostponementDate.getAnd().add(newQuery("postponementDate", QueryRelationshipType.LE, dueDateMax));
    subQueryOr.getOr().add(queryByServicesPostponementDate);

    final AbstractQueryDto queryByServicesDueDate = new AbstractQueryDto();
    queryByServicesDueDate.setAnd(new ArrayList<>());
    queryByServicesDueDate.getAnd().add(newQuery("postponementDate", QueryRelationshipType.NULL, true));
    queryByServicesDueDate.getAnd().add(newQuery("dueDate", QueryRelationshipType.GE, dueDateMin));
    queryByServicesDueDate.getAnd().add(newQuery("dueDate", QueryRelationshipType.LE, dueDateMax));
    subQueryOr.getOr().add(queryByServicesDueDate);

    serviceSubQuery.getSubQuery().getAnd().add(subQueryOr);
  }



  /**
   * Query by services catalogue id.
   *
   * @param cond condition.
   * @param id catalogue id.
   * @return predicate.
   */
  public static Predicate servicesCatalogueId(final QueryRelationshipType cond, final Long... id) {
    return () -> newQuery("services", "serviceCatalogue.id", cond, id);
  }

  /**
   * Query by asset wildcard search imo number and asset name with published version.
   *
   * @param value value.
   * @return predicate.
   */
  public static Predicate assetWildcardSearch(final String value) {
    return () -> {
      final AbstractQueryDto searchByName = new AbstractQueryDto();
      searchByName.setAnd(new ArrayList<>());
      searchByName.setSelectWhenFieldIsMissing(false);

      final AbstractQueryDto searchAllVersions =
          newQuery("allVersions", "name", QueryRelationshipType.LIKE, value.replace("*", "%"));
      searchByName.getAnd().add(searchAllVersions);

      final AbstractQueryDto assetVersion = new AbstractQueryDto();
      assetVersion.setJoinField("asset");
      assetVersion.setField("publishedVersionId");
      final AbstractQueryDto searchVersions =
          newQuery("allVersions", "assetVersionId", QueryRelationshipType.EQ, assetVersion);
      searchByName.getAnd().add(searchVersions);

      final AbstractQueryDto searchByImo =
          newQuery("publishedVersion", "imoNumber", QueryRelationshipType.LIKE, value.replace("*", "%"));

      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());
      query.getOr().add(searchByName);
      query.getOr().add(searchByImo);

      final AbstractQueryDto searchByNameIhs =
          newQuery("publishedVersion", "name", QueryRelationshipType.LIKE, value.replace("*", "%"));
      query.getOr().add(searchByNameIhs);

      return query;
    };
  }

  /**
   * Query by asset wildcard search imo number and asset name including former asset name..
   *
   * @param value value.
   * @return predicate.
   */
  public static Predicate assetSearchWithFormerAssetName(final String value) {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());

      final AbstractQueryDto searchByName =
          newQuery("allVersions", "name", QueryRelationshipType.LIKE, value.replace("*", "%"));

      final AbstractQueryDto searchByImo =
          newQuery("publishedVersion", "imoNumber", QueryRelationshipType.LIKE, value.replace("*", "%"));

      final AbstractQueryDto searchByNameIhs =
          newQuery("publishedVersion", "name", QueryRelationshipType.LIKE, value.replace("*", "%"));
      query.getOr().add(searchByNameIhs);

      final AbstractQueryDto searchByNameIhsFormerData =
          newQuery("publishedVersion.formerData", "keyName", QueryRelationshipType.LIKE, value.replace("*", "%"));
      query.getOr().add(searchByName);
      query.getOr().add(searchByImo);
      query.getOr().add(searchByNameIhs);
      query.getOr().add(searchByNameIhsFormerData);

      return query;
    };
  }

  /**
   * Query by former asset name with all versions of assets name except current version.
   *
   * @param name the former asset name.
   * @return predicate.
   */
  public static Predicate formerAssetName(final String name) {
    return () -> {

      final AbstractQueryDto searchByName = new AbstractQueryDto();
      searchByName.setAnd(new ArrayList<>());
      searchByName.setSelectWhenFieldIsMissing(false);

      final AbstractQueryDto searchAllVersions =
          newQuery("allVersions", "name", QueryRelationshipType.LIKE, name.replace("*", "%"));
      searchByName.getAnd().add(searchAllVersions);

      final AbstractQueryDto assetVersion = new AbstractQueryDto();
      assetVersion.setJoinField("asset");
      assetVersion.setField("publishedVersionId");
      final AbstractQueryDto searchVersions =
          newQuery("allVersions", "assetVersionId", QueryRelationshipType.NE, assetVersion);
      searchByName.getAnd().add(searchVersions);

      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());
      query.getOr().add(searchByName);

      final AbstractQueryDto searchByNameIhs =
          newQuery("publishedVersion.formerData", "keyName", QueryRelationshipType.LIKE, name.replace("*", "%"));
      query.getOr().add(searchByNameIhs);

      return query;
    };
  }

  /**
   * Query by asset lifecycle status id.
   *
   * @param cond condition.
   * @param values values.
   * @return predicate.
   */
  public static Predicate assetLifecycleStatusId(final QueryRelationshipType cond, final Long... values) {
    return () -> newQuery("publishedVersion", "assetLifecycleStatus.id", cond, values);
  }

  /**
   * Query by asset IMO number.
   *
   * @param cond condition.
   * @param imoNumber single or array of imo number.
   * @return predicate.
   */
  public static Predicate assetImoNumber(final QueryRelationshipType cond, final String... imoNumber) {
    return () -> newQuery("publishedVersion", "imoNumber", cond, imoNumber);
  }

  /**
   * Query by effective date.
   *
   * @param effectiveDateMin min effective date.
   * @param effectiveDateMax max effective date.
   * @return predicate.
   */
  public static Predicate assetEffectiveDate(final Date effectiveDateMin, final Date effectiveDateMax) {
    return () -> between("publishedVersion", "effectiveDate", effectiveDateMin, effectiveDateMax);
  }

  /**
   * Query by class department id.
   *
   * @param cond condition.
   * @param classDepartmentId single or list of class department id.
   * @return predicate.
   */
  public static Predicate classDepartmentId(final QueryRelationshipType cond, final Long... classDepartmentId) {
    return () -> newQuery("publishedVersion", "classDepartment.id", cond, classDepartmentId);
  }

  /**
   * Query by linked asset id.
   *
   * @param cond condition.
   * @param linkedAssetId link asset id or list of id.
   * @return predicate.
   */
  public static Predicate linkedAssetId(final QueryRelationshipType cond, final Long... linkedAssetId) {
    return () -> newQuery("publishedVersion", "linkedAsset.id", cond, linkedAssetId);
  }

  /**
   * Query by asset dead weight range.
   *
   * @param deadWeightMin min dead weight.
   * @param deadWeightMax max dead weight.
   * @return predicate.
   */
  public static Predicate deadWeight(final Double deadWeightMin, final Double deadWeightMax) {
    return () -> between("publishedVersion", "deadWeight", deadWeightMin, deadWeightMax);
  }

  /**
   * Returns the query to search assets based on builder name.
   * <p>
   * To support both MAST and IHS query, the criteria need to defines 2 conditions:
   * <ol>
   * <li>From MAST on publishedVersion.builder</li>
   * <li>From IHS on publishedVersion.builderInfo.name</li>
   * </ol>
   * </p>
   *
   * @param cond the query condition on builder name field.
   * @param builderName the ship builder name.
   * @return the query condition that cover both MAST and IHS search by builder name.
   */
  public static Predicate builderName(final QueryRelationshipType cond, final String... builderName) {
    return () -> {
      final AbstractQueryDto byMastIhsBuilderName = new AbstractQueryDto();
      byMastIhsBuilderName.setOr(new ArrayList<>());
      byMastIhsBuilderName.setSelectWhenFieldIsMissing(false);

      final AbstractQueryDto searchIhsBuilder =
          newQuery("publishedVersion.builderInfo", JoinType.LEFT, "name", cond, false, builderName);
      byMastIhsBuilderName.getOr().add(searchIhsBuilder);

      final AbstractQueryDto searchMastBuilder = newQuery("publishedVersion", "builder", cond, builderName);
      byMastIhsBuilderName.getOr().add(searchMastBuilder);

      return byMastIhsBuilderName;
    };
  }

  /**
   * Returns the query to search an asset with ship builder IMO or name from both MAST and IHS.
   * <p>
   * MAST only store the ship builder name without so the name search is for MAST assets. IHS will
   * have the ship builder IMO so IMO search is for IHS assets.
   * </p>
   *
   * @param builderImo the ship builder IMO number.
   * @param builderName the ship builder name.
   * @return the query condition for searching using builder IMO or builder name.
   */
  public static Predicate builderIdAndBuilderName(final String builderImo, final String builderName) {

    return () -> {
      final AbstractQueryDto byBuilderIdOrName = new AbstractQueryDto();
      byBuilderIdOrName.setOr(new ArrayList<>());
      byBuilderIdOrName.setSelectWhenFieldIsMissing(false);

      final AbstractQueryDto searchByBuilderId =
          newQuery("publishedVersion.builderInfo", JoinType.LEFT, "code", QueryRelationshipType.EQ, false, builderImo);
      byBuilderIdOrName.getOr().add(searchByBuilderId);

      final AbstractQueryDto searchByBuilderName =
          newQuery("publishedVersion", "builder", QueryRelationshipType.EQ, builderName);
      byBuilderIdOrName.getOr().add(searchByBuilderName);

      return byBuilderIdOrName;
    };
  }

  /**
   * Filter the result with assets with active services.
   *
   * @return predicate.
   */
  public static Predicate hasActiveServices() {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setJoinField("services");
      query.setJoinType(JoinType.INNER);
      query.setField("active");
      query.setRelationship(QueryRelationshipType.EQ);
      query.setValue(true);
      query.setSelectWhenFieldIsMissing(false);

      return query;
    };
  }

  /**
   * Filter the result with assets with active services.
   *
   * @return predicate.
   */
  public static Predicate excludeInActiveAsset() {
    return () -> {
      final AbstractQueryDto query = new AbstractQueryDto();
      query.setOr(new ArrayList<>());

      final AbstractQueryDto assetLifecycleStatus = new AbstractQueryDto();
      assetLifecycleStatus.setAnd(new ArrayList<>());

      query.getOr().add(newQuery("publishedVersion.ssch2", "status", QueryRelationshipType.IN, IHS_ASSET_STATUS));
      assetLifecycleStatus.getAnd()
          .add(newQuery("publishedVersion", "assetLifecycleStatus", QueryRelationshipType.IN, ASSET_LIFECYCLE_STATUS));
      /**
       * the below condition "queryExclude" to check mast only asset has been commented temporarily
       * to address the current issue from mast(LRD-17339) on prioritizing between IHS and MAST
       * asset
       */
      // final AbstractQueryDto queryExclude = new AbstractQueryDto();
      // queryExclude.setJoinField("publishedVersion");
      // queryExclude.setJoinType(JoinType.INNER);
      // queryExclude.setField("imoNumber");
      // queryExclude.setRelationship(QueryRelationshipType.NULL);
      // queryExclude.setValue(true);
      // queryExclude.setSelectWhenFieldIsMissing(false);
      //
      // assetLifecycleStatus.getAnd().add(queryExclude);
      query.getOr().add(assetLifecycleStatus);

      return query;
    };
  }


  /**
   * Create {@link AbstractQueryDto} with field only.
   *
   * @param field field name.
   * @param values field value.
   * @param type relationship type.
   * @param <T> generic type.
   * @return query dto.
   */
  private static <T> AbstractQueryDto newQuery(final String field, final QueryRelationshipType type,
      final T... values) {
    final AbstractQueryDto dto = new AbstractQueryDto();
    dto.setField(field);
    dto.setValue(getValue(type, values));
    dto.setRelationship(type);

    return dto;
  }

  /**
   * Create {@link AbstractQueryDto} with join field.
   *
   * @param joinField join field name.
   * @param joinType join type.
   * @param field field name.
   * @param values field value.
   * @param type relationship type.
   * @param selectMissing select when missing.
   * @param <T> generic type.
   * @return query dto.
   */
  private static <T> AbstractQueryDto newQuery(final String joinField, final JoinType joinType, final String field,
      final QueryRelationshipType type, final Boolean selectMissing, final T... values) {

    final AbstractQueryDto dto = new AbstractQueryDto();
    dto.setJoinField(joinField);
    dto.setJoinType(joinType);
    dto.setField(field);
    dto.setValue(getValue(type, values));
    dto.setRelationship(type);
    dto.setSelectWhenFieldIsMissing(selectMissing);

    return dto;
  }

  /**
   * Create {@link AbstractQueryDto} with default join type.
   *
   * @param joinField join field name.
   * @param field field name.
   * @param values field value.
   * @param type relationship type.
   * @param <T> generic type.
   * @return query dto.
   */
  private static <T> AbstractQueryDto newQuery(final String joinField, final String field,
      final QueryRelationshipType type, final T... values) {
    return newQuery(joinField, JoinType.INNER, field, type, false, values);
  }

  /**
   * Query based on list of codicil type ids.
   *
   * @param cond condition.
   * @param codicilStatusIds the codicil types to include.
   * @param codicilConfidentialityTypeIds the codicil allowable confidentiality types.
   * @param values values.
   * @return predicate.
   */
  public static Predicate codicilsQuery(final QueryRelationshipType cond, final List<Long> codicilStatusIds,
      final List<Long> codicilConfidentialityTypeIds, final Long... values) {
    return () -> {
      final AbstractQueryDto codicilQuery = new AbstractQueryDto();
      codicilQuery.setAnd(new ArrayList<>());
      codicilQuery.getAnd().add(newQuery("codicils", "typeId", QueryRelationshipType.IN, values));
      codicilQuery.getAnd().add(newQuery("codicils", "status", QueryRelationshipType.IN,
          codicilStatusIds.toArray(new Long[codicilStatusIds.size()])));
      // LRCD-3648-add codicil confidentiality check in mast-and-ihs-query
      codicilQuery.getAnd().add(newQuery("codicils", "confidentialityType.id", cond,
          codicilConfidentialityTypeIds.toArray(new Long[codicilConfidentialityTypeIds.size()])));

      return codicilQuery;
    };
  }

  /**
   * Query by due date range, status list and types for codicils or services by postponement date
   * range and services status..
   *
   * @param codicilStatusIds the list of codicil status to be filtered.
   * @param codicilTypeIds the list of codicil types to be filtered.
   * @param serviceStatusIds the list of status to be filtered.
   * @param dueDateMin the min due date.
   * @param dueDateMax the max due date.
   * @param postponementDateMin the max postponement date.
   * @param postponementDateMax the max postponement date.
   * @param confidentialityTypeIds the allowable confidentiality types.
   * @return predicate.
   */
  public static Predicate codicilsAndServicesDue(final List<Long> codicilStatusIds, final List<Long> codicilTypeIds,
      final List<Long> serviceStatusIds, final Date dueDateMin, final Date dueDateMax, final Date postponementDateMin,
      final Date postponementDateMax, final List<Long> confidentialityTypeIds) {
    return () -> {
      final AbstractQueryDto or = new AbstractQueryDto();
      or.setOr(new ArrayList<>());

      final AbstractQueryDto codicilsSubQuery = new AbstractQueryDto();
      codicilsSubQuery.setAnd(new ArrayList<>());
      codicilsSubQuery.getAnd().add(newQuery("codicils", "dueDate", QueryRelationshipType.GE, dueDateMin));
      codicilsSubQuery.getAnd().add(newQuery("codicils", "dueDate", QueryRelationshipType.LE, dueDateMax));
      codicilsSubQuery.getAnd().add(newQuery("codicils", "typeId", QueryRelationshipType.IN,
          codicilTypeIds.toArray(new Long[codicilTypeIds.size()])));
      codicilsSubQuery.getAnd().add(newQuery("codicils", "status", QueryRelationshipType.IN,
          codicilStatusIds.toArray(new Long[codicilStatusIds.size()])));
      codicilsSubQuery.getAnd().add(newQuery("codicils", "confidentialityType.id", QueryRelationshipType.IN,
          confidentialityTypeIds.toArray(new Long[confidentialityTypeIds.size()])));

      or.getOr().add(codicilsSubQuery);

      final AbstractQueryDto serviceSubQuery = new AbstractQueryDto();
      serviceSubQuery.setAnd(new ArrayList<>());
      serviceSubQuery.getAnd()
          .add(newQuery("services", "postponementDate", QueryRelationshipType.GE, postponementDateMin));
      serviceSubQuery.getAnd()
          .add(newQuery("services", "postponementDate", QueryRelationshipType.LE, postponementDateMax));
      serviceSubQuery.getAnd().add(newQuery("services", "surveyStatus", QueryRelationshipType.IN,
          serviceStatusIds.toArray(new Long[serviceStatusIds.size()])));

      or.getOr().add(serviceSubQuery);

      return or;
    };
  }

  /**
   * Returns the query to search jobs based on below conditions.
   * <ol>
   * <li>Gets jobs in the range of min closed and max closed dates.</li>
   * <li>Gets jobs with closed status.</li>
   * </ol>
   *
   * @param minCloseddate the minimun closed date condition on builder completedOn field.
   * @param maxClosedDate the maximum closed date condition on builder completedOn field.
   * @param jobStatusIds the job status list.
   * @return the query condition that cover both MAST and IHS search by builder name.
   */
  public static AbstractQueryDto closedJobsWithRange(final Date minCloseddate, final Date maxClosedDate,
      final List<Long> jobStatusIds) {
    final AbstractQueryDto closedJobsBuilder = new AbstractQueryDto();
    closedJobsBuilder.setAnd(new ArrayList<>());
    closedJobsBuilder.setSelectWhenFieldIsMissing(false);

    closedJobsBuilder.getAnd().add(newQuery("completedOn", QueryRelationshipType.GE, minCloseddate));
    closedJobsBuilder.getAnd().add(newQuery("completedOn", QueryRelationshipType.LE, maxClosedDate));
    closedJobsBuilder.getAnd()
        .add(newQuery("jobStatus.id", QueryRelationshipType.EQ, jobStatusIds.toArray(new Long[jobStatusIds.size()])));

    return closedJobsBuilder;
  }

  /**
   * Create between query.
   *
   * @param joinField join field name.
   * @param field field name.
   * @param fromValue field value from.
   * @param toValue field value to.
   * @return query dto.
   */
  private static AbstractQueryDto between(final String joinField, final String field, final Object fromValue,
      final Object toValue) {
    final AbstractQueryDto from = newQuery(joinField, field, QueryRelationshipType.GE, fromValue);
    final AbstractQueryDto to = newQuery(joinField, field, QueryRelationshipType.LE, toValue);
    final AbstractQueryDto between = new AbstractQueryDto();
    between.setAnd(new ArrayList<>());
    between.getAnd().add(from);
    between.getAnd().add(to);

    return between;
  }

  /**
   * Create between query.
   *
   * @param field field name.
   * @param fromValue field value from.
   * @param toValue field value to.
   * @return query dto.
   */
  private static AbstractQueryDto between(final String field, final Object fromValue, final Object toValue) {
    final AbstractQueryDto from = newQuery(field, QueryRelationshipType.GE, fromValue);
    final AbstractQueryDto to = newQuery(field, QueryRelationshipType.LE, toValue);
    final AbstractQueryDto between = new AbstractQueryDto();
    between.setAnd(new ArrayList<>());
    between.getAnd().add(from);
    between.getAnd().add(to);

    return between;
  }

  /**
   * Get value from varargs.
   *
   * @param type relationship.
   * @param values values.
   * @param <T> generic type.
   * @return object.
   */
  private static <T> Object getValue(final QueryRelationshipType type, final T[] values) {
    final Object returnVal;

    if (QueryRelationshipType.IN.equals(type)) {
      returnVal = values;
    } else if (values != null && values.length > 0) {
      returnVal = values[0];
    } else {
      returnVal = values;
    }

    return returnVal;
  }

  /**
   * Add to mandatory list.
   *
   * @param predicate clause.
   * @return this object.
   */
  public MastQueryBuilder mandatory(final Predicate predicate) {
    mandatory.add(predicate.evaluate());

    return this;
  }

  /**
   * Add to optional list.
   *
   * @param predicate clause.
   * @return this object.
   */
  public MastQueryBuilder optional(final Predicate predicate) {
    optional.add(predicate.evaluate());

    return this;
  }

  /**
   * Add to IHS priority mandatory list.
   *
   * @param predicate clause.
   * @return this object.
   */
  public MastQueryBuilder ihsPriorityMandatory(final Predicate predicate) {
    ihsPriorityMandatory.add(predicate.evaluate());
    return this;
  }

  /**
   * Add to IHS priority optional list.
   *
   * @param predicate clause.
   * @return this object.
   */
  public MastQueryBuilder ihsPriorityOptional(final Predicate predicate) {
    ihsPriorityOptional.add(predicate.evaluate());
    return this;
  }

  /**
   * Build the query.
   *
   * @return query.
   */
  public AbstractQueryDto build() {
    return buildQuery(mandatory, optional);

  }

  /**
   * Build the query with the given mandatory and optional field list .
   *
   * @param mandatoryList mandatory(and) field list.
   * @param optionalList optional(or) field list.
   * @return query.
   */
  public AbstractQueryDto buildQuery(final List<AbstractQueryDto> mandatoryList,
      final List<AbstractQueryDto> optionalList) {
    final AbstractQueryDto query = new AbstractQueryDto();

    final List<AbstractQueryDto> listOfAnds = new ArrayList<>();
    final List<AbstractQueryDto> listOfOrs = new ArrayList<>();

    if (!mandatoryList.isEmpty()) {
      listOfAnds.addAll(mandatoryList);
    }

    if (!optionalList.isEmpty()) {
      listOfOrs.addAll(optionalList);
    }

    if (!listOfAnds.isEmpty() && !listOfOrs.isEmpty()) {
      final AbstractQueryDto or = new AbstractQueryDto();
      or.setOr(listOfOrs);
      listOfAnds.add(or);
      query.setAnd(listOfAnds);
    } else if (!listOfOrs.isEmpty()) {
      query.setOr(listOfOrs);
    } else if (!listOfAnds.isEmpty()) {
      query.setAnd(listOfAnds);
    }
    return query;

  }

  /**
   * Build the query.
   *
   * @return query.
   */
  public PrioritisedAbstractQueryDto buildMastIhs() {

    final PrioritisedAbstractQueryDto pQuery = new PrioritisedAbstractQueryDto();
    final AbstractQueryDto mastQuery = buildQuery(mandatory, optional);

    if ((mastQuery.getAnd() != null && !mastQuery.getAnd().isEmpty())
        || (mastQuery.getOr() != null && !mastQuery.getOr().isEmpty())) {
      pQuery.setMastPriorityQuery(mastQuery);
    }

    final AbstractQueryDto ihsQuery = buildQuery(ihsPriorityMandatory, ihsPriorityOptional);

    if ((ihsQuery.getAnd() != null && !ihsQuery.getAnd().isEmpty())
        || (ihsQuery.getOr() != null && !ihsQuery.getOr().isEmpty())) {
      pQuery.setIhsPriorityQuery(ihsQuery);
    }
    return pQuery;
  }

  /**
   * Returns whether the query has any condition.
   *
   * @return true if query do not has any condition otherwise false.
   */
  public boolean isEmpty() {
    return mandatory.isEmpty() && optional.isEmpty() && ihsPriorityMandatory.isEmpty() && ihsPriorityOptional.isEmpty();
  }

  /**
   * Static method to create new query builder.
   *
   * @return query builder instance.
   */
  public static MastQueryBuilder create() {
    return new MastQueryBuilder();
  }
}
