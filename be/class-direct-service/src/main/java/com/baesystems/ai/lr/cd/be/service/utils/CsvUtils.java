package com.baesystems.ai.lr.cd.be.service.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Provides utility for csv exports.
 *
 * @author syalavarthi.
 *
 */
public final class CsvUtils {
  /**
   * Returns CsvUtils.
   */
  private CsvUtils() {

  }
  /**
   * The {@value #EQUAL_TO_SYMBOL} used in validation before adding to csv file.
   */
  public static final String EQUAL_TO_SYMBOL = "=";

  /**
   * Validates and Removes first character of a string if it starts with "=" symbol.
   *
   * @param value the data to be validated.
   * @return data after validation.
   */
  public static String sanitiseFirstCharacter(final String value) {
    String result = value;
    if (!StringUtils.isEmpty(result)) {
      if (result.trim().startsWith(EQUAL_TO_SYMBOL)) {
        return result.trim().substring(1);
      }
    }
    return result;
  }
}
