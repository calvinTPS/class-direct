package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Base export for codicil and defect.
 *
 * @author yng
 *
 * @param <T> type cast.
 */
public abstract class BasePdfExport<T> {

  /**
   * Injected service.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * key variable to parse to pug file.
   */
  private static final String TEMPLATE_HELPER = "helper";

  /**
   * Date formatter appended to working directory.
   */
  private static final DateTimeFormatter DIR_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS");

  /**
   * Date formatter appended to zip file.
   */
  protected static final DateTimeFormatter FILE_DATE_FORMAT = DateTimeFormatter.ofPattern("ddMMyyyyHHmm");

  /**
   * File name word splitter.
   */
  protected static final String WORD_SPLITTER = "_";

  /**
   * s3 supported file separator.
   */
  private static final String S3_FILE_SEPARATOR = "/";

  /**
   * s3 bucket.
   */
  @Value("${s3.export.bucket.path}")
  private String s3Bucket;


  /**
   * create pdf file.
   *
   * @param query body.
   * @return file name object.
   * @throws ClassDirectException value.
   */
  public final StringResponse downloadPdf(final T query) throws ClassDirectException {
    try {
      // get current time.
      final LocalDateTime current = LocalDateTime.now();

      // get user info
      final UserProfiles user = getUserInfo();

      // create working directory
      final String workingDir = createWorkingDir(current, user);

      // create model
      final Map<String, Object> model = new TreeMap<>();

      // include helper module
      model.put(TEMPLATE_HELPER, new TemplateHelper());
      model.put("currentDate", new Date());

      // generate model and file(s)
      // 1. generate model
      // 2. generate file (generate zip if > 1 file)
      // 3. return file name.
      final String filename = generateFile(model, query, user, workingDir, current);


      try {
        // retrieve s3 token
        final String key = user.getUserId() + S3_FILE_SEPARATOR + getS3ExportDir() + S3_FILE_SEPARATOR + filename;
        final String token = amazonStorageService.uploadFile(s3Bucket, key,
            PdfUtils.convertToLinuxPathDelimiter(workingDir + File.separator + filename), user.getUserId());

        return new StringResponse(token, filename);
      } finally {
        FileUtils.deleteDirectory(new File(workingDir));
      }


    } catch (final IOException | JadeException exception) {
      throw new ClassDirectException(exception);
    }

  }

  /**
   * generate file.
   *
   * @param model for template.
   * @param query body.
   * @param user profile.
   * @param workingDir working directory.
   * @param current date time.
   * @return name of generated file.
   * @throws ClassDirectException value.
   */
  protected abstract String generateFile(Map<String, Object> model, T query, UserProfiles user, String workingDir,
      LocalDateTime current) throws ClassDirectException;

  /**
   * Get directory to be store in s3.
   *
   * @return directory name.
   */
  protected abstract String getS3ExportDir();

  /**
   * Temporary base directory in local.
   *
   * @return directory path.
   */
  protected abstract String getTempDir();

  /**
   * Generate pdf file.
   *
   * @param prefix file prefix.
   * @param code asset code.
   * @param time current timestamp.
   * @param template file.
   * @param workingDir path.
   * @param model for template.
   * @return pdf file name.
   *
   * @throws JadeException value.
   * @throws IOException value.
   * @throws ParserConfigurationException value.
   * @throws SAXException value.
   * @throws DocumentException value.
   */
  public final String generatePdf(final String prefix, final String code, final LocalDateTime time,
      final String template, final String workingDir, final Map<String, Object> model)
      throws JadeException, IOException, ParserConfigurationException, SAXException, DocumentException {
    final String pdfFile = generatePdfWithoutWatermark(prefix, code, time, template, workingDir, model);

    PdfUtils.createWatermark(workingDir + File.separator + pdfFile);

    return pdfFile;
  }

  /**
   * Generate pdf without watermark.
   *
   * @param prefix file prefix.
   * @param code asset code.
   * @param time current timestamp.
   * @param template file.
   * @param workingDir path.
   * @param model for template.
   * @return pdf file name.
   *
   * @throws JadeException value.
   * @throws IOException value.
   * @throws ParserConfigurationException value.
   * @throws SAXException value.
   * @throws DocumentException value.
   */
  public final String generatePdfWithoutWatermark(final String prefix, final String code, final LocalDateTime time,
      final String template, final String workingDir, final Map<String, Object> model)
      throws JadeException, IOException, ParserConfigurationException, SAXException, DocumentException {
    final StringBuilder pdfFile = new StringBuilder();
    pdfFile.append(prefix).append(WORD_SPLITTER).append(code).append(WORD_SPLITTER)
        .append(time.format(FILE_DATE_FORMAT)).append(".pdf");

    PdfUtils.createPdfFromTemplate(model, workingDir, pdfFile.toString(), template);

    return pdfFile.toString();
  }


  /**
   * Create working directory.
   *
   * @param time current timestamp.
   * @param user user object.
   * @return working directory path.
   * @throws IOException value.
   */
  public final String createWorkingDir(final LocalDateTime time, final UserProfiles user) throws IOException {
    // prepare download directory by <user_id>_<timestamp>
    final String workingDir =
        getTempDir() + File.separator + user.getUserId() + WORD_SPLITTER + time.format(DIR_DATE_FORMAT);

    final File createDir = new File(workingDir);
    if (!createDir.exists()) {
      FileUtils.forceMkdir(createDir);
    }

    return workingDir;
  }

  /**
   * Get user info.
   *
   * @return user info.
   */
  public final UserProfiles getUserInfo() {
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    final UserProfiles user = token.getDetails();

    return user;
  }
}
