package com.baesystems.ai.lr.cd.service.asset.reference;

/**
 * Provides service to get latest version of reference data.
 *
 * @author MSidek
 *
 */
public interface ReferenceDataService {

  /**
   * Returns latest version of reference data.
   *
   * @return reference data version.
   */
  Long getCurrentVersion();
}
