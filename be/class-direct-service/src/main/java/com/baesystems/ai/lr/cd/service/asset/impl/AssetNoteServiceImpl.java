package com.baesystems.ai.lr.cd.service.asset.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.jcabi.aspects.Loggable;

/**
 * Provides the interface for querying asset note.
 *
 * @author yng
 *
 */
@Service(value = "AssetNoteService")
@Loggable(Loggable.DEBUG)
public class AssetNoteServiceImpl implements AssetNoteService {
  /**
   * The logger instantiated for {@link AssetNoteServiceImpl}.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetNoteServiceImpl.class);

  /**
   * The {@link AssetRetrofitService} from Spring context.
   *
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The default value for category type statutory.
   */
  private static final String CATEGORYSTATUTORY = "Statutory";

  @RestrictedAsset
  @Override
  public final PageResource<AssetNoteHDto> getAssetNote(final Integer page, final Integer size,
      @AssetID final Long assetId) throws ClassDirectException {
    PageResource<AssetNoteHDto> pageResource = null;

    pageResource = PaginationUtil.paginate(getAssetNote(assetId), page, size);
    return pageResource;
  }

  /**
   * Returns list of asset note.
   *
   * @param assetId the asset Id.
   * @throws ClassDirectException if API call fail.
   * @return the list of asset note.
   */
  public final List<AssetNoteHDto> getAssetNote(final long assetId) throws ClassDirectException {
    final List<AssetNoteHDto> contentH = new ArrayList<AssetNoteHDto>();

    try {
      // get roles for user
      final boolean equasisThesisUser = SecurityUtils.isEquasisThetisUser();

      final List<AssetNoteHDto> response = assetServiceDelegate.getAssetNoteDto(assetId).execute().body();
      if (response != null && !response.isEmpty()) {
        response.stream().forEachOrdered(assetNote -> {
          // hydrate the data
          Resources.inject(assetNote);

          // filter data by Equasis/Thetis
          final String catName =
              Optional.ofNullable(assetNote.getCategoryH()).map(CodicilCategoryHDto::getName).orElse(null);
          if (equasisThesisUser && !CATEGORYSTATUTORY.equals(catName)) {
            return;
          }


          // get asset item data
          if (assetNote.getAssetItem() != null) {
            try {
              final LazyItemHDto assetItem =
                  assetServiceDelegate.getAssetItemDto(assetId, assetNote.getAssetItem().getId()).execute().body();
              assetNote.setAssetItemH(assetItem);
            } catch (final IOException ioException) {
              LOGGER.error("Error executing getAssetItemDto({}): Id not found.", assetNote.getAssetItem().getId(),
                  ioException);
            }
          }

          contentH.add(assetNote);
        });
      }
    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getAssetNote.", ioException);
      throw new ClassDirectException(ioException);
    }
    // sort Asset Notes by descending Impose Date LRCD-800, AC-3.
    // sort data by issueDate descending
    if (!contentH.isEmpty()) {
      Collections.sort(contentH, new AssetNoteComparator());
    }
    return contentH;
  }


  @RestrictedAsset
  @Override
  public final AssetNoteHDto getAssetNoteByAssetIdAssetNoteId(@AssetID final Long assetId, final long assetNoteId)
      throws ClassDirectException {
    AssetNoteHDto assetNoteResponse = null;

    try {
      final AssetNoteHDto assetNote =
          assetServiceDelegate.getAssetNoteByAssetIdAssetNoteId(assetId, assetNoteId).execute().body();

      // get asset item data
      if (assetNote.getAssetItem() != null) {
        try {
          final LazyItemHDto assetItem =
              assetServiceDelegate.getAssetItemDto(assetId, assetNote.getAssetItem().getId()).execute().body();
          assetNote.setAssetItemH(assetItem);
        } catch (final IOException ioException) {
          LOGGER.error("Error executing getAssetItemDto({}): Id not found.", assetNote.getAssetItem().getId(),
              ioException);
        }
      }
      // hydrate job data
      try {
        if (assetNote.getJob() != null) {
          JobHDto jobDto = jobServiceDelegate.getJobsByJobId(assetNote.getJob().getId()).execute().body();
          assetNote.setJobH(jobDto);
        }
      } catch (final Exception exception) {
        LOGGER.error("Error fetching job for actionable item {} . | Error Message: {}", assetNote.getId(),
            exception.getMessage());
      }

      Resources.inject(assetNote);

      assetNoteResponse = assetNote;

    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getAssetNote.", ioException);
      throw new ClassDirectException(ioException);
    }

    return assetNoteResponse;
  }

  /**
   * Sub class for asset note comparator sort by impose date descending order.
   *
   * @author Syalavarthi
   *
   */
  static class AssetNoteComparator implements Comparator<AssetNoteHDto>, Serializable {

    /**
     * the static serial version id.
     */
    private static final long serialVersionUID = -3616861287199577556L;

    @Override
    public int compare(final AssetNoteHDto object1, final AssetNoteHDto object2) {
      final Optional<Date> date1 = Optional.ofNullable(object1.getImposedDate());
      final Optional<Date> date2 = Optional.ofNullable(object2.getImposedDate());
      if (date1.isPresent() && date2.isPresent()) {
        return date2.get().compareTo(date1.get());
      } else if (date1.isPresent() && !date2.isPresent()) {
        return -1;
      } else if (!date1.isPresent() && date2.isPresent()) {
        return 1;
      } else {
        return 0;
      }
    }

  }

}
