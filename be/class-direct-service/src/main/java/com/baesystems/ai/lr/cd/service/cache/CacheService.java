package com.baesystems.ai.lr.cd.service.cache;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;

/**
 * Provides the interface for cache service.
 * <br>
 * <b>This is only created for tester to clear the cache during development purpose. This method
 * must be disabled or deleted in production.</b>
 *
 *
 * @author yng
 *
 */
public interface CacheService {


  /**
   * Flushs the cache.
   *
   * @return the flush status message.
   */
  StringResponse flushCache();



}
