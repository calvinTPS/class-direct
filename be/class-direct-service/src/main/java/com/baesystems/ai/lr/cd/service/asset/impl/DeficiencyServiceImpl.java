package com.baesystems.ai.lr.cd.service.asset.impl;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RectificationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.DeficiencyService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * Provides deficiency service to get paginated deficiency list, list of deficiencies and single
 * deficiency associated rectifications and statutory findings.
 *
 * @author syalavarthi.
 * @author YWearn
 * @author sBollu 5/5/2017.
 */
@Service(value = "DeficiencyService")
public class DeficiencyServiceImpl implements DeficiencyService {

  /**
   * The logger object for deficiency service {@link DeficiencyServiceImpl}.
   */
  public static final Logger LOGGER = LoggerFactory.getLogger(DeficiencyServiceImpl.class);
  /**
   * The {@link AssetRetrofitService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  @Override
  public final PageResource<DeficiencyHDto> getDeficienciesPageResource(final Integer page, final Integer size,
      final Long assetId, final DeficiencyQueryHDto query) throws ClassDirectException {
    PageResource<DeficiencyHDto> pageResource = null;

    pageResource = PaginationUtil.paginate(getDeficiencies(assetId, query), page, size);

    return pageResource;
  }

  @Override
  public final List<DeficiencyHDto> getDeficiencies(final Long assetId, final DeficiencyQueryHDto deficiencyQueryHDto)
      throws ClassDirectException {
    LOGGER.info("getting deficiencies for asset");
    List<DeficiencyHDto> deficiencies = null;

    try {
      deficiencies = assetRetrofitService.getDeficiencies(assetId, deficiencyQueryHDto).execute().body();
      if (Optional.ofNullable(deficiencies).isPresent()) {
        deficiencies.stream().forEach(deficiency -> {
          Resources.inject(deficiency);
        });
      }
    } catch (final IOException exception) {
      LOGGER.error("error in excecuting getDeficienciesForAsset assetId {}", assetId, exception);
      throw new ClassDirectException(exception);
    }
    return deficiencies;
  }

  @Override
  public final DeficiencyHDto getDeficiencyByAssetIdDeficiencyId(final Long assetId, final long deficiencyId)
      throws ClassDirectException {
    DeficiencyHDto deficiency = null;
    final List<Callable<Void>> tasks = new ArrayList<Callable<Void>>();
    try {
      deficiency = assetRetrofitService.getDeficiencyDto(assetId, deficiencyId).execute().body();
      Optional.ofNullable(deficiency).ifPresent(deficiencyObj -> {

        Resources.inject(deficiencyObj);

        tasks.add(() -> {
          // rectifications associated with deficiency.
          Optional.ofNullable(deficiencyObj.getRectifications()).ifPresent(rectificationList -> {

            deficiencyObj
                .setRectificationsH(getRectifications(deficiencyObj.getRectifications(), assetId, deficiencyId));
          });
          return null;
        });

        tasks.add(() -> {
          // statutoryFindings associated with deficiency.
          deficiencyObj.setStatutoryFindings(getStatutoryFindings(deficiencyObj, assetId));
          return null;
        });

        // Removed Additional steps to inject list of checklist to the deficiency and rectifactions
        // @see <a href="https://bae-lr.atlassian.net/projects/LRCDC/issues/LRCDC-261">LRCDC-261</a>
      });
    } catch (final IOException exception) {
      LOGGER.error("Error in getDeficiencyByAssetIdDefectId" + exception);
      throw new ClassDirectException(exception);
    }
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    try {
      executor.invokeAll(tasks);
    } catch (final InterruptedException exception) {
      LOGGER.error("Executor error while getting deficiency {}.", deficiencyId);
      throw new ClassDirectException(exception);
    } finally {
      executor.shutdown();
    }

    return deficiency;
  }

  /**
   * Returns list of rectification details on the given rectification id list.
   *
   * @param rectifications the list of rectification ids.
   * @param assetId the asset unique identifier..
   * @param deficiencyId the deficienctt unique identifier.
   * @return the list of rectifications details.
   */
  private List<RectificationHDto> getRectifications(final List<LinkResource> rectifications, final Long assetId,
      final long deficiencyId) {
    List<RectificationHDto> response = rectifications.stream().map(rect -> {
      try {
        final RectificationHDto rectification = assetRetrofitService
            .getRectificationByRectificationId(assetId, deficiencyId, rect.getId()).execute().body();
        Optional.ofNullable(rectification).ifPresent(Resources::inject);
        return rectification;
      } catch (final IOException exception) {
        LOGGER.error(
            "Error query getDeficiencyByAssetIdDeficiencyId - getRectificationByRectificationId({}): Id not found.",
            rect.getId(), exception);
      }
      return null;
    }).filter(Objects::nonNull).collect(Collectors.toList());
    return response;
  }

  /**
   * Gets list of statutory findings for a deficiency.
   *
   * @param deficiency the deficiency payload with id.
   * @param assetId the asset unique identifier.
   * @return the list of statutory findings associated to the given deficiency.
   */
  private List<StatutoryFindingHDto> getStatutoryFindings(final DeficiencyHDto deficiency, final Long assetId) {

    List<StatutoryFindingHDto> statutoryFindings = new ArrayList<>();
    try {
      statutoryFindings =
          assetRetrofitService.getStatutoryFindingsForDeficiency(assetId, deficiency.getId()).execute().body();

      if (statutoryFindings != null) {
        statutoryFindings.forEach(Resources::inject);
        LOGGER.info("stautory status is match with codicilstatus SF_OPEN {}", statutoryFindings.size());
      }

    } catch (final IOException exception) {
      LOGGER.error("Error query getStatutoryFindingsForDeficiency assetId {}, deficiencyId {}", assetId,
          deficiency.getId(), exception);
    }
    return statutoryFindings;
  }

}
