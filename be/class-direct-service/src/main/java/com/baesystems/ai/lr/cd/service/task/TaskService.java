package com.baesystems.ai.lr.cd.service.task;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.HierarchyDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides the base model interface for updating tasks, getting tasks for asset or service, getting
 * checklist's for service.
 *
 * @author VKolagutla
 * @author SBollu
 */
public interface TaskService extends DueStatusCalculator<WorkItemLightHDto> {

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) paginated list
   * of tasks associated with the assetId using pmsAble filter. Filter for task has service with
   * continuous indicator as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   * @param assetId the asset unique identifier.
   * @param pmsAble pmsAble whether the task is pmsCreditable.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @return paginated list of tasks.
   * @throws ClassDirectException if execution fails or fail to find object with given unique
   *         identifier.
   * @throws UnauthorisedAccessException if user is not allowed to access.
   * @throws IOException if API call fails.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN', 'LR_SUPPORT', 'LR_INTERNAL', 'CLIENT')")
  PageResource<WorkItemLightHDto> getTasksByAssetId(final Long assetId, final Boolean pmsAble, final Integer page,
      final Integer size) throws ClassDirectException, UnauthorisedAccessException, IOException;



  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) list of tasks
   * associated with the assetId using pmsAble filter. Filter for task has service with continuous
   * indicator as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   *
   * @param assetId the asset unique identifier.
   * @param pmsAble whether the task is pmsCreditable.
   * @return list of tasks.
   * @throws ClassDirectException if execution fails or fail to find object with given unique
   *         identifier.
   * @throws IOException if API call fails.
   */
  List<WorkItemLightHDto> getTasksByAssetId(final Long assetId, final Boolean pmsAble)
      throws ClassDirectException, IOException;

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) paginated list
   * of tasks associated with the serviceId. Filter for task has service with continuous indicator
   * as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   * @param serviceId the service unique identifier.
   * @param assetId the asset unique identifier.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @return paginated list of tasks.
   * @throws ClassDirectException if execution fails or API call fails.
   */
  PageResource<WorkItemLightHDto> getTasksByService(final Long serviceId, final Long assetId, final Integer page,
      final Integer size) throws ClassDirectException;

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) paginated list
   * of tasks associated with list of services. Filter for task has service with continuous
   * indicator as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   * @param serviceList the list of schedule services.
   * @return the filtered tasks list group by service id.
   * @throws ClassDirectException if the service list is null or api to fetch task return error.
   */
  Map<Long, List<WorkItemLightHDto>> getTasksForServices(final List<ScheduledServiceHDto> serviceList)
      throws ClassDirectException;

  /**
   * Returns paginated list of checkList's associated with the serviceId.
   *
   * @param serviceId the service unique identifier.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @return list of checklist.
   * @throws ClassDirectException if execution fails or API call fails.
   * @throws IOException if API call fails.
   */
  PageResource<WorkItemLightHDto> getCheckListByService(final Long serviceId, final Integer page, final Integer size)
      throws ClassDirectException, IOException;

  /**
   * Updates list of pmsCreditable tasks to pmsCredited.
   *
   * @param workItemLightDto the list of pmsCreditable tasks.
   * @return list of pmsCredited tasks.
   * @throws ClassDirectException if execution fails or API call fails.
   * @throws UnauthorisedAccessException if user is not allowed to update.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN', 'LR_SUPPORT', 'LR_INTERNAL', 'CLIENT')")
  WorkItemLightListHDto updateTasksAsList(WorkItemLightListHDto workItemLightDto)
      throws ClassDirectException, UnauthorisedAccessException;


  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) hierarchical
   * list of tasks associated with the serviceId.
   *
   * @param serviceId the service unique identifier.
   * @param assetId the asset unique identifier.
   * @return list DTO which has services with hierarchical tasks..
   * @throws ClassDirectException if execution fails or API call fails.
   */
  WorkItemPreviewListHDto getHierarchicalTasksByService(final Long serviceId, final Long assetId)
      throws ClassDirectException;

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) hierarchical
   * list of tasks associated with the serviceId.
   *
   * @param serviceIds the service ids list.
   * @param assetId the asset unique identifier.
   * @return list DTO which has services with hierarchical tasks..
   * @throws ClassDirectException if execution fails or API call fails.
   */
  WorkItemPreviewListHDto getHierarchicalTasksByServiceIds(final List<Long> serviceIds, final Long assetId)
    throws ClassDirectException;

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) hierarchical
   * list of tasks associated with the assetId using pmsAble filter. Filter for task has service
   * with continuous indicator as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   * @param assetId the asset unique identifier.
   * @param pmsAble pmsAble whether the task is pmsCreditable.
   * @return paginated list of tasks.
   * @throws ClassDirectException if execution fails or fail to find object with given unique
   *         identifier.
   * @throws UnauthorisedAccessException if user is not allowed to access.
   * @throws IOException if API call fails.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN', 'LR_SUPPORT', 'LR_INTERNAL', 'CLIENT')")
  WorkItemPreviewListHDto getHierarchicalTasksByAssetId(final Long assetId, final Boolean pmsAble)
      throws ClassDirectException;

  /**
   * Returns list of checkList's associated with the serviceId grouped by checklist group id and sub
   * group id.
   *
   * @param serviceId the service unique identifier.
   * @return list of checklist.
   * @throws ClassDirectException if execution fails or API call fails.
   * @throws RecordNotFoundException if checklist id not found for checklist item.
   */
  HierarchyDto getCheckListHierarchyByService(final Long serviceId)
      throws RecordNotFoundException, ClassDirectException;

  /**
   * Returns list of checkList's associated with the serviceId grouped by checklist group id and sub
   * group id.
   *
   * @param serviceId list.
   * @return Map of service with checklist item.
   * @throws ClassDirectException if execution fails or API call fails.
   * @throws RecordNotFoundException if checklist id not found for checklist item.
   */
   Map<ScheduledServiceHDto, List<WorkItemLightHDto>> getCheckListItemByService(List<ScheduledServiceHDto> serviceId)
        throws RecordNotFoundException, ClassDirectException;

}
