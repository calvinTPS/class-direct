package com.baesystems.ai.lr.cd.service.survey.impl;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHeaderDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.OwnershipDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.survey.SurveyRequestDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.ProductTypeEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.InvalidDateException;
import com.baesystems.ai.lr.cd.be.exception.InvalidEmailException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.utils.ProductDemo;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceDemo;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.AssetEmblemUtils;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.mail.MailService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.survey.SurveyService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.enums.ClassStatus;
import com.baesystems.ai.lr.enums.PartyRole;
import com.jcabi.aspects.Loggable;

import lombok.Setter;

/**
 * Provides the local service validating Survey Request and sending mail to corresponding requested
 * user and asset CFO.
 *
 * @author NAvula
 * @author SBollu
 */
@Service(value = "SurveyService")
@Loggable(Loggable.DEBUG)
public class SurveyServiceImpl implements SurveyService {

  /**
   * The Logger display logs for {@link SurveyServiceImpl}.
   */
  private static final Logger LOG = LoggerFactory.getLogger(SurveyServiceImpl.class);
  /**
   * The {@link PortService} from Spring context.
   */
  @Autowired(required = true)
  private PortService portService;

  /**
   * The {@link MailService} from Spring context.
   */
  @Autowired(required = true)
  private MailService mailService;

  /**
   * The past date error message.
   */
  @Value("${dates.in.past}")
  private String dateInPastErrorMessage;

  /**
   * The arrival date error message.
   */
  @Value("${survey.date.same.or.after.arrival.date}")
  private String surveyDateSameOrAfterArrivalDateErrorMessage;

  /**
   * The departure date error message.
   */
  @Value("${survey.date.same.or.before.departure.date}")
  private String surveyDateSameOrBeforeDepartureDateErrorMessage;

  /**
   * The departure date after arrival date error message.
   */
  @Value("${departure.date.same.or.after.arrival.date}")
  private String departureDateSameOrAfterArrivalDateErrorMessage;

  /**
   * The arrival departure survey error message.
   */
  @Value("${arrival.departure.dates.same}")
  private String arrivalDepartureDatesSameErrorMessage;

  /**
   * The null date error message.
   */
  @Value("${null.date}")
  private String nullDateErrorMessage;

  /**
   * The departure time arrival time error message.
   */
  @Value("${departure.time.after.arrival.time}")
  private String departureTimeAfterArrivalTimeErrorMessage;

  /**
   * The record not found error message.
   */
  @Value("${record.not.found}")
  private String recordNotFound;

  /**
   * The error message for invalid email.
   */
  @Value("${invalid.email}")
  private String invalidEmailErrorMessage;
  /**
   * The email not found error message.
   */
  @Value("${user.email.not.found}")
  private String emailNotFound;
  /**
   * The root path to email assets (images, etc).
   */
  @Value("${notification.assets.path}")
  private String rootAssetPath;
  /**
   * The URL to asset detail.
   */
  @Value("${notification.assethub.link}")
  @Setter
  private String assetDetailUrl;
  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceService;
  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The Comma delimiter.
   */
  private static final String COMMA_DELIMITER = ",";

  /**
   * The SemiColon delimiter.
   */
  private static final String SEMI_COLON_DELIMITER = ";";

  /**
   * The PUG template variable that store root path.
   */
  private static final String PATH = "path";

  /**
   * The PUG template variable that store asset header details.
   */
  private static final String ASSET_HEADER = "asset";

  /**
   * The PUG template variable that store user company name.
   */
  private static final String CLIENT_NAME = "clientName";

  /**
   * The Date Pattern.
   */
  private static final String DATE_PATTERN_WITH_DOT = "dd.MM.YYYY";

  /**
   * The Date Pattern.
   */
  private static final String DATE_PATTERN_WITHOUT_DOT = "dd MMM YYYY";

  /**
   * The classificationService.
   */
  private static final String CLASSIFICATION_SERVICE = "classificationService";

  /**
   * The mmsStatutoryService.
   */
  private static final String MMS_STATUTORY_SERVICE = "mmsStatutoryService";

  /**
   * The request survey email title.
   */
  @Value("${request.survey.title}")
  private String emailTitle;

  /**
   * The request survey email additional text.
   */
  @Value("${request.survey.additional.text}")
  private String emailAdditionalText;
  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Returns success or failure status message for corresponding request survey of an asset.
   *
   * @param userId the user unique identifier.
   * @param dto the survey request model object.
   * @return success or failure status message.
   * @throws ClassDirectException if validation fails for date and email or any execution failure.
   * @throws IOException if API call fails.
   */
  @Override
  public final StatusDto validateSurveyRequest(final String userId, final SurveyRequestDto survey)
      throws IOException, ClassDirectException {
    validateDate(survey);
    final PortOfRegistryDto port = portService.getPort(survey.getPortId());

    final UserProfiles user = userProfileService.getUser(userId);
    if (null == user.getEmail()) {
      throw new InvalidEmailException(emailNotFound, user.getUserId());
    } else {
      if (!validateEmail(user.getEmail())) {
        throw new InvalidEmailException(invalidEmailErrorMessage, user.getEmail());
      }
    }

    String[] emails = null;
    String emailids = null;
    if (null != survey.getOtherEmails() && survey.getOtherEmails().length > 0) {
      emails = survey.getOtherEmails();
      emailids = String.join(SEMI_COLON_DELIMITER, emails);
      final boolean validEmail = Stream.of(emailids.split(SEMI_COLON_DELIMITER)).allMatch(this::validateEmail);
      if (!validEmail) {
        throw new InvalidEmailException(invalidEmailErrorMessage, emails);
      }
    }

    if (((null != survey.getPortAgent()) && (null != survey.getPortAgent().getEmail()))
        && (!validateEmail(survey.getPortAgent().getEmail()))) {
      throw new InvalidEmailException(invalidEmailErrorMessage, survey.getPortAgent().getEmail());
    }

    LOG.debug("Querying asset with asset id {}.", survey.getAssetId());
    final AssetHDto asset;
    try {
      asset = assetService.assetByCode(userId, survey.getAssetId());
    } catch (ClassDirectException exception) {
      throw new RecordNotFoundException("Unable to find asset with id LRV" + survey.getAssetId());
    }
    sendNewSurveyEmail(user, mailService, survey, port, emailids, asset);
    final StatusDto status = new StatusDto();
    status.setMessage("Email has been sent successfully");
    status.setStatus("200");
    return status;
  }

  /**
   * Verifies email is valid.
   *
   * @param email the unique email.
   * @return true for valid email otherwise false.
   */
  public final boolean validateEmail(final String email) {
    return Optional.ofNullable(email).map(mail -> mail.matches(CDConstants.EMAIL_PATTERN)).orElse(false);
  }


  /**
   * Returns constructed date time.if local time is null then LocalDateTime will construct with
   * midnight(00:00) or start of the day.
   *
   * @param localDate the localDate object.
   * @param localTime the localTime object.
   * @return Constructed dateTime.
   */
  private LocalDateTime constructDateTime(final LocalDate localDate, final LocalTime localTime) {
    LocalTime localTimeNew = null;
    if (localDate != null) {
      if (localTime == null) {
        localTimeNew = LocalTime.MIN;
      } else {
        localTimeNew = localTime;
      }
      return LocalDateTime.of(localDate, localTimeNew);
    }
    return null;
  }

  /**
   * Verifies
   * <ul>
   * <li>date of departure, survey date, arrival date is today or later.</li>
   * <li>arrival date and time is before departure date and time.</li>
   * <li>survey date is after arrival date and before departure date.</li>
   * </ul>
   *
   * @param survey the survey pay load.
   * @throws InvalidDateException if date validation fails.
   */
  public final void validateDate(final SurveyRequestDto survey) throws InvalidDateException {

    LocalDate todayDate = LocalDate.now();
    LocalDateTime todayDateTime = LocalDateTime.now();

    LocalDateTime sArrivalDateTime =
        constructDateTime(survey.getEstimatedDateOfArrival(), survey.getEstimatedTimeOfArrival());

    LocalTime sDepartureTime = Optional.ofNullable(survey.getEstimatedTimeOfDeparture()).orElse(LocalTime.MAX);
    LocalDateTime sDepartureDateTime = constructDateTime(survey.getEstimatedDateOfDeparture(), sDepartureTime);

    LocalDate sSurveyDate = survey.getSurveyStartDate();


    // If Arrival Date is not null, it should be after yesterday
    if (sArrivalDateTime != null && sArrivalDateTime.toLocalDate().isBefore(todayDate)) {
      throw new InvalidDateException(dateInPastErrorMessage, null, sArrivalDateTime, null, todayDateTime);
    }

    // If Departure Date is not null, it should be after yesterday
    if (sDepartureDateTime != null && sDepartureDateTime.toLocalDate().isBefore(todayDate)) {
      throw new InvalidDateException(dateInPastErrorMessage, null, null, sDepartureDateTime, todayDateTime);
    }

    // If Survey Date is not null, it should be after yesterday
    if (sSurveyDate != null && sSurveyDate.isBefore(todayDate)) {
      throw new InvalidDateException(dateInPastErrorMessage, sSurveyDate, null, null, todayDate);
    }

    if (sArrivalDateTime != null && sDepartureDateTime != null) {
      if (sDepartureDateTime.isBefore(sArrivalDateTime)) {
        throw new InvalidDateException(departureDateSameOrAfterArrivalDateErrorMessage,
            sDepartureDateTime.toLocalDate(), sArrivalDateTime.toLocalDate());
      }
    }

    if (sSurveyDate != null) {
      if (sArrivalDateTime != null && sSurveyDate.isBefore(sArrivalDateTime.toLocalDate())) {
        throw new InvalidDateException(surveyDateSameOrAfterArrivalDateErrorMessage, sSurveyDate,
            sArrivalDateTime.toLocalDate());
      }

      if (sDepartureDateTime != null && sSurveyDate.isAfter(sDepartureDateTime.toLocalDate())) {
        throw new InvalidDateException(surveyDateSameOrBeforeDepartureDateErrorMessage, sSurveyDate,
            sDepartureDateTime.toLocalDate());
      }
    }

  }

  /**
   * Returns service map sorted and grouped by product family.
   *
   * @param selServices selServices.
   * @return Map of sorted services grouped by product family.
   */
  private Map<String, ArrayList<ServiceDemo>> getSortedServiceMapByProductFamily(
      final List<ScheduledServiceHDto> selServices) {
    Map<String, ArrayList<ServiceDemo>> serviceMap = new HashMap<String, ArrayList<ServiceDemo>>();
    final ArrayList<ServiceDemo> classificationServicesList = new ArrayList<ServiceDemo>();
    final ArrayList<ServiceDemo> mmsStatutoryService = new ArrayList<ServiceDemo>();
    String serviceTitle = "";
    for (ScheduledServiceHDto service : selServices) {
      final ArrayList<String> sampleRange = new ArrayList<String>();
      String postponedDate = Optional.ofNullable(service.getPostponementDate()).map(ppDate -> {
        return DateUtils.getFormattedDatetoString(ppDate, DATE_PATTERN_WITHOUT_DOT);
      }).orElse("-");
      String assignedDate = Optional.ofNullable(service.getAssignedDate()).map(assignDate -> {
        return DateUtils.getFormattedDatetoString(assignDate, DATE_PATTERN_WITHOUT_DOT);
      }).orElse("-");
      String dueDate = Optional.ofNullable(service.getDueDate()).map(dDate -> {
        return DateUtils.getFormattedDatetoString(dDate, DATE_PATTERN_WITHOUT_DOT);
      }).orElse("-");


      if (service.getLowerRangeDate() != null && service.getUpperRangeDate() != null) {
        sampleRange.add(DateUtils.getFormattedDatetoString(service.getLowerRangeDate(), DATE_PATTERN_WITHOUT_DOT));
        sampleRange.add(DateUtils.getFormattedDatetoString(service.getUpperRangeDate(), DATE_PATTERN_WITHOUT_DOT));
      } else if (service.getLowerRangeDate() == null && service.getUpperRangeDate() != null) {
        sampleRange.add(dueDate);
        sampleRange.add(DateUtils.getFormattedDatetoString(service.getUpperRangeDate(), DATE_PATTERN_WITHOUT_DOT));
      } else if (service.getLowerRangeDate() != null && service.getUpperRangeDate() == null) {
        sampleRange.add(DateUtils.getFormattedDatetoString(service.getLowerRangeDate(), DATE_PATTERN_WITHOUT_DOT));
        sampleRange.add(dueDate);
      } else {
        sampleRange.add("");
        sampleRange.add("");
      }
      if (service.getServiceCatalogueH() != null) {
        Optional<Integer> occurenceNumber = Optional.ofNullable(service.getOccurrenceNumber());
        if (occurenceNumber.isPresent()) {
          serviceTitle = service.getServiceCatalogueH().getCode() + service.getOccurrenceNumber() + "-"
              + service.getServiceCatalogueH().getName();
        } else {
          serviceTitle = service.getServiceCatalogueH().getCode() + "-" + service.getServiceCatalogueH().getName();
        }
      }
      final ServiceDemo serv = new ServiceDemo(service, dueDate, service.getDueDate(), assignedDate, sampleRange,
              postponedDate, ProductTypeEnum.getById(service.getServiceCatalogueH().getProductGroupH().
              getProductType().getId()).name(), serviceTitle);

      if (serv.getProductTypeId().equals(ProductTypeEnum.CLASSIFICATION.getId())) {
        classificationServicesList.add(serv);
      } else {
        mmsStatutoryService.add(serv);
      }
    }
    serviceMap.put(CLASSIFICATION_SERVICE, ServiceUtils.sortClassificationService(classificationServicesList));
    serviceMap.put(MMS_STATUTORY_SERVICE, ServiceUtils.sortMMSStatutoryService(mmsStatutoryService));

    return serviceMap;
  }

  /**
   * Builds email model.
   *
   * @param port the port modal object.
   * @param survey the survey pay load.
   * @param asset the asset.
   * @param user the user.
   * @return The email model.
   * @throws IOException if API call fails.
   * @throws RecordNotFoundException if fail to find any object with given unique identifier.
   */
  private Map<String, Object> buildEmailModel(final PortOfRegistryDto port, final SurveyRequestDto survey,
      final AssetHDto asset, final UserProfiles user) throws IOException, RecordNotFoundException {
    final Map<String, Object> emailModel = new TreeMap<>();
    emailModel.put(PATH, rootAssetPath);
    emailModel.put("emailTitle", emailTitle);
    emailModel.put("emailAdditionalText", emailAdditionalText);
    emailModel.put("assetDetailsUrl", String.format(assetDetailUrl, asset.getCode()));
    if (asset.getCfoOfficeH() != null) {
      final List<String> addressList = new ArrayList<>();
      addressList.add(asset.getCfoOfficeH().getAddressLine1());
      addressList.add(asset.getCfoOfficeH().getAddressLine2());
      addressList.add(asset.getCfoOfficeH().getAddressLine3());
      final String address =
          addressList.stream().filter(addr -> addr != null).collect(Collectors.joining(COMMA_DELIMITER));
      emailModel.put("CFOOffice", asset.getCfoOfficeH().getName());
      emailModel.put("CFOPhone", asset.getCfoOfficeH().getPhoneNumber());
      emailModel.put("CFOAddress", address);
      emailModel.put("CFOEmailAddress", asset.getCfoOfficeH().getEmailAddress());
    }
    try {
      if (survey.getEstimatedDateOfArrival() != null) {
        emailModel.put("estimatedDateOfArrival",
            DateUtils.getFormattedLocaleDatetoString(survey.getEstimatedDateOfArrival(), DATE_PATTERN_WITHOUT_DOT));
      }
    } catch (final ParseException parseException) {
      LOG.error("Unable to parse estimated date of arrival : {}", survey.getEstimatedDateOfArrival(), parseException);
    }
    emailModel.put("estimatedTimeOfArrival", survey.getEstimatedTimeOfArrival());
    try {
      if (survey.getEstimatedDateOfDeparture() != null) {
        emailModel.put("estimatedDateOfDeparture",
            DateUtils.getFormattedLocaleDatetoString(survey.getEstimatedDateOfDeparture(), DATE_PATTERN_WITHOUT_DOT));
      }
    } catch (final ParseException parseException) {
      LOG.error("Unable to parse estimated date of departure : {}", survey.getEstimatedDateOfDeparture(),
          parseException);
    }
    emailModel.put("estimatedTimeOfDeparture", survey.getEstimatedTimeOfDeparture());
    try {
      if (survey.getSurveyStartDate() != null) {
        emailModel.put("surveyStartDate",
            DateUtils.getFormattedLocaleDatetoString(survey.getSurveyStartDate(), DATE_PATTERN_WITHOUT_DOT));
      }
    } catch (final ParseException parseException) {
      LOG.error("Unable to parse survey start date : {}", survey.getSurveyStartDate(), parseException);
    }
    if (survey.getPortAgent() != null) {
      emailModel.put("portAgentName", survey.getPortAgent().getName());
      emailModel.put("portAgentPhoneNumber", survey.getPortAgent().getPhoneNumber());
      emailModel.put("portAgentEmailAddress", survey.getPortAgent().getEmail());
    }
    if (port != null) {
      emailModel.put("portName", port.getName());
    }
    emailModel.put("berthOrAnchorage", survey.getBerthAnchorage());
    emailModel.put("shipContactDetails", survey.getShipContactDetails());
    emailModel.put("additionalComments", survey.getAdditionalComments());
    final List<ScheduledServiceHDto> services =
        serviceService.getServicesForAsset(AssetCodeUtil.getId(survey.getAssetId()));
    final List<ScheduledServiceHDto> selServices = services.parallelStream()
        .filter(service -> (survey.getServiceIds().contains(service.getId()))).collect(Collectors.toList());
    final ArrayList<ProductDemo> products = new ArrayList<>();

    Map<String, ArrayList<ServiceDemo>> servicesMap = getSortedServiceMapByProductFamily(selServices);

    Map<String, List<ServiceDemo>> classificationServices = servicesMap.get(CLASSIFICATION_SERVICE).stream()
        .collect(ServiceUtils.sortedGroupingBy(ServiceDemo::getProductName));

    classificationServices.entrySet().stream().forEach(entry -> {
      products.add(new ProductDemo(entry.getKey(), entry.getValue()));
    });

    Map<String, List<ServiceDemo>> mmsStatutoryServices = servicesMap.get(MMS_STATUTORY_SERVICE).stream()
        .collect(ServiceUtils.sortedGroupingBy(ServiceDemo::getProductTypeName));

    mmsStatutoryServices.entrySet().stream().forEach(entry -> {
      products.add(new ProductDemo(entry.getKey(), entry.getValue()));
    });

    emailModel.put("products", products);
    asset.setDueStatusH(assetService.calculateOverAllStatus(asset));
    final AssetHeaderDto assetHeaderDto = new AssetHeaderDto();
    assetHeaderDto.setName(asset.getName());
    assetHeaderDto.setImo(String.valueOf(asset.getImoNumber()));
    if (asset.getBuildDate() != null) {
      assetHeaderDto.setDateOfBuild(DateUtils.getFormattedDatetoString(asset.getBuildDate(), DATE_PATTERN_WITH_DOT));
    }
    if (asset.getAssetType() != null && !StringUtils.isEmpty(asset.getAssetType().getName())) {
      assetHeaderDto.setType(asset.getAssetType().getName());
    }
    if (asset.getAssetTypeHDto() != null && asset.getAssetTypeHDto().getCode() != null) {
      assetHeaderDto.setEmblemImage(AssetEmblemUtils.getEmblemImage(asset.getAssetTypeHDto().getCode()));
    }
    if (asset.getFlagStateDto() != null && !StringUtils.isEmpty(asset.getFlagStateDto().getName())) {
      assetHeaderDto.setFlag(asset.getFlagStateDto().getName());
    }
    assetHeaderDto.setGrossTonnage(String.valueOf(asset.getGrossTonnage()));
    if (asset.getClassStatusDto() != null && !StringUtils.isEmpty(asset.getClassStatusDto().getName())) {
      assetHeaderDto.setClassStatus(asset.getClassStatusDto().getName());
    }
    if (asset.getClassStatusDto() != null && asset.getClassStatusDto().getId() != null
        && !asset.getClassStatusDto().getId().equals(ClassStatus.NOT_LR_CLASSED.getValue())) {
      assetHeaderDto.setDueStatus(asset.getDueStatusH());
    }
    assetHeaderDto.setHasPostponedService(asset.getHasPostponedService());
    emailModel.put(ASSET_HEADER, assetHeaderDto);
    if (!StringUtils.isEmpty(user.getCompanyName())) {
      emailModel.put(CLIENT_NAME, user.getCompanyName());
    }
    return emailModel;
  }

  /**
   * Sends new survey email to the given assetCFO, requested user and other email's mentioned in
   * {@link SurveyRequestDto} pay load.
   *
   * @param user the user.
   * @param mail the mail.
   * @param survey the survey pay load.
   * @param port the port modal object.
   * @param emails the email's need to send as cc list.
   * @param asset the asset.
   * @throws IOException if API call fails.
   * @throws RecordNotFoundException if fail to find any object with given unique identifier.
   * @throws ClassDirectException if get cfo office details api fail.
   */
  private void sendNewSurveyEmail(final UserProfiles user, final MailService mail, final SurveyRequestDto survey,
      final PortOfRegistryDto port, final String emails, final AssetHDto asset)
      throws IOException, RecordNotFoundException, ClassDirectException {
    final OwnershipDto ownershipDto = new OwnershipDto();
    String assetIMO = null;
    StringBuilder mailIds = new StringBuilder();
    Optional.ofNullable(emails).ifPresent(email -> mailIds.append(email));
    final Map<String, Object> emailModel = buildEmailModel(port, survey, asset, user);
    if (asset.getImoNumber() != null) {
      assetIMO = asset.getImoNumber();
    }
    try {
      // if there is cfo and cfo office email is valid then proceed for survey or else throw
      // exception.

      if (asset.getCfoOfficeH() != null && asset.getCfoOfficeH().getDeleted().equals(Boolean.FALSE)
          && asset.getCfoOfficeH().getEmailAddress() != null
          && validateEmail(asset.getCfoOfficeH().getEmailAddress())) {
        if (mailIds.length() != 0) {
          mailIds.append(SEMI_COLON_DELIMITER);
        }
        LOG.debug("CFO office address {} for an asset {}", asset.getCfoOfficeH().getEmailAddress(), asset.getId());
        mailIds.append(asset.getCfoOfficeH().getEmailAddress());
      } else {
        LOG.error("There is no CFO email/is not valid email {} for an asset {}", asset.getCfoOfficeH(), asset.getId());
        throw new ClassDirectException("There is no CFO email/is not valid email for an asset :" + asset.getId());
      }

      // if the asset is in MAST, take all asset details from MAST cdh party
      if (asset.getCustomersH() != null && !asset.getCustomersH().isEmpty()) {
        asset.getCustomersH().forEach(cusH -> {
          CustomerLinkDto assetParty = cusH.getParty();
          if (assetParty != null && assetParty.getCustomer() != null) {
            String address =
                String.join(" ", StringUtils.defaultIfEmpty(assetParty.getCustomer().getAddressLine1(), ""),
                    StringUtils.defaultIfEmpty(assetParty.getCustomer().getAddressLine2(), ""),
                    StringUtils.defaultIfEmpty(assetParty.getCustomer().getAddressLine3(), ""));
            if (cusH.getPartyRoles() != null) {

              if (cusH.getPartyRoles().getId().longValue() == PartyRole.SHIP_MANAGER.getValue().longValue()) {

                if (!StringUtils.isEmpty(assetParty.getCustomer().getName())) {
                  emailModel.put("shipManager", assetParty.getCustomer().getName());
                }

                if (!StringUtils.isEmpty(assetParty.getCustomer().getPhoneNumber())) {
                  emailModel.put("shipManagerPhone", assetParty.getCustomer().getPhoneNumber());
                }
                if (!StringUtils.isEmpty(assetParty.getCustomer().getEmailAddress())) {
                  emailModel.put("shipManagerEmailAddress", assetParty.getCustomer().getEmailAddress());
                }

                emailModel.put("shipManagerAddress", address);
              }

              if (cusH.getPartyRoles().getId().longValue() == PartyRole.SHIP_OWNER.getValue().longValue()) {

                if (!StringUtils.isEmpty(assetParty.getCustomer().getName())) {
                  emailModel.put("registeredOwner", assetParty.getCustomer().getName());
                }

                if (!StringUtils.isEmpty(assetParty.getCustomer().getPhoneNumber())) {
                  emailModel.put("registeredOwnerPhone", assetParty.getCustomer().getPhoneNumber());
                }
                if (!StringUtils.isEmpty(assetParty.getCustomer().getEmailAddress())) {
                  emailModel.put("registeredOwnerEmailAddress", assetParty.getCustomer().getEmailAddress());
                }

                emailModel.put("registeredOwnerAddress", address);
              }
            }

          }
        });
      } else { // take the asset details from IHS.
        // get Registered Owner details using owner code.
        final Callable<Void> ownerResponse = () -> {
          if (asset.getIhsAssetDto() != null && asset.getIhsAssetDto().getIhsAsset() != null) {
            IhsAssetDto ihsAsset = asset.getIhsAssetDto().getIhsAsset();
            if (!StringUtils.isEmpty(ihsAsset.getOwner()) && !StringUtils.isEmpty(ihsAsset.getOwnerCode())) {
              Optional.ofNullable(assetService.getPersonalInformation(ihsAsset.getOwner(), ihsAsset.getOwnerCode()))
                  .ifPresent(registeredDto -> {
                    ownershipDto.setRegisteredOwner(registeredDto);
                  });
            }
          }
          return null;
        };
        // get company details using ship manager code.
        final Callable<Void> shipMangResponse = () -> {
          if (asset.getIhsAssetDto() != null && asset.getIhsAssetDto().getIhsAsset() != null) {
            IhsAssetDto ihsAsset = asset.getIhsAssetDto().getIhsAsset();
            if (!StringUtils.isEmpty(ihsAsset.getShipManager())
                && !StringUtils.isEmpty(ihsAsset.getShipManagerCode())) {
              Optional
                  .ofNullable(
                      assetService.getPersonalInformation(ihsAsset.getShipManager(), ihsAsset.getShipManagerCode()))
                  .ifPresent(shipManagerDto -> {
                    ownershipDto.setShipManager(shipManagerDto);
                  });
            }
          }
          return null;
        };

        final List<Callable<Void>> callables = Arrays.asList(ownerResponse, shipMangResponse);
        final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
        executor.invokeAll(callables);
        executor.shutdown();


        if (ownershipDto.getRegisteredOwner() != null) {
          if (!StringUtils.isEmpty(ownershipDto.getRegisteredOwner().getName())) {
            emailModel.put("registeredOwner", ownershipDto.getRegisteredOwner().getName());
          }
          if (!StringUtils.isEmpty(ownershipDto.getRegisteredOwner().getPhoneNumber())) {
            emailModel.put("registeredOwnerPhone", ownershipDto.getRegisteredOwner().getPhoneNumber());
          }
          if (!StringUtils.isEmpty(ownershipDto.getRegisteredOwner().getEmailAddress())) {
            emailModel.put("registeredOwnerEmailAddress", ownershipDto.getRegisteredOwner().getEmailAddress());
          }
          if (!StringUtils.isEmpty(ownershipDto.getRegisteredOwner().getAddress())) {
            emailModel.put("registeredOwnerAddress", ownershipDto.getRegisteredOwner().getAddress());
          }
        }
        if (ownershipDto.getShipManager() != null) {
          if (!StringUtils.isEmpty(ownershipDto.getShipManager().getName())) {
            emailModel.put("shipManager", ownershipDto.getShipManager().getName());
          }
          if (!StringUtils.isEmpty(ownershipDto.getShipManager().getPhoneNumber())) {
            emailModel.put("shipManagerPhone", ownershipDto.getShipManager().getPhoneNumber());
          }
          if (!StringUtils.isEmpty(ownershipDto.getShipManager().getEmailAddress())) {
            emailModel.put("shipManagerEmailAddress", ownershipDto.getShipManager().getEmailAddress());
          }
          if (!StringUtils.isEmpty(ownershipDto.getShipManager().getAddress())) {
            emailModel.put("shipManagerAddress", ownershipDto.getShipManager().getAddress());
          }
        }
      }

      mail.sendMailWithTemplate(user.getEmail(), mailIds.toString(),
          "Survey Request for " + asset.getName() + " " + assetIMO, "confirmation-survey-request.pug", emailModel);
      LOG.debug("Sending email with toemail {}, cc {}, subject {}, template {}, model {}.", user.getEmail(),
          mailIds.toString(), "Survey Request for " + assetIMO, "confirmation-survey-request.pug", emailModel);
    } catch (IOException | InterruptedException exception) {
      LOG.error("Error while Sending email with toemail {}, cc {}, subject {}, template {}, model {}.", user.getEmail(),
          mailIds.toString(), "Survey Request for " + assetIMO, "confirmation-survey-request.pug", emailModel,
          exception);
    }
  }

}
