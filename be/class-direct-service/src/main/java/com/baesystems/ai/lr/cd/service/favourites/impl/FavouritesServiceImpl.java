/**
 *
 */
package com.baesystems.ai.lr.cd.service.favourites.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.favourites.FavouritesService;

/**
 * Provides service to get favourite assets and delete favorite assets for user.
 *
 * @author NAvula.
 * @author syalavarthi 23-05-2017.
 */
@Service(value = "FavouritesService")
public class FavouritesServiceImpl implements FavouritesService {

  /**
   * The Logger {@value #LOGGER} for favourite Service Implementation.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FavouritesServiceImpl.class);

  /**
   * The {@link FavoritesRepository} from spring context.
   */
  @Autowired
  private FavouritesRepository favouritesRepository;

  @Override
  public final List<String> getFavouritesForUser(final String userId) throws RecordNotFoundException {
    return favouritesRepository.getUserFavourites(userId).stream().map(favourite -> favourite.getAssetId().toString())
        .collect(Collectors.toList());
  }

  @Override
  public final void deleteAllUserFavourites(final String userId) throws ClassDirectException {
    LOGGER.debug("Deleting all user favourites with user id : {}", userId);
    favouritesRepository.deleteAllUserFavourites(userId);
  }
}
