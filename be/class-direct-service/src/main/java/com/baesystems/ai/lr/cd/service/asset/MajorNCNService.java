package com.baesystems.ai.lr.cd.service.asset;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides MNCNS information like get list of mncns for an asset and single mncn by id based on
 * user role.
 *
 * @author syalavarthi.
 *
 */
public interface MajorNCNService {
  /**
   * Returns the paginated list of major non confirmitive notes.
   *
   * @param page the page number.
   * @param size the max amount of data to be display per page.
   * @param assetId the asset Id.
   * @return the paginated list of major non confirmitive note.
   * @throws ClassDirectException if API call fail.
   */
  PageResource<MajorNCNHDto> getMajorNCNs(final Integer page, final Integer size, final Long assetId)
      throws ClassDirectException;

  /**
   * Returns major non confirmitive note by providing asset Id and major non confirmitive note Id.
   *
   * @param assetId the asset Id.
   * @param mncnId the major non confirmitive note Id.
   * @return the major non confirmitive note.
   * @throws ClassDirectException if API call fail.
   */
  MajorNCNHDto getMajorNCNByMNCNId(final Long assetId, final long mncnId)
      throws ClassDirectException;
}
