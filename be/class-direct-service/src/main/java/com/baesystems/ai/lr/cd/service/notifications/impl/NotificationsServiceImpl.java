package com.baesystems.ai.lr.cd.service.notifications.impl;

import static com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum.ASSET_LISTING;
import static com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum.DUE_STATUS;
import static com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum.JOB_REPORTS;
import static com.baesystems.ai.lr.cd.be.utils.DateUtils.getFormattedDatetoString;
import static com.baesystems.ai.lr.cd.be.utils.DateUtils.getLocalDateOffset;
import static com.baesystems.ai.lr.cd.be.utils.DateUtils.getLocalDateToDate;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.codicilsAndServicesDue;
import static com.baesystems.ai.lr.enums.EmployeeRole.LEAD_SURVEYOR;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHeaderDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DueItemDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.JobItemDto;
import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobWithFlagsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.NotificationsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.NotificationTypes;
import com.baesystems.ai.lr.cd.be.domain.repositories.Notifications;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.AssetEmblemUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.mail.MailService;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.JobService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.favourites.FavouritesService;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.notifications.NotificationsService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

import retrofit2.Call;


/**
 * Provides notification service gets list of subscribed users and sends notifications which
 * includes asset listing, due status and jobs.
 *
 * @author RKaneysan.
 * @author syalavarthi 07-06-2017.
 */
@Service("NotificationsService")
public class NotificationsServiceImpl implements NotificationsService, InitializingBean {

  /**
   * The logger for NotificationsServiceImpl.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsServiceImpl.class);

  /**
   * The date pattern.
   */
  private static final String DATE_PATTERN_WITH_DOT = "dd.MM.YYYY";

  /**
   * The date pattern.
   */
  private static final String DATE_PATTERN_WITHOUT_DOT = "dd MMM YYYY";

  /**
   * The due status notification period to trigger notification.
   */
  @Value("${notification.due.status.period}")
  private String dueStatusNotificationPeriod;
  /**
   * The job report notification period to trigger notification.
   */
  @Value("${notification.job.report.period}")
  private String jobReportNotificationPeriod;

  /**
   * The record not found for given asset error message.
   */
  @Value("${record.not.found.for.asset.id}")
  private String assetNotFoundErrorMessage;

  /**
   * The due status notification error message.
   */
  @Value("${due.status.notification.error}")
  private String dueStatusNotificationErrorMessage;

  /**
   * The due status notification email subject.
   */
  @Value("${notification.duestatus.subject}")
  private String dueStatusEmailSubject;

  /**
   * The asset listing email heading title.
   */
  @Value("${notification.jobreport.title}")
  private String jobReportEmailTitle;

  /**
   * The job notification email subject.
   */
  @Value("${notification.jobreport.subject}")
  private String jobReportEmailSubject;

  /**
   * The due status notification email heading title.
   */
  @Value("${notification.duestatus.title}")
  private String dueNotificationEmailTitle;

  /**
   * The asset listing email heading title.
   */
  @Value("${notification.assetlisting.title}")
  private String assetListingEmailTitle;

  /**
   * The base path to asset listing download page.
   */
  @Value("${notification.assetlisting.link}")
  private String assetListingLink;

  /**
   * The base path to job report page in Service History page.
   */
  @Value("${notification.jobreport.link}")
  private String jobReportLink;

  /**
   * The asset listing email subject.
   */
  @Value("${notification.assetlisting.subject}")
  private String assetListingEmailSubject;

  /**
   * The root path to email assets (images, etc).
   */
  @Value("${notification.assets.path}")
  private String rootAssetPath;

  /**
   * The link to Actionable Item page.
   */
  @Value("${notification.duestatus.link.ai}")
  private String dueStatusAILink;

  /**
   * The link to CoC page.
   */
  @Value("${notification.duestatus.link.coc}")
  private String dueStatusCocLink;

  /**
   * The link to Statutory Finding page.
   */
  @Value("${notification.duestatus.link.sf}")
  private String dueStatusSFLink;

  /**
   * The link to Asset Details page.
   */
  @Value("${notification.assethub.link}")
  private String assetDetailsLink;

  /**
   * The employee not found error message.
   */
  @Value("${record.not.found.for.employee.id}")
  private String recordNotFoundForEmployeeId;

  /**
   * The notification template for Asset Status Update.
   */
  private static final String DUE_STATUS_NOTIFY_TEMPLATE = "asset-status-update.pug";

  /**
   * The notification template for asset listing.
   */
  private static final String ASSET_LISTING_EMAIL_TEMPLATE = "asset-listing-notification.pug";

  /**
   * The notification template for Job Report.
   */
  private static final String JOB_REPORT_EMAIL_TEMPLATE = "job-report.pug";

  /**
   * The notification field for Asset Header in report.
   */
  private static final String ASSET_HEADER = "assets";

  /**
   * The notification field for path.
   */
  private static final String PATH = "path";

  /**
   * The notification field for client name.
   */
  private static final String CLIENT_NAME = "clientName";

  /**
   * The notification field for DueItem Header.
   */
  private static final String EMAIL_TITLE = "emailTitle";

  /**
   * The notification field for Asset Header Link.
   */
  private static final String ASSET_LINK = "link";

  /**
   * The notification field for services.
   */
  private static final String DUE_STATUS_NOTIFICATION_AI = "actionableItem";

  /**
   * The notification field for services.
   */
  private static final String DUE_STATUS_NOTIFICATION_COC = "CoC";

  /**
   * The notification field for services.
   */
  private static final String DUE_STATUS_NOTIFICATION_SERVICE = "Service";
  /**
   * The notification field for statutory findings.
   */
  private static final String DUE_STATUS_NOTIFICATION_STATUTORY = "Statutory Findings";

  /**
   * The {@link AssetRetrofitService} from spring context.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * The {@link AssetService} from spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link StatutoryFindingService} from spring context.
   */
  @Autowired
  private StatutoryFindingService statutoryFindingService;

  /**
   * The {@link ActionableItemService} from spring context.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   * The {@link CoCService} from spring context.
   */
  @Autowired
  private CoCService cocService;

  /**
   * The {@link ServiceReferenceService} from spring context.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * The {@link NotificationsDao} from spring context.
   */
  @Autowired
  private NotificationsDao notificationDao;

  /**
   * The {@link MailService} from spring context.
   */
  @Autowired
  private MailService mailService;

  /**
   * The {@link JobService} from spring context.
   */
  @Autowired
  private JobService jobService;

  /**
   * The {@link JobReferenceService} from spring context.
   */
  @Autowired
  private JobReferenceService jobReferenceService;

  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Error Message for Error in Sending Job Notification.
   */
  @Value("${error.in.sending.job.notification}")
  private String errorInJobNotificationSending;

  /**
   * Error Message for Error in Sending Email.
   */
  @Value("${error.in.sending.email}")
  private String errorInSendingEmail;
  /**
   * serviceStatus List.
   */
  @Value("#{'${overallstatus.service_status_id}'.split(',')}")
  private List<Long> assetServiceStatusList;
  /**
   * actionableItemStatus List.
   */
  @Value("#{'${overallstatus.actionableitem_status_id}'.split(',')}")
  private List<Long> actionableItemStatusList;

  /**
   * cocStatus List.
   */
  @Value("#{'${overallstatus.coc_status_id}'.split(',')}")
  private List<Long> cocStatusList;

  /**
   * List of Statutory Finding Status IDs to take into account.
   */
  @Value("#{'${overallstatus.statutory_finding_status_id}'.split(',')}")
  private List<Long> statutoryFindingStatusIdList;

  /**
   * List of codicil type ids for due status notifica.
   */
  @Value("#{'${notification.duestatus.codicils.typeIds}'.split(',')}")
  private List<Long> codicilTypeIdsForDueStatus;
  /**
   * The codicil allowable confidentiality type ids.
   */
  @Value("#{'${codicil.confidentiality.type.id}'.split(',')}")
  private List<Long> confidentialityTypeIds;
  /**
   * The max due date threshold default value in years.
   */
  private static final Long DUE_DATE_MAX_IN_YEARS = 1L;
  /**
   * The max and min due date threshold default value in days.
   */
  private static final Long DUE_DATE_MAX_MIN_IN_DAYS = 7L;
  /**
   * The max postponement date threshold default value in years.
   */
  private static final Long POSTPONEMENT_DATE_MAX_IN_YEARS = 5L;
  /**
   * The {@link FavouritesService} from spring context.
   */
  @Autowired
  private FavouritesService favouritesService;

  @Override
  public final List<Notifications> getSubscribedUsers(final List<NotificationTypesEnum> typeIds) {
    return notificationDao.getSubscribedUsers(typeIds);
  }

  /**
   * Returns <code>true</code> if due status between previous week and current week for given
   * notification items are different. Otherwise, returns <code>false</code>.
   *
   * @param calculator service.
   * @param <Item> notification item.
   * @param item the notification item to be verified such as Actionable Items, Scheduled Services,
   *        Condition of Class or Statutory Findings.
   * @param referenceDate the date is used to check if there's change in due status between
   *        reference date and current date.
   * @return <code>true</code> if due status between previous week and current week for given
   *         notification items are different; <code>false</code> otherwise.
   */
  private <Item> Boolean isDueStatusChanged(final DueStatusCalculator<Item> calculator, final Item item,
      final LocalDate referenceDate) {
    final DueStatus currentDueStatus = calculator.calculateDueStatus(item);
    final DueStatus previousDueStatus = calculator.calculateDueStatus(item, referenceDate);
    final Boolean isDueStatusChanged = !currentDueStatus.equals(previousDueStatus);

    return isDueStatusChanged;
  }


  /**
   * Triggers due status notification and sends email to subscribed users.
   * <p>
   * Sends Due Status notification(s) for Scheduled Services, Condition of Classes, Actionable Items
   * and Statutory Findings to Subscribed Users for their favorite assets or fleet. This method is
   * only accessible by personnel with "LR Admin" role and "LR_SUPPORT".
   * </p>
   *
   * @return task uuid.
   */
  @Override
  public final String sendDueStatusNotification() {
    final List<Notifications> notifications = notificationDao.getSubscribedUsers(singletonList(DUE_STATUS));
    LOGGER.trace("Notify status change for {} ", DUE_STATUS.toString());

    List<Long> codicilStatusIdsList = new ArrayList<>();
    Stream.of(cocStatusList, actionableItemStatusList, statutoryFindingStatusIdList)
        .forEach(codicilStatusIdsList::addAll);

    final List<Callable<Void>> callables = new ArrayList<>();

    notifications.forEach(notification -> {
      callables.add(() -> {
        sendDueStatusNotificationForUser(notification, codicilStatusIdsList);
        return null;
      });
    });
    // invoke executor
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getName());
    try {
      executor.invokeAll(callables);
    } catch (final InterruptedException execption) {
      LOGGER.error("Error executing notification service - send due status notification.", execption);
    } finally {
      executor.shutdown();
    }

    return null;
  }

  @Override
  public final String sendDueStatusNotificationAsync() throws ClassDirectException {
    final String uuid = RandomStringUtils.randomAlphanumeric(CDConstants.MAX_UUID_LENGTH).toUpperCase(Locale.ENGLISH);
    ExecutorUtils.executeTask(this::sendDueStatusNotification, "DueStatusNotification", uuid);

    return uuid;
  }

  /**
   * Trigger asset listing notification and sends email to subscribed users.
   * <p>
   * Sends Asset Listing Notifications to subscribed users. This method is only accessible by
   * personnel with "LR Admin" role and "LR_SUPPORT".
   * </p>
   *
   * @return task uuid.
   */
  @Override
  public final String sendAssetListingNotification() {
    final List<Notifications> notificationsList = getSubscribedUsers(singletonList(ASSET_LISTING));

    final List<Callable<Void>> callables = new ArrayList<>();

    notificationsList.forEach(notification -> {
      callables.add(() -> {
        checkAssetListingNotificationForUser(notification.getUserId(), notification.isFavouritesOnly());
        return null;
      });
    });
    // invoke executor
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getName());
    try {
      executor.invokeAll(callables);
    } catch (final InterruptedException execption) {
      LOGGER.error("Error executing notification service - send asset listing notification.", execption);
    } finally {
      executor.shutdown();
    }
    return null;
  }

  @Override
  public final String sendAssetListingNotificationAsync() throws ClassDirectException {
    final String uuid = RandomStringUtils.randomAlphanumeric(CDConstants.MAX_UUID_LENGTH).toLowerCase(Locale.ENGLISH);
    ExecutorUtils.executeTask(this::sendAssetListingNotification, "AssetListingNotification", uuid);

    return uuid;
  }

  @Override
  public final String sendJobNotificationsAsync() throws ClassDirectException {
    final String uuid = RandomStringUtils.randomAlphanumeric(CDConstants.MAX_UUID_LENGTH).toUpperCase(Locale.ENGLISH);
    ExecutorUtils.executeTask(this::sendJobNotifications, "JobNotification", uuid);

    return uuid;
  }

  /**
   * Triggers job notifications and sends email to subscribed users with below conditions.
   * <p>
   * Sends Final Survey Report notification to subscribed users for their favorite assets or fleet.
   * This method is only accessible by personnel with "LR Admin" role and "LR_SUPPORT".
   * </p>
   *
   * <ul>
   * <li>As a admin user gets all jobs closed with in jobReportNotificationPeriod
   * {@link jobReportNotificationPeriod}.</li>
   * <li>Maps asset with list of closed jobs and sends list of assets to filter user assets.</li>
   * </ul>
   *
   * @return task uuid as null.
   */
  @Override
  public final String sendJobNotifications() {
    final List<Notifications> notifications = notificationDao.getSubscribedUsers(singletonList(JOB_REPORTS));
    LOGGER.trace("Notify status change for {} ", JOB_REPORTS.toString());

    UserProfiles user = getUserInfo();
    // Retrieve Reference Date Offset by Job Report Notification Period.
    final LocalDate referenceDate = getLocalDateOffset(jobReportNotificationPeriod);
    final Date toDate = new Date();
    final Date fromDate = getLocalDateToDate(referenceDate);

    List<JobWithFlagsHDto> jobList = null;
    try {
      jobList = jobService.getCompletedJobs(user.getUserId(), fromDate, toDate);

      if (jobList != null && !jobList.isEmpty()) {

        LOGGER.trace("completed jobs {} for user {} to send {} notification", jobList.size(), user.getUserId(),
            NotificationTypesEnum.JOB_REPORTS);
        List<Long> assetsWithCompletedJobs =
            jobList.stream().map(job -> job.getAsset().getId()).collect(Collectors.toList());

        Map<Long, List<JobWithFlagsHDto>> assetJobs =
            jobList.stream().collect(Collectors.groupingBy(job -> job.getAsset().getId().longValue()));

        if (assetsWithCompletedJobs != null && !assetsWithCompletedJobs.isEmpty()) {
          final List<Callable<Void>> callables = new ArrayList<>();

          notifications.forEach(notification -> {
            callables.add(() -> {
              sendJobNotificationsForUser(notification, assetsWithCompletedJobs, assetJobs);
              return null;
            });
          });
          // invoke executor
          final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getName());
          try {
            executor.invokeAll(callables);
          } catch (final InterruptedException execption) {
            LOGGER.error("Error executing notification service - send job notification.", execption);
          } finally {
            executor.shutdown();
          }
        }
      }
    } catch (final ClassDirectException exception) {
      LOGGER.error("Error executing notification service - get completed jobs.", exception);
    }
    return null;
  }

  /**
   * Returns Asset Header (Asset Header DTO) informations which contains asset relevant
   * informations.
   *
   * @param asset the asset.
   * @param lrAssetCode the asset code.
   * @return notification's asset header information which contains asset descriptions.
   */
  private AssetHeaderDto getAssetHeaderInformation(final AssetHDto asset, final String lrAssetCode) {
    asset.setDueStatusH(assetService.calculateOverAllStatus(asset));
    final AssetHeaderDto assetHeaderDto = new AssetHeaderDto();
    assetHeaderDto.setName(asset.getName());
    assetHeaderDto.setImo(String.valueOf(asset.getImoNumber()));
    assetHeaderDto.setDateOfBuild(getFormattedDatetoString(asset.getBuildDate(), DATE_PATTERN_WITH_DOT));
    assetHeaderDto.setDueStatus(asset.getDueStatusH());
    assetHeaderDto.setType(asset.getAssetType().getName());
    assetHeaderDto.setEmblemImage(AssetEmblemUtils.getEmblemImage(asset.getAssetTypeHDto().getCode()));
    assetHeaderDto.setFlag(asset.getFlagStateDto().getName());
    assetHeaderDto.setClassStatus(asset.getClassStatusDto().getName());
    assetHeaderDto.setLink(String.format(assetDetailsLink, lrAssetCode));
    assetHeaderDto.setHasPostponedService(asset.getHasPostponedService());
    return assetHeaderDto;
  }

  /**
   * Returns user profile with given user id.
   *
   * @param userId the id of the given user.
   * @return email address of the given user.
   * @throws ClassDirectException when user's email address or company information is not found.
   */
  private UserProfiles getUserProfiles(final String userId) throws ClassDirectException {
    final UserProfiles user = userProfileService.getUser(userId);
    if (StringUtils.isEmpty(user.getEmail())) {
      throw new ClassDirectException("No email found for user " + userId);
    }
    if (user.getCompany() == null) {
      final Company company = new Company();
      company.setName(user.getCompanyName());
      user.setCompany(company);
    }
    return user;
  }

  /**
   * Returns user's assets from user's accessible assets by checking below conditions.
   *
   * <ul>
   * <li>Checks subscribed user sends notification to subfleet or favourites adds condotion to
   * assets query.</li>
   * <li>For Jobs notification adds assets query filter with list of assets having closed jobs.</li>
   * <li>For Asset due status notifications fetches all codicil status ids, max due date and min
   * date for codicils and max postponement date and min postponement filters to services adds all
   * filters to mast query builder.</li>
   * </ul>
   *
   * @param userId user id.
   * @param favouritesOnly the flag to check notifications send for favourite assets or fleet.
   * @param assetsWithCompletedJobs the list of assets with completed jobs.
   * @param codicilStatusIdsList the list of codicil status ids to filter assets.
   * @return list of assets after applying filters.
   * @throws ClassDirectException if error in getting user assets.
   */
  private List<AssetHDto> getAssetsForUser(final String userId, final boolean favouritesOnly,
      final List<Long> assetsWithCompletedJobs, final List<Long> codicilStatusIdsList) throws ClassDirectException {

    LOGGER.info("Getting assets for users to send notifications");
    final List<AssetHDto> assets;

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    if (favouritesOnly) {
      assetQuery.setIsFavourite(Boolean.TRUE);
    }
    try {
      if (assetsWithCompletedJobs != null && !assetsWithCompletedJobs.isEmpty()) {
        assetQuery.setIdList(assetsWithCompletedJobs);
        assets = Optional.ofNullable(assetService.getAssetsByPagination(userId, assetQuery, null)).orElse(emptyList());

      } else {
        MastQueryBuilder queryBuilder = MastQueryBuilder.create();
        final Date maxDueDate = getMaxDueDateForDueNotification();
        final Date minDueDate = getMinDueDateForDueNotification();
        final Date maxPostponementDate = getMaxPostponementDateForDueNotification();
        final Date minPostponementDate = getMinPostponementDateForDueNotification();


        queryBuilder
            .mandatory(codicilsAndServicesDue(codicilStatusIdsList, codicilTypeIdsForDueStatus, assetServiceStatusList,
                minDueDate, maxDueDate, minPostponementDate, maxPostponementDate, confidentialityTypeIds));

        assets = Optional.ofNullable(assetService.getAssetsByPagination(userId, assetQuery, queryBuilder))
            .orElse(emptyList());
        LOGGER.info("Getting assets for users to send notifications assets{}", assets.size());
      }
    } catch (final ClassDirectException exception) {
      LOGGER.error("Error in Retrieving Asset for User with user id {}", userId, exception);
      throw new ClassDirectException(exception);
    }
    // filter out incomplete data.
    return assets
        .parallelStream().filter(asset -> Objects.nonNull(asset.getAssetType())
            && Objects.nonNull(asset.getFlagStateDto()) && Objects.nonNull(asset.getClassStatusDto()))
        .collect(Collectors.toList());
  }

  @Override
  public final List<NotificationTypes> getNotificationTypes() {
    return notificationDao.getNotificationTypes();
  }

  @Override
  public final StatusDto createNotification(@NotNull final String requesterId, @NotNull final Long typeId,
      @NotNull final Boolean favouritesOnly) throws RecordNotFoundException, ClassDirectException {
    StatusDto status = null;
    try {
      if (!StringUtils.isEmpty(requesterId) && typeId != 0) {
        if (notificationDao.verifyNotification(requesterId, typeId) != null) {
          self.deleteNotification(requesterId, typeId);
        }
        status = notificationDao.createNotification(requesterId, typeId, favouritesOnly);
      }
    } catch (final DataIntegrityViolationException exception) {
      LOGGER.error("Error while create notification", exception);
    }
    return status;
  }

  @Override
  public final StatusDto deleteNotification(@NotNull final String requesterId, @NotNull final Long typeId)
      throws ClassDirectException {
    StatusDto status = null;
    try {
      status = notificationDao.deleteNotification(requesterId, typeId);
    } catch (final IllegalArgumentException exception) {
      LOGGER.error("Error while delete notification", exception);
      throw new ClassDirectException(exception);
    }
    return status;
  }

  /**
   * Returns a {@link AssetHeaderDto} asset header includes actionable item, coc, sttautories and
   * services for a given asset.
   *
   * @param asset the asset model.
   * @param userProfile the user model.
   * @return the {@link AssetHeaderDto} asset header.
   */
  private AssetHeaderDto assetDueStatusResponse(final AssetHDto asset, final UserProfiles userProfile) {

    AssetHeaderDto response = null;
    //test
    LOGGER.debug("checking data for asset header", asset.getImoNumber(), userProfile.getName());
    final String assetCode = asset.getCode();
    final Long assetId = asset.getId();
    final LocalDate referenceDate = getLocalDateOffset(dueStatusNotificationPeriod);
    LOGGER.trace("Reference Date for the Due Status Notification is {}", referenceDate);
    final AssetHeaderDto assetHeaderDto = getAssetHeaderInformation(asset, assetCode);

    CodicilDefectQueryHDto codicilQuery = new CodicilDefectQueryHDto();
    codicilQuery.setDueDateMax(getMaxDueDateForDueNotification());
    codicilQuery.setDueDateMin(getMinDueDateForDueNotification());

    try {
      codicilQuery.setStatusList(actionableItemStatusList);
      Optional.ofNullable(actionableItemService.getActionableItemsByQuery(assetId, codicilQuery))
          .ifPresent(actionablePageResource -> {
            actionablePageResource.forEach(aiHdto -> {
              if (isDueStatusChanged(actionableItemService, aiHdto, referenceDate)) {
                // test
                LOGGER.debug("actionable items for asset header", asset.getImoNumber(), userProfile.getName());
                final DueItemDto itemDto = new DueItemDto();
                itemDto.setTitle(aiHdto.getTitle());
                Optional.ofNullable(aiHdto.getCategoryH()).ifPresent(category -> {
                  itemDto.setCategory(category.getName());
                });
                itemDto.setDueDate(getFormattedDatetoString(aiHdto.getDueDate(), DATE_PATTERN_WITHOUT_DOT));
                itemDto.setImposedDate(getFormattedDatetoString(aiHdto.getImposedDate(), DATE_PATTERN_WITHOUT_DOT));
                itemDto.setDueStatusIcon(aiHdto.getDueStatusH().getIcon());
                itemDto.setDueStatusLabel(aiHdto.getDueStatusH().getName());
                itemDto.setLink(String.format(dueStatusAILink, assetCode, aiHdto.getId()));
                assetHeaderDto.getListActionableItem().add(itemDto);
              }
            });
          });
    } catch (final Exception exception) {
      LOGGER.error(dueStatusNotificationErrorMessage, DUE_STATUS_NOTIFICATION_AI, asset.getId(),
          NotificationTypesEnum.DUE_STATUS, userProfile.getEmail(), exception);
    }

    try {
      codicilQuery.setStatusList(cocStatusList);
      Optional.ofNullable(cocService.getCocsByQuery(assetId, codicilQuery)).ifPresent(cocList -> {
        cocList.forEach(cocHdto -> {
          if (isDueStatusChanged(cocService, cocHdto, referenceDate)) {
            // test
            LOGGER.debug("coc", asset.getImoNumber(), userProfile.getName());
            final DueItemDto itemDto = new DueItemDto();
            itemDto.setTitle(cocHdto.getTitle());
            Optional.ofNullable(cocHdto.getCategoryH()).ifPresent(category -> {
              itemDto.setCategory(category.getName());
            });
            itemDto.setDueDate(getFormattedDatetoString(cocHdto.getDueDate(), DATE_PATTERN_WITHOUT_DOT));
            itemDto.setImposedDate(getFormattedDatetoString(cocHdto.getImposedDate(), DATE_PATTERN_WITHOUT_DOT));
            itemDto.setDueStatusIcon(cocHdto.getDueStatusH().getIcon());
            itemDto.setDueStatusLabel(cocHdto.getDueStatusH().getName());
            itemDto.setLink(String.format(dueStatusCocLink, assetCode, cocHdto.getId()));
            assetHeaderDto.getListConditionOfClass().add(itemDto);
          }
        });
      });
    } catch (final Exception exception) {
      LOGGER.error(dueStatusNotificationErrorMessage, DUE_STATUS_NOTIFICATION_COC, asset.getId(),
          NotificationTypesEnum.DUE_STATUS, userProfile.getEmail(), exception);
    }

    try {
      ServiceQueryDto serviceQuery = new ServiceQueryDto();
      serviceQuery.setStatusIdList(assetServiceStatusList);
      Optional.ofNullable(serviceReferenceService.getServices(assetId, serviceQuery)).ifPresent(serviceList -> {
        serviceList.forEach(serviceHdto -> {
          if (isDueStatusChanged(serviceReferenceService, serviceHdto, referenceDate)) {
            // test
            LOGGER.debug("services", asset.getImoNumber(), userProfile.getName());
            final DueItemDto itemDto = new DueItemDto();
            itemDto.setServiceName(serviceHdto.getServiceCatalogueH().getName());
            itemDto.setServiceCode(serviceHdto.getServiceCatalogueH().getCode());
            itemDto.setOccurrenceNumber(serviceHdto.getOccurrenceNumber());
            itemDto.setProduct(serviceHdto.getServiceCatalogueH().getProductCatalogue().getName());
            Optional.ofNullable(serviceHdto.getDueDate()).ifPresent(dueDate -> {
              itemDto.setDueDate(getFormattedDatetoString(dueDate, DATE_PATTERN_WITHOUT_DOT));
            });
            itemDto.setDueStatusIcon(serviceHdto.getDueStatusH().getIcon());
            itemDto.setDueStatusLabel(serviceHdto.getDueStatusH().getName());
            itemDto.setLink(String.format(assetDetailsLink, assetCode));

            Optional.ofNullable(serviceHdto.getLowerRangeDate()).ifPresent(lowerRangeDate -> {
              itemDto.setLowerRangeDate(getFormattedDatetoString(lowerRangeDate, DATE_PATTERN_WITHOUT_DOT));
            });
            Optional.ofNullable(serviceHdto.getUpperRangeDate()).ifPresent(upperRangeDate -> {
              itemDto.setUpperRangeDate(getFormattedDatetoString(upperRangeDate, DATE_PATTERN_WITHOUT_DOT));
            });
            Optional.ofNullable(serviceHdto.getPostponementDate()).ifPresent(postponementDate -> {
              itemDto.setPostponementDate(getFormattedDatetoString(postponementDate, DATE_PATTERN_WITHOUT_DOT));
            });
            assetHeaderDto.getListService().add(itemDto);
          }
        });
      });
    } catch (final Exception exception) {
      LOGGER.error(dueStatusNotificationErrorMessage, DUE_STATUS_NOTIFICATION_SERVICE, asset.getId(),
          NotificationTypesEnum.DUE_STATUS, userProfile.getEmail(), exception);
    }

    try {
      codicilQuery.setStatusList(cocStatusList);
      Optional.ofNullable(statutoryFindingService.getStatutoriesForDueNotification(assetId, codicilQuery))
          .ifPresent(statutoryFindings -> {
            statutoryFindings.forEach(sfDto -> {
              final Long statutoryId = sfDto.getId();
              if (isDueStatusChanged(statutoryFindingService, sfDto, referenceDate)) {
                // test
                LOGGER.debug("sf for asset", asset.getImoNumber(), userProfile.getName());
                final DueItemDto itemDto = new DueItemDto();
                itemDto.setTitle(sfDto.getTitle());
                itemDto.setImposedDate(getFormattedDatetoString(sfDto.getImposedDate(), DATE_PATTERN_WITHOUT_DOT));
                itemDto.setDueDate(getFormattedDatetoString(sfDto.getDueDate(), DATE_PATTERN_WITHOUT_DOT));
                itemDto.setDueStatusIcon(sfDto.getDueStatusH().getIcon());
                itemDto.setDueStatusLabel(sfDto.getDueStatusH().getName());
                Optional.ofNullable(sfDto.getDefect()).ifPresent(defect -> {
                  itemDto.setLink(String.format(dueStatusSFLink, assetCode, defect.getId(), statutoryId));
                });
                assetHeaderDto.getListStatutoryFinding().add(itemDto);
              }
            });
          });
    } catch (final Exception cdException) {
      LOGGER.error(dueStatusNotificationErrorMessage, DUE_STATUS_NOTIFICATION_STATUTORY, asset.getId(),
          NotificationTypesEnum.DUE_STATUS, userProfile.getEmail(), cdException);
    }

    if (!assetHeaderDto.getListActionableItem().isEmpty() || !assetHeaderDto.getListConditionOfClass().isEmpty()
        || !assetHeaderDto.getListStatutoryFinding().isEmpty() || !assetHeaderDto.getListService().isEmpty()) {
      response = assetHeaderDto;
    }
    // test
    LOGGER.debug("response is present", asset.getImoNumber(), userProfile.getName());
    return response;
  }

  /**
   * Gets closed jobs for given user by asset.
   *
   * @param asset the asset to get closed jons for given asset.
   * @param userProfile the user record.
   * @param jobsList the list of closed jobs for asset.
   *
   * @return the asset header info.
   */
  private AssetHeaderDto assetCompletedJobsResponse(final AssetHDto asset, final UserProfiles userProfile,
      final List<JobWithFlagsHDto> jobsList) {

    AssetHeaderDto response = null;
    final Long assetId = asset.getId();
    final String assetCode = asset.getCode();
    final AssetHeaderDto assetHeaderDto = getAssetHeaderInformation(asset, assetCode);

    try {
      List<JobWithFlagsHDto> completedJobsList =
          jobService.filterJobsBasedOnUserRole(userProfile.getUserId(), assetId, jobsList);
      LOGGER.trace("completed jobs {} for user {} to send {} notification", completedJobsList.size(),
          userProfile.getUserId(), NotificationTypesEnum.JOB_REPORTS);

      completedJobsList.forEach(job -> {
        Resources.inject(job);
        final JobItemDto item = new JobItemDto();

        Optional.ofNullable(job).map(JobWithFlagsHDto::getId).ifPresent(jobId -> {
          item.setJobNumber(job.getJobNumber());
          final EmployeeLinkDto employeeLinkDto = job.getEmployees().stream()
              .filter(emp -> emp.getEmployeeRole().getId().equals(LEAD_SURVEYOR.getValue())).findFirst().orElse(null);

          Optional.ofNullable(employeeLinkDto).map(EmployeeLinkDto::getLrEmployee).map(LinkResource::getId)
              .ifPresent(employeeId -> {
                try {
                  final Call<LrEmployeeDto> empLead = assetServiceDelegate.getEmployeeById(employeeId);
                  Optional.ofNullable(empLead.execute().body()).ifPresent(empLeadSurveyor -> {
                    item.setName(empLeadSurveyor.getName());
                  });
                } catch (final IOException ioException) {
                  LOGGER.error("Error executing JobService - getLeadSurveyorForJob.", ioException);
                }
              });

          Optional.ofNullable(job.getLastVisitDate()).ifPresent(lastVisitDate -> {
            item.setLastVisit(getFormattedDatetoString(lastVisitDate, DATE_PATTERN_WITHOUT_DOT));
          });
          Optional.ofNullable(job.getLocationDto())
              .ifPresent(locationHDto -> item.setLocation(job.getLocationDto().getName()));
          Optional.ofNullable(job.getJobStatus()).map(LinkResource::getId).ifPresent(jobStatusId -> {
            final JobStatusHDto jobStatus = jobReferenceService.getJobStatus(jobStatusId);
            item.setStatusLabel(jobStatus.getName());
          });
          item.setLink(String.format(jobReportLink, assetCode, jobId));

          assetHeaderDto.getJobs().add(item);
        });
      });

    } catch (final ClassDirectException exception) {
      LOGGER.error(errorInJobNotificationSending, asset.getId(), NotificationTypesEnum.JOB_REPORTS,
          userProfile.getEmail(), exception);
    }
    // Filter assets without jobs.
    if (!assetHeaderDto.getJobs().isEmpty()) {
      response = assetHeaderDto;
    }
    return response;

  }

  /**
   * Sends due status notification for given user with below conditions.
   *
   * <ul>
   * <li>Gets list of codicil status ids with types actionable item, coc, statutory and adds to
   * assets filter, along with this calculates codicil max due date range and min due date range
   * based on due staus calculations adds to assets filter.</li>
   * <li>Gets list of services status ids and range of postponement dates based on due status
   * calculation adds to assets filter.</li>
   * </ul>
   *
   * @param notification the user notification record.
   * @param codicilStatusIdsList tghe codicil sttaus ids to filter assets for due status
   *        notification.
   */
  private void sendDueStatusNotificationForUser(final Notifications notification,
      final List<Long> codicilStatusIdsList) {
    UserProfiles user = null;
    try {

      user = getUserProfiles(notification.getUserId());
      final UserProfiles userProfile = user;
      final List<AssetHDto> assets =
          getAssetsForUser(notification.getUserId(), notification.isFavouritesOnly(), null, codicilStatusIdsList);
      LOGGER.trace("assets {} for user {} to send {} notification", assets.size(), notification.getUserId(),
          NotificationTypesEnum.DUE_STATUS);

      final List<AssetHeaderDto> assetHeaderList = new ArrayList<>();
      assets.forEach(asset -> {
        AssetHeaderDto assetHeader = assetDueStatusResponse(asset, userProfile);
        if (assetHeader != null) {
          // test
          LOGGER.info("asset header for", asset.getImoNumber(), userProfile.getName(),
              NotificationTypesEnum.DUE_STATUS);
          assetHeaderList.add(assetHeader);
        }
      });

      if (!assetHeaderList.isEmpty()) {

        try {
          LOGGER.debug("Sending email to user {} ", user.getEmail());
          //test
          LOGGER.debug("Sending email to user {} for due status notification", user.getEmail());
          final Map<String, Object> model = new HashMap<>();
          model.put(ASSET_HEADER, assetHeaderList);

          Optional.ofNullable(user).map(UserProfiles::getCompany).ifPresent(company -> {
            model.put(CLIENT_NAME, company.getName());
          });
          model.put(EMAIL_TITLE, dueNotificationEmailTitle);
          model.put(PATH, rootAssetPath);
          mailService.sendMailWithTemplate(user.getEmail(), null, dueStatusEmailSubject,
              DUE_STATUS_NOTIFY_TEMPLATE, model);

        } catch (IOException exception) {
          LOGGER.error("Unable to send {} notification to user {}. Error Message: {}",
              NotificationTypesEnum.DUE_STATUS.getName(), user.getEmail(), exception);
        }
      } else {
        LOGGER.debug("No update for user {} sending {} notification", notification.getUserId(),
            NotificationTypesEnum.DUE_STATUS);
      }
    } catch (final ClassDirectException exception) {
      LOGGER.debug("Error in retrieving asset(s) - Due Status email notification for user {}",
          Optional.ofNullable(user).map(UserProfiles::getEmail).orElse(notification.getUserId()), exception);
    }
  }

  /**
   * Sends job notification for given user by checking below conditions.
   *
   * <ul>
   * <li>Gets user assets by checking user can access list of assets with closed jobs.</li>
   * <li>Verifies job accessibility for user if it is MMS job</li>.
   * </ul>
   *
   * @param notification the user notification record.
   * @param assetsWithCompletedJobs the list of assets with jobs compled on yesterday.
   * @param assetJobs the map with asset and list of closed jobs associated with that asset.
   */
  private void sendJobNotificationsForUser(final Notifications notification, final List<Long> assetsWithCompletedJobs,
      final Map<Long, List<JobWithFlagsHDto>> assetJobs) {
    List<AssetHDto> assets = null;
    UserProfiles user = null;

    try {
      final List<AssetHeaderDto> assetHeaderList = new ArrayList<>();
      assets =
          getAssetsForUser(notification.getUserId(), notification.isFavouritesOnly(), assetsWithCompletedJobs, null);
      LOGGER.trace("assets {} for user {} to send {} notification", assets.size(), notification.getUserId(),
          NotificationTypesEnum.JOB_REPORTS);
      user = getUserProfiles(notification.getUserId());
      final UserProfiles userProfile = user;
      assets.forEach(asset -> {
        List<JobWithFlagsHDto> jobsList = assetJobs.get(asset.getId());
        final AssetHeaderDto assetHeader = assetCompletedJobsResponse(asset, userProfile, jobsList);
        if (assetHeader != null) {
          assetHeaderList.add(assetHeader);
        }
      });

      // Filter out empty email.
      if (!assetHeaderList.isEmpty()) {

        try {
          LOGGER.trace("Sending email to user {} ", user.getEmail());

          final Map<String, Object> model = new HashMap<>();
          Optional.ofNullable(user).map(UserProfiles::getCompany).ifPresent(company -> {
            model.put(CLIENT_NAME, company.getName());
          });
          model.put(EMAIL_TITLE, jobReportEmailTitle);
          model.put(PATH, rootAssetPath);
          model.put(ASSET_HEADER, assetHeaderList);
          mailService.sendMailWithTemplate(user.getEmail(), null, jobReportEmailSubject, JOB_REPORT_EMAIL_TEMPLATE,
              model);

        } catch (final IOException exception) {
          LOGGER.error("Unable to send {} notification to user {}. Error Message: {}",
              NotificationTypesEnum.JOB_REPORTS.getName(), user.getEmail(), exception);
        }
      } else {
        LOGGER.debug("No update for user {} sending {} notification", notification.getUserId(),
            NotificationTypesEnum.JOB_REPORTS);
      }
    } catch (final ClassDirectException exception) {
      LOGGER.debug("Error in getting assets in send Job Notification for user", notification.getUserId(), exception);
    }
  }

  /**
   * Sends asset listing notification for given user.
   *
   * @param userId the primary key of user.
   */
  private void sendAssetListingNotificationForUser(final String userId) {
    UserProfiles user = null;
    try {
      user = getUserProfiles(userId);
      LOGGER.trace("Sending email to user {} ", user.getEmail());

      final Map<String, Object> model = new HashMap<>();
      Optional.ofNullable(user).map(UserProfiles::getCompany).ifPresent(company -> {
        model.put(CLIENT_NAME, company.getName());
      });
      model.put(EMAIL_TITLE, assetListingEmailTitle);
      model.put(ASSET_LINK, assetListingLink);
      model.put(PATH, rootAssetPath);
      mailService.sendMailWithTemplate(user.getEmail(), null, assetListingEmailSubject, ASSET_LISTING_EMAIL_TEMPLATE,
          model);
    } catch (IOException | ClassDirectException exception) {
      LOGGER.error("Unable to send {} notification to user {}. Error Message: {}",
          NotificationTypesEnum.ASSET_LISTING.getName(),
          Optional.ofNullable(user).map(UserProfiles::getEmail).orElse(userId), exception);
    }
  }

  /**
   * Checks uesr notifications subscribed to favourite assets/subfleet.
   *
   * <ul>
   * <li>if user subscribed to favourites only assets then find list of favourite assets if it is
   * there then sends asset listing notification to user.
   * <li>
   * <li>if user subscribed to subfleet then sends asset listing notification.</li>
   * </ul>
   *
   * @param userId the primary key of user.
   * @param isFavouritesOnly the users favourite flag to send notifcatios to favourites assets or
   *        subfleet.
   */
  private void checkAssetListingNotificationForUser(final String userId, final boolean isFavouritesOnly) {
    if (isFavouritesOnly) {
      try {
        List<String> assetIds = favouritesService.getFavouritesForUser(userId);
        if (assetIds != null && !assetIds.isEmpty()) {
          sendAssetListingNotificationForUser(userId);
        }
      } catch (RecordNotFoundException exception) {
        LOGGER.error("Unable to get user {} favourites. Error Message: {}", userId, exception);
      }
    } else {
      sendAssetListingNotificationForUser(userId);
    }

  }

  /**
   * Gets max due date reference to filter assets by codicil max due date for calculating due status
   * change.
   *
   * @return max due date reference.
   */
  private Date getMaxDueDateForDueNotification() {
    final LocalDate today = LocalDate.now();
    final LocalDate maxDueDateRef = today.plusYears(DUE_DATE_MAX_IN_YEARS).plusDays(DUE_DATE_MAX_MIN_IN_DAYS);
    return getLocalDateToDate(maxDueDateRef);
  }

  /**
   * Gets min due date reference to filter assets by codicil min due date for calculating due status
   * change.
   *
   *
   * @return min due date reference.
   */
  private Date getMinDueDateForDueNotification() {
    final LocalDate today = LocalDate.now();
    final LocalDate minDueDateRef = today.minusDays(DUE_DATE_MAX_MIN_IN_DAYS);
    return getLocalDateToDate(minDueDateRef);
  }

  /**
   * Gets max postponement date reference to filter assets by services max postponement date for
   * calculating due status change.
   *
   * <p>
   * min limit to postpone service is today.
   * </p>
   *
   * @return max postponement date reference.
   */
  private Date getMaxPostponementDateForDueNotification() {
    final LocalDate today = LocalDate.now();
    return getLocalDateToDate(today);
  }

  /**
   * Gets min postponement date reference to filter assets by services min postponement date for
   * calculating due status change.
   * <p>
   * max limit to postpone service is 5 years and additional one week to check notification change.
   * </p>
   *
   * @return min postponement date reference.
   */
  private Date getMinPostponementDateForDueNotification() {
    final LocalDate today = LocalDate.now();
    final LocalDate minPostponementRef =
        today.minusYears(POSTPONEMENT_DATE_MAX_IN_YEARS).minusDays(DUE_DATE_MAX_MIN_IN_DAYS);
    return getLocalDateToDate(minPostponementRef);
  }

  /**
   * Gets admin user info.
   *
   * @return user info.
   */
  public final UserProfiles getUserInfo() {
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    final UserProfiles user = token.getDetails();

    return user;
  }

  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   *
   */
  private NotificationsService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(NotificationsService.class);
  }

}
