package com.baesystems.ai.lr.cd.service.asset.impl;

import com.baesystems.ai.lr.dto.query.AbstractQueryDto;

/**
 * Predicate for MAST query.
 *
 * @author Faizal Sidek
 */
public interface Predicate {

  /**
   * Evaluate statement.
   *
   * @return query builder.
   */
  AbstractQueryDto evaluate();
}
