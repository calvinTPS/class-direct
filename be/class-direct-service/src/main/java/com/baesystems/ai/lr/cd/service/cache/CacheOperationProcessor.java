package com.baesystems.ai.lr.cd.service.cache;

import java.io.Serializable;
import java.util.Map;

import org.springframework.cache.Cache;

/**
 * Cache Operation Processor.
 *
 * @author RKaneysan.
 */
public interface CacheOperationProcessor extends Serializable {

  /**
   * Cache preProcessor.
   *
   * @param cache spring Cache.
   * @param cacheKey spring Cache Key.
   * @param context container to hold values to be updated.
   */
  void preProcessor(final Cache cache, final Object cacheKey, final Map<String, Object> context);

  /**
   * Cache postProcessor.
   *
   * @param cache spring Cache.
   * @param cacheKey spring Cache Key.
   * @param context container to hold values to be updated.
   */
  void postProcessor(final Cache cache, final Object cacheKey, final Map<String, Object> context);

}

