package com.baesystems.ai.lr.cd.service.export.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.cs10.CS10Service;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10FileVersion;
import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import com.baesystems.ai.lr.cd.be.domain.dto.export.CertificateAttachDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.service.certificate.CertificateService;
import com.baesystems.ai.lr.cd.service.export.CertificateDownloadService;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;

/**
 * Provides service methods to download certificate from CS10.
 *
 * @author sbollu
 *
 */
@Service(value = "CertificateDownloadService")
public class CertificateDownloadServiceImpl implements CertificateDownloadService {

  /**
   * The Logger to display logs {@link CertificateDownloadServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CertificateDownloadServiceImpl.class);

  /**
   * The {@link CertificateService} from Spring context.
   */
  @Autowired
  private CertificateService certService;

  /**
   * CS10 service.
   */
  @Autowired
  private CS10Service cs10Service;

  /**
   * The default status code for attachment not found.
   */
  private static final Integer STATUS = 404;


  @Override
  public final StringResponse downloadCertificateInfo(final CertificateAttachDto query)
      throws RecordNotFoundException, ClassDirectException {
    final long elapsedTime = System.currentTimeMillis();
    final long certId = query.getCertId();
    // get logged in userId.
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    final String userId = token.getDetails().getUserId();
    try {
      LOGGER.info("CertificateExport started for Certificate : {} ", certId);
      final SupplementaryInformationPageResourceDto certAttach =
          certService.getCertificateAttachment(query.getJobId(), query.getCertId());

      final SupplementaryInformationDto cAttach = Optional.ofNullable(certAttach)
          .map(SupplementaryInformationPageResourceDto::getContent).map(List<SupplementaryInformationDto>::stream)
          .flatMap(Stream::findFirst).orElseThrow(
              () -> new RecordNotFoundException("Attachments not available to download Certificate [" + certId + "]"));

      Integer attachNodeId =
          Optional.of(cAttach).map(SupplementaryInformationDto::getAttachmentUrl).map(Integer::valueOf).orElseThrow(
              () -> new RecordNotFoundException("Certificate not found in document server. [" + certId + "]"));
      Integer attachVersion =
          Optional.of(cAttach).map(SupplementaryInformationDto::getDocumentVersion).map(Long::intValue).orElse(null);
      String response;
      String fileName = "";

      try {
        final CS10FileVersion fileVersion = cs10Service.getFileVersion(attachNodeId, attachVersion);

        if (fileVersion != null && fileVersion.getFilename() != null && fileVersion.getFilename()
            .endsWith(".pdf")) {
          response = FileTokenGenerator.generateCS10Token(attachNodeId, attachVersion, userId);
        } else {
          response =
              FileTokenGenerator.generateCS10PdfZipToken(attachNodeId, attachVersion, userId);
        }
        fileName = Optional.ofNullable(fileVersion).map(CS10FileVersion::getFilename).orElse("");
      } catch (CS10Exception | ClassDirectSecurityException exception) {
        LOGGER.error("Unable to get file info.", exception);
        response = FileTokenGenerator.generateCS10PdfZipToken(attachNodeId, attachVersion, userId);
      }
      return new StringResponse(response, fileName);
    } catch (final RecordNotFoundException exception) {
      LOGGER.error("Error occurred in downloading certificate [" + certId + "] : " + exception);
      throw new RecordNotFoundException("Certificate not found in document server [" + certId + "] : ");
    } catch (final NumberFormatException | ClassDirectException exception) {
      LOGGER.error("Error occurred in downloading certificate [" + certId + "] : " + exception);
      throw new ClassDirectException("Error occured in downloading certificate [" + certId + "] : ");
    } finally {
      // benchmark time
      LOGGER.info("CertificateDownLoad ended for Certificate : {}. | Elapsed time (ms): {}", certId,
          System.currentTimeMillis() - elapsedTime);
    }
  }

}
