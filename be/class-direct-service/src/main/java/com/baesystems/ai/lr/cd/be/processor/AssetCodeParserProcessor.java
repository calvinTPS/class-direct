package com.baesystems.ai.lr.cd.be.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;

/**
 * @author Faizal Sidek
 */
public class AssetCodeParserProcessor implements Processor {
  @Override
  public final void process(final Exchange exchange) throws Exception {
    final Object assetId = exchange.getIn().getHeader("assetId");

    if (assetId != null && assetId instanceof String) {
      final Long id = AssetCodeUtil.getId((String) assetId);
      exchange.getIn().setHeader("assetId", id);
    }
  }
}
