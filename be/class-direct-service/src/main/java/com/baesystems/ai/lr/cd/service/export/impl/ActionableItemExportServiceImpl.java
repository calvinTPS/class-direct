package com.baesystems.ai.lr.cd.service.export.impl;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.export.ActionableItemExportService;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;
import de.neuland.jade4j.exceptions.JadeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Provides the actionable item export service for generating, and downloading actionable item
 * report.
 *
 * @author yng
 */
@Service(value = "ActionableItemExportService")
@Loggable(Loggable.DEBUG)
public class ActionableItemExportServiceImpl extends BasePdfExport<ExportPdfDto>
    implements ActionableItemExportService {
  /**
   * The logger instantiated for {@link ActionableItemExportServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ActionableItemExportServiceImpl.class);

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link ActionableItemService} from Spring context.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   * The report name prefix for actionable item.
   */
  private static final String PDF_PREFIX_AI = "AI";

  /**
   * The pug template file name for actionable item.
   */
  private static final String TEMPLATE_AI = "ai_listing_export_template.jade";

  /**
   * The directory name in Amazon S3 where the generated report to be uploaded. This variable in
   * only use at {@link BasePdfExport}. <br>
   * For more information see {@link ActionableItemExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "codicil_export";

  /**
   * This field is use as variable in pug template that store list of actionable item object.
   */
  private static final String AIS = "ais";

  /**
   * This field is use as variable in pug template that store asset object.
   */
  private static final String ASSET = "asset";

  /**
   * This field is use {@link BasePdfExport} as a part of file name generation.
   */
  private static final String FILE_SECTION = "fileSection";

  /**
   * This field is use to check if the pdf is single page or multi page.
   */
  private static final String ONLY_ONE_PAGE = "onlyOnePage";

  /**
   * The file generation working directory.
   */
  @Value("${codicil.export.temp.dir}")
  private String tempDir;

  @Override
  protected final String generateFile(final Map<String, Object> model, final ExportPdfDto query,
      final UserProfiles user, final String workingDirAI, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Retrieving AI object...");

      // create file section name
      model.put(FILE_SECTION, query.getCode());

      // get asset information
      final AssetHDto asset = assetService.assetByCode(user.getUserId(), query.getCode());
      if (asset == null) {
        throw new ClassDirectException("Asset not found.");
      } else {
        model.put(ASSET, asset);
      }

      // get AI by asset id.
      List<ActionableItemHDto> items = actionableItemService.getActionableItems(asset.getId(), null);

      // filter Items by ids
      if (items != null && !items.isEmpty()) {
        if (query.getItemsToExport() != null && !query.getItemsToExport().isEmpty()) {
          items = items.stream().filter(item -> query.getItemsToExport().contains(item.getId()))
              .collect(Collectors.toList());
        }

        // sort by due date and ID
        items.sort(ServiceUtils.codicilDefaultComparator());
      }

      model.put(AIS, items);

      String filename = generatePdf(PDF_PREFIX_AI, query.getCode(), current, TEMPLATE_AI, workingDirAI, model);

      int pageCountFORAI = ServiceUtils.getPageCount(workingDirAI, filename);
      //if the pdf has only one page then we will recall the page to put ONLY_ONE_PAGE flag in the model
      if (pageCountFORAI == 1) {
        model.put(ONLY_ONE_PAGE, "true");
        filename = generatePdf(PDF_PREFIX_AI, query.getCode(), current, TEMPLATE_AI, workingDirAI, model);
      }

      return filename;
    } catch (JadeException | IOException | ParserConfigurationException | SAXException | DocumentException exception) {
      throw new ClassDirectException(exception);
    }
  }

  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return this.tempDir;
  }
}
