package com.baesystems.ai.lr.cd.service.certificate;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateTemplateHDto;
import com.baesystems.ai.lr.dto.references.CertificateActionTakenDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

/**
 * Created by fwijaya on 24/1/2017.
 */
public interface CertificateReferenceService {
  /**
   * Get certificate templates.
   *
   * @return A list of {@link CertificateTemplateHDto}.
   */
  List<CertificateTemplateHDto> getCertificateTemplates();

  /**
   * Get certificate template.
   *
   * @param id id of the template.
   * @return {@link CertificateTemplateHDto}.
   */
  CertificateTemplateHDto getCertificateTemplate(Long id);

  /**
   * Get certificate types.
   *
   * @return A list of {@link CertificateTypeDto}.
   */
  List<CertificateTypeDto> getCertificateTypes();

  /**
   * Get certificate type.
   *
   * @param id id of the certificate type.
   * @return {@link CertificateTypeDto}.
   */
  CertificateTypeDto getCertificateType(Long id);

  /**
   * Get certificate statuses.
   *
   * @return A list of {@link CertificateStatusDto}.
   */
  List<CertificateStatusDto> getCertificateStatuses();

  /**
   * Get certificate status.
   *
   * @param id The certificate status id.
   * @return A {@link CertificateStatusDto}.
   */
  CertificateStatusDto getCertificateStatus(Long id);

  /**
   * Get surveyor.
   *
   * @param id Employee id.
   * @return {@link LrEmployeeDto}.
   */
  LrEmployeeDto getSurveyor(final Long id);

  /**
   * Get action taken dto.
   *
   * @return List of action taken dto.
   */
  List<CertificateActionTakenDto> getActionTakenDtos();

  /**
   * Get action taken dto.
   *
   * @param id id.
   * @return action taken dto.
   */
  CertificateActionTakenDto getActionTakenDto(final Long id);
}
