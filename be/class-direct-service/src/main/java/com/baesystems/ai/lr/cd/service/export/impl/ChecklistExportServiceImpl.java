package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ChecklistExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.HierarchyDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.export.ChecklistExportService;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Provides the checklist export service for generating, and downloading checklist report.
 *
 * @author yng
 */
@Service(value = "ChecklistExportService")
@Loggable(Loggable.DEBUG)
public class ChecklistExportServiceImpl extends BasePdfExport<ChecklistExportDto> implements ChecklistExportService {
  /**
   * The logger instantiated for {@link ChecklistExportServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ChecklistExportServiceImpl.class);

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link TaskService} from Spring context.
   */
  @Autowired
  private TaskService taskService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * The report name prefix for checklist.
   */
  private static final String PDF_PREFIX = "Checklist";

  /**
   * The pug template file name for checklist.
   */
  private static final String TEMPLATE = "checklist_export_template.jade";

  /**
   * The directory name in Amazon S3 where the generated report to be uploaded. This variable in
   * only use at {@link BasePdfExport}. <br>
   * For more information see {@link ChecklistExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "checklist_export";

  /**
   * This field is use as variable in pug template that store tree structure of checklist hierarchy.
   */
  private static final String CHECKLISTS = "checkLists";

  /**
   * This field is use as variable in pug template that store asset object.
   */
  private static final String ASSET = "asset";

  /**
   * This field is use as variable in pug template that store service object.
   */
  private static final String SERVICE = "service";

  /**
   * This field is use {@link BasePdfExport} as a part of file name generation.
   */
  private static final String FILE_SECTION = "fileSection";

  /**
   * The file generation working directory.
   */
  @Value("${checklist.export.temp.dir}")
  private String tempDir;

  @Override
  protected final String generateFile(final Map<String, Object> model, final ChecklistExportDto query,
      final UserProfiles user, final String workingDir, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Retrieving checklist object...");

      // create file section name
      model.put(FILE_SECTION, query.getAssetCode());

      // get asset information
      final AssetHDto asset = assetService.assetByCode(user.getUserId(), query.getAssetCode());
      if (asset == null) {
        throw new ClassDirectException("Asset is null in get model on checklist export.");
      } else {
        model.put(ASSET, asset);
      }

      // get service information
      List<ScheduledServiceHDto> services = serviceReferenceService.getServicesForAsset(asset.getId());
      services = services.stream().filter(service -> service.getId().equals(query.getServiceId()))
          .collect(Collectors.toList());
      if (!services.isEmpty()) {
        model.put(SERVICE, services.get(0));
      }

      // get checklist
      final HierarchyDto checklist = taskService.getCheckListHierarchyByService(query.getServiceId());
      model.put(CHECKLISTS, checklist.getChecklistGroups());

      final String filename = generatePdf(PDF_PREFIX, query.getAssetCode(), current, TEMPLATE, workingDir, model);

      return filename;
    } catch (JadeException | IOException | ParserConfigurationException | SAXException | DocumentException exception) {
      throw new ClassDirectException(exception);
    }
  }

  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return this.tempDir;
  }
}
