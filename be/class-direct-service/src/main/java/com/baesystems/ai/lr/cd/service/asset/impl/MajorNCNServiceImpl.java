package com.baesystems.ai.lr.cd.service.asset.impl;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.CLIENT;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.FLAG;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.MajorNCNService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.jcabi.aspects.Loggable;

/**
 * Provides MNCNS information like get list of mncns for an asset and single mncn by id based on
 * user role.
 *
 * @author syalavarthi.
 *
 */
@Service(value = "MajorNCNService")
@Loggable(Loggable.DEBUG)
public class MajorNCNServiceImpl implements MajorNCNService {
  /**
   * The logger instantiated for {@link AssetNoteServiceImpl}.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MajorNCNServiceImpl.class);

  /**
   * The {@link AssetRetrofitService} from Spring context.
   *
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;
  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  @RestrictedAsset
  @Override
  public final PageResource<MajorNCNHDto> getMajorNCNs(final Integer page, final Integer size, final Long assetId)
      throws ClassDirectException {
    PageResource<MajorNCNHDto> pageResource = null;

    pageResource = PaginationUtil.paginate(getMajorNCNs(assetId), page, size);
    return pageResource;
  }

  /**
   * Returns list of major non confirmitive notes.
   *
   * @param assetId the asset Id.
   * @throws ClassDirectException if API call fail.
   * @return the list of major non confirmitive note.
   */
  public final List<MajorNCNHDto> getMajorNCNs(final long assetId) throws ClassDirectException {
    final List<MajorNCNHDto> contentH = new ArrayList<MajorNCNHDto>();

    try {
      // get logged in userprofile.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      if (token != null) {
        final UserProfiles user = userProfileService.getUser(token.getPrincipal());

        final AssetHDto asset = assetService.assetByCode(
            Optional.ofNullable(user).map(UserProfiles::getUserId).orElse(null), AssetCodeUtil.getMastCode(assetId));

        final List<MajorNCNHDto> response = assetServiceDelegate.getMajorNCNdto(assetId).execute().body();
        // check user is internal user.
        final boolean isInternalUser = SecurityUtils.isInternalUser();
        final List<String> roles = new ArrayList<>();
        Optional.ofNullable(user.getRoles()).ifPresent(userRoles -> {
          roles.addAll(user.getRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList()));
        });
        Predicate<MajorNCNHDto> roleBasedFilter = this.isVisibleByRole(user, asset, roles);

        if (response != null && !response.isEmpty()) {
          response.stream().forEach(mncnNote -> {
            // For LR_ADMIN, LR_SUPPORT and LR_INTERNAL role, other roles have filtering
            if (!isInternalUser) {
              // for any of external roles roleBasedFilter is null then it will not visible for
              // user. we can skip filtering.
              if (roleBasedFilter == null || !filterMncnsByUserRole(mncnNote, roleBasedFilter)) {
                mncnNote = null;
              }
            }
            Optional.ofNullable(mncnNote).ifPresent(mncNote -> {
              // hydrate the data
              Resources.inject(mncNote);
              contentH.add(mncNote);
            });
          });
        }
      }
    } catch (final IOException ioException) {
      LOGGER.error("Error executing MajorNCNService-getMajorNCNdto.", ioException);
      throw new ClassDirectException(ioException);
    }

    return contentH;
  }

  /**
   * Filter MNCNs based on user roles.
   *
   * <p>
   * this method call for external user roles as CLIENT, FLAG, SHIPBUILDER, EOR.
   * </p>
   *
   * <ul>
   * <li>MNCNs visibility for CLIENT
   * <p>
   * user client code match with mncn doc_imo_number.
   * </p>
   * </li>
   * <li>MNCNs Jobs visibility for FLAG
   * <p>
   * user flag code match with job asset flagstate flagcode and mncn asset versionID should match
   * with published version of assset.
   * </p>
   * </li>
   * </ul>
   *
   * <p>
   * except these two roles MNCNs will not visible, predicate set to null.
   * </p>
   *
   * @param user the user model object.
   * @param asset the asset model object.
   * @param roles the user roles.
   * @return the condition.
   */
  private Predicate<MajorNCNHDto> isVisibleByRole(final UserProfiles user, final AssetHDto asset,
      final List<String> roles) {

    if (user.getClientCode() != null && roles.contains(CLIENT.toString())) {
      final String userClientCode = String.valueOf(user.getClientCode());
      return p -> userClientCode.equals(p.getDocCompanyImo());

    } else if (user.getFlagCode() != null && roles.contains(FLAG.toString())) {

      final String assetFlagCode =
          Optional.ofNullable(asset).map(AssetHDto::getFlagStateDto).map(FlagStateHDto::getFlagCode).orElse(null);
      return p -> user.getFlagCode().equals(assetFlagCode);
    }
    return null;
  }

  /**
   * Returns true if all predicate filter is true otherwise false.
   *
   * @param mncn the mncn model object.
   * @param filter the predicates on mncn.
   * @return true if all predicates pass otherwise false.
   */
  private boolean filterMncnsByUserRole(final MajorNCNHDto mncn, final Predicate<MajorNCNHDto> filter) {

    Optional<MajorNCNHDto> opt = Optional.ofNullable(mncn);
    boolean passFilter = false;
    passFilter = opt.filter(filter).isPresent();
    if (!passFilter) {
      return false;
    }
    return passFilter;

  }

  @RestrictedAsset
  @Override
  public final MajorNCNHDto getMajorNCNByMNCNId(final Long assetId, final long mncnId) throws ClassDirectException {
    MajorNCNHDto mncnResponse = null;

    try {
      // get logged in userprofile.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      if (token != null) {
        final UserProfiles user = userProfileService.getUser(token.getPrincipal());

        final AssetHDto asset = assetService.assetByCode(
            Optional.ofNullable(user).map(UserProfiles::getUserId).orElse(null), AssetCodeUtil.getMastCode(assetId));

        mncnResponse = assetServiceDelegate.getMajorNCNById(assetId, mncnId).execute().body();

        final List<String> roles = new ArrayList<>();
        Optional.ofNullable(user.getRoles()).ifPresent(userRoles -> {
          roles.addAll(user.getRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList()));
        });
        // check user is internal user.
        final boolean isInternalUser = SecurityUtils.isInternalUser();
        Predicate<MajorNCNHDto> roleBasedFilter = this.isVisibleByRole(user, asset, roles);

        Optional.ofNullable(mncnResponse).ifPresent(mncNote -> {
          if (!isInternalUser) {
            // for any of external roles roleBasedFilter is null then it will not visible for
            // user. we can skip filtering.
            if (roleBasedFilter == null || !filterMncnsByUserRole(mncNote, roleBasedFilter)) {
              mncNote = null;
            }
          }
          Optional.ofNullable(mncNote).ifPresent(Resources::inject);
        });
      }
    } catch (final IOException ioException) {
      LOGGER.error("Error executing MajorNCNService - getMajorNCN.", ioException);
      throw new ClassDirectException(ioException);
    }

    return mncnResponse;
  }

}
