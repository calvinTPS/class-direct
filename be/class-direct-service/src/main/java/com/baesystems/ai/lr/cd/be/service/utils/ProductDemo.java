package com.baesystems.ai.lr.cd.be.service.utils;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides product model used in survey email.
 *
 * @author shajahan.
 *
 */
public class ProductDemo {
  /**
  * name.
  */
  @Getter
  @Setter
  private String name;
  /**
  * services.
  */
  @Getter
  @Setter
  private List<ServiceDemo> services;

  /**
   * Constructs product demo with product name(product group name for classification service
   * and product family name for other services) and service list.
   *
   * @param nameP the product name.
   * @param servicesP the service list object.
   */
  public ProductDemo(final String nameP, final List<ServiceDemo> servicesP) {
    this.name = nameP;
    this.services = servicesP;
  }

}
