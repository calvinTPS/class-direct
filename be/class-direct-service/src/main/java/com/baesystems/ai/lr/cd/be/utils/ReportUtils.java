package com.baesystems.ai.lr.cd.be.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetItemHierarchyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetModelHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportContentHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.SurveyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedWorkItemAttributeValueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemQAModel;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.export.impl.ReportExportServiceImpl;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.ResolutionStatus;
import com.baesystems.ai.lr.enums.WorkItemType;

/**
 * Provides utilities for FAR/FSR and ESP exports.
 *
 * @author syalavarthi.
 *
 */
public final class ReportUtils {
  /**
   * Private constructor AssetOverAllStatusUtils.
   */
  private ReportUtils() {

  }

  /**
   * The logger for ReportUtils class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ReportUtils.class);
  /**
   * The field to lookup asset item from asset model.
   */
  private static final String ASSET_ITEM_LOOKUP = "assetItemLookUp";
  /**
   * The long value to repesent asst item parent id used in preparation of asset item task model.
   */
  private static final Integer PARENT_ASSET_ITEM_ID = 0;

  /**
   * The category called "postponed" used in Appendix I. <br>
   * Appendix I is displaying task by service group, along with a group called "postponed". The
   * purpose of this field is to hold the group name "postponed". <br>
   * For more information see {@link ReportUtils#generateTOCForAppendix(String, Map)}.
   */
  private static final String TASK_RESOLUTION_STATUS_POSTPONED = "postponed";

  /**
   * The regex created to match Appendix I table headers for task with non-postponed status.
   */
  private static final String TOC_PREFIX_1 = "Tasks\\s+credited\\s+for";

  /**
   * The regex created to match Appendix I table headers for task with postponed status.
   */
  private static final String TOC_PREFIX_2 = "Postponed\\s+tasks\\s+\\[optional\\]";
  /**
   * This field is use as variable in pug template that store list of tasks which already group by
   * service code with narratives <br>
   * Refer to {@link ReportUtils#getNarratives(Map, Map, TaskReferenceService, Object, boolean)} for
   * value assignment for this field.
   */
  private static final String SERVICE_NARRATIVES = "serviceNarratives";
  /**
   * The line break used to get service narratives and task narratives.
   */
  private static final String LINE_BREAK = "\r\n";
  /**
   * The value is used in pug to check weather it is task list or check list in narratives.
   */
  private static final String OTHER_CHECK_LIST_ITEMS = "Other checklist items";
  /**
   * The value is used in pug to display when there is no asset item hierarchy for tasks.
   */
  private static final String OTHER_TASK_LIST_ITEMS = "Other tasklist items";
  /**
   * The value is used to separete parent item and child item.
   */
  private static final String UNDERSCORE = "-";
  /**
   * The empty string.
   */
  private static final String EMPTY_STRING = "";
  /**
   * The dot string.
   */
  private static final String DOT = ".";

  /**
   * The decimal format for asset item display oder.
   */
  private static final String THREE_DECIMALS = "%03d";

  /**
   * Groups the list of task by product name, and survey code. <br>
   * This grouping list is then loop inside Appendix I pug template for data display.
   *
   * @param model the model from report.
   * @param serviceCatalogueLookup the id lookup for service catalogue.
   * @param taskReferenceService the task service reference.
   * @param espIndicatorServiceIds the esp indicator service ids.
   * @param isServiceNarrative the booelan value to get task narrative.
   * @return the list of task grouped by product name and survey code.
   */
  public static Map<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto,
  List<WorkItemHDto>>>> getGroupedTask(
      final Map<String, Object> model, final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup,
      final TaskReferenceService taskReferenceService, final HashSet<Long> espIndicatorServiceIds,
      final boolean isServiceNarrative) {

    final ReportContentHDto content = (ReportContentHDto) model.get(ReportExportServiceImpl.CONTENT);
    final Mapper objmapper = new DozerBeanMapper();
    // create surveyId-productCatalogueDto lookup, surveyId-serviceCatalogueHDto lookup
    final Map<Long, ProductCatalogueDto> surveyProductLookup = new HashMap<>();
    final Map<Long, ServiceCatalogueHDto> surveyCodeLookup = new HashMap<>();
    final Map<Long, SurveyHDto> surveyLookup = new HashMap<Long, SurveyHDto>();
    // group task with product (serviceCatalogue.productCatalogue)
    // LRCD-3535 display survey occurrence number with service code in Appendix I
    final Map<ProductCatalogueDto, Map<SurveyHDto, List<WorkItemHDto>>> groupedTask =
        new HashMap<ProductCatalogueDto, Map<SurveyHDto, List<WorkItemHDto>>>();
    final Predicate<SurveyDto> espIndicatorFilter =
        Optional.ofNullable(getEspIndicatorServicesFilter(espIndicatorServiceIds)).orElse((ai) -> {
          return true;
        });

    Optional.ofNullable(content.getSurveys()).ifPresent(services -> {
      services.stream().filter(espIndicatorFilter).forEach(survey -> {
        // get survey id
        final Long surveyId = survey.getId();
        if (survey.getServiceCatalogue() != null) {
          surveyProductLookup.put(surveyId,
              serviceCatalogueLookup.get(survey.getServiceCatalogue().getId()).getProductCatalogue());
        }

        surveyCodeLookup.put(survey.getId(), serviceCatalogueLookup.get(survey.getServiceCatalogue().getId()));
        final SurveyHDto surveyHDto = objmapper.map(survey, SurveyHDto.class);
        surveyHDto.setServiceCatalogueH(serviceCatalogueLookup.get(survey.getServiceCatalogue().getId()));
        surveyLookup.put(survey.getId(), surveyHDto);

      });

      if (content.getTasks() != null) {
        final Predicate<WorkItemDto> tasksFilter =
            Optional.ofNullable(getWorkIremTasksFilter(isServiceNarrative)).orElse((ai) -> {
              return true;
            });
        // Add only the credited task by checking the resolution status
        content.getTasks().stream().filter(tasksFilter).forEach(task -> {
          final WorkItemHDto taskH = objmapper.map(task, WorkItemHDto.class);
          // get survey id
          final Long surveyId = task.getSurvey().getId();

          // identify product name, if survey status is postponed group it as "postponed" group.
          final ProductCatalogueDto productCatalogue;
          if (!isServiceNarrative) {
            if (task.getResolutionStatus().getId().equals(ResolutionStatus.POSTPONED.getValue())) {

              productCatalogue = new ProductCatalogueDto();
              productCatalogue.setName(TASK_RESOLUTION_STATUS_POSTPONED);
              productCatalogue.setDisplayOrder(Integer.MAX_VALUE);
            } else {
              productCatalogue = surveyProductLookup.get(surveyId);
            }
          } else {
            productCatalogue = surveyProductLookup.get(surveyId);
          }

          // get survey
          final SurveyHDto surveyHDto = surveyLookup.get(surveyId);

          // Create Immutable pair of survey object and List of tasks
          List<WorkItemHDto> tasks = new ArrayList<>();
          if (surveyHDto != null) {
            if (groupedTask.get(productCatalogue) == null) {
              groupedTask.put(productCatalogue, new HashMap<SurveyHDto, List<WorkItemHDto>>());

              groupedTask.get(productCatalogue).put(surveyHDto, tasks);
              groupedTask.get(productCatalogue).get(surveyHDto).add(taskH);
            } else {
              if (groupedTask.get(productCatalogue).get(surveyHDto) == null) {
                groupedTask.get(productCatalogue).put(surveyHDto, tasks);
              }
              groupedTask.get(productCatalogue).get(surveyHDto).add(taskH);
            }
          } else {
            LOGGER.error("service {} not available for the task {} ", surveyId, task.getId());
          }
        });
      }

    });

    // get Appendix 1 including conditional tasks and answers with asset model hierarchy
    Map<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>> assetItemHierarchyTasks =
        new HashMap<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>>();
    for (Entry<ProductCatalogueDto, Map<SurveyHDto, List<WorkItemHDto>>> productEntry : groupedTask.entrySet()) {

      Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>> serviceCatalogueMap =
          new HashMap<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>();
      for (Entry<SurveyHDto, List<WorkItemHDto>> serviceEntry : productEntry.getValue().entrySet()) {

        // get question and answers for tasks
        if (!isServiceNarrative) {
          extractAppendix1Tasks(serviceEntry.getValue(), getConditionalTaskFilter(), getParentAttributeFilter(),
              taskReferenceService);
        }

        // prepares asset item hierarchy model for tasks
        Map<AssetItemHierarchyHDto, List<WorkItemHDto>> tasksHierarchy =
            new HashMap<AssetItemHierarchyHDto, List<WorkItemHDto>>();
        // to check checklist or task list
        Optional<WorkItemHDto> workItem = serviceEntry.getValue().stream().findFirst();

        // check narratives with new line
        if (serviceEntry.getKey().getNarrative() != null && isServiceNarrative) {
          String[] serviceNarratives = serviceEntry.getKey().getNarrative().split(LINE_BREAK);
          serviceEntry.getKey().setServiceNarratives(serviceNarratives);
        }
        if (workItem.isPresent() && isServiceNarrative
            && workItem.get().getWorkItemType().getId().equals(WorkItemType.CHECKLIST.value())) {
          // to identify cheklist in pug file
          List<WorkItemHDto> tasks = serviceEntry.getValue().stream().filter(checkList -> checkList.getNotes() != null)
              .collect(Collectors.toList());
          tasks.forEach(checkList -> {
            String[] taskNarratives = checkList.getNotes().split(LINE_BREAK);
            checkList.setTaskNarratives(taskNarratives);
          });
          if (!tasks.isEmpty()) {
            AssetItemHierarchyHDto item = new AssetItemHierarchyHDto();
            item.setName(OTHER_CHECK_LIST_ITEMS);
            tasksHierarchy.put(item, tasks);
          }
        } else {
          tasksHierarchy = prepareAssetItemModelForTasks(model, serviceEntry.getValue(), isServiceNarrative);
        }
        if (serviceCatalogueMap.get(serviceEntry.getKey()) == null) {
          serviceCatalogueMap.put(serviceEntry.getKey(), tasksHierarchy);
        }
        serviceCatalogueMap.put(serviceEntry.getKey(), tasksHierarchy);
      }
      // sort service catalogue by display order.
      Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>> sortedServiceCatalogues =
          serviceCatalogueMap.entrySet().stream()
              .sorted((e1, e2) -> e1.getKey().getServiceCatalogueH().getDisplayOrder()
                  .compareTo(e2.getKey().getServiceCatalogueH().getDisplayOrder()))
              .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

      assetItemHierarchyTasks.put(productEntry.getKey(), sortedServiceCatalogues);
    }
    // sort product catalogue by display order.
    Map<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>> sortedProductCatalogues =
        assetItemHierarchyTasks.entrySet().stream()
            .sorted((e1, e2) -> e1.getKey().getDisplayOrder().compareTo(e2.getKey().getDisplayOrder()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    return sortedProductCatalogues;

  }

  /**
   * Returns tasks filter based on service narrative value.
   *
   * @param isServiceNarrative the service narrative to check
   * @return predicate value.
   */
  private static Predicate<WorkItemDto> getWorkIremTasksFilter(final boolean isServiceNarrative) {
    if (!isServiceNarrative) {
      return task -> task.getWorkItemType().getId().equals(WorkItemType.TASK.value())
          && task.getResolutionStatus() != null;
    }
    return null;
  }

  /**
   * Gets asset items model to build asset items heirachy in appendix1 tasks.
   *
   * @param versionedAsset the asset link versioned resource.
   * @param model the model object to store child parent asset item map and asset item map used in
   *        task model preparation.
   * @param assetRetrofitService the asset retrofit service.
   * @throws ClassDirectException if mast api call fails.
   */
  public static void getAssetItemsModel(final LinkVersionedResource versionedAsset, final Map<String, Object> model,
      final AssetRetrofitService assetRetrofitService) throws ClassDirectException {
    AssetModelHDto assetModel = null;
    try {
      assetModel =
          assetRetrofitService.getAssetItemsModel(versionedAsset.getId(), versionedAsset.getVersion()).execute().body();
      if (assetModel != null && assetModel.getItems() != null) {
        Map<Long, ItemHDto> assetItemMap = new HashMap<>();
        Long level = 1L;
        prepareAssetItemsMap(assetModel.getItems(), assetItemMap, level);
        model.put(ASSET_ITEM_LOOKUP, assetItemMap);
      }
    } catch (final IOException exception) {
      LOGGER.error("failed to get asset items model : getAssetItemsModel asset {} and asset version id {} ",
          versionedAsset.getId(), versionedAsset.getVersion(), exception.getMessage());
      throw new ClassDirectException(exception.getMessage());
    }

    return;
  }

  /**
   * Prepares asset items map and parent child map with asset items model.
   *
   * @param list the list of asset items to be mapped.
   * @param assetItemMap the asset items map.
   * @param level the leve to indicate hierarchy.
   */
  public static void prepareAssetItemsMap(final List<ItemDto> list, final Map<Long, ItemHDto> assetItemMap,
      final Long level) {
    if (list != null) {
      list.stream().forEach(item -> {
        Long hierarvhyLevel = level;
        ItemHDto itemModel = new ItemHDto();
        itemModel.setId(item.getId());
        itemModel.setName(item.getName());
        itemModel.setDispalyOrder(String.format(THREE_DECIMALS, hierarvhyLevel));
        final LinkVersionedResource parentLinkVersionedResource = Optional.ofNullable(item.getParentItem())
            .orElse(new LinkVersionedResource(PARENT_ASSET_ITEM_ID.longValue()));
        itemModel.setParentAssetItem(parentLinkVersionedResource.getId());
        assetItemMap.put(item.getId(), itemModel);
        prepareAssetItemsMap(item.getItems(), assetItemMap, ++hierarvhyLevel);
      });
    }
    return;
  }

  /**
   * Returns list of tasks with Question and answers, filter tasks by.
   * <li>should have attributes</li>.
   * <li>should have parent task</li>.
   * <li>active task</li>.
   *
   * @param tasks The list of tasks to add question and answers.
   * @param conditionalTaskFilter The filter to check whether task has
   *        resolutionStatus(ConfirmatoryCheck (C),Complete(X)).
   * @param parentAttributeFilter the filter to check whether work item attribute has parent
   *        attribute.
   * @param taskReferenceService the task reference service.
   * @return list of tasks with Question and answers.
   */
  private static List<WorkItemHDto> extractAppendix1Tasks(final List<WorkItemHDto> tasks,
      final Predicate<WorkItemHDto> conditionalTaskFilter, final Predicate<WorkItemAttributeDto> parentAttributeFilter,
      final TaskReferenceService taskReferenceService) {
    List<WorkItemHDto> tasksWithQA = new ArrayList<>();
    Map<Long, Set<String>> allowedValueGroupsLookup = getAllowedValueGroupsLookup(taskReferenceService);
    if (tasks != null) {
      tasks.stream().filter(conditionalTaskFilter).forEach(task -> {
        if (task.getAttributes() != null && !task.getAttributes().isEmpty() && task.getParent() != null
            && task.isDeleted() == Boolean.FALSE) {
          List<WorkItemQAModel> workItemQAModel = new ArrayList<>();
          workItemQAModel =
              prepareQAndAModel(task.getAttributes(), parentAttributeFilter, allowedValueGroupsLookup, workItemQAModel);
          if (!workItemQAModel.isEmpty()) {
            task.setWorkItemQAModel(workItemQAModel);
            tasksWithQA.add(task);
          }
        }
      });
    }
    // sort tasks by task number LRCD-3401.
    tasksWithQA.sort((s1, s2) -> {
      int result = 0;
      if ((s1.getTaskNumber() != null && !s1.getTaskNumber().isEmpty())
          && (s2.getTaskNumber() != null && !s2.getTaskNumber().isEmpty())) {
        final String taskNo1 = s1.getTaskNumber().replace(UNDERSCORE, EMPTY_STRING);
        final String taskNo2 = s2.getTaskNumber().replace(UNDERSCORE, EMPTY_STRING);
        result = taskNo1.compareToIgnoreCase(taskNo2);
      }
      return result;
    });
    return tasksWithQA;
  }

  /**
   * Returns list of work item QA model for task.
   *
   * @param attributes the list of attributes for task.
   * @param parentAttributeFilter the filter to check whether work item attribute has parent
   *        attribute.
   * @param allowedValueGroupsLookup the allowed value groups lookup.
   * @param workItemQAModel The work item QA model to add child QA model.
   * @return the list of work item QA model.
   */
  private static List<WorkItemQAModel> prepareQAndAModel(final List<WorkItemAttributeDto> attributes,
      final Predicate<WorkItemAttributeDto> parentAttributeFilter,
      final Map<Long, Set<String>> allowedValueGroupsLookup, final List<WorkItemQAModel> workItemQAModel) {

    if (attributes != null) {
      attributes.stream().filter(parentAttributeFilter).forEach(attribute -> {
        final WorkItemAttributeDto parentAttribute = attribute;
        if (parentAttribute != null && parentAttribute.getAttributeValueString() != null
            && !parentAttribute.getAttributeValueString().isEmpty()) {
          WorkItemQAModel qaModel = new WorkItemQAModel();
          qaModel.setQuestion(parentAttribute.getDescription());
          qaModel.setAnswer(parentAttribute.getAttributeValueString());
          workItemQAModel.add(qaModel);
          prepareQAModelChild(attributes, parentAttribute, allowedValueGroupsLookup, workItemQAModel);
        }
      });
    }
    return workItemQAModel;
  }

  /**
   * Returns Qusrions and Answers child for the parent attribute.
   * <li>if work item conditional attribute parent id match with parent attribute id and</li>.
   * <li>if work item conditional attribute group value match with parent attribute value
   * string</li>.
   *
   * @param attributes The list of attributes
   * @param parentAttribute The parent attribute to check conditions.
   * @param allowedValueGroupsLookup the allowed value groups lookup.
   * @param workItemQAModel The work item QA model to add child QA model.
   *
   */
  private static void prepareQAModelChild(final List<WorkItemAttributeDto> attributes,
      final WorkItemAttributeDto parentAttribute, final Map<Long, Set<String>> allowedValueGroupsLookup,
      final List<WorkItemQAModel> workItemQAModel) {

    attributes.stream().filter(attribute -> attribute.getWorkItemConditionalAttribute() != null).forEach(attribute -> {

      if (attribute.getWorkItemConditionalAttribute().getParentAttribute() != null
          && attribute.getWorkItemConditionalAttribute().getParentAttribute().getId()
              .equals(parentAttribute.getWorkItemConditionalAttribute().getId())
          && parentAttribute.getAttributeValueString() != null && !parentAttribute.getAttributeValueString().isEmpty()
          && attribute.getAttributeValueString() != null && !attribute.getAttributeValueString().isEmpty()) {
        if (attribute.getWorkItemConditionalAttribute().getAllowedValueGroup() != null) {
          if (allowedValueGroupsLookup.get(attribute.getWorkItemConditionalAttribute().getAllowedValueGroup().getId())
              .contains(parentAttribute.getAttributeValueString())) {
            WorkItemQAModel qaModelChild = new WorkItemQAModel();
            qaModelChild.setQuestion(attribute.getDescription());
            qaModelChild.setAnswer(attribute.getAttributeValueString());
            workItemQAModel.add(qaModelChild);
            prepareQAModelChild(attributes, attribute, allowedValueGroupsLookup, workItemQAModel);
          }

        } else {

          WorkItemQAModel qaModelChild = new WorkItemQAModel();
          qaModelChild.setQuestion(attribute.getDescription());
          qaModelChild.setAnswer(attribute.getAttributeValueString());
          workItemQAModel.add(qaModelChild);
          prepareQAModelChild(attributes, attribute, allowedValueGroupsLookup, workItemQAModel);
        }
      }
    });

  }


  /**
   * Returns filter whether task has resolutionStatus(ConfirmatoryCheck (C),Complete(X)).
   *
   * @return predicate condition.
   */
  private static Predicate<WorkItemHDto> getConditionalTaskFilter() {
    return workItem -> workItem.getResolutionStatus() != null
        && (workItem.getResolutionStatus().getId().equals(ResolutionStatus.COMPLETE.getValue())
            || workItem.getResolutionStatus().getId().equals(ResolutionStatus.CONFIRMATORY_CHECK.getValue()));
  }

  /**
   * Returns filter whether work item attribute has parent attribute.
   *
   * @return predicate condition.
   */
  private static Predicate<WorkItemAttributeDto> getParentAttributeFilter() {
    return attribute -> attribute.getWorkItemConditionalAttribute() != null
        && attribute.getWorkItemConditionalAttribute().getParentAttribute() == null;
  }


  /**
   * Returns filter to check service is esp indicator service or not.
   *
   * @param espIndicatorServiceIds the list of esp indicator service ids.
   * @return predicate condition.
   */
  private static Predicate<SurveyDto> getEspIndicatorServicesFilter(final HashSet<Long> espIndicatorServiceIds) {
    if (espIndicatorServiceIds != null) {
      return survey -> espIndicatorServiceIds.contains(survey.getId());
    }
    return null;
  }

  /**
   * provides lookup for allowed value groups.
   *
   * @param taskReferenceService the task reference service.
   * @return allowed value groups lookup.
   */
  private static Map<Long, Set<String>> getAllowedValueGroupsLookup(final TaskReferenceService taskReferenceService) {

    Map<Long, Set<String>> allowedValueGroupsLookup = null;
    List<AllowedWorkItemAttributeValueHDto> allowedValues = taskReferenceService.getAllowedAttributeValues();

    allowedValueGroupsLookup =
        allowedValues.stream().collect(Collectors.groupingBy(p -> p.getAllowedValueGroup().getId(),
            Collectors.mapping(AllowedWorkItemAttributeValueHDto::getValue, Collectors.toSet())));

    return allowedValueGroupsLookup;

  }

  /**
   * Prepares asset items map with tasks.
   * <ul>
   * <li>checks asset item availability and task availability.</li>
   * <li>checks the asset item in asset model map. if it is not available asset item will be in
   * othet task list items group.</li>
   * <li>asset item available in asset item model map, then checks for parent item and updates
   * parent asset item name with asset item name and parent item name separating by underscore.</li>
   * <li>updates display order with parent item display oder and item display separated by dot.</li>
   * <li>then sort by item display order.</li>
   * </ul>
   *
   * @param model the model object with required data.
   * @param tasks the list of tasks to be grouped by items.
   * @param isServiceNarrative the booelan value to get task narrative.
   * @return the grouped item tasks.
   */
  private static Map<AssetItemHierarchyHDto, List<WorkItemHDto>> prepareAssetItemModelForTasks(
      final Map<String, Object> model, final List<WorkItemHDto> tasks, final boolean isServiceNarrative) {

    Map<AssetItemHierarchyHDto, List<WorkItemHDto>> tasksHierarchy =
        new HashMap<AssetItemHierarchyHDto, List<WorkItemHDto>>();
    Map<Long, ItemHDto> assetItemMap = (Map<Long, ItemHDto>) model.get(ASSET_ITEM_LOOKUP);
    if (tasks != null && assetItemMap != null) {

      tasks.stream().filter(taskItem -> taskItem != null).forEach(taskItem -> {
        List<WorkItemHDto> workItems = new ArrayList<>();
        AssetItemHierarchyHDto assetItemH = new AssetItemHierarchyHDto();
        ItemHDto itemH = new ItemHDto();
        String itemName = EMPTY_STRING;

        if (taskItem.getAssetItem() != null) {
          if (assetItemMap.containsKey(taskItem.getAssetItem().getId())) {
            itemH = assetItemMap.get(taskItem.getAssetItem().getId());
            assetItemH.setId(itemH.getId());
            assetItemH.setDispalyOrder(itemH.getDispalyOrder());
            if (itemH.getParentAssetItem() == PARENT_ASSET_ITEM_ID.longValue()) {
              itemName = itemH.getName();
            } else {
              ItemHDto parentItem = assetItemMap.get(itemH.getParentAssetItem());
              if (parentItem != null) {
                itemName = parentItem.getName() + UNDERSCORE + itemH.getName();
                assetItemH.setDispalyOrder(parentItem.getDispalyOrder() + DOT + itemH.getDispalyOrder());
              }
            }
          } else {
            itemName = OTHER_TASK_LIST_ITEMS;
            assetItemH.setDispalyOrder(String.valueOf(PARENT_ASSET_ITEM_ID));
          }
        }
        assetItemH.setParentAssetItemName(itemName);
        if (isServiceNarrative) {
          if (taskItem.getNotes() != null) {
            // check narratives and split by new line to print in pug file
            String[] taskNarratives = taskItem.getNotes().split(LINE_BREAK);
            taskItem.setTaskNarratives(taskNarratives);
            if (tasksHierarchy.get(assetItemH) == null) {
              tasksHierarchy.put(assetItemH, workItems);
            }
            tasksHierarchy.get(assetItemH).add(taskItem);
          }
        } else {

          if (tasksHierarchy.get(assetItemH) == null) {
            tasksHierarchy.put(assetItemH, workItems);
          }
          tasksHierarchy.get(assetItemH).add(taskItem);
        }

      });

    }

    Map<AssetItemHierarchyHDto, List<WorkItemHDto>> sortedTasksHierarchy = tasksHierarchy.entrySet().stream()
        .sorted((p, o) -> p.getKey().getDispalyOrder().compareTo(o.getKey().getDispalyOrder()))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

    return sortedTasksHierarchy;
  }

  /**
   * Generates page number for table of content in Appendix I. <br>
   * The generated Appendix I pdf (the initial generated Appendix I contains table of content
   * without page number) is reopen and loop to identify the page number for each content.
   *
   * @param filePath the Appendix I pdf file path.
   * @param tocMapping the table of content entries mapping object without value (page number).
   * @return updated the table of content entries mapping object with value (page number).
   * @throws InvalidPasswordException if the pdf content password, and password is invalid.
   * @throws IOException value if the pdf failed to read or write.
   */
  public static Map<String, Integer> generateTOCForAppendix(final String filePath,
      final Map<String, Integer> tocMapping) throws InvalidPasswordException, IOException {
    PDDocument doc;
    doc = PDDocument.load(new File(filePath));

    // create case insensitive lookup
    final Map<String, String> lowerCaseLookup = new TreeMap<>();
    for (final Entry<String, Integer> entry : tocMapping.entrySet()) {
      lowerCaseLookup.put(entry.getKey().toLowerCase(Locale.ENGLISH), entry.getKey());
    }

    // loop for checking page number
    for (final Entry<String, String> entry : lowerCaseLookup.entrySet()) {
      for (int i = 1; i <= doc.getDocumentCatalog().getPages().getCount(); i++) {
        final PDFTextStripper pdfStripper = new PDFTextStripper();
        pdfStripper.setStartPage(i);
        pdfStripper.setEndPage(i);
        final String pageText = pdfStripper.getText(doc);

        final Pattern pattern;

        if (entry.getKey().equals(TASK_RESOLUTION_STATUS_POSTPONED)) {
          pattern =
              Pattern.compile("(\\r|\\n)\\s*\\d+\\.\\s+" + TOC_PREFIX_2 + "\\s*(\\r|\\n)", Pattern.CASE_INSENSITIVE);
        } else {
          pattern = Pattern.compile(
              "(\\r|\\n)\\s*\\d+\\.\\s+" + TOC_PREFIX_1 + "\\s+" + Pattern.quote(entry.getKey()) + "\\s*(\\r|\\n)",
              Pattern.CASE_INSENSITIVE);
        }

        final Matcher matcher = pattern.matcher(pageText);

        if (matcher.find()) {
          if (entry.getKey().equals(TASK_RESOLUTION_STATUS_POSTPONED)) {
            tocMapping.put(TASK_RESOLUTION_STATUS_POSTPONED, i);
          } else {
            tocMapping.put(lowerCaseLookup.get(entry.getKey()), i);
          }
        }
      }
    }

    doc.close();

    return tocMapping;
  }

  /**
   * Sets service narratives by checking tasks with narratives and grouped by services.
   *
   * @param model the model from report.
   * @param serviceCatalogueLookup the id lookup for service catalogue.
   * @param taskReferenceService the task service reference.
   * @param espIndicatorServiceIds the esp indicator service ids.
   * @param isServiceNarrative the booelan value to get task narrative.
   */
  public static void setServiceNarratives(final Map<String, Object> model,
      final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup, final TaskReferenceService taskReferenceService,
      final HashSet<Long> espIndicatorServiceIds, final boolean isServiceNarrative) {
    Map<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>> groupedTasks =
        getGroupedTask(model, serviceCatalogueLookup, taskReferenceService, espIndicatorServiceIds, isServiceNarrative);

    Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>> servicenarrativesMap =
        new HashMap<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>();
    for (Entry<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto,
        List<WorkItemHDto>>>> productEntry : groupedTasks.entrySet()) {

      for (Entry<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>> serviceEntry : productEntry.getValue()
          .entrySet()) {

        Map<AssetItemHierarchyHDto, List<WorkItemHDto>> narrativeTasks = serviceEntry.getValue();
        String[] serviceNarratives = serviceEntry.getKey().getServiceNarratives();
        narrativeTasks.entrySet().stream().filter(entry -> entry.getValue() != null && !entry.getValue().isEmpty());

        if (!narrativeTasks.isEmpty() || (serviceNarratives != null && serviceNarratives.length > 0)) {

          servicenarrativesMap.put(serviceEntry.getKey(), narrativeTasks);
        }
      }
    }
    // sort service catalogue by display order.
    Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>> sortedServiceCatalogues =
        servicenarrativesMap.entrySet().stream()
            .sorted((e1, e2) -> e1.getKey().getServiceCatalogueH().getDisplayOrder()
                .compareTo(e2.getKey().getServiceCatalogueH().getDisplayOrder()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

    model.put(SERVICE_NARRATIVES, sortedServiceCatalogues);

  }

}
