package com.baesystems.ai.lr.cd.service.export.impl;

import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetIdAndIhsAssetId;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.OwnershipDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.UserPersonalInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemModelItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AssetInformationExportEnum;
import com.baesystems.ai.lr.cd.be.enums.AssetServiceExportEnum;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.enums.MastPartyRole;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.utils.CsvUtils;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.AssetExportService;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.enums.CodicilStatus;
import com.baesystems.ai.lr.enums.ServiceCreditStatus;
import com.baesystems.ai.lr.enums.WorkItemType;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Generates asset export PDF and return an encoded identifier that can use to trigger file
 * download.
 *
 * @author yng
 * @author YWearn 2017-05-17
 */
@Service(value = "AssetExportService")
@Loggable(Loggable.DEBUG)
public class AssetExportServiceImpl implements AssetExportService, InitializingBean {

  /**
   * The logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetExportServiceImpl.class);

  /**
   * The {@link AmazonStorageService} from Spring context.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceService;

  /**
   * The {@link TaskService} from Spring context.
   */
  @Autowired
  private TaskService taskService;

  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private CoCService coCService;

  /**
   * The {@link ActionableItemService} from Spring context.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   * The {@link AssetNoteService} from Spring context.
   */
  @Autowired
  private AssetNoteService assetNoteService;

  /**
   * The {@link StatutoryFindingService} from Spring context.
   */
  @Autowired
  private StatutoryFindingService statutoryFindingService;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The PDF Pug template file name for export with complete asset details.
   */
  private static final String PUG_TEMPLATE_FULL = "asset_export_template_full.jade";

  /**
   * The PDF Pug template file name for export with minimum details.
   */
  private static final String PUG_TEMPLATE_SIMPLE = "asset_export_template_simple.jade";

  /**
   * The constant of 1 kilobyte.
   */
  private static final int ONE_KB = 1024;

  /**
   * The variable name to identify helper utility in pug file.
   */
  private static final String TEMPLATE_HELPER = "helper";

  /**
   * The variable name to identify export configuration in pug file.
   */
  private static final String EXPORT_CONFIG = "exportConfig";

  /**
   * The variable name to identify a single asset in pug file.
   */
  private static final String ASSET_INFO = "asset";

  /**
   * The variable name to identify list of assets in pug file.
   */
  private static final String ASSET_LIST_INFO = "assets";

  /**
   * The variable name to identify single asset detail in pug file.
   */
  private static final String ASSET_DETAIL = "assetDetail";

  /**
   * The variable name to identify list of services in pug file.
   */
  private static final String SERVICES_BY_GROUP = "servicesByGroup";

  /**
   * Checklist by partheld service model name.
   */
  private static final String CHECKLIST_BY_PARTHELD_SERVICES = "checklistByPartheldServices";

  /**
   * The variable name to identify list of Cocs in pug file.
   */
  private static final String COCS_INFO = "cocs";

  /**
   * The variable name to identify list of actionale items in pug file.
   */
  private static final String ACTIONABLE_ITEMS_INFO = "actionableItems";

  /**
   * The variable name to identify list of asset notes in pug file.
   */
  private static final String ASSET_NOTE_INFO = "assetNotes";

  /**
   * The variable name to identify list of statutory findings in pug file.
   */
  private static final String STATUTORY_FINDING_INFO = "statutoryFindings";
  /**
   * The variable name to identify list of services with task in pug file.
   */
  private static final String SERVICE_GROUP_WITH_TASK = "serviceGroupWithTask";
  /**
   * The date format for parsing the date as part of working directory.
   */
  private static final DateTimeFormatter DIR_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS");

  /**
   * The date format for parsing the date as part of the PDF filename.
   */
  private static final DateTimeFormatter FILE_DATE_FORMAT = DateTimeFormatter.ofPattern("ddMMyyyyHHmm");

  /**
   * The characters use to split different part of the PDF file name.
   */
  private static final String WORD_SPLITTER = "_";

  /**
   * The zipped asset PDF file name prefix.
   */
  private static final String ZIP_FILE_PREFIX = new StringBuilder().append("asset").append(WORD_SPLITTER)
      .append("listing").append(WORD_SPLITTER).append("export").append(WORD_SPLITTER).toString();

  /**
   * The full detail asset export PDF file name prefix.
   */
  private static final String FULL_PDF_PREFIX = new StringBuilder().append("asset").append(WORD_SPLITTER).toString();

  /**
   * The minimum detail asset export PDF file name prefix.
   */
  private static final String SIMPLE_PDF_PREFIX = new StringBuilder().append("asset").append(WORD_SPLITTER)
      .append("listing").append(WORD_SPLITTER).append("export").append(WORD_SPLITTER).toString();

  /**
   * The S3 export directory name.
   */
  private static final String S3_ASSET_EXPORT_DIR = "asset_export";

  /**
   * The S3 directory separator character.
   */
  private static final String S3_FILE_SEPARATOR = "/";

  /**
   * The s3 bucket name from configuration.
   */
  @Value("${s3.export.bucket.path}")
  private String s3Bucket;

  /**
   * The file generation working directory.
   */
  @Value("${asset.export.temp.dir}")
  private String tempDir;

  /**
   * The file header use in asset export.
   */
  @Value("${asset.export.csv.fileheader}")
  private String assetHeader;

  /**
   * The legal artifact text use in asset export.
   */
  @Value("${export.legal.artifact}")
  private String legalArtifact;

  /**
   * The maximum asset allow to export per request.
   */
  @Value("${asset.export.max.count}")
  private int maxAssetExportCount;

  /**
   * The default encoding.
   */
  public static final String ENCODING = "UTF-8";

  /**
   * The default character use in CSV file export for empty value.
   */
  public static final String EMPTY_DELIMITER = "";

  /**
   * The class direct thread name prefix.
   */
  private static final String THREAD_NAME_IDENTIFIER = "-thread-";

  /**
   * Generates the assets PDF/CSV file in the S3 storage and return an encoded identifier token
   * which the invoker can used to trigger download.
   *
   * @param userId The login user identifier.
   * @param query The export configuration that defined which assets to be export into PDF.
   * @return The encoded identifier string that invoker can used to trigger download.
   * @throws ClassDirectException If any error on external API to fetch the requested asset(s)
   *         details.
   */
  @Override
  public final ExportStatusDto exportAssetAsync(final String userId, final AssetExportDto query)
      throws ClassDirectException {
    final String jobId = RandomStringUtils.randomAlphanumeric(CDConstants.MAX_UUID_LENGTH).toUpperCase(Locale.ENGLISH);
    final Date startTime = Calendar.getInstance().getTime();

    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    final UserProfiles user = token.getDetails();

    ExecutorUtils.executeTask(() -> {
      try {
        final StringResponse exportResponse = this.downloadAssetInfo(query, user);
        ExportStatusDto status = self.fetchExportStatus(userId, jobId);
        LOGGER.debug("Fetched original job status for userId: {} with jobId: {}. | {}", userId, jobId, status);
        status.setStatusCode(HttpStatus.OK.value());
        status.setEndTime(Calendar.getInstance().getTime());
        status.setDownloadToken(exportResponse.getResponse());
        status.setFileName(exportResponse.getFileName());
        status = self.updateExportStatus(userId, status);
        return status;
      } catch (final Exception e) {
        LOGGER.error("Fail to export assets for user: " + userId + " with jobId: " + jobId + ". | Error Message: "
            + e.getMessage(), e);

        ExportStatusDto status = self.fetchExportStatus(userId, jobId);
        status.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        status.setEndTime(Calendar.getInstance().getTime());
        status.setStatusDesc(e.getMessage());
        status = self.updateExportStatus(userId, status);
        return status;
      }
    }, "AssetsExport", jobId);

    ExportStatusDto result = new ExportStatusDto();
    result.setJobId(jobId);
    result.setStartTime(startTime);
    result.setStatusCode(HttpStatus.ACCEPTED.value());
    result = self.updateExportStatus(userId, result);
    return result;
  }

  /**
   * Updates the export job status.
   *
   * Notes: Used internal to update the export status into cache through Spring cache abstraction.
   *
   * @param userId The login user identifier.
   * @param status The export job status model.
   * @return The export job status model.
   */
  @Override
  public final ExportStatusDto updateExportStatus(final String userId, final ExportStatusDto status) {
    return status;
  }

  /**
     * Fetch the asset export status.
     *
     * @param userId
     *            The login user identifier.
     * @param jobId
     *            The export job unique identifier.
     * @return The export job status model.
     * @throws ClassDirectException
     *             If the export status code other than 200 and 202.
     */
    @Override
    public final ExportStatusDto fetchAssetExportStatus(final String userId, final String jobId)
            throws ClassDirectException {
        ExportStatusDto exportStatus;
        try {
            exportStatus = self.fetchExportStatus(userId, jobId);
            if (exportStatus.getStatusCode() != HttpStatus.OK.value()
                    &&  exportStatus.getStatusCode() != HttpStatus.ACCEPTED.value()) {
                throw new ClassDirectException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        "asset export failed with jobId: " + jobId + " for user: " + userId);
            }
        } catch (ClassDirectException classDirectException) {
            LOGGER.error("Cannot find export with jobId: " + jobId + " for user: " + userId);
            throw classDirectException;
        }

        return exportStatus;
    }

  /**
   * Fetches the export job status for a given user and unique job id.
   *
   * @param userId The login user identifier.
   * @param jobId The export job unique identifier.
   * @return The export job status model.
   * @throws ClassDirectException If the given user id and job id not found.
   */
  @Override
  public final ExportStatusDto fetchExportStatus(final String userId, final String jobId) throws ClassDirectException {
    throw new ClassDirectException(HttpStatus.NOT_FOUND.value(),
        "Cannot find export with jobId: " + jobId + " for user: " + userId);
  }

  /**
   * Generates the assets PDF file in the S3 storage and return an encoded identifier string which
   * the invoker can used to trigger download.
   *
   * @param query the export configuration that defined which assets to be export into PDF.
   * @return the encoded identifier string that invoker can used to trigger download.
   * @throws ClassDirectException if any error on external API to get the requested asset(s)
   *         details.
   */
  @Override
  public final StringResponse downloadAssetInfo(final AssetExportDto query, final UserProfiles user)
      throws ClassDirectException {

    try {
      // benchmark time
      LOGGER.info("AssetExport-start time: " + LocalDateTime.now());

      final LocalDateTime current = LocalDateTime.now();

      final String workingDir =
          tempDir + File.separator + user.getUserId() + WORD_SPLITTER + current.format(DIR_DATE_FORMAT);

      final File createDir = new File(workingDir);
      if (!createDir.exists()) {
        FileUtils.forceMkdir(createDir);
      }

      // generated pdf file list
      final List<String> pdfList = Collections.synchronizedList(new ArrayList<>());

      // call mast query and get asset list
      final AssetPageResource assetsPageResource = getAssetList(query, user);

      final List<AssetHDto> assets =
          Optional.ofNullable(assetsPageResource).map(AssetPageResource::getContent).orElse(new ArrayList<>(0));

      LOGGER.info("AssetExport-total assets returned: " + assets.size());

      // collection task for executor: for each asset
      final List<Callable<Void>> parentTask = new ArrayList<>();

      // check if single asset, if yes output pdf, else output packaged zip file
      String fileName = "";
      String filePath = "";


      // check if filetype is CSV
      if (!StringUtils.isEmpty(query.getFileType()) && query.getFileType().equalsIgnoreCase("CSV")) {

        LOGGER.info("AssetExport-generating simple csv template...");

        final StringBuilder csvFile = new StringBuilder();
        fileName =
            csvFile.append(SIMPLE_PDF_PREFIX).append(PdfUtils.replaceSpecialCharacters(user.getUserId(), WORD_SPLITTER))
                .append(WORD_SPLITTER).append(current.format(FILE_DATE_FORMAT)).append(".csv").toString();

        final StringBuilder outputStreamPathFilename = new StringBuilder();
        filePath = outputStreamPathFilename.append(workingDir).append(File.separator).append(fileName).toString();

        generateAssetCsv(assets, filePath);

        // check if asset requested is only displaying basic information
      } else if (query.getInfo().equals(AssetInformationExportEnum.BASIC_ASSET_INFO.getId())
          && query.getService().equals(AssetServiceExportEnum.NO_SERVICE_INFO.getId())
          && query.getCodicils().isEmpty()) {
        LOGGER.info("AssetExport-generating simple template...");

        // put assets information
        final Map<String, Object> model = new TreeMap<>();

        // get helper class
        model.put(TEMPLATE_HELPER, new TemplateHelper());

        // get export config
        model.put(EXPORT_CONFIG, query);
        // to print current date in pdf
        model.put("currentDate", new Date());
        // get asset information
        model.put(ASSET_LIST_INFO, assets);

        pdfList.add(generatePdf(model, workingDir, current));
      } else {
        LOGGER.info("AssetExport-generating full template...");

        // loop each asset to retrieve service and codicil information
        for (final AssetHDto asset : assets) {
          parentTask.add(() -> {
            try {
              Map<String, Object> model = getModel(query, asset, user);
              // to print current date in pdf
              model.put("currentDate", new Date());
              pdfList.add(generatePdf(model, asset, workingDir, current));
            } catch (Exception e) {
              LOGGER.error("Error generating pdf for asset {}. | Error Message: {}. | {}", asset.getId(),
                  e.getMessage(), e);
            }
            return null;
          });
        }

        // invoke executor
        final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
        try {
          executor.invokeAll(parentTask);
        } finally {
          executor.shutdown();
        }
      }

      // check if asset requested is only displaying basic information and basic service prepare csv
      // here
      if (!StringUtils.isEmpty(query.getFileType()) && !query.getFileType().equalsIgnoreCase("CSV")) {
        // this pdf list check in not applicable for basic information and basic service prepare csv
        // other than that it shouldn't be empty.
        if (pdfList.isEmpty()) {
          LOGGER.error("AssetExport- No pdf found for user {} , with assets {}", user.getUserId(), assets.size());
          throw new ClassDirectException(
              "AssetExport- No pdf found for user " + user.getUserId() + " with assets " + assets.size());
        } else if (pdfList.size() == 1) {
          final StringBuilder outputStreamPathFilename = new StringBuilder();
          outputStreamPathFilename.append(workingDir).append(File.separator).append(pdfList.get(0));

          fileName = pdfList.get(0);
          filePath = outputStreamPathFilename.toString();
        } else {
          final StringBuilder outFilename = new StringBuilder();
          outFilename.append(ZIP_FILE_PREFIX).append(current.format(FILE_DATE_FORMAT)).append(".zip");
          final StringBuilder outputStreamPathFilename = new StringBuilder();
          outputStreamPathFilename.append(workingDir).append(File.separator).append(outFilename.toString());

          try (final OutputStream outputStream = new FileOutputStream(outputStreamPathFilename.toString());
              final ZipOutputStream zipStream = new ZipOutputStream(outputStream)) {

            for (final String pdf : pdfList) {
              final ZipEntry entry = new ZipEntry(pdf);
              zipStream.putNextEntry(entry);

              try (final FileInputStream pdfStream = new FileInputStream(workingDir + File.separator + pdf)) {
                final byte[] buffer = new byte[ONE_KB];

                int len;
                while ((len = pdfStream.read(buffer)) > 0) {
                  zipStream.write(buffer, 0, len);
                }
              }
            }

            zipStream.closeEntry();
          }

          fileName = outFilename.toString();
          filePath = outputStreamPathFilename.toString();
        }
      }
      try {
        // place the file to s3.
        final String key = user.getUserId() + S3_FILE_SEPARATOR + S3_ASSET_EXPORT_DIR + S3_FILE_SEPARATOR + fileName;
        final String s3Token = amazonStorageService.uploadFile(s3Bucket, key,
            PdfUtils.convertToLinuxPathDelimiter(filePath), user.getUserId());

        return new StringResponse(s3Token, fileName);

      } finally {
        FileUtils.deleteDirectory(new File(workingDir));
      }

    } catch (final ClassDirectException | JadeException | IOException | InterruptedException exception) {
      LOGGER.error("AssetExport- exception occured in downloadAssetInfo. Error Message: " + exception.getMessage(),
          exception);
      throw new ClassDirectException("AssetExport-exception occured in downloadAssetInfo: " + exception.getMessage());
    } finally {
      // benchmark time
      LOGGER.info("AssetExport-end time: " + LocalDateTime.now());
    }
  }

  /**
   * Writes the export contents to file.
   *
   * @param assets the list of assets that need to export to CSV.
   * @param filePath the CSV file path.
   * @throws ClassDirectException if there is error on API to fetch asset related information.
   * @throws IOException exception if there is error on IO operation for writing file.
   */
  public final void generateAssetCsv(final List<AssetHDto> assets, final String filePath)
      throws ClassDirectException, IOException {
    final String[] fileHeaders = assetHeader.split(",");
    OutputStreamWriter fileWriter = null;
    fileWriter = new OutputStreamWriter(new FileOutputStream(new File(filePath)), ENCODING);
    // Create the CSVFormat object with "\n" as a record delimiter
    final CSVFormat csvFileFormat = CSVFormat.RFC4180.withFirstRecordAsHeader().withIgnoreEmptyLines(true).withTrim();
    // initialize CSVPrinter object
    final CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
    csvFilePrinter.printRecord((Object[]) fileHeaders);
    // collection task for executor
    final List<Callable<Void>> task = new ArrayList<>();

    try {
      // get asset detail information
      assets.forEach(asset -> {
        task.add(() -> {
          try {
            final TemplateHelper helper = new TemplateHelper();
            final List<List<Object>> assetDataList = new ArrayList<>();
            final List<ScheduledServiceHDto> services = serviceService.getServicesForAsset(asset.getId());

            final Optional<List<ScheduledServiceHDto>> servicesOpt =
                Optional.ofNullable(services).filter(list -> list.size() > 0);

            if (servicesOpt.isPresent()) {
              servicesOpt.get().sort((s1, s2) -> {
                int compareService = s1.getServiceCatalogueH().getProductCatalogue().getDisplayOrder()
                    .compareTo(s2.getServiceCatalogueH().getProductCatalogue().getDisplayOrder());
                if (compareService == 0) {
                  compareService = ServiceUtils.sortServiceByDueDate(s1.getDueDate(), s2.getDueDate());
                  if (compareService == 0) {
                    compareService =
                        ServiceUtils.sortServiceByOccurrenceNumber(s1.getOccurrenceNumber(), s2.getOccurrenceNumber());
                  }
                }
                return compareService;
              });
              servicesOpt.get().forEach(service -> {
                final List<Object> assetDataRecord = new ArrayList<Object>();
                assetDataRecord.add(CsvUtils.sanitiseFirstCharacter(asset.getName()));
                // Asset ID
                assetDataRecord.add(CsvUtils.sanitiseFirstCharacter(Optional.ofNullable(asset.getIhsAssetDto())
                    .map(IhsAssetDetailsDto::getId).orElse(EMPTY_DELIMITER)));
                // Product Catalogue
                assetDataRecord.add(CsvUtils.sanitiseFirstCharacter(
                    Optional.ofNullable(service.getServiceCatalogueH()).map(ServiceCatalogueHDto::getProductCatalogue)
                        .map(ProductCatalogueDto::getName).orElse(EMPTY_DELIMITER)));
                // Service Catalogue with OccurenceNumber
                assetDataRecord.add(CsvUtils.sanitiseFirstCharacter(
                    Optional.ofNullable(service.getServiceCatalogueName()).orElse(EMPTY_DELIMITER)));
                // Service Due Date
                assetDataRecord
                    .add(Optional.ofNullable(service.getDueDate()).map(helper::formatDate).orElse(EMPTY_DELIMITER));
                // Lower range date
                assetDataRecord.add(
                    Optional.ofNullable(service.getLowerRangeDate()).map(helper::formatDate).orElse(EMPTY_DELIMITER));
                // Upper range date
                assetDataRecord.add(
                    Optional.ofNullable(service.getUpperRangeDate()).map(helper::formatDate).orElse(EMPTY_DELIMITER));
                // Postponement date
                assetDataRecord.add(
                    Optional.ofNullable(service.getPostponementDate()).map(helper::formatDate).orElse(EMPTY_DELIMITER));

                assetDataRecord.add(CsvUtils.sanitiseFirstCharacter(service.getDueStatusH().getName()));
                assetDataList.add(assetDataRecord);
              });
            } else {
              final List<Object> assetDataRecord = new ArrayList<Object>();
              assetDataRecord.add(asset.getName());
              // Asset ID
              assetDataRecord.add(CsvUtils.sanitiseFirstCharacter(
                  Optional.ofNullable(asset.getIhsAssetDto()).map(IhsAssetDetailsDto::getId).orElse(EMPTY_DELIMITER)));

              assetDataList.add(assetDataRecord);
            }
            synchronized (csvFilePrinter) {
              assetDataList.forEach(dataList -> {
                try {
                  csvFilePrinter.printRecord(dataList);
                } catch (final IOException exception) {
                  LOGGER.error("Error in printing record for asset - {}. | Error message: {}", asset.getId(),
                      exception.getMessage());
                }
              });
            }
          } catch (IOException exception) {
            LOGGER.error("Error in fetching services for asset - {}. | Error message: {}", asset.getId(),
                exception.getMessage());
          }
          return null;
        });
      });

      // invoke executor
      final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
      try {
        executor.invokeAll(task);
      } catch (final InterruptedException e) {
        LOGGER.error("Executor error while getting asset data.", e);
        throw new ClassDirectException("Executor error while getting asset data.");
      } finally {
        executor.shutdown();
      }
      csvFilePrinter.printRecord(legalArtifact);
      LOGGER.debug("CSV file was created successfully with {} asset(s).", assets.size());
    } catch (final IOException exception) {
      LOGGER.error("Error in generating CSV file with {} asset(s). | Error Message: " + exception.getMessage(),
          exception);
      throw new ClassDirectException(exception);
    } finally {
      try {
        fileWriter.flush();
        fileWriter.close();
        csvFilePrinter.close();
      } catch (final IOException exception) {
        LOGGER.error("Error while flushing/closing fileWriter. | Error Message: " + exception, exception);
      }
    }
  }

  /**
   * Gets the list of asset information with the filter condition defined in the parameter.
   *
   * @param query the export filtering criteria.
   * @param user the detail of user requesting the asset export.
   * @return the list of assets information that fulfill the criteria.
   * @throws ClassDirectException if any API to fetch assets information fail.
   * @throws IOException if get subfleet api fails.
   */
  public final AssetPageResource getAssetList(final AssetExportDto query, final UserProfiles user)
      throws ClassDirectException, IOException {
    // trim asset Id prefix
    final List<Long> includedIds = new ArrayList<>();
    final List<Long> excludedIds = new ArrayList<>();

    if (query.getIncludes() != null) {
      includedIds.addAll(query.getIncludes().stream().map(AssetCodeUtil::getId).collect(Collectors.toList()));
    }

    if (query.getExcludes() != null) {
      excludedIds.addAll(query.getExcludes().stream().map(AssetCodeUtil::getId).collect(Collectors.toList()));
    }

    final MastQueryBuilder queryBuilder;
    if (query.getFilter() != null) {
      queryBuilder = assetService.convert(query.getFilter());
    } else {
      queryBuilder = MastQueryBuilder.create();
    }

    // Optimize mast ihs query to ensure filtering of includedID and excludedID if exists
    // Query priority: includes > exclude > all
    if (!includedIds.isEmpty()) {
      queryBuilder.mandatory(assetIdAndIhsAssetId(includedIds.toArray(new Long[] {})));
    } else if (!excludedIds.isEmpty()) {
      queryBuilder.mandatory(() -> {
        final AbstractQueryDto dto = new AbstractQueryDto();
        dto.setNot(assetIdAndIhsAssetId(excludedIds.toArray(new Long[] {})).evaluate());
        return dto;
      });
    }

    // call execute mast query
    final AssetPageResource assetsPageResource =
        assetService.getAssetByQueryBuilder(user.getUserId(), query.getFilter(), queryBuilder, 1, maxAssetExportCount);

    return assetsPageResource;
  }

  /**
   * Creates context of export details including the asset related information as well as helper
   * class to be pass into pug file for final report generation.
   *
   * @param query the export filtering criteria.
   * @param asset the asset details.
   * @param user the requested user's information.
   * @return model the context with all the data needed for pug file in the report generation.
   * @throws ClassDirectException if API to fetch asset related details fail.
   */
  public final Map<String, Object> getModel(final AssetExportDto query, final AssetHDto asset, final UserProfiles user)
      throws ClassDirectException {
    long elapsedTime = System.currentTimeMillis();
    final String logPrefix = "AssetExport[" + asset.getId() + "] : ";

    // collection task for executor: for each API call
    final List<Callable<Void>> task = new ArrayList<>();

    // model to parse into pug file
    final Map<String, Object> model = new ConcurrentHashMap<>();

    // get helper class
    model.put(TEMPLATE_HELPER, new TemplateHelper());

    // get export config
    model.put(EXPORT_CONFIG, query);

    // get asset information
    model.put(ASSET_INFO, asset);
    // get asset detail information
    model.put(ASSET_DETAIL, new AssetDetailsDto());
    if (query.getInfo().equals(AssetInformationExportEnum.FULL_ASSET_INFO.getId())) {
      task.add(() -> {
        try {
          final AssetDetailsDto assetDetail = assetService.getAssetDetailsByAsset(asset, user.getUserId());

          // Copy the mast asset customer to ihs if any
          if (asset.getCustomersH() != null) {
            final OwnershipDto ownership = Optional.ofNullable(assetDetail.getOwnership()).orElse(new OwnershipDto());
            asset.getCustomersH().forEach(customer -> {
              final PartyDto customerParty = customer.getParty().getCustomer();
              final long roleId = customer.getPartyRoles().getId().longValue();
              if (MastPartyRole.SHIP_OWNER.getValue().longValue() == roleId) {
                final UserPersonalInformationDto registeredOwner = toUserPersonalInformationDto(customerParty);
                ownership.setRegisteredOwner(registeredOwner);
              } else if (MastPartyRole.GROUP_OWNER.getValue().longValue() == roleId) {
                final UserPersonalInformationDto groupOwner = toUserPersonalInformationDto(customerParty);
                ownership.setGroupOwner(groupOwner);
              } else if (MastPartyRole.SHIP_MANAGER.getValue().longValue() == roleId) {
                final UserPersonalInformationDto shipManager = toUserPersonalInformationDto(customerParty);
                ownership.setShipManager(shipManager);
              } else if (MastPartyRole.SHIP_OPERATOR.getValue().longValue() == roleId) {
                final UserPersonalInformationDto shipOperator = toUserPersonalInformationDto(customerParty);
                ownership.setOperator(shipOperator);
              } else if (MastPartyRole.DOC_COMPANY.getValue().longValue() == roleId) {
                final UserPersonalInformationDto docCompany = toUserPersonalInformationDto(customerParty);
                ownership.setDoc(docCompany);
              }
            });
            assetDetail.setOwnership(ownership);
          }


          model.put(ASSET_DETAIL, assetDetail);
        } catch (final Exception exception) {
          LOGGER.error("{} Failed to get asset detail: {}", logPrefix, exception.getMessage());
        }
        return null;
      });
    }

    // get service detail information
    model.put(SERVICES_BY_GROUP, Collections.EMPTY_MAP);
    model.put(CHECKLIST_BY_PARTHELD_SERVICES, Collections.EMPTY_MAP);
    if (!asset.getRestricted() && (query.getService().equals(AssetServiceExportEnum.BASIC_SERVICE_INFO.getId())
        || query.getService().equals(AssetServiceExportEnum.FULL_SERVICE_INFO.getId()))) {
      task.add(() -> {
        try {
          final List<ScheduledServiceHDto> services = serviceService.getServicesForAsset(asset.getId());
          if (services != null) {
            final boolean fullServiceFlag = query.getService().equals(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());
            final Map<String, List<ServiceTaskContainer>> serviceGroup =
                populateAssetServiceGroup(services, asset, fullServiceFlag);
            for (Map.Entry<String, List<ServiceTaskContainer>> entry : serviceGroup.entrySet()) {
              List<ServiceTaskContainer> serviceList = entry.getValue();
              serviceList.sort((s1, s2) -> {
                int compareRetVal =
                    ServiceUtils.sortServiceByDueDate(s1.getService().getDueDate(), s2.getService().getDueDate());
                if (compareRetVal == 0) {
                  compareRetVal = s1.getService().getServiceCatalogueH().getDisplayOrder()
                      .compareTo(s2.getService().getServiceCatalogueH().getDisplayOrder());
                  if (compareRetVal == 0) {
                    compareRetVal = ServiceUtils.sortServiceByOccurrenceNumber(s1.getService().getOccurrenceNumber(),
                        s2.getService().getOccurrenceNumber());
                  }
                }
                return compareRetVal;
              });
            }
            model.put(SERVICES_BY_GROUP, serviceGroup);
            if (query.getService().equals(AssetServiceExportEnum.FULL_SERVICE_INFO.getId())) {
              LOGGER.trace("Add checklist by partheld services");
              Map<ScheduledServiceHDto, List<WorkItemLightHDto>> checklistItems;
              final List<ScheduledServiceHDto> partHeldServices = services.stream()
                  .filter(service -> service.getServiceCreditStatusH() != null
                      && ServiceCreditStatus.PART_HELD.value().equals(service.getServiceCreditStatusH().getId())
                      && service.getServiceCatalogueH().getWorkItemType() != null && service.getServiceCatalogueH()
                          .getWorkItemType().getId().equals(WorkItemType.CHECKLIST.value()))
                  .collect(Collectors.toList());
              if (!partHeldServices.isEmpty()) {
                try {
                  checklistItems = taskService.getCheckListItemByService(partHeldServices);
                  if (!checklistItems.isEmpty()) {
                    model.put(CHECKLIST_BY_PARTHELD_SERVICES, checklistItems);
                  }
                } catch (ClassDirectException exception) {
                  LOGGER.error("Unable to get tasklist for service " + partHeldServices, exception);
                }

              } else {
                LOGGER.trace("No partheld service found.");
              }
            }
          }
        } catch (final Exception exception) {
          LOGGER.error("{} Failed to get service detail: {}", logPrefix, exception.getMessage());
        }
        return null;
      });
    }

    // get CoC information
    model.put(COCS_INFO, Collections.EMPTY_LIST);
    if (!asset.getRestricted() && query.getCodicils().contains(CodicilType.COC.getId())) {
      task.add(() -> {
        try {
          final List<CoCHDto> cocList = coCService.getCoCForExport(asset.getId());
          List<CoCHDto> cocFinalList =
              cocList.stream().filter(coc -> coc.getStatusH().getId().equals(CodicilStatus.COC_OPEN.getValue()))
                  .collect(Collectors.toList());
          Optional.ofNullable(cocFinalList).ifPresent(cocsList -> {
            cocFinalList.sort(ServiceUtils.codicilDefaultComparator());
            model.put(COCS_INFO, cocsList);
          });
        } catch (final Exception exception) {
          LOGGER.error("{} Failed to get coc detail: {}", logPrefix, exception.getMessage());
        }
        return null;
      });
    }

    // get Actionable Item information
    model.put(ACTIONABLE_ITEMS_INFO, Collections.EMPTY_LIST);
    if (!asset.getRestricted() && query.getCodicils().contains(CodicilType.AI.getId())) {
      task.add(() -> {
        try {
          final List<ActionableItemHDto> actionableItems =
              actionableItemService.getActionableItems(asset.getId(), null);
          List<ActionableItemHDto> actionableItemsList = actionableItems.stream()
              .filter(actionableItem -> actionableItem.getStatusH().getId().equals(CodicilStatus.AI_OPEN.getValue())
                  || actionableItem.getStatusH().getId().equals(CodicilStatus.AI_CHANGE_RECOMMENDED.getValue()))
              .collect(Collectors.toList());
          actionableItemsList.sort(ServiceUtils.codicilDefaultComparator());
          model.put(ACTIONABLE_ITEMS_INFO, actionableItemsList);

        } catch (final Exception exception) {
          LOGGER.error("{} Failed to get actionable item detail: {}", logPrefix, exception.getMessage());
        }
        return null;
      });
    }

    // get Asset note information
    model.put(ASSET_NOTE_INFO, Collections.EMPTY_LIST);
    if (!asset.getRestricted() && query.getCodicils().contains(CodicilType.AN.getId())) {
      task.add(() -> {
        try {
          final PageResource<AssetNoteHDto> assetNotes = assetNoteService.getAssetNote(null, null, asset.getId());
          Optional.ofNullable(assetNotes).map(PageResource::getContent).ifPresent(assetNote -> {
            List<AssetNoteHDto> assetNotesList = assetNote.stream()
                .filter(item -> item.getStatusH().getId().equals(CodicilStatus.AN_OPEN.getValue())
                    || item.getStatusH().getId().equals(CodicilStatus.AN_CHANGE_RECOMMENDED.getValue()))
                .collect(Collectors.toList());
            assetNotesList.sort(ServiceUtils.assetNotesDefaultComparator());
            model.put(ASSET_NOTE_INFO, assetNotesList);
          });

        } catch (final Exception exception) {
          LOGGER.error("{} Failed to get asset note detail: {}", logPrefix, exception.getMessage());
        }
        return null;
      });
    }

    // get Statutory finding information
    model.put(STATUTORY_FINDING_INFO, Collections.EMPTY_LIST);
    if (!asset.getRestricted() && query.getCodicils().contains(CodicilType.SF.getId())) {
      task.add(() -> {
        try {
          final List<StatutoryFindingHDto> statutoryFindingList =
              statutoryFindingService.getStatutories(asset.getId(), new CodicilDefectQueryHDto());
          List<StatutoryFindingHDto> statutoryFindings = statutoryFindingList.stream()
              .filter(statutory -> statutory.getStatusH().getId().equals(CodicilStatus.SF_OPEN.getValue()))
              .collect(Collectors.toList());
          statutoryFindings.sort(ServiceUtils.codicilDefaultComparator());
          model.put(STATUTORY_FINDING_INFO, statutoryFindings);

        } catch (final Exception exception) {
          LOGGER.error("{} Failed to get statutory findings detail: {}", logPrefix, exception.getMessage());
        }
        return null;
      });
    }

    // invoke executor

    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(task.size(),
        this.getClass().getSimpleName() + THREAD_NAME_IDENTIFIER + getParentThreadId());
    try {
      executor.invokeAll(task);
    } catch (final InterruptedException e) {
      throw new ClassDirectException("Executor error while getting asset data for " + asset.getId());
    } finally {
      executor.shutdown();
      elapsedTime = System.currentTimeMillis() - elapsedTime;
      LOGGER.info(
          "{} Finish getting asset information for export. "
              + "| AssetId: {}, ServicesByGroup: {}, cocs: {}, AI: {}, AN: {}, SF: {} | Elapsed time (ms) : {}",
          logPrefix, asset.getId(), getListSizeFromModel(model, SERVICES_BY_GROUP),
          getListSizeFromModel(model, COCS_INFO), getListSizeFromModel(model, ACTIONABLE_ITEMS_INFO),
          getListSizeFromModel(model, ASSET_NOTE_INFO), getListSizeFromModel(model, STATUTORY_FINDING_INFO),
          elapsedTime);
    }
    if (query.getService().equals(AssetServiceExportEnum.FULL_SERVICE_INFO.getId())) {
      model.put(SERVICE_GROUP_WITH_TASK, Collections.EMPTY_MAP);
      Map<String, List<ServiceTaskContainer>> serviceGroupWithTask = new LinkedHashMap<>();
      Map<String, List<ServiceTaskContainer>> serviceGroup =
          (Map<String, List<ServiceTaskContainer>>) model.get(SERVICES_BY_GROUP);
      serviceGroup.entrySet().stream().forEach(serviceGp -> {
        List<ServiceTaskContainer> taskList = serviceGp.getValue().stream()
            .filter(taskContainer -> taskContainer.getService().getServiceCatalogueH().getWorkItemType() != null
                && taskContainer.getService().getServiceCatalogueH().getWorkItemType().getId()
                    .equals(WorkItemType.TASK.value())
                && taskContainer.getTask() != null)
            .collect(Collectors.toList());
        if (taskList.size() > 0) {
          serviceGroupWithTask.put(serviceGp.getKey(), taskList);
        }
      });
      model.put(SERVICE_GROUP_WITH_TASK, serviceGroupWithTask);
    }

    // get job ids AI,AN and Sf prepare lookup map.
    List<Long> codicilJobIds = new ArrayList<>();
    Set<Long> jobIds = new HashSet<>();
    List<ActionableItemHDto> actionableItems = (List<ActionableItemHDto>) model.get(ACTIONABLE_ITEMS_INFO);
    List<Long> actionableItemJobIds = actionableItems.stream().map(ActionableItemHDto::getJob)
        .filter(job -> job != null).map(LinkResource::getId).collect(Collectors.toList());

    jobIds.addAll(actionableItemJobIds);

    List<AssetNoteHDto> assetNotes = (List<AssetNoteHDto>) model.get(ASSET_NOTE_INFO);
    List<Long> assetNotesJobIds = assetNotes.stream().map(AssetNoteHDto::getJob).filter(job -> job != null)
        .map(LinkResource::getId).collect(Collectors.toList());
    jobIds.addAll(assetNotesJobIds);

    List<StatutoryFindingHDto> statutoryFindings = (List<StatutoryFindingHDto>) model.get(STATUTORY_FINDING_INFO);
    List<Long> statutoryFindingsJobIds = statutoryFindings.stream().map(StatutoryFindingHDto::getJob)
        .filter(job -> job != null).map(LinkResource::getId).collect(Collectors.toList());
    jobIds.addAll(statutoryFindingsJobIds);
    codicilJobIds.addAll(jobIds);

    Map<Long, JobHDto> jobsLookup = generateJobsLookup(codicilJobIds);

    actionableItems.forEach(actionableItem -> {
      if (actionableItem.getJob() != null) {
        actionableItem.setJobH(jobsLookup.get(actionableItem.getJob().getId()));
      }
    });

    assetNotes.forEach(assetNote -> {
      if (assetNote.getJob() != null) {
        assetNote.setJobH(jobsLookup.get(assetNote.getJob().getId()));
      }
    });

    statutoryFindings.forEach(statutory -> {
      if (statutory.getJob() != null) {
        statutory.setJobH(jobsLookup.get(statutory.getJob().getId()));
      }
    });
    model.put(ACTIONABLE_ITEMS_INFO, actionableItems);
    model.put(ASSET_NOTE_INFO, assetNotes);
    model.put(STATUTORY_FINDING_INFO, statutoryFindings);

    return model;
  }

  /**
   * Generates jobs lookup.
   *
   * @param jobIds The list of job ids.
   * @return the jobs lookup object.
   */
  public final Map<Long, JobHDto> generateJobsLookup(final List<Long> jobIds) {
    final Map<Long, JobHDto> jobDataLookUp = new ConcurrentHashMap<>();
    final ExecutorService executorService = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    final List<Callable<JobHDto>> callables = new ArrayList<>();
    try {

      jobIds.stream().forEach(id -> callables.add(() -> {
        JobHDto jobDto = null;
        try {
          jobDto = jobServiceDelegate.getJobsByJobId(id).execute().body();
        } catch (final Exception exception) {
          LOGGER.error("Error fetching job {} . | Error Message: {}", id, exception.getMessage());
        }
        return jobDto;
      }));

      final List<Future<JobHDto>> taskList = executorService.invokeAll(callables);
      for (final Future<JobHDto> task : taskList) {
        if (task.get() != null) {
          jobDataLookUp.put(task.get().getId(), task.get());
        }
      }

    } catch (final InterruptedException | ExecutionException exception) {
      LOGGER.debug("Error in executing jobs", exception.getMessage());
    } finally {
      executorService.shutdown();
    }
    return jobDataLookUp;
  }

  /**
   * Populates the assets services in hierarchy based on service catalogue and product catalogue.
   *
   * @param services the list of services associated to the asset.
   * @param asset the asset information.
   * @param fullServiceFlag the flag to determine if need to further fetch the service task
   *        information.
   * @return the hierarchy asset services.
   * @throws ClassDirectException if fail to fetch the tasks associated to the asset services.
   */
  private Map<String, List<ServiceTaskContainer>> populateAssetServiceGroup(final List<ScheduledServiceHDto> services,
      final AssetHDto asset, final boolean fullServiceFlag) throws ClassDirectException {
    final Map<String, List<ServiceTaskContainer>> serviceGroup = new LinkedHashMap<>();
    final Map<Long, WorkItemPreviewScheduledServiceHDto> serviceTasksLookup;

    // First fetch all the task for all services.
    if (fullServiceFlag) {
      final List<Long> ids = services.stream().map(service -> service.getId()).collect(Collectors.toList());
      WorkItemPreviewListHDto workItemPreviewListHDto =
          taskService.getHierarchicalTasksByServiceIds(ids, asset.getId());
      if (null != workItemPreviewListHDto) {
        List<WorkItemPreviewScheduledServiceHDto> workItemPreviewScheduledServiceList =
            workItemPreviewListHDto.getServicesH();
        serviceTasksLookup = workItemPreviewScheduledServiceList.stream()
            .collect(Collectors.toMap(WorkItemPreviewScheduledServiceHDto::getId, Function.identity()));
      } else {
        serviceTasksLookup = new HashMap<>(0);
      }
    } else {
      serviceTasksLookup = new HashMap<>(0);
    }
    services.sort((s1, s2) -> s1.getServiceCatalogueH().getProductCatalogue().getDisplayOrder()
        .compareTo(s2.getServiceCatalogueH().getProductCatalogue().getDisplayOrder()));
    services.forEach(service -> {
      if (service.getServiceCatalogueH() != null && service.getServiceCatalogueH().getProductCatalogue() != null) {
        // create hash grouping by service group
        if (serviceGroup.get(service.getServiceCatalogueH().getProductCatalogue().getName()) == null) {
          serviceGroup.put(service.getServiceCatalogueH().getProductCatalogue().getName(), new ArrayList<>());
        }

        // instantiate container instance
        final ServiceTaskContainer container = new ServiceTaskContainer();
        Optional<Integer> occurenceNumber = Optional.ofNullable(service.getOccurrenceNumber());
        if (occurenceNumber.isPresent()) {
          service.setServiceCatalogueName(String.join(" - ", service.getServiceCatalogueH().getName(),
              String.valueOf(service.getOccurrenceNumber())));
        } else {
          service.setServiceCatalogueName(service.getServiceCatalogueH().getName());
        }
        container.setService(service);
        container.setTask(null);

        // get task list for service, if full service info is selected
        if (fullServiceFlag) {
          final WorkItemPreviewScheduledServiceHDto hierarchyTask = serviceTasksLookup.get(service.getId());
          if (hierarchyTask != null) {
            if (service.getServiceCatalogueH() != null && service.getServiceCatalogueH().getContinuousIndicator()) {
              if (hierarchyTask.getItemsH() != null && !hierarchyTask.getItemsH().isEmpty()) {
                container.setTask(filterTaskList(hierarchyTask, getResolutionStatusFortask()));
              }
            } else {
              container.setTask(hierarchyTask);
            }
            if (container.getTask().getItemsH() != null && !hierarchyTask.getItemsH().isEmpty()) {
              container.getTask().getItemsH().forEach(this::sortTasklistByTaskNo);
              container.getTask().setItemsH(sortTasklistByItemDisplayOrder(container.getTask().getItemsH()));
            }
          }
        }

        // put into service group hash
        final List<ServiceTaskContainer> serviceGroupList =
            serviceGroup.get(service.getServiceCatalogueH().getProductCatalogue().getName());
        serviceGroupList.add(container);
        serviceGroup.put(service.getServiceCatalogueH().getProductCatalogue().getName(), serviceGroupList);
      }
    });
    return serviceGroup;
  }

  /**
   * Sort task list by task number.
   *
   * @param taskItem work item.
   */
  private void sortTasklistByTaskNo(final WorkItemModelItemHDto taskItem) {
    if (taskItem.getTasksH() != null && !taskItem.getTasksH().isEmpty()) {
      Collections.sort(taskItem.getTasksH(), (taskItemdto1, taskitemdto2) -> {
        int result = 0;
        if ((taskItemdto1 != null && taskItemdto1.getTaskNumber() != null && !taskItemdto1.getTaskNumber().isEmpty())
            && (taskitemdto2 != null && taskitemdto2.getTaskNumber() != null
                && !taskitemdto2.getTaskNumber().isEmpty())) {
          final String taskNo1 = taskItemdto1.getTaskNumber().replace("-", "");
          final String taskNo2 = taskitemdto2.getTaskNumber().replace("-", "");
          result = taskNo1.compareToIgnoreCase(taskNo2);
        }
        return result;
      });
    }

    if (taskItem.getItemsH() != null && !taskItem.getItemsH().isEmpty()) {
      taskItem.getItemsH().forEach(this::sortTasklistByTaskNo);
    }
  }

  /**
   * Sort task list by items display order.
   *
   * @param itemsList itemsList.
   * @return List<WorkItemModelItemHDto>.
   */

  private List<WorkItemModelItemHDto> sortTasklistByItemDisplayOrder(final List<WorkItemModelItemHDto> itemsList) {
    Collections.sort(itemsList, (itemDto1, itemDto2) -> {
      return itemDto1.getDisplayOrder().compareTo(itemDto2.getDisplayOrder());
    });
    itemsList.forEach(items -> {
      if (items.getItemsH() != null && !items.getItemsH().isEmpty()) {
        sortTasklistByItemDisplayOrder(items.getItemsH());
      }
    });
    return itemsList;
  }

  /**
   * Filter task list based on resolution status.
   *
   * @param service the service items which have to be filtered.
   * @param filter the condition to filter.
   * @return WorkItemPreviewScheduledServiceHDto.
   */
  private WorkItemPreviewScheduledServiceHDto filterTaskList(final WorkItemPreviewScheduledServiceHDto service,
      final Predicate<WorkItemLightHDto> filter) {
    // segregation of first level items
    if (service.getItemsH() != null && !service.getItemsH().isEmpty()) {
      List<WorkItemModelItemHDto> serviceItems = service.getItemsH().stream().map(itemH -> {
        return recursiveTask(itemH, filter);
      }).filter(Objects::nonNull).collect(Collectors.toList());
      service.setItemsH(serviceItems);
    }
    return service;
  }

  /**
   * Loop through the items.
   *
   * @param item item.
   * @param filter item.
   * @return WorkItemModelItemHDto.
   */
  private WorkItemModelItemHDto recursiveTask(final WorkItemModelItemHDto item,
      final Predicate<WorkItemLightHDto> filter) {
    // filter for next level valid items.
    if (item.getItemsH() != null) {
      List<WorkItemModelItemHDto> validItems = item.getItemsH().stream().map(itemH -> {
        return recursiveItem(itemH, filter);
      }).filter(Objects::nonNull).collect(Collectors.toList());
      item.setItemsH(validItems);
    }
    // filter for valid tasks.
    if (item.getTasksH() != null) {
      final List<WorkItemLightHDto> validTasks =
          item.getTasksH().stream().filter(filter).collect(Collectors.<WorkItemLightHDto>toList());
      item.setTasksH(validTasks);
    }
    return item;
  }

  /**
   * Loop through item to get task.
   *
   * @param item item.
   * @param filter filter.
   * @return WorkItemModelItemHDto.
   */
  private WorkItemModelItemHDto recursiveItem(final WorkItemModelItemHDto item,
      final Predicate<WorkItemLightHDto> filter) {
    // filter for next level valid items.
    if (item.getItemsH() != null) {
      List<WorkItemModelItemHDto> validItems = item.getItemsH().stream().map(itemH -> {
        return recursiveItem(itemH, filter);
      }).filter(Objects::nonNull).collect(Collectors.toList());
      item.setItemsH(validItems);
    }
    // filter for valid tasks.
    if (item.getTasks() != null) {
      final List<WorkItemLightHDto> validTasks =
          item.getTasksH().stream().filter(filter).collect(Collectors.<WorkItemLightHDto>toList());
      item.setTasksH(validTasks);
    }
    return item;
  }

  /**
   * Returns filter whether task has continuous service or not.
   *
   * @return predicate condition.
   */
  private Predicate<WorkItemLightHDto> getResolutionStatusFortask() {
    return filteredTask -> filteredTask.getResolutionStatus() == null;
  }

  /**
   * Creates new asset user information model from MAST customer model.
   *
   * @param mastParty the MAST asset customer model.
   * @return the asset user information model.
   */
  private UserPersonalInformationDto toUserPersonalInformationDto(final PartyDto mastParty) {
    final UserPersonalInformationDto userInfo = new UserPersonalInformationDto();

    userInfo.setName(mastParty.getName());
    userInfo.setEmailAddress(mastParty.getEmailAddress());
    userInfo.setPhoneNumber(mastParty.getPhoneNumber());
    List<String> addressList = Arrays.asList(mastParty.getAddressLine1(), mastParty.getAddressLine2(),
        mastParty.getAddressLine3(), mastParty.getCity(), mastParty.getPostcode(), mastParty.getCountry());
    final String address =
        addressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));
    userInfo.setAddress(address);
    return userInfo;
  }

  /**
   * Fetches the list from the model map and return the collection size.
   *
   * @param model the export model with asset related information.
   * @param key the model map key.
   * @return the collection size for given key.
   */
  private int getListSizeFromModel(final Map<String, Object> model, final String key) {
    int size = -1;
    Object value = model.get(key);

    if (value instanceof Collection) {
      size = ((Collection<?>) value).size();
    } else if (value instanceof Map) {
      size = ((Map<?, ?>) value).size();
    }

    return size;
  }

  /**
   * Extracts the parent thread number from thread name.
   *
   * @return the parent thread number.
   */
  private String getParentThreadId() {
    String threadId = "";
    final String threadName = Thread.currentThread().getName();

    if (StringUtils.contains(threadName, THREAD_NAME_IDENTIFIER)) {
      threadId = StringUtils.substringAfterLast(threadName, THREAD_NAME_IDENTIFIER);
    }
    return threadId;
  }


  /**
   * Prepares the single asset PDF file name and invoke the actual PDF generation.
   *
   * @param model the context with all the data needed for pug file in the report generation.
   * @param asset the asset detail.
   * @param workingDir the directory to generate the PDF file.
   * @param date the date of the PDF generation.
   * @return the generated PDF file name.
   * @throws ClassDirectException if the PDF generation fail.
   */
  public final String generatePdf(final Map<String, Object> model, final AssetHDto asset, final String workingDir,
      final LocalDateTime date) throws ClassDirectException {
    // get asset code
    final String assetCode = Optional.ofNullable(asset.getCode()).orElse("");

    // get asset name
    final String assetName = Optional.ofNullable(asset.getName()).orElse("");

    asset.setImoNumber(AssetCodeUtil.getAssetImo(asset));

    final StringBuilder pdfFile = new StringBuilder();
    pdfFile.append(FULL_PDF_PREFIX).append(assetCode).append(WORD_SPLITTER)
        .append(PdfUtils.replaceSpecialCharacters(assetName, WORD_SPLITTER)).append(WORD_SPLITTER)
        .append(date.format(FILE_DATE_FORMAT)).append(".pdf");

    printToPdf(model, workingDir, pdfFile.toString(), PUG_TEMPLATE_FULL);

    LOGGER.info("AssetExport- pdf created with name: {}, code: {}", assetName, assetCode);
    return pdfFile.toString();
  }

  /**
   * Prepares the multiple assets PDF file name and invoke the actual PDF generation.
   *
   * @param model the context with all the data needed for pug file in the report generation.
   * @param workingDir the directory to generate the PDF file.
   * @param date the date of the PDF generation.
   * @return the generated PDF file name.
   * @throws ClassDirectException if the PDF generation fail.
   */
  public final String generatePdf(final Map<String, Object> model, final String workingDir, final LocalDateTime date)
      throws ClassDirectException {
    final StringBuilder pdfFile = new StringBuilder();
    pdfFile.append(SIMPLE_PDF_PREFIX).append(date.format(FILE_DATE_FORMAT)).append(".pdf");

    printToPdf(model, workingDir, pdfFile.toString(), PUG_TEMPLATE_SIMPLE);

    LOGGER.info("AssetExport-pdf created");
    return pdfFile.toString();
  }

  /**
   * Invokes the PDF generation using pub file and the data model.
   *
   * @param model the context with all the data needed for pug file in the report generation.
   * @param workingDir the directory to generate the PDF file.
   * @param filename the final PDF file name.
   * @param template the pug template file.
   * @throws ClassDirectException if the PDF generation using pug fail.
   */
  public final void printToPdf(final Map<String, Object> model, final String workingDir, final String filename,
      final String template) throws ClassDirectException {
    try {
      PdfUtils.createPdfFromTemplate(model, workingDir, filename, template);
      PdfUtils.createWatermark(workingDir + File.separator + filename);
    } catch (SAXException | IOException | DocumentException | ParserConfigurationException exception) {
      throw new ClassDirectException("Failed to create pdf file: " + exception);
    }

  }

  /**
   * The Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The reference to itself in order to trigger the cacheable annotation.
   *
   */
  private AssetExportService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(AssetExportService.class);
  }

  /**
   * Provides a container to store the services related task in proper grouping.
   *
   * @author yng
   */
  public static final class ServiceTaskContainer {
    /**
     * The service detail.
     */
    private ScheduledServiceHDto service;

    /**
     * Task related to this service.
     */
    private WorkItemPreviewScheduledServiceHDto task;

    /**
     * Gets the service detail.
     *
     * @return the service detail.
     */
    public ScheduledServiceHDto getService() {
      return service;
    }

    /**
     * Sets the service detail.
     *
     * @param serviceObj the service detail.
     */
    public void setService(final ScheduledServiceHDto serviceObj) {
      this.service = serviceObj;
    }

    /**
     * Gets task related to given service.
     *
     * @return task related to given service.
     */
    public WorkItemPreviewScheduledServiceHDto getTask() {
      return task;
    }

    /**
     * Sets task related to given service.
     *
     * @param pTask task related to given service.
     */
    public void setTask(final WorkItemPreviewScheduledServiceHDto pTask) {
      this.task = pTask;
    }

  }


}

