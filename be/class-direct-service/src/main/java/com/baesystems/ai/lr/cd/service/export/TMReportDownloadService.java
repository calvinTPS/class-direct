package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.AttachmentExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides service to get encrypted token which contain generated PDF report in server file path
 * location.
 *
 * @author syalavarthi.
 *
 */
public interface TMReportDownloadService {
  /**
   * Generates TM report PDF for given asset.
   *
   * @param query query body with search parameters.
   * @return the encrypted token which contain generated PDF report in server file path location.
   * @throws ClassDirectException if API call from MAST, or PDF template compilation fail.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadAttachmentInfo(AttachmentExportDto query) throws ClassDirectException;
}
