package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.MajorNCNService;
import com.baesystems.ai.lr.cd.service.export.MNCNAttachmentService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.enums.ConfidentialityType;

import retrofit2.Call;

/**
 * Provides service to fetches attachments for major non confirmitive note.
 *
 * @author syalavarthi.
 */
@Service("MNCNAttachmentService")
public class MNCNAttachmentServiceImpl implements MNCNAttachmentService {
  /**
   * The logger for MNCNAttachmentServiceImpl.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MNCNAttachmentServiceImpl.class);

  /**
   * The {@Link AttachmentService} from Spring context.
   */
  @Autowired
  private AttachmentRetrofitService attachmentRetrofitService;

  /**
   * The {@Link MajorNCNService} from Spring context.
   */
  @Autowired
  private MajorNCNService majorNCNService;

  /**
   * The confidentiality type zero used in validation.
   */
  private static final Long CONFIDENTIALITY_ZERO = 0L;

  @Override
  public final List<SupplementaryInformationHDto> getAttachmentsMncn(final Long assetId, final Long mncnId)
      throws ClassDirectException {
    final List<SupplementaryInformationHDto> supplementaryInfoDtos = new ArrayList<>();

    try {

      // get logged in userId.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      final String userId = token.getDetails().getUserId();

      // get mncn by mncn id.
      MajorNCNHDto majorNCN = majorNCNService.getMajorNCNByMNCNId(assetId, mncnId);
      if (!StringUtils.isEmpty(majorNCN)) {

        final Call<List<SupplementaryInformationHDto>> attachmentsCall =
            attachmentRetrofitService.getAttachmentsByAssetIdAndMncnId(assetId, mncnId);

        Optional.ofNullable(attachmentsCall.execute().body()).ifPresent(attachments ->

        supplementaryInfoDtos.addAll(attachments.stream().filter(attachmentInfo -> {
          final Long confidentialityType =
              Optional.ofNullable(attachmentInfo).map(SupplementaryInformationHDto::getConfidentialityType)
                  .map(LinkResource::getId).orElse(CONFIDENTIALITY_ZERO);
          return confidentialityType.equals(ConfidentialityType.LR_AND_CUSTOMER.getValue())
              || confidentialityType.equals(ConfidentialityType.ALL.getValue());

        }).map(attachment -> {
          Resources.inject(attachment);
          Integer attachNodeId = null;
          Integer attachVersion = null;
          try {
            attachNodeId = Optional.ofNullable(attachment.getAttachmentUrl()).map(Integer::valueOf).orElse(null);

            attachVersion = Optional.ofNullable(attachment.getDocumentVersion()).map(Long::intValue).orElse(null);
            attachment.setToken(FileTokenGenerator.generateCS10Token(attachNodeId, attachVersion, userId));
            LOGGER.info("attachment token " + attachment.getToken());
          } catch (final ClassDirectException exception) {
            LOGGER.error(" error occured in generating token with attachmenNodeId {} and attachmentVersionId {}",
                attachNodeId, attachVersion);
          }
          return attachment;
        }).collect(Collectors.toList()))

        );
      } else {
        LOGGER.error("record not found for mncn {}", mncnId);
        throw new RecordNotFoundException("record not found for mncn " + mncnId);
      }

    } catch (final IOException exception) {
      LOGGER.error(" error occured getting attachments for mncnId {} and asset id {}", mncnId, assetId);
      throw new RecordNotFoundException(exception.getMessage());
    }

    return supplementaryInfoDtos;
  }

}
