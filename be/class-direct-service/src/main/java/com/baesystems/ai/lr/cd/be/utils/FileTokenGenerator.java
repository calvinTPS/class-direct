package com.baesystems.ai.lr.cd.be.utils;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import com.amazonaws.regions.Regions;
import com.baesystems.ai.lr.cd.be.domain.dto.download.CS10FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.download.FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.download.S3FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.enums.DownloadTypeEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provides methods to generate encrypted,encoded token and to decrypt ,decode download token.
 *
 * @author Faizal Sidek
 * @author SBollu.
 */
public final class FileTokenGenerator {


  /**
   * The logger object to display logs {@link FileTokenGenerator}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FileTokenGenerator.class);

  /**
   * The default S3 region to Ireland.
   */
  private static final String DEFAULT_S3_REGION = Regions.EU_WEST_1.toString();

  /**
   * The default object mapper.
   */
  private static ObjectMapper objectMapper;

  static {
    objectMapper = new ObjectMapper();
    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
  }

  /**
   * Constructs default FileTokenGenerator.
   */
  private FileTokenGenerator() {

  }

  /**
   * Generates S3 file token with default region.
   *
   * @param bucketName the s3 bucket name.
   * @param key the s3 file key.
   * @return encoded string token.
   * @throws ClassDirectException if any padding,algorithm and invalid key exception occurs.
   */
  public static String generateS3Token(final String bucketName, final String key) throws ClassDirectException {
    final CDAuthToken userToken = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    String userId = userToken.getDetails().getUserId();
    return generateS3Token(DEFAULT_S3_REGION, bucketName, key, userId);
  }

  /**
   * Generates S3 file token with specific region, bucket name and key.
   *
   * @param region the specific region name with token to be generate.
   * @param bucketName the bucket name with token to be generate.
   * @param key the file key.
   * @param userId the unique identifier of user.
   * @return string token.
   * @throws ClassDirectSecurityException if any padding,algorithm and invalid key exception
   *         occurs..
   */
  public static String generateS3Token(final String region, final String bucketName, final String key,
      final String userId) throws ClassDirectSecurityException {
    final S3FileToken fileToken = new S3FileToken();
    fileToken.setBucketName(bucketName);
    fileToken.setFileKey(key);
    fileToken.setRegion(region);
    final FileToken token = new FileToken();
    token.setType(DownloadTypeEnum.S3);
    token.setS3Token(fileToken);

    return convertObjectToBase64EncodedJson(token, userId);
  }

  /**
   * Generates CS10 file token.
   *
   * @param nodeId the cs10 node id.
   * @param version the cs10 file version.
   * @param userId the unique identifier of user.
   * @return string token.
   * @throws ClassDirectSecurityException when error.
   */
  public static String generateCS10Token(final Integer nodeId, final Integer version, final String userId)
      throws ClassDirectSecurityException {
    final CS10FileToken token = new CS10FileToken();
    token.setNodeId(nodeId);
    token.setVersionNumber(version);
    final FileToken fileToken = new FileToken();
    fileToken.setType(DownloadTypeEnum.CS10);
    fileToken.setCs10Token(token);

    return convertObjectToBase64EncodedJson(fileToken, userId);
  }

  /**
   * Generates CS10 file token as pdf zip.
   *
   * @param nodeId  the cs10 node id.
   * @param version the cs10 file version.
   * @param userId  the unique identifier of user.
   * @return string token.
   * @throws ClassDirectSecurityException when error.
   */
  public static String generateCS10PdfZipToken(final Integer nodeId, final Integer version, final String userId)
    throws ClassDirectSecurityException {
    final CS10FileToken token = new CS10FileToken();
    token.setNodeId(nodeId);
    token.setVersionNumber(version);
    token.setZipFormat(Boolean.TRUE);
    final FileToken fileToken = new FileToken();
    fileToken.setType(DownloadTypeEnum.CS10);
    fileToken.setCs10Token(token);

    return convertObjectToBase64EncodedJson(fileToken, userId);
  }

  /**
   * Decodes encrypted token to object.
   *
   * @param token the encrypted token string.
   * @param userId the unique identifier of user.
   * @return file token.
   * @throws ClassDirectSecurityException if any padding,algorithm and invalid key exception occurs.
   */
  public static FileToken decodeEncodedJsonToFileToken(final String token, final String userId)
      throws ClassDirectSecurityException {
    try {
      String decryptedValue = SecurityUtils.decrypt(token, userId);
      return objectMapper.readValue(decryptedValue, FileToken.class);
    } catch (IOException | InterruptedException excep) {
      LOGGER.error("Failed to decode encrypted token: {} | ErrorMessage: {}",
          new Object[] {token, excep.getMessage(), excep});
      throw new ClassDirectSecurityException("Invalid Token.");
    }

  }

  /**
   * Converts object to encrypted and encoded base64 string.
   *
   * @param token the file token object.
   * @param userId the unique identifier of user.
   * @return encoded string.
   * @throws ClassDirectSecurityException if any padding,algorithm and invalid key exception occurs.
   */
  private static String convertObjectToBase64EncodedJson(final FileToken token, final String userId)
      throws ClassDirectSecurityException {
    try {
      return SecurityUtils.encrypt(objectMapper.writeValueAsString(token), userId);
    } catch (IOException | InterruptedException exception) {
      LOGGER.error("Unable to encrypt file token: {} | ErrorMessage: {}",
          new Object[] {token, exception.getMessage(), exception});
      throw new ClassDirectSecurityException("Invalid Token.");
    }
  }
}
