package com.baesystems.ai.lr.cd.be.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import javax.activation.MimetypesFileTypeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.baesystems.ai.lr.cd.be.cs10.CS10Service;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10File;
import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import com.baesystems.ai.lr.cd.be.cs10.exception.DownloadTimeoutException;
import com.baesystems.ai.lr.cd.be.domain.dto.download.CS10FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.download.FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.download.S3FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.enums.DownloadTypeEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.awsglacieraudit.AwsGlacierAuditService;

/**
 * Provides spring REST controller for file download.
 *
 * @author Faizal Sidek
 * @author sbollu
 */
@RestController
@RequestMapping("/download-file")
public class DownloadController {

  /**
   * The logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(DownloadController.class);

  /**
   * The content encoding header name.
   */
  private static final String HEADER_CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding";

  /**
   * The content description header name.
   */
  private static final String HEADER_CONTENT_DESCRIPTION = "Content-Description";

  /**
   * The default encoding.
   */
  private static final String DEFAULT_ENCODING = "base64";

  /**
   * The glacier note.
   */
  private static final String GLACIER_NOTE_TEMPLATE = "UserID: %s; DateTime: %s; URL: %s";

  /**
   * The default vault name.
   */
  @Value("${export.archive.default.vault}")
  private String defaultVault;

  /**
   * The number of retry when an upload to glacier fails.
   */
  @Value("${export.upload.to.glacier.retries}")
  private int numberOfRetries;

  /**
   * The delay in milliseconds between retries.
   */
  @Value("${export.upload.delay.ms}")
  private int retryDelaysInMilliSecs;

  /**
   * The {@link AmazonStorageService} from Spring context.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link CS10Service} from Spring context.
   */
  @Autowired
  private CS10Service cs10Service;

  /**
   * The {@link AwsGlacierAuditService} from Spring context.
   */
  @Autowired
  private AwsGlacierAuditService awsGlacierAuditService;

  /**
   * The {@link AesSecurityService} from Spring context.
   */
  @Autowired
  private AesSecurityService aesSecurityService;
  /**
  *
  */
  @Value("${unauthorized.access.user.Masquerade}")
  private String unauthorizedAccess;

  /**
   * Performs download on object.
   *
   * @param token the string in base64 representation.
   * @param masqUserId the masquerade userId.
   * @return file binary.
   * @throws UnauthorisedAccessException when user doesn't have access to download.
   * @throws ClassDirectException when execution fails.
   */
  @RequestMapping(method = RequestMethod.GET)
  public final ResponseEntity<byte[]> performDownload(@RequestParam("token") final String token,
      @RequestParam(value = "x-masq-id", required = false) final String masqUserId)
      throws UnauthorisedAccessException, ClassDirectException {
    HttpHeaders headers = null;
    byte[] contents = null;

    final CDAuthToken userToken = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();

    boolean isAdmin = SecurityUtils.isAdmin();


    String userId = Optional.ofNullable(masqUserId).map(euid -> {
      String masqId = aesSecurityService.decryptValue(euid);
      if (isAdmin) {
        return masqId;
      } else {
        LOGGER.error("User {} doesn't have permission to masquerade ", userToken.getDetails().getUserId());
        return null;
      }
    }).orElse(userToken.getDetails().getUserId());

    if (StringUtils.isEmpty(userId)) {
      LOGGER.error("Invalid user with empty identification/User {} doesn't have permission to masquerade ",
          userToken.getDetails().getUserId());
      throw new UnauthorisedAccessException(unauthorizedAccess);
    }

    try {
      FileToken fileToken = FileTokenGenerator.decodeEncodedJsonToFileToken(token, userId);
      if (fileToken.getType() == DownloadTypeEnum.S3 && fileToken.getS3Token() != null) {
        LOGGER.debug("Downloading from S3.");
        final S3FileToken s3FileToken = fileToken.getS3Token();
        final String fileKey = s3FileToken.getFileKey();

        final String fileName = fileKey.substring(fileKey.lastIndexOf('/') + 1);
        contents = amazonStorageService.downloadFile(s3FileToken.getRegion(), s3FileToken.getBucketName(), fileKey);
        final MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
        final String contentType = mimetypesFileTypeMap.getContentType(fileName);
        headers = getHeaders(contentType, fileName, contents.length);
        storeInAmazon(defaultVault, getGlacierNotes(token), fileName, contents, token);
      } else if (fileToken.getType() == DownloadTypeEnum.CS10 && fileToken.getCs10Token() != null) {
        final CS10FileToken cs10FileToken = fileToken.getCs10Token();
        final CS10File cs10File;
        if (cs10FileToken.getVersionNumber() == null) {
          cs10File = cs10Service.getLatestFile(cs10FileToken.getNodeId(), cs10FileToken.getZipFormat());
        } else {
          cs10File = cs10Service.getFileContents(cs10FileToken.getNodeId(), cs10FileToken.getVersionNumber(),
              cs10FileToken.getZipFormat());
        }

        contents = cs10File.getContents();
        headers = getHeaders(cs10File.getMimeType(), cs10File.getFilename(), contents.length);
        storeInAmazon(defaultVault, getGlacierNotes(token), cs10File.getFilename(), contents, token);
      } else {
        throw new ClassDirectException("Invalid token.");
      }
    } catch (UnauthorisedAccessException unAuthorisedAccessException) {
      LOGGER.error("User doesn't have permission to masquerade.", unAuthorisedAccessException);
      throw unAuthorisedAccessException;
    } catch (ClassDirectSecurityException classDirectSecurityException) {
      LOGGER.error("Download interrupted due to security reasons.", classDirectSecurityException);
      throw classDirectSecurityException;
    } catch (DownloadTimeoutException cs10TimeoutException) {
      LOGGER.error("Download failed due to CS10 timed out.", cs10TimeoutException);
      throw cs10TimeoutException;
    } catch (CS10Exception cs10Exception) {
      LOGGER.error("Download failed on CS10.", cs10Exception);
      throw cs10Exception;
    }
    return new ResponseEntity<>(contents, headers, HttpStatus.OK);
  }

  /**
   * Provides OPTIONS method for CORS scenario.
   *
   * @param token the encrypted token which containing file path location.
   * @return access control headers.
   */
  @RequestMapping(method = RequestMethod.OPTIONS)
  public final ResponseEntity<String> performDownloadOptions(@RequestParam("token") final String token) {

    final HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "GET");
    headers.set(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
    headers.set(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "*");
    headers.set(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "*");

    return new ResponseEntity<>("", headers, HttpStatus.OK);
  }

  /**
   * Handles security exception and throws HTTP status 401.
   */
  @ExceptionHandler({UnauthorisedAccessException.class, ClassDirectSecurityException.class})
  @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Invalid Token.")
  public final void handleSecurityError() {
    // Will be handle by annotation to return HTTP error code 401
  }



  /**
   * Generates HTTP headers for file download.
   *
   * @param contentType the content type.
   * @param fileName the file name.
   * @param size the file size.
   * @return headers.
   */
  private HttpHeaders getHeaders(final String contentType, final String fileName, final Integer size) {
    final HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.CONTENT_TYPE, contentType);
    headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");
    headers.set(HEADER_CONTENT_TRANSFER_ENCODING, DEFAULT_ENCODING);
    headers.set(HttpHeaders.CONTENT_LENGTH, String.valueOf(size));
    headers.set(HEADER_CONTENT_DESCRIPTION, "ClassDirect file download.");

    return headers;
  }

  /**
   * Constructs glacier notes.
   *
   * @param token the token id.
   * @return notes.
   * @throws ClassDirectException if user has no authentication.
   */
  private String getGlacierNotes(final String token) throws ClassDirectException {
    final CDAuthToken authToken = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    if (authToken == null) {
      throw new ClassDirectException("Invalid auth token.");
    }
    final String currentDate = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE);

    return String.format(GLACIER_NOTE_TEMPLATE, authToken.getPrincipal(), currentDate, token);
  }

  /**
   * Provides asynchronous storage in Amazon.
   *
   * @param vaultName the Vault name.
   * @param description the Description.
   * @param fileName the file name.
   * @param contents the contents.
   * @param token the token.
   */
  private void storeInAmazon(final String vaultName, final String description, final String fileName,
      final byte[] contents, final String token) {
    final CDAuthToken userToken = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    CompletableFuture.supplyAsync(() -> {
      for (int retries = 0; retries < numberOfRetries; ++retries) {
        try {
          LOGGER.debug("Archiving to AWS Glacier");
          return amazonStorageService.archiveFile(vaultName, description, fileName, contents);
        } catch (final ClassDirectException e) {
          LOGGER.error("Error archiving to amazon: " + e);
          LOGGER.error("Retrying: #" + (retries + 1) + ", with delay of: " + retryDelaysInMilliSecs + " ms");
          try {
            Thread.sleep(retryDelaysInMilliSecs);
          } catch (final InterruptedException e1) {
            LOGGER.error("Delay interrupted: " + e1);
          }
        }
      }
      return null;
    }).thenAccept(archiveId -> {
      if (archiveId != null) {
        String userId = null;
        if (userToken != null) {
          userId = userToken.getPrincipal();
        }
        LOGGER.debug("Saving audit information to cd db");
        try {
          awsGlacierAuditService.storeArchive(archiveId, vaultName, token, LocalDateTime.now(), userId);
        } catch (final ClassDirectException e) {
          LOGGER.error("Error saving audit information to cd db: " + e);
        }
      }
    });
  }

}
