package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.certificate.CertificateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetItemHierarchyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionPageResourceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ESPReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportContentHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.SurveyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.ReportUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.export.EspExportService;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.certificates.CertificateDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyWithTaskListDto;
import com.baesystems.ai.lr.enums.CodicilCategory;
import com.baesystems.ai.lr.enums.EmployeeRole;
import com.baesystems.ai.lr.enums.ReportType;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;
import lombok.Getter;
import lombok.Setter;

/**
 * Provides service for generate ESP PDF, generate lookup for resolutionStatus,serviceCatalogue and
 * actionTaken reference data,and to extract certificate COCs,actionableItems,assetNote,surveys
 * information.
 *
 * @author yng
 * @author SBollu.
 * @author syalavarthi.
 */
@Service(value = "EspExportService")
@Loggable(Loggable.DEBUG)
public class EspExportServiceImpl extends BasePdfExport<ESPReportExportDto> implements EspExportService {
  /**
   * The Logger to display logs {@link EspExportServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(EspExportServiceImpl.class);

  /**
   * The {@link JobRetrofitService} from Spring context.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The {@link AssetRetrofitService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private CoCService cocService;

  /**
   * The {@link AssetNoteService} from Spring context.
   */
  @Autowired
  private AssetNoteService assetNoteService;

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link CertificateRetrofitService} from Spring context.
   */
  @Autowired
  private CertificateRetrofitService certificateRetrofitService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceService;

  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link JobReferenceService} from Spring context.
   */
  @Autowired
  private JobReferenceService jobReferenceService;

  /**
   * The {@link EmployeeReferenceService} from Spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;

  /**
   * The {@link TaskReferenceService} from Spring context.
   */
  @Autowired
  private TaskReferenceService taskReferenceService;

  /**
   * The {@link PortService} from Spring context.
   */
  @Autowired
  private PortService portService;

  /**
   * The report name prefix for ESP report.
   */
  private static final String ESP_PREFIX = "ESP";

  /**
   * The PUG template file name for ESP report.
   */
  private static final String ESP_TEMPLATE = "esp_export_template.jade";
  /**
   * The pug template file name for Appendix I.
   */
  private static final String APPENDIX_TASKLIST_TEMPLATE = "appendix_tasklist_export_template.jade";
  /**
   * This field is use as variable in pug template to store report object content. <br>
   * Refer to
   * {@link EspExportServiceImpl#generateFile(Map, ReportExportHDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  public static final String CONTENT = "content";

  /**
   * The directory name in Amazon S3 where the generated ESP to be uploaded. This variable in only
   * use at {@link BasePdfExport}. <br>
   * For more information see {@link EspExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "esp_export";

  /**
   * This field is use as variable in pug template that store list of surveyor. <br>
   * Refer to
   * {@link EspExportServiceImpl#generateFile(Map, ReportExportHDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String EMPLOYEE_LIST = "employeeList";

  /**
   * This field is use as variable in pug template that store issuer entity information. <br>
   * Refer to
   * {@link EspExportServiceImpl#generateFile(Map, ReportExportHDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String REPORT_ISSUE_BY = "reportIssueBy";

  /**
   * This field is use as variable in pug template that store authoriser entity information. <br>
   * Refer to
   * {@link EspExportServiceImpl#generateFile(Map, ReportExportHDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String REPORT_AUTHORISED_BY = "reportAuthorisedBy";

  /**
   * The working directory name where the report is generated. This variable in only use at
   * {@link BasePdfExport} <br>
   * For more information see {@link EspExportServiceImpl#getTempDir()}.
   */
  @Value("${esp.export.temp.dir}")
  private String tempDir;

  /**
   * The PUG template variable that store job object for ESP report generation.
   *
   */
  private static final String JOB = "job";
  /**
   * The PUG template variable that store certificate information for ESP report generation.
   */
  private static final String CERTIFICATES = "certificates";

  /**
   * The PUG template variable that store survey information for ESP report generation.
   */
  private static final String SURVEYS = "surveys";

  /**
   * The PUG template variable that store COCs information for ESP report generation.
   */
  private static final String COCS = "cocs";

  /**
   * The PUG template variable that store actionableItem information for ESP report generation.
   */
  private static final String ACTIONABLE_ITEMS = "actionableItems";

  /**
   * The PUG template variable that store assetNote information for ESP report generation.
   */
  private static final String ASSET_NOTES = "assetNotes";

  /**
   * The PUG template variable that store action taken reference data information.
   */
  private static final String ACTION_TAKEN_LOOKUP = "actionTakenLookup";

  /**
   * The PUG template variable that store certificate action reference data information.
   */
  private static final String CERTIFICATE_ACTION_LOOKUP = "certificateActionLookup";

  /**
   * The PUG template variable that store resolution status reference data information.
   */
  private static final String RESOLUTION_STATUS_LOOKUP = "resolutionStatusLookup";

  /**
   * The PUG template variable that store service catalog reference data information.
   */
  public static final String SERVICE_CATALOGUE_LOOKUP = "serviceCatalogueLookup";

  /**
   * The PUG template variable that store service catalog reference data information.
   */
  private static final String SERVICE_CREDIT_STATUS_LOOKUP = "serviceCreditStatusLookup";

  /**
   * The PUG template variable that store date information.
   */
  private static final String DATE = "date";

  /**
   * The Date formatter to format given date.
   */
  private static final String DATE_FORMATTER = "dd/MM/yyyy";

  /**
   * The defect category id for hull.
   */
  private static final Long HULL_DEFECT_CATEGORY_ID = 2L;

  /**
   * The confidentiality type id for lr internal.
   */
  private static final Long LR_INTERNAL_CONFIDENTIALITY_TYPE_ID = 1L;
  /**
   * This field is use to check if the pdf is single page or multi page.
   */
  private static final String ONLY_ONE_PAGE = "onlyOnePage";
  /**
   * This field is use as variable in pug template that store list of tasks which already group by
   * product name, and service code. <br>
   * Refer to
   * {@link EspExportServiceImpl#generateFile(Map, ESPReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String GROUPED_TASK = "groupedTask";
  /**
   * The report name prefix for Appendix I.
   */
  private static final String APPENDIX_TASKLIST_PREFIX = "APPENDIX1";
  /**
   * This field is use as variable in pug template that store the list of appendices to be populated
   * to ESp report. <br>
   * Refer to
   * {@link EspExportServiceImpl#generateFile(Map, ReportExportHDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String APPENDIX_LIST = "appendixList";

  /**
   * The Appendix I name that use to print to ESP report.
   */
  private static final String APPENDIX_TASKLIST_NAME = "Appendix 1 - Tasks";
  /**
   * The report type used in checking ESP or not in pug.
   */
  private static final String REPORT_TYPE = "reportType";
  /**
   * The report name prefix for ESP report.
   */
  private static final String ESP = "ESP";
  /**
   * The esp indicator service ids used to collect esp indicator service ids.
   */
  private static final String ESP_INDICATOR_SERVICE_IDS = "espIndicatorServiceIds";

  /**
   * This field is use as variable in pug template that store list of table of content entries for
   * Appendix I. <br>
   * Refer to
   * {@link EspExportServiceImpl#generateFile(Map, ReportExportHDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String TOC_MAPPING = "tocMapping";

  @Override
  protected final String generateFile(final Map<String, Object> model, final ESPReportExportDto query,
      final UserProfiles user, final String workingDir, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Generating ESP report...");
      final Comparator<ReportHDto> versionComparator = Comparator.comparing(ReportHDto::getReportVersion);
      // Get the corresponding FAR report (Assumption: there should only be one)
      List<ReportHDto> reports = jobRetrofitService.getReportsByJobId(query.getJobId()).execute().body();
      List<ReportHDto> fsrReports = reports.stream().filter(report -> {
        return report.getReportType().getId().equals(ReportType.FSR.value());
      }).sorted(versionComparator.reversed()).collect(Collectors.toList());

      if (fsrReports.isEmpty()) {
        LOGGER.error("Number of FSRs found for ESP generation: {}", fsrReports.size());
        throw new ClassDirectException("No FSR found for ESP generation.");
      }
      final ReportHDto fsrReport = fsrReports.get(0);

      // get current date
      final LocalDate date = LocalDate.now();
      final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
      model.put(DATE, date.format(formatter));

      // get job information => location, firstVisit, lastVisit
      final JobHDto job = jobRetrofitService.getJobsByJobId(query.getJobId()).execute().body();
      Resources.inject(job);
      model.put(JOB, job);
      // get asset model from job.
      ReportUtils.getAssetItemsModel(job.getAsset(), model, assetRetrofitService);

      // set report type related information
      String code = "";
      if (job.getJobNumber() != null && !job.getJobNumber().isEmpty()) {
        code = job.getJobNumber();
      } else {
        code = job.getId().toString();
      }
      // get report object
      final ReportDto report =
          jobRetrofitService.getSurveysByJobIdAndReportId(query.getJobId(), fsrReport.getId()).execute().body();

      // get attending surveyor list for the Job
      final List<LrEmployeeInfoContainer> lrEmployeeList = getAttendingSurveyorList(job.getEmployees());
      model.put(EMPLOYEE_LIST, lrEmployeeList);

      // get report issue by
      if (report.getIssuedBy() != null) {
        model.put(REPORT_ISSUE_BY, employeeReferenceService.getEmployee(report.getIssuedBy().getId()));
      } else {
        model.put(REPORT_ISSUE_BY, new LrEmployeeDto());
      }
      // get report authorized by
      if (report.getAuthorisedBy() != null) {
        model.put(REPORT_AUTHORISED_BY, employeeReferenceService.getEmployee(report.getAuthorisedBy().getId()));
      } else {
        model.put(REPORT_AUTHORISED_BY, new LrEmployeeDto());
      }
      // query service catalogues for lookup
      final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup = generateServiceCatalogueLookup();
      model.put(SERVICE_CATALOGUE_LOOKUP, serviceCatalogueLookup);
      // query reference data lookup
      final Map<Long, ServiceCreditStatusHDto> serviceCreditStatusLookup = generateServiceStatusLookup();
      model.put(SERVICE_CREDIT_STATUS_LOOKUP, serviceCreditStatusLookup);

      // get services of an asset
      PageResource<ScheduledServiceHDto> assetServices =
          assetService.getServices(null, null, job.getAsset().getId(), new ServiceQueryDto());
      final Optional<List<ScheduledServiceHDto>> servicesListOpt =
          Optional.ofNullable(assetServices).map(PageResource::getContent);

      // get report snapshot => AI,Survey,Certificates
      final String content = report.getContent();
      ReportContentHDto contentObj;
      HashSet<Long> espIndicatorServiceIds = null;
      if (content != null) {
        contentObj = convertJsonToObject(content);
        Resources.inject(contentObj);
        validatePortOfRegistry(contentObj);
        final List<SurveyDto> surveys = extractSurveyInfo(contentObj.getSurveys(), serviceCatalogueLookup);
        model.put(SURVEYS, surveys);
        espIndicatorServiceIds = (HashSet<Long>) surveys.stream().map(SurveyDto::getId).collect(Collectors.toSet());
        model.put(ESP_INDICATOR_SERVICE_IDS, espIndicatorServiceIds);
        filterEspIndicatorServicesOnly(servicesListOpt, contentObj, model);
        model.put(CONTENT, contentObj);

      } else {
        throw new ClassDirectException(
            "Report content is null. This may cause by MAST version of this report doesn't exist.");
      }
      // get coc information
      final List<CoCHDto> cocs = extractCocInfo(job.getAsset().getId(), job.getId());
      model.put(COCS, cocs);

      // get AN information
      final List<AssetNoteHDto> notes = extractAssetNoteInfo(job.getAsset().getId(), job.getId());
      model.put(ASSET_NOTES, notes);

      // query action taken for lookup
      final Map<Long, ActionTakenHDto> actionTakenLookup = generateActionTakenLookup();
      model.put(ACTION_TAKEN_LOOKUP, actionTakenLookup);

      // query resolution status for lookup
      final Map<Long, JobResolutionStatusHDto> resolutionStatusLookup = generateResolutionStatusLookup();
      model.put(RESOLUTION_STATUS_LOOKUP, resolutionStatusLookup);

      // file collection
      final List<String> fileList = new ArrayList<>();
      final List<String> appendixList = new ArrayList<>();

      Map<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>> groupedTask = null;
      if (espIndicatorServiceIds != null) {
        // get grouped task
        groupedTask = ReportUtils.getGroupedTask(model, serviceCatalogueLookup, taskReferenceService,
            espIndicatorServiceIds, false);
        model.put(REPORT_TYPE, ESP);
        model.put(GROUPED_TASK, groupedTask);
        // get serviceNarrativesSS
        ReportUtils.setServiceNarratives(model, serviceCatalogueLookup, taskReferenceService, espIndicatorServiceIds,
            true);
      }
      if (groupedTask != null && !groupedTask.isEmpty()) {
        // generate appendix I - create TOC mapping without page number
        Map<String, Integer> tocMappingEsp = new LinkedHashMap<>();
        for (final Entry<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>>
        map : groupedTask.entrySet()) {
          tocMappingEsp.put(map.getKey().getName(), 0);
        }
        model.put(TOC_MAPPING, tocMappingEsp);

        // generate file + toc without page number
        String appendixTasklistFile = generatePdfWithoutWatermark(APPENDIX_TASKLIST_PREFIX, code, current,
            APPENDIX_TASKLIST_TEMPLATE, workingDir, model);

        // re-create TOC mapping with page number (by looping created pdf file)
        tocMappingEsp =
            ReportUtils.generateTOCForAppendix(workingDir + File.separator + appendixTasklistFile, tocMappingEsp);
        model.put(TOC_MAPPING, tocMappingEsp);

        // generate file + toc with page number
        appendixTasklistFile = generatePdfWithoutWatermark(APPENDIX_TASKLIST_PREFIX, code, current,
            APPENDIX_TASKLIST_TEMPLATE, workingDir, model);
        fileList.add(appendixTasklistFile);
        // add appendix name in sequence
        appendixList.add(APPENDIX_TASKLIST_NAME);
        // get appendix list
        model.put(APPENDIX_LIST, appendixList);
        // add appendix name in sequence
        appendixList.add(APPENDIX_TASKLIST_NAME);
      }
      // generate report file
      String reportFile =
          generatePdfWithoutWatermark(ESP_PREFIX, job.getId().toString(), current, ESP_TEMPLATE, workingDir, model);

      int pageCountForESP = ServiceUtils.getPageCount(workingDir, reportFile);
      if (pageCountForESP == 1) {
        model.put(ONLY_ONE_PAGE, "true");
        reportFile =
            generatePdfWithoutWatermark(ESP_PREFIX, job.getId().toString(), current, ESP_TEMPLATE, workingDir, model);
      }
      fileList.add(reportFile);
      // merge pdf file
      Collections.reverse(fileList);
      final String returnFileName = packageFile(fileList, workingDir, code, current, ESP);
      return returnFileName;
    } catch (final IOException | JadeException | ParserConfigurationException | SAXException
        | DocumentException exception) {
      throw new ClassDirectException(exception);
    }
  }

  /**
   * Filters ESP indicator services only from asset services and adds certificates and actionable
   * items to report model.
   *
   * @param servicesListOpt the optional asset services list.
   * @param contentObj the generated content json object from job.
   * @param model the model used in pug.
   * @throws IOException if mast api fails.
   */
  private void filterEspIndicatorServicesOnly(final Optional<List<ScheduledServiceHDto>> servicesListOpt,
      final ReportContentHDto contentObj, final Map<String, Object> model) throws IOException {

    if (servicesListOpt.isPresent()) {
      // filters only esp indicator service ids from asset services.
      HashSet<Long> espIndicatorServiceIds = servicesListOpt.get().stream().filter(dto -> {
        boolean isespId = Optional.ofNullable(dto.getServiceCatalogueH()).map(ServiceCatalogueHDto::getEspIndicator)
            .filter(ei -> ei.equals(Boolean.TRUE)).orElse(false);
        return isespId;
      }).map(ScheduledServiceHDto::getId).collect(Collectors.toCollection(HashSet::new));

      // get certificate information
      final List<CertificateDto> certificates = Optional.ofNullable(contentObj.getCertificates())
          .map(certList -> extractCertificateInfo(certList, espIndicatorServiceIds)).orElse(new ArrayList<>());
      model.put(CERTIFICATES, certificates);

      // query certificate action for lookup
      final Map<Long, CertificateActionDto> certificateActionLookup = extractCertificateActionInfo(certificates);
      model.put(CERTIFICATE_ACTION_LOOKUP, certificateActionLookup);

      final List<ActionableItemDto> items = Optional.ofNullable(contentObj.getWipActionableItems())
          .map(itemList -> extractActionableItemInfo(itemList, espIndicatorServiceIds)).orElse(new ArrayList<>());
      model.put(ACTIONABLE_ITEMS, items);
    }


  }

  /**
   * Packages the file into single PDF if more than one pdf is generated. <br>
   * Each report and appendices is generated in separate pdf. This method will combine all pdf into
   * one.
   *
   * @param filesList the list of file and appendices.
   * @param workingDir the working directory where all generated pdf is located.
   * @param code the code for file section report name.
   * @param current the timestamp when report generation is triggered.
   * @param prefix the file prefix name.
   * @return file the file name.
   * @throws FileNotFoundException if corresponding file not found.
   * @throws IOException if file fail to read or write.
   * @throws ClassDirectException if the file list is empty.
   */
  public final String packageFile(final List<String> filesList, final String workingDir, final String code,
      final LocalDateTime current, final String prefix)
      throws FileNotFoundException, IOException, ClassDirectException {
    if (filesList.isEmpty()) {
      throw new ClassDirectException("Error: No file generated.");
    }

    final String filename = filesList.get(0);

    if (filesList.size() > 1) {

      // merge pdf files
      final PDFMergerUtility merger = new PDFMergerUtility();

      for (final String pdf : filesList) {
        merger.addSource(new File(workingDir + File.separator + pdf));
      }

      merger.setDestinationFileName(workingDir + File.separator + filename);
      merger.mergeDocuments(null);
    }

    return filename;
  }


  /**
   * Extracts COCs filtered by jobId and defect with hull category.
   *
   * @param assetId the unique identifier for asset.
   * @param jobId the unique identifier for job.
   * @return filtered COCs.
   * @throws ClassDirectException if API call fails.
   */
  public final List<CoCHDto> extractCocInfo(final Long assetId, final Long jobId) throws ClassDirectException {

    // get list of coc by asset id.
    final PageResource<CoCHDto> unfilteredCocsResource = cocService.getCoC(null, null, assetId);
    final List<CoCHDto> unfilteredCocs = unfilteredCocsResource.getContent();

    // filter the list with only raisedOnJob or closedOnJob match jobId
    // then, filter if the coc has defect of category Hull
    final List<CoCHDto> cocs = unfilteredCocs.stream().filter(coc -> {

      boolean filterFlag = false;
      final Long raisedOnJobId = Optional.ofNullable(coc.getRaisedOnJob()).map(LinkResource::getId).orElse(null);
      final Long closedOnJobId = Optional.ofNullable(coc.getClosedOnJob()).map(LinkResource::getId).orElse(null);

      if (jobId.equals(raisedOnJobId) || jobId.equals(closedOnJobId)) {
        filterFlag = true;
      }

      return filterFlag;
    }).filter(coc -> {

      boolean filterFlag = false;
      final Long defectId = Optional.ofNullable(coc.getDefect()).map(LinkResource::getId).orElse(null);

      try {
        final DefectHDto defect = assetRetrofitService.getAssetDefectDto(assetId, defectId).execute().body();

        if (defect.getCategory().getId().equals(HULL_DEFECT_CATEGORY_ID)) {
          filterFlag = true;
        }
      } catch (final Exception exception) {
        LOGGER.info("Failed to query defect. FilterFlag = {}", filterFlag);
      }

      return filterFlag;
    }).collect(Collectors.toList());

    return cocs;
  }

  /**
   * Returns list of actionableItems whose service has ESP indicator .
   *
   * @param items actionableItem list.
   * @param espIndicatorServiceIds list of id's whose service has ESP indicator.
   * @return filtered actionableItem List.
   */
  public final List<ActionableItemDto> extractActionableItemInfo(final List<ActionableItemDto> items,
      final HashSet<Long> espIndicatorServiceIds) {
    List<ActionableItemDto> filteredItems = items.stream().filter(hdto -> {
      // filter for EspServices
      boolean esp = Optional.ofNullable(hdto.getScheduledServices()).map(scdto -> hdto.getScheduledServices()
          .parallelStream().map(LinkResource::getId).anyMatch(espIndicatorServiceIds::contains)).orElse(false);
      return esp;
    }).collect(Collectors.toList());

    return filteredItems;
  }

  /**
   * Extracts asset note information for given asset and filtered by given jobId.
   *
   * @param assetId the unique identifier for asset..
   * @param jobId the unique identifier for job.
   * @return filtered asset notes.
   * @throws ClassDirectException if API call fails.
   */
  public final List<AssetNoteHDto> extractAssetNoteInfo(final Long assetId, final Long jobId)
      throws ClassDirectException {

    // get list of asset note by asset id.
    final PageResource<AssetNoteHDto> unfilteredNotesResource = assetNoteService.getAssetNote(null, null, assetId);
    final List<AssetNoteHDto> unfilteredNotes = unfilteredNotesResource.getContent();

    // filter the list with only raisedOnJob or closedOnJob match jobId
    // then, filter if the AN has category = class, and confidentialityType != LR Internal
    final List<AssetNoteHDto> notes = unfilteredNotes.stream().filter(note -> {

      boolean filterFlag = false;
      final Long raisedOnJobId = Optional.ofNullable(note.getRaisedOnJob()).map(LinkResource::getId).orElse(null);
      final Long closedOnJobId = Optional.ofNullable(note.getClosedOnJob()).map(LinkResource::getId).orElse(null);

      if (jobId.equals(raisedOnJobId) || jobId.equals(closedOnJobId)) {
        filterFlag = true;
      }

      return filterFlag;
    }).filter(note -> {

      boolean filterFlag = false;

      final Long categoryId = Optional.ofNullable(note.getCategory()).map(LinkResource::getId).orElse(null);
      final Long confidentialityTypeId =
          Optional.ofNullable(note.getConfidentialityType()).map(LinkResource::getId).orElse(null);

      if (CodicilCategory.AN_CLASS.getValue().equals(categoryId)
          && !LR_INTERNAL_CONFIDENTIALITY_TYPE_ID.equals(confidentialityTypeId)) {
        filterFlag = true;
      }

      return filterFlag;
    }).collect(Collectors.toList());

    return notes;
  }

  /**
   * Validates port of registry and gets port of registry from {@link PortService#getPort(long)} and
   * sets to content.
   *
   * @param reportContentDto the report content model object.
   * @return valid reportContentDto object.
   */
  private ReportContentHDto validatePortOfRegistry(final ReportContentHDto reportContentDto) {

    ReportContentHDto reportContentObj = reportContentDto;
    if (reportContentObj.getRegisteredPort() != null) {
      try {
        PortOfRegistryDto port = portService.getPort(reportContentObj.getRegisteredPort().getId());
        Optional.ofNullable(port).ifPresent(portDetails -> {
          reportContentObj.setRegisteredPortH(portDetails);
        });
      } catch (final RecordNotFoundException exception) {
        LOGGER.error("Record not found for port of registry {}", reportContentObj.getRegisteredPort().getId(),
            exception);
      }
    }
    return reportContentObj;
  }

  /**
   * Returns list of certificates whose service has ESP indicator.
   *
   * @param certificates certificate list.
   * @param espIndicatorServiceIds list of id's whose service has ESP indicator.
   * @return list of Certificates with ESP indicator true.
   */
  public final List<CertificateDto> extractCertificateInfo(final List<CertificateDto> certificates,
      final HashSet<Long> espIndicatorServiceIds) {

    List<CertificateDto> filteredCertificates = certificates.stream().filter(hdto -> {

      boolean isEspService = Optional.ofNullable(hdto.getService()).map(LinkResource::getId)
          .map(espIndicatorServiceIds::contains).orElse(false);
      return isEspService;
    }).collect(Collectors.toList());

    return filteredCertificates;
  }

  /**
   * Returns surveys whose service has ESP indicator.
   *
   * @param surveys the survey list.
   * @param serviceCatalogueLookup the serviceCatalogueLookup has reference data information of
   *        serviceCatalogue.
   * @return filtered list of surveys.
   */
  public final List<SurveyDto> extractSurveyInfo(final List<SurveyWithTaskListDto> surveys,
      final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup) {
    List<SurveyDto> filteredSurveys = new ArrayList<>();
    if (Optional.ofNullable(surveys).isPresent()) {
      filteredSurveys = surveys.stream().filter(dto -> {
        boolean isESPSurvey = Optional.ofNullable(dto.getServiceCatalogue()).map(LinkResource::getId)
            .map(scId -> serviceCatalogueLookup.get(scId)).map(ServiceCatalogueHDto::getEspIndicator).orElse(false);
        return isESPSurvey;
      }).collect(Collectors.toList());
      // LRCD-3197 surveys should sort by ervice catalogue display order.
      filteredSurveys.sort((s1, s2) -> {
        int compareRetVal = serviceCatalogueLookup.get(s1.getServiceCatalogue().getId()).getDisplayOrder()
            .compareTo(serviceCatalogueLookup.get(s2.getServiceCatalogue().getId()).getDisplayOrder());
        return compareRetVal;
      });
    }
    return filteredSurveys;
  }

  /**
   * Extracts certificate action information from given list of certificates.
   *
   * @param certificates the list of certificates to get certificate action info.
   * @return certificate action list.
   * @throws IOException if API call fails.
   */
  public final Map<Long, CertificateActionDto> extractCertificateActionInfo(final List<CertificateDto> certificates)
      throws IOException {
    final Map<Long, CertificateActionDto> certActionLookup = new TreeMap<>();

    for (final CertificateDto cert : certificates) {
      final CertificateActionPageResourceHDto resource =
          certificateRetrofitService.certificateActionQuery(cert.getId()).execute().body();

      final List<CertificateActionHDto> certificateActions = resource.getContent();

      if (!certificateActions.isEmpty()) {
        certActionLookup.put(cert.getId(), certificateActions.get(0));
      }
    }

    return certActionLookup;
  }

  /**
   * Generates lookup.
   *
   * @return return lookup.
   */
  public final Map<Long, ActionTakenHDto> generateActionTakenLookup() {
    final List<ActionTakenHDto> actionTakenList = assetReferenceService.getActionTakenList();
    final Map<Long, ActionTakenHDto> actionTakenLookup =
        actionTakenList.stream().collect(Collectors.toMap(ActionTakenHDto::getId, Function.identity()));
    return actionTakenLookup;
  }

  /**
   * Generates lookup for ServiceCatalogue.
   *
   * @return return lookup.
   */
  public final Map<Long, ServiceCatalogueHDto> generateServiceCatalogueLookup() {
    final List<ServiceCatalogueHDto> serviceCatalogueList = serviceService.getServiceCatalogues();
    final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup =
        serviceCatalogueList.stream().collect(Collectors.toMap(ServiceCatalogueHDto::getId, Function.identity()));
    return serviceCatalogueLookup;
  }

  /**
   * Generates lookup for ResolutionStatus.
   *
   * @return return lookup.
   */
  public final Map<Long, JobResolutionStatusHDto> generateResolutionStatusLookup() {
    final List<JobResolutionStatusHDto> resolutionStatusList = jobReferenceService.getResolutionStatuses();
    final Map<Long, JobResolutionStatusHDto> resolutionStatusLookup =
        resolutionStatusList.stream().collect(Collectors.toMap(JobResolutionStatusHDto::getId, Function.identity()));
    return resolutionStatusLookup;
  }

  /**
   * Generates reference data service status for mapping in pug file.
   *
   * @return the service status lookup object.
   */
  public final Map<Long, ServiceCreditStatusHDto> generateServiceStatusLookup() {
    final List<ServiceCreditStatusHDto> serviceCreditStatusRefList = serviceService.getServiceCreditStatuses();
    final Map<Long, ServiceCreditStatusHDto> serviceCreditStatusLookup = serviceCreditStatusRefList.stream()
        .collect(Collectors.toMap(ServiceCreditStatusHDto::getId, Function.identity()));
    return serviceCreditStatusLookup;
  }

  /**
   * converts report content JSON string to object.
   *
   * @param content the JSON string.
   * @return JSON object.
   * @throws JsonParseException if JSON parsing fails.
   * @throws JsonMappingException if JSON mapping fails.
   * @throws IOException if any IO exception occurs.
   */
  public final ReportContentHDto convertJsonToObject(final String content)
      throws JsonParseException, JsonMappingException, IOException {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper.readValue(content, ReportContentHDto.class);
  }

  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return this.tempDir;
  }

  /**
   * Generates attending surveyor information. <br>
   * Attending surveyor is a list of employee under job object and filtered by surveyor role.
   *
   * @param lrEmployees the list of employee.
   * @return list of employee filtered by surveyor.
   */
  public final List<LrEmployeeInfoContainer> getAttendingSurveyorList(final List<EmployeeLinkDto> lrEmployees) {
    final List<LrEmployeeInfoContainer> lrEmployeeList = new ArrayList<>();
    if (lrEmployees != null) {
      lrEmployees.stream()
          .filter(employee -> employee.getEmployeeRole().getId().equals(EmployeeRole.AUTHORISING_SURVEYOR.getValue())
              || employee.getEmployeeRole().getId().equals(EmployeeRole.EIC_CASE_SURVEYOR.getValue())
              || employee.getEmployeeRole().getId().equals(EmployeeRole.LEAD_SURVEYOR.getValue()))
          .forEach(lrEmployee -> {
            try {
              final LrEmployeeInfoContainer container = new LrEmployeeInfoContainer();
              container.setEmployeeLink(lrEmployee);

              final LrEmployeeDto employeeDetail =
                  employeeReferenceService.getEmployee(lrEmployee.getLrEmployee().getId());
              if (employeeDetail != null) {
                container.setEmployee(employeeDetail);
              }

              lrEmployeeList.add(container);
            } catch (final Exception e) {
              LOGGER.info("No employee detail found.");
            }
          });
    }
    return lrEmployeeList;
  }

  /**
   * Provides container class for employee related object field. This container collect both MAST
   * employee object {@link EmployeeLinkDto} and {@link LrEmployeeDto} for data printing in pug
   * template.
   *
   * @author yng
   *
   */
  public static final class LrEmployeeInfoContainer {

    /**
     * Contain role information.
     */
    @Getter
    @Setter
    private EmployeeLinkDto employeeLink;

    /**
     * Contain employee name information.
     */
    @Getter
    @Setter
    private LrEmployeeDto employee;
  }

}
