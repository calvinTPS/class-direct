package com.baesystems.ai.lr.cd.be.service.security;

import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

import java.util.Set;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * This aspect will check for {@link com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset}.
 * If method is annotated with that annotation, this aspect will check and assert access control
 * policy.
 *
 * @author Faizal Sidek
 */
@Aspect
// Added order to precedence the @Cacheable
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Component
public class RestrictedAssetAspect {

  /**
   * Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(RestrictedAssetAspect.class);

  /**
   * Injected subfleet service.
   */
  @Autowired
  private SubFleetService subFleetService;

  /**
   * Injected profile service.
   */
  @Autowired
  private UserProfileService profileService;

  /**
   * Intercept method with @{@link RestrictedAsset} annotation.
   *
   * @param joinPoint proceeding join point.
   * @param restrictedAsset annotation.
   * @return modified result.
   * @throws Throwable when error.
   */
  @Around("@annotation(restrictedAsset)")
  public Object checkAccessibleAsset(final ProceedingJoinPoint joinPoint, final RestrictedAsset restrictedAsset)
      throws Throwable {
    Object retVal = joinPoint.proceed();
    final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    final Method method = signature.getMethod();
    LOGGER.debug("restrictedAsset calling method {} ", method.getName());
    final Class<?> returnType = method.getReturnType();

    Long assetId = null;
    final Annotation[][] allAnnotations = joinPoint.getTarget().getClass()
        .getMethod(method.getName(), method.getParameterTypes()).getParameterAnnotations();
    for (int i = 0; i < joinPoint.getArgs().length; i++) {
      final Annotation[] annotations = allAnnotations[i];
      for (final Annotation annotation : annotations) {
        if (annotation.annotationType().equals(AssetID.class)) {
          assetId = (Long) joinPoint.getArgs()[i];
        }
      }
    }

    LOGGER.debug("Intercepting method {} ", method.getName());
    if (AssetPageResource.class.isAssignableFrom(returnType)) {
      LOGGER.debug("Handling asset page resource.");
      final AssetPageResource assets = (AssetPageResource) retVal;
      checkAssetListAndPerformAccessControl(assets.getContent());
    } else if (AssetHDto.class.isAssignableFrom(returnType)) {
      LOGGER.debug("Handling single asset.");
      checkAssetListAndPerformAccessControl(Collections.singletonList((AssetHDto) retVal));
    } else if (assetId != null && !isAssetAccessible(assetId)) {
      LOGGER.debug("Nullify return type {} ", returnType);
      retVal = nullifyReturnValue(returnType);
    } else {
      LOGGER.debug("Skip invocation since return type {} doesn't match anything", returnType);
    }
    return retVal;
  }

  /**
   * Nullify return value, if return type is collection, make collection as empty list.
   *
   * @param returnType type of return value.
   * @return null or empty object.
   */
  private Object nullifyReturnValue(final Class<?> returnType) {
    Object returnValue = null;
    if (List.class.isAssignableFrom(returnType)) {
      returnValue = Collections.emptyList();
    }

    if (returnType.isAssignableFrom(BasePageResource.class)) {
      returnValue = PaginationUtil.paginate(Collections.emptyList(), null, null);
    }

    if (returnType.isAssignableFrom(CodicilActionableHDto.class)) {
      final CodicilActionableHDto codicils = new CodicilActionableHDto();
      codicils.setActionableItemHDtos(Collections.emptyList());
      codicils.setCocHDtos(Collections.emptyList());
      returnValue = codicils;
    }

    return returnValue;
  }

  /**
   * Check if asset is accessible.
   *
   * @param assetId asset id.
   * @return accessible flag.
   */
  private Boolean isAssetAccessible(final Long assetId) {
    Boolean accessible;
    try {
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();

      if (token != null) {
        final List<Long> accessibleAssetIds = subFleetService.getSubFleetById(token.getPrincipal());
        if (accessibleAssetIds.isEmpty()) {
          accessible = true;
        } else {
          accessible = accessibleAssetIds.contains(assetId);
        }
      } else {
        accessible = false;
      }
    } catch (final ClassDirectException exception) {
      LOGGER.debug("Error accessing asset.", exception);
      accessible = false;
    }

    return accessible;
  }

  /**
   * Check against subfleet or user client id.
   *
   * @param assets asset list.
   */
  private void checkAssetListAndPerformAccessControl(final List<AssetHDto> assets) {
    try {
      LOGGER.debug("Performing access control on assets {} ", assets);
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      if (token != null) {
        if (assets.parallelStream().anyMatch(asset -> asset.getIsEOR().equals(Boolean.TRUE))) {
          LOGGER.debug("Asset is an EOR asset.");
          final Set<String> eorList = profileService.getEorsByUserId(token.getPrincipal());
          if (!assets.stream().filter(asset -> {
            final String imo = AssetCodeUtil.getAssetImo(asset);
            return eorList.contains(imo);
          }).findAny().isPresent()) {
            throw new AccessDeniedException("User has no access to this asset.");
          }
        } else if (checkEquasisThetisAccess(token)) {
          if (!assets.stream().filter(asset -> asset.getId().equals(token.getEquasisThetisAccessibleAssetId()))
              .findAny().isPresent()) {
            throw new AccessDeniedException("User has no access to this asset.");
          }
        } else {
          final List<Long> accessibleAssetIds = subFleetService.getSubFleetById(token.getPrincipal());
          if (!accessibleAssetIds.isEmpty()) {
            assets.parallelStream().filter(asset -> !accessibleAssetIds.contains(asset.getId())
                && asset.getCode().startsWith(CDConstants.MAST_PREFIX)).forEach(this::nullifyAsset);
          } else {
            final UserProfiles currentuser = profileService.getUser(token.getPrincipal());
            if (currentuser.getRestrictAll() != null && currentuser.getRestrictAll().equals(Boolean.TRUE)) {
              assets.parallelStream().filter(asset -> asset.getCode().startsWith(CDConstants.MAST_PREFIX))
                  .forEach(this::nullifyAsset);
            } else {
              LOGGER.debug("Asset is not restricted.");
            }
          }
        }
      }
    } catch (final ClassDirectException exception) {
      LOGGER.debug("Record not found for user.", exception);
    }
  }

  /**
   * Null or set restricted to asset.
   *
   * @param asset asset dto.
   */
  private void nullifyAsset(final AssetHDto asset) {
    asset.setRestricted(true);
  }

  /**
   * Check if user is equasis thetis.
   *
   * @param token auth token.
   * @return flag.
   */
  private boolean checkEquasisThetisAccess(final CDAuthToken token) {
    return token.getAuthorities().stream().filter(authority -> authority.getAuthority().equals(Role.EQUASIS.toString())
        || authority.getAuthority().equals(Role.THETIS.toString())).findAny().isPresent();
  }

}
