package com.baesystems.ai.lr.cd.service.export.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * Node for tree construct.
 *
 * @author yng.
 *
 * @param <T> data type.
 */
public class Node<T> {

  /**
   * Node data.
   */
  private T data = null;

  /**
   * Node children list.
   */
  private final List<Node<T>> children = new ArrayList<>();

  /**
   * Node parent data.
   */
  private Node<T> parent = null;

  /**
   * Constructor.
   *
   * @param dataObj node data.
   */
  public Node(final T dataObj) {
    this.data = dataObj;
  }

  /**
   * Add single child node.
   *
   * @param childObj node.
   */
  public final void addChild(final Node<T> childObj) {
    childObj.setParent(this);
    this.children.add(childObj);
  }

  /**
   * Add single child data.
   *
   * @param dataObj child.
   */
  public final void addChild(final T dataObj) {
    final Node<T> newChild = new Node<>(dataObj);
    newChild.setParent(this);
    children.add(newChild);
  }

  /**
   * Add list of child node.
   *
   * @param childrenObj list.
   */
  public final void addChildren(final List<Node<T>> childrenObj) {
    for (final Node<T> t : childrenObj) {
      t.setParent(this);
    }
    this.children.addAll(childrenObj);
  }

  /**
   * Get list of child node.
   *
   * @return children list.
   */
  public final List<Node<T>> getChildren() {
    return children;
  }

  /**
   * Getter for data.
   *
   * @return data object.
   */
  public final T getData() {
    return data;
  }

  /**
   * Setter for data.
   *
   * @param dataObj object.
   */
  public final void setData(final T dataObj) {
    this.data = dataObj;
  }

  /**
   * Getter for parent.
   *
   * @param parentObj object.
   */
  public final void setParent(final Node<T> parentObj) {
    this.parent = parentObj;
  }

  /**
   * Setter for parent.
   *
   * @return parent object.
   */
  public final Node<T> getParent() {
    return parent;
  }
}
