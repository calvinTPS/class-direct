package com.baesystems.ai.lr.cd.service.reference.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.jcabi.aspects.Loggable;


/**
 * Provides service to get flag details by id, user flag details and list of all flag details.
 *
 * @author msidek
 * @author syalavarthi 23-05-2017.
 *
 */
@Service(value = "FlagStateService")
@Loggable(Loggable.DEBUG)
public class FlagStateServiceImpl implements FlagStateService {
  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Injects {@link FlagsAssociationsService}.
   */
  @Autowired
  private FlagsAssociationsService flagsAssociationsService;


  /**
   * Injects {@link AssetReferenceService}.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;
  /**
   * The maximum page size.
   */
  @Value("${flagstate.service.assets.maxpagesize}")
  private Integer maximumPageSize;

  /**
   * The application logger for FlagStateServiceImpl.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FlagStateServiceImpl.class);

  @ResourceProvider
  @Override
  public final FlagStateHDto getFlagById(final Long id) throws ClassDirectException {
    final FlagStateHDto flag = assetReferenceService.getFlagState(id);
    return flag;
  }

  @Override
  public final List<FlagStateHDto> getUserFlagStates(final String userId) throws IOException, ClassDirectException {
    List<FlagStateHDto> flagStateHDtos = new ArrayList<>();
    if (userId != null) {
      final UserProfiles user = userProfileService.getUser(userId);
      boolean isFlagRole = user.getRoles().stream().filter(role -> {
        return role.getRoleName().equals(Role.FLAG.name());
      }).findFirst().isPresent();
       if (isFlagRole) {
        flagStateHDtos = getFlagStates().stream().filter(flag -> flag.getFlagCode().equals(user.getFlagCode()))
            .collect(Collectors.toList());
        // LRCD-2623 - include the secondary / primary flag for user filter
        final List<FlagStateHDto> extraFlagList = new ArrayList<>();
        flagStateHDtos.forEach(flagState -> {
          List<String> extraFlagCodeList;
          try {
            extraFlagCodeList = flagsAssociationsService.getAssociatedFlags(flagState.getFlagCode());
            extraFlagCodeList.stream().forEach(flagCode -> {
              final FlagStateHDto extraFlag = getFlagStateByCode(flagCode);
              Optional.ofNullable(extraFlag).ifPresent(extraFlagList::add);
            });
          } catch (final ClassDirectException e) {
            LOGGER.error("Fail to fetch associated flag for flagCode: {}. | Error Message : {}",
                flagState.getFlagCode(), e.getMessage());
          }
        });
        flagStateHDtos.addAll(extraFlagList);
      } else {
        flagStateHDtos = getFlagStates();
      }
    }
    return flagStateHDtos;
  }

  /**
   * Returns the flag based on the given flag code.
   *
   * @param flagCode the flag code.
   * @return the flag model if found otherwise null.
   */
  private FlagStateHDto getFlagStateByCode(final String flagCode) {
    FlagStateHDto result = null;
    final List<FlagStateHDto> flags;
    try {
      flags = getFlagStates();
      if (flags != null) {
        result = flags.stream().filter(flag -> flagCode.equalsIgnoreCase(flag.getFlagCode())).findFirst()
            .orElse(null);
      }

    } catch (ClassDirectException e) {
      LOGGER.error("Fail to fetch flags with flag code: {}. | Error Message : {}", flagCode, e.getMessage());
    }
    return result;
  }

  /**
   * Returns all the active flag (excluded the deleted flag == true).
   */
  @Override
  public final List<FlagStateHDto> getFlagStates() throws ClassDirectException {
    List<FlagStateHDto> flags = null;
    LOGGER.debug("Finding flags from MAST.");
    flags = assetReferenceService.getFlagStates();
    if (flags != null) {
      flags = flags.stream().filter(flag -> flag != null && flag.getDeleted().equals(Boolean.FALSE))
          .collect(Collectors.toList());
    }
    return flags;
  }
}
