package com.baesystems.ai.lr.cd.be.service.security.impl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.domain.dto.security.bean.CookieBean;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;
import com.baesystems.ai.lr.cd.be.service.security.ClassDirectSecurityService;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;

/**
 * Provides class-direct security by implementing MD5 encryption and services to get invalid input
 * parameters and cookie configuration.
 *
 * @author RKaneysan
 * @version 1.0
 * @since 2016-09-15
 * @author syalavarthi 21-06-2017.
 */
@Service(value = "ClassDirectSecurityService")
public class ClassDirectSecurityServiceImpl implements ClassDirectSecurityService {

  /**
   * The logger for ClassDirectSecurityServiceImpl {@link ClassDirectSecurityService}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ClassDirectSecurityServiceImpl.class);

  /**
   * The date pattern for date formatting.
   */
  private static final String DATE_PATTERN = "yyyyMMddHHmmss";

  /**
   * The invalid input parameter/s error message.
   */
  @Value("${security.invalid.input.params}")
  private String invalidInputParameters;

  /**
   * The empty shared key error message.
   */
  @Value("${security.empty.shared.key}")
  private String sharedKeyEmpty;

  /**
   * The invalid hash value error message.
   */
  @Value("${security.invalid.hash}")
  private String hashInvalidErrorMessage;

  /**
   * The exceeded expiration period Error Message.
   */
  @Value("${security.exceeded.expiration.period}")
  private String expirationPeriodExceeded;

  /**
   * The exceeded expiration period error message.
   */
  @Value("${security.invalid.date.time.input}")
  private String invalidTimeInput;

  /**
   * The invalid timestamp format error message.
   */
  @Value("${security.invalid.time.format}")
  private String invalidTimeFormat;

  /**
   * The expiration period for equasis.
   */
  @Value("${expiration.period.equasis.seconds}")
  private String expirationPeriodEquasis;

  /**
   * The equasis shared key.
   */
  @Value("${equasis.shared.key}")
  private String equasisSharedKey;

  /**
   * The domain for cookie.
   */
  @Value("${cookie.domain}")
  private String cookieDomain;

  /**
   * The cookie for Http Only.
   */
  @Value("${cookie.isHttpOnly}")
  private boolean isHttpOnly;

  /**
   * The maximum age for cookie.
   */
  @Value("${cookie.maxAge}")
  private int cookieMaxAge;

  /**
   * The path for cookie.
   */
  @Value("${cookie.path}")
  private String cookiePath;

  /**
   * The secure cookie.
   */
  @Value("${cookie.secure}")
  private boolean isSecure;

  /**
   * The comments for cookie.
   */
  @Value("${cookie.comments}")
  private String cookieComments;

  /**
   * The version for cookie.
   */
  @Value("${cookie.version}")
  private String cookieVersion;

  /**
   * The french time zone id.
   */
  private static final String FRENCH_TIME_ZONE = "Europe/Paris";

  @Override
  public final void authenticateHash(final String imoNumber, final String time, final String actualRequestHash)
      throws ClassDirectSecurityException {
    final String expectedHash = SecurityUtils.getMessageDigestEncyption(imoNumber + time + equasisSharedKey);

    if (!expectedHash.trim().equals(actualRequestHash)) {
      LOGGER.debug("Expected Hash : {}, Actual Hash : {}", expectedHash, actualRequestHash);
      throw new ClassDirectSecurityException(hashInvalidErrorMessage);
    }
  }

  @Override
  public final void validateTime(final String time) throws ClassDirectSecurityException {
    /**
     * Validate Time if it follows timestamp format and not exceeded expiration period.
     */
    final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(DATE_PATTERN);
    String errorMsg = "";

    try {
      final LocalDateTime requestDate = LocalDateTime.parse(time, dateFormat);
      final LocalDateTime currentDate = ZonedDateTime.now(ZoneId.of(FRENCH_TIME_ZONE)).toLocalDateTime();
      final Long expirationPeriod = Long.valueOf(expirationPeriodEquasis);
      // Compute time elapsed in seconds.
      final Long timeElapsed = ChronoUnit.SECONDS.between(requestDate, currentDate);

      LOGGER.debug("Expiration Period : {}, Time Elapsed : {}", expirationPeriod, timeElapsed);

      if (timeElapsed < 0) {
        errorMsg = invalidTimeInput;
      } else if (timeElapsed > expirationPeriod) {
        errorMsg = expirationPeriodExceeded;
      }
    } catch (DateTimeParseException parseException) {
      errorMsg = invalidTimeFormat;
    }

    if (!StringUtils.isEmpty(errorMsg)) {
      throw new ClassDirectSecurityException(errorMsg);
    }
  }

  /**
   * @return the invalidInputParameters.
   */
  @Override
  public final String getInvalidInputParameters() {
    return invalidInputParameters;
  }

  /**
   * return Cookie config.
   */
  @Override
  public final CookieBean getCookieConfig() {
    final CookieBean cookieBean = new CookieBean();
    cookieBean.setDomain(cookieDomain);
    cookieBean.setHttpOnly(isHttpOnly);
    cookieBean.setMaxAge(cookieMaxAge);
    cookieBean.setPath(cookiePath);
    cookieBean.setSecure(isSecure);
    cookieBean.setComments(cookieComments);
    cookieBean.setVersion(Integer.parseInt(cookieVersion.trim()));
    return cookieBean;
  }
}
