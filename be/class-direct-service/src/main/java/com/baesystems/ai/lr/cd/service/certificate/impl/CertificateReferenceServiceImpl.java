package com.baesystems.ai.lr.cd.service.certificate.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.CertificateReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateTemplateHDto;
import com.baesystems.ai.lr.cd.service.certificate.CertificateReferenceService;
import com.baesystems.ai.lr.dto.references.CertificateActionTakenDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

import retrofit2.Call;

/**
 * Created by fwijaya on 24/1/2017.
 */
@Service("CertificateReferenceService")
public class CertificateReferenceServiceImpl implements CertificateReferenceService, InitializingBean {
  /**
   * Log4j logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CertificateReferenceServiceImpl.class);
  /**
   * log message.
   */
  private static final String LOG_MESSAGE = "Can't find any element matched id {}.";

  /**
   * Injected {@link AssetRetrofitService}.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;
  /**
   * Retrofit service delegate.
   */
  @Autowired
  private CertificateReferenceRetrofitService certReferenceDelegate;
  /**
   * Spring context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   */
  private CertificateReferenceService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(CertificateReferenceService.class);
  }

  /**
   * Get certificate templates.
   *
   * @return A list of {@link CertificateTemplateHDto}.
   */
  @ResourceProvider
  @Override
  public final List<CertificateTemplateHDto> getCertificateTemplates() {
    List<CertificateTemplateHDto> certificateTemplates = new ArrayList<>();
    try {
      final Call<List<CertificateTemplateHDto>> caller = certReferenceDelegate.getCertificateTemplates();
      certificateTemplates = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return certificateTemplates;
  }

  /**
   * Get certificate template.
   *
   * @param id id of the template.
   * @return {@link CertificateTemplateHDto}.
   */
  @ResourceProvider
  @Override
  public final CertificateTemplateHDto getCertificateTemplate(final Long id) {
    CertificateTemplateHDto certificateTemplate = null;
    try {
      final List<CertificateTemplateHDto> certificateTemplates = self.getCertificateTemplates();
      certificateTemplate =
          certificateTemplates.stream().filter(template -> template.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, id);
    }
    return certificateTemplate;
  }

  /**
   * Get certificate types.
   *
   * @return A list of {@link CertificateTypeDto}.
   */
  @ResourceProvider
  @Override
  public final List<CertificateTypeDto> getCertificateTypes() {
    List<CertificateTypeDto> certificateTypes = new ArrayList<>();
    try {
      final Call<List<CertificateTypeDto>> caller = certReferenceDelegate.getCertificateTypes();
      certificateTypes = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return certificateTypes;
  }

  /**
   * Get certificate type.
   *
   * @param id id of the certificate type.
   * @return {@link CertificateTypeDto}.
   */
  @ResourceProvider
  @Override
  public final CertificateTypeDto getCertificateType(final Long id) {
    CertificateTypeDto certificateType = null;
    try {
      final List<CertificateTypeDto> certificateTypes = self.getCertificateTypes();
      certificateType = certificateTypes.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, id);
    }
    return certificateType;
  }

  /**
   * Get certificate statuses.
   *
   * @return A list of {@link CertificateStatusDto}.
   */
  @ResourceProvider
  @Override
  public final List<CertificateStatusDto> getCertificateStatuses() {
    List<CertificateStatusDto> statuses = new ArrayList<>();
    try {
      final Call<List<CertificateStatusDto>> caller = certReferenceDelegate.getCertificateStatuses();
      statuses = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return statuses;
  }

  /**
   * Get certificate status.
   *
   * @param id The certificate status id.
   * @return A {@link CertificateStatusDto}.
   */
  @ResourceProvider
  @Override
  public final CertificateStatusDto getCertificateStatus(final Long id) {
    CertificateStatusDto certificateStatusDto = null;
    try {
      final List<CertificateStatusDto> statuses = self.getCertificateStatuses();
      certificateStatusDto = statuses.stream().filter(status -> status.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, id);
    }
    return certificateStatusDto;
  }

  /**
   * Get surveyor.
   *
   * @param id Employee id.
   * @return {@link LrEmployeeDto}.
   */
  @Override
  public final LrEmployeeDto getSurveyor(final Long id) {
    final Call<LrEmployeeDto> empLead = assetServiceDelegate.getEmployeeById(id);
    try {
      final LrEmployeeDto surveyor = empLead.execute().body();
      return surveyor;
    } catch (IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return null;
  }

  /**
   * Get all action taken dto.
   *
   * @param id id.
   * @return action taken dto.
   */
  @ResourceProvider
  @Override
  public final List<CertificateActionTakenDto> getActionTakenDtos() {
    Call<List<CertificateActionTakenDto>> call = certReferenceDelegate.getCertificateActionTaken();
    try {
      List<CertificateActionTakenDto> list = call.execute().body();
      return list;
    } catch (IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return null;
  }

  /**
   * Get action taken dto.
   *
   * @param id id.
   * @return action taken dto.
   */
  @ResourceProvider
  @Override
  public final CertificateActionTakenDto getActionTakenDto(final Long id) {
    CertificateActionTakenDto actionTakenDto = null;
    try {
      List<CertificateActionTakenDto> list = self.getActionTakenDtos();
      actionTakenDto = Optional.ofNullable(list)
          .get().stream()
          .filter(dto -> dto.getId().equals(id))
          .findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.error(LOG_MESSAGE, id);
    }
    return actionTakenDto;
  }
}
