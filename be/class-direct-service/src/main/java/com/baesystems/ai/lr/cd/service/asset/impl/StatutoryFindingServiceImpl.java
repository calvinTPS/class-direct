package com.baesystems.ai.lr.cd.service.asset.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.DeficiencyService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.dto.paging.PageResource;


/**
 * Provides the base model implementation for the Statutory Finding Service. Gets paginated
 * statutory findings, statutory findings list and get single statutory finding.
 *
 * @author syalavarthi.
 * @author sbollu.
 *
 */
@Service("StatutoryService")
public class StatutoryFindingServiceImpl implements StatutoryFindingService {
  /**
   * The logger for StatutoryFindingServiceImpl.
   */
  public static final Logger LOGGER = LoggerFactory.getLogger(StatutoryFindingServiceImpl.class);
  /**
   * The {@link DeficiencyService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The {@link DeficiencyService} from Spring context.
   */
  @Autowired
  private DeficiencyService deficiencyService;
  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The {@value #DUE_SOON_IN_MONTHS} due soon threshold is default value in month.
   * <p>
   * to calculate due status if Three months before the Due Date has passed, status is due soon.<br>
   * {@link StatutoryFindingServiceImpl#calculateDueStatus(StatutoryFindingHDto, LocalDate)}.
   * </p>
   */
  private static final Long DUE_SOON_IN_MONTHS = 3L;

  /**
   * The {@value #IMMINENT_IN_MONTHS} imminent threshold default value in month.
   * <p>
   * to calculate due status if One month before the Due Date has passed, status is imminent. <br>
   * {@link StatutoryFindingServiceImpl#calculateDueStatus(StatutoryFindingHDto, LocalDate)}.
   * </p>
   */
  private static final Long IMMINENT_IN_MONTHS = 1L;

  @Override
  public final PageResource<StatutoryFindingHDto> getStatutorysPageResource(final Integer page, final Integer size,
      final Long assetId, final CodicilDefectQueryHDto query) throws ClassDirectException {

    PageResource<StatutoryFindingHDto> pageResource = null;

    pageResource = PaginationUtil.paginate(getStatutories(assetId, query), page, size);

    return pageResource;
  }

  @Override
  public final List<StatutoryFindingHDto> getStatutories(final Long assetId, final CodicilDefectQueryHDto query)
      throws ClassDirectException {
    List<StatutoryFindingHDto> statutoryFindings = null;
    try {
      statutoryFindings = assetRetrofitService.getStatutoryFindingsForAsset(assetId, query).execute().body();

      // filter statutory status open.
      if (statutoryFindings != null) {
        final ExecutorService executorService = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
        final List<Callable<Void>> callables = new ArrayList<>();
        final Map<Long, CodicilCategoryHDto> codicilCategoryLookup = generateCodicilCategoryLookup();
        statutoryFindings.forEach(statutoryFinding -> callables.add(() -> {
          Resources.inject(statutoryFinding);
          if (statutoryFinding.getAsset() != null) {
            statutoryFinding.setCode(AssetCodeUtil.getMastCode(statutoryFinding.getAsset().getId()));
          }
          Optional.ofNullable(statutoryFinding.getCategory()).ifPresent(category -> {
            statutoryFinding.setCategoryH(codicilCategoryLookup.get(category.getId()));
          });
          return null;
        }));
        try {
          executorService.invokeAll(callables);
        } catch (InterruptedException exception) {
          LOGGER.error("Error while hydrating: {} ", exception);
        } finally {
          executorService.shutdown();
        }
        LOGGER.info("stautory status is match with codicilstatus SF_OPEN {}", statutoryFindings.size());
      }

    } catch (final IOException exception) {
      LOGGER.error("Error query getStatutoryFindingsForDeficiency assetId {}", assetId, exception);
      throw new ClassDirectException(exception);
    }
    return statutoryFindings;
  }


  @Override
  public final List<StatutoryFindingHDto> getStatutoriesForDueNotification(final Long assetId,
      final CodicilDefectQueryHDto query) throws ClassDirectException {
    List<StatutoryFindingHDto> statutoryFindings = null;
    try {
      statutoryFindings = assetRetrofitService.getStatutoryFindingsForAsset(assetId, query).execute().body();

      if (statutoryFindings != null) {

        statutoryFindings.forEach(statutoryFinding -> {
          Resources.inject(statutoryFinding);
        });
      }

    } catch (final IOException exception) {
      LOGGER.error("Error query getStatutoriesForDueNotification assetId {}", assetId, exception);
      throw new ClassDirectException(exception);
    }
    return statutoryFindings;
  }

  @Override
  public final StatutoryFindingHDto getStatutoryFindingById(final Long assetId, final Long deficiencyId,
      final Long statutoryFindingId) throws ClassDirectException {

    StatutoryFindingHDto statutoryFinding = null;

    try {
      statutoryFinding =
          assetRetrofitService.getStatutoryFindingById(assetId, deficiencyId, statutoryFindingId).execute().body();
      Optional.ofNullable(statutoryFinding).ifPresent(statutory -> {
        final Map<Long, CodicilCategoryHDto> codicilCategoryLookup = generateCodicilCategoryLookup();
        Resources.inject(statutory);
        if (statutory.getAsset() != null) {
          statutory.setCode(AssetCodeUtil.getMastCode(statutory.getAsset().getId()));
        }
        Optional.ofNullable(statutory.getCategory()).ifPresent(category -> {
          statutory.setCategoryH(codicilCategoryLookup.get(category.getId()));
        });
        // hydrate job data
        try {
          if (statutory.getJob() != null) {
            JobHDto jobDto = jobServiceDelegate.getJobsByJobId(statutory.getJob().getId()).execute().body();
            statutory.setJobH(jobDto);
          }
        } catch (final Exception exception) {
          LOGGER.error("Error fetching job for statutory {} . | Error Message: {}", statutory.getId(),
              exception.getMessage());
        }
        // get deficiency associated with statutory finding.
        try {
          DeficiencyHDto deficiency = deficiencyService.getDeficiencyByAssetIdDeficiencyId(assetId, deficiencyId);
          Optional.ofNullable(deficiency).ifPresent(def -> {
            statutory.setDeficiencyH(def);
          });
        } catch (final ClassDirectException exception) {
          LOGGER.error("Error getting getDeficiencyByAssetIdDeficiencyId: assetId {} deficiencyId {}", assetId,
              deficiencyId, exception);
        }

      });
    } catch (final IOException exception) {
      LOGGER.error("Error query getStatutoryFindingById assetId {} deficienctId {} statutoryFindingId {}", assetId,
          deficiencyId, statutoryFindingId, exception);
      throw new ClassDirectException(exception);
    }
    return statutoryFinding;
  }

  @Override
  public final DueStatus calculateDueStatus(final StatutoryFindingHDto statutoryFindingHDto) {
    return calculateDueStatus(statutoryFindingHDto, LocalDate.now());
  }

  /*
   * Returns calculated dueStatus for given due date of Statutory Findings . Not Due-More than three
   * months before the Due Date, Due Soon-Three months before the Due Date has passed,Imminent-One
   * month before the Due Date has passed, Overdue-Due Date is passed.
   *
   * @see
   * com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator#calculateDueStatus(java.lang.Object,
   * java.time.LocalDate)
   */
  @Override
  public final DueStatus calculateDueStatus(final StatutoryFindingHDto sFinding, final LocalDate date) {
    // Default not due.
    DueStatus status = DueStatus.NOT_DUE;

    if (sFinding.getDueDate() != null) {
      // conversion of dueDate (Date object to LocalDate object).
      final LocalDate dueDate = DateUtils.getLocalDateFromDate(sFinding.getDueDate());
      // if Due Date is passed,status is overDue.
      if (dueDate.isBefore(date)) {
        status = DueStatus.OVER_DUE;
        // if One month before the Due Date has passed ,status is imminent.
      } else if (date.plusMonths(IMMINENT_IN_MONTHS).isAfter(dueDate)) {
        status = DueStatus.IMMINENT;
        // if Three months before the Due Date has passed,status is due soon.
      } else if (date.plusMonths(DUE_SOON_IN_MONTHS).isAfter(dueDate)) {
        status = DueStatus.DUE_SOON;
      }
    }

    return status;
  }

  /**
   * Generates reference data codicil catagory for mapping in pug file.
   *
   * @return the codicil catagory lookup object.
   */
  private Map<Long, CodicilCategoryHDto> generateCodicilCategoryLookup() {
    final List<CodicilCategoryHDto> codicilCategoryList = assetReferenceService.getCodicilCategories();
    final Map<Long, CodicilCategoryHDto> codicilCategoryLookup =
        codicilCategoryList.stream().collect(Collectors.toMap(CodicilCategoryHDto::getId, Function.identity()));
    return codicilCategoryLookup;
  }

}
