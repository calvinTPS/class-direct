package com.baesystems.ai.lr.cd.service.product.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ProductReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.ProductCatalogueHDto;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import retrofit2.Call;

/**
 * Provides product related reference data.
 *
 * @author PFauchon on 24/2/2017.
 * @author YWearn on 2017-10-02.
 */
@Service(value = "productReferenceServiceImpl")
public class ProductReferenceServiceImpl implements ProductReferenceService, InitializingBean {
  /**
   * Log4j logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProductReferenceService.class);

  /**
   * Retrofit object.
   */
  @Autowired
  private ProductReferenceRetrofitService productReferenceDelegate;


  /**
   * Gets all the catalogues.
   *
   * @return a list of product catalogues.
   */
  @ResourceProvider
  @Override
  public final List<ProductCatalogueHDto> getProductCatalogues() {
    List<ProductCatalogueHDto> certificateTemplates = new ArrayList<>();
    try {
      final Call<List<ProductCatalogueHDto>> caller = productReferenceDelegate.getProductCataloguesDto();
      certificateTemplates = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return certificateTemplates;
  }


  /**
   * Gets a single catalogue.
   *
   * @param id id of the catalogue.
   * @return a product catalogue.
   */
  @ResourceProvider
  @Override
  public final ProductCatalogueHDto getProductCatalogue(final Long id) {
    return self.getProductCatalogues().stream().filter(catalogue -> catalogue.getId().equals(id)).findFirst()
        .orElse(null);
  }

  @ResourceProvider
  @Override
  public final List<ProductGroupDto> getProductGroups() {
    List<ProductGroupDto> productGroups = new ArrayList<>();
    try {
      final Call<List<ProductGroupDto>> caller = productReferenceDelegate.getProductGroupsDto();
      productGroups = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed to fetch product groups.", ioe);
    }
    return productGroups;
  }

  @ResourceProvider
  @Override
  public final ProductGroupDto getProductGroup(final Long id) {
    return self.getProductGroups().stream().filter(catalogue -> catalogue.getId().equals(id)).findFirst().orElse(null);
  }

  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   *
   */
  private ProductReferenceService self = null;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(ProductReferenceService.class);
  }
}
