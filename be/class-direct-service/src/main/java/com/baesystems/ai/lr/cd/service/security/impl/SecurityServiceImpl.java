package com.baesystems.ai.lr.cd.service.security.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dto.security.RolesDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.service.security.SecurityService;

/**
 * @author vkovuru
 *
 */
@Service(value = "SecurityService")
public class SecurityServiceImpl implements SecurityService {

  /**
   * inject securityDAO.
   */
  @Autowired
  private SecurityDAO securityDAO;

  /*
   * (non-Javadoc)
   *
   * @see com.baesystems.ai.lr.cd.service.security.SecurityService#hasRoles(java.lang.String,
   * java.util.List)
   */
  @Override
  public final boolean hasRoles(final String userName, final RolesDto roles) {
    return securityDAO.hasRole(userName, roles.getRoles());
  }
}
