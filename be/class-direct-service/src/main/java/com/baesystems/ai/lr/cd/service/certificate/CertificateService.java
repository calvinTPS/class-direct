package com.baesystems.ai.lr.cd.service.certificate;

import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificatePageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateQueryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;

/**
 * Provides methods to get certificates and certificate attachments.
 *
 * @author fwijaya on 20/1/2017.
 * @author sbollu
 *
 */
public interface CertificateService {

  /**
   * Returns paginated list of certificates for given asset.
   *
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @param order the sorting order eg: ascending or descending.
   * @param sort the sort field.
   * @param query the query body.
   * @return paginated list of certificates.
   * @throws ClassDirectException if execution or API call fails.
   * @throws RecordNotFoundException if attachment not found for the certificate.
   */

  CertificatePageResource getCertificateQueryPageResource(final Integer page, final Integer size, final String sort,
      final String order, final CertificateQueryHDto query) throws RecordNotFoundException, ClassDirectException;

  /**
   * Returns attachment for given certificate id and job id.
   *
   * @param jobId the unique identifier of job.
   * @param certId the unique identifier of certificate.
   * @return SupplementaryInformationPageResourceDto object with attachment details.
   * @throws ClassDirectException if execution or API call fails.
   */
  SupplementaryInformationPageResourceDto getCertificateAttachment(final Long jobId, final Long certId)
      throws ClassDirectException;

}
