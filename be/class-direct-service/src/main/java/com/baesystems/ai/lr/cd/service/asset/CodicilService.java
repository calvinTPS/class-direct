package com.baesystems.ai.lr.cd.service.asset;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilStatusesHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides methods to get list of codicil associated with asset.
 *
 * @author yng
 * @author sbollu
 *
 */
public interface CodicilService {
  /**
   * Returns list of paginated codicils associated to an asset.
   *
   * @param page the number of pages to be returned.
   * @param size the size of elements per page to be returned.
   * @param assetId the asset unique identifier.
   * @return list of codicil.
   * @throws ClassDirectException if API call to fetch COC's and actionable items fails or execution fails.
   */
  PageResource<Object> getCodicil(final Integer page, final Integer size, final Long assetId)
      throws ClassDirectException;

  /**
   * Gets all codicil statuses including (COC, AI, AN, SF, MNCN, Defect and Deficiency).
   *
   * @return codicil statuses.
   */
  CodicilStatusesHDto getCodicilStatuses();
}
