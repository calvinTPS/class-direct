package com.baesystems.ai.lr.cd.be.processor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers.DateDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.monitorjbl.json.JsonView;
import com.monitorjbl.json.JsonViewSerializer;

/**
 * @author VMandalapu
 */
public class CustomJsonSerializerModule extends SimpleModule {

  /**
   *
   */
  private static final long serialVersionUID = -727633465747252611L;

  /**
   *
   */
  public CustomJsonSerializerModule() {
    super();
    addSerializer(Date.class, new DateSerializer(false, new ISO8601DateFormat()));
    addDeserializer(Date.class, new DateDeserializer(new DateDeserializer(), new ISO8601DateFormat(), _name));
    addSerializer(LocalDate.class, LocalDateSerializer.INSTANCE);
    addSerializer(LocalTime.class, LocalTimeSerializer.INSTANCE);

    addDeserializer(LocalDate.class, LocalDateDeserializer.INSTANCE);
    addDeserializer(LocalTime.class, LocalTimeDeserializer.INSTANCE);

    addSerializer(LocalDateTime.class, LocalDateTimeSerializer.INSTANCE);
    addDeserializer(LocalDateTime.class, LocalDateTimeDeserializer.INSTANCE);

  //for field filtering
    JsonViewSerializer jsonViewSerializer = new JsonViewSerializer();
    jsonViewSerializer.registerCustomSerializer(DueStatus.class, new DueStatus.DueStatusSerializer());

    addSerializer(JsonView.class, jsonViewSerializer);

  }

}
