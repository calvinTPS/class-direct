package com.baesystems.ai.lr.cd.service.asset;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides the interface for assset note service.
 *
 * @author yng
 *
 */
public interface AssetNoteService {
  /**
   * Returns the paginated list of asset note.
   *
   * @param page the page number.
   * @param size the max amount of data to be display per page.
   * @param assetId the asset Id.
   * @return the paginated list of asset note.
   * @throws ClassDirectException if API call fail.
   */
  PageResource<AssetNoteHDto> getAssetNote(final Integer page, final Integer size, final Long assetId)
      throws ClassDirectException;

  /**
   * Returns asset note by providing asset Id and asset note Id.
   *
   * @param assetId the asset Id.
   * @param assetNoteId the asset note Id.
   * @return the asset note object
   * @throws ClassDirectException if API call fail.
   */
  AssetNoteHDto getAssetNoteByAssetIdAssetNoteId(final Long assetId, final long assetNoteId)
      throws ClassDirectException;
}
