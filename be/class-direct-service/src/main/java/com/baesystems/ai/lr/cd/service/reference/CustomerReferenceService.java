package com.baesystems.ai.lr.cd.service.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides service for fetching customer's {@link CountryDto}.
 *
 * @author YWearn
 */
public interface CustomerReferenceService {

  /**
   * Gets all country reference data from external API.
   * @return the complete list of country reference data.
   * @throws ClassDirectException if any error happen on the external API call.
   */
  List<CountryHDto> getCountries() throws ClassDirectException;

  /**
   * Gets single country reference data based on id.
   * @param countryId the internal id of a country.
   * @return the country detail including the name and description.
   * @throws ClassDirectException if any error happen on the external API call.
   */
  CountryHDto getCountry(final long countryId) throws ClassDirectException;

}
