package com.baesystems.ai.lr.cd.be.service.security.impl;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.MasqueradeUserDto;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;

/**
 * This class implements System Security using Class-Direct for Equasis user.
 *
 * @author RKaneysan
 * @version 1.0
 * @since 2016-09-15
 */
@Service(value = "AesSecurityService")
public class AesSecurityServiceImpl implements AesSecurityService {

  /**
   * Logger for ClassDirectSecurityServiceImpl.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AesSecurityServiceImpl.class);

  /**
   * Encoding for UTF-8.
   */
  private static final String ENCODING_UTF_8 = "UTF-8";

  /**
   * Value separator.
   */
  private static final String SEPARATOR = ";";

  /**
   * Secret Key for Application.
   */
  @Value("${application.secret.key}")
  private String applicationSecretKey;

  /**
   * Initial Vector for Application.
   */
  @Value("${application.init.vector}")
  private String applicationInitVector;

  @Override
  public final String encrypt(final String roleName, final Long assetId) {
    return encryptValue(roleName + SEPARATOR + assetId);
  }

  @Override
  public final String decryptRoleName(final String encryptedValue) {
    final String decryptedValue = decryptValue(encryptedValue);
    String roleName = null;

    if (decryptedValue != null && decryptedValue.split(SEPARATOR).length > 0) {
      roleName = decryptedValue.split(SEPARATOR)[0];
    }

    return roleName;
  }

  @Override
  public final Long decryptAssetId(final String encryptedValue) {
    final String decryptedValue = decryptValue(encryptedValue);
    Long assetId = 0L;

    if (decryptedValue != null && decryptedValue.split(SEPARATOR).length > 0) {
      assetId = Long.valueOf(decryptedValue.split(SEPARATOR)[1]);
    }

    return assetId;
  }

  @Override
  public final StringResponse encryptMasqueradeUserId(final MasqueradeUserDto dto) {
    return new StringResponse(encryptValue(dto.getUserId()));
  }

  /**
   * Encrypt the string.
   *
   * @param value string to be encrypted.
   * @return encrypted value.
   */
  private String encryptValue(final String value) {
    String encryptedValue = null;
    IvParameterSpec ivParamSpec;

    try {
      ivParamSpec = new IvParameterSpec(applicationInitVector.getBytes(ENCODING_UTF_8));
      final SecretKeySpec skeySpec = new SecretKeySpec(applicationSecretKey.getBytes(ENCODING_UTF_8), "AES");
      final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParamSpec);
      final byte[] encrypted = cipher.doFinal(value.getBytes(ENCODING_UTF_8));
      encryptedValue = Base64.encodeBase64URLSafeString(encrypted);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
        | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException exception) {

      LOGGER.info("AES Security Service : {}", exception.getMessage());
    }
    LOGGER.info("Encrypted value : {}", encryptedValue);
    return encryptedValue;
  }

  /**
   * Decrypt the string.
   *
   * @param encryptedValue encrypted string.
   * @return decrypted value.
   */
  @Override
  public final String decryptValue(final String encryptedValue) {
    String decryptedValue = null;
    try {
      final IvParameterSpec ivParamSpec = new IvParameterSpec(applicationInitVector.getBytes(ENCODING_UTF_8));
      final SecretKeySpec skeySpec = new SecretKeySpec(applicationSecretKey.getBytes(ENCODING_UTF_8), "AES");
      final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParamSpec);
      final byte[] original = cipher.doFinal(Base64.decodeBase64(encryptedValue.getBytes(ENCODING_UTF_8)));
      decryptedValue = new String(original, ENCODING_UTF_8);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
        | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException exception) {
      LOGGER.error("AES Security Service : {}", exception.getMessage());
    }

    return decryptedValue;
  }
}
