package com.baesystems.ai.lr.cd.be.processor;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.util.MessageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;

/**
 * Provides methods to process the incoming headers to enable masquerading.
 *
 * @author yng
 * @author sbollu
 *
 */
public class EnableMasqueradeProcessor implements Processor {

  /**
   * Logger for {@link EnableMasqueradeProcessor}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(EnableMasqueradeProcessor.class);

  /**
   * The {@link AesSecurityService} from Spring context.
   */
  @Autowired
  private AesSecurityService aesSecurityService;

  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The error message for unauthorize.
   */
  @Value("${unauthorized.access.user.Masquerade}")
  private String unauthorizedAccess;

  /**
   * The error message for user not found.
   */
  @Value("${record.not.found.with.id}")
  private String userNotFound;


  @Override
  public final void process(final Exchange exchange)
      throws IOException, RecordNotFoundException, UnauthorisedAccessException, ClassDirectException {

    // login userId
    final String userId = Optional.ofNullable(exchange).map(Exchange::getIn)
        .map(m -> m.getHeader(CDConstants.HEADER_USER_ID)).map(Object::toString).orElse(null);
    if (!StringUtils.isEmpty(userId)) {
      UserProfiles headerUser = userProfileService.getUser(userId);
      if (headerUser != null) {
        List<String> roles = headerUser.getRoles().stream().map(Roles::getRoleName).collect(Collectors.toList());
        boolean isAdmin = SecurityUtils.isAdmin(roles);
        // permission check of login user
        if (isAdmin) {
          // userId to masquerade
          final String masqUserId =
              Optional.ofNullable(exchange).map(Exchange::getIn).map(m -> m.getHeader(CDConstants.HEADER_MASQ_ID))
                  .map(Object::toString).map(euid -> aesSecurityService.decryptValue(euid)).orElse(null);

          if (masqUserId != null) {
            printMasqLog(exchange);
            exchange.getIn().setHeader("userId", masqUserId);
          } else {
            LOGGER.info("UserId is null. Masquerading is ignored.");
          }
        } else {
          LOGGER.error("User {} doesn't have permission to masquerade ", userId);
          throw new UnauthorisedAccessException(unauthorizedAccess);
        }

      } else {
        LOGGER.error("User {} not found ", userId);
        throw new RecordNotFoundException(userNotFound);
      }

    } else {
      LOGGER.error("Invalid user");
      throw new ClassDirectException(userNotFound);
    }
  }

  /**
   * Logs masquerading activity info.
   *
   * @param exchange the camel exchange object.
   */
  public final void printMasqLog(final Exchange exchange) {
    final StringBuilder logInfo = new StringBuilder();

    logInfo.append("UserId:[").append(exchange.getIn().getHeader("userId")).append("]  ");
    logInfo.append("MasqueradeId:[").append(exchange.getIn().getHeader("x-masq-id")).append("]  ");
    logInfo.append("URL:[").append(exchange.getIn().getHeader("CamelHttpMethod")).append(":")
        .append(exchange.getIn().getHeader("CamelHttpUri")).append("]  ");
    logInfo.append("Headers:[").append(convertHeadersToJSON(exchange.getIn().getHeaders())).append("]  ");
    if (exchange.getIn().getBody() != null) {
      logInfo.append("Body:[").append(MessageHelper.extractBodyAsString(exchange.getIn()).replaceAll("(\\r|\\n)", ""))
          .append("]");
    } else {
      logInfo.append("Body:[]");
    }
    LOGGER.info(logInfo.toString());
  }

  /**
   * Flattens header to value type string.
   *
   * @param map the map contain headers.
   * @return the flatten headers string.
   */
  public final String convertHeadersToJSON(final Map<String, Object> map) {
    final Map<String, String> result = new TreeMap<>();

    // flatten object to string
    map.forEach((key, value) -> {
      if (value != null) {
        result.put(key, value.toString());
      } else {
        result.put(key, "null");
      }
    });

    return result.toString();
  }
}
