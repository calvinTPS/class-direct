package com.baesystems.ai.lr.cd.service.cache;

import com.baesystems.ai.lr.dto.base.IdDto;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.function.Function;
import java.util.stream.Collectors;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;

/**
 * Provides an aspect to intercept the reference data fetching to lookup from cache instead of actual API call.
 *
 * @author MSidek
 * @author YWearn on 2017-10-04
 *
 */
@Aspect
@Component
public class CacheAspect {
  /**
   * The appplication logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CacheAspect.class);

  /**
   * The cache container to hold cached reference data.
   */
  @Autowired
  private CacheContainer cache;

  /**
   * The {@link SubFleetService}.
   */
  @Autowired
  private SubFleetService subFleetService;

  /**
   * After returning from pointcut, put the result into cache.
   *
   * @param joinPoint the method join point.
   * @param retVal the method return values.
   * @throws Throwable if error.
   */
  @AfterReturning(pointcut = "@annotation(com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider)",
      returning = "retVal")
  public void cacheResult(final JoinPoint joinPoint, final Object retVal) throws Throwable {
    LOGGER.debug("Cache the result if the result is List.");

    final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    final Method method = signature.getMethod();

    if (List.class.isAssignableFrom(retVal.getClass())) {
      final List<? extends IdDto> list = (List<? extends IdDto>) retVal;
      if (list.isEmpty()) {
        LOGGER.debug("Result is empty. Skipping.");
      } else {
        final ParameterizedType listType = (ParameterizedType) method.getGenericReturnType();
        final Class<?> type = (Class<?>) listType.getActualTypeArguments()[0];
        LOGGER.debug("Putting into {} objects of type {} into cache.", list.size(), type.getSimpleName());
        if (cache.get(type).isEmpty()) {
          final Map<Long, ?> cacheMap = toIdDtoMap(list);
          cache.set(type, cacheMap);
        } else {
          LOGGER.debug("Cache is populated. Skipping.");
        }
      }
    } else {
      LOGGER.debug("Object is not a list. Ignore.");
    }
  }

  /**
   * Intercept the method with <code>@ResourceProvider</code>.
   *
   * @param joinPoint method join point.
   * @return result.
   * @throws Throwable when error
   */
  @Around("@annotation(com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider)")
  public Object checkAndGetFromCache(final ProceedingJoinPoint joinPoint) throws Throwable {
    LOGGER.debug("Intercepting execution of {}.", joinPoint.getSignature().getName());
    Object result;

    final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    final Method method = signature.getMethod();

    Class<?> type;
    if (Collection.class.isAssignableFrom(method.getReturnType())) {
      final ParameterizedType listType = (ParameterizedType) method.getGenericReturnType();
      type = (Class<?>) listType.getActualTypeArguments()[0];
    } else {
      type = method.getReturnType();
    }

    LOGGER.debug("Checking cache for return type {}.", type.getSimpleName());
    final Map<Long, ?> cachedResultMap = cache.get(type);
    if (cachedResultMap.isEmpty()) {
      LOGGER.debug("Cache is empty. Proceed to actual method.");
      result = joinPoint.proceed();
    } else if (!Collection.class.isAssignableFrom(method.getReturnType())) {
      LOGGER.debug("Iterating the results to get single result");
      final Long id = (Long) joinPoint.getArgs()[0];
      result = cachedResultMap.get(id);
      if (result == null) {
        LOGGER.warn(
            "Cannot find result for return type {} with ID {} from cache" + ", going to trigger actual implementation.",
            type.getSimpleName(), id);
        result = joinPoint.proceed();
      }
    } else {
      LOGGER.debug("Full list of cache requested for return type {}.", type.getSimpleName());
      result = new ArrayList<>(cachedResultMap.values());
    }

    return result;
  }

  /**
   * Converts the list object into map with id as key.
   *
   * @param values the list of object to cache.
   * @return the map of cache object.
   */
  private Map<Long, ? extends IdDto> toIdDtoMap(final List<? extends IdDto> values) {
    Map<Long, ? extends IdDto> resultMap = new HashMap<>(0);
    if (values != null) {
      resultMap = values.stream().collect(Collectors.toMap(IdDto::getId, Function.identity()));
    }
    return resultMap;
  }

  /**
   * Recursively resolve all fields from superclasses.
   *
   * @param fields list of fields.
   * @param type object type.
   * @return list of fields
   */
  private List<Field> getAllFields(final List<Field> fields, final Class<?> type) {
    fields.addAll(Arrays.asList(type.getDeclaredFields()));

    if (type.getSuperclass() != null) {
      fields.addAll(getAllFields(fields, type.getSuperclass()));
    }

    return fields;
  }

  /**
   * Re-cache data after save subfleet.
   *
   * We don't care about the saveSubFleet that the data migration team is apparently using.
   *
   * @param joinPoint contain arguments from method saveSubFleet.
   */
  @After("execution(* com.baesystems.ai.lr.cd.service.subfleet.SubFleetService.saveSubFleetOptimized(..))")
  public void recacheAfterSaveSubFleet(final JoinPoint joinPoint) {
    LOGGER.debug("Re-caching AccessibleAssetIds...");
    final Object[] arguments = joinPoint.getArgs();

    new Thread(() -> {
      try {
        // This calls MAST-IHS query, cache it please.
        subFleetService.getAccessibleAssetIdsForUser((String) arguments[0]);
      } catch (final ClassDirectException exception) {
        LOGGER.debug("Unable to re-cache AccessibleAssetIds: " + exception);
      }
    }).start();
  }
}
