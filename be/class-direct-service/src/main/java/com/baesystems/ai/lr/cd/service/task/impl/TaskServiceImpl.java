package com.baesystems.ai.lr.cd.service.task.impl;

import static com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum.UPDATE_MPMS_TASKS;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AuditPmsAbleTaskDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.HierarchyDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemModelItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.MastAPIException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ExceptionUtil;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.TaskUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.enums.ResolutionStatus;
import com.baesystems.ai.lr.enums.WorkItemType;

import retrofit2.Call;

/**
 * Provides local service for updating tasks, getting tasks for asset or service, getting
 * checklist's for service.
 *
 * @author VKolagutla
 * @author sBollu
 *
 */
@Service(value = "TaskService")
public class TaskServiceImpl implements TaskService {


  /**
   * The application logger.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(TaskService.class);
  /**
   * The {@link TaskRetrofitService} from Spring context.
   */
  @Autowired
  private TaskRetrofitService taskRetrofitService;

  /**
   * The {@link ServiceReferenceRetrofitService} from Spring context.
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceReference;

  /**
   * The error message for record not found for given asset.
   */
  @Value("${record.not.found.for.asset.id}")
  private String recordNotFound;

  /**
   * The error message for record not found for given service.
   */
  @Value("${record.not.found.for.service.id}")
  private String recordNotFoundForService;

  /**
   * The long value is {@value #THREE_MONTHS}.
   */
  private static final Long THREE_MONTHS = 3L;

  /**
   * The long value is {@value #ONE_MONTH}.
   */
  private static final Long ONE_MONTH = 1L;

  /**
   * The error message for record not found for given checklist.
   */
  @Value("${record.not.found.for.checklist.id}")
  private String checklistIdNotFound;
  /**
   * The Audit Service for audit.
   */
  @Autowired
  private AuditService auditService;

  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) list of tasks
   * associated with the assetId using pmsAble filter. Filter for task has service with continuous
   * indicator as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   *
   *
   * @param assetId the asset unique identifier.
   * @param pmsAble whether the task is pmsCreditable.
   * @return list of tasks.
   * @throws ClassDirectException if execution fails or fail to find object with given unique
   *         identifier.
   */
  @Override
  public final List<WorkItemLightHDto> getTasksByAssetId(final Long assetId, final Boolean pmsAble)
      throws ClassDirectException {
    try {
      final List<ScheduledServiceHDto> services = getServicesByAssetId(assetId);
      boolean pmsFilter = Optional.ofNullable(pmsAble).orElse(false);
      final List<Long> serviceIds = Collections.synchronizedList(new ArrayList<>());
      final Set<Long> continuousIds = new HashSet<>();
      if (services != null) {
        getContinuousServiceIds(services, serviceIds, continuousIds);
        final WorkItemQueryDto query = returnTaskQueryDto(serviceIds, pmsFilter);

        Predicate<WorkItemLightHDto> taskOnly = pmsTasksFilter(continuousIds, pmsFilter);
        return getTasksForServices(query, taskOnly);
      } else {
        throw new RecordNotFoundException(recordNotFound, assetId);
      }
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
      throw new ClassDirectException(ioe);
    }
  }

  /**
   * Verifies whether task has service with continuous indicator.Filter should be based on
   * conditions, 1.Not started - Resolution status is null,Pms Credit date is null. 2.postponed not
   * credited - Resolution status is postponed,Pms Credit date is null. 3.pms'ed not credited -
   * Resolution status is null,Pms Credit date is null, PmsApplicable is true. 4.exclude
   * resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).
   *
   * @param workItem the workItem model object.
   * @return true if task has service with continuous indicator otherwise false.
   */
  private boolean isContinuousTaskService(final WorkItemLightHDto workItem) {
    // resolutionStatus - postponed.
    boolean postponed = Optional.ofNullable(workItem.getResolutionStatus()).map(LinkResource::getId)
        .map(id -> ResolutionStatus.POSTPONED.getValue().equals(id)).orElse(false);

    // exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X))
    boolean excludedWCX = Optional.ofNullable(workItem.getResolutionStatus()).map(LinkResource::getId)
        .map(id -> ResolutionStatus.WAIVED.getValue().equals(id)
            || ResolutionStatus.CONFIRMATORY_CHECK.getValue().equals(id)
            || ResolutionStatus.COMPLETE.getValue().equals(id))
        .orElse(false);
    // continuous service task - notstarted,postponed not credited,pmsed not credited.
    return ((isNotStarted(workItem) && isCredited(workItem)) || (postponed && isCredited(workItem))
        || nonCreditedPms(workItem)) && !excludedWCX;
  }

  /**
   * Verifies whether task has started.
   *
   * @param workItem the workItem model object.
   * @return true resolution status is null otherwise false.
   */
  private boolean isNotStarted(final WorkItemLightHDto workItem) {
    return workItem.getResolutionStatus() == null;
  }

  /**
   * Verifies whether task is credited.
   *
   * @param workItem the workItem model object.
   * @return true if pms credit date is null otherwise false.
   */
  private boolean isCredited(final WorkItemLightHDto workItem) {
    return workItem.getPmsCreditDate() == null;
  }

  /**
   * Verifies whether task is creditable.
   *
   * @param workItem the workItem model object.
   * @return true if task is able to credit otherwise false.
   */
  private boolean nonCreditedPms(final WorkItemLightHDto workItem) {
    return Optional.ofNullable(workItem.getPmsApplicable()).orElse(false) && isCredited(workItem);
  }

  /**
   * Returns list of service details associated with the assetId.
   *
   * @param assetId the asset unique identifier.
   * @return list of services.
   * @throws ClassDirectException if execution fails or API call fails.
   */
  public final List<ScheduledServiceHDto> getServicesByAssetId(final Long assetId) throws ClassDirectException {
    List<ScheduledServiceHDto> services = null;
    try {
      final Call<List<ScheduledServiceHDto>> servicesCall = serviceReference.getAssetServices(assetId);
      services = servicesCall.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
      throw new ClassDirectException(ioe);
    }
    return services;

  }

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) list of tasks
   * associated with the serviceId.Filter for task has service with continuous indicator as shown
   * below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   * @param serviceId the service unique identifier.
   * @param assetId the asset unique identifier.
   * @return list of tasks.
   * @throws ClassDirectException if execution fails or API call fails.
   */
  public final List<WorkItemLightHDto> getTasksByService(final Long serviceId, final Long assetId)
      throws ClassDirectException {

    try { // get services for given asset.
      List<ScheduledServiceHDto> services = getServicesByAssetId(assetId);
      if (services != null) {
        services = services.parallelStream().filter(service -> service.getId().longValue() == serviceId)
            .collect(Collectors.toList());
        final Set<Long> continousIds = new HashSet<>();
        getContinuousServiceIds(services, null, continousIds);
        final List<Long> ids = Collections.singletonList(serviceId);
        final WorkItemQueryDto query = returnTaskQueryDto(ids, null);
        final Predicate<WorkItemLightHDto> filter = getContinuousServiceFilterTask(continousIds);
        return getTasksForServices(query, filter);
      } else {
        throw new RecordNotFoundException(recordNotFound, assetId);
      }
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
      throw new ClassDirectException(ioe);
    }
  }

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) paginated list
   * of tasks associated with list of services. Filter for task has service with continuous
   * indicator as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   * @param serviceList the list of schedule services.
   * @return the filtered tasks list group by service id.
   * @throws ClassDirectException if the service list is null or api to fetch task return error.
   */
  @Override
  public final Map<Long, List<WorkItemLightHDto>> getTasksForServices(final List<ScheduledServiceHDto> serviceList)
      throws ClassDirectException {
    final Map<Long, List<WorkItemLightHDto>> resultMap;
    if (serviceList != null) {
      try {
        final Set<Long> continousServiceIdLookup = new HashSet<>();
        final List<Long> serviceIdList = new ArrayList<>();
        getContinuousServiceIds(serviceList, serviceIdList, continousServiceIdLookup);
        final Predicate<WorkItemLightHDto> filter = getContinuousServiceFilterTask(continousServiceIdLookup);

        final WorkItemQueryDto query = new WorkItemQueryDto();
        query.setScheduledServiceId(serviceIdList);
        query.setTaskTypeId(WorkItemType.TASK.value());

        final List<WorkItemLightHDto> taskList = getTasksForServices(query, filter);

        resultMap = taskList.stream()
            .collect(Collectors.groupingBy(task -> task.getScheduledService().getId(), Collectors.toList()));

      } catch (final RecordNotFoundException | IOException e) {
        final List<String> serviceIdList = serviceList.parallelStream().map(ScheduledServiceHDto::getId)
            .map(String::valueOf).collect(Collectors.toList());
        final String serviceIds = String.join(", ", serviceIdList);
        LOGGER.error("Fail to fetch tasks for services : " + serviceIds, e);
        throw new ClassDirectException(
            "Fail to fetch tasks for services : " + serviceIds + ". Error Message - " + e.getMessage());
      }
    } else {
      throw new ClassDirectException("No services specified.");
    }
    return resultMap;
  }


  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) list of hydrated
   * tasks associated with the serviceId.Filter for task has service with continuous indicator as
   * shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   * @param tasks the actual list of tasks from mast.
   * @param filter the predicate condition to get hydrated tasks.
   * @return list of hydrated tasks.
   */
  public final List<WorkItemLightHDto> getHydratedTasksByService(final List<WorkItemLightDto> tasks,
      final Predicate<WorkItemLightHDto> filter) {

    List<WorkItemLightHDto> returnTask = Collections.emptyList();
    if (tasks != null && !tasks.isEmpty()) {
      Stream<WorkItemLightHDto> stream = tasks.stream().map(task -> {
        WorkItemLightHDto taskH = new WorkItemLightHDto();
        BeanUtils.copyProperties(task, taskH);
        return taskH;
      });
      if (filter != null) {
        stream = stream.filter(filter);
      }
      returnTask = stream.peek(Resources::inject).collect(Collectors.toList());
    }
    return returnTask;

  }

  /**
   * Returns filter whether task has continuous service or not.
   *
   * @param continuosServiceIdLookup the continuous service Id's associated with single assetId.
   * @return predicate condition.
   */
  private Predicate<WorkItemLightHDto> getContinuousServiceFilterTask(final Set<Long> continuosServiceIdLookup) {
    Predicate<WorkItemLightHDto> taskOnly = workItem -> {
      Long serviceId = Optional.ofNullable(workItem).map(WorkItemLightHDto::getScheduledService)
          .map(LinkResource::getId).orElse(null);
      if (continuosServiceIdLookup.contains(serviceId)) {
        return isContinuousTaskService(workItem);
      } else {
        return true;
      }
    };
    return taskOnly;
  }

  /**
   * Returns filter whether task has continuous service or not.
   *
   * @param continousIds the continuous service Id's associated with single assetId.
   * @param pmsFilter the tasks has PMS applicable true.
   * @return predicate condition
   */
  private Predicate<WorkItemLightHDto> pmsTasksFilter(final Set<Long> continousIds, final boolean pmsFilter) {
    Predicate<WorkItemLightHDto> filter = workItem -> {

      // nonCreditedPmsTask - PmsApplicable should be true and pmscreditDate should be null
      boolean nonCreditedPmsTask = nonCreditedPms(workItem);
      // not credited - pmscreditDate should be null.
      boolean notStarted = isNotStarted(workItem);

      // for continuousService task.
      Long serviceId = Optional.ofNullable(workItem).map(WorkItemLightHDto::getScheduledService)
          .map(LinkResource::getId).orElse(null);
      if (continousIds.contains(serviceId)) {

        boolean isContinuousServiceTask = isContinuousTaskService(workItem);

        if (pmsFilter) {
          // pmstaskList for continuous services.
          return isContinuousServiceTask && nonCreditedPmsTask && notStarted;
        } else {
          // taskList for continuous services.
          return isContinuousServiceTask;
        }
      } else {
        if (pmsFilter) {
          // pmstaskList for nonContinuous services.
          return nonCreditedPmsTask && notStarted;
        } else {
          // taskList
          return true;
        }
      }
    };

    return filter;
  }

  /**
   * Returns the query DTO to get list of tasks.
   *
   * @param serviceIds the list of unique identifier of service.
   * @param pmsAble the flag to indicate only fetch MPMS task.
   * @return the query DTO to get tasks.
   */
  private WorkItemQueryDto returnTaskQueryDto(final List<Long> serviceIds, final Boolean pmsAble) {
    // Set the id's to scheduledServiceId in the WorkItemQuery to get the tasks.
    final WorkItemQueryDto query = new WorkItemQueryDto();
    query.setScheduledServiceId(serviceIds);
    query.setTaskTypeId(WorkItemType.TASK.value());
    Optional.ofNullable(pmsAble).ifPresent(query::setPmsable);
    return query;
  }

  /**
   * Sets list of service id's and service id's has continuous indicator.
   *
   * @param services the list of services to filter for continuous service.
   * @param serviceIds the list of service id's.
   * @param continuousIds the list of continuous service id's.
   */
  public final void getContinuousServiceIds(final List<ScheduledServiceHDto> services, final List<Long> serviceIds,
      final Set<Long> continuousIds) {
    // get continuous service ids.

    List<Long> conIds = services.stream().filter(dto -> {
      // actual service Ids.
      if (serviceIds != null) {
        serviceIds.add(dto.getId());
      }
      Resources.inject(dto);
      boolean isContinous = Optional.ofNullable(dto.getServiceCatalogueH())
          .map(ServiceCatalogueHDto::getContinuousIndicator).orElse(false);
      return isContinous;
    }).map(ScheduledServiceHDto::getId).collect(Collectors.toList());
    continuousIds.addAll(conIds);
  }

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) paginated list
   * of tasks associated with the assetId using pmsAble filter.
   *
   * @param assetId the asset unique identifier.
   * @param pmsAble pmsAble whether the task is pmsCreditable.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @return paginated list of tasks.
   * @throws ClassDirectException if execution fails or fail to find object with given unique
   *         identifier.
   * @throws UnauthorisedAccessException if user is not allowed to access.
   * @throws IOException if API call fails.
   */
  @Override
  public final PageResource<WorkItemLightHDto> getTasksByAssetId(final Long assetId, final Boolean pmsAble,
      final Integer page, final Integer size) throws ClassDirectException, UnauthorisedAccessException, IOException {
    LOGGER.debug("Querying task with page {} and size {}.", page, size);
    PageResource<WorkItemLightHDto> pageResource = null;
    pageResource = PaginationUtil.paginate(getTasksByAssetId(assetId, pmsAble), page, size);
    return pageResource;
  }

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) paginated list
   * of tasks associated with the serviceId.
   *
   * @param serviceId the service unique identifier.
   * @param assetId the asset unique identifier.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @return paginated list of tasks.
   * @throws ClassDirectException if execution fails or API call fails.
   */
  @Override
  public final PageResource<WorkItemLightHDto> getTasksByService(final Long serviceId, final Long assetId,
      final Integer page, final Integer size) throws ClassDirectException {
    LOGGER.debug("Querying task with page {} and size {}.", page, size);
    PageResource<WorkItemLightHDto> pageResource = null;

    pageResource = PaginationUtil.paginate(getTasksByService(serviceId, assetId), page, size);

    return pageResource;
  }

  @Override
  public final DueStatus calculateDueStatus(final WorkItemLightHDto actionableItemInput) {
    return calculateDueStatus(actionableItemInput, LocalDate.now());
  }

  /*
   * Returns calculated dueStatus for given due date and postponement date of tasks. If postponement
   * date has value, then postponement date is considered as due date. <ul><li>Not Due-More than
   * three months before the Due Date</li> <li>Due Soon - Three months before the Due Date has
   * passed</li> <li>Imminent-One month before the Due Date has passed, Overdue-Due Date is
   * passed.<li></ul>
   *
   * @see
   * com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator#calculateDueStatus(java.lang.Object,
   * java.time.LocalDate)
   */
  @Override
  public final DueStatus calculateDueStatus(final WorkItemLightHDto task, final LocalDate date) {

    // Default not due.
    DueStatus status = DueStatus.NOT_DUE;

    // If postponement date is null.
    if (task.getPostponementDate() == null) {

      // If DueDate not NULL
      if (task.getDueDate() != null) {
        // DueDate to LocalDate
        final LocalDate dueDate = DateUtils.getLocalDateFromDate(task.getDueDate());

        if (dueDate.isBefore(date)) {
          status = DueStatus.OVER_DUE;

        } else if (date.plusMonths(ONE_MONTH).isAfter(dueDate)) {
          status = DueStatus.IMMINENT;

        } else if (date.plusMonths(THREE_MONTHS).isAfter(dueDate)) {
          status = DueStatus.DUE_SOON;
        }
      }
    } else {

      // If postponement date not null.
      if (task.getPostponementDate() != null) {
        // postponement Date to LocalDate
        final LocalDate postponementDate = DateUtils.getLocalDateFromDate(task.getPostponementDate());

        if (postponementDate.isBefore(date)) {
          status = DueStatus.OVER_DUE;

        } else if (date.plusMonths(ONE_MONTH).isAfter(postponementDate)) {
          status = DueStatus.IMMINENT;

        } else if (date.plusMonths(THREE_MONTHS).isAfter(postponementDate)) {
          status = DueStatus.DUE_SOON;
        }
      }
    }
    return status;
  }

  /**
   * Returns filtered list of tasks associated with the query.
   *
   * @param queryDto the query model object .
   * @param filter the list of predicates need to apply to filter tasks.
   * @return list of filtered Tasks.
   * @throws RecordNotFoundException if object not found for given unique identifier.
   * @throws IOException if API call fails.
   */
  public final List<WorkItemLightHDto> getTasksForServices(final WorkItemQueryDto queryDto,
      final Predicate<WorkItemLightHDto> filter) throws RecordNotFoundException, IOException {


    // If tasks null throw Record Not Found Exception.
    final Call<List<WorkItemLightHDto>> retrofitTasks = taskRetrofitService.getTask(queryDto);

    if (retrofitTasks != null) {

      Stream<WorkItemLightHDto> stream = retrofitTasks.execute().body().stream().peek(Resources::inject);

      if (filter != null) {
        stream = stream.filter(filter);
      }
      return stream.collect(Collectors.toList());

    } else {
      throw new RecordNotFoundException(recordNotFoundForService, queryDto.getScheduledServiceId().get(0));
    }
  }

  /**
   * Returns list of checkList associated with the serviceId.
   *
   * @param serviceId the service unique identifier.
   * @return list of checklist.
   * @throws ClassDirectException if execution fails or API call fails.
   * @throws RecordNotFoundException if object not found for given unique identifier.
   * @throws IOException if API call fails.
   */
  public final List<WorkItemLightHDto> getCheckListByService(final Long serviceId)
      throws RecordNotFoundException, IOException {

    try {

      final List<Long> ids = Collections.singletonList(serviceId);

      // Set the id's to scheduledServiceId in the WorkItemQuery to get the tasks.
      final WorkItemQueryDto query = new WorkItemQueryDto();
      query.setScheduledServiceId(ids);
      query.setTaskTypeId(WorkItemType.CHECKLIST.value());
      return getTasksForServices(query, null);

    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
      throw ioe;
    }
  }

  /**
   * Returns paginated list of checkList associated with the serviceId.
   *
   * @param serviceId the service unique identifier.
   * @param page the number of pages to be returned.
   * @param size the size of elements to be returned.
   * @return paginated list of checklist.
   * @throws ClassDirectException if execution fails or API call fails.
   * @throws IOException if API call fails.
   */
  @Override
  public final PageResource<WorkItemLightHDto> getCheckListByService(final Long serviceId, final Integer page,
      final Integer size) throws ClassDirectException, IOException {
    LOGGER.debug("Querying task with page {} and size {}.", page, size);
    PageResource<WorkItemLightHDto> pageResource = null;

    pageResource = PaginationUtil.paginate(getCheckListByService(serviceId), page, size);

    return pageResource;
  }

  /**
   * Updates list of pmsCreditable tasks to pmsCredited.update pms_date value.
   *
   * @param serviceId the service unique identifier.
   * @param workItemLightListHDto the list of pmsCreditable tasks.
   * @return list of pmsCredited tasks.
   * @throws ClassDirectException if execution fails or API call fails.
   * @throws UnauthorisedAccessException if user is not allowed to update.
   */
  @Override
  public final WorkItemLightListHDto updateTasksAsList(final WorkItemLightListHDto workItemLightListHDto)
      throws ClassDirectException, UnauthorisedAccessException {
    WorkItemLightListHDto workItemLightListOut = new WorkItemLightListHDto();
    WorkItemLightListDto workItemLightList = new WorkItemLightListDto();
    try {
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      if (token != null) {
        UserProfiles user = userProfileService.getUser(token.getPrincipal());

        // prepares list with tasks to be credited.
        List<AuditPmsAbleTaskDto> auditablePmsTasksToBeCredited =
            TaskUtils.getTasksToBeCredited(workItemLightListHDto, true);

        // prepare old object for audit purpose.
        List<AuditPmsAbleTaskDto> tasks = TaskUtils.getTasksToBeCredited(workItemLightListHDto, false);

        List<WorkItemLightDto> workItemLightDtoList = workItemLightListHDto.getTasks().parallelStream()
            .map(TaskUtils::convertHdtoToDto).collect(Collectors.toList());
        workItemLightList.setTasks(workItemLightDtoList);
        // mast API call to put tasks.
        workItemLightList = taskRetrofitService.updateTaskList(workItemLightList).execute().body();
        if (workItemLightList.getStaleElements()) {
          throw new ClassDirectException(workItemLightList.getStalenessMessage());
        }

        List<WorkItemLightHDto> workItemLightHDtoList =
          workItemLightList.getTasks().parallelStream().map(TaskUtils::convertDtoToHDto).collect(Collectors.toList());
        workItemLightListOut.setTasks(workItemLightHDtoList);

        // audit log each task
        auditablePmsTasksToBeCredited.forEach(task -> {
          AuditPmsAbleTaskDto oldValue =
              tasks.stream().filter(oldTask -> oldTask.getId().equals(task.getId())).findFirst().get();
          auditService.audit(user, user, oldValue, task, UPDATE_MPMS_TASKS);
        });
      }
    } catch (final IOException exception) {
      final MastAPIException mex = ExceptionUtil.toMastException(exception);
      if (mex != null) {
        LOGGER.error("TaskServiceImpl.updateTaskList:" + exception.getMessage(), exception);
        throw new ClassDirectException(mex.getErrorCode(), mex.getMessage());
      } else {
        LOGGER.error("TaskServiceImpl.updateTaskList:" + exception.getMessage(), exception);
        throw new ClassDirectException(exception.getMessage());
      }
    }
    return workItemLightListOut;
  }

  @Override
  public final WorkItemPreviewListHDto getHierarchicalTasksByService(final Long serviceId, final Long assetId)
      throws ClassDirectException {
    final WorkItemPreviewListHDto tasks =
        getHierarchicalTasksByServiceIds(Collections.singletonList(serviceId), assetId);

    if (tasks.getServicesH() != null && !tasks.getServicesH().isEmpty()) {
      tasks.getServicesH().forEach(service -> {
        service.getItemsH().forEach(this::sortTasklistByTaskNumber);
      });
    }

    return tasks;
  }

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) list of
   * hierarchical tasks associated with the serviceId.
   *
   * @param serviceIds the service unique identifier.
   * @param assetId the asset unique identifier.
   * @return list DTO which has services with hierarchical tasks.
   * @throws ClassDirectException if execution fails or API call fails.
   */
  @Override
  public final WorkItemPreviewListHDto getHierarchicalTasksByServiceIds(final List<Long> serviceIds, final Long assetId)
      throws ClassDirectException {
    final WorkItemPreviewListHDto returnHydratedListDto = new WorkItemPreviewListHDto();
    // get services for given asset.
    List<ScheduledServiceHDto> services = getServicesByAssetId(assetId);

    if (services != null) {
      // get scheduled service object for given serviceId.
      services = services.parallelStream().filter(service -> serviceIds.contains(service.getId()))
          .collect(Collectors.toList());
      // get query DTO to query Mast for hierarchical tasks
      WorkItemQueryDto query = returnTaskQueryDto(serviceIds, null);
      // mast API call to get hierarchical tasks.
      final WorkItemPreviewListHDto htask = getHierarchicalTasks(query);

      // get continuous service ids.
      final Set<Long> continuousIds = new HashSet<>();
      getContinuousServiceIds(services, null, continuousIds);
      final Predicate<WorkItemLightHDto> filter = getContinuousServiceFilterTask(continuousIds);
      // services hierarchy
      List<WorkItemPreviewScheduledServiceHDto> returnHydratedServices = getFilterServices(htask, filter);
      // set hydrated hierarchical services.
      returnHydratedListDto.setServicesH(returnHydratedServices);
    } else {
      throw new RecordNotFoundException(recordNotFound, assetId);
    }
    return returnHydratedListDto;
  }


  /**
   * Returns hydrated services with valid hydrated items. 1
   *
   * @param serviceList the actual list DTO contains list of services.
   * @param filter he list of predicates need to apply to filter tasks.
   * @return the hydrated list of services.
   */
  private List<WorkItemPreviewScheduledServiceHDto> getFilterServices(final WorkItemPreviewListHDto serviceList,
      final Predicate<WorkItemLightHDto> filter) {
    List<WorkItemPreviewScheduledServiceHDto> hydratedServices = Collections.emptyList();
    if (serviceList.getServices() != null && !serviceList.getServices().isEmpty()) {
      hydratedServices = serviceList.getServices().stream().map(service -> {
        // convert mast dto to hydrated CD dto.
        WorkItemPreviewScheduledServiceHDto serviceH = new WorkItemPreviewScheduledServiceHDto();
        BeanUtils.copyProperties(service, serviceH);
        Resources.inject(serviceH);
        // filter first level items.
        filterItems(serviceH, filter);
        return serviceH;
      }).collect(Collectors.toList());
    }
    return hydratedServices;
  }

  /**
   * Returns list DTO.
   *
   * @param query the task query model object.
   * @return listDto from API call.
   * @throws ClassDirectException if API call or execution fails.
   */
  private WorkItemPreviewListHDto getHierarchicalTasks(final WorkItemQueryDto query) throws ClassDirectException {
    try {
      // mast API call to get hierarchical tasks.
      final Call<WorkItemPreviewListHDto> tasksCall = taskRetrofitService.getHierarchicalTask(query, Boolean.TRUE);
      final WorkItemPreviewListHDto listDto = tasksCall.execute().body();
      return listDto;
    } catch (IOException ioe) {
      throw new ClassDirectException(ioe);
    }
  }

  /**
   * Sets hierarchical hydrated items after next level item recursion process to hydrated service.
   *
   * @param service the service items which have to be filtered.
   * @param filter the condition to filter.
   */
  private void filterItems(final WorkItemPreviewScheduledServiceHDto service,
      final Predicate<WorkItemLightHDto> filter) {

    // segregation of first level items
    if (service.getItems() != null && !service.getItems().isEmpty()) {
      List<WorkItemModelItemHDto> serviceItems = service.getItems().stream().map(item -> {
        // convert mast dto to hydrated CD dto.
        WorkItemModelItemHDto itemH = new WorkItemModelItemHDto();
        BeanUtils.copyProperties(item, itemH);
        // filter for next level valid items.
        itemH = recursiveItem(itemH, filter);
        return itemH;
      }).filter(Objects::nonNull).collect(Collectors.toList());
      // set hydrated items to hydrated services.
      service.setItemsH(serviceItems);
      // set actual services to null.
      service.setItems(null);
    }
  }


  /**
   * Returns hydrated item with valid next level items.
   * <ol>
   * Next level item segregation.
   * <ol>
   * Items process
   * <li>Item will be converted to hydrated items.</li>
   * <li>check whether down level items exists.if yes then we will repeat the Item process through
   * recursion</li>
   * <li>apply filter to get not null Items</li>
   * <li>Set valid next level items to hydrated item list</li>
   * <li>Set actual next level items to null to avoid redundant data</li>
   * </ol>
   * <ol>
   * Tasks process
   * <li>Task list will be processed to get hydrated tasks</li>
   * <li>In process we apply filter to get continuous service tasks. For more information on see
   * {@link TaskServiceImpl#isContinuousTaskService(WorkItemLightHDto)}</li>
   * <li>Set valid tasks to hydrated task list</li>
   * <li>Set actual tasks to null to avoid redundant data</li>
   * </ol>
   * <ol>
   * If Items and Tasks are null or empty
   * <li>If the result Items process and Tasks process is null,Then there will not be a next level
   * items</li>
   * </ol>
   * </ol>
   *
   * @param item the next level item to be filtered for valid Items.
   * @param filter the filter condition to get tasks.
   * @return hydrated item with valid next level items.
   */
  private WorkItemModelItemHDto recursiveItem(final WorkItemModelItemHDto item,
      final Predicate<WorkItemLightHDto> filter) {
    // filter for next level valid items.
    if (item.getItems() != null) {
      List<WorkItemModelItemHDto> validItems = item.getItems().stream().map(anItem -> {
        WorkItemModelItemHDto itemH = new WorkItemModelItemHDto();
        BeanUtils.copyProperties(anItem, itemH);
        itemH = recursiveItem(itemH, filter);
        return itemH;
      }).filter(Objects::nonNull).collect(Collectors.toList());
      item.setItemsH(validItems);
      // set actual items to null.
      item.setItems(null);
    }

    // filter for valid tasks.
    if (item.getTasks() != null) {
      final List<WorkItemLightHDto> validTasks = getHydratedTasksByService(item.getTasks(), filter);
      item.setTasksH(validTasks);
      item.setTasks(null);
    }
    // remove the hierarchical items if next level items and tasks are empty.
    if ((item.getItemsH() == null || item.getItemsH().isEmpty())
        && (item.getTasksH() == null || item.getTasksH().isEmpty())) {
      return null;
    } else {
      return item;
    }

  }

  /**
   * Returns filtered(continuous services) or non filtered(non continuous services) list of
   * hierarchical tasks associated with the assetId using pmsAble filter. Filter for task has
   * service with continuous indicator as shown below.
   * <ol>
   * <li>Not started - Resolution status is null, Pms Credit date is null.</li>
   * <li>postponed not credited - Resolution status is postponed, Pms Credit date is null.</li>
   * <li>pms'ed not credited - Resolution status is null, Pms Credit date is null, PmsApplicable is
   * true.</li>
   * <li>exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X)).</li>
   * </ol>
   *
   *
   *
   * @param assetId the asset unique identifier.
   * @param pmsAble whether the task is pmsCreditable.
   * @return list of tasks.
   * @throws ClassDirectException if execution fails or fail to find object with given unique
   *         identifier.
   */
  @Override
  public final WorkItemPreviewListHDto getHierarchicalTasksByAssetId(final Long assetId, final Boolean pmsAble)
      throws ClassDirectException {
    final WorkItemPreviewListHDto returnHydratedListDto = new WorkItemPreviewListHDto();
    boolean pmsFilter = Optional.ofNullable(pmsAble).orElse(false);
    // get services for given asset.
    final List<ScheduledServiceHDto> services = getServicesByAssetId(assetId);

    final List<Long> serviceIds = Collections.synchronizedList(new ArrayList<>());
    final Set<Long> continuousIds = new HashSet<>();
    if (services != null) {
      getContinuousServiceIds(services, serviceIds, continuousIds);
      // query DTO.
      final WorkItemQueryDto query = returnTaskQueryDto(serviceIds, pmsFilter);

      // mast API call to get hierarchical tasks.
      final WorkItemPreviewListHDto htask = getHierarchicalTasks(query);
      final Predicate<WorkItemLightHDto> filter = pmsTasksFilter(continuousIds, pmsFilter);
      // services hierarchy
      List<WorkItemPreviewScheduledServiceHDto> returnHydratedServices = getFilterServices(htask, filter);
      if (returnHydratedServices != null && !returnHydratedServices.isEmpty()) {
        returnHydratedServices.forEach(service -> {
          if (service.getItemsH() != null && !service.getItemsH().isEmpty()) {
            service.getItemsH().forEach(this::sortTasklistByTaskNumber);
            service.setItemsH(sortTasklistByItemDisplayOrder(service.getItemsH()));
          }
        });
      }
      returnHydratedListDto.setServicesH(returnHydratedServices);
    } else {
      throw new RecordNotFoundException(recordNotFound, assetId);
    }
    return returnHydratedListDto;
  }

  /**
   * Sort task list by task number.
   *
   * @param item work item.
   */
  private void sortTasklistByTaskNumber(final WorkItemModelItemHDto item) {
    if (item.getTasksH() != null && !item.getTasksH().isEmpty()) {
      Collections.sort(item.getTasksH(), (dto1, dto2) -> {
        final int result;
        if ((dto1 != null && dto1.getTaskNumber() != null && !dto1.getTaskNumber().isEmpty())
            && (dto2 != null && dto2.getTaskNumber() != null && !dto2.getTaskNumber().isEmpty())) {
          final String taskNumber1 = dto1.getTaskNumber().replace("-", "");
          final String taskNumber2 = dto2.getTaskNumber().replace("-", "");

          result = taskNumber1.compareToIgnoreCase(taskNumber2);
        } else {
          result = 0;
        }
        return result;
      });
    }

    if (item.getItemsH() != null && !item.getItemsH().isEmpty()) {
      item.getItemsH().forEach(this::sortTasklistByTaskNumber);
    }
  }

  /**
   * Sort task list by items display order.
   *
   * @param itemsList itemsList.
   * @return List<WorkItemModelItemHDto>.
   */
  private List<WorkItemModelItemHDto> sortTasklistByItemDisplayOrder(final List<WorkItemModelItemHDto> itemsList) {
    Collections.sort(itemsList, (itemDto1, itemDto2) -> {
      return itemDto1.getDisplayOrder().compareTo(itemDto2.getDisplayOrder());
    });
    itemsList.forEach(items -> {
      if (items.getItemsH() != null && !items.getItemsH().isEmpty()) {
        sortTasklistByItemDisplayOrder(items.getItemsH());
      }
    });
    return itemsList;
  }

  /**
   * Returns root DTO which holds list of checklist grouped by it's group and subgroups.
   *
   * @param ckListItems the list of checklist items.
   * @return preview list DTO.
   * @throws RecordNotFoundException if checklist id not found for checklist item.
   */
  private HierarchyDto buildCheckListHierarchy(final List<WorkItemLightHDto> ckListItems)
      throws RecordNotFoundException {
    final HierarchyDto hierarchyContainer = new HierarchyDto();
    // build groups
    for (final WorkItemLightHDto ckListItem : ckListItems) {
      // from data base checklist Id is nullable field b'coz tasks will not have checklist id.
      // For checklist checklist Id is mandatory.
      buildRelationship(ckListItem, hierarchyContainer);
    }
    return hierarchyContainer;
  }

  /**
   * builds parent child relation ship for given checklist.
   *
   * @param ckItem the checklist item which should place under hierarchy.
   * @param container tree data structure to hold the lists.
   */
  private void buildRelationship(final WorkItemLightHDto ckItem, final HierarchyDto container) {

    ChecklistGroupDto groupDto = ckItem.getChecklistH().getGroupDto();
    ChecklistSubgroupDto subgroupDto = ckItem.getChecklistH().getSubGroupDto();

    List<WorkItemLightHDto> ckItems = container.get(groupDto).get(subgroupDto).getChecklistItems();
    ckItems.add(ckItem);

  }

  @Override
  public final HierarchyDto getCheckListHierarchyByService(final Long serviceId)
      throws RecordNotFoundException, ClassDirectException {
    HierarchyDto previewDto = null;
    try {
      List<WorkItemLightHDto> checkLists = getCheckListByService(serviceId);
      if (isValidCheckListItem(checkLists)) {
        previewDto = buildCheckListHierarchy(sortCheckListItem(checkLists));
      }
      // sort & build checklist hierarchy
    } catch (RecordNotFoundException re) {
      LOGGER.error("Failed to build checklist hierarchy", re);
      throw re;
    } catch (IOException e) {
      LOGGER.error("Failed to fetch checklist for serviceId : {}", serviceId);
      throw new ClassDirectException(e);
    }
    return previewDto;
  }

  @Override
  public final Map<ScheduledServiceHDto, List<WorkItemLightHDto>> getCheckListItemByService(
      final List<ScheduledServiceHDto> serviceList) throws RecordNotFoundException, ClassDirectException {

    Map<ScheduledServiceHDto, List<WorkItemLightHDto>> checkListMap =
        new HashMap<ScheduledServiceHDto, List<WorkItemLightHDto>>();
    final List<Callable<Void>> task = new ArrayList<>();

    serviceList.forEach(service -> {
      task.add(() -> {
        List<WorkItemLightHDto> checkListWorkItem = new ArrayList<WorkItemLightHDto>();
        try {
          List<WorkItemLightHDto> checkLists = getCheckListByService(service.getId());
          if (isValidCheckListItem(checkLists)) {
            // sort checklist Item
            checkListWorkItem = sortCheckListItem(checkLists);
            checkListWorkItem = checkListWorkItem.stream()
                .filter(checkListItem -> checkListItem.getResolutionStatusH() == null).collect(Collectors.toList());
            if (!checkListWorkItem.isEmpty()) {
              checkListMap.put(service, checkListWorkItem);
            }
          }
        } catch (IOException e) {
          LOGGER.error("Failed to fetch checklist for serviceId : {}", service.getId());
          throw new ClassDirectException(e);
        } catch (RecordNotFoundException re) {
          LOGGER.error("Failed to sort & filter checklist item", re);
          throw re;
        }
        return null;
      });
    });

    // invoke executor
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    try {
      executor.invokeAll(task);
    } catch (final InterruptedException e) {
      LOGGER.error("Executor error while getting outstanding checklist item", e);
      throw new ClassDirectException("Executor error while getting outstanding checklist item.");
    } finally {
      executor.shutdown();
    }
    return checkListMap;
  }

  /**
   * sorted the checklistItem by group displayOrder, subGroup displayOrder and Checklist item
   * reference code.
   *
   * @param ckListItems the checklist item list which will be sorted.
   * @return ckListItems sorted.
   */
  private List<WorkItemLightHDto> sortCheckListItem(final List<WorkItemLightHDto> ckListItems) {
    ckListItems.sort((cKItem1, cKItem2) -> {
      int compareRetVal = cKItem1.getChecklistH().getGroupDto().getDisplayOrder()
          .compareTo(cKItem2.getChecklistH().getGroupDto().getDisplayOrder());
      if (compareRetVal == 0) {
        compareRetVal = cKItem1.getChecklistH().getSubGroupDto().getDisplayOrder()
            .compareTo(cKItem2.getChecklistH().getSubGroupDto().getDisplayOrder());
        if (compareRetVal == 0) {
          compareRetVal = ServiceUtils.sortItembyReferenceCode(cKItem1.getReferenceCode(), cKItem2.getReferenceCode());
        }
      }
      return compareRetVal;
    });
    return ckListItems;
  }

  /**
   * validate the checklist item list for emplty list and null value for checklist .
   *
   * @param ckListItems the checklist item list which will be validated.
   * @throws RecordNotFoundException if checklist id not found for checklist item.
   * @return boolean value.
   */
  private boolean isValidCheckListItem(final List<WorkItemLightHDto> ckListItems) throws RecordNotFoundException {
    if (!ckListItems.isEmpty()) {
      for (final WorkItemLightHDto ckListItem : ckListItems) {
        // from data base checklist Id is nullable field b'coz tasks will not have checklist id.
        // For checklist checklist Id is mandatory.
        if (ckListItem.getChecklist() != null) {
          continue;
        } else {
          LOGGER.error("Checklist Id not found for checklistItem : {}", ckListItem.getId());
          throw new RecordNotFoundException(checklistIdNotFound, ckListItem.getId());
        }
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Provides methods to compare string and sorting of strings by ascending order.
   *
   * @author sbollu
   *
   */
  static class ChecklistComparator implements Comparator<WorkItemLightHDto>, Serializable {

    /**
     * The generated serial version UID.
     */
    private static final long serialVersionUID = 3242634557479447579L;

    @Override
    public int compare(final WorkItemLightHDto checklist1, final WorkItemLightHDto checklist2) {

      String firstString = checklist1.getReferenceCode();
      String secondString = checklist2.getReferenceCode();

      if (firstString != null && secondString != null && firstString.equals(secondString)) {
        return 0;
      }

      if (firstString == null) {
        return -1;
      }
      if (secondString == null) {
        return 1;
      }
      return firstString.compareTo(secondString);
    }
  }

  /**
   * Provides methods to compare string and sorting of strings by ascending order.
   *
   * @author sbollu
   *
   */
  static class ChecklistRefDataComparator implements Comparator<ReferenceDataDto>, Serializable {

    /**
     * The generated serial version UID.
     */
    private static final long serialVersionUID = 5050563299391317832L;

    @Override
    public int compare(final ReferenceDataDto data1, final ReferenceDataDto data2) {

      String firstString = data1.getName();
      String secondString = data2.getName();

      if (firstString != null && secondString != null && firstString.equals(secondString)) {
        return 0;
      }

      if (firstString == null) {
        return -1;
      }
      if (secondString == null) {
        return 1;
      }
      return firstString.compareTo(secondString);
    }

  }
}
