package com.baesystems.ai.lr.cd.service.audit;

/**
 * Provides service for audit management, checks old value and new value difference, gets the field
 * value that get changed.
 *
 * @author VKolagutla
 * @author syalavarthi 15-06-2017.
 *
 * @param <T> auditHelper the type of object that get updated.
 */
public abstract class AuditHelper<T> {

  /**
   * Checks old object and new object difference.
   *
   * @param oldObject the old value of target user.
   * @param newObject the new value of target user.
   * @return true if changes in old object and with new object.
   */
  public abstract boolean isMatchAuditScenario(T oldObject, T newObject);

  /**
   * Gets the field value that get changed.
   *
   * @param object the updated object.
   * @return field value that get updated.
   */
  public abstract String getUpdateField(T object);

}
