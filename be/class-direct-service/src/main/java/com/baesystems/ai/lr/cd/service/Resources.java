package com.baesystems.ai.lr.cd.service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.annotation.DueStatusField;
import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;


/**
 * Context aware resource injector.
 *
 * @author Faizal Sidek
 */
@Component
public class Resources implements ApplicationContextAware, InitializingBean {
  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(Resources.class);

  /**
   * package.
   */
  private static final String PACKAGE = "com.baesystems.ai.lr.cd";

  /**
   * The threshold in miliseconds to print a warn message if resource inject taking longer than this value.
   */
  private static final long WARN_ELAPSE_TIME = 1000L;


  /**
   * Maximum parameter count.
   */
  private static final Integer MAX_PARAM_COUNT = 1;

  /**
   * Clean up thread that idle for more than configured time Only keep the minimum thread pool size.
   */
  private static final long THREAD_IDLE_TIMEOUT_SECOND = 60;

  /**
   * The thread ratio value to determine to disable using of executor.
   */
  private static final int DISABLE_THREAD_RATIO = 0;

  /**
   * Actual injector.
   */
  private static Resources injector;


  /**
   * Spring application context.
   */
  private ApplicationContext context;

  /**
   * Injected resource providers.
   */
  @Autowired
  private ResourceProviderScanner providers;

  /**
   * error message.
   */
  @Value("${thread.per.core.ratio}")
  private int threadPerCoreRatio;

  /**
   * The total thread per call for resource injection executor service.
   */
  @Value("${resourcesinject.thread.per.core.ratio}")
  private int executorThreadPerCoreRatio;

  /**
   * Long run threadpool for resource injection.
   */
  private ExecutorService resourcesInjectExecutor;

  /**
   * Set of DueStatusCalculator implementation classes.
   */
  @SuppressWarnings("rawtypes")
  private Set<Class<? extends DueStatusCalculator>> dueStatusCalculators;

  /**
   * Method name for due status calculator interface.
   */
  private static final String DUE_STATUS_CALCULATOR_METHOD = "calculateDueStatus";

  /**
   * Helper method to inject dependencies.
   *
   * @param dao to be injected.
   */
  public static final void inject(final Object dao) {
    if (null != injector) {
      long elapsedTime = System.currentTimeMillis();
      injector.doInject(dao);
      elapsedTime = System.currentTimeMillis() - elapsedTime;
      if (elapsedTime > WARN_ELAPSE_TIME) {
        LOGGER.warn("Resource inject on {} is taking {} ms.", dao.getClass().getSimpleName(), elapsedTime);
      }
    } else {
      LOGGER.error("Injector is not set. Have you created Resources in spring context?");
    }
  }


  @Override
  public final void afterPropertiesSet() {
    final Resources resObj = context.getBean(Resources.class);
    resObj.initExecutor();
    resObj.initDueStatusCalculator();
    setInjector(resObj);
  }

  /**
   * Set injector.
   *
   * @param resources injector.
   */
  public static final void setInjector(final Resources resources) {
    Resources.injector = resources;
  }

  /**
   * method to setup executor pool for resource injection.
   */
  public void initExecutor() {
    try {
      if (executorThreadPerCoreRatio == DISABLE_THREAD_RATIO) {
        LOGGER.info("Detected zero thread ratio, going to skip thread pool setup.");
        resourcesInjectExecutor = null;
      } else {
        final int totalThread = executorThreadPerCoreRatio * CDConstants.AVAILABLE_PROCESSORS;
        LOGGER.info("Setup Resource Injector thread pool with {} threads.", totalThread);
        resourcesInjectExecutor = Executors.newWorkStealingPool(totalThread);
        /*
        resourcesInjectExecutor = ExecutorUtils.newThreadPoolExecutor(CDConstants.AVAILABLE_PROCESSORS,
            executorThreadPerCoreRatio * CDConstants.AVAILABLE_PROCESSORS, THREAD_IDLE_TIMEOUT_SECOND
            , TimeUnit.SECONDS, this.getClass().getSimpleName());
        */
      }
    } catch (final Exception e) {
      LOGGER.error("Fail to setup resourcesInjectExecutor.", e);
    }
  }

  /**
   * Method to scan package for DueStatusCalculator.
   */
  public void initDueStatusCalculator() {
    final Reflections reflections = new Reflections(PACKAGE);
    dueStatusCalculators = reflections.getSubTypesOf(DueStatusCalculator.class);
    LOGGER.debug("Total {} DueStatusCalculator implementations", dueStatusCalculators.size());
  }

  /**
   * Cleanup code when shutdown.
   */
  @PreDestroy
  public void cleanup() {
    LOGGER.info("Cleanup executor...");
    if (resourcesInjectExecutor != null) {
      resourcesInjectExecutor.shutdown();
    }
  }

  /**
   * Recursively lookup field with superclasses.
   *
   * @param fieldName field lookup name.
   * @param clazz class to be lookup.
   * @return field.
   * @throws NoSuchFieldException if no field found.
   */
  private Field lookUpField(final String fieldName, final Class<?> clazz) throws NoSuchFieldException {
    final Class<?> superClass = clazz.getSuperclass();

    if (superClass.getSimpleName().equalsIgnoreCase("Object")) {
      LOGGER.debug("Reached end of ancestors tree");
      throw new NoSuchFieldException(fieldName);
    }

    final Optional<Field> field = Stream.of(superClass.getDeclaredFields())
        .filter(fiel -> fiel.getName().equalsIgnoreCase(fieldName)).findFirst();

    Field returnField = null;

    if (field.isPresent()) {
      returnField = field.get();
    } else {
      returnField = lookUpField(fieldName, superClass);
    }

    return returnField;
  }

  /**
   * Actual method to inject dependencies.
   *
   * @param dao dao to be populated.
   */
  private void doInject(final Object dao) {
    final Class<?> clazz = dao.getClass();

    final List<Callable<Void>> tasks = new ArrayList<>();

    // saving due status fields in a list to avoid looping twice on all the fields.
    final List<Field> dueStatusFields = new ArrayList<>();

    for (Field field : clazz.getDeclaredFields()) {
      // Checking against the two custom annotations, @linkedResource & @DueStatusField
      if (field.isAnnotationPresent(LinkedResource.class)) {
        tasks.add(() -> {
          injectLinkResource(dao, field);
          return null;
        });
      } else if (field.isAnnotationPresent(DueStatusField.class)) {
        dueStatusFields.add(field);
      }
    }

    if (!tasks.isEmpty()) {
      if (resourcesInjectExecutor != null) {
        try {
          resourcesInjectExecutor.invokeAll(tasks);
        } catch (InterruptedException exception) {
          LOGGER.error("Error executing Resource inject in threadpool.", exception);
        }
      } else {
        tasks.forEach(task -> {
          try {
            task.call();
          } catch (final Exception exception) {
            LOGGER.error("Error executing Resource inject in caller thread.", exception);
          }
        });
      }
    }
    // due to race condition we have to inject duestatus after injecting link resources.
    if (!dueStatusFields.isEmpty()) {
      dueStatusFields.forEach(field -> {
        injectDueStatus(dao, field, this.dueStatusCalculators);
      });
    }
  }

  /**
   * Inject link resource on model.
   *
   * @param dao object.
   * @param field attribute annotated with LinkResource.
   */
  private void injectLinkResource(final Object dao, final Field field) {
    final Class<?> clazz = dao.getClass();

    LOGGER.trace("Field {} from class {} is annotated with @LinkedResource. Attempting to inject.", field.getName(),
        dao.getClass());
    try {
      final LinkedResource meta = field.getAnnotation(LinkedResource.class);
      final Field idField = lookUpField(meta.referencedField(), clazz);
      AccessController.doPrivileged(new SetFieldAccessibleAction(idField));
      if (idField.get(dao) == null) {
        LOGGER.debug("ID for linked DTO is null. Skipping.");
        return;
      }

      LOGGER.debug("Getting value from idField {}, idField type is {}", idField.getName(),
          idField.getType().getSimpleName());
      Long id = null;
      if (idField.get(dao) instanceof LinkResource) {
        final LinkResource linkResource = (LinkResource) idField.get(dao);
        id = linkResource.getId();
      } else if (idField.get(dao) instanceof Long) {
        id = (Long) idField.get(dao);
      } else {
        LOGGER.error("Unsupported field type {}", idField.getType().getSimpleName());
        return;
      }
      final Class<?> type = field.getType();

      final Set<TypeProvider> serviceProviders = lookup(type);
      if (serviceProviders.isEmpty()) {
        LOGGER.error("Providers for type {} not found.", type.getSimpleName());
        return;
      }

      final Optional<TypeProvider> specificProvider = serviceProviders.stream()
          .filter(provider -> provider.getMethod().getParameterCount() == MAX_PARAM_COUNT).findAny();
      final Optional<TypeProvider> genericProvider =
          serviceProviders.stream().filter(provider -> provider.getMethod().getParameterCount() == 0).findAny();

      Object result = null;
      if (specificProvider.isPresent()) {
        LOGGER.trace("Searching with specific id.");
        final TypeProvider provider = specificProvider.get();
        result = executeMethod(provider.getBeanName(), provider.getMethod(), new Object[] {id});
      } else if (genericProvider.isPresent()) {
        LOGGER.trace("Searching while iterating the object");
        final TypeProvider provider = genericProvider.get();
        result = executeMethod(provider.getBeanName(), provider.getMethod(), new Object[] {});
      } else {
        LOGGER.debug("No providers found, skipping.");
        return;
      }

      if (result == null) {
        LOGGER.debug("No result found, proceed with next annotation.");
        return;
      }
      injectFields(result, id, field, dao);
    } catch (SecurityException | IllegalAccessException exception) {
      LOGGER.error("Cannot access reflected class. Continue with next field.", exception);
    } catch (IllegalArgumentException | InvocationTargetException exception) {
      LOGGER.error("Unable to invoke method.", exception);
    } catch (NoSuchFieldException | NoSuchMethodException exception) {
      LOGGER.error("Method not found.", exception);
    } finally {
      LOGGER.debug("Field {} from class {} is annotated with @LinkedResource. Finish processing.", field.getName(),
          dao.getClass());
    }
  }


  /**
   * @param dao obj.
   * @param field field.
   * @param implementations list of CaculateDueStatus implementation classes.
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  private void injectDueStatus(final Object dao, final Field field,
      final Set<Class<? extends DueStatusCalculator>> implementations) {
    LOGGER.trace("Calculating due status with {} implementations.", implementations.size());
    implementations.stream().filter(implementation -> {
      // we don't want interfaces as we're going to instantiate the class.
      if (implementation.isInterface()) {
        return false;
      }
      try {
        // getting the method
        final Optional<Method> method = Optional
            .ofNullable(MethodUtils.getAccessibleMethod(implementation, DUE_STATUS_CALCULATOR_METHOD, dao.getClass()));

        return method.isPresent();
      } catch (final SecurityException exception) {
        return false;
      }
    }).findFirst().ifPresent(implementation -> {
      try {
        LOGGER.trace("Selected implementation class: {}", implementation.getSimpleName());

        // create our duestatus calculator
        final DueStatusCalculator calculator = implementation.newInstance();

        final String getMethod = "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
        // find the get method for due status
        final Class clazz = dao.getClass();
        final Method method = clazz.getDeclaredMethod(getMethod, DueStatus.class);

        // Calling the actual method
        if (method != null) {
          method.invoke(dao, calculator.calculateDueStatus(dao));
        }
      } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
          | NoSuchMethodException | SecurityException exception) {
        LOGGER.error("Error calculating due status for {} using implementation class: {}"
            , dao.getClass().getSimpleName(), implementation.getSimpleName(), exception);
      } finally {
        LOGGER.debug("Finish calculating due status for {} using implementation class: {}"
            , dao.getClass().getSimpleName(), implementation.getSimpleName());
      }
    });

  }

  /**
   * Invoke method with given parameter.
   *
   * @param beanRef spring bean reference.
   * @param invoker method invoker.
   * @param params parameters to be passed.
   * @return method result.
   * @throws IllegalAccessException when access to method is denied.
   * @throws IllegalArgumentException when method does not expect specified parameters.
   * @throws InvocationTargetException when unable to invoke the method.
   * @throws NoSuchMethodException when method not found.
   */
  private Object executeMethod(final String beanRef, final Method invoker, final Object[] params)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
    final Object service = context.getBean(beanRef);
    final Method realInvoker = service.getClass().getMethod(invoker.getName(), invoker.getParameterTypes());
    return realInvoker.invoke(service, params);
  }

  /**
   * Inject the fields with result.
   *
   * @param result to be injected.
   * @param id of dao.
   * @param field field to be injected.
   * @param dao of injected object.
   */
  private void injectFields(final Object result, final Long id, final Field field, final Object dao) {
    Object finalResult = null;
    if (result instanceof Collection) {
      LOGGER.trace("Result is a collection, iterating...");
      @SuppressWarnings("rawtypes")
      final Collection resources = (Collection) result;
      LOGGER.trace("Size of resources is {}.", resources.size());

      for (final Object resource : resources) {
        if (resource instanceof BaseDto) {
          final BaseDto linkedDto = (BaseDto) resource;
          if (linkedDto.getId().equals(id)) {
            LOGGER.debug("Found linked DTO with id {}. Attempting to inject the field : {} - {}.", id
                , dao.getClass().getSimpleName(), field.getName());
            finalResult = linkedDto;
          }
        }
      }
      if (finalResult == null) {
        LOGGER.debug("Cannot find linked DTO with id {} for field : {} - {}.", id
            , dao.getClass().getSimpleName(), field.getName());
      }
    } else {
      LOGGER.debug("Found single linked DTO with id {}. Attempting to inject the field : {} - {}.", id
            , dao.getClass().getSimpleName(), field.getName());
      finalResult = result;
    }
    setField(field, dao, finalResult);
  }

  /**
   * Set the field.
   *
   * @param field field to be set.
   * @param dao object dao.
   * @param result result to be injected.
   */
  private void setField(final Field field, final Object dao, final Object result) {
    AccessController.doPrivileged(new SetFieldValueAction(field, dao, result));
  }

  /**
   * Lookup type for service bean and method.
   *
   * @param type of dto
   * @return entry of bean and method.
   * @throws NoSuchMethodException if no method found.
   */
  private Set<TypeProvider> lookup(final Class<?> type) {
    LOGGER.debug("Lookup service-method for type {}.", type.getSimpleName());

    return providers.getAllProviders().stream()
        .filter(provider -> provider.getType().getSimpleName().equals(type.getSimpleName()))
        .collect(Collectors.toSet());
  }

  /**
   * Set application context.
   *
   * @param ctx application context.
   */
  @Override
  public final void setApplicationContext(final ApplicationContext ctx) throws BeansException {
    LOGGER.debug("Setting up application context.");
    context = ctx;
  }

  /**
   * Inner class of type {@link PrivilegedAction}.
   *
   * @author MSidek
   */
  public static class SetFieldAccessibleAction implements PrivilegedAction<Field> {

    /**
     * Field of bean to set.
     */
    private final Field field;

    /**
     * Default constructor.
     *
     * @param fieldToBeSet field of object.
     */
    public SetFieldAccessibleAction(final Field fieldToBeSet) {
      this.field = fieldToBeSet;
    }

    @Override
    public Field run() {
      field.setAccessible(true);

      return field;
    }
  }


  /**
   * Inner class of type {@link PrivilegedAction}.
   *
   * @author MSidek
   */
  private static class SetFieldValueAction extends SetFieldAccessibleAction {

    /**
     * Main object.
     */
    private final Object object;

    /**
     * Value to be set.
     */
    private final Object value;

    /**
     * Default constructor.
     *
     * @param fieldToBeSet object field.
     * @param objectToBeSet object itself.
     * @param valueToBeSet value to be set on field.
     */
    public SetFieldValueAction(final Field fieldToBeSet, final Object objectToBeSet, final Object valueToBeSet) {
      super(fieldToBeSet);
      this.object = objectToBeSet;
      this.value = valueToBeSet;
    }

    @Override
    public final Field run() {
      final Field field = super.run();
      try {
        field.set(object, value);
      } catch (IllegalArgumentException | IllegalAccessException exception) {
        LOGGER.error("Unable to set value.", exception);
      }

      return field;
    }
  }
}
