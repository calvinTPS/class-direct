package com.baesystems.ai.lr.cd.service.security;

import com.baesystems.ai.lr.cd.be.domain.dto.security.RolesDto;

/**
 * @author vkovuru
 *
 */
public interface SecurityService {

  /**
   * @param userName user name from header.
   * @param roles roles.
   * @return boolean.
   */
  boolean hasRoles(final String userName, final RolesDto roles);
}
