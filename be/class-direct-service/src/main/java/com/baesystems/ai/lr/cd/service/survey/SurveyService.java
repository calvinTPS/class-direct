package com.baesystems.ai.lr.cd.service.survey;

import java.io.IOException;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.survey.SurveyRequestDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * Validates Survey Request and sends mail to corresponding requested user and asset CFO.
 *
 * @author NAvula
 * @author SBollu
 *
 */
public interface SurveyService {
  /**
   * Returns success or failure status message for corresponding request survey of an asset.
   *
   * @param userId the user unique identifier.
   * @param dto the survey request model object.
   * @return success or failure status message.
   * @throws ClassDirectException if validation fails for date and email or any execution failure.
   * @throws IOException if API call fails.
   * @throws RecordNotFoundException if fail to find any object with given unique identifier.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StatusDto validateSurveyRequest(final String userId, SurveyRequestDto dto)
      throws ClassDirectException, IOException, RecordNotFoundException;

}
