package com.baesystems.ai.lr.cd.be.service.service.reference;

import java.io.IOException;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.references.MmsServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceGroupHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;

/**
 * Provides service to fetch scheduled services for an asset and it relevant reference data
 * including service credit status, service catalogue and service groups.
 *
 * @author VMandalapu
 * @author syalavarthi 13-06-2017.
 *
 */
public interface ServiceReferenceService extends DueStatusCalculator<ScheduledServiceHDto> {

  /**
   * Gets list of services for an asset.
   *
   * @param assetId the primary key of an asset.
   * @return list of services.
   * @throws IOException if mast api fails to get list of services for asset.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  List<ScheduledServiceHDto> getServicesForAsset(final Long assetId)
      throws IOException;

  /**
   * Gets list of services for an asset.
   *
   * @param assetId the primary key of an asset.
   * @param query the query to filter services.
   * @return list of services.
   * @throws IOException if mast api fails to get list of services for asset.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  List<ScheduledServiceHDto> getServices(final Long assetId, final ServiceQueryDto query)
      throws IOException;

  /**
   * Gets list of service credit statuses.
   *
   * @return list of service credit statuses.
   */
  List<ServiceCreditStatusHDto> getServiceCreditStatuses();

  /**
   * Gets service credit status by id.
   *
   * @param id the primary key of service credit status.
   * @return service credit status.
   */
  ServiceCreditStatusHDto getServiceCreditStatus(final Long id);

  /**
   * Gets list of service catalogues including the deleted flag equals to true.
   *
   * @return list of service catalogues.
   */
  List<ServiceCatalogueHDto> getServiceCatalogues();

  /**
   * Gets list of service catalogues without deleted flag.
   *
   * @return list of service catalogues.
   */
  List<ServiceCatalogueHDto> getActiveServiceCatalogues();

  /**
   * Gets list of MMS service catalogues.
   * This method is purposely use in asset mast-and-ihs-query to fetch MMS related asset.
   *
   * @return list of MMS service catalogues.
   */
  List<MmsServiceCatalogueHDto> getMmsServiceCatalogues();

  /**
   * Gets service catalogue by id.
   *
   * @param id the service catalogue primary key..
   * @return service catalogue.
   */
  ServiceCatalogueHDto getServiceCatalogue(final Long id);

  /**
   * Gets list of service groups.
   *
   * @return service groups.
   * @throws IOException if mast api call fail.
   */
  List<ServiceGroupHDto> getServiceGroups() throws IOException;

  /**
   * Gets a service group by id..
   *
   * @param id the primary key of service group.
   * @return service group.
   * @throws IOException if mast api call fail.
   */
  ServiceGroupHDto getServiceGroup(Long id) throws IOException;

}
