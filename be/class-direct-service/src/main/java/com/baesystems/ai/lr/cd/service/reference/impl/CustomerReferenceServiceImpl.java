package com.baesystems.ai.lr.cd.service.reference.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import retrofit2.Call;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.reference.CustomerReferenceService;
import com.jcabi.aspects.Loggable;

/**
 * Provides service layer to get customer related reference data such as country.
 *
 * @author YWearn
 *
 */
@Service(value = "CustomerReferenceService")
@Loggable(Loggable.DEBUG)
public class CustomerReferenceServiceImpl implements CustomerReferenceService, InitializingBean {

  /**
   * The application logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerReferenceService.class);

  /**
   * The spring context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The asset reference api service.
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceDelegate;

  /**
   * The instance of this service object from Spring context.
   */
  private CustomerReferenceService self = null;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(CustomerReferenceService.class);
  }

  @ResourceProvider
  @Override
  public final List<CountryHDto> getCountries() throws ClassDirectException {
    List<CountryHDto> countryList = new ArrayList<>();
    try {
      final Call<List<CountryHDto>> caller = assetReferenceDelegate.getCountryList();
      countryList = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Error while get country reference data from MAST API.", ioe);
    }
    return countryList;
  }

  @ResourceProvider
  @Override
  public final CountryHDto getCountry(final long countryId) throws ClassDirectException {
    return self.getCountries().stream()
            .filter(country -> country.getId().equals(countryId))
            .findFirst()
            .orElse(null);
  }

}
