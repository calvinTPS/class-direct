package com.baesystems.ai.lr.cd.service.userprofile.helper;

import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.LREmailDomainsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.LREmailDomains;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import static com.baesystems.ai.lr.cd.be.utils.ExecutorUtils.newFixedThreadPool;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * @author fwijaya on 16/5/2017.
 */
public abstract class BaseSSOProvider implements SSOUserProvider, InitializingBean {

  /**
   * Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(BaseSSOProvider.class);

  /**
   * A list of LR email domains.
   */
  @Getter
  private List<String> lREmailDomains = new ArrayList<>();

  /**
   * LR Email domains DAO.
   */
  @Autowired
  private LREmailDomainsDao lrEmailDomainsDao;

  /**
   * @throws Exception exceptions.
   */
  @Override
  public final void afterPropertiesSet() throws Exception {
    final List<LREmailDomains> result = lrEmailDomainsDao.getLrEmailDomains();
    lREmailDomains = result.stream().map(domain -> domain.getDomain()).collect(Collectors.toList());
  }

  /**
   * Fetches user from azure then executes the callback.
   *
   * @param userProfiles list of user profiles.
   * @param callback callback.
   * @return the sso user data.
   * @throws ClassDirectException exception.
   */
  @Override
  public final List<LRIDUserDto> fetchUsers(final List<UserProfiles> userProfiles,
                                            final UpdateUserCallback callback) throws ClassDirectException {
    final Map<String, UserProfiles> usersMap =
      userProfiles.stream().filter(userProfile -> isMatch(userProfile.getEmail()))
        .collect(toMap(UserProfiles::getEmail, identity()));
    final List<LRIDUserDto> userDtoList = new ArrayList<>();
    final List<String> userEmails =
      usersMap.values().stream().map(userProfile -> userProfile.getEmail()).collect(Collectors.toList());
    final List<Callable<Void>> tasks = new ArrayList<>();
    for (final List<String> emails : Lists.partition(userEmails, getQueryLimit())) {
      tasks.add(() -> {
        final String formattedEmails = UserProfileServiceUtils.getFormattedEmail(emails, getEmailFilter());
        final List<LRIDUserDto> ssoProfiles = getSSOUser(formattedEmails);
        userDtoList.addAll(ssoProfiles);
        ssoProfiles.forEach(ssoProfile -> {
          if (ssoProfile.getSignInName() != null && !ssoProfile.getSignInName().isEmpty()) {
            UserProfiles userProfile = usersMap.get(ssoProfile.getSignInName());
            if (userProfile != null) {
              userProfile.setCdUser(true);
              LOGGER.debug("SSO UserProfile : {}", ssoProfile);
              try {
                callback.update(userProfile, ssoProfile);
              } catch (final ClassDirectException classDirectException) {
                LOGGER.error("Error occured while updating {} user information in database", userProfile.getName(),
                             classDirectException);
              }
            } else {
              LOGGER.debug("User not found in SSO for email : {}", ssoProfile.getSignInName());
            }
          }
        });
        return null;
      });
    }
    if (tasks.size() == 1) {
      try {
        tasks.get(0).call();
      } catch (Exception exception) {
        LOGGER.error(exception.getMessage());
        throw new ClassDirectException(exception);
      }
    } else if (tasks.size() > 1) {
      final ExecutorService executor = newFixedThreadPool(this.getClass().getSimpleName());
      try {
        executor.invokeAll(tasks);
      } catch (final InterruptedException interruptedException) {
        LOGGER.error(interruptedException.getMessage());
        throw new ClassDirectException(interruptedException);
      } finally {
        executor.shutdown();
      }
    }
    return userDtoList;
  }

  /**
   * Fetches sso user for a given email.
   *
   * @param email email.
   * @return SSO user data from azure.
   * @throws ClassDirectException exceptions.
   */
  @Override
  public final List<LRIDUserDto> fetchUsers(final String email) throws ClassDirectException {
    final String formattedEmails =
      UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(email), getEmailFilter());
    final List<LRIDUserDto> result = getSSOUser(formattedEmails);
    return result;
  }

  /**
   * Returns query limit.
   *
   * @return query limit.
   */
  protected abstract int getQueryLimit();

  /**
   * Returns email filter.
   *
   * @return email filter.
   */
  protected abstract String getEmailFilter();

  /**
   * Returns SSO user for given formatted emails.
   *
   * @param formattedEmails formatted emails.
   * @return SSO users.
   */
  protected abstract List<LRIDUserDto> getSSOUser(String formattedEmails);

}
