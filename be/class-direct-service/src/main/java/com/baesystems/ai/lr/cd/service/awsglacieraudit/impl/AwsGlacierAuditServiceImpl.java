package com.baesystems.ai.lr.cd.service.awsglacieraudit.impl;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AwsGlacierAuditDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.AwsGlacierAudit;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.awsglacieraudit.AwsGlacierAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author fwijaya on 6/3/2017.
 */
@Service("AwsGlacierAuditService")
public class AwsGlacierAuditServiceImpl implements AwsGlacierAuditService {
  /**
   * Injected dao.
   */
  @Autowired
  private AwsGlacierAuditDao awsGlacierAuditDao;

  /**
   * Store information regarding the file archived in AWS Glacier into classdirect DB.
   *
   * @param glacierArchiveId AWS Glacier archive id.
   * @param glacierArchiveVault AWS Glacier vault name.
   * @param downloadRequestUrl Download url.
   * @param exportTime Export time.
   * @param userId User id.
   * @return the {@link AwsGlacierAudit} object saved to db.
   * @throws ClassDirectException exception.
   */
  @Override
  public final AwsGlacierAudit storeArchive(final String glacierArchiveId, final String glacierArchiveVault,
    final String downloadRequestUrl, final LocalDateTime exportTime, final String userId)
    throws ClassDirectException {
    return awsGlacierAuditDao
      .storeArchive(glacierArchiveId, glacierArchiveVault, downloadRequestUrl, exportTime, userId);
  }

  /**
   * Find {@link AwsGlacierAudit} by its archive id and vault name.
   *
   * @param glacierArchiveId archive id.
   * @param glacierArchiveVault vault name.
   * @return {@link AwsGlacierAudit}.
   * @throws ClassDirectException exception.
   */
  @Override
  public final AwsGlacierAudit findAuditByArchiveIdAndVaultName(final String glacierArchiveId, final String
    glacierArchiveVault)
    throws RecordNotFoundException {
    try {
      final AwsGlacierAudit awsGlacierAudit =
        awsGlacierAuditDao.findAuditByArchiveIdAndVaultName(glacierArchiveId, glacierArchiveVault);
      return awsGlacierAudit;
    } catch (ClassDirectException exception) {
      throw new RecordNotFoundException(exception.getMessage());
    }
  }
}
