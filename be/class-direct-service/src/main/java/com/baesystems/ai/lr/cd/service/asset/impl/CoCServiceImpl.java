package com.baesystems.ai.lr.cd.service.asset.impl;

import static com.baesystems.ai.lr.cd.be.utils.DateUtils.getLocalDateToDate;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCServiceRangeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RepairHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ServiceScheduleDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.enums.CodicilStatus;
import com.baesystems.ai.lr.enums.FaultStatus;
import com.jcabi.aspects.Loggable;
import retrofit2.Call;

/**
 * Provides service for querying condition of class.
 *
 * @author yng
 *
 */
@Service(value = "CoCService")
@Loggable(Loggable.DEBUG)
public class CoCServiceImpl implements CoCService {
  /**
   * The logger instantiated for {@link CoCServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CoCServiceImpl.class);

  /**
   * The {@link AssetRetrofitService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * The {@link ServiceReferenceRetrofitService} from Spring context.
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceRefDelegate;

  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetRefDelegate;

  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The default year range for next due date.
   */
  private static final int DEFAULT_YEAR_RANGE = 7;

  /**
   * The low range key name for asset service scheduled range.
   */
  private static final String LOW_RANGE = "lowRange";

  /**
   * The high range key name for asset service scheduled range.
   */
  private static final String HIGH_RANGE = "highRange";

  /**
   * The due soon threshold default value in month.
   */
  private static final Long DUE_SOON_TH = 3L;

  /**
   * The imminent threshold default value in month.
   */
  private static final Long IMMINENT_TH = 1L;

  /**
   * The default lower range in month.
   */
  private static final int DEFAULT_LOWER_RANGE = 30;
  /**
   * The service status ids list to calculte time scale for asset.
   */
  @Value("#{'${overallstatus.service_status_id}'.split(',')}")
  private List<Long> assetServiceStatusList;

  /**
   * The coc status ids list to calculate time scale for asset.
   */
  @Value("#{'${overallstatus.coc_status_id}'.split(',')}")
  private List<Long> cocStatusList;

  /**
   * The actionable item status ids list to calculate time scale for asset.
   */
  @Value("#{'${overallstatus.actionableitem_status_id}'.split(',')}")
  private List<Long> actionableItemStatusList;

  /**
   * The statutory finding status ids list to calculate time scale for asset.
   */
  @Value("#{'${overallstatus.statutory_finding_status_id}'.split(',')}")
  private List<Long> statutoryFindingStatusIdList;

  @RestrictedAsset
  @Override
  public final PageResource<CoCHDto> getCoC(final Integer page, final Integer size, @AssetID final Long assetId)
      throws ClassDirectException {
    PageResource<CoCHDto> pageResource = null;
    pageResource = PaginationUtil.paginate(getCoC(assetId), page, size);

    return pageResource;
  }

  /**
   * Returns the condition of class for export scenario where it is optimize to fetch only open
   * condition of class and open defect information.
   *
   * @param assetId the asset identifier
   * @return the condition of class detail and associated defect detail (if any).
   * @throws ClassDirectException if external API to fetch condition of class or defect fail.
   */
  @RestrictedAsset
  @Override
  public final List<CoCHDto> getCoCForExport(final Long assetId) throws ClassDirectException {
    List<CoCHDto> response = null;
    final Map<Long, DefectHDto> defectLookup;
    try {

      final CodicilDefectQueryHDto defectQuery = new CodicilDefectQueryHDto();
      defectQuery.setStatusList(new ArrayList<>(1));
      defectQuery.getStatusList().add(FaultStatus.OPEN.getValue());
      final List<DefectHDto> defectList = assetServiceDelegate.getDefectsByQuery(assetId, defectQuery).execute().body();
      final Optional<List<DefectHDto>> defectListOpt = Optional.ofNullable(defectList).filter(list -> list.size() > 0);
      if (defectListOpt.isPresent()) {
        defectLookup = defectListOpt.get().stream().peek(defect -> Resources.inject(defect))
            .collect(Collectors.toMap(DefectHDto::getId, Function.identity()));
        LOGGER.debug("Loaded {} open defects for cocs on asset: {}.", defectLookup.size(), assetId);
      } else {
        defectLookup = new HashMap<>(0);
        LOGGER.debug("No open defect for asset: {}", assetId);
      }

      final CodicilDefectQueryHDto cocQuery = new CodicilDefectQueryHDto();
      cocQuery.setStatusList(new ArrayList<>(1));
      cocQuery.getStatusList().add(CodicilStatus.COC_OPEN.getValue());
      response = assetServiceDelegate.getCoCQueryDto(cocQuery, assetId).execute().body();

      final Optional<List<CoCHDto>> cocListOpt = Optional.ofNullable(response).filter(list -> list.size() > 0);

      if (cocListOpt.isPresent()) {
        final List<Callable<Void>> task = new ArrayList<>();

        cocListOpt.get().forEach(coc -> {
          task.add(() -> {
            try {
              Resources.inject(coc);
              // Fetch defect if any.
              if (coc.getDefect() != null) {
                final DefectHDto defect = defectLookup.get(coc.getDefect().getId());
                if (defect != null) {
                  coc.setDefectH(defect);
                } else {
                  LOGGER.error("Cannot find open defect for coc: {} with id: {}", coc.getId(), coc.getDefect().getId());
                }
              }
            } catch (final Exception e) {
              LOGGER.error("Error fetching coc {} for asset {} . | Error Message: {}", coc.getId(), assetId,
                  e.getMessage());
            }
            return null;
          });
          task.add(() -> {
            try {
              // hydrate job data
              if (coc.getJob() != null) {
                JobHDto jobDto = jobServiceDelegate.getJobsByJobId(coc.getJob().getId()).execute().body();
                if (jobDto != null) {
                  coc.setJobH(jobDto);
                } else {
                  LOGGER.error("Cannot find job for coc: {} with id: {}", coc.getId(), coc.getJob().getId());
                }
              }
            } catch (final Exception e) {
              LOGGER.error("Error fetching job for coc {} . | Error Message: {}", coc.getId(), e.getMessage());
            }
            return null;
          });

        });

        final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
        try {
          executor.invokeAll(task);
        } catch (final InterruptedException exception) {
          throw new ClassDirectException(exception);
        } finally {
          executor.shutdown();
        }
      } else {
        LOGGER.debug("No open coc for asset: {}", assetId);
      }
    } catch (final ClassDirectException | IOException exception) {
      LOGGER.error("Error executing assetService - getCoCForExport.", exception);
      throw new ClassDirectException(exception);
    }
    return response;
  }

  /**
   * Returns list of condition of class.
   *
   * @param assetId the asset Id.
   * @return the list of condition of class.
   *
   * @throws ClassDirectException if API call fail.
   */
  @Override
  public final List<CoCHDto> getCoC(final long assetId) throws ClassDirectException {
    List<CoCHDto> response = null;

    try {
      response = assetServiceDelegate.getCoCDto(assetId).execute().body();
    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getCoC.", ioException);
      throw new ClassDirectException("Fail to fetch Coc: " + ioException.getMessage());
    }

    if (response != null && !response.isEmpty()) {
      final List<Callable<Void>> enrichTaskList = response.parallelStream().map(coc -> {
        final Callable<Void> assetItemTask = () -> {
          if (coc.getAssetItem() != null) {
            try {
              final LazyItemHDto assetItem =
                  assetServiceDelegate.getAssetItemDto(assetId, coc.getAssetItem().getId()).execute().body();
              coc.setAssetItemH(assetItem);
            } catch (final Exception e) {
              LOGGER.error("Asset item with id:{} not found.", coc.getAssetItem().getId(), e);
            }
          }
          Resources.inject(coc);
          return null;
        };
        return assetItemTask;
      }).collect(Collectors.toList());

      final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
      try {
        executor.invokeAll(enrichTaskList);
      } catch (InterruptedException e) {
        LOGGER.error("Error while enrich Coc for asset id : {}", assetId, e);
        throw new ClassDirectException("Fail to fetch Coc: " + e.getMessage());
      } finally {
        executor.shutdown();
      }
    }

    return response;
  }

  @Override
  public final Map<String, Long> getCoCServiceRange(final long assetId) throws IOException {

    final Map<String, LocalDateTime> range = new HashMap<>();

    range.put(LOW_RANGE, LocalDateTime.now());

    Date maxDueDate = new Date();

    LocalDate minDueDateRef = LocalDate.now().minusMonths(DEFAULT_LOWER_RANGE);

    Date minDueDate = getLocalDateToDate(minDueDateRef);

    final Function<Date, LocalDateTime> convertDate = date -> DateUtils.getDateToLocalDate(date);

    List<CoCHDto> coCs = null;
    CodicilDefectQueryHDto query = new CodicilDefectQueryHDto();
    query.setStatusList(cocStatusList);
    query.setDueDateMax(maxDueDate);
    query.setDueDateMin(minDueDate);
    final Call<List<CoCHDto>> coCResponse = assetServiceDelegate.getCoCQueryDto(query, assetId);
    coCs = coCResponse.execute().body();
    if (coCs != null) {
      coCs.parallelStream().map(coc -> convertDate.apply(coc.getDueDate())).sorted().findFirst()
          .ifPresent(date -> range.put(LOW_RANGE, date));
    }

    List<ActionableItemHDto> actionableItems = null;
    query.setStatusList(actionableItemStatusList);
    final Call<List<ActionableItemHDto>> aIResponse = assetServiceDelegate.getActionableItemQueryDto(query, assetId);
    actionableItems = aIResponse.execute().body();
    if (actionableItems != null) {
      actionableItems.parallelStream().map(ai -> convertDate.apply(ai.getDueDate())).sorted().findFirst()
          .ifPresent(date -> range.put(LOW_RANGE, date));
    }

    List<StatutoryFindingHDto> statutoryFindings = null;
    query.setStatusList(statutoryFindingStatusIdList);
    final Call<List<StatutoryFindingHDto>> statutoryResponse =
        assetServiceDelegate.getStatutoryFindingsForAsset(assetId, query);
    statutoryFindings = statutoryResponse.execute().body();
    if (statutoryFindings != null) {
      statutoryFindings.parallelStream().map(sf -> convertDate.apply(sf.getDueDate())).sorted().findFirst()
          .ifPresent(date -> range.put(LOW_RANGE, date));
    }

    List<ScheduledServiceHDto> services = null;
    ServiceQueryDto serviceQuery = new ServiceQueryDto();
    serviceQuery.setStatusIdList(assetServiceStatusList);
    serviceQuery.setMaxDueDate(maxDueDate);
    serviceQuery.setMinDueDate(minDueDate);
    final Map<String, String> queryString = AssetCodeUtil.serviceQuerytoQueryMap(serviceQuery);
    final Call<List<ScheduledServiceHDto>> scheduledServiceHDtos =
        serviceRefDelegate.getAssetServicesQuery(assetId, queryString);
    services = scheduledServiceHDtos.execute().body();
    if (services != null) {
      services.parallelStream().map(service -> convertDate.apply(service.getDueDate())).sorted().findFirst()
          .ifPresent(date -> range.put(LOW_RANGE, date));
    }

    range.put(HIGH_RANGE, range.get(LOW_RANGE).plusYears(DEFAULT_YEAR_RANGE));
    // converting the range to return a string

    return range.keySet().stream()
        .collect(Collectors.toMap(key -> key, key -> DateUtils.getLocalDateToEpoch(range.get(key))));
  }

  @Override
  public final CoCServiceRangeHDto getCoCServiceRangeWithDetails(final long assetId, final ServiceQueryDto queryHDto)
      throws IOException, ClassDirectException, CloneNotSupportedException {

    final CoCServiceRangeHDto serviceRangeDto = new CoCServiceRangeHDto();

    if (queryHDto != null) {
      Date maxDueDate;
      if (queryHDto.getMaxDueDate() != null) {
        maxDueDate = queryHDto.getMaxDueDate();
      } else {
        maxDueDate = new Date();
      }

      Date minDueDate;
      if (queryHDto.getMinDueDate() != null) {
        minDueDate = queryHDto.getMinDueDate();
      } else {
        LocalDate minDueDateRef = LocalDate.now().minusMonths(DEFAULT_LOWER_RANGE);
        minDueDate = getLocalDateToDate(minDueDateRef);
      }

      final Function<Date, LocalDateTime> convertDate = date -> DateUtils.getDateToLocalDate(date);

      final Callable<Object> getLowRangeForCoCsWithDto = () -> {
        final CodicilDefectQueryHDto cocQuery = new CodicilDefectQueryHDto();
        cocQuery.setStatusList(queryHDto.getStatusIdList());
        cocQuery.setDueDateMax(maxDueDate);
        cocQuery.setDueDateMin(minDueDate);
        return assetService.getCodicils(assetId, cocQuery);
      };

      final Callable<Object> getLowRangeForServicesWithDto = () -> {
        return assetService.getServicesWithFutureDueDateSupport(assetId, queryHDto);
      };

      final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
      List<Callable<Object>> tasks = Arrays.asList(getLowRangeForCoCsWithDto, getLowRangeForServicesWithDto);
      try {
        final List<Future<Object>> futures = executor.invokeAll(tasks);
        final LocalDateTime currentDate = LocalDateTime.now();
        LocalDateTime lowerRangeDate = LocalDateTime.now();
        for (final Future<Object> future : futures) {
          final Object object = future.get();
          if (object instanceof CodicilActionableHDto) {
            final CodicilActionableHDto cocActionableHDto = (CodicilActionableHDto) object;
            final List<CoCHDto> coCs = cocActionableHDto.getCocHDtos();
            final List<ActionableItemHDto> actionableItems = cocActionableHDto.getActionableItemHDtos();
            final List<StatutoryFindingHDto> statutoryFindings = cocActionableHDto.getStatutoryFindingHDtos();
            if (coCs != null) {
              serviceRangeDto.setCocHDtos(coCs);
              lowerRangeDate = coCs.parallelStream().map(coc -> convertDate.apply(coc.getDueDate()))
                  .filter(isLowestDate(lowerRangeDate)).sorted().findFirst().orElse(currentDate);
            }

            if (actionableItems != null) {
              serviceRangeDto.setActionableItemHDtos(actionableItems);
              lowerRangeDate = actionableItems.parallelStream().map(ai -> convertDate.apply(ai.getDueDate()))
                  .filter(isLowestDate(lowerRangeDate)).sorted().findFirst().orElse(currentDate);
            }

            if (statutoryFindings != null) {
              serviceRangeDto.setStatutoryFindingHDtos(statutoryFindings);
              lowerRangeDate = statutoryFindings.parallelStream().map(sf -> convertDate.apply(sf.getDueDate()))
                  .filter(isLowestDate(lowerRangeDate)).sorted().findFirst().orElse(currentDate);
            }
          } else if (object instanceof ServiceScheduleDto) {
            final ServiceScheduleDto serviceDto = (ServiceScheduleDto) object;
            final List<ScheduledServiceHDto> services = serviceDto.getServices();
            serviceRangeDto.setServices(services);
            serviceRangeDto.setRepeatedServices(serviceDto.getRepeatedServices());
            if (services != null) {
              lowerRangeDate = services.parallelStream().map(service -> convertDate.apply(service.getDueDate()))
                  .filter(isLowestDate(lowerRangeDate)).sorted().findFirst().orElse(currentDate);
            }
          }
        }
        serviceRangeDto.setLowRange(DateUtils.getLocalDateToEpoch(lowerRangeDate));
        serviceRangeDto.setHighRange(DateUtils.getLocalDateToEpoch(lowerRangeDate.plusYears(DEFAULT_YEAR_RANGE)));
      } catch (final InterruptedException | ExecutionException interruptedException) {
        LOGGER.error(interruptedException.getMessage());
        throw new ClassDirectException(interruptedException);
      } finally {
        executor.shutdown();
      }
    }
    return serviceRangeDto;
  }

  /**
   * Check if due date is earlier than current date and the lower range date.
   *
   * @param lowerRangeDate lower range date from service schedule range date.
   * @return flag on whether due date is earlier than both current date and lower range date.
   */
  private Predicate<LocalDateTime> isLowestDate(final LocalDateTime lowerRangeDate) {
    return dueDate -> dueDate.isBefore(lowerRangeDate);
  }

  @RestrictedAsset
  @Override
  public final CoCHDto getCoCByAssetIdCoCId(@AssetID final Long assetId, final long cocId) throws ClassDirectException {
    CoCHDto cocResponse = null;

    try {
      final CoCHDto coc = assetServiceDelegate.getCoCByAssetIdCoCId(assetId, cocId).execute().body();
      final List<Callable<Void>> task = new ArrayList<>();

      if (coc != null) {
        task.add(() -> {
          // get defect data
          if (coc.getDefect() != null) {
            try {
              final DefectHDto defect =
                  assetServiceDelegate.getAssetDefectDto(assetId, coc.getDefect().getId()).execute().body();
              Optional.ofNullable(defect).ifPresent(defectH -> {
                Resources.inject(defectH);
                coc.setDefectH(defectH);
              });
            } catch (final IOException ioException) {
              LOGGER.error("Error executing getAssetDefectDto({}): Id not found.", coc.getDefect().getId(),
                  ioException);
            }
          }
          return null;
        });

        task.add(() -> {
          // get repair data
          final List<RepairHDto> repairs = assetServiceDelegate.getCoCRepairDto(assetId, coc.getId()).execute().body();
          repairs.forEach(repair -> {
            repair.setRepairsH(new ArrayList<>());
            if (repair.getRepairs() != null) {
              repair.getRepairs().forEach(rep -> {
                try {
                  final LazyItemHDto item =
                      assetServiceDelegate.getAssetItemDto(assetId, rep.getItem().getItem().getId()).execute().body();
                  repair.getRepairsH().add(item);
                } catch (final IOException exception) {
                  LOGGER.error("Error query getCoC - getItem({}): Id not found.", rep.getItem().getItem().getId(),
                      exception);
                }
              });
            }

            repair.setRepairTypesH(new ArrayList<>());
            if (repair.getRepairTypes() != null) {
              repair.getRepairTypes().forEach(repairType -> {
                final RepairTypeHDto type = assetRefDelegate.getRepairType(repairType.getId());
                repair.getRepairTypesH().add(type);
              });
            }

            Resources.inject(repair);
          });
          coc.setRepairH(repairs);
          return null;
        });

        task.add(() -> {
          // get asset item data
          if (coc.getAssetItem() != null) {
            try {
              final LazyItemHDto assetItem =
                  assetServiceDelegate.getAssetItemDto(assetId, coc.getAssetItem().getId()).execute().body();
              coc.setAssetItemH(assetItem);
            } catch (final IOException exception) {
              LOGGER.error("Error query getCoC - getAssetItemDto({}): Id not found.", coc.getAssetItem().getId(),
                  exception);
            }
          }
          return null;
        });
        task.add(() -> {
          try {
            // hydrate job data
            if (coc.getJob() != null) {
              JobHDto jobDto = jobServiceDelegate.getJobsByJobId(coc.getJob().getId()).execute().body();
              coc.setJobH(jobDto);
            }
          } catch (final Exception e) {
            LOGGER.error("Error fetching job for coc {} . | Error Message: {}", coc.getId(), e.getMessage());
          }
          return null;
        });

      }
      final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
      try {
        for (final Future<Void> future : executor.invokeAll(task)) {
          future.get();
        }
      } catch (final InterruptedException | ExecutionException exception) {
        throw new ClassDirectException(exception);
      } finally {
        executor.shutdown();
      }

      Resources.inject(coc);

      cocResponse = coc;

    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getCoCByAssetIdCoCId.", ioException);
      throw new ClassDirectException(ioException);
    }

    return cocResponse;
  }

  @Override
  public final List<CoCHDto> getCocsByQuery(final long assetId, final CodicilDefectQueryHDto query)
      throws ClassDirectException {
    List<CoCHDto> response = null;
    try {
      response = assetServiceDelegate.getCoCQueryDto(query, assetId).execute().body();
    } catch (final IOException ioException) {
      LOGGER.error("Error executing assetService - getCocsByQuery.", ioException);
      throw new ClassDirectException("Fail to fetch cocs for asset " + assetId + " : " + ioException.getMessage());
    }

    if (response != null && !response.isEmpty()) {
      response.forEach(item -> {
        Resources.inject(item);
      });
    }
    return response;
  }

  @Override
  public final DueStatus calculateDueStatus(final CoCHDto cocInput) {
    return calculateDueStatus(cocInput, LocalDate.now());
  }

  // calculate dueStatus for CoC.
  @Override
  public final DueStatus calculateDueStatus(final CoCHDto coc, final LocalDate date) {
    DueStatus status = DueStatus.NOT_DUE;

    if (coc.getDueDate() != null) {

      final LocalDate dueDate = DateUtils.getLocalDateFromDate(coc.getDueDate());

      if (dueDate.isBefore(date)) {
        status = DueStatus.OVER_DUE;

      } else if (date.plusMonths(IMMINENT_TH).isAfter(dueDate)) {
        status = DueStatus.IMMINENT;

      } else if (date.plusMonths(DUE_SOON_TH).isAfter(dueDate)) {
        status = DueStatus.DUE_SOON;
      }
    }
    return status;
  }

}
