package com.baesystems.ai.lr.cd.service.countryfiles;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * Provides service to fetches attachments for country file.
 *
 * @author Faizal Sidek
 * @author syalavarthi 22-05-2017.
 */
public interface CountryFileService {

  /**
   * Gets attachments for country by country code.
   *
   * @param flagId from reference data flags.
   * @return List of SupplementaryInformationHDtos.
   * @throws RecordNotFoundException when no country file found for country id.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  List<SupplementaryInformationHDto> getAttachmentsCountryFile(Long flagId) throws RecordNotFoundException;
}
