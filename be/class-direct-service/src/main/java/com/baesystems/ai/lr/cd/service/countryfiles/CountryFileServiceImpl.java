package com.baesystems.ai.lr.cd.service.countryfiles;

import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsLast;
import static java.util.Comparator.reverseOrder;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.service.Resources;

import retrofit2.Call;

/**
 * Provides service to fetches attachments for country file.
 *
 * @author Faizal Sidek
 * @author syalavarthi 22-05-2017.
 */
@Service("CountryFileService")
public class CountryFileServiceImpl implements CountryFileService {

  /**
   * The logger for CountryFileServiceImpl.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CountryFileServiceImpl.class);

  /**
   * The {@Link AttachmentService} from Spring context.
   */
  @Autowired
  private AttachmentRetrofitService attachmentRetrofitService;

  @Override
  public final List<SupplementaryInformationHDto> getAttachmentsCountryFile(final Long flagId)
      throws RecordNotFoundException {
    final List<SupplementaryInformationHDto> supplementaryInfoDtos = new ArrayList<>();

    try {

      // get logged in userId.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      final String userId = token.getDetails().getUserId();
      final Call<List<SupplementaryInformationHDto>> attachmentsCall =
          attachmentRetrofitService.getAttachmentsByFlagId(flagId);

      Optional.ofNullable(attachmentsCall.execute().body()).ifPresent(attachmentList ->

        supplementaryInfoDtos.addAll(attachmentList.stream().map(supplement -> {
          Resources.inject(supplement);
          Integer attachNodeId = null;
          Integer attachVersion = null;
          try {
            attachNodeId =
                Optional.ofNullable(supplement.getAttachmentUrl()).map(Integer::valueOf).orElse(null);

            attachVersion =
                Optional.ofNullable(supplement.getDocumentVersion()).map(Long::intValue).orElse(null);
            supplement.setToken(FileTokenGenerator.generateCS10Token(attachNodeId, attachVersion, userId));
            LOGGER.info("attachment token " + supplement.getToken());
          } catch (final ClassDirectException exception) {
            LOGGER.error(" error occured in generating token with attachmenNodeId {} and attachmentVersionId {}",
                attachNodeId, attachVersion);
          }
          return supplement;
        }).sorted(new SupplementaryComparator()).collect(Collectors.toList()))

      );

    } catch (final IOException exception) {
      LOGGER.error(" error occured getting attachments for flagId {} ", flagId);
      throw new RecordNotFoundException(exception.getMessage());
    }

    return supplementaryInfoDtos;
  }

  /**
   * Sub class for comparator.
   *
   * @author SYalavarthi.
   *
   */
  static class SupplementaryComparator implements Comparator<SupplementaryInformationHDto>, Serializable {

    /**
     * static serial version id.
     */
    private static final long serialVersionUID = -3616861287199577556L;

    @Override
    public int compare(final SupplementaryInformationHDto object1, final SupplementaryInformationHDto object2) {
      // sort by attachment date, followed by updated date. Both in descending order.
      // null values will be appear at the bottom of the list.
      return nullsLast(
      comparing(SupplementaryInformationHDto::getAttachmentDateWithTime, nullsLast(reverseOrder()))
            .thenComparing(SupplementaryInformationHDto::getUpdatedDateWithTime, nullsLast(reverseOrder()))
      ).compare(object1, object2);
    }
  }
}
