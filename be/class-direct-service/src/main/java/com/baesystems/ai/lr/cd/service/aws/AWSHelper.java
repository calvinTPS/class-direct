package com.baesystems.ai.lr.cd.service.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.glacier.AmazonGlacierClientBuilder;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManager;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManagerBuilder;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

/**
 * Thin class to wrap S3 and Glacier client creation.
 *
 * @author Faizal Sidek
 */
@Component
public class AWSHelper {

  /**
   * Helper method to create S3 client. This will help mocking.
   *
   * @param credentials aws credentials.
   * @param region region.
   * @return s3 client.
   */
  public AmazonS3 createS3Client(final BasicAWSCredentials credentials, final String region) {
    final AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard()
          .withRegion(Regions.fromName(region));

    if (credentials != null) {
      builder.withCredentials(new AWSStaticCredentialsProvider(credentials));
    } else {
      builder.withCredentials(InstanceProfileCredentialsProvider.getInstance());
    }
    return builder.build();
  }

  /**
   * Create Glacier transfer manager.
   *
   * @param credentials aws credential.
   * @param region aws region.
   * @return transfer manager.
   */
  public ArchiveTransferManager createTransferManager(final BasicAWSCredentials credentials, final String region) {
    final AmazonGlacierClientBuilder glacierBuilder = AmazonGlacierClientBuilder.standard()
            .withRegion(Regions.fromName(region));

    if (credentials != null) {
      glacierBuilder.withCredentials(new AWSStaticCredentialsProvider(credentials));
    } else {
      glacierBuilder.withCredentials(InstanceProfileCredentialsProvider.getInstance());
    }

    final ArchiveTransferManagerBuilder archiveBuilder = new ArchiveTransferManagerBuilder();
    archiveBuilder.setGlacierClient(glacierBuilder.build());
    return archiveBuilder.build();
  }

  /**
   * Create new Kms client.
   *
   * @param credentials AWS credential.
   * @param region aws region.
   * @return aws kms client.
   */
  public AWSKMS createKmsClient(final BasicAWSCredentials credentials, final String region) {
    final AWSKMSClientBuilder builder = AWSKMSClientBuilder.standard().withRegion(Regions.fromName(region));

    if (credentials != null) {
      builder.withCredentials(new AWSStaticCredentialsProvider(credentials));
    } else {
      builder.withCredentials(InstanceProfileCredentialsProvider.getInstance());
    }

    return builder.build();
  }

  /**
   * Copy content to file.
   *
   * @param target target file.
   * @param content content in byte array.
   * @throws IOException when error.
   */
  public void writeByteArrayToFile(final File target, final byte[] content) throws IOException {
    FileUtils.writeByteArrayToFile(target, content);
  }

  /**
   * IO function to copy s3 object to byte array.
   *
   * @param s3Object s3 object.
   * @return byte array.
   * @throws IOException when error.
   */
  public byte[] s3ObjectToByteArray(final S3Object s3Object) throws IOException {
    try (InputStream objectStream = s3Object.getObjectContent()) {
      return IOUtils.toByteArray(objectStream);
    }
  }

}
