package com.baesystems.ai.lr.cd.service;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.mockito.internal.util.MockUtil;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;

/**
 * This component scans packages for {@link ResourceProvider} annotation.
 *
 * @author MSidek
 *
 */
@Component
public class ResourceProviderScanner implements InitializingBean, ApplicationContextAware {
  /**
   * Logger object.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ResourceProviderScanner.class);

  /**
   * Maximum parameter count for specific providers.
   *
   */
  private static final Integer SPECIFIC_PROVIDER_ARGS_LENGTH = 1;

  /**
   * Maximum parameter count for generic providers.
   *
   */
  private static final Integer GENERIC_PROVIDER_ARGS_LENGTH = 0;

  /**
   * Default package is root.
   *
   */
  private static final String DEFAULT_PACKAGE = "com.baesystems.ai";

  /**
   * Set of resource providers which having search by id of type Long.
   *
   */
  private final Set<TypeProvider> specificProviders = new HashSet<>();

  /**
   * Set of resource providers which having no arguments.
   *
   */
  private final Set<TypeProvider> genericProviders = new HashSet<>();

  /**
   * Packages to be scanned.
   *
   */
  private String[] packages = new String[] {DEFAULT_PACKAGE};

  /**
   * Application context.
   *
   */
  private ApplicationContext context;

  /**
   * Setter for {@link #packages}.
   *
   * @param scanPackages package to be scanned.
   *
   */
  public final void setPackages(final String[] scanPackages) {
    this.packages = (String[]) scanPackages.clone();
  }

  /**
   * Getter for {@link #specificProviders}.
   *
   * @return specific providers.
   *
   */
  public final Set<TypeProvider> getSpecificProviders() {
    return specificProviders;
  }

  /**
   * Getter for {@link #genericProviders}.
   *
   * @return generic providers.
   *
   */
  public final Set<TypeProvider> getGenericProviders() {
    return genericProviders;
  }

  /**
   * Returns all providers.
   *
   * @return set of providers.
   *
   */
  public final Set<TypeProvider> getAllProviders() {
    final Set<TypeProvider> providers = new HashSet<>();
    providers.addAll(genericProviders);
    providers.addAll(specificProviders);
    return providers;
  }

  @Override
  public final void afterPropertiesSet() throws Exception {
    LOGGER.debug("Scanning classpath for @ResourceProvider");

    final ConfigurationBuilder builder = new ConfigurationBuilder();
    Stream.of(packages).forEach(pack -> builder.setUrls(ClasspathHelper.forPackage(pack)));
    builder.setScanners(new MethodAnnotationsScanner());

    final Reflections reflections = new Reflections(builder);
    final Set<Method> methods = reflections.getMethodsAnnotatedWith(ResourceProvider.class);
    LOGGER.debug("Found {} method(s)", methods.size());

    methods.stream().filter(method -> method.getParameterCount() == GENERIC_PROVIDER_ARGS_LENGTH)
        .forEach(method -> lookup(genericProviders, method));
    methods.stream().filter(method -> method.getParameterCount() == SPECIFIC_PROVIDER_ARGS_LENGTH)
        .forEach(method -> lookup(specificProviders, method));
  }

  @Override
  public final void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
    this.context = applicationContext;
  }

  /**
   * Iterate beans and populate the providers.
   *
   * @param providers providers list.
   * @param method method finder.
   *
   */
  private void lookup(final Set<TypeProvider> providers, final Method method) {
    final Class<?> iface = getValidInterface(method.getDeclaringClass().getInterfaces());
    final List<String> beanNames = Arrays.asList(context.getBeanNamesForType(iface));

    LOGGER.debug("Found {} beans for type {} ", beanNames.size(), method.getDeclaringClass().getSimpleName());
    beanNames.forEach(beanName -> {
      LOGGER.debug("Lookup for bean with name {} ", beanName);
      final Object bean = context.getBean(beanName);
      try {
        Method actualMethod = bean.getClass().getMethod(method.getName(), method.getParameterTypes());

        if (bean instanceof Advised) {
          final Advised adviced = (Advised) bean;
          actualMethod =
              adviced.getTargetSource().getTargetClass().getMethod(method.getName(), method.getParameterTypes());
        }

        final Boolean isMock = new MockUtil().isMock(bean) && !new MockUtil().isSpy(bean);
        if (!isMock) {
          LOGGER.debug("Bean {} has resource provider method with name {} with {} parameters and return type {}",
              bean.getClass().getSimpleName(), actualMethod.getName(), actualMethod.getParameterCount(),
              actualMethod.getGenericReturnType());

          if (Collection.class.isAssignableFrom(actualMethod.getReturnType())) {
            LOGGER.debug("Inferring return type from list.");
            final ParameterizedType listType = (ParameterizedType) actualMethod.getGenericReturnType();
            final Class<?> target = (Class<?>) listType.getActualTypeArguments()[0];
            LOGGER.debug("Add {}, {}->{}", target.getSimpleName(), beanName, actualMethod.getName());
            providers.add(new TypeProvider(target, beanName, actualMethod));
          } else {
            LOGGER.debug("Add {}, {}->{}", actualMethod.getReturnType().getSimpleName(), beanName,
                actualMethod.getName());
            providers.add(new TypeProvider(actualMethod.getReturnType(), beanName, actualMethod));
          }
        } else {
          LOGGER.debug("This is mock bean. Skipping...");
        }
      } catch (NoSuchMethodException noMethodException) {
        LOGGER.error("Method not found.", noMethodException);
      }
    });
  }

  /**
   * Iterate interface if the class implementing multiple interfaces.
   *
   * @param classes list of interfaces.
   * @return valid interface
   *
   */
  private Class<?> getValidInterface(final Class<?>[] classes) {
    return Stream.of(classes).filter(clazz -> clazz.getPackage().getName().contains(DEFAULT_PACKAGE)).findAny().get();
  }
}
