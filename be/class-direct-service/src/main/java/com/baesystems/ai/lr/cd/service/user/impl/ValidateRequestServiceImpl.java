package com.baesystems.ai.lr.cd.service.user.impl;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dto.user.UserDto;
import com.baesystems.ai.lr.cd.service.user.ValidateRequestService;
import com.jcabi.aspects.Loggable;

/**
 * Service to validate request.
 *
 * @author vmandalapu
 */
@Service(value = "ValidateRequestService")
@Loggable(Loggable.DEBUG)
public class ValidateRequestServiceImpl implements ValidateRequestService {

  /**
   * Logger delcaration.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ValidateRequestServiceImpl.class);

  @Override
  public final UserDto requestHasRoles(final String user) {
    LOGGER.debug("Validing request for equais/thetis with user {}", user);
    final UserDto userDto = new UserDto();
    if (user != null && user.equalsIgnoreCase(EQUASIS.toString())) {
      userDto.setEquasis(true);
    } else if (user != null && user.equalsIgnoreCase(THETIS.toString())) {
      userDto.setThetis(true);
    }
    return userDto;
  }

}
