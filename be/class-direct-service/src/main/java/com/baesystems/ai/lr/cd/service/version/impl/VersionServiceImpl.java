package com.baesystems.ai.lr.cd.service.version.impl;

import com.baesystems.ai.lr.cd.be.domain.dto.version.VersionDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.version.VersionService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Provides local service to get the build number.
 *
 * @author fwijaya on 15/6/2017.
 */
@Service("VersionService")
public class VersionServiceImpl implements VersionService {
  /**
   * The context.
   */
  @Autowired
  @Setter
  private ServletContext servletContext;
  /**
   * The string token in the manifest file that shows the build number.
   */
  private static final String MANIFEST_BUILD_TOKEN = "Build";
  /**
   * Delimiter used to parse MANIFEST.MF file.
   */
  private static final String SCANNER_DELIMITER = ":|\\R";
  /**
   * The character set used to read the MANIFEST.MF file.
   */
  private static final String CHAR_SET = "UTF-8";
  /**
   * Returns the version/build number.
   *
   * @return The build number.
   * @throws ClassDirectException When no build number is found in the manifest.
   */
  @Override
  public final VersionDto getVersion() throws ClassDirectException {
    final InputStream is = servletContext.getResourceAsStream("/META-INF/MANIFEST.MF");
    final Scanner scanner = new Scanner(is, CHAR_SET).useDelimiter(SCANNER_DELIMITER);
    while (scanner.hasNext()) {
      final String nextToken = scanner.next();
      if (nextToken.equalsIgnoreCase(MANIFEST_BUILD_TOKEN)) {
        final String buildNumber = scanner.next().trim();
        return new VersionDto(buildNumber);
      }
    }
    throw new ClassDirectException("Build number not found in MANIFEST.MF");
  }
}
