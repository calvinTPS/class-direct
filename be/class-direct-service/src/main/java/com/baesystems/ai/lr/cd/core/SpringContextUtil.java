package com.baesystems.ai.lr.cd.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Provides the base model utility for the Spring context. Gets beans from spring context.
 *
 * @author RKaneysan
 *
 */
public class SpringContextUtil implements ApplicationContextAware, InitializingBean {
  /**
   * Logger object.
   *
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(SpringContextUtil.class);

  /**
   * The {@link ApplicationContext} from static spring context.
   */
  private static ApplicationContext staticContext;

  /**
   * The {@link ApplicationContext} from spring context.
   */
  private ApplicationContext applicationContext;

  /**
   * Gets spring application context.
   *
   * @return applicationContext Spring Application Context.
   */
  public final ApplicationContext getApplicationContext() {
    return applicationContext;
  }

  /**
   * Gets bean with bean name and class.
   *
   * @param beanName bean name.
   * @param <T> clazz the requested class type.
   * @param clazz the requested class name.
   * @return <T> the bean.
   */
  public static final <T> T getBean(final String beanName, final Class<T> clazz) {
    return staticContext.getBean(beanName, clazz);
  }

  /**
   * Gets bean with class.
   *
   * @param <T> clazz the requested class type.
   * @param clazz the requested class name.
   * @return <T> the bean.
   */
  public static final <T> T getBean(final Class<T> clazz) {
    return staticContext.getBean(clazz);
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.
   * context.ApplicationContext)
   */
  @Override
  public final void setApplicationContext(final ApplicationContext appContext) throws BeansException {
    LOGGER.debug("Setting application context.");
    applicationContext = appContext;
  }

  /**
   * Gets application context.
   *
   * @return staticContext Static Context.
   */
  public static ApplicationContext getStaticContext() {
    return staticContext;
  }

  /**
   * Sets application context.
   *
   * @param statContext Static Context.
   */
  public static void setStaticContext(final ApplicationContext statContext) {
    SpringContextUtil.staticContext = statContext;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
   */
  @Override
  public final void afterPropertiesSet() {
    LOGGER.debug("Setting static application context.");

    setStaticContext(applicationContext);
  }
}
