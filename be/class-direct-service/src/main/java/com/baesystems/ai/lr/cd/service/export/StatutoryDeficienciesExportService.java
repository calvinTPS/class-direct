package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides methods for generating statutory deficiencies PDF export.
 *
 * @author sbollu
 *
 */
public interface StatutoryDeficienciesExportService {

  /**
   * Generates statutory deficiencies PDF export.
   *
   * @param query the query object.
   * @return the encrypted token which contain generated PDF export file path location in server..
   * @throws ClassDirectException if IO exception occurred or Jade Exception occurred or document
   *         Exception occurred.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadPdf(ExportPdfDto query) throws ClassDirectException;

}
