package com.baesystems.ai.lr.cd.service.user;

import com.baesystems.ai.lr.cd.be.domain.dto.user.UserDto;

/**
 * validate interface.
 *
 * @author vmandalapu
 */
public interface ValidateRequestService {

  /**
   * Method to validate request has roles or not.
   *
   * @param user userId.
   * @return UserDto.
   */
  UserDto requestHasRoles(final String user);
}
