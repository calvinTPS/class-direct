package com.baesystems.ai.lr.cd.service.asset.impl;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetEffectiveDate;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetIdAndIhsAssetId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetImoNumber;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetLifecycleStatusId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetSearchWithFormerAssetName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetTypeCode;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetTypeId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetWildcardSearch;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.buildDate;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.builderIdAndBuilderName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.builderName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.classDepartmentId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.classStatusId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.clientCode;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.clientName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.deadWeight;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.flagStateCode;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.grossTonnage;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.hasActiveServices;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.ihsAssetId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.leadImo;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.linkedAssetId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.yardNumber;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.IDueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetEarliestEntitiesHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.EquipmentDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.IhsAssetClientDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.IhsCustomersDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.OwnershipDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.PrincipalDimensionDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RegistryInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RulesetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.UserPersonalInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CustomerLinkHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.RepeatOfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ServiceScheduleDto;
import com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.MmsServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.PartyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.repositories.Favourites;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.enums.CustomerRolesEnum;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.enums.MastPartyRole;
import com.baesystems.ai.lr.cd.be.enums.OrderOptionEnum;
import com.baesystems.ai.lr.cd.be.enums.SortOptionEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.TooManyResultsException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.AssetOverAllStatusUtils;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.FutureDueDateEngine;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.cd.service.reference.CustomerReferenceService;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.BaseAssetDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsCbsbDto;
import com.baesystems.ai.lr.dto.ihs.IhsCompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsHistDto;
import com.baesystems.ai.lr.dto.ihs.IhsOvnaDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.EarliestEntityQueryDto;
import com.baesystems.ai.lr.dto.query.IhsSisterQueryDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.FlagStateDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.OfficeRole;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.jcabi.aspects.Loggable;

import retrofit2.Call;

/**
 * Implementation class for Asset related transactions.
 *
 * @author VMandalapu
 */
@Service(value = "AssetService")
@Loggable(Loggable.DEBUG)
public class AssetServiceImpl implements AssetService, ApplicationListener<ContextRefreshedEvent> {

  /**
   * Date Pattern .
   */
  private static final String DATE_PATTERN_REVERSE = "yyyyMMdd";

  /**
   * Date Pattern .
   */
  private static final String DATE_PATTERN_YEAR_ONLY = "yyyy";

  /**
   * Date Pattern.
   */
  private static final String DATE_PATTERN_KEEL_AND_BUILD_DATE = "dd MMM yyyy";

  /**
   * The empty string.
   */
  private static final String EMPTY_STRING = "";

  /**
   * error message.
   */
  @Value("${record.not.found.for.asset.id}")
  private String recordNotFoundForAsset;

  /**
   * error message.
   */
  @Value("${no.ihs.id.found.for.asset.id}")
  private String noIhsIdFoundForAsset;

  /**
   * Unparsable Date Format error message.
   */
  @Value("${unparsable.date.format.error}")
  private String unparsableDateFormat;

  /**
   * error message.
   */
  @Value("${ihs.record.not.found.for.imo.number}")
  private String recordNotFoundForIHS;
  /**
   * error message.
   */
  @Value("${record.not.found.for.contact.id}")
  private String recordNotFoundForContact;

  /**
   *
   */
  @Value("${favourite.successful.update}")
  private String successfulUpdate;

  /**
   * Injected {@link FlagsAssociationsService}.
   */
  @Autowired
  private FlagsAssociationsService flagsAssociationsService;

  /**
   * Injected {@link CustomerReferenceService}.
   */
  @Autowired
  private CustomerReferenceService customerReferenceService;

  /**
   * The {@link ActionableItemService} implementation from Spring Context.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   * The {@link CoCService} implementation from Spring Context.
   */
  @Autowired
  private CoCService cocService;

  /**
   * The {@link ServiceReferenceService} implementation from Spring Context.
   */
  @Autowired
  private ServiceReferenceService scheduleService;

  /**
   * The {@link TaskService} implementation from Spring Context.
   */
  @Autowired
  private TaskService taskService;

  /**
   * Logger for AssetServiceImpl class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetServiceImpl.class);

  /**
   * Constant for first element of a record.
   */
  public static final int FIRST_ELEMENT_OF_RECORD = 0;

  /**
   * Constant for one record.
   */
  private static final int ONE_RECORD = 1;
  /**
   * Constant integer initial_size.
   */
  private static final int INITIAL_SIZE = 0;

  /**
   * Injected {@link AssetRetrofitService}.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * Injected {@link IhsRetrofitService}.
   */
  @Autowired
  private IhsRetrofitService ihsServiceDelegate;

  /**
   * ServiceReference retrofit.
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceReferenceDelegate;

  /**
   * The {@link SubFleetService} from Spring context.
   */
  @Autowired
  private SubFleetService subFleetService;

  /**
   * The {@link FlagStateService} from Spring context.
   */
  @Autowired
  private FlagStateService flagStateService;

  /**
   * Invalid Imo Number Error Message.
   */
  @Value("${security.invalid.imo.number}")
  private String invalidImoNumber;

  /**
   * More than One Asset Error Message.
   */
  @Value("${security.more.than.one.asset}")
  private String moreThanOneAsset;

  /**
   * Asset Not Found Error Message.
   */
  @Value("${security.asset.not.found}")
  private String assetNotFound;

  /**
   * serviceStatus List.
   */
  @Value("#{'${overallstatus.service_status_id}'.split(',')}")
  private List<Long> assetServiceStatusList;

  /**
   * taskStatus List.
   */
  @Value("#{'${overallstatus.task_status_id}'.split(',')}")
  private List<Long> taskStatusList;

  /**
   * actionableItemStatus List.
   */
  @Value("#{'${overallstatus.actionableitem_status_id}'.split(',')}")
  private List<Long> actionableItemStatusList;

  /**
   * cocStatus List.
   */
  @Value("#{'${overallstatus.coc_status_id}'.split(',')}")
  private List<Long> cocStatusList;

  /**
   * excludedServicesList.
   */
  @Value("#{'${overallstatus.excludedservicecodelist_status_id}'.split(',')}")
  private List<String> excludedServicesList;

  /**
   * List of Statutory Finding Status IDs to take into account.
   */
  @Value("#{'${overallstatus.statutory_finding_status_id}'.split(',')}")
  private List<Long> statutoryFindingStatusIdList;

  /**
   * The default CFO office.
   */
  @Value("${default.asset.cfo}")
  private String defaultAssetCFO;

  /**
   * The serviceCreditStatus List.
   */
  @Value("#{'${service.credit.status.id}'.split(',')}")
  private List<Long> serviceCreditStatusList;

  /**
   * Injected FavouritesRepository.
   */
  @Autowired
  private FavouritesRepository favouritesRepository;

  /**
   * Injected userProfileService.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Injected {@Link JobRetrofitService}.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * Injected {@link StatutoryFindingService}.
   */
  @Autowired
  private StatutoryFindingService statutoryFindingService;

  /**
   * Injected {@link AssetReferenceService}.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link EmployeeReferenceService} from Spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;
  /**
   * The {@link ProductReferenceService} from spring context.
   */
  @Autowired
  private ProductReferenceService productReferenceService;

  /**
   * The default page to fetch technical sisters and registry book sisters count.
   */
  private static final int DEFAULT_PAGE = 1;
  /**
   * The default size to fetch technical sisters and registry book sisters count.
   */
  private static final int DEFAULT_SIZE = 1;

  /**
   * The sorting priority as IHS.
   */
  private static final String SORTING_PRIORITY_IHS = "IHS";
  /**
   * The max page size to fetch assets from mast-and-ihs-query.
   */
  @Value("${assets.maxsize.perpage}")
  private Integer maxPageSize;

  /**
   * Self inject.
   */
  private AssetService self;
  /**
   * The codicil allowable confidentiality type ids.
   */
  @Value("#{'${codicil.confidentiality.type.id}'.split(',')}")
  private List<Long> confidentialityTypeIds;

  @Override
  public final AssetPageResource getAssets(final String userId, final AssetQueryHDto assetQuery, final Integer page,
      final Integer size) throws ClassDirectException, IOException {
    MastQueryBuilder queryBuilder = convert(assetQuery);
    final AssetPageResource assets = getAssetByQueryBuilder(userId, assetQuery, queryBuilder, page, size);
    if (assetQuery.getHasActiveServices() != null && Boolean.TRUE.equals(assetQuery.getHasActiveServices())) {
      LOGGER.trace("All these assets has active services.");
      assets.getContent().forEach(asset -> asset.setHasActiveServices(Boolean.TRUE));
    } else {
      LOGGER.trace("Filtering assets with active services.");
      final List<Long> mastAssetIds =
          assets.getContent().stream().filter(asset -> asset.getCode().startsWith(CDConstants.MAST_PREFIX))
              .map(BaseDto::getId).collect(Collectors.toList());
      if (mastAssetIds != null && !mastAssetIds.isEmpty()) {
        LOGGER.trace("Filtering MAST assets");
        queryBuilder =
            MastQueryBuilder.create().mandatory(assetId(QueryRelationshipType.IN, mastAssetIds.toArray(new Long[] {})))
                .mandatory(hasActiveServices());
        final List<Long> assetIdsWithActiveService = executeMastIhsQuery(queryBuilder, null, null, Boolean.FALSE)
            .stream().map(BaseDto::getId).collect(Collectors.toList());
        assets.getContent().stream().filter(asset -> asset.getCode().startsWith(CDConstants.MAST_PREFIX))
            .filter(asset -> assetIdsWithActiveService.contains(asset.getId()))
            .forEach(asset -> asset.setHasActiveServices(Boolean.TRUE));
      }
    }

    return assets;
  }

  @RestrictedAsset
  @Override
  public final AssetPageResource getAssetByQueryBuilder(final String userId, final AssetQueryHDto assetQuery,
      final MastQueryBuilder assetQueryBuilder, final Integer page, final Integer size)
      throws ClassDirectException, IOException {

    AssetPageResource assets = getDefaultPaginatedAssets();
    MastQueryBuilder queryBuilder = assetQueryBuilder;

    if (!StringUtils.isEmpty(userId)) {
      // Get Subfleet only
      if (Optional.ofNullable(assetQuery).map(AssetQueryHDto::getIsSubfleet).isPresent()) {
        queryBuilder = subfleetQuery(userId, queryBuilder);
      }

      final UserProfiles user = userProfileService.getUser(userId);
      final List<Favourites> favourites = favouritesRepository.getUserFavourites(userId);
      Set<Long> mastIhsFavouriteAssets = new HashSet<Long>();
      Set<Long> mastOnlyFavouriteAssets = new HashSet<Long>();

      // prepare the mastonlyFavourite and mastIhsFavourite set depends on the favourite source
      favourites.stream().forEach(favourite -> {
        if (favourite.getSource().equals("LRV")) {
          mastOnlyFavouriteAssets.add(favourite.getAssetId());
        } else {
          mastIhsFavouriteAssets.add(favourite.getAssetId());
        }
      });

      // Set Favourite function
      /**
       * set the asset as Favourite, if the below conditions satisfies. The favourite source is IHS
       * & favourite id value matches with IMONumber. The favourite source is LRV & favourite id
       * value matches with assetId.
       */
      final Consumer<AssetHDto> setFavourite = (AssetHDto asset) -> {
        if (asset.getIhsAssetDto() != null) {
          asset.setIsFavourite(mastIhsFavouriteAssets.contains(Long.valueOf(asset.getImoNumber())));
        } else {
          asset.setIsFavourite(mastOnlyFavouriteAssets.contains(asset.getId()));
        }
      };

      if (Boolean.TRUE.equals(assetQuery.getIsFavourite())) {
        // Only return asset if user have "favorites" because asset
        // queried with "favorites" option.
        if (!favourites.isEmpty()) {
          // Set isFavourite on each asset
          queryBuilder = favouriteQuery(favourites, queryBuilder);
          filterByUser(queryBuilder, user);
          assets = getAssetsCall(queryBuilder, page, size, assetQuery.getSort(), assetQuery.getOrder(), setFavourite);
        }
      } else if (Boolean.TRUE.equals(assetQuery.getIsEOR())) {
        // Set EOR on each asset
        final Consumer<AssetHDto> setEor = asset -> asset.setIsEOR(assetQuery.getIsEOR());
        queryBuilder = eorQuery(userId, queryBuilder);
        assets = getAssetsCall(queryBuilder, page, size, assetQuery.getSort(), assetQuery.getOrder(), setEor);
      } else {
        filterByUser(queryBuilder, user);
        // Final step for subfleet query if there is no filter for
        // internal user without client /
        // flag / shipbuilder
        if (queryBuilder.isEmpty() && user.getClientCode() == null
            && (user.getFlagCode() == null || user.getFlagCode().isEmpty()) && user.getShipBuilderCode() == null) {
          queryBuilder.mandatory(assetId(QueryRelationshipType.GT, 0L));
        }
        assets = getAssetsCall(queryBuilder, page, size, assetQuery.getSort(), assetQuery.getOrder(), setFavourite);
      }
      final long hydrateStartTime = System.currentTimeMillis();
      hydrateAssetList(assets.getContent());
      if (assets.getContent() != null) {
        LOGGER.debug("Hydrated {} assets. | Elapsed Time (ms): {}", assets.getContent().size(),
            (System.currentTimeMillis() - hydrateStartTime));
      }
      calculateOverallStatus(assets.getContent());
    }
    return assets;
  }

  /**
   * Hydrates AssetType and CFO office details in asset list with resource injection.
   *
   * @param assets the asset list.
   */
  private void hydrateAssetList(final List<AssetHDto> assets) {
    if (assets != null && !assets.isEmpty()) {
      final List<Callable<Void>> callables = new ArrayList<>();
      assets.stream().forEach(assetHdto -> callables.add(() -> {
        if (assetHdto.getAssetType() != null) {
          AssetTypeHDto hdto = AssetCodeUtil.convertDtoToHDto(assetHdto.getAssetType());
          Resources.inject(hdto);
          assetHdto.setAssetTypeHDto(hdto);
        }
        assetHdto.setCfoOfficeH(getCfoOfficeDetails(assetHdto));
        return null;
      }));

      final ExecutorService executorService =
          ExecutorUtils.newFixedThreadPool(CDConstants.AVAILABLE_PROCESSORS, this.getClass().getSimpleName());
      try {
        executorService.invokeAll(callables);
      } catch (InterruptedException exception) {
        LOGGER.error("Error while hydrating: {} ", exception);
      } finally {
        executorService.shutdown();
      }
    }
  }

  /**
   * @param assetList list of assets.
   * @throws ClassDirectException exception.
   */
  protected final void calculateOverallStatus(final List<AssetHDto> assetList) throws ClassDirectException {
    final EarliestEntityQueryDto earliestEntityQuery = new EarliestEntityQueryDto();
    List<AssetEarliestEntitiesHDto> entities = null;
    if (assetList != null && !assetList.isEmpty()) {
      final Map<Long, AssetHDto> assetMap =
          assetList.stream().collect(Collectors.toMap(AssetHDto::getId, Function.identity()));

      final List<Long> ids = new ArrayList<>(assetMap.keySet());

      earliestEntityQuery.setAssetIdList(ids);
      earliestEntityQuery.setServiceStatusIdList(assetServiceStatusList);
      earliestEntityQuery.setExcludedServiceCodeList(excludedServicesList);
      earliestEntityQuery.setActionableItemStatusIdList(actionableItemStatusList);
      earliestEntityQuery.setTaskStatusIdList(taskStatusList);
      earliestEntityQuery.setCocStatusIdList(cocStatusList);
      earliestEntityQuery.setStatutoryFindingStatusIdList(statutoryFindingStatusIdList);

      final Call<List<AssetEarliestEntitiesHDto>> earliestEntitiesCall =
          assetServiceDelegate.assetEarliestEntities(earliestEntityQuery);
      if (earliestEntitiesCall != null) {
        try {
          entities = earliestEntitiesCall.execute().body();
          if (entities != null) {
            entities.forEach(earliestEntities -> {
              try {
                final Long assetId = earliestEntities.getAssetId();
                final AssetHDto asset = assetMap.get(assetId);
                if (asset != null) {
                  final long dueStatusStartTime = System.currentTimeMillis();
                  calculateSingleAssetDueStatus(asset, earliestEntities);
                  LOGGER.debug("Populated due status for asset with id: {}. | Elapsed Time (ms): {}", assetId,
                      (System.currentTimeMillis() - dueStatusStartTime));
                } else {
                  LOGGER.error("Cannot find asset with id: {}", assetId);
                }
              } catch (final Exception e) {
                LOGGER.error("Exception in processing Mast API : {}", e.getMessage(), e);
              }
            });
          }
        } catch (final IOException e) {
          LOGGER.error("Earliest entities not found: {}", e.getMessage(), e);
          throw new ClassDirectException("Earliest entities not found: " + e.getMessage());
        }
      }
    }
  }

  /**
   * @param asset asset.
   * @param earliestEntities earliestEntity from Mast.
   * @throws ClassDirectException exception.
   */
  protected final void calculateSingleAssetDueStatus(final AssetHDto asset,
      final AssetEarliestEntitiesHDto earliestEntities) throws ClassDirectException {

    final CoCDto cocDto = earliestEntities.getEarliestCoCByDueDate();
    final ActionableItemDto statuActItemDto = earliestEntities.getEarliestStatutoryActionableItemByDueDate();
    final ActionableItemDto nonstatuActItemDto = earliestEntities.getEarliestNonStatutoryActionableItemByDueDate();
    final ScheduledServiceDto upperServiceDto = earliestEntities.getEarliestServiceByUpperRangeDateOrDueDate();
    final ScheduledServiceDto lowerServiceDto = earliestEntities.getEarliestServiceByLowerRangeDate();
    final ScheduledServiceDto postponementserviceDto = earliestEntities.getEarliestServiceByPostponementDate();
    final WorkItemDto nonPmsTaskDto = earliestEntities.getEarliestNonPMSTaskByDueDate();
    final WorkItemDto pmsTaskDto = earliestEntities.getEarliestPMSTaskByDueDate();
    final WorkItemDto nonPmsTaskPostponmentDto = earliestEntities.getEarliestNonPMSTaskByPostponementDate();
    final WorkItemDto pmsTaskPostponmentDto = earliestEntities.getEarliestPMSTaskByPostponementDate();
    final StatutoryFindingDto statutoryFindingByDueDateDto = earliestEntities.getEarliestStatutoryFindingByDueDate();

    // Coc.
    final CoCHDto coc = AssetOverAllStatusUtils.mastToCdDto(cocDto, new CoCHDto(), cocService);

    // Actionable Items.
    final ActionableItemHDto statuActItem =
        AssetOverAllStatusUtils.mastToCdDto(statuActItemDto, new ActionableItemHDto(), actionableItemService);

    final ActionableItemHDto nonstatuActItem =
        AssetOverAllStatusUtils.mastToCdDto(nonstatuActItemDto, new ActionableItemHDto(), actionableItemService);

    // Scheduled Service.
    final ScheduledServiceHDto upperService =
        AssetOverAllStatusUtils.mastToCdDto(upperServiceDto, new ScheduledServiceHDto(), scheduleService);

    final ScheduledServiceHDto lowerService =
        AssetOverAllStatusUtils.mastToCdDto(lowerServiceDto, new ScheduledServiceHDto(), scheduleService);

    final ScheduledServiceHDto postponementservice =
        AssetOverAllStatusUtils.mastToCdDto(postponementserviceDto, new ScheduledServiceHDto(), scheduleService);

    // Tasks.
    final WorkItemLightHDto nonPmsTask =
        AssetOverAllStatusUtils.mastToCdDto(nonPmsTaskDto, new WorkItemLightHDto(), taskService);

    final WorkItemLightHDto nonPmsTaskPostponment =
        AssetOverAllStatusUtils.mastToCdDto(nonPmsTaskPostponmentDto, new WorkItemLightHDto(), taskService);

    final WorkItemLightHDto pmsTask =
        AssetOverAllStatusUtils.mastToCdDto(pmsTaskDto, new WorkItemLightHDto(), taskService);

    // Statutory finding.
    final StatutoryFindingHDto statutoryFinding = AssetOverAllStatusUtils.mastToCdDto(statutoryFindingByDueDateDto,
        new StatutoryFindingHDto(), statutoryFindingService);

    // if the Due Date has passed and the task is eligible for PMS i.e task
    // goes to IMMINENT.
    if (DueStatus.OVER_DUE.equals(AssetOverAllStatusUtils.getDueStatus(pmsTask))) {
      pmsTask.setDueStatusH(DueStatus.IMMINENT);
    }

    final WorkItemLightHDto pmsTaskPostponment =
        AssetOverAllStatusUtils.mastToCdDto(pmsTaskPostponmentDto, new WorkItemLightHDto(), taskService);

    // if the Postponement Date has passed and the task is eligible for PMS
    // i.e task goes to
    // IMMINENT.
    if (DueStatus.OVER_DUE.equals(AssetOverAllStatusUtils.getDueStatus(pmsTaskPostponment))) {
      pmsTaskPostponment.setDueStatusH(DueStatus.IMMINENT);
    }

    // check any service with postponement date LRCD-3222.
    if (postponementservice != null) {
      asset.setHasPostponedService(true);
    }
    // OverAll Status Calculation for Asset.
    final DueStatus assetStatus = Arrays
        .asList(new IDueStatusDto[] {statuActItem, nonstatuActItem, coc, upperService, lowerService,
            postponementservice, pmsTask, pmsTaskPostponment, nonPmsTask, nonPmsTaskPostponment, statutoryFinding})
        .stream().map(AssetOverAllStatusUtils::getDueStatus).filter(s -> s != null)
        .sorted((s1, s2) -> Integer.compare(s1.getPriority(), s2.getPriority())).findFirst().orElse(DueStatus.NOT_DUE);
    LOGGER.debug("Asset {},   dueStatus {}", asset.getId(), assetStatus);

    asset.setDueStatusH(assetStatus);
  }

  /**
   * Create query for subfleet.
   *
   * @param userId The target user id.
   * @param queryBuilder The query builder.
   * @return The mast query builder.
   * @throws ClassDirectException The CD exception.
   * @throws IOException The io exception.
   */
  private MastQueryBuilder subfleetQuery(final String userId, final MastQueryBuilder queryBuilder)
      throws ClassDirectException, IOException {

    // If subfleet is not enable, do not need to pass in all the assetId
    final boolean subfleetEnabled = subFleetService.isSubFleetEnabled(userId);

    if (subfleetEnabled) {
      final List<Long> subfleetIds = subFleetService.getSubFleetIds(userId);
      queryBuilder.mandatory(assetId(QueryRelationshipType.IN, subfleetIds.toArray(new Long[] {})));
    }
    return queryBuilder;
  }

  /**
   * Method to create query for favourites.
   *
   * @param favourites favourites.
   * @param queryBuilder queryBuilder.
   * @return MastQuery.
   * @throws ClassDirectException exception.
   */
  private MastQueryBuilder favouriteQuery(final List<Favourites> favourites, final MastQueryBuilder queryBuilder)
      throws ClassDirectException {
    List<Long> filterableAssetIds = new ArrayList<>();
    List<Long> filterableIHSAssetIds = new ArrayList<>();

    final Map<String, List<Long>> favouriteIds = favourites.stream().collect(
        Collectors.groupingBy(Favourites::getSource, Collectors.mapping(Favourites::getAssetId, Collectors.toList())));

    if (!favouriteIds.isEmpty()) {
      if (favouriteIds.containsKey(CDConstants.IHS_PREFIX)) {
        filterableIHSAssetIds = favouriteIds.get(CDConstants.IHS_PREFIX);
      }
      if (favouriteIds.containsKey(CDConstants.MAST_PREFIX)) {
        filterableAssetIds = favouriteIds.get(CDConstants.MAST_PREFIX);
      }
    }
    if (!filterableIHSAssetIds.isEmpty() && !filterableAssetIds.isEmpty()) {

      queryBuilder.ihsPriorityOptional(assetId(QueryRelationshipType.IN, filterableAssetIds.toArray(new Long[] {})));
      queryBuilder
          .ihsPriorityOptional(ihsAssetId(QueryRelationshipType.IN, filterableIHSAssetIds.toArray(new Long[] {})));
    }
    if (!filterableAssetIds.isEmpty() && filterableIHSAssetIds.isEmpty()) {

      queryBuilder.ihsPriorityMandatory(assetId(QueryRelationshipType.IN, filterableAssetIds.toArray(new Long[] {})));
    }
    if (!filterableIHSAssetIds.isEmpty() && filterableAssetIds.isEmpty()) {

      queryBuilder
          .ihsPriorityMandatory(ihsAssetId(QueryRelationshipType.IN, filterableIHSAssetIds.toArray(new Long[] {})));
    }
    return queryBuilder;
  }

  /**
   * Method to create query for eor assets.
   *
   * @param userId userId.
   * @param queryBuilder queryBuilder.
   * @return MastQuery.
   * @throws ClassDirectException exception.
   */
  private MastQueryBuilder eorQuery(final String userId, final MastQueryBuilder queryBuilder)
      throws ClassDirectException {
    final Set<String> eorImos = userProfileService.getEorsByUserId(userId);
    queryBuilder.mandatory(assetImoNumber(QueryRelationshipType.IN, eorImos.toArray(new String[] {})));
    return queryBuilder;
  }

  /**
   * @return AssetPageResource pageresource.
   */
  private AssetPageResource getDefaultPaginatedAssets() {
    final AssetPageResource assets = new AssetPageResource();
    assets.setContent(Collections.emptyList());
    final PaginationDto pagination = new PaginationDto();
    pagination.setSize(INITIAL_SIZE);
    pagination.setNumber(INITIAL_SIZE);
    pagination.setFirst(false);
    pagination.setLast(false);
    pagination.setTotalPages(INITIAL_SIZE);
    pagination.setTotalElements(0L);
    pagination.setNumberOfElements(INITIAL_SIZE);
    assets.setPagination(pagination);
    return assets;
  }

  @Override
  public final MastQueryBuilder populateAssetQuery(final UserProfiles userProfiles, final MastQueryBuilder assetQuery)
      throws IOException, ClassDirectException {
    return assetQuery;
  }

  /**
   * MAST call to get assets.
   *
   * @param queryBuilder query.
   * @param page page.
   * @param size size.
   * @param extraProcesses List of consumers to do extra processing on each asset.
   * @param order order by.
   * @param sort sort by.
   * @return assets.
   * @throws ClassDirectException exception.
   */
  private AssetPageResource getAssetsCall(final MastQueryBuilder queryBuilder, final Integer page, final Integer size,
      final SortOptionEnum sort, final OrderOptionEnum order, final Consumer<AssetHDto>... extraProcesses)
      throws ClassDirectException {

    final AssetPageResource assetPageResource = mastIhsQuery(queryBuilder, page, size, sort, order);
    if (assetPageResource.getContent() != null && !assetPageResource.getContent().isEmpty()) {
      assetPageResource.getContent().parallelStream().forEach(asset -> {

        for (final Consumer<AssetHDto> process : extraProcesses) {
          process.accept(asset);
        }
      });
    }
    return assetPageResource;
  }

  /**
   * Get assets filtered by {@link AssetQueryHDto}.
   *
   * @param page page number.
   * @param size result size.
   * @param query filter.
   * @return page resource for {@link AssetHDto}.
   * @throws ClassDirectException when exception.
   * @throws IOException when error.
   */
  @RestrictedAsset
  @Override
  public final AssetPageResource getAssetsQueryPageResource(final String userId, final Integer page, final Integer size,
      final String sort, final String order, final AssetQueryHDto query) throws ClassDirectException, IOException {
    LOGGER.debug("Querying assets with page {} and size {}.", page, size);
    AssetPageResource pageResource = null;
    if (sort != null) {
      try {
        query.setSort(SortOptionEnum.valueOf(sort));
      } catch (final IllegalArgumentException exception) {
        LOGGER.debug("Unable to add sort option." + exception.getMessage());
      }
    }

    if (null != order) {
      try {
        query.setOrder(OrderOptionEnum.valueOf(order));
      } catch (final IllegalArgumentException exception) {
        LOGGER.debug("Unable to add order option." + exception.getMessage());
      }
    }
    pageResource = getAssets(userId, query, page, size);
    return pageResource;

  }

  @RestrictedAsset
  @Override
  public final AssetHDto assetByCode(final String userId, final String assetCode) throws ClassDirectException {

    final MastQueryBuilder query = createQueryAssetByCode(assetCode);
    filterByCurrentUser(query);
    final List<AssetHDto> assets = mastIhsQuery(query, null, null);
    final AssetHDto asset = getAssetByAssetCode(assets, assetCode);

    final MastQueryBuilder activeServiceQuery = createQueryAssetByCode(assetCode);
    activeServiceQuery.mandatory(hasActiveServices());
    final List<AssetHDto> filteredAssets = mastIhsQuery(activeServiceQuery, null, null);
    asset.setHasActiveServices(!filteredAssets.isEmpty());

    hydrateAssetList(assets);
    calculateOverallStatus(assets);

    Optional.ofNullable(userId).ifPresent(
        id -> asset.setIsFavourite(favouritesRepository.isUserFavourite(userId, AssetCodeUtil.getId(assetCode))));

    // set the eor flag
    if (SecurityUtils.isEORUser()) {
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      if (token != null) {
        final Set<String> eorImos = userProfileService.getEorsByUserId(token.getPrincipal());
        final String assetImo = AssetCodeUtil.getAssetImo(asset);
        if (eorImos.contains(assetImo)) {
          asset.setIsEOR(true);
        }
      }
    }

    return asset;
  }

  /**
   * Gets CFO Office address for a given asset.
   *
   * @param asset the asset model.
   * @return CFO office details.
   * @throws ClassDirectException when error in getting CFO office details or any error in
   *         execution/executor service.
   */
  private OfficeDto getCfoOfficeDetails(final AssetHDto asset) {
    // default CFO officeId of an given asset. In case either multiple CFO
    // office exist or no CFO
    // office.
    Long officeId = Long.valueOf(defaultAssetCFO);
    OfficeDto officeCFO = null;
    try {
      if (asset.getOffices() != null) {
        List<OfficeLinkDto> officeLinkDtos = asset.getOffices().stream()
            .filter(dto -> dto.getOfficeRole().getId().longValue() == OfficeRole.CFO.getValue().longValue())
            .filter(office -> office.getOffice() != null).collect(Collectors.toList());
        // office
        if (officeLinkDtos.size() == 1) {
          LOGGER.error("The CFO Office {} for asset {} ", officeLinkDtos.get(0).getOffice().getId(), asset.getId());
          officeId = officeLinkDtos.get(0).getOffice().getId();
        }
      }
      if (officeId > 0) {
        officeCFO = employeeReferenceService.getOffice(officeId);
      }
    } catch (final Exception exception) {
      LOGGER.error("Error in getting CFO Office {} for asset {} ", officeId, asset.getId(), exception);
    }
    return officeCFO;
  }

  @Override
  public final StatusDto createFavourite(@NotNull final String userId, @NotNull final String assetCode)
      throws ClassDirectException {
    final StatusDto status = new StatusDto(successfulUpdate);
    final Favourites favourites = favouritesRepository.verifyFavourite(userId, assetCode);
    if (favourites == null) {
      try {
        favouritesRepository.createUserFavourite(userId, assetCode);
      } catch (final DataIntegrityViolationException exception) {
        LOGGER.error("Error while create favourite", exception);
      }
    }
    return status;
  }

  @Override
  public final StatusDto deleteFavourite(@NotNull final String userId, @NotNull final String assetCode)
      throws ClassDirectException {
    final StatusDto status = new StatusDto(successfulUpdate);
    final Favourites favourites = favouritesRepository.verifyFavourite(userId, assetCode);
    if (favourites != null) {
      try {
        favouritesRepository.deleteUserFavourite(userId, assetCode);
      } catch (final IllegalArgumentException exception) {
        LOGGER.error("Error while delete favourite", exception);
      }
    }

    return status;
  }

  @RestrictedAsset
  @Override
  public final AssetPageResource getRegisterBookSister(final String leadShip, final String userId,
      final String assetCode, final Integer page, final Integer size) throws ClassDirectException, IOException {
    LOGGER.debug("Querying register book sisters assets with page {} and size {}.", page, size);
    AssetPageResource assets = null;
    if (!StringUtils.isEmpty(leadShip)) {
      assets = getRegisterBookSisterAssets(leadShip, userId, assetCode, page, size);
      calculateOverallStatus(assets.getContent());
    } else {
      throw new ClassDirectException("Invalid leadShip to fetch register book sisters: " + leadShip);
    }
    return assets;
  }

  @Override
  public final Long getRegisterBookSistersCount(final String leadShip, final String userId, final String assetCode)
      throws ClassDirectException {
    LOGGER.debug("Getting register book sisters count for lead IMO {}", leadShip);
    final Long intialCount = 0L;
    if (!StringUtils.isEmpty(leadShip)) {
      return Optional.ofNullable(getRegisterBookSisterAssets(leadShip, userId, assetCode, DEFAULT_PAGE, DEFAULT_SIZE))
          .map(AssetPageResource::getPagination).map(PaginationDto::getTotalElements).orElse(intialCount);
    } else {
      return intialCount;
    }
  }

  /**
   * Returns list of paginated assets has same lead IMO(Register Book Sister Assets).
   *
   * @param leadShip the leadShip IMO number to fetch register book sisters of asset.
   * @param userId userId the unique identification of user.
   * @param assetCode the asset shouldn't include in register book sisters of asset.
   * @param page page the number of pages to be returned.
   * @param size size the size of elements to be returned.
   * @return paginated list of register book sister assets.
   * @throws ClassDirectException if any API call or execution fails.
   */
  private AssetPageResource getRegisterBookSisterAssets(final String leadShip, final String userId,
      final String assetCode, final Integer page, final Integer size) throws ClassDirectException {
    AssetPageResource assetPageResource = getDefaultPaginatedAssets();
    final UserProfiles user = userProfileService.getUser(userId);
    Integer currentPage = null;
    try {
      if (page != null && page > 0) {
        currentPage = page - 1;
      }
      final MastQueryBuilder query =
          MastQueryBuilder.create().mandatory(assetId(QueryRelationshipType.NE, true, AssetCodeUtil.getId(assetCode)));
      filterByUser(query, user);
      final AbstractQueryDto queryDto = query.build();
      IhsSisterQueryDto sisterQueryDto = new IhsSisterQueryDto();
      sisterQueryDto.setLeadImoNumber(leadShip);
      sisterQueryDto.setAdditionalFilter(queryDto);
      final Call<MultiAssetPageResourceDto> registerBookSisterCall =
          ihsServiceDelegate.registerBookSistersIhsQuery(sisterQueryDto, currentPage, size, null, null);
      final MultiAssetPageResourceDto registerBookSistersDto = registerBookSisterCall.execute().body();
      if (registerBookSistersDto != null) {
        final List<MultiAssetDto> multiAssetDtos = registerBookSistersDto.getContent();
        final List<AssetHDto> assets = extractAssetHDto(multiAssetDtos, true);
        assetPageResource.setContent(assets);
        assetPageResource.setPagination(registerBookSistersDto.getPagination());
      }
    } catch (final IOException exception) {
      LOGGER.error("Exception when executing ihsServiceDelegate to fetch register book sisters.", exception);
      throw new ClassDirectException(exception.getMessage());
    }
    return resetMastPagination(assetPageResource);
  }

  @Override
  public final Long getTechnicalSistersCount(final String leadImo, final String userId, final String assetCode)
      throws ClassDirectException {
    LOGGER.debug("Getting technical sisters count for lead IMO {}", leadImo);
    final Long intialCount = 0L;
    if (!StringUtils.isEmpty(leadImo) && !StringUtils.isEmpty(assetCode)) {
      return Optional.ofNullable(getTechnicalSisterAssets(leadImo, userId, assetCode, DEFAULT_PAGE, DEFAULT_SIZE))
          .map(AssetPageResource::getPagination).map(PaginationDto::getTotalElements).orElse(intialCount);
    } else if (!StringUtils.isEmpty(assetCode)) {
      return intialCount;
    } else {
      throw new ClassDirectException("Invalid asset : " + assetCode + " to fetch technical sisters");
    }
  }

  @RestrictedAsset
  @Override
  public final AssetPageResource getTechnicalSister(final String leadImo, final String userId, final String assetCode,
      final Integer page, final Integer size) throws ClassDirectException, IOException {
    LOGGER.debug("Querying technical sister assets with page {} and size {}.", page, size);
    AssetPageResource assets = null;
    if (!StringUtils.isEmpty(leadImo) && !StringUtils.isEmpty(assetCode)) {
      assets = getTechnicalSisterAssets(leadImo, userId, assetCode, page, size);
      calculateOverallStatus(assets.getContent());
    } else {
      throw new ClassDirectException(
          "Invalid lead IMO: " + leadImo + " or asset : " + assetCode + " to fetch technical sisters");
    }
    return assets;
  }

  /**
   * Returns list of paginated assets has same lead IMO(Technical sister assets).
   *
   * @param leadImo the IMO number to fetch technical sisters of asset.
   * @param userId userId the unique identification of user.
   * @param assetCode the assetCode to fetch technical sisters of asset.
   * @param page page the number of pages to be returned.
   * @param size size the size of elements to be returned.
   * @return paginated list of technical sister assets.
   * @throws ClassDirectException if any API call or execution fails.
   */
  private AssetPageResource getTechnicalSisterAssets(final String leadImo, final String userId, final String assetCode,
      final Integer page, final Integer size) throws ClassDirectException {
    final UserProfiles user = userProfileService.getUser(userId);
    final MastQueryBuilder query = MastQueryBuilder.create().mandatory(leadImo(leadImo))
        .mandatory(assetId(QueryRelationshipType.NE, AssetCodeUtil.getId(assetCode)));
    filterByUser(query, user);
    AssetPageResource assets = mastIhsQuery(query, page, size, null, null);
    return assets;
  }

  @RestrictedAsset
  @Override
  public final CodicilActionableHDto getCodicils(@AssetID final Long assetId, final CodicilDefectQueryHDto queryHDto)
      throws IOException, ClassDirectException {
    final CodicilActionableHDto actionableHDto = new CodicilActionableHDto();

    if (queryHDto != null && queryHDto.getStatusList() != null) {
      List<CodicilStatusHDto> codicilStatuses = assetReferenceService.getCodicilStatuses();

      List<Long> codicilStatusIds = codicilStatuses.stream()
          .filter(status -> status.getTypeId().equals(CodicilType.COC.getId())
              && queryHDto.getStatusList().contains(status.getId()))
          .map(CodicilStatusHDto::getId).collect(Collectors.toList());

      List<Long> actionableItemStatusIds = codicilStatuses.stream()
          .filter(status -> status.getTypeId().equals(CodicilType.AI.getId())
              && queryHDto.getStatusList().contains(status.getId()))
          .map(CodicilStatusHDto::getId).collect(Collectors.toList());

      List<Long> statutoryStatusIds = codicilStatuses.stream()
          .filter(status -> status.getTypeId().equals(CodicilType.SF.getId())
              && queryHDto.getStatusList().contains(status.getId()))
          .map(CodicilStatusHDto::getId).collect(Collectors.toList());

      // queryHDto.getStatusList().clear();
      queryHDto.setStatusList(codicilStatusIds);
      final Call<List<CoCHDto>> chDtos = assetServiceDelegate.getCoCQueryDto(queryHDto, assetId);
      final List<CoCHDto> cocHDtos = chDtos.execute().body();

      queryHDto.getStatusList().clear();
      queryHDto.setStatusList(actionableItemStatusIds);
      final Call<List<ActionableItemHDto>> actionableItemHDtos =
          assetServiceDelegate.getActionableItemQueryDto(queryHDto, assetId);
      final List<ActionableItemHDto> actionableItems = actionableItemHDtos.execute().body();

      queryHDto.getStatusList().clear();
      queryHDto.setStatusList(statutoryStatusIds);
      final List<StatutoryFindingHDto> statutoryFindings = statutoryFindingService.getStatutories(assetId, queryHDto);

      final ExecutorService executorService = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
      final List<Callable<Void>> callables = new ArrayList<Callable<Void>>();
      try {
        if (cocHDtos != null) {
          cocHDtos.forEach(coc -> {
            // hydrate job get job status
            callables.add(cocResponse(coc));
          });
        }
        if (actionableItems != null) {
          actionableItems.forEach(item -> {
            // hydrate job get job status
            callables.add(actItemResponse(item));
          });
        }
        final List<Future<Void>> taskList = executorService.invokeAll(callables);
        for (final Future<Void> task : taskList) {
          task.get();
        }
      } catch (InterruptedException | ExecutionException exception) {
        LOGGER.error("Error while executing AssetService.getCodicil: {} ", exception);
      } finally {
        executorService.shutdown();
      }
      actionableHDto.setActionableItemHDtos(actionableItems);
      actionableHDto.setCocHDtos(cocHDtos);
      actionableHDto.setStatutoryFindingHDtos(statutoryFindings);
    }
    return actionableHDto;
  }

  /**
   * @param coc coc.
   * @return response.
   */
  private Callable<Void> cocResponse(final CoCHDto coc) {
    final Callable<Void> cocResp = () -> {
      try {
        if (coc.getJob() != null && coc.getJob().getId() != null) {
          Optional.ofNullable(jobServiceDelegate.getJobsByJobId(coc.getJob().getId()).execute().body())
              .ifPresent(job -> {
                Resources.inject(job);
                coc.setJobH(job);
              });
        }
      } catch (final IOException ioException) {
        LOGGER.error("getCodicils: Job not found for coc.", ioException);
      }
      Resources.inject(coc);
      if (coc.getAsset() != null) {
        coc.setCode(AssetCodeUtil.getMastCode(coc.getAsset().getId()));
      }
      return null;
    };
    return cocResp;
  }

  /**
   * @param item item.
   * @return response.
   */
  private Callable<Void> actItemResponse(final ActionableItemHDto item) {
    final Callable<Void> itemResp = () -> {
      try {
        if (item.getJob() != null && item.getJob().getId() != null) {
          Optional.ofNullable(jobServiceDelegate.getJobsByJobId(item.getJob().getId()).execute().body())
              .ifPresent(job -> {
                Resources.inject(job);
                item.setJobH(job);
              });
        }
      } catch (final IOException ioException) {
        LOGGER.error("getCodicils: Job not found for actionable iems.", ioException);
      }
      Resources.inject(item);
      if (item.getAsset() != null) {
        item.setCode(AssetCodeUtil.getMastCode(item.getAsset().getId()));
      }
      return null;
    };
    return itemResp;
  }

  @Override
  public final Long getAssetIdByImoNumber(final String imoNumber) throws ClassDirectException {
    final Long assetId = getAssetByImoNumber(imoNumber).getId();
    return assetId;
  }

  @Override
  public final AssetHDto getAssetByImoNumber(final String imoNumber) throws ClassDirectException {
    AssetHDto assetDetail = null;

    final List<String> imoNoList = Collections.singletonList(imoNumber);
    final MastQueryBuilder assetQuery = MastQueryBuilder.create()
        .mandatory(assetImoNumber(QueryRelationshipType.IN, imoNoList.toArray(new String[] {})));
    // filter query by user
    filterByCurrentUser(assetQuery);
    // Make query to MAST to get Asset.
    final List<AssetHDto> assets = mastIhsQuery(assetQuery, null, null);
    if (assets.isEmpty()) {
      throw new RecordNotFoundException(assetNotFound);
    } else {
      LOGGER.info("Asset result size: {}", assets.size());
      // Check size if it is zero, one or more.
      if (assets.size() > ONE_RECORD) {
        throw new ClassDirectException(moreThanOneAsset);
      } else {
        // Check if imo number is correct.
        final AssetHDto asset = assets.get(FIRST_ELEMENT_OF_RECORD);
        if (asset.getIhsAsset() != null && !asset.getIhsAsset().getId().equals(imoNumber)) {
          throw new RecordNotFoundException(invalidImoNumber);
        } else {
          assetDetail = asset;
          LOGGER.info("Imo Number : {}, Asset Id : {}", imoNumber, asset.getId());
        }
      }
    }
    return assetDetail;
  }

  /**
   * @param assetId long assetId.
   * @param query query.
   * @return list of services.
   * @throws IOException exception.
   * @throws RecordNotFoundException exception.
   */
  private List<ScheduledServiceHDto> getServices(final long assetId, final ServiceQueryDto query)
      throws IOException, RecordNotFoundException {
    query.setStatusIdList(serviceCreditStatusList);
    final Map<String, String> queryString = AssetCodeUtil.serviceQuerytoQueryMap(query);
    final Call<List<ScheduledServiceHDto>> call = serviceReferenceDelegate.getAssetServicesQuery(assetId, queryString);

    final List<ScheduledServiceHDto> services = call.execute().body();

    if (services == null) {
      throw new RecordNotFoundException(recordNotFoundForAsset, assetId);
    }
    List<ScheduledServiceHDto> filteredServices =
        services.stream().filter(serv -> serv.getActive().equals(Boolean.TRUE)).peek(service -> {
          Resources.inject(service);
          // inject product group.
          Optional<Long> productGroupId = Optional.ofNullable(service).map(ScheduledServiceHDto::getServiceCatalogueH)
              .map(ServiceCatalogueHDto::getProductCatalogue).map(ProductCatalogueDto::getProductGroup)
              .map(LinkResource::getId);
          if (productGroupId.isPresent()) {
            ProductGroupDto productGroup = productReferenceService.getProductGroup(productGroupId.get());
            if (productGroup != null) {
              service.getServiceCatalogueH().setProductGroupH(productGroup);
            }
          }
          if (service.getAsset() != null) {
            service.setCode(AssetCodeUtil.getMastCode(service.getAsset().getId()));
          }
        }).collect(Collectors.toList());

    return filteredServices;
  }

  /**
   * @param page page number.
   * @param size result size.
   * @param query filter.
   * @param assetId assetId.
   * @throws ClassDirectException when exception.
   * @throws CloneNotSupportedException exception.
   */
  @RestrictedAsset
  @Override
  public final PageResource<ScheduledServiceHDto> getServices(final Integer page, final Integer size,
      @AssetID final Long assetId, final ServiceQueryDto query) throws ClassDirectException, IOException {
    LOGGER.debug("Querying assets with page {} and size {}.", page, size);
    return PaginationUtil.paginate(getServices(assetId, query), page, size);
  }

  @Override
  public final DueStatus calculateOverAllStatus(final AssetHDto asset) {
    try {
      calculateOverallStatus(Collections.singletonList(asset));
      return asset.getDueStatusH();
    } catch (final ClassDirectException exception) {
      LOGGER.error("Error in retrieving Overall Due Status for Asset {}", exception.getMessage(), exception);
      return null;
    }
  }

  @Override
  public final AssetDetailsDto getAssetDetailsByAssetId(final Long assetId, final String userId, final Boolean isEor)
      throws ClassDirectException {

    final boolean eor = isEor.booleanValue();
    final MastQueryBuilder query = MastQueryBuilder.create().mandatory(assetIdAndIhsAssetId(assetId));
    if (!eor) {
      filterByCurrentUser(query);
    }
    final List<AssetHDto> assetDtos = mastIhsQuery(query, null, null);
    final boolean isAssetFound = !assetDtos.isEmpty();
    AssetDetailsDto assetDetail = null;
    if (isAssetFound) {
      final AssetHDto assetDto = assetDtos.get(FIRST_ELEMENT_OF_RECORD);
      assetDetail = getAssetDetailsByAsset(assetDto, userId);
    } else {
      throw new RecordNotFoundException(recordNotFoundForAsset, assetId);
    }
    return assetDetail;
  }

  @Override
  public final AssetDetailsDto getAssetDetailsByAsset(final AssetHDto assetDto, final String userId)
      throws ClassDirectException {
    AssetDetailsDto assetDetail = null;
    if (assetDto.getId() != null) {
      final boolean isLrAsset = assetDto.getCode().startsWith(CDConstants.MAST_PREFIX);
      // Get authentication user roles from user profiles. This flag is
      // used by for LR Only Asset,
      // IHS Only Asset and Both IHS and LR Asset.
      UserProfiles user = userProfileService.getUser(userId);
      final boolean isEquasisThetisUser = user.getRoles().stream().allMatch(role -> {
        final String roleName = role.getRoleName();
        if (roleName != null) {
          return roleName.equals(EQUASIS.toString()) || roleName.equals(THETIS.toString());
        }
        return false;
      });

      final List<Long> accessibleAssetIds = subFleetService.getSubFleetById(userId);
      if (user.getRestrictAll() != null && user.getRestrictAll()
          || (!accessibleAssetIds.isEmpty() && !accessibleAssetIds.contains(assetDto.getId()))) {
        assetDto.setRestricted(Boolean.TRUE);
      }

      // Checks if the asset is LR ONLY Asset and Not IHS Asset.
      if (isLrAsset && assetDto.getIhsAssetDto() == null) {
        assetDetail = populateLrOnlyAssets(assetDto);
      } else {
        // Cater for Only IHS Asset and Both LR Asset and IHS Asset.
        assetDetail = populateIhsAssets(assetDto, isLrAsset, isEquasisThetisUser);
      }
    }
    return assetDetail;

  }

  /**
   * Populate only IHS Asset's Registry Information.
   *
   * @param formerDataList IHS Former Asset Information.
   * @param ihsHistDto IHS History Information.
   * @param ihsAssetDto IHS Asset Information.
   * @return RegistryInformationDto.
   * @throws ClassDirectException ClassDirectException.
   */
  private RegistryInformationDto getIhsRegistryInformation(final IhsAssetDto ihsAssetDto,
      final List<IhsOvnaDto> formerDataList, final IhsHistDto ihsHistDto) throws ClassDirectException {

    final RegistryInformationDto registryInformationDto = new RegistryInformationDto();
    registryInformationDto.setPortOfRegistry(ihsAssetDto.getPortName());
    registryInformationDto.setCallSign(ihsAssetDto.getCallSign());
    Optional.ofNullable(ihsAssetDto.getDateOfBuild()).ifPresent(dateOfBuild -> {
      registryInformationDto
          .setDateOfBuild(DateUtils.getFormattedDatetoString(dateOfBuild, DATE_PATTERN_KEEL_AND_BUILD_DATE));
      registryInformationDto.setYearOfBuild(DateUtils.getFormattedDatetoString(dateOfBuild, DATE_PATTERN_YEAR_ONLY));
    });
    registryInformationDto.setYardNumber(ihsAssetDto.getYardNumber());
    registryInformationDto.setBuilder(ihsAssetDto.getBuilder());
    Optional.ofNullable(ihsAssetDto.getIhsAssetType()).ifPresent(assetType -> {
      registryInformationDto.setAssetTypeDetails(assetType.getStatDeCode());
    });
    Optional.ofNullable(formerDataList).ifPresent(formerAssetList -> {
      Set<String> assetnames = new HashSet<>();
      Set<String> formerAssetNames =
          formerAssetList.stream().map(IhsOvnaDto::getG01Name).distinct().collect(Collectors.toSet());
      formerAssetNames.removeIf(p -> !assetnames.add(p.toLowerCase()));
      // LRCD-3713 remove former asset names that matches to current asset name.
      formerAssetNames.remove(ihsAssetDto.getName());
      registryInformationDto.setFormerAssetNames(formerAssetNames.stream().collect(Collectors.joining(", ")));
    });
    Optional.ofNullable(ihsHistDto.getKeelLayingDate()).ifPresent(strKeelLayingDate -> {
      try {
        Optional.ofNullable(DateUtils.getFormattedStringDatetoDate(strKeelLayingDate, DATE_PATTERN_REVERSE))
            .ifPresent(keelLayingDate -> {
              registryInformationDto.setKeelLayingDate(
                  DateUtils.getFormattedDatetoString(keelLayingDate, DATE_PATTERN_KEEL_AND_BUILD_DATE));
            });
      } catch (final ParseException parseException) {
        LOGGER.error("Unable to parse keel laying date : {}", strKeelLayingDate, parseException);
      }
    });
    registryInformationDto.setFlag(ihsAssetDto.getFlagName());
    registryInformationDto.setCountryOfBuild(ihsAssetDto.getCountryOfBuild());
    return registryInformationDto;
  }

  /**
   * Populate Lloyds Register Asset's Registry Information.
   *
   * @param assetDto Asset HDto.
   * @return RegistryInformationDto.
   * @throws ClassDirectException ClassDirectException.
   */
  private RegistryInformationDto getLrRegistryInformation(final AssetHDto assetDto) throws ClassDirectException {
    final RegistryInformationDto registryInformationDto = new RegistryInformationDto();
    final List<BaseAssetDto> formerDataList = assetDto.getAllVersions();
    final AssetTypeDto assetTypeDto = assetDto.getAssetType();
    final IhsAssetDetailsDto ihsAssetDetailsDto = assetDto.getIhsAssetDto();
    // LRCD-3602 to get portname from ihs asset data instead of api call.
    Optional.ofNullable(ihsAssetDetailsDto).map(IhsAssetDetailsDto::getIhsAsset).ifPresent(ihsAsset -> {
      registryInformationDto.setPortOfRegistry(ihsAsset.getPortName());
    });
    Optional.ofNullable(assetTypeDto).map(AssetTypeDto::getName).ifPresent(assetTypeName -> {
      registryInformationDto.setAssetTypeDetails(assetTypeName);
    });

    Optional.ofNullable(assetDto.getCountryOfBuild()).map(LinkResource::getId).ifPresent(countryId -> {
      try {
        Optional.ofNullable(customerReferenceService.getCountry(countryId)).map(CountryHDto::getName)
            .ifPresent(countryName -> {
              registryInformationDto.setCountryOfBuild(countryName);
            });
      } catch (final ClassDirectException classDirectException) {
        LOGGER.error("Unable to retrieve country of build with countryid :{}", countryId, classDirectException);
      }
    });

    Optional.ofNullable(assetDto.getFlagStateDto()).map(FlagStateDto::getName).ifPresent(flagName -> {
      registryInformationDto.setFlag(flagName);
    });

    if (!CollectionUtils.isEmpty(formerDataList)) {
      // remove duplicate assets names irrespective of case sensitivity.
      Set<String> assetnames = new HashSet<>();
      Set<String> formerAssetNames =
          formerDataList.stream().filter(formerAsset -> formerAsset.getVersionId() < assetDto.getVersionId())
              .map(BaseAssetDto::getName).distinct().collect(Collectors.toSet());
      formerAssetNames.removeIf(p -> !assetnames.add(p.toLowerCase()));
      // LRCD-3713 remove former asset names that matches to current asset name.
      formerAssetNames.remove(assetDto.getName());
      registryInformationDto.setFormerAssetNames(formerAssetNames.stream().collect(Collectors.joining(", ")));
    }

    Optional.ofNullable(assetDto.getAssetLifeCycleStatusDto()).map(ReferenceDataDto::getName)
        .ifPresent(assetLifecycleStatusName -> {
          registryInformationDto.setAssetLifeCycleStatus(assetLifecycleStatusName);
        });

    registryInformationDto.setLead(assetDto.getIsLead());
    registryInformationDto.setCallSign(assetDto.getCallSign());
    registryInformationDto.setBuilder(assetDto.getBuilder());
    registryInformationDto.setYardNumber(assetDto.getYardNumber());

    Optional.ofNullable(assetDto.getKeelLayingDate()).ifPresent(keelLayingDate -> {
      registryInformationDto
          .setKeelLayingDate(DateUtils.getFormattedDatetoString(keelLayingDate, DATE_PATTERN_KEEL_AND_BUILD_DATE));
    });

    Optional.ofNullable(assetDto.getBuildDate()).ifPresent(dateOfBuild -> {
      registryInformationDto
          .setDateOfBuild(DateUtils.getFormattedDatetoString(dateOfBuild, DATE_PATTERN_KEEL_AND_BUILD_DATE));
      registryInformationDto.setYearOfBuild(DateUtils.getFormattedDatetoString(dateOfBuild, DATE_PATTERN_YEAR_ONLY));
    });

    return registryInformationDto;
  }

  /**
   * Populate LR Only Assets. (Not IHS Assets).
   *
   * @param assetDto Asset Hydrated Dto.
   * @return AssetDetailsDto Asset Details Dto.
   * @throws ClassDirectException ClassDirectException.
   */
  private AssetDetailsDto populateLrOnlyAssets(final AssetHDto assetDto) throws ClassDirectException {
    final AssetDetailsDto assetDetail = new AssetDetailsDto();
    // restrict view of details(Class history, notations and descriptive
    // notes section) for
    // restricted and non LRasset.
    final boolean isRestricted = assetDto.getRestricted();
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    try {
      // Initializing five pool thread.
      List<Callable<Void>> tasks = null;
      // Registry Information.
      final Callable<Void> populateRegistryInformation = () -> {
        assetDetail.setRegistryInformation(getLrRegistryInformation(assetDto));
        return null;
      };

      // Ruleset, class notation and descriptive note.
      final Callable<Void> populateRuleSetDetails = () -> {
        final RulesetDetailsDto rulesetDetailsDto = getRulesetDetails(assetDto.getMachineryClassNotation(),
            assetDto.getHullIndicator(), assetDto.getDescriptiveNote());
        assetDetail.setRulesetDetailsDto(rulesetDetailsDto);
        return null;
      };

      if (isRestricted) {
        tasks = Arrays.asList(populateRegistryInformation);
      } else {
        tasks = Arrays.asList(populateRegistryInformation, populateRuleSetDetails);
      }
      executor.invokeAll(tasks);
    } catch (final InterruptedException interruptedException) {
      LOGGER.error(interruptedException.getMessage());
      throw new ClassDirectException(interruptedException);
    } finally {
      executor.shutdown();
    }
    return assetDetail;
  }

  /**
   * Populate Ihs Assets. (Only IHS Assets and Both LR and IHS Assets.)
   *
   * @param assetDto Asset Hydrated Dto.
   * @param isLrAsset indicator if LR Asset or not.
   * @param isEquasisThetisUser indicator if Equasis / Thetis User or not.
   * @return AssetDetailsDto Asset Details Dto.
   * @throws ClassDirectException Class Direct Exception.
   */
  private AssetDetailsDto populateIhsAssets(final AssetHDto assetDto, final boolean isLrAsset,
      final boolean isEquasisThetisUser) throws ClassDirectException {

    final AssetDetailsDto assetDetail = new AssetDetailsDto();
    final IhsAssetDetailsDto ihsAssetDetailsDto = assetDto.getIhsAssetDto();
    final String imoNumber = ihsAssetDetailsDto.getId();
    final IhsAssetDto ihsAssetDto = ihsAssetDetailsDto.getIhsAsset();
    final IhsHistDto ihsHistDto = ihsAssetDetailsDto.getIhsHist();
    final List<IhsOvnaDto> formerDataList = ihsAssetDto.getFormerData();

    // Hide Other Personnel's information for Equasis/Thetis User.
    if (isEquasisThetisUser) {
      ihsAssetDto.setGboCode(null);
      ihsAssetDto.setOperatorCode(null);
      ihsAssetDto.setShipManagerCode(null);
      ihsAssetDto.setTechManagerCode(null);
      ihsAssetDto.setDocCode(null);
    }

    // restrict view of details(Class history, notations and descriptive
    // notes section) for
    // restricted and non LRasset.
    boolean isRestricted = false;
    if (isLrAsset) {
      isRestricted = assetDto.getRestricted();
    }

    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    try {
      // Initializing five pool thread.
      List<Callable<Void>> tasks = null;

      // Registry Information.
      final Callable<Void> populateRegistryInformation = () -> {
        RegistryInformationDto registryInformationDto = null;

        if (isLrAsset) {
          // Cater for Lloyds Register Asset's specific - Registry
          // information.
          registryInformationDto = getLrRegistryInformation(assetDto);
        } else {
          // Cater for IHS Asset specific - Registry information.
          registryInformationDto = getIhsRegistryInformation(ihsAssetDto, formerDataList, ihsHistDto);
        }

        final IhsCbsbDto ihsCbsb = ihsAssetDto.getIhsCbsb();
        if (ihsCbsb != null && ihsCbsb.getDecodeA() != null && ihsCbsb.getDecodeB() != null) {
          registryInformationDto.setYard(ihsCbsb.getDecodeA() + ihsCbsb.getDecodeB());
        }

        registryInformationDto.setOfficialNumber(ihsAssetDto.getOfficialNo());
        registryInformationDto.setMmsiNumber(ihsAssetDto.getMmsi());
        assetDetail.setRegistryInformation(registryInformationDto);
        return null;
      };
      // Principal Dimension.
      final Callable<Void> populatePrincipalDimension = () -> {
        final PrincipalDimensionDto principalDimensionDto = getPrincipalDimension(imoNumber);
        assetDetail.setPrincipalDimension(principalDimensionDto);
        return null;
      };
      // Ownership.
      final Callable<Void> populateOwnershipDetails = () -> {
        final OwnershipDto ownershipInfoDto = getOwnerShipAndManagementDetails(ihsAssetDto);
        assetDetail.setOwnership(ownershipInfoDto);
        return null;
      };

      if (isLrAsset) {
        // Ruleset, class notation and descriptive note.
        final Callable<Void> populateRuleSetDetails = () -> {
          final RulesetDetailsDto rulesetDetailsDto = getRulesetDetails(assetDto.getMachineryClassNotation(),
              assetDto.getHullIndicator(), assetDto.getDescriptiveNote());
          assetDetail.setRulesetDetailsDto(rulesetDetailsDto);
          return null;
        };

        if (isEquasisThetisUser) {
          tasks = Arrays.asList(populateRegistryInformation, populatePrincipalDimension, populateRuleSetDetails,
              populateOwnershipDetails);
        } else {
          // Equipment Details.
          final Callable<Void> populateEquipmentDetails = () -> {
            final EquipmentDetailsDto equipmentInfoDto = getEquipmentDetails(imoNumber);
            assetDetail.setEquipmentDetails(equipmentInfoDto);
            return null;
          };
          // LRCD - 3138
          if (isRestricted) {
            tasks = Arrays.asList(populateRegistryInformation, populatePrincipalDimension, populateEquipmentDetails,
                populateOwnershipDetails);
          } else {
            tasks = Arrays.asList(populateRegistryInformation, populatePrincipalDimension, populateRuleSetDetails,
                populateEquipmentDetails, populateOwnershipDetails);
          }
        }
      } else {
        tasks = Arrays.asList(populateRegistryInformation, populatePrincipalDimension, populateOwnershipDetails);
      }
      executor.invokeAll(tasks);
    } catch (final InterruptedException interruptedException) {
      LOGGER.error(interruptedException.getMessage());
      throw new ClassDirectException(interruptedException);
    } finally {
      executor.shutdown();
    }
    return assetDetail;
  }

  /**
   * Retrieve Principal Dimension for given AssetHDto.
   *
   * @param imoNumber imo Number.
   * @return PrincipalDimensionDto.
   * @throws ClassDirectException Class Direct Exception.
   */
  private PrincipalDimensionDto getPrincipalDimension(final String imoNumber) throws ClassDirectException {
    final PrincipalDimensionDto principalDimensionDto = new PrincipalDimensionDto();
    try {
      final Call<IhsPrincipalDimensionsDto> ihsPrincipalDtoCall =
          ihsServiceDelegate.getPrincipalDimensionsByImoNumber(imoNumber);
      final IhsPrincipalDimensionsDto ihsPrincipalDimensionsDto = ihsPrincipalDtoCall.execute().body();

      LOGGER.debug("ihsPrincipalDimensionsDto : {}", ihsPrincipalDimensionsDto);

      // Checks if the principal dimension dto is null or empty.
      if (ihsPrincipalDimensionsDto == null || ihsPrincipalDimensionsDto.getLengthOverall() == null) {
        LOGGER.error(recordNotFoundForIHS, imoNumber);
      } else {
        principalDimensionDto.setLengthOverall(ihsPrincipalDimensionsDto.getLengthOverall());
        if (ihsPrincipalDimensionsDto.getLengthBtwPerpendiculars() != null) {
          NumberFormat formatter = new DecimalFormat("#.###");
          try {
            String lengthBtwPerpendiculars =
                formatter.format(Double.valueOf(ihsPrincipalDimensionsDto.getLengthBtwPerpendiculars()));
            principalDimensionDto.setLengthBetweenPerpendiculars(lengthBtwPerpendiculars);
          } catch (final NumberFormatException exception) {
            LOGGER.error("error occured in converting LengthBtwPerpendiculars {}", imoNumber, exception);
          }
        }
        principalDimensionDto.setBreadthMoulded(ihsPrincipalDimensionsDto.getBreadthMoulded());
        principalDimensionDto.setBreadthExtreme(ihsPrincipalDimensionsDto.getBreadthExtreme());
        principalDimensionDto.setDraughtMax(ihsPrincipalDimensionsDto.getDraughtMax());

        // TODO Air Draught information is not available in mast.

        principalDimensionDto.setDepthMoulded(ihsPrincipalDimensionsDto.getDepthMoulded());
        principalDimensionDto.setGrossTonnage(ihsPrincipalDimensionsDto.getGrossTonnage());
        principalDimensionDto.setNetTonnage(ihsPrincipalDimensionsDto.getNetTonnage());
        principalDimensionDto.setDeadWeight(ihsPrincipalDimensionsDto.getDeadWeight());
        principalDimensionDto.setDecks(ihsPrincipalDimensionsDto.getDecks());
        principalDimensionDto.setPropulsion(ihsPrincipalDimensionsDto.getPropulsion());
      }
    } catch (final IOException ioException) {
      LOGGER.error(ioException.getMessage());
      throw new ClassDirectException(ioException);
    }
    return principalDimensionDto;
  }

  /**
   * Retrieve Ruleset Details for given AssetHDto.
   *
   * @param machineryNotation Machinery Notation.
   * @param hullIndicator Hull Indicator.
   * @param descriptiveNote Descriptive Note.
   * @return RulesetDetailsDto.
   */
  private RulesetDetailsDto getRulesetDetails(final String machineryNotation, final Integer hullIndicator,
      final String descriptiveNote) {
    final RulesetDetailsDto rulesetDetailsDto = new RulesetDetailsDto();
    rulesetDetailsDto.setMachineryNotation(machineryNotation);
    rulesetDetailsDto.setDescriptiveNote(descriptiveNote);
    rulesetDetailsDto.setHullIndicator(hullIndicator);
    return rulesetDetailsDto;
  }

  /**
   * Retrieve Equipment Details for given AssetHDto.
   *
   * @param imoNumber imo Number.
   * @return EquipmentDetailsDto.
   * @throws ClassDirectException ClassDirectException.
   */
  private EquipmentDetailsDto getEquipmentDetails(final String imoNumber) throws ClassDirectException {
    final EquipmentDetailsDto equipmentInfoDto = new EquipmentDetailsDto();

    try {
      final Call<IhsEquipmentDetailsDto> ihsEquipmentDtoCall =
          ihsServiceDelegate.getEquipmentDetailsByImoNumber(imoNumber);
      final IhsEquipmentDetailsDto ihsEquipmentDetailsDto = ihsEquipmentDtoCall.execute().body();

      LOGGER.debug("ihsEquipmentDetailsDto : {}", ihsEquipmentDetailsDto);

      // Checks if the equipment details dto is null or empty.
      if (ihsEquipmentDetailsDto == null || ihsEquipmentDetailsDto.getEquipment2Dto() == null
          || ihsEquipmentDetailsDto.getEquipment4Dto() == null) {
        throw new RecordNotFoundException(recordNotFoundForIHS, imoNumber);
      } else {
        equipmentInfoDto.setCableLength(ihsEquipmentDetailsDto.getEquipment2Dto().getCableLength());
        equipmentInfoDto.setCableGrade(ihsEquipmentDetailsDto.getEquipment2Dto().getCableGrade());
        equipmentInfoDto.setCableDiameter(ihsEquipmentDetailsDto.getEquipment2Dto().getCableDiameter());
        equipmentInfoDto.setEquipmentLetter(ihsEquipmentDetailsDto.getEquipment4Dto().getEquipmentLetter());
      }
    } catch (final IOException ioException) {
      LOGGER.error(ioException.getMessage());
      throw new ClassDirectException(ioException);
    }
    return equipmentInfoDto;
  }

  /**
   * Retrieve OwnerShip and Management Details for given AssetHDto.
   *
   * @param ihsAssetDto Ihs Asset Details Dto.
   * @return OwnershipDto.
   */
  private OwnershipDto getOwnerShipAndManagementDetails(final IhsAssetDto ihsAssetDto) {
    final OwnershipDto ownershipDto = new OwnershipDto();

    if (ihsAssetDto.getOwner() != null && ihsAssetDto.getOwnerCode() != null) {
      UserPersonalInformationDto registeredOwnerDto =
          getPersonalInformation(ihsAssetDto.getOwner(), ihsAssetDto.getOwnerCode());
      if (registeredOwnerDto == null) {
        registeredOwnerDto = new UserPersonalInformationDto();
        registeredOwnerDto.setName(ihsAssetDto.getOwner());
      }
      ownershipDto.setRegisteredOwner(registeredOwnerDto);
    }

    if (ihsAssetDto.getGbo() != null && ihsAssetDto.getGboCode() != null) {
      UserPersonalInformationDto groupOwner = getPersonalInformation(ihsAssetDto.getGbo(), ihsAssetDto.getGboCode());
      if (groupOwner == null) {
        groupOwner = new UserPersonalInformationDto();
        groupOwner.setName(ihsAssetDto.getGbo());
      }
      ownershipDto.setGroupOwner(groupOwner);
    }

    if (ihsAssetDto.getOperator() != null && ihsAssetDto.getOperatorCode() != null) {
      UserPersonalInformationDto operatorDto =
          getPersonalInformation(ihsAssetDto.getOperator(), ihsAssetDto.getOperatorCode());
      if (operatorDto == null) {
        operatorDto = new UserPersonalInformationDto();
        operatorDto.setName(ihsAssetDto.getOperator());
      }
      ownershipDto.setOperator(operatorDto);
    }

    if (ihsAssetDto.getShipManager() != null && ihsAssetDto.getShipManagerCode() != null) {
      UserPersonalInformationDto shipManagerDto =
          getPersonalInformation(ihsAssetDto.getShipManager(), ihsAssetDto.getShipManagerCode());
      if (shipManagerDto == null) {
        shipManagerDto = new UserPersonalInformationDto();
        shipManagerDto.setName(ihsAssetDto.getShipManager());
      }
      ownershipDto.setShipManager(shipManagerDto);

    }

    if (ihsAssetDto.getTechManager() != null && ihsAssetDto.getTechManagerCode() != null) {
      UserPersonalInformationDto technicalManagerDto =
          getPersonalInformation(ihsAssetDto.getTechManager(), ihsAssetDto.getTechManagerCode());
      if (technicalManagerDto == null) {
        technicalManagerDto = new UserPersonalInformationDto();
        technicalManagerDto.setName(ihsAssetDto.getTechManager());
      }
      ownershipDto.setTechnicalManager(technicalManagerDto);

    }

    if (ihsAssetDto.getDocCompany() != null && ihsAssetDto.getDocCode() != null) {
      final Call<IhsCompanyContactDto> ihsCompanyContactDtoCall =
          ihsServiceDelegate.getCompanyContactByImoNumber(ihsAssetDto.getDocCode());
      UserPersonalInformationDto docCompanyDto = new UserPersonalInformationDto();

      try {
        final IhsCompanyContactDto companyContactDto = ihsCompanyContactDtoCall.execute().body();
        if (companyContactDto != null) {
          docCompanyDto.setName(ihsAssetDto.getDocCompany());
          List<String> addressList = Arrays.asList(companyContactDto.getStreetNo(), companyContactDto.getStreetName(),
              companyContactDto.getAddressLine1(), companyContactDto.getStreetName1(),
              companyContactDto.getStreetName2(), companyContactDto.getAddressLine2(),
              companyContactDto.getAddressLine3(), companyContactDto.getTownName1(), companyContactDto.getTownName2(),
              companyContactDto.getPrePostCode(), companyContactDto.getPostPostCode(),
              companyContactDto.getCountryFullName());
          final String address =
              addressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));

          docCompanyDto.setAddress(address);
          docCompanyDto.setEmailAddress(companyContactDto.getEmail());
          docCompanyDto.setPhoneNumber(companyContactDto.getTelephone());
          ownershipDto.setDoc(docCompanyDto);
        }
      } catch (final IOException ioException) {
        LOGGER.error("Record not found when getting doc company information {}", ihsAssetDto.getTechManagerCode(),
            ioException.getMessage());
      }
      if (StringUtils.isEmpty(docCompanyDto.getName())) {
        ownershipDto.setDoc(docCompanyDto);
        ownershipDto.getDoc().setName(ihsAssetDto.getDocCompany());
      }
    }

    return ownershipDto;
  }

  @Override
  public final UserPersonalInformationDto getPersonalInformation(final String name, final String contactImoNo) {
    UserPersonalInformationDto personalInformation = null;

    try {
      final Call<IhsPersonnelContactDto> ihsContactDtoCall =
          ihsServiceDelegate.getPersonnelContactByImoNumber(contactImoNo);

      final IhsPersonnelContactDto ihsContactDto = ihsContactDtoCall.execute().body();
      if (ihsContactDto != null) {
        personalInformation = new UserPersonalInformationDto();
        personalInformation.setName(name);
        List<String> addressList =
            Arrays.asList(ihsContactDto.getStreetNo(), ihsContactDto.getStreetName(), ihsContactDto.getAddressLine1(),
                ihsContactDto.getStreet1(), ihsContactDto.getStreet2(), ihsContactDto.getAddressLine2(),
                ihsContactDto.getAddressLine3(), ihsContactDto.getTownName1(), ihsContactDto.getTownName2(),
                ihsContactDto.getPrePostCode(), ihsContactDto.getPostPostCode(), ihsContactDto.getFullCountryName());
        final String address =
            addressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));
        personalInformation.setAddress(address);
        personalInformation.setEmailAddress(ihsContactDto.getEmail());
        personalInformation.setPhoneNumber(ihsContactDto.getTelephone());
      } else {
        LOGGER.error(recordNotFoundForContact, contactImoNo);
      }
    } catch (final IOException ioException) {
      LOGGER.error("Record not found when getting personal information {}", contactImoNo, ioException.getMessage());
    }
    return personalInformation;
  }

  /**
   * To get company information by code and customer role.
   *
   * @param docCode the docCode.
   * @param role role.
   * @return value.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  private IhsCustomersDto getCompanyInformation(final String docCode, final CustomerRolesEnum role)
      throws ClassDirectException, IOException {
    final IhsCustomersDto techManager = new IhsCustomersDto();
    final Call<IhsCompanyContactDto> ihsCompanyContactDtoCall =
        ihsServiceDelegate.getCompanyContactByImoNumber(docCode);

    final IhsCompanyContactDto companyDto = ihsCompanyContactDtoCall.execute().body();
    Optional.ofNullable(companyDto).ifPresent(company -> {
      techManager.setCompanyName(company.getCompanyName());
      techManager.setImoNumber(docCode);
      techManager.setRelationShip(role);
    });

    return techManager;
  }

  /**
   * Delegate execution to mast-ihs query.
   *
   * @param query query dto.
   * @param sort sort option.
   * @param order order option.
   * @return list of assets.
   * @throws ClassDirectException when error.
   */
  @Override
  public final List<AssetHDto> mastIhsQuery(final MastQueryBuilder query, final SortOptionEnum sort,
      final OrderOptionEnum order) throws ClassDirectException {
    LOGGER.debug("Performing query with object {} ", query.toString());
    return executeMastIhsQuery(query, sort, order, true);
  }

  /**
   * Check query dto empty.
   *
   * @param query dto.
   * @return empty flag.
   */
  private boolean checkQueryDtoEmpty(final AbstractQueryDto query) {
    boolean isEmpty = query == null;
    if (!isEmpty) {
      isEmpty = (StringUtils.isEmpty(query.getJoinField()) || StringUtils.isEmpty(query.getField()))
          && org.springframework.util.StringUtils.isEmpty(query.getValue());
      if (isEmpty && query.getAnd() != null && !query.getAnd().isEmpty()) {
        boolean allEmpty = true;
        for (final AbstractQueryDto dto : query.getAnd()) {
          if (!checkQueryDtoEmpty(dto)) {
            allEmpty = false;
          }
        }
        isEmpty = allEmpty;
      }

      if (isEmpty && query.getOr() != null && !query.getOr().isEmpty()) {
        boolean allEmpty = true;
        for (final AbstractQueryDto dto : query.getOr()) {
          if (!checkQueryDtoEmpty(dto)) {
            allEmpty = false;
          }
        }
        isEmpty = allEmpty;
      }
    }

    return isEmpty;
  }

  /**
   * Check priority query dto empty.
   *
   * @param query dto.
   * @return empty flag.
   */
  private boolean checkPriorityQueryDtoEmpty(final PrioritisedAbstractQueryDto query) {
    AbstractQueryDto abstractQuery = query.getMastPriorityQuery();
    boolean isQueryEmpty = true;
    if (query.getIhsPriorityQuery() != null || !checkQueryDtoEmpty(abstractQuery)) {
      isQueryEmpty = false;
    }
    return isQueryEmpty;
  }

  @Override
  public final List<AssetHDto> executeMastIhsQuery(final MastQueryBuilder query, final SortOptionEnum sort,
      final OrderOptionEnum order, final Boolean hydrateAssets) throws ClassDirectException {
    List<AssetHDto> assets;
    final PrioritisedAbstractQueryDto queryDto = query.buildMastIhs();
    if (checkPriorityQueryDtoEmpty(queryDto)) {
      throw new TooManyResultsException("Query dto has too many results.");
    }
    try {
      final List<MultiAssetDto> multiAssetDtos;
      if (null != sort && null != order) {
        multiAssetDtos =
            assetServiceDelegate.mastIhsQuery(queryDto, sort.getField(), order.name()).execute().body().getContent();
      } else {
        multiAssetDtos = assetServiceDelegate.mastIhsQuery(queryDto).execute().body().getContent();
      }
      assets = extractAssetHDto(multiAssetDtos, hydrateAssets);
    } catch (final IOException exception) {
      LOGGER.error("Exception when executing delegate.", exception);
      throw new ClassDirectException(exception);
    }
    LOGGER.debug("Found {} asset(s).", assets.size());

    return assets;
  }

  /**
   * Extract list of {@link AssetHDto} from a given Collection of {@link MultiAssetDto}.
   *
   * @param multiAssetDtos Collection of {@link MultiAssetDto}
   * @param hydrateAssets Whether or not to hydrate the assets.
   * @return A list of {@link AssetHDto}
   */
  private List<AssetHDto> extractAssetHDto(final Collection<MultiAssetDto> multiAssetDtos,
      final Boolean hydrateAssets) {
    final List<AssetHDto> assets = new ArrayList<>();
    if (multiAssetDtos != null && !multiAssetDtos.isEmpty()) {
      multiAssetDtos.stream()
          .filter(multiAssetDto -> multiAssetDto.getMastAsset() != null || multiAssetDto.getIhsAsset() != null)
          .forEach(multiAssetDto -> {
            AssetHDto asset = new AssetHDto();
            Boolean assetFound = false;

            asset.setImoNumber(multiAssetDto.getImoNumber());
            if (multiAssetDto.getMastAsset() != null && multiAssetDto.getIhsAsset() != null) {
              assetFound = true;
              BeanUtils.copyProperties(multiAssetDto.getMastAsset(), asset);
              asset.setIhsAssetDto(multiAssetDto.getIhsAsset());
              asset.setCode(AssetCodeUtil.getMastCode(asset.getId()));
              asset.setIsMMSService(multiAssetDto.getFlagMMS());
            } else if (multiAssetDto.getMastAsset() != null) {
              assetFound = true;
              BeanUtils.copyProperties(multiAssetDto.getMastAsset(), asset);
              asset.setCode(AssetCodeUtil.getMastCode(asset.getId()));
              asset.setIsMMSService(multiAssetDto.getFlagMMS());
            } else if (multiAssetDto.getIhsAsset() != null && multiAssetDto.getIhsAsset().getIhsAsset() != null) {
              assetFound = true;
              BeanUtils.copyProperties(multiAssetDto.getIhsAsset().getIhsAsset(), asset);
              try {
                asset.setId(Long.parseLong(multiAssetDto.getIhsAsset().getIhsAsset().getId()));
              } catch (final NumberFormatException exception) {
                LOGGER.error("Error in converting asset id {} ", multiAssetDto.getIhsAsset().getIhsAsset().getId(),
                    exception);
              }
              asset.setCode(AssetCodeUtil.getIhsCode(asset.getId()));
              asset.setIhsAssetDto(multiAssetDto.getIhsAsset());
              asset.setIsMMSService(Boolean.TRUE.equals(multiAssetDto.getFlagMMS()));
            } else {
              LOGGER.debug("No result found.");
            }

            if (assetFound) {
              try {
                if (hydrateAssets) {
                  asset = self.hydrateAssetData(asset);
                }
              } catch (final Exception e) {
                LOGGER.debug("Unable to hydrate asset data.");
              }

              assets.add(asset);
            }
          });
    }
    return assets;

  }

  /**
   * Perform mast-ihs query with pagination options.
   *
   * @param query query object.
   * @param page page number.
   * @param size page size.
   * @param sort sort options.
   * @param order order options.
   * @return asset page resource.
   * @throws ClassDirectException when error.
   */
  @Override
  public final AssetPageResource mastIhsQuery(final MastQueryBuilder query, final Integer page, final Integer size,
      final SortOptionEnum sort, final OrderOptionEnum order) throws ClassDirectException {
    LOGGER.debug("Performing query with object {} ", query.toString());

    return executeMastIhsQuery(query, page, size, sort, order);
  }

  @Override
  public final AssetPageResource executeMastIhsQuery(final MastQueryBuilder query, final Integer page,
      final Integer size, final SortOptionEnum sort, final OrderOptionEnum order) throws ClassDirectException {
    final MultiAssetPageResourceDto multiAssetPageResourceDtos;
    Integer pageNumber = page;
    if (pageNumber != null) {
      pageNumber = pageNumber - 1;
    }
    try {
      Call<MultiAssetPageResourceDto> call;
      final PrioritisedAbstractQueryDto dto = query.buildMastIhs();
      if (checkPriorityQueryDtoEmpty(dto) && (page == null && size == null)) {
        throw new TooManyResultsException("Query has too many results.");
      }

      if (sort != null && order != null) {
        if (sort.name().equals(SortOptionEnum.ClassSociety.name())
            || sort.name().equals(SortOptionEnum.DeadWeight.name())) {
          call = assetServiceDelegate.mastIhsQuery(dto, pageNumber, size, sort.getField(), order.name(),
              SORTING_PRIORITY_IHS);
        } else {
          call = assetServiceDelegate.mastIhsQuery(dto, pageNumber, size, sort.getField(), order.name());
        }
      } else {
        call = assetServiceDelegate.mastIhsQueryUnsorted(dto, pageNumber, size);
      }
      multiAssetPageResourceDtos = call.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Exception when executing mast and ihs query.", exception, exception.getMessage());
      throw new ClassDirectException(exception);
    }
    final List<MultiAssetDto> multiAssetDtos = multiAssetPageResourceDtos.getContent();
    final List<AssetHDto> assets = extractAssetHDto(multiAssetDtos, true);

    final AssetPageResource assetPageResource = getDefaultPaginatedAssets();
    assetPageResource.setContent(assets);
    assetPageResource.setPagination(multiAssetPageResourceDtos.getPagination());

    return resetMastPagination(assetPageResource);
  }

  @Override
  public final void filterByUser(final MastQueryBuilder query, final UserProfiles user) throws RecordNotFoundException {
    LOGGER.info("Begin filter by user profile");
    if (user.getClientCode() != null) {
      if (!StringUtils.isEmpty(user.getClientType())) {
        query.ihsPriorityMandatory(
            clientCode(QueryRelationshipType.EQ, Collections.singletonMap(user.getClientType(), user.getClientCode())));
      } else {
        query.ihsPriorityMandatory(clientCode(QueryRelationshipType.EQ, user.getClientCode()));
      }
    } else if (user.getFlagCode() != null && !user.getFlagCode().isEmpty()) {
      final List<String> flagCodes = new ArrayList<>();
      flagCodes.add(user.getFlagCode());
      try {
        flagCodes.addAll(flagsAssociationsService.getAssociatedFlags(user.getFlagCode()));
      } catch (final ClassDirectException exception) {
        LOGGER.error("Error getting associated flags for flag code: " + user.getFlagCode(), exception);
      }

      query.ihsPriorityMandatory(flagStateCode(QueryRelationshipType.IN, flagCodes.toArray(new String[] {})));
    } else if (user.getShipBuilderCode() != null) {
      final String builderImo = user.getShipBuilderCode();
      final String builderName = Optional.ofNullable(user.getCompanyName()).orElse(EMPTY_STRING);
      // LRCD-2620 : New logic to search both MAST Asset (using
      // ShipBuilder Name)
      // and IHS Asset (using ShipBuilder IMO) due to ship builder role is
      // removed
      // from MAST MAST_CDH_PARTY table.
      query.mandatory(builderIdAndBuilderName(builderImo, builderName));
    } else {
      // Block EOR access
      final List<String> rolesList =
          user.getRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList());
      if (SecurityUtils.isEORUser(rolesList)) {
        try {
          eorQuery(user.getUserId(), query);
        } catch (ClassDirectException e) {
          LOGGER.error("Fail to set user eor filter with error: {}", e.getMessage(), e);
          throw new RecordNotFoundException("Cannot find user eor with error: " + e.getMessage());
        }
      }
    }
  }

  /**
   * Constructs the asset query filter for eor or client with eor user.
   *
   * @param query the mast query model.
   * @param user the user requested the asset.
   * @throws ClassDirectException if fail to fetch users eor from database.
   */
  private void filterByUserWithEor(final MastQueryBuilder query, final UserProfiles user) throws ClassDirectException {
    LOGGER.info("Begin filter by user profile with eor: {}", user.getUserId());
    if (user.getClientCode() != null) {
      final List<String> eorImos = user.getEors().stream().map(eor -> eor.getImoNumber()).collect(Collectors.toList());
      // Client with EOR
      query.mandatory(MastQueryBuilder.clientCodeAndEor(eorImos, user.getClientCode()));

    } else {
      eorQuery(user.getUserId(), query);
    }
  }

  /**
   * Apply current user's client/ship/flag codes to the query.
   *
   * @param query The mast query.
   * @throws ClassDirectException ClassDirectException.
   */
  private void filterByCurrentUser(final MastQueryBuilder query) throws ClassDirectException {
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    if (token != null) {
      final UserProfiles user = userProfileService.getUser(token.getPrincipal());
      if (SecurityUtils.isEORUser()) {
        filterByUserWithEor(query, user);
      } else {
        filterByUser(query, user);
      }
    } else {
      LOGGER.error("Filter By User credentials Failed. Token is null.");
    }
  }

  /**
   * Convert 0-index to 1-index pagination.
   *
   * @param assetPageResource asset page resource.
   * @return asset page resource.
   */
  private AssetPageResource resetMastPagination(final AssetPageResource assetPageResource) {
    final PaginationDto theOriginal = assetPageResource.getPagination();
    if (theOriginal != null) {
      theOriginal.setNumber(theOriginal.getNumber() + 1);
      assetPageResource.setPagination(theOriginal);
    }
    return assetPageResource;
  }

  /**
   * Convert from {@link AssetQueryHDto} to {@link MastQueryBuilder}.
   *
   * @param assetQuery original query.
   * @return mast-ihs query dto.
   */
  @SuppressWarnings("checkstyle:methodlength")
  @Override
  public final MastQueryBuilder convert(final AssetQueryHDto assetQuery) {
    MastQueryBuilder queryBuilder = MastQueryBuilder.create();

    final List<Long> codicilTypeIds = new ArrayList<>();
    final List<Long> statusIds = new ArrayList<>();
    List<Long> mmsServiceCatalogueIds = new ArrayList<>(0);

    if (assetQuery.getAssetTypeId() != null && !assetQuery.getAssetTypeId().isEmpty()) {
      queryBuilder = queryBuilder.ihsPriorityMandatory(
          assetTypeId(QueryRelationshipType.IN, assetQuery.getAssetTypeId().toArray(new Long[] {})));
    }
    if (assetQuery.getFlagStateCodes() != null && !assetQuery.getFlagStateCodes().isEmpty()) {
      queryBuilder.ihsPriorityMandatory(
          flagStateCode(QueryRelationshipType.IN, assetQuery.getFlagStateCodes().toArray(new String[] {})));
    }
    if (assetQuery.getGrossTonnageMin() != null || assetQuery.getGrossTonnageMax() != null) {
      queryBuilder =
          queryBuilder.mandatory(grossTonnage(assetQuery.getGrossTonnageMin(), assetQuery.getGrossTonnageMax()));
    }
    if (assetQuery.getClassStatusId() != null && !assetQuery.getClassStatusId().isEmpty()) {
      queryBuilder = queryBuilder
          .mandatory(classStatusId(QueryRelationshipType.IN, assetQuery.getClassStatusId().toArray(new Long[] {})));
    }
    if (assetQuery.getBuildDateMin() != null || assetQuery.getBuildDateMax() != null) {
      final Date buildDateMin;
      if (assetQuery.getBuildDateMin() != null) {
        buildDateMin = assetQuery.getBuildDateMin();
      } else {
        buildDateMin = new Date(0);
      }
      final Date buildDateMax;
      if (assetQuery.getBuildDateMax() != null) {
        buildDateMax = assetQuery.getBuildDateMax();
      } else {
        buildDateMax = new Date();
      }
      queryBuilder = queryBuilder.mandatory(buildDate(buildDateMin, buildDateMax));
    }
    if (assetQuery.getIdList() != null && !assetQuery.getIdList().isEmpty()) {
      queryBuilder =
          queryBuilder.mandatory(assetId(QueryRelationshipType.IN, assetQuery.getIdList().toArray(new Long[] {})));
    }
    if (assetQuery.getClientIds() != null && !assetQuery.getClientIds().isEmpty()) {
      List<String> clientCodes = new ArrayList<String>(assetQuery.getClientIds().size());
      for (Long clientId : assetQuery.getClientIds()) {
        clientCodes.add(String.valueOf(clientId));
      }
      queryBuilder = queryBuilder.ihsPriorityMandatory(
          MastQueryBuilder.clientCode(QueryRelationshipType.IN, clientCodes.toArray(new String[] {})));
    }
    if (assetQuery.getClientName() != null && !assetQuery.getClientName().isEmpty()) {
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      if (token != null && token.getDetails() != null) {
        final UserProfiles user = token.getDetails();
        if (user != null && user.getClientType() != null) {
          queryBuilder.ihsPriorityMandatory(
              clientName(QueryRelationshipType.LIKE, user.getClientType(), assetQuery.getClientName()));
        } else {
          queryBuilder.ihsPriorityMandatory(clientName(QueryRelationshipType.LIKE, assetQuery.getClientName()));
        }
      }
    }
    if (assetQuery.getYardNumber() != null && !assetQuery.getYardNumber().isEmpty()) {
      queryBuilder =
          queryBuilder.mandatory(yardNumber(QueryRelationshipType.LIKE, assetQuery.getYardNumber().replace("*", "%")));
    }
    if (assetQuery.getAssetTypeCodes() != null && !assetQuery.getAssetTypeCodes().isEmpty()) {
      queryBuilder = queryBuilder
          .mandatory(assetTypeCode(QueryRelationshipType.IN, assetQuery.getAssetTypeCodes().toArray(new String[] {})));
    }
    if (assetQuery.getSearch() != null && !assetQuery.getSearch().isEmpty()) {
      String actualSearchValue = assetQuery.getSearch().substring(1, assetQuery.getSearch().length() - 1);
      if (ServiceUtils.isImoNumber(actualSearchValue)) {
        Long imoNumber = Long.parseLong(actualSearchValue);
        queryBuilder.ihsPriorityMandatory(ihsAssetId(QueryRelationshipType.IN, imoNumber));
      } else {
        if (assetQuery.getIncludeFormerAssetNames() != null && assetQuery.getIncludeFormerAssetNames()) {
          queryBuilder = queryBuilder.mandatory(assetSearchWithFormerAssetName(assetQuery.getSearch()));
        } else {
          queryBuilder = queryBuilder.mandatory(assetWildcardSearch(assetQuery.getSearch()));
        }
      }
    }
    if (assetQuery.getLifecycleStatusId() != null && !assetQuery.getLifecycleStatusId().isEmpty()) {
      queryBuilder = queryBuilder.mandatory(
          assetLifecycleStatusId(QueryRelationshipType.IN, assetQuery.getLifecycleStatusId().toArray(new Long[] {})));
    }
    if (assetQuery.getImoNumber() != null && !assetQuery.getImoNumber().isEmpty()) {
      queryBuilder = queryBuilder
          .mandatory(assetImoNumber(QueryRelationshipType.IN, assetQuery.getImoNumber().toArray(new String[] {})));
    }
    if (assetQuery.getEffectiveDateMin() != null && assetQuery.getEffectiveDateMax() != null) {
      queryBuilder = queryBuilder
          .mandatory(assetEffectiveDate(assetQuery.getEffectiveDateMin(), assetQuery.getEffectiveDateMax()));
    }
    if (assetQuery.getClassDepartmentId() != null && !assetQuery.getClassDepartmentId().isEmpty()) {
      queryBuilder = queryBuilder.mandatory(
          classDepartmentId(QueryRelationshipType.IN, assetQuery.getClassDepartmentId().toArray(new Long[] {})));
    }
    if (assetQuery.getLinkedAssetId() != null && !assetQuery.getLinkedAssetId().isEmpty()) {
      queryBuilder = queryBuilder
          .mandatory(linkedAssetId(QueryRelationshipType.IN, assetQuery.getLinkedAssetId().toArray(new Long[] {})));
    }
    if (assetQuery.getDeadWeightMin() != null && assetQuery.getDeadWeightMax() != null) {
      queryBuilder = queryBuilder.mandatory(deadWeight(assetQuery.getDeadWeightMin(), assetQuery.getDeadWeightMax()));
    }
    if (assetQuery.getBuilder() != null && !assetQuery.getBuilder().isEmpty()) {
      queryBuilder =
          queryBuilder.mandatory(builderName(QueryRelationshipType.LIKE, "%" + assetQuery.getBuilder() + "%"));
    }
    if (!StringUtils.isEmpty(assetQuery.getFormerAssetName())) {
      queryBuilder = queryBuilder.mandatory(MastQueryBuilder.formerAssetName(assetQuery.getFormerAssetName()));
    }
    if (!Optional.ofNullable(assetQuery.getIncludeInactiveAssets()).orElse(false)) {
      queryBuilder = queryBuilder.ihsPriorityMandatory(MastQueryBuilder.excludeInActiveAsset());
    }

    if (!CollectionUtils.isEmpty(assetQuery.getCodicilTypeId())) {
      // LRCD-3616 include only active codicil and actionable item types.
      assetQuery.getCodicilTypeId().stream().forEach(e -> {
        if (e.equals(CodicilType.AI.getName())) {
          codicilTypeIds.add(CodicilType.AI.getId());
          statusIds.addAll(actionableItemStatusList);
        } else if (e.equals(CodicilType.COC.getName())) {
          codicilTypeIds.add(CodicilType.COC.getId());
          statusIds.addAll(cocStatusList);
        }
      });
    }
    if (Boolean.TRUE.equals(assetQuery.getIsMMSService())) {
      final List<MmsServiceCatalogueHDto> mmsServiceCatalogue = scheduleService.getMmsServiceCatalogues();
      if (mmsServiceCatalogue != null) {
        mmsServiceCatalogueIds =
            mmsServiceCatalogue.stream().map(MmsServiceCatalogueHDto::getId).collect(Collectors.toList());
      }
    }
    /**
     * MMS and Codiciltype filter will be set in different layer of mast query for FleetList and
     * SurveySchedule. HasActiveServices equals to true indicates the mast query build for
     * SurveySchedule.
     */
    if (Boolean.TRUE.equals(assetQuery.getHasActiveServices())) {
      // mast query for SurveySchedule
      queryBuilder = queryBuilder
          .mandatory(MastQueryBuilder.buildServiceCodicilQuery(assetQuery, mmsServiceCatalogueIds, codicilTypeIds,
          statusIds, confidentialityTypeIds));
    } else {
      // mast query for Fleetlist
      if (Boolean.TRUE.equals(assetQuery.getIsMMSService())) {
        queryBuilder = queryBuilder.mandatory(MastQueryBuilder.serviceWithMMSService(mmsServiceCatalogueIds));
      }
      if (CollectionUtils.isNotEmpty(assetQuery.getCodicilTypeId())) {
        queryBuilder = queryBuilder.mandatory(MastQueryBuilder.codicilsQuery(QueryRelationshipType.IN, statusIds,
            confidentialityTypeIds,
            codicilTypeIds.toArray(new Long[codicilTypeIds.size()])));
      }
    }

    return queryBuilder;
  }

  @Override
  public final AssetHDto hydrateAssetData(final AssetHDto asset) throws ClassDirectException {
    LOGGER.trace("Hydrate Asset data: {}", asset.getCode());
    // hydrate asset
    Resources.inject(asset);

    if (asset.getCustomers() != null && !asset.getCustomers().isEmpty()) {
      List<CustomerLinkHDto> hydartedList = asset.getCustomers().stream().map(customer -> {
        CustomerLinkHDto customerH = new CustomerLinkHDto();
        customerH.setParty(customer);
        PartyRoleHDto role = Optional.ofNullable(customer.getRelationship()).map(LinkResource::getId).map(id -> {
          return assetReferenceService.getPartyRole(id);
        }).orElse(null);
        customerH.setPartyRoles(role);
        return customerH;
      }).collect(Collectors.toList());
      asset.setCustomersH(hydartedList);
      // To avoid redundant data
      asset.setCustomers(null);
    }
    return asset;
  }

  @Override
  public final void onApplicationEvent(final ContextRefreshedEvent event) {
    self = event.getApplicationContext().getBean(AssetService.class);
  }

  @Override
  public final IhsAssetClientDto getCustomersByImoNumber(final String imoNumber) throws ClassDirectException {
    final IhsAssetClientDto ihsClientsForAsset = new IhsAssetClientDto();
    final List<IhsCustomersDto> clients = new ArrayList<>();
    final Call<IhsAssetDetailsDto> call = ihsServiceDelegate.getAssetDetailsByImoNumber(imoNumber);
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    try {
      final IhsAssetDetailsDto ihsAsset = call.execute().body();
      if (ihsAsset != null && ihsAsset.getIhsAsset() != null) {
        ihsClientsForAsset.setIhsAssetDetailsDto(ihsAsset);
        final IhsAssetDto ihsAssetObj = ihsAsset.getIhsAsset();

        // get company details using doc code.
        final Callable<Void> companyResponse = () -> {
          if (ihsAssetObj.getDocCompany() != null && ihsAssetObj.getDocCode() != null) {
            Optional.ofNullable(getCompanyInformation(ihsAssetObj.getDocCode(), CustomerRolesEnum.DOC_COMPANY))
                .ifPresent(companyDto -> {
                  clients.add(companyDto);
                });
          }
          return null;
        };
        // get company details using owner code.
        final Callable<Void> ownerResponse = () -> {
          if (ihsAssetObj.getOwner() != null && ihsAssetObj.getOwnerCode() != null) {
            Optional.ofNullable(getCompanyInformation(ihsAssetObj.getOwnerCode(), CustomerRolesEnum.REGISTERED_OWNER))
                .ifPresent(registeredDto -> {
                  clients.add(registeredDto);
                });
          }
          return null;
        };
        // get company details using gbo code.
        final Callable<Void> regOwnerResponse = () -> {
          if (ihsAssetObj.getGbo() != null && ihsAssetObj.getGboCode() != null) {
            Optional
                .ofNullable(getCompanyInformation(ihsAssetObj.getGboCode(), CustomerRolesEnum.GROUP_BENEFICIAL_OWNER))
                .ifPresent(groupOwner -> {
                  clients.add(groupOwner);
                });
          }
          return null;
        };
        // get company details using operator code.
        final Callable<Void> shipOperResponse = () -> {
          if (ihsAssetObj.getOperator() != null && ihsAssetObj.getOperatorCode() != null) {
            Optional.ofNullable(getCompanyInformation(ihsAssetObj.getOperatorCode(), CustomerRolesEnum.SHIP_OPERATOR))
                .ifPresent(operatorDto -> {
                  clients.add(operatorDto);
                });
          }
          return null;
        };
        // get company details using ship manager code.
        final Callable<Void> shipMangResponse = () -> {
          if (ihsAssetObj.getShipManager() != null && ihsAssetObj.getShipManagerCode() != null) {
            Optional.ofNullable(getCompanyInformation(ihsAssetObj.getShipManagerCode(), CustomerRolesEnum.SHIP_MANAGER))
                .ifPresent(shipManagerDto -> {
                  clients.add(shipManagerDto);
                });
          }
          return null;
        };
        // get company details using tech manager code.
        final Callable<Void> technicalMangResponse = () -> {
          if (ihsAssetObj.getTechManager() != null && ihsAssetObj.getTechManagerCode() != null) {
            Optional
                .ofNullable(
                    getCompanyInformation(ihsAssetObj.getTechManagerCode(), CustomerRolesEnum.TECHNICAL_MANAGER))
                .ifPresent(technicalManagerDto -> {
                  clients.add(technicalManagerDto);
                });
          }
          return null;
        };
        final List<Callable<Void>> callables = Arrays.asList(companyResponse, ownerResponse, regOwnerResponse,
            shipOperResponse, shipMangResponse, technicalMangResponse);
        try {
          executor.invokeAll(callables);
        } catch (final InterruptedException exeception) {
          LOGGER.error("Error while executing executors in getCustomersByImoNumber: " + exeception);
        }
        ihsClientsForAsset.setCustomers(clients);
      }
    } catch (final IOException ioException) {
      LOGGER.error("Error while executing getCustomersByImoNumber" + ioException);
    } finally {
      executor.shutdown();
    }
    return ihsClientsForAsset;
  }

  /**
   * To Create query to get Asset by assetCode.
   *
   * @param assetCode assetCode.
   * @return MastQueryBuilder.
   * @throws ClassDirectException exception.
   */
  private MastQueryBuilder createQueryAssetByCode(final String assetCode) throws ClassDirectException {
    final MastQueryBuilder query;
    if (assetCode.startsWith(CDConstants.MAST_PREFIX)) {
      query = MastQueryBuilder.create().mandatory(assetId(QueryRelationshipType.EQ, AssetCodeUtil.getId(assetCode)));
    } else if (assetCode.startsWith(CDConstants.IHS_PREFIX)) {
      query = MastQueryBuilder.create().mandatory(ihsAssetId(QueryRelationshipType.EQ, AssetCodeUtil.getId(assetCode)));
    } else {
      throw new ClassDirectException(HttpStatus.BAD_REQUEST.value(), "Unsupported asset code.");
    }
    return query;
  }

  /**
   * Filter asset either in mast or Ihs.
   *
   * @param assets assets.
   * @param assetCode assetCode.
   * @return AssetHDto.
   * @throws ClassDirectException exception.
   */
  private AssetHDto getAssetByAssetCode(final List<AssetHDto> assets, final String assetCode)
      throws ClassDirectException {
    final Long id = AssetCodeUtil.getId(assetCode);
    if (assets.isEmpty()) {
      throw new RecordNotFoundException("Record with code " + assetCode + " not found.");
    }
    return assets.stream().filter(asset -> {
      boolean found =
          asset.getId() != null && asset.getId().equals(id) && assetCode.startsWith(CDConstants.MAST_PREFIX);
      LOGGER.info("is asset found {} with asset id {}", found, asset.getId());
      if (assetCode.startsWith(CDConstants.IHS_PREFIX) && null != asset.getIhsAssetDto()) {
        found = asset.getIhsAssetDto().getIhsAsset().getId().equals(String.valueOf(id));
      }
      return found;
    }).findFirst().orElseThrow(() -> new RecordNotFoundException("Record with code " + assetCode + " not found."));
  }

  @Override
  public final AssetHDto eorAssetByCode(final String userId, final String eorAssetCode) throws ClassDirectException {
    boolean eorFlag = false;
    AssetHDto asset = null;
    final Set<String> eorImos = userProfileService.getEorsByUserId(userId);
    if (!eorImos.isEmpty()) {
      final MastQueryBuilder query = createQueryAssetByCode(eorAssetCode);
      final List<AssetHDto> assets = mastIhsQuery(query, null, null);
      asset = getAssetByAssetCode(assets, eorAssetCode);
      calculateOverallStatus(assets);
      asset.setCfoOfficeH(getCfoOfficeDetails(asset));
      asset.setIsEOR(true);
      final String imo = AssetCodeUtil.getAssetImo(asset);
      eorFlag = eorImos.contains(imo);
    }
    if (!eorFlag) {
      throw new RecordNotFoundException("Record with code " + eorAssetCode + " is not an EOR asset.");
    }
    return asset;
  }

  @Override
  public final ServiceScheduleDto getServicesWithFutureDueDateSupport(final Long assetId, final ServiceQueryDto query)
      throws ClassDirectException, IOException, CloneNotSupportedException {

    final List<ScheduledServiceHDto> services = getServices(assetId, query);

    List<RepeatOfDto> repeatedService = null;

    if (query.isIncludeFutureDueDates()) {
      repeatedService = FutureDueDateEngine.calculate(services, query.getMaxDueDate());
    }

    final ServiceScheduleDto serviceSchedule = new ServiceScheduleDto();
    serviceSchedule.setServices(sortServices(services));
    serviceSchedule.setRepeatedServices(repeatedService);

    return serviceSchedule;
  }

  /**
   * Sorts services by service name.
   *
   * @param servicesList the services list to be sorted.
   * @return services.
   */
  private List<ScheduledServiceHDto> sortServices(final List<ScheduledServiceHDto> servicesList) {
    servicesList.forEach(serv -> {
      Optional<Integer> occurenceNumber = Optional.ofNullable(serv.getOccurrenceNumber());
      if (occurenceNumber.isPresent()) {
        serv.setServiceCatalogueName(
            String.join(" - ", serv.getServiceCatalogueH().getName(), String.valueOf(serv.getOccurrenceNumber())));
      } else {
        serv.setServiceCatalogueName(serv.getServiceCatalogueH().getName());
      }
    });
    servicesList.sort(
        (s1, s2) -> s1.getServiceCatalogueH().getDisplayOrder().compareTo(s2.getServiceCatalogueH().getDisplayOrder()));
    return servicesList;
  }

  /**
   * Adds user filter to asset query.
   *
   * @param query the asset query dto.
   * @param user the user profile object.
   * @throws ClassDirectException if any error in executing asset query.
   */
  @Override
  public final void filterMastAssetByUser(final AssetQueryHDto query, final UserProfiles user)
      throws ClassDirectException {
    LOGGER.debug("Begin filter by user profile");
    if (UserProfileServiceUtils.isClientUser(user)) {
      // customer query to get client id.
      final PartyQueryHDto clientQuery = new PartyQueryHDto();
      clientQuery.setImoNumber(user.getClientCode());
      clientQuery.setShipBuilder(false);
      try {
        Optional.ofNullable(userProfileService.retrieveClientInformation(null, null, clientQuery))
            .flatMap(partyDto -> partyDto.stream().findFirst()).ifPresent(partyDto -> {
              query.setPartyId(Collections.singletonList(partyDto.getId()));
              // get party role id's and amend to query
              List<Long> roleIds = new ArrayList<>();
              for (MastPartyRole role : EnumSet.allOf(MastPartyRole.class)) {
                roleIds.add(role.getValue());
              }
              query.setPartyRoleId(roleIds);
            });
      } catch (final ClassDirectException classDirectException) {
        LOGGER.error("Error retrieving client information for imo number :{}", query.getImoNumber(),
            classDirectException);
      }
    } else if (UserProfileServiceUtils.isFlagUser(user)) {
      final List<String> flagCodes = new ArrayList<>();
      flagCodes.add(user.getFlagCode());
      try {
        flagCodes.addAll(flagsAssociationsService.getAssociatedFlags(user.getFlagCode()));
        // stream the list and use the set to filter it

        List<Long> flagIds =
            flagStateService.getFlagStates().stream().filter(flag -> flagCodes.contains(flag.getFlagCode()))
                .map(FlagStateHDto::getId).collect(Collectors.toList());
        query.setFlagStateId(flagIds);
      } catch (final ClassDirectException exception) {
        LOGGER.error("Error getting associated flags for flag code: " + user.getFlagCode(), exception);
      }
    } else if (UserProfileServiceUtils.isBuilderUser(user)) {
      // LRCD-2620 : New logic to search both MAST Asset (using
      // ShipBuilder Name)
      // and IHS Asset (using ShipBuilder IMO) due to ship builder role is
      // removed
      // from MAST MAST_CDH_PARTY table.
      if (user.getCompanyName() != null) {
        query.setBuilder(user.getCompanyName());
      } else {
        LOGGER.error("User's company Name is Null", user.getUserId());
      }
    } else {
      // Block EOR access
      final List<String> rolesList =
          user.getRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList());
      if (SecurityUtils.isEORUser(rolesList)) {
        try {
          final Set<String> eorImos = userProfileService.getEorsByUserId(user.getUserId());
          query.setImoNumber(new ArrayList<>(eorImos));
        } catch (ClassDirectException e) {
          LOGGER.error("Fail to set user eor filter with error: {}", e.getMessage(), e);
          throw new RecordNotFoundException("Cannot find user eor with error: " + e.getMessage());
        }
      }
    }
  }

  @Override
  public final String updateFavouriteList(final String userId, final FavouritesDto favourites)
      throws ClassDirectException {
    if (!StringUtils.isEmpty(userId) && favourites != null) {
      try {
        favouritesRepository.updateUserFavouriteList(userId, favourites);
        return successfulUpdate;
      } catch (final DataIntegrityViolationException exception) {
        LOGGER.error("Error while create/delete favourites", exception);
      }
    } else {
      throw new ClassDirectException("Invalid userId: " + userId + " or assetCodes to create or remove favorites");
    }
    return null;
  }

  @Override
  public final List<AssetHDto> getAssetsByPagination(final String userId, final AssetQueryHDto assetQuery,
      final MastQueryBuilder queryBuilder) throws ClassDirectException {

    List<AssetHDto> result = null;
    Integer initialPage = 1;
    try {

      AssetPageResource assetPageResource = null;

      if (queryBuilder == null) {
        assetPageResource = getAssets(userId, assetQuery, initialPage, maxPageSize);
      } else {
        assetPageResource = getAssetByQueryBuilder(userId, assetQuery, queryBuilder, initialPage, maxPageSize);
      }

      result = assetPageResource.getContent();

      Double totalPage = assetPageResource.getPagination().getTotalElements() / maxPageSize.doubleValue();
      if (totalPage % 1 != 0) {
        totalPage = totalPage + 1;
      }
      final Integer totalPages = totalPage.intValue();
      LOGGER.debug("total pages {} and page size {}", totalPages, maxPageSize);
      final ExecutorService executorService = ExecutorUtils.newFixedThreadPool("AssetServiceMastAndIhsQuery");
      final List<Callable<List<AssetHDto>>> runnables = new ArrayList<>();

      if (totalPages > 1) {
        for (int page = 1; page < totalPages; page++) {
          // this starts from second call so current page is page+1.
          final Integer currentPage = page + 1;
          runnables.add(() -> {
            try {
              AssetPageResource pageResource = null;

              if (queryBuilder == null) {
                pageResource = getAssets(userId, assetQuery, currentPage, maxPageSize);
              } else {
                pageResource = getAssetByQueryBuilder(userId, assetQuery, queryBuilder, currentPage, maxPageSize);
              }

              List<AssetHDto> assetsList = new ArrayList<>();
              Optional.ofNullable(pageResource).map(AssetPageResource::getContent).ifPresent(content -> {
                assetsList.addAll(content);
              });
              return assetsList;
            } catch (final IOException exception) {
              LOGGER.error("Error in getting assets using mast-and-ihs-uery for user {}", userId, exception);
              throw new ClassDirectException(exception.getMessage());
            }
          });
        }

        try {
          final List<Future<List<AssetHDto>>> futures = executorService.invokeAll(runnables);
          for (final Future<List<AssetHDto>> future : futures) {
            result.addAll(future.get());
          }
        } catch (InterruptedException | ExecutionException exception) {
          LOGGER.error("Unable to invoke target.");
          throw new ClassDirectException("Execution of mast-and-ihs-uery failed.");
        }
      }

    } catch (final IOException exception) {
      LOGGER.error("Error in executing mast-and-ihs-query : getAssetsByPagination for user {}", userId,
          exception.getMessage());
      throw new ClassDirectException(exception);
    }
    return result;
  }

}

