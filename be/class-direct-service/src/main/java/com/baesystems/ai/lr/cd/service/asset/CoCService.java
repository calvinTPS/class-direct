package com.baesystems.ai.lr.cd.service.asset;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCServiceRangeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.processor.DueStatusCalculator;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Provides the interface for condition of class service.
 *
 * @author yng
 *
 */
public interface CoCService extends DueStatusCalculator<CoCHDto> {
  /**
   * Returns the paginated list of condition of class.
   *
   * @param page the page number.
   * @param size the max amount of data to be display per page.
   * @param assetId the asset Id.
   * @return the paginated list of condition of class.
   * @throws ClassDirectException if API call fail.
   */
  PageResource<CoCHDto> getCoC(final Integer page, final Integer size, final Long assetId) throws ClassDirectException;

  /**
   * Returns the condition of class for export scenario where it is optimize to fetch only condition of class and
   * defect information.
   * @param assetId the asset identifier
   * @return the condition of class detail and associated defect detail (if any).
   * @throws ClassDirectException if external API to fetch condition of class or defect fail.
   */
  List<CoCHDto> getCoCForExport(final Long assetId) throws ClassDirectException;

  /**
   * Returns the condition of class range due date based on below conditions.
   *
   * <p> Calculate minimum due date with Coc, AI, SF and Sevices no longer than 30 months previous to todays date,
   * Max due date always minDueDate+7 years.</p>
   *
   * <ul>
   * <li> For coc gets cocs for an asset with filter open status then filter by overdue status and fetches due date
   * minimum no longer than 30 months previous to todays date. </li>
   * <li> For ai gets ais for an asset with filter open, change recommended and continous satisfactory  status then
   * filter by overdue status and fetches due date minimum no longer than 30 months previous to todays date. </li>
   * <li> For sf gets sfs for an asset with filter open status then filter by overdue status and fetches due date
   * minimum no longer than 30 months previous to todays date. </li>
   * <li> For services gets services for an asset with filter active status then filter by overdue status and fetches
   *  due date minimum no longer than 30 months previous to todays date. </li>
   * </ul>
   *
   * <p> finally fetches min due date by minimum value from all coc, ai, sf and services. <p>
   * @param assetId the asset id.
   * @return the map contains range due value.
   * @throws IOException if API call fail.
   */
  Map<String, Long> getCoCServiceRange(final long assetId) throws IOException;

  /**
   * Returns single condition of class by providing asset Id, and condition of class Id.
   *
   * @param assetId the asset id.
   * @param cocId the condition of class id.
   * @return the condition of class.
   * @throws ClassDirectException if API call fail.
   */
  CoCHDto getCoCByAssetIdCoCId(final Long assetId, final long cocId) throws ClassDirectException;

  /**
   * Returns list of condition of class for an asset.
   *
   * @param assetId the asset Id.
   * @return the list of condition of class.
   *
   * @throws ClassDirectException if mast API call fail.
   */
  List<CoCHDto> getCoC(final long assetId)  throws ClassDirectException;

  /**
   * Returns condition of class of an asset by query with due date max(1year + 7 days) and min(past 7 days) with
   * codicil status list with 14(Open) status For asset due sttaus notifications.
   *
   * @param assetId the asset model object.
   * @param query the input query parameter to fileter condition of class.
   * @return the list of condition of class.
   * @throws ClassDirectException if mast api call fail.
   */
  List<CoCHDto> getCocsByQuery(final long assetId, final CodicilDefectQueryHDto query)
      throws ClassDirectException;

  /**
   * Returns the condition of class, actionable item, statutory findings, scheduled services,
   * repeated services together with range due date based on below conditions.
   *
   * <p>
   * Calculate minimum due date with Coc, AI, SF and Sevices no longer than 30 months previous to
   * todays date, Max due date always minDueDate+7 years.
   * </p>
   *
   * <ul>
   * <li>For coc gets cocs for an asset with filter open status then filter by overdue status and
   * fetches due date minimum no longer than 30 months previous to todays date.</li>
   * <li>For ai gets ais for an asset with filter open, change recommended and continous
   * satisfactory status then filter by overdue status and fetches due date minimum no longer than
   * 30 months previous to todays date.</li>
   * <li>For sf gets sfs for an asset with filter open status then filter by overdue status and
   * fetches due date minimum no longer than 30 months previous to todays date.</li>
   * <li>For services gets services for an asset with filter active status then filter by overdue
   * status and fetches due date minimum no longer than 30 months previous to todays date.</li>
   * </ul>
   *
   * <p>
   * finally fetches min due date by minimum value from all coc, ai, sf and services.
   * <p>
   *
   * @param assetId the asset id.
   * @param query service query dto.
   * @return the map contains range due value.
   * @throws IOException if API call fail.
   * @throws CloneNotSupportedException if service object is unable to be cloned.
   * @throws ClassDirectException if there is error mast API to get statutory findings.
   */
  CoCServiceRangeHDto getCoCServiceRangeWithDetails(final long assetId, final ServiceQueryDto query)
      throws IOException, CloneNotSupportedException, ClassDirectException;
}
