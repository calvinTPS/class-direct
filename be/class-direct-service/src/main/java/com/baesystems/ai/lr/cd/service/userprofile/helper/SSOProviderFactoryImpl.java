package com.baesystems.ai.lr.cd.service.userprofile.helper;

import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fwijaya on 15/5/2017.
 */
@Component
public class SSOProviderFactoryImpl implements SSOProviderFactory {

  /**
   * Provider to call internal AD.
   */
  @Autowired
  @Qualifier("internalSSOProvider")
  private SSOUserProvider internalSSOProvider;
  /**
   * Provider to call external AD.
   */
  @Autowired
  @Qualifier("externalSSOProvider")
  private SSOUserProvider externalSSOProvider;

  /**
   * Return SSO user providers.
   *
   * @param userProfiles List of user profiles.
   * @return {@link SSOUserProvider}.
   */
  @Override
  public final List<SSOUserProvider> getProviders(final List<UserProfiles> userProfiles) {
    final List<SSOUserProvider> providers = new ArrayList<>();
    if (hasEmail(userProfiles, internalSSOProvider)) {
      providers.add(internalSSOProvider);
    }
    if (hasEmail(userProfiles, externalSSOProvider)) {
      providers.add(externalSSOProvider);
    }
    return providers;
  }

  /**
   * Return SSO user provider.
   *
   * @param email user email.
   * @return SSO user provider.
   */
  @Override
  public final SSOUserProvider getProvider(final String email) {
    if (internalSSOProvider.isMatch(email)) {
      return internalSSOProvider;
    } else if (externalSSOProvider.isMatch(email)) {
      return externalSSOProvider;
    } else {
      return null;
    }
  }

  /**
   * Checks whether user profiles contain internal user.
   *
   * @param userProfiles list of user profiles..
   * @param provider provider.
   * @return true if the list contain internal user.
   */
  private boolean hasEmail(final List<UserProfiles> userProfiles, final SSOUserProvider provider) {
    for (UserProfiles userProfile : userProfiles) {
      if (provider.isMatch(userProfile.getEmail())) {
        return true;
      }
    }
    return false;
  }
}
