package com.baesystems.ai.lr.cd.service.cache;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.cd.service.ResourceProviderScanner;
import com.baesystems.ai.lr.cd.service.asset.reference.ReferenceDataService;

/**
 * Cache scheduler.
 *
 * @author Faizal Sidek.
 */
@Component
public class CacheScheduler implements ApplicationListener<ContextRefreshedEvent>, ApplicationContextAware {

  /**
   * Object logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CacheScheduler.class);

  /**
   * Injected provider.
   */
  @Autowired
  private ResourceProviderScanner provider;

  /**
   * Injected context.
   */
  private ApplicationContext context;

  /**
   * Injected cache.
   */
  @Autowired
  private CacheContainer cache;

  /**
   * Injected reference data service.
   */
  @Autowired
  private ReferenceDataService referenceDataService;

  @Override
  public final void onApplicationEvent(final ContextRefreshedEvent event) {
    initializeCache();
  }

  /**
   * Check version periodically for every 5 minutes.
   */
  @Scheduled(cron = "0 */5 * * * *")
  public void checkVersion() {
    LOGGER.debug("Checking reference data version.");
    final Long version = referenceDataService.getCurrentVersion();

    if (version > cache.getVersion() || cache.isEmpty()) {
      LOGGER.debug("Invalidate current cache and reinitializing.");

      cache.invalidate();
      initializeCache();
    } else {
      LOGGER.debug("Version is up to date.");
    }
  }

  /**
   * Initialize caches with version.
   */
  private void initializeCache() {
    LOGGER.debug("Found {} generic providers.", provider.getGenericProviders().size());

    provider.getGenericProviders().stream().forEach(provider -> {
      final Method method = provider.getMethod();
      final Class<?> type = provider.getType();
      final String bean = provider.getBeanName();

      if (Collection.class.isAssignableFrom(method.getReturnType())) {
        try {
          LOGGER.debug("Initializing cache for type {} ", type.getSimpleName());
          executeMethod(bean, method, new Object[] {});
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
            | NoSuchMethodException exception) {
          LOGGER.error("Error executing method.", exception);
        }
      }
    });
    cache.setVersion(referenceDataService.getCurrentVersion());

    LOGGER.debug("Cache set with version {} ", cache.getVersion());
  }

  /**
   * Invoke method with given parameter.
   *
   * @param beanRef spring bean reference.
   * @param invoker method invoker.
   * @param params parameters to be passed.
   * @return method result.
   * @throws IllegalAccessException when access to method is denied.
   * @throws IllegalArgumentException when method does not expect specified parameters.
   * @throws InvocationTargetException when unable to invoke the method.
   * @throws NoSuchMethodException when method not found.
   */
  private Object executeMethod(final String beanRef, final Method invoker, final Object[] params)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
    final Object service = context.getBean(beanRef);
    final Method realInvoker = service.getClass().getMethod(invoker.getName(), invoker.getParameterTypes());
    return realInvoker.invoke(service, params);
  }

  @Override
  public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
    this.context = applicationContext;
  }
}
