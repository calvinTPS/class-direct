package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xml.sax.SAXException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import de.neuland.jade4j.JadeConfiguration;
import de.neuland.jade4j.exceptions.JadeException;
import de.neuland.jade4j.template.ClasspathTemplateLoader;
import de.neuland.jade4j.template.JadeTemplate;
import de.neuland.jade4j.template.TemplateLoader;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Utility for pdf printing.
 *
 * @author yng
 *
 */
public final class PdfUtils {

  /**
   * The logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(PdfUtils.class);

  /**
   * Constant for Alphanumeric regex.
   */
  private static final String ALPHA_NUMERIC_REGEX = "[^a-zA-Z0-9]";

  /**
   * The file name for empty water mark pdf.
   */
  private static final String WATER_MARK_PDF = "watermark_";

  /**
   * The portrait page width used in determining created pdf is portrait or landscape to overlay
   * image in backround.
   */
  private static final float PORTRAIT_PDF_WIDTH = 595.28f;
  /**
   * The float zero width used in comparing created pdf width with portrait pdf width.
   */
  private static final float FLOAT_ZERO = 0.00000001f;
  /**
   * The file name for portrait water mark image.
   */
  private static final String WATER_MARK_PORTRAIT = "watermark_portrait.png";
  /**
   * The file name for land scape water mark image.
   */
  private static final String WATER_MARK_LANDSCAPE = "watermark_landscape.png";

  /**
   * Empty constructor.
   */
  private PdfUtils() {

  }

  /**
   * Generate pdf with watermark using image overlay to existing pdf.
   *
   * @param pdfPath path to pdf file.
   * @throws IOException value.
   */
  public static void createWatermark(final String pdfPath) throws IOException {

    final File pdfFile = new File(pdfPath);
    final String watermarkDir = pdfFile.getParent();
    final String pdfFilename = pdfFile.getName();
    // Make the watermark filename unique so it is not overridden during multi asset pdf exports.
    final String filePath = watermarkDir + File.separator + WATER_MARK_PDF + pdfFilename;
    final File tempFile = new File(filePath);
    FileUtils.moveFile(new File(pdfPath), tempFile);
    PdfReader reader = new PdfReader(filePath);
    PdfStamper stamp = null;

    try {
      stamp = new PdfStamper(reader, new FileOutputStream(pdfPath));

      PdfContentByte contentByte;
      final float width = reader.getPageSize(1).getWidth();

      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      InputStream imageStream = null;

      if (Math.abs(width - PORTRAIT_PDF_WIDTH) < FLOAT_ZERO) {

        imageStream = classLoader.getResourceAsStream(WATER_MARK_PORTRAIT);

      } else {
        imageStream = classLoader.getResourceAsStream(WATER_MARK_LANDSCAPE);

      }
      byte[] imageBytes = IOUtils.toByteArray(imageStream);

      Image image = Image.getInstance(imageBytes);

      image.setAbsolutePosition(0, 0);
      for (int i = 1; i <= reader.getNumberOfPages(); i++) {
        contentByte = stamp.getUnderContent(i);
        contentByte.addImage(image);

        // watermark.pdf is the document which is a one page PDF with your watermark image in it.
        // Notice here, you can skip pages from being watermarked.
      }
    } catch (final DocumentException ioException) {
      LOGGER.error("Exception in overlay watermark image to PDF" + ioException.getMessage());
    } finally {
      reader.close();
      if (stamp != null) {
        try {
          stamp.close();
        } catch (DocumentException e) {
          LOGGER.error("Fail to close PDF stamper. | Error Message: {}", e.getMessage());
        }
      }
      FileUtils.deleteQuietly(tempFile);
    }
  }

  /**
   * Flatten string for filename creation.
   *
   * @param text to be replaced.
   * @param replace character to the text.
   * @return replaced text.
   */
  public static String replaceSpecialCharacters(final String text, final String replace) {
    return text.replaceAll(ALPHA_NUMERIC_REGEX, replace);
  }

  /**
   * Replace file path delimiter to linux.
   *
   * @param text to be replaced.
   * @return replaced text.
   */
  public static String convertToLinuxPathDelimiter(final String text) {
    return text.replaceAll("\\\\", "/");
  }


  /**
   * Create pdf from template file.
   *
   * @param model a hash contain value to be populated to template.
   * @param workingDir the directory where the file to be created.
   * @param filename file name to be created.
   * @param template pug template file.
   * @throws JadeException value.
   * @throws IOException value.
   * @throws ParserConfigurationException value.
   * @throws SAXException value.
   * @throws DocumentException value.
   */
  @SuppressFBWarnings(value = "DM_DEFAULT_ENCODING", justification = "NullOutputStream do not have encoding.")
  public static void createPdfFromTemplate(final Map<String, Object> model, final String workingDir,
      final String filename, final String template)
      throws JadeException, IOException, ParserConfigurationException, SAXException, DocumentException {
    // populate data to pug, and convert to html
    final TemplateLoader loader = new ClasspathTemplateLoader();

    final JadeConfiguration configuration = new JadeConfiguration();
    configuration.setTemplateLoader(loader);

    // for dev only, not to cache template
    configuration.setCaching(false);
    configuration.setPrettyPrint(true);

    final JadeTemplate jadeTemplate = configuration.getTemplate(template);
    String body = configuration.renderTemplate(jadeTemplate, model);

    // convert html to xhtml
    try (final InputStream in = new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8));
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ByteArrayOutputStream errOutStream = new ByteArrayOutputStream()) {
      final PrintWriter errOut = new PrintWriter(errOutStream);
      final Tidy htmlCleaner = new Tidy();
      htmlCleaner.setXHTML(true);
      htmlCleaner.setErrout(errOut);
      htmlCleaner.parse(in, out);
      if (out.size() > 0) {
        body = out.toString(StandardCharsets.UTF_8.name());
      } else {
        LOGGER.warn("Fail to tidy html to xhtml. Error Message: {}",
            errOutStream.toString(StandardCharsets.UTF_8.name()));
      }
    }

    // convert xhtml to pdf
    try (OutputStream os = new FileOutputStream(new File(workingDir + File.separator + filename))) {
      final ITextRenderer renderer = new ITextRenderer();
      renderer.getFontResolver().addFont("lte50327.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
      renderer.getFontResolver().addFont("lte50329.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
      renderer.getFontResolver().addFont("lte50331.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
      renderer.setDocumentFromString(body);
      renderer.layout();
      renderer.createPDF(os);
    }
  }

}
