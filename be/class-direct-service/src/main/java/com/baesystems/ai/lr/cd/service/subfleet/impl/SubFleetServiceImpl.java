package com.baesystems.ai.lr.cd.service.subfleet.impl;

import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetId;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BaseDtoPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetDto;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetOptimizedDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum;
import com.baesystems.ai.lr.cd.be.enums.SubfleetMoveOperationEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * @author syalavarthi
 */
@Service(value = "SubFleetService")
public class SubFleetServiceImpl implements SubFleetService, InitializingBean {
  /**
   * Log4j logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(SubFleetServiceImpl.class);
  /**
   * inject subFleetDAO.
   */
  @Autowired
  private SubFleetDAO subFleetDAO;
  /**
   * assetService Object.
   */
  @Autowired
  private AssetService assetService;
  /**
   * userProfileService Object.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Audit Service for user management audit.
   */
  @Autowired
  private AuditService auditService;

  /**
   * The {@link UserProfileDao} from spring context.
   */
  @Autowired
  private UserProfileDao userProfileDao;
  /**
   * The {@link AssetRetrofitService} from spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * Constant integer intial_size.
   */
  private static final int INITIAL_SIZE = 0;

  /**
   * The maximum page size.
   */
  @Value("${subfleet.service.maxpagesize:16}")
  private Integer maximumPageSize;

  @Override
  public final List<Long> getSubFleetById(final String selectedUserId) throws RecordNotFoundException {
    return subFleetDAO.getSubFleetById(selectedUserId);
  }

  @Override
  public final PageResource<AssetHDto> getSubfleet(final String selectedUserId, final Integer page, final Integer size)
      throws RecordNotFoundException, ClassDirectException, IOException {

    LOG.debug("Querying assets with page {} and size {}.", page, size);
    final PageResource<AssetHDto> pageResource = getDefaultPaginatedAssets();

    // check with restrictAll
    final UserProfiles user = userProfileService.getUser(selectedUserId);
    final List<Long> subfleetIds = getSubFleetIds(selectedUserId);
    if (!user.getRestrictAll() && subfleetIds != null && !subfleetIds.isEmpty()) {
      final MastQueryBuilder queryBuilder = MastQueryBuilder.create();
      queryBuilder.mandatory(assetId(QueryRelationshipType.IN, subfleetIds.toArray(new Long[] {})));
      final AssetPageResource assetPageResource =
          assetService.executeMastIhsQuery(queryBuilder, page, size, null, null);
      pageResource.setContent(assetPageResource.getContent());
      pageResource.setPagination(assetPageResource.getPagination());
    }

    return pageResource;
  }


  @Override
  @SuppressFBWarnings
  public final PageResource<AssetHDto> getRestrictedAssets(final String selectedUserId, final Integer page,
      final Integer size) throws RecordNotFoundException, ClassDirectException, IOException {

    LOG.debug("Querying assets with page {} and size {}.", page, size);
    final PageResource<AssetHDto> pageResource = getDefaultPaginatedAssets();

    final List<Long> availableAssetIds = self.getAccessibleAssetIdsForUser(selectedUserId);
    final UserProfiles user = userProfileService.getUser(selectedUserId);

    // Get all available assets
    MastQueryBuilder queryBuilder;
    if (user.getRestrictAll()) {
      queryBuilder = MastQueryBuilder.create()
          .mandatory(assetId(QueryRelationshipType.IN, availableAssetIds.toArray(new Long[] {})));
    } else {
      final List<Long> accessibleAssetIds = self.getSubFleetIds(selectedUserId);
      @SuppressWarnings("unchecked")
      final List<Long> restrictedAssetIds =
          (List<Long>) CollectionUtils.removeAll(availableAssetIds, accessibleAssetIds);
      queryBuilder = MastQueryBuilder.create()
          .mandatory(assetId(QueryRelationshipType.IN, restrictedAssetIds.toArray(new Long[] {})));
    }

    final AssetPageResource assets = assetService.executeMastIhsQuery(queryBuilder, page, size, null, null);
    pageResource.setContent(assets.getContent());
    pageResource.setPagination(assets.getPagination());
    return pageResource;
  }

  @Override
  public final StatusDto saveSubFleet(final String selectedUserId, final SubFleetDto subfleet)
      throws RecordNotFoundException, SQLException, ClassDirectException {
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    String tokenPrincipal = token.getPrincipal();
    final UserProfiles requesterUser = userProfileService.getUser(tokenPrincipal);
    final UserProfiles user = userProfileService.getUser(selectedUserId);
    List<Long> oldIds = getSubFleetById(selectedUserId);
    final StatusDto status =
        subFleetDAO.saveSubFleet(selectedUserId, subfleet.getAccessibleAssetIds(), subfleet.getRestrictedAssetIds());
    auditService.audit(requesterUser, user, oldIds, subfleet.getAccessibleAssetIds(), AuditScenarioEnum.SAVE_SUBFLEET);
    return status;
  }

  @SuppressWarnings("unchecked")
  @Override
  public final StatusDto saveSubFleetOptimized(final String selectedUserId, final SubFleetOptimizedDto subfleet)
      throws RecordNotFoundException, SQLException, ClassDirectException {
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    String tokenPrincipal = token.getPrincipal();
    final UserProfiles requesterUser = userProfileService.getUser(tokenPrincipal);
    final UserProfiles user = userProfileService.getUser(selectedUserId);
    List<Long> oldIds = getSubFleetById(selectedUserId);
    // Get current subfleet list
    final List<Long> allIds = self.getAccessibleAssetIdsForUser(selectedUserId);
    // Use HashSet to protect against duplicates.
    final HashSet<Long> restrictedIds = new HashSet<>(getRestrictedAssetsIds(selectedUserId));
    final HashSet<Long> accessibleIds = new HashSet<>(CollectionUtils.removeAll(allIds, restrictedIds));
    final Optional<List<Long>> includedAssetIds = Optional.ofNullable(subfleet.getIncludedAssetIds());
    final Optional<List<Long>> excludedAssetIds = Optional.ofNullable(subfleet.getExcludedAssetIds());
    final Boolean exactlyOneArrayProvided =
        Arrays.asList(includedAssetIds, excludedAssetIds).stream().filter(list -> list.isPresent()).count() == 1;
    if (!exactlyOneArrayProvided) {
      throw new ClassDirectException("Provide an included or excluded list of asset ids, not both");
    }

    if (subfleet.getMoveTo().equals(SubfleetMoveOperationEnum.ACCESSIBLE.getName())) {
      includedAssetIds.ifPresent(ids -> {
        restrictedIds.removeAll(ids);
        accessibleIds.addAll(ids);
      });

      excludedAssetIds.ifPresent(ids -> {
        if (ids.isEmpty()) {
          // Everything is becoming accessible.
          accessibleIds.clear();
          accessibleIds.addAll(allIds);

          restrictedIds.clear();
        } else {
          accessibleIds.addAll(CollectionUtils.removeAll(restrictedIds, ids));

          restrictedIds.clear();
          restrictedIds.addAll(ids);
        }
      });
    } else if (subfleet.getMoveTo().equals(SubfleetMoveOperationEnum.RESTRICTED.getName())) {
      includedAssetIds.ifPresent(ids -> {
        accessibleIds.removeAll(ids);
        restrictedIds.addAll(ids);
      });

      excludedAssetIds.ifPresent(ids -> {
        // All these excluded assets are becoming accessible
        if (ids.isEmpty()) {
          // Everything is becoming restricted.
          accessibleIds.clear();

          restrictedIds.clear();
          restrictedIds.addAll(allIds);
        } else {
          restrictedIds.addAll(CollectionUtils.removeAll(accessibleIds, ids));

          accessibleIds.clear();
          accessibleIds.addAll(ids);
        }
      });
    } else {
      throw new ClassDirectException("Unsupported move operation");
    }

    final List<Long> accessibleIdsList = accessibleIds.stream().collect(Collectors.toList());
    final List<Long> restrictedIdsList = restrictedIds.stream().collect(Collectors.toList());

    StatusDto status = subFleetDAO.saveSubFleet(selectedUserId, accessibleIdsList, restrictedIdsList);

    if (accessibleIdsList.isEmpty() || restrictedIdsList.isEmpty()) {
      // subfleet will be empty if user didn't move his assets, all the assets are accessible or
      // user move all assets to restricted, in this scenario's auditing user information below.
      final UserProfiles newUser = userProfileDao.getUser(user.getUserId());
      auditService.audit(requesterUser, user, user, newUser, AuditScenarioEnum.SAVE_SUBFLEET_ALL);
    } else {
      auditService.audit(requesterUser, user, oldIds, accessibleIdsList, AuditScenarioEnum.SAVE_SUBFLEET);
    }

    return status;
  }



  /**
   * to set default pagination.
   *
   * @return AssetPageResource pageresource.
   */
  private PageResource<AssetHDto> getDefaultPaginatedAssets() {
    final PageResource<AssetHDto> assets = new AssetPageResource();
    assets.setContent(Collections.emptyList());
    final PaginationDto pagination = new PaginationDto();
    pagination.setSize(INITIAL_SIZE);
    pagination.setNumber(INITIAL_SIZE);
    pagination.setFirst(false);
    pagination.setLast(false);
    pagination.setTotalPages(INITIAL_SIZE);
    pagination.setTotalElements(0L);
    pagination.setNumberOfElements(INITIAL_SIZE);
    assets.setPagination(pagination);
    return assets;
  }

  @Override
  public final List<Long> getSubFleetIds(final String selectedUserId)
      throws RecordNotFoundException, ClassDirectException, IOException {
    final UserProfiles user = userProfileService.getUser(selectedUserId);

    List<Long> availableAssetIds = new ArrayList<>();

    // If lrcd_db is empty, then subfleets = total accessible assets
    if (!user.getRestrictAll()) {
      availableAssetIds = getSubFleetById(selectedUserId);
      if (availableAssetIds.isEmpty()) {
        availableAssetIds = self.getAccessibleAssetIdsForUser(selectedUserId);
      }
    }

    return availableAssetIds;
  }

  @Override
  public final List<Long> getRestrictedAssetsIds(final String selectedUserId)
      throws RecordNotFoundException, ClassDirectException {
    final List<Long> availableAssetIds = self.getAccessibleAssetIdsForUser(selectedUserId);
    final UserProfiles user = userProfileService.getUser(selectedUserId);
    if (!user.getRestrictAll()) {
      try {
        availableAssetIds.removeAll(getSubFleetIds(selectedUserId));
      } catch (final IOException exception) {
        LOG.error("Error getting subfleet id", exception);
      }
    }

    return availableAssetIds;
  }

  @Override
  public final List<Long> getAccessibleAssetIdsForUser(final String selectedUserId) throws ClassDirectException {
    return this.getAccessibleAssetIdsForUserWithoutCaching(selectedUserId);
  }


  @Override
  public final List<Long> getAccessibleAssetIdsForUserWithoutCaching(final String selectedUserId)
      throws ClassDirectException {
    final UserProfiles selectedUser = userProfileService.getUser(selectedUserId);
    final AssetQueryHDto builder = new AssetQueryHDto();
    List<Long> assetIds = Collections.emptyList();
    // adds user filter to query
    assetService.filterMastAssetByUser(builder, selectedUser);

    try {
      final BaseDtoPageResource basePgeResource =
          assetRetrofitService.assetQueryAsBaseDto(builder, 1, 1).execute().body();

      if (basePgeResource != null && basePgeResource.getPagination() != null && basePgeResource.getContent() != null) {
        assetIds = new ArrayList<>();
        Double totalPage = basePgeResource.getPagination().getTotalElements() / maximumPageSize.doubleValue();
        if (totalPage % 1 != 0) {
          totalPage = totalPage + 1;
        }
        Integer totalPages = totalPage.intValue();
        LOG.debug("total pages {} and page size {}", totalPages, maximumPageSize);
        final ExecutorService executorService = ExecutorUtils.newFixedThreadPool("SubfleetServiceMastAndIhsQuery");
        final List<Callable<List<Long>>> runnables = new ArrayList<>();
        for (int page = 1; page < (totalPages + 1); page++) {
          // mast asset/query staring page size 0.
          final Integer currentPage = page - 1;
          runnables.add(() -> {
            try {
              final BaseDtoPageResource assetPageResource =
                  assetRetrofitService.assetQueryAsBaseDto(builder, currentPage, maximumPageSize).execute().body();

              final List<Long> assetIdSet = new ArrayList<>();
              Optional.ofNullable(assetPageResource).map(BaseDtoPageResource::getContent).ifPresent(content -> {
                assetIdSet.addAll(content.stream().map(BaseDto::getId).collect(Collectors.toList()));
              });
              return assetIdSet;

            } catch (final IOException exception) {
              LOG.error("Error in getting assets id's using assets/query for user {}", selectedUserId, exception);
              throw new ClassDirectException(exception.getMessage());
            }
          });

        }
        try {
          final List<Future<List<Long>>> futures = executorService.invokeAll(runnables);
          for (final Future<List<Long>> future : futures) {
            assetIds.addAll(future.get());
          }
        } catch (InterruptedException | ExecutionException exception) {
          LOG.error("Unable to invoke target.");
          throw new ClassDirectException("Execution of asset query failed.");
        }
      }

    } catch (final IOException exception) {
      LOG.error("Error in getting assets id's using assets/query for user {}",
          new Object[] {selectedUserId, exception});
      throw new ClassDirectException(exception.getMessage());
    }

    return assetIds;
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * com.baesystems.ai.lr.cd.service.subfleet.SubFleetService#isSubFleetEnabled(java.lang.String)
   */
  @Override
  public final boolean isSubFleetEnabled(final String selectedUserId) throws ClassDirectException {
    boolean result = false;
    final UserProfiles user = userProfileService.getUser(selectedUserId);

    // If restricted All mean subfleet whitelist is enabled.
    if (user.getRestrictAll()) {
      result = true;
    } else {
      // Need further check if any whitelist defined.
      final long subFleetCount = subFleetDAO.getSubFleetCountByUserId(selectedUserId);
      result = (subFleetCount > 0);
    }
    return result;
  }


  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but with Spring awareness.
   *
   */
  private SubFleetService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(SubFleetService.class);
  }
}
