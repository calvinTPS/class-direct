package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.export.CocExportService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Provides the coc export service for generating, and downloading checklist report.
 *
 * @author yng
 */
@Service(value = "CocExportService")
@Loggable(Loggable.DEBUG)
public class CocExportServiceImpl extends BasePdfExport<ExportPdfDto> implements CocExportService {

  /**
   * The logger instantiated for {@link CocExportServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CocExportServiceImpl.class);

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private CoCService cocService;

  /**
   * The report name prefix for coc.
   */
  private static final String PDF_PREFIX_COC = "CoC";

  /**
   * The pug template file name for coc.
   */
  private static final String TEMPLATE_COC = "coc_listing_export_template.jade";

  /**
   * The directory name in Amazon S3 where the generated report to be uploaded. This variable in
   * only use at {@link BasePdfExport}. <br>
   * For more information see {@link CocExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "codicil_export";

  /**
   * This field is use as variable in pug template that store list of coc.
   */
  private static final String COCS = "cocs";

  /**
   * This field is use as variable in pug template that store asset object.
   */
  private static final String ASSET = "asset";

  /**
   * This field is use to check if the pdf is single page or multi page.
   */
  private static final String ONLY_ONE_PAGE = "onlyOnePage";

  /**
   * The file generation working directory.
   */
  @Value("${codicil.export.temp.dir}")
  private String tempDir;

  @Override
  protected final String generateFile(final Map<String, Object> model, final ExportPdfDto query,
      final UserProfiles user, final String workingDirCOC, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Retrieving coc object...");

      // get asset information
      final AssetHDto asset = assetService.assetByCode(user.getUserId(), query.getCode());
      if (asset == null) {
        throw new ClassDirectException("Asset not found.");
      } else {
        model.put(ASSET, asset);
      }

      // get coc by asset id.
      final PageResource<CoCHDto> cocResource = cocService.getCoC(null, null, asset.getId());
      List<CoCHDto> cocs = cocResource.getContent();

      // filter COCs by ids.
      if (cocs != null && !cocs.isEmpty()) {
        if (query.getItemsToExport() != null && !query.getItemsToExport().isEmpty()) {
          cocs = cocs.stream().filter(item -> query.getItemsToExport().contains(item.getId()))
              .collect(Collectors.toList());
        }

        cocs.sort(ServiceUtils.codicilDefaultComparator());
      }

      model.put(COCS, cocs);

      String filename = generatePdf(PDF_PREFIX_COC, query.getCode(), current, TEMPLATE_COC, workingDirCOC, model);
      int pageCountForCOC = ServiceUtils.getPageCount(workingDirCOC, filename);
      if (pageCountForCOC == 1) {
        model.put(ONLY_ONE_PAGE, "true");
        filename = generatePdf(PDF_PREFIX_COC, query.getCode(), current, TEMPLATE_COC, workingDirCOC, model);
      }
      return filename;
    } catch (JadeException | IOException | ParserConfigurationException | SAXException | DocumentException exception) {
      throw new ClassDirectException(exception);
    }

  }


  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return this.tempDir;
  }
}
