package com.baesystems.ai.lr.cd.service.port.impl;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.PortRegistryService;
import com.baesystems.ai.lr.cd.be.domain.dto.port.PortOfRegistryInputDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.jcabi.aspects.Loggable;

import retrofit2.Call;

/**
 *
 * Provides service to fetch port registry information from external system.
 *
 * @author VKovuru
 *
 */
@Service(value = "PortService")
@Loggable(Loggable.DEBUG)
public class PortServiceImpl implements PortService {
  /**
  * The application logger object.
  */
  private static final Logger LOG = LogManager.getLogger();
  /**
   * The {@link PortRegistryService} from Spring context.
   */
  @Autowired
  private PortRegistryService portRegistryService;

  /**
   * The error message when record not found.
   */
  @Value("${record.not.found}")
  private String recordNotFoundMessage;

  /**
   * Returns a list of ports using the pass in port name, the returned result should be sorted by port name.
   * @param portOfRegistry  the search criteria with the partial port name and limit (optional).
   * @return the list of ports information matched the search criteria.
   * @throws IOException if external API to mast to fetch the port registry fail.
   */
  @Override
  public final List<PortOfRegistryDto> getPorts(final PortOfRegistryInputDto portOfRegistry) throws IOException {
    LOG.debug(
        "Start of method PortServiceImpl.getPorts(final PortOfRegistryInputDto portOfRegistry(search[{}], limit[{}]))",
        portOfRegistry.getSearch(), portOfRegistry.getLimit());

    int limit = Optional.ofNullable(portOfRegistry.getLimit()).map(Integer::intValue).orElse(CDConstants.LIMIT_ZERO);

    // call to Retrofit service
    final Call<List<PortOfRegistryDto>> call = portRegistryService.getPortOfRegistryDtos();
    final List<PortOfRegistryDto> ports = call.execute().body();

    List<PortOfRegistryDto> filteredPorts = null;
    Stream<PortOfRegistryDto> resultStream = ports.stream();

    // if search is empty return all the records with limit.
    if (!StringUtils.isEmpty(portOfRegistry.getSearch())) {
      resultStream = resultStream.filter(port -> StringUtils.startsWithIgnoreCase(port.getName()
          , portOfRegistry.getSearch()));
    }

    if (limit > CDConstants.LIMIT_ZERO) {
      filteredPorts = resultStream.sorted(Comparator.comparing(PortOfRegistryDto::getName)).limit(limit)
          .collect(Collectors.toList());
    } else {
      //No limit filter
      filteredPorts = resultStream.sorted(Comparator.comparing(PortOfRegistryDto::getName))
          .collect(Collectors.toList());
    }

    LOG.trace("End of method PortServiceImpl.getPorts(final PortOfRegistryInputDto portOfRegistry)");
    return filteredPorts;
  }

  /**
   * Returns a single port information that matched the given id.
   * @param portID the port identifier.
   * @return the matched port information with the given id.
   * @throws RecordNotFoundException if no port matched the given port id.
   */
  @Override
  public final PortOfRegistryDto getPort(final long portID) throws RecordNotFoundException {
    LOG.debug("Start of method PortServiceImpl.getPort({})", portID);
    PortOfRegistryDto portDto = null;
    final Call<PortOfRegistryDto> portCall = portRegistryService.getPort(portID);
    try {
      portDto = portCall.execute().body();
    } catch (IOException exception) {
      LOG.error("Error while getting port", exception);
    }

    if (portDto == null) {
      throw new RecordNotFoundException(recordNotFoundMessage);
    }
    return portDto;
  }
}
