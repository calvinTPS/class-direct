package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.UsersExportService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.jcabi.aspects.Loggable;

/**
 * Provides service to fetches encrypted token which contain generated PDF report in server file
 * path location.
 *
 * @author syalavarthi.
 *
 */
@Service(value = "UsersExportService")
@Loggable(Loggable.DEBUG)
public class UsersExportServiceImpl implements UsersExportService {

  /***
   * The logger for UsersExportServiceImpl.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UsersExportServiceImpl.class);

  /**
   * The {@link AmazonStorageService} from spring context.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;
  /**
   * The file generation working directory.
   */
  @Value("${user.export.filepath}")
  private String filePath;
  /**
   * The csv file header in report.
   */
  @Value("${user.export.csv.fileheader}")
  private String fileheader;
  /**
   * The legal artifact text in report.
   */
  @Value("${export.legal.artifact}")
  private String legalArtifact;
  /**
   * The {@value #S3_FILE_SEPARATOR} s3 supported file separator.
   */
  private static final String S3_FILE_SEPARATOR = "/";

  /**
   * The s3 working directory to store generated file.
   */
  @Value("${s3.export.bucket.path}")
  private String s3Bucket;

  @Override
  public final StringResponse downloadFile(final UserProfileDto usersexportDto)
      throws ClassDirectException, IOException {

    // get all users
    final List<UserProfiles> users = userProfileService.getUsers(usersexportDto, null, null, null, null);
    final String originalFilePath =
        filePath + LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyyhhmm'.csv'"));

    File file = new File(originalFilePath);
    if (!file.getParentFile().exists()) {
      if (!file.getParentFile().mkdirs()) {
        throw new ClassDirectException("Error in creating file");
      }
    }
    final String[] fileHeaders = fileheader.split(",");

    InputStream targetStream = null;
    try {
      file = UserProfileServiceUtils.generateCSV(users, file, fileHeaders, legalArtifact);

      targetStream = new FileInputStream(file);
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      final UserProfiles user = token.getDetails();


      final String key = user.getUserId() + S3_FILE_SEPARATOR + originalFilePath;
      final String s3Token =
          amazonStorageService.uploadFile(s3Bucket, key, PdfUtils.convertToLinuxPathDelimiter(originalFilePath),
              user.getUserId());

      return new StringResponse(s3Token, file.getName());

    } finally {
      if (targetStream != null) {
        try {
          targetStream.close();
          FileUtils.forceDelete(file);
        } catch (final Exception exception) {
          LOGGER.info("Fail to delete file - " + file.getName() + " | Error : " + exception.getMessage(), exception);
        }
      }
    }
  }

}
