package com.baesystems.ai.lr.cd.service.audit;

import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AuditScenarioEnum;

/**
 * Provides service for user audit management.
 *
 * @author VKolagutla
 * @author syalavarthi 14-06-2017.
 *
 */
public interface AuditService {

  /**
   * Logs the requester user info, target user info and to check
   * difference in old value and new value with matching scenarios.
   *
   * @param requesterUser the admin user to performs operations.
   * @param targetUser the target user.
   * @param oldValue the old value of target user.
   * @param newValue the new value of target user.
   * @param scenarios the user operations to be performed.
   */
  void audit(final UserProfiles requesterUser, final UserProfiles targetUser, final Object oldValue,
      final Object newValue, final AuditScenarioEnum... scenarios);
}
