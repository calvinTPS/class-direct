package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AttachmentExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.service.export.TMReportDownloadService;

import retrofit2.Call;

/**
 * Provides service to get encrypted token which contain generated PDF report in server file path
 * location.
 *
 * @author syalavarthi.
 *
 */
@Service(value = "TMReportDownloadService")
public class TMReportDownloadServiceImpl implements TMReportDownloadService {

  /**
   * The logger for TMReportDownloadServiceImpl.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(TMReportDownloadServiceImpl.class);
  /**
   * The {@link AttachmentRetrofitService} from spring context.
   */
  @Autowired
  private AttachmentRetrofitService attachmentRetrofitService;

  @Override
  public final StringResponse downloadAttachmentInfo(final AttachmentExportDto query) throws ClassDirectException {
    final long elapsedTime = System.currentTimeMillis();
    final long attachmentId = query.getAttachmentId();
    Integer attachNodeId = null;
    Integer attachVersion = null;
    // get logged in userId.
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    final String userId = token.getDetails().getUserId();
    try {
      LOGGER.info("AttachmentExport started for TMReport : {} ", attachmentId);

      SupplementaryInformationHDto attachment = null;

      final Call<SupplementaryInformationHDto> attachmentCall =
          attachmentRetrofitService.getAttachmentByJobIdattAchmentId(query.getJobId(), query.getAttachmentId());
      try {
        attachment = attachmentCall.execute().body();
      } catch (final IOException exception) {
        LOGGER.error("Exception when getting attachments for job: " + exception);
        throw new ClassDirectException(exception);
      }

      attachNodeId = Optional.ofNullable(attachment.getAttachmentUrl()).map(url -> Integer.valueOf(url))
          .orElseThrow(() -> new ClassDirectException(
              "Attachment NodeId is not available to enrich downloadAttachmentInfo for Attachment [" + attachmentId
                  + "]"));

      attachVersion =
          Optional.ofNullable(attachment.getDocumentVersion()).map(version -> version.intValue()).orElse(null);
      return new StringResponse(FileTokenGenerator.generateCS10Token(attachNodeId, attachVersion, userId));
    } catch (final NumberFormatException | ClassDirectException exception) {
      throw new ClassDirectException("AttachmentDownLoad-exception occured in downloadAttachmentInfo for attachment ["
          + attachmentId + "] : " + exception);
    } finally {
      LOGGER.info("AttachmentDownLoad ended for attachment : {}. | Elapsed time (ms): {}", attachmentId,
          System.currentTimeMillis() - elapsedTime);
    }
  }

}
