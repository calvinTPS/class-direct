package com.baesystems.ai.lr.cd.be.utils;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author VMandalapu
 *
 */
public interface ServiceHelper {

  /**
   * @param dtos value
   * @param id value
   * @throws RecordNotFoundException exception
   */
  void verifyModel(final List<ScheduledServiceHDto> dtos, final Long id) throws RecordNotFoundException;
}
