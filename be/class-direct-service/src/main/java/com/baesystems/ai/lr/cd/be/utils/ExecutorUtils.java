package com.baesystems.ai.lr.cd.be.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutorService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.ThreadLocalStorage;
import com.amazonaws.xray.entities.Entity;
import com.amazonaws.xray.entities.Segment;
import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingAspectImpl;
import com.baesystems.ai.lr.cd.be.domain.profiling.ProfilingStat;


/**
 * Utility to create new thread pool executor with proper naming.
 *
 * @author YWearn
 */
@Component
public final class ExecutorUtils {

  /**
   * Total number of thread.
   */
  @Value("${thread.per.core.ratio}")
  private Integer threadPerCoreRatio;

  /**
   * Total threads per execution.
   */
  private static Integer totalThreads = CDConstants.AVAILABLE_PROCESSORS;

  /**
   * Default task executor.
   */
  private static AsyncTaskExecutor taskExecutor;

  /**
   * Initialize thread per core constant.
   */
  @PostConstruct
  public void initializeThreadPerCore() {
    ExecutorUtils.setTotalThreads(threadPerCoreRatio * CDConstants.AVAILABLE_PROCESSORS);
    final ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
    threadPoolTaskExecutor.setMaxPoolSize(ExecutorUtils.totalThreads);
    threadPoolTaskExecutor.initialize();
    ExecutorUtils.setTaskExecutor(threadPoolTaskExecutor);
  }

  /**
   * Reference to static member.
   *
   * @param threadPoolTaskExecutor executor to be set.
   */
  private static void setTaskExecutor(final ThreadPoolTaskExecutor threadPoolTaskExecutor) {
    ExecutorUtils.taskExecutor = threadPoolTaskExecutor;
  }

  /**
   * Setter for {@link ExecutorUtils#totalThreads}.
   *
   * @param threads number of threads.
   */
  public static void setTotalThreads(final Integer threads) {
    ExecutorUtils.totalThreads = threads;
  }

  /**
   * Constructor.
   */
  private ExecutorUtils() {

  }

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorUtils.class);

  /**
   * Creates a new FixedThreadPoolExecutor with default total threads.
   *
   * @param prefix prefix the thread name.
   * @return executor service with name.
   */
  public static ExecutorService newFixedThreadPool(final String prefix) {
    return new DelegatingSecurityContextExecutorService(
        Executors.newFixedThreadPool(totalThreads, ExecutorUtils.namedThreadFactory(prefix)));
  }

  /**
   * Creates a new FixedThreadPoolExecutor with specific total threads and thread prefix.
   *
   * @param threads total number of threads.
   * @param prefix prefix the thread name.
   * @return executor service with name.
   */
  public static ExecutorService newFixedThreadPool(final Integer threads, final String prefix) {
    return new DelegatingSecurityContextExecutorService(
        Executors.newFixedThreadPool(threads, ExecutorUtils.namedThreadFactory(prefix)));
  }


  /**
   * Return a new ThreadPoolExecutor.
   *
   * @param corePoolSize minimum thread to keep alive.
   * @param maximumPoolSize maximum thread allow to create when all thread is busy.
   * @param keepAlive idle time to clean up extra threads created beyond corePoolSize.
   * @param timeUnit time unit for the keep alive.
   * @param prefix naming for the created thread.
   * @return ThreadPoolExecutor.
   */
  public static ExecutorService newThreadPoolExecutor(final int corePoolSize, final int maximumPoolSize,
      final long keepAlive, final TimeUnit timeUnit, final String prefix) {

    return new DelegatingSecurityContextExecutorService(new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAlive,
        timeUnit, new LinkedBlockingQueue<Runnable>(), namedThreadFactory(prefix)));
  }

  /**
   * Execute task asynchronously in the background.
   *
   * <p>
   * Create new callable task which running asynchronously in the background. Newly spawn task will
   * have their own UUID and X-Ray segment.
   * </p>
   *
   * @param callable task callable.
   * @param <T> type of return result.
   * @param taskName task name.
   * @param uuid unique id to identify request.
   * @return future.
   */
  public static <T> Future<T> executeTask(final Callable<T> callable, final String taskName, final String uuid) {
    return taskExecutor.submit(() -> {
      Thread.currentThread().setName(taskName);
      MDC.put(CDConstants.REQUEST_ID, uuid);
      AWSXRay.clearThreadLocal();
      final Segment segment = AWSXRay.beginSegment(taskName);
      try {
        return callable.call();
      } catch (Throwable exception) {
        segment.addException(exception);
        throw exception;
      } finally {
        segment.end();
        AWSXRay.sendSegment(segment);
        AWSXRay.clearThreadLocal();
        MDC.remove(CDConstants.REQUEST_ID);
      }
    });
  }

  /**
   * Custom thread factory that going to name created thread.
   *
   * @param prefix naming of thread.
   * @return ThreadFactory that naming created thread.
   */
  public static ThreadFactory namedThreadFactory(final String prefix) {
    return new BasicThreadFactory.Builder().namingPattern("cd-" + prefix + "-thread-%d").wrappedFactory(r -> {
      final Entity entity = ThreadLocalStorage.get();
      final String mdc = MDC.get(CDConstants.REQUEST_ID);
      final ProfilingStat currentStat = ProfilingAspectImpl.THREAD_CONTEXT.get();
      return new Thread(() -> {
        if (entity != null) {
          LOGGER.trace("Setting X-Ray current segment to new thread local storage.");
          ThreadLocalStorage.set(entity);
        }
        if (!StringUtils.isEmpty(mdc)) {
          LOGGER.trace("Setting current MDC.");
          MDC.put(CDConstants.REQUEST_ID, mdc);
        }
        if (currentStat != null) {
          LOGGER.trace("Setting profiling stat to child thread.");
          ProfilingAspectImpl.THREAD_CONTEXT.set(currentStat);
        }
        try {
          r.run();
        } finally {
          LOGGER.trace("Clearing local thread storage.");
          if (entity != null) {
            ThreadLocalStorage.clear();
          }
          if (!StringUtils.isEmpty(mdc)) {
            MDC.clear();
          }
          if (currentStat != null) {
            ProfilingAspectImpl.THREAD_CONTEXT.remove();
          }
        }
      });
    }).uncaughtExceptionHandler(
        (t, e) -> LOGGER.error("Thread [{}] fail with following error : {}", t.getName(), e.getMessage())).build();
  }
}
