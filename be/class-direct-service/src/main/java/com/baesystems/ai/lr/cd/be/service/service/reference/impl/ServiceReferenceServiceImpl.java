package com.baesystems.ai.lr.cd.be.service.service.reference.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.MmsServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceGroupHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.enums.ProductType;

import retrofit2.Call;

/**
 * Provides service to fetch scheduled services for an asset and it relevant reference data
 * including service credit status, service catalogue and service groups.
 *
 * @author VMandalapu
 * @author syalavarthi 13-06-2017.
 */
@Service(value = "ServiceService")
public class ServiceReferenceServiceImpl implements ServiceReferenceService, InitializingBean {

  /**
   * The logger for ServiceReferenceService {@link ServiceReferenceServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ServiceReferenceServiceImpl.class);

  /**
   * The {@link ServiceReferenceRetrofitService} from spring context.
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceReferenceDelegate;


  /**
   * The {@link ProductReferenceService} from spring context.
   */
  @Autowired
  private ProductReferenceService productReferenceService;

  /**
   * The {@value #DUE_SOON_IN_MONTHS} due soon threshold is default value in months.
   * <p>
   * to calculate due status if Three months before the Due Date has passed, status is due soon.<br>
   * {@link ServiceReferenceServiceImpl#calculateDueStatus(ScheduledServiceHDto, LocalDate)}.
   * </p>
   */
  private static final Long DUE_SOON_IN_MONTHS = 3L;

  /**
   * The {@value #IMMINENT_IN_MONTHS} imminent threshold default value in month.
   * <p>
   * to calculate due status if One month before the Due Date has passed, status is imminent. <br>
   * {@link ServiceReferenceServiceImpl#calculateDueStatus(ScheduledServiceHDto, LocalDate)}.
   * </p>
   */
  private static final Long IMMINENT_IN_MONTHS = 1L;

  /**
   * The serviceCreditStatus List.
   */
  @Value("#{'${service.credit.status.id}'.split(',')}")
  private List<Long> serviceCreditStatusList;

  @RestrictedAsset
  @Override
  public final List<ScheduledServiceHDto> getServicesForAsset(@AssetID final Long assetId) throws IOException {

    ServiceQueryDto query = new ServiceQueryDto();
    query.setStatusIdList(serviceCreditStatusList);
    return getServices(assetId, query);
  }

  @Override
  public final List<ScheduledServiceHDto> getServices(@AssetID final Long assetId, final ServiceQueryDto query)
      throws IOException {
    final Map<String, String> queryString = AssetCodeUtil.serviceQuerytoQueryMap(query);
    final Call<List<ScheduledServiceHDto>> call = serviceReferenceDelegate.getAssetServicesQuery(assetId, queryString);

    final List<ScheduledServiceHDto> dtos = call.execute().body();
    final Optional<List<ScheduledServiceHDto>> servicesOpt = Optional.ofNullable(dtos).filter(list -> list.size() > 0);
    List<ScheduledServiceHDto> filterDtos = new ArrayList<>();
    if (servicesOpt.isPresent()) {
      filterDtos = servicesOpt.get().stream().filter(serv -> serv.getActive().equals(Boolean.TRUE)).peek(serv -> {
        Resources.inject(serv);

        // inject product group.
        Optional<Long> productGroupId = Optional.ofNullable(serv).map(ScheduledServiceHDto::getServiceCatalogueH)
            .map(ServiceCatalogueHDto::getProductCatalogue).map(ProductCatalogueDto::getProductGroup)
            .map(LinkResource::getId);
        if (productGroupId.isPresent()) {
          ProductGroupDto productGroup = productReferenceService.getProductGroup(productGroupId.get());
          if (productGroup != null) {
            serv.getServiceCatalogueH().setProductGroupH(productGroup);
          }
        }

        Optional<Integer> occurenceNumber = Optional.ofNullable(serv.getOccurrenceNumber());
        if (occurenceNumber.isPresent()) {
          serv.setServiceCatalogueName(
              String.join(" - ", serv.getServiceCatalogueH().getName(), String.valueOf(serv.getOccurrenceNumber())));
        } else {
          serv.setServiceCatalogueName(serv.getServiceCatalogueH().getName());
        }
      }).collect(Collectors.toList());
    }
    return filterDtos;
  }

  @ResourceProvider
  @Override
  public final List<ServiceCreditStatusHDto> getServiceCreditStatuses() {
    List<ServiceCreditStatusHDto> creditStatusHDtos = null;

    try {
      final Call<List<ServiceCreditStatusHDto>> caller = serviceReferenceDelegate.getServiceCreditStatuses();
      creditStatusHDtos = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution Failed for ServiceCreditStatus ", ioe);
    }

    return creditStatusHDtos;
  }

  @ResourceProvider
  @Override
  public final ServiceCreditStatusHDto getServiceCreditStatus(final Long id) {
    ServiceCreditStatusHDto creditStatus = null;

    try {
      final List<ServiceCreditStatusHDto> creditStatusHDtos = self.getServiceCreditStatuses();
      creditStatus = creditStatusHDtos.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException exception) {
      LOGGER.debug("Can't find any element matched id {}.", id);
    }

    return creditStatus;
  }

  @ResourceProvider
  @Override
  public final List<ServiceCatalogueHDto> getServiceCatalogues() {
    List<ServiceCatalogueHDto> catalogueHDtos = null;

    try {
      final Call<List<ServiceCatalogueHDto>> caller = serviceReferenceDelegate.getServiceCatalogues();
      catalogueHDtos = caller.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution Failed for ServiceCatalogues " + exception);
    }

    return catalogueHDtos;
  }

  @Override
  public final List<ServiceCatalogueHDto> getActiveServiceCatalogues() {
    List<ServiceCatalogueHDto> catalogueHDtos = null;

    final List<ServiceCatalogueHDto> fullCatalogueHDtos = self.getServiceCatalogues();

    if (fullCatalogueHDtos != null) {
      final Predicate<ServiceCatalogueHDto> isDeleted =
          (catalogue) -> Optional.ofNullable(catalogue).map(ServiceCatalogueHDto::getDeleted).orElse(Boolean.FALSE);

      catalogueHDtos = fullCatalogueHDtos.stream().filter(isDeleted.negate()).collect(Collectors.toList());
    }
    return catalogueHDtos;
  }

  @ResourceProvider
  @Override
  public final List<MmsServiceCatalogueHDto> getMmsServiceCatalogues() {
    List<MmsServiceCatalogueHDto> mmsCatalogueHDtos = new ArrayList<>();

    final List<ServiceCatalogueHDto> catalogueHDtos = self.getServiceCatalogues();

    if (catalogueHDtos != null) {
      final List<ProductGroupDto> productGroups = productReferenceService.getProductGroups();
      if (productGroups != null) {
        final Set<Long> mmsProductGroupIds = productGroups.stream()
            .filter(pg -> pg.getProductType() != null && pg.getProductType().getId() != null
                && pg.getProductType().getId().longValue() == ProductType.MMS.getValue().longValue())
            .map(ProductGroupDto::getId).collect(Collectors.toSet());
        mmsCatalogueHDtos = catalogueHDtos.stream().filter(cat -> {
          final Long productGroupId = Optional.ofNullable(cat.getProductCatalogue())
              .map(ProductCatalogueDto::getProductGroup).map(LinkResource::getId).orElse(null);
          return mmsProductGroupIds.contains(productGroupId);
        }).map(sc -> {
          final MmsServiceCatalogueHDto mmsServiceCatalogue = new MmsServiceCatalogueHDto();
          // Need the id only for now
          mmsServiceCatalogue.setId(sc.getId());
          return mmsServiceCatalogue;
        }).collect(Collectors.toList());
      }
    }
    return mmsCatalogueHDtos;
  }

  @ResourceProvider
  @Override
  public final ServiceCatalogueHDto getServiceCatalogue(final Long id) {
    ServiceCatalogueHDto serviceCatalogue = null;

    try {
      final List<ServiceCatalogueHDto> catalogueHDtos = self.getServiceCatalogues();
      serviceCatalogue = catalogueHDtos.stream().filter(type -> type.getId().equals(id)).findFirst().get();
      if (serviceCatalogue != null) {
        if (serviceCatalogue.getServiceGroupH() == null && serviceCatalogue.getServiceGroup() != null) {
          try {
            serviceCatalogue.setServiceGroupH(self.getServiceGroup(serviceCatalogue.getServiceGroup().getId()));
          } catch (final IOException e) {
            LOGGER.error("Fail to find service group using id: {}", serviceCatalogue.getServiceGroup().getId(), e);
          }
        }
      }
    } catch (final NoSuchElementException exception) {
      LOGGER.error("Execution Failed for ServiceCatalogue ", exception);
    }

    return serviceCatalogue;
  }

  @ResourceProvider
  @Override
  public final List<ServiceGroupHDto> getServiceGroups() throws IOException {
    List<ServiceGroupHDto> groupHDtos = null;

    try {
      final Call<List<ServiceGroupHDto>> caller = serviceReferenceDelegate.getServiceGroups();
      groupHDtos = caller.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution Failed for ServiceGroups ", exception);
      throw new IOException(exception);
    }

    return groupHDtos;
  }

  @ResourceProvider
  @Override
  public final ServiceGroupHDto getServiceGroup(final Long id) throws IOException {
    ServiceGroupHDto serviceGroupHDto = null;
    try {
      final List<ServiceGroupHDto> groupHDtos = self.getServiceGroups();
      serviceGroupHDto = groupHDtos.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException exception) {
      LOGGER.error("Execution Failed for ServiceGroup " + exception);
    }

    return serviceGroupHDto;
  }

  @Override
  public final DueStatus calculateDueStatus(final ScheduledServiceHDto serviceInput) {
    return calculateDueStatus(serviceInput, LocalDate.now());
  }

  @Override
  public final DueStatus calculateDueStatus(final ScheduledServiceHDto service, final LocalDate date) {
    DueStatus status = DueStatus.NOT_DUE;

    if (service.getPostponementDate() == null) {
      if (service.getUpperRangeDate() != null
          && DateUtils.getLocalDateFromDate(service.getUpperRangeDate()).isBefore(date)
          || service.getDueDate() != null && DateUtils.getLocalDateFromDate(service.getDueDate()).isBefore(date)) {
        status = DueStatus.OVER_DUE;
      } else if (service.getUpperRangeDate() != null
          && date.plusMonths(IMMINENT_IN_MONTHS).isAfter(DateUtils.getLocalDateFromDate(service.getUpperRangeDate()))
          || service.getDueDate() != null
              && date.plusMonths(IMMINENT_IN_MONTHS).isAfter(DateUtils.getLocalDateFromDate(service.getDueDate()))) {
        status = DueStatus.IMMINENT;
      } else if (service.getUpperRangeDate() != null
          && date.plusMonths(DUE_SOON_IN_MONTHS).isAfter(DateUtils.getLocalDateFromDate(service.getUpperRangeDate()))
          || service.getDueDate() != null
              && date.plusMonths(DUE_SOON_IN_MONTHS).isAfter(DateUtils.getLocalDateFromDate(service.getDueDate()))) {
        status = DueStatus.DUE_SOON;
      } else if (service.getLowerRangeDate() != null
          && DateUtils.getLocalDateFromDate(service.getLowerRangeDate()).isBefore(date)) {
        status = DueStatus.DUE_SOON;
      }
    } else {

      final LocalDate postponementDate = DateUtils.getLocalDateFromDate(service.getPostponementDate());

      if (postponementDate.isBefore(date)) {
        status = DueStatus.OVER_DUE;
      } else if (date.plusMonths(IMMINENT_IN_MONTHS).isBefore(postponementDate)
          && date.plusMonths(DUE_SOON_IN_MONTHS).isAfter(postponementDate)) {
        status = DueStatus.DUE_SOON;
      } else if (date.plusMonths(IMMINENT_IN_MONTHS).isAfter(postponementDate)) {
        status = DueStatus.IMMINENT;
      }
    }
    return status;
  }

  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   *
   */
  private ServiceReferenceService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(ServiceReferenceService.class);
  }

}
