package com.baesystems.ai.lr.cd.service.port;

import java.io.IOException;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.port.PortOfRegistryInputDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;

/**
 * Interface to define methods of all Port transactions.
 *
 * @author VKovuru
 *
 */
public interface PortService {

  /**
   * @return List<PortOfRegistryDto>
   * @param inputDto input
   * @throws IOException exceptions
   */
  List<PortOfRegistryDto> getPorts(PortOfRegistryInputDto inputDto) throws IOException;

  /**
   * @param portID id.
   * @return value.
   * @throws IOException exception.
   * @throws RecordNotFoundException exception.
   */
  PortOfRegistryDto getPort(long portID) throws RecordNotFoundException;
}
