package com.baesystems.ai.lr.cd.service.subfleet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetDto;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetOptimizedDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * @author syalavarthi
 *
 */
@SuppressWarnings("CPD-START")
public interface SubFleetService {

  /**
   * @param selectedUserId id.
   * @return value.
   * @throws RecordNotFoundException exception.
   */
  List<Long> getSubFleetById(final String selectedUserId) throws RecordNotFoundException;

  /**
   * Get getSubfleet ids.
   *
   * @param selectedUserId id.
   * @return value.
   * @throws RecordNotFoundException exception.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  @Cacheable(
      cacheNames = CacheConstants.CACHE_SUBFLEET, key = "'" + CacheConstants.PREFIX_USER
          + "'.concat(#selectedUserId).concat('" + CacheConstants.POSTFIX_SUBFLEET_IDS + "')",
      condition = "#selectedUserId != null")
  List<Long> getSubFleetIds(final String selectedUserId)
      throws RecordNotFoundException, ClassDirectException, IOException;

  /**
   * GetRestrictedAssets ids.
   *
   * @param selectedUserId id.
   * @return value.
   * @throws RecordNotFoundException exception.
   * @throws ClassDirectException exception.
   */
  @Cacheable(
      cacheNames = CacheConstants.CACHE_SUBFLEET, key = "'" + CacheConstants.PREFIX_USER
          + "'.concat(#selectedUserId).concat('" + CacheConstants.POSTFIX_RESTRICTED_ASSET_IDS + "')",
      condition = "#selectedUserId != null")
  List<Long> getRestrictedAssetsIds(final String selectedUserId) throws RecordNotFoundException, ClassDirectException;

  /**
   * Get getSubfleet paginated assets.
   *
   * @param selectedUserId id.
   * @param page int.
   * @param size int.
   * @return value.
   * @throws RecordNotFoundException exception.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  PageResource<AssetHDto> getSubfleet(final String selectedUserId, final Integer page, final Integer size)
      throws RecordNotFoundException, ClassDirectException, IOException;

  /**
   * Get getRestrictedAssets paginated assets.
   *
   * @param selectedUserId id.
   * @param page int.
   * @param size int.
   * @return value.
   * @throws RecordNotFoundException exception.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  PageResource<AssetHDto> getRestrictedAssets(final String selectedUserId, final Integer page, final Integer size)
      throws RecordNotFoundException, ClassDirectException, IOException;

  /**
   * This is deprecated. Save subfleet using accessible and restricted assets ids.
   *
   * USED BY THE DATA MIGRATION TEAM.
   *
   * @param selectedUserId id.
   * @param subfleet subfleet.
   * @throws RecordNotFoundException exception.
   * @throws SQLException exception.
   * @throws ClassDirectException exception.
   * @return status.
   */
  @Caching(evict = {
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#selectedUserId).concat('"
              + CacheConstants.POSTFIX_SUBFLEET_IDS + "')",
          condition = "#selectedUserId != null"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#selectedUserId).concat('"
              + CacheConstants.POSTFIX_RESTRICTED_ASSET_IDS + "')",
          condition = "#selectedUserId != null"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#selectedUserId).concat('"
              + CacheConstants.POSTFIX_ACCESSIBLE_ASSET_IDS + "')",
          condition = "#selectedUserId != null"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_USERS,
          key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#selectedUserId)",
          condition = "#selectedUserId != null")})
  StatusDto saveSubFleet(final String selectedUserId, final SubFleetDto subfleet)
      throws RecordNotFoundException, SQLException, ClassDirectException;

  /**
   * Save subfleet using partial list of included/excluded ids.
   *
   * @param selectedUserId id.
   * @param subfleet subfleet.
   * @throws RecordNotFoundException exception.
   * @throws SQLException exception.
   * @throws ClassDirectException exception.
   * @return status.
   */
  @Caching(evict = {
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#selectedUserId).concat('"
              + CacheConstants.POSTFIX_SUBFLEET_IDS + "')",
          condition = "#selectedUserId != null"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#selectedUserId).concat('"
              + CacheConstants.POSTFIX_RESTRICTED_ASSET_IDS + "')",
          condition = "#selectedUserId != null"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_SUBFLEET,
          key = "'" + CacheConstants.PREFIX_USER + "'.concat(#selectedUserId).concat('"
              + CacheConstants.POSTFIX_ACCESSIBLE_ASSET_IDS + "')",
          condition = "#selectedUserId != null"),
      @CacheEvict(cacheNames = CacheConstants.CACHE_USERS,
          key = "'" + CacheConstants.PREFIX_USER_ID + "'.concat(#selectedUserId)",
          condition = "#selectedUserId != null")})
  StatusDto saveSubFleetOptimized(final String selectedUserId, final SubFleetOptimizedDto subfleet)
      throws RecordNotFoundException, SQLException, ClassDirectException;

  /**
   * Get list of accessible asset ids.
   *
   * @param selectedUserId user profile object.
   * @return list of asset id.
   * @throws ClassDirectException when error.
   */
  @Cacheable(
      cacheNames = CacheConstants.CACHE_SUBFLEET, key = "'" + CacheConstants.PREFIX_USER
          + "'.concat(#selectedUserId).concat('" + CacheConstants.POSTFIX_ACCESSIBLE_ASSET_IDS + "')",
      condition = "#selectedUserId != null")
  List<Long> getAccessibleAssetIdsForUser(String selectedUserId) throws ClassDirectException;

  /**
   * Get list of accessible asset ids by user id (without Spring Security checking as it is used by
   * Scheduling Service - {@link com.baesystems.ai.lr.cd.service.cache.CacheScheduler}).
   *
   * @param selectedUserId user profile object.
   * @return list of asset id.
   * @throws ClassDirectException when error.
   */
  List<Long> getAccessibleAssetIdsForUserWithoutCaching(String selectedUserId) throws ClassDirectException;

  /**
   * Returns flag to indicate if a given user has enabled the Sub Fleet whitelist feature.
   *
   * @param selectedUserId the user indentifier.
   * @return true if user's sub fleet whitelist feature is enabled, otherwise false.
   * @throws ClassDirectException if error on fetching result from database.
   */
  boolean isSubFleetEnabled(String selectedUserId) throws ClassDirectException;

}
