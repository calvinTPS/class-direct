package com.baesystems.ai.lr.cd.be.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Spring controller to handle Sitekit/Azure policy redirection:
 * 1. Delay after success edit profile, workaround for few second delay for sitekit to sync changes to multi-nodes.
 * 2. Log success edit profile and reset password message.
 * 3. Log error message.
 *
 * @author Yep Choon
 */
@Controller
@RequestMapping(value = "/user-profile")
public class UserProfileController {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileController.class);

  /**
   * Convert second to miliseconds.
   */
  private static final long SECOND_TO_MILISECONDS = 1000L;

  /**
   * Spring controller redirection prefix.
   */
  private static final String REDIRECT_PREFIX = "redirect:";

  /**
   * Default redirect url after sitekit policy redirection.
   */
  @Value("${sitekit.default.redirect}")
  private String redirectUrl;

  /**
   * Seconds to delay before redirection to CD landing page.
   */
  @Value("${sitekit.editprofile.delay}")
  private int delayInSeconds;

  /**
   * Redirect url if user cancel login.
   */
  @Value("${sitekit.cancel.login.redirect}")
  private String cancelLoginRedirectUrl;

  /**
   * The key in the referrer that indicates user cancellation if present.
   */
  @Value("${sitekit.cancel.key}")
  private String cancelKey;

  /**
   * Sitekit login policy name.
   */
  @Value("${sitekit.login.policyname}")
  private String sitekitLoginPolicyName;

  /**
   * Handle delay before redirect back to CD landing page after user edit profile.
   *
   * @param userId the login user id.
   * @return the redirect url.
   */
  @RequestMapping(value = "/success-edit")
  public final String performSuccessEditRedirect(@RequestHeader("userId") final String userId) {
    LOGGER.debug("User:{} finish edit profile. Delay {} seconds before redirect to {}.", userId, delayInSeconds,
        redirectUrl);

    try {
      Thread.sleep(SECOND_TO_MILISECONDS * delayInSeconds);
    } catch (InterruptedException e) {
      LOGGER.error("Error while delaying edit profile redirection. ErrorMessage: {}", e.getMessage());
    }

    return REDIRECT_PREFIX + redirectUrl;
  }

  /**
   * Handle redirect back to CD landing page after user reset password.
   *
   * @param userId the login user id.
   * @return the redirect url.
   */
  @RequestMapping(value = "/success-reset")
  public final String performSuccessResetPasswordRedirect(@RequestHeader("userId") final String userId) {
    LOGGER.debug("User:{} successfully reset password. Redirect to {}.", userId, redirectUrl);

    return REDIRECT_PREFIX + redirectUrl;
  }


  /**
   * Handle error during user edit profile, reset password or login.
   *
   * @param userId the login user id.
   * @return the redirect url.
   */
  /**
   * Handle error during user edit profile, reset password or login.
   *
   * @param userId the login user id.
   * @param error the error code and error message from sitekit.
   * @return the redirect url
   */
  @RequestMapping(value = "/error")
  public final String performErrorRedirect(@RequestHeader(value = "userId", required = false) final String userId,
      @ModelAttribute final SitekitError error) {
    LOGGER.debug("Referer header: {}", error.getReferrer());
    boolean isCancelFromLoginPage = StringUtils.containsIgnoreCase(error.getReferrer(), sitekitLoginPolicyName)
        && StringUtils.containsIgnoreCase(error.getReferrer(), cancelKey);
    if (isCancelFromLoginPage) {
      // Cancelling from login page.
      LOGGER.error("User:{} hit error while login. ErrorCode: {}, ErrorMessage: {}. Redirect to {}.", userId,
          error.getErrorCode(), error.getErrorMessage(), cancelLoginRedirectUrl);
      return REDIRECT_PREFIX + cancelLoginRedirectUrl;
    }

    // For everything else, redirect to CD root.
    LOGGER.error(
        "User:{} hit error while edit profile/reset password. ErrorCode: {}, ErrorMessage: {}. Redirect to {}.", userId,
        error.getErrorCode(), error.getErrorMessage(), redirectUrl);
    return REDIRECT_PREFIX + redirectUrl;
  }

  /**
   * Return sitekit error form mapping object.
   *
   * @return sitekit error form object.
   */
  @ModelAttribute
  public final SitekitError getModel() {
    return new SitekitError();
  }

  /**
   * Sitekit error form object.
   *
   * @author YWearn
   */
  public static class SitekitError {

    /**
     * Sitekit referrer.
     */
    private String referrer;

    /**
     * Sitekit error code.
     */
    private String errorCode;

    /**
     * Sitekit error message.
     */
    private String errorMessage;


    /**
     * Sitekit referrer url getter.
     *
     * @return referrer url.
     */
    public final String getReferrer() {
      return referrer;
    }

    /**
     * Sitekit referrer url setter.
     *
     * @param referrerStr referrer url.
     */
    public final void setReferrer(final String referrerStr) {
      this.referrer = referrerStr;
    }

    /**
     * Sitekit error code getter.
     *
     * @return error code.
     */
    public final String getErrorCode() {
      return errorCode;
    }

    /**
     * Sitekit error code setter.
     *
     * @param errorCodeStr error code.
     */
    public final void setErrorCode(final String errorCodeStr) {
      this.errorCode = errorCodeStr;
    }

    /**
     * Sitekit error message getter.
     *
     * @return error message.
     */
    public final String getErrorMessage() {
      return errorMessage;
    }

    /**
     * Sitekit error message getter.
     *
     * @param errorMessageStr error message.
     */
    public final void setErrorMessage(final String errorMessageStr) {
      this.errorMessage = errorMessageStr;
    }

  }

}


