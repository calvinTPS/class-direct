package com.baesystems.ai.lr.cd.service.userprofile.helper;

import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

import java.util.List;

/**
 * SSO User Provider.
 *
 * @author fwijaya on 16/5/2017.
 */
public interface SSOUserProvider {
  /**
   * Fetches user from azure then executes the callback.
   *
   * @param userProfiles list of user profiles.
   * @param callback callback.
   * @return the sso user data.
   * @throws ClassDirectException exception.
   */
  List<LRIDUserDto> fetchUsers(List<UserProfiles> userProfiles, UpdateUserCallback callback)
    throws ClassDirectException;

  /**
   * Fetches sso user for a given email.
   *
   * @param email email.
   * @return SSO user data from azure.
   * @throws ClassDirectException exceptions.
   */
  List<LRIDUserDto> fetchUsers(final String email) throws ClassDirectException;

  /**
   * Checks whether the email matches the provider.
   *
   * @param email email.
   * @return true if match.
   */
  boolean isMatch(final String email);
}
