package com.baesystems.ai.lr.cd.service.security;

/**
 * Service to check for availability.
 *
 * @author Faizal Sidek
 */
public interface AvailabilityService {

  /**
   * Check for service availability.
   *
   * @return availabitity flag.
   */
  boolean isServiceAvailable();

  /**
   * Periodically check for service availability.
   */
  void checkForServiceAvailability();
}
