package com.baesystems.ai.lr.cd.be.utils;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;

/**
 * @author VMandalapu
 *
 */
@Component
public class ServiceHelperImpl implements ServiceHelper {

  /**
   *
   */
  @Value("${not.found.error}")
  private String errorMessage;

  @Override
  public final void verifyModel(final List<ScheduledServiceHDto> dtos, final Long id) throws RecordNotFoundException {
    if (null == dtos) {
      throw new RecordNotFoundException(String.format(errorMessage, id));
    }
  }

}
