package com.baesystems.ai.lr.cd.be.processor;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultHeaderFilterStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides feature to only allow white list field(s) returned to rest caller.
 * Since camel by default copy all the request headers into response headers, this will be
 * helpful to remove these request headers from returning to caller.
 * @author YWearn
 *
 */
public class ClassDirectHeaderFilterStrategy extends DefaultHeaderFilterStrategy {

  /**
   * The application logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ClassDirectHeaderFilterStrategy.class);

  /**
   * The list of reponse headers allowed to return to rest caller.
   */
  @Getter
  @Setter
  private List<String> whitelist;

  /* (non-Javadoc)
   * @see org.apache.camel.impl.DefaultHeaderFilterStrategy#extendedFilter
   * (org.apache.camel.spi.HeaderFilterStrategy.Direction, java.lang.String, java.lang.Object
   * , org.apache.camel.Exchange)
   */
  @Override
  protected final boolean extendedFilter(final Direction direction, final String key, final Object value
      , final Exchange exchange) {
    boolean needFilter = false;
    if (Direction.OUT == direction) {
      LOGGER.trace("OUT Header [{}] : {}", key, value);
      if (whitelist != null && whitelist.size() > 0) {
        boolean allow = whitelist.parallelStream().anyMatch(allowHeader -> allowHeader.equalsIgnoreCase(key));
        if (allow) {
          LOGGER.trace("Allow whitelist header [{}] : {}", key, value);
        } else {
          LOGGER.trace("Filtered header [{} : {}]", key, value);
          needFilter = true;
        }
      } else {
        needFilter = true;
      }
    } else {
      LOGGER.trace("IN Header [{}] : {}", key, value);
    }
    return needFilter;
  }


}
