package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides the base model interface for FAR/FSR report export service.
 *
 * @author yng
 *
 */
public interface ReportExportService {

  /**
   * Generate FAR/FSR report in pdf format for download.
   *
   * @param query the POST body. For more information on see {@link ReportExportDto}
   * @return the encrypted token which contain generated PDF report in server file path location.
   * @throws ClassDirectException if API call from MAST, or PDF template compilation fail.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadPdf(ReportExportDto query) throws ClassDirectException;
}
