package com.baesystems.ai.lr.cd.service.flagstate;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

import java.util.List;

/**
 * Provides the remote service for removing, adding and getting flags associations.
 *
 * @author fwijaya on 20/4/2017.
 */
public interface FlagsAssociationsService {
  /**
   * Removes flags associations.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @return The number of entities deleted.
   * @throws ClassDirectException if unchecked JPA exception occured.
   */
  int removeAssociation(final String flagCode, final String secondaryFlagCode) throws ClassDirectException;

  /**
   * Adds flags associations.
   *
   * @param flagCode Flag code.
   * @param secondaryFlagCode Secondary flag code.
   * @throws ClassDirectException if unchecked JPA exception occured.
   */
  void addAssociation(final String flagCode, final String secondaryFlagCode) throws ClassDirectException;

  /**
   * Returns flag associations for a flag code.
   *
   * @param flagCode Flag code.
   * @return Flag associations for a flag code.
   * @throws ClassDirectException if unchecked JPA exception occured.
   */
  List<String> getAssociatedFlags(final String flagCode) throws ClassDirectException;

}
