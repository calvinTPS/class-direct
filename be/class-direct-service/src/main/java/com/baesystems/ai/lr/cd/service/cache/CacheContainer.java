package com.baesystems.ai.lr.cd.service.cache;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Provides a container to get and store cache.
 *
 * @author msidek
 *
 */
@Component
public class CacheContainer {
  /**
   * The application logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CacheContainer.class);

  /**
   * The internal field to store all cache.
   */
  private final Map<Class<?>, Map<Long, ?>> cache = new HashMap<>();

  /**
   * The cache version.
   */
  private Long version;

  /**
   * Gets cache with key type.
   *
   * @param type the cache object data type.
   * @param <T> The cache object data type.
   * @return the map of cached object.
   */
  @SuppressWarnings("unchecked")
  public <T> Map<Long, T> get(final Class<T> type) {
    Map<Long, T> values;
    synchronized (cache) {
      LOGGER.debug("Resolving cache of type {} ", type.getSimpleName());
      if (!cache.containsKey(type)) {
        LOGGER.debug("Can't find any matching that type, returning empty.");
        values = new HashMap<Long, T>();
      } else {
        values = (Map<Long, T>) cache.get(type);
        LOGGER.debug("Found {} objects of type {} ", values.size(), type.getSimpleName());
      }
    }
    return values;
  }

  /**
   * Store the cache object map into memory.
   *
   * @param type the cache object data type.
   * @param value the cache object map.
   */
  public void set(final Class<?> type, final Map<Long, ?> value) {
    synchronized (cache) {
      LOGGER.debug("Setting cache of type {} with {} objects.", type.getSimpleName(), value.size());
      cache.put(type, value);
    }
  }

  /**
   * Invalidates all caches.
   */
  public void invalidate() {
    synchronized (cache) {
      cache.clear();
    }
  }

  /**
   * Gets the cache {@link #version}.
   *
   * @return version.
   *
   */
  public final Long getVersion() {
    return version;
  }

  /**
   * Sets the cache {@link #version}.
   *
   * @param ver new version
   *
   */
  public final void setVersion(final Long ver) {
    this.version = ver;
  }

  /**
   * Checks if internal cache is empty.
   *
   * @return cache status.
   */
  public final Boolean isEmpty() {
    return cache.isEmpty();
  }
}
