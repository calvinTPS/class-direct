package com.baesystems.ai.lr.cd.service.job.reference;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemTypeHDto;

/**
 * Provides services to get list of job status, single job status,list of work item types, single work
 * item, list of report types, single report type and all the reference data associated with job.
 *
 * @author yng
 * @author syalavarthi 23-05-2017.
 *
 */
public interface JobReferenceService {
  /**
   * Gets all job status.
   *
   * @return list of job status.
   */
  List<JobStatusHDto> getJobStatuses();

  /**
   * Gets specific job status by id.
   *
   * @param id the job status id.
   * @return job status.
   *
   */
  JobStatusHDto getJobStatus(Long id);

  /**
   * Gets all report types for a job.
   *
   * @return list of report type.
   */
  List<ReportTypeHDto> getReportTypes();

  /**
   * Gets specific report type by id.
   *
   * @param id the report type id.
   * @return report type.
   */
  ReportTypeHDto getReportType(Long id);


  /**
   * Gets all work item types.
   *
   * @return list of work item types.
   */
  List<WorkItemTypeHDto> getWorkItemTypes();

  /**
   * Gets specific workItemType by id.
   *
   * @param id the workitem type id.
   * @return work item type.
   */
  WorkItemTypeHDto getWorkItemType(Long id);

  /**
   * Fetch all job resolution statuses.
   *
   * @return list of resolution statuses.
   */
  List<JobResolutionStatusHDto> getResolutionStatuses();

  /**
   * Gets specific job resolution status.
   *
   * @param id the resolution status id.
   * @return job resolution status.
   */
  JobResolutionStatusHDto getResolutionStatus(Long id);

  /**
   * Returns job location for a given location id.
   *
   * @param locationId the location id.
   * @return job location.
   */
  LocationHDto getJobLocation(final Long locationId);

  /**
   * Returns job locations.
   *
   * @return list of job locations.
   */
  List<LocationHDto> getJobLocations();

}
