package com.baesystems.ai.lr.cd.be.service.utils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.dto.codicils.CodicilDto;

/**
 * Provides utilities for services.
 *
 * @author syalavarthi
 *
 */
public final class ServiceUtils {
  /**
   * No years for sorting.
   */
  static final Long YEARS = 100L;

  /**
   * Asset imoNumber Length.
   */
  public static final int ASSET_IMO_LENGTH = 7;

  /**
   * The logger instantiated for {@link ServiceUtils}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ServiceUtils.class);

  /**
   * Returns ServiceUtils.
   */
  private ServiceUtils() {

  }

  /**
   * Return the default Comparator for Codicil,which is sorting by.
   *
   * <li>Due Date</li>
   * <li>Reference Code</li>
   *
   * @param <T> If due date is <code>null</code>, set it to a hundred years later so that it appears
   *        at the bottom of the list.
   *
   * @return A codicil Comparator that can be passed in to {@link List#sort(Comparator)}
   */
  public static <T extends CodicilDto> Comparator<T> codicilDefaultComparator() {
    return Comparator
        .comparing((T o) -> Optional.ofNullable(o.getDueDate())
            .orElse(Date.from(LocalDate.now().plusYears(YEARS).atStartOfDay(ZoneId.systemDefault()).toInstant())))
        .thenComparing(Comparator.comparing(T::getReferenceCode));
  }

  /**
   * Return the default Comparator for AssetNotes, which is sorting by.
   *
   * <li>Imposed Date</li>
   * <li>Reference Code</li>
   *
   * If imposed date is <code>null</code>, set it to a hundred years later so that it appears at the
   * bottom of the list.
   *
   * @return A asset notes Comparator that can be passed in to {@link List#sort(Comparator)}
   */
  public static Comparator<AssetNoteHDto> assetNotesDefaultComparator() {
    return Comparator
        .comparing((AssetNoteHDto o) -> Optional.ofNullable(o.getImposedDate())
            .orElse(Date.from(LocalDate.now().plusYears(YEARS).atStartOfDay(ZoneId.systemDefault()).toInstant())))
        .thenComparing(Comparator.comparing(AssetNoteHDto::getReferenceCode));
  }

  /**
   * Sorts services by due date.
   *
   * @param duedate1 the date field to be compared.
   * @param dueDate2 the date field to be compared.
   * @return result.
   */
  public static int sortServiceByDueDate(final Date duedate1, final Date dueDate2) {
    if ((duedate1 != null && dueDate2 != null && duedate1.equals(dueDate2)) || (duedate1 == null && dueDate2 == null)) {
      return 0;
    }

    if (duedate1 == null) {
      return -1;
    }
    if (dueDate2 == null) {
      return 1;
    }
    return duedate1.compareTo(dueDate2);
  }

  /**
   * Sorts services by occurrence number.
   *
   * @param occurrenceNumber1 the date field to be compared.
   * @param occurrenceNumber2 the date field to be compared.
   * @return result.
   */
  public static int sortServiceByOccurrenceNumber(final Integer occurrenceNumber1, final Integer occurrenceNumber2) {
    if ((occurrenceNumber1 != null && occurrenceNumber2 != null) && occurrenceNumber1.equals(occurrenceNumber2)
        || (occurrenceNumber1 == null && occurrenceNumber2 == null)) {
      return 0;
    }

    if (occurrenceNumber1 == null) {
      return -1;
    }
    if (occurrenceNumber2 == null) {
      return 1;
    }
    return occurrenceNumber1.compareTo(occurrenceNumber2);
  }

  /**
   * Sorts item by reference number.
   *
   * @param referenceCode1 the string field to be compared.
   * @param referenceCode2 the string field to be compared.
   * @return result.
   */
  public static int sortItembyReferenceCode(final String referenceCode1, final String referenceCode2) {
    if ((referenceCode1 != null && referenceCode2 != null) && referenceCode1.equals(referenceCode2)
        || (referenceCode1 == null && referenceCode2 == null)) {
      return 0;
    }

    if (referenceCode1 == null) {
      return -1;
    }
    if (referenceCode2 == null) {
      return 1;
    }
    return referenceCode1.compareTo(referenceCode2);
  }

  /**
   * Gets Page count for PDF file.
   *
   * @param workingDir File path and name.
   * @param filename file name.
   * @return page count for PDF.
   */
  public static int getPageCount(final String workingDir, final String filename) {
    PDDocument doc = null;
    int pageCount = 0;
    String filePath = workingDir + File.separator + filename;
    try {
      doc = PDDocument.load(new File(filePath));
    } catch (final InvalidPasswordException invalidPasswordException) {
      LOGGER.error("Exception in page count of PDF" + invalidPasswordException.getMessage());
    } catch (final IOException ioException) {
      LOGGER.error("Exception in page count of PDF" + ioException.getMessage());
    }
    if (null != doc) {
      pageCount = doc.getDocumentCatalog().getPages().getCount();
      try {
        doc.close();
      } catch (IOException ioException) {
        LOGGER.error("Exception in page count of PDF" + ioException.getMessage());
      }
    }
    return pageCount;
  }

  /**
   * Grouping without affecting the existing sorted list.
   *
   * @param <T> type of the value
   * @param <K> key of the value
   * @param function serviceDemo get method
   * @return Collector implementing the group-by operation
   */
  public static <T, K extends Comparable<K>> Collector<T, ?, LinkedHashMap<K, List<T>>> sortedGroupingBy(
      final Function<T, K> function) {
    return Collectors.groupingBy(function, LinkedHashMap::new, Collectors.toList());
  }

  /**
   * Sorting the classification service in the below order.
   * <ul>
   * <li>Product Family ID asc</li>
   * <li>Product Group display_order asc</li>
   * <li>Service due date asc</li>
   * <li>Service catalogue display_order asc</li>
   * <li>Service occurrence number asc</li>
   * </ul>
   *
   * @param servicesList the list of service to be sorted.
   * @return servicesList the sorted service list.
   */
  public static ArrayList<ServiceDemo> sortClassificationService(final ArrayList<ServiceDemo> servicesList) {

    servicesList
        .sort(Comparator.comparing(ServiceDemo::getProductTypeId).thenComparing(ServiceDemo::getProductDisplayOrder)
            .thenComparing(ServiceDemo::getOrigionalDueDate, Comparator.nullsLast(Comparator.naturalOrder()))
            .thenComparing(ServiceDemo::getCatalogueDisplayOrder)
            .thenComparing(ServiceDemo::getOccurrenceNumber, Comparator.nullsLast(Comparator.naturalOrder())));

    return servicesList;
  }

  /**
   * Sorting the mms & statutory service in the below order.
   * <ul>
   * <li>Product Family ID asc</li>
   * <li>Service due date asc</li>
   * <li>Service catalogue display_order asc</li>
   * <li>Service occurrence number asc</li>
   * </ul>
   *
   * @param servicesList the list of service to be sorted.
   * @return servicesList the sorted service list.
   */
  public static ArrayList<ServiceDemo> sortMMSStatutoryService(final ArrayList<ServiceDemo> servicesList) {

    servicesList.sort(Comparator.comparing(ServiceDemo::getProductTypeId)
        .thenComparing(ServiceDemo::getOrigionalDueDate, Comparator.nullsLast(Comparator.naturalOrder()))
        .thenComparing(ServiceDemo::getCatalogueDisplayOrder)
        .thenComparing(ServiceDemo::getOccurrenceNumber, Comparator.nullsLast(Comparator.naturalOrder())));

    return servicesList;
  }


  /**
   * The search input is IMO number if the input having only numeric values and the length is 7.
   *
   * @param searchInput the search input value.
   * @return boolean indicate the search input is ImoNumber or Not.
   */
  public static boolean isImoNumber(final String searchInput) {
    return searchInput.matches("[0-9]+") && searchInput.length() == ASSET_IMO_LENGTH;
  }

}
