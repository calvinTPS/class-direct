package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ChecklistExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides the interface for checklist export service.
 *
 * @author yng
 *
 */
public interface ChecklistExportService {

  /**
   * Generates checklist report in pdf format for download.
   *
   * @param query the post body. For more information see {@link ChecklistExportDto}
   * @return the encrypted token which contain generated PDF report in server file path location.
   * @throws ClassDirectException if API call from MAST, or PDF template compilation fail.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS','THETIS')")
  StringResponse downloadPdf(ChecklistExportDto query) throws ClassDirectException;
}
