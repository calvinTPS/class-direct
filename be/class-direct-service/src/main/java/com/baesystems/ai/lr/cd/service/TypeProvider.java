package com.baesystems.ai.lr.cd.service;

import java.lang.reflect.Method;

/**
 * This class contains map of type-service provider.
 *
 * @author MSidek
 *
 */
public class TypeProvider {

  /**
   * Type to be mapped.
   *
   */
  private Class<?> type;

  /**
   * Reference to service name.
   *
   */
  private String beanName;

  /**
   * Method annotated with ResourceProvider.
   *
   */
  private Method method;

  /**
   * Default constructor.
   *
   * @param mappedType type of bean to be mapped.
   * @param bean Spring bean name.
   * @param serviceMethod bean method.
   *
   */
  public TypeProvider(final Class<?> mappedType, final String bean, final Method serviceMethod) {
    this.type = mappedType;
    this.beanName = bean;
    this.method = serviceMethod;
  }

  /**
   * Getter for {@link #type}.
   *
   * @return type.
   *
   */
  public final Class<?> getType() {
    return type;
  }

  /**
   * Setter for {@link #type}.
   *
   * @param beanType type of bean.
   *
   */
  public final void setType(final Class<?> beanType) {
    this.type = beanType;
  }

  /**
   * Getter for {@link #beanName}.
   *
   * @return bean name.
   *
   */
  public final String getBeanName() {
    return beanName;
  }

  /**
   * Setter for {@link #beanName}.
   *
   * @param name name of spring bean.
   *
   */
  public final void setBeanName(final String name) {
    this.beanName = name;
  }

  /**
   * Getter for {@link #method}.
   *
   * @return method.
   *
   */
  public final Method getMethod() {
    return method;
  }

  /**
   * Setter for {@link #method}.
   *
   * @param serviceMethod method of the provider.
   *
   */
  public final void setMethod(final Method serviceMethod) {
    this.method = serviceMethod;
  }

  @Override
  public final String toString() {
    return "TypeProvider [type=" + type.getSimpleName() + ", beanName=" + beanName + ", method=" + method.getName()
        + ", method-args-count=" + method.getParameterCount() + "]";
  }


}
