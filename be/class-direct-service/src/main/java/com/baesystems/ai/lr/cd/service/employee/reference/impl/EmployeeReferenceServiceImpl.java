package com.baesystems.ai.lr.cd.service.employee.reference.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.employee.EmployeeRetrofitService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;

import retrofit2.Call;

/**
 * Gets employee reference data.
 *
 * @author syalavarthi
 *
 */
@Service(value = "EmployeeReferenceService")
public class EmployeeReferenceServiceImpl implements EmployeeReferenceService, InitializingBean {
  /**
   * The logger for EmployeeReferenceService.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProductReferenceService.class);

  /**
   * The EmployeeRetrofitService from spring context.
   */
  @Autowired
  private EmployeeRetrofitService employeeRetrofitService;

  @Override
  @ResourceProvider
  public final List<LrEmployeeDto> getEmployeeList() {
    List<LrEmployeeDto> employeeList = new ArrayList<>();
    try {
      final Call<List<LrEmployeeDto>> caller = employeeRetrofitService.getEmployee();
      employeeList = caller.execute().body();
    } catch (final IOException ioException) {
      LOGGER.error("Execution failed to get employee reference data.", ioException);
    }
    return employeeList;
  }

  @Override
  @ResourceProvider
  public final LrEmployeeDto getEmployee(final Long id) {
    LrEmployeeDto employee = null;
    try {
      final List<LrEmployeeDto> employeeList = self.getEmployeeList();
      employee = employeeList.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException exception) {
      LOGGER.error("Execution Failed for ServiceGroup " + exception);
    }

    return employee;
  }

  @Override
  @ResourceProvider
  public final List<OfficeDto> getOfficeList() {
    List<OfficeDto> officeList = new ArrayList<>();
    try {
      final Call<List<OfficeDto>> caller = employeeRetrofitService.getOffice();
      officeList = caller.execute().body();
    } catch (final IOException ioException) {
      LOGGER.error("Execution failed to get office reference data.", ioException);
    }
    return officeList;
  }

  @Override
  @ResourceProvider
  public final OfficeDto getOffice(final Long id) {
    OfficeDto office = null;
    try {
      final List<OfficeDto> officeList = self.getOfficeList();
      office = officeList.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException exception) {
      LOGGER.error("Execution Failed for ServiceGroup " + exception);
    }
    return office;
  }

  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   *
   */
  private EmployeeReferenceService self = null;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(EmployeeReferenceService.class);
  }

}
