package com.baesystems.ai.lr.cd.service.aws.impl;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.parboiled.common.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManager;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.service.aws.AWSHelper;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;

/**
 * Implementation of {@link com.baesystems.ai.lr.cd.service.aws.AmazonStorageService}.
 *
 * @author Faizal Sidek
 */
@Service
public class AmazonStorageServiceImpl implements AmazonStorageService {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AmazonStorageService.class);

  /**
   * AWS access key.
   */
  @Value("${s3.access_key}")
  private String accessKey;

  /**
   * AWS secret key.
   */
  @Value("${s3.secret_key}")
  private String secretKey;

  /**
   * Temp folder for file.
   */
  @Value("${s3.archive.temp.path}")
  private String tempPathFolder;

  /**
   * Default region for S3 storage.
   */
  @Value("${s3.default.region}")
  private String s3DefaultRegion = Regions.EU_WEST_1.toString();

  /**
   * AWS Credentials.
   */
  private BasicAWSCredentials credentials;

  /**
   * Initialize AWS credentials.
   */
  @PostConstruct
  public final void initializeCredentials() {
    if (StringUtils.isEmpty(accessKey) || StringUtils.isEmpty(secretKey)) {
      LOGGER.info("AWS accessKey or secretKey is empty. Going to use EC2 IAM role to authenticate AWS services.");
    } else {
      credentials = new BasicAWSCredentials(accessKey, secretKey);
    }
  }

  /**
   * Thin class to help create client.
   */
  @Autowired
  private AWSHelper awsHelper;

  @Override
  public final String uploadFile(final String region, final String bucketName, final String key,
      final String sourceFilePath, final String userId) throws ClassDirectException {
    try {
      final AmazonS3 client = awsHelper.createS3Client(credentials, region);
      client.putObject(bucketName, key, new File(sourceFilePath));
      return FileTokenGenerator.generateS3Token(region, bucketName, key, userId);
    } catch (SdkClientException exception) {
      LOGGER.error("Error when uploading to S3.", exception);
      throw new ClassDirectException("Failed to upload file to S3.");
    }
  }

  @Override
  public final String uploadFile(final String bucketName, final String key, final String sourceFilePath,
      final String userId) throws ClassDirectException {
    return uploadFile(s3DefaultRegion, bucketName, key, sourceFilePath, userId);
  }

  @Override
  public final byte[] downloadFile(final String region, final String bucketName, final String fileKey)
      throws ClassDirectException {
    try {
      LOGGER.debug("Downloading file from region {} with bucket name {} and key {} ", region, bucketName, fileKey);
      final AmazonS3 client = awsHelper.createS3Client(credentials, region);
      final S3Object s3Object = client.getObject(bucketName, fileKey);
      return awsHelper.s3ObjectToByteArray(s3Object);
    } catch (IOException exception) {
      LOGGER.error("Failed to download file.", exception);
      throw new ClassDirectException("Failed to download file from S3.");
    }
  }

  @Override
  public final String archiveFile(final String vaultName, final String description, final String fileName,
      final byte[] contents) throws ClassDirectException {
    try {
      LOGGER.debug("Archiving file to vault {} with file name {} and description {} ", vaultName, fileName,
          description);
      final ArchiveTransferManager transferManager = awsHelper.createTransferManager(credentials, s3DefaultRegion);
      final File tempFile = new File(tempPathFolder, fileName);
      awsHelper.writeByteArrayToFile(tempFile, contents);

      final String archiveId = transferManager.upload(vaultName, description, tempFile).getArchiveId();
      LOGGER.debug("Successfully archived with id {} ", archiveId);
      if (tempFile.delete()) {
        LOGGER.debug("Temp file deleted.");
      }
      return archiveId;
    } catch (IOException | AmazonServiceException exception) {
      LOGGER.error("Failed to archive the file.", exception);
      throw new ClassDirectException("Failed to archive file " + fileName);
    }
  }
}
