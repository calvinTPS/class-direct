package com.baesystems.ai.lr.cd.service.export;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.CertificateAttachDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;


/**
 * Provides service methods to download certificates from CS10.
 *
 * @author sbollu
 *
 */
public interface CertificateDownloadService {

  /**
   * Downloads certificate from CS10 and generates Certificate PDF for given asset.
   *
   * @param query the post body.
   * @return the encrypted string token which contain file path location of generated PDF report in server.
   * @throws ClassDirectException if API call, or PDF template compilation fails.
   */
  @PreAuthorize("!hasAnyAuthority('EQUASIS,THETIS')")
  StringResponse downloadCertificateInfo(CertificateAttachDto query) throws ClassDirectException;

}
