package com.baesystems.ai.lr.cd.service.employee.reference;

import java.util.List;

import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;

/**
 * Provides employee reference data.
 *
 * @author syalavarthi.
 *
 */
public interface EmployeeReferenceService {

  /**
   * Gets employee list.
   *
   * @return A list of {@link LrEmployeeDto}.
   */
  List<LrEmployeeDto> getEmployeeList();

  /**
   * Gets employee by employee id.
   *
   * @param id id of the employee.
   * @return {@link LrEmployeeDto}.
   */
  LrEmployeeDto getEmployee(Long id);

  /**
   * Gets office list.
   *
   * @return A list of {@link OfficeDto}.
   */
  List<OfficeDto> getOfficeList();

  /**
   * Gets office by office id.
   *
   * @param id id of the office.
   * @return {@link OfficeDto}.
   */
  OfficeDto getOffice(Long id);
}
