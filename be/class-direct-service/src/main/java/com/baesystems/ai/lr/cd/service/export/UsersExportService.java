package com.baesystems.ai.lr.cd.service.export;

import java.io.IOException;

import org.springframework.security.access.prepost.PreAuthorize;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides service to fetches encrypted token which contain generated PDF report in server file
 * path location.
 *
 *
 * @author syalavarthi.
 *
 */
public interface UsersExportService {
  /**
   * Generates users report in pdf format for download.
   *
   * @param usersexportDto the query body with search parameters.
   * @return the encrypted token which contain generated PDF report in server file path location.
   * @throws ClassDirectException if API call from MAST, or PDF template compilation fail.
   * @throws IOException if error generating csv.
   */
  @PreAuthorize("hasAnyAuthority('LR_ADMIN','LR_SUPPORT')")
  StringResponse downloadFile(UserProfileDto usersexportDto) throws ClassDirectException, IOException;

}
