package com.baesystems.ai.lr.cd.be.processor;

import java.time.LocalDate;

import com.baesystems.ai.lr.cd.be.enums.DueStatus;

/**
 * @author VKolagutla
 *
 * @param <T> class.
 */
public interface DueStatusCalculator<T> {

  /**
   * This is method for DueStatus calculator with reference date of LocalDate.now().
   *
   * @param object obj.
   * @return status.
   */
  DueStatus calculateDueStatus(T object);

  /**
   * This is overloading method for DueStatus calculator to be able change reference date.
   *
   * @param object obj.
   * @param currentDate current Date.
   * @return status.
   */
  DueStatus calculateDueStatus(T object, LocalDate currentDate);
}
