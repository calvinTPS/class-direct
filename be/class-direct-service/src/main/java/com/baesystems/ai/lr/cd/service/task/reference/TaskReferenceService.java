package com.baesystems.ai.lr.cd.service.task.reference;

import java.io.IOException;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.CheckListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedValueGroupsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedWorkItemAttributeValueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;

/**
 * Provides methods to get reference data related to tasks.
 *
 * @author VKolagutla
 * @author sBollu
 *
 */
public interface TaskReferenceService {

  /**
   * Fetch all postponement types.
   *
   * @return list of postponement types.
   */
  List<PostponementTypeHDto> getPostponementTypes();

  /**
   * Get specific postponement type.
   *
   * @param id of postponement type.
   * @return postponement type.
   */
  PostponementTypeHDto getPostponementType(Long id);

  /**
   * Returns checklist's reference data.
   *
   * @return list of checklist's.
   */
  List<CheckListHDto> getCheckLists();

  /**
   * Returns specific checklist {@link CheckListHDto} by checklist id.
   *
   * @param id the unique identifier of checklist.
   * @return details of specific checklist by checklist id.
   */
  CheckListHDto getCheckList(Long id);

  /**
   * Returns checklist groups reference data.
   *
   * @return list of checklist groups.
   */
  List<ChecklistGroupDto> getChecklistGroups();

  /**
   * Returns specific checklist group {@link ChecklistGroupDto} by checklist group id.
   *
   * @param id the unique identifier of checklist group.
   * @return details of specific checklist group by checklist group id.
   */
  ChecklistGroupDto getChecklistGroup(Long id);

  /**
   * Returns checklist subgroups reference data.
   *
   * @return list of checklist subgroups.
   */
  List<ChecklistSubgroupDto> getChecklistSubgroups();

  /**
   * Returns specific checklist subgroup {@link ChecklistSubgroupDto} by checklist subgroup id.
   *
   * @param id the unique identifier of checklist subgroup.
   * @return details of specific checklist subgroup by checklist subgroup id.
   */
  ChecklistSubgroupDto getChecklistSubgroup(Long id);

  /**
   * Returns list of allowed value groups for work item conditional attributes.
   *
   * @return list of allowed value groups.
   * @throws IOException if mast api call fail.
   */
  List<AllowedValueGroupsHDto> getAllowedValueGroups() throws IOException;

  /**
   * Returns allowed value group by allowed value group id.
   * @param id unique identifier of allowed value group.
   *
   * @return allowed value group.
   * @throws IOException if mast api call fail.
   */
  AllowedValueGroupsHDto getAllowedValueGroup(Long id) throws IOException;

  /**
   * Returns list of allowed attribute values for work item conditional attributes.
   *
   * @return list of allowed attribute values.
   */
  List<AllowedWorkItemAttributeValueHDto> getAllowedAttributeValues();

  /**
   * Returns allowed attribute value by allowed attribute value id.
   * @param id unique identifier of allowed attribute value.
   *
   * @return allowed attribute value.
   */
  AllowedWorkItemAttributeValueHDto getAllowedAttributeValue(Long id);


}
