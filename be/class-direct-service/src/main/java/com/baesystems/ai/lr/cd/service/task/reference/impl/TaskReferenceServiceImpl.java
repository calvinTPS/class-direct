package com.baesystems.ai.lr.cd.service.task.reference.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CheckListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedValueGroupsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedWorkItemAttributeValueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import retrofit2.Call;


/**
 * @author VKolagutla
 * @author sBollu
 *
 */
@Service("taskReferenceService")
public class TaskReferenceServiceImpl implements TaskReferenceService, InitializingBean {

  /**
   * The Logger object to display logs {@link TaskReferenceServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(JobReferenceService.class);

  /**
   * The {@link TaskReferenceRetrofitService} from Spring context.
   */
  @Autowired
  private TaskReferenceRetrofitService taskReferenceRetrofitService;

  /**
   * Fetch all postponementTypes.
   *
   * @return list of postponementTypes.
   */
  @ResourceProvider
  @Override
  public final List<PostponementTypeHDto> getPostponementTypes() {
    List<PostponementTypeHDto> postponementTypes = new ArrayList<>();

    try {
      final Call<List<PostponementTypeHDto>> caller = taskReferenceRetrofitService.getPostponementTypes();
      postponementTypes = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }

    return postponementTypes;
  }

  /**
   * Get postponementType by id.
   *
   * @param id of postponementType.
   * @return postponementType.
   *
   */
  @ResourceProvider
  @Override
  public final PostponementTypeHDto getPostponementType(final Long id) {
    PostponementTypeHDto postponementType = null;

    final List<PostponementTypeHDto> postponementTypes = self.getPostponementTypes();
    final Optional<PostponementTypeHDto> postponementTypeOpt =
        postponementTypes.stream().filter(postponementTypeObj -> postponementTypeObj.getId().equals(id)).findFirst();

    if (postponementTypeOpt.isPresent()) {
      postponementType = postponementTypeOpt.get();
    }

    return postponementType;
  }


  @Override
  @ResourceProvider
  public final List<CheckListHDto> getCheckLists() {
    List<CheckListHDto> ckList = new ArrayList<>();
    final Call<List<CheckListHDto>> checkList = taskReferenceRetrofitService.getChecklists();
    try {
      ckList = checkList.execute().body();
      ckList.forEach(Resources::inject);
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getCheckLists().", exception);
    }
    return ckList;
  }

  @ResourceProvider
  @Override
  public final CheckListHDto getCheckList(final Long id) {
    CheckListHDto checkList = null;

    final List<CheckListHDto> ckList = self.getCheckLists();

    checkList = ckList.stream().filter(ckObj -> ckObj.getId().equals(id)).findFirst().orElse(null);

    return checkList;
  }


  @Override
  @ResourceProvider
  public final List<ChecklistGroupDto> getChecklistGroups() {
    List<ChecklistGroupDto> ckGroupList = new ArrayList<>();
    final Call<List<ChecklistGroupDto>> checkListGroup = taskReferenceRetrofitService.getChecklistGroups();
    try {
      ckGroupList = checkListGroup.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getChecklistGroups().", exception);
    }
    return ckGroupList;
  }

  @ResourceProvider
  @Override
  public final ChecklistGroupDto getChecklistGroup(final Long id) {
    ChecklistGroupDto checkListGroup = null;

    final List<ChecklistGroupDto> ckList = self.getChecklistGroups();

    checkListGroup = ckList.stream().filter(ckObj -> ckObj.getId().equals(id)).findFirst().orElse(null);

    return checkListGroup;
  }



  @Override
  @ResourceProvider
  public final List<ChecklistSubgroupDto> getChecklistSubgroups() {
    List<ChecklistSubgroupDto> ckSubgroupList = new ArrayList<>();
    final Call<List<ChecklistSubgroupDto>> checkList = taskReferenceRetrofitService.getChecklistSubgroups();
    try {
      ckSubgroupList = checkList.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getChecklistSubgroups().", exception);
    }
    return ckSubgroupList;
  }

  @ResourceProvider
  @Override
  public final ChecklistSubgroupDto getChecklistSubgroup(final Long id) {
    ChecklistSubgroupDto checkListSubgroup = null;

    final List<ChecklistSubgroupDto> ckList = self.getChecklistSubgroups();

    checkListSubgroup = ckList.stream().filter(ckObj -> ckObj.getId().equals(id)).findFirst().orElse(null);

    return checkListSubgroup;
  }

  @ResourceProvider
  @Override
  public final List<AllowedValueGroupsHDto> getAllowedValueGroups() throws IOException {
    List<AllowedValueGroupsHDto> allowedValueGroups = new ArrayList<>();
    final Call<List<AllowedValueGroupsHDto>> groups = taskReferenceRetrofitService.getAllowedValueGroups();
    try {
      allowedValueGroups = groups.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getAllowedValueGroups().", exception);
    }
    return allowedValueGroups;
  }

  @ResourceProvider
  @Override
  public final AllowedValueGroupsHDto getAllowedValueGroup(final Long id) throws IOException {
    AllowedValueGroupsHDto allowedValuegroup = null;
    final List<AllowedValueGroupsHDto> groupList = self.getAllowedValueGroups();
    allowedValuegroup = groupList.stream().filter(ckObj -> ckObj.getId().equals(id)).findFirst().orElse(null);

    return allowedValuegroup;
  }

  @ResourceProvider
  @Override
  public final List<AllowedWorkItemAttributeValueHDto> getAllowedAttributeValues() {
    List<AllowedWorkItemAttributeValueHDto> allowedAttributeValues = new ArrayList<>();
    final Call<List<AllowedWorkItemAttributeValueHDto>> values =
        taskReferenceRetrofitService.getAllowedAttributeValues();
    try {
      allowedAttributeValues = values.execute().body();
    } catch (final IOException exception) {
      LOGGER.error("Execution failed for getAllowedValueGroups().", exception);
    }
    return allowedAttributeValues;
  }

  @ResourceProvider
  @Override
  public final AllowedWorkItemAttributeValueHDto getAllowedAttributeValue(final Long id) {
    AllowedWorkItemAttributeValueHDto allowedAttributeValue = null;
    try {
      final List<AllowedWorkItemAttributeValueHDto> values = self.getAllowedAttributeValues();
      allowedAttributeValue = values.stream().filter(ckObj -> ckObj.getId().equals(id)).findFirst().orElse(null);
      if (allowedAttributeValue != null) {
        if (allowedAttributeValue.getAllowedValueGroupH() == null
            && allowedAttributeValue.getAllowedValueGroup() != null) {
          try {
            allowedAttributeValue
                .setAllowedValueGroupH(self.getAllowedValueGroup(allowedAttributeValue.getAllowedValueGroup().getId()));
          } catch (final IOException e) {
            LOGGER.error("Fail to find service group using id: {}",
                allowedAttributeValue.getAllowedValueGroup().getId(), e);
          }
        }
      }
    } catch (final NoSuchElementException exception) {
      LOGGER.error("Execution Failed for ServiceCatalogue ", exception);
    }
    return allowedAttributeValue;
  }

  /**
   * The context {@link ApplicationContext}.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service {@link TaskReferenceService} from spring context.
   *
   */
  @SuppressFBWarnings
  private TaskReferenceService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(TaskReferenceService.class);
  }

}
