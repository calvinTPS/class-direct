package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.DeficiencyService;
import com.baesystems.ai.lr.cd.service.export.StatutoryDeficienciesExportService;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Provides local service for generating statutory deficiencies PDF export.
 *
 * @author sbollu
 *
 */
@Service(value = "StatutoryDeficienciesExportService")
@Loggable(Loggable.DEBUG)
public class StatutoryDeficienciesExportServiceImpl extends BasePdfExport<ExportPdfDto>
    implements StatutoryDeficienciesExportService {
  /**
   * {@value #LOGGER} is the Logger prints logs for local service.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(StatutoryDeficienciesExportServiceImpl.class);

  /**
   * The {@link DeficiencyService} from spring context.
   */
  @Autowired
  private DeficiencyService deficiencyService;

  /**
   * The export PDF prefix value is {@value #PDF_PREFIX_SDEFICIENCY}.
   */
  private static final String PDF_PREFIX_SDEFICIENCY = "SDEFICIENCY";
  /**
   * The Pug template value is {@value #TEMPLATE}.
   */
  private static final String TEMPLATE_STAT_DEF = "statutory_deficiencies_listing_export_template.jade";

  /**
   * The {@link AssetService} from spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The directory name in Amazon S3 where the generated statutory findings PDF to be uploaded. This
   * variable in only use at {@link BasePdfExport}. <br>
   * For more information see {@link StatutoryDeficienciesExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "sd_export";

  /**
   * This field is use as variable in pug template that store list of statutory findings object. The
   * list value is {@value #SDLIST}.<br>
   * Refer to {@link StatutoryDeficienciesExportServiceImpl# generateFile(Map, ExportPdfDto,
   * UserProfiles, <br>
   * String, LocalDateTime)}.
   */
  private static final String SDLIST = "defList";

  /**
   * This field is use as variable in pug template that store asset object. <br>
   * The asset value is {@value #ASSET}. Refer to
   * {@link StatutoryDeficienciesExportServiceImpl#generateFile(Map, ExportPdfDto, UserProfiles,<br>
   * String, LocalDateTime)}.
   */
  private static final String ASSET = "asset";

  /**
   * This field is use to check if the pdf is single page or multi page.
   */
  private static final String ONLY_ONE_PAGE = "onlyOnePage";

  /**
   * The working directory name where the PDF is generated. This variable in only use at
   * {@link BasePdfExport} <br>
   * For more information see {@link StatutoryDeficienciesExportServiceImpl#getTempDir()}.
   */
  @Value("${statutoryDeficiencies.export.temp.dir}")
  private String tempDir;

  @Override
  protected final String generateFile(final Map<String, Object> model, final ExportPdfDto query,
      final UserProfiles user, final String workingDirSD, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Retrieving object...");

      // get asset information
      final AssetHDto asset = assetService.assetByCode(user.getUserId(), query.getCode());
      if (asset == null) {
        throw new ClassDirectException("Asset not found.");
      } else {
        model.put(ASSET, asset);
      }

      // get SDeficiencies by asset id.
      List<DeficiencyHDto> defList = deficiencyService.getDeficiencies(asset.getId(), new DeficiencyQueryHDto());

      // filter deficiency by id
      if (defList != null && !defList.isEmpty()) {
        if (query.getItemsToExport() != null && !query.getItemsToExport().isEmpty()) {
          defList = defList.stream().filter(item -> query.getItemsToExport().contains(item.getId()))
              .collect(Collectors.toList());
        }
        // sort deficiency by incident date LRCD-3352
        defList.sort((s1, s2) -> {
          return ServiceUtils.sortServiceByDueDate(s1.getIncidentDate(), s2.getIncidentDate());
        });
      }
      model.put(SDLIST, defList);

      String filename =
          generatePdf(PDF_PREFIX_SDEFICIENCY, query.getCode(), current, TEMPLATE_STAT_DEF, workingDirSD, model);
      int pageCountForSD = ServiceUtils.getPageCount(workingDirSD, filename);
      // if the pdf has only one page then we will recall the page to put ONLY_ONE_PAGE flag in the
      // model
      if (pageCountForSD == 1) {
        model.put(ONLY_ONE_PAGE, "true");
        filename =
            generatePdf(PDF_PREFIX_SDEFICIENCY, query.getCode(), current, TEMPLATE_STAT_DEF, workingDirSD, model);
      }

      return filename;
    } catch (JadeException | IOException | ParserConfigurationException | SAXException | DocumentException exception) {
      throw new ClassDirectException(exception);
    }
  }

  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return tempDir;
  }

}
