package com.baesystems.ai.lr.cd.be.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.baesystems.ai.lr.cd.be.domain.dto.task.AuditPmsAbleTaskDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightListHDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

/**
 * @author SBollu
 *
 */
public final class TaskUtils {

  /**
   * Private constructor TaskUtils.
   */
  private TaskUtils() {
    super();
  }

  /**
   * Logger for TaskUtils class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(TaskUtils.class);


  /**
   * @param workItemLightHDto workItemLightHDto.
   * @return WorkItemLightDto object.
   */
  public static WorkItemLightDto convertHdtoToDto(final WorkItemLightHDto workItemLightHDto) {

    final WorkItemLightDto workItemLightDto = new WorkItemLightDto();
    BeanUtils.copyProperties(workItemLightHDto, workItemLightDto);

    return workItemLightDto;
  }

  /**
   * @param workItemLightDto workItemLightDto.
   * @return WorkItemLightHDto object.
   */
  public static WorkItemLightHDto convertDtoToHDto(final WorkItemLightDto workItemLightDto) {

    final WorkItemLightHDto workItemLightHDto = new WorkItemLightHDto();
    BeanUtils.copyProperties(workItemLightDto, workItemLightHDto);

    return workItemLightHDto;
  }

  /**
   * Prepares list of audiatable pms tasks.
   *
   * @param updatableTasks the list of pms tasks to be credited.
   * @param includePmsDateCredited the flag to include pms credit date.
   * @return list of audiatable pms tasks.
   */
  public static List<AuditPmsAbleTaskDto> getTasksToBeCredited(final WorkItemLightListHDto updatableTasks,
      final boolean includePmsDateCredited) {

    List<AuditPmsAbleTaskDto> auditPmsAbleTasks = new ArrayList<AuditPmsAbleTaskDto>();

    updatableTasks.getTasks().stream().forEach(task -> {

      AuditPmsAbleTaskDto auditDto = new AuditPmsAbleTaskDto();
      auditDto.setId(task.getId());
      if (includePmsDateCredited) {
        auditDto.setDateCredited(task.getPmsCreditDate());
      }
      auditDto.setTaskName(task.getName());
      auditDto.setTaskNumber(task.getTaskNumber());
      auditPmsAbleTasks.add(auditDto);
    });

    return auditPmsAbleTasks;

  }
}
