package com.baesystems.ai.lr.cd.service.userprofile.helper;

import com.baesystems.ai.lr.cd.be.domain.dao.lrid.InternalUserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author fwijaya on 16/5/2017.
 */
@Service("internalSSOProvider")
public class InternalSSOProvider extends BaseSSOProvider {
  /**
   * Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(InternalSSOProvider.class);

  /**
   * Azure query limit.
   */
  @Value("${internal.lr.ad.users.query.limit}")
  @Getter
  private int queryLimit;

  /**
   * Email filter.
   */
  @Value("${internal.lr.ad.email.filter}")
  @Getter
  private String emailFilter;

  /**
   * retrofit.
   */
  @Autowired
  private InternalUserRetrofitService retrofitService;

  /**
   * Check whether the given email is internal LR email.
   *
   * @param email The email address.
   * @return true if email matches.
   */
  @Override
  public final boolean isMatch(final String email) {
    return getLREmailDomains().contains(email.substring(email.indexOf('@') + 1).toLowerCase(Locale.ENGLISH));
  }

  /**
   * Returns SSO user for given formatted emails.
   *
   * @param formattedEmails formatted emails.
   * @return SSO users.
   */
  @Override
  public final List<LRIDUserDto> getSSOUser(final String formattedEmails) {
    final List<LRIDUserDto> userDtoList = new ArrayList<>();
    final Call<List<LRIDUserDto>> azureUser = retrofitService.getInternalUsers(formattedEmails);
    if (azureUser != null) {
      try {
        final List<LRIDUserDto> result = azureUser.execute().body();
        if (result != null && !result.isEmpty()) {
          LOGGER.info("Found {} users from azure email {}", result.size(), formattedEmails);
          userDtoList.addAll(result);
        }
      } catch (final IOException exception) {
        LOGGER.error("Error while querying azure", exception);
      }
    }
    return userDtoList;
  }
}
