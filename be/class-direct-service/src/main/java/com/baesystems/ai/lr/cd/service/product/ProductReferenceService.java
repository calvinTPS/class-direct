package com.baesystems.ai.lr.cd.service.product;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.certificate.ProductCatalogueHDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;

/**
 * Gets product reference data.
 */
public interface ProductReferenceService {

  /**
   * Gets list of product catalogues.
   *
   * @return A list of {@link ProductCatalogueHDto}.
   */
  List<ProductCatalogueHDto> getProductCatalogues();

  /**
   * Gets product catalogue by id.
   *
   * @param id id of the catalogue.
   * @return {@link ProductCatalogueHDto}.
   */
  ProductCatalogueHDto getProductCatalogue(Long id);

  /**
   * Gets list of product groups.
   *
   * @return A list of {@link ProductGroupDto}.
   */
  List<ProductGroupDto> getProductGroups();

  /**
   * Gets product group by id.
   *
   * @param id id of the product group.
   * @return {@link ProductGroupDto}.
   */
  ProductGroupDto getProductGroup(Long id);
}
