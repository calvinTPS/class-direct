package com.baesystems.ai.lr.cd.service.asset.impl;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.CLIENT;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.FLAG;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.closedJobsWithRange;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.maxBy;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baesystems.ai.lr.cd.be.domain.annotation.AssetID;
import com.baesystems.ai.lr.cd.be.domain.annotation.RestrictedAsset;
import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CustomerLinkHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.employee.EmployeeDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.AttachmentsCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobWithFlagsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.service.JobSurveyDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AttachmentCategoryEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.JobService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobWithFlagsQueryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.enums.AttachmentCategory;
import com.baesystems.ai.lr.enums.EmployeeRole;
import com.baesystems.ai.lr.enums.JobCategory;
import com.baesystems.ai.lr.enums.JobStatus;
import com.baesystems.ai.lr.enums.PartyRole;
import com.baesystems.ai.lr.enums.ReportType;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcabi.aspects.Loggable;

import retrofit2.Call;

/**
 * Implementation class for jobs related transaction.
 *
 * @author yng
 * @author SBollu
 * @author YWearn 2018-03-07
 * @author syalavarthi 2018-03-21.
 *
 */
@Service(value = "JobService")
@Loggable(Loggable.DEBUG)
public class JobServiceImpl implements JobService {

  /**
   * The application logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(JobServiceImpl.class);

  /**
   * The {@link JobRetrofitService} from Spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The {@link AttachmentRetrofitService} from Spring context.
   */
  @Autowired
  private AttachmentRetrofitService attachmentService;

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;
  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * {@value #REPORT_TYPE_TM} is display report type TM.
   */
  private static final String REPORT_TYPE_TM = "TM";

  /**
   * {@value #REPORT_TYPE_MMS} is display report type MMS.
   */
  private static final String REPORT_TYPE_MMS = "MMS";

  /**
   * {@value #REPORT_TYPE_ESP} is display report type ESP.
   */
  private static final String REPORT_TYPE_ESP = "ESP";

  /**
   * {@value #SURVEY_REPORTS_TO_BE_RETURNED} the number of survey reports to be returned.
   */
  private static final int SURVEY_REPORTS_TO_BE_RETURNED = 4;

  /**
   * The list of report type that CD need, currently FAR and FSR.
   */
  private static final List<String> VALID_REPORT_TYPE_LIST = Arrays.asList(new String[] {"FAR", "FSR"});

  /**
   * The {@link EmployeeReferenceService} from Spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;

  /**
   * Returns survey reports the list of reports for each job(FAR, FSR, TM and ESP) associated with
   * asset.
   *
   * @param assetId the primary key of the asset.
   * @return the list of reports as {@link ReportHDto}.
   *
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   */
  public final List<ReportHDto> getSurveyReports(final long assetId) throws ClassDirectException {
    List<ReportHDto> responseResult = new ArrayList<ReportHDto>();
    final List<ReportHDto> response = new ArrayList<ReportHDto>();

    final ExecutorService executorService = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());

    final List<Callable<List<ReportHDto>>> callables = new ArrayList<>();
    try {
      final List<JobCDDto> jobResponse = getJobsByAssetIdCall(assetId, true);

      jobResponse.forEach(job -> {
        callables.add(reportResponse(job));
      });

      final List<Future<List<ReportHDto>>> taskList = executorService.invokeAll(callables);
      for (final Future<List<ReportHDto>> task : taskList) {
        try {
          response.addAll(task.get());
        } catch (final ExecutionException exp) {
          LOGGER.error("Error executing JobService - getSurveyReports.", exp);
          throw new ClassDirectException(exp);
        }

      }
      // sort data by issueDate descending
      if (!response.isEmpty()) {
        Collections.sort(response, new ReportComparator());
        responseResult = response.stream().distinct().limit(SURVEY_REPORTS_TO_BE_RETURNED).collect(Collectors.toList());
      }

    } catch (final InterruptedException exception) {
      LOGGER.error("Error executing JobService - getSurveyReports.", exception);
      throw new ClassDirectException(exception);
    } finally {
      executorService.shutdown();
    }
    return responseResult;
  }

  /**
   * Returns a {@link Callable} task to inject report details for a given job. Current supported
   * reports includes FAR, FSR, TM and ESP.
   *
   * @param job the primary identifier of job.
   * @return the {@link Callable} task that inject report details.
   */
  private Callable<List<ReportHDto>> reportResponse(final JobCDDto job) {
    final Callable<List<ReportHDto>> repResp = () -> {
      final List<ReportHDto> response = new ArrayList<>();
      try {

        // get job
        final JobCDDto jobObj = job;
        final List<ReportHDto> jobReports = jobObj.getReports();
        // reports in job taking too long to sent response to front load and it is recursive, so
        // removing reports from job object.
        jobObj.setReports(null);

        // add TM and ESP reports.
        if (jobObj.getHasESPReport()) {
          final ReportHDto report = new ReportHDto();
          report.setReportTypeDto(new ReportTypeHDto());
          report.getReportTypeDto().setName(REPORT_TYPE_ESP);
          report.setJobH(jobObj);
          report.setJob(new LinkResource(jobObj.getId()));
          response.add(report);
        }

        if (jobObj.getHasTMReport()) {
          final ReportHDto report = new ReportHDto();
          report.setReportTypeDto(new ReportTypeHDto());
          report.getReportTypeDto().setName(REPORT_TYPE_TM);
          report.setJobH(jobObj);
          report.setJob(new LinkResource(jobObj.getId()));

          // Retrieve list of TM attachments associated to the TM report.
          AttachmentsCDDto attachmentWrapper = getAttachments(jobObj.getId());
          List<SupplementaryInformationHDto> tmAttachments = getTMAttachments(attachmentWrapper);
          report.setAttachments(tmAttachments);
          // Set the report issue date as the latest attachment creation date.
          tmAttachments.stream().map(SupplementaryInformationHDto::getCreationDate).filter(date -> date != null)
              .max(Date::compareTo).ifPresent(date -> report.setIssueDate(date));
          response.add(report);
        }

        // Get list of report from MAST and filter only FAR and FSR with the latest version.
        List<ReportHDto> reportList = new ArrayList<>();
        if (jobReports != null) {
          final Comparator<ReportHDto> versionComparator = Comparator.comparing(ReportHDto::getReportVersion);

          final Map<String, ReportHDto> filteredReports = jobReports.stream().filter(report -> {
            Resources.inject(report);
            String reportType =
                Optional.ofNullable(report).map(ReportHDto::getReportTypeDto).map(ReportTypeHDto::getName).orElse("");
            return VALID_REPORT_TYPE_LIST.contains(reportType);
          }).collect(groupingBy(report -> report.getReportTypeDto().getName(),
              collectingAndThen(maxBy(versionComparator), Optional::get)));

          reportList.addAll(filteredReports.values());

          if (!reportList.isEmpty()) {
            response.addAll(reportList);
          }
        }

      } catch (final ClassDirectException exception) {
        LOGGER.error("Error executing JobService - getSurveyReports (inner).", exception);
      }
      return response;
    };
    return repResp;
  }

  /**
   * Returns survey reports resource for an asset.
   *
   * @param page the page number.
   * @param size result size.
   * @param assetId the primary key of the asset.
   * @return page resource for {@link ReportHDto}.
   *
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   *
   */
  @RestrictedAsset
  @Override
  public final PageResource<ReportHDto> getSurveyReports(final Integer page, final Integer size,
      @AssetID final Long assetId) throws ClassDirectException {
    PageResource<ReportHDto> pageResource = null;

    pageResource = PaginationUtil.paginate(getSurveyReports(assetId), page, size);

    return pageResource;
  }

  @RestrictedAsset
  @Override
  public final List<JobCDDto> getJobsByAssetIdCall(final Long assetId, final Boolean isSurveyReports)
      throws ClassDirectException {
    List<JobCDDto> filteredjobs = new ArrayList<>();

    try {

      // Get logged in userprofile.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      final UserProfiles user = token.getDetails();

      // Get roles for user
      final List<String> roles = SecurityUtils.getRoles();
      final boolean isClientUser = user.getClientCode() != null && roles.contains(CLIENT.toString());

      // Prepare query
      final JobWithFlagsQueryDto query = new JobWithFlagsQueryDto();
      query.setAssetId(Arrays.asList(assetId));
      query.setEspJob(true);
      query.setTmJob(true);
      if (isClientUser) {
        final AssetHDto asset = assetService.assetByCode(
            Optional.ofNullable(user).map(UserProfiles::getUserId).orElse(null), AssetCodeUtil.getMastCode(assetId));

        addClientRoleFilter(query, asset, user);
      }

      final Call<List<JobWithFlagsHDto>> jobsCall = jobServiceDelegate.getJobsWithFlags(query);
      final List<JobWithFlagsHDto> jobs = jobsCall.execute().body();

      if (jobs != null && !jobs.isEmpty()) {

        hydrateJobList(jobs);

        boolean isSurveyReportsFilter = Optional.ofNullable(isSurveyReports).orElse(false);

        final List<Callable<JobCDDto>> callableJobTaskList = jobs.parallelStream().map(jobsFromMast -> {
          final Callable<JobCDDto> callableJobTask = () -> {

            JobCDDto job = setJob(jobsFromMast);
            try {
              setServicesReportsForJob(job, isSurveyReportsFilter);
            } catch (final Exception e) {
              LOGGER.error("Error executing JobService - getJobsByAssetIdCall---while setJob", e);

            }
            if (job.getIsFARorFSRReportType() == null || !job.getIsFARorFSRReportType()) {
              job = null;
            } else {
              if (job.getIsMMS()) {
                // filter for migrated MMS Jobs.
                if (job.getIsMigrated()) {
                  job = null;
                } else {
                  if (!filterMMSJobsByUserRole(user, roles, job)) {
                    job = null;
                  }
                }
              }
            }
            return job;
          };
          return callableJobTask;
        }).collect(Collectors.toList());


        final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
        final long startTime = System.currentTimeMillis();
        try {
          LOGGER.info("Invoking all tasks for jobs");
          final List<Future<JobCDDto>> futureListCallable = executor.invokeAll(callableJobTaskList);
          futureListCallable.forEach(future -> {
            try {
              if (future.get() != null) {
                filteredjobs.add(future.get());
              }
            } catch (final InterruptedException | ExecutionException exception) {
              LOGGER.error("Error in retrieving adding job task. " + exception.getMessage());
            }
          });
        } catch (final InterruptedException interruptedException) {
          LOGGER.error(interruptedException.getMessage());
        } finally {
          executor.shutdown();
          LOGGER.debug("Finish filter jobs.| Elapsed Time (ms): {}", System.currentTimeMillis() - startTime);
        }
      }

    } catch (final IOException ioException) {
      LOGGER.error("Error executing JobService - getJobsByAssetIdCall.", ioException);
      throw new ClassDirectException(ioException);
    }
    return filteredjobs;
  }

  /**
   * Prepares jobs with flags query with client code and client role filters.
   *
   * @param query the jobs with flags query to be updated.
   * @param asset the asset object.
   * @param user the user object.
   * @return query.
   */
  private JobWithFlagsQueryDto addClientRoleFilter(final JobWithFlagsQueryDto query, final AssetHDto asset,
      final UserProfiles user) {
    final Long userClientCode = Long.valueOf(user.getClientCode());

    String clientCode = null;
    final Optional<List<CustomerLinkHDto>> custListOpt = Optional.ofNullable(asset).map(AssetHDto::getCustomersH);
    if (custListOpt.isPresent()) {
      clientCode = custListOpt.get().parallelStream()
          .filter(cusDto -> PartyRole.DOC_COMPANY.getValue().equals(cusDto.getPartyRoles().getId()))
          .map(CustomerLinkHDto::getParty).map(CustomerLinkDto::getCustomer).map(PartyDto::getImoNumber)
          .filter(Objects::nonNull).findFirst().orElse(null);
    }
    if (clientCode != null) {
      query.setAssetPartyRoleId(PartyRole.DOC_COMPANY.getValue());
      query.setAssetPartyId(userClientCode);
    }
    return query;
  }

  /**
   * Returns the job's lead surveyor information if there is lead surveyor in employee list.
   *
   * @param empList the empList for a given job.
   * @return the lead surveyor.
   */
  private EmployeeDto getLeadSurveyorForJob(final List<EmployeeLinkDto> empList) {
    EmployeeDto leadSurveyor = null;
    final EmployeeLinkDto employeeLinkDto = Optional.ofNullable(empList).orElse(new ArrayList<>(0)).stream()
        .filter(employee -> employee.getEmployeeRole().getId().equals(EmployeeRole.LEAD_SURVEYOR.getValue()))
        .findFirst().orElse(null);
    if (employeeLinkDto != null) {
      final LrEmployeeDto empLead = employeeReferenceService.getEmployee(employeeLinkDto.getLrEmployee().getId());
      if (!StringUtils.isEmpty(empLead)) {
        final LrEmployeeDto empLeadSurveyor = empLead;
        leadSurveyor = new EmployeeDto();
        leadSurveyor.setId(empLeadSurveyor.getId());
        leadSurveyor.setName(empLeadSurveyor.getName());
      }
    }
    return leadSurveyor;
  }

  @Override
  public final JobCDDto getJob(final Long jobId) throws ClassDirectException {
    final JobWithFlagsQueryDto query = new JobWithFlagsQueryDto();
    JobCDDto job = null;
    try {

      // filter to display non migrated MMS jobs
      // get roles for user
      final List<String> roles = SecurityUtils.getRoles();

      // get logged in userprofile.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      final UserProfiles user = token.getDetails();

      final boolean isClientUser = user.getClientCode() != null && roles.contains(CLIENT.toString());

      query.setSearch(jobId);
      query.setEspJob(true);
      query.setTmJob(true);

      final Call<List<JobWithFlagsHDto>> jobDtofromMast = jobServiceDelegate.getJobsWithFlags(query);

      final List<JobWithFlagsHDto> jobs = jobDtofromMast.execute().body();

      if (jobs != null && !jobs.isEmpty()) {

        JobWithFlagsHDto jobMast = jobs.get(0);
        Resources.inject(jobMast);

        if (isClientUser) {
          final AssetHDto asset =
              assetService.assetByCode(Optional.ofNullable(user).map(UserProfiles::getUserId).orElse(null),
                  AssetCodeUtil.getMastCode(jobMast.getAsset().getId()));

          addClientRoleFilter(query, asset, user);
          final List<JobWithFlagsHDto> jobsForClient = jobServiceDelegate.getJobsWithFlags(query).execute().body();
          if (jobsForClient != null && !jobsForClient.isEmpty()) {
            jobMast = jobsForClient.get(0);
          }

        }
        if (jobMast != null) {

          job = setJob(jobMast);
          try {
            setServicesReportsForJob(job, false);
          } catch (final Exception e) {
            LOGGER.error("Error executing JobService - getJobsByAssetIdCall---while setJob", e);

          }
          if (job.getIsFARorFSRReportType() == null || !job.getIsFARorFSRReportType()) {
            job = null;
          } else {
            if (job.getIsMMS()) {
              // filter for migrated MMS Jobs.
              if (job.getIsMigrated()) {
                job = null;
              } else {
                if (!filterMMSJobsByUserRole(user, roles, job)) {
                  job = null;
                }
              }
            }
          }
        }
      }
    } catch (final IOException ioException) {
      LOGGER.error("Error executing JobService - getjob.", ioException);
    }

    return job;
  }

  /**
   * Returns hydrated job list with resource injection.
   *
   * @param jobs the job list.
   */
  private void hydrateJobList(final List<JobWithFlagsHDto> jobs) {
    final long startTime = System.currentTimeMillis();
    final List<Callable<Void>> callables = new ArrayList<>();
    jobs.forEach(jobHdto -> callables.add(() -> {
      Resources.inject(jobHdto);
      return null;
    }));
    final ExecutorService executorService = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
    try {
      executorService.invokeAll(callables);
    } catch (InterruptedException exception) {
      LOGGER.error("Error while hydrating: {} ", exception);
    } finally {
      executorService.shutdown();
      LOGGER.debug("Finish hydrated job list of {}. | Elapsed Time (ms): {}", jobs.size(),
          System.currentTimeMillis() - startTime);
    }
  }

  /**
   * Sets services, lead surveyor, report information and more to job.
   *
   * @param jobWithFlagsDto the job model object.
   * @return the Job information.
   */
  private JobCDDto setJob(final JobWithFlagsHDto jobWithFlagsDto) {
    JobCDDto job = null;
    if (jobWithFlagsDto != null) {
      job = new JobCDDto();
      job.setId(jobWithFlagsDto.getId());
      job.setJobNumber(jobWithFlagsDto.getJobNumber());
      if (jobWithFlagsDto.getLocationDto() != null) {
        job.setLocation(jobWithFlagsDto.getLocationDto().getName());
      }
      job.setHasTMReport(jobWithFlagsDto.getFlagTM());
      job.setHasESPReport(jobWithFlagsDto.getFlagESP());
      if (jobWithFlagsDto.getLastVisitDate() != null) {
        job.setLastVisitDate(DateFormatUtils.format(jobWithFlagsDto.getLastVisitDate(), "yyyy-MM-dd"));
      }
      if (jobWithFlagsDto.getFirstVisitDate() != null) {
        job.setFirstVisitDate(DateFormatUtils.format(jobWithFlagsDto.getFirstVisitDate(), "yyyy-MM-dd"));
      }
      job.setLeadSurveyor(getLeadSurveyorForJob(jobWithFlagsDto.getEmployees()));
      job.setAssetId(jobWithFlagsDto.getAsset().getId());
      if (jobWithFlagsDto.getJobCategory() != null
          && jobWithFlagsDto.getJobCategory().getId().equals(JobCategory.MMS.getValue())) {
        job.setIsMMS(true);
      } else {
        job.setIsMMS(false);
      }
      job.setIsMigrated(jobWithFlagsDto.getMigrated());
    }
    return job;
  }

  @Override
  public final List<JobWithFlagsHDto> getCompletedJobs(final String userId, final Date fromDate, final Date toDate)
      throws ClassDirectException {
    if (fromDate == null || toDate == null) {
      throw new IllegalArgumentException();
    }
    List<JobWithFlagsHDto> jobs = null;

    try {
      List<Long> jobStatusIds = new ArrayList<Long>();
      jobStatusIds.add(JobStatus.CLOSED.getValue());

      AbstractQueryDto queryBuilder = closedJobsWithRange(fromDate, toDate, jobStatusIds);

      jobs = jobServiceDelegate.getJobsByAbstarctQuery(queryBuilder).execute().body();

      if (jobs != null && !jobs.isEmpty()) {
        final LocalDate fromLocalDate = DateUtils.getLocalDateFromDate(fromDate);
        final LocalDate toLocalDate = DateUtils.getLocalDateFromDate(toDate);

        jobs = jobs.stream().filter(job -> {
          if (job.getCompletedOn() != null) {
            final LocalDate jobDate = DateUtils.getLocalDateFromDate(job.getCompletedOn());
            LOGGER.trace("job id {} and job completed on {}, is valid in between dates {} {} notification result {}",
                job.getId(), job.getCompletedOn(), fromLocalDate, toLocalDate,
                (jobDate.equals(fromLocalDate) || jobDate.isAfter(fromLocalDate))
                    && (jobDate.isEqual(toLocalDate) || jobDate.isBefore(toLocalDate)));

            return (jobDate.equals(fromLocalDate) || jobDate.isAfter(fromLocalDate))
                && (jobDate.isEqual(toLocalDate) || jobDate.isBefore(toLocalDate));
          }
          return false;
        }).collect(Collectors.toList());
      }

    } catch (final IOException exception) {
      LOGGER.error("Error executing JobService - getCompletedJobs.", exception);
      throw new ClassDirectException(exception);
    }
    return jobs;
  }

  @Override
  public final List<JobWithFlagsHDto> filterJobsBasedOnUserRole(final String userId, final Long assetId,
      final List<JobWithFlagsHDto> jobsList) throws ClassDirectException {

    List<JobWithFlagsHDto> jobs = Collections.synchronizedList(new ArrayList<JobWithFlagsHDto>());
    try {
      // get logged in userprofile.
      final UserProfiles user = userProfileService.getUser(userId);

      final List<String> roles = SecurityUtils.getRoles();

      final boolean isClientUser = user.getClientCode() != null && roles.contains(CLIENT.toString());

      jobsList.forEach(job -> {
        try {

          if (isClientUser) {
            final AssetHDto asset =
                assetService.assetByCode(Optional.ofNullable(user).map(UserProfiles::getUserId).orElse(null),
                    AssetCodeUtil.getMastCode(job.getAsset().getId()));

            final JobWithFlagsQueryDto query = new JobWithFlagsQueryDto();
            query.setSearch(job.getId());
            addClientRoleFilter(query, asset, user);
            final List<JobWithFlagsHDto> jobsForClient = jobServiceDelegate.getJobsWithFlags(query).execute().body();
            if (jobsForClient != null && !jobsForClient.isEmpty()) {
              job = jobsForClient.get(0);
            }

          }
          if (job != null) {
            JobCDDto jobCDDto = setJob(job);
            jobCDDto = setServicesReportsForJob(jobCDDto, false);
            // filter for MMS Jobs.
            if (job.getJobCategory() != null && job.getJobCategory().getId().equals(JobCategory.MMS.getValue())) {
              // filter for migrated MMS Jobs.
              if (job.getMigrated()) {
                job = null;
              } else {
                // display MMS jobs for LR_ADMIN, LR_SUPPORT and LR_INTERNAL role, other roles have
                // filtering
                if (!filterMMSJobsByUserRole(user, roles, jobCDDto)) {
                  job = null;
                }
              }
            }

            Optional.ofNullable(job).ifPresent(jobs::add);
          }
        } catch (final Exception exception) {
          LOGGER.error("Error occured getting services for job {}", exception);
        }
      });
    } catch (final ClassDirectException exception) {
      LOGGER.error("Error executing JobService - getCompletedJobs.", exception);
      throw new ClassDirectException(exception);
    }
    return jobs;
  }

  /**
   * Filter job by user role and mms visibility.
   * <ul>
   * <li>For LR_ADMIN, LR_INTERNAL and LR_SUPPORT mms job is visible.</li>
   * <li>For Client role job visibility checking is from jobs with flags query.</li>
   * <li>For Flag mms jobs visible only for flag user praposed flag match with current flag or
   * praposed flag should be null.</li>
   * <li>Other user roles SHIP_BUILDER, EOR should not see mms jobs.</li>
   * </ul>
   *
   * @param user the user object.
   * @param roles the user roles.
   * @param jobCDDto the job to be updated.
   * @return true if job is visible for user role.
   */
  private boolean filterMMSJobsByUserRole(final UserProfiles user, final List<String> roles, final JobCDDto jobCDDto) {
    boolean isJobVisible = false;
    // check user is flag user.
    final boolean isFlagUser = user.getFlagCode() != null && roles.contains(FLAG.toString());

    final boolean isClientUser = user.getClientCode() != null && roles.contains(CLIENT.toString());

    final boolean isInternalUser = SecurityUtils.isInternalUser();
    if (isInternalUser || isClientUser) {
      isJobVisible = true;
    } else if (isFlagUser) {
      // get user flag
      final List<FlagStateHDto> flags = assetReferenceService.getFlagStates();
      Long flagId = flags.stream().filter(flag -> flag.getFlagCode().equalsIgnoreCase(user.getFlagCode())).findFirst()
          .get().getId();

      if (jobCDDto.getCurrentFlag() != null && jobCDDto.getCurrentFlag().equals(flagId)
          && (jobCDDto.getProposedFlag() == null
              || jobCDDto.getProposedFlag() != null && flagId.equals(jobCDDto.getProposedFlag()))) {
        isJobVisible = true;
      }
    } else {
      isJobVisible = false;
    }
    return isJobVisible;
  }

  @Override
  public final List<ReportCDDto> getReports(final Long jobId) {
    final List<ReportCDDto> reportList = new ArrayList<>();
    try {
      // get job esp and tm flag
      final JobCDDto jobObj = getJob(jobId);
      if (jobObj != null) {
        final List<ReportHDto> farfsrReports = getFilteredJobReports(jobObj);

        if (!farfsrReports.isEmpty()) {
          final List<Callable<ReportCDDto>> callableReportTaskList = farfsrReports.parallelStream().map(report -> {
            final Callable<ReportCDDto> callableReportTask = () -> {
              final ReportCDDto reportDto = new ReportCDDto();
              reportDto.setId(report.getId());
              reportDto.setReportType(report.getReportTypeDto().getName());
              reportDto.setReportVersion(report.getReportVersion());

              Optional.ofNullable(report.getJob()).ifPresent(reportJob -> {
                reportDto.setJobId(reportJob.getId());
              });

              return reportDto;
            };
            return callableReportTask;
          }).collect(Collectors.toList());

          final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
          try {
            LOGGER.info("Invoking all report task for the jobs - {}", jobObj.getId());
            final List<Future<ReportCDDto>> futureListCallable = executor.invokeAll(callableReportTaskList);
            futureListCallable.forEach(future -> {
              try {
                reportList.add(future.get());
              } catch (final InterruptedException | ExecutionException exception) {
                LOGGER.error("Error in retrieving adding report task. " + exception.getMessage());
              }
            });
          } catch (final InterruptedException interruptedException) {
            LOGGER.error(interruptedException.getMessage());
          } finally {
            executor.shutdown();
          }
        }

        // To check for FSR report
        if (jobObj.getHasESPReport()) {
          ReportCDDto fsrReport = reportList.stream().filter(report -> "FSR".equalsIgnoreCase(report.getReportType()))
              .findFirst().orElse(null);
          if (fsrReport != null) {
            final ReportCDDto report = new ReportCDDto();
            report.setReportType(REPORT_TYPE_ESP);
            report.setJobId(jobId);
            reportList.add(report);
          } else {
            LOGGER.warn("No FSR found for ESP generation.");
          }
        }

        AttachmentsCDDto attachmentWrapper = new AttachmentsCDDto();
        attachmentWrapper = getAttachments(jobObj.getId());

        if (jobObj.getHasTMReport()) {
          // get logged in userprofile.
          final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
          final UserProfiles user = token.getDetails();

          // check whether user is only eor user or eor with any other role
          final List<String> roles = new ArrayList<>();
          Optional.ofNullable(user.getRoles()).ifPresent(userRoles -> {
            roles.addAll(user.getRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList()));
          });

          boolean isEorRole = SecurityUtils.isEORUser(roles);

          if (isEorRole) {

            final AssetHDto asset =
                assetService.assetByCode(user.getUserId(), AssetCodeUtil.getMastCode(jobObj.getAssetId()));
            final UserEOR usereor = userProfileService.getEOR(user.getUserId(), asset.getImoNumber());

            // Is not an EOR asset or EOR asset with TM report access
            if (usereor == null || usereor.getAccessTmReport()) {
              final ReportCDDto report = new ReportCDDto();
              report.setReportType(REPORT_TYPE_TM);
              report.setJobId(jobId);
              report.setAttachments(getTMAttachments(attachmentWrapper));
              reportList.add(report);
            }
          } else {
            final ReportCDDto report = new ReportCDDto();
            report.setReportType(REPORT_TYPE_TM);
            report.setJobId(jobId);
            report.setAttachments(getTMAttachments(attachmentWrapper));
            reportList.add(report);
          }
        }

        List<SupplementaryInformationHDto> mmsAttachments = getMMSAttachments(attachmentWrapper);
        if (!mmsAttachments.isEmpty()) {
          final ReportCDDto report = new ReportCDDto();
          report.setReportType(REPORT_TYPE_MMS);
          report.setJobId(jobId);
          report.setAttachments(mmsAttachments);
          reportList.add(report);
        }
      }
    } catch (final ClassDirectException exception) {
      LOGGER.error("Error executing JobService - getReports.", exception);
    }
    return reportList;
  }

  /**
   * Returns the latest version of FAR and FSR reports if exists.
   *
   * @param jobObj the job detail.
   * @return the list of latest version of FAR and FSR reports.
   * @throws ClassDirectException if the MAST api to fetch reports fail, otherwise always success.
   */
  private List<ReportHDto> getFilteredJobReports(final JobCDDto jobObj) throws ClassDirectException {
    final List<ReportHDto> reportList = new ArrayList<>();

    try {
      // Get list of report from MAST and filter only FAR and FSR with the latest version.
      final List<ReportHDto> reportResponse = jobServiceDelegate.getReportsByJobId(jobObj.getId()).execute().body();

      if (reportResponse != null) {
        final Comparator<ReportHDto> versionComparator =
            Comparator.comparing(ReportHDto::getReportVersion, Comparator.nullsFirst(Comparator.naturalOrder()));

        final Map<String, ReportHDto> filteredReports = reportResponse.stream().filter(report -> {
          Resources.inject(report);
          report.setJobH(jobObj);
          String reportType =
              Optional.ofNullable(report).map(ReportHDto::getReportTypeDto).map(ReportTypeHDto::getName).orElse("");
          return VALID_REPORT_TYPE_LIST.contains(reportType);
        }).collect(groupingBy(report -> report.getReportTypeDto().getName(),
            collectingAndThen(maxBy(versionComparator), Optional::get)));

        reportList.addAll(filteredReports.values());
      } else {
        LOGGER.error("No reports found for given job - {}", jobObj.getId());
      }
    } catch (final IOException exception) {
      LOGGER.error("Error executing JobService - getFilteredJobReport.", exception);
      throw new ClassDirectException(exception);
    }
    return reportList;
  }


  /**
   * Returns TM report attachments for a job.
   *
   * @param attachmentWrapper list of attachments of the job.
   * @return the TM Attachments list of {@link SupplementaryInformationHDto}.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   */
  private List<SupplementaryInformationHDto> getTMAttachments(final AttachmentsCDDto attachmentWrapper)
      throws ClassDirectException {
    final List<SupplementaryInformationHDto> attachments = attachmentWrapper.getAttachments();
    final List<Long> tmReportCategoryIds = AttachmentCategory.getThicknessMeasurementCategories();
    final List<SupplementaryInformationHDto> tmAttachments =
        Optional.ofNullable(attachments).orElseGet(Collections::emptyList).stream()
            .filter(attachment -> tmReportCategoryIds.contains(attachment.getAttachmentCategory().getId()))
            .collect(Collectors.toList());

    return tmAttachments;
  }

  /**
   * Returns MMS report attachments for a job.
   *
   * @param attachmentWrapper list of attachments of the job.
   * @return the MMS Attachments list of {@link SupplementaryInformationHDto}.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   */
  private List<SupplementaryInformationHDto> getMMSAttachments(final AttachmentsCDDto attachmentWrapper)
      throws ClassDirectException {
    final List<SupplementaryInformationHDto> attachments = attachmentWrapper.getAttachments();
    final List<Long> mmsReportCategoryIds = AttachmentCategoryEnum.getMMSCategories();
    final List<SupplementaryInformationHDto> mmsAttachments =
        Optional.ofNullable(attachments).orElseGet(Collections::emptyList).stream()
            .filter(attachment -> mmsReportCategoryIds.contains(attachment.getAttachmentCategory().getId()))
            .collect(Collectors.toList());

    return mmsAttachments;
  }

  /**
   * Sets list of services and reports for a job.
   *
   * @param jobCDDto the job.
   * @param isSurveyReports the flag to identity api is calling from survey reports.
   * @return the list of services.
   * @throws IOException if Mast API fail otherwise always success.
   * @throws JsonParseException if there is error while converting services using object mapper
   *         otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   */
  private JobCDDto setServicesReportsForJob(final JobCDDto jobCDDto, final boolean isSurveyReports)
      throws IOException, JsonParseException, ClassDirectException {

    JobCDDto job = jobCDDto;
    final List<JobSurveyDto> services = new ArrayList<JobSurveyDto>();
    List<ReportHDto> reports = getFilteredJobReports(job);
    Optional.ofNullable(reports).ifPresent(reportResponse -> {
      final List<Callable<List<JobSurveyDto>>> tasks = reportResponse.parallelStream()
          .filter(reportFilter -> reportFilter.getReportType() != null
              && (reportFilter.getReportType().getId().equals(ReportType.FAR.value())
                  || reportFilter.getReportType().getId().equals(ReportType.FSR.value())))
          .map(report -> {
            job.setIsFARorFSRReportType(true);
            final Callable<List<JobSurveyDto>> task = () -> {
              final List<JobSurveyDto> serviceDtos = new ArrayList<JobSurveyDto>();
              final ObjectMapper mapper = new ObjectMapper();
              final String jsonContent = report.getContent();
              if (!StringUtils.isEmpty(jsonContent) && !jsonContent.equals("null")) {
                final JSONObject obj = new JSONObject(jsonContent);
                final JSONArray surveyObj = obj.getJSONArray("surveys");

                if (!obj.isNull("proposedFlag")) {
                  final JSONObject praposedFlagObj = obj.getJSONObject("proposedFlag");
                  final Long proposedFlag = mapper.readValue(praposedFlagObj.toString(), LinkResource.class).getId();
                  job.setProposedFlag(proposedFlag);
                }

                if (!obj.isNull("currentFlag")) {
                  final JSONObject currentFlagObj = obj.getJSONObject("currentFlag");
                  final Long currentFlag = mapper.readValue(currentFlagObj.toString(), LinkResource.class).getId();
                  job.setCurrentFlag(currentFlag);
                }
                for (int i = 0, size = surveyObj.length(); i < size; i++) {
                  final JSONObject objectInArray = surveyObj.getJSONObject(i);

                  final JSONObject objSurveyStatus = objectInArray.getJSONObject("surveyStatus");
                  final long surveyStatusId = mapper.readValue(objSurveyStatus.toString(), LinkResource.class).getId();
                  ServiceCreditStatusHDto serviceCreditStatus =
                      serviceReferenceService.getServiceCreditStatus(surveyStatusId);
                  final JobSurveyDto service = new JobSurveyDto();
                  service.setStatus(serviceCreditStatus.getName());
                  // LRCD-3289 service name should be from service objet not from service catalogue
                  // name.
                  service.setName(objectInArray.getString("name"));
                  service.setId(objectInArray.getLong("id"));
                  serviceDtos.add(service);
                }
              }
              return serviceDtos;
            };
            return task;
          }).collect(Collectors.toList());

      final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());

      try {
        final List<Future<List<JobSurveyDto>>> callableFutureList = executor.invokeAll(tasks);
        callableFutureList.forEach(future -> {
          try {
            services.addAll(future.get());
          } catch (final InterruptedException | ExecutionException exception) {
            LOGGER.error("Error in retrieving adding services for job. " + exception.getMessage());
          }
        });
      } catch (final InterruptedException interruptedException) {
        LOGGER.error(interruptedException.getMessage());
      } finally {
        executor.shutdown();
      }
    });

    List<JobSurveyDto> distinctServices =
        services.stream().filter(distinctByKey(JobSurveyDto::getId)).collect(Collectors.toList());
    job.setServices(distinctServices);
    if (isSurveyReports) {
      job.setReports(reports);
    }

    return job;
  }


  /**
   * Sub class for comparator.
   *
   * @author SBollu
   *
   */
  static class ReportComparator implements Comparator<ReportHDto>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3616861287199577556L;

    @Override
    public int compare(final ReportHDto object1, final ReportHDto object2) {
      final Optional<Date> date1 = Optional.ofNullable(object1.getIssueDate());
      final Optional<Date> date2 = Optional.ofNullable(object2.getIssueDate());
      if (date1.isPresent() && date2.isPresent()) {
        return date2.get().compareTo(date1.get());
      } else if (date1.isPresent() && !date2.isPresent()) {
        return -1;
      } else if (!date1.isPresent() && date2.isPresent()) {
        return 1;
      } else {
        return 0;
      }
    }

  }

  @Override
  public final AttachmentsCDDto getAttachments(final Long jobId) throws ClassDirectException {
    final AttachmentsCDDto attachments = new AttachmentsCDDto();

    try {
      // get logged in userprofile.
      final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
      final String userId = token.getDetails().getUserId();
      final List<SupplementaryInformationHDto> supplementaryInfoDtos = new ArrayList<>();

      Optional.ofNullable(attachmentService.getAttachmentsByJobId(jobId).execute().body()).ifPresent(attachment -> {

        final List<Callable<SupplementaryInformationHDto>> callableSuppTaskList =
            attachment.stream().map(supplement -> {
              // Create a Callable task for a Supplementary task.
              final Callable<SupplementaryInformationHDto> callableSupptask = () -> {
                Resources.inject(supplement);
                final Integer attachNodeId =
                    Optional.ofNullable(supplement.getAttachmentUrl()).map(url -> Integer.valueOf(url)).orElse(null);
                final Integer attachVersion = Optional.ofNullable(supplement.getDocumentVersion())
                    .map(version -> version.intValue()).orElse(null);
                try {
                  supplement.setToken(FileTokenGenerator.generateCS10Token(attachNodeId, attachVersion, userId));
                  LOGGER.info("Job Attachment Token : {}", supplement.getToken());
                } catch (final ClassDirectException exception) {
                  LOGGER.error("Error occured while generating Job Attachment Token with node id : {}, version id : {}",
                      attachNodeId, attachVersion, exception);
                }
                return supplement;
              };
              return callableSupptask;
            }).collect(Collectors.toList());

        final ExecutorService executor = ExecutorUtils.newFixedThreadPool(this.getClass().getSimpleName());
        try {
          LOGGER.info("{} Invoking all supplementary task to get attachments.", this.getClass().getSimpleName());
          final List<Future<SupplementaryInformationHDto>> futureListCallable =
              executor.invokeAll(callableSuppTaskList);
          futureListCallable.forEach(future -> {
            try {
              supplementaryInfoDtos.add(future.get());
              attachments.setAttachments(supplementaryInfoDtos);
            } catch (final InterruptedException | ExecutionException exception) {
              LOGGER.error("Error in retrieving adding supplementary task. " + exception.getMessage());
            }
          });
        } catch (final InterruptedException interruptedException) {
          LOGGER.error(interruptedException.getMessage());
        } finally {
          executor.shutdown();
        }
      });
    } catch (final IOException exception) {
      LOGGER.error("Error in retrieving getAttachments services for job. " + exception.getMessage());
      throw new ClassDirectException(exception);
    }
    return attachments;
  }

  /**
   * Get distinct elements based on id.
   *
   * @param keyExtractor keyExtractor.
   * @param <T> predicate.
   * @return Predicate <T>.
   */
  private static <T> Predicate<T> distinctByKey(final Function<? super T, ?> keyExtractor) {
    Set<Object> serviceSet = ConcurrentHashMap.newKeySet();
    return t -> serviceSet.add(keyExtractor.apply(t));
  }
}
