package com.baesystems.ai.lr.cd.service.security.impl;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AvailabilityDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.SystemMaintenance;
import com.baesystems.ai.lr.cd.service.security.AvailabilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link AvailabilityService}.
 *
 * @author Faizal Sidek
 */
@Service("AvailabilityService")
public class AvailabilityServiceImpl implements AvailabilityService {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AvailabilityService.class);

  /**
   * Flag for system availability.
   */
  private boolean isAvailable = true;

  /**
   * Injected DAO.
   */
  @Autowired
  private AvailabilityDao availabilityDao;

  @Override
  public final boolean isServiceAvailable() {
    return isAvailable;
  }

  /**
   * Periodically check for service availablitity.
   */
  @Override
  @Scheduled(fixedDelayString = "${availability.check.delay:60000}")
  public final void checkForServiceAvailability() {
    final SystemMaintenance systemMaintenance = availabilityDao.getActiveSystemMaintenance();
    isAvailable = systemMaintenance == null;
  }
}
