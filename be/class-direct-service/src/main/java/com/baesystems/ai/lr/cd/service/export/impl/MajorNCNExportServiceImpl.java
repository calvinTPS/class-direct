package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.MajorNCNService;
import com.baesystems.ai.lr.cd.service.export.MajorNCNExportService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;

/**
 * Provides the asset note export service for generating, and downloading major non confirmative
 * notes report.
 *
 * @author syalavarthi.
 */
@Service(value = "MajorNCNExportService")
@Loggable(Loggable.DEBUG)
public class MajorNCNExportServiceImpl extends BasePdfExport<ExportPdfDto> implements MajorNCNExportService {
  /**
   * The logger instantiated for {@link MajorNCNExportServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MajorNCNExportServiceImpl.class);

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link MajorNCNService} from Spring context.
   */
  @Autowired
  private MajorNCNService majorNCNService;

  /**
   * The report name prefix for mncn.
   */
  private static final String PDF_PREFIX_MNCN = "MNCN";

  /**
   * The pug template file name for mncn.
   */
  private static final String TEMPLATE_MNCN = "mncn_listing_export_template.jade";

  /**
   * The directory name in Amazon S3 where the generated report to be uploaded. This variable in
   * only use at {@link BasePdfExport}. <br>
   * For more information see {@link MajorNCNExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "codicil_export";

  /**
   * This field is use as variable in pug template that store list of major non confirmative note
   * object.
   */
  private static final String MNCNS = "mncns";

  /**
   * This field is use as variable in pug template that store asset object.
   */
  private static final String ASSET = "asset";

  /**
   * This field is use {@link BasePdfExport} as a part of file name generation.
   */
  private static final String FILE_SECTION = "fileSection";

  /**
   * This field is use to check if the pdf is single page or multi page.
   */
  private static final String ONLY_ONE_PAGE = "onlyOnePage";

  /**
   * The file generation working directory.
   */
  @Value("${codicil.export.temp.dir}")
  private String tempDir;

  @Override
  protected final String generateFile(final Map<String, Object> model, final ExportPdfDto query,
      final UserProfiles user, final String workingDirMNCN, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Retrieving MNCN object...");

      // create file section name
      model.put(FILE_SECTION, query.getCode());

      // get asset information
      final AssetHDto asset = assetService.assetByCode(user.getUserId(), query.getCode());
      if (asset == null) {
        throw new ClassDirectException("Asset not found.");
      } else {
        model.put(ASSET, asset);
      }

      // get MNCN by asset id.
      final PageResource<MajorNCNHDto> mncnResource = majorNCNService.getMajorNCNs(null, null, asset.getId());
      List<MajorNCNHDto> mncns = mncnResource.getContent();

      // filter mncn by ids
      if (mncns != null && !mncns.isEmpty()) {
        if (query.getItemsToExport() != null && !query.getItemsToExport().isEmpty()) {
          mncns = mncns.stream().filter(item -> query.getItemsToExport().contains(item.getId()))
              .collect(Collectors.toList());
        }

      }

      model.put(MNCNS, mncns);

      String filename = generatePdf(PDF_PREFIX_MNCN, query.getCode(), current, TEMPLATE_MNCN, workingDirMNCN, model);

      int pageCountForMncn = ServiceUtils.getPageCount(workingDirMNCN, filename);
      // if the pdf has only one page then we will recall the page to put ONLY_ONE_PAGE flag in the
      // model
      if (pageCountForMncn == 1) {
        model.put(ONLY_ONE_PAGE, "true");
        filename = generatePdf(PDF_PREFIX_MNCN, query.getCode(), current, TEMPLATE_MNCN, workingDirMNCN, model);
      }

      return filename;
    } catch (JadeException | IOException | ParserConfigurationException | SAXException | DocumentException exception) {
      throw new ClassDirectException(exception);
    }
  }

  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return this.tempDir;
  }

}
