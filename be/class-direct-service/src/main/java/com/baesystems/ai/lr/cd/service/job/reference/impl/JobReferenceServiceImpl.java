package com.baesystems.ai.lr.cd.service.job.reference.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.cd.be.domain.annotation.ResourceProvider;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.JobReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemTypeHDto;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import retrofit2.Call;

/**
 * Provides services to get list of job status, single job status,list of work item types, single
 * work item, list of report types, single report type and all the reference data associated with
 * job.
 *
 * @author yng
 * @author syalavarthi 23-05-2017.
 *
 */
@Service("jobReferenceService")
public class JobReferenceServiceImpl implements JobReferenceService, InitializingBean {
  /**
   * The logger for JobReferenceServiceImpl {@value #LOGGER}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(JobReferenceServiceImpl.class);

  /**
   * The log message display in logs if element can't find by id {@value #LOG_MESSAGE}.
   */
  private static final String LOG_MESSAGE = "Can't find any element matched id {}.";

  /**
   * The {@link JobReferenceRetrofitService} from spring context.
   */
  @Autowired
  private JobReferenceRetrofitService jobReferenceDelegate;

  /**
   * Returns job location for a given location id.
   *
   * @param locationId the location id.
   * @return job location.
   */
  @ResourceProvider
  @Override
  public final LocationHDto getJobLocation(final Long locationId) {
    LocationHDto locationHDto = null;
    final List<LocationHDto> list = self.getJobLocations();
    if (list != null && !list.isEmpty()) {
      locationHDto = list.stream().filter(location -> location.getId().equals(locationId)).findFirst().get();
    }
    return locationHDto;
  }

  /**
   * Returns job locations.
   *
   * @return list of job locations.
   */
  @ResourceProvider
  @Override
  public final List<LocationHDto> getJobLocations() {
    List<LocationHDto> locations = new ArrayList<>();
    try {
      locations = jobReferenceDelegate.getLocations().execute().body();
    } catch (IOException exception) {
      LOGGER.debug("Unable to retrieve job locations: " + exception.getMessage());
    }
    return locations;
  }

  /**
   * Fetches job statuses.
   *
   * @return list of job status.
   */
  @ResourceProvider
  @Override
  public final List<JobStatusHDto> getJobStatuses() {
    List<JobStatusHDto> jobStatuses = new ArrayList<>();

    try {
      final Call<List<JobStatusHDto>> caller = jobReferenceDelegate.getJobStatuses();
      jobStatuses = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }

    return jobStatuses;
  }

  /**
   * Gets job status by id.
   *
   * @param id the job status id.
   * @return job status.
   *
   */
  @ResourceProvider
  @Override
  public final JobStatusHDto getJobStatus(final Long id) {
    JobStatusHDto jobStatus = null;

    try {
      final List<JobStatusHDto> jobStatuses = getJobStatuses();
      jobStatus = jobStatuses.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, id);
    }

    return jobStatus;
  }

  /**
   * Fetches report types.
   *
   * @return list of report type.
   */
  @ResourceProvider
  @Override
  public final List<ReportTypeHDto> getReportTypes() {
    List<ReportTypeHDto> reportTypes = new ArrayList<>();

    try {
      final Call<List<ReportTypeHDto>> caller = jobReferenceDelegate.getReportTypes();
      reportTypes = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }

    return reportTypes;
  }

  /**
   * Gets report type by id.
   *
   * @param id the report type id.
   * @return report type.
   *
   */
  @ResourceProvider
  @Override
  public final ReportTypeHDto getReportType(final Long id) {
    ReportTypeHDto reportType = null;

    try {
      final List<ReportTypeHDto> reportTypes = getReportTypes();
      reportType = reportTypes.stream().filter(type -> type.getId().equals(id)).findFirst().get();
    } catch (final NoSuchElementException noElementFound) {
      LOGGER.debug(LOG_MESSAGE, id);
    }

    return reportType;
  }

  @ResourceProvider
  @Override
  public final List<WorkItemTypeHDto> getWorkItemTypes() {

    List<WorkItemTypeHDto> workItemTypes = new ArrayList<>();

    try {
      final Call<List<WorkItemTypeHDto>> caller = jobReferenceDelegate.getWorkItemTypes();
      workItemTypes = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return workItemTypes;
  }

  /**
   * Gets work item type by id.
   *
   * @param id the workItem type.
   * @return work item type.
   *
   */
  @ResourceProvider
  @Override
  public final WorkItemTypeHDto getWorkItemType(final Long id) {
    WorkItemTypeHDto workItemType = null;

    final List<WorkItemTypeHDto> workItemTypes = self.getWorkItemTypes();
    final Optional<WorkItemTypeHDto> workItemTypeOpt =
        workItemTypes.stream().filter(workItemTypeObj -> workItemTypeObj.getId().equals(id)).findFirst();

    if (workItemTypeOpt.isPresent()) {
      workItemType = workItemTypeOpt.get();
    }

    return workItemType;
  }

  @ResourceProvider
  @Override
  public final List<JobResolutionStatusHDto> getResolutionStatuses() {

    List<JobResolutionStatusHDto> resolutionStatuses = new ArrayList<>();

    try {
      final Call<List<JobResolutionStatusHDto>> caller = jobReferenceDelegate.getResolutionStatuses();
      resolutionStatuses = caller.execute().body();
    } catch (final IOException ioe) {
      LOGGER.error("Execution failed.", ioe);
    }
    return resolutionStatuses;
  }

  /**
   * Gets resolution status by id.
   *
   * @param id the resolution status id.
   * @return resolution status.
   *
   */
  @ResourceProvider
  @Override
  public final JobResolutionStatusHDto getResolutionStatus(final Long id) {
    JobResolutionStatusHDto resolutionStatus = null;

    final List<JobResolutionStatusHDto> resolutionStatuses = self.getResolutionStatuses();
    final Optional<JobResolutionStatusHDto> resolutionStatusOpt =
        resolutionStatuses.stream().filter(resolutionStatusObj -> resolutionStatusObj.getId().equals(id)).findFirst();

    if (resolutionStatusOpt.isPresent()) {
      resolutionStatus = resolutionStatusOpt.get();
    }

    return resolutionStatus;
  }

  /**
   * Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * This service but spring-awared.
   *
   */
  @SuppressFBWarnings
  private JobReferenceService self;

  @Override
  public final void afterPropertiesSet() throws Exception {
    self = context.getBean(JobReferenceService.class);
  }
}
