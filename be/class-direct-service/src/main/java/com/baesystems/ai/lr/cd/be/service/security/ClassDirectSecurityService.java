package com.baesystems.ai.lr.cd.be.service.security;

import com.baesystems.ai.lr.cd.be.domain.dto.security.bean.CookieBean;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;

/**
 * Provides class-direct security by implementing MD5 encryption and services to get invalid input
 * parameters and cookie configuration.
 *
 * @author RKaneysan
 * @author syalavarthi 21-06-2017.
 */
public interface ClassDirectSecurityService {

  /**
   * Gets string invalid parameters from configuration file.
   *
   * @return invalid input parameters error message.
   */
  String getInvalidInputParameters();

  /**
   * Gets cookie bean container for cookie configuration.
   *
   * @return cookie bean container for cookie configuration.
   */
  CookieBean getCookieConfig();

  /**
   * Authenticates and applies MD5 encryption with given parameters (param imoNumber, param time, param sharedKey) and
   * authenticate hash by comparing hash input value with newly created hash using input parameters
   * and respective shared key.
   *
   * @param imoNumber The given imo number.
   * @param time The request time stamp.
   * @param hash The shared key value.
   * @throws ClassDirectSecurityException if the given has/shared key does not match with the newly generated hash key.
   */
  void authenticateHash(final String imoNumber, final String time, final String hash)
      throws ClassDirectSecurityException;

  /**
   * Validates Time if it is in correct timestamp format and not exceeded Expiration Period (as per
   * System Configuration).
   *
   * @param time the time input parameter.
   * @throws ClassDirectSecurityException if expiration period exceeds or error in date conversions.
   */
  void validateTime(final String time) throws ClassDirectSecurityException;
}
