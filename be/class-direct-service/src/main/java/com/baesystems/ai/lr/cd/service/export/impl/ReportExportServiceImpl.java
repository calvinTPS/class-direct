package com.baesystems.ai.lr.cd.service.export.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetItemHierarchyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportContentHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.SurveyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.ReportUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.export.ReportExportService;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.EmployeeRole;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.enums.WorkItemType;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcabi.aspects.Loggable;
import com.lowagie.text.DocumentException;

import de.neuland.jade4j.exceptions.JadeException;
import lombok.Getter;
import lombok.Setter;

/**
 * Provides the report export service for generating, and downloading FAR/FSR report. The main
 * report and appendices for FSR/FAR is generated and combined to single PDF as final report.
 *
 * @author yng.
 * @author syalavarthi.
 */
@Service(value = "ReportExportService")
@Loggable(Loggable.DEBUG)
public class ReportExportServiceImpl extends BasePdfExport<ReportExportDto> implements ReportExportService {
  /**
   * The logger instantiated for log printing.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ReportExportServiceImpl.class);

  /**
   * The {@link JobRetrofitService} from Spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The {@link PortService} from Spring context.
   */
  @Autowired
  private PortService portService;

  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link EmployeeReferenceService} from Spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceService;

  /**
   * The {@link JobReferenceService} from Spring context.
   */
  @Autowired
  private JobReferenceService jobReferenceService;

  /**
   * The report name prefix for FAR report.
   */
  private static final String FAR_PREFIX = "FAR";

  /**
   * The report name prefix for FSR report.
   */
  private static final String FSR_PREFIX = "FSR";

  /**
   * The report name prefix for Appendix I.
   */
  private static final String APPENDIX_TASKLIST_PREFIX = "APPENDIX1";

  /**
   * The report name prefix for Appendix II.
   */
  private static final String APPENDIX_READINGS_PREFIX = "APPENDIX2";

  /**
   * The report name prefix for Appendix III.
   */
  private static final String APPENDIX_CHECKLIST_PREFIX = "APPENDIX3";

  /**
   * The pug template file name for FAR report.
   */
  private static final String FAR_TEMPLATE = "far_export_template.jade";

  /**
   * The pug template file name for FSR report.
   */
  private static final String FSR_TEMPLATE = "fsr_export_template.jade";

  /**
   * The pug template file name for Appendix I.
   */
  private static final String APPENDIX_TASKLIST_TEMPLATE = "appendix_tasklist_export_template.jade";

  /**
   * The pug template file name for Appendix II.
   */
  private static final String APPENDIX_READINGS_TEMPLATE = "appendix_readings_export_template.jade";

  /**
   * The pug template file name for Appendix III.
   */
  private static final String APPENDIX_CHECKLIST_TEMPLATE = "appendix_checklist_export_template.jade";

  /**
   * This field is use as variable in pug template to store report object content. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  public static final String CONTENT = "content";

  /**
   * This field is use as variable in pug template that store job object. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String JOB = "job";

  /**
   * This field is use as variable in pug template that store report object. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String REPORT = "report";

  /**
   * This field is use as variable in pug template that store reference data for service credit
   * status as lookup. The purpose of this lookup is to provide mapping of to obtain service credit
   * status name. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String SERVICE_CREDIT_STATUS_LOOKUP = "serviceCreditStatusLookup";

  /**
   * This field is use as variable in pug template that store reference data for resolution status
   * as lookup. The purpose of this lookup is to provide mapping of to obtain resolution status
   * name. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String RESOLUTION_STATUS_LOOKUP = "resolutionStatusLookup";

  /**
   * This field is use as variable in pug template that store reference data for service catalogue
   * as lookup. The purpose of this lookup is to provide mapping of to obtain service catalogue
   * code. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  public static final String SERVICE_CATALOGUE_LOOKUP = "serviceCatalogueLookup";

  /**
   * This field is use as variable in pug template that store reference data for action taken as
   * lookup. The purpose of this lookup is to provide mapping of to obtain action taken name. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String ACTION_TAKEN_LOOKUP = "actionTakenLookup";

  /**
   * This field is use as variable in pug template that store reference data for certificate action
   * as lookup. The purpose of this lookup is to provide mapping of to obtain certificate action
   * name. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String CERTIFICATE_ACTION_LOOKUP = "certificateActionLookup";

  /**
   * This field is use as variable in pug template that store list of tasks which already group by
   * product name, and service code. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String GROUPED_TASK = "groupedTask";

  /**
   * This field is use as variable in pug template that store list of table of content entries for
   * Appendix I. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String TOC_MAPPING = "tocMapping";

  /**
   * The directory name in Amazon S3 where the generated FAR/FSR to be uploaded. This variable in
   * only use at {@link BasePdfExport}. <br>
   * For more information see {@link ReportExportServiceImpl#getS3ExportDir()}.
   */
  private static final String S3_EXPORT_DIR = "report_export";

  /**
   * This field is use as variable in pug template that store list of surveyor. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String EMPLOYEE_LIST = "employeeList";

  /**
   * This field is use as variable in pug template that store issuer entity information. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String REPORT_ISSUE_BY = "reportIssueBy";

  /**
   * This field is use as variable in pug template that store authoriser entity information. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String REPORT_AUTHORISED_BY = "reportAuthorisedBy";

  /**
   * This field is use as variable in pug template that store the list of appendices to be populated
   * to FAR/FSR report. <br>
   * Refer to
   * {@link ReportExportServiceImpl#generateFile(Map, ReportExportDto, UserProfiles, String, LocalDateTime)}
   * for value assignment for this field.
   */
  private static final String APPENDIX_LIST = "appendixList";

  /**
   * The Appendix I name that use to print to FAR/FSR report.
   */
  private static final String APPENDIX_TASKLIST_NAME = "Appendix 1 - Tasks";

  /**
   * The Appendix II name that use to print to FAR/FSR report.
   */
  private static final String APPENDIX_READINGS_NAME = "Appendix 2 - Readings";

  /**
   * The Appendix III name that use to print to FAR/FSR report.
   */
  private static final String APPENDIX_CHECKLIST_NAME = "Appendix 3 - Checklist items";

  /**
   * This field is use as variable in pug template that store tabular format data for report
   * printing in Appendix II tailshaft section. <br>
   * Refer to {@link ReportExportServiceImpl#generateAppendix2Model(ReportContentHDto, Map, Map)}
   * for value assignment for this field.
   */
  private static final String TAILSHAFT_READINGS = "tailshaftReadings";

  /**
   * This field is use as variable in pug template that store tabular format data for report
   * printing in Appendix II survey group by service name. <br>
   * Refer to {@link ReportExportServiceImpl#generateAppendix2Model(ReportContentHDto, Map, Map)}
   * for value assignment for this field.
   */
  private static final String SURVEYS_BY_GROUP = "surveysByGroup";

  /**
   * This field is use as variable in pug template that store list of checklist object for Appendix
   * III. <br>
   * Refer to {@link ReportExportServiceImpl#generateAppendix3Model(List, Map)} for value assignment
   * for this field.
   */
  private static final String CHECKLIST = "checklist";

  /**
   * The DS service code. This is created due to no enum from MAST.
   */
  private static final String SERVICE_CODE_DS = "DS";

  /**
   * The DS service code. This is created due to no enum available from MAST.
   */
  private static final String SERVICE_CODE_TS = "TS";

  /**
   * The reading category for value "comparative". This is created due to no enum available from
   * MAST.
   */
  private static final Long READING_CATEGORY_COMPARATIVE = 3L;

  /**
   * The attribute data type for value "float". This is created due to no enum available from MAST.
   */
  private static final Long ATTRIBUTE_DATA_TYPE_FLOAT = 2L;

  /**
   * The field to map in model for report type.
   */
  private static final String REPORT_TYPE = "reportType";

  /**
   * The field to map in model for classRecommendation.
   */
  private static final String CLASS_RECOMMENDATION = "classRecommendation";
  /**
   * This field is use to check if the pdf is single page or multi page.
   */
  private static final String ONLY_ONE_PAGE = "onlyOnePage";

  /**
   * The {@link AssetRetrofitService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The {@link TaskReferenceService} from Spring context.
   */
  @Autowired
  private TaskReferenceService taskReferenceService;
  /**
   * The working directory name where the report is generated. This variable in only use at
   * {@link BasePdfExport} <br>
   * For more information see {@link ReportExportServiceImpl#getTempDir()}.
   */
  @Value("${report.export.temp.dir}")
  private String tempDir;

  @Override
  protected final String generateFile(final Map<String, Object> model, final ReportExportDto query,
      final UserProfiles user, final String workingDir, final LocalDateTime current) throws ClassDirectException {
    try {
      LOGGER.info("Generating report...");

      // get job information => location, jobNumber
      final JobHDto job = jobServiceDelegate.getJobsByJobId(query.getJobId()).execute().body();
      Resources.inject(job);
      model.put(JOB, job);

      // get asset model from job.
      ReportUtils.getAssetItemsModel(job.getAsset(), model, assetRetrofitService);

      // get report object => classRecommendation
      final ReportDto report =
          jobServiceDelegate.getSurveysByJobIdAndReportId(query.getJobId(), query.getReportId()).execute().body();
      model.put(REPORT, report);

      if (report.getClassRecommendation() != null) {
        String[] classRecommendations = report.getClassRecommendation().split("\n");
        model.put(CLASS_RECOMMENDATION, classRecommendations);
      }
      // to print current date in pdf
      model.put("currentDate", new Date());

      // set report type related information
      String prefix = "";
      String template = "";
      String code = "";
      if (job.getJobNumber() != null && !job.getJobNumber().isEmpty()) {
        code = job.getJobNumber();
      } else {
        code = job.getId().toString();
      }

      if (report.getReportType().getId().equals(ReportType.FAR.value())) {
        prefix = FAR_PREFIX;
        template = FAR_TEMPLATE;
        model.put(REPORT_TYPE, FAR_PREFIX);
      } else if (report.getReportType().getId().equals(ReportType.FSR.value())) {
        prefix = FSR_PREFIX;
        template = FSR_TEMPLATE;
        model.put(REPORT_TYPE, FSR_PREFIX);
      } else {
        throw new ClassDirectException("Report type not found.");
      }

      // query reference data lookup
      final Map<Long, ServiceCreditStatusHDto> serviceCreditStatusLookup = generateServiceStatusLookup();
      model.put(SERVICE_CREDIT_STATUS_LOOKUP, serviceCreditStatusLookup);
      final Map<Long, JobResolutionStatusHDto> resolutionStatusLookup = generateResolutionStatusLookup();
      model.put(RESOLUTION_STATUS_LOOKUP, resolutionStatusLookup);
      final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup = generateServiceCatalogueLookup();
      model.put(SERVICE_CATALOGUE_LOOKUP, serviceCatalogueLookup);
      final Map<Long, ActionTakenHDto> actionTakenLookup = generateActionTakenLookup();
      model.put(ACTION_TAKEN_LOOKUP, actionTakenLookup);

      // get report snapshot => CoC, AI, AN, Survey
      final String content = report.getContent();
      Map<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>> groupedTask;
      Map<Long, CertificateActionDto> certificateActionLookup = new TreeMap<>();
      ReportContentHDto contentObj;
      if (content != null) {
        contentObj = convertJsonToObject(content);
        Resources.inject(contentObj);
        validateReportContent(contentObj);
        validatePortOfRegistry(contentObj);
      } else {
        throw new ClassDirectException(
            "Report content is null. This may cause by MAST version of this report doesn't exist.");
      }
      contentObj.getSurveys().forEach(survey -> {
        LOGGER.debug("Survey for {} : {}", job.getAsset().getId(), survey.getStringRepresentationForHash());
      });

      // LRCD-3197 surveys should sort by ervice catalogue display order.
      contentObj.getSurveys().sort((s1, s2) -> {
        int compareRetVal = serviceCatalogueLookup.get(s1.getServiceCatalogue().getId()).getDisplayOrder()
            .compareTo(serviceCatalogueLookup.get(s2.getServiceCatalogue().getId()).getDisplayOrder());
        return compareRetVal;
      });

      model.put(CONTENT, contentObj);
      // get grouped task
      groupedTask = ReportUtils.getGroupedTask(model, serviceCatalogueLookup, taskReferenceService, null, false);
      model.put(GROUPED_TASK, groupedTask);
      // get serviceNarrativesSS
      if (prefix.equals(FSR_PREFIX)) {
        ReportUtils.setServiceNarratives(model, serviceCatalogueLookup, taskReferenceService, null, true);
      }
      // extract certificate action from content as lookup
      if (contentObj.getCertificateActions() != null) {
        certificateActionLookup = generateCertificateActionLookup(contentObj.getCertificateActions());
      }
      model.put(CERTIFICATE_ACTION_LOOKUP, certificateActionLookup);

      // get attending surveyor
      final List<EmployeeInfoContainer> employeeList = getAttendingSurveyor(job.getEmployees());
      model.put(EMPLOYEE_LIST, employeeList);

      // get report issue by from LRCD-3197 it should be tha lead surveyour.
      if (!employeeList.isEmpty()) {
        employeeList.forEach(employee -> {
          if (employee.getEmployeeLink().getEmployeeRole().getId().equals(EmployeeRole.LEAD_SURVEYOR.getValue())) {
            model.put(REPORT_ISSUE_BY, employee.getEmployee());
          }
        });
      }

      // get report authorized by
      if (report.getAuthorisedBy() != null) {
        model.put(REPORT_AUTHORISED_BY, employeeReferenceService.getEmployee(report.getAuthorisedBy().getId()));
      } else {
        model.put(REPORT_AUTHORISED_BY, new LrEmployeeDto());
      }

      // file collection
      final List<String> fileList = new ArrayList<>();
      final List<String> appendixList = new ArrayList<>();

      if (contentObj.getTasks() != null && !contentObj.getTasks().isEmpty()) {
        // generate appendix III
        contentObj.setTasks(contentObj.getTasks());
        generateAppendix3Model(contentObj.getTasks(), model);
        final List<WorkItemDto> checklist = (List<WorkItemDto>) model.get(CHECKLIST);
        if (!checklist.isEmpty()) {
          final String appendixChecklistFile = generatePdfWithoutWatermark(APPENDIX_CHECKLIST_PREFIX, code, current,
              APPENDIX_CHECKLIST_TEMPLATE, workingDir, model);
          fileList.add(appendixChecklistFile);
        }
        // generate appendix II
        generateAppendix2Model(contentObj, model, serviceCatalogueLookup);
        final List<TailshaftReadingsTabularDataFormat> tailShaftReadings =
            (List<TailshaftReadingsTabularDataFormat>) model.get(TAILSHAFT_READINGS);
        final Map<String, Map<Long, List<SurveyTabularDataFormat>>> surveyByGroup =
            (Map<String, Map<Long, List<SurveyTabularDataFormat>>>) model.get(SURVEYS_BY_GROUP);

        if (!tailShaftReadings.isEmpty() || !surveyByGroup.isEmpty()) {
          final String appendixReadingsFile = generatePdfWithoutWatermark(APPENDIX_READINGS_PREFIX, code, current,
              APPENDIX_READINGS_TEMPLATE, workingDir, model);
          fileList.add(appendixReadingsFile);
        }
        // generate appendix I - create TOC mapping without page number
        Map<String, Integer> tocMapping = new LinkedHashMap<>();
        for (final Entry<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>>
        map : groupedTask.entrySet()) {
          tocMapping.put(map.getKey().getName(), 0);
        }
        model.put(TOC_MAPPING, tocMapping);

        // generate file + toc without page number
        String appendixTasklistFile = generatePdfWithoutWatermark(APPENDIX_TASKLIST_PREFIX, code, current,
            APPENDIX_TASKLIST_TEMPLATE, workingDir, model);

        // re-create TOC mapping with page number (by looping created pdf file)
        tocMapping = ReportUtils.generateTOCForAppendix(workingDir + File.separator + appendixTasklistFile, tocMapping);
        model.put(TOC_MAPPING, tocMapping);

        // generate file + toc with page number
        appendixTasklistFile = generatePdfWithoutWatermark(APPENDIX_TASKLIST_PREFIX, code, current,
            APPENDIX_TASKLIST_TEMPLATE, workingDir, model);
        fileList.add(appendixTasklistFile);

        // add appendix name in sequence
        appendixList.add(APPENDIX_TASKLIST_NAME);
        appendixList.add(APPENDIX_READINGS_NAME);
        appendixList.add(APPENDIX_CHECKLIST_NAME);
      }

      // get appendix list
      model.put(APPENDIX_LIST, appendixList);

      // generate report file
      String reportFile = generatePdfWithoutWatermark(prefix, code, current, template, workingDir, model);

      int pageCountForFAR = ServiceUtils.getPageCount(workingDir, reportFile);
      if (pageCountForFAR == 1) {
        model.put(ONLY_ONE_PAGE, "true");
        reportFile = generatePdfWithoutWatermark(prefix, code, current, template, workingDir, model);
      }

      fileList.add(reportFile);

      // merge pdf file
      Collections.reverse(fileList);
      final String returnFileName = packageFile(fileList, workingDir, code, current, prefix);

      return returnFileName;
    } catch (final JadeException | IOException | ParserConfigurationException | SAXException
        | DocumentException exception) {
      LOGGER.error("Error generating FAR/FSR for job {}, report {}. | Error Message: {}. | {}", query.getJobId(),
          query.getReportId(), exception.getMessage(), exception);
      throw new ClassDirectException(exception);
    }
  }



  /**
   * Validates each field in report content object, if any of the field null set empty list to avoid
   * error in report generation.
   *
   * @param contentObj the report content model object.
   * @return valid report content object.
   */
  private ReportContentHDto validateReportContent(final ReportContentHDto contentObj) {

    ReportContentHDto jsonContent = contentObj;
    final Class<ReportContentHDto> reportContentClass = ReportContentHDto.class;

    Stream.of(reportContentClass.getDeclaredFields()).forEach(field -> {
      try {
        field.setAccessible(true);
        if (field.get(jsonContent) == null) {
          if (field.getType().equals(List.class)) {
            field.set(jsonContent, Collections.emptyList());
          }
        }
      } catch (IllegalArgumentException | IllegalAccessException exception) {
        LOGGER.error("Error in validationg report content object", exception.getMessage(), exception);
      }
    });

    return jsonContent;
  }

  /**
   * Validates port of registry and gets port of registry from {@link PortService#getPort(long)} and
   * sets to content.
   *
   * @param reportContentDto the report content model object.
   * @return valid reportContentDto object.
   */
  private ReportContentHDto validatePortOfRegistry(final ReportContentHDto reportContentDto) {

    ReportContentHDto reportContentObj = reportContentDto;
    if (reportContentObj.getRegisteredPort() != null) {
      try {
        PortOfRegistryDto port = portService.getPort(reportContentObj.getRegisteredPort().getId());
        Optional.ofNullable(port).ifPresent(portDetails -> {
          reportContentObj.setRegisteredPortH(portDetails);
        });
      } catch (final RecordNotFoundException exception) {
        LOGGER.error("Record not found for port of registry {}", reportContentObj.getRegisteredPort().getId(),
            exception);
      }
    }
    return reportContentObj;
  }

  /**
   * Generates appendix III data. <br>
   * Appendix III consists of list of checklist. Checklist share the same object with list of task.
   * Checklist is obtained by filter workItemType = 2.
   *
   * @param tasks the list of tasks extracted from report content.
   * @param model the model parse to pug file.
   */
  public final void generateAppendix3Model(final List<WorkItemDto> tasks, final Map<String, Object> model) {

    // filter if it is a checklist type, and resolution status is not null
    final List<WorkItemDto> checklist =
        tasks.stream().filter(task -> Optional.ofNullable(task.getWorkItemType()).map(LinkResource::getId).isPresent())
            .filter(task -> task.getWorkItemType().getId().equals(WorkItemType.CHECKLIST.value()))
            .filter(task -> task.getResolutionStatus() == null).collect(Collectors.toList());

    model.put(CHECKLIST, checklist);
  }

  /**
   * Generates appendix II data. <br>
   * Whole appendix 2 is separated into three story in MAST. Refer to LRD-8024, LRD-9683, and
   * LRD-9685 for more information. <br>
   * <br>
   * Appendix II is divided into two section:
   * <ol>
   * <li>TailShaft reading</li>
   * <li>Surveys by group</li>
   * </ol>
   * Both sections above are filtered to only show list of task with non null value at field
   * “attributes”, then within the attributes itself the field “attributeValueString” and
   * “readingCategory” is not null. <br>
   * Further data preprocessing for:<br>
   * <ol>
   * <li>TailShaft is describe in {@link ReportExportServiceImpl#getDataForTailShaft(List)}</li>
   * <li>Surveys by group is describe in
   * {@link ReportExportServiceImpl#getDataForSurvey(List, ReportContentHDto, Map)}</li>
   * </ol>
   *
   * @see <a href="https://bae-lr.atlassian.net/browse/LRD-8024">LRD-8024</a>
   * @see <a href="https://bae-lr.atlassian.net/browse/LRD-9683">LRD-9683</a>
   * @see <a href="https://bae-lr.atlassian.net/browse/LRD-9685">LRD-9685</a>
   *
   * @param content the content from report content.
   * @param model the model parse to pug file.
   * @param serviceCatalogueLookup the id lookup for service catalogue.
   */
  public final void generateAppendix2Model(final ReportContentHDto content, final Map<String, Object> model,
      final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup) {

    // REMARK: The 2 steps filtering below is used for all 3 sections in Appendix 2
    // step: filter the tasks
    // where: task.attributes != null & !empty
    final List<WorkItemDto> filteredTasks = content.getTasks().stream()
        .filter(task -> task.getAttributes() != null && !task.getAttributes().isEmpty()).collect(Collectors.toList());

    // step: filter the attributes inside the task
    // where: attributeValueString != null && workItemConditionalAttribute.readingCategory != null
    filteredTasks.forEach(task -> {
      task.getAttributes()
          .removeIf(attribute -> attribute.getAttributeDataType() == null
              || attribute.getWorkItemConditionalAttribute() == null
              || attribute.getWorkItemConditionalAttribute().getReadingCategory() == null);
    });

    // implement logic for LRD-8024
    model.put(TAILSHAFT_READINGS, getDataForTailShaft(filteredTasks));

    // implement logic for LRD-9683, LRD-9685
    model.put(SURVEYS_BY_GROUP, getDataForSurvey(filteredTasks, content, serviceCatalogueLookup));
  }


  /**
   * Generates data for Appendix II tailshaft reading section. Refer to LRD-8024 for more
   * information. <br>
   * The input tasks is further filter with service code “TS” and “DS”, and field “readingCategory”
   * = 3 (comparative). Then, group the data by service code, and then process the data to report
   * tabular format.
   *
   * @see <a href="https://bae-lr.atlassian.net/browse/LRD-8024">LRD-8024</a>
   *
   * @param inTasks the pre-filtered tasks from
   *        {@link ReportExportServiceImpl#generateAppendix2Model(ReportContentHDto, Map, Map)}.
   * @return the tabular data for report printing.
   */
  public final List<TailshaftReadingsTabularDataFormat> getDataForTailShaft(final List<WorkItemDto> inTasks) {

    // Map<attributeId, taskName> for report printing purpose
    final Map<Long, String> taskNameLookup = new ConcurrentHashMap<>();

    // step: filter the list with "DS" and "TS" type
    // step: create map of key = attributeId, value = task name for result lookup
    // step: convert it to map of key = serviceCode, value = list of attribute
    // step: filter the data with readingCategory = 3 (Comparative)
    final Map<String, List<WorkItemAttributeDto>> dataCollect = inTasks.stream()
        .filter(task -> task.getServiceCode().equals(SERVICE_CODE_DS) || task.getServiceCode().equals(SERVICE_CODE_TS))
        .map(task -> {
          task.getAttributes().forEach(attr -> {
            taskNameLookup.put(attr.getId(), task.getName());
          });
          return task;
        }).collect(Collectors.groupingBy(WorkItemDto::getServiceCode,
            Collectors.mapping(WorkItemDto::getAttributes, Collector.of(ArrayList::new, List::addAll, (x, y) -> {
              x.addAll(y.stream().filter(attr -> attr.getWorkItemConditionalAttribute().getReadingCategory()
                  .getId() != READING_CATEGORY_COMPARATIVE).collect(Collectors.toList()));
              return x;
            }))));

    // calculating and printing the result
    final List<TailshaftReadingsTabularDataFormat> reportData = new ArrayList<>();

    // step: loop attributes for service code DS
    // step: check for attributes under service code TS where
    // DS.workItemConditionalAttribute.comparativeAttributeCode =
    // TS.workItemConditionAttribute.referenceCode
    // step: if condition above matched, print to report
    Optional.ofNullable(dataCollect.get(SERVICE_CODE_DS)).ifPresent(attributesDS -> {
      attributesDS.forEach(attrDS -> {

        Optional.ofNullable(dataCollect.get(SERVICE_CODE_TS)).ifPresent(attributesTS -> {
          attributesTS.forEach(attrTS -> {
            if (attrDS.getWorkItemConditionalAttribute().getComparativeAttributeCode()
                .equals(attrTS.getWorkItemConditionalAttribute().getReferenceCode())) {
              // both matching TS and DS will always have same taskname and description
              // using DS object value for report printing
              final TailshaftReadingsTabularDataFormat data = new TailshaftReadingsTabularDataFormat();

              // item field is task name
              data.setItemField(taskNameLookup.get(attrDS.getId()));
              data.setReadingField(attrDS.getDescription());
              data.setDsField(attrDS.getAttributeValueString());
              data.setTsField(attrTS.getAttributeValueString());

              // formula to calculate age variance calculation
              // = (tsValue - dsValue)/ dsValue * 100, where Value = attributeValueString
              // it will only be calculated if both DS & TS attributeDataType.id = float
              // return result in 2 decimal format + "%" sign
              if (attrDS.getAttributeDataType().getId().equals(ATTRIBUTE_DATA_TYPE_FLOAT)
                  && attrTS.getAttributeDataType().getId().equals(ATTRIBUTE_DATA_TYPE_FLOAT)) {
                final float ageVariance =
                    (Float.valueOf(attrTS.getAttributeValueString()) - Float.valueOf(attrDS.getAttributeValueString()))
                        / Float.valueOf(attrDS.getAttributeValueString()) * 100;

                final DecimalFormat twoDecimalFormat = new DecimalFormat("###.##");
                data.setAgeVarianceField(twoDecimalFormat.format(ageVariance) + "%");
              }

              // add the data to report
              reportData.add(data);
            }
          });
        });
      });
    });

    return reportData;
  }


  /**
   * Generates Appendix II survey by group section. Refer to LRD-9683, and LRD-9685 for more
   * information. <br>
   * The input task data is grouped by service code, then process the data to report tabular format.
   *
   *
   * @see <a href="https://bae-lr.atlassian.net/browse/LRD-9683">LRD-9683</a>
   * @see <a href="https://bae-lr.atlassian.net/browse/LRD-9685">LRD-9685</a>
   *
   * @param inTasks the pre-filtered tasks from
   *        {@link ReportExportServiceImpl#generateAppendix2Model(ReportContentHDto, Map, Map)}.
   * @param content the content from report content.
   * @param serviceCatalogueLookup the id lookup for service catalogue.
   * @return the tabular data for report printing.
   */
  public final Map<String, Map<Long, List<SurveyTabularDataFormat>>> getDataForSurvey(final List<WorkItemDto> inTasks,
      final ReportContentHDto content, final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup) {

    // Map<serviceId, serviceName> for task grouping purpose
    final Map<Long, String> serviceIdServiceCatalogueNameLookup = new ConcurrentHashMap<>();

    // map survey id with service name
    Optional.ofNullable(content.getSurveys()).ifPresent(surveys -> {
      surveys.forEach(survey -> {
        Optional.ofNullable(survey.getName()).ifPresent(name -> {
          serviceIdServiceCatalogueNameLookup.put(survey.getId(), name);
        });
      });
    });


    // Map<serviceName, List<task>> for data grouping
    final Map<String, List<WorkItemDto>> groupedTask = new TreeMap<>();


    // perform task grouping by service name
    for (final WorkItemDto task : inTasks) {

      final String serviceName = serviceIdServiceCatalogueNameLookup.get(task.getSurvey().getId());
      if (serviceName != null) {
        if (groupedTask.get(serviceName) == null) {
          groupedTask.put(serviceName, new ArrayList<>());
        }
        groupedTask.get(serviceName).add(task);
      } else {
        LOGGER.error("Service {} not available for task {}", task.getSurvey().getId(), task.getId());
      }
    }

    // perform task display grouping
    // further split out grouping by service name, reading category as report is printing data by
    // survey name, then by reading category
    final Map<String, Map<Long, List<SurveyTabularDataFormat>>> reportData = new TreeMap<>();

    groupedTask.forEach((serviceName, tasks) -> {
      if (reportData.get(serviceName) == null) {
        reportData.put(serviceName, new TreeMap<>());
      }

      tasks.forEach(task -> {
        task.getAttributes().forEach(attr -> {

          final Long readingCategoryId = attr.getWorkItemConditionalAttribute().getReadingCategory().getId();

          // reading category id = 3, only print to report when task.serviceCode = DS or TS
          // reading category id = 2, only print to report when attr.previousAttributeValueString ==
          // null
          if (readingCategoryId == READING_CATEGORY_COMPARATIVE
              && !(task.getServiceCode().equals(SERVICE_CODE_DS) || task.getServiceCode().equals(SERVICE_CODE_TS))) {
            return;
          } else if (readingCategoryId == 2 && attr.getPreviousAttributeValueString() == null) {
            return;
          }

          // generate the report data
          // all the data passed the condition on above code will be included as a part of report
          // data
          if (reportData.get(serviceName).get(readingCategoryId) == null) {
            reportData.get(serviceName).put(readingCategoryId, new ArrayList<>());
          }

          final SurveyTabularDataFormat data = new SurveyTabularDataFormat();

          // item field is task name
          data.setItemField(task.getName());
          data.setReadingField(attr.getDescription());
          data.setValueReportedField(attr.getAttributeValueString());
          data.setPreviousField(attr.getPreviousAttributeValueString());

          reportData.get(serviceName).get(readingCategoryId).add(data);
        });
      });
      if (reportData.get(serviceName).isEmpty()) {
        reportData.remove(serviceName);
      }
    });

    return reportData;
  }

  /**
   * Generates certificate action lookup for linking, and mapping with certificate object in pug
   * file.
   *
   * @param list the list of certificate action from report content.
   * @return the certificate action lookup object.
   */
  public final Map<Long, CertificateActionDto> generateCertificateActionLookup(final List<CertificateActionDto> list) {
    final Map<Long, CertificateActionDto> lookup = new TreeMap<>();
    for (final CertificateActionDto entry : list) {
      Optional.ofNullable(entry.getCertificate()).map(LinkVersionedResource::getId)
          .ifPresent(id -> lookup.put(id, entry));
    }

    return lookup;
  }

  /**
   * Generates reference data resolution status for mapping in pug file.
   *
   * @return the resolution status lookup object.
   */
  public final Map<Long, JobResolutionStatusHDto> generateResolutionStatusLookup() {
    final List<JobResolutionStatusHDto> resolutionStatusList = jobReferenceService.getResolutionStatuses();
    final Map<Long, JobResolutionStatusHDto> resolutionStatusLookup =
        resolutionStatusList.stream().collect(Collectors.toMap(JobResolutionStatusHDto::getId, Function.identity()));
    return resolutionStatusLookup;
  }

  /**
   * Generates reference data action taken for mapping in pug file.
   *
   * @return the action taken lookup object.
   */
  public final Map<Long, ActionTakenHDto> generateActionTakenLookup() {
    final List<ActionTakenHDto> actionTakenList = assetReferenceService.getActionTakenList();
    final Map<Long, ActionTakenHDto> actionTakenLookup =
        actionTakenList.stream().collect(Collectors.toMap(ActionTakenHDto::getId, Function.identity()));
    return actionTakenLookup;
  }

  /**
   * Generates reference data service status for mapping in pug file.
   *
   * @return the service status lookup object.
   */
  public final Map<Long, ServiceCreditStatusHDto> generateServiceStatusLookup() {
    final List<ServiceCreditStatusHDto> serviceCreditStatusRefList = serviceService.getServiceCreditStatuses();
    final Map<Long, ServiceCreditStatusHDto> serviceCreditStatusLookup = serviceCreditStatusRefList.stream()
        .collect(Collectors.toMap(ServiceCreditStatusHDto::getId, Function.identity()));
    return serviceCreditStatusLookup;
  }

  /**
   * Generates reference data service catalogue for mapping in pug file.
   *
   * @return the service catalogue lookup object.
   */
  public final Map<Long, ServiceCatalogueHDto> generateServiceCatalogueLookup() {
    final List<ServiceCatalogueHDto> serviceCatalogueList = serviceService.getServiceCatalogues();
    final Map<Long, ServiceCatalogueHDto> serviceCatalogueLookup =
        serviceCatalogueList.stream().collect(Collectors.toMap(ServiceCatalogueHDto::getId, Function.identity()));
    return serviceCatalogueLookup;
  }

  /**
   * Packages the file into single PDF if more than one pdf is generated. <br>
   * Each report and appendices is generated in separate pdf. This method will combine all pdf into
   * one.
   *
   * @param fileList the list of file and appendices.
   * @param workingDir the working directory where all generated pdf is located.
   * @param code the code for file section report name.
   * @param current the timestamp when report generation is triggered.
   * @param prefix the file prefix name.
   * @return file the file name.
   * @throws FileNotFoundException if corresponding file not found.
   * @throws IOException if file fail to read or write.
   * @throws ClassDirectException if the file list is empty.
   */
  public final String packageFile(final List<String> fileList, final String workingDir, final String code,
      final LocalDateTime current, final String prefix)
      throws FileNotFoundException, IOException, ClassDirectException {
    if (fileList.isEmpty()) {
      throw new ClassDirectException("Error: No file generated.");
    }

    final String filename = fileList.get(0);

    if (fileList.size() > 1) {

      // merge pdf files
      final PDFMergerUtility merger = new PDFMergerUtility();

      for (final String pdf : fileList) {
        merger.addSource(new File(workingDir + File.separator + pdf));
      }

      merger.setDestinationFileName(workingDir + File.separator + filename);
      merger.mergeDocuments(null);
    }

    return filename;
  }

  /**
   * Generates attending surveyor information. <br>
   * Attending surveyor is a list of employee under job object and filtered by surveyor role id 2
   * and 11 from LRCD-3197.
   *
   * @param employees the list of employee.
   * @return list of employee filtered by surveyor.
   */
  public final List<EmployeeInfoContainer> getAttendingSurveyor(final List<EmployeeLinkDto> employees) {
    final List<EmployeeInfoContainer> employeeList = new ArrayList<>();
    if (employees != null) {
      employees.stream()
          .filter(
              employee -> employee.getEmployeeRole().getId().equals(EmployeeRole.SURVEYOR_ASSESSOR_INSPECTOR.getValue())
                  || employee.getEmployeeRole().getId().equals(EmployeeRole.LEAD_SURVEYOR.getValue()))
          .forEach(employee -> {
            try {
              final EmployeeInfoContainer container = new EmployeeInfoContainer();
              container.setEmployeeLink(employee);
              final LrEmployeeDto employeeDetail =
                  employeeReferenceService.getEmployee(employee.getLrEmployee().getId());
              if (employeeDetail != null) {
                container.setEmployee(employeeDetail);
              }

              employeeList.add(container);
            } catch (final Exception e) {
              LOGGER.info("No employee detail found.");
            }
          });
    }
    return employeeList;
  }

  /**
   * Provides container class for Appendix-2 surveys field.
   *
   * @author yng
   *
   */
  public static final class SurveyTabularDataFormat {

    /**
     * The Appendix II survey item field.
     */
    @Getter
    @Setter
    private String itemField;

    /**
     * The Appendix II survey reading field.
     */
    @Getter
    @Setter
    private String readingField;

    /**
     * The Appendix II survey value reported field.
     */
    @Getter
    @Setter
    private String valueReportedField;

    /**
     * The Appendix II survey previous field.
     */
    @Getter
    @Setter
    private String previousField;
  }


  /**
   * Provides container class for Appendix-2 Tailshaft Readings field.
   *
   * @author yng
   *
   */
  public static final class TailshaftReadingsTabularDataFormat {

    /**
     * The Appendix II tailshaft item field.
     */
    @Getter
    @Setter
    private String itemField;

    /**
     * The Appendix II tailshaft reading field.
     */
    @Getter
    @Setter
    private String readingField;

    /**
     * The Appendix II tailshaft DS field.
     */
    @Getter
    @Setter
    private String dsField;

    /**
     * The Appendix II tailshaft TS field.
     */
    @Getter
    @Setter
    private String tsField;

    /**
     * The Appendix II tailshaft age variance field.
     */
    @Getter
    @Setter
    private String ageVarianceField;
  }



  /**
   * Provides container class for employee related object field. This container collect both MAST
   * employee object {@link EmployeeLinkDto} and {@link LrEmployeeDto} for data printing in pug
   * template.
   *
   * @author yng
   *
   */
  public static final class EmployeeInfoContainer {

    /**
     * Contain role information.
     */
    @Getter
    @Setter
    private EmployeeLinkDto employeeLink;

    /**
     * Contain employee name information.
     */
    @Getter
    @Setter
    private LrEmployeeDto employee;
  }

  /**
   * Converts json string to report content object.
   *
   * @param content the json string obtained from report object.
   * @return json the report content object.
   * @throws JsonParseException if JSON parsing fail.
   * @throws JsonMappingException if JSON field mapping rule fail.
   * @throws IOException if JSON cannot be read.
   */
  public final ReportContentHDto convertJsonToObject(final String content)
      throws JsonParseException, JsonMappingException, IOException {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper.readValue(content, ReportContentHDto.class);
  }

  @Override
  protected final String getS3ExportDir() {
    return S3_EXPORT_DIR;
  }

  @Override
  protected final String getTempDir() {
    return this.tempDir;
  }


}
