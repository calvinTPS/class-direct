package org.springframework.cache.interceptor;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.util.CollectionUtils;

import com.baesystems.ai.lr.cd.service.cache.CacheOperationProcessor;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides the custom cache interceptor service for performing special case handling in cache evict operation for some
 * spring cache.
 *
 * @author RKaneysan.
 */
public class CustomCacheInterceptor extends CacheInterceptor {

  /**
   * Serial version uid.
   */
  private static final long serialVersionUID = 8870925082961618128L;

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CustomCacheInterceptor.class);

  /**
   * Cache Operation Processor Map.
   */
  @Getter
  @Setter
  private Map<String, CacheOperationProcessor> customCacheProcessorMap;

  /**
   * Returns cache operation result for each cache operation executions.
   *
   * @param invoker method invoker.
   * @param target target class in which method is being invoked.
   * @param method method being invoked.
   * @param args arguments of the method being invoked.
   * @return result of cache operation.
   */
  @Override
  protected Object execute(final CacheOperationInvoker invoker, final Object target, final Method method,
      final Object[] args) {
    LOGGER.trace("Custom Cache Interceptor invoked.");
    Object result = null;
    final List<CacheContextContainer> contextList = getCacheContextContainers(target, method, args);
    if (!contextList.isEmpty()) {
      contextList.forEach(context -> {
        final Map<String, Object> processorContext = context.getCacheProcessorContext();
        final Cache cache = context.getCache();
        final Object cacheKey = context.getCacheKey();
        context.getProcessor().preProcessor(cache, cacheKey, processorContext);
      });

      result = super.execute(invoker, target, method, args);

      contextList.forEach(context -> {
        final Map<String, Object> processorContext = context.getCacheProcessorContext();
        final Cache cache = context.getCache();
        final Object cacheKey = context.getCacheKey();
        context.getProcessor().postProcessor(cache, cacheKey, processorContext);
      });
    } else {
      result = super.execute(invoker, target, method, args);
    }
    return result;
  }

  /**
   * Returns Cache Context Containers for given method with given argument as signature of the target object.
   *
   * @param target target object.
   * @param method method invoked.
   * @param args arguments in method invoked.
   * @return Cache Context Containers.
   */
  protected List<CacheContextContainer> getCacheContextContainers(final Object target, final Method method,
      final Object[] args) {
    final List<CacheContextContainer> contextList = new ArrayList<>();
    final Class<?> targetClass = getTargetClass(target);
    final List<CacheOperation> evictCacheOperList = getCacheOperationSource().getCacheOperations(method, targetClass)
        .stream().filter(cacheOp -> cacheOp instanceof CacheEvictOperation).collect(Collectors.toList());
    if (!CollectionUtils.isEmpty(evictCacheOperList)) {
      for (CacheOperation operation : evictCacheOperList) {
        final CacheOperationContext cacheOperationContext =
            super.getOperationContext(operation, method, args, target, targetClass);
        final Collection<? extends Cache> cacheList = cacheOperationContext.getCaches();
        Optional.ofNullable(cacheList).flatMap(caches -> caches.stream().findFirst()).ifPresent(cache -> {
          final String cacheName = cache.getName();
          final CacheOperationProcessor currentProcessor = customCacheProcessorMap.get(cacheName);
          LOGGER.info("Method: {}, Cache: {}, CurrentProcessor : {}", method.getName(), cacheName, currentProcessor);
          if (currentProcessor != null) {
            final Object cacheKey = cacheOperationContext.generateKey(CacheOperationExpressionEvaluator.NO_RESULT);
            final CacheContextContainer contextHolder = new CacheContextContainer();
            contextHolder.setCache(cache);
            contextHolder.setCacheKey(cacheKey);
            contextHolder.setProcessor(currentProcessor);
            contextList.add(contextHolder);
          }
        });
      }
    }
    return contextList;
  }

  /**
   * Returns target class for given target object.
   *
   * @param target object.
   * @return target class.
   */
  private Class<?> getTargetClass(final Object target) {
    Class<?> targetClass = AopProxyUtils.ultimateTargetClass(target);
    if (targetClass == null && target != null) {
      targetClass = target.getClass();
    }
    return targetClass;
  }



  /**
   * Gets the cached value from underlying cache implementation with additional logic to check if the cached
   * value is null.
   * <p>
   * In rare scenario on heavy loaded of concurrent put to cache and evict cache, the value (not the
   * {@link ValueWrapper}) of the cached item can be null. To avoid this, method should return null instead of
   * {@link ValueWrapper} so the caller will treat this as cache not found scenario and trigger the actual method.
   * </p>
   * @see org.springframework.cache.interceptor.AbstractCacheInvoker#doGet(org.springframework.cache.Cache
   * , java.lang.Object)
   */
  @Override
  protected final ValueWrapper doGet(final Cache cache, final Object key) {
    ValueWrapper value = super.doGet(cache, key);
    if (value != null && value.get() == null) {
      LOGGER.error("Detected null cache value for key: {} in cache : {}. Return null to treat as cache missed."
          , key, cache.getName());
      value = null;
    }
    return value;
  }

  /**
   * Injects the value to the underlying cache implementation, log error message in-case of null value injected
   * which should not ever happen.
   * @see org.springframework.cache.interceptor.AbstractCacheInvoker#doPut(org.springframework.cache.Cache
   * , java.lang.Object, java.lang.Object)
   */
  @Override
  protected void doPut(final Cache cache, final Object key, final Object result) {
    if (result != null) {
      super.doPut(cache, key, result);
    } else {
      LOGGER.error("Detected null cache value for key: {} in cache : {}. Skip put to cache.", key, cache.getName());
    }
  }




  /**
   * Static Inner Class for Cache Context Container.
   */
  static class CacheContextContainer {
    /**
     * Custom Cache Processor.
     */
    @Getter
    @Setter
    private CacheOperationProcessor processor;

    /**
     * Spring Cache.
     */
    @Getter
    @Setter
    private Cache cache;

    /**
     * Cache Key.
     */
    @Getter
    @Setter
    private Object cacheKey;

    /**
     * Cache Processor Context.
     */
    @Getter
    @Setter
    private Map<String, Object> cacheProcessorContext = new HashMap<>();
  }
}

