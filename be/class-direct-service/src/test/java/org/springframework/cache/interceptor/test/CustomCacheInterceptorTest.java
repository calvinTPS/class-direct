package org.springframework.cache.interceptor.test;


import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ACTIVE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.AnnotationCacheOperationSource;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean;
import org.springframework.cache.interceptor.CacheInterceptor;
import org.springframework.cache.interceptor.CacheOperationSource;
import org.springframework.cache.interceptor.CustomCacheInterceptor;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.core.SpringContextUtil;
import com.baesystems.ai.lr.cd.service.cache.CacheOperationProcessor;
import com.baesystems.ai.lr.cd.service.cache.impl.UserCacheProcessor;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.cd.service.userprofile.impl.UserProfileServiceImpl;

import lombok.Getter;

/**
 * Unit test for Custom Cache Interceptor.
 *
 * @author RKaneysan.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CustomCacheInterceptorTest.Config.class)
public class CustomCacheInterceptorTest {

  /**
   * User id 1001.
   */
  private static final String USER_ID_1001 = "1001";

  /**
   * Injected userProfile service.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Injected user profile dao.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  /**
   * Inject cache manager.
   */
  @Autowired
  private CacheManager cacheManager;

  /**
   * Test case to check if user cache updated when update user given user not yet re-login.
   *
   * @throws Throwable Exception if any.
   */
  @Test
  public final void shouldUpdateCachedUserprofileWhomNotYetRelogin() throws Throwable {
    final UserAccountStatus userStatus = mockUserAccountStatus(ACTIVE);
    final UserProfiles mockedUserProfile = new UserProfiles();
    mockedUserProfile.setUserId(USER_ID_1001);
    mockedUserProfile.setStatus(userStatus);
    Mockito.when(userProfileDao.getUser(USER_ID_1001)).thenReturn(mockedUserProfile);

    final UserProfiles userprofile = userProfileService.getUser(USER_ID_1001);
    Cache userCache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    final ValueWrapper cachedValue = userCache.get(CacheConstants.PREFIX_USER_ID + USER_ID_1001);
    assertNotNull(cachedValue);

    UserProfiles cachedUser = (UserProfiles) cachedValue.get();
    assertNotNull(cachedUser);
    assertEquals(cachedUser, userprofile);

    final UserProfiles updatedUserprofile = new UserProfiles();
    updatedUserprofile.setUserId(USER_ID_1001);
    updatedUserprofile.setUserAccExpiryDate(LocalDate.now());
    updatedUserprofile.setStatus(userStatus);
    updatedUserprofile.setCompany(new Company());
    Mockito.when(userProfileDao.updateUser(USER_ID_1001, USER_ID_1001, updatedUserprofile, null))
        .thenReturn(updatedUserprofile);
    Mockito.when(userProfileDao.getUser(USER_ID_1001)).thenReturn(updatedUserprofile);
    userProfileService.updateUser(USER_ID_1001, USER_ID_1001, updatedUserprofile, null);

    final ValueWrapper updatedCachedValue = userCache.get(CacheConstants.PREFIX_USER_ID + USER_ID_1001);
    assertNotNull(updatedCachedValue);
    final UserProfiles updatedCachedUser = (UserProfiles) updatedCachedValue.get();
    assertNotNull(updatedCachedUser);
    assertEquals(updatedCachedUser.getUserId(), updatedUserprofile.getUserId());
    assertEquals(updatedCachedUser.getUserAccExpiryDate(), updatedUserprofile.getUserAccExpiryDate());
    assertEquals(updatedCachedUser.getStatus(), updatedUserprofile.getStatus());
    assertEquals(updatedCachedUser.getCompany(), updatedUserprofile.getCompany());
  }

  /**
   * Test case to check if user cache evicted when update user if user have re-login.
   *
   * @throws Throwable Exception if any.
   */
  @Test
  public final void shouldEvictCachedUserprofileWhomRelogin() throws Throwable {
    final UserAccountStatus userStatus = mockUserAccountStatus(ACTIVE);
    final UserProfiles mockedUserProfile = new UserProfiles();
    mockedUserProfile.setUserId(USER_ID_1001);
    mockedUserProfile.setStatus(userStatus);
    Mockito.when(userProfileDao.getUser(USER_ID_1001)).thenReturn(mockedUserProfile);

    final UserProfiles userprofile = userProfileService.getUser(USER_ID_1001);
    Cache userCache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    final ValueWrapper cachedValue = userCache.get(CacheConstants.PREFIX_USER_ID + USER_ID_1001);
    assertNotNull(cachedValue);

    UserProfiles cachedUser = (UserProfiles) cachedValue.get();
    assertNotNull(cachedUser);
    assertEquals(cachedUser, userprofile);

    final UserProfiles updatedUserprofile = new UserProfiles();
    updatedUserprofile.setUserId(USER_ID_1001);
    updatedUserprofile.setUserAccExpiryDate(LocalDate.now());
    updatedUserprofile.setStatus(userStatus);
    updatedUserprofile.setLastLogin(LocalDateTime.now());
    Mockito.when(userProfileDao.updateUser(USER_ID_1001, USER_ID_1001, updatedUserprofile, null))
        .thenReturn(updatedUserprofile);
    Mockito.when(userProfileDao.getUser(USER_ID_1001)).thenReturn(updatedUserprofile);
    userProfileService.updateUser(USER_ID_1001, USER_ID_1001, updatedUserprofile, null);

    final ValueWrapper updatedCachedValue = userCache.get(CacheConstants.PREFIX_USER_ID + USER_ID_1001);
    assertNull(updatedCachedValue);
  }

  /**
   * Mock User Account Status.
   *
   * @return UserAccountStatus updated user account status.
   * @param statusEnum account status enumerator.
   */
  protected static final UserAccountStatus mockUserAccountStatus(final AccountStatusEnum statusEnum) {
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(statusEnum.getId());
    status.setName(statusEnum.getName());
    return status;
  }

  /**
   * Inner config.
   */
  @Configuration
  @EnableCaching
  @EnableAspectJAutoProxy
  public static class Config extends BaseMockConfiguration {
    /**
     * User Profile Cache Operation Processor.
     */
    @Autowired
    @Getter
    private UserCacheProcessor userCacheOperationProcessor;

    /**
     * Mock user profile service.
     *
     * @return mock object.
     */
    @Override
    public UserProfileService userProfileService() {
      return new UserProfileServiceImpl();
    }

    /**
     * Cache Manager.
     *
     * @return cache Manager.
     */
    @Bean
    public SimpleCacheManager cacheManager() {
      final SimpleCacheManager cacheManager = new SimpleCacheManager();
      final List<Cache> caches = new ArrayList<Cache>();
      caches.add(assetCacheBean().getObject());
      caches.add(userCacheBean().getObject());
      cacheManager.setCaches(caches);
      return cacheManager;
    }

    /**
     * Asset cache factory.
     *
     * @return asset cache factory.
     */
    @Bean
    public ConcurrentMapCacheFactoryBean assetCacheBean() {
      ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean();
      cacheFactoryBean.setName(CacheConstants.CACHE_ASSETS_BY_USER);
      return cacheFactoryBean;
    }

    /**
     * User cache factory.
     *
     * @return user cache factory.
     */
    @Bean
    public ConcurrentMapCacheFactoryBean userCacheBean() {
      ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean();
      cacheFactoryBean.setName(CacheConstants.CACHE_USERS);
      return cacheFactoryBean;
    }

    /**
     * @return Cache Operation Source.
     */
    @Bean
    public CacheOperationSource cacheOperationSource() {
      return new AnnotationCacheOperationSource();
    }

    /**
     * Returns User Profile Cache Operation Processor.
     *
     * @return User Profiles Cache Operation Processor.
     */
    @Bean(name = "userCacheProcessor")
    public UserCacheProcessor userProfileCacheOperationProcessor() {
      return new UserCacheProcessor();
    }

    /**
     * Returns Custom Cache Interceptor.
     *
     * @return Cache Interceptor.
     */
    @Bean
    public CacheInterceptor cacheInterceptor() {
      final ApplicationContext context = springContextUtil().getApplicationContext();
      final CustomCacheInterceptor interceptor = new CustomCacheInterceptor();
      interceptor.setCacheOperationSources(cacheOperationSource());
      interceptor.setCacheManager(cacheManager());
      interceptor.setApplicationContext(context);
      final Map<String, CacheOperationProcessor> processorMap = new LinkedHashMap<>();
      processorMap.put(CacheConstants.CACHE_USERS, (UserCacheProcessor) context.getBean("userCacheProcessor"));
      interceptor.setCustomCacheProcessorMap(processorMap);
      return interceptor;
    }

    /**
     * Initialize spring context utility.
     *
     * @return spring context utility.
     */
    @Bean
    public SpringContextUtil springContextUtil() {
      return new SpringContextUtil();
    }
  }
}

