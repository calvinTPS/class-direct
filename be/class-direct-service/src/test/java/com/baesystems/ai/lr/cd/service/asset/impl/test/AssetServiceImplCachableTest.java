package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

import org.hibernate.mapping.Map;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.security.RestrictedAssetAspect;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;

import retrofit2.mock.Calls;

/**
 * Unit test for AssetService with Cacheable to ensure Cacheable and Restricted Aspect works fine.
 *
 * @author YWearn
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AssetServiceImplCachableTest {

  /**
   * Mast asset id 1.
   */
  private static final Long MAST_ASSET_ID_1 = 1000001L;

  /**
   * Mast asset id 2.
   */
  private static final Long MAST_ASSET_ID_2 = 1000002L;

  /**
   * User id 1001.
   */
  private static final String USER_ID_1001 = "1001";

  /**
   * Injected userProfile service.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Injected asset service.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Injected subFleet service.
   */
  @Autowired
  private SubFleetService subFleetService;

  /**
   * Injects the mocked ihs service for test scenario.
   */
  @Autowired
  private IhsRetrofitService ihsServiceDelegate;


  /**
   * App context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * Injected aspect.
   */
  @Autowired
  private RestrictedAssetAspect assetAspect;

  /**
   * Should return restricted flag correctly after updated user's subfleets. The restricted aspect
   * should kick in even result returned from cache.
   *
   * @throws Throwable Exception if any.
   */
  @Test
  public final void shouldReturnAssetWithCorrectRestrictedFlag() throws Throwable {
    mockMastAssets(MAST_ASSET_ID_1, MAST_ASSET_ID_2);

    final UserProfiles userProfile = new UserProfiles();
    userProfile.setUserId(USER_ID_1001);
    final Roles adminRole = new Roles();
    adminRole.setRoleName(Role.LR_ADMIN.name());
    userProfile.setRoles(new HashSet<>(1));
    userProfile.getRoles().add(adminRole);
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(USER_ID_1001, Collections.emptyMap()));
    when(userProfileService.getUser(eq(USER_ID_1001))).thenReturn(userProfile);

    AssetHDto asset = null;

    asset = assetService.assetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertEquals(MAST_ASSET_ID_1, asset.getId());
    assertFalse(asset.getRestricted());

    final List<Long> subfleetList = new ArrayList<>(1);
    subfleetList.add(MAST_ASSET_ID_2);
    when(subFleetService.getSubFleetById(eq(USER_ID_1001))).thenReturn(subfleetList);

    asset = assetService.assetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertEquals(MAST_ASSET_ID_1, asset.getId());
    assertTrue(asset.getRestricted());


    verify(assetAspect, atLeastOnce()).checkAccessibleAsset(any(), any());
  }

  /**
   * Tests success scenario for {@link AssetService#getAssetDetailsByAsset(AssetHDto, String)}.
   *
   * @throws ClassDirectException if the ihs api call return error.
   */
  @Test
  public final void testGetAssetDetailsByAsset() throws ClassDirectException {
    final String userId = "ADMIN_1234";
    when(userProfileService.getUser(eq(userId))).thenReturn(mockAdminUserProfile(userId));

    final long assetId = 1L;
    final String imoNumber = "IMO0001";
    final String machineryClassNotation = "CLASS_NOTATION";
    final Integer hullIndicator = 1;
    final String descriptiveNote = "DESCRIPTIVE_NOTE";

    final IhsPrincipalDimensionsDto dimension = new IhsPrincipalDimensionsDto();
    when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber(eq(imoNumber))).thenReturn(Calls.response(dimension));

    final AssetHDto asset = new AssetHDto();
    asset.setId(assetId);
    asset.setCode(AssetCodeUtil.getMastCode(assetId));
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().setId(imoNumber);
    // For ruleset logic
    asset.setMachineryClassNotation(machineryClassNotation);
    asset.setHullIndicator(hullIndicator);
    asset.setDescriptiveNote(descriptiveNote);

    final AssetDetailsDto assetDetail = assetService.getAssetDetailsByAsset(asset, userId);

    Assert.assertNotNull(assetDetail);
    Assert.assertNotNull(assetDetail.getRulesetDetailsDto());
    assertEquals(asset.getMachineryClassNotation(), assetDetail.getRulesetDetailsDto().getMachineryNotation());
    assertEquals(asset.getHullIndicator(), assetDetail.getRulesetDetailsDto().getHullIndicator());
    assertEquals(asset.getDescriptiveNote(), assetDetail.getRulesetDetailsDto().getDescriptiveNote());
  }

  /**
   * Mocks admin user with given user id.
   *
   * @param id the user id.
   * @return the mocked user with admin role.
   */
  private UserProfiles mockAdminUserProfile(final String id) {
    final UserProfiles adminUser = new UserProfiles();
    final Roles adminRole = new Roles();
    adminRole.setRoleName(Role.LR_ADMIN.name());
    adminUser.setUserId(id);
    adminUser.setRoles(new HashSet<>(1));
    adminUser.getRoles().add(adminRole);
    adminUser.setRestrictAll(Boolean.FALSE);
    return adminUser;
  }


  /**
   * Mock multiple mast-asset.
   *
   * @param assetIds id of assets.
   */
  @SuppressWarnings("unchecked")
  private void mockMastAssets(final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setMastAsset(mockMastAsset(assetId));
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class),
          any(Integer.class), any(Integer.class)))
        .thenReturn(Calls.response(multiAssetPageResourceDto));

    final ServiceReferenceRetrofitService serviceReferenceRetrofit =
        context.getBean(ServiceReferenceRetrofitService.class);
    final List<ScheduledServiceHDto> serviceList = new ArrayList<>(0);
    when(serviceReferenceRetrofit.getAssetServicesQuery(any(Long.class),
        (java.util.Map<String, String>) (any(Map.class)))).thenReturn(Calls.response(serviceList));
  }


  /**
   * Mock single asset.
   *
   * @param assetId asset id.
   * @return asset.
   */
  private AssetLightDto mockMastAsset(final Long assetId) {
    final AssetLightDto asset = new AssetLightDto();
    asset.setId(assetId);
    asset.setName("Test Asset " + assetId);
    return asset;
  }

  /**
   * Inner config.
   */
  @Configuration
  @EnableCaching
  @EnableAspectJAutoProxy
  public static class Config extends BaseMockConfiguration {

    /**
     * Mock.
     *
     * @return mock object.
     */
    @Override
    public AssetService assetService() {
      return Mockito.spy(new AssetServiceImpl());
    }

    /**
     * Restricted asset aspect.
     *
     * @return aspect.
     */
    @Bean
    public RestrictedAssetAspect restrictedAssetAspect() {
      return Mockito.spy(new RestrictedAssetAspect());
    }

    /**
     * Cache Manager.
     *
     * @return cache Manager.
     */
    @Bean
    public SimpleCacheManager cacheManager() {
      final SimpleCacheManager cacheManager = new SimpleCacheManager();
      final List<Cache> caches = new ArrayList<Cache>();
      caches.add(assetCacheBean().getObject());
      caches.add(userCacheBean().getObject());
      cacheManager.setCaches(caches);
      return cacheManager;
    }

    /**
     * Asset cache factory.
     *
     * @return asset cache factory.
     */
    @Bean
    public ConcurrentMapCacheFactoryBean assetCacheBean() {
      ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean();
      cacheFactoryBean.setName(CacheConstants.CACHE_ASSETS_BY_USER);
      return cacheFactoryBean;
    }

    /**
     * User cache factory.
     *
     * @return user cache factory.
     */
    @Bean
    public ConcurrentMapCacheFactoryBean userCacheBean() {
      ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean();
      cacheFactoryBean.setName(CacheConstants.CACHE_USERS);
      return cacheFactoryBean;
    }

  }
}
