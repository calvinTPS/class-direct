package com.baesystems.ai.lr.cd.service.reference.impl.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import retrofit2.mock.Calls;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.reference.CustomerReferenceService;
import com.baesystems.ai.lr.cd.service.reference.impl.CustomerReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * Provides unit tests for {@link CustomerReferenceServiceImpl}.
 *
 * @author YWearn
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CustomerReferenceServiceTest.Config.class)
public class CustomerReferenceServiceTest {

  /**
   * Injects the {@link CustomerReferenceService} for testing.
   */
  @Autowired
  private CustomerReferenceService customerReferenceService;

  /**
   * Injects the {@link AssetRetrofitService} for testing.
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceRetrofitService;

  /**
   * The country list test data.
   */
  private static final String[] COUNTRY_LIST = new String[] {"Malaysia", "UK", "US" };

  /**
   * Tests successfully retrieve list of country reference data.
   * @throws ClassDirectException if the external API hits error.
   */
  @Test
  public final void retrieveCountryListTest() throws ClassDirectException {

    Mockito.when(assetReferenceRetrofitService.getCountryList()).thenReturn(Calls.response(mockCountryList()));

    final List<CountryHDto> countryList = customerReferenceService.getCountries();

    Assert.assertNotNull(countryList);
    Assert.assertEquals(COUNTRY_LIST.length, countryList.size());
  }

  /**
   * Tests success scenario to retrieve single country with id.
   * @throws ClassDirectException if the external API hits error.
   */
  @Test
  public final void retrieveSingleCountryTest() throws ClassDirectException {
    final int testCountryId = 0;

    Mockito.when(assetReferenceRetrofitService.getCountryList()).thenReturn(Calls.response(mockCountryList()));

    final CountryHDto country = customerReferenceService.getCountry(testCountryId);

    Assert.assertNotNull(country);
    Assert.assertEquals(COUNTRY_LIST[testCountryId], country.getName());
  }

  /**
   * Tests error scenario when retrieve single country. Should return null instead of error.
   * @throws ClassDirectException if the external API hits error.
   */
  @Test
  public final void retrieveSingleCountryErrorTest() throws ClassDirectException {
    final int testCountryId = 0;

    Mockito.when(assetReferenceRetrofitService.getCountryList())
      .thenReturn(Calls.failure(new IOException("Mock Error.")));

    final CountryHDto country = customerReferenceService.getCountry(testCountryId);

    Assert.assertNull(country);
  }


  /**
   * Provides mocked country reference data for testing.
   * @return list of mocked country reference data.
   */
  protected final List<CountryHDto> mockCountryList() {
    final List<CountryHDto> countryList = new ArrayList<>(COUNTRY_LIST.length);
    long index = 0;

    for (String countryName : COUNTRY_LIST) {
      final CountryHDto country = new CountryHDto();
      country.setId(index++);
      country.setName(countryName);
      countryList.add(country);
    }

    return countryList;
  }


  /**
   * Provides Spring in-class configuration for unit test.
   *
   * @author YWearn
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * @see com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration#customerReferenceService()
     */
    @Override
    public CustomerReferenceService customerReferenceService() {
     return new CustomerReferenceServiceImpl();
    }

  }

}
