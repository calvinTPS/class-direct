package com.baesystems.ai.lr.cd.service.test;

/**
 * @author Faizal Sidek
 */
public enum UserId {
  /**
   * Reference to Alice user.
   */
  ALICE("d5aac4be-2714-43b7-a18e-cc27c1dff473"), BOB("bf5e932e-7b97-4451-93ea-8af1ee916155");

  /**
   * Id of the user.
   */
  private String id;

  /**
   * Private constructor.
   *
   * @param userId id of user.
   */
  UserId(final String userId) {
    this.id = userId;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return id.
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for {@link #id}.
   *
   * @param userId id of the user.
   */
  public void setId(final String userId) {
    this.id = userId;
  }
}
