package com.baesystems.ai.lr.cd.be.utils.test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.RepeatOfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.utils.FutureDueDateEngine;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.enums.ProductType;
import com.baesystems.ai.lr.enums.ServiceGroup;
import com.baesystems.ai.lr.enums.ServiceType;

/**
 * @author yng
 *
 */
public final class FutureDueDateEngineTest {

  /**
   * 2017.
   */
  private static final int TWO_ZERO_ONE_SEVEN = 2017;

  /**
   * 2016.
   */
  private static final int TWO_ZERO_ONE_SIX = 2016;

  /**
   * 2012.
   */
  private static final int TWO_ZERO_ONE_TWO = 2012;

  /**
   * 2010.
   */
  private static final int TWO_ZERO_ONE_ZERO = 2010;

  /**
   * 12.
   */
  private static final long TWELVE = 12L;

  /**
   * 3.
   */
  private static final long THREE = 3L;

  /**
   * 4.
   */
  private static final long FOUR = 4L;

  /**
   * 5.
   */
  private static final long FIVE = 5L;

  /**
   * 6.
   */
  private static final long SIX = 6L;

  /**
   * 7.
   */
  private static final long SEVEN = 7L;

  /**
   * 8.
   */
  private static final long EIGHT = 8L;

  /**
   * 9.
   */
  private static final long NINE = 9L;

  /**
   * 10.
   */
  private static final long TEN = 10L;

  /**
   * 11.
   */
  private static final long ELEVEN = 11L;

  /**
   * 24.
   */
  private static final long TWENTY_FOUR = 24L;

  /**
   * 25.
   */
  private static final long TWENTY_FIVE = 25L;

  /**
   * 31.
   */
  private static final long THIRTY_ONE = 31L;

  /**
   * 36.
   */
  private static final long THIRTY_SIX = 36L;

  /**
   * 37.
   */
  private static final long THIRTEEN = 13L;

  /**
   * 60.
   */
  private static final long SIXTY = 60L;

  /**
   * Test main calculate method.
   *
   * @throws CloneNotSupportedException value.
   */
  @Test
  public void testCalculate() throws CloneNotSupportedException {

    final List<RepeatOfDto> result = FutureDueDateEngine.calculate(scheduledServiceData(),
        new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());

    Assert.assertEquals(THIRTY_ONE, result.size());
  }

  /**
   * Test calculate rule 1.
   *
   * @throws CloneNotSupportedException value.
   */
  @Test
  public void testCalculateRule1() throws CloneNotSupportedException {

    // create max limit test case
    final Date maxLimit = new GregorianCalendar(TWO_ZERO_ONE_SEVEN, 1, 1).getTime();

    // create data test case
    final ScheduledServiceHDto data = new ScheduledServiceHDto();
    data.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data.setCyclePeriodicity((int) TWELVE);

    final ScheduledServiceHDto data1 = new ScheduledServiceHDto();
    data1.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 2, 1).getTime());
    data1.setCyclePeriodicity((int) TWELVE);

    List<ScheduledServiceHDto> dataList = new ArrayList<ScheduledServiceHDto>();
    dataList.add(data);
    dataList.add(data1);

    List<RepeatOfDto> output = FutureDueDateEngine.calculateRule1(dataList, maxLimit);
    Assert.assertEquals(THIRTEEN, output.size());

    // create data test case
    List<ScheduledServiceHDto> dataList1 = new ArrayList<ScheduledServiceHDto>();
    data.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 2).getTime());
    dataList1.add(data);

    output = FutureDueDateEngine.calculateRule1(dataList1, maxLimit);
    Assert.assertEquals(SIX, output.size());
  }

  /**
   * Test calculate rule 2.
   *
   * @throws CloneNotSupportedException value.
   */
  @Test
  public void testCalculateRule2() throws CloneNotSupportedException {

    // create max limit test case
    final Date maxLimit = new GregorianCalendar(TWO_ZERO_ONE_SEVEN, 1, 1).getTime();

    // create data test case
    List<ScheduledServiceHDto> dataList = new ArrayList<ScheduledServiceHDto>();
    final ScheduledServiceHDto data = new ScheduledServiceHDto();
    data.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data.setCyclePeriodicity((int) TWELVE);
    dataList.add(data);

    // create renewal due date list test case
    final List<Date> renewalDueDateList = new ArrayList<>();
    renewalDueDateList.add(new GregorianCalendar(TWO_ZERO_ONE_TWO, 1, 1).getTime());
    renewalDueDateList.add(new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());
    renewalDueDateList.add(new GregorianCalendar(TWO_ZERO_ONE_SEVEN, 1, 1).getTime());

    final List<RepeatOfDto> output =
        FutureDueDateEngine.calculateRule2(dataList, renewalDueDateList, maxLimit);
    Assert.assertEquals(2, output.size());
  }


  /**
   * Test create group list.
   */
  @Test
  public void testCreateGroupList() {
    final List<ScheduledServiceHDto> data = scheduledServiceData();

    // LRCD-3393:Inconsistency in appearance of second cycle of machinery items in service schedule
    // test to check if services has same service group id,product type id and service type id then those should
    // be added in List<ScheduledServiceHDto>
    final Map<Long, Map<Long, Map<Long, List<ScheduledServiceHDto>>>> groupedList =
        FutureDueDateEngine.createGroupList(data);

    Assert.assertEquals(FOUR, groupedList.size());

    Assert.assertEquals(1L, groupedList.get(ServiceGroup.MACHINERY.getValue()).size());
    Assert.assertEquals(THREE, groupedList.get(ServiceGroup.MACHINERY.getValue())
        .get(ProductType.CLASSIFICATION.getValue()).size());

    Assert.assertEquals(1L, groupedList.get(ServiceGroup.HULL.getValue()).size());
    Assert.assertEquals(THREE,
        groupedList.get(ServiceGroup.HULL.getValue()).get(ProductType.STATUTORY.getValue()).size());

    Assert.assertEquals(1L,
        groupedList.get(ServiceGroup.BOILERS_STEAMPIPES_AND_PROPULSION.getValue()).size());
    Assert.assertEquals(1L,
        groupedList.get(ServiceGroup.BOILERS_STEAMPIPES_AND_PROPULSION.getValue())
        .get(ProductType.CLASSIFICATION.getValue()).size());

    Assert.assertEquals(1L, groupedList.get(ServiceGroup.CHEMICAL_CODE_HSSC.getValue()).size());
    Assert.assertEquals(2L, groupedList.get(ServiceGroup.CHEMICAL_CODE_HSSC.getValue())
        .get(ProductType.MMS.getValue()).size());
  }

  /**
   * Test calculation for product type classfication.
   *
   * @throws CloneNotSupportedException value.
   */
  @Test
  public void testCalculateForProductTypeClassfication() throws CloneNotSupportedException {
    // create sample test data
    final List<ScheduledServiceHDto> data = scheduledServiceData();
    final Map<Long, List<ScheduledServiceHDto>> testList1 = new TreeMap<>();

    testList1.put(ServiceType.ANNUAL.value(), data.subList(0, 1));
    testList1.put(ServiceType.INTERMEDIATE.value(), data.subList(1, 2));
    testList1.put(ServiceType.RENEWAL.value(), data.subList(2, 3));
    testList1.put(ServiceType.STANDALONE.value(), data.subList(6, 7));

    final List<RepeatOfDto> response =
        FutureDueDateEngine.calculateForProductTypeClassfication(testList1,
            new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());

    Assert.assertEquals(THIRTEEN, response.size());


  }

  /**
   * Test calculation for product type MMS.
   *
   * @throws CloneNotSupportedException value.
   */
  @Test
  public void testCalculateForProductTypeMMS() throws CloneNotSupportedException {
    // create sample test data
    final List<ScheduledServiceHDto> data = scheduledServiceData();
    final Map<Long, List<ScheduledServiceHDto>> testList1 = new TreeMap<>();

    testList1.put(ServiceType.PERIODIC.value(), data.subList(6, 7));
    testList1.put(ServiceType.RENEWAL.value(), data.subList(7, 8));

    final List<RepeatOfDto> response = FutureDueDateEngine.calculateForProductTypeMMS(
        testList1, new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());

    Assert.assertEquals(TWELVE, response.size());
  }

  /**
   * Test calculation for product type statutory.
   *
   * @throws CloneNotSupportedException value.
   */
  @Test
  public void testCalculateForProductTypeStatutory() throws CloneNotSupportedException {
    // create sample test data
    final List<ScheduledServiceHDto> data = scheduledServiceData();
    final Map<Long, List<ScheduledServiceHDto>> testList1 = new TreeMap<>();

    testList1.put(ServiceType.ANNUAL.value(), data.subList(3, 4));
    testList1.put(ServiceType.INTERMEDIATE.value(), data.subList(4, 5));
    testList1.put(ServiceType.RENEWAL.value(), data.subList(5, 6));

    final List<RepeatOfDto> response = FutureDueDateEngine.calculateForProductTypeStatutory(
        testList1, new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());

    Assert.assertEquals(SIX, response.size());
  }

  /**
   * Test for due date update.
   *
   * @throws CloneNotSupportedException value.
   */
  @Test
  public void testCreateNewDueDateObject() throws CloneNotSupportedException {
    // create test data
    final ScheduledServiceHDto data = new ScheduledServiceHDto();
    data.setId(1L);
    data.setServiceCatalogueH(new ServiceCatalogueHDto());
    data.setLowerRangeDate(new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());
    data.setUpperRangeDate(new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());
    data.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_SIX, 1, 1).getTime());

    // case: test with both offset null
    data.getServiceCatalogueH().setLowerRangeDateOffsetMonths(null);
    data.getServiceCatalogueH().setUpperRangeDateOffsetMonths(null);

    RepeatOfDto result =
        FutureDueDateEngine.createNewDueDateObject(data, LocalDate.of(TWO_ZERO_ONE_SEVEN, 1, 1));

    Assert.assertEquals(new GregorianCalendar(TWO_ZERO_ONE_SEVEN, 0, 1).getTime(),
        result.getDueDate());
    Assert.assertNull(result.getLowerRangeDate());
    Assert.assertNull(result.getUpperRangeDate());

    // case: test with both offset with value
    data.getServiceCatalogueH().setLowerRangeDateOffsetMonths((int) THREE);
    data.getServiceCatalogueH().setUpperRangeDateOffsetMonths((int) THREE);

    result =
        FutureDueDateEngine.createNewDueDateObject(data, LocalDate.of(TWO_ZERO_ONE_SEVEN, 1, 1));

    Assert.assertEquals(new GregorianCalendar(TWO_ZERO_ONE_SEVEN, 0, 1).getTime(),
        result.getDueDate());
    Assert.assertEquals(new GregorianCalendar(TWO_ZERO_ONE_SIX, (int) NINE, 2).getTime(),
        result.getLowerRangeDate());
    Assert.assertEquals(
        new GregorianCalendar(TWO_ZERO_ONE_SEVEN, 2, (int) THIRTY_ONE).getTime(),
        result.getUpperRangeDate());
    Assert.assertEquals(result.getDueStatusH(), DueStatus.NOT_DUE);
  }


  /**
   * Create sample scheduled service data.
   *
   * @return list of sample data.
   */
  public List<ScheduledServiceHDto> scheduledServiceData() {

    // create test case with mapping:
    // ServiceGroup | ProductType | ServiceType
    // Machinery | Classification | Annual
    // Machinery | Classification | Intermediate
    // Machinery | Classification | Renewal
    //
    // Hull | Statutory | Annual
    // Hull | Statutory | Intermediate
    // Hull | Statutory | Renewal
    //
    // Boiler | Classification | Standalone
    //
    // Chemical | MMS | Periodic
    // Chemical | MMS | Renewal
    //
    // null | Classification | Annual
    // Machinery | null | Annual
    // Machinery | Classification | null

    final ScheduledServiceHDto data1 = new ScheduledServiceHDto();
    data1.setId(1L);
    data1.setServiceCatalogueH(new ServiceCatalogueHDto());
    data1.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.MACHINERY.getValue()));
    data1.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data1.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.CLASSIFICATION.getValue()));
    data1.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.ANNUAL.value()));
    data1.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data1.setCyclePeriodicity((int) TWELVE);

    final ScheduledServiceHDto data2 = new ScheduledServiceHDto();
    data2.setId(2L);
    data2.setServiceCatalogueH(new ServiceCatalogueHDto());
    data2.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.MACHINERY.getValue()));
    data2.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data2.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.CLASSIFICATION.getValue()));
    data2.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.INTERMEDIATE.value()));
    data2.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data2.setCyclePeriodicity((int) THIRTY_SIX);

    final ScheduledServiceHDto data3 = new ScheduledServiceHDto();
    data3.setId(THREE);
    data3.setServiceCatalogueH(new ServiceCatalogueHDto());
    data3.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.MACHINERY.getValue()));
    data3.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data3.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.CLASSIFICATION.getValue()));
    data3.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.RENEWAL.value()));
    data3.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data3.setCyclePeriodicity((int) SIXTY);



    final ScheduledServiceHDto data4 = new ScheduledServiceHDto();
    data4.setId(FOUR);
    data4.setServiceCatalogueH(new ServiceCatalogueHDto());
    data4.getServiceCatalogueH().setServiceGroup(new LinkResource(ServiceGroup.HULL.getValue()));
    data4.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data4.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.STATUTORY.getValue()));
    data4.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.ANNUAL.value()));
    data4.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data4.setCyclePeriodicity((int) TWELVE);

    final ScheduledServiceHDto data5 = new ScheduledServiceHDto();
    data5.setId(FIVE);
    data5.setServiceCatalogueH(new ServiceCatalogueHDto());
    data5.getServiceCatalogueH().setServiceGroup(new LinkResource(ServiceGroup.HULL.getValue()));
    data5.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data5.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.STATUTORY.getValue()));
    data5.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.INTERMEDIATE.value()));
    data5.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data5.setCyclePeriodicity((int) TWELVE);

    final ScheduledServiceHDto data6 = new ScheduledServiceHDto();
    data6.setId(SIX);
    data6.setServiceCatalogueH(new ServiceCatalogueHDto());
    data6.getServiceCatalogueH().setServiceGroup(new LinkResource(ServiceGroup.HULL.getValue()));
    data6.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data6.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.STATUTORY.getValue()));
    data6.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.RENEWAL.value()));
    data6.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_TWO, 1, 1).getTime());
    data6.setCyclePeriodicity((int) TWENTY_FOUR);



    final ScheduledServiceHDto data7 = new ScheduledServiceHDto();
    data7.setId(SEVEN);
    data7.setServiceCatalogueH(new ServiceCatalogueHDto());
    data7.getServiceCatalogueH().setServiceGroup(
        new LinkResource(ServiceGroup.BOILERS_STEAMPIPES_AND_PROPULSION.getValue()));
    data7.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data7.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.CLASSIFICATION.getValue()));
    data7.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.STANDALONE.value()));
    data7.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data7.setCyclePeriodicity((int) TWELVE);



    final ScheduledServiceHDto data8 = new ScheduledServiceHDto();
    data8.setId(EIGHT);
    data8.setServiceCatalogueH(new ServiceCatalogueHDto());
    data8.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.CHEMICAL_CODE_HSSC.getValue()));
    data8.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data8.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.MMS.getValue()));
    data8.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.PERIODIC.value()));
    data8.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data8.setCyclePeriodicity((int) TWELVE);

    final ScheduledServiceHDto data9 = new ScheduledServiceHDto();
    data9.setId(NINE);
    data9.setServiceCatalogueH(new ServiceCatalogueHDto());
    data9.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.CHEMICAL_CODE_HSSC.getValue()));
    data9.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data9.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.MMS.getValue()));
    data9.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.RENEWAL.value()));
    data9.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data9.setCyclePeriodicity((int) TWENTY_FOUR);



    final ScheduledServiceHDto data10 = new ScheduledServiceHDto();
    data10.setId(TEN);
    data10.setServiceCatalogueH(new ServiceCatalogueHDto());
    data10.getServiceCatalogueH().setServiceGroup(null);
    data10.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data10.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.CLASSIFICATION.getValue()));
    data10.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.ANNUAL.value()));

    final ScheduledServiceHDto data11 = new ScheduledServiceHDto();
    data11.setId(ELEVEN);
    data11.setServiceCatalogueH(new ServiceCatalogueHDto());
    data11.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.MACHINERY.getValue()));
    data11.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data11.getServiceCatalogueH().getProductCatalogue().setProductType(null);
    data11.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.ANNUAL.value()));

    final ScheduledServiceHDto data12 = new ScheduledServiceHDto();
    data12.setId(TWELVE);
    data12.setServiceCatalogueH(new ServiceCatalogueHDto());
    data12.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.MACHINERY.getValue()));
    data12.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data12.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.CLASSIFICATION.getValue()));
    data12.getServiceCatalogueH().setServiceType(null);

    final ScheduledServiceHDto data13 = new ScheduledServiceHDto();
    data13.setId(THIRTEEN);
    data13.setServiceCatalogueH(new ServiceCatalogueHDto());
    data13.getServiceCatalogueH()
    .setServiceGroup(new LinkResource(ServiceGroup.MACHINERY.getValue()));
    data13.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    data13.getServiceCatalogueH().getProductCatalogue()
    .setProductType(new LinkResource(ProductType.CLASSIFICATION.getValue()));
    data13.getServiceCatalogueH().setServiceType(new LinkResource(ServiceType.ANNUAL.value()));
    data13.setDueDate(new GregorianCalendar(TWO_ZERO_ONE_ZERO, 1, 1).getTime());
    data13.setCyclePeriodicity((int) TWELVE);


    final List<ScheduledServiceHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);
    data.add(data4);
    data.add(data5);
    data.add(data6);
    data.add(data7);
    data.add(data8);
    data.add(data9);
    data.add(data10);
    data.add(data11);
    data.add(data12);
    data.add(data13);

    return data;
  }
}


