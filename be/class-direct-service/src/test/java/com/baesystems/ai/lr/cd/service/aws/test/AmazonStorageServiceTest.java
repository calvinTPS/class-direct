package com.baesystems.ai.lr.cd.service.aws.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManager;
import com.amazonaws.services.glacier.transfer.UploadResult;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.aws.AWSHelper;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.aws.impl.AmazonStorageServiceImpl;

/**
 * Provides Unit test methods for {@link com.baesystems.ai.lr.cd.service.aws.AmazonStorageService}.
 *
 * @author Faizal Sidek
 */
@RunWith(MockitoJUnitRunner.class)
public class AmazonStorageServiceTest {

  /**
   * The Default String region value is {@value #DEFAULT_REGION}.
   */
  private static final String DEFAULT_REGION = Regions.DEFAULT_REGION.toString();

  /**
   * The Default String bucket value is {@value #DEFAULT_BUCKET}.
   */
  private static final String DEFAULT_BUCKET = "lr-classdirect";

  /**
   * The Default String S3 file key value is {@value #S3_FILE_KEY}.
   */
  private static final String S3_FILE_KEY = "user101/test.pdf";

  /**
   * The Default String user id value is {@value #USER_ID}.
   */
  private static final String USER_ID = "37be2b5b-63ee-4535-bca4-202d556d626e";

  /**
   * The Default String sample file path value is {@value #SAMPLE_FILE_PATH}.
   */
  private static final String SAMPLE_FILE_PATH = "/tmp/classdirect/test.pdf";

  /**
   * The Default String VAULT value is {@value #DEFAULT_VAULT_NAME}.
   */
  private static final String DEFAULT_VAULT_NAME = "ClassDirectArchive";

  /**
   * The Default String sample description value is {@value #SAMPLE_DESCRIPTION}.
   */
  private static final String SAMPLE_DESCRIPTION =
      "objectID=123; 2017-02-21 10:55; https://classdirect-test.marine.mast.ids/api/download-file?token=1234567";

  /**
   * The Default String file name value is {@value #FILE_NAME}.
   */
  private static final String FILE_NAME = SAMPLE_FILE_PATH.substring(SAMPLE_FILE_PATH.lastIndexOf('/') + 1);

  /**
   * The {@link AmazonStorageService} from Spring context.
   */
  @InjectMocks
  private AmazonStorageService amazonStorageService = new AmazonStorageServiceImpl();

  /**
   * The {@link AWSHelper} from Spring context..
   */
  @Mock
  private AWSHelper awsHelper;

  /**
   * The {@link SecurityContext} from test Spring context.
   */
  private SecurityContext securityCtx;

  /**
   * This method will provide prerequisite mocking of user authentication.
   */
  @Before
  public void setUp() {
    securityCtx = SecurityContextHolder.getContext();
    SecurityContext context = new SecurityContextImpl();
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    CDAuthToken token = new CDAuthToken(listOfRoles);

    token.setDetails(user);
    token.setAuthenticated(true);
    context.setAuthentication(token);
    SecurityContextHolder.setContext(context);
  }


  /**
   * Tear Down.
   */
  @After
  public void tearDown() {
    SecurityContextHolder.setContext(securityCtx);
  }

  /**
   * Tests success scenario for
   * {@link AmazonStorageService#uploadFile(String, String, String, String)}.encrypts and encodes to
   * a S3 string token to upload to S3 region according to {@link com.amazonaws.regions.Regions}.
   *
   * @throws ClassDirectException if any padding,algorithm and invalid key exception occurs.
   */
  @Test
  public final void successfullyUploadToS3() throws ClassDirectException {
    final AmazonS3 client = mock(AmazonS3.class);
    when(awsHelper.createS3Client(any(), anyString())).thenReturn(client);

    final String token =
        amazonStorageService.uploadFile(DEFAULT_REGION, DEFAULT_BUCKET, S3_FILE_KEY, SAMPLE_FILE_PATH, USER_ID);
    assertNotNull(token);
    verify(client, atLeastOnce()).putObject(eq(DEFAULT_BUCKET), eq(S3_FILE_KEY), any(File.class));
  }

  /**
   * Tests success scenario for {@link AmazonStorageService#uploadFile(String, String, String)}
   * .encrypts and encodes to a S3 string token to upload to S3 with default region.
   *
   * @throws ClassDirectException if any padding,algorithm and invalid key exception occurs.
   */
  @Test
  public final void successfullyUploadToS3WithDefaultRegion() throws ClassDirectException {
    final AmazonS3 client = mock(AmazonS3.class);
    when(awsHelper.createS3Client(any(), anyString())).thenReturn(client);
    final String token = amazonStorageService.uploadFile(DEFAULT_BUCKET, S3_FILE_KEY, SAMPLE_FILE_PATH, USER_ID);
    assertNotNull(token);
    verify(client, atLeastOnce()).putObject(eq(DEFAULT_BUCKET), eq(S3_FILE_KEY), any(File.class));
  }

  /**
   * Tests success scenario for {@link AmazonStorageService#downloadFile(String, String, String)}
   * .decrypts and decode to proper S3 file token to download file from S3 region according to
   * {@link com.amazonaws.regions.Regions}.
   *
   * @throws ClassDirectException if any padding,algorithm and invalid key exception occurs.
   * @throws IOException if any API error occurs.
   */
  @Test
  public final void successfullyDownloadFromS3() throws ClassDirectException, IOException {
    final AmazonS3 client = mock(AmazonS3.class);
    final S3Object s3Object = mock(S3Object.class);
    when(client.getObject(eq(DEFAULT_BUCKET), eq(S3_FILE_KEY))).thenReturn(s3Object);
    when(awsHelper.createS3Client(any(), anyString())).thenReturn(client);
    when(awsHelper.s3ObjectToByteArray(anyObject())).thenReturn(new byte[] {});

    final byte[] contents = amazonStorageService.downloadFile(DEFAULT_REGION, DEFAULT_BUCKET, S3_FILE_KEY);
    assertNotNull(contents);
    verify(awsHelper, atLeastOnce()).s3ObjectToByteArray(anyObject());
    verify(awsHelper, atLeastOnce()).createS3Client(any(), eq(DEFAULT_REGION));
    verify(client, atLeastOnce()).getObject(eq(DEFAULT_BUCKET), eq(S3_FILE_KEY));
  }

  /**
   * Tests success scenario for
   * {@link AmazonStorageService#archiveFile(String, String, String, byte[])}.Archives the file to
   * glacier.
   *
   * @throws ClassDirectException when upload fails.
   * @throws IOException if any API error occurs.
   */
  @Test
  public final void successfullyArchiveFile() throws ClassDirectException, IOException {
    final ArchiveTransferManager transferManager = mock(ArchiveTransferManager.class);
    final UploadResult uploadResult = mock(UploadResult.class);
    when(uploadResult.getArchiveId()).thenReturn("123456");
    when(transferManager.upload(eq(DEFAULT_VAULT_NAME), eq(SAMPLE_DESCRIPTION), anyObject())).thenReturn(uploadResult);
    when(awsHelper.createTransferManager(any(), anyString())).thenReturn(transferManager);

    final String uploadId =
        amazonStorageService.archiveFile(DEFAULT_VAULT_NAME, SAMPLE_DESCRIPTION, FILE_NAME, new byte[] {});
    assertNotNull(uploadId);
    verify(awsHelper, atLeastOnce()).createTransferManager(any(), anyString());
    verify(awsHelper, atLeastOnce()).writeByteArrayToFile(any(), any());
    verify(transferManager, atLeastOnce()).upload(eq(DEFAULT_VAULT_NAME), eq(SAMPLE_DESCRIPTION), anyObject());
  }
}
