/**
 * Simple DTOs meant for testing purpose only.
 * Created by PFauchon on 21/11/2016.
 */
package com.baesystems.ai.lr.cd.be.processor.test.dtos;
