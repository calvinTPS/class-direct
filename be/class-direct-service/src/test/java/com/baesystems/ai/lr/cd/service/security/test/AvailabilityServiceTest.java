package com.baesystems.ai.lr.cd.service.security.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AvailabilityDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.SystemMaintenance;
import com.baesystems.ai.lr.cd.service.security.AvailabilityService;
import com.baesystems.ai.lr.cd.service.security.impl.AvailabilityServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.service.security.AvailabilityService}.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AvailabilityServiceTest {

  /**
   * Inject availability service.
   */
  @Autowired
  private AvailabilityService availabilityService;

  /**
   * Inject dao.
   */
  @Autowired
  private AvailabilityDao availabilityDao;

  /**
   * Should returns false when system is not available.
   */
  @Test
  public final void shouldReturnFalseWhenSystemIsNotAvailable() {
    when(availabilityDao.getActiveSystemMaintenance()).thenReturn(new SystemMaintenance());

    availabilityService.checkForServiceAvailability();
    assertFalse(availabilityService.isServiceAvailable());
  }

  /**
   * Should returns true when system is available.
   */
  @Test
  public final void shouldReturnTrueWhenSystemIsAvailable() {
    when(availabilityDao.getActiveSystemMaintenance()).thenReturn(null);

    availabilityService.checkForServiceAvailability();
    assertTrue(availabilityService.isServiceAvailable());
  }

  /**
   * Inner config.
   */
  @Configuration
  public static class Config {

    /**
     * Returns mock dao.
     *
     * @return dao.
     */
    @Bean
    public AvailabilityDao availabilityDao() {
      return mock(AvailabilityDao.class);
    }

    /**
     * Returns real service.
     *
     * @return service.
     */
    @Bean
    public AvailabilityService availabilityService() {
      return new AvailabilityServiceImpl();
    }
  }
}
