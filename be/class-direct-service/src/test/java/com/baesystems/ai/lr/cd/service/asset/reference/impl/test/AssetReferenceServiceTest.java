package com.baesystems.ai.lr.cd.service.asset.reference.impl.test;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BuilderHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ServiceCreditStatusRefHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.asset.reference.impl.AssetReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.cache.CacheContainer;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for asset reference service {@link AssetReferenceService}.
 *
 * @author msidek
 * @author syalavarthi 15-06-2017.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AssetReferenceServiceTest.Config.class)
public class AssetReferenceServiceTest {

  /**
   * The spring application context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link AssetReferenceService} from spring context.
   *
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link AssetReferenceRetrofitService} from spring context.
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceRetrofitService;

  /**
   * The mock object to initialize service.
   *
   */
  @Before
  public void init() {
    reset(context.getBean(AssetReferenceRetrofitService.class));
  }

  /**
   * Tests success scenario to get list of asset types {@link AssetReferenceService#getAssetTypes()}
   * mast api call success.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void successfullyRetrieveListOfAssetTypes() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getAssetTypeDtos()).thenReturn(Calls.response(mockAssetTypes()));

    final List<AssetTypeHDto> assetTypes = assetReferenceService.getAssetTypes();
    Assert.assertNotNull(assetTypes);
    Assert.assertNotEquals(0, assetTypes.size());
  }

  /**
   * Tests failure scenario to get list of asset types {@link AssetReferenceService#getAssetTypes()}
   * when mast api call fail.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void failedToRetriveListOfAssetTypes() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    when(retrofitService.getAssetTypeDtos()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<AssetTypeHDto> assetTypes = assetReferenceService.getAssetTypes();
    Assert.assertNotNull(assetTypes);
    Assert.assertEquals(0, assetTypes.size());
  }

  /**
   * Tests success scenario to get asset type by id
   * {@link AssetReferenceService#getAssetType(Long)}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void successfullyGetAssetType() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAssetTypeDtos()).thenReturn(Calls.response(mockAssetTypes()));


    final AssetTypeHDto assetType = assetReferenceService.getAssetType(1234L);
    Assert.assertNotNull(assetType);
  }

  /**
   * Tests failure scenario to get asset type by id empty result
   * {@link AssetReferenceService#getAssetType(Long)} when mast api call fail.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void emptyResultForGetAssetType() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAssetTypeDtos()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final AssetTypeHDto assetType = assetReferenceService.getAssetType(1234L);
    Assert.assertNull(assetType);
  }

  /**
   * Tests success scenario to get asset catagories list
   * {@link AssetReferenceService#getAssetCategories()}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void successfullyGetAssetCategories() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAssetCategories()).thenReturn(Calls.response(mockAssetCategories()));

    final List<AssetCategoryHDto> assetCategories = assetReferenceService.getAssetCategories();
    Assert.assertNotNull(assetCategories);
    Assert.assertNotEquals(0, assetCategories.size());
  }

  /**
   * Tests failure scenario to get asset catagories list
   * {@link AssetReferenceService#getAssetCategories()} when mast api call fail.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void emptyResultForGetAssetCategories() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAssetCategories()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<AssetCategoryHDto> assetCategories = assetReferenceService.getAssetCategories();
    Assert.assertNotNull(assetCategories);
    Assert.assertEquals(0, assetCategories.size());
  }

  /**
   * Tests success scenario to get asset category
   * {@link AssetReferenceService#getAssetCategory(Long)}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void successfullyGetAssetCategory() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAssetCategories()).thenReturn(Calls.response(mockAssetCategories()));

    final AssetCategoryHDto assetCategory = assetReferenceService.getAssetCategory(123L);
    Assert.assertNotNull(assetCategory);
  }

  /**
   * Tests failure scenario to get asset category
   * {@link AssetReferenceService#getAssetCategory(Long)}. when mast api call fail.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void emptyResultGetAssetCategory() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAssetCategories()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final AssetCategoryHDto assetCategory = assetReferenceService.getAssetCategory(123L);
    Assert.assertNull(assetCategory);
  }

  /**
   * Tests success scenario to get list of class statuses
   * {@link AssetReferenceService#getClassStatuses()}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void successfullyGetClassStatuses() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getClassStatus()).thenReturn(Calls.response(mockClassStatuses()));


    final List<ClassStatusHDto> classStatuses = assetReferenceService.getClassStatuses();
    Assert.assertNotNull(classStatuses);
    Assert.assertFalse(classStatuses.isEmpty());
  }

  /**
   * Tests failure scenario to get empty list of class statuses
   * {@link AssetReferenceService#getClassStatuses()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void emptyResultWhenGetClassStatuses() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getClassStatus()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<ClassStatusHDto> classStatuses = assetReferenceService.getClassStatuses();
    Assert.assertNotNull(classStatuses);
    Assert.assertTrue(classStatuses.isEmpty());
  }

  /**
   * Tests failure scenario to get empty list of class statuses
   * {@link AssetReferenceService#getClassStatuses()} with null.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public void getClassStatusesNull() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getClassStatus()).thenReturn(Calls.response(mockClassStatusesNull()));

    final List<ClassStatusHDto> classStatuses = assetReferenceService.getClassStatuses();
    Assert.assertNull(classStatuses);
  }

  /**
   * Tests success scenario to get class statuses {@link AssetReferenceService#getClassStatuses()}
   * when deleted true from class statuses.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void removedDeletedFromClassStatuses() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getClassStatus()).thenReturn(Calls.response(mockClassStatusesNotNull()));

    final List<ClassStatusHDto> classStatuses = assetReferenceService.getClassStatuses();
    Assert.assertNotNull(classStatuses);
    Assert.assertEquals("Cancelled.", classStatuses.get(0).getDescription());
    Assert.assertEquals(2, classStatuses.size());
  }

  /**
   * Tests success scenario to get class status {@link AssetReferenceService#getClassStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void getSingleClassStatus() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getClassStatus()).thenReturn(Calls.response(mockClassStatuses()));

    Long classStatusId = 6L;
    final ClassStatusHDto classStatus = assetReferenceService.getClassStatus(classStatusId);
    Assert.assertNotNull(classStatus);
    Assert.assertEquals(classStatusId, classStatus.getId());
  }

  /**
   * Tests failure scenario to get class status {@link AssetReferenceService#getClassStatus(Long)}
   * when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void getSingleClassStatusReturnsEmpty() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getClassStatus()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final ClassStatusHDto classStatus = assetReferenceService.getClassStatus(6L);
    Assert.assertNull(classStatus);
  }

  /**
   * Tests success scenario to list of get flag states
   * {@link AssetReferenceService#getFlagStates()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void successfullyGetFlagStates() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getFlagState()).thenReturn(Calls.response(mockFlagStates()));

    final List<FlagStateHDto> assetFlags = assetReferenceService.getFlagStates();
    Assert.assertNotNull(assetFlags);
    Assert.assertNotEquals(0, assetFlags.size());
  }

  /**
   * Tests failure scenario to list of get flag states {@link AssetReferenceService#getFlagStates()}
   * when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void emptyResultWhenGetFlagStates() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getFlagState()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<FlagStateHDto> flagStates = assetReferenceService.getFlagStates();
    Assert.assertNotNull(flagStates);
    Assert.assertTrue(flagStates.isEmpty());
  }

  /**
   * Tests success scenario to get flag state {@link AssetReferenceService#getFlagState(long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void getSingleFlagState() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getFlagState()).thenReturn(Calls.response(mockFlagStates()));

    Long flagStateId = 1234L;
    final FlagStateHDto flagState = assetReferenceService.getFlagState(flagStateId);
    Assert.assertNotNull(flagState);
    Assert.assertEquals(flagStateId, flagState.getId());
  }

  /**
   * Tests failure scenario to get flag state {@link AssetReferenceService#getFlagState(long)} when
   * mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void getSingleFlagStateReturnsEmpty() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getFlagState()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final FlagStateHDto classStatus = assetReferenceService.getFlagState(1234L);
    Assert.assertNull(classStatus);
  }

  /**
   * Tests success scenario to get list of codicil categories
   * {@link AssetReferenceService#getCodicilCategories()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void successfullyRetrieveListOfCodicilCategories() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getCodicilCategories()).thenReturn(Calls.response(mockCodicilCategories()));

    final List<CodicilCategoryHDto> codicilCategories = assetReferenceService.getCodicilCategories();
    Assert.assertNotNull(codicilCategories);
    Assert.assertNotEquals(0, codicilCategories.size());
  }

  /**
   * Tests failure scenario to get list of codicil categories
   * {@link AssetReferenceService#getCodicilCategories()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void failedToRetrieveListOfCodicilCategories() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getCodicilCategories()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<CodicilCategoryHDto> codicilCategories = assetReferenceService.getCodicilCategories();
    Assert.assertNotNull(codicilCategories);
    Assert.assertEquals(0, codicilCategories.size());
  }

  /**
   * Tests success scenario to get codicil category
   * {@link AssetReferenceService#getAssetType(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void successfullyGetCodicilCategory() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getCodicilCategories()).thenReturn(Calls.response(mockCodicilCategories()));

    final CodicilCategoryHDto codicilCategory = assetReferenceService.getCodicilCategory(123L);
    Assert.assertNotNull(codicilCategory);
  }

  /**
   * Tests failure scenario to get codicil category
   * {@link AssetReferenceService#getCodicilCategory(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void emptyResultForCodicilCategory() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getCodicilCategories()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final CodicilCategoryHDto codicilCategory = assetReferenceService.getCodicilCategory(123L);
    Assert.assertNull(codicilCategory);

  }

  /**
   * Tests success scenario to get list of builders {@link AssetReferenceService#getBuilders()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testBuilders() throws Exception {
    final Integer defaultCount = 15;
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getBuilders()).thenReturn(Calls.response(mockBuilders(defaultCount)));

    final List<BuilderHDto> builderHDtos = assetReferenceService.getBuilders();
    Assert.assertNotNull(builderHDtos);
  }

  /**
   * Tests success scenario to get builder by id
   * {@link AssetReferenceService#getBuilderById(Long)}}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testBuilderById() throws Exception {
    final Integer defaultCount = 15;
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getBuilders()).thenReturn(Calls.response(mockBuilders(defaultCount)));

    final BuilderHDto builderHDto = assetReferenceService.getBuilderById(1L);
    Assert.assertNotNull(builderHDto);
  }

  /**
   * Tests failure scenario to get builder by id {@link AssetReferenceService#getBuilderById(Long)}
   * when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testBuilderByIdEmpty() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getBuilders()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final BuilderHDto builderHDto = assetReferenceService.getBuilderById(1L);
    Assert.assertNull(builderHDto);
  }

  /**
   * Tests success scenario to get list of due statuses
   * {@link AssetReferenceService#getDueStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testDueStatuses() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getDueStatuses()).thenReturn(Calls.response(mockDueStatuses()));

    final List<DueStatusDto> dueStatuses = assetReferenceService.getDueStatuses();
    Assert.assertNotNull(dueStatuses);
  }

  /**
   * Tests success scenario to get due status
   * {@link AssetReferenceService#getDueStatusesById(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testDueStatusesById() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getDueStatuses()).thenReturn(Calls.response(mockDueStatuses()));

    final DueStatusDto duestatus = assetReferenceService.getDueStatusesById(12L);
    Assert.assertNotNull(duestatus);
  }

  /**
   * Tests failure scenario to get due status {@link AssetReferenceService#getDueStatusesById(Long)}
   * when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testDueStatusesByIdEmpty() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);
    Mockito.when(retrofitService.getDueStatuses()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final DueStatusDto dueStatus = assetReferenceService.getDueStatusesById(12L);
    Assert.assertNull(dueStatus);
  }

  /**
   * Tests success scenario to get list of defect statuses
   * {@link AssetReferenceService#getDefectStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetDefectStatuses() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<DefectStatusHDto> mockData = new ArrayList<DefectStatusHDto>();
    mockData.add(new DefectStatusHDto());
    mockData.add(new DefectStatusHDto());

    Mockito.when(retrofitService.getDefectStatuses()).thenReturn(Calls.response(mockData));

    final List<DefectStatusHDto> defectStatuses = assetReferenceService.getDefectStatuses();
    Assert.assertNotNull(defectStatuses);
  }

  /**
   * Tests failure scenario to get list of defect statuses
   * {@link AssetReferenceService#getDefectStatuses()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetDefectStatusesWithException() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getDefectStatuses()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<DefectStatusHDto> defectStatuses = assetReferenceService.getDefectStatuses();
    Assert.assertEquals(defectStatuses.isEmpty(), true);
  }

  /**
   * Tests success scenario to get defect status by id
   * {@link AssetReferenceService#getDefectStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetDefectStatus() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<DefectStatusHDto> mockData = new ArrayList<DefectStatusHDto>();
    final DefectStatusHDto sample1 = new DefectStatusHDto();
    sample1.setId(1L);
    mockData.add(sample1);

    Mockito.when(retrofitService.getDefectStatuses()).thenReturn(Calls.response(mockData));

    final DefectStatusHDto defectStatus = assetReferenceService.getDefectStatus(1L);
    Assert.assertNotNull(defectStatus);
  }

  /**
   * Tests failure scenario to get defect status by id
   * {@link AssetReferenceService#getDefectStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetDefectStatusWithException() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getDefectStatuses()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final DefectStatusHDto defectStatus = assetReferenceService.getDefectStatus(1L);
    Assert.assertNull(defectStatus);
  }

  /**
   * Tests success scenario to get list of repair actions
   * {@link AssetReferenceService#getRepairActions()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairActions() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<RepairActionHDto> mockData = new ArrayList<RepairActionHDto>();
    mockData.add(new RepairActionHDto());
    mockData.add(new RepairActionHDto());

    Mockito.when(retrofitService.getRepairActions()).thenReturn(Calls.response(mockData));

    final List<RepairActionHDto> repairActions = assetReferenceService.getRepairActions();
    Assert.assertNotNull(repairActions);
  }

  /**
   * Tests failure scenario to get list of repair actions
   * {@link AssetReferenceService#getRepairActions()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairActionsWithException() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getRepairActions()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<RepairActionHDto> repairActions = assetReferenceService.getRepairActions();
    Assert.assertEquals(repairActions.isEmpty(), true);
  }

  /**
   * Tests success scenario to get repair action by id
   * {@link AssetReferenceService#getRepairAction(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairAction() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<RepairActionHDto> mockData = new ArrayList<RepairActionHDto>();
    final RepairActionHDto sample1 = new RepairActionHDto();
    sample1.setId(1L);
    mockData.add(sample1);

    Mockito.when(retrofitService.getRepairActions()).thenReturn(Calls.response(mockData));

    final RepairActionHDto repairAction = assetReferenceService.getRepairAction(1L);
    Assert.assertNotNull(repairAction);
  }

  /**
   * Tests failure scenario to get repair action by id
   * {@link AssetReferenceService#getRepairAction(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairActionWithException() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getRepairActions()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final RepairActionHDto repairAction = assetReferenceService.getRepairAction(1L);
    Assert.assertNull(repairAction);
  }

  /**
   * Tests success scenario to get list of codicil statuses
   * {@link AssetReferenceService#getCodicilStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetCodicilStatuses() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<CodicilStatusHDto> mockData = new ArrayList<CodicilStatusHDto>();
    mockData.add(new CodicilStatusHDto());
    mockData.add(new CodicilStatusHDto());

    Mockito.when(retrofitService.getCodicilStatuses()).thenReturn(Calls.response(mockData));

    final List<CodicilStatusHDto> codicilStatuses = assetReferenceService.getCodicilStatuses();
    Assert.assertNotNull(codicilStatuses);
  }

  /**
   * Tests failure scenario to get list of codicil statuses
   * {@link AssetReferenceService#getCodicilStatuses()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetCodicilStatusesWithException() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getCodicilStatuses()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<CodicilStatusHDto> codicilStatuses = assetReferenceService.getCodicilStatuses();
    Assert.assertEquals(codicilStatuses.isEmpty(), true);
  }

  /**
   * Tests success scenario to get codicil status by id
   * {@link AssetReferenceService#getCodicilStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetCodicilStatus() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<CodicilStatusHDto> mockData = new ArrayList<CodicilStatusHDto>();
    final CodicilStatusHDto sample1 = new CodicilStatusHDto();
    sample1.setId(1L);
    mockData.add(sample1);

    Mockito.when(retrofitService.getCodicilStatuses()).thenReturn(Calls.response(mockData));

    final CodicilStatusHDto codicilStatus = assetReferenceService.getCodicilStatus(1L);
    Assert.assertNotNull(codicilStatus);
  }

  /**
   * Tests failure scenario to get codicil status by id
   * {@link AssetReferenceService#getCodicilStatus(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetCodicilStatusWithException() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getCodicilStatuses()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final CodicilStatusHDto codicilStatus = assetReferenceService.getCodicilStatus(1L);
    Assert.assertNull(codicilStatus);
  }

  /**
   * Tests success scenario to get list of action taken
   * {@link AssetReferenceService#getActionTakenList()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetActionTakenList() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<ActionTakenHDto> mockData = new ArrayList<ActionTakenHDto>();
    mockData.add(new ActionTakenHDto());
    mockData.add(new ActionTakenHDto());

    Mockito.when(retrofitService.getActionTakenList()).thenReturn(Calls.response(mockData));

    final List<ActionTakenHDto> actionTaken = assetReferenceService.getActionTakenList();
    Assert.assertNotNull(actionTaken);
  }

  /**
   * Tests failure scenario to get list of action taken
   * {@link AssetReferenceService#getActionTakenList()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetActionTakenListWithException() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getActionTakenList()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<ActionTakenHDto> actionTaken = assetReferenceService.getActionTakenList();
    Assert.assertEquals(actionTaken.isEmpty(), true);
  }

  /**
   * Tests success scenario to get action taken by id
   * {@link AssetReferenceService#getCodicilStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetActionTaken() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    // create mock data
    final List<ActionTakenHDto> mockData = new ArrayList<ActionTakenHDto>();
    final ActionTakenHDto sample1 = new ActionTakenHDto();
    sample1.setId(1L);
    mockData.add(sample1);

    Mockito.when(retrofitService.getActionTakenList()).thenReturn(Calls.response(mockData));

    final ActionTakenHDto codicilStatus = assetReferenceService.getActionTaken(1L);
    Assert.assertNotNull(codicilStatus);
  }

  /**
   * Tests failure scenario to get codicil status by id
   * {@link AssetReferenceService#getCodicilStatus(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetActionTakenWithException() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getActionTakenList()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final ActionTakenHDto actionTaken = assetReferenceService.getActionTaken(1L);
    Assert.assertNull(actionTaken);
  }

  /**
   * Tests success scenario to get list of repair types
   * {@link AssetReferenceService#getRepairTypes()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairTypes() {

    // create mock data
    final List<RepairTypeHDto> mockData = new ArrayList<RepairTypeHDto>();
    mockData.add(new RepairTypeHDto());
    mockData.add(new RepairTypeHDto());

    Mockito.when(assetReferenceRetrofitService.getRepairTypes()).thenReturn(Calls.response(mockData));

    final List<RepairTypeHDto> repairTypes = assetReferenceService.getRepairTypes();
    Assert.assertNotNull(repairTypes);
    Assert.assertEquals(repairTypes.size(), 2);
  }

  /**
   * Tests failure scenario to get list of repair types
   * {@link AssetReferenceService#getRepairTypes()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairTypesWithException() {

    Mockito.when(assetReferenceRetrofitService.getRepairTypes())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<RepairTypeHDto> repairTypes = assetReferenceService.getRepairTypes();
    Assert.assertTrue(repairTypes.isEmpty());
  }

  /**
   * Tests success scenario to get list of class maintaince status
   * {@link AssetReferenceService#getClassMaintenanceStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetClassMaintainceStatus() {

    // create mock data
    List<ClassMaintenanceStatusHDto> classMaintenanceStatusList = new ArrayList<>();
    classMaintenanceStatusList.add(new ClassMaintenanceStatusHDto());
    classMaintenanceStatusList.add(new ClassMaintenanceStatusHDto());

    Mockito.when(assetReferenceRetrofitService.getClassMaintenanceStatuses())
        .thenReturn(Calls.response(classMaintenanceStatusList));

    final List<ClassMaintenanceStatusHDto> classMaintainceStatus = assetReferenceService.getClassMaintenanceStatuses();
    Assert.assertNotNull(classMaintainceStatus);
    Assert.assertEquals(classMaintainceStatus.size(), 2);
  }

  /**
   * Tests failure scenario to get list of class maintaince status
   * {@link AssetReferenceService#getClassMaintenanceStatuses()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetClassMaintainceStatusFail() {

    Mockito.when(assetReferenceRetrofitService.getClassMaintenanceStatuses())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<ClassMaintenanceStatusHDto> classMaintainceStatus = assetReferenceService.getClassMaintenanceStatuses();
    Assert.assertNotNull(classMaintainceStatus);
    Assert.assertTrue(classMaintainceStatus.isEmpty());
  }

  /**
   * Tests success scenario to get single class maintaince status
   * {@link AssetReferenceService#getClassMaintenanceStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetClassMaintainceStatusById() {

    // create mock data
    List<ClassMaintenanceStatusHDto> classMaintenanceStatusList = new ArrayList<>();
    ClassMaintenanceStatusHDto classMaintenanceStatus = new ClassMaintenanceStatusHDto();
    classMaintenanceStatus.setId(1L);
    classMaintenanceStatusList.add(classMaintenanceStatus);
    classMaintenanceStatusList.add(new ClassMaintenanceStatusHDto());

    Mockito.when(assetReferenceRetrofitService.getClassMaintenanceStatuses())
        .thenReturn(Calls.response(classMaintenanceStatusList));

    final ClassMaintenanceStatusHDto classMaintainceStatus = assetReferenceService.getClassMaintenanceStatus(1L);
    Assert.assertNotNull(classMaintainceStatus);
  }

  /**
   * Tests failure scenario to get single class maintaince status
   * {@link AssetReferenceService#getClassMaintenanceStatus(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetClassMaintainceStatusByIdFail() {

    Mockito.when(assetReferenceRetrofitService.getClassMaintenanceStatuses())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    assetReferenceService.getClassMaintenanceStatus(1L);
  }

  /**
   * Tests success scenario to get list of service credit status
   * {@link AssetReferenceService#getServiceCreditStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetServiceCreditStatuses() {

    // create mock data
    List<ServiceCreditStatusRefHDto> serviceCreditStatus = new ArrayList<>();
    serviceCreditStatus.add(new ServiceCreditStatusRefHDto());
    serviceCreditStatus.add(new ServiceCreditStatusRefHDto());

    Mockito.when(assetReferenceRetrofitService.getServiceCreditStatuses())
        .thenReturn(Calls.response(serviceCreditStatus));

    final List<ServiceCreditStatusRefHDto> serviceCreditStatusList = assetReferenceService.getServiceCreditStatuses();
    Assert.assertNotNull(serviceCreditStatusList);
    Assert.assertEquals(serviceCreditStatusList.size(), 2);
  }

  /**
   * Tests failure scenario to get list of service credit status
   * {@link AssetReferenceService#getServiceCreditStatuses()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetServiceCreditStatusesFail() {

    // create mock data
    List<ServiceCreditStatusRefHDto> serviceCreditStatus = new ArrayList<>();
    serviceCreditStatus.add(new ServiceCreditStatusRefHDto());
    serviceCreditStatus.add(new ServiceCreditStatusRefHDto());

    Mockito.when(assetReferenceRetrofitService.getServiceCreditStatuses())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<ServiceCreditStatusRefHDto> serviceCreditStatusList = assetReferenceService.getServiceCreditStatuses();
    Assert.assertNotNull(serviceCreditStatusList);
    Assert.assertTrue(serviceCreditStatusList.isEmpty());
  }

  /**
   * Tests success scenario to get single service credit status
   * {@link AssetReferenceService#getServiceCreditStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetServiceCreditStatusesById() {

    // create mock data
    List<ServiceCreditStatusRefHDto> serviceCreditStatus = new ArrayList<>();

    ServiceCreditStatusRefHDto serviceCreditStatus1 = new ServiceCreditStatusRefHDto();
    serviceCreditStatus1.setId(1L);
    serviceCreditStatus.add(serviceCreditStatus1);
    serviceCreditStatus.add(new ServiceCreditStatusRefHDto());

    Mockito.when(assetReferenceRetrofitService.getServiceCreditStatuses())
        .thenReturn(Calls.response(serviceCreditStatus));

    final ServiceCreditStatusRefHDto response = assetReferenceService.getServiceCreditStatus(1L);
    Assert.assertNotNull(response);
  }

  /**
   * Tests failure scenario to get single service credit status
   * {@link AssetReferenceService#getServiceCreditStatus(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetServiceCreditStatusesByIdFail() {

    Mockito.when(assetReferenceRetrofitService.getServiceCreditStatuses())
        .thenReturn(Calls.failure(new IOException("Mock exception")));
    final ServiceCreditStatusRefHDto response = assetReferenceService.getServiceCreditStatus(1L);
  }

  /**
   * Tests success scenario to get list of party roles
   * {@link AssetReferenceService#getPartyRoles()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetPartyRoles() {

    // create mock data
    List<PartyRoleHDto> partyRolesList = new ArrayList<>();
    partyRolesList.add(new PartyRoleHDto());
    partyRolesList.add(new PartyRoleHDto());

    Mockito.when(assetReferenceRetrofitService.getPartyRoles()).thenReturn(Calls.response(partyRolesList));

    final List<PartyRoleHDto> partyRoles = assetReferenceService.getPartyRoles();
    Assert.assertNotNull(partyRoles);
    Assert.assertEquals(partyRoles.size(), 2);
  }

  /**
   * Tests failure scenario to get list of service credit status
   * {@link AssetReferenceService#getPartyRoles()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetPartyRolesFail() {

    // create mock data
    List<PartyRoleHDto> partyRolesList = new ArrayList<>();
    partyRolesList.add(new PartyRoleHDto());
    partyRolesList.add(new PartyRoleHDto());

    Mockito.when(assetReferenceRetrofitService.getPartyRoles())
        .thenReturn(Calls.failure(new IOException("Mock exception")));
    final PartyRoleHDto partyRole = assetReferenceService.getPartyRole(1L);
  }

  /**
   * Tests success scenario to get single party role
   * {@link AssetReferenceService#getPartyRole(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetPartyRolesById() {

    // create mock data
    List<PartyRoleHDto> partyRolesList = new ArrayList<>();
    PartyRoleHDto party = new PartyRoleHDto();
    party.setId(1L);
    partyRolesList.add(party);
    partyRolesList.add(new PartyRoleHDto());
    Mockito.when(assetReferenceRetrofitService.getPartyRoles()).thenReturn(Calls.response(partyRolesList));
    final PartyRoleHDto partyRole = assetReferenceService.getPartyRole(1L);
    Assert.assertNotNull(partyRole);
  }

  /**
   * Tests failure scenario to get single party role
   * {@link AssetReferenceService#getPartyRole(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetPartyRolesByIdFail() {

    // create mock data
    List<PartyRoleHDto> partyRolesList = new ArrayList<>();
    partyRolesList.add(new PartyRoleHDto());
    partyRolesList.add(new PartyRoleHDto());

    Mockito.when(assetReferenceRetrofitService.getPartyRoles())
        .thenReturn(Calls.failure(new IOException("Mock exception")));
    final List<PartyRoleHDto> partyRoles = assetReferenceService.getPartyRoles();
    Assert.assertNotNull(partyRoles);
    Assert.assertTrue(partyRoles.isEmpty());
  }

  /**
   * Tests success scenario to get list of asset life cycle status
   * {@link AssetReferenceService#getAssetLifecycleStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetAssetLifeCycleStatus() {

    // create mock data
    List<AssetLifeCycleStatusDto> assetLifeCycleStatuses = new ArrayList<>();
    AssetLifeCycleStatusDto assetLifeCycleStatus = new AssetLifeCycleStatusDto();
    assetLifeCycleStatus.setId(1L);
    assetLifeCycleStatuses.add(assetLifeCycleStatus);
    assetLifeCycleStatuses.add(new AssetLifeCycleStatusDto());

    Mockito.when(assetReferenceRetrofitService.getAssetLifecycleStatuses())
        .thenReturn(Calls.response(assetLifeCycleStatuses));

    final List<AssetLifeCycleStatusDto> response = assetReferenceService.getAssetLifecycleStatuses();
    Assert.assertNotNull(response);
    Assert.assertEquals(response.size(), 2);
  }

  /**
   * Tests failure scenario to get list of asset life cycle status
   * {@link AssetReferenceService#getAssetLifecycleStatuses()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetAssetLifeCycleStatusFail() {

    // create mock data
    List<AssetLifeCycleStatusDto> assetLifeCycleStatuses = new ArrayList<>();
    AssetLifeCycleStatusDto assetLifeCycleStatus = new AssetLifeCycleStatusDto();
    assetLifeCycleStatus.setId(1L);
    assetLifeCycleStatuses.add(assetLifeCycleStatus);
    assetLifeCycleStatuses.add(new AssetLifeCycleStatusDto());

    Mockito.when(assetReferenceRetrofitService.getAssetLifecycleStatuses())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<AssetLifeCycleStatusDto> response = assetReferenceService.getAssetLifecycleStatuses();
    Assert.assertNotNull(response);
    Assert.assertTrue(response.isEmpty());
  }

  /**
   * Tests success scenario to get single asset life cycle status by id
   * {@link AssetReferenceService#getAssetLifecycleStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetAssetLifeCycleStatusById() {

    // create mock data
    List<PartyRoleHDto> partyRolesList = new ArrayList<>();
    PartyRoleHDto party = new PartyRoleHDto();
    party.setId(1L);
    partyRolesList.add(party);
    partyRolesList.add(new PartyRoleHDto());
    Mockito.when(assetReferenceRetrofitService.getPartyRoles()).thenReturn(Calls.response(partyRolesList));
    final PartyRoleHDto partyRole = assetReferenceService.getPartyRole(1L);
    Assert.assertNotNull(partyRole);
  }

  /**
   * Tests failure scenario to get single asset life cycle status by id
   * {@link AssetReferenceService#getAssetLifecycleStatus(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetAssetLifeCycleStatusByIdFail() {

    // create mock data
    List<AssetLifeCycleStatusDto> assetLifeCycleStatuses = new ArrayList<>();
    AssetLifeCycleStatusDto assetLifeCycleStatus = new AssetLifeCycleStatusDto();
    assetLifeCycleStatus.setId(1L);
    assetLifeCycleStatuses.add(assetLifeCycleStatus);
    assetLifeCycleStatuses.add(new AssetLifeCycleStatusDto());

    Mockito.when(assetReferenceRetrofitService.getAssetLifecycleStatuses())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final AssetLifeCycleStatusDto response = assetReferenceService.getAssetLifecycleStatus(1L);
    Assert.assertNull(response);

  }

  /**
   * Tests success scenario to get repair type by id
   * {@link AssetReferenceService#getRepairType(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairType() throws Exception {

    // create mock data
    final List<RepairTypeHDto> mockData = new ArrayList<RepairTypeHDto>();
    final RepairTypeHDto sample1 = new RepairTypeHDto();
    sample1.setId(1L);
    mockData.add(sample1);

    Mockito.when(assetReferenceRetrofitService.getRepairTypes()).thenReturn(Calls.response(mockData));

    final RepairTypeHDto repairType = assetReferenceService.getRepairType(1L);
    Assert.assertNotNull(repairType);
    Assert.assertEquals(sample1.getId(), repairType.getId());
  }

  /**
   * Tests failure scenario to get repair type by id
   * {@link AssetReferenceService#getRepairType(Long)} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetRepairTypeWithException() throws Exception {

    Mockito.when(assetReferenceRetrofitService.getRepairTypes())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final RepairTypeHDto repairType = assetReferenceService.getRepairType(1L);
    Assert.assertNull(repairType);
  }

  /**
   * Tests success scenario to get list of major non confirmitive notes
   * {@link AssetReferenceService#getMncnStatusList()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetMncnList() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getMajorNCNs()).thenReturn(Calls.response(mockMajorNcns()));

    final List<MajorNCNStatusHDto> mncnList = assetReferenceService.getMncnStatusList();
    Assert.assertNotNull(mncnList);
    Assert.assertFalse(mncnList.isEmpty());
    Assert.assertEquals("Open", mncnList.get(0).getName());
  }

  /**
   * Tests failure scenario to get list of major non confirmitive notes
   * {@link AssetReferenceService#getMncnStatusList()} when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetMncnListFail() {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getMajorNCNs()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final List<MajorNCNStatusHDto> mncnList = assetReferenceService.getMncnStatusList();
    Assert.assertEquals(mncnList.isEmpty(), true);
  }

  /**
   * Tests success scenario to get major non confirmitive note by id
   * {@link AssetReferenceService#getMncnStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetMncn() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getMajorNCNs()).thenReturn(Calls.response(mockMajorNcns()));

    final MajorNCNStatusHDto mncn = assetReferenceService.getMncnStatus(1L);
    Assert.assertNotNull(mncn);
    Assert.assertEquals("Open", mncn.getName());
  }

  /**
   * Tests failure scenario to get major non confirmitive note by id
   * {@link AssetReferenceService#getMncnStatus(Long)} if mast api call fail.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testGetMncnFail() throws Exception {
    final AssetReferenceRetrofitService retrofitService = context.getBean(AssetReferenceRetrofitService.class);

    Mockito.when(retrofitService.getMajorNCNs()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final MajorNCNStatusHDto mncn = assetReferenceService.getMncnStatus(1L);
    Assert.assertNull(mncn);
  }

  /**
   * Mocks Builders data.
   *
   * @param count the size of builders.
   * @return builders.
   */
  private List<BuilderHDto> mockBuilders(final Integer count) {
    final List<BuilderHDto> builderHDtos = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final BuilderHDto builderHDto = new BuilderHDto();
      builderHDto.setApprovalStatus(true);
      builderHDto.setId(Long.valueOf(i));
      builderHDto.setName(generateRandomString());
      builderHDtos.add(builderHDto);
    }
    return builderHDtos;
  }

  /**
   * Generates random 32 chars string.
   *
   * @return string.
   *
   */
  private String generateRandomString() {
    final SecureRandom random = new SecureRandom();
    return new BigInteger(130, random).toString(32);
  }


  /**
   * Mocks list of {@link ClassStatusHDto} with single class status.
   *
   * @return list of {@link ClassStatusHDto}.
   *
   */
  private List<ClassStatusHDto> mockClassStatuses() {
    final List<ClassStatusHDto> classStatuses = new ArrayList<>();

    final ClassStatusHDto classStatus = new ClassStatusHDto();
    classStatus.setName("Cancelled");
    classStatus.setId(6L);
    classStatus.setDescription("Cancelled.");
    classStatuses.add(classStatus);

    return classStatuses;
  }

  /**
   * Mocks class statuses with null.
   *
   * @return mocked class statuses.
   */
  private List<ClassStatusHDto> mockClassStatusesNull() {
    final List<ClassStatusHDto> classStatuses = null;

    return classStatuses;
  }

  /**
   * Mocks list of {@link ClassStatusHDto}.
   *
   * @return list of {@link ClassStatusHDto}.
   */
  private List<ClassStatusHDto> mockClassStatusesNotNull() {
    final List<ClassStatusHDto> classStatuses = new ArrayList<>();

    final ClassStatusHDto classStatus = new ClassStatusHDto();
    classStatus.setName("Cancelled");
    classStatus.setId(6L);
    classStatus.setDeleted(true);
    classStatus.setDescription("Deleted.");
    classStatuses.add(classStatus);

    final ClassStatusHDto classStatus1 = new ClassStatusHDto();
    classStatus1.setName("Cancelled");
    classStatus1.setId(1L);
    classStatus1.setDeleted(true);
    classStatus1.setDescription("Deleted1.");
    classStatuses.add(classStatus1);

    final ClassStatusHDto classStatus2 = new ClassStatusHDto();
    classStatus2.setName("Cancelled");
    classStatus2.setId(2L);
    classStatus2.setDeleted(false);
    classStatus2.setDescription("Cancelled.");
    classStatuses.add(classStatus2);

    final ClassStatusHDto classStatus3 = new ClassStatusHDto();
    classStatus3.setName("Cancelled1");
    classStatus3.setId(3L);
    classStatus3.setDeleted(false);
    classStatuses.add(classStatus3);

    return classStatuses;
  }

  /**
   * Mocks asset category list.
   *
   * @return list of asset categories.
   */
  private List<AssetCategoryHDto> mockAssetCategories() {
    final List<AssetCategoryHDto> assetCategories = new ArrayList<>();
    final AssetCategoryHDto category = new AssetCategoryHDto();
    category.setId(123L);
    category.setBusinessProcess(new LinkResource(123L));
    category.setName("ABC");
    assetCategories.add(category);

    return assetCategories;
  }

  /**
   * Mocks asset type list.
   *
   * @return asset types.
   *
   */
  private List<AssetTypeHDto> mockAssetTypes() {
    final List<AssetTypeHDto> assetTypes = new ArrayList<>();
    final AssetTypeHDto assetType = new AssetTypeHDto();
    assetType.setId(1234L);
    assetType.setCode("1234");
    assetType.setCategory(new LinkResource(123L));
    assetTypes.add(assetType);

    return assetTypes;
  }



  /**
   * Mocks flag state list.
   *
   * @return flag states.
   */
  private List<FlagStateHDto> mockFlagStates() {
    final List<FlagStateHDto> flagStates = new ArrayList<>();
    final FlagStateHDto flagStete = new FlagStateHDto();
    flagStete.setId(1234L);
    flagStete.setName("Ageshima");
    flagStates.add(flagStete);

    return flagStates;
  }

  /**
   *
   * Mocks codicil category list.
   *
   * @return codicil categories.
   */
  private List<CodicilCategoryHDto> mockCodicilCategories() {
    final List<CodicilCategoryHDto> codicilCategories = new ArrayList<>();
    final CodicilCategoryHDto codicilCategory = new CodicilCategoryHDto();
    codicilCategory.setId(123L);
    codicilCategory.setName("TestCodicilCategory");
    codicilCategories.add(codicilCategory);

    return codicilCategories;
  }

  /**
   * Mocks due statuses list.
   *
   * @return due statuses.
   */
  private List<DueStatusDto> mockDueStatuses() {
    final List<DueStatusDto> dueStatuses = new ArrayList<>();
    final DueStatusDto dueStatus = new DueStatusDto();
    dueStatus.setId(12L);
    dueStatuses.add(dueStatus);

    return dueStatuses;
  }

  /**
   * Mocks major non confirmitive notes list.
   *
   * @return list of major non confirmitive notes.
   */
  private List<MajorNCNStatusHDto> mockMajorNcns() {
    // create mock data
    final List<MajorNCNStatusHDto> mockData = new ArrayList<MajorNCNStatusHDto>();
    MajorNCNStatusHDto mncn1 = new MajorNCNStatusHDto();
    mncn1.setId(1L);
    mncn1.setName("Open");
    mncn1.setDeleted(false);
    mockData.add(mncn1);
    MajorNCNStatusHDto mncn2 = new MajorNCNStatusHDto();
    mncn2.setId(2L);
    mncn2.setName("Open - Downgraded");
    mncn2.setDeleted(false);
    mockData.add(mncn2);
    MajorNCNStatusHDto mncn3 = new MajorNCNStatusHDto();
    mncn3.setId(3L);
    mncn3.setName("Closed");
    mncn3.setDeleted(false);
    mockData.add(mncn3);
    return mockData;
  }

  /**
   * Spring in-class configurations.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create {@link AssetReferenceService} bean.
     *
     * @return asset reference service.
     *
     */
    @Bean
    @Override
    public AssetReferenceService assetReferenceService() {
      return new AssetReferenceServiceImpl();
    }

    /**
     * Return cache.
     *
     * @return cache object.
     *
     */
    @Override
    @Bean
    public CacheContainer cache() {
      final CacheContainer container = Mockito.mock(CacheContainer.class);
      Mockito.when(container.get(anyObject())).thenReturn(new HashMap<Long, Object>());

      return container;
    }
  }
}
