package com.baesystems.ai.lr.cd.service.version.test;

import javax.servlet.ServletContext;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Proxy on Spring's ServletContext.
 *
 * @author fwijaya on 15/6/2017.
 */
public class ServletContextProxy implements InvocationHandler {
  /**
   * Spring's servlet context.
   */
  private ServletContext context;
  /**
   * The path to the manifest file.
   */
  private String manifestPath;

  /**
   * Constructs a proxy.
   *
   * @param servletContext The Spring's injected context.
   * @param path The path to manifest file.
   */
  public ServletContextProxy(final ServletContext servletContext, final String path) {
    this.context = servletContext;
    this.manifestPath = path;
  }

  /**
   * Intercepts method call on the Spring's injected context.
   *
   * @param proxy Proxy.
   * @param method Method call.
   * @param args Method argument.
   * @return Result of method call.
   * @throws Throwable Any exception that might happen while invoking method.
   */
  @Override
  public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
    if (method.getName().equalsIgnoreCase("getResourceAsStream")) {
      return getClass().getClassLoader().getResourceAsStream(manifestPath);
    } else {
      return method.invoke(context, args);
    }
  }
}
