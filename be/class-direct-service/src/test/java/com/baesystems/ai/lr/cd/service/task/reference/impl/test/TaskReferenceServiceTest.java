package com.baesystems.ai.lr.cd.service.task.reference.impl.test;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.JobReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedValueGroupsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedWorkItemAttributeValueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.cd.service.task.reference.impl.TaskReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

import retrofit2.mock.Calls;

/**
 * @author VKolagutla
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TaskReferenceServiceTest.Config.class)
public class TaskReferenceServiceTest {

  /**
   * Spring application context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * Injected task reference service.
   *
   */
  @Autowired
  private TaskReferenceService taskReferenceService;

  /**
   * Constants for work item type id1.
   */
  private static final Long POSTPONEMENT_TYPE_ID_1 = 1L;

  /**
   * Constants for work item type id2.
   */
  private static final Long POSTPONEMENT_TYPE_ID_2 = 2L;

  /**
   * Initialize mocked object.
   */
  @Before
  public final void init() {
    reset(context.getBean(JobReferenceRetrofitService.class));
  }

  /**
   * Positive test for {@link TaskReferenceService#getPostponementTypes()}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void successfullyRetrieveListOfPostponementTypes() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);

    when(retrofitService.getPostponementTypes()).thenReturn(Calls.response(mockPostponementTypes()));

    final List<PostponementTypeHDto> jobStatuses = taskReferenceService.getPostponementTypes();
    Assert.assertNotNull(jobStatuses);
    Assert.assertNotEquals(0, jobStatuses.size());
  }

  /**
   * Negative Test for {@link TaskReferenceService#getPostponementTypes()}: fail to retrieve list of
   * job status.
   *
   * @throws Exception when error.
   */
  @Test
  public final void failedToRetrieveListOfPostponementTypes() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getPostponementTypes()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<PostponementTypeHDto> postponementTypes = taskReferenceService.getPostponementTypes();
    Assert.assertNotNull(postponementTypes);
    Assert.assertEquals(0, postponementTypes.size());
  }

  /**
   * Positive test for {@link TaskReferenceService#getPostponementType(Long)}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void successfullyGetWorkItemType() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getPostponementTypes()).thenReturn(Calls.response(mockPostponementTypes()));

    final PostponementTypeHDto postponementType = taskReferenceService.getPostponementType(POSTPONEMENT_TYPE_ID_1);
    Assert.assertNotNull(postponementType);
  }

  /**
   * Negative Test for {@link TaskReferenceService#getPostponementType(Long)}: empty result.
   *
   * @throws Exception when error.
   */
  @Test
  public final void emptyResultForGetWorkItemType() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getPostponementTypes()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final PostponementTypeHDto postponementType = taskReferenceService.getPostponementType(POSTPONEMENT_TYPE_ID_1);
    Assert.assertNull(postponementType);
  }

  /**
   * Tests success scenario to get list of allowed value groups
   * {@link TaskReferenceService#getAllowedValueGroups()}.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedValueGroups() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedValueGroups()).thenReturn(Calls.response(mockAllowedValueGroups()));

    final List<AllowedValueGroupsHDto> groups = taskReferenceService.getAllowedValueGroups();
    Assert.assertNotNull(groups);
    Assert.assertNotNull(groups.get(0).getName());
    Assert.assertEquals(groups.get(0).getName(), "Yes");
  }

  /**
   * Tests failure scenario to get list of allowed value groups
   * {@link TaskReferenceService#getAllowedValueGroups()} when mast api call fails.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedValueGroupsFail() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedValueGroups()).thenReturn(Calls.failure(new IOException()));

    final List<AllowedValueGroupsHDto> groups = taskReferenceService.getAllowedValueGroups();
    Assert.assertNotNull(groups);
    Assert.assertTrue(groups.isEmpty());
  }

  /**
   * Tests success scenario to allowed value group by id
   * {@link TaskReferenceService#getAllowedValueGroup()}.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedValueGroupById() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedValueGroups()).thenReturn(Calls.response(mockAllowedValueGroups()));

    final AllowedValueGroupsHDto group = taskReferenceService.getAllowedValueGroup(1L);
    Assert.assertNotNull(group);
    Assert.assertNotNull(group.getName());
    Assert.assertEquals(group.getName(), "Yes");
  }

  /**
   * Tests failure scenario to get allowed value group by id
   * {@link TaskReferenceService#getAllowedValueGroup()} when mast api call fails.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedValueGroupByIdFail() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedValueGroups()).thenReturn(Calls.failure(new IOException()));

    final AllowedValueGroupsHDto group = taskReferenceService.getAllowedValueGroup(1L);
    Assert.assertNull(group);
  }


  /**
   * Tests success scenario to get list of allowed values
   * {@link TaskReferenceService#getAllowedAttributeValues()}.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedAttributeValues() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedAttributeValues()).thenReturn(Calls.response(mockAllowedAttributeValues()));

    final List<AllowedWorkItemAttributeValueHDto> groups = taskReferenceService.getAllowedAttributeValues();
    Assert.assertNotNull(groups);
    Assert.assertNotNull(groups.get(0).getValue());
    Assert.assertEquals(groups.get(0).getValue(), "Yes");
  }

  /**
   * Tests failure scenario to get list of allowed values
   * {@link TaskReferenceService#getAllowedAttributeValues()} when mast api call fails.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedAttributeValuesFail() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedAttributeValues()).thenReturn(Calls.failure(new IOException()));

    final List<AllowedWorkItemAttributeValueHDto> groups = taskReferenceService.getAllowedAttributeValues();
    Assert.assertNotNull(groups);
    Assert.assertTrue(groups.isEmpty());
  }

  /**
   * Tests success scenario to allowed value by id
   * {@link TaskReferenceService#getAllowedAttributeValue(Long)}.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedAttributeValueById() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedAttributeValues()).thenReturn(Calls.response(mockAllowedAttributeValues()));

    final AllowedWorkItemAttributeValueHDto group = taskReferenceService.getAllowedAttributeValue(1L);
    Assert.assertNotNull(group);
    Assert.assertNotNull(group.getValue());
    Assert.assertEquals(group.getValue(), "Yes");
  }

  /**
   * Tests failure scenario to get allowed value by id
   * {@link TaskReferenceService#getAllowedAttributeValue(Long)}. when mast api call fails.
   *
   * @throws Exception when mast api call fails.
   *
   */
  @Test
  public final void testAllowedAttributeValueByIdFail() throws Exception {
    final TaskReferenceRetrofitService retrofitService = context.getBean(TaskReferenceRetrofitService.class);
    when(retrofitService.getAllowedAttributeValues()).thenReturn(Calls.failure(new IOException()));

    final AllowedWorkItemAttributeValueHDto group = taskReferenceService.getAllowedAttributeValue(1L);
    Assert.assertNull(group);
  }

  /**
   * Mock work item type list.
   *
   * @return caller.
   * @throws Exception when error.
   */
  private List<PostponementTypeHDto> mockPostponementTypes() throws Exception {
    final List<PostponementTypeHDto> workItemTypes = new ArrayList<>();
    final PostponementTypeHDto workItemType = new PostponementTypeHDto();
    workItemType.setId(POSTPONEMENT_TYPE_ID_1);
    workItemType.setName("Conditionally Approved");
    workItemTypes.add(workItemType);
    final PostponementTypeHDto workItemType1 = new PostponementTypeHDto();
    workItemType1.setId(POSTPONEMENT_TYPE_ID_2);
    workItemType1.setName("Approved");
    workItemTypes.add(workItemType1);

    return workItemTypes;
  }

  /**
   * Mock allowed value groups list.
   *
   * @return caller.
   * @throws Exception when error.
   */
  private List<AllowedValueGroupsHDto> mockAllowedValueGroups() {
    final List<AllowedValueGroupsHDto> groups = new ArrayList<>();
    AllowedValueGroupsHDto value = new AllowedValueGroupsHDto();
    value.setId(1L);
    value.setName("Yes");
    groups.add(value);
    AllowedValueGroupsHDto value2 = new AllowedValueGroupsHDto();
    value2.setId(2L);
    value2.setName("None");
    groups.add(value2);
    return groups;
  }

  /**
   * Mock allowed values list.
   *
   * @return caller.
   * @throws Exception when error.
   */
  private List<AllowedWorkItemAttributeValueHDto> mockAllowedAttributeValues() {
    List<AllowedWorkItemAttributeValueHDto> values = new ArrayList<>();
    AllowedWorkItemAttributeValueHDto allowedValue = new AllowedWorkItemAttributeValueHDto();
    allowedValue.setId(1L);
    allowedValue.setValue("Yes");
    values.add(allowedValue);
    return values;
  }

  /**
   * @author VKolagutla
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create {@link TaskReferenceService} bean.
     *
     * @return task reference service.
     *
     */
    @Bean
    @Override
    public TaskReferenceService taskReferenceService() {
      return new TaskReferenceServiceImpl();
    }
  }

}
