package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.DefectService;
import com.baesystems.ai.lr.cd.service.asset.impl.DefectServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.JobFaultDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FaultCategoryDto;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Provides unit tests for defect service {@link DefectService}.
 *
 * @author yng
 * @author syalavarthi 24-05-2017.
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Response.class, Resources.class, SecurityUtils.class})
@ContextConfiguration(classes = DefectServiceTest.Config.class)
public class DefectServiceTest {
  /**
   * The {@value #LOGGER} Logger initialization for DefectServiceTest.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(DefectServiceTest.class);

  /**
   * The {@link AssetRetrofitService} from spring context.
   *
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The {@link JobRetrofitService} from spring context.
   *
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The {@link DefectService} from spring context.
   *
   */
  @Autowired
  private DefectService defectService;

  /**
   * Injected context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * Tests success scenario to get defect by id
   * {@link DefectService#getDefectByAssetIdDefectId(Long, long)}.
   * <p>
   * internal calls return data.
   * </p>
   *
   * @throws Exception if mast api fail or any error in execution.
   */
  @Test
  public final void testGetDefectByAssetIdDefectId() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    // test with job and Item data
    doReturn(mockSingleDefectDataWithJobsDefectItemNotNull()).when(assetRetrofitService)
        .getDefectByAssetIdDefectId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockJobsData()).when(jobRetrofitService).getJobsByJobId(Matchers.anyLong());
    doReturn(mockItemsData()).when(assetRetrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockCocData()).when(assetRetrofitService).getCoCbyDefectAssetId(Matchers.anyLong(), Matchers.anyLong());

    DefectHDto result = defectService.getDefectByAssetIdDefectId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getJobsH());
    Assert.assertNotNull(result.getItemsH());

    // test without job and Item data
    doReturn(mockSingleDefectDataWithJobsDefectItemNull()).when(assetRetrofitService)
        .getDefectByAssetIdDefectId(Matchers.anyLong(), Matchers.anyLong());

    result = defectService.getDefectByAssetIdDefectId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNull(result.getJobsH());
    Assert.assertNull(result.getItemsH());
  }

  /**
   * Tests failure scenario to get defect by id
   * {@link DefectService#getDefectByAssetIdDefectId(Long, long)}.
   * <p>
   * internal calls return error.
   * </p>
   *
   * @throws Exception if mast api fail or any error in execution.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetDefectByAssetIdDefectIdMultiCondition() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    // simulate exception for logger
    doReturn(mockSingleDefectDataWithJobsDefectItemNotNull()).when(assetRetrofitService)
        .getDefectByAssetIdDefectId(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.failure(new IOException()));
    when(assetRetrofitService.getAssetItemDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));
    when(assetRetrofitService.getCoCbyDefectAssetId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    defectService.getDefectByAssetIdDefectId(1L, 1L);

    // simulate MAST call failed
    when(assetRetrofitService.getDefectByAssetIdDefectId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    defectService.getDefectByAssetIdDefectId(1L, 1L);
  }

  /**
   * Tests success scenario to get list of paginated defects for asset
   * {@link DefectService#getDefectsByQuery(Integer, Integer, Long, CodicilDefectQueryHDto)} to
   * ensure the data is hydrated correctly.
   *
   * @throws ClassDirectException when API call/executor fail.
   */
  @Test
  public final void testGetDefectsByQuery() throws ClassDirectException {

    // create test data for defect
    final DefectHDto defect1 = new DefectHDto();
    defect1.setId(1L);
    defect1.setJob(new LinkResource(1L));
    defect1.setRaisedOnJob(new LinkResource(1L));
    defect1.setClosedOnJob(new LinkResource(1L));
    final List<DefectItemDto> itemData = new ArrayList<>();
    final DefectItemDto item1 = new DefectItemDto();
    item1.setId(1L);
    item1.setItem(new ItemLightDto());
    item1.getItem().setId(1L);
    defect1.setItems(itemData);

    final DefectHDto defect2 = new DefectHDto();
    defect2.setId(2L);
    defect2.setItems(itemData);

    final List<DefectHDto> defects = new ArrayList<>();
    defects.add(defect1);
    defects.add(defect2);

    final Response<List<DefectHDto>> defectsResponse = PowerMockito.mock(Response.class);
    Mockito.when(defectsResponse.body()).thenReturn(defects);
    final Call<List<DefectHDto>> defectsCaller = Calls.response(defectsResponse);

    // create test data for job
    final JobHDto job1 = new JobHDto();
    job1.setId(1L);

    final Response<JobHDto> jobResponse = PowerMockito.mock(Response.class);
    Mockito.when(jobResponse.body()).thenReturn(job1);
    final Call<JobHDto> jobCaller = Calls.response(jobResponse);

    // mock API call
    Mockito.when(assetRetrofitService.getDefectsByQuery(Mockito.anyLong(), Mockito.any())).thenReturn(defectsCaller);
    Mockito.when(jobRetrofitService.getJobsByJobId(Mockito.anyLong())).thenReturn(jobCaller);

    // call test method
    final PageResource<DefectHDto> response =
        defectService.getDefectsByQuery(null, null, 1L, new CodicilDefectQueryHDto());

    Assert.assertNotNull(response.getContent());
    Assert.assertEquals(2, response.getContent().size());

    for (final DefectHDto defect : response.getContent()) {
      if (defect.getId().equals(1L)) {
        Assert.assertNotNull(defect.getJobH());
        Assert.assertNotNull(defect.getRaisedOnJobH());
        Assert.assertNotNull(defect.getClosedOnJobH());
      } else if (defect.getId().equals(2L)) {
        Assert.assertNull(defect.getJobH());
        Assert.assertNull(defect.getRaisedOnJobH());
        Assert.assertNull(defect.getClosedOnJobH());
      }
    }

  }

  /**
   * Tests failure scenario to get list of paginated defects for asset
   * {@link DefectService#getDefectsByQuery(Integer, Integer, Long, CodicilDefectQueryHDto)} when
   * mast api call fails.
   *
   * @throws ClassDirectException when API call/executor fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetDefectsByQueryFail() throws ClassDirectException {
    // mock API call
    Mockito.when(assetRetrofitService.getDefectsByQuery(Mockito.anyLong(), Mockito.any()))
        .thenReturn(Calls.failure(new IOException("mock")));

    defectService.getDefectsByQuery(null, null, 1L, new CodicilDefectQueryHDto());
  }


  /**
   * Mock defect Data.
   *
   * @return call value.
   * @throws Exception if any error in formating date.
   */
  @SuppressWarnings("unchecked")
  private Call<List<DefectHDto>> mockDefectData() throws Exception {
    final List<DefectHDto> defects = new ArrayList<DefectHDto>();

    // data with jobs and items
    final DefectHDto data1 = new DefectHDto();
    data1.setId(1L);
    data1.setIncidentDate(new Date(22 / 01 / 2016));

    final List<JobFaultDto> jobData = new ArrayList<>();
    final JobFaultDto job1 = new JobFaultDto();
    job1.setId(1L);
    job1.setJob(new LinkResource(1L));
    job1.getJob().setId(1L);
    jobData.add(job1);
    data1.setJobs(jobData);

    final List<DefectItemDto> itemData = new ArrayList<>();
    final DefectItemDto item1 = new DefectItemDto();
    item1.setId(1L);
    item1.setItem(new ItemLightDto());
    item1.getItem().setId(1L);
    itemData.add(item1);
    data1.setItems(itemData);
    defects.add(data1);

    // data without jobs and items
    final DefectHDto data2 = new DefectHDto();
    data2.setId(2L);
    data2.setIncidentDate(new Date(25 / 01 / 2015));
    defects.add(data2);


    final Response<List<DefectHDto>> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(defects);

    final Call<List<DefectHDto>> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mock Single defect data with job and AssetItem info.
   *
   * @return call value.
   */
  @SuppressWarnings("unchecked")
  private Call<DefectHDto> mockSingleDefectDataWithJobsDefectItemNotNull() {
    // data with jobs and items
    final DefectHDto data = new DefectHDto();
    data.setId(1L);

    final List<JobFaultDto> jobData = new ArrayList<>();
    final JobFaultDto job = new JobFaultDto();
    job.setId(1L);
    job.setJob(new LinkResource(1L));
    job.getJob().setId(1L);
    jobData.add(job);
    data.setJobs(jobData);

    final List<DefectItemDto> itemData = new ArrayList<>();
    final DefectItemDto item = new DefectItemDto();
    item.setId(1L);
    item.setItem(new ItemLightDto());
    item.getItem().setId(1L);
    itemData.add(item);
    data.setItems(itemData);

    data.setCategory(mockFaultCategory());

    final Response<DefectHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(data);

    final Call<DefectHDto> caller = Calls.response(response);

    return caller;
  }


  /**
   * mock fault category.
   *
   * @return fault category.
   */
  private FaultCategoryDto mockFaultCategory() {
    final FaultCategoryDto faultCategory = new FaultCategoryDto();
    faultCategory.setId(1L);
    faultCategory.setDeleted(false);
    faultCategory.setName("Deficiency Category Name");
    faultCategory.setDescription("Deficiency Category Description");
    faultCategory.setFaultGroup(new LinkResource(1L));
    faultCategory.getFaultGroup().setId(1L);
    faultCategory.setProductCatalogue(new LinkResource(1L));
    faultCategory.getProductCatalogue().setId(1L);
    faultCategory.setCodicilCategory(new LinkResource(1L));
    faultCategory.getCodicilCategory().setId(1L);
    return faultCategory;
  }

  /**
   * Mock Single defect data with job and AssetItem null.
   *
   * @return call value.
   */
  @SuppressWarnings("unchecked")
  private Call<DefectHDto> mockSingleDefectDataWithJobsDefectItemNull() {
    // data without jobs and items
    final DefectHDto data = new DefectHDto();
    data.setId(1L);

    final Response<DefectHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(data);

    final Call<DefectHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mock Jobs Data.
   *
   * @return call value.
   * @throws Exception if any error in formating date.
   */
  @SuppressWarnings("unchecked")
  private Call<JobHDto> mockJobsData() throws Exception {
    final Response<JobHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(new JobHDto());

    final Call<JobHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mock Items Data.
   *
   * @return call value.
   * @throws Exception if any error in formating date.
   */
  @SuppressWarnings("unchecked")
  private Call<LazyItemHDto> mockItemsData() throws Exception {
    final Response<LazyItemHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(new LazyItemHDto());

    final Call<LazyItemHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mock coc Data.
   *
   * @return call value.
   * @throws Exception if any error in formating date.
   */
  @SuppressWarnings("unchecked")
  private Call<List<CoCHDto>> mockCocData() throws Exception {
    final List<CoCHDto> cocs = new ArrayList<>();
    cocs.add(new CoCHDto());

    final Response<List<CoCHDto>> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(cocs);

    final Call<List<CoCHDto>> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mock user profiles.
   *
   * @param userId the user id.
   * @throws ClassDirectException if any error in getting user.
   */
  private void mockUserProfile(final String userId) throws ClassDirectException {
    final UserProfileService userProfileService = context.getBean(UserProfileService.class);

    final UserProfiles user = new UserProfiles();
    user.setUserId(userId);
    user.setRestrictAll(false);
    when(userProfileService.getUser(eq(userId))).thenReturn(user);
  }

  /**
   * Spring in-class configurations.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create {@link DefectService} bean.
     *
     * @return defect service.
     *
     */
    @Override
    @Bean
    public DefectService defectService() {
      return new DefectServiceImpl();
    }

    /**
     * Create {@link AssetRetrofitService}.
     *
     * @return asset retrofit service.
     *
     */
    @Override
    @Bean
    public AssetRetrofitService assetRetrofitService() {
      return Mockito.mock(AssetRetrofitService.class);
    }

    /**
     * Create {@link JobRetrofitService}.
     *
     * @return job retrofit service.
     *
     */
    @Override
    @Bean
    public JobRetrofitService jobRetrofitService() {
      return Mockito.mock(JobRetrofitService.class);
    }
  }
}
