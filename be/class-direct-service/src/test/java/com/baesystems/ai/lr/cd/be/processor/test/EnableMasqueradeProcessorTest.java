package com.baesystems.ai.lr.cd.be.processor.test;

import java.util.HashSet;
import java.util.Set;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultMessage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import com.baesystems.ai.lr.cd.be.processor.EnableMasqueradeProcessor;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.impl.AesSecurityServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;

/**
 * Provides unit test methods for {@link EnableMasqueradeProcessor}.
 *
 * @author yng
 * @author sbollu
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EnableMasqueradeProcessorTest.Config.class)
public class EnableMasqueradeProcessorTest {

  /**
   * The {@link EnableMasqueradeProcessor} from {@link EnableMasqueradeProcessorTest.Config}.
   */
  @Autowired
  private EnableMasqueradeProcessor enableMasqueradeProcessor;

  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The test user id.
   */
  private static final String TEST_MASQ_USER_ID = "1";

  /**
   * The encrypted test user id.
   */
  private static final String ENCRYPTED_TEST_MASQ_USER_ID = "9JXkt9BIWW6dH4PnFzq8FA==";

  /**
   * Test Success scenario for {@link EnableMasqueradeProcessor#process(Exchange)}.
   *
   * @throws Exception if exchange is <code>null</code>.
   */
  @Test
  public final void processorFlowTest() throws Exception {

    final Exchange exchange = Mockito.mock(Exchange.class);

    // test header with value and null, without body
    Mockito.when(exchange.getIn()).thenReturn(mockDefaultMessage());

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUser());

    enableMasqueradeProcessor.process(exchange);
    Assert.assertEquals(TEST_MASQ_USER_ID, exchange.getIn().getHeader("userId"));
    Assert.assertNull(null, exchange.getIn().getHeader("null-header"));

    // test header with body not null
    final DefaultMessage message2 = mockDefaultMessage();
    message2.setBody(new Object());
    Mockito.when(exchange.getIn()).thenReturn(message2);

    enableMasqueradeProcessor.process(exchange);
    Assert.assertTrue(exchange.getIn().getBody() instanceof Object);
  }

  /**
   * Test failure scenario for {@link EnableMasqueradeProcessor#process(Exchange)}, if login user
   * doesn't have permission to masquerade.
   *
   *
   * @throws Exception if login user doesn't have permission to masquerade.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void testprocessorFlowForUnauthorisedUser() throws Exception {

    final Exchange exchange = Mockito.mock(Exchange.class);

    // test header with value and null, without body
    Mockito.when(exchange.getIn()).thenReturn(mockDefaultMessage());

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockNonAdminUser());

    enableMasqueradeProcessor.process(exchange);
  }


  /**
   * Test failure scenario for {@link EnableMasqueradeProcessor#process(Exchange)}, if user not
   * found with login userId.
   *
   * @throws Exception if user not found with login user id.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testprocessorFlowForNullUser() throws Exception {

    final Exchange exchange = Mockito.mock(Exchange.class);
    // test header with value and null, without body
    Mockito.when(exchange.getIn()).thenReturn(mockDefaultMessage());

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(null);

    enableMasqueradeProcessor.process(exchange);
  }

  /**
   * Test failure scenario for {@link EnableMasqueradeProcessor#process(Exchange)}, if userId is
   * null.
   *
   *
   * @throws Exception if login user id is null.
   */
  @Test(expected = ClassDirectException.class)
  public final void testprocessorFlowForNullUserId() throws Exception {

    final Exchange exchange = Mockito.mock(Exchange.class);
    // test header with value and null, without body
    Mockito.when(exchange.getIn()).thenReturn(mockDefaultMessageWithNullUserId());

    enableMasqueradeProcessor.process(exchange);
  }

  /**
   * Returns mock exchange message.
   *
   * @return the mock exchange message.
   */
  public final DefaultMessage mockDefaultMessage() {
    final DefaultMessage message = new DefaultMessage();
    message.setHeader("userId", "ABC");
    message.setHeader("x-masq-id", ENCRYPTED_TEST_MASQ_USER_ID);
    message.setHeader("null-header", null);

    return message;
  }

  /**
   * Returns mock exchange message with empty userId.
   *
   * @return the mock exchange message.
   */
  public final DefaultMessage mockDefaultMessageWithNullUserId() {
    final DefaultMessage message = new DefaultMessage();
    message.setHeader("userId", " ");
    message.setHeader("x-masq-id", ENCRYPTED_TEST_MASQ_USER_ID);
    message.setHeader("null-header", null);

    return message;
  }

  /**
   * Returns mock user with masquerade permission.
   *
   * @return mock user.
   */
  private UserProfiles mockUser() {
    UserProfiles userProfile = new UserProfiles();
    userProfile.setUserId("ABC");
    Roles role = new Roles();
    role.setRoleId(Role.LR_ADMIN.getId());
    role.setRoleName(Role.LR_ADMIN.toString());
    Set<Roles> rolesObject = new HashSet<Roles>();
    rolesObject.add(role);
    userProfile.setRoles(rolesObject);

    return userProfile;
  }

  /**
   * Returns mock user without masquerade permission.
   *
   * @return mock user.
   */
  private UserProfiles mockNonAdminUser() {
    UserProfiles userProfile = new UserProfiles();
    userProfile.setUserId("ABC");
    Roles role = new Roles();
    role.setRoleId(Role.CLIENT.getId());
    role.setRoleName(Role.CLIENT.toString());
    Set<Roles> rolesObject = new HashSet<Roles>();
    rolesObject.add(role);
    userProfile.setRoles(rolesObject);

    return userProfile;
  }

  /**
   *
   * Provides Spring in-class configurations.
   *
   * @author yng
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public EnableMasqueradeProcessor enableMasqueradeProcessor() {
      return new EnableMasqueradeProcessor();
    }

    @Override
    @Bean
    public AesSecurityService aesSecurityService() {
      return new AesSecurityServiceImpl();
    }
  }

}
