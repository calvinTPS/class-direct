package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.DeficiencyService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.impl.StatutoryFindingServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;

import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Unit test class for StatutoryFindingService.
 *
 * @author syalavarthi.
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest({Response.class, Resources.class})
@ContextConfiguration(classes = StatutoryFindingServiceTest.Config.class)
public class StatutoryFindingServiceTest {
  /**
   * Logger initialization.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(DeficiencyServiceTest.class);
  /**
   * The {@link AssetRetrofitService} from Spring context.
   *
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The {@link StatutoryFindingService} from Spring context.
   *
   */
  @Autowired
  private StatutoryFindingService statutoryService;
  /**
   * The {@link DeficiencyService} from Spring context.
   */
  @Autowired
  private DeficiencyService deficiencyService;
  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;
  /**
   * Page Size of 1.
   *
   */
  public static final int PAGE_SIZE = 1;

  /**
   * Tests {@link StatutoryFindingService#getStatutories(Long, CodicilDefectQueryHDto)} on success
   * scenario.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test
  public final void testGetStatutoryFindingsList() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(Calls.response(mockStatutoryFindings())).when(assetRetrofitService)
        .getStatutoryFindingsForAsset(Matchers.anyLong(), Matchers.any(CodicilDefectQueryHDto.class));
    doReturn(mockCodicilCategories()).when(assetReferenceService).getCodicilCategories();

    final List<StatutoryFindingHDto> response = statutoryService.getStatutories(1L, new CodicilDefectQueryHDto());
    Assert.assertNotNull(response);
    Assert.assertEquals(response.get(0).getTitle(), "statutory title");
    Assert.assertEquals(response.get(0).getStatusH().getName(), "Open");
    Assert.assertEquals(response.get(0).getActionTakenH().getName(), "Raised");

    Assert.assertNotNull(response.get(0).getDueDate());
    Assert.assertEquals(DateUtils.getDateToEpoch(response.get(0).getDueDate()), response.get(0).getDueDateEpoch());

    Assert.assertNotNull(response.get(0).getImposedDate());
    Assert.assertEquals(DateUtils.getDateToEpoch(response.get(0).getImposedDate()),
        response.get(0).getImposedDateEpoch());
    Assert.assertNotNull(response.get(0).getCategory());
    Assert.assertNotNull(response.get(0).getCategoryH());
  }

  /**
   * Tests {@link StatutoryFindingService#getStatutories(Long, CodicilDefectQueryHDto)} on negative
   * scenario.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test(expected = ClassDirectException.class)
  public final void testStatutoryFindingsFail() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(assetRetrofitService.getStatutoryFindingsForAsset(Matchers.anyLong(),
        Matchers.any(CodicilDefectQueryHDto.class))).thenReturn(Calls.failure(new IOException()));

    final List<StatutoryFindingHDto> response = statutoryService.getStatutories(1L, new CodicilDefectQueryHDto());
    Assert.assertNull(response);
  }

  /**
   * Tests
   * {@link StatutoryFindingService#getStatutorysPageResource(Integer, Integer, Long, CodicilDefectQueryHDto)}
   * on success scenario.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test
  public final void testGetStatutoryFindingsPageResource() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(Calls.response(mockStatutoryFindings())).when(assetRetrofitService)
        .getStatutoryFindingsForAsset(Matchers.anyLong(), Matchers.any(CodicilDefectQueryHDto.class));

    final PageResource<StatutoryFindingHDto> response =
        statutoryService.getStatutorysPageResource(1, PAGE_SIZE, 1L, new CodicilDefectQueryHDto());
    Assert.assertNotNull(response);
    Assert.assertEquals(response.getContent().size(), PAGE_SIZE);

  }

  /**
   * Tests success scenario to get statutory findings for due notifications
   * {@link StatutoryFindingService#getStatutoriesForDueNotification(Long, CodicilDefectQueryHDto)}.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test
  public final void testGetStatutoryFindingsDueNotifications() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(Calls.response(mockStatutoryFindings())).when(assetRetrofitService)
        .getStatutoryFindingsForAsset(Matchers.anyLong(), Matchers.any(CodicilDefectQueryHDto.class));

    final List<StatutoryFindingHDto> response =
        statutoryService.getStatutoriesForDueNotification(1L, new CodicilDefectQueryHDto());
    Assert.assertNotNull(response);
    Assert.assertEquals(response.size(), 2);

  }

  /**
   * Tests failure scenario to get statutory findings for due notifications
   * {@link StatutoryFindingService#getStatutoriesForDueNotification(Long, CodicilDefectQueryHDto)}
   * when mast api call fails.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetStatutoryFindingsDueNotificationsFail() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(assetRetrofitService.getStatutoryFindingsForAsset(Matchers.anyLong(),
        Matchers.any(CodicilDefectQueryHDto.class))).thenReturn(Calls.failure(new IOException("mock exception")));

    statutoryService.getStatutoriesForDueNotification(1L, new CodicilDefectQueryHDto());
  }


  /**
   * Tests {@link StatutoryFindingService#getStatutoryFindingById(Long, Long, Long)} on success
   * scenario.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test
  public final void testGetStatutoryFindings() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    doReturn(mockCodicilCategories()).when(assetReferenceService).getCodicilCategories();
    when(deficiencyService.getDeficiencyByAssetIdDeficiencyId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(mockDeficiency());
    doReturn(Calls.response(mockSingleStatutoryFinding())).when(assetRetrofitService)
        .getStatutoryFindingById(Matchers.anyLong(), Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobsData()));

    StatutoryFindingHDto statutory = statutoryService.getStatutoryFindingById(1L, 1L, 1L);
    Assert.assertNotNull(statutory);
    Assert.assertEquals(statutory.getDescription(), "Statutory1");
    Assert.assertEquals(statutory.getTitle(), "Statutory Title");
    Assert.assertEquals(statutory.getStatusH().getName(), "Open");
    Assert.assertNotNull(statutory.getDeficiencyH());
    Assert.assertEquals(statutory.getDeficiencyH().getTitle(), "Deficiency Title");
    Assert.assertNotNull(statutory.getCategory());
    Assert.assertNotNull(statutory.getCategoryH());
    Assert.assertNotNull(statutory.getJobH());
    Assert.assertEquals(statutory.getJobH().getJobNumber(), "123");
  }

  /**
   * Tests {@link StatutoryFindingService#getStatutoryFindingById(Long, Long, Long)} on negative
   * scenario.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetStatutoryFindingFail() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    when(assetRetrofitService.getStatutoryFindingById(Matchers.anyLong(), Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    StatutoryFindingHDto statutory = statutoryService.getStatutoryFindingById(1L, 1L, 1L);
    Assert.assertNull(statutory);
  }

  /**
   * Tests {@link StatutoryFindingService#getStatutoryFindingById(Long, Long, Long)} on negative
   * scenario internal api calls fail.
   *
   * @throws Exception if the external API call to get statutory hit error.
   */
  @Test
  public final void testGetStatutoryFindingDefaiciencyFail() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(deficiencyService.getDeficiencyByAssetIdDeficiencyId(Mockito.anyLong(), Mockito.anyLong()))
        .thenThrow(new ClassDirectException("Mock"));
    doReturn(Calls.response(mockSingleStatutoryFinding())).when(assetRetrofitService)
        .getStatutoryFindingById(Matchers.anyLong(), Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.failure(new IOException()));

    StatutoryFindingHDto statutory = statutoryService.getStatutoryFindingById(1L, 1L, 1L);
    Assert.assertNotNull(statutory);
    Assert.assertEquals(statutory.getDescription(), "Statutory1");
    Assert.assertEquals(statutory.getTitle(), "Statutory Title");
    Assert.assertEquals(statutory.getStatusH().getName(), "Open");
    Assert.assertNull(statutory.getDeficiencyH());
    Assert.assertNull(statutory.getJobH());
  }

  /**
   * Mocks list of statutory finding.
   *
   * @return statutory findings.
   */
  private List<StatutoryFindingHDto> mockStatutoryFindings() {

    List<StatutoryFindingHDto> statutoryFindings = new ArrayList<>();

    final Date today = Calendar.getInstance().getTime();

    StatutoryFindingHDto statutory1 = new StatutoryFindingHDto();
    statutory1.setId(1L);
    statutory1.setStatus(new LinkResource(23L));
    statutory1.setStatusH(new CodicilStatusHDto());
    statutory1.getStatusH().setName("Open");
    statutory1.setDescription("Statutory1");
    statutory1.setTitle("statutory title");
    statutory1.setDueDate(today);
    statutory1.setImposedDate(today);
    statutory1.setCategory(new LinkResource(1L));
    statutory1.setJob(new LinkResource(1L));

    statutory1.setActionTaken(new LinkResource(1L));
    statutory1.setActionTakenH(new ActionTakenHDto());
    statutory1.getActionTakenH().setName("Raised");
    statutoryFindings.add(statutory1);

    StatutoryFindingHDto statutory2 = new StatutoryFindingHDto();
    statutory2.setId(2L);
    statutory2.setStatus(new LinkResource(23L));
    statutory2.setDescription("Statutory2");
    statutory2.setCategory(new LinkResource(2L));
    statutoryFindings.add(statutory2);
    statutory2.setJob(new LinkResource(1L));
    statutory2.setAsset(new LinkResource(1L));

    return statutoryFindings;

  }

  /**
   * mock single statutory finding.
   *
   * @return single statutory.
   */
  private StatutoryFindingHDto mockSingleStatutoryFinding() {
    StatutoryFindingHDto statutory1 = new StatutoryFindingHDto();
    statutory1.setId(1L);
    statutory1.setStatus(new LinkResource(23L));
    statutory1.setStatusH(new CodicilStatusHDto());
    statutory1.getStatusH().setName("Open");
    statutory1.setNarrative("Narrative");
    statutory1.setDescription("Statutory1");
    statutory1.setTitle("Statutory Title");
    statutory1.setImposedDate(new Date());
    statutory1.setDueDate(new Date());
    statutory1.setCategory(new LinkResource(1L));
    statutory1.setJob(new LinkResource(1L));
    statutory1.setAsset(new LinkResource(1L));
    return statutory1;
  }

  /**
   * mock deficiency.
   *
   * @return deficiency.
   */
  private DeficiencyHDto mockDeficiency() {

    final DeficiencyHDto data1 = new DeficiencyHDto();
    data1.setId(1L);
    data1.setDeleted(false);
    data1.setJob(new LinkResource(1L));
    data1.getJob().setId(1L);
    data1.setTitle("Deficiency Title");
    return data1;
  }

  /**
   * Tests {@link StatutoryFindingServiceImpl#calculateDueStatus(StatutoryFindingHDto, LocalDate)}
   * positive scenario for given due date of Statutory Findings.
   *
   */
  @Test
  public final void calculateDueStatusTest() {
    final List<StatutoryFindingHDto> findings = new ArrayList<>();
    // OVERDUE Status
    // dueStatus should be overDue(Due Date is passed).
    final LocalDate twoDaysAgo = LocalDate.now().minusDays(2L);
    final LocalDate oneMonthago = LocalDate.now().minusMonths(1L);

    // overDue statutory findings.
    final StatutoryFindingHDto finding1 = new StatutoryFindingHDto();
    finding1.setDueDate(DateUtils.getLocalDateToDate(twoDaysAgo));
    findings.add(finding1);

    final StatutoryFindingHDto finding3 = new StatutoryFindingHDto();
    finding3.setDueDate(DateUtils.getLocalDateToDate(oneMonthago));
    findings.add(finding3);

    final DueStatus status1 = statutoryService.calculateDueStatus(finding1);
    Assert.assertEquals(DueStatus.OVER_DUE, status1);

    final DueStatus status3 = statutoryService.calculateDueStatus(finding3);
    Assert.assertEquals(DueStatus.OVER_DUE, status3);


    // IMMINENT Status
    // dueStatus should be imminent.(One month before the Due Date has passed)
    final LocalDate today = LocalDate.now();
    final LocalDate twentynineDaysBefore = LocalDate.now().plusMonths(1L).minusDays(1L);

    // imminent statutory findings.
    final StatutoryFindingHDto finding = new StatutoryFindingHDto();
    finding.setDueDate(DateUtils.getLocalDateToDate(today));
    findings.add(finding);

    final StatutoryFindingHDto finding4 = new StatutoryFindingHDto();
    finding4.setDueDate(DateUtils.getLocalDateToDate(twentynineDaysBefore));
    findings.add(finding4);

    final DueStatus status = statutoryService.calculateDueStatus(finding);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    final DueStatus status4 = statutoryService.calculateDueStatus(finding4);
    Assert.assertEquals(DueStatus.IMMINENT, status4);



    // DUESOON status
    // dueStatus should be duesoon.(Three months before the Due Date has passed)
    final LocalDate eightydaysbefore = LocalDate.now().plusMonths(3L).minusDays(1L);
    final LocalDate oneMonthbefore = LocalDate.now().plusMonths(1L);
    final LocalDate twoMonthsbefore = LocalDate.now().plusMonths(2L);

    // due soon statutory findings.
    final StatutoryFindingHDto finding5 = new StatutoryFindingHDto();
    finding5.setDueDate(DateUtils.getLocalDateToDate(oneMonthbefore));
    findings.add(finding5);

    final StatutoryFindingHDto finding7 = new StatutoryFindingHDto();
    finding7.setDueDate(DateUtils.getLocalDateToDate(eightydaysbefore));
    findings.add(finding7);

    final StatutoryFindingHDto finding2 = new StatutoryFindingHDto();
    finding2.setDueDate(DateUtils.getLocalDateToDate(twoMonthsbefore));
    findings.add(finding2);


    final DueStatus status2 = statutoryService.calculateDueStatus(finding2);
    Assert.assertEquals(DueStatus.DUE_SOON, status2);

    final DueStatus status5 = statutoryService.calculateDueStatus(finding5);
    Assert.assertEquals(DueStatus.DUE_SOON, status5);

    final DueStatus status7 = statutoryService.calculateDueStatus(finding7);
    Assert.assertEquals(DueStatus.DUE_SOON, status7);

    // NOTDUE Status
    // dueStatus should be notDue.(More than three months before the Due Date)
    final LocalDate threeMonthsBefore = LocalDate.now().plusMonths(3L).plusDays(1L);
    final LocalDate fourMonthsBefore = LocalDate.now().plusMonths(4L);


    // not due statutory findings.
    final StatutoryFindingHDto finding8 = new StatutoryFindingHDto();
    finding8.setDueDate(DateUtils.getLocalDateToDate(threeMonthsBefore));
    findings.add(finding8);

    final StatutoryFindingHDto finding6 = new StatutoryFindingHDto();
    finding6.setDueDate(DateUtils.getLocalDateToDate(fourMonthsBefore));
    findings.add(finding6);

    // due date null.default due status notDue.
    final StatutoryFindingHDto finding9 = new StatutoryFindingHDto();
    finding9.setDueDate(null);
    findings.add(finding9);

    final DueStatus status6 = statutoryService.calculateDueStatus(finding6);
    Assert.assertEquals(DueStatus.NOT_DUE, status6);

    final DueStatus status8 = statutoryService.calculateDueStatus(finding8);
    Assert.assertEquals(DueStatus.NOT_DUE, status8);

    final DueStatus status9 = statutoryService.calculateDueStatus(finding9);
    Assert.assertEquals(DueStatus.NOT_DUE, status9);

  }

  /**
   *
   * Mocks codicil category list.
   *
   * @return codicil categories.
   */
  private List<CodicilCategoryHDto> mockCodicilCategories() {
    final List<CodicilCategoryHDto> codicilCategories = new ArrayList<>();
    final CodicilCategoryHDto codicilCategory = new CodicilCategoryHDto();
    codicilCategory.setId(1L);
    codicilCategory.setName("TestCodicilCategory");
    codicilCategories.add(codicilCategory);

    return codicilCategories;
  }

  /**
   * Mocks Jobs Data.
   *
   * @return call value.
   */
  private JobHDto mockJobsData() {
    JobHDto job = new JobHDto();
    job.setId(1L);
    job.setJobNumber("123");
    return job;

  }

  /**
   * Spring in-class configurations.
   *
   * @author SYalavarthi.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create {@link StatutoryFindingService} bean.
     *
     * @return deficiency service.
     *
     */
    @Override
    @Bean
    public StatutoryFindingService statutoryService() {
      return new StatutoryFindingServiceImpl();
    }

    /**
     * Create {@link AssetRetrofitService}.
     *
     * @return asset retrofit service.
     *
     */
    @Override
    @Bean
    public AssetRetrofitService assetRetrofitService() {
      return Mockito.mock(AssetRetrofitService.class);
    }
  }
}
