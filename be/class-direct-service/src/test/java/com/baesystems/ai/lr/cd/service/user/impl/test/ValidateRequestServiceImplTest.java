package com.baesystems.ai.lr.cd.service.user.impl.test;


import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.user.UserDto;
import com.baesystems.ai.lr.cd.service.user.ValidateRequestService;
import com.baesystems.ai.lr.cd.service.user.impl.ValidateRequestServiceImpl;

/**
 * Test class for {@link ValidateRequestServiceImpl}.
 *
 * @author vmandalapu
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class ValidateRequestServiceImplTest {

  /**
   * test method.
   */
  @Test
  public final void validateRequestTest() {
    final ValidateRequestService requestService = new ValidateRequestServiceImpl();
    Assert.assertNotNull(requestService);

    final UserDto dto = requestService.requestHasRoles(EQUASIS.toLowerCase(Locale.getDefault()));
    Assert.assertNotNull(dto);
    Assert.assertTrue(dto.isEquasis());
    Assert.assertFalse(dto.isThetis());

    final UserDto dto1 = requestService.requestHasRoles(THETIS.toLowerCase(Locale.getDefault()));
    Assert.assertNotNull(dto1);
    Assert.assertTrue(dto1.isThetis());
    Assert.assertFalse(dto1.isEquasis());

    final UserDto dto2 = requestService.requestHasRoles(null);
    Assert.assertNotNull(dto2);
    Assert.assertFalse(dto2.isThetis());
    Assert.assertFalse(dto2.isEquasis());
  }
}
