package com.baesystems.ai.lr.cd.be.service.auditservice.test;

import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightListHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.baesystems.ai.lr.cd.service.audit.impl.AuditServiceImpl;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.task.impl.TaskServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.fasterxml.jackson.core.JsonProcessingException;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for audit service {@link AuditService} for crediting mpms tasks.
 *
 * @author syalavarthi.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AuditScenarioCreditMPMSTask {

  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The {@link TaskRetrofitService} from Spring context.
   */
  @Autowired
  private TaskRetrofitService taskRetrofit;
  /**
   * The {@link TaskService} from Spring context.
   */
  @Autowired
  private TaskService taskService;

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public final void init() {
    final CDAuthToken auth = new CDAuthToken("test");
    auth.setPrincipal("101");
    final UserProfiles user = new UserProfiles();
    user.setUserId("111");
    user.setClientCode("100100");
    user.setShipBuilderCode("100100");
    user.setFlagCode("ALA");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success audit scenario to credit mpms tasks
   * {@link TaskService#updateTasksAsList(com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightListHDto)}.
   *
   * @throws RecordNotFoundException if no record admin user performs request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public void testUpdateMpmsTasks() throws RecordNotFoundException, ClassDirectException, JsonProcessingException {

    final Date currentDate = DateUtils.getLocalDateToDate(LocalDate.now());

    WorkItemLightListHDto workItemLightList = new WorkItemLightListHDto();
    List<WorkItemLightHDto> workItemLightDtos = new ArrayList<>();
    WorkItemLightHDto workItemLightDto = new WorkItemLightHDto();
    workItemLightDto.setId(1L);
    workItemLightDto.setUpdatedBy("LONLBZ");
    workItemLightDto.setUpdatedDate(currentDate);
    workItemLightDto.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto);

    WorkItemLightHDto workItemLightDto1 = new WorkItemLightHDto();
    workItemLightDto1.setId(2L);
    workItemLightDto1.setUpdatedBy("LONLBZ");
    workItemLightDto1.setUpdatedDate(currentDate);
    workItemLightDto1.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto1);

    workItemLightList.setTasks(workItemLightDtos);

    Mockito.when(taskRetrofit.updateTaskList(Mockito.any(WorkItemLightListDto.class)))
        .thenReturn(Calls.response(workItemList()));

    when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUser());

    WorkItemLightListHDto lightListDto = taskService.updateTasksAsList(workItemLightList);
    Assert.assertEquals(currentDate, lightListDto.getTasks().get(0).getPmsCreditDate());
    Assert.assertEquals(currentDate, lightListDto.getTasks().get(1).getPmsCreditDate());

  }

  /**
   * Returns mock user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("LR_ADMIN");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }


  /**
   * Returns mock listDto consist of task list.
   *
   * @return mock tasklistDto.
   */
  protected WorkItemLightListDto workItemList() {

    final Date currentDate = DateUtils.getLocalDateToDate(LocalDate.now());

    WorkItemLightListDto workItemLightListDto = new WorkItemLightListDto();
    List<WorkItemLightDto> workItemLightDtos = new ArrayList<>();
    WorkItemLightDto workItemLightDto = new WorkItemLightDto();
    workItemLightDto.setId(1L);
    workItemLightDto.setUpdatedBy("LONLBZ");
    workItemLightDto.setUpdatedDate(currentDate);
    workItemLightDto.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto);

    WorkItemLightDto workItemLightDto1 = new WorkItemLightDto();
    workItemLightDto1.setId(2L);
    workItemLightDto1.setUpdatedBy("LONLBZ");
    workItemLightDto1.setUpdatedDate(currentDate);
    workItemLightDto1.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto1);
    workItemLightListDto.setTasks(workItemLightDtos);


    return workItemLightListDto;
  }

  /**
   * Inner config.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Mocks task service.
     *
     * @return mock object.
     */
    @Override
    public TaskService taskService() {
      return new TaskServiceImpl();
    }

    /**
     * Mocks audit service.
     *
     * @return mock object.
     */
    @Override
    public AuditService auditService() {
      return new AuditServiceImpl();
    }
  }

}
