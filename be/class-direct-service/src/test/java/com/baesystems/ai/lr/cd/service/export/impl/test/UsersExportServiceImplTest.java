package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.UsersExportService;
import com.baesystems.ai.lr.cd.service.export.impl.UsersExportServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;


/**
 * Provides unit tests for user export service {@link UsersExportService}.
 *
 * @author syalavarthi.
 *
 */

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = UsersExportServiceImplTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class UsersExportServiceImplTest {

  /**
   * The {@link UsersExportService} from spring context.
   */
  @Autowired
  private UsersExportService usersExportService;
  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The {@link AmazonStorageService} from spring context.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;
  /**
   * The csv file header.
   */
  @Value("${user.export.csv.fileheader}")
  private String fileheader;

  /**
   * Resets mock.
   */
  @Before
  public void resetMock() {

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success scenario {@link UsersExportService#downloadFile(UserProfileDto)} users export.
   * <p>
   * with single user email using {@link UserProfileDto}.
   * </p>
   *
   * @throws ClassDirectException if API call from MAST, or PDF template compilation fail.
   * @throws IOException if error generating csv.
   */
  @Test
  public final void downloadFileTest() throws ClassDirectException, IOException {
    // create UserProfileDto mock body
    final UserProfileDto dto = new UserProfileDto();
    dto.setEmail("david.classdirect@gmail.com");

    Mockito.doReturn(mockUser()).when(userProfileService).getUsers(Mockito.any(UserProfileDto.class),
        Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Mockito.isNull(String.class),
        Mockito.isNull(String.class));

    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    // call method
    final StringResponse response = usersExportService.downloadFile(dto);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   * Tests success scenario {@link UsersExportService#downloadFile(UserProfileDto)} users export.
   * <p>
   * with multiple users as included users using {@link UserProfileDto}.
   * </p>
   *
   * @throws ClassDirectException if API call from MAST, or PDF template compilation fail.
   * @throws IOException if error generating csv.
   */
  @Test
  public final void downloadFileTestMultiple() throws ClassDirectException, IOException {
    // create UserProfileDto mock body
    final UserProfileDto dto = new UserProfileDto();
    final List<String> includedUser = new ArrayList<>();
    includedUser.add("david.classdirect@gmail.com");
    includedUser.add("alice.classdirect@gmail.com");
    includedUser.add("bob.classdirect@gmail.com");
    dto.setIncludedUsers(includedUser);
    Mockito.doReturn(mockUserList()).when(userProfileService).getUsers(Mockito.any(UserProfileDto.class),
        Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Mockito.isNull(String.class),
        Mockito.isNull(String.class));

    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    // call method
    final StringResponse response = usersExportService.downloadFile(dto);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());

  }

  /**
   * Mock single user.
   *
   * @return user.
   */
  private List<UserProfiles> mockUser() {
    final List<UserProfiles> userProfiles = new ArrayList<>();
    final UserProfiles userProfile = new UserProfiles();
    userProfile.setUserId("052d354b-096a-407c-bf5e-34a38a646525");
    userProfile.setFirstName("David");
    userProfile.setLastName("Marsh");
    userProfile.setShipBuilderCode("5");
    userProfile.setEmail("david.classdirect@gmail.com");
    userProfile.setLastLogin(LocalDateTime.of(2017, 12, 9, 2, 20, 20));
    final UserAccountStatus status = new UserAccountStatus();
    status.setName("Active");
    status.setId(1L);
    userProfile.setStatus(status);
    final Roles role = new Roles();
    role.setRoleId(5L);
    role.setRoleName("SHIP_BUILDER");
    final Set<Roles> rolesObject = new HashSet<Roles>();
    rolesObject.add(role);
    userProfile.setRoles(rolesObject);
    userProfiles.add(userProfile);
    return userProfiles;
  }

  /**
   * mock multiple users.
   *
   * @return users list.
   */
  private List<UserProfiles> mockUserList() {
    final List<UserProfiles> userProfiles = mockUser();
    final UserProfiles userProfile = new UserProfiles();
    userProfile.setUserId("=052d354b-096a-407c-bf5e-34a38a646527");
    userProfile.setFirstName("=alice");
    userProfile.setLastName("=Marsh");
    userProfile.setFlagCode("=ALA");
    userProfile.setClientCode(null);
    userProfile.setEmail("alice.classdirect@gmail.com");
    userProfile.setLastLogin(null);
    userProfile.setCompany(new Company());
    userProfile.setCompanyName("");
    userProfile.setName("alice" + " " + "Marsh");
    final UserAccountStatus status = new UserAccountStatus();
    status.setName("Active");
    status.setId(1L);
    userProfile.setStatus(status);
    final Roles role = new Roles();
    role.setRoleId(5L);
    role.setRoleName("=CLIENT");
    final Set<Roles> rolesObject = new HashSet<Roles>();
    rolesObject.add(role);
    userProfile.setRoles(rolesObject);
    userProfile.setDateOfModification(LocalDateTime.of(2017, 12, 9, 2, 20, 20));
    userProfiles.add(userProfile);

    final UserProfiles userProfile1 = new UserProfiles();
    userProfile1.setUserId("052d354b-096a-407c-bf5e-34a38a646526");
    userProfile1.setFirstName("bob");
    userProfile1.setLastName("Marsh");
    userProfile1.setClientCode("5");
    userProfile1.setEmail("bob.classdirect@gmail.com");
    final UserAccountStatus status1 = new UserAccountStatus();
    status1.setName("Active");
    status1.setId(1L);
    userProfile1.setStatus(status1);
    final Roles role1 = new Roles();
    role1.setRoleId(5L);
    role1.setRoleName("LR_ADMIN");
    final Set<Roles> rolesObject1 = new HashSet<Roles>();
    rolesObject1.add(role1);
    userProfile1.setRoles(rolesObject1);
    userProfile1.setDateOfModification(LocalDateTime.of(2017, 12, 9, 2, 20, 20));
    userProfile1.setLastLogin(LocalDateTime.of(2017, 12, 9, 2, 20, 20));
    userProfiles.add(userProfile1);
    final UserProfiles userProfile2 = new UserProfiles();
    userProfile2.setUserId("052d354b-096a-407c-bf5e-34a38a646528");
    userProfile2.setFirstName("abc");
    userProfile2.setLastName("Marsh");
    userProfile2.setClientCode("5");
    userProfile2.setFlagCode(null);
    userProfile2.setShipBuilderCode(null);
    userProfile2.setEmail("abc.classdirect@gmail.com");
    final UserAccountStatus status2 = new UserAccountStatus();
    status2.setName("Active");
    status2.setId(1L);
    userProfile2.setStatus(status2);
    userProfile2.setRoles(null);
    userProfile2.setDateOfModification(null);
    userProfile2.setCompany(null);
    userProfiles.add(userProfile2);
    return userProfiles;
  }

  /**
   * Spring in-class configurations.
   *
   * @author SYalavarthi
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create {@link UsersExportService} bean.
     *
     * @return UsersExportService value.
     */
    @Bean
    @Override
    public UsersExportService usersExportService() {
      return new UsersExportServiceImpl();
    }
  }
}
