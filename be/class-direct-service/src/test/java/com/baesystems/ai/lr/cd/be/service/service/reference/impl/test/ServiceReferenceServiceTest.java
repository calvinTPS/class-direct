package com.baesystems.ai.lr.cd.be.service.service.reference.impl.test;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.reset;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceGroupHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.service.reference.impl.ServiceReferenceServiceImpl;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.ServiceHelper;
import com.baesystems.ai.lr.cd.be.utils.ServiceHelperImpl;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for service reference {@link ServiceReferenceService}.
 *
 * @author VMandalapu
 * @author syalavarthi 13-6-2017.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceReferenceServiceTest.Config.class)
@DirtiesContext
public class ServiceReferenceServiceTest {
  /**
   * The spring application context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link ServiceReferenceService} from spring context.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;
  /**
   * The {@link ProductReferenceService} from spring context.
   */
  @Autowired
  private ProductReferenceService productReferenceService;

  /**
   * The notification period.
   */
  @Value("${notification.due.status.period}")
  private String notificationPeriodOffSet;

  /**
   * The mock object initialization.
   *
   */
  @Before
  public final void init() {
    reset(context.getBean(ServiceReferenceRetrofitService.class));
  }

  /**
   * Tests multiple success scenarios.
   * <ul>
   * <li>get list of service groups {@link ServiceReferenceService#getServiceGroups()}.</li>
   * <li>get single service group by id {@link ServiceReferenceService#getServiceGroup(Long)}.</li>
   * <li>get list of service catalogues {@link ServiceReferenceService#getServiceCatalogues()}.</li>
   * <li>get single service catalogue {@link ServiceReferenceService#getServiceCatalogue(Long)}.
   * </li>
   * <li>get list of service credit statuses
   * {@link ServiceReferenceService#getServiceCreditStatuses()}.</li>
   * <li>get single service credit status by id
   * {@link ServiceReferenceService#getServiceCreditStatus(Long)}.</li>
   * <li>get list of services for asset
   * {@link ServiceReferenceService#getServicesForAsset(Long)}.</li>
   * </ul>
   *
   * @throws IOException if any of mast api fails.
   */
  @Test
  public final void successfullyRetrieveListOfReferenceServices() throws IOException {

    mockServiceAssetList(3L);

    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);

    Mockito.when(retrofitService.getServiceGroups()).thenReturn(Calls.response(mockServiceGroups()));

    Mockito.when(retrofitService.getServiceCatalogues()).thenReturn(Calls.response(mockServiceCatalogues()));

    Mockito.when(retrofitService.getServiceCreditStatuses()).thenReturn(Calls.response(mockServiceCreditStatuses()));
    Mockito.when(productReferenceService.getProductGroup(anyLong())).thenReturn(mockProductGroup());

    List<ServiceGroupHDto> serviceGroupHDtos = serviceReferenceService.getServiceGroups();
    Assert.assertNotNull(serviceGroupHDtos);

    ServiceGroupHDto serviceGroupHDto = serviceReferenceService.getServiceGroup(1L);
    Assert.assertNotNull(serviceGroupHDto);

    List<ServiceCatalogueHDto> serviceCatalogueHDtos = serviceReferenceService.getServiceCatalogues();
    Assert.assertNotNull(serviceCatalogueHDtos);

    ServiceCatalogueHDto serviceCatalogueHDto = serviceReferenceService.getServiceCatalogue(1L);
    Assert.assertNotNull(serviceCatalogueHDto);

    List<ServiceCreditStatusHDto> serviceCreditStatusHDtos = serviceReferenceService.getServiceCreditStatuses();
    Assert.assertNotNull(serviceCreditStatusHDtos);

    ServiceCreditStatusHDto serviceCreditStatusHDto = serviceReferenceService.getServiceCreditStatus(1L);
    Assert.assertNotNull(serviceCreditStatusHDto);

    List<ScheduledServiceHDto> scheduledServiceHDtos = serviceReferenceService.getServicesForAsset(1L);
    Assert.assertNotNull(scheduledServiceHDtos);
    scheduledServiceHDtos.stream().forEach(ser -> Assert.assertTrue(ser.getActive().equals(Boolean.TRUE)));
    scheduledServiceHDtos.stream().forEach(ser -> Assert.assertTrue(ser.getServiceCatalogueName() != null));
    scheduledServiceHDtos.stream().forEach(ser -> Assert.assertNotNull(ser.getServiceCatalogueH().getProductGroupH()));

  }

  /**
   * Tests failure scenario to get list of services for asset
   * {@link ServiceReferenceService#getServicesForAsset(Long)} when error in getting product
   * group.
   *
   * @throws IOException if any of mast api fails.
   */
  @Test
  public final void testGetServicesForAsset() throws IOException {

    mockServiceAssetList(3L);

    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);

    Mockito.when(retrofitService.getServiceGroups()).thenReturn(Calls.response(mockServiceGroups()));

    Mockito.when(productReferenceService.getProductGroup(anyLong())).thenReturn(null);
    List<ScheduledServiceHDto> services = serviceReferenceService.getServicesForAsset(1L);
    Assert.assertNotNull(services);
    services.stream().forEach(ser -> Assert.assertTrue(ser.getActive().equals(Boolean.TRUE)));
    services.stream().forEach(ser -> Assert.assertTrue(ser.getServiceCatalogueName() != null));
    services.stream().forEach(ser -> Assert.assertNull(ser.getServiceCatalogueH().getProductGroupH()));

  }

  /**
   * Tests success scenario to calculate due status for services
   * {@link ServiceReferenceService#calculateDueStatus(ScheduledServiceHDto)}.
   *
   * @throws IOException if mast api fails to get services for asset.
   */
  @Test
  public final void calculateDueStatusTest() throws IOException {
    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);
    List<ScheduledServiceHDto> ss = new ArrayList<>();

    final LocalDate date = LocalDate.now();
    final LocalDate date1 = LocalDate.now().minusDays(2L);
    final LocalDate date3 = LocalDate.now().plusMonths(1L);
    final LocalDate date4 = LocalDate.now().minusMonths(1L);
    // To cater non-leap year February month (no of days = 28 days).
    final LocalDate date5 = LocalDate.now().plusMonths(1L).minusDays(1L);
    final LocalDate date6 = LocalDate.now().plusMonths(3L).plusDays(1L);
    final LocalDate date7 = LocalDate.now().plusMonths(1L).plusDays(1L);
    final LocalDate date8 = LocalDate.now().plusMonths(3L).minusDays(1L);

    ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setPostponementDate(DateUtils.getLocalDateToDate(date));
    ss.add(service);

    ScheduledServiceHDto service3 = new ScheduledServiceHDto();
    service3.setPostponementDate(DateUtils.getLocalDateToDate(date1));
    ss.add(service3);

    ScheduledServiceHDto service4 = new ScheduledServiceHDto();
    service4.setPostponementDate(DateUtils.getLocalDateToDate(date3));
    ss.add(service4);

    ScheduledServiceHDto service5 = new ScheduledServiceHDto();
    service5.setPostponementDate(DateUtils.getLocalDateToDate(date4));
    ss.add(service5);

    ScheduledServiceHDto service6 = new ScheduledServiceHDto();
    service6.setPostponementDate(DateUtils.getLocalDateToDate(date5));
    ss.add(service6);

    ScheduledServiceHDto service7 = new ScheduledServiceHDto();
    service7.setPostponementDate(DateUtils.getLocalDateToDate(date6));
    ss.add(service7);

    ScheduledServiceHDto service8 = new ScheduledServiceHDto();
    service8.setPostponementDate(DateUtils.getLocalDateToDate(date7));
    ss.add(service8);

    ScheduledServiceHDto service9 = new ScheduledServiceHDto();
    service9.setPostponementDate(DateUtils.getLocalDateToDate(date8));
    ss.add(service9);

    Mockito.when(retrofitService.getAssetServices(Mockito.any(Long.class))).thenReturn(Calls.response(ss));

    DueStatus status = serviceReferenceService.calculateDueStatus(service);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    DueStatus status3 = serviceReferenceService.calculateDueStatus(service3);
    Assert.assertEquals(DueStatus.OVER_DUE, status3);

    DueStatus status4 = serviceReferenceService.calculateDueStatus(service4);
    Assert.assertEquals(DueStatus.NOT_DUE, status4);

    DueStatus status5 = serviceReferenceService.calculateDueStatus(service5);
    Assert.assertEquals(DueStatus.OVER_DUE, status5);

    DueStatus status6 = serviceReferenceService.calculateDueStatus(service6);
    Assert.assertEquals(DueStatus.IMMINENT, status6);

    DueStatus status7 = serviceReferenceService.calculateDueStatus(service7);
    Assert.assertEquals(DueStatus.NOT_DUE, status7);

    DueStatus status8 = serviceReferenceService.calculateDueStatus(service8);
    Assert.assertEquals(DueStatus.DUE_SOON, status8);

    DueStatus status9 = serviceReferenceService.calculateDueStatus(service9);
    Assert.assertEquals(DueStatus.DUE_SOON, status9);

  }

  /**
   * Tests success scenario to calculate due status for services
   * {@link ServiceReferenceService#calculateDueStatus(ScheduledServiceHDto)} with earlier
   * postponement date.
   *
   * @throws IOException if mast api fails to get services for asset.
   */
  @Test
  public final void calculateDueStatusTestWithEarlierDate() throws IOException {
    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);
    List<ScheduledServiceHDto> ss = new ArrayList<>();
    final Long oneDay = 1L;
    final Long threeDays = 3L;
    final Long oneMonth = 1L;
    // Changed value according variable name.
    // This is point of reference for date from due status is calculated.
    final LocalDate previousDate = LocalDate.now().minusDays(oneDay);

    final LocalDate date = previousDate;
    final LocalDate date2 = previousDate.minusDays(threeDays);
    // Date 3 is changed to "exceeding 1 month" form current date in order to have "Not Due" Status.
    final LocalDate date3 = previousDate.plusMonths(oneMonth).plusDays(oneDay);
    final LocalDate date4 = previousDate.minusMonths(oneMonth).minusDays(oneDay);
    // Cater for non-leap year February month.
    final LocalDate date5 = previousDate.plusMonths(oneMonth).minusDays(2L);
    final LocalDate date6 = previousDate.plusMonths(3L).plusDays(threeDays);

    ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setPostponementDate(DateUtils.getLocalDateToDate(date));
    ss.add(service);

    ScheduledServiceHDto service3 = new ScheduledServiceHDto();
    service3.setPostponementDate(DateUtils.getLocalDateToDate(date2));
    ss.add(service3);

    ScheduledServiceHDto service4 = new ScheduledServiceHDto();
    service4.setPostponementDate(DateUtils.getLocalDateToDate(date3));
    ss.add(service4);

    ScheduledServiceHDto service5 = new ScheduledServiceHDto();
    service5.setPostponementDate(DateUtils.getLocalDateToDate(date4));
    ss.add(service5);

    ScheduledServiceHDto service6 = new ScheduledServiceHDto();
    service6.setPostponementDate(DateUtils.getLocalDateToDate(date5));
    ss.add(service6);

    ScheduledServiceHDto service7 = new ScheduledServiceHDto();
    service7.setPostponementDate(DateUtils.getLocalDateToDate(date6));
    ss.add(service7);

    Mockito.when(retrofitService.getAssetServices(Mockito.any(Long.class))).thenReturn(Calls.response(ss));

    DueStatus status = serviceReferenceService.calculateDueStatus(service, previousDate);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    DueStatus status3 = serviceReferenceService.calculateDueStatus(service3, previousDate);
    Assert.assertEquals(DueStatus.OVER_DUE, status3);

    DueStatus status4 = serviceReferenceService.calculateDueStatus(service4, previousDate);
    Assert.assertEquals(DueStatus.DUE_SOON, status4);

    DueStatus status5 = serviceReferenceService.calculateDueStatus(service5, previousDate);
    Assert.assertEquals(DueStatus.OVER_DUE, status5);

    DueStatus status6 = serviceReferenceService.calculateDueStatus(service6, previousDate);
    Assert.assertEquals(DueStatus.IMMINENT, status6);

    DueStatus status7 = serviceReferenceService.calculateDueStatus(service7, previousDate);
    Assert.assertEquals(DueStatus.NOT_DUE, status7);
  }

  /**
   * Tests success scenario to calculate due status for services
   * {@link ServiceReferenceService#calculateDueStatus(ScheduledServiceHDto)} with postponement date
   * null.
   *
   * @throws IOException if mast api fails to get services for asset.
   */
  @Test
  public final void calculateDueStatusTestNull() throws IOException {

    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);
    List<ScheduledServiceHDto> ss = new ArrayList<>();

    final LocalDate date = LocalDate.now();
    final LocalDate date1 = LocalDate.now().minusDays(2L);
    final LocalDate date3 = LocalDate.now().plusMonths(3L).plusDays(10L);
    final LocalDate date4 = LocalDate.now().minusMonths(1L);
    // To cater non-leap year February month (no of days = 28 days).
    final LocalDate date5 = LocalDate.now().plusMonths(1L).minusDays(1L);
    final LocalDate date6 = LocalDate.now().plusMonths(2L);
    final LocalDate date7 = LocalDate.now().plusMonths(3L);

    ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setPostponementDate(null);
    service.setUpperRangeDate(DateUtils.getLocalDateToDate(date));
    service.setDueDate(null);
    ss.add(service);

    ScheduledServiceHDto service3 = new ScheduledServiceHDto();
    service3.setPostponementDate(null);
    service3.setUpperRangeDate(null);
    service3.setDueDate(DateUtils.getLocalDateToDate(date));
    ss.add(service3);

    ScheduledServiceHDto service4 = new ScheduledServiceHDto();
    service4.setPostponementDate(DateUtils.getLocalDateToDate(date3));
    ss.add(service4);

    ScheduledServiceHDto service5 = new ScheduledServiceHDto();
    service5.setPostponementDate(null);
    service5.setUpperRangeDate(DateUtils.getLocalDateToDate(date4));
    ss.add(service5);

    ScheduledServiceHDto service6 = new ScheduledServiceHDto();
    service6.setPostponementDate(null);
    service6.setDueDate(DateUtils.getLocalDateToDate(date4));
    ss.add(service6);

    ScheduledServiceHDto service7 = new ScheduledServiceHDto();
    service7.setPostponementDate(DateUtils.getLocalDateToDate(date6));
    ss.add(service7);

    ScheduledServiceHDto service8 = new ScheduledServiceHDto();
    service8.setPostponementDate(null);
    service8.setUpperRangeDate(DateUtils.getLocalDateToDate(date3));
    ss.add(service8);

    ScheduledServiceHDto service9 = new ScheduledServiceHDto();
    service9.setPostponementDate(null);
    service9.setDueDate(DateUtils.getLocalDateToDate(date3));
    ss.add(service9);

    ScheduledServiceHDto service10 = new ScheduledServiceHDto();
    service10.setPostponementDate(null);
    service10.setLowerRangeDate(DateUtils.getLocalDateToDate(date));
    ss.add(service10);

    ScheduledServiceHDto service11 = new ScheduledServiceHDto();
    service11.setPostponementDate(null);
    service11.setLowerRangeDate(DateUtils.getLocalDateToDate(date1));
    ss.add(service11);

    ScheduledServiceHDto service12 = new ScheduledServiceHDto();
    service12.setPostponementDate(null);
    service12.setUpperRangeDate(null);
    service12.setDueDate(DateUtils.getLocalDateToDate(date7));
    ss.add(service12);

    ScheduledServiceHDto service13 = new ScheduledServiceHDto();
    service13.setPostponementDate(null);
    service13.setUpperRangeDate(null);
    service13.setDueDate(DateUtils.getLocalDateToDate(date6));
    ss.add(service13);

    ScheduledServiceHDto service14 = new ScheduledServiceHDto();
    service14.setPostponementDate(null);
    service14.setUpperRangeDate(DateUtils.getLocalDateToDate(date6));
    ss.add(service14);

    ScheduledServiceHDto service15 = new ScheduledServiceHDto();
    service15.setPostponementDate(null);
    service15.setUpperRangeDate(DateUtils.getLocalDateToDate(date7));
    ss.add(service15);

    ScheduledServiceHDto service16 = new ScheduledServiceHDto();
    service16.setPostponementDate(null);
    service16.setUpperRangeDate(DateUtils.getLocalDateToDate(date5));
    ss.add(service16);

    Mockito.when(retrofitService.getAssetServices(Mockito.any(Long.class))).thenReturn(Calls.response(ss));

    DueStatus status = serviceReferenceService.calculateDueStatus(service);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    DueStatus status3 = serviceReferenceService.calculateDueStatus(service3);
    Assert.assertEquals(DueStatus.IMMINENT, status3);

    DueStatus status4 = serviceReferenceService.calculateDueStatus(service4);
    Assert.assertEquals(DueStatus.NOT_DUE, status4);

    DueStatus status5 = serviceReferenceService.calculateDueStatus(service5);
    Assert.assertEquals(DueStatus.OVER_DUE, status5);

    DueStatus status6 = serviceReferenceService.calculateDueStatus(service6);
    Assert.assertEquals(DueStatus.OVER_DUE, status6);

    DueStatus status7 = serviceReferenceService.calculateDueStatus(service7);
    Assert.assertEquals(DueStatus.DUE_SOON, status7);

    DueStatus status8 = serviceReferenceService.calculateDueStatus(service8);
    Assert.assertEquals(DueStatus.NOT_DUE, status8);

    DueStatus status9 = serviceReferenceService.calculateDueStatus(service9);
    Assert.assertEquals(DueStatus.NOT_DUE, status9);

    DueStatus status10 = serviceReferenceService.calculateDueStatus(service10);
    Assert.assertEquals(DueStatus.NOT_DUE, status10);

    DueStatus status11 = serviceReferenceService.calculateDueStatus(service11);
    Assert.assertEquals(DueStatus.DUE_SOON, status11);

    DueStatus status12 = serviceReferenceService.calculateDueStatus(service12);
    Assert.assertEquals(DueStatus.NOT_DUE, status12);

    DueStatus status13 = serviceReferenceService.calculateDueStatus(service13);
    Assert.assertEquals(DueStatus.DUE_SOON, status13);

    DueStatus status14 = serviceReferenceService.calculateDueStatus(service14);
    Assert.assertEquals(DueStatus.DUE_SOON, status14);

    DueStatus status15 = serviceReferenceService.calculateDueStatus(service15);
    Assert.assertEquals(DueStatus.NOT_DUE, status15);

    DueStatus status16 = serviceReferenceService.calculateDueStatus(service16);
    Assert.assertEquals(DueStatus.IMMINENT, status16);
  }

  /**
   * Tests failure scenario to get service groups {@link ServiceReferenceService#getServiceGroups()}
   * when mast api fails.
   *
   * @throws IOException if mast api fail.
   */
  @Test(expected = IOException.class)
  public final void testServiceGroupException() throws IOException {
    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);
    Mockito.when(retrofitService.getServiceGroups()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    serviceReferenceService.getServiceGroups();
  }

  /**
   * Tests success scenario to get service catalogue
   * {@link ServiceReferenceService#mockServiceCatalogues()}.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public final void successfullyRetrieveListOfServiceCatalogues() throws IOException {

    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);

    List<ServiceCatalogueHDto> mockServiceCatalogues = mockServiceCatalogues();
    Mockito.when(retrofitService.getServiceCatalogues()).thenReturn(Calls.response(mockServiceCatalogues));

    List<ServiceCatalogueHDto> serviceCatalogueHDtos = serviceReferenceService.getServiceCatalogues();

    Assert.assertNotNull(serviceCatalogueHDtos);
    Assert.assertEquals(mockServiceCatalogues.size(), serviceCatalogueHDtos.size());
  }

  /**
   * Tests success scenario to get service catalogue
   * {@link ServiceReferenceService#mockServiceCatalogues()} with deleted flag true.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public final void successfullyRetrieveListOfActiveServiceCatalogues() throws IOException {

    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);

    Mockito.when(retrofitService.getServiceCatalogues()).thenReturn(Calls.response(mockServiceCatalogues()));

    List<ServiceCatalogueHDto> serviceCatalogueHDtos = serviceReferenceService.getActiveServiceCatalogues();

    Assert.assertNotNull(serviceCatalogueHDtos);
    serviceCatalogueHDtos.forEach(
        catalogue -> Assert.assertFalse("ServiceCatalogue isDeleted flag should be false.", catalogue.getDeleted()));
  }

  /**
   * Mocks asset service list.
   *
   * @param count the size of services for asset.
   */
  private void mockServiceAssetList(final Long count) {
    ServiceReferenceRetrofitService retrofitService = context.getBean(ServiceReferenceRetrofitService.class);
    List<ScheduledServiceHDto> assets = new ArrayList<>();

    for (int i = 0; i < count; i++) {
      assets.add(mockServiceAsset(1L + new Integer(i).longValue()));
    }

    Mockito.when(retrofitService.getAssetServicesQuery(Mockito.anyLong(), Mockito.anyMap()))
        .thenReturn(Calls.response(assets));
  }

  /**
   * Mocks scheduled service.
   *
   * @param id the primary key of asset.
   * @return scheduled service.
   *
   */
  private ScheduledServiceHDto mockServiceAsset(final Long id) {
    Long assetId = id;
    if (null == assetId) {
      assetId = 1L;
    }

    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    dto.setId(assetId);
    final Long oneMonth = 1L;
    dto.setId(assetId);
    dto.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusMonths(oneMonth)));
    dto.setDueStatusH(serviceReferenceService.calculateDueStatus(dto, LocalDate.now()));
    ServiceCatalogueHDto serviceCatalogue = new ServiceCatalogueHDto();
    serviceCatalogue.setId(id);
    serviceCatalogue.setName("name" + id);
    serviceCatalogue.setProductCatalogue(new ProductCatalogueDto());
    serviceCatalogue.getProductCatalogue().setProductGroup(new LinkResource());
    serviceCatalogue.getProductCatalogue().getProductGroup().setId(id);
    dto.setServiceCatalogueH(serviceCatalogue);
    if (assetId / 2 != 0) {

      dto.setActive(Boolean.TRUE);
    } else {
      dto.setActive(Boolean.FALSE);
    }
    return dto;
  }

  /**
   * Mocks service credit statuses.
   *
   * @return value list of service credit statuses.
   * @throws IOException if mast api call fail.
   */
  private List<ServiceCreditStatusHDto> mockServiceCreditStatuses() throws IOException {
    List<ServiceCreditStatusHDto> dtos = new ArrayList<>();
    ServiceCreditStatusHDto dto = new ServiceCreditStatusHDto();
    dto.setId(1L);
    dto.setCode("12324");
    dto.setName("CreditStatus");
    dtos.add(dto);
    return dtos;
  }

  /**
   * Mocks list of service catalogues.
   *
   * @return list of service catalogues.
   * @throws IOException if mast api call fails.
   */
  private List<ServiceCatalogueHDto> mockServiceCatalogues() throws IOException {
    List<ServiceCatalogueHDto> dtos = new ArrayList<>();
    ServiceCatalogueHDto dto = new ServiceCatalogueHDto();
    dto.setId(1L);
    dto.setCode("12324");
    dto.setName("Catalogue Active");
    dto.setDeleted(Boolean.FALSE);
    dto.setProductCatalogue(new ProductCatalogueDto());
    dto.getProductCatalogue().setProductGroup(new LinkResource());
    dto.getProductCatalogue().getProductGroup().setId(1L);
    dtos.add(dto);

    dto = new ServiceCatalogueHDto();
    dto.setId(1L);
    dto.setCode("12324");
    dto.setName("Catalogue Deleted");
    dto.setDeleted(Boolean.TRUE);
    dto.setProductCatalogue(new ProductCatalogueDto());
    dto.getProductCatalogue().setProductGroup(new LinkResource());
    dto.getProductCatalogue().getProductGroup().setId(1L);
    dtos.add(dto);

    return dtos;
  }

  /**
   * Mocks list service groups.
   *
   * @return the list of service groups.
   * @throws IOException if mast api call fails.
   */
  private List<ServiceGroupHDto> mockServiceGroups() throws IOException {
    List<ServiceGroupHDto> dtos = new ArrayList<>();
    ServiceGroupHDto dto = new ServiceGroupHDto();
    dto.setId(1L);
    dto.setName("Group");
    dtos.add(dto);
    return dtos;
  }

  /**
   * Mocks product group.
   *
   * @return the list of product grou.
   */
  private ProductGroupDto mockProductGroup() {
    ProductGroupDto dto = new ProductGroupDto();
    dto.setId(1L);
    dto.setName("Group1");
    dto.setDisplayOrder(1);

    return dto;
  }

  /**
   * Spring in-class Configuration.
   *
   * @author VMandalapu
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create object for ServiceReferenceServiceImpl.
     *
     * @return ServiceReferenceServiceImpl
     */
    @Override
    @Bean
    public ServiceReferenceService serviceReferenceService() {
      return new ServiceReferenceServiceImpl();
    }

    /**
     * @return serviceHelperImpl
     */
    @Override
    @Bean
    public ServiceHelper serviceHelper() {
      return new ServiceHelperImpl();
    }

  }

}
