package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.MajorNCNService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.MajorNCNExportService;
import com.baesystems.ai.lr.cd.service.export.impl.MajorNCNExportServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;


/**
 * Provides unit test for {@link MajorNCNExportService}.
 *
 * @author syalavarthi
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = MajorNCNExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class MajorNCNExportServiceTest {

  /**
   * The {@link MajorNCNService} from spring context.
   */
  @Autowired
  private MajorNCNService majorNCNService;

  /**
   * The {@link AmazonStorageService} from spring context.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link AssetService} from spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link MajorNCNExportService} from spring context.
   */
  @Autowired
  private MajorNCNExportService majorNCNExportService;

  /**
   * Provides mock reset before any test class is executed.
   */
  @Before
  public void resetMock() {
    Mockito.reset(majorNCNService);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Test positive case scenario for mncn pdf generation.
   *
   * @throws ClassDirectException if API call fail.
   */
  @Test
  public void testGenerateAnPDF() throws ClassDirectException {

    // mock asset data
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("My asset");
    asset.setLeadImo("1000019");
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("AB");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    asset.setIhsAssetDto(ihsAssetDetailsDto);
    asset.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    asset.getClassMaintenanceStatusDto().setName("Single");

    // mock mncn data
    List<MajorNCNHDto> mncns = new ArrayList<>();
    MajorNCNHDto mncn = new MajorNCNHDto();
    mncn.setId(1L);
    mncn.setMncnNumber("5931855A");
    mncn.setActionTaken(new LinkResource(1L));
    mncn.setAreaUnderReview("Corrective Actions");
    mncn.setDocCompanyCountry("United States of America");
    mncn.setDocCompanyImo("1234");
    mncn.setDocCompanyName("TEAM SHIP Maritime GmbH & Co KG");
    mncn.setDueDate(new GregorianCalendar(2017, 11, 11).getTime());
    mncn.setIsmClause("Establish procedures to maintain the ship, 10.1");
    mncn.setJob(new LinkResource(1L));
    mncn.setStatus(new LinkResource(1L));
    mncn.setClosureDate(new GregorianCalendar(2017, 12, 12).getTime());
    MajorNCNStatusHDto status2 = new MajorNCNStatusHDto();
    status2.setName("Closed");
    mncn.setStatusH(status2);
    mncn.setDescription("The Company's procedures for the implementation of corrective actions including measures"
        + "intended to prevent recurrence are not in all cases effective.");
    mncn.setObjectiveEvidence(
        "- Procedure manual chapter 6.3.4 (corrective / preventive actions of internal audits and "
            + "third party inspections) requires for deficiencies observed during internal / external audits,"
            + " PSC or FSC inspections a corrective action report (Form F58) to be drawn up. Although in sampled "
            + "cases Form 58 were found completed, it was found that actions taken were only limited to correcting "
            + "identified items. No root cause analysis, corrective- / preventive actions were carried out in sampled"
            + " cases. - Similar deficiencies that were found during previous PSC inspections "
            + "(e.g. USCG dated 17/10/2017 and Vina del Mar - Capuaba dated 20/08/2017) "
            + "are found recurring during this attendance. - During interim ISM  "
            + "audit on 07/07/2017 it was found that NOx files are not available on "
            + "board - situation found still existing during this attendance.");
    mncn.setCorrectiveAction(
        "- Technical deficiencies as stated in the PSC report are to be rectified to satisfaction of PSC "
            + "and Class Surveyor. Company to provide a root cause analysis and a corrective action plan "
            + " for all PSC deficiencies as well as any additional deficiencies identified. To carry out "
            + "a proper Internal Audit and present the report to auditors.");
    mncns.add(mncn);

    MajorNCNHDto mncn2 = new MajorNCNHDto();
    mncn2.setId(2L);
    mncn2.setMncnNumber("5315855A");
    mncn2.setActionTaken(new LinkResource(2L));
    mncn2.setAreaUnderReview("Planned Maintenance");
    mncn2.setDocCompanyCountry("Malaysia");
    mncn2.setDocCompanyImo("5678");
    mncn2.setDocCompanyName("TEAM SHIP Maritime GmbH & Co KG");
    mncn2.setDueDate(new GregorianCalendar(2017, 12, 12).getTime());
    mncn2.setIsmClause("Implementation of corrective actions, 9.2");
    mncn2.setJob(new LinkResource(2L));
    mncn2.setStatus(new LinkResource(1L));
    MajorNCNStatusHDto status1 = new MajorNCNStatusHDto();
    mncn2.setClosureDate(new GregorianCalendar(2017, 12, 12).getTime());
    status1.setName("Open");
    mncn2.setStatusH(status1);
    mncn2.setDescription(
        "The company's procedure to ensure its ships are maintained in conformity with the provisions of"
            + " relevant rules and regulations is not in all cases effective.");
    mncn2.setObjectiveEvidence(
        "As indicated in the PSC detention report, the vessel proceeded to its berth in Antwerp without "
            + " having all required charts and publications on board (e.g. chart 1874 of an expired edition"
            + ", charts for the section between Vlissingen and Antwerp not available on board, sailing direction"
            + " NP 28 not on board).");
    mncn2.setCorrectiveAction(
        "- Technical deficiencies as stated in the PSC report are to be rectified to satisfaction of PSC "
            + "and Class Surveyor. Company to provide a root cause analysis and a corrective action plan "
            + " for all PSC deficiencies as well as any additional deficiencies identified. To carry out "
            + "a proper Internal Audit and present the report to auditors.");
    mncns.add(mncn2);
    MajorNCNHDto mncn3 = new MajorNCNHDto();
    mncn3.setId(2L);
    mncn3.setMncnNumber("5955555A");
    mncn3.setActionTaken(new LinkResource(2L));
    mncn3.setAreaUnderReview("Voyage planning and execution");
    mncn3.setDocCompanyCountry("Singapore");
    mncn3.setDocCompanyImo("5931855");
    mncn3.setDocCompanyName("TEAM SHIP Maritime GmbH & Co KG");
    mncn3.setDueDate(new GregorianCalendar(2017, 10, 10).getTime());
    mncn3.setIsmClause("Establishing procedures in support of the SMS, 7");
    mncn3.setJob(new LinkResource(2L));
    mncn3.setStatus(new LinkResource(2L));
    MajorNCNStatusHDto status = new MajorNCNStatusHDto();
    status.setName("Open - Downgraded");
    mncn3.setStatusH(status);
    mncn3.setClosureDate(new GregorianCalendar(2017, 12, 12).getTime());
    mncn3.setDescription(
        "The Company's procedures for voyage planning and execution are not in all cases effectively implemented"
            + " in the shipboard routine.");
    mncn3.setObjectiveEvidence(
        "- The vessel is detained in the port of Antwerp by PSC. PSC report dated 08/11/2017 contains various "
            + "objective evidence that the ship is not maintained in accordance with applicable rules and regulations"
            + " (see audit report narrative for additional information).  - Although a planned maintenance system "
            + "(MESPAS) is found now to be installed, this system was not installed until approximately two weeks "
            + "ago (note that the dispensation letter relating to the planned maintenance system from Flag "
            + "(nr. 745.17) was only valid until 05/08/2017). The company's interim solution for carrying "
            + " out maintenance did not ensure that all required inspections, tests etc. were carried out "
            + "as required.");
    mncn3.setCorrectiveAction(
        "- Technical deficiencies as stated in the PSC report are to be rectified to satisfaction of PSC and "
            + " Class Surveyor. Company to provide a root cause analysis and a corrective action plan for "
            + "all PSC deficiencies as well as any additional deficiencies identified. To carry out a proper "
            + " Internal Audit and present the report to auditors.");
    mncn3.setClosureDate(new GregorianCalendar(2017, 10, 10).getTime());
    mncn3.setClosureReason("Evidence provided that the voyage planning issue has been addressed and confirmed by "
        + " LR surveyor and DOC company");
    mncns.add(mncn3);

    final MajorNCNPageResource mncnsResource = new MajorNCNPageResource();
    mncnsResource.setContent(mncns);

    Mockito.when(
        majorNCNService.getMajorNCNs(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(mncnsResource);
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(asset);
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ExportPdfDto query = new ExportPdfDto();
    query.setCode("LRV1");
    Set<Long> itemsToExport = new HashSet<Long>();
    itemsToExport.add(1L);
    itemsToExport.add(2L);
    query.setItemsToExport(itemsToExport);

    final StringResponse response = majorNCNExportService.downloadPdf(query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   *
   * Provides Spring in-class configurations.
   *
   * @author syalavarthi.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public MajorNCNExportService majorNCNExportService() {
      return new MajorNCNExportServiceImpl();
    }
  }

}
