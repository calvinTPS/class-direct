package com.baesystems.ai.lr.cd.service.test;


import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.asset.reference.impl.AssetReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.cache.CacheContainer;
import com.baesystems.ai.lr.cd.service.test.bean.Asset;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import retrofit2.Call;
import retrofit2.mock.Calls;


import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;

/**
 * Unit test for Resources injector.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ResourceTest.Config.class)
public class ResourceTest {
  /**
   * Constant asset type id.
   */
  private static final Long ASSET_TYPE_ID = 1234L;

  /**
   * Constant asset category id.
   */
  private static final Long ASSET_CATEGORY_ID = 123L;

  /**
   * Constant business process id.
   */
  private static final Long BUSINESS_PROCESS_ID = 12345L;

  /**
   * Injected asset reference service.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * Injected retrofit service.
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceRetrofitService;

  /**
   * Initialize mocked object.
   *
   * @throws Exception when error.
   */
  @Before
  public void init() throws Exception {
    doReturn(mockAssetTypes()).when(assetReferenceRetrofitService).getAssetTypeDtos();
    doReturn(mockAssetCategories()).when(assetReferenceRetrofitService).getAssetCategories();
  }

  /**
   * Reset mockito objects.
   *
   * @throws Exception when error.
   */
  @After
  public void restart() throws Exception {
    reset(assetReferenceRetrofitService);
    reset(assetReferenceService);
  }

  /**
   * Test for injecting resource.
   *
   * @throws Exception when error.
   */
  @Test
  public void successfullyInjectResource() throws Exception {
    AssetTypeHDto assetType = assetReferenceRetrofitService.getAssetTypeDtos().execute().body().get(0);
    Assert.assertNotNull(assetType);
    Assert.assertNull(assetType.getCategoryDto());

    Resources.inject(assetType);
    Assert.assertNotNull(assetType.getCategoryDto());
  }

  /**
   * Service lookup failed test.
   *
   * @throws Exception thrown
   */
  @Test
  public void serviceLookupFailed() throws Exception {
    Asset asset = new Asset();
    asset.setRegisteredPort(new LinkResource(ASSET_CATEGORY_ID));

    Resources.inject(asset);
    Assert.assertNull(asset.getRegisteredPortDto());
  }

  /**
   * Field injection failed.
   */
  @Test
  public void fieldInjectionNotFound() {
    TestDto dto = new TestDto();
    Resources.inject(dto);
    Assert.assertNull(dto.getDto());
  }

  /**
   * Mock asset type list.
   *
   * @return caller.
   * @throws Exception when error.
   */
  private Call<List<AssetTypeHDto>> mockAssetTypes() throws Exception {
    List<AssetTypeHDto> assetTypes = new ArrayList<>();
    AssetTypeHDto assetType = new AssetTypeHDto();
    assetType.setId(ASSET_TYPE_ID);
    assetType.setCategory(new LinkResource(ASSET_CATEGORY_ID));
    assetTypes.add(assetType);

    return Calls.response(assetTypes);
  }

  /**
   * Mock asset category list.
   *
   * @return list of asset categories.
   * @throws Exception when error.
   */
  private Call<List<AssetCategoryHDto>> mockAssetCategories() throws Exception {
    List<AssetCategoryHDto> assetCategories = new ArrayList<>();
    AssetCategoryHDto category = new AssetCategoryHDto();
    category.setId(ASSET_CATEGORY_ID);
    category.setBusinessProcess(new LinkResource(BUSINESS_PROCESS_ID));
    category.setName("ABC");
    assetCategories.add(category);

    return Calls.response(assetCategories);
  }

  /**
   * Test dao.
   */
  private static class TestDto extends AssetTypeDto {

    /**
     * Test injection.
     */
    @LinkedResource(referencedField = "test")
    private ClassStatusHDto dto;

    /**
     * Linked resource to AssetCategoryDto.
     */
    @LinkedResource(referencedField = "category")
    private AssetCategoryHDto categoryDto;

    /**
     * Getter for {@link #dto}.
     *
     * @return value of dto.
     */
    public final ClassStatusHDto getDto() {
      return dto;
    }

    /**
     * Setter for {@link #dto}.
     *
     * @param classStatusHDto value to be set.
     */
    public final void setDto(final ClassStatusHDto classStatusHDto) {
      this.dto = classStatusHDto;
    }

    /**
     * Getter for {@link #categoryDto}.
     *
     * @return value of categoryDto.
     */
    public final AssetCategoryHDto getCategoryDto() {
      return categoryDto;
    }

    /**
     * Setter for {@link #categoryDto}.
     *
     * @param assetCategoryHDto value to be set.
     */
    public final void setCategoryDto(final AssetCategoryHDto assetCategoryHDto) {
      this.categoryDto = assetCategoryHDto;
    }
  }


  /**
   * Spring in-class configurations.
   *
   * @author msidek
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create new spied asset reference service.
     *
     * @return asset reference service.
     */
    @Bean
    public AssetReferenceService assetReferenceService() {
      return spy(new AssetReferenceServiceImpl());
    }

    /**
     * Initialize {@link Resources} as Spring bean.
     *
     * @return resources.
     */
    @Bean
    public Resources resources() {
      return new Resources();
    }

    /**
     * Return cache.
     *
     * @return cache object.
     */
    @Bean
    public CacheContainer cache() {
      return new CacheContainer();
    }
  }
}
