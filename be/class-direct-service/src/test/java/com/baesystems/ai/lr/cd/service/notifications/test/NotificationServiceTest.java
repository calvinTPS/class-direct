package com.baesystems.ai.lr.cd.service.notifications.test;

import static com.baesystems.ai.lr.cd.be.enums.DueStatus.DUE_SOON;
import static com.baesystems.ai.lr.cd.be.enums.DueStatus.IMMINENT;
import static com.baesystems.ai.lr.cd.be.enums.DueStatus.NOT_DUE;
import static com.baesystems.ai.lr.cd.be.enums.DueStatus.OVER_DUE;
import static com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum.DUE_STATUS;
import static com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum.JOB_REPORTS;
import static com.baesystems.ai.lr.enums.EmployeeRole.LEAD_SURVEYOR;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobWithFlagsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.NotificationsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.NotificationTypes;
import com.baesystems.ai.lr.cd.be.domain.repositories.Notifications;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.NotificationTypesEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.AssetEmblemUtils;
import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import com.baesystems.ai.lr.cd.mail.MailService;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.JobService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.favourites.FavouritesService;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.notifications.NotificationsService;
import com.baesystems.ai.lr.cd.service.notifications.impl.NotificationsServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for notification service {@link NotificationService}.
 *
 * @author Faizal Sidek
 * @author syalavarthi 07-06-2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@PrepareForTest({ExecutorUtils.class})
@ContextConfiguration
public class NotificationServiceTest {

  /**
   * notification successful update message.
   */
  @Value("${notification.successful.create}")
  private String notificationSuccessfulCreate;

  /**
   * notification message.
   */
  @Value("${notification.successful.delete}")
  private String notificationSuccessfulDelete;

  /**
   * The {@link NotificationsService} from spring context.
   */
  @Autowired
  private NotificationsService notificationsService;

  /**
   * The {@link AssetRetrofitService} from spring context.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * The {@link AssetService} from spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link ActionableItemService} from spring context.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   * The {@link CoCService} from spring context.
   */
  @Autowired
  private CoCService coCService;

  /**
   * The {@link StatutoryFindingService} from spring context.
   */
  @Autowired
  private StatutoryFindingService statutoryFindingService;

  /**
   * The {@link ServiceReferenceService} from spring context.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * The {@link NotificationsDao} from spring context.
   */
  @Autowired
  private NotificationsDao notificationsDao;

  /**
   * The {@link MailService} from spring context.
   */
  @Autowired
  private MailService mailService;

  /**
   * The {@link JobService} from spring context.
   */
  @Autowired
  private JobService jobService;

  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileDao;
  /**
   * The {@link JobReferenceService} from spring context.
   */
  @Autowired
  private JobReferenceService jobReferenceService;
  /**
   * The {@link FavouritesService} from spring context.
   */
  @Autowired
  private FavouritesService favouritesService;

  /**
   * Resets mail service.
   *
   * @throws Exception when error in reset mail service.
   */
  @After
  public final void cleanUp() throws Exception {
    reset(mailService);
  }

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public final void init() {
    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success scenario to get subscribed users
   * {@link NotificationsService#getSubscribedUsers(List)}.
   *
   * @throws ClassDirectException if error in getting subscribed users from database.
   */
  @Test
  public final void shouldReturnSubscribedUsers() throws ClassDirectException {
    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    Assert.assertNotNull(notificationsService.getSubscribedUsers(Collections.singletonList(DUE_STATUS)));
  }

  /**
   * Tests success scenario to send all notifications (Due status
   * {@link NotificationsService#sendDueStatusNotification()}, asset listing
   * {@link NotificationsService#sendAssetListingNotification()} and job
   * {@link NotificationsService#sendJobNotifications()}).
   *
   * @throws ClassDirectException if error in any of notification fail in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void sendSendAllNotifications() throws ClassDirectException, IOException {

    mockNotificationDao(Collections.singletonMap("102", DUE_STATUS));

    mockActionableItems(5);
    doReturn(IMMINENT).when(actionableItemService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(actionableItemService).calculateDueStatus(anyObject(), any());

    mockCoc(5);
    doReturn(IMMINENT).when(coCService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(coCService).calculateDueStatus(anyObject(), any());

    mockScheduleService(5);
    doReturn(IMMINENT).when(serviceReferenceService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(serviceReferenceService).calculateDueStatus(anyObject(), any());

    mockUserProfiles(Collections.singletonList("102"));
    mockNotificationDao(Collections.singletonMap("102", NotificationTypesEnum.ASSET_LISTING));
    mockUserProfiles(Collections.singletonList("102"));
    mockNotificationDao(Collections.singletonMap("102", JOB_REPORTS));
    mockAssetsUser2(1, false);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("102"));
    mockActionableItems(5);
    mockSF(5);
    doReturn(OVER_DUE).when(statutoryFindingService).calculateDueStatus(anyObject());
    doReturn(IMMINENT).when(statutoryFindingService).calculateDueStatus(anyObject(), any());

    List<String> assetIds = new ArrayList<>();
    assetIds.add("1");
    doReturn(assetIds).when(favouritesService).getFavouritesForUser(anyString());

    notificationsService.sendAssetListingNotification();
    notificationsService.sendJobNotifications();
    mockAssetsUser2DueNotification(1, false);
    notificationsService.sendDueStatusNotification();

    verify(mailService, times(3)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send Due status notification
   * {@link NotificationsService#sendDueStatusNotification()} for all types of codicils.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForAllCodicils() throws ClassDirectException, IOException {

    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssetsForDueNotifications(1, false);

    mockActionableItems(5);
    doReturn(IMMINENT).when(actionableItemService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(actionableItemService).calculateDueStatus(anyObject(), any());

    mockCoc(5);
    doReturn(IMMINENT).when(coCService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(coCService).calculateDueStatus(anyObject(), any());

    mockScheduleService(5);
    doReturn(IMMINENT).when(serviceReferenceService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(serviceReferenceService).calculateDueStatus(anyObject(), any());

    mockSF(5);
    doReturn(OVER_DUE).when(statutoryFindingService).calculateDueStatus(anyObject());
    doReturn(IMMINENT).when(statutoryFindingService).calculateDueStatus(anyObject(), any());

    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendDueStatusNotification();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send Due status notification
   * {@link NotificationsService#sendDueStatusNotification()} fail to get coc, actionable items,
   * services and sfs.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationFail() throws ClassDirectException, IOException {

    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssets(1, false);

    Mockito.when(actionableItemService.getActionableItemsByQuery(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenThrow(new ClassDirectException("mock"));
    Mockito.when(coCService.getCocsByQuery(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenThrow(new ClassDirectException("mock"));
    Mockito.when(serviceReferenceService.getServices(anyLong(), Mockito.any(ServiceQueryDto.class)))
        .thenThrow(new IOException("mock"));
    Mockito.when(statutoryFindingService.getStatutoriesForDueNotification(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenThrow(new ClassDirectException("mock"));

    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendDueStatusNotification();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send Due status notification
   * {@link NotificationsService#sendDueStatusNotification()} failed to send email.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void sendDueStatusNotificationFail() throws ClassDirectException, IOException {

    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssetsForDueNotifications(1, true);

    mockActionableItems(5);
    doReturn(IMMINENT).when(actionableItemService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(actionableItemService).calculateDueStatus(anyObject(), any());

    mockCoc(5);
    doReturn(IMMINENT).when(coCService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(coCService).calculateDueStatus(anyObject(), any());

    mockScheduleService(5);
    doReturn(IMMINENT).when(serviceReferenceService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(serviceReferenceService).calculateDueStatus(anyObject(), any());

    mockSF(5);
    doReturn(OVER_DUE).when(statutoryFindingService).calculateDueStatus(anyObject());
    doReturn(IMMINENT).when(statutoryFindingService).calculateDueStatus(anyObject(), any());

    Mockito.when(mailService.sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class))).thenThrow(new IOException());

    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendDueStatusNotification();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send Due status notification
   * {@link NotificationsService#sendDueStatusNotification()} for all types of codicils with empty
   * asset header list.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForAllCodicilsWithEmptyAssetHeaderList()
      throws ClassDirectException, IOException {

    mockNotificationDao(Collections.singletonMap("101", NotificationTypesEnum.DUE_STATUS));
    mockAssets(1, true);

    mockActionableItems(5);
    doReturn(DUE_SOON).when(actionableItemService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(actionableItemService).calculateDueStatus(anyObject(), any());

    mockCoc(5);
    doReturn(DUE_SOON).when(coCService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(coCService).calculateDueStatus(anyObject(), any());

    mockScheduleService(5);
    doReturn(DUE_SOON).when(serviceReferenceService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(serviceReferenceService).calculateDueStatus(anyObject(), any());

    mockSF(5);
    doReturn(OVER_DUE).when(statutoryFindingService).calculateDueStatus(anyObject());
    doReturn(OVER_DUE).when(statutoryFindingService).calculateDueStatus(anyObject(), any());

    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendDueStatusNotification();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send asset listing notification
   * {@link NotificationsService#sendAssetListingNotification} with out header.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendAssetListingNotificationsUserprofileEmailEmpty()
      throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", NotificationTypesEnum.ASSET_LISTING));
    mockUserProfilesWithoutEmail(Collections.singletonList("101"));

    notificationsService.sendAssetListingNotification();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send asset listing notification
   * {@link NotificationsService#sendAssetListingNotification} with out favourite assets when user
   * subscribed to favourites only.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldNotSendAssetListingNotifications() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", NotificationTypesEnum.ASSET_LISTING));
    mockUserProfiles(Collections.singletonList("101"));
    List<String> assetIds = new ArrayList<>();
    doReturn(assetIds).when(favouritesService).getFavouritesForUser(anyString());
    notificationsService.sendAssetListingNotification();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send asset listing notification
   * {@link NotificationsService#sendAssetListingNotification} when user favourite assets api fail.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void sendAssetListingNotificationsFail() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", NotificationTypesEnum.ASSET_LISTING));
    mockUserProfiles(Collections.singletonList("101"));
    Mockito.when(favouritesService.getFavouritesForUser(anyString())).thenThrow(new RecordNotFoundException("Mock"));
    notificationsService.sendAssetListingNotification();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send asset listing notifications
   * {@link NotificationsService#sendAssetListingNotification()} fail to send notification when
   * subscribed user fail to get user email.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldFailToSendNotifications() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", NotificationTypesEnum.ASSET_LISTING));
    mockUserProfilesWithoutEmail(Collections.singletonList("101"));

    notificationsService.sendAssetListingNotification();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send due status notification
   * {@link NotificationsService#sendDueStatusNotification()} with empty asset.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForAllCodicilsWithEmptyAsset()
      throws ClassDirectException, IOException {

    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));

    notificationsService.sendDueStatusNotification();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send due status notification
   * {@link NotificationsService#sendDueStatusNotification()}.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForAllJobWithEmptyAsset() throws ClassDirectException, IOException {

    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssets(1, false);
    mockJobItems(0);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));
    notificationsService.sendJobNotifications();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send job notification
   * {@link NotificationsService#sendJobNotifications()} with out favourites.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendJobReportNotification() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssets(1, false);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notification
   * {@link NotificationsService#sendJobNotifications()} with out favourites fail to send email.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void sendJobReportNotificationFailEmail() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssets(1, false);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));
    Mockito.when(mailService.sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class))).thenThrow(new IOException());

    notificationsService.sendJobNotifications();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send job notification
   * {@link NotificationsService#sendJobNotifications()} with out favourites fail to get employees.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void sendJobsNotificationFailToGetEmployee() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssets(1, false);
    mockJobItems(5);
    mockEmployee();
    Mockito.when(assetServiceDelegate.getEmployeeById(11L)).thenReturn(Calls.failure(new IOException()));
    Mockito.when(jobReferenceService.getJobStatus(Mockito.any())).thenReturn(mockJobStatus());
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send job notification
   * {@link NotificationsService#sendJobNotifications()} with favourites only.
   *
   * @throws ClassDirectException if error in mast api fail or error in execution.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendJobReportNotificationWithFavourite() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssets(1, true);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));
    Mockito.when(jobReferenceService.getJobStatus(Mockito.any())).thenReturn(mockJobStatus());

    notificationsService.sendJobNotifications();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} with favourite assets only.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendJobReportNotificationForAssetFromSubfleet() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssets(1, false);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));
    Mockito.when(jobReferenceService.getJobStatus(Mockito.any())).thenReturn(mockJobStatus());
    doReturn(true).when(mailService).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));

    notificationsService.sendJobNotifications();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} fail to get actionable items.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void failToSendJobReportNotificationWithoutAssetTypeAndFlagState()
      throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssets(1, false);
    mockJobItemsThrowException(1L, "101");
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} with out asset type.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void failToSendJobReportNotificationWithoutAssetType() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssetsWithoutAssetType(1);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} fail to get jobs.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void sendJobReportNotificationFail() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssetsWithoutAssetType(1);
    Mockito.when(jobService.getCompletedJobs(anyString(), anyObject(), anyObject()))
        .thenThrow(new ClassDirectException("fail"));
    Mockito.when(jobService.filterJobsBasedOnUserRole(anyString(), anyObject(), anyObject()))
        .thenThrow(new ClassDirectException("fail"));
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} with out asset type and class statuses.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void failToSendJobReportNotificationWithoutAssetTypeAndFlagStateAndClassStatus()
      throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssetsWithoutAssetTypeAndFlagStateAndClassStatus(1);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} with out flag state.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void failToSendJobReportNotificationWithoutFlagState() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssetsWithoutFlagState(1);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} with out class statuses.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void failToSendJobReportNotificationWithoutClassStatus() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssetsWithoutClassStatus(1);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendJobNotifications();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send job notifications
   * {@link NotificationsService#sendJobNotifications()} fails to sned emial.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void failToSendJobReportNotification() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", JOB_REPORTS));
    mockAssetsWithoutClassStatus(1);
    mockJobItems(5);
    mockEmployee();
    mockUserProfiles(Collections.singletonList("101"));
    Mockito.when(mailService.sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class))).thenThrow(new IOException());

    notificationsService.sendJobNotifications();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to due status notifications
   * {@link NotificationsService#sendDueStatusNotification()} actionable items only.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForAIOnly() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssetsForDueNotifications(1, false);
    mockActionableItems(5);
    doReturn(OVER_DUE).when(actionableItemService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(actionableItemService).calculateDueStatus(anyObject(), any());

    mockCoc(0);
    mockScheduleService(0);
    mockSF(0);
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendDueStatusNotification();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to due status notifications
   * {@link NotificationsService#sendDueStatusNotification()} statutory findings only with out
   * favourites flag.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForSFOnly() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssetsForDueNotifications(1, false);
    mockActionableItems(0);

    mockCoc(0);
    mockScheduleService(0);
    mockSF(1);
    doReturn(OVER_DUE).when(statutoryFindingService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(statutoryFindingService).calculateDueStatus(anyObject(), any());
    mockUserProfiles(Collections.singletonList("101"));

    notificationsService.sendDueStatusNotification();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send asset listing notifications
   * {@link NotificationsService#sendAssetListingNotification()} fail to send notification when
   * subscribed user fail to get user details with statutory findings only along with favourites.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForSF() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssets(1, true);
    mockActionableItems(0);

    mockCoc(0);
    mockScheduleService(0);
    mockSF(1);
    doReturn(OVER_DUE).when(statutoryFindingService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(statutoryFindingService).calculateDueStatus(anyObject(), any());
    mockUserProfilesWithoutEmail(Collections.singletonList("101"));

    notificationsService.sendDueStatusNotification();

    verify(mailService, times(0)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send due status notifications
   * {@link NotificationsService#sendDueStatusNotification()} with coc only.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForCocOnly() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssetsForDueNotifications(1, false);

    mockActionableItems(0);
    mockCoc(5);
    mockSF(0);
    doReturn(DUE_SOON).when(coCService).calculateDueStatus(anyObject());
    doReturn(NOT_DUE).when(coCService).calculateDueStatus(anyObject(), any());

    mockScheduleService(0);
    mockUserProfiles(Collections.singletonList("101"));
    doReturn(true).when(mailService).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));

    notificationsService.sendDueStatusNotification();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send due notification for Schedule Services only
   * {@link NotificationsService#sendDueStatusNotification()} with favourites.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendDueStatusNotificationForServiceOnly() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", DUE_STATUS));
    mockAssetsForDueNotifications(1, true);
    mockActionableItems(0);
    mockCoc(0);

    mockScheduleService(5);
    doReturn(IMMINENT).when(serviceReferenceService).calculateDueStatus(anyObject());
    doReturn(DUE_SOON).when(serviceReferenceService).calculateDueStatus(anyObject(), any());
    doReturn(true).when(mailService).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));

    mockUserProfiles(Collections.singletonList("101"));
    notificationsService.sendDueStatusNotification();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests success scenario to send asset listing notifications
   * {@link NotificationsService#sendAssetListingNotification()}.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldSendAssetListingNotifications() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", NotificationTypesEnum.ASSET_LISTING));
    List<String> assetIds = new ArrayList<>();
    assetIds.add("1");
    doReturn(assetIds).when(favouritesService).getFavouritesForUser(anyString());
    mockUserProfiles(Collections.singletonList("101"));
    doReturn(true).when(mailService).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));

    notificationsService.sendAssetListingNotification();

    verify(mailService, times(1)).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Tests failure scenario to send asset listing notifications
   * {@link NotificationsService#sendAssetListingNotification()} fail to send notification when
   * subscribed user fail to get user details.
   *
   * @throws ClassDirectException if error in getting user or any error in execution flow.
   * @throws IOException when error in getting services for asset.
   */
  @Test
  public final void shouldNotSendAssetListingNotificationIfError() throws ClassDirectException, IOException {
    mockNotificationDao(Collections.singletonMap("101", NotificationTypesEnum.ASSET_LISTING));
    mockUserProfilesThrowException("101");

    notificationsService.sendAssetListingNotification();

    verify(mailService, never()).sendMailWithTemplate(anyString(), eq(null), anyString(), anyString(),
        anyMapOf(String.class, Object.class));
  }

  /**
   * Mocks NotificationTypes.
   */
  @Test
  public final void getNotificationTypesTest() {
    final List<NotificationTypes> types = new ArrayList<>();
    final NotificationTypes type1 = new NotificationTypes();
    type1.setId(1L);
    type1.setName("Asset Listing");
    final NotificationTypes type2 = new NotificationTypes();
    type2.setId(2L);
    type2.setName("Due Status");
    final NotificationTypes type3 = new NotificationTypes();
    type3.setId(3L);
    type3.setName("Job Reports");
    types.add(type1);
    types.add(type2);
    types.add(type3);

    doReturn(types).when(notificationsDao).getNotificationTypes();
    final List<NotificationTypes> notificationTypes = notificationsService.getNotificationTypes();
    Assert.assertFalse(notificationTypes.isEmpty());
  }

  /**
   * Tests success scenario to create notification
   * {@link NotificationsService#createNotification(String, Long)}.
   *
   * @throws RecordNotFoundException if the given user id is not valid.
   * @throws ClassDirectException if the existing record fail to delete.
   */
  @Test
  public final void createNotificationTestSuccess() throws RecordNotFoundException, ClassDirectException {
    final StatusDto status = new StatusDto(notificationSuccessfulCreate);
    doReturn(status).when(notificationsDao).createNotification("109", 1L, true);
    final StatusDto statusExpec = notificationsService.createNotification("109", 1L, true);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Tests failure scenario to create notification
   * {@link NotificationsService#createNotification(String, Long)} inner call fail.
   *
   * @throws RecordNotFoundException if the given user id is not valid.
   * @throws ClassDirectException if the existing record fail to delete.
   */
  @Test
  public final void createNotificationTest() throws RecordNotFoundException, ClassDirectException {
    Mockito.when(notificationsDao.createNotification(Mockito.anyString(), Mockito.anyLong(), Mockito.anyBoolean()))
        .thenThrow(new DataIntegrityViolationException(" "));
    final StatusDto statusExpec = notificationsService.createNotification("109", 1L, true);
    Assert.assertNull(statusExpec);
  }

  /**
   * Tests failure scenario to create notification
   * {@link NotificationsService#createNotification(String, Long)} when there is no user Id.
   *
   * @throws RecordNotFoundException if the given user id is not valid.
   * @throws ClassDirectException if the existing record fail to delete.
   */
  @Test
  public final void createNotificationFail() throws RecordNotFoundException, ClassDirectException {
    final StatusDto status = new StatusDto(notificationSuccessfulCreate);
    doReturn(status).when(notificationsDao).createNotification("", 1L, true);
    StatusDto result = notificationsService.createNotification("", 1L, true);
    Assert.assertNull(result);
  }

  /**
   * Tests failure scenario to create notification
   * {@link NotificationsService#createNotification(String, Long)} when there is no type id.
   *
   * @throws RecordNotFoundException if the given user id is not valid.
   * @throws ClassDirectException if the existing record fail to delete.
   */
  @Test
  public final void createNotificationFailWithOutTypeId() throws RecordNotFoundException, ClassDirectException {
    final StatusDto status = new StatusDto(notificationSuccessfulCreate);
    doReturn(status).when(notificationsDao).createNotification("", 0L, true);
    StatusDto result = notificationsService.createNotification("", 0L, true);
    Assert.assertNull(result);
  }

  /**
   * Tests success scenario to create notification
   * {@link NotificationsService#createNotification(String, Long)} with change for favourites only
   * flag.
   *
   * @throws RecordNotFoundException if the given user id is not valid.
   * @throws ClassDirectException if the existing record fail to delete.
   */
  @Test
  public final void testCreateNotificationWithExistingType() throws RecordNotFoundException, ClassDirectException {
    final StatusDto status = new StatusDto(notificationSuccessfulCreate);
    doReturn(new Notifications()).when(notificationsDao).verifyNotification("109", 1L);
    doReturn(status).when(notificationsDao).deleteNotification("109", 1L);
    doReturn(status).when(notificationsDao).createNotification("109", 1L, true);
    final StatusDto statusExpec = notificationsService.createNotification("109", 1L, true);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Tests success scenario to delete notification
   * {@link NotificationsService#deleteNotification(String, Long)}.
   *
   * @throws ClassDirectException if error in deleting notification from database.
   */
  @Test
  public final void deleteNotificationTestSuccess() throws ClassDirectException {
    final StatusDto status = new StatusDto(notificationSuccessfulDelete);
    doReturn(status).when(notificationsDao).deleteNotification("109", 1L);
    final StatusDto statusExpec = notificationsService.deleteNotification("109", 1L);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Tests failure scenario to delete notification
   * {@link NotificationsService#deleteNotification(String, Long)} inner call fail.
   *
   * @throws ClassDirectException if error in deleting notification from database.
   */
  @Test(expected = ClassDirectException.class)
  public final void deleteNotificationTest() throws ClassDirectException {
    Mockito.when(notificationsDao.deleteNotification(Mockito.anyString(), Mockito.anyLong()))
        .thenThrow(new IllegalArgumentException(" "));
    final StatusDto statusExpec = notificationsService.deleteNotification("109", 1L);
    Assert.assertNull(statusExpec);
  }

  /**
   * Mock user profiles.
   *
   * @param userIds the list of user ids.
   * @throws ClassDirectException if any error in getting user.
   */
  private void mockUserProfiles(final List<String> userIds) throws ClassDirectException {
    // Mock role for user profile.
    final Roles role = new Roles();
    role.setRoleId(Role.CLIENT.getId());
    role.setRoleName(Role.CLIENT.toString());

    final Set<Roles> roles = new HashSet<>();
    roles.add(role);

    final Company company = new Company();
    company.setName("Ership Internacional SA");

    userIds.forEach(userId -> {
      final UserProfiles user = new UserProfiles();
      user.setUserId(userId);
      user.setClientCode("1000003");
      user.setEmail("test@gmail.com");
      user.setRoles(roles);
      user.setCompany(company);
      user.setCompanyName(company.getName());
      user.setName("");
      try {
        doReturn(user).when(userProfileDao).getUser(eq(userId));
      } catch (ClassDirectException classException) {
        throw new RuntimeException(classException);
      }
    });
  }

  /**
   * Mock user profiles.
   *
   * @param userIds the list of user ids.
   * @throws ClassDirectException if any error in getting user.
   */
  private void mockUserProfilesWithoutEmail(final List<String> userIds) throws ClassDirectException {
    userIds.forEach(userId -> {
      final UserProfiles user = new UserProfiles();
      try {
        doReturn(user).when(userProfileDao).getUser(eq(userId));
      } catch (final ClassDirectException exception) {
        throw new RuntimeException(exception);
      }
    });
  }

  /**
   * Mocks user profiles.
   *
   * @param userIds the list of user ids.
   * @throws ClassDirectException if any error in getting user.
   */
  private void mockUserProfilesWithoutCompany(final List<String> userIds) throws ClassDirectException {
    userIds.forEach(userId -> {
      final UserProfiles user = new UserProfiles();
      user.setEmail("test@gmail.com");
      try {
        doReturn(user).when(userProfileDao).getUser(eq(userId));
      } catch (final ClassDirectException exception) {
        throw new RuntimeException(exception);
      }
    });
  }

  /**
   * Mocks user to throw exception.
   *
   * @param userId the id of user.
   * @throws ClassDirectException if any error in getting user.
   */
  private void mockUserProfilesThrowException(final String userId) throws ClassDirectException {
    doThrow(new RecordNotFoundException("Mock exception.")).when(userProfileDao).getUser(eq(userId));
  }

  /**
   * Mocks subscribed users.
   *
   * @param userNotificationMap the map of user id and notification type.
   * @throws ClassDirectException if any error in getting subscribed users.
   */
  private void mockNotificationDao(final Map<String, NotificationTypesEnum> userNotificationMap)
      throws ClassDirectException {

    final List<Notifications> types = new ArrayList<>();
    userNotificationMap.forEach((userId, type) -> {
      final Notifications notification = new Notifications();
      notification.setTypeId(type.getId());
      notification.setUserId(userId);
      if (!userId.equals("101")) {
        notification.setFavouritesOnly(false);
      } else {
        notification.setFavouritesOnly(true);
      }

      types.add(notification);
    });

    doReturn(types).when(notificationsDao).getSubscribedUsers(anyListOf(NotificationTypesEnum.class));
  }


  /**
   * Mocks paginated assets.
   *
   * @param count the assets count.
   * @param onlyFavourites the flag to set assets.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssets(final int count, final boolean onlyFavourites) throws ClassDirectException, IOException {
    doReturn(getAssets(count, onlyFavourites)).when(assetService).getAssetsByPagination(eq("101"),
        any(AssetQueryHDto.class), any(MastQueryBuilder.class));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mocks paginated assets.
   *
   * @param count the assets count.
   * @param onlyFavourites the flag to set assets.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsForDueNotifications(final int count, final boolean onlyFavourites)
      throws ClassDirectException, IOException {
    doReturn(getAssets(count, onlyFavourites)).when(assetService).getAssetsByPagination(eq("101"),
        any(AssetQueryHDto.class), any(MastQueryBuilder.class));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mocks paginated assets for user2.
   *
   * @param count the assets count.
   * @param onlyFavourites the flag to set assets.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsUser2(final int count, final boolean onlyFavourites) throws ClassDirectException, IOException {
    doReturn(getAssets(count, onlyFavourites)).when(assetService).getAssetsByPagination(eq("102"),
        any(AssetQueryHDto.class), Mockito.eq(null));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mocks paginated assets for user2.
   *
   * @param count the assets count.
   * @param onlyFavourites the flag to set assets.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsUser2DueNotification(final int count, final boolean onlyFavourites)
      throws ClassDirectException, IOException {
    doReturn(getAssets(count, onlyFavourites)).when(assetService).getAssetsByPagination(eq("102"),
        any(AssetQueryHDto.class), any(MastQueryBuilder.class));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mocks list of assets.
   *
   * @param count the assets count.
   * @param onlyFavourites the flag to set assets.
   *
   * @return list of assets.
   */
  private List<AssetHDto> getAssets(final int count, final boolean onlyFavourites) {
    final List<AssetHDto> assets = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setName(generateRandomString(30));
      asset.setDueStatusH(OVER_DUE);
      asset.setLeadImo("1000019");
      asset.setId((long) i);
      final AssetTypeDto assetType = new AssetTypeDto();
      assetType.setName("General Cargo Barge, non propelled");
      asset.setAssetType(assetType);
      asset.setBuildDate(new Date());
      asset.setIsFavourite(onlyFavourites);

      final FlagStateHDto flag = new FlagStateHDto();
      flag.setName(generateRandomString(30));
      asset.setFlagStateDto(flag);

      final ClassStatusHDto classStatus = new ClassStatusHDto();
      classStatus.setName("Class Contemplated");
      asset.setClassStatusDto(classStatus);
      AssetTypeHDto assetTypeH = new AssetTypeHDto();
      assetTypeH.setId(1L);
      assetTypeH.setCode("A12");
      asset.setAssetTypeHDto(assetTypeH);
      assets.add(asset);
    }
    return assets;
  }

  /**
   * Mocks asset with subfllet ids..
   *
   * @param count the assets count.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsWithSubfleetIds(final int count) throws ClassDirectException, IOException {
    AssetPageResource assetPageResource = new AssetPageResource();
    final List<AssetHDto> assets = new ArrayList<>();
    assetPageResource.setContent(assets);
    for (int i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setName(generateRandomString(30));
      asset.setDueStatusH(OVER_DUE);
      asset.setLeadImo("1000019");
      asset.setId((long) i);

      final AssetTypeDto assetType = new AssetTypeDto();
      assetType.setName("General Cargo Barge, non propelled");
      asset.setAssetType(assetType);
      asset.setBuildDate(new Date());

      final FlagStateHDto flag = new FlagStateHDto();
      flag.setName(generateRandomString(30));
      asset.setFlagStateDto(flag);

      final ClassStatusHDto classStatus = new ClassStatusHDto();
      classStatus.setName("Class Contemplated");
      asset.setClassStatusDto(classStatus);
      assets.add(asset);
    }

    doReturn(assetPageResource).when(assetService).getAssets(eq("101"), any(AssetQueryHDto.class), eq(null), eq(null));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mocks asset with out asset type.
   *
   * @param count asset count.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsWithoutAssetType(final int count) throws ClassDirectException, IOException {
    AssetPageResource assetPageResource = new AssetPageResource();
    final List<AssetHDto> assets = new ArrayList<>();
    assetPageResource.setContent(assets);
    for (int i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setName(generateRandomString(30));
      asset.setDueStatusH(OVER_DUE);
      asset.setLeadImo("1000019");
      asset.setId((long) i);
      asset.setBuildDate(new Date());

      final FlagStateHDto flag = new FlagStateHDto();
      flag.setName(generateRandomString(30));
      asset.setFlagStateDto(flag);

      final ClassStatusHDto classStatus = new ClassStatusHDto();
      classStatus.setName("Class Contemplated");
      asset.setClassStatusDto(classStatus);
      assets.add(asset);
    }

    doReturn(assetPageResource).when(assetService).getAssets(eq("101"), any(AssetQueryHDto.class), eq(null), eq(null));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mocks asset with out flag state and asset type.
   *
   * @param count asset count.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsWithoutAssetTypeAndFlagState(final int count) throws ClassDirectException, IOException {
    AssetPageResource assetPageResource = new AssetPageResource();
    final List<AssetHDto> assets = new ArrayList<>();
    assetPageResource.setContent(assets);
    for (int i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setName(generateRandomString(30));
      asset.setDueStatusH(OVER_DUE);
      asset.setLeadImo("1000019");
      asset.setId((long) i);
      asset.setBuildDate(new Date());

      final ClassStatusHDto classStatus = new ClassStatusHDto();
      classStatus.setName("Class Contemplated");
      asset.setClassStatusDto(classStatus);
      assets.add(asset);
    }

    doReturn(assetPageResource).when(assetService).getAssets(eq("101"), any(AssetQueryHDto.class), eq(null), eq(null));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mock asset with out flag state, asset type and class statuses.
   *
   * @param count asset count.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsWithoutAssetTypeAndFlagStateAndClassStatus(final int count)
      throws ClassDirectException, IOException {
    AssetPageResource assetPageResource = new AssetPageResource();
    final List<AssetHDto> assets = new ArrayList<>();
    assetPageResource.setContent(assets);
    for (int i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setName(generateRandomString(30));
      asset.setDueStatusH(OVER_DUE);
      asset.setLeadImo("1000019");
      asset.setId((long) i);
      asset.setBuildDate(new Date());
      assets.add(asset);
    }

    doReturn(assetPageResource).when(assetService).getAssets(eq("101"), any(AssetQueryHDto.class), eq(null), eq(null));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mock asset with out flag state.
   *
   * @param count asset count.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsWithoutFlagState(final int count) throws ClassDirectException, IOException {
    final List<AssetHDto> assets = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setName(generateRandomString(30));
      asset.setDueStatusH(OVER_DUE);
      asset.setLeadImo("1000019");
      asset.setId((long) i);

      final AssetTypeDto assetType = new AssetTypeDto();
      assetType.setName("General Cargo Barge, non propelled");
      asset.setAssetType(assetType);
      asset.setBuildDate(new Date());

      final ClassStatusHDto classStatus = new ClassStatusHDto();
      classStatus.setName("Class Contemplated");
      asset.setClassStatusDto(classStatus);
      assets.add(asset);
    }

    doReturn(assets).when(assetService).getAssetsByPagination(eq("101"), any(AssetQueryHDto.class),
        any(MastQueryBuilder.class));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mock asset with out class statuses.
   *
   * @param count asset count.
   * @throws ClassDirectException if error in getting assets.
   * @throws IOException if api call fails.
   */
  private void mockAssetsWithoutClassStatus(final int count) throws ClassDirectException, IOException {
    AssetPageResource assetPageResource = new AssetPageResource();
    final List<AssetHDto> assets = new ArrayList<>();
    assetPageResource.setContent(assets);
    for (int i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setName(generateRandomString(30));
      asset.setDueStatusH(OVER_DUE);
      asset.setLeadImo("1000019");
      asset.setId((long) i);

      final AssetTypeDto assetType = new AssetTypeDto();
      assetType.setName("General Cargo Barge, non propelled");
      asset.setAssetType(assetType);
      asset.setBuildDate(new Date());

      final FlagStateHDto flag = new FlagStateHDto();
      flag.setName(generateRandomString(30));
      asset.setFlagStateDto(flag);

      assets.add(asset);
    }

    doReturn(assetPageResource).when(assetService).getAssets(eq("101"), any(AssetQueryHDto.class), eq(null), eq(null));
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());
  }

  /**
   * Mocks List of Actionable Items.
   *
   * @param count the total items.
   * @throws ClassDirectException when error in getting actionable items.
   */
  private void mockActionableItems(final int count) throws ClassDirectException {
    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final ActionableItemHDto actionableItem = new ActionableItemHDto();
      actionableItem.setTitle(generateRandomString(30));
      final CodicilCategoryHDto codicilHdto = new CodicilCategoryHDto();
      codicilHdto.setName(generateRandomString(30));
      actionableItem.setCategoryH(codicilHdto);
      actionableItem.setImposedDate(new Date());
      actionableItem.setDueDate(new Date());
      actionableItem.setDueStatusH(OVER_DUE);
      actionableItem.setId((long) i);
      actionableItems.add(actionableItem);
    }

    doReturn(actionableItems).when(actionableItemService).getActionableItemsByQuery(anyLong(),
        any(CodicilDefectQueryHDto.class));
  }

  /**
   * Mocks List of Job Items.
   *
   * @param count the total items.
   * @throws ClassDirectException if error in getting completed jobs or in execution flow.
   */
  private void mockJobItems(final int count) throws ClassDirectException {
    final LinkResource employeeRole = new LinkResource();
    employeeRole.setId(LEAD_SURVEYOR.getValue());
    final EmployeeLinkDto employee = new EmployeeLinkDto();
    employee.setEmployeeRole(employeeRole);
    employee.setLrEmployee(employeeRole);
    final List<EmployeeLinkDto> employees = new ArrayList<>();
    employees.add(employee);
    final List<JobWithFlagsHDto> jobs = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final JobWithFlagsHDto job = new JobWithFlagsHDto();
      job.setId((long) i);
      job.setEmployees(employees);
      job.setLastVisitDate(new Date());
      job.setLocationDto(new LocationHDto());
      job.setAsset(new LinkVersionedResource((long) i, (long) i));
      job.setJobStatus(new LinkResource(1L));
      jobs.add(job);
    }
    doReturn(jobs).when(jobService).getCompletedJobs(anyString(), anyObject(), anyObject());
    doReturn(jobs).when(jobService).filterJobsBasedOnUserRole(anyString(), anyObject(), anyObject());
    JobStatusHDto jobStatus = new JobStatusHDto();
    doReturn(jobStatus).when(jobReferenceService).getJobStatus(anyLong());
  }

  /**
   * Mocks List of Job Items fail to get completed jobs.
   *
   * @param assetId the the primary key of asset.
   * @param userId the user primary key.
   * @throws ClassDirectException if error in getting completed jobs or in execution flow.
   */
  private void mockJobItemsThrowException(final long assetId, final String userId) throws ClassDirectException {
    doThrow(new ClassDirectException("mock")).when(jobService).getCompletedJobs(userId, null, null);
  }

  /**
   * Mocks Employee object.
   *
   * @throws ClassDirectException fail to get employee by id.
   */
  private void mockEmployee() throws ClassDirectException {
    final LrEmployeeDto empLead = new LrEmployeeDto();
    empLead.setId(11L);
    empLead.setDeleted(false);
    empLead.setOneWorldNumber(null);
    empLead.setFirstName("idstestuser6");
    empLead.setLastName(" ");
    empLead.setName("idstestuser6");
    empLead.setEmailAddress("idstestuser6@lr.org");
    empLead.setInternalId("11");

    Mockito.when(assetServiceDelegate.getEmployeeById(11L)).thenReturn(Calls.response(empLead));
  }

  /**
   * Mock List of Conditions of Class.
   *
   * @param count the total number of CoC mock object.
   * @throws ClassDirectException when error in getting codicils.
   */
  private void mockCoc(final int count) throws ClassDirectException {
    List<CoCHDto> coCList = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final CoCHDto coc = new CoCHDto();
      coc.setTitle(generateRandomString(30));
      final CodicilCategoryHDto codicilHdto = new CodicilCategoryHDto();
      codicilHdto.setName(generateRandomString(30));
      coc.setCategoryH(codicilHdto);
      coc.setImposedDate(new Date());
      coc.setDueDate(new Date());
      coc.setDueStatusH(OVER_DUE);
      coc.setId((long) i);
      coCList.add(coc);
    }
    doReturn(coCList).when(coCService).getCocsByQuery(anyLong(), any(CodicilDefectQueryHDto.class));
  }

  /**
   * Mock List of Statutory Finding objects.
   *
   * @param count the total number of statutory finding mock object.
   * @throws ClassDirectException when error in getting statutories.
   */
  private void mockSF(final int count) throws ClassDirectException {
    final List<StatutoryFindingHDto> statutoryFindingHDtos = new ArrayList<>();

    for (int i = 0; i < count; i++) {
      final StatutoryFindingHDto statutoryFindingHDto = new StatutoryFindingHDto();
      statutoryFindingHDto.setTitle(generateRandomString(30));
      statutoryFindingHDto.setImposedDate(new Date());
      statutoryFindingHDto.setDueDate(new Date());
      statutoryFindingHDto.setDueStatusH(OVER_DUE);
      statutoryFindingHDto.setId((long) i);
      statutoryFindingHDto.setDefect(new LinkResource((long) i));
      statutoryFindingHDtos.add(statutoryFindingHDto);
    }
    doReturn(statutoryFindingHDtos).when(statutoryFindingService).getStatutoriesForDueNotification(anyLong(),
        any(CodicilDefectQueryHDto.class));
  }

  /**
   * Mock List of Scheduled Services.
   *
   * @param count the total number of scheduled service mock object.
   * @throws ClassDirectException error in execution flow.
   * @throws IOException mast api fails to get services for asset.
   */
  private void mockScheduleService(final int count) throws ClassDirectException, IOException {
    final List<ScheduledServiceHDto> scheduledServiceHDtos = new ArrayList<>();

    for (int i = 0; i < count; i++) {
      final ProductCatalogueDto productCatalogueDto = new ProductCatalogueDto();
      productCatalogueDto.setName(generateRandomString(30));
      final ServiceCatalogueHDto serviceCatalogueH = new ServiceCatalogueHDto();
      serviceCatalogueH.setName(generateRandomString(30));
      serviceCatalogueH.setProductCatalogue(productCatalogueDto);
      serviceCatalogueH.setCode("YZ");
      final ScheduledServiceHDto service = new ScheduledServiceHDto();
      service.setServiceCatalogueH(serviceCatalogueH);
      service.setDueDate(new Date());
      service.setLowerRangeDate(new Date());
      service.setUpperRangeDate(new Date());
      service.setPostponementDate(new Date());
      service.setDueStatusH(OVER_DUE);
      service.setId((long) i);
      service.setOccurrenceNumber(i);
      service.setAsset(new LinkResource((long) i));
      scheduledServiceHDtos.add(service);
    }

    doReturn(scheduledServiceHDtos).when(serviceReferenceService).getServices(anyLong(),
        Mockito.any(ServiceQueryDto.class));
  }

  /**
   * Generates random alphanumeric.
   *
   * @param length the length of string.
   * @return random string.
   */
  private String generateRandomString(final Integer length) {
    return new BigInteger(130, new SecureRandom()).toString(length);
  }

  /**
   * Mocks job status.
   *
   * @return job sttaus.
   */
  private JobStatusHDto mockJobStatus() {
    JobStatusHDto jobStatus = new JobStatusHDto();
    jobStatus.setId(1L);
    jobStatus.setDescription("Closed");
    return jobStatus;
  }


  /**
   * Inner config.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Configurer.
     *
     * @return properties configurer.
     */
    @Bean
    public PropertyPlaceholderConfigurer configurer() {
      final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
      configurer.setLocations(new ClassPathResource("errormessage-config.properties"),
          new ClassPathResource("lr-ad-config.properties"), new ClassPathResource("sitekit-config.properties"),
          new ClassPathResource("classdirect-config.properties"), new ClassPathResource("asset-services.properties"));

      return configurer;
    }

    /**
     * Override with real object.
     *
     * @return service.
     */
    @Override
    @Bean
    public NotificationsService notificationsService() {
      return new NotificationsServiceImpl();
    }

    /**
     * AssetEmblemUtils.
     *
     * @return asset emblem utils.
     */
    @Bean
    public AssetEmblemUtils assetEmblemUtils() {
      return Mockito.spy(new AssetEmblemUtils());
    }
  }
}
