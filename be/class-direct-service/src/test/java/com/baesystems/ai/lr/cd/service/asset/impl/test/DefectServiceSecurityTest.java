package com.baesystems.ai.lr.cd.service.asset.impl.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.service.asset.DefectService;
import com.baesystems.ai.lr.cd.service.asset.impl.DefectServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.paging.PageResource;


/**
 * Provides unit test to test Security Expressions (@PreAuthorize).
 *
 * @author sbollu
 * @author syalavarthi 24-05-2017.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestExecutionListeners(listeners = {ServletTestExecutionListener.class, DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class,
    WithSecurityContextTestExecutionListener.class})
public class DefectServiceSecurityTest {

  /**
   * The {@link DefectService} from spring context.
   *
   */
  @Autowired
  private DefectService defectService;

  /**
   * Tests success scenario get paginated defects
   * {@link DefectService#getDefectsByQuery(Integer, Integer, Long, CodicilDefectQueryHDto)} api
   * will not call for equisis thesis roles.
   *
   * @throws Exception if user no access rights to the api.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void shouldReturnExpForEquasis() throws Exception {
    final PageResource<DefectHDto> result =
        defectService.getDefectsByQuery(null, null, 1L, new CodicilDefectQueryHDto());
  }

  /**
   * @author sbollu
   *
   */
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  public static class Config extends BaseMockConfiguration {


    @Override
    @Bean
    public DefectService defectService() {
      return new DefectServiceImpl();
    }

    /**
     * @param auth auth.
     * @throws Exception if error in authentication.
     */
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
      auth.inMemoryAuthentication().withUser(" ").password(" ").authorities("EQUASIS", "THESIS");
    }

  }
}
