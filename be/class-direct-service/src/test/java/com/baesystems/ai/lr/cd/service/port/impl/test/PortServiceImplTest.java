package com.baesystems.ai.lr.cd.service.port.impl.test;

import static org.mockito.Matchers.anyLong;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.PortRegistryService;
import com.baesystems.ai.lr.cd.be.domain.dto.port.PortOfRegistryInputDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.port.impl.PortServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;

import retrofit2.mock.Calls;

/**
 * Provides unit test on {@link PortService}.
 *
 * @author VKovuru
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PortServiceImplTest.Config.class)
@DirtiesContext
public class PortServiceImplTest {

  /**
   * Injected mock {@link PortRegistryService}.
   *
   */
  @Autowired
  private PortRegistryService portRegistryService;
  /**
   * Injected {@link PortService}.
   *
   */
  @Autowired
  private PortService portService;

  /**
   *
   * @throws Exception exception
   */
  @Test
  public final void testGetPortsWithSearchValue() throws Exception {
    Mockito.when(portRegistryService.getPortOfRegistryDtos()).thenReturn(Calls.response(mockPortList()));

    final PortOfRegistryInputDto portDto = new PortOfRegistryInputDto();

    // Search with prefix without limit.
    String[] expectedPortNames = new String[]{"London", "Londonderry", "Longhope, Hoy", "Longkou Pt"};
    portDto.setSearch("lon");
    portDto.setLimit(null);
    List<PortOfRegistryDto>  resultList = portService.getPorts(portDto);
    Assert.assertNotNull(resultList);
    Assert.assertEquals(expectedPortNames.length, resultList.size());
    List<String> actualPortNames = resultList.stream().map(PortOfRegistryDto::getName).collect(Collectors.toList());
    // Should be sorted with 4 records
    Assert.assertArrayEquals(expectedPortNames, actualPortNames.toArray(new String[expectedPortNames.length]));

    // Search with prefix with limit
    expectedPortNames = new String[]{"London", "Londonderry"};
    portDto.setSearch("lon");
    portDto.setLimit(expectedPortNames.length);
    resultList = portService.getPorts(portDto);
    Assert.assertNotNull(portDto);
    Assert.assertNotNull(resultList);
    Assert.assertEquals(expectedPortNames.length, resultList.size());
    Assert.assertEquals(expectedPortNames.length, resultList.size());
    actualPortNames = resultList.stream().map(PortOfRegistryDto::getName).collect(Collectors.toList());
    // Should be sorted with 2 records
    Assert.assertArrayEquals(expectedPortNames, actualPortNames.toArray(new String[expectedPortNames.length]));

    // Search without prefix without limit
    expectedPortNames = new String[]{"Kuala Lumpur", "London", "Londonderry", "Longhope, Hoy", "Longkou Pt"};
    portDto.setSearch(null);
    portDto.setLimit(null);
    resultList = portService.getPorts(portDto);
    Assert.assertNotNull(resultList);
    Assert.assertEquals(expectedPortNames.length, resultList.size());
    actualPortNames = resultList.stream().map(PortOfRegistryDto::getName).collect(Collectors.toList());
    // Should be sorted with 5 records
    Assert.assertArrayEquals(expectedPortNames, actualPortNames.toArray(new String[expectedPortNames.length]));

    // Search without prefix without limit
    expectedPortNames = new String[]{"Kuala Lumpur", "London", "Londonderry"};
    portDto.setSearch(null);
    portDto.setLimit(expectedPortNames.length);
    resultList = portService.getPorts(portDto);
    Assert.assertNotNull(resultList);
    Assert.assertEquals(expectedPortNames.length, resultList.size());
    actualPortNames = resultList.stream().map(PortOfRegistryDto::getName).collect(Collectors.toList());
    // Should be sorted with 3 records
    Assert.assertArrayEquals(expectedPortNames, actualPortNames.toArray(new String[expectedPortNames.length]));
  }
   /**
   *
   * @throws Exception exception
   */
  @Test
  public final void testGetPortById() throws Exception {
    final Long testId = 1L;
    PortOfRegistryDto mockObject = new PortOfRegistryDto();
    mockObject.setId(testId);
    mockObject.setName("Portufal, Lisboa");
    Mockito.when(portRegistryService.getPort(testId)).thenReturn(Calls.response(mockObject));
    PortOfRegistryDto portOfRegistry = portService.getPort(testId);
    Assert.assertNotNull(portOfRegistry);
    Assert.assertEquals(testId, portOfRegistry.getId());
    Assert.assertEquals(mockObject.getName(), portOfRegistry.getName());
  }

  /**
   *
   * @throws Exception exception
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testNoPortById() throws Exception {
    Mockito.when(portRegistryService.getPort(anyLong())).thenReturn(Calls.failure(new IOException("Mock exception")));
    portService.getPort(1L);
  }

  /**
   * Populates a list of port information for testing.
   * @return the list of port information for testing.
   */
  private List<PortOfRegistryDto> mockPortList() {
    final String[] portNameAry = new String[]{"Londonderry", "Longhope, Hoy", "Longkou Pt", "London", "Kuala Lumpur"};
    final List<PortOfRegistryDto> portList = new ArrayList<>(portNameAry.length);

    for (int i = 0; i < portNameAry.length; i++) {
      final PortOfRegistryDto port = new PortOfRegistryDto();
      port.setId(new Long(i + 1));
      port.setName(portNameAry[i]);
      portList.add(port);
    }
    return portList;
  }

  /**
   * Provides mock Spring bean config for testing.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Returns {@link portService} for unit test.
     * @return the {@link portService}.
     */
    @Override
    @Bean
    public PortService portService() {
      return new PortServiceImpl();
    }
  }
}
