package com.baesystems.ai.lr.cd.be.utils.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.ServiceHelper;
import com.baesystems.ai.lr.cd.be.utils.ServiceHelperImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * @author syalavarthi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceHelperTest.Config.class)
@DirtiesContext
public class ServiceHelperTest {

  /**
  *
  */
  @Value("${not.found.error}")
  private String errorMessage;
  /**
   * ServiceHelper obj.
   */
  @Autowired
  private ServiceHelper serviceHelper;

  /**
   * @throws RecordNotFoundException exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void verifyModelTestNull() throws RecordNotFoundException {
    List<ScheduledServiceHDto> dtos = null;
    serviceHelper.verifyModel(dtos, 1L);
  }

  /**
   * @throws RecordNotFoundException exception.
   */
  @Test
  public final void verifyModelTest() throws RecordNotFoundException {
    List<ScheduledServiceHDto> dtosList = new ArrayList<>();
    ScheduledServiceHDto serviceDto = new ScheduledServiceHDto();
    dtosList.add(serviceDto);
    serviceHelper.verifyModel(dtosList, 1L);
    assertEquals(1, dtosList.size());
    assertFalse(dtosList.isEmpty());
  }
  /**
   * @author syalavarthi.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create ServiceHelper service.
     *
     * @return ServiceHelper.
     */
    @Override
    @Bean
    public ServiceHelper serviceHelper() {
      return new ServiceHelperImpl();
    }
  }

}
