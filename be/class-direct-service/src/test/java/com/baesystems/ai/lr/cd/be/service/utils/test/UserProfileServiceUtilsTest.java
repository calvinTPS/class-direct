package com.baesystems.ai.lr.cd.be.service.utils.test;

import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

/**
 * Test case for {@link UserProfileServiceUtils}.
 *
 * @author Faizal Sidek
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class UserProfileServiceUtilsTest {

  /**
   * Will return true when user is client/builder/flag.
   */
  @Test
  public final void returnTrueIfUserIsClientBuilderOrFlag() {
    UserProfiles userProfiles = new UserProfiles();

    userProfiles.setClientCode("1");
    Assert.assertTrue(UserProfileServiceUtils.isClientUser(userProfiles));

    userProfiles = new UserProfiles();
    userProfiles.setFlagCode("ALA");
    Assert.assertTrue(UserProfileServiceUtils.isFlagUser(userProfiles));

    userProfiles = new UserProfiles();
    userProfiles.setShipBuilderCode("1");
    Assert.assertTrue(UserProfileServiceUtils.isBuilderUser(userProfiles));
  }

  /**
   * Return false if user is not client.
   */
  @Test
  public final void returnFalseIfUserIsNotClientBuilderOrFlag() {
    final UserProfiles user = new UserProfiles();
    Assert.assertFalse(UserProfileServiceUtils.isClientUser(user));
    Assert.assertFalse(UserProfileServiceUtils.isFlagUser(user));
    Assert.assertFalse(UserProfileServiceUtils.isBuilderUser(user));
  }
}
