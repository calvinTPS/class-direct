package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.StatutoryFindingsExportService;
import com.baesystems.ai.lr.cd.service.export.impl.StatutoryFindingsExportServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;

/**
 * Provides unit test methods for {@link StatutoryFindingsExportService}.
 *
 * @author sbollu
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = StatutoryFindingsExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class StatutoryFindingsExportServiceTest {

  /**
   * The {@link StatutoryFindingService} from {@link StatutoryFindingsExportServiceTest.Config}
   * class.
   */
  @Autowired
  private StatutoryFindingService sFindingService;


  /**
   * The {@link AssetService} from {@link StatutoryFindingsExportServiceTest.Config} class.
   */
  @Autowired
  private AssetService assetService;


  /**
   * The {@link StatutoryFindingsExportService} from
   * {@link StatutoryFindingsExportServiceTest.Config} class.
   */
  @Autowired
  private StatutoryFindingsExportService sfExportService;


  /**
   * The {@link AmazonStorageService} from {@link StatutoryFindingsExportServiceTest.Config} class.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * Provides mock reset before any test class is executed.
   */
  @Before
  public void resetMock() {
    Mockito.reset(sFindingService);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }


  /**
   * Tests success scenario of generating PDF for
   * {@link StatutoryFindingsExportService#downloadPdf(ExportPdfDto)}.
   *
   * @throws ClassDirectException if io exception occurred or Jade Exception occurred or document
   *         Exception occurred.
   */
  @Test
  public void testGenerateSFPDF() throws ClassDirectException {
    // mock asset data
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("My asset");
    asset.setLeadImo("1000019");
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("AB");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    asset.setIhsAssetDto(ihsAssetDetailsDto);
    asset.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    asset.getClassMaintenanceStatusDto().setName("Single");

    // mock sf data
    final StatutoryFindingHDto data1 = new StatutoryFindingHDto();
    data1.setId(1L);
    data1.setTitle("sf 1");
    data1.setImposedDate(new GregorianCalendar(2017, 1, 2).getTime());
    data1.setDueDate(new GregorianCalendar(2017, 1, 2).getTime());
    CodicilCategoryHDto category = new CodicilCategoryHDto();
    category.setName("Category1");
    data1.setCategoryH(category);
    data1.setDescription("sf1");
    CodicilStatusHDto status = new CodicilStatusHDto();
    status.setName("Status");
    data1.setStatusH(status);



    final StatutoryFindingHDto data2 = new StatutoryFindingHDto();
    data2.setId(2L);
    data2.setTitle("sf 2");
    data2.setImposedDate(new GregorianCalendar(2017, 3, 5).getTime());
    data2.setDueDate(new GregorianCalendar(2017, 3, 5).getTime());


    final StatutoryFindingHDto data3 = new StatutoryFindingHDto();
    data3.setId(3L);
    data3.setTitle("sf 3");
    data3.setImposedDate(new GregorianCalendar(2017, 3, 1).getTime());
    data3.setDueDate(new GregorianCalendar(2017, 3, 1).getTime());

    final List<StatutoryFindingHDto> sfMock = new ArrayList<>();
    sfMock.add(data1);
    sfMock.add(data2);
    sfMock.add(data3);


    Mockito.when(sFindingService.getStatutories(Matchers.anyLong(), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(sfMock);
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(asset);
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ExportPdfDto query = new ExportPdfDto();
    query.setCode("LRV1");
    Set<Long> itemsToExport = new HashSet<Long>();
    itemsToExport.add(1L);
    itemsToExport.add(2L);
    query.setItemsToExport(itemsToExport);
    final StringResponse response = sfExportService.downloadPdf(query);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   *
   * Provides Spring in-class configurations.
   *
   * @author sbollu
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * un-mock StatutoryFindings export service.
     *
     * @return
     */
    @Override
    @Bean
    public StatutoryFindingsExportService sfExportService() {
      return new StatutoryFindingsExportServiceImpl();
    }
  }

}
