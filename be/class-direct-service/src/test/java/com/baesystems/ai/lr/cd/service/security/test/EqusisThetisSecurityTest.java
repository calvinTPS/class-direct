package com.baesystems.ai.lr.cd.service.security.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AttachmentExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ChecklistExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ESPReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.PmsTaskListExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.export.TaskListExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.survey.SurveyRequestDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.service.reference.impl.ServiceReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.JobService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.impl.JobServiceImpl;
import com.baesystems.ai.lr.cd.service.countryfiles.CountryFileService;
import com.baesystems.ai.lr.cd.service.countryfiles.CountryFileServiceImpl;
import com.baesystems.ai.lr.cd.service.export.ActionableItemExportService;
import com.baesystems.ai.lr.cd.service.export.AssetExportService;
import com.baesystems.ai.lr.cd.service.export.AssetNoteExportService;
import com.baesystems.ai.lr.cd.service.export.ChecklistExportService;
import com.baesystems.ai.lr.cd.service.export.CocExportService;
import com.baesystems.ai.lr.cd.service.export.EspExportService;
import com.baesystems.ai.lr.cd.service.export.PmsTaskListExportService;
import com.baesystems.ai.lr.cd.service.export.ReportExportService;
import com.baesystems.ai.lr.cd.service.export.StatutoryDeficienciesExportService;
import com.baesystems.ai.lr.cd.service.export.StatutoryFindingsExportService;
import com.baesystems.ai.lr.cd.service.export.TMReportDownloadService;
import com.baesystems.ai.lr.cd.service.export.TaskListExportService;
import com.baesystems.ai.lr.cd.service.export.UsersExportService;
import com.baesystems.ai.lr.cd.service.export.impl.ActionableItemExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.AssetExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.AssetNoteExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.ChecklistExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.CocExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.EspExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.PmsTaskListExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.ReportExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.StatutoryDeficienciesExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.StatutoryFindingsExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.TMReportDownloadServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.TaskListExportServiceImpl;
import com.baesystems.ai.lr.cd.service.export.impl.UsersExportServiceImpl;
import com.baesystems.ai.lr.cd.service.survey.SurveyService;
import com.baesystems.ai.lr.cd.service.survey.impl.SurveyServiceImpl;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.task.impl.TaskServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Test Class to test Security Expressions (@PreAuthorize).
 *
 * @author syalavarthi.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestExecutionListeners(listeners = {ServletTestExecutionListener.class, DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class,
    WithSecurityContextTestExecutionListener.class})
public class EqusisThetisSecurityTest {
  /**
   * Injected SurveyService.
   *
   */
  @Autowired
  private SurveyService surveyService;
  /**
   * Injected AssetService.
   *
   */
  @Autowired
  private AssetService assetService;
  /**
   * Injected ServiceReferenceService.
   *
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;
  /**
   * Injected TaskService.
   *
   */
  @Autowired
  private TaskService taskService;
  /**
   * Injected CountryFileService.
   *
   */
  @Autowired
  private CountryFileService countryFileService;
  /**
   * Injected ActionableItemExportService.
   */
  @Autowired
  private ActionableItemExportService actionableItemExportService;
  /**
   * Injected AssetExportService.
   */
  @Autowired
  private AssetExportService assetExportService;
  /**
   * Injected assetNoteExportService.
   */
  @Autowired
  private AssetNoteExportService assetNoteExportService;
  /**
   * Injected ChecklistExportService.
   */
  @Autowired
  private ChecklistExportService checklistExportService;
  /**
   * Injected CocExportService.
   */
  @Autowired
  private CocExportService cocExportService;
  /**
   * Injected PmsTaskListExportService.
   */
  @Autowired
  private PmsTaskListExportService pmsTaskListExportService;
  /**
   * Injected ReportExportService.
   */
  @Autowired
  private ReportExportService reportExportService;
  /**
   * Injected TaskListExportService.
   */
  @Autowired
  private TaskListExportService taskListExportService;
  /**
   * Injected TMReportDownloadService.
   */
  @Autowired
  private TMReportDownloadService tMReportDownloadService;
  /**
   * Injected UsersExportService.
   */
  @Autowired
  private UsersExportService usersExportService;

  /**
   * Injected StatutoryDeficienciesExportService.
   */
  @Autowired
  private StatutoryDeficienciesExportService sdExportService;

  /**
   * Injected StatutoryFindingsExportService.
   */
  @Autowired
  private StatutoryFindingsExportService sfExportService;
  /**
   * Injected JobService.
   */
  @Autowired
  private JobService jobService;

  /**
   * injects EspExportService.
   */
  @Autowired
  private EspExportService espExportService;
  /**
   * Constant user id.
   */
  private static final String USER_ID = "c0615f39-227c-4406-b2ad-367f0da6461c";
  /**
   * Constant asset code.
   */
  private static final String ASSET_CODE = "LRV1";
  /**
   * Constant asset id.
   */
  private static final Long ASSET_ID = 1L;
  /**
   * Constant flag id.
   */
  private static final Long FLAG_ID = 100L;

  /**
   * Constant flag id.
   */
  private static final String LEAD_IMO = "100010";

  /**
   * submit request survey.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void requestSurveyReturnExceptionForEquasis() throws Exception {
    StatusDto response = surveyService.validateSurveyRequest(USER_ID, new SurveyRequestDto());
  }

  /**
   * create favourite test.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void createFavouriteReturnExceptionForEquasis() throws Exception {
    StatusDto response = assetService.createFavourite(USER_ID, ASSET_CODE);
  }

  /**
   * Tests user access for method
   * {@link AssetService#createFavouriteList(String, com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto)}.
   *
   * @throws Exception if user doesn't have access to method.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void createFavouriteListReturnExceptionForEquasis() throws Exception {
    FavouritesDto fav = new FavouritesDto();
    String response = assetService.updateFavouriteList(USER_ID, fav);
  }

  /**
   * get services for asset.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void getServicesReturnExceptionForEquasis() throws Exception {
    List<ScheduledServiceHDto> response = serviceReferenceService.getServicesForAsset(ASSET_ID);
  }

  /**
   * get technical sisters for asset.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void getTechnicalSisterReturnExceptionForEquasis() throws Exception {
    PageResource<AssetHDto> response = assetService.getTechnicalSister(LEAD_IMO, USER_ID, ASSET_CODE, null, null);
  }

  /**
   * get registryBookSister for asset.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void getRegistryBookSisterReturnExceptionForEquasis() throws Exception {
    PageResource<AssetHDto> response = assetService.getRegisterBookSister(LEAD_IMO, USER_ID, ASSET_CODE, null, null);
  }

  /**
   * Get tasks by asetId pagination.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void getTasksReturnExceptionForEquasis() throws Exception {
    PageResource<WorkItemLightHDto> response = taskService.getTasksByAssetId(ASSET_ID, false, null, null);
  }

  /**
   * get country file by flag id.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void getCountryFileExceptionForEquasis() throws Exception {
    List<SupplementaryInformationHDto> response = countryFileService.getAttachmentsCountryFile(FLAG_ID);
  }

  /**
   * get country file by flag id.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadPdfAIFileExceptionForEquasis() throws Exception {
    StringResponse response = actionableItemExportService.downloadPdf(new ExportPdfDto());
  }

  /**
   * Asset Export.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadAssetFileExceptionForEquasis() throws Exception {
    StringResponse response = assetExportService.downloadAssetInfo(new AssetExportDto(), new UserProfiles());
  }

  /**
   * Asset notes export.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadAssetNOtesExceptionForEquasis() throws Exception {
    StringResponse response = assetNoteExportService.downloadPdf(new ExportPdfDto());
  }

  /**
   * check list export.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadChecklistExceptionForEquasis() throws Exception {
    StringResponse response = checklistExportService.downloadPdf(new ChecklistExportDto());
  }

  /**
   * coc export.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadCocExceptionForEquasis() throws Exception {
    StringResponse response = cocExportService.downloadPdf(new ExportPdfDto());
  }

  /**
   * downloadPmsTaskListInfo.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadPmsExceptionForEquasis() throws Exception {
    StringResponse response = pmsTaskListExportService.downloadPmsTaskListInfo(new PmsTaskListExportDto());
  }

  /**
   * download FAR report.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadFARExceptionForEquasis() throws Exception {
    StringResponse response = reportExportService.downloadPdf(new ReportExportDto());
  }

  /**
   * download tasklist.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadTaskExceptionForEquasis() throws Exception {
    StringResponse response = taskListExportService.downloadTaskListInfo(new TaskListExportDto());
  }

  /**
   * download tm report.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadTMExceptionForEquasis() throws Exception {
    StringResponse response = tMReportDownloadService.downloadAttachmentInfo(new AttachmentExportDto());
  }

  /**
   * export users.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadUsersExceptionForEquasis() throws Exception {
    StringResponse response = usersExportService.downloadFile(new UserProfileDto());
  }

  /**
   * export Statutory Deficiencies.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadStatutoryDeficienciesExceptionForEquasis() throws Exception {
    StringResponse response = sdExportService.downloadPdf(new ExportPdfDto());
  }

  /**
   * export Statutory Findings.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void downloadStatutoryFindingsExceptionForEquasis() throws Exception {
    StringResponse response = sfExportService.downloadPdf(new ExportPdfDto());
  }

  /**
   * get jobs test.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void getJobsExceptionForEquasis() throws Exception {
    List<JobCDDto> jobs = jobService.getJobsByAssetIdCall(1L, false);
  }

  /**
   * get survey repports.
   *
   * @throws Exception Throwable when error.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void getSurveyReportsExceptionForEquasis() throws Exception {
    PageResource<ReportHDto> reports = jobService.getSurveyReports(null, null, 1L);
  }

  /**
   * tests to throw AccessDeniedException for unauthorized access.
   *
   * @throws Exception if unauthorized user exports the report.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void shouldReturnExceptionEspReportExport() throws Exception {
    StringResponse response = espExportService.downloadPdf(new ESPReportExportDto());
  }

  /**
   * @author syalavarthi
   *
   */
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  public static class Config extends BaseMockConfiguration {


    @Override
    @Bean
    public SurveyService surveyService() {
      return new SurveyServiceImpl();
    }

    @Override
    @Bean
    public AssetService assetService() {
      return new AssetServiceImpl();
    }

    @Override
    @Bean
    public ServiceReferenceService serviceReferenceService() {
      return new ServiceReferenceServiceImpl();
    }

    @Override
    @Bean
    public TaskService taskService() {
      return new TaskServiceImpl();
    }

    @Override
    @Bean
    public CountryFileService countryFileService() {
      return new CountryFileServiceImpl();
    }

    @Override
    @Bean
    public ActionableItemExportService actionableItemExportService() {
      return new ActionableItemExportServiceImpl();
    }

    @Override
    @Bean
    public AssetExportService assetExportService() {
      return new AssetExportServiceImpl();
    }

    @Override
    @Bean
    public AssetNoteExportService assetNoteExportService() {
      return new AssetNoteExportServiceImpl();
    }

    @Override
    @Bean
    public ChecklistExportService checklistExportService() {
      return new ChecklistExportServiceImpl();
    }

    @Override
    @Bean
    public CocExportService cocExportService() {
      return new CocExportServiceImpl();
    }

    @Override
    @Bean
    public PmsTaskListExportService pmsTaskListExportService() {
      return new PmsTaskListExportServiceImpl();
    }

    @Override
    @Bean
    public ReportExportService reportExportService() {
      return new ReportExportServiceImpl();
    }

    @Override
    @Bean
    public TaskListExportService taskListExportService() {
      return new TaskListExportServiceImpl();
    }

    @Override
    @Bean
    public TMReportDownloadService tMReportDownloadService() {
      return new TMReportDownloadServiceImpl();
    }

    @Override
    @Bean
    public UsersExportService usersExportService() {
      return new UsersExportServiceImpl();
    }

    @Override
    @Bean
    public StatutoryDeficienciesExportService sdExportService() {
      return new StatutoryDeficienciesExportServiceImpl();
    }

    @Override
    @Bean
    public StatutoryFindingsExportService sfExportService() {
      return new StatutoryFindingsExportServiceImpl();
    }

    @Override
    @Bean
    public JobService jobService() {
      return new JobServiceImpl();
    }

    @Override
    @Bean
    public EspExportService espExportService() {
      return new EspExportServiceImpl();
    }

    /**
     * @param auth auth.
     * @throws Exception exception.
     */
    @Autowired
    public final void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
      auth.inMemoryAuthentication().withUser(" ").password(" ").authorities("EQUASIS", "THESIS");
    }

  }
}
