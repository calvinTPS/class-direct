package com.baesystems.ai.lr.cd.service.export.impl.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNotePageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.EquipmentDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.OwnershipDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.PrincipalDimensionDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RegistryInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RulesetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.UserPersonalInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CustomerLinkHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.TaskPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AssetInformationExportEnum;
import com.baesystems.ai.lr.cd.be.enums.AssetServiceExportEnum;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.enums.MastPartyRole;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.AssetExportService;
import com.baesystems.ai.lr.cd.service.export.impl.AssetExportServiceImpl;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.defects.JobFaultDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.FaultCategoryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.enums.CodicilStatus;
import com.baesystems.ai.lr.enums.ServiceStatus;
import com.baesystems.ai.lr.enums.WorkItemType;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for {@link AssetExportService}.
 *
 * @author yng
 * @author YWearn 2017-05-17
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@PrepareForTest(SecurityContextHolder.class)
@ContextConfiguration(classes = AssetExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class AssetExportServiceTest {

  /**
   * The application logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetExportServiceTest.class);

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link AssetExportService} from Spring context.
   */
  @Autowired
  private AssetExportService assetExportService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceService;

  /**
   * The {@link TaskService} from Spring context.
   */
  @Autowired
  private TaskService taskService;

  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private CoCService coCService;

  /**
   * The {@link ActionableItemService} from Spring context.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   * The {@link AssetNoteService} from Spring context.
   */
  @Autowired
  private AssetNoteService assetNoteService;

  /**
   * The {@link StatutoryFindingService} from Spring context.
   */
  @Autowired
  private StatutoryFindingService statutoryFindingService;

  /**
   * The {@link AmazonStorageService} from Spring context.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The default year for date field.
   */
  private static final int YEAR_2017 = 2017;

  /**
   * The test mast asset id: 1.
   */
  private static final long TEST_MAST_ASSET_ID = 1L;

  /**
   * The test ihs asset id: 1000020.
   */
  private static final long TEST_IHS_ASSET_ID = 1000020L;

  /**
   * The test statutory finding id: 4.
   */
  private static final long STATUTORY_4 = 4L;

  /**
   * The maximum asset allow to export per request.
   */
  @Value("${asset.export.max.count}")
  private int maxAssetExportCount;

  /**
   * Resets mock before each test cases invoked.
   */
  @Before
  public void resetMock() {
    Mockito.reset(assetService, serviceService, taskService, coCService, actionableItemService, assetNoteService);
    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);

    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(auth);
    PowerMockito.mockStatic(SecurityContextHolder.class);
    PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(securityContext);
  }

  /**
   * Tests positive scenario of asset export.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testAssetExport() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());

    final List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(1L);
    codicilConfig.add(2L);
    codicilConfig.add(3L);
    codicilConfig.add(4L);
    codicilConfig.add(5L);
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(mockServiceData());
    when(taskService.getTasksForServices(any())).thenReturn(mockTaskDataForAssetFullExport());
    when(taskService.getCheckListItemByService(any())).thenReturn(mockCheckListHierarchyByService());
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCData());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobsData()));

    // call asset export
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();
    final ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    final long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);
  }

  /**
   * Tests scenario where all the external API to fetch asset details hit error but the report
   * should still be generated.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testAssetExportFailCall() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    query.setExcludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());

    final List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(1L);
    codicilConfig.add(2L);
    codicilConfig.add(3L);
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenThrow(new ClassDirectException("fail test."));
    when(serviceService.getServicesForAsset(anyLong())).thenThrow(new IOException("fail test."));
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenThrow(new ClassDirectException("fail test."));
    when(coCService.getCoCForExport(anyLong())).thenThrow(new ClassDirectException("fail test."));
    when(actionableItemService.getActionableItem(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenThrow(new ClassDirectException("fail test."));
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenThrow(new ClassDirectException("fail test."));
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobsData()));

    // call asset export
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();
    final ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated even if some of the inner API fail.", HttpStatus.OK.value(),
        response.getStatusCode());
    final long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);

  }

  /**
   * Tests scenario where the task and defect information for given asset is not found.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testAssetExportFailInnerCall() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    query.setExcludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());

    final List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(1L);
    codicilConfig.add(2L);
    codicilConfig.add(3L);
    query.setCodicils(codicilConfig);

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenThrow(new ClassDirectException("fail test."));
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(mockServiceData());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenThrow(new ClassDirectException("fail test."));
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCData());
    when(actionableItemService.getActionableItem(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenThrow(new ClassDirectException("fail test."));
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenThrow(new ClassDirectException("fail test."));
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobsData()));

    // call asset export
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();
    final ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated even if some of the inner API fail.", HttpStatus.OK.value(),
        response.getStatusCode());
    final long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);
  }

  /**
   * Tests not to fail the method even if some of the asset related details is not available.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testAssetExportApiReturnNull() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    query.setExcludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());

    final List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(1L);
    codicilConfig.add(2L);
    codicilConfig.add(3L);
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenReturn(null);
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(null);
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(null);
    when(coCService.getCoCForExport(anyLong())).thenReturn(null);
    when(actionableItemService.getActionableItem(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(null);
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong())).thenReturn(null);
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.failure(new IOException()));

    // call asset export
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();
    final ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated even if some of the API return empty result.",
        HttpStatus.OK.value(), response.getStatusCode());
    final long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);
  }

  /**
   * Tests the complete PDF generation flow.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testAssetExportInnerApiReturnNull() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());

    final List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(1L);
    codicilConfig.add(2L);
    codicilConfig.add(3L);
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(mockServiceData());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(null);
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCData());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    // call asset export
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();
    final ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated even if some of the inner API return empty.", HttpStatus.OK.value(),
        response.getStatusCode());
    final long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);
  }

  /**
   * Tests export single asset.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testSingleAssetReturned() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());

    final List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(1L);
    codicilConfig.add(2L);
    codicilConfig.add(3L);
    query.setCodicils(codicilConfig);

    // mock single asset
    final List<AssetHDto> singleItem = new ArrayList<>();
    singleItem.add(mockAssetsData().get(0));

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(singleItem));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(mockServiceData());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(mockTaskData());
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCDataWithDefect());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    // call asset export
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();
    final ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report for single asset should be generated.", HttpStatus.OK.value(),
        response.getStatusCode());
    final long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);
  }

  /**
   * Tests multiple combination of export criteria.
   * <p>
   * Tested criteria with combination of:
   * <ol>
   * <li>With included or/and excluded ids.</li>
   * <li>With basic or full asset information.</li>
   * <li>With option to include service or exclude service information.</li>
   * <li>With codicils or without.</li>
   * </ol>
   * </p>
   *
   * @throws ClassDirectException value.
   * @throws IOException value.
   */
  @Test
  public final void testMultiConfigCombination() throws ClassDirectException, IOException {
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(AssetHDto.class), anyString())).thenReturn(mockAssetDetailData());
    when(assetService.getAssetDetailsByAsset(Mockito.any(AssetHDto.class), anyString()))
        .thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(mockServiceData());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(mockTaskData());
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCDataWithDefect());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(taskService.getHierarchicalTasksByServiceIds(any(), any())).thenReturn(mockHierarchicalCheckList());
    when(assetService.convert(any())).thenReturn(MastQueryBuilder.create());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    // config combination 1
    AssetExportDto query = new AssetExportDto();
    List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.BASIC_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.BASIC_SERVICE_INFO.getId());
    List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(CodicilType.COC.getId());
    codicilConfig.add(CodicilType.AI.getId());
    codicilConfig.add(CodicilType.AN.getId());
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated with full asset | with Coc, AI, AN " + "| with basic service info.",
        HttpStatus.OK.value(), response.getStatusCode());
    long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);

    // config combination 2
    query = new AssetExportDto();
    ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());
    codicilConfig = new ArrayList<>();
    codicilConfig.add(CodicilType.COC.getId());
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");

    response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated with full asset | with Coc | will full service info.",
        HttpStatus.OK.value(), response.getStatusCode());
    elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);

    // config combination 2
    query = new AssetExportDto();
    ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.NO_SERVICE_INFO.getId());
    codicilConfig = new ArrayList<>();
    codicilConfig.add(CodicilType.AI.getId());
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");

    response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated with full asset | with AI | no service info.",
        HttpStatus.OK.value(), response.getStatusCode());
    elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);

    // config combination 3
    query = new AssetExportDto();
    ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.BASIC_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.NO_SERVICE_INFO.getId());
    query.setCodicils(new ArrayList<>(0));
    query.setFileType("PDF");

    response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated with basic asset | no codicil | no service info.",
        HttpStatus.OK.value(), response.getStatusCode());
    elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);

    // config combination 4
    query = new AssetExportDto();
    ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.BASIC_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.NO_SERVICE_INFO.getId());
    codicilConfig = new ArrayList<>();
    codicilConfig.add(CodicilType.AI.getId());
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");

    response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    Assert.assertEquals("Report should be generated with basic asset | with AI | without service info.",
        HttpStatus.OK.value(), response.getStatusCode());
    elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);
  }

  /**
   * Tests fetching export status with invalid userId and jobId which should throw
   * ClassDirectException.
   *
   * @throws ClassDirectException If the export status is not available for userId and jobId
   *         combination.
   */
  @Test(expected = ClassDirectException.class)
  public final void testFetchInvalidExportStatus() throws ClassDirectException {
    // Fetching with invalid userId and jobId will throw exception.
    assetExportService.fetchExportStatus("", "");
  }

  /**
   * Tests the export service to defines MAST asset query to utilizes the included asset ids and
   * excluded asset ids (if exists) in MAST IHS query, this should apply even if the custom filter
   * is used.
   * <p>
   * This will be more efficient as MAST will only return the assets that defined in included or not
   * in excluded list instead of return all assets then CD do a post filter.
   * </p>
   *
   * @throws ClassDirectException if there is an API error, otherwise should always success.
   * @throws IOException if get subfleet api fails.
   */
  // Disable this test for now as the test is testing the implementation class method instead of the
  // interface.
  // For now do not seem to be critical.
  // @Test
  public final void testIncludesAndExcludesFilterWhenAssetQueryIsNotNull() throws ClassDirectException, IOException {
    // mock user
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");

    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    query.setIncludes(new ArrayList<>());
    query.setExcludes(new ArrayList<>());
    query.setInfo(AssetInformationExportEnum.BASIC_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.BASIC_SERVICE_INFO.getId());
    query.setCodicils(new ArrayList<>());
    query.setAssetQuery(new AssetQueryHDto());

    final AssetExportServiceImpl impl = (AssetExportServiceImpl) assetExportService;

    List<AbstractQueryDto> conditions = null;

    // Tests filter on include LRV, MAST query filter should include the asset id
    when(assetService.convert(any())).thenReturn(MastQueryBuilder.create());
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any())).thenReturn(new AssetPageResource());
    ids.add("LRV" + TEST_MAST_ASSET_ID);
    query.setIncludes(ids);

    impl.getAssetList(query, user);

    final ArgumentCaptor<MastQueryBuilder> argument = ArgumentCaptor.forClass(MastQueryBuilder.class);
    final ArgumentCaptor<Integer> pageSizeArgument = ArgumentCaptor.forClass(Integer.class);
    verify(assetService).getAssetByQueryBuilder(any(), any(), argument.capture(), any(), pageSizeArgument.capture());
    conditions = getQueryInConditions(argument.getValue());
    assertNotNull(conditions);
    for (AbstractQueryDto cond : conditions) {
      Assert.assertTrue(
          "Asset id is expected to be in the query filter - IN condition. | Actual: "
              + Arrays.toString((Long[]) cond.getValue()) + ", Expected: " + TEST_MAST_ASSET_ID,
          Arrays.asList((Long[]) cond.getValue()).contains(TEST_MAST_ASSET_ID));
    }
    assertEquals(maxAssetExportCount, pageSizeArgument.getValue().intValue());

    // Tests filter on include IHS, MAST query filter should include the asset id
    Mockito.reset(assetService);
    when(assetService.convert(any())).thenReturn(MastQueryBuilder.create());
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any())).thenReturn(new AssetPageResource());
    query.getIncludes().clear();
    query.getIncludes().add("IHS" + TEST_IHS_ASSET_ID);

    impl.getAssetList(query, user);

    verify(assetService).getAssetByQueryBuilder(any(), any(), argument.capture(), any(), pageSizeArgument.capture());
    conditions = getQueryInConditions(argument.getValue());
    assertNotNull(conditions);
    for (AbstractQueryDto cond : conditions) {
      Assert.assertTrue(
          "Asset id is expected to be in the query filter - IN condition. | Actual: "
              + Arrays.toString((Long[]) cond.getValue()) + ", Expected: " + TEST_IHS_ASSET_ID,
          Arrays.asList((Long[]) cond.getValue()).contains(TEST_IHS_ASSET_ID));
    }
    assertEquals(maxAssetExportCount, pageSizeArgument.getValue().intValue());

    // Tests filter on excluded LRV, MAST query filter should include the asset id
    Mockito.reset(assetService);
    when(assetService.convert(any())).thenReturn(MastQueryBuilder.create());
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any())).thenReturn(new AssetPageResource());

    query.getIncludes().clear();
    query.getExcludes().add("LRV" + TEST_MAST_ASSET_ID);

    impl.getAssetList(query, user);

    verify(assetService).getAssetByQueryBuilder(any(), any(), argument.capture(), any(), pageSizeArgument.capture());
    conditions = getQueryNotInConditions(argument.getValue());
    assertNotNull(conditions);
    for (AbstractQueryDto cond : conditions) {
      Assert.assertTrue(
          "Asset id is expected to be in the query filter - NOT IN condition. | Actual: "
              + Arrays.toString((Long[]) cond.getValue()) + ", Expected: " + TEST_MAST_ASSET_ID,
          Arrays.asList((Long[]) cond.getValue()).contains(TEST_MAST_ASSET_ID));
    }
    assertEquals(maxAssetExportCount, pageSizeArgument.getValue().intValue());

    // Tests filter on excluded IHS, MAST query filter should include the asset id
    Mockito.reset(assetService);
    when(assetService.convert(any())).thenReturn(MastQueryBuilder.create());
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any())).thenReturn(new AssetPageResource());

    query.getIncludes().clear();
    query.getExcludes().add("IHS" + TEST_IHS_ASSET_ID);

    impl.getAssetList(query, user);

    verify(assetService).getAssetByQueryBuilder(any(), any(), argument.capture(), any(), pageSizeArgument.capture());
    conditions = getQueryNotInConditions(argument.getValue());
    assertNotNull(conditions);
    for (AbstractQueryDto cond : conditions) {
      Assert.assertTrue(
          "Asset id is expected to be in the query filter - NOT IN condition. | Actual: "
              + Arrays.toString((Long[]) cond.getValue()) + ", Expected: " + TEST_IHS_ASSET_ID,
          Arrays.asList((Long[]) cond.getValue()).contains(TEST_IHS_ASSET_ID));
    }
    assertEquals(maxAssetExportCount, pageSizeArgument.getValue().intValue());
  }

  /**
   * Tests asset export as csv with basic service and basic information.
   *
   * @throws ClassDirectException if there is error fetching services related to asset.
   * @throws IOException if there is error fetching asset related details.
   */
  @Test
  public final void testAssetExportCsv() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setFileType("CSV");

    final List<Long> codicilConfig = new ArrayList<>();
    query.setCodicils(codicilConfig);

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(Mockito.eq(1L))).thenReturn(mockServiceData());
    when(serviceService.getServicesForAsset(Mockito.eq(2L))).thenReturn(mockServiceData());
    when(serviceService.getServicesForAsset(Mockito.eq(3L))).thenReturn(Collections.<ScheduledServiceHDto>emptyList());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(mockTaskData());
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCData());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    // call asset export
    final UserProfiles mockUser = (UserProfiles) SecurityContextHolder.getContext().getAuthentication().getDetails();
    final ExportStatusDto response = invokeExport(mockUser.getUserId(), query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());
    final long elapsedTime = response.getEndTime().getTime() - response.getStartTime().getTime();
    LOGGER.info("Export async response. | jobId: {}, statusCode: {} | Elapsed time(ms): {}", response.getJobId(),
        response.getStatusCode(), elapsedTime);
    Assert.assertEquals(HttpStatus.OK.value(), response.getStatusCode());
  }

  /**
   * Invokes the actual async asset export and poll the final response once export ready.
   *
   * @param userId The invoked user identifier.
   * @param query The export query configuation.
   * @return The final export status.
   * @throws ClassDirectException If error on generating assets export.
   */
  private ExportStatusDto invokeExport(final String userId, final AssetExportDto query) throws ClassDirectException {
    final ExportStatusDto response = assetExportService.exportAssetAsync(userId, query);
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getJobId());

    final String jobId = response.getJobId();
    // Maximum wait for 30 seconds
    final int maxRetry = 60;
    final long sleepMs = 500L;
    int retry = 0;

    ExportStatusDto result = null;

    while (retry++ < maxRetry) {
      result = assetExportService.fetchExportStatus(userId, jobId);
      if (result != null && result.getEndTime() != null) {
        break;
      }
      try {
        Thread.sleep(sleepMs);
      } catch (InterruptedException e) {
      }
    }
    Assert.assertNotNull("Export is not completed after " + maxRetry + " retries.", result);
    Assert.assertNotEquals("Export is not completed after " + maxRetry + " retries.", 202, result.getStatusCode());
    return result;
  }

  /**
   * Tests asset export as PDF with basic service and basic information.
   *
   * @throws ClassDirectException if there is error fetching services related to asset.
   * @throws IOException if there is error fetching asset related details.
   */
  @Test
  public final void testAssetExportPDFWithBasicInfoAndService() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);

    // basic asset export
    query.setInfo(AssetInformationExportEnum.BASIC_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.NO_SERVICE_INFO.getId());
    final List<Long> codicilConfig = new ArrayList<>();
    query.setCodicils(codicilConfig);

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(Mockito.eq(1L))).thenReturn(mockServiceData());
    when(serviceService.getServicesForAsset(Mockito.eq(2L))).thenReturn(mockServiceData());
    when(serviceService.getServicesForAsset(Mockito.eq(3L))).thenReturn(Collections.<ScheduledServiceHDto>emptyList());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(mockTaskData());
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCData());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    // call ihs function
    final StringResponse response = assetExportService.downloadAssetInfo(query, mockUser());

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   * Tests asset export for single restricted asset with no service details.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testAssetExportRestrictedWithNOService() throws ClassDirectException, IOException {
    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(mockAssetsData()));
    when(assetService.getAssetDetailsByAsset(any(AssetHDto.class), anyString())).thenReturn(mockAssetDetailData());
    when(assetService.getAssetDetailsByAsset(Mockito.any(AssetHDto.class), anyString()))
        .thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(mockServiceData());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(mockTaskData());
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCDataWithDefect());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(assetService.convert(any())).thenReturn(MastQueryBuilder.create());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    AssetExportDto query = new AssetExportDto();
    List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.BASIC_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.NO_SERVICE_INFO.getId());
    List<Long> codicilConfig = new ArrayList<>();
    query.setCodicils(codicilConfig);
    query.setFileType("PDF");
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    StringResponse response = assetExportService.downloadAssetInfo(query, mockUser());

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());

  }

  /**
   * Tests export for single restricted asset with full service and full information.
   *
   * @throws ClassDirectException if there is error fetching asset related details.
   * @throws IOException if there is error fetching services related to asset.
   */
  @Test
  public final void testSingleRestrictedAssetReturned() throws ClassDirectException, IOException {
    // mock query data
    final AssetExportDto query = new AssetExportDto();
    final List<String> ids = new ArrayList<>();
    ids.add("LRV1");
    ids.add("LRV2");
    query.setIncludes(ids);
    query.setInfo(AssetInformationExportEnum.FULL_ASSET_INFO.getId());
    query.setService(AssetServiceExportEnum.FULL_SERVICE_INFO.getId());

    final List<Long> codicilConfig = new ArrayList<>();
    codicilConfig.add(1L);
    codicilConfig.add(2L);
    codicilConfig.add(3L);
    query.setCodicils(codicilConfig);

    // mock single asset
    final List<AssetHDto> singleItem = new ArrayList<>();
    singleItem.add(mockAssetsData().get(1));

    // mock ihsquery call
    when(assetService.getAssetByQueryBuilder(any(), any(), any(), any(), any()))
        .thenReturn(mockAssetWithPageResource(singleItem));
    when(assetService.getAssetDetailsByAsset(any(), anyString())).thenReturn(mockAssetDetailData());
    when(serviceService.getServicesForAsset(anyLong())).thenReturn(mockServiceData());
    when(taskService.getTasksByService(anyLong(), any(), isNull(Integer.class), isNull(Integer.class)))
        .thenReturn(mockTaskData());
    when(coCService.getCoCForExport(anyLong())).thenReturn(mockCoCDataWithDefect());
    when(actionableItemService.getActionableItems(anyLong(), Mockito.any())).thenReturn(mockActionableItemData());
    when(assetNoteService.getAssetNote(isNull(Integer.class), isNull(Integer.class), anyLong()))
        .thenReturn(mockAssetNoteData());
    when(statutoryFindingService.getStatutories(anyLong(), any(CodicilDefectQueryHDto.class)))
        .thenReturn(mockStatutoryFindingData());
    when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString())).thenReturn("token");

    // call ihs function
    final StringResponse response = assetExportService.downloadAssetInfo(query, mockUser());

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   * Returns the list of mock asset for LRV and IHS inclusion and exclusion testing.
   *
   * @return the list of mock asset for LRV and IHS inclusion and exclusion testing.
   */
  public final List<AssetHDto> mockAssetsDataForInclusionExclusionTest() {
    final List<AssetHDto> assets = mockAssetsData();
    assets.remove(2);

    return assets;
  }

  /**
   * Returns the mock asset data.
   *
   * @return list of assets.
   */
  public final List<AssetHDto> mockAssetsData() {
    final List<AssetHDto> assets = new ArrayList<>();
    final AssetHDto data1 = new AssetHDto();
    data1.setId(1L);
    data1.setName("v2 LADY K II");
    data1.setIsLead(false);
    data1.setLeadImo("1000019");
    data1.setImoNumber("1000019");
    data1.setAssetType(new AssetTypeDto());
    data1.getAssetType().setName("ABC TYPE");
    data1.setClassStatusDto(new ClassStatusHDto());
    data1.getClassStatusDto().setName("In Class (Laid Up)");
    data1.setBuildDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setFlagStateDto(new FlagStateHDto());
    data1.getFlagStateDto().setName("Jersey");
    data1.setAssetLifeCycleStatusDto(new AssetLifeCycleStatusDto());
    data1.getAssetLifeCycleStatusDto().setName("In service");
    data1.setGrossTonnage(2.1D);
    data1.setIhsAssetDto(new IhsAssetDetailsDto());
    data1.getIhsAssetDto().setId("1000019");
    data1.setEffectiveDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    data1.getClassMaintenanceStatusDto().setName("Dual");
    data1.setCoClassificationSocietyDto(new ClassificationSocietyHDto());
    data1.getCoClassificationSocietyDto().setName("American Bureau Of Shipping");
    data1.getCoClassificationSocietyDto().setId(2L);
    data1.getCoClassificationSocietyDto().setCode("ABS");
    data1.setDueStatusH(DueStatus.DUE_SOON);
    // Test special character
    data1.setClassNotation("\"&'<>¡¢£¤¥ ¦§¨©ª«¬®¯°± ²³´µ¶·¸¹º»¼½¾¿À ÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ ×ØÙÚÛÜÝÞßàáâãäåæ çèéêëìíîïðñ"
        + "òóôõö÷øùúûüýþÿŒœŠš Ÿƒˆ˜ΑΒΓΔΕΖΗΘΙΚΛΜΝΞ ΟΠΡΣΤΥΦΧΨΩαβγδ εζ ηθικλμνξοπρ ςστυφχψ ωϑϒϖ–—‘’‚“”„†‡•…‰′ ″‹›‾⁄€ℑ℘ℜ™ℵ"
        + "←↑→ ↓↔↵⇐⇑⇒⇓⇔∀∂ ∃∅∇∈∉∋∏∑−∗√∝∞∠∧∨∩ ∪∫∴∼≅≈≠≡≤≥⊂⊃⊄⊆⊇⊕⊗⊥⋅⌈⌉⌊ ⌋⟨⟩◊♠♣♥♦\u00A6\u00AC\u007C");
    // Mast has the customer information
    data1.setCustomersH(new ArrayList<>(5));
    data1.setHasPostponedService(true);
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("DDDD");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    data1.setIhsAssetDto(ihsAssetDetailsDto);

    final CustomerLinkHDto registeredOwner = new CustomerLinkHDto();
    registeredOwner.setPartyRoles(new PartyRoleHDto());
    registeredOwner.setParty(new CustomerLinkDto());
    registeredOwner.getPartyRoles().setId(MastPartyRole.SHIP_OWNER.getValue());
    registeredOwner.getParty().setCustomer(new PartyDto());
    registeredOwner.getParty().getCustomer().setName("MAST Registered Owner");
    registeredOwner.getParty().getCustomer().setEmailAddress("registeredowner@mast.com");
    registeredOwner.getParty().getCustomer().setPhoneNumber("1111111111");
    registeredOwner.getParty().getCustomer().setAddressLine1("Address Line 1");
    registeredOwner.getParty().getCustomer().setAddressLine3("Address Line 3");
    registeredOwner.getParty().getCustomer().setCity("New York");
    registeredOwner.getParty().getCustomer().setCountry("US");
    registeredOwner.getParty().getCustomer().setPostcode("13654");
    data1.getCustomersH().add(registeredOwner);

    final CustomerLinkHDto groupOwner = new CustomerLinkHDto();
    groupOwner.setPartyRoles(new PartyRoleHDto());
    groupOwner.setParty(new CustomerLinkDto());
    groupOwner.getPartyRoles().setId(MastPartyRole.GROUP_OWNER.getValue());
    groupOwner.getParty().setCustomer(new PartyDto());
    groupOwner.getParty().getCustomer().setName("MAST Group Owner");
    groupOwner.getParty().getCustomer().setEmailAddress("groupowner@mast.com");
    groupOwner.getParty().getCustomer().setPhoneNumber("1111111111");
    groupOwner.getParty().getCustomer().setAddressLine1("Address Line 1");
    groupOwner.getParty().getCustomer().setAddressLine2("Address Line 2");
    data1.getCustomersH().add(groupOwner);

    final CustomerLinkHDto shipManager = new CustomerLinkHDto();
    shipManager.setPartyRoles(new PartyRoleHDto());
    shipManager.setParty(new CustomerLinkDto());
    shipManager.getPartyRoles().setId(MastPartyRole.SHIP_MANAGER.getValue());
    shipManager.getParty().setCustomer(new PartyDto());
    shipManager.getParty().getCustomer().setName("MAST Ship Manager");
    shipManager.getParty().getCustomer().setEmailAddress("shipmanager@mast.com");
    shipManager.getParty().getCustomer().setPhoneNumber("1111111111");
    shipManager.getParty().getCustomer().setAddressLine1("Address Line 1");
    shipManager.getParty().getCustomer().setAddressLine2("Address Line 2");
    data1.getCustomersH().add(shipManager);

    final CustomerLinkHDto shipOperator = new CustomerLinkHDto();
    shipOperator.setPartyRoles(new PartyRoleHDto());
    shipOperator.setParty(new CustomerLinkDto());
    shipOperator.getPartyRoles().setId(MastPartyRole.SHIP_OPERATOR.getValue());
    shipOperator.getParty().setCustomer(new PartyDto());
    shipOperator.getParty().getCustomer().setName("MAST Ship Operator");
    shipOperator.getParty().getCustomer().setEmailAddress("shipoperator@mast.com");
    shipOperator.getParty().getCustomer().setPhoneNumber("1111111111");
    shipOperator.getParty().getCustomer().setAddressLine1("Address Line 1");
    shipOperator.getParty().getCustomer().setAddressLine2("Address Line 2");
    data1.getCustomersH().add(shipOperator);

    final CustomerLinkHDto docCompany = new CustomerLinkHDto();
    docCompany.setPartyRoles(new PartyRoleHDto());
    docCompany.setParty(new CustomerLinkDto());
    docCompany.getPartyRoles().setId(MastPartyRole.DOC_COMPANY.getValue());
    docCompany.getParty().setCustomer(new PartyDto());
    docCompany.getParty().getCustomer().setName("MAST Doc Company");
    docCompany.getParty().getCustomer().setEmailAddress("doccompany@mast.com");
    docCompany.getParty().getCustomer().setPhoneNumber("1111111111");
    docCompany.getParty().getCustomer().setAddressLine1("Address Line 1");
    docCompany.getParty().getCustomer().setAddressLine2("Address Line 2");
    data1.getCustomersH().add(docCompany);
    assets.add(data1);

    // data2
    final AssetHDto data2 = new AssetHDto();
    data2.setId(2L);
    data2.setName("v2 LADY K II - 2");
    data2.setIsLead(true);
    data2.setLeadImo("1000020");
    data2.setAssetType(new AssetTypeHDto());
    data2.getAssetType().setName("ABC TYPE");
    data2.setClassStatusDto(new ClassStatusHDto());
    data2.getClassStatusDto().setName("COMPLETED");
    data2.setIsMMSService(false);
    data2.setBuildDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setFlagStateDto(new FlagStateHDto());
    data2.getFlagStateDto().setName("Jersey");
    data2.setGrossTonnage(2.1D);
    data2.setIhsAssetDto(new IhsAssetDetailsDto());
    data2.getIhsAssetDto().setId("1000020");
    data2.setEffectiveDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    data2.getClassMaintenanceStatusDto().setName("Single");
    data2.setCoClassificationSocietyDto(new ClassificationSocietyHDto());
    data2.getCoClassificationSocietyDto().setName("American Bureau Of Shipping");
    data2.getCoClassificationSocietyDto().setId(2L);
    data2.setDueStatusH(DueStatus.IMMINENT);
    data2.setRestricted(Boolean.TRUE);
    assets.add(data2);

    // data3
    final AssetHDto data3 = new AssetHDto();
    data3.setId(3L);
    data3.setName("v2 LADY K II - 3");
    data3.setIsLead(true);
    data3.setLeadImo("1000020");
    data3.setAssetType(new AssetTypeDto());
    data3.getAssetType().setName("ABC TYPE");
    data3.setClassStatusDto(new ClassStatusHDto());
    data3.getClassStatusDto().setName("COMPLETED");
    data3.setIsMMSService(false);
    data3.setBuildDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data3.setFlagStateDto(new FlagStateHDto());
    data3.getFlagStateDto().setName("Jersey");
    data3.setGrossTonnage(2.1D);
    data3.setEffectiveDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data3.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    data3.getClassMaintenanceStatusDto().setName("Single");
    data3.setCoClassificationSocietyDto(new ClassificationSocietyHDto());
    data3.getCoClassificationSocietyDto().setName("American Bureau Of Shipping");
    data3.getCoClassificationSocietyDto().setId(2L);
    data3.setDueStatusH(DueStatus.OVER_DUE);
    assets.add(data3);
    return assets;
  }


  /**
   * Returns the mocked asset data with page resource.
   *
   * @param assets the list of asset details.
   * @return the mocked asset data with page resource.
   */
  public final AssetPageResource mockAssetWithPageResource(final List<AssetHDto> assets) {
    final AssetPageResource resource = new AssetPageResource();
    resource.setContent(assets);
    resource.setPagination(new PaginationDto());

    return resource;
  }

  /**
   * Returns the mocked asset data with multi-asset page resource.
   *
   * @return the mocked asset data with multi-asset page resource.
   */
  protected final MultiAssetPageResourceDto mockEmptyMultiAssetWithPageResource() {
    final MultiAssetPageResourceDto resource = new MultiAssetPageResourceDto();
    resource.setContent(null);
    resource.setPagination(new PaginationDto());
    return resource;
  }

  /**
   * Returns the mocked asset detail data.
   *
   * @return the mocked asset detail data.
   */
  public final AssetDetailsDto mockAssetDetailData() {
    final AssetDetailsDto asset = new AssetDetailsDto();

    asset.setRegistryInformation(new RegistryInformationDto());
    asset.getRegistryInformation().setPortOfRegistry("KL Port");
    asset.getRegistryInformation().setCallSign("KL Call Sign");
    asset.getRegistryInformation().setOfficialNumber("010-987-001");
    asset.getRegistryInformation().setAssetTypeDetails("LNG Tanker");
    asset.getRegistryInformation().setFormerAssetNames("Princess Tanya, Princess Tanya, Princess Tanya");
    asset.getRegistryInformation().setBuilder("A builder");
    asset.getRegistryInformation().setYard(null);
    asset.getRegistryInformation().setYardNumber("y1");
    asset.getRegistryInformation().setKeelLayingDate("01/09/2016");
    asset.getRegistryInformation().setDateOfBuild("01/11/1111");
    asset.getRegistryInformation().setYearOfBuild("1111");
    asset.getRegistryInformation().setCountryOfBuild("MY");
    asset.getRegistryInformation().setFlag("flag 1");
    asset.getRegistryInformation().setMmsiNumber("0123456789");

    asset.setRulesetDetailsDto(new RulesetDetailsDto());
    asset.getRulesetDetailsDto().setHullIndicator(10);
    asset.getRulesetDetailsDto().setMachineryNotation("No notation");
    asset.getRulesetDetailsDto().setDescriptiveNote("ruleset note");

    asset.setEquipmentDetails(new EquipmentDetailsDto());
    asset.getEquipmentDetails().setCableLength(2.1D);
    asset.getEquipmentDetails().setCableGrade("Grade A");
    asset.getEquipmentDetails().setCableDiameter("10m");
    asset.getEquipmentDetails().setEquipmentLetter("Equip letter");

    asset.setPrincipalDimension(new PrincipalDimensionDto());
    asset.getPrincipalDimension().setLengthOverall(2.1D);
    asset.getPrincipalDimension().setLengthBetweenPerpendiculars("3m");
    asset.getPrincipalDimension().setBreadthMoulded(2.1D);
    asset.getPrincipalDimension().setBreadthExtreme(2.1D);
    asset.getPrincipalDimension().setDraughtMax(2.1D);
    asset.getPrincipalDimension().setDepthMoulded(2.1D);
    asset.getPrincipalDimension().setGrossTonnage(10);
    asset.getPrincipalDimension().setNetTonnage(10);
    asset.getPrincipalDimension().setDeadWeight(10);
    asset.getPrincipalDimension().setDecks("this deck");
    asset.getPrincipalDimension().setPropulsion("this propulsion");

    asset.setOwnership(new OwnershipDto());
    asset.getOwnership().setRegisteredOwner(new UserPersonalInformationDto());
    asset.getOwnership().getRegisteredOwner().setName("My Name");
    asset.getOwnership().getRegisteredOwner().setPhoneNumber("012-345-6789");
    asset.getOwnership().getRegisteredOwner().setEmailAddress("myname@myname.com");
    asset.getOwnership().getRegisteredOwner().setAddress("10 my street, my country");

    asset.getOwnership().setGroupOwner(new UserPersonalInformationDto());
    asset.getOwnership().getGroupOwner().setName("My Name");
    asset.getOwnership().getGroupOwner().setPhoneNumber("012-345-6789");
    asset.getOwnership().getGroupOwner().setEmailAddress("myname@myname.com");
    asset.getOwnership().getGroupOwner().setAddress("10 my street, my country");

    asset.getOwnership().setShipManager(new UserPersonalInformationDto());
    asset.getOwnership().getShipManager().setName("My Name");
    asset.getOwnership().getShipManager().setPhoneNumber("012-345-6789");
    asset.getOwnership().getShipManager().setEmailAddress("myname@myname.com");
    asset.getOwnership().getShipManager().setAddress("10 my street, my country");

    asset.getOwnership().setOperator(new UserPersonalInformationDto());
    asset.getOwnership().getOperator().setName("My Name");
    asset.getOwnership().getOperator().setPhoneNumber("012-345-6789");
    asset.getOwnership().getOperator().setEmailAddress("myname@myname.com");
    asset.getOwnership().getOperator().setAddress("10 my street, my country");

    asset.getOwnership().setDoc(new UserPersonalInformationDto());
    asset.getOwnership().getDoc().setName("My Name");
    asset.getOwnership().getDoc().setPhoneNumber("012-345-6789");
    asset.getOwnership().getDoc().setEmailAddress("myname@myname.com");
    asset.getOwnership().getDoc().setAddress("10 my street, my country");

    return asset;
  }

  /**
   * Returns the mocked list of services data.
   *
   * @return the mocked list of services data.
   */
  public final List<ScheduledServiceHDto> mockServiceData() {
    final List<ScheduledServiceHDto> services = new ArrayList<>();

    // data1
    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setId(4126718L);
    service1.setDueDate(null);
    service1.setLowerRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service1.setUpperRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service1.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service1.setDueStatusH(DueStatus.DUE_SOON);
    service1.setServiceCatalogueH(new ServiceCatalogueHDto());
    service1.getServiceCatalogueH().setName("Service");
    service1.getServiceCatalogueH().setCode("AAA");
    service1.getServiceCatalogueH().setContinuousIndicator(true);
    service1.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service1.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service1.setServiceStatus(new LinkResource(ServiceStatus.NOT_STARTED.getValue()));
    service1.getServiceCatalogueH().getProductCatalogue().setDisplayOrder(1);
    service1.getServiceCatalogueH().setDisplayOrder(1);
    service1.getServiceCatalogueH().setWorkItemType(new LinkResource(WorkItemType.TASK.value()));
    service1.setActive(Boolean.TRUE);
    service1.setOccurrenceNumber(5);
    ServiceCreditStatusHDto serviceCreditStatus = new ServiceCreditStatusHDto();
    serviceCreditStatus.setId(1L);
    service1.setServiceCreditStatusH(serviceCreditStatus);
    services.add(service1);

    // data2
    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    service2.setId(2L);
    service2.setDueDate(null);
    service2.setLowerRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service2.setUpperRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service2.setDueStatusH(DueStatus.DUE_SOON);
    service2.setServiceCatalogueH(new ServiceCatalogueHDto());
    service2.getServiceCatalogueH().setName("Service");
    service2.getServiceCatalogueH().setCode("BBB");
    service2.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service2.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service2.getServiceCatalogueH().setContinuousIndicator(false);
    service2.getServiceCatalogueH().getProductCatalogue().setDisplayOrder(1);
    service2.setServiceStatus(new LinkResource(ServiceStatus.PART_HELD.getValue()));
    service2.getServiceCatalogueH().setWorkItemType(new LinkResource(WorkItemType.TASK.value()));
    service2.getServiceCatalogueH().setDisplayOrder(1);
    service2.setActive(Boolean.TRUE);
    service2.setOccurrenceNumber(4);
    ServiceCreditStatusHDto serviceCreditStatus2 = new ServiceCreditStatusHDto();
    serviceCreditStatus2.setId(2L);
    service2.setServiceCreditStatusH(serviceCreditStatus2);
    services.add(service2);

    // data3
    final ScheduledServiceHDto service3 = new ScheduledServiceHDto();
    service3.setId(3L);
    service3.setDueDate(null);
    service3.setLowerRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service3.setUpperRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service3.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service3.setDueStatusH(DueStatus.DUE_SOON);
    service3.setServiceCatalogueH(new ServiceCatalogueHDto());
    service3.getServiceCatalogueH().setName("Service");
    service3.getServiceCatalogueH().setCode("CCC");
    service3.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service3.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service3.getServiceCatalogueH().getProductCatalogue().setDisplayOrder(1);
    service3.setPostponementTypeH(new PostponementTypeHDto());
    service3.getPostponementTypeH().setName("=Conditionally Approved");
    service3.getServiceCatalogueH().setWorkItemType(new LinkResource(WorkItemType.CHECKLIST.value()));
    service3.setServiceStatus(new LinkResource(ServiceStatus.PART_HELD.getValue()));
    service3.getServiceCatalogueH().setDisplayOrder(1);
    service3.getServiceCatalogueH().setContinuousIndicator(false);
    service3.setActive(Boolean.TRUE);
    service3.setOccurrenceNumber(2);
    ServiceCreditStatusHDto serviceCreditStatus3 = new ServiceCreditStatusHDto();
    serviceCreditStatus3.setId(2L);
    service3.setServiceCreditStatusH(serviceCreditStatus3);
    services.add(service3);


    // data4
    final ScheduledServiceHDto service4 = new ScheduledServiceHDto();
    service4.setId(4L);
    service4.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service4.setLowerRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service4.setUpperRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service4.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service4.setDueStatusH(DueStatus.DUE_SOON);
    service4.setServiceCatalogueH(new ServiceCatalogueHDto());
    service4.getServiceCatalogueH().setName("Second Machinery service 1 ");
    service4.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service4.getServiceCatalogueH().getProductCatalogue().setName("Second Machinery");
    service4.getServiceCatalogueH().getProductCatalogue().setDisplayOrder(2);
    service4.setPostponementTypeH(new PostponementTypeHDto());
    service4.getPostponementTypeH().setName("Conditionally Approved");
    service4.getServiceCatalogueH().setContinuousIndicator(false);
    service4.setServiceStatus(new LinkResource(ServiceStatus.PART_HELD.getValue()));
    service4.getServiceCatalogueH().setWorkItemType(new LinkResource(WorkItemType.CHECKLIST.value()));
    service4.getServiceCatalogueH().setDisplayOrder(1);
    service4.setActive(Boolean.TRUE);
    service4.setOccurrenceNumber(2);
    services.add(service4);


    // data5
    final ScheduledServiceHDto service5 = new ScheduledServiceHDto();
    service5.setId(5L);
    service5.setDueDate(null);
    service5.setLowerRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service5.setUpperRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service5.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service5.setDueStatusH(DueStatus.DUE_SOON);
    service5.setServiceCatalogueH(new ServiceCatalogueHDto());
    service5.getServiceCatalogueH().setName("Second Machinery service 2");
    service5.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service5.getServiceCatalogueH().getProductCatalogue().setName("Second Machinery");
    service5.getServiceCatalogueH().getProductCatalogue().setDisplayOrder(1);
    service5.setPostponementTypeH(new PostponementTypeHDto());
    service5.getPostponementTypeH().setName("Conditionally Approved");
    service5.setServiceStatus(new LinkResource(ServiceStatus.PART_HELD.getValue()));
    service5.getServiceCatalogueH().setDisplayOrder(1);
    service5.getServiceCatalogueH().setWorkItemType(new LinkResource(WorkItemType.CHECKLIST.value()));
    service5.setOccurrenceNumber(null);
    service5.getServiceCatalogueH().setContinuousIndicator(false);
    service5.setActive(Boolean.TRUE);
    services.add(service5);
    // data6
    final ScheduledServiceHDto service6 = new ScheduledServiceHDto();
    service6.setId(5L);
    service6.setDueDate(null);
    service6.setLowerRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service6.setUpperRangeDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service6.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    service6.setDueStatusH(DueStatus.DUE_SOON);
    service6.setServiceCatalogueH(new ServiceCatalogueHDto());
    service6.getServiceCatalogueH().setName("Second Machinery service 2");
    service6.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service6.getServiceCatalogueH().getProductCatalogue().setName("Second Machinery");
    service6.getServiceCatalogueH().getProductCatalogue().setDisplayOrder(1);
    service6.setPostponementTypeH(new PostponementTypeHDto());
    service6.getPostponementTypeH().setName("Conditionally Approved");
    service6.setServiceStatus(new LinkResource(ServiceStatus.NOT_STARTED.getValue()));
    service6.getServiceCatalogueH().setDisplayOrder(1);
    service6.getServiceCatalogueH().setContinuousIndicator(false);
    service6.setOccurrenceNumber(null);
    service6.setActive(Boolean.TRUE);
    services.add(service6);

    return services;
  }

  /**
   * Returns the mocked task data.
   *
   * @return the mocked task data.
   */
  public final TaskPageResource mockTaskData() {
    final TaskPageResource pageResource = new TaskPageResource();
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final Map<Long, WorkItemLightHDto> taskMap = new HashMap<Long, WorkItemLightHDto>();
    // data 1
    final WorkItemLightHDto data1 = new WorkItemLightHDto();
    data1.setName("Task 1");
    data1.setTaskNumber("000001-ADFGOS-000001");
    data1.setResolutionStatus(new LinkResource(1L));
    data1.setAssignedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    tasks.add(data1);
    // data 2
    final WorkItemLightHDto data2 = new WorkItemLightHDto();
    data2.setName("Task 2");
    data2.setTaskNumber("000001-ADFGOS-000002");
    data2.setResolutionStatus(new LinkResource(2L));
    data2.setAssignedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    tasks.add(data2);

    pageResource.setContent(tasks);
    pageResource.setPagination(new PaginationDto());

    return pageResource;
  }


  /**
   * Returns the mocked task data.
   *
   * @return the mocked task data.
   */
  public final Map<Long, List<WorkItemLightHDto>> mockTaskDataForAssetFullExport() {
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final Map<Long, List<WorkItemLightHDto>> taskMap = new HashMap<Long, List<WorkItemLightHDto>>();
    // data 1
    final WorkItemLightHDto data1 = new WorkItemLightHDto();
    data1.setName("Task 1");
    data1.setLongDescription("Task 1 Test");
    data1.setTaskNumber("000001-ADFGOS-000001");
    data1.setResolutionStatus(new LinkResource(1L));
    data1.setAssignedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    tasks.add(data1);
    // data 2
    final WorkItemLightHDto data2 = new WorkItemLightHDto();
    data2.setName("Task 2");
    data2.setLongDescription("Task 2 Test");
    data2.setTaskNumber("000001-ADFGOS-000002");
    data2.setResolutionStatus(new LinkResource(2L));
    data2.setAssignedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setPostponementDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    tasks.add(data2);
    taskMap.put(1L, tasks);

    return taskMap;
  }

  /**
   * Returns the mocked coc data.
   *
   * @return the mocked coc data.
   */
  public final List<CoCHDto> mockCoCData() {
    final List<CoCHDto> cocs = new ArrayList<>();

    // data1
    final CoCHDto data1 = new CoCHDto();
    data1.setId(1L);
    data1.setTitle("Condition of Class 1");
    data1.setCategoryH(new CodicilCategoryHDto());
    data1.getCategoryH().setName("Class");
    data1.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setStatusH(new CodicilStatusHDto());
    data1.getStatusH().setName("Draft");
    data1.getStatusH().setId(CodicilStatus.COC_OPEN.getValue());
    data1.setNarrative("This is a type of codicil");
    data1.setReferenceCode("test");
    data1.setJob(new LinkResource(1L));
    cocs.add(data1);

    // data2
    final CoCHDto data2 = new CoCHDto();
    data2.setId(2L);
    data2.setTitle("Condition of Class 2");
    data2.setCategoryH(new CodicilCategoryHDto());
    data2.getCategoryH().setName("Class");
    data2.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setDueDate(new GregorianCalendar(YEAR_2017, 2, 1).getTime());
    data2.setStatusH(new CodicilStatusHDto());
    data2.getStatusH().setName("Draft");
    data2.getStatusH().setId(CodicilStatus.COC_OPEN.getValue());
    data2.setNarrative("This is a type of codicil");
    data2.setJob(new LinkResource(1L));
    data2.setReferenceCode("test2");
    cocs.add(data2);

    return cocs;
  }

  /**
   * Returns the mocked coc with defect data.
   *
   * @return the mocked coc with defect data.
   */
  public final List<CoCHDto> mockCoCDataWithDefect() {
    final List<CoCHDto> resultList = new ArrayList<>();
    final CoCHDto coc1 = new CoCHDto();
    coc1.setId(1L);
    coc1.setTitle("Condition of Class 1");
    coc1.setCategoryH(new CodicilCategoryHDto());
    coc1.getCategoryH().setName("Class");
    coc1.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    coc1.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    coc1.setStatusH(new CodicilStatusHDto());
    coc1.getStatusH().setName("Draft");
    coc1.setNarrative("This is a type of codicil");
    coc1.setJob(new LinkResource(1L));
    JobHDto job = new JobHDto();
    job.setJobNumber("1234");
    coc1.setJobH(job);
    coc1.setDefectH(new DefectHDto());
    coc1.getDefectH().setTitle("Defect 1");
    coc1.getDefectH().setCategory(new FaultCategoryDto());
    coc1.getDefectH().getCategory().setName("Hull");
    coc1.getDefectH().setIncidentDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    coc1.getDefectH().setDefectStatusH(new DefectStatusHDto());
    coc1.getDefectH().getDefectStatusH().setName("Open");
    coc1.getDefectH().setJobs(new ArrayList<JobFaultDto>());

    final JobFaultDto coc1Job1 = new JobFaultDto();
    coc1Job1.setJob(new LinkResource(1L));
    coc1.getDefectH().getJobs().add(coc1Job1);
    final JobFaultDto coc1Job2 = new JobFaultDto();
    coc1Job2.setJob(new LinkResource(2L));
    coc1.getDefectH().getJobs().add(coc1Job2);

    resultList.add(coc1);


    final CoCHDto coc2 = new CoCHDto();
    coc2.setId(2L);
    coc2.setTitle("Condition of Class 2");
    coc2.setCategoryH(new CodicilCategoryHDto());
    coc2.getCategoryH().setName("Class");
    coc2.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    coc2.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    coc2.setStatusH(new CodicilStatusHDto());
    coc2.getStatusH().setName("Draft");
    coc2.setNarrative("This is a type of codicil");
    coc2.setJob(new LinkResource(1L));
    coc2.setJobH(job);
    coc2.setDefectH(new DefectHDto());
    coc2.getDefectH().setTitle("Defect 1");
    coc2.getDefectH().setCategory(new FaultCategoryDto());
    coc2.getDefectH().getCategory().setName("Hull");
    coc2.getDefectH().setIncidentDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    coc2.getDefectH().setDefectStatusH(new DefectStatusHDto());
    coc2.getDefectH().getDefectStatusH().setName("Open");
    coc2.getDefectH().setJobs(new ArrayList<JobFaultDto>());
    List<JobHDto> jobs = new ArrayList<JobHDto>();
    jobs.add(job);
    coc2.getDefectH().setJobsH(jobs);

    final JobFaultDto coc2Job1 = new JobFaultDto();
    coc2Job1.setJob(new LinkResource(1L));
    coc2.getDefectH().getJobs().add(coc2Job1);
    final JobFaultDto coc2Job2 = new JobFaultDto();
    coc2Job2.setJob(new LinkResource(2L));
    coc2.getDefectH().getJobs().add(coc2Job2);
    resultList.add(coc2);

    return resultList;
  }

  /**
   * Returns the mocked actionable item data.
   *
   * @return the mocked actionable item data.
   */
  public final List<ActionableItemHDto> mockActionableItemData() {
    final List<ActionableItemHDto> actionableItems = new ArrayList<>();

    // data1
    final ActionableItemHDto data1 = new ActionableItemHDto();
    data1.setTitle("Actionable Item 1");
    data1.setId(6L);
    data1.setCategoryH(new CodicilCategoryHDto());
    data1.getCategoryH().setName("Class");
    data1.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setStatusH(new CodicilStatusHDto());
    data1.getStatusH().setName("Draft");
    data1.getStatusH().setId(3L);
    data1.setNarrative("This is a type of codicil");
    data1.setAssetItemH(new LazyItemHDto());
    data1.getAssetItemH().setName("v2 ROU PLATFORM ASSET MODEL");
    data1.setJob(new LinkResource(1L));
    data1.setReferenceCode("test1");
    actionableItems.add(data1);

    // data2
    final ActionableItemHDto data2 = new ActionableItemHDto();
    data2.setId(2L);
    data2.setTitle("Actionable Item 2");
    data2.setCategoryH(new CodicilCategoryHDto());
    data2.getCategoryH().setName("Class");
    data2.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setStatusH(new CodicilStatusHDto());
    data2.getStatusH().setName("Draft");
    data2.getStatusH().setId(4L);
    data2.setJob(new LinkResource(5L));
    data2.setNarrative("This is a type of codicil");
    data2.setReferenceCode("test2");
    actionableItems.add(data2);

    // data3
    final ActionableItemHDto data3 = new ActionableItemHDto();
    data3.setId(3L);
    data3.setTitle("Actionable Item 2");
    data3.setCategoryH(new CodicilCategoryHDto());
    data3.getCategoryH().setName("Class");
    data3.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data3.setDueDate(new GregorianCalendar(YEAR_2017, 4, 1).getTime());
    data3.setStatusH(new CodicilStatusHDto());
    data3.getStatusH().setName("Draft");
    data3.getStatusH().setId(4L);
    data3.setNarrative("This is a type of codicil");
    data3.setJob(new LinkResource(6L));
    data3.setReferenceCode("test3");
    actionableItems.add(data3);

    // data3
    final ActionableItemHDto data4 = new ActionableItemHDto();
    data4.setId(4L);
    data4.setTitle("Actionable Item 4");
    data4.setCategoryH(new CodicilCategoryHDto());
    data4.getCategoryH().setName("Class");
    data4.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data4.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data4.setStatusH(new CodicilStatusHDto());
    data4.getStatusH().setName("Draft");
    data4.getStatusH().setId(3L);
    data4.setNarrative("This is a type of codicil");
    data4.setJob(new LinkResource(8L));
    data4.setReferenceCode("test4");
    actionableItems.add(data4);
    return actionableItems;
  }

  /**
   * Returns the mocked asset note data.
   *
   * @return the mocked asset note data.
   */
  public final AssetNotePageResource mockAssetNoteData() {
    final AssetNotePageResource pageResource = new AssetNotePageResource();
    final List<AssetNoteHDto> assetNotes = new ArrayList<>();

    // data1
    final AssetNoteHDto data1 = new AssetNoteHDto();
    data1.setId(1L);
    data1.setTitle("Asset Note 1");
    data1.setCategoryH(new CodicilCategoryHDto());
    data1.getCategoryH().setName("Class");
    data1.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setStatusH(new CodicilStatusHDto());
    data1.getStatusH().setName("Draft");
    data1.getStatusH().setId(10L);
    data1.setNarrative("This is a type of codicil");
    data1.setAssetItemH(new LazyItemHDto());
    data1.getAssetItemH().setName("v2 ROU PLATFORM ASSET MODEL");
    data1.setJob(new LinkResource(1L));
    data1.setReferenceCode("test");
    assetNotes.add(data1);

    // data2
    final AssetNoteHDto data2 = new AssetNoteHDto();
    data2.setId(2L);
    data2.setTitle("Asset Note 2");
    data2.setCategoryH(new CodicilCategoryHDto());
    data2.getCategoryH().setName("Class");
    data2.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setStatusH(new CodicilStatusHDto());
    data2.getStatusH().setName("Draft");
    data2.getStatusH().setId(10L);
    data2.setNarrative("This is a type of codicil");
    data2.setJob(new LinkResource(1L));
    data2.setReferenceCode("test22");
    assetNotes.add(data2);

    // data3
    final AssetNoteHDto data3 = new AssetNoteHDto();
    data3.setId(6L);
    data3.setTitle("Asset Note 2");
    data3.setCategoryH(new CodicilCategoryHDto());
    data3.getCategoryH().setName("Class");
    data3.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data3.setDueDate(new GregorianCalendar(YEAR_2017, 4, 1).getTime());
    data3.setStatusH(new CodicilStatusHDto());
    data3.getStatusH().setName("Draft");
    data3.getStatusH().setId(14L);
    data3.setNarrative("This is a type of codicil");
    data3.setJob(new LinkResource(1L));
    assetNotes.add(data3);

    // data3
    final AssetNoteHDto data4 = new AssetNoteHDto();
    data4.setId(3L);
    data4.setTitle("Asset Note 2");
    data4.setCategoryH(new CodicilCategoryHDto());
    data4.getCategoryH().setName("Class");
    data4.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data4.setDueDate(new GregorianCalendar(YEAR_2017, 2, 1).getTime());
    data4.setStatusH(new CodicilStatusHDto());
    data4.getStatusH().setName("Draft");
    data4.getStatusH().setId(14L);
    data4.setNarrative("This is a type of codicil");
    data4.setJob(new LinkResource(1L));
    assetNotes.add(data4);
    pageResource.setPagination(new PaginationDto());
    pageResource.setContent(assetNotes);
    return pageResource;
  }

  /**
   * Returns the mocked statutory finding data.
   *
   * @return the mocked statutory finding data.
   */
  public final List<StatutoryFindingHDto> mockStatutoryFindingData() {

    final StatutoryFindingHDto data1 = new StatutoryFindingHDto();
    data1.setId(1L);
    data1.setTitle("sf 1");
    data1.setDescription("sf desc 1");
    data1.setNarrative("sf narrative 1");
    data1.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data1.setDueDate(new GregorianCalendar(YEAR_2017, 4, 1).getTime());
    data1.setStatus(new LinkResource(CodicilStatus.SF_OPEN.getValue()));
    data1.setStatusH(new CodicilStatusHDto());
    data1.getStatusH().setName("Draft");
    data1.setJob(new LinkResource(1L));
    data1.getStatusH().setId(23L);
    data1.setReferenceCode("test");

    final StatutoryFindingHDto data2 = new StatutoryFindingHDto();
    data2.setId(5L);
    data2.setTitle("sf 2");
    data2.setDescription("sf desc 2");
    data2.setNarrative("sf narrative 2");
    data2.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data2.setStatus(new LinkResource(CodicilStatus.SF_OPEN.getValue()));
    data2.setStatusH(new CodicilStatusHDto());
    data2.getStatusH().setName("Draft");
    data2.setJob(new LinkResource(1L));
    data2.getStatusH().setId(23L);
    data2.setReferenceCode("test1");

    final StatutoryFindingHDto data3 = new StatutoryFindingHDto();
    data3.setId(2L);
    data3.setTitle("sf 3");
    data3.setDescription("sf desc 3");
    data3.setNarrative("sf narrative 3");
    data3.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data3.setDueDate(new GregorianCalendar(YEAR_2017, 2, 1).getTime());
    data3.setStatusH(new CodicilStatusHDto());
    data3.setStatus(new LinkResource(CodicilStatus.SF_OPEN.getValue()));
    data3.getStatusH().setName("Draft");
    data3.getStatusH().setId(23L);
    data3.setJob(new LinkResource(4L));
    data3.setReferenceCode("test2");

    final StatutoryFindingHDto data4 = new StatutoryFindingHDto();
    data4.setId(STATUTORY_4);
    data4.setTitle("sf 4");
    data4.setDescription("sf desc 4");
    data4.setNarrative("sf narrative 4");
    data4.setImposedDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data4.setDueDate(new GregorianCalendar(YEAR_2017, 1, 1).getTime());
    data4.setStatus(new LinkResource(CodicilStatus.SF_OPEN.getValue()));
    data4.setStatusH(new CodicilStatusHDto());
    data4.getStatusH().setName("Draft");
    data4.getStatusH().setId(23L);
    data4.setReferenceCode("test2");


    final List<StatutoryFindingHDto> list = new ArrayList<>();
    list.add(data1);
    list.add(data2);
    list.add(data3);
    list.add(data4);

    return list;
  }

  /**
   * Returns the in condition in the MAST query.
   *
   * @param builder the mast query builder.
   * @return the list of query criteria.
   */
  private List<AbstractQueryDto> getQueryInConditions(final MastQueryBuilder builder) {
    return Optional.ofNullable(builder).map(MastQueryBuilder::build).map(AbstractQueryDto::getAnd)
        .map(list -> list.get(0)).map(AbstractQueryDto::getOr).orElse(null);
  }

  /**
   * Returns the not in condition in the MAST query.
   *
   * @param builder the mast query builder.
   * @return the list of query criteria.
   */
  private List<AbstractQueryDto> getQueryNotInConditions(final MastQueryBuilder builder) {
    return Optional.ofNullable(builder).map(MastQueryBuilder::build).map(AbstractQueryDto::getAnd)
        .map(list -> list.get(0)).map(AbstractQueryDto::getNot).map(AbstractQueryDto::getOr).orElse(null);
  }

  /**
   * Mock user profile.
   *
   * @return user profile object.
   */
  public final UserProfiles mockUser() {
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");

    return user;
  }

  /**
   * Converts json to object.
   *
   * @return mapped object.
   * @throws IOException if coping of stream fails.
   */
  public WorkItemPreviewListHDto mockHierarchicalCheckList() throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    WorkItemPreviewListHDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/HeirachyChecklist.json"), writer,
        Charset.defaultCharset());
    content = mapper.readValue(writer.toString(), WorkItemPreviewListHDto.class);
    return content;

  }

  /**
   * mock data for checklist item with service information.
   *
   * @return mapped object.
   */
  public Map<ScheduledServiceHDto, List<WorkItemLightHDto>> mockCheckListHierarchyByService() {

    Map<ScheduledServiceHDto, List<WorkItemLightHDto>> checkListWorkItem =
        new HashMap<ScheduledServiceHDto, List<WorkItemLightHDto>>();
    List<WorkItemLightHDto> checklistItems = new ArrayList<WorkItemLightHDto>();

    WorkItemLightHDto checklistitem1 = new WorkItemLightHDto();
    checklistitem1.setReferenceCode("AAAA195900");
    checklistitem1.setLongDescription("Automatic sprinklers and automatic water mist nozzles ‘basic’"
        + " or ‘extended’ function testing carried out");

    WorkItemLightHDto checklistitem2 = new WorkItemLightHDto();
    checklistitem2.setReferenceCode("AAAA012100");
    checklistitem2.setLongDescription(
        "Line throwing rockets and ships distress flares all found" + " to be satisfactory (SOLAS regs. III/6 & 18)");

    checklistItems.add(checklistitem1);
    checklistItems.add(checklistitem2);

    List<ScheduledServiceHDto> serviceList = mockServiceData();

    checkListWorkItem.put(serviceList.get(0), checklistItems);
    checkListWorkItem.put(serviceList.get(1), checklistItems);
    checkListWorkItem.put(serviceList.get(2), checklistItems);

    return checkListWorkItem;
  }

  /**
   * Mocks Jobs Data.
   *
   * @return call value.
   */
  private JobHDto mockJobsData() {
    JobHDto job = new JobHDto();
    job.setId(1L);
    job.setJobNumber("123");
    return job;

  }

  /**
   * Provides the Spring context for unit test.
   *
   * @author yng
   *
   */
  @Configuration
  @EnableCaching
  @ComponentScan(value = {"com.baesystems.ai.lr.cd.be.utils"})
  public static class Config extends BaseMockConfiguration {

    /**
     * Returns the implementation of {@link AssetExportService}.
     *
     * @return
     */
    @Override
    @Bean
    public AssetExportService assetExportService() {
      return new AssetExportServiceImpl();
    }

    /**
     * Cache Manager.
     *
     * @return cache Manager.
     */
    @Bean
    public SimpleCacheManager cacheManager() {
      final SimpleCacheManager cacheManager = new SimpleCacheManager();
      final List<Cache> caches = new ArrayList<>();
      caches.add(assetsExportCacheBean().getObject());
      cacheManager.setCaches(caches);
      return cacheManager;
    }

    /**
     * The assets export cache factory.
     *
     * @return the assets export cache factory.
     */
    @Bean
    public ConcurrentMapCacheFactoryBean assetsExportCacheBean() {
      ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean();
      cacheFactoryBean.setName(CacheConstants.CACHE_ASSETS_EXPORT);
      return cacheFactoryBean;
    }


  }

}

