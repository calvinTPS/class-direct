package com.baesystems.ai.lr.cd.be.processor.test;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultMessage;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.baesystems.ai.lr.cd.be.processor.AssetCodeParserProcessor;

/**
 * Test AssetCodeParserProcessor class.
 *
 * @author yng
 *
 */
public class AssetCodeParserProcessorTest {

  /**
   * Test processor.
   *
   * @throws Exception value.
   */
  @Test
  public final void processorFlowTest() throws Exception {
    final Exchange exchange = Mockito.mock(Exchange.class);

    final DefaultMessage message = new DefaultMessage();

    Mockito.when(exchange.getIn()).thenReturn(message);

    // test assetId != null
    message.setHeader("assetId", "LRV1");

    final AssetCodeParserProcessor processor = new AssetCodeParserProcessor();
    processor.process(exchange);

    Assert.assertEquals(1L, exchange.getIn().getHeader("assetId"));

    // test assetId == null
    message.removeHeader("assetId");

    processor.process(exchange);
    Assert.assertNull(exchange.getIn().getHeader("assetId"));

    // test assetId is not instance of string
    message.setHeader("assetId", new Object());

    processor.process(exchange);
    Assert.assertFalse(exchange.getIn().getHeader("assetId") instanceof String);
  }
}
