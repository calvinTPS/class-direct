package com.baesystems.ai.lr.cd.be.utils.test;

import static com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator.decodeEncodedJsonToFileToken;
import static com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator.generateCS10Token;
import static com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator.generateS3Token;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import com.baesystems.ai.lr.cd.be.domain.dto.download.FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DownloadTypeEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;

/**
 * Provides Unit test methods for FileTokenGenerator.
 *
 * @author Faizal Sidek
 * @author SBollu.
 */
public class FileTokenGeneratorTest {

  /**
   * The default s3 bucket.
   */
  private static final String DEFAULT_BUCKET = "lr-classdirect";

  /**
   * The default file key.
   */
  private static final String DEFAULT_KEY = "test/sample.pdf";

  /**
   * The default node id.
   */
  private static final Integer DEFAULT_NODE = 10001;

  /**
   * The default version.
   */
  private static final Integer DEFAULT_VERSION = 1;

  /**
   * The default sample s3 token.
   */
  private static final String SAMPLE_ENCODED_S3_TOKEN =
      "MOEUt56Egwx0czcon3549J_ZmpHHcKpOQzFKC6KMtaCzR-lXB5t0jcLCkDckr8hOhY27NTszRK9H14WAAiY8m3li3za7KvPFNONvOeRbWB9AdZK"
          + "QufkB82RkWy2brxof";

  /**
   * The default sample CS10 token.
   */
  private static final String SAMPLE_ENCODED_CS10_TOKEN =
      "XzqvbktgzIeCKD131jY9eCVpcaKEp-1w7CIIqdCKsu8-M-NQpFKIbTm507rJSJmNBOhvcJ3IuO5Y2Ddt0dGYbQ";
  /**
   * The {@link SecurityContext} from test Spring context.
   */
  private SecurityContext securityCtx;

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public void setUp() {
    securityCtx = SecurityContextHolder.getContext();
    SecurityContext context = new SecurityContextImpl();
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    CDAuthToken token = new CDAuthToken(listOfRoles);

    token.setDetails(user);
    token.setAuthenticated(true);
    context.setAuthentication(token);
    SecurityContextHolder.setContext(context);
  }


  /**
   * Destroys context after all tests executed.
   */
  @After
  public void tearDown() {
    SecurityContextHolder.setContext(securityCtx);
  }


  /**
   * Tests success scenario for {@link generateS3Token(String, String)}.encrypts and encodes to a S3
   * string token.
   *
   * @throws ClassDirectException if any padding, algorithm and invalid key exception occurs.
   */
  @Test
  public final void successfullyGenerateDefaultS3Token() throws ClassDirectException {
    final String s3Token = generateS3Token(DEFAULT_BUCKET, DEFAULT_KEY);
    assertNotNull(s3Token);
    assertFalse(s3Token.isEmpty());
  }

  /**
   * Tests success scenario for {@link generateCS10Token(Integer, Integer)}.encrypts and encodes to
   * CS10 string token.
   *
   * @throws ClassDirectException if any padding, algorithm and invalid key exception occurs.
   */
  @Test
  public final void successfullyGenerateCS10Token() throws ClassDirectException {
    final String cs10Token = generateCS10Token(DEFAULT_NODE, DEFAULT_VERSION, "37be2b5b-63ee-4535-bca4-202d556d626e");
    assertNotNull(cs10Token);
    assertFalse(cs10Token.isEmpty());
  }

  /**
   * Tests success scenario for {@link decodeEncodedJsonToFileToken(String)}.decrypts and decodes
   * the string to proper S3 file token.
   *
   * @throws ClassDirectException if any padding, algorithm and invalid key exception occurs.
   */
  @Test
  public final void successfullyDecodeS3Token() throws ClassDirectException {
    final FileToken fileToken =
        decodeEncodedJsonToFileToken(SAMPLE_ENCODED_S3_TOKEN, "37be2b5b-63ee-4535-bca4-202d556d626e");
    assertNotNull(fileToken);
    assertEquals(DownloadTypeEnum.S3, fileToken.getType());
    assertNotNull(fileToken.getS3Token());
    // decrypted bucketName should be equal to DEFAULT_BUCKET
    assertEquals(DEFAULT_BUCKET, fileToken.getS3Token().getBucketName());
    // decrypted key should be equal to DEFAULT_KEY
    assertEquals(DEFAULT_KEY, fileToken.getS3Token().getFileKey());
  }


  /**
   * Tests success scenario for {@link decodeEncodedJsonToFileToken(String)}.decrypts and decodes
   * the string to proper CS10 file token.
   *
   * @throws ClassDirectException if any padding, algorithm and invalid key exception occurs.
   */
  @Test
  public final void successfullyDecodeCS10Token() throws ClassDirectException {
    final FileToken fileToken =
        decodeEncodedJsonToFileToken(SAMPLE_ENCODED_CS10_TOKEN, "37be2b5b-63ee-4535-bca4-202d556d626e");
    assertNotNull(fileToken);
    assertEquals(DownloadTypeEnum.CS10, fileToken.getType());
    assertNotNull(fileToken.getCs10Token());
    // decrypted Node should be equal to DEFAULT_NODE.
    assertEquals(DEFAULT_NODE, fileToken.getCs10Token().getNodeId());
    // decrypted Node should be equal to DEFAULT_VERSION.
    assertEquals(DEFAULT_VERSION, fileToken.getCs10Token().getVersionNumber());
  }
}
