package com.baesystems.ai.lr.cd.service.survey.impl.test;

import static com.baesystems.ai.lr.cd.be.enums.DueStatus.OVER_DUE;
import static com.baesystems.ai.lr.cd.service.test.UserId.ALICE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.UserPersonalInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CustomerLinkHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.port.PortAgentDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.survey.SurveyRequestDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.enums.ProductTypeEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.InvalidDateException;
import com.baesystems.ai.lr.cd.be.exception.InvalidEmailException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.AssetEmblemUtils;
import com.baesystems.ai.lr.cd.mail.MailService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.survey.SurveyService;
import com.baesystems.ai.lr.cd.service.survey.impl.SurveyServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;

/**
 * Provides unit test methods for {@link SurveyService}.
 *
 * @author NAvula
 * @author SBOllu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class SurveyServiceImplTest {

  /**
   * The {@link SurveyService} from Spring context.
   */
  @Autowired
  private SurveyService surveyService;


  /**
   * The {@link SurveyServiceImpl} from Spring context.
   */
  @Autowired
  private SurveyServiceImpl surveyServiceImpl;


  /**
   * The {@link PortService} from Spring context.
   */
  @Autowired
  private PortService portService;


  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;
  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The {@link MailService} from Spring context.
   */
  @Autowired
  private MailService mailService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceService;

  /**
   * Tests success and failure scenarios of {@link SurveyServiceImpl#validateEmail(String)}.
   *
   * @throws Exception if email validation fails.
   */
  @Test
  public final void validateEmailTest() throws Exception {
    boolean email = surveyServiceImpl.validateEmail("johnfoo@baesystems.com");
    assertEquals(email, true);
    boolean emailTest1 = surveyServiceImpl.validateEmail("johnfoo@ba@e.com");
    assertEquals(emailTest1, false);

  }

  /**
   * Tests success scenario of
   * {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}.
   * <ol>
   * <li>tests success scenario,when asset has all related reference data</li>
   * <li>tests success scenario,when asset has no data other than name and IMO number</li>
   * <li>tests success scenario,when asset partial reference data</li>
   * <li>tests success scenario,when asset has services that have neither lower range date nor upper
   * range date in their schedule{@link #LRCD-2747}</li>
   * <li>tests success scenario,when asset has services that have dueDate,assignDate, lower range
   * date and upper range date as null values</li>
   * </ol>
   *
   * @throws Exception if date validation fails or email validation fails or API call fails or
   *         execution fails.
   */
  @Test
  public final void testSurveyRequest() throws Exception {
    String emailsids = "john@baesystems.com;paul@baesystems.com";
    Map<String, Object> model = new TreeMap<String, Object>();

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserValidEmail());
    Mockito.when(mailService.sendMailWithTemplate("John.Smith@baesystems.com", emailsids,
        "Survey Request: 7920756 LADY K II", "confirmation-survey-request.pug", model)).thenReturn(Boolean.TRUE);
    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());
    SurveyRequestDto surveyDto = getSurveyDtos();

    AssetHDto asset = getAssetHdto();
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockOfficeDto(asset));
    Mockito.when(assetService.calculateOverAllStatus(Mockito.any())).thenReturn(DueStatus.OVER_DUE);

    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getRegisteredOwner());

    List<ScheduledServiceHDto> hdtos = mockServices();
    Mockito.when(serviceService.getServicesForAsset(Mockito.eq(1L))).thenReturn(hdtos);

    List<Long> ids = new ArrayList<>();
    ids.add(1L);
    ids.add(2L);
    ids.add(3L);
    ids.add(4L);
    ids.add(5L);
    surveyDto.setServiceIds(ids);

    StatusDto status = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
    assertEquals("Email has been sent successfully", status.getMessage());

    SurveyRequestDto surveyDto1 = getSurveyDtosTest();
    Mockito.when(assetService.assetByCode(ALICE.getId(), AssetCodeUtil.getMastCode(1L)))
        .thenReturn(getAssetHdtoNoReferenceData());
    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getShipManager());

    ScheduledServiceHDto hdto1 = mockServiceAsset(1L);
    List<ScheduledServiceHDto> hdtos1 = new ArrayList<>();
    hdtos.add(hdto1);
    Mockito.when(serviceService.getServicesForAsset(2L)).thenReturn(hdtos1);

    List<Long> ids1 = new ArrayList<>();
    ids1.add(0L);
    ids1.add(1L);
    surveyDto1.setServiceIds(ids);
    StatusDto status1 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto1);
    assertEquals("Email has been sent successfully", status1.getMessage());

    Mockito.when(assetService.assetByCode(ALICE.getId(), AssetCodeUtil.getMastCode(1L))).thenReturn(getAssetHdtoRef());
    Mockito.when(assetService.calculateOverAllStatus(any())).thenReturn(DueStatus.OVER_DUE);
    StatusDto status2 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto1);
    assertEquals("Email has been sent successfully", status2.getMessage());

    ScheduledServiceHDto service = mockServiceAssetWithOutRangeDates(1L);
    List<ScheduledServiceHDto> services = new ArrayList<>();
    services.add(service);
    Mockito.when(serviceService.getServicesForAsset(3L)).thenReturn(services);

    List<Long> serviceIds = new ArrayList<>();
    serviceIds.add(860L);
    surveyDto1.setServiceIds(serviceIds);
    StatusDto statusNoRangeDates = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto1);
    assertEquals("Email has been sent successfully", statusNoRangeDates.getMessage());

    ScheduledServiceHDto serviceWithOutDates = mockServiceAssetWithOutDates(1L);
    List<ScheduledServiceHDto> noDatesservices = new ArrayList<>();
    noDatesservices.add(serviceWithOutDates);
    Mockito.when(serviceService.getServicesForAsset(1L)).thenReturn(noDatesservices);

    List<Long> servIds = new ArrayList<>();
    serviceIds.add(860L);
    surveyDto1.setServiceIds(servIds);
    StatusDto statusNoDates = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto1);
    assertEquals("Email has been sent successfully", statusNoDates.getMessage());

    Mockito.when(assetService.assetByCode(ALICE.getId(), AssetCodeUtil.getMastCode(1L)))
        .thenReturn(getAssetHdtoWithCustomersData());
    Mockito.when(assetService.calculateOverAllStatus(any())).thenReturn(DueStatus.OVER_DUE);

    StatusDto status3 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto1);
    assertEquals("Email has been sent successfully", status3.getMessage());

  }


  /**
   * Tests failure scenario of
   * {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}, if asset CFO mail is
   * null or any other mails are null.
   *
   * @throws Exception if date validation fails or email validation fails or API call fails or
   *         execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testSurveyRequestforNullCFOandOtherMails() throws Exception {
    String emailsids = "john@baesystems.com;paul@baesystems.com";
    Map<String, Object> model = new TreeMap<String, Object>();

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserValidEmail());
    Mockito.when(mailService.sendMailWithTemplate("John.Smith@baesystems.com", emailsids,
        "Survey Request: 7920756 LADY K II", "confirmation-survey-request.pug", model)).thenReturn(Boolean.TRUE);
    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());
    SurveyRequestDto surveyDto = getSurveyDtosNullemails();

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(getAssetHdto());
    Mockito.when(assetService.calculateOverAllStatus(Mockito.any())).thenReturn(DueStatus.OVER_DUE);

    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getRegisteredOwner());

    ScheduledServiceHDto hdto = mockServiceAsset(1L);
    List<ScheduledServiceHDto> hdtos = new ArrayList<>();
    hdtos.add(hdto);
    Mockito.when(serviceService.getServicesForAsset(Mockito.eq(1L))).thenReturn(hdtos);

    List<Long> ids = new ArrayList<>();
    ids.add(0L);
    ids.add(1L);
    surveyDto.setServiceIds(ids);

    StatusDto status = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
    assertEquals("Email has been sent successfully", status.getMessage());

    SurveyRequestDto surveyDto1 = getSurveyDtosTest();
    Mockito.when(assetService.assetByCode(ALICE.getId(), AssetCodeUtil.getMastCode(1L)))
        .thenReturn(getAssetHdtoOfficeData());
    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getShipManager());

    ScheduledServiceHDto hdto1 = mockServiceAsset(1L);
    List<ScheduledServiceHDto> hdtos1 = new ArrayList<>();
    hdtos.add(hdto1);
    Mockito.when(serviceService.getServicesForAsset(1L)).thenReturn(hdtos1);

    List<Long> ids1 = new ArrayList<>();
    ids1.add(0L);
    ids1.add(1L);
    surveyDto1.setServiceIds(ids);
    StatusDto status1 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto1);
    assertEquals("Email has been sent successfully", status1.getMessage());

    Mockito.when(assetService.assetByCode(ALICE.getId(), AssetCodeUtil.getMastCode(1L))).thenReturn(getAssetHdto());
    SurveyRequestDto surveyDto2 = getSurveyDtos();
    Mockito.when(assetService.calculateOverAllStatus(any())).thenReturn(DueStatus.OVER_DUE);
    StatusDto status2 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto2);
    assertEquals("Email has been sent successfully", status2.getMessage());


  }

  /**
   * Tests success scenario of
   * {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}, with default cfo office.
   *
   * @throws Exception if date validation fails or email validation fails or API call fails or
   *         execution fails.
   */
  @Test
  public final void testSurveyRequestWithOffices() throws Exception {
    String emailsids = "john@baesystems.com;paul@baesystems.com";
    Map<String, Object> model = new TreeMap<String, Object>();

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserValidEmail());
    Mockito.when(mailService.sendMailWithTemplate("John.Smith@baesystems.com", emailsids,
        "Survey Request: 7920756 LADY K II", "confirmation-survey-request.pug", model)).thenReturn(Boolean.TRUE);
    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());
    SurveyRequestDto surveyDto = getSurveyDtos();

    Mockito.when(assetService.assetByCode(ALICE.getId(), AssetCodeUtil.getMastCode(1L)))
        .thenReturn(getAssetHdtoOffices());
    Mockito.when(assetService.calculateOverAllStatus(Mockito.any())).thenReturn(DueStatus.OVER_DUE);

    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getRegisteredOwner());

    ScheduledServiceHDto hdto = mockServiceAsset(1L);
    List<ScheduledServiceHDto> hdtos = new ArrayList<>();
    hdtos.add(hdto);
    Mockito.when(serviceService.getServicesForAsset(Mockito.eq(1L))).thenReturn(hdtos);

    List<Long> ids = new ArrayList<>();
    ids.add(0L);
    ids.add(1L);
    surveyDto.setServiceIds(ids);
    StatusDto status = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
    assertEquals("Email has been sent successfully", status.getMessage());

  }

  /**
   * Tests failure scenario of {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}
   * when get asset by code fail.
   *
   * @throws Exception if date validation fails or email validation fails or API call fails or
   *         execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testSurveyRequestGetAssetFail() throws Exception {
    String emailsids = "john@baesystems.com;paul@baesystems.com";
    Map<String, Object> model = new TreeMap<String, Object>();

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserValidEmail());
    Mockito.when(mailService.sendMailWithTemplate("John.Smith@baesystems.com", emailsids,
        "Survey Request: 7920756 LADY K II", "confirmation-survey-request.pug", model)).thenReturn(Boolean.TRUE);
    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(null);
    SurveyRequestDto surveyDto = getSurveyDtos();

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString()))
        .thenThrow(new ClassDirectException("Mock Exception"));
    Mockito.when(assetService.calculateOverAllStatus(Mockito.any())).thenReturn(DueStatus.OVER_DUE);

    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getRegisteredOwner());

    ScheduledServiceHDto hdto = mockServiceAsset(1L);
    List<ScheduledServiceHDto> hdtos = new ArrayList<>();
    hdtos.add(hdto);
    Mockito.when(serviceService.getServicesForAsset(Mockito.eq(1L))).thenReturn(hdtos);

    List<Long> ids = new ArrayList<>();
    ids.add(0L);
    ids.add(1L);
    surveyDto.setServiceIds(ids);

    surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
  }

  /**
   * Tests success scenario of
   * {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}.
   * <ol>
   * <li>tests success scenario,when asset has service with occurrence number and serviceCatalouge
   * has multiple indicator</li>
   * <li>tests success scenario,when asset has service with null occurrence number and
   * serviceCatalouge has multiple indicator</li>
   * <li>tests success scenario,when asset has service with out occurrence number and
   * serviceCatalouge has multiple indicator as false</li>
   * <li>tests success scenario,when asset has service with out occurrence number and
   * serviceCatalouge has null multiple indicator</li>
   * </ol>
   *
   * @throws Exception if date validation fails or email validation fails or API call fails or
   *         execution fails.
   */
  @Test
  public final void testSurveyRequestforInclusionOfOccurenceNumInServiceName() throws Exception {
    String emailsids = "john@baesystems.com;paul@baesystems.com";
    Map<String, Object> model = new TreeMap<String, Object>();

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserValidEmail());
    Mockito.when(mailService.sendMailWithTemplate("John.Smith@baesystems.com", emailsids,
        "Survey Request: 7920756 LADY K II", "confirmation-survey-request.pug", model)).thenReturn(Boolean.TRUE);
    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());

    SurveyRequestDto surveyDto = getSurveyDtosTest();
    surveyDto.setPortAgent(new PortAgentDto());
    AssetHDto asset = getAssetHdto();
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockOfficeDto(asset));
    Mockito.when(assetService.calculateOverAllStatus(Mockito.any())).thenReturn(DueStatus.OVER_DUE);
    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getShipManager());

    // multipleIndicator is true
    ScheduledServiceHDto service = mockServiceWithOccurrenceNumber(1L);
    List<ScheduledServiceHDto> services = new ArrayList<>();
    services.add(service);
    Mockito.when(serviceService.getServicesForAsset(1L)).thenReturn(services);

    List<Long> ids = new ArrayList<>();
    ids.add(2L);
    ids.add(1L);
    surveyDto.setServiceIds(ids);
    PortAgentDto portAgent = new PortAgentDto();
    portAgent.setEmail("portagent@gmail.com");
    surveyDto.setPortAgent(portAgent);
    StatusDto status = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
    assertEquals("Email has been sent successfully", status.getMessage());

    // multipleIndicator is null
    ScheduledServiceHDto serviceNullMultipleIndicator = mockServiceWithNullMutipleIndicator(1L);
    List<ScheduledServiceHDto> servicesNullMultiIndicator = new ArrayList<>();
    servicesNullMultiIndicator.add(serviceNullMultipleIndicator);
    Mockito.when(serviceService.getServicesForAsset(1L)).thenReturn(servicesNullMultiIndicator);

    StatusDto status1 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
    assertEquals("Email has been sent successfully", status1.getMessage());


    // multipleIndicator is false
    ScheduledServiceHDto serviceFalseMultipleIndicator = mockServiceWithFalseMutipleIndicator(1L);
    List<ScheduledServiceHDto> servicesFalseMultiIndicator = new ArrayList<>();
    servicesFalseMultiIndicator.add(serviceFalseMultipleIndicator);
    Mockito.when(serviceService.getServicesForAsset(1L)).thenReturn(servicesFalseMultiIndicator);

    StatusDto status2 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
    assertEquals("Email has been sent successfully", status2.getMessage());


    // service occurrence is null.
    ScheduledServiceHDto serviceNullOccurrenceNumber = mockServiceWithMutipleIndicatorNullOccurenceNUmber(1L);
    List<ScheduledServiceHDto> servicesNullOccurrenceNumber = new ArrayList<>();
    servicesNullOccurrenceNumber.add(serviceNullOccurrenceNumber);
    Mockito.when(serviceService.getServicesForAsset(1L)).thenReturn(servicesNullOccurrenceNumber);

    StatusDto status3 = surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
    assertEquals("Email has been sent successfully", status3.getMessage());
  }

  /**
   * Returns mock SurveyDto pay load with valid email's.
   *
   * @return mock SurveyDto pay load.
   */
  private SurveyRequestDto getSurveyDtos() {
    SurveyRequestDto surveyDto = new SurveyRequestDto();
    String[] emails = {"john@baesystems.com", "paul@baesystems.com"};
    surveyDto.setOtherEmails(emails);
    LocalDate today = LocalDate.now();
    LocalTime now = LocalTime.now();

    surveyDto.setEstimatedDateOfArrival(today.plusMonths(1L));
    surveyDto.setEstimatedDateOfDeparture(LocalDate.now().plusMonths(1L).plusWeeks(1L));
    surveyDto.setSurveyStartDate(today.plusMonths(1L).plusWeeks(1L));

    surveyDto.setEstimatedTimeOfArrival(now);
    surveyDto.setEstimatedTimeOfDeparture(now);
    surveyDto.setAssetId("LRV1");
    surveyDto.setPortId(1L);
    surveyDto.setOtherEmails(emails);
    PortAgentDto portAgent = new PortAgentDto();
    portAgent.setEmail("port.agent@baesystems.com");
    portAgent.setName("Port Agent Name");
    surveyDto.setPortAgent(portAgent);
    return surveyDto;
  }

  /**
   * Returns mock SurveyDto pay load with empty email's.
   *
   * @return mock SurveyDto pay load.
   */
  private SurveyRequestDto getSurveyDtosNullemails() {
    SurveyRequestDto surveyDto = new SurveyRequestDto();
    String[] emails = {};
    surveyDto.setOtherEmails(emails);
    LocalDate today = LocalDate.now();
    LocalTime now = LocalTime.now();

    surveyDto.setEstimatedDateOfArrival(today.plusMonths(1L));
    surveyDto.setEstimatedDateOfDeparture(LocalDate.now().plusMonths(1L).plusWeeks(1L));
    surveyDto.setSurveyStartDate(today.plusMonths(1L).plusWeeks(1L));

    surveyDto.setEstimatedTimeOfArrival(now);
    surveyDto.setEstimatedTimeOfDeparture(now);
    surveyDto.setAssetId("LRV1");
    surveyDto.setPortId(1L);
    surveyDto.setOtherEmails(emails);
    PortAgentDto portAgent = new PortAgentDto();
    portAgent.setEmail("port.agent@baesystems.com");
    portAgent.setName("Port Agent Name");
    surveyDto.setPortAgent(portAgent);
    return surveyDto;
  }

  /**
   * Returns mock SurveyDto pay load with null email's.
   *
   * @return mock pay load.
   */
  private SurveyRequestDto getSurveyDtosTest() {
    SurveyRequestDto surveyDto = new SurveyRequestDto();
    surveyDto.setAssetId("LRV1");
    surveyDto.setPortId(1L);
    return surveyDto;
  }


  /**
   * Returns mock port.
   *
   * @return port
   */
  private PortOfRegistryDto getPort() {
    PortOfRegistryDto port = new PortOfRegistryDto();
    port.setName("Kuala Lumpur");
    port.setId(1L);
    port.setInternalId(port.getId().toString());
    OfficeDto sdo = new OfficeDto();
    sdo.setId(1L);
    sdo.setName("SDO Name");
    sdo.setPhoneNumber("SDO Phone");
    sdo.setEmailAddress("SDO EmailAddress");
    port.setSdo(sdo);
    return port;
  }

  /**
   * Returns mock asset.
   *
   * @return mock asset.
   */
  private AssetHDto getAssetHdto() {
    AssetHDto hDto = new AssetHDto();
    hDto.setId(1L);
    hDto.setLeadImo("1");
    hDto.setName("Asset Name");
    // AssetTypeH
    AssetTypeHDto assetTypeHDto = new AssetTypeHDto();
    assetTypeHDto.setId(1L);
    hDto.setAssetTypeHDto(assetTypeHDto);
    // office
    List<OfficeLinkDto> linkDto = new ArrayList<>();
    OfficeLinkDto ofLinkDto = new OfficeLinkDto();
    ofLinkDto.setOffice(new LinkResource(1L));
    ofLinkDto.setOfficeRole(new LinkResource(2L));
    linkDto.add(ofLinkDto);
    hDto.setOffices(linkDto);
    // FlagStateDto
    FlagStateHDto flagStateHdto = new FlagStateHDto();
    flagStateHdto.setId(1L);
    flagStateHdto.setName("Flag State");
    hDto.setFlagStateDto(flagStateHdto);
    // AssetClassStatus
    ClassStatusHDto classStatusHdto = new ClassStatusHDto();
    classStatusHdto.setId(1L);
    classStatusHdto.setName("Class Status");
    hDto.setClassStatusDto(classStatusHdto);
    // LifecycleStatus
    AssetLifeCycleStatusDto lifeCycleDto = new AssetLifeCycleStatusDto();
    lifeCycleDto.setId(1L);
    lifeCycleDto.setName("Life Cycle Name");
    hDto.setAssetLifeCycleStatusDto(lifeCycleDto);
    hDto.setDueStatusH(DueStatus.OVER_DUE);
    // IhsAssetDetails
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    IhsAssetDto ihsAsset = new IhsAssetDto();
    ihsAsset.setOwner("OWNER118");
    ihsAsset.setOwnerCode("5000018");
    ihsAsset.setShipManager("ShipManager14");
    ihsAsset.setShipManagerCode("1811");
    ihsAssetDetailsDto.setIhsAsset(ihsAsset);
    hDto.setIhsAssetDto(ihsAssetDetailsDto);
    AssetTypeHDto assetTypeH = new AssetTypeHDto();
    assetTypeH.setId(1L);
    assetTypeH.setCode("A12");
    hDto.setAssetTypeHDto(assetTypeH);
    AssetTypeDto assetType = new AssetTypeDto();
    assetType.setId(1L);
    assetType.setName("Type1");
    hDto.setAssetType(assetType);
    hDto.setBuildDate(new Date());

    return hDto;
  }

  /**
   * Returns mock registeredOwner.
   *
   * @return mock registeredOwner.
   */
  private UserPersonalInformationDto getRegisteredOwner() {
    UserPersonalInformationDto registeredOwner = new UserPersonalInformationDto();
    registeredOwner.setName("OWNER118");
    registeredOwner.setPhoneNumber("0101012278");
    registeredOwner.setEmailAddress("test@testmail.com");
    registeredOwner.setAddress("address123");
    return registeredOwner;
  }

  /**
   * Returns mock shipManager details.
   *
   * @return mock shipManager details.
   */
  private UserPersonalInformationDto getShipManager() {
    UserPersonalInformationDto shipManager = new UserPersonalInformationDto();
    shipManager.setName("ShipManager14");
    shipManager.setPhoneNumber("0101012274");
    shipManager.setEmailAddress("test12@testmail.com");
    shipManager.setAddress("address4567");
    return shipManager;
  }

  /**
   * Returns mock asset without reference data.
   *
   * @return mock asset.
   */
  private AssetHDto getAssetHdtoNoReferenceData() {
    AssetHDto hDto = new AssetHDto();
    hDto.setId(1L);
    hDto.setLeadImo("1");
    hDto.setName("Asset Name");
    OfficeDto officeDto = new OfficeDto();
    officeDto.setId(1L);
    officeDto.setEmailAddress("paul@baesystems.com");
    officeDto.setDeleted(Boolean.FALSE);
    hDto.setCfoOfficeH(officeDto);
    return hDto;
  }


  /**
   * Returns mock asset without reference data.
   *
   * @return mock asset.
   */
  private AssetHDto getAssetHdtoWithCustomersData() {
    AssetHDto hDto = new AssetHDto();
    hDto.setId(1L);
    hDto.setLeadImo("1");
    hDto.setName("Asset Name");
    // customers
    List<CustomerLinkHDto> linkDto = new ArrayList<>();
    CustomerLinkHDto cusLinkDto = new CustomerLinkHDto();
    PartyRoleHDto pHdto = new PartyRoleHDto();
    pHdto.setId(3L);
    cusLinkDto.setPartyRoles(pHdto);

    PartyDto partyDto = new PartyDto();
    partyDto.setId(1L);
    partyDto.setName("Test1");
    partyDto.setEmailAddress("Test1@bae.com");
    partyDto.setEmailAddress("Test1@bae.com");
    partyDto.setAddressLine2("TestAddress");
    partyDto.setPhoneNumber("1234");

    CustomerLinkDto cusDto = new CustomerLinkDto();
    cusDto.setCustomer(partyDto);
    cusLinkDto.setParty(cusDto);
    linkDto.add(cusLinkDto);

    CustomerLinkHDto cusLinkDto1 = new CustomerLinkHDto();
    PartyRoleHDto pHdto1 = new PartyRoleHDto();
    pHdto1.setId(4L);
    cusLinkDto1.setPartyRoles(pHdto1);

    PartyDto partyDto1 = new PartyDto();
    partyDto1.setId(1L);
    partyDto1.setName("Test1");
    partyDto1.setEmailAddress("Test1@bae.com");
    partyDto1.setAddressLine2("TestAddress");
    partyDto1.setPhoneNumber("1234");


    CustomerLinkDto cusDto1 = new CustomerLinkDto();
    cusDto1.setCustomer(partyDto1);
    cusLinkDto1.setParty(cusDto1);
    linkDto.add(cusLinkDto1);
    hDto.setCustomersH(linkDto);
    OfficeDto officeDto = new OfficeDto();
    officeDto.setId(1L);
    officeDto.setDeleted(Boolean.FALSE);
    officeDto.setEmailAddress("paul@baesystems.com");
    hDto.setCfoOfficeH(officeDto);
    hDto.setClassStatusDto(new ClassStatusHDto());

    return hDto;
  }

  /**
   * Returns mock asset with office reference data.
   *
   * @return mock asset.
   */
  private AssetHDto getAssetHdtoOfficeData() {
    AssetHDto hDto = new AssetHDto();
    hDto.setId(1L);
    hDto.setLeadImo("1");
    hDto.setName("Asset Name");
    // office
    List<OfficeLinkDto> linkDto = new ArrayList<>();
    OfficeLinkDto ofLinkDto = new OfficeLinkDto();
    ofLinkDto.setOffice(new LinkResource(1L));
    ofLinkDto.setOfficeRole(new LinkResource(2L));
    linkDto.add(ofLinkDto);
    hDto.setOffices(linkDto);
    return hDto;
  }

  /**
   * Returns mock asset with reference data.
   *
   * @return mock asset.
   */
  private AssetHDto getAssetHdtoRef() {
    AssetHDto hDto = new AssetHDto();
    hDto.setId(1L);
    hDto.setLeadImo("1");
    hDto.setName("Asset Name");
    // AssetTypeH
    AssetTypeHDto assetTypeDto = new AssetTypeHDto();
    assetTypeDto.setId(1L);
    assetTypeDto.setName("Asset Type");
    hDto.setAssetTypeHDto(assetTypeDto);
    // AssetType
    AssetTypeDto assetType = new AssetTypeDto();
    assetType.setId(1L);
    hDto.setAssetType(assetType);
    // FlagStateDto
    FlagStateHDto flagStateHdto = new FlagStateHDto();
    flagStateHdto.setId(1L);
    hDto.setFlagStateDto(flagStateHdto);
    // AssetClassStatus
    ClassStatusHDto classStatusHdto = new ClassStatusHDto();
    classStatusHdto.setId(10L);
    hDto.setClassStatusDto(classStatusHdto);
    // LifecycleStatus
    AssetLifeCycleStatusDto lifeCycleDto = new AssetLifeCycleStatusDto();
    lifeCycleDto.setId(1L);
    hDto.setAssetLifeCycleStatusDto(lifeCycleDto);
    OfficeDto officeDto = new OfficeDto();
    officeDto.setId(1L);
    officeDto.setDeleted(Boolean.FALSE);
    officeDto.setEmailAddress("paul@baesystems.com");
    hDto.setCfoOfficeH(officeDto);
    return hDto;
  }

  /**
   * Tests success scenario of
   * {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}.
   *
   * @throws InvalidDateException if date validation fails.
   * @throws InvalidEmailException if email validation fails.
   * @throws RecordNotFoundException if object not found with given unique identifier.
   * @throws IOException if API call fails.
   * @throws ClassDirectException if execution fails.
   */
  @Test
  public final void validateSurveyRequest()
      throws InvalidDateException, InvalidEmailException, RecordNotFoundException, IOException, ClassDirectException {

    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());
    Mockito.when(assetService.assetByCode(Mockito.any(), Mockito.anyString()))
        .thenReturn(mockOfficeDto(getAssetHdto()));
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserValidEmail());
    doReturn(OVER_DUE).when(assetService).calculateOverAllStatus(any());

    SurveyRequestDto surveyDto = getSurveyDtos();
    List<Long> ids = new ArrayList<>();
    ids.add(0L);
    ids.add(1L);
    surveyDto.setServiceIds(ids);
    assertEquals(HttpStatus.OK.toString(), surveyService.validateSurveyRequest(ALICE.getId(), surveyDto).getStatus());
  }

  /**
   * Tests failure scenario of {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}
   * when error in getting cfo details.
   *
   * @throws InvalidDateException if date validation fails.
   * @throws InvalidEmailException if email validation fails.
   * @throws RecordNotFoundException if object not found with given unique identifier.
   * @throws IOException if API call fails.
   * @throws ClassDirectException if execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testSurveyRequestFail()
      throws InvalidDateException, InvalidEmailException, RecordNotFoundException, IOException, ClassDirectException {
    String emailsids = "john@baesystems.com;paul@baesystems.com";
    Map<String, Object> model = new TreeMap<String, Object>();

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserValidEmail());
    Mockito.when(mailService.sendMailWithTemplate("John.Smith@baesystems.com", emailsids,
        "Survey Request: 7920756 LADY K II", "confirmation-survey-request.pug", model)).thenReturn(Boolean.TRUE);
    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());
    SurveyRequestDto surveyDto = getSurveyDtos();
    AssetHDto asset = getAssetHdto();
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(assetService.calculateOverAllStatus(Mockito.any())).thenReturn(DueStatus.OVER_DUE);

    Mockito.when(assetService.getPersonalInformation(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getRegisteredOwner());

    ScheduledServiceHDto hdto = mockServiceAsset(1L);
    List<ScheduledServiceHDto> hdtos = new ArrayList<>();
    hdtos.add(hdto);
    Mockito.when(serviceService.getServicesForAsset(Mockito.eq(1L))).thenReturn(hdtos);

    List<Long> ids = new ArrayList<>();
    ids.add(0L);
    ids.add(1L);
    surveyDto.setServiceIds(ids);

    surveyService.validateSurveyRequest(ALICE.getId(), surveyDto);
  }

  /**
   * Tests failure scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if date of
   * departure is in past.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test(expected = InvalidDateException.class)
  public final void validateSurveyDate() throws InvalidDateException {
    SurveyRequestDto surveyDto = getSurveyDtos();
    surveyDto.setEstimatedDateOfDeparture(surveyDto.getEstimatedDateOfDeparture().minusMonths(1L));
    surveyServiceImpl.validateDate(surveyDto);

  }

  /**
   * Tests failure scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if survey
   * date is before today.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test(expected = InvalidDateException.class)
  public final void validateSurveyDateBeforeToday() throws InvalidDateException {
    SurveyRequestDto surveyDto = getSurveyDtos();
    surveyDto.setSurveyStartDate(surveyDto.getSurveyStartDate().minusMonths(2L));
    surveyServiceImpl.validateDate(surveyDto);

  }

  /**
   * Tests failure scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if date of
   * departure is in past.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test(expected = InvalidDateException.class)
  public final void validatePastDate() throws InvalidDateException {
    SurveyRequestDto surveyDto = getSurveyDtos();
    surveyDto.setEstimatedDateOfDeparture(LocalDate.now().minusMonths(1L));
    surveyServiceImpl.validateDate(surveyDto);
  }

  /**
   * Tests failure scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if date of
   * departure is before survey date.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test(expected = InvalidDateException.class)
  public final void validateDeparturePastDate() throws InvalidDateException {
    LocalDate today = LocalDate.now();
    SurveyRequestDto surveyDto = getSurveyDtos();
    surveyDto.setEstimatedDateOfDeparture(today.plusMonths(1L).plusWeeks(1L));
    surveyDto.setSurveyStartDate(today.plusMonths(1L).plusWeeks(1L).plusWeeks(1L));
    surveyServiceImpl.validateDate(surveyDto);
  }

  /**
   * Tests failure scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if date of
   * departure,survey date,date of arrival is in past.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test(expected = InvalidDateException.class)
  public final void validateallDatesShouldnotinPast() throws InvalidDateException {
    LocalDate today = LocalDate.now();
    SurveyRequestDto surveyDto = getSurveyDtos();
    surveyDto.setEstimatedDateOfDeparture(today.minusMonths(2L).plusWeeks(1L));
    surveyDto.setSurveyStartDate(today.minusMonths(2L).plusWeeks(1L).plusWeeks(1L));
    surveyDto.setEstimatedDateOfArrival(today.minusMonths(2L).plusWeeks(1L).plusWeeks(1L));
    surveyServiceImpl.validateDate(surveyDto);
  }


  /**
   * Tests success scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if
   * departure date and time is null.
   *
   * @throws InvalidDateException if a date isn't valid.
   * @throws IOException if mast doesn't response.
   * @throws RecordNotFoundException if the given asset doesn't exist.
   * @throws ClassDirectException if execution fails,email validation fails.
   */
  @Test
  public final void testArrivalTimes()
      throws InvalidDateException, IOException, RecordNotFoundException, ClassDirectException {

    SurveyRequestDto surveyDto = getSurveyDtos();

    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());
    Mockito.when(assetService.assetByCode(Mockito.any(), Mockito.anyString())).thenReturn(getAssetHdto());

    surveyServiceImpl.validateDate(surveyDto);

    // test with both unset at the same time
    surveyDto.setEstimatedTimeOfDeparture(null);
    surveyServiceImpl.validateDate(surveyDto);

    // only time of departure time unset
    surveyDto.setEstimatedTimeOfArrival(LocalTime.NOON);
    surveyServiceImpl.validateDate(surveyDto);
  }

  /**
   * Tests success scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, when survey
   * date time is the same or after arrival or before or same as departure.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test
  public final void testSurveyTimes() throws InvalidDateException {
    SurveyRequestDto surveyDto = getSurveyDtos();

    surveyDto.setEstimatedTimeOfDeparture(null);
    surveyDto.setEstimatedTimeOfArrival(null);


    // survey date same as arrival
    surveyDto.setSurveyStartDate(surveyDto.getEstimatedDateOfArrival());
    surveyServiceImpl.validateDate(surveyDto);

    // survey date before arrival
    surveyDto.setSurveyStartDate(surveyDto.getEstimatedDateOfArrival().minusDays(1));
    try {
      surveyServiceImpl.validateDate(surveyDto);
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidDateException);
    }

    // survey date same as departure
    surveyDto = getSurveyDtos();

    surveyDto.setEstimatedTimeOfDeparture(null);
    surveyDto.setEstimatedTimeOfArrival(null);

    surveyDto.setSurveyStartDate(surveyDto.getEstimatedDateOfDeparture());
    surveyServiceImpl.validateDate(surveyDto);

    // test survey date after departure date

    // survey date before arrival
    surveyDto.setSurveyStartDate(surveyDto.getEstimatedDateOfDeparture().plusDays(1));
    try {
      surveyServiceImpl.validateDate(surveyDto);
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidDateException);
    }

  }

  /**
   * Tests success scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if dates
   * and time work properly when the dates are the same but the time isn't.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test
  public void testArrivalAndDepartureDatesSameDay() throws InvalidDateException {
    LocalDate tomorrow = LocalDate.now().plusDays(1);
    SurveyRequestDto surveyDto = getSurveyDtos();


    // Same day different time (correct times)
    surveyDto.setEstimatedTimeOfArrival(LocalTime.MIN);
    surveyDto.setEstimatedTimeOfDeparture(LocalTime.MAX);

    surveyDto.setEstimatedDateOfArrival(tomorrow);
    surveyDto.setEstimatedDateOfDeparture(tomorrow);

    surveyDto.setSurveyStartDate(tomorrow);

    // Same day different time (depart before arriving)
    surveyDto.setEstimatedTimeOfArrival(LocalTime.MAX);
    surveyDto.setEstimatedTimeOfDeparture(LocalTime.MIN);
    surveyDto.setSurveyStartDate(tomorrow);


    try {
      surveyServiceImpl.validateDate(surveyDto);
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidDateException);
    }

    // Same day, no time set
    surveyDto.setEstimatedTimeOfArrival(null);
    surveyDto.setEstimatedTimeOfDeparture(null);

    surveyServiceImpl.validateDate(surveyDto);

    // departure date before arrival date
    surveyDto.setEstimatedTimeOfArrival(null);
    surveyDto.setEstimatedTimeOfDeparture(null);
    surveyDto.setSurveyStartDate(tomorrow);

    surveyDto.setEstimatedDateOfDeparture(tomorrow);
    surveyDto.setEstimatedDateOfArrival(tomorrow.plusDays(2));


    try {
      surveyServiceImpl.validateDate(surveyDto);
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidDateException);
    }
  }

  /**
   * Tests success scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if dates
   * and time work properly when the dates are the currentDate but the time isn't.
   *
   * @throws InvalidDateException exception.
   */
  @Test
  public void testArrivalAndDepartureDatesCurrentDay() throws InvalidDateException {

    LocalDate today = LocalDate.now();
    SurveyRequestDto surveyDto = getSurveyDtos();

    // Arrival should earlier than departure.
    surveyDto.setEstimatedTimeOfArrival(LocalTime.of(1, 30));
    surveyDto.setEstimatedTimeOfDeparture(LocalTime.of(6, 30));
    surveyDto.setEstimatedDateOfArrival(today);
    surveyDto.setEstimatedDateOfDeparture(today);
    surveyDto.setSurveyStartDate(today);

    surveyServiceImpl.validateDate(surveyDto);

    // Arrival same as departure time. Still accept
    surveyDto.setEstimatedTimeOfArrival(LocalTime.of(0, 00));
    surveyDto.setEstimatedTimeOfDeparture(LocalTime.of(0, 00));
    surveyDto.setEstimatedDateOfArrival(today);
    surveyDto.setEstimatedDateOfDeparture(today);
    surveyDto.setSurveyStartDate(today);

    surveyServiceImpl.validateDate(surveyDto);

    // Arrival without time and same day as Departure with time, accepted.
    surveyDto.setEstimatedTimeOfArrival(null);
    surveyDto.setEstimatedTimeOfDeparture(LocalTime.of(1, 30));
    surveyDto.setEstimatedDateOfArrival(today);
    surveyDto.setEstimatedDateOfDeparture(today);
    surveyDto.setSurveyStartDate(today);

    // Arrival with time but departure same day without time, still accept.
    surveyDto.setEstimatedTimeOfArrival(LocalTime.of(1, 30));
    surveyDto.setEstimatedTimeOfDeparture(null);
    surveyDto.setEstimatedDateOfArrival(today);
    surveyDto.setEstimatedDateOfDeparture(today);
    surveyDto.setSurveyStartDate(today);

    // Arrival and departure same day without time, still accept.
    surveyDto.setEstimatedTimeOfArrival(null);
    surveyDto.setEstimatedTimeOfDeparture(null);
    surveyDto.setEstimatedDateOfArrival(today);
    surveyDto.setEstimatedDateOfDeparture(today);
    surveyDto.setSurveyStartDate(today);

    surveyServiceImpl.validateDate(surveyDto);

  }

  /**
   * Tests failure scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test(expected = InvalidDateException.class)
  public void testArrivalAndDepartureDatesCurrentDayfail() throws InvalidDateException {
    LocalDate today = LocalDate.now();
    SurveyRequestDto surveyDto = getSurveyDtos();
    // Arrival time after Departure time.
    surveyDto.setEstimatedTimeOfArrival(LocalTime.of(6, 30));
    surveyDto.setEstimatedTimeOfDeparture(LocalTime.of(1, 30));
    surveyDto.setEstimatedDateOfArrival(today);
    surveyDto.setEstimatedDateOfDeparture(today);
    surveyDto.setSurveyStartDate(today);
    surveyServiceImpl.validateDate(surveyDto);
  }

  /**
   * Tests success scenario of {@link SurveyServiceImpl#validateDate(SurveyRequestDto)}, if one or
   * more of the dates are missing then it should be valid no matter what.
   *
   * @throws InvalidDateException if date validation fails.
   */
  @Test
  public void anyDateMissing() throws InvalidDateException {
    LocalDate tomorrow = LocalDate.now().plusDays(1);
    SurveyRequestDto surveyDto = getSurveyDtos();


    surveyDto.setEstimatedDateOfDeparture(null);
    surveyDto.setSurveyStartDate(tomorrow);
    surveyDto.setEstimatedDateOfArrival(tomorrow);

    surveyServiceImpl.validateDate(surveyDto);


    surveyDto.setEstimatedDateOfDeparture(null);
    surveyDto.setSurveyStartDate(null);
    surveyDto.setEstimatedDateOfArrival(tomorrow);

    surveyServiceImpl.validateDate(surveyDto);

    surveyDto.setEstimatedDateOfDeparture(tomorrow);
    surveyDto.setSurveyStartDate(null);
    surveyDto.setEstimatedDateOfArrival(tomorrow);

    surveyServiceImpl.validateDate(surveyDto);


    surveyDto.setEstimatedDateOfDeparture(null);
    surveyDto.setSurveyStartDate(null);
    surveyDto.setEstimatedDateOfArrival(null);

    surveyServiceImpl.validateDate(surveyDto);


    surveyDto.setEstimatedDateOfDeparture(tomorrow);
    surveyDto.setSurveyStartDate(null);
    surveyDto.setEstimatedDateOfArrival(null);

    surveyServiceImpl.validateDate(surveyDto);

    surveyDto.setEstimatedDateOfDeparture(tomorrow);
    surveyDto.setSurveyStartDate(tomorrow);
    surveyDto.setEstimatedDateOfArrival(null);

    surveyServiceImpl.validateDate(surveyDto);

    surveyDto.setEstimatedDateOfDeparture(tomorrow);
    surveyDto.setSurveyStartDate(null);
    surveyDto.setEstimatedDateOfArrival(tomorrow);

    surveyServiceImpl.validateDate(surveyDto);



    surveyDto.setEstimatedDateOfDeparture(null);
    surveyDto.setSurveyStartDate(tomorrow);
    surveyDto.setEstimatedDateOfArrival(null);

    surveyServiceImpl.validateDate(surveyDto);

    surveyDto.setEstimatedDateOfDeparture(null);
    surveyDto.setSurveyStartDate(tomorrow);
    surveyDto.setEstimatedDateOfArrival(tomorrow);

    surveyServiceImpl.validateDate(surveyDto);

    surveyDto.setEstimatedDateOfDeparture(tomorrow);
    surveyDto.setSurveyStartDate(tomorrow);
    surveyDto.setEstimatedDateOfArrival(null);

    surveyServiceImpl.validateDate(surveyDto);


  }


  /**
   * Tests failure scenarios of
   * {@link SurveyService#validateSurveyRequest(String, SurveyRequestDto)}. Unit test on different
   * invalid email scenarios.
   *
   * @throws IOException if mast can't be contacted.
   * @throws RecordNotFoundException if one of the requested assets doesn't exist.
   * @throws ClassDirectException if execution fails,validation of date and email fails.
   */
  @Test
  public final void invalidEmail() throws IOException, RecordNotFoundException, ClassDirectException {

    Mockito.when(portService.getPort(Mockito.anyLong())).thenReturn(getPort());
    Mockito.when(assetService.assetByCode(Mockito.any(), Mockito.anyString())).thenReturn(getAssetHdto());

    SurveyRequestDto survey = getSurveyDtos();
    survey.setOtherEmails(new String[] {"test@test.com", "some weirdemail@werd mail.com"});
    try {
      assertEquals(HttpStatus.OK.toString(), surveyService.validateSurveyRequest(ALICE.getId(), survey).getStatus());
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidEmailException);
    }

    survey.setOtherEmails(new String[] {"test@test.com"});

    survey.getPortAgent().setEmail("some weirdemail@werd mail.com");
    try {
      assertEquals(HttpStatus.OK.toString(), surveyService.validateSurveyRequest(ALICE.getId(), survey).getStatus());
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidEmailException);
    }
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(getUserNoEmail());
    try {
      assertEquals(HttpStatus.OK.toString(), surveyService.validateSurveyRequest(ALICE.getId(), survey).getStatus());
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidEmailException);
    }
    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    up.setEmail("abcbae.com");
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(up);
    try {
      assertEquals(HttpStatus.OK.toString(), surveyService.validateSurveyRequest(ALICE.getId(), survey).getStatus());
      fail("Shouldn't reach here, it should throw exception");
    } catch (Exception e) {
      assertTrue(e instanceof InvalidEmailException);
    }
  }

  /**
   * Returns mock user without email.
   *
   * @return valid mock User profile.
   */
  private UserProfiles getUserNoEmail() {
    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    return up;
  }

  /**
   * Returns mock user with email.
   *
   * @return valid mock User profile.
   */
  private UserProfiles getUserValidEmail() {
    UserProfiles up = new UserProfiles();
    up.setUserId(ALICE.getId());
    up.setEmail("abc@bae.com");
    Company com = new Company();
    com.setName("BAE");
    up.setCompany(com);
    up.setCompanyName(com.getName());
    return up;
  }

  /**
   * Returns mock service for given assetId.
   *
   * @param id the unique identifier of an asset.
   * @return mock service for an asset.
   */
  private ScheduledServiceHDto mockServiceAsset(final Long id) {
    Long assetId = id;
    if (assetId == null) {
      assetId = 1L;
    }

    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    ServiceCatalogueHDto cat = new ServiceCatalogueHDto();
    cat.setName("name");
    cat.setDisplayOrder(1);
    cat.setProductGroupH(mockProductGroup(1L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setDisplayOrder(1);
    pCat.setName("pName");
    dto.setDueStatusH(DueStatus.DUE_SOON);
    cat.setProductCatalogue(pCat);
    dto.setServiceCatalogueH(cat);
    dto.setLowerRangeDate(new Date());
    dto.setDueDate(new Date());
    dto.setAssignedDate(new Date());
    dto.setId(assetId);
    return dto;
  }

  /**
   * Returns mock service has occurrence number.
   *
   * @param id the unique identifier of an asset.
   * @return mock service has occurrence number for an asset.
   */
  private ScheduledServiceHDto mockServiceWithOccurrenceNumber(final Long id) {
    Long assetId = id;
    if (assetId == null) {
      assetId = 1L;
    }

    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    ServiceCatalogueHDto cat = new ServiceCatalogueHDto();
    cat.setName("name");
    cat.setMultipleIndicator(Boolean.TRUE);
    cat.setDisplayOrder(1);
    cat.setProductGroupH(mockProductGroup(1L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setName("pName");
    pCat.setDisplayOrder(1);
    dto.setDueStatusH(DueStatus.DUE_SOON);
    cat.setProductCatalogue(pCat);
    dto.setServiceCatalogueH(cat);
    dto.setLowerRangeDate(new Date());
    dto.setDueDate(new Date());
    dto.setAssignedDate(new Date());
    dto.setId(assetId);
    dto.setOccurrenceNumber(1234);
    return dto;
  }

  /**
   * Returns mock service has serviceCatalogue with null multiple indicator.
   *
   * @param id the unique identifier of an asset.
   * @return mock service for an asset.
   */
  private ScheduledServiceHDto mockServiceWithNullMutipleIndicator(final Long id) {
    Long assetId = id;
    if (assetId == null) {
      assetId = 1L;
    }

    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    ServiceCatalogueHDto cat = new ServiceCatalogueHDto();
    cat.setName("name");
    cat.setMultipleIndicator(null);
    cat.setDisplayOrder(1);
    cat.setProductGroupH(mockProductGroup(2L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setName("pName");
    pCat.setDisplayOrder(1);
    dto.setDueStatusH(DueStatus.DUE_SOON);
    cat.setProductCatalogue(pCat);
    dto.setServiceCatalogueH(cat);
    dto.setLowerRangeDate(new Date());
    dto.setDueDate(new Date());
    dto.setAssignedDate(new Date());
    dto.setId(assetId);
    return dto;
  }

  /**
   * Returns mock service has serviceCatalogue with false multiple indicator.
   *
   * @param id the unique identifier of an asset.
   * @return mock service for an asset.
   */
  private ScheduledServiceHDto mockServiceWithFalseMutipleIndicator(final Long id) {
    Long assetId = id;
    if (assetId == null) {
      assetId = 1L;
    }
    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    ServiceCatalogueHDto cat = new ServiceCatalogueHDto();
    cat.setName("name");
    cat.setMultipleIndicator(Boolean.FALSE);
    cat.setDisplayOrder(1);
    cat.setProductGroupH(mockProductGroup(3L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setName("pName");
    pCat.setDisplayOrder(1);
    dto.setDueStatusH(DueStatus.DUE_SOON);
    cat.setProductCatalogue(pCat);
    dto.setServiceCatalogueH(cat);
    dto.setLowerRangeDate(new Date());
    dto.setDueDate(new Date());
    dto.setAssignedDate(new Date());
    dto.setId(assetId);
    return dto;
  }


  /**
   * Returns mock service has serviceCatalogue with false multiple indicator.
   *
   * @param id the unique identifier of an asset.
   * @return mock service for an asset.
   */
  private ScheduledServiceHDto mockServiceWithMutipleIndicatorNullOccurenceNUmber(final Long id) {
    Long assetId = id;
    if (assetId == null) {
      assetId = 1L;
    }

    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    ServiceCatalogueHDto cat = new ServiceCatalogueHDto();
    cat.setName("name");
    cat.setMultipleIndicator(Boolean.TRUE);
    cat.setDisplayOrder(1);
    cat.setProductGroupH(mockProductGroup(2L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setName("pName");
    pCat.setDisplayOrder(1);
    dto.setDueStatusH(DueStatus.DUE_SOON);
    cat.setProductCatalogue(pCat);
    dto.setServiceCatalogueH(cat);
    dto.setLowerRangeDate(new Date());
    dto.setDueDate(new Date());
    dto.setAssignedDate(new Date());
    dto.setId(assetId);
    dto.setOccurrenceNumber(null);
    return dto;
  }

  /**
   * Returns mock service which does n't have lower range date and upper range date for given
   * assetId.
   *
   * @param id the unique identifier of an asset.
   * @return mock service for an asset.
   */
  private ScheduledServiceHDto mockServiceAssetWithOutRangeDates(final Long id) {
    Long assetId = id;
    if (assetId == null) {
      assetId = 1L;
    }

    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    ServiceCatalogueHDto cat = new ServiceCatalogueHDto();
    cat.setName("name");
    cat.setDisplayOrder(1);
    cat.setProductGroupH(mockProductGroup(2L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setName("pName");
    pCat.setDisplayOrder(1);
    dto.setDueStatusH(DueStatus.DUE_SOON);
    cat.setProductCatalogue(pCat);
    dto.setServiceCatalogueH(cat);
    dto.setDueDate(new Date());
    dto.setAssignedDate(new Date());
    dto.setId(assetId);
    return dto;
  }

  /**
   * Returns mock service which does n't have any date for given assetId.
   *
   * @param id the unique identifier of an asset.
   * @return mock service for an asset.
   */
  private ScheduledServiceHDto mockServiceAssetWithOutDates(final Long id) {
    Long assetId = id;
    if (assetId == null) {
      assetId = 1L;
    }

    final ScheduledServiceHDto dto = new ScheduledServiceHDto();
    ServiceCatalogueHDto cat = new ServiceCatalogueHDto();
    cat.setName("name");
    cat.setDisplayOrder(1);
    cat.setProductGroupH(mockProductGroup(1L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setName("pName");
    pCat.setDisplayOrder(1);
    dto.setDueStatusH(DueStatus.DUE_SOON);
    cat.setProductCatalogue(pCat);
    dto.setServiceCatalogueH(cat);
    dto.setId(assetId);
    return dto;
  }

  /**
   * Returns mock services.
   *
   * @return mock services.
   */
  private List<ScheduledServiceHDto> mockServices() {
    List<ScheduledServiceHDto> services = new ArrayList<>();
    Date today = new Date();

    Date threeMonths = new Date();
    Calendar calender = Calendar.getInstance();
    calender.setTime(today);
    calender.add(Calendar.DATE, 90);
    threeMonths = calender.getTime();

    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    ServiceCatalogueHDto catalogue = new ServiceCatalogueHDto();
    catalogue.setName("name");
    catalogue.setCode("AB");
    catalogue.setDisplayOrder(1);
    catalogue.setProductGroupH(mockProductGroup(1L));
    ProductCatalogueDto pCat = new ProductCatalogueDto();
    pCat.setId(1L);
    pCat.setName("pName");
    pCat.setDisplayOrder(1);
    pCat.setProductType(new LinkResource(ProductTypeEnum.CLASSIFICATION.getId()));
    catalogue.setProductCatalogue(pCat);
    service1.setDueStatusH(DueStatus.DUE_SOON);
    service1.setServiceCatalogueH(catalogue);
    service1.setId(1L);
    service1.setUpperRangeDate(today);
    service1.setLowerRangeDate(today);
    service1.setPostponementDate(today);
    service1.setAssignedDate(today);
    service1.setDueDate(today);
    services.add(service1);

    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    ServiceCatalogueHDto catalogue2 = new ServiceCatalogueHDto();
    catalogue2.setName("name");
    catalogue2.setDisplayOrder(1);
    catalogue2.setProductGroupH(mockProductGroup(3L));
    catalogue2.setCode("DE");
    ProductCatalogueDto pCat2 = new ProductCatalogueDto();
    pCat2.setId(2L);
    pCat2.setName("pName");
    pCat2.setDisplayOrder(1);
    pCat2.setProductType(new LinkResource(ProductTypeEnum.MMS.getId()));
    catalogue2.setProductCatalogue(pCat2);
    service2.setDueStatusH(DueStatus.DUE_SOON);
    service2.setServiceCatalogueH(catalogue2);
    service2.setId(2L);
    service2.setUpperRangeDate(null);
    service2.setLowerRangeDate(null);
    service2.setPostponementDate(null);
    service2.setAssignedDate(null);
    service2.setDueDate(null);
    service2.setOccurrenceNumber(2);
    services.add(service2);

    final ScheduledServiceHDto service3 = new ScheduledServiceHDto();
    ServiceCatalogueHDto catalogue3 = new ServiceCatalogueHDto();
    catalogue3.setName("name");
    catalogue3.setDisplayOrder(1);
    catalogue3.setProductGroupH(mockProductGroup(2L));
    catalogue3.setCode("YZ");
    ProductCatalogueDto pCat3 = new ProductCatalogueDto();
    pCat3.setId(1L);
    pCat3.setName("pName");
    pCat3.setDisplayOrder(1);
    pCat3.setProductType(new LinkResource(ProductTypeEnum.STATUTORY.getId()));
    catalogue3.setProductCatalogue(pCat3);
    service3.setDueStatusH(DueStatus.DUE_SOON);
    service3.setServiceCatalogueH(catalogue3);
    service3.setId(3L);
    service3.setUpperRangeDate(null);
    service3.setLowerRangeDate(null);
    service3.setPostponementDate(null);
    service3.setAssignedDate(null);
    service3.setDueDate(null);
    service3.setOccurrenceNumber(3);
    services.add(service3);

    final ScheduledServiceHDto service4 = new ScheduledServiceHDto();
    ServiceCatalogueHDto catalogue4 = new ServiceCatalogueHDto();
    catalogue4.setName("name");
    catalogue4.setDisplayOrder(1);
    catalogue4.setProductGroupH(mockProductGroup(1L));
    catalogue4.setCode("PQ");
    ProductCatalogueDto pCat4 = new ProductCatalogueDto();
    pCat4.setId(1L);
    pCat4.setName("pName");
    pCat4.setDisplayOrder(1);
    pCat4.setProductType(new LinkResource(ProductTypeEnum.STATUTORY.getId()));
    catalogue4.setProductCatalogue(pCat4);
    service4.setDueStatusH(DueStatus.DUE_SOON);
    service4.setServiceCatalogueH(catalogue4);
    service4.setId(4L);
    service4.setUpperRangeDate(today);
    service4.setLowerRangeDate(null);
    service4.setPostponementDate(today);
    service4.setAssignedDate(today);
    service4.setDueDate(today);
    service4.setOccurrenceNumber(4);
    services.add(service4);

    final ScheduledServiceHDto service5 = new ScheduledServiceHDto();
    service5.setDueStatusH(DueStatus.DUE_SOON);
    service5.setServiceCatalogueH(catalogue3);
    service5.setId(5L);
    service5.setUpperRangeDate(today);
    service5.setLowerRangeDate(null);
    service5.setPostponementDate(today);
    service5.setAssignedDate(today);
    service5.setDueDate(today);
    service5.setOccurrenceNumber(5);
    services.add(service5);

    final ScheduledServiceHDto service6 = new ScheduledServiceHDto();
    service6.setDueStatusH(DueStatus.DUE_SOON);
    service6.setServiceCatalogueH(catalogue);
    service6.setId(1L);
    service6.setUpperRangeDate(today);
    service6.setLowerRangeDate(today);
    service6.setPostponementDate(today);
    service6.setAssignedDate(today);
    service6.setDueDate(threeMonths);
    service6.setOccurrenceNumber(6);
    services.add(service6);
    return services;
  }

  /**
   * Returns mock asset having cfo office with valid email.
   *
   * @param asset the asset model.
   * @return mock OfficeDto.
   */
  private AssetHDto mockOfficeDto(final AssetHDto asset) {
    OfficeDto officeDto = new OfficeDto();
    officeDto.setId(1L);
    officeDto.setEmailAddress("paul@baesystems.com");
    officeDto.setDeleted(Boolean.FALSE);
    asset.setCfoOfficeH(officeDto);
    return asset;
  }

  /**
   * Returns mock product group data.
   *
   * @param id the product type id.
   * @return mock productGrpDTO.
   */
  private ProductGroupDto mockProductGroup(final Long id) {
    ProductGroupDto productGrp = new ProductGroupDto();
    productGrp.setProductType(new LinkResource(id));
    return productGrp;
  }

  /**
   * Returns mock asset with offices reference data.
   *
   * @return mock asset.
   */
  private AssetHDto getAssetHdtoOffices() {
    AssetHDto hDto = new AssetHDto();
    hDto.setId(1L);
    hDto.setLeadImo("1");
    hDto.setName("Asset Name");
    // office
    OfficeDto officeDto = new OfficeDto();
    officeDto.setId(1L);
    officeDto.setEmailAddress("paul@baesystems.com");
    officeDto.setDeleted(Boolean.FALSE);
    hDto.setCfoOfficeH(officeDto);
    return hDto;
  }

  /**
   * Service inner class.
   *
   * @author NAvula
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create survey service.
     *
     * @return service.
     */
    @Override
    @Bean
    public SurveyService surveyService() {
      return new SurveyServiceImpl();
    }

    /**
     * AssetEmblemUtils.
     *
     * @return asset emblem utils.
     */
    @Bean
    public AssetEmblemUtils assetEmblemUtils() {
      return Mockito.spy(new AssetEmblemUtils());
    }
  }
}
