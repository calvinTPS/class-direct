package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.DefectService;
import com.baesystems.ai.lr.cd.service.asset.impl.ActionableItemServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.JobFaultDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FaultCategoryDto;
import com.baesystems.ai.lr.enums.CodicilCategory;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Provides unit test for {@link ActionableItemService}.
 *
 * @author yng
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Response.class, Resources.class, SecurityUtils.class})
@ContextConfiguration(classes = ActionableItemServiceTest.Config.class)
public class ActionableItemServiceTest {
  /**
   * The logger instantiated for {@link CoCServiceTest}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ActionableItemServiceTest.class);

  /**
   * The Spring application context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link ActionableItemService} from {@link ActionableItemServiceTest.Config} class.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   * The {@link JobRetrofitService} from {@link ActionableItemServiceTest.Config} class.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The {@link DefectService} from {@link ActionableItemServiceTest.Config} class.
   */
  @Autowired
  private DefectService defectService;

  /**
   * The notification period value from Spring properties file.
   */
  @Value("${notification.due.status.period}")
  private String notificationPeriodOffSet;

  /**
   * Provides initialization for mock object.
   *
   */
  @Before
  public final void init() {
    reset(context.getBean(AssetRetrofitService.class));
  }

  /**
   * Tests success scenario for get equasis/thetis user and non-equasis/thetis user roles actionable
   * item.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetActionableItem() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    PowerMockito.mockStatic(SecurityUtils.class);
    BDDMockito.given(SecurityUtils.isEquasisThetisUser()).willReturn(false);

    doReturn(mockActionableItemData()).when(retrofitService).getActionableItemDto(Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());

    PageResource<ActionableItemHDto> result = actionableItemService.getActionableItem(null, null, 1L);

    Assert.assertNotNull(result.getContent());
    Assert.assertEquals(Long.valueOf(2L), result.getPagination().getTotalElements());

    // test data output for equasis user, data with statutory type doesn't show
    BDDMockito.given(SecurityUtils.isEquasisThetisUser()).willReturn(true);
    when(retrofitService.getAssetItemDto(Matchers.anyLong(), Matchers.anyLong()))
    .thenReturn(Calls.failure(new IOException("Mock")));

    result = actionableItemService.getActionableItem(null, null, 1L);

    Assert.assertNotNull(result.getContent());
    Assert.assertEquals(Long.valueOf(1L), result.getPagination().getTotalElements());
    Assert.assertEquals(CodicilCategory.AI_STATUTORY.getValue(), result.getContent().get(0).getCategory().getId());

    // test data output for thetis user, data with statutory type doesn't show
    BDDMockito.given(SecurityUtils.isEquasisThetisUser()).willReturn(true);
    result = actionableItemService.getActionableItem(null, null, 1L);

    Assert.assertNotNull(result.getContent());
    Assert.assertEquals(Long.valueOf(1L), result.getPagination().getTotalElements());

  }

  /**
   * Test get actionable item with API call fail.
   *
   * @throws Exception if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetActionableItemException() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(retrofitService.getActionableItemDto(1)).thenReturn(Calls.failure(new IOException("Mocked Exception.")));
    actionableItemService.getActionableItem(null, null, 1L);
  }

  /**
   * Test positive case scenario get actionable item by asset Id and actionable item Id.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetActionableItemByAssetIdActionableItemId() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockSingleActionableItemData()).when(retrofitService)
        .getActionableItemByAssetIdActionableItemId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.failure(new IOException("Mock")));

    final ActionableItemHDto result = actionableItemService.getActionableItemByAssetIdActionableItemId(1L, 1L);

    Assert.assertNotNull(result);
  }

  /**
   * Tests failure scenario get actionable item by asset Id
   * {@link ActionableItemService#getActionableItemByAssetIdActionableItemId(Long, long)} when jobs
   * api fails.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetActionableItemByAssetIdActionableItemIdMultiCondition() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockSingleActionableItemDataWithNullAssetItemId()).when(retrofitService)
        .getActionableItemByAssetIdActionableItemId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.failure(new IOException("Mock")));
    final ActionableItemHDto result = actionableItemService.getActionableItemByAssetIdActionableItemId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNull(result.getJobH());
  }

  /**
   * Test positive case scenario get actionable item by asset Id and defect Id.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetActionableItemByAssetIdDefectId() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockSingleActionableItemData()).when(retrofitService)
        .getActionableItemByAssetIdActionableItemId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockSingleDefectDataWithJobsDefectItemNotNull()).when(defectService)
        .getDefectByAssetIdDefectId(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobsData()));
    when(retrofitService.getAssetItemDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));
    when(retrofitService.getCoCbyDefectAssetId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    final ActionableItemHDto result = actionableItemService.getActionableItemByAssetIdActionableItemId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getDefectH());
    Assert.assertEquals(result.getDefectH().getTitle(), "Defect Title");
    Assert.assertEquals(result.getDefectH().getCategory().getName(), "Defect Category Name");
    Assert.assertNotNull(result.getJobH());
    Assert.assertEquals(result.getJobH().getJobNumber(), "123");
  }

  /**
   * Test get actionable item by assetId and defect Id with null condtion.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetActionableItemByAssetIdDefectIdMultiCondition() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockSingleActionableItemData()).when(retrofitService)
        .getActionableItemByAssetIdActionableItemId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(defectService.getDefectByAssetIdDefectId(Matchers.anyLong(), Matchers.anyLong()))
        .thenThrow(new ClassDirectException(""));

    final ActionableItemHDto result = actionableItemService.getActionableItemByAssetIdActionableItemId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNull(result.getDefectH());
  }

  /**
   * Test get actionable item by asset Id and actionable item Id with API call fail.
   *
   * @throws Exception value.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetActionableItemByAssetIdActionableItemIdException() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(retrofitService.getActionableItemByAssetIdActionableItemId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException("Mocked Exception.")));
    actionableItemService.getActionableItemByAssetIdActionableItemId(1L, 1L);
  }

  /**
   * Tests success scenario to get actionable items with query
   * {@link ActionableItemService#getActionableItemsByQuery(long, CodicilDefectQueryHDto)}.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetActionableItemQuery() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);
    CodicilDefectQueryHDto query = new CodicilDefectQueryHDto();

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockActionableItemDataForQuery()).when(retrofitService)
        .getActionableItemQueryDto(Matchers.any(CodicilDefectQueryHDto.class), Matchers.anyLong());

    List<ActionableItemHDto> result = actionableItemService.getActionableItemsByQuery(1L, query);
    Assert.assertNotNull(result);
    Assert.assertEquals(result.size(), 2);
  }

  /**
   * Tests failure scenario to get actionable items with query
   * {@link ActionableItemService#getActionableItemsByQuery(long, CodicilDefectQueryHDto)} when mast
   * api call fails.
   *
   * @throws Exception if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetActionableItemQueryFail() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);
    CodicilDefectQueryHDto query = new CodicilDefectQueryHDto();

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(retrofitService.getActionableItemQueryDto(Matchers.any(CodicilDefectQueryHDto.class), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException("Mocked Exception.")));

    actionableItemService.getActionableItemsByQuery(1L, query);
  }

  /**
   * Tests {@link ActionableItemService#calculateDueStatus(ActionableItemHDto, LocalDate)}.
   * positive scenario for given due date of actionable items.
   *
   */
  @Test
  public final void calculateDueStatusTest() {
    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    // OVERDUE Status
    // dueStatus should be overDue(Due Date is passed).
    final LocalDate twoDaysAgo = LocalDate.now().minusDays(2L);
    final LocalDate oneMonthago = LocalDate.now().minusMonths(1L);

    // overDue statutory findings.
    final ActionableItemHDto actionableItem1 = new ActionableItemHDto();
    actionableItem1.setDueDate(DateUtils.getLocalDateToDate(twoDaysAgo));
    actionableItems.add(actionableItem1);

    final ActionableItemHDto actionableItem3 = new ActionableItemHDto();
    actionableItem3.setDueDate(DateUtils.getLocalDateToDate(oneMonthago));
    actionableItems.add(actionableItem3);

    final DueStatus status1 = actionableItemService.calculateDueStatus(actionableItem1);
    Assert.assertEquals(DueStatus.OVER_DUE, status1);

    final DueStatus status3 = actionableItemService.calculateDueStatus(actionableItem3);
    Assert.assertEquals(DueStatus.OVER_DUE, status3);

    // IMMINENT Status
    // dueStatus should be imminent.(One month before the Due Date has passed)
    final LocalDate today = LocalDate.now();
    final LocalDate twentynineDaysBefore = LocalDate.now().plusMonths(1L).minusDays(1L);

    // imminent statutory findings.
    final ActionableItemHDto actionableItem = new ActionableItemHDto();
    actionableItem.setDueDate(DateUtils.getLocalDateToDate(today));
    actionableItems.add(actionableItem);

    final ActionableItemHDto finding4 = new ActionableItemHDto();
    finding4.setDueDate(DateUtils.getLocalDateToDate(twentynineDaysBefore));
    actionableItems.add(finding4);

    final DueStatus status = actionableItemService.calculateDueStatus(actionableItem);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    final DueStatus status4 = actionableItemService.calculateDueStatus(finding4);
    Assert.assertEquals(DueStatus.IMMINENT, status4);

    // DUESOON status
    // dueStatus should be duesoon.(Three months before the Due Date has passed)
    final LocalDate eightydaysbefore = LocalDate.now().plusMonths(3L).minusDays(1L);
    final LocalDate oneMonthbefore = LocalDate.now().plusMonths(1L);
    final LocalDate twoMonthsbefore = LocalDate.now().plusMonths(2L);

    // due soon statutory findings.
    final ActionableItemHDto actionableItem5 = new ActionableItemHDto();
    actionableItem5.setDueDate(DateUtils.getLocalDateToDate(oneMonthbefore));
    actionableItem5.setCategory(new LinkResource(5L));
    actionableItems.add(actionableItem5);

    final ActionableItemHDto actionableItem7 = new ActionableItemHDto();
    actionableItem7.setDueDate(DateUtils.getLocalDateToDate(eightydaysbefore));
    actionableItem7.setCategory(new LinkResource(5L));
    actionableItems.add(actionableItem7);

    final ActionableItemHDto actionableItem2 = new ActionableItemHDto();
    actionableItem2.setDueDate(DateUtils.getLocalDateToDate(twoMonthsbefore));
    actionableItem2.setCategory(new LinkResource(5L));
    actionableItems.add(actionableItem2);


    final DueStatus status2 = actionableItemService.calculateDueStatus(actionableItem2);
    Assert.assertEquals(DueStatus.DUE_SOON, status2);

    final DueStatus status5 = actionableItemService.calculateDueStatus(actionableItem5);
    Assert.assertEquals(DueStatus.DUE_SOON, status5);

    final DueStatus status7 = actionableItemService.calculateDueStatus(actionableItem7);
    Assert.assertEquals(DueStatus.DUE_SOON, status7);

    // NOTDUE Status
    // dueStatus should be notDue.(More than three months before the Due Date)
    final LocalDate threeMonthsBefore = LocalDate.now().plusMonths(3L).plusDays(1L);
    final LocalDate fourMonthsBefore = LocalDate.now().plusMonths(4L);


    // not due statutory findings.
    final ActionableItemHDto actionableItem8 = new ActionableItemHDto();
    actionableItem8.setDueDate(DateUtils.getLocalDateToDate(threeMonthsBefore));
    actionableItems.add(actionableItem8);

    final ActionableItemHDto actionableItem6 = new ActionableItemHDto();
    actionableItem6.setDueDate(DateUtils.getLocalDateToDate(fourMonthsBefore));
    actionableItems.add(actionableItem6);

    // due date null.default due status notDue.
    final ActionableItemHDto actionableItem9 = new ActionableItemHDto();
    actionableItem9.setDueDate(null);
    actionableItems.add(actionableItem9);

    final DueStatus status6 = actionableItemService.calculateDueStatus(actionableItem6);
    Assert.assertEquals(DueStatus.NOT_DUE, status6);

    final DueStatus status8 = actionableItemService.calculateDueStatus(actionableItem8);
    Assert.assertEquals(DueStatus.NOT_DUE, status8);

    final DueStatus status9 = actionableItemService.calculateDueStatus(actionableItem9);
    Assert.assertEquals(DueStatus.NOT_DUE, status9);

  }
  /**
   * Returns asset item mock data.
   *
   * @return the asset item mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<LazyItemHDto> mockAssetItemData() {
    final LazyItemHDto result = new LazyItemHDto();
    result.setId(1L);

    final Response<LazyItemHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(result);

    final Call<LazyItemHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns single defect mock data with job and asset item info.
   *
   * @return the single defect mock data.
   */
  private DefectHDto mockSingleDefectDataWithJobsDefectItemNotNull() {
    // data with jobs and items
    final DefectHDto data = new DefectHDto();
    data.setId(1L);
    data.setTitle("Defect Title");

    final List<JobFaultDto> jobData = new ArrayList<>();
    final JobFaultDto job = new JobFaultDto();
    job.setId(1L);
    job.setJob(new LinkResource(1L));
    job.getJob().setId(1L);
    jobData.add(job);
    data.setJobs(jobData);

    final List<DefectItemDto> itemData = new ArrayList<>();
    final DefectItemDto item = new DefectItemDto();
    item.setId(1L);
    item.setItem(new ItemLightDto());
    item.getItem().setId(1L);
    itemData.add(item);
    data.setItems(itemData);

    data.setCategory(mockFaultCategory());
    return data;
  }

  /**
   * Returns fault category mock data.
   *
   * @return the fault category mock data.
   */
  private FaultCategoryDto mockFaultCategory() {
    final FaultCategoryDto faultCategory = new FaultCategoryDto();
    faultCategory.setId(1L);
    faultCategory.setDeleted(false);
    faultCategory.setName("Defect Category Name");
    faultCategory.setDescription("Defect Category Description");
    faultCategory.setFaultGroup(new LinkResource(1L));
    faultCategory.getFaultGroup().setId(1L);
    faultCategory.setProductCatalogue(new LinkResource(1L));
    faultCategory.getProductCatalogue().setId(1L);
    faultCategory.setCodicilCategory(new LinkResource(1L));
    faultCategory.getCodicilCategory().setId(1L);
    return faultCategory;
  }

  /**
   * Returns actionable item mock data.
   *
   * @return the actionable item mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<List<ActionableItemHDto>> mockActionableItemData() {
    final List<ActionableItemHDto> actionableItemResponse = new ArrayList<ActionableItemHDto>();

    // data 1: object with asset item, and non statutory category
    final ActionableItemHDto data1 = new ActionableItemHDto();
    data1.setId(1L);
    data1.setAssetItem(new NamedLinkResource(1L));
    data1.setCategory(new LinkResource(CodicilCategory.AI_CLASS.getValue()));
    final CodicilCategoryHDto data1CategoryH = new CodicilCategoryHDto();
    data1CategoryH.setId(CodicilCategory.AI_CLASS.getValue());
    data1CategoryH.setName("Non-Statutory");
    data1.setCategoryH(data1CategoryH);
    data1.setJob(new LinkResource(1L));
    data1.setDueDate(new Date());
    actionableItemResponse.add(data1);

    // data 2: object with statutory category
    final ActionableItemHDto data2 = new ActionableItemHDto();
    data2.setId(2L);
    data2.setCategory(new LinkResource(CodicilCategory.AI_STATUTORY.getValue()));
    final CodicilCategoryHDto data2CategoryH = new CodicilCategoryHDto();
    data2CategoryH.setId(CodicilCategory.AI_STATUTORY.getValue());
    data2CategoryH.setName("Statutory");
    data1.setDueDate(new Date());
    actionableItemResponse.add(data2);

    final Response<List<ActionableItemHDto>> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(actionableItemResponse);

    final Call<List<ActionableItemHDto>> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns actionable item mock data with open status list.
   *
   * @return the actionable item mock data.
   */
  private Call<List<ActionableItemHDto>> mockActionableItemDataForQuery() {
    final List<ActionableItemHDto> actionableItemResponse = new ArrayList<ActionableItemHDto>();

    // data 1: object with asset item, and non statutory category
    final ActionableItemHDto data1 = new ActionableItemHDto();
    data1.setId(1L);
    data1.setAssetItem(new NamedLinkResource(1L));
    data1.setCategory(new LinkResource(CodicilCategory.AI_CLASS.getValue()));
    final CodicilCategoryHDto data1CategoryH = new CodicilCategoryHDto();
    data1CategoryH.setId(CodicilCategory.AI_CLASS.getValue());
    data1CategoryH.setName("Non-Statutory");
    data1.setCategoryH(data1CategoryH);
    data1.setJob(new LinkResource(1L));
    data1.setDueDate(new Date());
    actionableItemResponse.add(data1);
    CodicilStatusHDto statusHObject = new CodicilStatusHDto();
    statusHObject.setId(3L);
    data1.setStatusH(statusHObject);

    // data 2: object with statutory category
    final ActionableItemHDto data2 = new ActionableItemHDto();
    data2.setId(2L);
    data2.setCategory(new LinkResource(CodicilCategory.AI_STATUTORY.getValue()));
    final CodicilCategoryHDto data2CategoryH = new CodicilCategoryHDto();
    data2CategoryH.setId(CodicilCategory.AI_STATUTORY.getValue());
    data2CategoryH.setName("Statutory");
    data2.setCategoryH(data2CategoryH);
    statusHObject.setId(4L);
    data1.setStatusH(statusHObject);
    actionableItemResponse.add(data2);

    final Response<List<ActionableItemHDto>> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(actionableItemResponse);

    final Call<List<ActionableItemHDto>> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns single actionable item mock data.
   *
   * @return the single actionable item mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<ActionableItemHDto> mockSingleActionableItemData() {
    final ActionableItemHDto actionableItemResponse = new ActionableItemHDto();
    actionableItemResponse.setId(1L);
    actionableItemResponse.setDefect(new LinkResource(1L));
    actionableItemResponse.getDefect().setId(1L);
    actionableItemResponse.setJob(new LinkResource(1L));
    actionableItemResponse.setAssetItem(new NamedLinkResource(1L));

    final Response<ActionableItemHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(actionableItemResponse);

    final Call<ActionableItemHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns actionable item mock data with null asset item Id.
   *
   * @return the actionable item mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<ActionableItemHDto> mockSingleActionableItemDataWithNullAssetItemId() {
    final ActionableItemHDto actionableItemResponse = new ActionableItemHDto();
    actionableItemResponse.setId(1L);

    final Response<ActionableItemHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(actionableItemResponse);

    final Call<ActionableItemHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mocks Jobs Data.
   *
   * @return call value.
   * @throws Exception if any error in formating date.
   */
  private JobHDto mockJobsData() throws Exception {
    JobHDto job = new JobHDto();
    job.setId(1L);
    job.setJobNumber("123");
    return job;

  }

  /**
   * Provides Spring in-class configurations.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public ActionableItemService actionableItemService() {
      return PowerMockito.spy(new ActionableItemServiceImpl());
    }
  }
}
