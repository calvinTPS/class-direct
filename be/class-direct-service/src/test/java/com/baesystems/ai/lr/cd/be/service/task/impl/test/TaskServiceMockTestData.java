package com.baesystems.ai.lr.cd.be.service.task.impl.test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.CheckListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemTypeHDto;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.enums.ResolutionStatus;
import com.baesystems.ai.lr.enums.WorkItemType;

/**
 * Provides mock test data for task service .
 *
 * @author sbollu
 *
 */
public class TaskServiceMockTestData {

  /**
   * Returns mock list of unsorted checklist.
   *
   * @return mock list of unsorted checklist.
   */
 protected List<WorkItemLightHDto> mockUnSortedChecklistData() {

    List<ChecklistGroupDto> checkListGroupDtoList = mockGroupData();
     // checklist subgroup mock data
     ChecklistSubgroupDto subgrOneDto = new ChecklistSubgroupDto();
     subgrOneDto.setId(1L);
     subgrOneDto.setDisplayOrder(1);
     subgrOneDto.setName("Documentation");

     ChecklistSubgroupDto subgrTwoDto = new ChecklistSubgroupDto();
     subgrTwoDto.setId(2L);
     subgrTwoDto.setDisplayOrder(2);
     subgrTwoDto.setName("Certificate-Naval");

     ChecklistSubgroupDto subgrFourDto = new ChecklistSubgroupDto();
     subgrFourDto.setId(4L);
     subgrFourDto.setDisplayOrder(4);
     subgrFourDto.setName("Certified Lifeboatmen");

    // checklist group mock data
    final List<WorkItemLightHDto> ckList = new ArrayList<>();
    // group1 and subgroup1
    final WorkItemLightHDto firstCK = new WorkItemLightHDto();
    firstCK.setId(1L);
    firstCK.setReferenceCode("A1");
    firstCK.setTaskNumber("000001-XXXX-000004");
    firstCK.setChecklist(new LinkResource(1L));
    firstCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto firstCkDTO = new CheckListHDto();
    firstCkDTO.setChecklistGroup(new LinkResource(1L));
    firstCkDTO.setChecklistSubgroup(new LinkResource(1L));
    firstCkDTO.setGroupDto(checkListGroupDtoList.get(0));
    firstCkDTO.setSubGroupDto(subgrOneDto);
    firstCK.setChecklistH(firstCkDTO);

    // group1 and subgroup2
    final WorkItemLightHDto secondCK = new WorkItemLightHDto();
    secondCK.setId(2L);
    secondCK.setReferenceCode("A2");
    secondCK.setTaskNumber("000001-XXXX-000003");
    secondCK.setChecklist(new LinkResource(2L));
    secondCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto secondCkDTO = new CheckListHDto();
    secondCkDTO.setChecklistGroup(new LinkResource(1L));
    secondCkDTO.setChecklistSubgroup(new LinkResource(2L));
    secondCkDTO.setGroupDto(checkListGroupDtoList.get(0));
    secondCkDTO.setSubGroupDto(subgrTwoDto);
    secondCK.setChecklistH(secondCkDTO);

    // group2 and subgroup2
    final WorkItemLightHDto thirdCK = new WorkItemLightHDto();
    thirdCK.setId(3L);
    thirdCK.setReferenceCode("A3");
    thirdCK.setTaskNumber("000001-XXXX-000001");
    thirdCK.setChecklist(new LinkResource(3L));
    thirdCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto thirdCkDTO = new CheckListHDto();
    thirdCkDTO.setChecklistGroup(new LinkResource(2L));
    thirdCkDTO.setChecklistSubgroup(new LinkResource(2L));
    thirdCkDTO.setGroupDto(checkListGroupDtoList.get(1));
    thirdCkDTO.setSubGroupDto(subgrTwoDto);
    thirdCK.setChecklistH(thirdCkDTO);

    // group2 and subgroup2
    final WorkItemLightHDto fourthCK = new WorkItemLightHDto();
    fourthCK.setId(4L);
    fourthCK.setReferenceCode("A4");
    fourthCK.setTaskNumber("000002-XXXX-000005");
    fourthCK.setChecklist(new LinkResource(3L));
    fourthCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto fourthCkDTO = new CheckListHDto();
    fourthCkDTO.setChecklistGroup(new LinkResource(2L));
    fourthCkDTO.setChecklistSubgroup(new LinkResource(2L));
    fourthCkDTO.setGroupDto(checkListGroupDtoList.get(1));
    fourthCkDTO.setSubGroupDto(subgrTwoDto);
    fourthCK.setChecklistH(fourthCkDTO);

    // group3 and subgroup1
    final WorkItemLightHDto fifthCK = new WorkItemLightHDto();
    fifthCK.setId(5L);
    fifthCK.setReferenceCode("A5");
    fifthCK.setTaskNumber("000002-XXXX-000003");
    fifthCK.setChecklist(new LinkResource(3L));
    fifthCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto fifthCkDTO = new CheckListHDto();
    fifthCkDTO.setChecklistGroup(new LinkResource(3L));
    fifthCkDTO.setChecklistSubgroup(new LinkResource(1L));
    fifthCkDTO.setGroupDto(checkListGroupDtoList.get(2));
    fifthCkDTO.setSubGroupDto(subgrOneDto);
    fifthCK.setChecklistH(fifthCkDTO);

    // group1 and subgroup4
    final WorkItemLightHDto sixthCK = new WorkItemLightHDto();
    sixthCK.setId(6L);
    sixthCK.setReferenceCode("A6");
    sixthCK.setTaskNumber("000001-XXXX-000008");
    sixthCK.setChecklist(new LinkResource(2L));
    sixthCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto sixthCKDTO = new CheckListHDto();
    sixthCKDTO.setChecklistGroup(new LinkResource(1L));
    sixthCKDTO.setChecklistSubgroup(new LinkResource(2L));
    sixthCKDTO.setGroupDto(checkListGroupDtoList.get(0));
    sixthCKDTO.setSubGroupDto(subgrFourDto);
    sixthCK.setChecklistH(sixthCKDTO);

    // group3 and subgroup1
    final WorkItemLightHDto seventhCK = new WorkItemLightHDto();
    seventhCK.setId(7L);
    seventhCK.setReferenceCode("A7");
    seventhCK.setTaskNumber("000002-XXXX-000001");
    seventhCK.setChecklist(new LinkResource(3L));
    seventhCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto seventhCkDTO = new CheckListHDto();
    seventhCkDTO.setChecklistGroup(new LinkResource(3L));
    seventhCkDTO.setChecklistSubgroup(new LinkResource(1L));
    seventhCkDTO.setGroupDto(checkListGroupDtoList.get(2));
    seventhCkDTO.setSubGroupDto(subgrOneDto);
    seventhCK.setChecklistH(seventhCkDTO);

    // group3 and subgroup1
    final WorkItemLightHDto eighthCK = new WorkItemLightHDto();
    eighthCK.setId(8L);
    eighthCK.setReferenceCode("A8");
    eighthCK.setTaskNumber("000002-XXXX-000001");
    eighthCK.setChecklist(new LinkResource(3L));
    eighthCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto eighthCKDTO = new CheckListHDto();
    eighthCKDTO.setChecklistGroup(new LinkResource(3L));
    eighthCKDTO.setChecklistSubgroup(new LinkResource(1L));
    eighthCKDTO.setGroupDto(checkListGroupDtoList.get(2));
    eighthCKDTO.setSubGroupDto(subgrOneDto);
    eighthCK.setChecklistH(eighthCKDTO);

    ckList.add(firstCK);
    ckList.add(secondCK);
    ckList.add(thirdCK);
    ckList.add(fourthCK);
    ckList.add(fifthCK);
    ckList.add(sixthCK);
    ckList.add(seventhCK);
    ckList.add(eighthCK);
    return ckList;
  }

 /**
  * Returns mock list of groupDate List.
  *
  * @return mock list of groupDate.
  */
 private List<ChecklistGroupDto> mockGroupData() {
      List<ChecklistGroupDto> checkListGroupDtoList =  new ArrayList<ChecklistGroupDto>();
      ChecklistGroupDto grOneDto = new ChecklistGroupDto();
      grOneDto.setId(1L);
      grOneDto.setDisplayOrder(1);
      grOneDto.setName("Cargo Pump Room");
      checkListGroupDtoList.add(grOneDto);

      ChecklistGroupDto grTwoDto = new ChecklistGroupDto();
      grTwoDto.setId(2L);
      grTwoDto.setDisplayOrder(2);
      grTwoDto.setName("Survey Preparation");
      checkListGroupDtoList.add(grTwoDto);

      ChecklistGroupDto grThreeDto = new ChecklistGroupDto();
      grThreeDto.setId(3L);
      grThreeDto.setDisplayOrder(3);
      grThreeDto.setName("Accommodation");
      checkListGroupDtoList.add(grThreeDto);
      return checkListGroupDtoList;
 }

  /**
   * Returns mock list of checklist's.
   *
   * @return mock list of checklist which has null checklist Id.
   */
  protected List<WorkItemLightHDto> mockChecklistDataWithNullChecklistId() {

    final Date date = new Date();


    final List<WorkItemLightHDto> ckList = new ArrayList<>();
    // group1 and subgroup1
    final WorkItemLightHDto groupOneCK = new WorkItemLightHDto();
    groupOneCK.setId(1L);
    groupOneCK.setReferenceCode("A1");
    groupOneCK.setName("task 1");
    groupOneCK.setDueDate(date);
    groupOneCK.setChecklist(new LinkResource(1L));
    groupOneCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto grOneCkDTO = new CheckListHDto();
    grOneCkDTO.setChecklistGroup(new LinkResource(1L));
    grOneCkDTO.setChecklistSubgroup(new LinkResource(1L));

    ChecklistGroupDto grOneDto = new ChecklistGroupDto();
    grOneDto.setId(1L);
    grOneDto.setName("GROUP ONE");
    grOneCkDTO.setGroupDto(grOneDto);

    ChecklistSubgroupDto subgrOneDto = new ChecklistSubgroupDto();
    subgrOneDto.setId(1L);
    subgrOneDto.setName("GROUP ONE SUBGROUP ONE");
    grOneCkDTO.setSubGroupDto(subgrOneDto);
    groupOneCK.setChecklistH(grOneCkDTO);

    // group2 and subgroup2
    final WorkItemLightHDto groupTwoSubTwoCK = new WorkItemLightHDto();
    groupTwoSubTwoCK.setId(3L);
    groupTwoSubTwoCK.setReferenceCode("A3");
    groupTwoSubTwoCK.setName("task 3");
    groupTwoSubTwoCK.setDueDate(date);
    groupTwoSubTwoCK.setChecklist(new LinkResource(3L));
    groupTwoSubTwoCK.setWorkItemTypeH(mockCheckList());
    CheckListHDto grTwosubTwoCkDTO = new CheckListHDto();
    grTwosubTwoCkDTO.setChecklistGroup(new LinkResource(2L));
    grTwosubTwoCkDTO.setChecklistSubgroup(new LinkResource(2L));

    ChecklistGroupDto grTwosubTwoDto = new ChecklistGroupDto();
    grTwosubTwoDto.setId(2L);
    grTwosubTwoDto.setName("GROUP TWO");
    grTwosubTwoCkDTO.setGroupDto(grTwosubTwoDto);

    ChecklistSubgroupDto subgrTwo2Dto = new ChecklistSubgroupDto();
    subgrTwo2Dto.setId(2L);
    subgrTwo2Dto.setName("GROUP TWO SUBGROUP TWO");
    grTwosubTwoCkDTO.setSubGroupDto(subgrTwo2Dto);
    groupTwoSubTwoCK.setChecklistH(grTwosubTwoCkDTO);


    // CheckList Id is null
    final WorkItemLightHDto ckIdNull = new WorkItemLightHDto();
    ckIdNull.setId(4L);
    ckIdNull.setReferenceCode("A4");
    ckIdNull.setName("task 4");
    ckIdNull.setDueDate(date);
    ckIdNull.setChecklist(null);
    ckIdNull.setWorkItemTypeH(mockCheckList());

    ckList.add(groupOneCK);
    ckList.add(groupTwoSubTwoCK);
    ckList.add(ckIdNull);
    return ckList;

  }


  /**
   * Returns mock empty checklist.
   *
   * @return mock empty checklist.
   */
  protected List<WorkItemLightHDto> mockEmptyChecklist() {
    final List<WorkItemLightHDto> ckList = new ArrayList<>();
    return ckList;
  }


  /**
   * Returns mock work item type DTO with checklist as workItem.
   *
   * @return mock work item type DTO.
   */
  protected WorkItemTypeHDto mockCheckList() {
    final WorkItemTypeHDto type = new WorkItemTypeHDto();
    type.setId(WorkItemType.CHECKLIST.value());
    type.setName(WorkItemType.CHECKLIST.name());
    return type;
  }

  /**
   * Returns mock work item type DTO with task as work item.
   *
   * @return mock work item type DTO.
   */
  protected WorkItemTypeHDto mockTask() {
    final WorkItemTypeHDto type = new WorkItemTypeHDto();
    type.setId(WorkItemType.TASK.value());
    type.setName(WorkItemType.TASK.name());
    return type;
  }

  /**
   * Returns list of continuous mock services.
   *
   * @return list of continuous mock services.
   */
  protected List<WorkItemLightHDto> mockContinuousServices() {
    final Date date = new Date();
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    // Other Tasks
    final WorkItemLightHDto task6 = new WorkItemLightHDto();
    task6.setPmsApplicable(false);
    task6.setPmsCredited(Boolean.TRUE);
    task6.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task6.setDueDate(date);
    task6.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task6.setScheduledService(new LinkResource(1L));
    tasks.add(task6);
    // for resolution status code X pms credited
    final WorkItemLightHDto task7 = new WorkItemLightHDto();
    task7.setPmsApplicable(false);
    task7.setPmsCredited(Boolean.TRUE);
    task7.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task7.setResolutionStatus(new LinkResource(ResolutionStatus.COMPLETE.getValue()));
    task7.setDueDate(date);
    task7.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task7.setScheduledService(new LinkResource(1L));
    tasks.add(task7);
    // for resolution status code X and pms not credited
    final WorkItemLightHDto task12 = new WorkItemLightHDto();
    task12.setPmsApplicable(false);
    task12.setPmsCredited(Boolean.FALSE);
    task12.setResolutionStatus(new LinkResource(ResolutionStatus.COMPLETE.getValue()));
    task12.setDueDate(date);
    task12.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task12.setScheduledService(new LinkResource(1L));
    tasks.add(task12);
    // for resolution status code X and pms null
    final WorkItemLightHDto task16 = new WorkItemLightHDto();
    task16.setPmsApplicable(false);
    task16.setResolutionStatus(new LinkResource(ResolutionStatus.COMPLETE.getValue()));
    task16.setDueDate(date);
    task16.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task16.setScheduledService(new LinkResource(1L));
    tasks.add(task16);
    // task for resolution status code W pms credited
    final WorkItemLightHDto task8 = new WorkItemLightHDto();
    task8.setPmsApplicable(false);
    task8.setPmsCredited(Boolean.TRUE);
    task8.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task8.setResolutionStatus(new LinkResource(ResolutionStatus.WAIVED.getValue()));
    task8.setDueDate(date);
    task8.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task8.setScheduledService(new LinkResource(1L));
    tasks.add(task8);
    // task for resolution status code W and pms not credited
    final WorkItemLightHDto task11 = new WorkItemLightHDto();
    task11.setPmsApplicable(false);
    task11.setPmsCredited(Boolean.FALSE);
    task11.setResolutionStatus(new LinkResource(ResolutionStatus.WAIVED.getValue()));
    task11.setDueDate(date);
    task11.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task11.setScheduledService(new LinkResource(1L));
    tasks.add(task11);
    // task for resolution status code W and pms null
    final WorkItemLightHDto task17 = new WorkItemLightHDto();
    task17.setPmsApplicable(false);
    task17.setResolutionStatus(new LinkResource(ResolutionStatus.WAIVED.getValue()));
    task17.setDueDate(date);
    task17.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task17.setScheduledService(new LinkResource(1L));
    tasks.add(task17);
    // task for resolution status code C
    final WorkItemLightHDto task9 = new WorkItemLightHDto();
    task9.setPmsApplicable(true);
    task9.setPmsCredited(Boolean.TRUE);
    task9.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task9.setResolutionStatus(new LinkResource(ResolutionStatus.CONFIRMATORY_CHECK.getValue()));
    task9.setDueDate(date);
    task9.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task9.setScheduledService(new LinkResource(1L));
    tasks.add(task9);
    // task for resolution status code C and pms not credited
    final WorkItemLightHDto task10 = new WorkItemLightHDto();
    task10.setPmsApplicable(true);
    task10.setPmsCredited(Boolean.FALSE);
    task10.setResolutionStatus(new LinkResource(ResolutionStatus.CONFIRMATORY_CHECK.getValue()));
    task10.setDueDate(date);
    task10.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task10.setScheduledService(new LinkResource(1L));
    tasks.add(task10);
    // task for resolution status code C and pms null
    final WorkItemLightHDto task18 = new WorkItemLightHDto();
    task18.setPmsApplicable(true);
    task18.setPmsCredited(Boolean.FALSE);
    task18.setResolutionStatus(new LinkResource(ResolutionStatus.CONFIRMATORY_CHECK.getValue()));
    task18.setDueDate(date);
    task18.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task18.setScheduledService(new LinkResource(1L));
    tasks.add(task18);
    // for resolution status code N(Not Applicable) and pms not credited
    final WorkItemLightHDto task13 = new WorkItemLightHDto();
    task13.setPmsApplicable(false);
    task13.setPmsCredited(Boolean.FALSE);
    task13.setResolutionStatus(new LinkResource(ResolutionStatus.NOT_APPLICABLE.getValue()));
    task13.setDueDate(date);
    task13.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task13.setScheduledService(new LinkResource(1L));
    tasks.add(task13);
    // for resolution status code N(Not Applicable) and pms credited
    final WorkItemLightHDto task14 = new WorkItemLightHDto();
    task14.setPmsApplicable(false);
    task14.setPmsCredited(Boolean.TRUE);
    task14.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task14.setResolutionStatus(new LinkResource(ResolutionStatus.NOT_APPLICABLE.getValue()));
    task14.setDueDate(date);
    task14.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task14.setScheduledService(new LinkResource(1L));
    tasks.add(task14);
    // for resolution status code N(Not Applicable) and pms null
    final WorkItemLightHDto task15 = new WorkItemLightHDto();
    task15.setPmsApplicable(false);
    task15.setResolutionStatus(new LinkResource(ResolutionStatus.NOT_APPLICABLE.getValue()));
    task15.setDueDate(date);
    task15.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task15.setScheduledService(new LinkResource(1L));
    tasks.add(task15);

    return tasks;

  }


  /**
   * Returns mock listDto consist of task list.
   *
   * @return mock tasklistDto.
   */
  protected WorkItemLightListDto workItemList() {

    final Date currentDate = DateUtils.getLocalDateToDate(LocalDate.now());

    WorkItemLightListDto workItemLightListDto = new WorkItemLightListDto();
    List<WorkItemLightDto> workItemLightDtos = new ArrayList<>();
    WorkItemLightDto workItemLightDto = new WorkItemLightDto();
    workItemLightDto.setId(1L);
    workItemLightDto.setUpdatedBy("LONLBZ");
    workItemLightDto.setUpdatedDate(currentDate);
    workItemLightDto.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto);

    WorkItemLightDto workItemLightDto1 = new WorkItemLightDto();
    workItemLightDto1.setId(2L);
    workItemLightDto1.setUpdatedBy("LONLBZ");
    workItemLightDto1.setUpdatedDate(currentDate);
    workItemLightDto1.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto1);
    workItemLightListDto.setTasks(workItemLightDtos);


    return workItemLightListDto;
  }

  /**
   * Returns list of mock services.
   *
   * @return list of mock services.
   */
  protected List<ScheduledServiceHDto> mockServices() {
    ServiceCatalogueHDto hdto = new ServiceCatalogueHDto();
    hdto.setId(1L);
    hdto.setContinuousIndicator(Boolean.TRUE);
    final List<ScheduledServiceHDto> services = new ArrayList<>();
    final ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setId(1L);

    service.setServiceCatalogueH(hdto);
    services.add(service);

    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setId(2L);
    ServiceCatalogueHDto hdto1 = new ServiceCatalogueHDto();
    hdto1.setId(2L);
    hdto1.setContinuousIndicator(Boolean.FALSE);
    service1.setServiceCatalogueH(hdto1);
    services.add(service1);

    final ScheduledServiceHDto conService = new ScheduledServiceHDto();
    conService.setId(3L);
    conService.setServiceCatalogueH(hdto);
    services.add(conService);

    return services;
  }



  /**
   * Returns list of non continuous mock services.
   *
   * @return list of non continuous mock services.
   */
  protected List<WorkItemLightHDto> mockNonContinuousServices() {
    final Date date = new Date();
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setReferenceCode("A1");
    task.setName("task 1");
    task.setPmsApplicable(false);
    task.setDueDate(date);
    task.setWorkItemTypeH(mockTask());
    task.setScheduledService(new LinkResource(2L));
    tasks.add(task);

    final WorkItemLightHDto task1 = new WorkItemLightHDto();
    task1.setReferenceCode("A1");
    task1.setName("task 1");
    task1.setPmsApplicable(true);
    task1.setDueDate(date);
    task1.setWorkItemTypeH(mockTask());
    task1.setScheduledService(new LinkResource(2L));
    tasks.add(task1);

    final WorkItemLightHDto task2 = new WorkItemLightHDto();
    task2.setReferenceCode("A1");
    task2.setName("task 1");
    task2.setPmsApplicable(true);
    task2.setPmsCredited(Boolean.FALSE);
    task2.setDueDate(date);
    task2.setWorkItemTypeH(mockTask());
    task2.setScheduledService(new LinkResource(2L));
    tasks.add(task2);
    return tasks;
  }


  /**
   * Returns list of tasks has continuous services.
   *
   * @return list of tasks has continuous services.
   */
  protected List<WorkItemLightHDto> mockTaskshasContinuousServices() {
    final Date date = new Date();
    // task not started ResolutionStatus and PmsCredited are null
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setId(1L);
    task.setPmsApplicable(false);
    task.setDueDate(date);
    task.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task.setScheduledService(new LinkResource(1L));
    tasks.add(task);

    // task not started ResolutionStatus = null and PmsCredited false
    final WorkItemLightHDto task4 = new WorkItemLightHDto();
    task4.setId(2L);
    task4.setPmsApplicable(false);
    task4.setDueDate(date);
    task4.setPmsCredited(Boolean.FALSE);
    task4.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task4.setScheduledService(new LinkResource(1L));
    tasks.add(task4);

    // task postponed not credited (pmsCredited = null)
    final WorkItemLightHDto task1 = new WorkItemLightHDto();
    task1.setId(3L);
    task1.setPmsApplicable(false);
    task1.setResolutionStatus(new LinkResource(ResolutionStatus.POSTPONED.getValue()));
    task1.setDueDate(date);
    task1.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task1.setScheduledService(new LinkResource(1L));
    tasks.add(task1);

    // task postponed not credited (pmsCredited = false)
    final WorkItemLightHDto task3 = new WorkItemLightHDto();
    task3.setId(4L);
    task3.setPmsApplicable(false);
    task3.setResolutionStatus(new LinkResource(ResolutionStatus.POSTPONED.getValue()));
    task3.setPmsCredited(Boolean.FALSE);
    task3.setDueDate(date);
    task3.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task3.setScheduledService(new LinkResource(1L));
    tasks.add(task3);

    // task pms'ed not credited (pmsCredited false)
    final WorkItemLightHDto task2 = new WorkItemLightHDto();
    task2.setId(5L);
    task2.setPmsApplicable(true);
    task2.setPmsCredited(Boolean.FALSE);
    task2.setDueDate(date);
    task2.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task2.setScheduledService(new LinkResource(1L));
    tasks.add(task2);


    // task pms'ed not credited (pmsCredited null)
    final WorkItemLightHDto task5 = new WorkItemLightHDto();
    task5.setId(6L);
    task5.setPmsApplicable(true);
    task5.setDueDate(date);
    task5.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task5.setScheduledService(new LinkResource(1L));
    tasks.add(task5);


    // Other Tasks
    final WorkItemLightHDto task6 = new WorkItemLightHDto();
    task6.setId(7L);
    task6.setPmsApplicable(false);
    task6.setPmsCredited(Boolean.TRUE);
    task6.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task6.setDueDate(date);
    task6.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task6.setScheduledService(new LinkResource(1L));
    tasks.add(task6);

    final WorkItemLightHDto task7 = new WorkItemLightHDto();
    task7.setId(8L);
    task7.setPmsApplicable(false);
    task7.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task7.setResolutionStatus(new LinkResource(ResolutionStatus.COMPLETE.getValue()));
    task7.setDueDate(date);
    task7.setWorkItemTypeH(mockTask());
    // Service is Continuous service
    task7.setScheduledService(new LinkResource(1L));
    tasks.add(task7);
    return tasks;

  }

  /**
   * Returns list of tasks for multiple services.
   *
   * @return list of tasks for multiple services.
   */
  protected List<WorkItemLightHDto> mockTasksForMultiServices() {
    final Date date = new Date();

    // task not started ResolutionStatus and PmsCredited are null
    final List<WorkItemLightHDto> taskList = new ArrayList<>();
    final WorkItemLightHDto svc1NotStartedTask = new WorkItemLightHDto();
    svc1NotStartedTask.setId(1L);
    svc1NotStartedTask.setPmsApplicable(false);
    svc1NotStartedTask.setDueDate(date);
    svc1NotStartedTask.setPmsCredited(Boolean.FALSE);
    svc1NotStartedTask.setWorkItemTypeH(mockTask());
    svc1NotStartedTask.setScheduledService(new LinkResource(1L));
    taskList.add(svc1NotStartedTask);

    // task postponed not credited (pmsCredited = null)
    final WorkItemLightHDto svc1PostponedNotCreditedTask = new WorkItemLightHDto();
    svc1PostponedNotCreditedTask.setId(3L);
    svc1PostponedNotCreditedTask.setPmsApplicable(false);
    svc1PostponedNotCreditedTask.setResolutionStatus(new LinkResource(ResolutionStatus.POSTPONED.getValue()));
    svc1PostponedNotCreditedTask.setDueDate(date);
    svc1PostponedNotCreditedTask.setWorkItemTypeH(mockTask());
    svc1PostponedNotCreditedTask.setScheduledService(new LinkResource(1L));
    taskList.add(svc1PostponedNotCreditedTask);

    // task postponed not credited (pmsCredited = null)
    final WorkItemLightHDto svc2PostponedNotCreditedTask = new WorkItemLightHDto();
    svc2PostponedNotCreditedTask.setId(4L);
    svc2PostponedNotCreditedTask.setPmsApplicable(false);
    svc2PostponedNotCreditedTask.setResolutionStatus(new LinkResource(ResolutionStatus.POSTPONED.getValue()));
    svc2PostponedNotCreditedTask.setDueDate(date);
    svc2PostponedNotCreditedTask.setWorkItemTypeH(mockTask());
    svc2PostponedNotCreditedTask.setScheduledService(new LinkResource(2L));
    taskList.add(svc2PostponedNotCreditedTask);

    return taskList;
  }


  /**
   * Returns list of tasks.
   *
   * @return list of tasks.
   */
  protected List<WorkItemLightHDto> mockNonPmsTasks() {

    final Date date = new Date();

    // continuousService pms task
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setId(1L);
    task.setPmsApplicable(true);
    task.setPmsCredited(Boolean.FALSE);
    task.setDueDate(date);
    task.setWorkItemTypeH(mockTask());
    task.setScheduledService(new LinkResource(1L));
    tasks.add(task);

    // continuousService nonPms task
    final WorkItemLightHDto task1 = new WorkItemLightHDto();
    task1.setId(2L);
    task1.setPmsApplicable(false);
    task1.setPmsCredited(Boolean.TRUE);
    task1.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task1.setDueDate(date);
    task1.setScheduledService(new LinkResource(1L));
    task1.setWorkItemTypeH(mockTask());
    tasks.add(task1);

    // nonContinuous nonPmsTask
    final WorkItemLightHDto task2 = new WorkItemLightHDto();
    task2.setId(6L);
    task2.setPmsApplicable(false);
    task2.setResolutionStatus(new LinkResource(ResolutionStatus.POSTPONED.getValue()));
    task2.setDueDate(date);
    task2.setWorkItemTypeH(mockTask());
    task2.setScheduledService(new LinkResource(2L));
    tasks.add(task2);

    return tasks;

  }


  /**
   * Returns list of tasks has credit statuses.
   *
   * @return list of tasks has credit statuses.
   */
  protected List<WorkItemLightHDto> mockTasksWithCreditStatus() {
    final Date date = new Date();


    // continuous service pms = true,pms credited true.
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setId(1L);
    task.setPmsApplicable(true);
    task.setPmsCredited(Boolean.TRUE);
    task.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task.setDueDate(date);
    task.setWorkItemTypeH(mockTask());
    task.setScheduledService(new LinkResource(1L));
    tasks.add(task);

    // nonContinuous service pms = true , resolution status != null
    final WorkItemLightHDto task1 = new WorkItemLightHDto();
    task1.setId(1L);
    task1.setPmsApplicable(true);
    task1.setResolutionStatus(new LinkResource(ResolutionStatus.COMPLETE.getValue()));
    task1.setResolutionDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task1.setDueDate(date);
    task1.setWorkItemTypeH(mockTask());
    task1.setScheduledService(new LinkResource(2L));
    tasks.add(task1);
    return tasks;
  }


  /**
   * Return list of tasks having PMS tasks and credit statuses.
   *
   * @return list of tasks having PMS tasks and credit statuses.
   */
  protected List<WorkItemLightHDto> mockTasks() {
    final Date date = new Date();

    // continuous service,pmsApplicable = true ,resolutionStatus, pmsCreditDate and pmsCredited is
    // null
    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setId(1L);
    task.setPmsApplicable(true);
    task.setDueDate(date);
    task.setWorkItemTypeH(mockTask());
    task.setScheduledService(new LinkResource(1L));

    tasks.add(task);

    // continuous service,pmsApplicable = true,resolutionStatus,pmsCreditDate is null and
    // pmsCredited false
    final WorkItemLightHDto task1 = new WorkItemLightHDto();
    task1.setId(2L);
    task1.setPmsApplicable(true);
    task1.setPmsCredited(false);
    task1.setDueDate(date);
    task1.setWorkItemTypeH(mockTask());
    task1.setScheduledService(new LinkResource(1L));
    tasks.add(task1);

    // continuous service,pmsApplicable = false,resolutionStatus and pmsCredited is null
    final WorkItemLightHDto task2 = new WorkItemLightHDto();
    task2.setId(3L);
    task2.setPmsApplicable(false);
    task2.setDueDate(date);
    task2.setWorkItemTypeH(mockTask());
    task2.setScheduledService(new LinkResource(1L));
    tasks.add(task2);

    // non continuous service
    final WorkItemLightHDto task3 = new WorkItemLightHDto();
    task3.setId(4L);
    task3.setPmsApplicable(true);
    task3.setPmsCredited(Boolean.TRUE);
    task3.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task3.setDueDate(date);
    task3.setWorkItemTypeH(mockTask());
    task3.setScheduledService(new LinkResource(2L));
    tasks.add(task3);
    // non continuous service
    final WorkItemLightHDto task4 = new WorkItemLightHDto();
    task4.setId(5L);
    task4.setPmsApplicable(true);
    task4.setPmsCredited(Boolean.FALSE);
    task4.setDueDate(date);
    task4.setWorkItemTypeH(mockTask());
    task4.setScheduledService(new LinkResource(2L));
    tasks.add(task4);
    return tasks;

  }

  /**
   * Returns list of tasks.
   *
   * @return list of tasks with mixed data.
   */
  protected List<WorkItemLightHDto> mockMixedDataTasks() {
    final Date date = new Date();

    final List<WorkItemLightHDto> tasks = new ArrayList<>();
    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setReferenceCode("A1");
    task.setName("task 1");
    task.setPmsApplicable(false);
    task.setWorkItemTypeH(mockTask());
    task.setScheduledService(new LinkResource(1L));
    tasks.add(task);

    final WorkItemLightHDto task1 = new WorkItemLightHDto();
    task1.setReferenceCode("A1");
    task1.setName("task 1");
    task1.setPmsApplicable(true);
    task1.setDueDate(date);
    task1.setWorkItemTypeH(mockTask());
    task1.setScheduledService(new LinkResource(1L));
    tasks.add(task1);

    final WorkItemLightHDto task2 = new WorkItemLightHDto();
    task2.setReferenceCode("A1");
    task2.setName("task 1");
    task2.setPmsApplicable(true);
    task2.setPmsCredited(true);
    task2.setPmsCreditDate(DateUtils.getLocalDateToDate(LocalDate.now()));
    task2.setDueDate(date);
    task2.setWorkItemTypeH(mockTask());
    task2.setScheduledService(new LinkResource(2L));
    tasks.add(task2);
    return tasks;
  }
}
