package com.baesystems.ai.lr.cd.be.utils.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.baesystems.ai.lr.cd.be.utils.AssetEmblemUtils;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * @author VKolagutla
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AssetEmblemUtilsTest.Config.class)
public class AssetEmblemUtilsTest {

  /**
   * assetEmblemUtils.
   */
  @Autowired
  private AssetEmblemUtils assetEmblemUtils;

  /**
   * lookUp map.
   */
  private Map<String, String> lookUp;

  /**
   * To get lookup map from {@link AssetEmblemUtils}.
   */
  @Before
  public void setUp() {

    lookUp = assetEmblemUtils.getLookupmap();
  }

  /**
   * Test getEmblem with assetCode.
   */
  @Test
  public void testGetEmblem() {

    String result = AssetEmblemUtils.getEmblemImage("A11");

    assertEquals(lookUp.get("A11"), result);

  }

  /**
   * Test getEmblem with single char assetCode.
   */
  @Test
  public void testGetEmblemWithSingleCharCode() {
    String result = AssetEmblemUtils.getEmblemImage("A");
    assertEquals(lookUp.get("A11"), result);
  }

  /**
   * Test getEmblem with two char assetCode.
   */
  @Test
  public void testGetEmblemWithDoubleCharCode() {
    String result = AssetEmblemUtils.getEmblemImage("A2");
    assertEquals(lookUp.get("A21"), result);
  }

  /**
   * Test getEmblem with four char assetCode.
   */
  @Test
  public void testGetEmblemWithFourCharCode() {
    String result = AssetEmblemUtils.getEmblemImage("A211");
    assertEquals(lookUp.get("A21"), result);
  }

  /**
   * Test get emblem with non existing assetCode.
   */
  @Test
  public void testGetEmblemNull() {
    String result = AssetEmblemUtils.getEmblemImage("A55");
    assertNull(result);
  }

  /**
   * @author VKolagutla
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Initialize {@link AssetEmblemUtils} as Spring bean.
     *
     * @return AssetEmblemUtils.
     */
    @Bean
    public AssetEmblemUtils assetEmblemUtils() {
      return new AssetEmblemUtils();
    }
  }
}
