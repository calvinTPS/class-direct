package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetNoteServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Provides unit test for {@link AssetNoteService}.
 *
 * @author pfauchon
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Response.class, Resources.class, SecurityUtils.class})
@ContextConfiguration(classes = AssetNoteServiceImplTest.Config.class)
public class AssetNoteServiceImplTest {
  /**
   * The logger instantiated for {@link AssetNoteServiceImplTest}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetNoteServiceImplTest.class);

  /**
   * The Spring application context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link AssetNoteService} from {@link AssetNoteServiceImplTest.Config} class.
   */
  @Autowired
  private AssetNoteService assetNoteService;

  /**
   * The {@link AssetRetrofitService} from {@link AssetNoteServiceImplTest.Config} class.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The default value for group type other.
   */
  private static final String OTHER_GROUP = "OtherGroup";

  /**
   * The default value for group type statutory.
   */
  private static final String CATEGORYSTATUTORY = "Statutory";

  /**
   * Provides initialization for mock object..
   *
   */
  @Before
  public final void init() {
    reset(context.getBean(AssetRetrofitService.class));
  }

  /**
   * Tests positive case scenario for get asset note.
   *
   * @throws ClassDirectException if API call fail.
   */
  @Test
  public final void testGetAssetNote() throws ClassDirectException {

    final LinkResource linkResource1 = new LinkResource(1L);
    final CodicilCategoryHDto category1 = new CodicilCategoryHDto();
    category1.setId(linkResource1.getId());
    category1.setName("some name");

    final LinkResource linkResource2 = new LinkResource(1L);
    final CodicilCategoryHDto category2 = new CodicilCategoryHDto();
    category2.setId(linkResource2.getId());
    category2.setName(CATEGORYSTATUTORY);

    final List<AssetNoteHDto> notes = new ArrayList<>();
    final AssetNoteHDto note1 = new AssetNoteHDto();
    note1.setCategory(linkResource1);
    note1.setCategoryH(category1);
    note1.setJob(new LinkResource(1L));
    note1.setImposedDate(null);

    final AssetNoteHDto note2 = new AssetNoteHDto();
    note2.setCategory(linkResource1);
    note2.setCategoryH(category1);
    note2.setJob(new LinkResource(2L));
    note2.setImposedDate(new GregorianCalendar(2017, 1, 1).getTime());

    final AssetNoteHDto note3 = new AssetNoteHDto();
    note3.setCategory(linkResource1);
    note3.setCategoryH(category1);
    note3.setImposedDate(new GregorianCalendar(2016, 2, 2).getTime());

    final AssetNoteHDto note4 = new AssetNoteHDto();
    note4.setCategory(linkResource2);
    note4.setCategoryH(category2);
    note4.setImposedDate(new GregorianCalendar(2018, 1, 11).getTime());

    final AssetNoteHDto note5 = new AssetNoteHDto();
    note5.setCategory(linkResource1);
    note5.setCategoryH(category1);
    note5.setImposedDate(new GregorianCalendar(2018, 1, 1).getTime());

    final AssetNoteHDto note6 = new AssetNoteHDto();
    note6.setAssetItem(new NamedLinkResource(1L));
    note6.setCategory(linkResource1);
    note6.setCategoryH(category1);

    notes.add(note1);
    notes.add(note2);
    notes.add(note3);
    notes.add(note4);
    notes.add(note5);
    notes.add(note6);

    doReturn(mockAssetItemData()).when(assetRetrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(assetRetrofitService.getAssetNoteDto(anyLong())).thenReturn(Calls.response(notes));

    // mock user
    PowerMockito.mockStatic(SecurityUtils.class);

    // test for normal user
    BDDMockito.given(SecurityUtils.isEquasisThetisUser()).willReturn(false);
    final PageResource<AssetNoteHDto> result = assetNoteService.getAssetNote(null, null, 1L);
    assertEquals(6, result.getPagination().getTotalElements().longValue());
    assertEquals(6, result.getContent().size());
    assertEquals(new GregorianCalendar(2018, 1, 11).getTime(), result.getContent().get(0).getImposedDate());
    assertEquals(new GregorianCalendar(2018, 1, 1).getTime(), result.getContent().get(1).getImposedDate());
    assertEquals(new GregorianCalendar(2017, 1, 1).getTime(), result.getContent().get(2).getImposedDate());
    assertEquals(new GregorianCalendar(2016, 2, 2).getTime(), result.getContent().get(3).getImposedDate());
    Assert.assertNull(result.getContent().get(4).getImposedDate());
    Assert.assertNull(result.getContent().get(5).getImposedDate());

    // test for user equasis
    BDDMockito.given(SecurityUtils.isEquasisThetisUser()).willReturn(true);
    final PageResource<AssetNoteHDto> result2 = assetNoteService.getAssetNote(1, 2, 1L);
    assertEquals(0, result2.getPagination().getTotalPages().intValue());
    assertEquals(1, result2.getPagination().getTotalElements().longValue());
    assertEquals(1, result2.getContent().size());

    // test for user thetis
    BDDMockito.given(SecurityUtils.isEquasisThetisUser()).willReturn(true);
    final PageResource<AssetNoteHDto> result3 = assetNoteService.getAssetNote(1, 2, 1L);
    assertEquals(0, result3.getPagination().getTotalPages().intValue());
    assertEquals(1, result3.getPagination().getTotalElements().longValue());
    assertEquals(1, result3.getContent().size());
  }

  /**
   * Tests to see that the proper exception is throwns when mast isn't available.
   *
   * @throws ClassDirectException if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testMastUnavailable() throws ClassDirectException {

    when(assetRetrofitService.getAssetNoteDto(anyLong())).thenReturn(Calls.failure(new IOException()));

    final PageResource<AssetNoteHDto> result3 = assetNoteService.getAssetNote(1, 2, 1L);


  }


  /**
   * Test positive case scenario for get asset note by asset Id and asset note Id.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetAssetNoteByAssetIdAssetNoteId() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockSingleAssetNoteData()).when(retrofitService).getAssetNoteByAssetIdAssetNoteId(Matchers.anyLong(),
        Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobsData()));

    final AssetNoteHDto result = assetNoteService.getAssetNoteByAssetIdAssetNoteId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getJobH());
    Assert.assertEquals(result.getJobH().getJobNumber(), "123");
  }

  /**
   * Test positive case scenario for get asset note by asset Id and asset note Id with null
   * condition.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetAssetNoteByAssetIdAssetNoteIdMultiCondition() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockSingleAssetNoteDataWithNullAssetItemId()).when(retrofitService)
        .getAssetNoteByAssetIdAssetNoteId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.failure(new IOException()));
    final AssetNoteHDto result = assetNoteService.getAssetNoteByAssetIdAssetNoteId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNull(result.getJobH());
  }

  /**
   * Test get asset note by asset Id and asset note Id with API call fail.
   *
   * @throws Exception if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetAssetNoteByAssetIdAssetNoteIdException() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(retrofitService.getAssetNoteByAssetIdAssetNoteId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException("Mocked Exception.")));
    assetNoteService.getAssetNoteByAssetIdAssetNoteId(1L, 1L);
  }

  /**
   * Test positive case scenario for get asset note by asset Id and asset note Id when fail to get
   * asset item.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetAssetNoteByAssetIdFail() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    doReturn(mockSingleAssetNoteDataWithNullAssetItemId()).when(retrofitService)
        .getAssetNoteByAssetIdAssetNoteId(Matchers.anyLong(), Matchers.anyLong());
    when(retrofitService.getAssetItemDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException("Mocked Exception.")));
    final AssetNoteHDto result = assetNoteService.getAssetNoteByAssetIdAssetNoteId(1L, 1L);

    Assert.assertNotNull(result);
  }


  /**
   * Returns single asset note mock data.
   *
   * @return the single asset note mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<AssetNoteHDto> mockSingleAssetNoteData() {
    final AssetNoteHDto assetNote = new AssetNoteHDto();
    assetNote.setAssetItem(new NamedLinkResource(1L));
    assetNote.setJob(new LinkResource(1L));

    final Response<AssetNoteHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(assetNote);

    final Call<AssetNoteHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns single asset note data with null asset item Id.
   *
   * @return the single asset note mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<AssetNoteHDto> mockSingleAssetNoteDataWithNullAssetItemId() {
    final AssetNoteHDto assetNote = new AssetNoteHDto();

    final Response<AssetNoteHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(assetNote);

    final Call<AssetNoteHDto> caller = Calls.response(response);

    return caller;
  }


  /**
   * Returns asset item mock data.
   *
   * @return the asset item mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<LazyItemHDto> mockAssetItemData() {
    final LazyItemHDto result = new LazyItemHDto();
    result.setId(1L);

    final Response<LazyItemHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(result);

    final Call<LazyItemHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mocks Jobs Data.
   *
   * @return call value.
   */
  private JobHDto mockJobsData() {
    JobHDto job = new JobHDto();
    job.setId(1L);
    job.setJobNumber("123");
    return job;

  }


  /**
   * Provides Spring in-class configurations.
   *
   * @author yng
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Bean
    @Override
    public AssetNoteService assetNoteService() {
      return new AssetNoteServiceImpl();
    }
  }
}
