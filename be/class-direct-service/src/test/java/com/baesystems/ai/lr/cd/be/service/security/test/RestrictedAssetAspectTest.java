package com.baesystems.ai.lr.cd.be.service.security.test;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.CustomerRolesEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.security.RestrictedAssetAspect;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import retrofit2.mock.Calls;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class RestrictedAssetAspectTest {

  /**
   * Default user id.
   */
  private static final String DEFAULT_USER_ID = "102";

  /**
   * Default client code.
   */
  private static final String DEFAULT_CLIENT_CODE = "4";

  /**
   * Default asset id.
   */
  private static final Long DEFAULT_ASSET_ID = 1000019L;

  /**
   * Asset id 101.
   */
  private static final Long ASSET_ID_101 = 100001L;

  /**
   * Asset id 102.
   */
  private static final Long ASSET_ID_102 = 100002L;

  /**
   * Asset id 103.
   */
  private static final Long ASSET_ID_103 = 100003L;

  /**
   * Default mock count.
   */
  private static final Integer MAX_MOCK_COUNT = 5;

  /**
   * Injected asset service.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Injected aspect.
   */
  @Autowired
  private RestrictedAssetAspect assetAspect;

  /**
   * Injected context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * Reset mocks.
   */
  @After
  public final void resetMocks() {
    final SubFleetService subFleetService = context.getBean(SubFleetService.class);
    final UserProfileService userProfileService = context.getBean(UserProfileService.class);
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final ServiceReferenceRetrofitService service = context.getBean(ServiceReferenceRetrofitService.class);

    reset(assetAspect, assetService, subFleetService, userProfileService, assetRetrofitService, service);
  }

  /**
   * Aspect should intercept method with return type AssetPageResource.
   *
   * @throws Throwable when error.
   */
  @Test
  public final void shouldInterceptGetAllAssets() throws Throwable {
    mockUserProfile(DEFAULT_USER_ID);
    mockAsset(ASSET_ID_101, ASSET_ID_102, ASSET_ID_103);
    mockEmptyScheduledServices();
    mockSubfleet(DEFAULT_USER_ID, ASSET_ID_101);
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(DEFAULT_USER_ID, Collections.emptyMap()));

    final AssetPageResource assetPageResource =
        assetService.getAssetsQueryPageResource(DEFAULT_USER_ID, 1, 1, null, null, new AssetQueryHDto());
    assertNotNull(assetPageResource);
    assertNotNull(assetPageResource.getContent());
    assertFalse(assetPageResource.getContent().isEmpty());
    assetPageResource.getContent().stream()
        .filter(asset -> asset.getId().equals(ASSET_ID_102) || asset.getId().equals(ASSET_ID_103))
        .forEach(asset -> assertTrue(asset.getRestricted()));

    verify(assetAspect, atLeastOnce()).checkAccessibleAsset(any(), any());
  }

  /**
   * Aspect should intercept method with return type of
   * {@link com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto}.
   *
   * @throws Throwable when error.
   */
  @Test
  public final void shouldInterceptReturnTypeAssetHdto() throws Throwable {
    mockUserProfile(DEFAULT_USER_ID);
    mockAsset(ASSET_ID_102);
    mockEmptyScheduledServices();
    mockSubfleet(DEFAULT_USER_ID, ASSET_ID_101);
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(DEFAULT_USER_ID, Collections.emptyMap()));

    final AssetHDto asset = assetService.assetByCode(DEFAULT_USER_ID, AssetCodeUtil.getMastCode(ASSET_ID_102));
    assertNotNull(asset);
    assertTrue(asset.getRestricted());
    verify(assetAspect, atLeastOnce()).checkAccessibleAsset(any(), any());
  }

  /**
   * Aspect should't be executed in non-annotated method.
   *
   * @throws Throwable when error.
   */
  @Test
  public final void shouldNotInterceptIfNotAnnotated() throws Throwable {
    mockAsset(ASSET_ID_101, ASSET_ID_102, ASSET_ID_103);

    final MastQueryBuilder query =
      MastQueryBuilder.create().mandatory(MastQueryBuilder.assetId(QueryRelationshipType.GT, 1L));
    final List<AssetHDto> assets = assetService.mastIhsQuery(query, null, null);
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    assets.forEach(asset -> assertFalse(asset.getRestricted()));
    verify(assetAspect, never()).checkAccessibleAsset(any(), any());
  }

  /**
   * Aspect should intercept and assert access control policy when client code is available.
   *
   * @throws Throwable when error.
   */
  @Test
  public final void shouldInterceptAndAssertAccessControlWithClientCode() throws Throwable {
    mockUserProfileWithClientId(DEFAULT_USER_ID, DEFAULT_CLIENT_CODE);
    mockEmptyScheduledServices();
    mockAsset(ASSET_ID_101, ASSET_ID_102);
    mockSubfleetEmpty(DEFAULT_USER_ID);
    mockClientSubfleet(ASSET_ID_101, ASSET_ID_102);
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(DEFAULT_USER_ID, Collections.emptyMap()));

    final AssetPageResource assets =
        assetService.getAssetsQueryPageResource(DEFAULT_USER_ID, 1, 1, null, null, new AssetQueryHDto());
    assertNotNull(assets);
    assertNotNull(assets.getContent());
    assertFalse(assets.getContent().isEmpty());
    assets.getContent().stream().filter(asset -> asset.getId().equals(ASSET_ID_103))
        .forEach(asset -> assertTrue(asset.getRestricted()));
    verify(assetAspect, atLeastOnce()).checkAccessibleAsset(any(), any());
  }

  /**
   * Aspect should intercept and null asset-related items.
   *
   * @throws Throwable when error.
   */
  @Test
  public final void shouldInterceptAndNullAssetRelatedItems() throws Throwable {
    mockSubfleetEmpty(DEFAULT_USER_ID);
    mockSubfleet(DEFAULT_USER_ID, ASSET_ID_102, ASSET_ID_103);
    mockEmptyScheduledServices();
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(DEFAULT_USER_ID, Collections.emptyMap()));

    final PageResource<ScheduledServiceHDto> scheduledServices =
        assetService.getServices(null, null, ASSET_ID_101, new ServiceQueryDto());
    assertNotNull(scheduledServices);
    assertNotNull(scheduledServices.getContent());
    assertTrue(scheduledServices.getContent().isEmpty());
    verify(assetAspect, atLeastOnce()).checkAccessibleAsset(any(), any());
  }

  /**
   * Mock empty subfleet.
   *
   * @param userId user id.
   * @throws ClassDirectException when error.
   */
  private void mockSubfleetEmpty(final String userId) throws ClassDirectException {
    final SubFleetService subFleetService = context.getBean(SubFleetService.class);

    when(subFleetService.getSubFleetById(eq(userId))).thenReturn(Collections.emptyList());
  }

  /**
   * Mock subfleet for client.
   *
   * @param assetIds asset ids.
   * @throws ClassDirectException when error.
   */
  private void mockClientSubfleet(final Long... assetIds) throws ClassDirectException {
    final SubFleetService subFleetService = context.getBean(SubFleetService.class);

    when(subFleetService.getAccessibleAssetIdsForUser(anyString()))
        .thenReturn(Stream.of(assetIds).collect(Collectors.toList()));
  }

  /**
   * Mock subfleet.
   *
   * @param userId user id.
   * @param subfleetIds asset ids.
   * @throws ClassDirectException when error.
   */
  private void mockSubfleet(final String userId, final Long... subfleetIds) throws ClassDirectException {
    final SubFleetService subFleetService = context.getBean(SubFleetService.class);

    when(subFleetService.getSubFleetById(eq(userId)))
        .thenReturn(Arrays.stream(subfleetIds).collect(Collectors.toList()));
  }

  /**
   * Mock user profiles.
   *
   * @param userId user id.
   * @throws ClassDirectException when error.
   */
  private void mockUserProfile(final String userId) throws ClassDirectException {
    final UserProfileService userProfileService = context.getBean(UserProfileService.class);

    final UserProfiles user = new UserProfiles();
    user.setUserId(userId);
    user.setRestrictAll(false);
    user.setRoles(new HashSet<>(1));
    final Roles roleAdmin = new Roles();
    roleAdmin.setRoleName(Role.LR_ADMIN.name());
    user.getRoles().add(roleAdmin);
    when(userProfileService.getUser(eq(userId))).thenReturn(user);
  }

  /**
   * Mock user profiles with client id.
   *
   * @param userId user id.
   * @param clientId client id.
   * @throws ClassDirectException when error.
   */
  private void mockUserProfileWithClientId(final String userId, final String clientId) throws ClassDirectException {
    final UserProfileService userProfileService = context.getBean(UserProfileService.class);

    final UserProfiles user = new UserProfiles();
    user.setUserId(userId);
    user.setRestrictAll(false);
    user.setClientCode(clientId);
    user.setClientType(CustomerRolesEnum.DOC_COMPANY.toString());
    when(userProfileService.getUser(eq(userId))).thenReturn(user);
  }

  /**
   * Mock asset query result.
   *
   * @param count result count.
   * @throws ClassDirectException exception when error.
   */
  private void mockAssetQueryResult(final Integer count) throws ClassDirectException {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> assets = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      final AssetLightDto mastAsset = new AssetLightDto();
      mastAsset.setId((long) i);
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setMastAsset(mastAsset);
      assets.add(multiAssetDto);
    }
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(assets);

    when(assetRetrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class)))
       .thenReturn(Calls.response(pageResource));
  }

  /**
   * Mock single asset.
   *
   * @param assetIds asset ids.
   * @throws ClassDirectException when error.
   */
  private void mockAsset(final Long... assetIds) throws ClassDirectException {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> assets = new ArrayList<>();
    for (final Long assetId : assetIds) {
      final AssetLightDto mastAsset = new AssetLightDto();
      mastAsset.setId(assetId);
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setMastAsset(mastAsset);
      assets.add(multiAssetDto);
    }
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(assets);
    when(assetRetrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class)))
    .thenReturn(Calls.response(pageResource));
    MultiAssetPageResourceDto multiAssetPageResourceDtoDesc = new MultiAssetPageResourceDto();
    multiAssetPageResourceDtoDesc.setContent(assets);
    when(assetRetrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(multiAssetPageResourceDtoDesc));
  }

  /**
   * Mock empty scheduled services.
   *
   * @throws ClassDirectException when error.
   */
  private void mockEmptyScheduledServices() throws ClassDirectException {
    final ServiceReferenceRetrofitService service = context.getBean(ServiceReferenceRetrofitService.class);

    when(service.getAssetServicesQuery(anyLong(), anyMap())).thenReturn(Calls.response(Collections.emptyList()));
  }

  /**
   * Internal config.
   */
  @Configuration
  @EnableAspectJAutoProxy
  public static class Config extends BaseMockConfiguration {

    /**
     * Restricted asset aspect.
     *
     * @return aspect.
     */
    @Bean
    public RestrictedAssetAspect restrictedAssetAspect() {
      return Mockito.spy(new RestrictedAssetAspect());
    }

    @Override
    public AssetService assetService() {
      return spy(new AssetServiceImpl());
    }
  }
}
