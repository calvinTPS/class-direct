package com.baesystems.ai.lr.cd.service.certificate.impl.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.CertificateReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateTemplateHDto;
import com.baesystems.ai.lr.cd.service.certificate.CertificateReferenceService;
import com.baesystems.ai.lr.cd.service.certificate.impl.CertificateReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.references.CertificateActionTakenDto;
import com.baesystems.ai.lr.dto.references.CertificateTemplateDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

import retrofit2.mock.Calls;

/**
 * Created by fwijaya on 25/1/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class CertificateReferenceServiceImplTest {

  /**
   * Spring application context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * Certificate reference service.
   */
  @Autowired
  private CertificateReferenceService certService;

  /**
   * Certificate types constant.
   */
  private static final String[] CERT_TYPES = {"Full", "Conditional", "Interim"};

  /**
   * Certificate status constant.
   */
  private static final String[] CERT_STATUSES = {"Draft", "Final"};

  /**
   * Certificate action taken.
   */
  private static final String[] CERT_ACTION_TAKEN = {"None", "Issued", "Endorsed", "Extended"};

  /**
   * Constant.
   */
  private static final Long ONE = 1L;

  /**
   * Constant.
   */
  private static final Long TEN = 10L;


  /**
   * Test getting certificate templates, checking for name and form number.
   */
  @Test
  public void testGetCertificateTemplates() {
    CertificateReferenceRetrofitService retrofitService = context.getBean(CertificateReferenceRetrofitService.class);
    Mockito.when(retrofitService.getCertificateTemplates()).thenReturn(Calls.response(mockCertificateTemplateDtos()));
    List<CertificateTemplateHDto> list = certService.getCertificateTemplates();
    Assert.notEmpty(list);
    list.stream().forEach(dto -> {
      Assert.notNull(dto.getFormNumber());
      Assert.notNull(dto.getName());
    });
  }

  /**
   * Test getting particular certificate template.
   */
  @Test
  public void testGetCertificateTemplate() {
    CertificateReferenceRetrofitService retrofitService = context.getBean(CertificateReferenceRetrofitService.class);
    Mockito.when(retrofitService.getCertificateTemplates()).thenReturn(Calls.response(mockCertificateTemplateDtos()));
    CertificateTemplateDto dto = certService.getCertificateTemplate(ONE);
    Assert.notNull(dto);
    Assert.isTrue(dto.getId().equals(ONE));
  }

  /**
   * Test getting certificate types.
   */
  @Test
  public void testGetCertificateTypes() {
    CertificateReferenceRetrofitService retrofitService = context.getBean(CertificateReferenceRetrofitService.class);
    Mockito.when(retrofitService.getCertificateTypes()).thenReturn(Calls.response(mockCertificateTypeDtos()));
    List<CertificateTypeDto> certificateTypes = certService.getCertificateTypes();
    Assert.notEmpty(certificateTypes);
    certificateTypes.stream().forEach(type -> Assert.isTrue(Arrays.asList(CERT_TYPES).contains(type.getName())));
  }

  /**
   * Test getting a particular certificate type.
   */
  @Test
  public void testGetCertificateType() {
    CertificateReferenceRetrofitService retrofitService = context.getBean(CertificateReferenceRetrofitService.class);
    Mockito.when(retrofitService.getCertificateTypes()).thenReturn(Calls.response(mockCertificateTypeDtos()));
    CertificateTypeDto certificateType = certService.getCertificateType(ONE);
    Assert.notNull(certificateType);
    Assert.isTrue(certificateType.getId().equals(ONE));
    Assert.isTrue(certificateType.getName().equals(CERT_TYPES[ONE.intValue()]));
  }

  /**
   * Test getting statuses.
   */
  @Test
  public void testGetStatuses() {
    CertificateReferenceRetrofitService retrofitService = context.getBean(CertificateReferenceRetrofitService.class);
    Mockito.when(retrofitService.getCertificateStatuses()).thenReturn(Calls.response(mockCertificateStatusDtos()));
    List<CertificateStatusDto> list = certService.getCertificateStatuses();
    Assert.notNull(list);
    list.stream().forEach(status -> Assert.isTrue(Arrays.asList(CERT_STATUSES).contains(status.getName())));
  }

  /**
   * Test getting particular status.
   */
  @Test
  public void testGetStatus() {
    CertificateReferenceRetrofitService retrofitService = context.getBean(CertificateReferenceRetrofitService.class);
    Mockito.when(retrofitService.getCertificateStatuses()).thenReturn(Calls.response(mockCertificateStatusDtos()));
    CertificateStatusDto dto = certService.getCertificateStatus(ONE);
    Assert.notNull(dto);
    Assert.isTrue(dto.getId().equals(ONE));
  }

  /**
   * Test getting surveyor.
   */
  @Test
  public void testGetSurveyor() {
    AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    Mockito.when(assetRetrofitService.getEmployeeById(Mockito.anyLong()))
        .thenReturn(Calls.response(new LrEmployeeDto()));
    LrEmployeeDto dto = certService.getSurveyor(ONE);
    Assert.notNull(dto);
  }

  /**
   * Test getting action taken dto.
   */
  @Test
  public void testGetActionTaken() {
    CertificateReferenceRetrofitService retrofitService = context.getBean(CertificateReferenceRetrofitService.class);
    Mockito.when(retrofitService.getCertificateActionTaken())
        .thenReturn(Calls.response(mockCertificateActionTakenDtos()));
    CertificateActionTakenDto dto = certService.getActionTakenDto(ONE);
    Assert.notNull(dto);
    org.junit.Assert.assertEquals(dto.getName(), CERT_ACTION_TAKEN[ONE.intValue()]);
  }

  /**
   * Mock a list of {@link CertificateActionTakenDto}.
   *
   * @return mock object.
   */
  private List<CertificateActionTakenDto> mockCertificateActionTakenDtos() {
    List<CertificateActionTakenDto> list = new ArrayList<>();
    IntStream.range(0, CERT_ACTION_TAKEN.length).forEach(i -> list.add(new CertificateActionTakenDto() {
      {
        setId(new Long(i));
        setName(CERT_ACTION_TAKEN[i]);
      }
    }));
    return list;
  }

  /**
   * Mock a list of {@link CertificateStatusDto}.
   *
   * @return mock object.
   */
  private List<CertificateStatusDto> mockCertificateStatusDtos() {
    List<CertificateStatusDto> list = new ArrayList<>();
    IntStream.range(0, CERT_STATUSES.length).forEach(i -> list.add(new CertificateStatusDto() {
      {
        setId(new Long(i));
        setName(CERT_STATUSES[i]);
      }
    }));
    return list;
  }

  /**
   * Mock a list of {@link CertificateTypeDto}.
   *
   * @return mock object.
   */
  private List<CertificateTypeDto> mockCertificateTypeDtos() {
    List<CertificateTypeDto> list = new ArrayList<>();
    IntStream.range(0, CERT_TYPES.length).forEach(i -> list.add(new CertificateTypeDto() {
      {
        setId(new Long(i));
        setName(CERT_TYPES[i]);
      }
    }));
    return list;
  }

  /**
   * Mock a list of {@link CertificateTemplateDto}.
   *
   * @return mock object.
   */
  private List<CertificateTemplateHDto> mockCertificateTemplateDtos() {
    List<CertificateTemplateHDto> list = new ArrayList<>();
    IntStream.range(0, TEN.intValue()).forEach(i -> list.add(new CertificateTemplateHDto() {
      {
        setId(new Long(i));
        setFormNumber("Form" + i);
        setName("Cert" + i);
      }
    }));
    return list;
  }

  /**
   * Inner config.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Mock.
     *
     * @return mock object.
     */
    @Override
    public CertificateReferenceService certificateReferenceService() {
      return new CertificateReferenceServiceImpl();
    }
  }
}
