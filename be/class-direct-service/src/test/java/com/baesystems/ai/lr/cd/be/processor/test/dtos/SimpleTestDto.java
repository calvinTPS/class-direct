package com.baesystems.ai.lr.cd.be.processor.test.dtos;

import com.baesystems.ai.lr.cd.be.enums.DueStatus;
/**
 * test DTO.
 * @author shirin.kulkarni
 *
 */
public class SimpleTestDto {

  /**
   * int var.
   */
  private int id;

  /**
   * String var.
   */
  private String name;

  /**
   * Due status Enum.
   */
  private DueStatus dueStatus;

  /**
   * @return the id
   */
  public int getId() {
    return id;
  }

  /**
   * @param idParam the id to set.
   */
  public final void setId(final int idParam) {
    this.id = idParam;
  }

  /**
   * @return the name.
   */
  public String getName() {
    return name;
  }

  /**
   * @param nameParam the name to set.
   */
  public final void setName(final String nameParam) {
    this.name = nameParam;
  }

  /**
   * @return the dueStatus
   */
  public DueStatus getDueStatus() {
    return dueStatus;
  }

  /**
   * @param dueStatusParam the dueStatus to set.
   */
  public final void setDueStatus(final DueStatus dueStatusParam) {
    this.dueStatus = dueStatusParam;
  }
}
