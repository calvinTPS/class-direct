package com.baesystems.ai.lr.cd.be.service.service.reference.impl.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.reference.impl.FlagStateServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.paging.PaginationDto;



/**
 * Provides unit test for {@link FlagStateService}.
 *
 * @author msidek
 * @author syalavarthi 23-05-2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FlagStateServiceImplTest.Config.class)
public class FlagStateServiceImplTest {

  /**
   * Log4J Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(FlagStateServiceImplTest.class);

  /**
   * The {@link FlagStateService} from spring context.
   */
  @Autowired
  private FlagStateService flagStateService;

  /**
   * The {@link AssetService} from spring context.
   */
  @Autowired
  private AssetService assetService;


  /**
   * The {@link AssetReferenceService} from spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The {@link FlagsAssociationsService} from Spring context.
   */
  @Autowired
  private FlagsAssociationsService flagsAssociationsService;

  /**
   * Resets mock objects.
   */
  @Before
  public void resetMocks() {
    Mockito.reset(assetService);
  }

  /**
   * Tests success scenario to get flag state by id {@link FlagStateService#getFlagById(Long)}.
   *
   * @throws Exception if mast api fails.
   */
  @Test
  public final void testGetFlagByIdSuccess() throws Exception {
    // Mock flag list
    final Map<String, FlagStateHDto> mockedFlagsMap = mockFlags();
    final FlagStateHDto mockedFlag = mockedFlagsMap.get("ALA");

    Mockito.when(assetReferenceService.getFlagState(mockedFlag.getId())).thenReturn(mockedFlag);

    FlagStateHDto flagState = flagStateService.getFlagById(mockedFlag.getId());
    Assert.assertNotNull(flagState);
    Assert.assertEquals(mockedFlag.getId(), flagState.getId());
    Assert.assertEquals("ALA", flagState.getName());
  }

  /**
   * Tests failure scenario to get flag state by id {@link FlagStateService#getFlagById(Long)} where
   * the given flag id not found.
   *
   * @throws Exception if mast api fails.
   */
  @Test
  public final void testGetFlagByIdFail() throws Exception {
    final Long invalidFlagId = -1L;

    // Mock flag list
    final Map<String, FlagStateHDto> mockedFlagsMap = mockFlags();
    final List<FlagStateHDto> mockedFlags = new ArrayList<>(mockedFlagsMap.values());
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockedFlags);

    FlagStateHDto invalidFlagState = flagStateService.getFlagById(invalidFlagId);
    Assert.assertNull(invalidFlagState);
  }

  /**
   * Tests success scenario to get user flags for admin role,
   * {@link FlagStateService#getUserFlagStates(String)}. The method should return all the flags
   * except those that are deleted.
   *
   * @throws Exception if error when get user or get flag reference data or any error occur in the
   *         flow.
   */
  @Test
  public final void testGetAdminFlagStates() throws Exception {
    // Mock flag list
    final Map<String, FlagStateHDto> mockedFlagsMap = mockFlags();
    final List<FlagStateHDto> mockedFlags = new ArrayList<>(mockedFlagsMap.values());
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockedFlags);

    // Mock admin user
    final UserProfiles admin = new UserProfiles();
    admin.setUserId("ADMIN");
    admin.setRoles(getRolesLrAdmin());
    Mockito.when(userProfileService.getUser(admin.getUserId())).thenReturn(admin);

    final List<FlagStateHDto> flagStateHDtos = flagStateService.getUserFlagStates(admin.getUserId());

    Assert.assertNotNull(flagStateHDtos);
    // Exclude the deleted flag
    Assert.assertEquals(mockedFlags.size() - 1, flagStateHDtos.size());
    flagStateHDtos.forEach(flag -> {
      Assert.assertFalse(flag.getDeleted());
    });
  }

  /**
   * Tests success scenario to get user flags for client/ship builder role,
   * {@link FlagStateService#getUserFlagStates(String)}. The method should return all the flags of
   * the user's assets except those that are deleted.
   *
   *
   * @throws Exception if error when get user or get flag reference data or any error occur in the
   *         flow.
   */
  @Test
  public final void testGetShipBuilderOrClientFlagState() throws Exception {

    // Mock flag list
    final Map<String, FlagStateHDto> mockedFlagsMap = mockFlags();
    final List<FlagStateHDto> mockedFlags = new ArrayList<>(mockedFlagsMap.values());
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockedFlags);

    // Mock ship builder and client user
    final UserProfiles shipBuilder = new UserProfiles();
    shipBuilder.setUserId("SHIP_BUILDER");
    shipBuilder.setRoles(getRolesShipBuilder());
    Mockito.when(userProfileService.getUser(shipBuilder.getUserId())).thenReturn(shipBuilder);

    final UserProfiles client = new UserProfiles();
    client.setUserId("CLIENT");
    client.setRoles(getRolesClient());
    Mockito.when(userProfileService.getUser(client.getUserId())).thenReturn(client);

    // Mock asset list
    final List<AssetHDto> assetList = new ArrayList<>();
    assetList.add(mockAsset(1L, mockedFlagsMap.get("AIA")));
    assetList.add(mockAsset(2L, mockedFlagsMap.get("AIA")));
    assetList.add(mockAsset(3L, mockedFlagsMap.get("FRA")));
    // Asset with deleted flag
    assetList.add(mockAsset(4L, mockedFlagsMap.get("ALA")));

    final AssetPageResource pageResource = new AssetPageResource();
    final PaginationDto pageDto = new PaginationDto();
    pageResource.setPagination(pageDto);
    pageResource.setContent(assetList);

    Mockito
        .when(assetService.getAssetsQueryPageResource(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt(),
            Mockito.isNull(String.class), Mockito.isNull(String.class), Mockito.any(AssetQueryHDto.class)))
        .thenReturn(createEmptyAssetPageResourceWithDifferentPage(0, 1, 2L), pageResource);

    final List<FlagStateHDto> shipBuilderFlags = flagStateService.getUserFlagStates(shipBuilder.getUserId());
    Assert.assertNotNull(shipBuilderFlags);
    shipBuilderFlags.forEach(flag -> LOGGER.debug(flag.getFlagCode()));
    /** temp change to be reverted **/
    Assert.assertEquals(4, shipBuilderFlags.size());
    shipBuilderFlags.forEach(flag -> {
      Assert.assertFalse(flag.getDeleted());
    });

    Mockito
        .when(assetService.getAssetsQueryPageResource(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt(),
            Mockito.isNull(String.class), Mockito.isNull(String.class), Mockito.any(AssetQueryHDto.class)))
        .thenReturn(createEmptyAssetPageResourceWithDifferentPage(0, 1, 2L), pageResource);

    final List<FlagStateHDto> clientFlags = flagStateService.getUserFlagStates(client.getUserId());
    Assert.assertNotNull(clientFlags);
    clientFlags.forEach(flag -> LOGGER.debug(flag.getFlagCode()));
    /** temp change to be reverted **/
    Assert.assertEquals(4, clientFlags.size());
    clientFlags.forEach(flag -> {
      Assert.assertFalse(flag.getDeleted());
    });
  }

  /**
   * Tests success scenario to get user flags for flag role,
   * {@link FlagStateService#getUserFlagStates(String)}. The method should return the flags that
   * this user belongs as well as the associated primary or secondary flag(s).
   *
   * @throws Exception if error when get user or get flag reference data or any error occur in the
   *         flow.
   */
  @Test
  public final void testGetFlagUserFlagStates() throws Exception {

    // Mock flag list
    final Map<String, FlagStateHDto> mockedFlagsMap = mockFlags();
    final List<FlagStateHDto> mockedFlags = new ArrayList<>(mockedFlagsMap.values());
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockedFlags);

    // Mock flag user
    final UserProfiles flagUser = new UserProfiles();
    flagUser.setUserId("FLAG_FRENCH");
    flagUser.setFlagCode(mockedFlagsMap.get("FRA").getFlagCode());
    flagUser.setRoles(getRolesFlag());
    Mockito.when(userProfileService.getUser(flagUser.getUserId())).thenReturn(flagUser);


    // Mock the flag association
    List<String> secondaryFlagCodes = new ArrayList<>();
    secondaryFlagCodes.add(mockedFlagsMap.get("FAT").getFlagCode());
    secondaryFlagCodes.add(mockedFlagsMap.get("RIF").getFlagCode());
    secondaryFlagCodes.add(mockedFlagsMap.get("ALA").getFlagCode());
    Mockito.when(flagsAssociationsService.getAssociatedFlags(mockedFlagsMap.get("FRA").getFlagCode()))
        .thenReturn(secondaryFlagCodes);

    final List<FlagStateHDto> flagStateHDtos = flagStateService.getUserFlagStates(flagUser.getUserId());

    Assert.assertNotNull(flagStateHDtos);
    // Exclude the deleted flag
    Assert.assertEquals(3, flagStateHDtos.size());
    flagStateHDtos.forEach(flag -> {
      Assert.assertFalse(flag.getDeleted());
    });
  }

  /**
   * Tests success scenario to get reference data flags {@link FlagStateService#getFlagStates()}.
   * The method should exclude those flags that marked as deleted.
   *
   * @throws Exception if mast api fails.
   */
  @Test
  public void testGetAllFlagStatesSuccess() throws Exception {

    // Mock flag list
    final Map<String, FlagStateHDto> mockedFlagsMap = mockFlags();
    final List<FlagStateHDto> mockedFlags = new ArrayList<>(mockedFlagsMap.values());
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockedFlags);

    final List<FlagStateHDto> flagStateHDtos = flagStateService.getFlagStates();

    Assert.assertNotNull(flagStateHDtos);
    // Exclude the deleted flag
    Assert.assertEquals(mockedFlags.size() - 1, flagStateHDtos.size());
    flagStateHDtos.forEach(flag -> {
      Assert.assertFalse(flag.getDeleted());
    });

  }

  /**
   * Tests failure scenario to get reference data flags {@link FlagStateService#getFlagStates()}
   * when internal call fails. The method should return empty list.
   *
   * @throws Exception if mast api fails.
   */
  @Test
  public void testGetAllFlagStatesFail() throws Exception {
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(Collections.<FlagStateHDto>emptyList());
    final List<FlagStateHDto> flagStates = flagStateService.getFlagStates();
    Assert.assertNotNull(flagStates);
    Assert.assertTrue(flagStates.isEmpty());
  }

  /**
   * Mocks an asset with given id and flag.
   *
   * @param id the asset id.
   * @param flagState the asset flag.
   * @return the mocked asset model.
   */
  private AssetHDto mockAsset(final Long id, final FlagStateHDto flagState) {
    final AssetHDto dto = new AssetHDto();
    dto.setId(id);
    dto.setFlagStateDto(flagState);
    return dto;
  }

  /**
   * Gets the roles pre-populated with lr_admin.
   *
   * @return roles the roles pre-populated with lr_admin.
   */
  private Set<Roles> getRolesLrAdmin() {
    Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setIsAdmin(true);
    role.setIslrAdmin(true);
    role.setRoleName(Role.LR_ADMIN.name());
    roles.add(role);
    return roles;
  }

  /**
   * Gets the roles pre-populated with client.
   *
   * @return roles the roles pre-populated with client.
   */
  private Set<Roles> getRolesClient() {
    Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setIsAdmin(false);
    role.setIslrAdmin(false);
    role.setRoleName(Role.CLIENT.name());
    roles.add(role);
    return roles;
  }

  /**
   * Gets the roles pre-populated with ship builder.
   *
   * @return roles the roles pre-populated with ship builder.
   */
  private Set<Roles> getRolesShipBuilder() {
    Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setIsAdmin(false);
    role.setIslrAdmin(false);
    role.setRoleName(Role.SHIP_BUILDER.name());
    roles.add(role);
    return roles;
  }

  /**
   * Gets the roles pre-populated with flag.
   *
   * @return roles the roles pre-populated with flag.
   */
  private Set<Roles> getRolesFlag() {
    Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setIsAdmin(true);
    role.setIslrAdmin(true);
    role.setRoleName(Role.FLAG.name());
    roles.add(role);
    return roles;
  }

  /**
   * Mocks map of flags for testing including the one marked as deleted.
   *
   * @return the map of flags with the flag code as key.
   */
  private Map<String, FlagStateHDto> mockFlags() {
    final Map<String, FlagStateHDto> flags = new HashMap<>();

    final FlagStateHDto aiaFlagDto = new FlagStateHDto();
    aiaFlagDto.setId(1L);
    aiaFlagDto.setName("AIA");
    aiaFlagDto.setFlagCode("AIA");
    aiaFlagDto.setDeleted(false);
    flags.put(aiaFlagDto.getFlagCode(), aiaFlagDto);

    final FlagStateHDto alaFlagDto = new FlagStateHDto();
    alaFlagDto.setId(2L);
    alaFlagDto.setName("ALA");
    alaFlagDto.setFlagCode("ALA");
    alaFlagDto.setDeleted(true);
    flags.put(alaFlagDto.getFlagCode(), alaFlagDto);

    // Primary flag
    final FlagStateHDto fraFlagDto = new FlagStateHDto();
    fraFlagDto.setId(3L);
    fraFlagDto.setFlagCode("FRA");
    fraFlagDto.setName("France");
    fraFlagDto.setDeleted(false);
    flags.put(fraFlagDto.getFlagCode(), fraFlagDto);

    // Secondary flag to FRA
    final FlagStateHDto rifFlagDto = new FlagStateHDto();
    rifFlagDto.setId(4L);
    rifFlagDto.setFlagCode("RIF");
    rifFlagDto.setName("French International Register");
    rifFlagDto.setDeleted(false);
    flags.put(rifFlagDto.getFlagCode(), rifFlagDto);

    // Secondary flag to FRA
    final FlagStateHDto fatFlagDto = new FlagStateHDto();
    fatFlagDto.setId(4L);
    fatFlagDto.setFlagCode("FAT");
    fatFlagDto.setName("French Antarctic Territory");
    fatFlagDto.setDeleted(false);
    flags.put(fatFlagDto.getFlagCode(), fatFlagDto);

    return flags;
  }

  /**
   * Create empty asset page resource.
   *
   * @param pageNumber current page number.
   * @param totalPages total number of pages.
   * @param totalNumberOfElements the total number of elements.
   * @return asset page resource.
   */
  private AssetPageResource createEmptyAssetPageResourceWithDifferentPage(final Integer pageNumber,
      final Integer totalPages, final Long totalNumberOfElements) {
    final AssetPageResource assets = new AssetPageResource();
    assets.setContent(Collections.singletonList(new AssetHDto()));

    final PaginationDto pagination = new PaginationDto();
    pagination.setNumber(pageNumber);
    pagination.setTotalPages(totalPages);
    pagination.setTotalElements(totalNumberOfElements);
    assets.setPagination(pagination);

    return assets;
  }

  /**
   * Provides the needed Spring bean for testing purpose.
   *
   * @author msidek
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public FlagStateService flagStateService() {
      return new FlagStateServiceImpl();
    }
  }
}
