package com.baesystems.ai.lr.cd.service.cache.test;

import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.asset.reference.impl.AssetReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.cache.CacheContainer;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

import retrofit2.mock.Calls;

/**
 * Test case for {@link CacheContainer}.
 *
 * @author msidek
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CacheContainerTest.Config.class)
public class CacheContainerTest {
  /**
   * Default object count.
   *
   */
  private static final Integer DEFAULT_COUNT = 30;

  /**
   * Injected asset ref service.
   *
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * Injected asset delegate.
   *
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceDelegate;

  /**
   * Injected cache.
   *
   */
  @Autowired
  private CacheContainer cache;

  /**
   * Reset mock behaviour.
   *
   */
  @Before
  public void reset() {
    Mockito.reset(assetReferenceDelegate, assetReferenceService, cache);
  }

  /**
   * Test for {@link CacheContainer#get(Class)} and
   * {@link CacheContainer#set(Class, Map)} .
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void cacheGetAndSet() throws Exception {
    mockAssetCategories(DEFAULT_COUNT);
    List<AssetCategoryHDto> assetCategories = assetReferenceService.getAssetCategories();
    Assert.assertNotNull(assetCategories);
    Assert.assertNotEquals(0, assetCategories.size());

    Mockito.verify(assetReferenceDelegate, Mockito.atLeastOnce()).getAssetCategories();
    Mockito.verify(cache, Mockito.atLeastOnce()).get(AssetCategoryHDto.class);
    Mockito.verify(cache, Mockito.atLeastOnce()).set(anyObject(), Mockito.any(Map.class));

    assetCategories = assetReferenceService.getAssetCategories();
    Assert.assertNotNull(assetCategories);
    Assert.assertNotEquals(0, assetCategories.size());

    Mockito.verify(assetReferenceDelegate, Mockito.atMost(2)).getAssetCategories();
  }

  /**
   * Mock asset categories.
   *
   * @param count count of object.
   *
   */
  private void mockAssetCategories(final Integer count) {
    final List<AssetCategoryHDto> assetCategories = new ArrayList<>();
    for (Integer i = 0; i < count; i++) {
      final AssetCategoryHDto assetCategory = new AssetCategoryHDto();
      assetCategory.setId(i.longValue());
      assetCategories.add(assetCategory);
    }

    Mockito.when(assetReferenceDelegate.getAssetCategories()).thenReturn(Calls.response(assetCategories));
  }

  /**
   * Spring config class.
   *
   * @author msidek
   *
   */
  @Configuration
  @EnableAspectJAutoProxy
  public static class Config extends BaseMockConfiguration {

    /**
     * Mock {@link AssetReferenceRetrofitService}.
     *
     * @return mock service.
     *
     */
    @Bean
    public AssetReferenceRetrofitService assetReferenceDelegate() {
      return Mockito.mock(AssetReferenceRetrofitService.class);
    }

    @Bean
    @Override
    public CacheContainer cache() {
      return Mockito.spy(new CacheContainer());
    }

    /**
     * Return spied {@link AssetReferenceService}.
     *
     * @return asset reference service.
     *
     */
    @Bean
    public AssetReferenceService assetReferenceService() {
      return Mockito.spy(new AssetReferenceServiceImpl());
    }
  }
}
