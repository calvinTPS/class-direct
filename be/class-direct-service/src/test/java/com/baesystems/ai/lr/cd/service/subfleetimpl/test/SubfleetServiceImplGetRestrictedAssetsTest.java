package com.baesystems.ai.lr.cd.service.subfleetimpl.test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.subfleet.impl.SubFleetServiceImpl;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import static com.baesystems.ai.lr.cd.service.asset.impl.test.MastQueryBuilderTest.assertJoinFieldWithoutValue;
import static com.baesystems.ai.lr.cd.service.asset.impl.test.MastQueryBuilderTest.checkJoinFieldIsSet;
import static com.baesystems.ai.lr.cd.service.asset.impl.test.MastQueryBuilderTest.checkOnlyAndSet;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
/**
 * Unit test for {@link SubFleetService#getRestrictedAssets(String, Integer, Integer)}.
 *
 * @author Faizal Sidek
 */
@RunWith(MockitoJUnitRunner.class)
public class SubfleetServiceImplGetRestrictedAssetsTest {

  /**
   * Default user id.
   */
  private static final String DEFAULT_USER_ID = "37be2b5b-63ee-4535-bca4-202d556d626e";

  /**
   * Maximum page size.
   */
  private static final Integer MAX_PAGE_SIZE = 16;

  /**
   * Primary class to be test.
   */
  @InjectMocks
  private SubFleetService subFleetService = new SubFleetServiceImpl();

  /**
   * Mock app context.
   */
  @Mock
  private ApplicationContext applicationContext;

  /**
   * Mock {@link UserProfileService}.
   */
  @Mock
  private UserProfileService userProfileService;

  /**
   * Mock {@link AssetService}.
   */
  @Mock
  private AssetService assetService;

  /**
   * Argument captor for {@link MastQueryBuilder}.
   */
  @Captor
  private ArgumentCaptor<MastQueryBuilder> queryBuilderCaptor;

  /**
   * Mock subfleet service.
   */
  private SubFleetService self;

  /**
   * Initialize test case.
   *
   * @throws Exception when general error occurs.
   */
  @Before
  public final void setup() throws Exception {
    self = mock(SubFleetService.class);
    when(applicationContext.getBean(eq(SubFleetService.class))).thenReturn(self);

    ((SubFleetServiceImpl) subFleetService).afterPropertiesSet();
  }

  /**
   * Method getRestrictedAssets will return all assets when user has restrictAll flag.
   *
   * @throws ClassDirectException when general error occurs.
   * @throws IOException          when retrofit error occurs.
   */
  @Test
  public final void shouldReturnsAllAssetsIfUserHasRestrictAllFlag() throws ClassDirectException, IOException {
    mockGetAccessibleAssetIdsForUser();
    mockExecuteMastIhsQuery();
    mockGetUser(true);

    final PageResource<AssetHDto> assets = subFleetService.getRestrictedAssets(DEFAULT_USER_ID, 0, MAX_PAGE_SIZE);
    assertNotNull(assets);
    assertNotNull(assets.getContent());
    assertFalse(assets.getContent().isEmpty());

    verify(self, atMost(1)).getAccessibleAssetIdsForUser(eq(DEFAULT_USER_ID));
    verify(assetService, atMost(1)).executeMastIhsQuery(any(), eq(0), eq(MAX_PAGE_SIZE), eq(null), eq(null));
    final AbstractQueryDto captured = queryBuilderCaptor.getValue().build();
    assertNotNull(captured);
    checkOnlyAndSet(captured);
    checkJoinFieldIsSet(captured.getAnd().get(0));
    assertJoinFieldWithoutValue(captured.getAnd().get(0), "publishedVersion", "id", QueryRelationshipType.IN);
  }

  /**
   * Method getRestrictedAssets will return all assets when user is not restricted.
   *
   * @throws ClassDirectException when general error occurs.
   * @throws IOException          when retrofit error occurs.
   */
  @Test
  public final void shouldReturnAssetsRestrictedByAvailableAssetIds() throws ClassDirectException, IOException {
    mockGetAccessibleAssetIdsForUser();
    mockExecuteMastIhsQuery();
    mockGetUser(false);
    mockGetSubfleetIds(DEFAULT_USER_ID);

    final PageResource<AssetHDto> assets = subFleetService.getRestrictedAssets(DEFAULT_USER_ID, 0, MAX_PAGE_SIZE);
    assertNotNull(assets);
    assertNotNull(assets.getContent());
    assertFalse(assets.getContent().isEmpty());

    verify(assetService, atMost(1)).executeMastIhsQuery(any(), eq(0), eq(MAX_PAGE_SIZE), eq(null), eq(null));
    verify(self, atMost(1)).getAccessibleAssetIdsForUser(eq(DEFAULT_USER_ID));
    verify(self, atMost(1)).getSubFleetIds(eq(DEFAULT_USER_ID));

    final AbstractQueryDto captured = queryBuilderCaptor.getValue().build();
    assertNotNull(captured);
    checkOnlyAndSet(captured);
    checkJoinFieldIsSet(captured.getAnd().get(0));
    assertJoinFieldWithoutValue(captured.getAnd().get(0), "publishedVersion", "id", QueryRelationshipType.IN);
  }

  /**
   * Mock {@link SubFleetService#getAccessibleAssetIdsForUser(String)}.
   *
   * @throws ClassDirectException when error happen.
   */
  private void mockGetAccessibleAssetIdsForUser() throws ClassDirectException {
    final Integer assetCount = 10;
    final List<Long> assetIds = new ArrayList<>();
    for (int i = 0; i < assetCount; i++) {
      assetIds.add((long) i + 1);
    }
    when(self.getAccessibleAssetIdsForUser(eq(DEFAULT_USER_ID))).thenReturn(assetIds);
  }

  /**
   * Mock {@link AssetService#executeMastIhsQuery(MastQueryBuilder, Integer, Integer, SortOptionEnum, OrderOptionEnum)}.
   *
   * @throws ClassDirectException when error happen.
   */
  private void mockExecuteMastIhsQuery() throws ClassDirectException {
    final AssetPageResource assetPageResource = new AssetPageResource();
    assetPageResource.setContent(Collections.singletonList(new AssetHDto()));
    assetPageResource.setPagination(new PaginationDto());
    when(assetService.executeMastIhsQuery(queryBuilderCaptor.capture(), anyInt(), anyInt(), eq(null), eq(null)))
      .thenReturn(assetPageResource);
  }

  /**
   * Mock {@link UserProfileService#getUser(String)}.
   *
   * @param isRestricted set restrictAll flag.
   * @throws ClassDirectException when error.
   */
  private void mockGetUser(final Boolean isRestricted) throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setRestrictAll(isRestricted);
    when(userProfileService.getUser(eq(DEFAULT_USER_ID))).thenReturn(user);
  }

  /**
   * Mock {@link SubFleetService#getSubFleetIds(String)}.
   *
   * @param userId user id.
   * @throws ClassDirectException when general error happen.
   * @throws IOException when io exception happen.
   */
  private void mockGetSubfleetIds(final String userId) throws ClassDirectException, IOException {
    final Integer assetCount = 5;
    final List<Long> assetIds = new ArrayList<>();
    for (int i = 0; i < assetCount; i++) {
      assetIds.add((long) i + 1);
    }
    when(self.getSubFleetIds(eq(userId))).thenReturn(assetIds);
  }
}
