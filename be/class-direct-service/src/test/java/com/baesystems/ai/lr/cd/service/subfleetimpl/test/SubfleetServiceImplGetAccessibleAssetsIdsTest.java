package com.baesystems.ai.lr.cd.service.subfleetimpl.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.core.util.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BaseDtoPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.PartyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.PartyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.subfleet.impl.SubFleetServiceImpl;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.paging.PaginationDto;

import retrofit2.mock.Calls;

/**
 * Unit test for {@link SubFleetService#getAccessibleAssetIdsForUser(String)}.
 *
 * @author Faizal Sidek
 */
@RunWith(MockitoJUnitRunner.class)
public class SubfleetServiceImplGetAccessibleAssetsIdsTest {

  /**
   * Default user id.
   */
  private static final String USER_ID = "37be2b5b-63ee-4535-bca4-202d556d626e";

  /**
   * Default flag code.
   */
  private static final String DEFAULT_FLAG = "ALA";

  /**
   * Test class.
   */
  @InjectMocks
  private SubFleetService subFleetService = new SubFleetServiceImpl();

  /**
   * Injected user profile service.
   */
  @Mock
  private UserProfileService userProfileService;

  /**
   * Injected asset service.
   */
  @Mock
  private AssetService assetService = new AssetServiceImpl();

  /**
   * Injected retrofit service.
   */
  @Mock
  private AssetRetrofitService assetRetrofitService;

  /**
   * Mock flag associated service.
   */
  @Mock
  private FlagsAssociationsService flagsAssociationsService;

  /**
   * Mock flag state service.
   */
  @Mock
  private FlagStateService flagStateService;

  /**
   * Captor for asset query.
   */
  @Captor
  private ArgumentCaptor<AssetQueryHDto> assetQueryCaptor;

  /**
   * Maximum page size.
   */
  private static final Integer MAX_PAGE_SIZE = 16;

  /**
   * Setup field injection.
   *
   * @throws NoSuchFieldException when error.
   * @throws IllegalAccessException when error.
   */
  @Before
  public final void setup() throws NoSuchFieldException, IllegalAccessException {
    final Field maxPageSizeField = subFleetService.getClass().getDeclaredField("maximumPageSize");
    maxPageSizeField.setAccessible(true);
    maxPageSizeField.set(subFleetService, MAX_PAGE_SIZE);
  }

  /**
   * Run {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for
   * client/builder user.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void getAccessibleAssetIdsForClientOrBuilderUser() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setClientCode("1");
    user.setClientType("DOC_COMPANY");
    List<PartyHDto> patyDetails = new ArrayList<>();
    PartyHDto party = new PartyHDto();
    party.setId(1L);
    patyDetails.add(party);
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(Collections.emptyList());
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, 1, 1L)));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), Mockito.any(Integer.class),
        Mockito.any(Integer.class))).thenReturn(Calls.response(assetPageResource));
    Mockito.when(userProfileService.retrieveClientInformation(Mockito.any(Integer.class), Mockito.any(Integer.class),
        Mockito.any(PartyQueryHDto.class))).thenReturn(patyDetails);

    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, times(1)).assetQueryAsBaseDto(assetQueryCaptor.capture(), any(), any());
    verify(assetRetrofitService, never()).assetQuery(any(), anyInt(), any());
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getFlagStateId());
  }

  /**
   * Tests failure scenario to get accessible asset id's
   * {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for client/builder
   * user fail to get client information.
   *
   * @throws ClassDirectException when fail to get client information/ mast api call fail.
   */
  @Test
  public final void getAccessibleAssetIdsFailForClientUser() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setClientCode("1");
    List<PartyHDto> patyDetails = new ArrayList<>();
    PartyHDto party = new PartyHDto();
    party.setId(1L);
    patyDetails.add(party);
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(Collections.emptyList());
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, 1, 1L)));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), Mockito.any(Integer.class),
        Mockito.any(Integer.class))).thenReturn(Calls.response(assetPageResource));
    Mockito.when(userProfileService.retrieveClientInformation(anyInt(), anyInt(), any()))
        .thenThrow(new ClassDirectException("Mock exception"));

    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, times(1)).assetQueryAsBaseDto(assetQueryCaptor.capture(), any(), any());
    verify(assetRetrofitService, never()).assetQuery(any(), anyInt(), any());
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getFlagStateId());
    assertNull(assetQueryCaptor.getAllValues().get(0).getPartyRoleId());

  }

  /**
   * Run {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for other users.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void getAccessibleAssetIdsForNonClientBuilderOrFlagUser() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);

    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(Collections.emptyList());
    final PaginationDto pagination = new PaginationDto();
    pagination.setTotalPages(1);
    assetPageResource.setPagination(pagination);
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, 1, 1L)));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), eq(0), eq(MAX_PAGE_SIZE)))
        .thenReturn(Calls.response(assetPageResource));
    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, atLeastOnce()).assetQueryAsBaseDto(assetQueryCaptor.capture(), anyInt(), anyInt());
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    Assert.isEmpty(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getFlagStateId());
    assertNull(assetQueryCaptor.getAllValues().get(0).getBuilder());
  }

  /**
   * Run {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for flag user.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void getAccessibleAssetIdsForFlagUser() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setFlagCode(DEFAULT_FLAG);
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);

    List<FlagStateHDto> flags = new ArrayList<>();
    FlagStateHDto flag1 = new FlagStateHDto();
    flag1.setId(1L);
    flag1.setFlagCode("IND");
    flags.add(flag1);
    FlagStateHDto flag2 = new FlagStateHDto();
    flag1.setId(2L);
    flag1.setFlagCode(DEFAULT_FLAG);
    flags.add(flag2);

    List<String> flagCodes = new ArrayList<>();
    flagCodes.add(DEFAULT_FLAG);

    final BaseDtoPageResource basePageResource = new BaseDtoPageResource();
    final BaseDto asset = new BaseDto();
    asset.setId(1234L);
    basePageResource.setContent(Collections.singletonList(asset));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, 1, 1L)));
    when(assetRetrofitService.assetQueryAsBaseDto(any(), Mockito.any(Integer.class), Mockito.any(Integer.class)))
        .thenReturn(Calls.response(basePageResource));
    when(flagStateService.getFlagStates()).thenReturn(flags);
    when(flagsAssociationsService.getAssociatedFlags(any())).thenReturn(flagCodes);

    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, atMost(1)).assetQueryAsBaseDto(assetQueryCaptor.capture(), Mockito.any(Integer.class),
        Mockito.any(Integer.class));
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    Assert.isEmpty(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getBuilder());
  }

  /**
   * Tests failure scenario to get accessible asset id's
   * {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for flag user when
   * flag states api fails.
   *
   * @throws ClassDirectException when get flag states for asset api fail or mast api call fail.
   */
  @Test
  public final void getAccessibleAssetIdsFailForFlagUser() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setFlagCode(DEFAULT_FLAG);
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);

    List<String> flagCodes = new ArrayList<>();
    flagCodes.add(DEFAULT_FLAG);

    final BaseDtoPageResource basePageResource = new BaseDtoPageResource();
    final BaseDto asset = new BaseDto();
    asset.setId(1234L);
    basePageResource.setContent(Collections.singletonList(asset));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, 1, 1L)));
    when(assetRetrofitService.assetQueryAsBaseDto(any(), Mockito.any(Integer.class), Mockito.any(Integer.class)))
        .thenReturn(Calls.response(basePageResource));
    when(flagStateService.getFlagStates()).thenThrow(new ClassDirectException("Mock Exception"));
    when(flagsAssociationsService.getAssociatedFlags(any())).thenReturn(flagCodes);

    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, atMost(1)).assetQueryAsBaseDto(assetQueryCaptor.capture(), Mockito.any(Integer.class),
        Mockito.any(Integer.class));
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    Assert.isEmpty(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getFlagStateId());
    assertNull(assetQueryCaptor.getAllValues().get(0).getBuilder());
  }

  /**
   * Run {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for admin user
   * in thread.
   *
   * @throws ClassDirectException when service throws unexpected error.
   */
  @Test
  public final void getAccessibleAssetIdForAdminWillRunMultiThread() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);

    final Integer totalPages = 3;
    final Long totalNumberOfElements = 45L;

    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(Collections.emptyList());
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1))).thenReturn(
        Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, totalPages, totalNumberOfElements)));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(MAX_PAGE_SIZE)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, totalPages, totalNumberOfElements)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(1, totalPages, totalNumberOfElements)))
        .thenReturn(
            Calls.response(createEmptyAssetPageResourceWithDifferentPage(2, totalPages, totalNumberOfElements)));

    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, times(totalPages)).assetQueryAsBaseDto(assetQueryCaptor.capture(), anyInt(),
        eq(MAX_PAGE_SIZE));
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    Assert.isEmpty(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getFlagStateId());
    assertNull(assetQueryCaptor.getAllValues().get(0).getBuilder());
  }

  /**
   * Tests success scenario to get accessible asset id's
   * {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for builder.
   *
   * @throws ClassDirectException mast api call fail or error in execution flow.
   */
  @Test
  public final void getAccessibleAssetIdsForBuilderUser() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setShipBuilderCode("12345");
    user.setCompanyName("builder");
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);

    final BaseDtoPageResource basePageResource = new BaseDtoPageResource();
    final BaseDto asset = new BaseDto();
    asset.setId(1234L);
    basePageResource.setContent(Collections.singletonList(asset));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, 1, 1L)));
    when(assetRetrofitService.assetQueryAsBaseDto(any(), Mockito.any(Integer.class), Mockito.any(Integer.class)))
        .thenReturn(Calls.response(basePageResource));

    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, atMost(1)).assetQueryAsBaseDto(assetQueryCaptor.capture(), Mockito.any(Integer.class),
        Mockito.any(Integer.class));
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    Assert.isEmpty(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getFlagStateId());
  }

  /**
   * Tests failure scenario to get accessible asset id's
   * {@link SubFleetService#getAccessibleAssetIdsForUserWithoutCaching(String)} for builder when
   * user company null.
   *
   * @throws ClassDirectException mast api call fail or error in execution flow.
   */
  @Test
  public final void getAccessibleAssetIdsFailForBuilderUser() throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setShipBuilderCode("12345");
    when(userProfileService.getUser(eq(USER_ID))).thenReturn(user);

    final BaseDtoPageResource basePageResource = new BaseDtoPageResource();
    final BaseDto asset = new BaseDto();
    asset.setId(1234L);
    basePageResource.setContent(Collections.singletonList(asset));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(any(), anyInt(), eq(1)))
        .thenReturn(Calls.response(createEmptyAssetPageResourceWithDifferentPage(0, 1, 1L)));
    when(assetRetrofitService.assetQueryAsBaseDto(any(), Mockito.any(Integer.class), Mockito.any(Integer.class)))
        .thenReturn(Calls.response(basePageResource));

    subFleetService.getAccessibleAssetIdsForUserWithoutCaching(USER_ID);
    verify(assetRetrofitService, atMost(1)).assetQueryAsBaseDto(assetQueryCaptor.capture(), Mockito.any(Integer.class),
        Mockito.any(Integer.class));
    assertNotNull(assetQueryCaptor.getAllValues().get(0).getClientIds());
    Assert.isEmpty(assetQueryCaptor.getAllValues().get(0).getClientIds());
    assertNull(assetQueryCaptor.getAllValues().get(0).getFlagStateId());
    assertNull(assetQueryCaptor.getAllValues().get(0).getBuilder());
  }

  /**
   * Create empty asset page resource.
   *
   * @param pageNumber current page number.
   * @param totalPages total number of pages.
   * @param totalNumberOfElements the total number of elements.
   * @return asset page resource.
   */
  private BaseDtoPageResource createEmptyAssetPageResourceWithDifferentPage(final Integer pageNumber,
      final Integer totalPages, final Long totalNumberOfElements) {
    final BaseDtoPageResource assets = new BaseDtoPageResource();
    assets.setContent(Collections.singletonList(new BaseDto()));

    final PaginationDto pagination = new PaginationDto();
    pagination.setNumber(pageNumber);
    pagination.setTotalPages(totalPages);
    pagination.setTotalElements(totalNumberOfElements);
    assets.setPagination(pagination);

    return assets;
  }
}
