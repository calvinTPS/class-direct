package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RectificationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.DeficiencyService;
import com.baesystems.ai.lr.cd.service.asset.impl.DeficiencyServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.CodeLinkResource;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.defects.DeficiencyChecklistDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyChecklistLinkDto;
import com.baesystems.ai.lr.dto.defects.RectificationChecklistDto;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Provides unit tests for deficiency service {@link DeficiencyService}.
 *
 * @author syalavarthi.
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest({Response.class, Resources.class})
@ContextConfiguration(classes = DeficiencyServiceTest.Config.class)
public class DeficiencyServiceTest {
  /**
   * The {@link AssetRetrofitService} from spring context.
   *
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The {@link DeficiencyService} from spring context.
   *
   */
  @Autowired
  private DeficiencyService deficiencyService;
  /**
   * The logger for deficiency service test {@link DeficiencyServiceTest}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(DeficiencyServiceTest.class);

  /**
   * Tests success scenario for
   * {@link DeficiencyService#getDeficiencies(Long, DeficiencyQueryHDto)}.
   *
   * @throws Exception if mast api call fails.
   */
  @Test
  public final void testGetdeficiencyList() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(Calls.response(getDeficiencies())).when(assetRetrofitService).getDeficiencies(Matchers.anyLong(),
        Matchers.any(DeficiencyQueryHDto.class));
    final List<DeficiencyHDto> response = deficiencyService.getDeficiencies(1L, new DeficiencyQueryHDto());
    int workItemsSize = 1;
    Assert.assertNotNull(response);
    Assert.assertEquals(response.get(0).getTitle(), "Deficiency Title");
    Assert.assertEquals(response.get(0).getStatusH().getName(), "Open");
    Assert.assertEquals(response.get(0).getDeficiencyChecklists().get(0).getWorkItem().getName(), "task 1");
    Assert.assertEquals(response.get(0).getDeficiencyChecklists().size(), workItemsSize);
  }

  /**
   * Tests failure scenario for {@link DeficiencyService#getDeficiencies(Long, DeficiencyQueryHDto)}
   * when mast api call fails.
   *
   * @throws Exception if mast api call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetdeficiencyListFail() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(assetRetrofitService.getDeficiencies(Matchers.anyLong(), Matchers.any(DeficiencyQueryHDto.class)))
        .thenReturn(Calls.failure(new IOException()));

    deficiencyService.getDeficiencies(1L, new DeficiencyQueryHDto());
  }

  /**
   * Tests success scenario for
   * {@link DeficiencyService#getDeficiencyByAssetIdDeficiencyId(Long, long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test
  public final void testGetdeficiencyByAssetId() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(Calls.response(mockDeficiency())).when(assetRetrofitService).getDeficiencyDto(Matchers.anyLong(),
        Matchers.anyLong());
    doReturn(Calls.response(mockRectification())).when(assetRetrofitService)
        .getRectificationByRectificationId(Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong());
    doReturn(Calls.response(mockStatutoryFindings())).when(assetRetrofitService)
        .getStatutoryFindingsForDeficiency(Mockito.anyLong(), Mockito.anyLong());

    int workItemsSize = 1;

    int size = mockStatutoryFindings().size();
    final DeficiencyHDto deficiency = deficiencyService.getDeficiencyByAssetIdDeficiencyId(1L, 1L);
    Assert.assertNotNull(deficiency);
    Assert.assertEquals(deficiency.getTitle(), "Deficiency Title");
    Assert.assertEquals(deficiency.getStatusH().getName(), "Open");
    Assert.assertNotNull(deficiency.getStatutoryFindings());
    Assert.assertEquals(deficiency.getStatutoryFindings().get(0).getDescription(), "Statutory1");
    Assert.assertEquals(deficiency.getStatutoryFindings().size(), size);
    Assert.assertEquals(deficiency.getStatutoryFindings().size(), mockStatutoryFindings().size());
    Assert.assertNotNull(deficiency.getRectificationsH());
    Assert.assertEquals(deficiency.getRectificationsH().get(0).getNarrative(), "Rectification1");
    Assert.assertEquals(deficiency.getDeficiencyChecklists().get(0).getWorkItem().getName(), "task 1");
    Assert.assertEquals(deficiency.getDeficiencyChecklists().size(), workItemsSize);

    // Rectification checklist checking
    Assert.assertNotNull(deficiency.getRectificationsH().get(0).getRectificationChecklists());
    Assert.assertEquals(1, deficiency.getRectificationsH().get(0).getRectificationChecklists().size());
    Assert.assertEquals("task 1", deficiency.getRectificationsH().get(0).getRectificationChecklists().get(0)
        .getDeficiencyChecklist().getWorkItem().getName());
  }

  /**
   * Tests failure scenario for
   * {@link DeficiencyService#getDeficiencyByAssetIdDeficiencyId(Long, long)} if work item is empty.
   *
   * @throws Exception if mast api call fails.
   */
  @Test
  public final void testGetdeficiencyByAssetIdWorkItemNull() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(Calls.response(mockDeficiency())).when(assetRetrofitService).getDeficiencyDto(Matchers.anyLong(),
        Matchers.anyLong());
    doReturn(Calls.response(mockRectification())).when(assetRetrofitService)
        .getRectificationByRectificationId(Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong());
    doReturn(Calls.response(mockStatutoryFindings())).when(assetRetrofitService)
        .getStatutoryFindingsForDeficiency(Mockito.anyLong(), Mockito.anyLong());

    doReturn(Calls.failure(new IOException("Mock retrofit call"))).when(assetRetrofitService)
        .getTasksByAssetId(Mockito.anyLong());

    int size = mockStatutoryFindings().size();
    final DeficiencyHDto deficency = deficiencyService.getDeficiencyByAssetIdDeficiencyId(1L, 1L);
    Assert.assertNotNull(deficency);
    Assert.assertEquals(deficency.getTitle(), "Deficiency Title");
    Assert.assertEquals(deficency.getStatusH().getName(), "Open");
    Assert.assertNotNull(deficency.getStatutoryFindings());
    Assert.assertEquals(deficency.getStatutoryFindings().get(0).getDescription(), "Statutory1");
    Assert.assertEquals(deficency.getStatutoryFindings().size(), size);
    Assert.assertEquals(deficency.getStatutoryFindings().size(), mockStatutoryFindings().size());
    Assert.assertNotNull(deficency.getRectificationsH());
    Assert.assertEquals(deficency.getRectificationsH().get(0).getNarrative(), "Rectification1");
    Assert.assertNotNull(deficency.getDeficiencyChecklists());
    Assert.assertFalse(deficency.getDeficiencyChecklists().isEmpty());

  }

  /**
   * getDeficiencyByAssetIdDeficiencyId: retrofit call fails.
   *
   * @throws Exception value.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetdeficiencyByAssetIdFail() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    // simulate MAST call failed
    when(assetRetrofitService.getDeficiencyDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    deficiencyService.getDeficiencyByAssetIdDeficiencyId(1L, 1L);
  }

  /**
   * getDeficiencyByAssetIdDeficiencyId: statutory call fails.
   *
   * @throws Exception value.
   */
  @Test
  public final void testGetdeficiencyFail() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(Calls.response(mockDeficiency())).when(assetRetrofitService).getDeficiencyDto(Matchers.anyLong(),
        Matchers.anyLong());
    when(
        assetRetrofitService.getRectificationByRectificationId(Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong()))
            .thenReturn(Calls.failure(new IOException()));
    when(assetRetrofitService.getStatutoryFindingsForDeficiency(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    final DeficiencyHDto deficency = deficiencyService.getDeficiencyByAssetIdDeficiencyId(1L, 1L);
    Assert.assertNotNull(deficency);
    Assert.assertEquals(deficency.getTitle(), "Deficiency Title");
    Assert.assertEquals(deficency.getStatusH().getName(), "Open");
    Assert.assertNotNull(deficency.getStatutoryFindings());
    Assert.assertTrue(deficency.getStatutoryFindings().isEmpty());
    Assert.assertNotNull(deficency.getRectificationsH());
    Assert.assertTrue(deficency.getRectificationsH().isEmpty());
  }

  /**
   * mock deficiencies.
   *
   * @return deficiency list.
   */
  private List<DeficiencyHDto> getDeficiencies() {
    List<DeficiencyHDto> deficiencies = new ArrayList<>();

    deficiencies.add(mockDeficiency());

    DeficiencyHDto deficiency = new DeficiencyHDto();
    deficiency.setId(2L);
    deficiency.setFlagStateConsulted(true);
    deficiencies.add(deficiency);

    return deficiencies;

  }

  /**
   * mock deficiency.
   *
   * @return deficiency.
   */
  private DeficiencyHDto mockDeficiency() {

    final DeficiencyHDto data1 = new DeficiencyHDto();
    data1.setId(1L);
    data1.setDeleted(false);
    data1.setJob(new LinkResource(1L));
    data1.getJob().setId(1L);
    data1.setTitle("Deficiency Title");
    data1.setConfidentialityType(new LinkResource(1L));
    data1.getConfidentialityType().setId(1L);
    data1.setRaisedOnJob(new LinkResource(1L));
    data1.getRaisedOnJob().setId(1L);
    data1.setClosedOnJob(new LinkResource(1L));
    data1.getClosedOnJob().setId(1L);
    data1.setIncidentDate(new Date());
    final DefectStatusHDto defectStatus = new DefectStatusHDto();
    defectStatus.setId(1L);
    defectStatus.setName("Open");
    data1.setStatusH(defectStatus);
    data1.setRectifications(new ArrayList<>());
    data1.getRectifications().add(new LinkResource(1L));

    List<DeficiencyChecklistDto> deficiencyChecklists = new ArrayList<>();
    DeficiencyChecklistDto deficiencyChecklist = new DeficiencyChecklistDto();
    CodeLinkResource namedLinkResource = new CodeLinkResource(1L);
    namedLinkResource.setName("task 1");
    deficiencyChecklist.setWorkItem(namedLinkResource);
    deficiencyChecklists.add(deficiencyChecklist);
    data1.setDeficiencyChecklists(deficiencyChecklists);

    return data1;
  }

  /**
   * mock StatutoryFindings.
   *
   * @return statutory findings.
   */
  private List<StatutoryFindingHDto> mockStatutoryFindings() {

    List<StatutoryFindingHDto> statutoryFindings = new ArrayList<>();

    StatutoryFindingHDto statutory1 = new StatutoryFindingHDto();
    statutory1.setId(1L);
    statutory1.setStatus(new LinkResource(23L));
    statutory1.setDescription("Statutory1");
    statutoryFindings.add(statutory1);

    StatutoryFindingHDto statutory2 = new StatutoryFindingHDto();
    statutory2.setId(2L);
    statutory2.setStatus(new LinkResource(21L));
    statutory2.setDescription("Statutory2");
    statutoryFindings.add(statutory2);

    return statutoryFindings;

  }

  /**
   * Mocks rectification dto with rectification checklist for unit test.
   *
   * @return the mocked rectification dto.
   */
  private RectificationHDto mockRectification() {

    final RectificationHDto rectification1 = new RectificationHDto();
    rectification1.setId(1L);
    rectification1.setNarrative("Rectification1");
    rectification1.setRepairAction(new LinkResource(1L));
    final RectificationChecklistDto rectificationChecklist = new RectificationChecklistDto();
    final DeficiencyChecklistLinkDto deficienctChecklistLink = new DeficiencyChecklistLinkDto();
    CodeLinkResource namedLinkResource = new CodeLinkResource(1L);
    namedLinkResource.setName("task 1");
    deficienctChecklistLink.setWorkItem(namedLinkResource);
    rectificationChecklist.setDeficiencyChecklist(deficienctChecklistLink);
    final List<RectificationChecklistDto> rectificationChecklists = new ArrayList<>(1);
    rectificationChecklists.add(rectificationChecklist);
    rectification1.setRectificationChecklists(rectificationChecklists);
    return rectification1;
  }


  /**
   * Mocks Deficiency Data.
   *
   * @return mock deficiencies.
   * @throws Exception if execution fails.
   */
  @SuppressWarnings("unchecked")
  private List<DeficiencyHDto> mockDeficiencyData() throws Exception {
    final ObjectMapper mapper = new ObjectMapper();
    List<DeficiencyHDto> response = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/Deficiency-List.json"), writer,
        Charset.defaultCharset());
    JavaType type =
        mapper.getTypeFactory().constructParametrizedType(ArrayList.class, List.class, DeficiencyHDto.class);
    response = mapper.readValue(writer.toString(), type);

    return response;
  }


  /**
   * Tests success scenario of display name for checkList item in DeficiencyChecklistItems for
   * {@link DeficiencyService#getDeficiencies(Long, DeficiencyQueryHDto)}.
   *
   * @throws Exception if mast API call fails.
   */
  @Test
  public final void shouldDisplayNameInDeficiencyChecklistItems() throws Exception {
    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    doReturn(Calls.response(mockDeficiencyData())).when(assetRetrofitService).getDeficiencies(Matchers.anyLong(),
        Matchers.any(DeficiencyQueryHDto.class));
    final List<DeficiencyHDto> response = deficiencyService.getDeficiencies(1L, new DeficiencyQueryHDto());

    Assert.assertEquals(response.get(0).getDeficiencyChecklists().get(0).getWorkItem().getName(), "Task 1");
  }


  /**
   * Spring in-class configurations.
   *
   * @author SYalavarthi.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create {@link DeficiencyService} bean.
     *
     * @return deficiency service.
     *
     */
    @Override
    @Bean
    public DeficiencyService deficiencyService() {
      return new DeficiencyServiceImpl();
    }

    /**
     * Create {@link AssetRetrofitService}.
     *
     * @return asset retrofit service.
     *
     */
    @Override
    @Bean
    public AssetRetrofitService assetRetrofitService() {
      return Mockito.mock(AssetRetrofitService.class);
    }

  }
}
