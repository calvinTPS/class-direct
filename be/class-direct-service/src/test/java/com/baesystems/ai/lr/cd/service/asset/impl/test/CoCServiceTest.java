package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCServiceRangeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RepairHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.StatutoryFindingHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.ProductCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.RepeatOfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ServiceScheduleDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.impl.CoCServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.defects.DefectItemLinkDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Provides unit test for {@link CoCService}.
 *
 * @author yng
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Response.class, Resources.class})
@ContextConfiguration(classes = CoCServiceTest.Config.class)
public class CoCServiceTest {

  /**
   * The logger instantiated for {@link CoCServiceTest}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CoCServiceTest.class);

  /**
   * Default year range.
   */
  private static final int DEFAULT_YEAR_RANGE = 7;

  /**
   * Default offset week.
   */
  private static final long DEFAULT_OFFSET_WEEK = 5;

  /**
   * The Spring application context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link CoCService} from {@link CoCServiceTest.Config} class.
   */
  @Autowired
  private CoCService cocService;

  /**
   * The {@link AssetReferenceService} from {@link CoCServiceTest.Config} class.
   */
  @Autowired
  private AssetReferenceService refService;

  /**
   * The notification period value from Spring properties file.
   */
  @Value("${notification.due.status.period}")
  private String notificationPeriodOffSet;

  /**
   * The {@link AssetRetrofitService} from {@link CoCServiceTest.Config} class.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * The {@link ServiceReferenceRetrofitService} from {@link CoCServiceTest.Config} class.
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceRefDelegate;
  /**
   * The {@link ServiceReferenceService} from spring context.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;
  /**
   * The {@link ProductReferenceService} from spring context.
   */
  @Autowired
  private ProductReferenceService productReferenceService;
  /**
   * The {@link JobRetrofitService} from spring context.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Provides initialization for mock object.
   */
  @Before
  public final void init() {
    reset(context.getBean(AssetRetrofitService.class));
    reset(assetService);
  }

  /**
   * Test positive case scenario for Calculate Due Status With Previous Date.
   */
  @Test
  public final void testCalculateDueStatusWithPreviousDate() {
    final Long sixDays = 6L;
    final Long oneDays = 1L;
    final CoCHDto cocHDto = new CoCHDto();
    cocHDto.setStatus(new LinkResource(14L));
    cocHDto.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(oneDays)));

    // final CocDueStatusCalculator cocDueStatusCal = new CocDueStatusCalculator();
    final DueStatus status = cocService.calculateDueStatus(cocHDto, LocalDate.now().minusDays(sixDays));
    Assert.assertEquals(DueStatus.IMMINENT, status);
  }

  /**
   * Test success scenario get service range {@link CoCService#getCoCServiceRange(long)} with all
   * apis success.
   *
   * @throws IOException if mast API call fail.
   */
  @Test
  public final void testGetCoCServiceServiceRange() throws IOException {

    final ZonedDateTime date = ZonedDateTime.now();

    final List<CoCHDto> cocs = new ArrayList<>();
    final CoCHDto coc = new CoCHDto();
    coc.setDueDate(Date.from(date.toInstant()));
    coc.setStatus(new LinkResource(14L));
    cocs.add(coc);

    final CoCHDto coc1 = new CoCHDto();
    coc1.setDueDate(Date.from(date.toInstant()));
    coc1.setStatus(new LinkResource(1L));
    cocs.add(coc1);

    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    final ActionableItemHDto aI = new ActionableItemHDto();
    aI.setDueDate(Date.from(date.toInstant()));
    aI.setStatus(new LinkResource(3L));
    actionableItems.add(aI);

    final ActionableItemHDto aI1 = new ActionableItemHDto();
    aI1.setDueDate(Date.from(date.toInstant()));
    aI1.setStatus(new LinkResource(2L));
    actionableItems.add(aI1);

    final List<StatutoryFindingHDto> statutories = new ArrayList<>();
    final StatutoryFindingHDto sf = new StatutoryFindingHDto();
    sf.setDueDate(Date.from(date.toInstant()));
    sf.setDueStatusH(DueStatus.OVER_DUE);
    statutories.add(sf);

    final StatutoryFindingHDto sf1 = new StatutoryFindingHDto();
    sf1.setDueDate(Date.from(date.toInstant()));
    sf1.setDueStatusH(DueStatus.IMMINENT);
    statutories.add(sf1);

    final List<ScheduledServiceHDto> services = new ArrayList<>();
    final ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setLowerRangeDate(null);
    service.setDueDate(Date.from(date.toInstant()));
    service.setServiceStatus(new LinkResource(2L));
    services.add(service);

    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setLowerRangeDate(null);
    service1.setDueDate(Date.from(date.toInstant()));
    service.setServiceStatus(new LinkResource(5L));
    services.add(service);

    final Map<String, Long> map = new HashMap<>();
    map.put("lowRange", date.toEpochSecond() * 1000L);
    map.put("highRange", date.plusYears(DEFAULT_YEAR_RANGE).toEpochSecond() * 1000L);

    Mockito.when(assetServiceDelegate.getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.anyLong()))
        .thenReturn(Calls.response(cocs));
    Mockito.when(serviceRefDelegate.getAssetServicesQuery(Mockito.anyLong(), Mockito.any()))
        .thenReturn(Calls.response(services));
    Mockito.when(
        assetServiceDelegate.getActionableItemQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.anyLong()))
        .thenReturn(Calls.response(actionableItems));
    Mockito.when(
        assetServiceDelegate.getStatutoryFindingsForAsset(Mockito.anyLong(), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(Calls.response(statutories));
    final Map<String, Long> list = cocService.getCoCServiceRange(1L);
    Assert.assertNotNull(list);

    final Instant i = Instant.ofEpochSecond(list.get("lowRange") / 1000L);
    final ZonedDateTime resultLowRange = ZonedDateTime.ofInstant(i, ZoneId.systemDefault());

    final Instant i2 = Instant.ofEpochSecond(list.get("highRange") / 1000L);
    final ZonedDateTime resultHighRange = ZonedDateTime.ofInstant(i2, ZoneId.systemDefault());

    Assert.assertEquals(date.getYear(), resultLowRange.getYear());
    Assert.assertEquals(date.getMonth(), resultLowRange.getMonth());
    Assert.assertEquals(date.getDayOfMonth(), resultLowRange.getDayOfMonth());


    final ZonedDateTime sevenYearsFromNow = date.plusYears(DEFAULT_YEAR_RANGE);
    Assert.assertEquals(sevenYearsFromNow.getYear(), resultHighRange.getYear());
    Assert.assertEquals(sevenYearsFromNow.getMonth(), resultHighRange.getMonth());
    Assert.assertEquals(sevenYearsFromNow.getDayOfMonth(), resultHighRange.getDayOfMonth());

  }

  /**
   * Test success scenario get service range with details
   * {@link CoCService#getCoCServiceRangeWithDetails(long)} with api success. Verify the dto for
   * cocs, actionable items, statutory findings, services, repeated services, low range date and
   * high range date is populated correctly.
   *
   * @throws IOException if mast API call fail.
   * @throws CloneNotSupportedException if the service object is unable to be cloned.
   * @throws ClassDirectException if there is error mast API to get statutory findings.
   */
  @Test
  public final void testGetCoCServiceServiceRangeWithDetails()
      throws IOException, CloneNotSupportedException, ClassDirectException {

    final ZonedDateTime currentDate = ZonedDateTime.now();
    final ZonedDateTime lowerRangeDate = currentDate.minusWeeks(DEFAULT_OFFSET_WEEK);
    final CoCServiceRangeHDto serviceRangeDto = new CoCServiceRangeHDto();
    final Instant minDueDateInstant = currentDate.toInstant();
    final Instant maxDueDateInstant = currentDate.plusYears(DEFAULT_YEAR_RANGE).toInstant();
    final Date minDueDate = Date.from(minDueDateInstant);
    final Date maxDueDate = Date.from(maxDueDateInstant);

    final List<CoCHDto> cocs = new ArrayList<>();
    final CoCHDto coc = new CoCHDto();
    coc.setDueDate(minDueDate);
    coc.setDueStatusH(DueStatus.OVER_DUE);
    cocs.add(coc);

    final CoCHDto coc1 = new CoCHDto();
    coc1.setDueDate(maxDueDate);
    coc1.setDueStatusH(DueStatus.NOT_DUE);
    cocs.add(coc1);
    serviceRangeDto.setCocHDtos(cocs);

    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    final ActionableItemHDto aI = new ActionableItemHDto();
    aI.setDueDate(maxDueDate);
    aI.setDueStatusH(DueStatus.IMMINENT);
    actionableItems.add(aI);

    final ActionableItemHDto aI1 = new ActionableItemHDto();
    aI1.setDueDate(minDueDate);
    aI1.setDueStatusH(DueStatus.DUE_SOON);
    actionableItems.add(aI1);
    serviceRangeDto.setActionableItemHDtos(actionableItems);

    final List<StatutoryFindingHDto> statutories = new ArrayList<>();
    final StatutoryFindingHDto sf = new StatutoryFindingHDto();
    sf.setDueDate(maxDueDate);
    sf.setDueStatusH(DueStatus.OVER_DUE);
    statutories.add(sf);

    final StatutoryFindingHDto sf1 = new StatutoryFindingHDto();
    sf1.setDueDate(minDueDate);
    sf1.setDueStatusH(DueStatus.IMMINENT);
    statutories.add(sf1);
    serviceRangeDto.setStatutoryFindingHDtos(statutories);

    // Lowest Due Date is set to serviceDto.
    final List<ScheduledServiceHDto> services = new ArrayList<>();
    final ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setDueDate(Date.from(lowerRangeDate.toInstant()));
    service.setServiceStatus(new LinkResource(2L));
    services.add(service);

    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setDueDate(maxDueDate);
    service1.setServiceStatus(new LinkResource(5L));
    services.add(service1);
    serviceRangeDto.setServices(services);

    final List<RepeatOfDto> repeatedServices = new ArrayList<>();
    serviceRangeDto.setRepeatedServices(repeatedServices);

    serviceRangeDto.setLowRange(lowerRangeDate.toEpochSecond() * 1000L);
    serviceRangeDto.setHighRange(lowerRangeDate.plusYears(DEFAULT_YEAR_RANGE).toEpochSecond() * 1000L);

    final CodicilActionableHDto codicilActionableHDto = new CodicilActionableHDto();
    codicilActionableHDto.setCocHDtos(cocs);
    codicilActionableHDto.setActionableItemHDtos(actionableItems);
    codicilActionableHDto.setStatutoryFindingHDtos(statutories);

    Mockito.when(assetService.getCodicils(Mockito.anyLong(), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(codicilActionableHDto);

    final ServiceScheduleDto serviceScheduleDto = new ServiceScheduleDto();
    serviceScheduleDto.setServices(services);
    serviceScheduleDto.setRepeatedServices(repeatedServices);

    Mockito
        .when(assetService.getServicesWithFutureDueDateSupport(Mockito.anyLong(), Mockito.any(ServiceQueryDto.class)))
        .thenReturn(serviceScheduleDto);

    final ServiceQueryDto query = new ServiceQueryDto();
    query.setMinDueDate(minDueDate);
    query.setMaxDueDate(maxDueDate);
    final CoCServiceRangeHDto resultDto = cocService.getCoCServiceRangeWithDetails(1L, query);
    Assert.assertNotNull(resultDto);

    Assert.assertEquals(serviceRangeDto.getLowRange(), resultDto.getLowRange());
    Assert.assertEquals(serviceRangeDto.getHighRange(), resultDto.getHighRange());

    Assert.assertEquals(resultDto.getCocHDtos(), serviceRangeDto.getCocHDtos());
    Assert.assertEquals(resultDto.getActionableItemHDtos(), serviceRangeDto.getActionableItemHDtos());
    Assert.assertEquals(resultDto.getStatutoryFindingHDtos(), serviceRangeDto.getStatutoryFindingHDtos());
    Assert.assertEquals(resultDto.getServices(), serviceRangeDto.getServices());
    Assert.assertEquals(resultDto.getRepeatedServices(), serviceRangeDto.getRepeatedServices());
  }

  /**
   * Test failure scenario to get service range {@link CoCService#getCoCServiceRange(long)} with
   * null values from mast api.
   *
   * @throws IOException if mast API call fail.
   */
  @Test
  public final void testGetCoCServiceNull() throws IOException {
    final ZonedDateTime now = ZonedDateTime.now();
    final List<CoCHDto> cocs = null;

    final List<ActionableItemHDto> actionableItems = null;

    final List<ScheduledServiceHDto> services = null;

    final List<StatutoryFindingHDto> statutories = null;

    final Map<String, Long> map = new HashMap<>();
    map.put("lowRange", now.toEpochSecond() * 1000L);
    map.put("highRange", now.plusYears(DEFAULT_YEAR_RANGE).toEpochSecond() * 1000L);

    Mockito.when(assetServiceDelegate.getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.anyLong()))
        .thenReturn(Calls.response(cocs));
    Mockito.when(serviceRefDelegate.getAssetServicesQuery(Mockito.anyLong(), Mockito.any()))
        .thenReturn(Calls.response(services));
    Mockito.when(
        assetServiceDelegate.getActionableItemQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.anyLong()))
        .thenReturn(Calls.response(actionableItems));

    Mockito.when(
        assetServiceDelegate.getStatutoryFindingsForAsset(Mockito.anyLong(), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(Calls.response(statutories));
    final Map<String, Long> list = cocService.getCoCServiceRange(1L);

    Assert.assertNotNull(list);

    final Instant i = Instant.ofEpochSecond(list.get("lowRange") / 1000L);
    final ZonedDateTime resultLowRange = ZonedDateTime.ofInstant(i, ZoneId.systemDefault());

    final Instant i2 = Instant.ofEpochSecond(list.get("highRange") / 1000L);
    final ZonedDateTime resultHighRange = ZonedDateTime.ofInstant(i2, ZoneId.systemDefault());

    Assert.assertEquals(now.getYear(), resultLowRange.getYear());
    Assert.assertEquals(now.getMonth(), resultLowRange.getMonth());
    Assert.assertEquals(now.getDayOfMonth(), resultLowRange.getDayOfMonth());


    final ZonedDateTime sevenYearsFromNow = now.plusYears(DEFAULT_YEAR_RANGE);
    Assert.assertEquals(sevenYearsFromNow.getYear(), resultHighRange.getYear());
    Assert.assertEquals(sevenYearsFromNow.getMonth(), resultHighRange.getMonth());
    Assert.assertEquals(sevenYearsFromNow.getDayOfMonth(), resultHighRange.getDayOfMonth());

  }

  /**
   * Test failure scenario to get service range with details
   * {@link CoCService#getCoCServiceRangeWithDetails(long)} with null values for cocs, actionable
   * items, statutories, services and repeated services from mast api. Verify that "low range date"
   * is set for current date and "high range date" is set 7 years from current date.
   *
   * @throws IOException if mast API call fail.
   * @throws CloneNotSupportedException if the service object is unable to be cloned.
   * @throws ClassDirectException if there is error mast API to get statutory findings.
   */
  @Test
  public final void testGetCoCServiceRangeNull() throws IOException, CloneNotSupportedException, ClassDirectException {
    final ZonedDateTime date = ZonedDateTime.now();
    final List<CoCHDto> cocs = null;
    final List<ActionableItemHDto> actionableItems = null;
    final List<StatutoryFindingHDto> statutories = null;
    final List<ScheduledServiceHDto> services = null;
    final List<RepeatOfDto> repeatedServices = null;
    final CoCServiceRangeHDto serviceRangeDto = new CoCServiceRangeHDto();
    final Date minDueDate = Date.from(date.toInstant());
    final Date maxDueDate = Date.from(date.plusYears(DEFAULT_YEAR_RANGE).toInstant());

    serviceRangeDto.setLowRange(date.toEpochSecond() * 1000L);
    serviceRangeDto.setLowRange(date.plusYears(DEFAULT_YEAR_RANGE).toEpochSecond() * 1000L);
    serviceRangeDto.setCocHDtos(cocs);
    serviceRangeDto.setActionableItemHDtos(actionableItems);
    serviceRangeDto.setStatutoryFindingHDtos(statutories);
    serviceRangeDto.setServices(services);
    serviceRangeDto.setRepeatedServices(repeatedServices);

    final CodicilActionableHDto codicilActionableHDto = new CodicilActionableHDto();
    codicilActionableHDto.setCocHDtos(cocs);
    codicilActionableHDto.setActionableItemHDtos(actionableItems);
    codicilActionableHDto.setStatutoryFindingHDtos(statutories);

    Mockito.when(assetService.getCodicils(Mockito.anyLong(), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(codicilActionableHDto);

    final ServiceScheduleDto serviceScheduleDto = new ServiceScheduleDto();
    serviceScheduleDto.setServices(services);
    serviceScheduleDto.setRepeatedServices(repeatedServices);

    Mockito
        .when(assetService.getServicesWithFutureDueDateSupport(Mockito.anyLong(), Mockito.any(ServiceQueryDto.class)))
        .thenReturn(serviceScheduleDto);

    final ServiceQueryDto query = new ServiceQueryDto();
    query.setMinDueDate(minDueDate);
    query.setMaxDueDate(maxDueDate);

    final CoCServiceRangeHDto resultDto = cocService.getCoCServiceRangeWithDetails(1L, query);
    Assert.assertNotNull(resultDto);

    final Instant i = Instant.ofEpochSecond(date.toEpochSecond());
    final ZonedDateTime resultLowRange = ZonedDateTime.ofInstant(i, ZoneId.systemDefault());
    Assert.assertEquals(date.getYear(), resultLowRange.getYear());
    Assert.assertEquals(date.getMonth(), resultLowRange.getMonth());
    Assert.assertEquals(date.getDayOfMonth(), resultLowRange.getDayOfMonth());

    final Instant i2 = Instant.ofEpochSecond(date.plusYears(7).toEpochSecond());
    final ZonedDateTime resultHighRange = ZonedDateTime.ofInstant(i2, ZoneId.systemDefault());
    final ZonedDateTime sevenYearsFromNow = date.plusYears(7);
    Assert.assertEquals(sevenYearsFromNow.getYear(), resultHighRange.getYear());
    Assert.assertEquals(sevenYearsFromNow.getMonth(), resultHighRange.getMonth());
    Assert.assertEquals(sevenYearsFromNow.getDayOfMonth(), resultHighRange.getDayOfMonth());

    Assert.assertEquals(serviceRangeDto.getCocHDtos(), resultDto.getCocHDtos());
    Assert.assertEquals(serviceRangeDto.getActionableItemHDtos(), resultDto.getActionableItemHDtos());
    Assert.assertEquals(serviceRangeDto.getStatutoryFindingHDtos(), resultDto.getStatutoryFindingHDtos());
    Assert.assertEquals(serviceRangeDto.getServices(), resultDto.getServices());
    Assert.assertEquals(serviceRangeDto.getRepeatedServices(), resultDto.getRepeatedServices());
  }

  /**
   * Tests scenario to get coc list for a given asset for export, it should return only the open
   * CoCs and open defect associate to the coc if any.
   *
   * @throws IOException if external API call fail.
   * @throws ClassDirectException if error fetching the open cocs and associated defects.
   */
  @Test
  public final void testGetCoCForExport() throws IOException, ClassDirectException {
    final List<DefectHDto> defectList = new ArrayList<>(2);
    final DefectHDto coc1Defect1 = new DefectHDto();
    coc1Defect1.setId(1L);
    coc1Defect1.setTitle("CoC#1 Defect#1");
    defectList.add(coc1Defect1);

    final DefectHDto coc2Defect1 = new DefectHDto();
    coc2Defect1.setId(2L);
    coc2Defect1.setTitle("CoC#2 Defect#1");
    defectList.add(coc2Defect1);

    final DefectHDto coc3Defect1 = new DefectHDto();
    coc3Defect1.setId(3L);
    coc3Defect1.setTitle("CoC#2 Defect#1");
    SurveyDto survey3 = new SurveyDto();
    survey3.setServiceCatalogue(new LinkResource(3L));
    survey3.getServiceCatalogue().setId(3L);
    coc1Defect1.setSurvey(survey3);
    defectList.add(coc3Defect1);

    final ZonedDateTime date = ZonedDateTime.now();
    final List<CoCHDto> cocList = new ArrayList<>();
    final CoCHDto coc1 = new CoCHDto();
    coc1.setId(1L);
    SurveyDto survey2 = new SurveyDto();
    survey2.setServiceCatalogue(new LinkResource(2L));
    survey2.getServiceCatalogue().setId(2L);
    coc1.setSurvey(survey2);
    coc1.setDefect(new LinkResource(coc1Defect1.getId()));
    cocList.add(coc1);
    final CoCHDto coc2 = new CoCHDto();
    coc2.setId(2L);
    SurveyDto survey = new SurveyDto();
    survey.setServiceCatalogue(new LinkResource(1L));
    survey.getServiceCatalogue().setId(1L);
    coc2.setSurvey(survey);
    coc2.setDefect(new LinkResource(coc2Defect1.getId()));
    coc2.setDueDate(Date.from(date.toInstant()));
    cocList.add(coc2);
    final CoCHDto coc3 = new CoCHDto();
    coc3.setId(3L);
    coc3.setDefect(new LinkResource(coc2Defect1.getId()));
    coc3.setDueDate(Date.from(date.toInstant()));
    cocList.add(coc3);
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.eq(1L)))
        .thenReturn(mockServicecatalogueData().get(0));
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.eq(2L)))
        .thenReturn(mockServicecatalogueData().get(1));
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.eq(3L)))
        .thenReturn(mockServicecatalogueData().get(2));
    Mockito.when(productReferenceService.getProductGroup(Mockito.eq(1L))).thenReturn(mockProductGroupData().get(0));
    Mockito.when(productReferenceService.getProductGroup(Mockito.eq(2L))).thenReturn(mockProductGroupData().get(1));
    Mockito.when(productReferenceService.getProductGroup(Mockito.eq(3L))).thenReturn(mockProductGroupData().get(2));
    Mockito.when(assetServiceDelegate.getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.eq(1L)))
        .thenReturn(Calls.response(cocList));
    Mockito.when(assetServiceDelegate.getDefectsByQuery(Mockito.eq(1L), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(Calls.response(defectList));

    final List<CoCHDto> list = cocService.getCoCForExport(1L);
    Assert.assertNotNull(list);
    final CoCHDto actualCocl =
        list.parallelStream().filter(coc -> coc.getId() == coc1.getId()).findFirst().orElse(null);
    Assert.assertNotNull(actualCocl);
    Assert.assertEquals(coc1Defect1.getId(), actualCocl.getDefect().getId());

    final CoCHDto actualCoc2 =
        list.parallelStream().filter(coc -> coc.getId() == coc2.getId()).findFirst().orElse(null);
    Assert.assertNotNull(actualCocl);
    Assert.assertEquals(coc2Defect1.getId(), actualCoc2.getDefect().getId());
  }

  /**
   * Tests scenario to get coc list for a given asset for export when the coc list is empty or
   * defect list is empty.
   *
   * @throws IOException if external API call fail.
   * @throws ClassDirectException if error fetching the open cocs and associated defects.
   */
  @Test
  public final void testGetCoCForExportWithEmpty() throws IOException, ClassDirectException {
    final List<CoCHDto> cocList = new ArrayList<>();
    final CoCHDto coc1 = new CoCHDto();
    coc1.setId(1L);
    coc1.setStatus(new LinkResource(14L));
    coc1.setDefect(new LinkResource(1L));
    cocList.add(coc1);
    final CoCHDto coc2 = new CoCHDto();
    coc2.setId(2L);
    coc2.setStatus(new LinkResource(14L));
    coc2.setDefect(new LinkResource(2L));
    cocList.add(coc2);

    Mockito.when(assetServiceDelegate.getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.eq(1L)))
        .thenReturn(Calls.response(cocList));
    Mockito.when(assetServiceDelegate.getDefectsByQuery(Mockito.eq(1L), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(Calls.response(Collections.<DefectHDto>emptyList()));

    final List<CoCHDto> list = cocService.getCoCForExport(1L);
    Assert.assertNotNull(list);
    final CoCHDto actualCocl =
        list.parallelStream().filter(coc -> coc.getId() == coc1.getId()).findFirst().orElse(null);
    Assert.assertNotNull(actualCocl);
    Assert.assertNull(actualCocl.getDefectH());

    final CoCHDto actualCoc2 =
        list.parallelStream().filter(coc -> coc.getId() == coc2.getId()).findFirst().orElse(null);
    Assert.assertNotNull(actualCocl);
    Assert.assertNull(actualCoc2.getDefectH());

    reset(assetServiceDelegate);
    Mockito.when(assetServiceDelegate.getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.eq(1L)))
        .thenReturn(Calls.response(Collections.<CoCHDto>emptyList()));
    Mockito.when(assetServiceDelegate.getDefectsByQuery(Mockito.eq(1L), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(Calls.response(Collections.<DefectHDto>emptyList()));
    final List<CoCHDto> emptyList = cocService.getCoCForExport(1L);
    Assert.assertNotNull(emptyList);
    Assert.assertEquals(0, emptyList.size());
  }

  /**
   * Tests failure scenario scenario get service range {@link CoCService#getCoCServiceRange(long)}
   * when mast api calls fail.
   *
   * @throws IOException if mast API call fail.
   */
  @Test(expected = IOException.class)
  public final void testGetCoCServiceFail() throws IOException {

    Mockito.when(assetServiceDelegate.getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.anyLong()))
        .thenReturn(Calls.failure(new IOException("Mock")));
    Mockito.when(serviceRefDelegate.getAssetServicesQuery(Mockito.anyLong(), Mockito.any()))
        .thenReturn(Calls.failure(new IOException("Mock")));
    Mockito.when(
        assetServiceDelegate.getActionableItemQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Mockito.anyLong()))
        .thenReturn(Calls.failure(new IOException("Mock")));
    Mockito.when(
        assetServiceDelegate.getStatutoryFindingsForAsset(Mockito.anyLong(), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(Calls.failure(new IOException("Mock")));
    cocService.getCoCServiceRange(1L);
  }

  /**
   * Test failure scenario get service range with details
   * {@link CoCService#getCoCServiceRangeWithDetails(long)} with api success.
   *
   * @throws IOException if mast API call fail.
   * @throws CloneNotSupportedException if the service object is unable to be cloned.
   * @throws ClassDirectException if there is error mast API to get statutory findings.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetCoCServiceServiceRangeFail()
      throws IOException, CloneNotSupportedException, ClassDirectException {
    final ZonedDateTime date = ZonedDateTime.now();
    final CoCServiceRangeHDto serviceRangeDto = new CoCServiceRangeHDto();

    final List<CoCHDto> cocs = new ArrayList<>();
    final Instant dueDateInstant = date.minusWeeks(DEFAULT_OFFSET_WEEK).toInstant();
    final Date lowerDueDate = Date.from(dueDateInstant);
    final Date higherDueDate = Date.from(dueDateInstant);

    final CoCHDto coc = new CoCHDto();
    coc.setDueDate(lowerDueDate);
    coc.setStatus(new LinkResource(14L));
    cocs.add(coc);

    final CoCHDto coc1 = new CoCHDto();
    coc1.setDueDate(higherDueDate);
    coc1.setStatus(new LinkResource(1L));
    cocs.add(coc1);
    serviceRangeDto.setCocHDtos(cocs);

    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    final ActionableItemHDto aI = new ActionableItemHDto();
    aI.setDueDate(lowerDueDate);
    aI.setStatus(new LinkResource(3L));
    actionableItems.add(aI);

    final ActionableItemHDto aI1 = new ActionableItemHDto();
    aI1.setDueDate(higherDueDate);
    aI1.setStatus(new LinkResource(2L));
    actionableItems.add(aI1);
    serviceRangeDto.setActionableItemHDtos(actionableItems);

    final List<StatutoryFindingHDto> statutories = new ArrayList<>();
    final StatutoryFindingHDto sf = new StatutoryFindingHDto();
    sf.setDueDate(higherDueDate);
    sf.setDueStatusH(DueStatus.OVER_DUE);
    statutories.add(sf);

    final StatutoryFindingHDto sf1 = new StatutoryFindingHDto();
    sf1.setDueDate(lowerDueDate);
    sf1.setDueStatusH(DueStatus.IMMINENT);
    statutories.add(sf1);
    serviceRangeDto.setStatutoryFindingHDtos(statutories);

    final List<ScheduledServiceHDto> services = new ArrayList<>();
    final ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setDueDate(higherDueDate);
    service.setServiceStatus(new LinkResource(2L));
    services.add(service);

    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setDueDate(lowerDueDate);
    service.setServiceStatus(new LinkResource(5L));
    services.add(service);
    serviceRangeDto.setServices(services);

    final List<RepeatOfDto> repeatedServices = new ArrayList<>();
    serviceRangeDto.setRepeatedServices(repeatedServices);

    Date minDueDate = lowerDueDate;
    final ZonedDateTime highRangeDate = date.plusYears(DEFAULT_YEAR_RANGE);
    Date maxDueDate = Date.from(highRangeDate.toInstant());
    serviceRangeDto.setLowRange(date.toEpochSecond() * 1000L);
    serviceRangeDto.setLowRange(highRangeDate.toEpochSecond() * 1000L);

    final CodicilActionableHDto codicilActionableHDto = new CodicilActionableHDto();
    codicilActionableHDto.setCocHDtos(cocs);
    codicilActionableHDto.setActionableItemHDtos(actionableItems);
    codicilActionableHDto.setStatutoryFindingHDtos(statutories);

    Mockito.when(assetService.getCodicils(Mockito.anyLong(), Mockito.any(CodicilDefectQueryHDto.class)))
        .thenReturn(codicilActionableHDto);

    final ServiceScheduleDto serviceScheduleDto = new ServiceScheduleDto();
    serviceScheduleDto.setServices(services);
    serviceScheduleDto.setRepeatedServices(repeatedServices);

    Mockito
        .when(assetService.getServicesWithFutureDueDateSupport(Mockito.anyLong(), Mockito.any(ServiceQueryDto.class)))
        .thenThrow(new CloneNotSupportedException());

    final ServiceQueryDto query = new ServiceQueryDto();
    query.setMinDueDate(minDueDate);
    query.setMaxDueDate(maxDueDate);
    query.setIncludeFutureDueDates(true);
    final CoCServiceRangeHDto resultDto = cocService.getCoCServiceRangeWithDetails(1L, query);
  }

  /**
   * Test positive case scenario converting data to epoch time.
   */
  @Test
  public final void testEpoch() {

    final Date date = new Date();

    final Long epoch1 = date.toInstant().atZone(ZoneId.systemDefault()).toEpochSecond() * 1000L;

    final List<ScheduledServiceHDto> services = new ArrayList<>();

    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setDueDate(date);
    service1.setLowerRangeDate(date);
    service1.setUpperRangeDate(date);
    service1.setAssignedDate(date);
    final Long dueDateEpoch = service1.getDueDateEpoch();
    final Long lowRangeDateEpoch = service1.getLowerRangeDateEpoch();
    final Long upperRangeDateEpoch = service1.getUpperRangeDateEpoch();
    final Long assignedDateEpoch = service1.getAssignedDateEpoch();
    services.add(service1);

    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    service2.setAssignedDate(null);
    service2.setDueDate(null);
    service2.setLowerRangeDate(null);
    service2.setUpperRangeDate(null);
    services.add(service2);
    final Long assignedDueDateEpochNull = service2.getAssignedDateEpoch();
    final Long dueDateEpochNull = service2.getDueDateEpoch();
    final Long lowrangeDateEpochNull = service2.getLowerRangeDateEpoch();
    final Long upperrangeDateEpochNull = service2.getUpperRangeDateEpoch();

    final List<CoCHDto> cocs = new ArrayList<>();
    final CoCHDto coc = new CoCHDto();
    coc.setDueDate(date);
    coc.setImposedDate(date);
    final Long dueDate = coc.getDueDateEpoch();
    final Long imposedDateEpoch = coc.getImposedDateEpoch();
    cocs.add(coc);

    final CoCHDto coc1 = new CoCHDto();
    coc1.setDueDate(null);
    coc1.setImposedDate(null);
    final Long dueDateNull = coc1.getDueDateEpoch();
    final Long imposedDateEpochNull = coc1.getImposedDateEpoch();
    cocs.add(coc1);


    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    final ActionableItemHDto actionableItem = new ActionableItemHDto();
    actionableItem.setDueDate(date);
    actionableItem.setImposedDate(date);
    actionableItems.add(actionableItem);
    final Long actionableDueDateEpoch = actionableItem.getDueDateEpoch();
    final Long actionableImposedDateEpoch = actionableItem.getImposedDateEpoch();

    final ActionableItemHDto actionableItem1 = new ActionableItemHDto();
    actionableItem1.setDueDate(null);
    actionableItem1.setImposedDate(null);
    actionableItems.add(actionableItem1);
    final Long actionableDueDateEpochNull = actionableItem1.getDueDateEpoch();
    final Long actionableImposedDateEpochNull = actionableItem1.getImposedDateEpoch();

    Mockito.when(serviceRefDelegate.getAssetServices(1L)).thenReturn(Calls.response(services));
    Mockito.when(assetServiceDelegate.getCoCDto(1L)).thenReturn(Calls.response(cocs));
    Mockito.when(assetServiceDelegate.getActionableItemDto(1L)).thenReturn(Calls.response(actionableItems));

    Assert.assertEquals(epoch1, dueDateEpoch);
    Assert.assertEquals(epoch1, lowRangeDateEpoch);
    Assert.assertEquals(epoch1, upperRangeDateEpoch);
    Assert.assertEquals(epoch1, assignedDateEpoch);
    Assert.assertEquals(epoch1, dueDate);
    Assert.assertEquals(epoch1, imposedDateEpoch);
    Assert.assertEquals(epoch1, actionableDueDateEpoch);
    Assert.assertEquals(epoch1, actionableImposedDateEpoch);
    Assert.assertEquals(null, assignedDueDateEpochNull);
    Assert.assertEquals(null, dueDateEpochNull);
    Assert.assertEquals(null, lowrangeDateEpochNull);
    Assert.assertEquals(null, upperrangeDateEpochNull);
    Assert.assertEquals(null, dueDateNull);
    Assert.assertEquals(null, imposedDateEpochNull);
    Assert.assertEquals(null, actionableDueDateEpochNull);
    Assert.assertEquals(null, actionableImposedDateEpochNull);

  }


  /**
   * Test positive case scenario calculate due status.
   */
  @Test
  public final void calculateDueStatusTest() {

    final List<CoCHDto> cocs = new ArrayList<>();

    final LocalDate date = LocalDate.now();
    final LocalDate date1 = LocalDate.now().minusDays(2L);
    final LocalDate date2 = LocalDate.now().plusDays(2L);
    final LocalDate date3 = LocalDate.now().plusMonths(1L);
    final LocalDate date4 = LocalDate.now().plusMonths(3L);
    final LocalDate date5 = LocalDate.now().plusMonths(1L).minusDays(1L);
    final LocalDate date6 = LocalDate.now().plusMonths(3L).minusDays(1L);

    final CoCHDto coc = new CoCHDto();
    coc.setDueDate(DateUtils.getLocalDateToDate(date));

    final CoCHDto coc1 = new CoCHDto();
    coc1.setDueDate(DateUtils.getLocalDateToDate(date1));

    final CoCHDto coc2 = new CoCHDto();
    coc2.setDueDate(DateUtils.getLocalDateToDate(date2));

    final CoCHDto coc3 = new CoCHDto();
    coc3.setDueDate(DateUtils.getLocalDateToDate(date3));

    final CoCHDto coc4 = new CoCHDto();
    coc4.setDueDate(DateUtils.getLocalDateToDate(date5));

    final CoCHDto coc5 = new CoCHDto();
    coc5.setDueDate(DateUtils.getLocalDateToDate(date6));

    final CoCHDto coc6 = new CoCHDto();
    coc6.setDueDate(DateUtils.getLocalDateToDate(date4));

    final CoCHDto coc7 = new CoCHDto();
    coc7.setDueDate(null);

    Mockito.when(assetServiceDelegate.getCoCDto(1L)).thenReturn(Calls.response(cocs));

    final DueStatus status = cocService.calculateDueStatus(coc);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    final DueStatus status1 = cocService.calculateDueStatus(coc1);
    Assert.assertEquals(DueStatus.OVER_DUE, status1);

    final DueStatus status2 = cocService.calculateDueStatus(coc2);
    Assert.assertEquals(DueStatus.IMMINENT, status2);

    final DueStatus status3 = cocService.calculateDueStatus(coc3);
    Assert.assertEquals(DueStatus.DUE_SOON, status3);

    final DueStatus status4 = cocService.calculateDueStatus(coc4);
    Assert.assertEquals(DueStatus.IMMINENT, status4);

    final DueStatus status5 = cocService.calculateDueStatus(coc5);
    Assert.assertEquals(DueStatus.DUE_SOON, status5);

    final DueStatus status6 = cocService.calculateDueStatus(coc6);
    Assert.assertEquals(DueStatus.NOT_DUE, status6);

    final DueStatus status7 = cocService.calculateDueStatus(coc7);
    Assert.assertEquals(DueStatus.NOT_DUE, status7);
  }

  /**
   * Test positive case scenario getting condition of class.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetCoC() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockCoCData()).when(retrofitService).getCoCDto(Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());

    final PageResource<CoCHDto> result = cocService.getCoC(null, null, 1L);

    Assert.assertNotNull(result.getContent());
    Assert.assertEquals(2L, result.getContent().size());
  }

  /**
   * Test get condition of class API call fail.
   *
   * @throws Exception if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetCoCException() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockCoCData()).when(retrofitService).getCoCDto(Matchers.anyLong());

    when(retrofitService.getCoCDto(1)).thenReturn(Calls.failure(new IOException("Mocked Exception.")));
    cocService.getCoC(null, null, 1L);
  }

  /**
   * Test positive case scenario get condition of class by asset Id and condition of class Id.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetCoCByAssetIdCoCId() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockActionTaken()).when(refService).getActionTaken(Matchers.anyLong());
    doReturn(mockSingleCoCData()).when(retrofitService).getCoCByAssetIdCoCId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockDefectData()).when(retrofitService).getAssetDefectDto(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockRepairData()).when(retrofitService).getCoCRepairDto(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobsData()));

    final CoCHDto result = cocService.getCoCByAssetIdCoCId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getJobH());
    Assert.assertEquals(result.getJobH().getJobNumber(), "123");
  }

  /**
   * Returns action taken mock data.
   *
   * @return the action taken mock data.
   */
  private ActionTakenHDto mockActionTaken() {
    final ActionTakenHDto dto = new ActionTakenHDto();
    dto.setId(1L);
    dto.setName("Closed");
    return dto;
  }

  /**
   * Test positive case get condition of class by asset Id and condition of class Id with null
   * condition.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetCoCByAssetIdCoCIdMultiCondition() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockSingleCoCDataWithNullDefectAndAssetItemId()).when(retrofitService)
        .getCoCByAssetIdCoCId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockDefectData()).when(retrofitService).getAssetDefectDto(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockRepairData()).when(retrofitService).getCoCRepairDto(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockAssetItemData()).when(retrofitService).getAssetItemDto(Matchers.anyLong(), Matchers.anyLong());
    when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.failure(new IOException()));

    final CoCHDto result = cocService.getCoCByAssetIdCoCId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNull(result.getJobH());
  }

  /**
   * Test get condition of class by asset Id and condition of class Id API call fail.
   *
   * @throws Exception if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetCoCByAssetIdCoCIdException() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    // simulate exception for logger
    doReturn(mockSingleCoCDataWithNullDefectAndAssetItemId()).when(retrofitService)
        .getCoCByAssetIdCoCId(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockDefectData()).when(retrofitService).getAssetDefectDto(Matchers.anyLong(), Matchers.anyLong());
    doReturn(mockRepairData()).when(retrofitService).getCoCRepairDto(Matchers.anyLong(), Matchers.anyLong());
    when(retrofitService.getAssetItemDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    CoCHDto result = cocService.getCoCByAssetIdCoCId(1L, 1L);

    // simulate MAST call failed.
    when(retrofitService.getCoCByAssetIdCoCId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException("Mocked Exception.")));

    result = cocService.getCoCByAssetIdCoCId(1L, 1L);
  }

  /**
   * Tests success scenario getting condition of class by query
   * {@link CoCService#getCocsByQuery(long, CodicilDefectQueryHDto)}.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetCoCByQuery() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    doReturn(mockCoCData()).when(retrofitService).getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class),
        Matchers.anyLong());

    final List<CoCHDto> result = cocService.getCocsByQuery(1L, new CodicilDefectQueryHDto());

    Assert.assertNotNull(result);
    Assert.assertEquals(2L, result.size());
    result.forEach(coc -> Assert.assertTrue(coc.getStatusH().getId() == 14));
  }

  /**
   * Tests failure scenario getting condition of class by query
   * {@link CoCService#getCocsByQuery(long, CodicilDefectQueryHDto)} when mast api call
   * fails.
   *
   * @throws Exception if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetCoCByQueryFail() throws Exception {
    // mock retrofit
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });

    when(retrofitService.getCoCQueryDto(Mockito.any(CodicilDefectQueryHDto.class), Matchers.anyLong()))
        .thenReturn((Calls.failure(new IOException("Mocked Exception."))));

    cocService.getCocsByQuery(1L, new CodicilDefectQueryHDto());
  }

  /**
   * Returns condition of class mock data.
   *
   * @return the condition of class mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<List<CoCHDto>> mockCoCData() {
    final List<CoCHDto> cocResponse = new ArrayList<CoCHDto>();

    // data1
    final CoCHDto data1 = new CoCHDto();
    data1.setId(1L);
    data1.setAssetItem(new NamedLinkResource(1L));
    data1.setDefect(new LinkResource(1L));
    CodicilStatusHDto codicilStatus = new CodicilStatusHDto();
    codicilStatus.setId(14L);
    data1.setStatusH(codicilStatus);
    cocResponse.add(data1);

    // data2
    final CoCHDto data2 = new CoCHDto();
    data2.setId(2L);
    data2.setAssetItem(new NamedLinkResource(2L));
    data2.setDefect(new LinkResource(2L));
    data2.setStatusH(codicilStatus);
    cocResponse.add(data2);

    final Response<List<CoCHDto>> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(cocResponse);

    final Call<List<CoCHDto>> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns single condition of class mock data.
   *
   * @return the single condition of class mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<CoCHDto> mockSingleCoCData() {
    final CoCHDto cocResponse = new CoCHDto();
    cocResponse.setId(1L);
    cocResponse.setAssetItem(new NamedLinkResource(1L));
    cocResponse.setDefect(new LinkResource(1L));
    cocResponse.setActionTaken(new LinkResource(1L));
    cocResponse.setJob(new LinkResource(1L));

    final Response<CoCHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(cocResponse);

    final Call<CoCHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns condition of class mock data with null defect and asset item id.
   *
   * @return the condition of class mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<CoCHDto> mockSingleCoCDataWithNullDefectAndAssetItemId() {
    final CoCHDto cocResponse = new CoCHDto();
    cocResponse.setId(1L);

    final Response<CoCHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(cocResponse);

    final Call<CoCHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns defect mock data.
   *
   * @return the defect mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<DefectHDto> mockDefectData() {
    final DefectHDto result = new DefectHDto();

    final Response<DefectHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(result);

    final Call<DefectHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns repair mock data.
   *
   * @return the repair mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<List<RepairHDto>> mockRepairData() {
    final List<RepairHDto> repairs = new ArrayList<>();

    // data with items & repair types
    final RepairHDto data1 = new RepairHDto();
    data1.setId(1L);

    final List<RepairItemDto> repairItemData = new ArrayList<>();
    final RepairItemDto item1 = new RepairItemDto();
    final DefectItemLinkDto itemLink1 = new DefectItemLinkDto();
    itemLink1.setItem(new LinkResource(1L));
    itemLink1.getItem().setId(1L);
    item1.setItem(itemLink1);
    repairItemData.add(item1);

    data1.setRepairs(repairItemData);

    data1.setRepairTypes(new ArrayList<LinkResource>());
    data1.getRepairTypes().add(new LinkResource(1L));


    repairs.add(data1);

    // data without items
    final RepairHDto data2 = new RepairHDto();
    data2.setId(2L);
    repairs.add(data2);


    final Response<List<RepairHDto>> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(repairs);

    final Call<List<RepairHDto>> caller = Calls.response(response);

    return caller;
  }

  /**
   * Returns asset item mock data.
   *
   * @return the asset item mock data.
   */
  @SuppressWarnings("unchecked")
  private Call<LazyItemHDto> mockAssetItemData() {
    final LazyItemHDto result = new LazyItemHDto();
    result.setId(1L);

    final Response<LazyItemHDto> response = PowerMockito.mock(Response.class);
    when(response.body()).thenReturn(result);

    final Call<LazyItemHDto> caller = Calls.response(response);

    return caller;
  }

  /**
   * Mocks Jobs Data.
   *
   * @return call value.
   */
  private JobHDto mockJobsData() {
    JobHDto job = new JobHDto();
    job.setId(1L);
    job.setJobNumber("123");
    return job;

  }

  /**
   * Mocks service catalogue data.
   *
   * @return value.
   */
  private List<ServiceCatalogueHDto> mockServicecatalogueData() {
    List<ServiceCatalogueHDto> serviceCatalogueList = new ArrayList<>();
    ServiceCatalogueHDto catalogue1 = new ServiceCatalogueHDto();
    catalogue1.setId(1L);
    ProductCatalogueHDto productCatalogue = new ProductCatalogueHDto();
    productCatalogue.setId(1L);
    productCatalogue.setProductGroup(new LinkResource(1L));
    catalogue1.setProductCatalogue(productCatalogue);
    serviceCatalogueList.add(catalogue1);

    ServiceCatalogueHDto catalogue2 = new ServiceCatalogueHDto();
    catalogue2.setId(2L);
    ProductCatalogueHDto productCatalogue1 = new ProductCatalogueHDto();
    productCatalogue1.setId(2L);
    productCatalogue1.setProductGroup(new LinkResource(2L));
    catalogue2.setProductCatalogue(productCatalogue1);
    serviceCatalogueList.add(catalogue2);

    ServiceCatalogueHDto catalogue3 = new ServiceCatalogueHDto();
    catalogue3.setId(3L);
    ProductCatalogueHDto productCatalogue2 = new ProductCatalogueHDto();
    productCatalogue2.setId(1L);
    productCatalogue2.setProductGroup(new LinkResource(3L));
    catalogue3.setProductCatalogue(productCatalogue2);
    serviceCatalogueList.add(catalogue3);
    return serviceCatalogueList;

  }

  /**
   * Mocks product group data.
   *
   * @return value.
   */
  private List<ProductGroupDto> mockProductGroupData() {
    List<ProductGroupDto> productGroupList = new ArrayList<>();

    ProductGroupDto productGroup = new ProductGroupDto();
    productGroup.setId(1L);
    productGroup.setDisplayOrder(1);
    productGroup.setName("Product Group 1");
    productGroupList.add(productGroup);

    ProductGroupDto productGroup2 = new ProductGroupDto();
    productGroup2.setId(2L);
    productGroup2.setDisplayOrder(2);
    productGroup2.setName("Product Group 2");
    productGroupList.add(productGroup2);


    ProductGroupDto productGroup3 = new ProductGroupDto();
    productGroup3.setId(3L);
    productGroup3.setDisplayOrder(3);
    productGroup3.setName("Product Group 3");
    productGroupList.add(productGroup3);

    return productGroupList;

  }


  /**
   * Provides Spring in-class configurations.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public CoCService cocService() {
      return new CoCServiceImpl();
    }

    @Override
    @Bean
    public AssetRetrofitService assetRetrofitService() {
      return PowerMockito.mock(AssetRetrofitService.class);
    }

  }
}
