package com.baesystems.ai.lr.cd.service.job.reference.impl.test;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.JobReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemTypeHDto;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.job.reference.impl.JobReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

import retrofit2.mock.Calls;

/**
 * Provides unit test for JobReferenceService {@link JobReferenceService}.
 *
 * @author yng
 * @author syalavarthi 23-05-2017.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JobReferenceServiceTest.Config.class)
public class JobReferenceServiceTest {
  /**
   * The spring application context {@link ApplicationContext}.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link JobReferenceService} from spring context.
   *
   */
  @Autowired
  private JobReferenceService jobReferenceService;

  /**
   * Initialize mocked object.
   */
  @Before
  public final void init() {
    reset(context.getBean(AssetReferenceRetrofitService.class));
  }

  /**
   * Tests success scenario to get job statuses {@link JobReferenceService#getJobStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void successfullyRetrieveListOfJobStatuses() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);

    when(retrofitService.getJobStatuses()).thenReturn(Calls.response(mockJobStatuses()));

    final List<JobStatusHDto> jobStatuses = jobReferenceService.getJobStatuses();
    Assert.assertNotNull(jobStatuses);
    Assert.assertNotEquals(0, jobStatuses.size());
  }

  /**
   * Tests failure scenario to get job statuses {@link JobReferenceService#getJobStatuses()} when
   * inner call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void failedToRetrieveListOfJobStatuses() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getJobStatuses()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<JobStatusHDto> jobStatuses = jobReferenceService.getJobStatuses();
    Assert.assertNotNull(jobStatuses);
    Assert.assertEquals(0, jobStatuses.size());
  }

  /**
   * Tests success scenario to get single job status by id
   * {@link JobReferenceService#getJobStatus(Long)}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public final void successfullyGetJobStatus() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getJobStatuses()).thenReturn(Calls.response(mockJobStatuses()));

    final JobStatusHDto jobStatus = jobReferenceService.getJobStatus(1L);
    Assert.assertNotNull(jobStatus);
  }

  /**
   * Tests failure scenario to get single job status by id
   * {@link JobReferenceService#getJobStatus(Long)} when inner call fail.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void emptyResultForGetJobStatus() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getJobStatuses()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final JobStatusHDto jobStatus = jobReferenceService.getJobStatus(1L);
    Assert.assertNull(jobStatus);
  }

  /**
   * Tests success scenario to get report types {@link JobReferenceService#getReportTypes()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void successfullyRetrieveListOfReportTypes() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);

    when(retrofitService.getReportTypes()).thenReturn(Calls.response(mockReportTypes()));

    final List<ReportTypeHDto> reportTypes = jobReferenceService.getReportTypes();
    Assert.assertNotNull(reportTypes);
    Assert.assertNotEquals(0, reportTypes.size());
  }

  /**
   * Tests failure scenario to get report types {@link JobReferenceService#getReportTypes()} when
   * inner call fail.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void failedToRetrieveListOfReportTypes() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getReportTypes()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<ReportTypeHDto> reportTypes = jobReferenceService.getReportTypes();
    Assert.assertNotNull(reportTypes);
    Assert.assertEquals(0, reportTypes.size());
  }

  /**
   * Tests success scenario for get single report type by id
   * {@link JobReferenceService#getReportType(Long)}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public final void successfullyGetReportType() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getReportTypes()).thenReturn(Calls.response(mockReportTypes()));

    final ReportTypeHDto reportType = jobReferenceService.getReportType(1L);
    Assert.assertNotNull(reportType);
  }

  /**
   * Tests failure scenario for to get single report type by id
   * {@link JobReferenceService#getReportType(Long)} empty result when inner call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void emptyResultForGetReportType() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getReportTypes()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final ReportTypeHDto reportType = jobReferenceService.getReportType(1L);
    Assert.assertNull(reportType);
  }

  /**
   * Tests success scenario to get list of work items
   * {@link JobReferenceService#getWorkItemTypes()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void successfullyRetrieveListOfWorkItemTypes() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);

    when(retrofitService.getWorkItemTypes()).thenReturn(Calls.response(mockWorkItemTypes()));

    final List<WorkItemTypeHDto> workItemTypes = jobReferenceService.getWorkItemTypes();
    Assert.assertNotNull(workItemTypes);
    Assert.assertEquals("Task", workItemTypes.get(0).getName());
    Assert.assertNotEquals(0, workItemTypes.size());
  }

  /**
   * Tests failure scenario to get list of work items {@link JobReferenceService#getWorkItemTypes()}
   * when inner call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void failedToRetrieveListOfWorkItemTypes() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getWorkItemTypes()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<WorkItemTypeHDto> workItemTypes = jobReferenceService.getWorkItemTypes();
    Assert.assertNotNull(workItemTypes);
    Assert.assertEquals(0, workItemTypes.size());
  }

  /**
   * Tests success scenario to get work item by id
   * {@link JobReferenceService#getWorkItemType(Long)}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public final void successfullyGetWorkItemType() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getWorkItemTypes()).thenReturn(Calls.response(mockWorkItemTypes()));

    final WorkItemTypeHDto workItemType = jobReferenceService.getWorkItemType(1L);
    Assert.assertNotNull(workItemType);
    Assert.assertEquals("Task", workItemType.getName());
  }

  /**
   * Tests failure scenario to get work item type by id
   * {@link JobReferenceService#getWorkItemType(Long)} when inner call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void emptyResultForGetWorkItemType() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getWorkItemTypes()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final WorkItemTypeHDto workItemType = jobReferenceService.getWorkItemType(1L);
    Assert.assertNull(workItemType);
  }

  /**
   * Tests success scenario to get list of resolution statuses
   * {@link JobReferenceService#getResolutionStatuses()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void successfullyRetrieveListOfResolutionStatuses() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);

    when(retrofitService.getResolutionStatuses()).thenReturn(Calls.response(mockResolutinStatuses()));

    final List<JobResolutionStatusHDto> resolutionStatus = jobReferenceService.getResolutionStatuses();
    Assert.assertNotNull(resolutionStatus);
    Assert.assertEquals("Complete", resolutionStatus.get(0).getName());
    Assert.assertNotEquals(0, resolutionStatus.size());
  }

  /**
   * Tests failure scenario to get list of resolution statuses
   * {@link JobReferenceService#getResolutionStatuses()} when inner call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void failedToRetrieveListOfResolutionStatuses() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getResolutionStatuses()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<JobResolutionStatusHDto> resolutionStatus = jobReferenceService.getResolutionStatuses();
    Assert.assertNotNull(resolutionStatus);
    Assert.assertEquals(0, resolutionStatus.size());
  }

  /**
   * Tests success scenario to get resolution statuses by id
   * {@link JobReferenceService#getResolutionStatuses(Long)}.
   *
   * @throws Exception if mast api call fail.
   *
   */
  @Test
  public final void successfullyGetResolutionStatuses() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getResolutionStatuses()).thenReturn(Calls.response(mockResolutinStatuses()));

    final JobResolutionStatusHDto resolutionStaus = jobReferenceService.getResolutionStatus(1L);
    Assert.assertNotNull(resolutionStaus);
    Assert.assertEquals("Complete", resolutionStaus.getName());
  }

  /**
   * Tests negative scenario to get resolution status by id
   * {@link JobReferenceService#getResolutionStatuses(Long)} when inner call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public final void emptyResultForGetResolutionStatuses() throws Exception {
    final JobReferenceRetrofitService retrofitService = context.getBean(JobReferenceRetrofitService.class);
    when(retrofitService.getResolutionStatuses()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final JobResolutionStatusHDto resolutionStatus = jobReferenceService.getResolutionStatus(1L);
    Assert.assertNull(resolutionStatus);
  }

  /**
   * Mock job status list.
   *
   * @return caller.
   * @throws Exception if mast api call fail.
   */
  private List<JobStatusHDto> mockJobStatuses() throws Exception {
    final List<JobStatusHDto> jobStatuses = new ArrayList<>();
    final JobStatusHDto jobStatus = new JobStatusHDto();
    jobStatus.setId(1L);
    jobStatus.setName("completed");
    jobStatuses.add(jobStatus);

    return jobStatuses;
  }

  /**
   * Mock report type list.
   *
   * @return caller.
   * @throws Exception if mast api call fail.
   */
  private List<ReportTypeHDto> mockReportTypes() throws Exception {
    final List<ReportTypeHDto> reportTypes = new ArrayList<>();
    final ReportTypeHDto reportType = new ReportTypeHDto();
    reportType.setId(1L);
    reportType.setName("FSR");
    reportTypes.add(reportType);

    return reportTypes;
  }

  /**
   * Mock work item type list.
   *
   * @return caller.
   * @throws Exception if mast api call fail.
   */
  private List<WorkItemTypeHDto> mockWorkItemTypes() throws Exception {
    final List<WorkItemTypeHDto> workItemTypes = new ArrayList<>();
    final WorkItemTypeHDto workItemType = new WorkItemTypeHDto();
    workItemType.setId(1L);
    workItemType.setName("Task");
    workItemTypes.add(workItemType);
    final WorkItemTypeHDto workItemType1 = new WorkItemTypeHDto();
    workItemType1.setId(2L);
    workItemType1.setName("Checklist");
    workItemTypes.add(workItemType1);

    return workItemTypes;
  }

  /**
   * Mock job resolution statuses list.
   *
   * @return caller.
   * @throws Exception if mast api call fail.
   */
  private List<JobResolutionStatusHDto> mockResolutinStatuses() throws Exception {
    final List<JobResolutionStatusHDto> resolutionStatuses = new ArrayList<>();
    final JobResolutionStatusHDto resolutionStatus = new JobResolutionStatusHDto();
    resolutionStatus.setId(1L);
    resolutionStatus.setName("Complete");
    resolutionStatuses.add(resolutionStatus);
    final JobResolutionStatusHDto resolutionStatus1 = new JobResolutionStatusHDto();
    resolutionStatus1.setId(2L);
    resolutionStatus1.setName("Confirmatory Check");
    resolutionStatuses.add(resolutionStatus1);

    return resolutionStatuses;
  }

  /**
   * Spring in-class configurations.
   *
   * @author yng
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create {@link JobReferenceService} bean.
     *
     * @return job reference service.
     *
     */
    @Bean
    @Override
    public JobReferenceService jobReferenceService() {
      return new JobReferenceServiceImpl();
    }
  }
}
