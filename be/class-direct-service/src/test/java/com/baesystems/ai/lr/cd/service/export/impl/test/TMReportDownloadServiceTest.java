package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AttachmentExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.export.TMReportDownloadService;
import com.baesystems.ai.lr.cd.service.export.impl.TMReportDownloadServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for {@link TMReportDownloadService}.
 *
 * @author syalavarthi.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TMReportDownloadServiceTest.Config.class)
public class TMReportDownloadServiceTest {
  /**
   * The {@link AttachmentRetrofitService} from spring context.
   */
  @Autowired
  private AttachmentRetrofitService attachmentRetrofitService;

  /**
   * The {@link TMReportDownloadService} from spring context.
   */
  @Autowired
  private TMReportDownloadService tMReportDownloadService;


  /**
   * The {@link SecurityContext} from spring context.
   */
  private SecurityContext securityCtx;

  /**
   * This method will provide prerequisite mocking of user authentication.
   */
  @Before
  public void setUp() {
    securityCtx = SecurityContextHolder.getContext();
    SecurityContext context = new SecurityContextImpl();
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    CDAuthToken token = new CDAuthToken(listOfRoles);

    token.setDetails(user);
    token.setAuthenticated(true);
    context.setAuthentication(token);
    SecurityContextHolder.setContext(context);
  }


  /**
   * Tear Down.
   */
  @After
  public void tearDown() {
    SecurityContextHolder.setContext(securityCtx);
  }

  /**
   * Tests success scenario to download tm report
   * {@link TMReportDownloadService#downloadAttachmentInfo(AttachmentExportDto)}.
   * <p>
   * with valid attachment url and document version.
   * </p>
   *
   * @throws ClassDirectException if mast api call fails.
   */
  @Test
  public final void testDownloadAttachmentInfo() throws ClassDirectException {

    final AttachmentExportDto dto = new AttachmentExportDto();
    dto.setJobId(1L);
    dto.setAttachmentId(3L);

    SupplementaryInformationHDto attachDto = new SupplementaryInformationHDto();
    attachDto.setId(3L);
    attachDto.setAttachmentUrl("167");
    attachDto.setDocumentVersion(1L);

    Mockito.when(attachmentRetrofitService.getAttachmentByJobIdattAchmentId(1L, 3L))
        .thenReturn(Calls.response(attachDto));
    // call method
    final StringResponse response = tMReportDownloadService.downloadAttachmentInfo(dto);

    Assert.assertNotNull(response);
  }

  /**
   * Tests failure scenario to download tm report
   * {@link TMReportDownloadService#downloadAttachmentInfo(AttachmentExportDto)}.
   * <p>
   * with valid attachment url and document version.
   * </p>
   *
   * @throws ClassDirectException if mast api call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testDownloadAttachmentInfoFail() throws ClassDirectException {

    final AttachmentExportDto dto = new AttachmentExportDto();
    dto.setJobId(1L);
    dto.setAttachmentId(3L);

    Mockito.when(attachmentRetrofitService.getAttachmentByJobIdattAchmentId(1L, 3L))
        .thenReturn(Calls.failure(new IOException("IO Exception")));

    // call method
    tMReportDownloadService.downloadAttachmentInfo(dto);
  }

  /**
   * Tests failure scenario to download tm report
   * {@link TMReportDownloadService#downloadAttachmentInfo(AttachmentExportDto)}.
   * <p>
   * with invalid attachment url and valid document version.
   * </p>
   *
   * @throws ClassDirectException if mast api call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testDownloadCertificateInfoNumberFormatException() throws ClassDirectException {

    final AttachmentExportDto dto = new AttachmentExportDto();
    dto.setJobId(1L);
    dto.setAttachmentId(3L);

    SupplementaryInformationHDto attachDto = new SupplementaryInformationHDto();
    attachDto.setId(3L);
    attachDto.setAttachmentUrl("jhfjghjfgh");
    attachDto.setDocumentVersion(1L);

    Mockito.when(attachmentRetrofitService.getAttachmentByJobIdattAchmentId(1L, 3L))
        .thenReturn(Calls.response(attachDto));

    // call method
    tMReportDownloadService.downloadAttachmentInfo(dto);
  }

  /**
   * Tests failure scenario to download tm report
   * {@link TMReportDownloadService#downloadAttachmentInfo(AttachmentExportDto)}.
   * <p>
   * with attachment url and document version both null.
   * </p>
   *
   * @throws ClassDirectException if mast api call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testDownloadAttachmentsforNullValues() throws ClassDirectException {
    final AttachmentExportDto dto = new AttachmentExportDto();
    dto.setJobId(1L);
    dto.setAttachmentId(3L);

    SupplementaryInformationHDto attachDto = new SupplementaryInformationHDto();
    attachDto.setId(3L);
    attachDto.setAttachmentUrl(null);
    attachDto.setDocumentVersion(null);

    Mockito.when(attachmentRetrofitService.getAttachmentByJobIdattAchmentId(1L, 3L))
        .thenReturn(Calls.response(attachDto));

    // call method
    tMReportDownloadService.downloadAttachmentInfo(dto);
  }


  /**
   * Spring in-class configurations.
   *
   * @author syalavarthi.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create {@link TMReportDownloadService} bean.
     *
     * @return TM download service.
     *
     */
    @Override
    @Bean
    public TMReportDownloadService tMReportDownloadService() {
      return new TMReportDownloadServiceImpl();
    }
  }
}
