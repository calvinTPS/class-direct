package com.baesystems.ai.lr.cd.service.awsglacieraudit.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.AwsGlacierAuditDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.AwsGlacierAudit;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.awsglacieraudit.AwsGlacierAuditService;
import com.baesystems.ai.lr.cd.service.awsglacieraudit.impl.AwsGlacierAuditServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author fwijaya on 7/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AwsGlacierAuditServiceTest {
  /**
   * Archive id.
   */
  private static final String ARCHIVE_ID = "1";
  /**
   * Vault name.
   */
  private static final String VAULT_NAME = "ClassDirectTestArchive";
  /**
   * Inject service.
   */
  @InjectMocks
  private AwsGlacierAuditService awsGlacierAuditService = new AwsGlacierAuditServiceImpl();

  /**
   * Inject DAO.
   */
  @Mock
  private AwsGlacierAuditDao awsGlacierAuditDao;

  /**
   * Test successfully storing aws glacier audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testSuccessfullyStoringAwsGlacierAudit() throws ClassDirectException {
    when(awsGlacierAuditDao
      .storeArchive(anyString(), anyString(), anyString(), any(LocalDateTime.class), anyString()))
      .thenReturn(new AwsGlacierAudit());
    final AwsGlacierAudit awsGlacierAudit =
      awsGlacierAuditService.storeArchive(ARCHIVE_ID, VAULT_NAME, "", LocalDateTime.now(), "");
    assertNotNull(awsGlacierAudit);
    verify(awsGlacierAuditDao, atLeastOnce())
      .storeArchive(anyString(), anyString(), anyString(), any(LocalDateTime.class), anyString());
  }

  /**
   * Test failure storing aws glacier audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void testFailStoringAwsGlacierAudit() throws ClassDirectException {
    doThrow(ClassDirectException.class).when(awsGlacierAuditDao)
      .storeArchive(anyString(), anyString(), anyString(), any(LocalDateTime.class), anyString());
    final AwsGlacierAudit awsGlacierAudit =
      awsGlacierAuditService.storeArchive(ARCHIVE_ID, VAULT_NAME, "", LocalDateTime.now(), "");
  }

  /**
   * Test successfully find aws glacier audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testSuccessfullyFindAudit() throws ClassDirectException {
    when(awsGlacierAuditDao.findAuditByArchiveIdAndVaultName(ARCHIVE_ID, VAULT_NAME))
      .thenReturn(new AwsGlacierAudit());
    final AwsGlacierAudit awsGlacierAudit =
      awsGlacierAuditService.findAuditByArchiveIdAndVaultName(ARCHIVE_ID, VAULT_NAME);
    assertNotNull(awsGlacierAudit);
    verify(awsGlacierAuditDao, atLeastOnce()).findAuditByArchiveIdAndVaultName(ARCHIVE_ID, VAULT_NAME);
  }

  /**
   * Test failure finding aws glacier audit.
   *
   * @throws ClassDirectException exception.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testFailureFindAudit() throws ClassDirectException {
    doThrow(ClassDirectException.class).when(awsGlacierAuditDao)
      .findAuditByArchiveIdAndVaultName(ARCHIVE_ID, VAULT_NAME);
    final AwsGlacierAudit awsGlacierAudit =
      awsGlacierAuditService.findAuditByArchiveIdAndVaultName(ARCHIVE_ID, VAULT_NAME);
  }
}
