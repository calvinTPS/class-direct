package com.baesystems.ai.lr.cd.service.export.impl.test;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.service.asset.MajorNCNService;
import com.baesystems.ai.lr.cd.service.export.MNCNAttachmentService;
import com.baesystems.ai.lr.cd.service.export.impl.MNCNAttachmentServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for {@link MNCNAttachmentService}.
 *
 * @author syalavarthi.
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest({FileTokenGenerator.class})
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = MNCNAttachmentServiceTest.Config.class)
public class MNCNAttachmentServiceTest {
  /**
   * The {@value #TOKEN} is to set token to download attachment
   * {@link SupplementaryInformationHDto#getToken()}.
   */
  private static final String TOKEN = "eyJ0eXBlIjoiQ1MxMCIsImNzMTBUb2tlbiI6";

  /**
   * The {@link MNCNAttachmentService} from Spring context.
   */
  @InjectMocks
  private MNCNAttachmentService mncnAttachmentService = new MNCNAttachmentServiceImpl();

  /**
   * The {@link AttachmentRetrofitService} from Spring context.
   */
  @Mock
  private AttachmentRetrofitService attachmentRetrofitService;
  /**
   * The {@link MajorNCNService} from Spring context.
   */
  @Autowired
  private MajorNCNService majorNCNService;

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public final void init() {
    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success scenario for {@link MNCNAttachmentService#getAttachmentsMncn(Long, Long, Long)}
   * when mncn has access for user.
   *
   * @throws Exception if mast api call fails.
   */
  @Test
  public final void getAttachmentsMajorNcn() throws Exception {
    PowerMockito.mockStatic(FileTokenGenerator.class, invocation -> {
      return TOKEN;
    });

    when(attachmentRetrofitService.getAttachmentsByAssetIdAndMncnId(eq(1L), eq(1L)))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    when(majorNCNService.getMajorNCNByMNCNId(eq(1L), eq(1L))).thenReturn(mockMajorNCN());

    final List<SupplementaryInformationHDto> supplemtaryInfo = mncnAttachmentService.getAttachmentsMncn(1L, 1L);
    Assert.assertNotNull(supplemtaryInfo);
    Assert.assertFalse(supplemtaryInfo.isEmpty());
    Assert.assertNotNull(supplemtaryInfo.get(0).getToken());
    Assert.assertEquals(supplemtaryInfo.get(0).getTitle(), "MNCN1");
    Assert.assertEquals(supplemtaryInfo.get(1).getTitle(), "MNCN2");
    Assert.assertEquals(supplemtaryInfo.get(2).getTitle(), "MNCN4");
  }

  /**
   * Tests success scenario for {@link MNCNAttachmentService#getAttachmentsMncn(Long, Long, Long)}
   * when mncn has no access for user.
   *
   * @throws Exception if mast api call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void getAttachmentsMajorNcnNoAccess() throws Exception {
    PowerMockito.mockStatic(FileTokenGenerator.class, invocation -> {
      return TOKEN;
    });

    when(attachmentRetrofitService.getAttachmentsByAssetIdAndMncnId(eq(1L), eq(1L)))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    when(majorNCNService.getMajorNCNByMNCNId(eq(1L), eq(1L))).thenReturn(null);

    mncnAttachmentService.getAttachmentsMncn(1L, 1L);

  }

  /**
   * Tests failure scenario for {@link MNCNAttachmentService#getAttachmentsMncn(Long, Long, Long)}.
   * when external API return error.
   *
   * @throws Exception if mast api call fails.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getAttachmentsMajorNcnFail() throws Exception {
    when(attachmentRetrofitService.getAttachmentsByAssetIdAndMncnId(eq(1L), eq(1L)))
        .thenReturn(Calls.failure(new IOException("mock exception")));
    when(majorNCNService.getMajorNCNByMNCNId(eq(1L), eq(1L))).thenReturn(mockMajorNCN());

    final List<SupplementaryInformationHDto> supplemtaryInfo = mncnAttachmentService.getAttachmentsMncn(1L, 1L);
    Assert.assertNull(supplemtaryInfo);
  }

  /**
   * mock list of SupplementaryInformationHDto.
   *
   * @return the list of SupplementaryInformationHDto.
   * @throws Exception if error occur in date conversion.
   */
  private List<SupplementaryInformationHDto> getSupplementaryInfo() throws Exception {
    List<SupplementaryInformationHDto> dtos = new ArrayList<>();
    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

    SupplementaryInformationHDto supplemtaryDto = new SupplementaryInformationHDto();
    supplemtaryDto.setAttachmentType(new LinkResource(1L));
    supplemtaryDto.setAttachmentCategory(new LinkResource(1L));
    supplemtaryDto.setAttachmentUrl("123");
    supplemtaryDto.setDocumentVersion(1L);
    supplemtaryDto.setToken(TOKEN);
    supplemtaryDto.setTitle("MNCN1");
    supplemtaryDto.setUpdatedDate(dateformat.parse("22/07/2017"));
    supplemtaryDto.setUpdatedDateWithTime("2017-10-09T17:08:36Z");
    supplemtaryDto.setAttachmentDateWithTime("2016-12-22T15:53:14Z");
    supplemtaryDto.setConfidentialityType(new LinkResource(2L));

    // mock attachment type
    AttachmentTypeHDto attachmentType = new AttachmentTypeHDto();
    attachmentType.setId(1L);
    supplemtaryDto.setAttachmentTypeH(attachmentType);

    // mock attachment category
    AttachmentCategoryHDto category = new AttachmentCategoryHDto();
    category.setId(1L);
    supplemtaryDto.setAttachmentCategoryH(category);

    supplemtaryDto.setId(1L);
    dtos.add(supplemtaryDto);

    SupplementaryInformationHDto supplemtaryDto2 = new SupplementaryInformationHDto();
    supplemtaryDto2.setTitle("MNCN2");
    supplemtaryDto2.setId(2L);
    supplemtaryDto2.setAttachmentUrl(null);
    supplemtaryDto2.setDocumentVersion(null);
    supplemtaryDto2.setUpdatedDateWithTime("2017-09-01T17:08:06Z");
    supplemtaryDto2.setAttachmentDateWithTime("2017-01-31T12:14:19Z");
    supplemtaryDto2.setConfidentialityType(new LinkResource(3L));
    supplemtaryDto2.setUpdatedDate(dateformat.parse("17/08/2017"));
    AttachmentTypeHDto attachment2 = new AttachmentTypeHDto();
    attachment2.setId(1L);
    supplemtaryDto2.setAttachmentTypeH(attachment2);
    dtos.add(supplemtaryDto2);
    SupplementaryInformationHDto supplemtaryDto3 = new SupplementaryInformationHDto();
    supplemtaryDto3.setTitle("MNCN3");
    supplemtaryDto3.setId(3L);
    supplemtaryDto3.setUpdatedDate(null);
    supplemtaryDto3.setUpdatedDateWithTime("2017-10-09T17:13:21Z");
    supplemtaryDto3.setAttachmentDateWithTime("2016-12-19T11:10:18Z");
    supplemtaryDto3.setConfidentialityType(new LinkResource(1L));
    dtos.add(supplemtaryDto3);
    SupplementaryInformationHDto supplemtaryDto4 = new SupplementaryInformationHDto();
    supplemtaryDto4.setTitle("MNCN4");
    supplemtaryDto4.setId(4L);
    supplemtaryDto4.setUpdatedDate(null);
    supplemtaryDto4.setUpdatedDateWithTime("2017-10-09T17:14:44Z");
    supplemtaryDto4.setAttachmentDateWithTime("2017-01-11T15:12:31Z");
    supplemtaryDto4.setConfidentialityType(new LinkResource(2L));
    dtos.add(supplemtaryDto4);
    return dtos;
  }

  /**
   * Returns Major NCN.
   *
   * @return major ncn.
   */
  private MajorNCNHDto mockMajorNCN() {

    MajorNCNHDto mncn = new MajorNCNHDto();
    mncn.setId(1L);
    mncn.setActionTaken(new LinkResource(1L));
    mncn.setDocCompanyCountry("US");
    mncn.setDocCompanyImo("100100");
    mncn.setDocCompanyName("ABC");
    mncn.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    mncn.setJob(new LinkResource(1L));
    mncn.setStatus(new LinkResource(1L));
    mncn.setDescription("result");
    mncn.setObjectiveEvidence("evidence");
    return mncn;
  }

  /**
   * Provides Spring in-class configurations.
   *
   * @author syalavarthi
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public MNCNAttachmentService mNCNAttachmentService() {
      return PowerMockito.spy(new MNCNAttachmentServiceImpl());
    }
  }
}
