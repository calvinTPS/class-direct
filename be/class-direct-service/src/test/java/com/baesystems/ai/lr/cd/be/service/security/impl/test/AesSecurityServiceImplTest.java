package com.baesystems.ai.lr.cd.be.service.security.impl.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.MasqueradeUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.impl.AesSecurityServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * @author RKaneysan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@DirtiesContext
public class AesSecurityServiceImplTest {

  /**
   * Default asset id.
   */
  private static final Long DEFAULT_ASSET_ID = 1L;

  /**
   * Encrypted value.
   */
  private static final String ENCRYPTED_VALUE = "kHCKws5wbQILOTnxhSX8sw==";

  /**
   * Injected mock {@link AesSecurityService}.
   *
   */
  @Autowired
  private AesSecurityService aesSecurityService;

  /**
   * Successfully encrypt the role name and asset id.
   *
   */
  @Test
  public final void successfullyEncryptTheValue() {
    final String encryptedValue = aesSecurityService.encrypt(Role.EQUASIS.toString(), DEFAULT_ASSET_ID);
    assertNotNull(encryptedValue);
    assertFalse(encryptedValue.isEmpty());
  }

  /**
   * Successfully decrypt role.
   */
  @Test
  public final void successfullyDecryptRole() {
    final String roleName = aesSecurityService.decryptRoleName(ENCRYPTED_VALUE);
    assertNotNull(roleName);
    assertFalse(roleName.isEmpty());
    assertEquals(Role.EQUASIS.toString(), roleName);
  }

  /**
   * Successfully decrypt asset id.
   */
  @Test
  public final void successfullyDecryptAssetId() {
    final Long assetId = aesSecurityService.decryptAssetId(ENCRYPTED_VALUE);
    assertNotNull(assetId);
    assertEquals(DEFAULT_ASSET_ID, assetId);
  }

  /**
   * Test encrypt masquerade user id.
   */
  @Test
  public final void testEncryptMasqueradeUserId() {
    final MasqueradeUserDto dto = new MasqueradeUserDto();
    dto.setUserId(String.valueOf(DEFAULT_ASSET_ID));

    final StringResponse encryptedValue = aesSecurityService.encryptMasqueradeUserId(dto);

    assertNotNull(encryptedValue.getResponse());
    assertEquals(String.valueOf(DEFAULT_ASSET_ID), aesSecurityService.decryptValue(encryptedValue.getResponse()));
  }

  /**
   * Spring bean config.
   *
   * @author rkaneysan
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create aesSecurityService for {@link AesSecurityService}.
     *
     * @return aesSecurityService.
     */
    @Override
    @Bean
    public AesSecurityService aesSecurityService() {
      return new AesSecurityServiceImpl();
    }
  }
}
