package com.baesystems.ai.lr.cd.be.utils.test;

import com.baesystems.ai.lr.cd.be.utils.ExecutorUtils;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.junit.Test;


/**
 * @author YWearn
 */
public class ExecutorUtilsTest {

  /**
   * Default thread size.
   */
  private static final int THREAD_SIZE = 2;

  /**
   * Default maximum thread size.
   */
  private static final int THREAD_MAX_SIZE = 3;

  /**
   * Default thread name prefix.
   */
  private static final String THREAD_PREFIX = "test";

  /**
   * Default thread timeout.
   */
  private static final long THREAD_TIMEOUT = 60;

  /**
   * Expected thread name.
   */
  private static final String EXPECTED_THREAD_PREFIX = "cd-" + THREAD_PREFIX + "-thread-1";

  /**
   * Test case to create a new fixed thread pool.
   */
  @Test
  public final void testCreateFixedThreadPool() {
    final ExecutorService executor = ExecutorUtils.newFixedThreadPool(THREAD_PREFIX);
    Assert.assertNotNull(executor);
  }

  /**
   * Test case to create a new thread pool.
   */
  @Test
  public final void testCreateThreadPool() {
    final ExecutorService executor = ExecutorUtils
      .newThreadPoolExecutor(THREAD_SIZE, THREAD_MAX_SIZE, THREAD_TIMEOUT, TimeUnit.SECONDS, THREAD_PREFIX);
    Assert.assertNotNull(executor);
  }

  /**
   * Test case to create a thread factory that named thread properly.
   */
  @Test
  public final void testCreateThreadFactory() {
    final ThreadFactory factory = ExecutorUtils.namedThreadFactory(THREAD_PREFIX);
    Assert.assertNotNull(factory);

    final Runnable dummyRunable = () -> {
      return;
    };
    final Thread sampleThread = factory.newThread(dummyRunable);
    Assert.assertNotNull(sampleThread);
    Assert.assertEquals(EXPECTED_THREAD_PREFIX, sampleThread.getName());
  }


}
