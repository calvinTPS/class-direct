package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.amazonaws.services.s3.model.S3Object;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.export.TaskListExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.TaskPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.task.impl.test.TaskServiceMockTestData;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.TaskListExportService;
import com.baesystems.ai.lr.cd.service.export.impl.TaskListExportServiceImpl;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemModelItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPreviewScheduledServiceDto;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author VKolagutla
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@PrepareForTest({S3Object.class})
@ContextConfiguration(classes = TaskListExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class TaskListExportServiceTest extends TaskServiceMockTestData {

  /**
   * Constant value task.
   */
  private static final String TASK = "task";

  /**
   * Constant value service id..
   */
  private static final long SERVICE_ID = 101L;

  /**
   * Constant value asset id..
   */
  private static final long ASSET_ID = 1L;
  /**
   *
   */
  private static final double GROSS_TONNAGE = 123.55;

  /**
   * pmsTaskListExportService.
   */
  @Autowired
  private TaskListExportService taskListExportService;

  /**
   * Injected asset service.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Inject task service.
   */
  @Autowired
  private TaskService taskService;

  /**
   * Inject service service.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * amazonStorageService.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link TaskRetrofitService} from Spring context.
   */
  @Autowired
  private TaskRetrofitService taskRetrofit;

  /**
   * The directory test package path.
   */
  private static final String PATH = "data/";

  /**
   * inject login user.
   */
  @Before
  public final void injectLoginUser() {
    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Test case for success download PMS PDF.
   *
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  @Test
  public final void testDownloadTaskListInfo() throws ClassDirectException, IOException {

    LocalDate date = LocalDate.now();

    final TaskPageResource pageResource = new TaskPageResource();

    final PostponementTypeHDto postponementType = new PostponementTypeHDto();
    postponementType.setName("postponement");

    List<ScheduledServiceHDto> services = new ArrayList<>();
    ScheduledServiceHDto service = new ScheduledServiceHDto();
    service.setId(SERVICE_ID);
    service.setDueDate(DateUtils.getLocalDateToDate(date));
    service.setPostponementDate(DateUtils.getLocalDateToDate(date));
    service.setPostponementTypeH(postponementType);
    service.setDueStatusH(DueStatus.DUE_SOON);
    service.setLowerRangeDate(DateUtils.getLocalDateToDate(date));
    service.setUpperRangeDate(DateUtils.getLocalDateToDate(date));
    service.setServiceCatalogueH(mockServiceCatalogueDate());
    services.add(service);

    final TaskListExportDto dto = new TaskListExportDto();
    dto.setAssetCode("LRV1");
    dto.setServiceId(SERVICE_ID);
    dto.setItemType(TASK);


    final WorkItemTypeHDto type = new WorkItemTypeHDto();
    type.setName(TASK);
    final JobResolutionStatusHDto status = new JobResolutionStatusHDto();
    status.setName("Not Credited");

    List<WorkItemLightHDto> pmsTasks = new ArrayList<>();
    final WorkItemLightHDto pmsTask1 = new WorkItemLightHDto();
    pmsTask1.setId(1L);
    pmsTask1.setName("Task1");
    pmsTask1.setDueDate(DateUtils.getLocalDateToDate(date));
    pmsTask1.setTaskNumber("1234567");
    pmsTask1.setAssignedDate(DateUtils.getLocalDateToDate(date));
    pmsTask1.setNotes("Testing Note 123.");
    pmsTask1.setWorkItemTypeH(type);
    pmsTask1.setPostponementTypeH(postponementType);
    pmsTask1.setResolutionStatusH(status);
    pmsTask1.setResolutionStatus(new LinkResource(1L));
    pmsTask1.setPmsCreditDate(DateUtils.getLocalDateToDate(date));
    pmsTask1.setPostponementDate(DateUtils.getLocalDateToDate(date));
    pmsTasks.add(pmsTask1);

    final WorkItemLightHDto pmsTask2 = new WorkItemLightHDto();
    pmsTask2.setId(2L);
    pmsTask2.setName("Task2 with a very long name");
    pmsTask2.setDueDate(DateUtils.getLocalDateToDate(date));
    pmsTask2.setTaskNumber("9999999");
    pmsTask2.setPostponementTypeH(postponementType);
    pmsTask2.setResolutionStatus(new LinkResource(2L));
    pmsTask2.setPostponementDate(DateUtils.getLocalDateToDate(date));
    pmsTask2.setAssignedDate(DateUtils.getLocalDateToDate(date));
    pmsTasks.add(pmsTask2);

    pageResource.setContent(pmsTasks);

    FlagStateHDto flag = new FlagStateHDto();
    flag.setName("Flag");

    ClassStatusHDto classStatus = new ClassStatusHDto();
    classStatus.setName("laid up");

    ClassMaintenanceStatusHDto classMain = new ClassMaintenanceStatusHDto();
    classMain.setName("Class Maintenance");

    ClassificationSocietyHDto classificationSociety = new ClassificationSocietyHDto();
    classificationSociety.setId(1L);
    classificationSociety.setCode("ALB");
    AssetTypeDto assetType = new AssetTypeDto();
    assetType.setName("Asset Type");

    final AssetHDto asset = new AssetHDto();
    asset.setId(ASSET_ID);
    asset.setName("Test Asset");
    asset.setLeadImo("9999999");
    asset.setBuildDate(DateUtils.getLocalDateToDate(date));
    asset.setGrossTonnage(GROSS_TONNAGE);
    asset.setDueStatusH(DueStatus.IMMINENT);
    asset.setFlagStateDto(flag);
    asset.setIsMMSService(true);
    asset.setClassStatusDto(classStatus);
    asset.setKeelLayingDate(DateUtils.getLocalDateToDate(date));
    asset.setAssetType(assetType);
    asset.setClassMaintenanceStatusDto(classMain);
    asset.setEffectiveDate(DateUtils.getLocalDateToDate(date));
    asset.setHasPostponedService(true);
    asset.setCoClassificationSocietyDto(classificationSociety);
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("AB");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    asset.setIhsAssetDto(ihsAssetDetailsDto);
    asset.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    asset.getClassMaintenanceStatusDto().setName("Single");

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(taskService.getHierarchicalTasksByService(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(mockHierarchicalTasks());
    Mockito.when(serviceReferenceService.getServicesForAsset(Mockito.anyLong())).thenReturn(services);
    Mockito.when(amazonStorageService.uploadFile(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
        Mockito.anyString())).thenReturn("response");

    final WorkItemPreviewListHDto workItemLightDto1 = taskService.getHierarchicalTasksByService(SERVICE_ID, ASSET_ID);
    Assert.assertNotNull(workItemLightDto1);


    // call method
    final StringResponse response = taskListExportService.downloadTaskListInfo(dto);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   * Converts json to object.
   *
   * @return mapped object.
   * @throws IOException if coping of stream fails.
   */
  public WorkItemPreviewListHDto mockHierarchicalTasks() throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    WorkItemPreviewListHDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/HierarchicalTasks.json"), writer,
        Charset.defaultCharset());
    content = mapper.readValue(writer.toString(), WorkItemPreviewListHDto.class);
    return content;

  }

  /**
   * Returns action taken mock data.
   *
   * @return the action taken mock data.
   */
  public ServiceCatalogueHDto mockServiceCatalogueDate() {
    final ServiceCatalogueHDto data = new ServiceCatalogueHDto();
    data.setId(1L);
    data.setName("Screwshaft");
    return data;
  }

  /**
   * Returns mock hierarchical Non Continuous services has PMS tasks.
   *
   * @return list of mock services.
   * @throws JsonParseException if json parse fails.
   * @throws JsonMappingException if json mapping fails.
   * @throws IOException if coping of stream fails.
   */
  public List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> mockPMSTaskService()
      throws JsonParseException, JsonMappingException, IOException {
    List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> serviceList = new ArrayList<>();
    serviceList.add(returnObject("Service"));
    return serviceList;
  }

  /**
   * Converts json to object.
   *
   * @param jsonName the json file name.
   * @return mapped object.
   * @throws IOException if coping of stream fails.
   */
  public WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto> returnObject(final String jsonName)
      throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto> content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(PATH + jsonName + ".json"), writer,
        Charset.defaultCharset());
    JavaType type = mapper.getTypeFactory().constructParametrizedType(WorkItemPreviewScheduledServiceDto.class,
        WorkItemPreviewScheduledServiceDto.class, WorkItemModelItemDto.class);
    content = mapper.readValue(writer.toString(), type);
    return content;

  }

  /**
   * Sprint in-class configurations.
   *
   * @author VKolagutla
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * un-mock tasklist export service.
     *
     * @return
     */
    @Override
    @Bean
    public TaskListExportService taskListExportService() {
      return new TaskListExportServiceImpl();
    }
  }
}
