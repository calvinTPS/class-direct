package com.baesystems.ai.lr.cd.be.processor.test.dtos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PFauchon on 21/11/2016.
 */
public class SimpleDTO {
  /**
   * Inner DTO to test depth filtering.
   */
  private SimpleInnerDTO inner;

  /**
   * int var.
   */
  private int id;

  /**
   * String var.
   */
  private String name;


  /**
   * List of other POJO to test list and depth.
   */
  private List<SimpleInnerDTO> innerList = new ArrayList<>();


  /**
   * Get inner dto.
   *
   * @return the innerdto
   */
  public final SimpleInnerDTO getInner() {
    return inner;
  }

  /**
   * Set inner dto.
   *
   * @param innerParam get the dto
   */
  public final void setInner(final SimpleInnerDTO innerParam) {
    this.inner = innerParam;
  }


  /**
   * get Id.
   *
   * @return the id
   */
  public final int getId() {
    return id;
  }

  /**
   * set id.
   *
   * @param idParam the id
   */
  public final void setId(final int idParam) {
    this.id = idParam;
  }


  /**
   * get name.
   *
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * set Name.
   *
   * @param nameParam the name
   */
  public final void setName(final String nameParam) {
    this.name = nameParam;
  }


  /**
   * get list of inner dtos.
   *
   * @return the innerlist
   */
  public final List<SimpleInnerDTO> getInnerList() {
    return innerList;
  }


  /**
   * set list of inner dto.
   *
   * @param innerListParam the innerlist
   */
  public final void setInnerList(final List<SimpleInnerDTO> innerListParam) {
    this.innerList = innerListParam;
  }
}
