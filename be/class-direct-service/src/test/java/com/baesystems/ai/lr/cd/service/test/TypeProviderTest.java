package com.baesystems.ai.lr.cd.service.test;

import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.service.TypeProvider;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.service.TypeProvider}.
 *
 * @author Faizal Sidek
 */
public class TypeProviderTest {

    /**
     * Generic test.
     *
     */
    @Test
    public final void genericTest() {
        final Class<?> clazz = AssetTypeHDto.class;
        final Method method = clazz.getDeclaredMethods()[0];
        final String beanName = "assetReferenceService";

        TypeProvider provider = new TypeProvider(clazz, beanName, method);
        Assert.assertEquals(clazz.getName(), provider.getType().getName());
        Assert.assertEquals(method.getName(), provider.getMethod().getName());
        Assert.assertEquals(beanName, provider.getBeanName());

        provider.setType(clazz);
        provider.setBeanName(beanName);
        provider.setMethod(method);

        Assert.assertEquals(clazz.getName(), provider.getType().getName());
        Assert.assertEquals(method.getName(), provider.getMethod().getName());
        Assert.assertEquals(beanName, provider.getBeanName());
    }
}
