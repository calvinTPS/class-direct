package com.baesystems.ai.lr.cd.service.userprofile.impl.test;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.LR_ADMIN;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.LR_INTERNAL;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;
import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ACTIVE;
import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ARCHIVED;
import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.DISABLED;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Contains;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.InternalUserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.UserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BaseDtoPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.ExportSummaryDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserEORDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.LREmailDomainsDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.LREmailDomains;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.TermsConditions;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.exception.UnauthorisedAccessException;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.favourites.FavouritesService;
import com.baesystems.ai.lr.cd.service.favourites.impl.FavouritesServiceImpl;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.subfleet.impl.SubFleetServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.cd.service.userprofile.impl.UserProfileServiceImpl;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;

import retrofit2.mock.Calls;

/**
 * @author sbollu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UserProfileServiceTest.Config.class)
@DirtiesContext
public class UserProfileServiceTest extends UserProfileServiceMockTestData {

  /**
   * Logger for User Profile Service Test.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileServiceTest.class);

  /**
   * Injected user profile dao.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  /**
   * SubFleet Dao.
   */
  @Autowired
  private SubFleetDAO subFleetDao;

  /**
   * Injected user profile service.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Injected Favourites service.
   */
  @Autowired
  private FavouritesService favouritesService;

  /**
   * Injected Favourites Repository.
   */
  @Autowired
  private FavouritesRepository favouritesRepository;

  /**
   * assetService.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Injected {@link #subfleetService}.
   */
  @Autowired
  private SubFleetService subfleetService;

  /**
   * Injected userRetrofitService.
   */
  @Autowired
  private UserRetrofitService userRetrofitService;

  /**
   * assetRetrofitService.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * Injected flag state retrofit service.
   */
  @Autowired
  private FlagStateService flagStateService;

  /**
   * Security context.
   */
  private SecurityContext securityCtx;

  /**
   * message for successful creation.
   */
  @Value("${user.successful.create}")
  private String userSuccessfulCreate;

  /**
   * message for successful creation of EOR.
   */
  @Value("${userEOR.successful.create}")
  private String userEORSuccessfulCreate;

  /**
   * eor update message.
   */
  @Value("${userEOR.successful.update}")
  private String userEORSuccessfulUpdate;

  /**
   * user TC create message.
   */
  @Value("${userTC.successful.create}")
  private String userTCSuccessfulCreate;

  /**
   * message for existing user.
   */
  @Value("${user.exists}")
  private String userExists;

  /**
   * Cache Manager.
   */
  @Autowired
  private CacheManager cacheManager;

  /**
   * LR Email domains dao.
   */
  @Autowired
  private LREmailDomainsDao lrEmailDomainsDao;

  /**
   * Internal User Retrofit Service.
   */
  @Autowired
  private InternalUserRetrofitService internalUserRetrofitService;

  /**
   * external email filter.
   */
  @Value("${sitekit.email.filter}")
  private String externalEmailFilter;

  /**
   * This methods is setup pre-requisite mocking.
   *
   * @throws Exception exception.
   */
  @Before
  public void setUp() throws Exception {
    securityCtx = SecurityContextHolder.getContext();
    SecurityContext context = new SecurityContextImpl();
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    listOfRoles.add(new SimpleGrantedAuthority(EQUASIS.toString()));
    listOfRoles.add(new SimpleGrantedAuthority(THETIS.toString()));
    UserProfiles user = new UserProfiles();
    CDAuthToken token = new CDAuthToken(listOfRoles);

    token.setDetails(user);
    token.setAuthenticated(true);
    context.setAuthentication(token);
    SecurityContextHolder.setContext(context);

    // mock approve api
    StatusDto status = new StatusDto();
    status.setStatus("200");
    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));
  }

  /**
   * Clean up.
   *
   * @throws Exception Exception.
   */
  @After
  public final void cleanUp() throws Exception {
    // Clear cache for user and subfleet.
    cacheManager.getCache(CacheConstants.CACHE_USERS).clear();
    cacheManager.getCache(CacheConstants.CACHE_SUBFLEET).clear();
  }

  /**
   * Test Case for Caching New SSO Users.
   *
   * @throws Exception Object.
   */
  @Test
  public final void testCacheNewSsoUsersMutipleTimes() throws Exception {
    Cache cache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    SortedMap<String, String> userMap = mockUserIds();
    List<UserProfiles> userList = mockUsers(userMap);
    final String currentUserId = SUCCESS_USER_ID;
    // Reset DAO for User Profile as it still have reference from other test case.
    reset(userProfileDao);

    // Reinitialize DAO for User Profile.
    when(userProfileDao.getUsers(currentUserId)).thenReturn(userList);

    List<LRIDUserDto> azureList = new ArrayList<>();
    userList.forEach(user -> {
      LRIDUserDto lridDto = getUpdatedAzureDto(user);
      azureList.add(lridDto);
    });

    // Populate registered emails.
    List<String> includedUserEmails = new ArrayList<>();
    userMap.entrySet().forEach(user -> {
      LOGGER.info("User Id: {}, User Email : {}", user.getKey(), user.getValue());
      includedUserEmails.add(user.getValue());
    });
    String formattedEmails = UserProfileServiceUtils.getFormattedEmail(includedUserEmails, externalEmailFilter);
    // Populate Azure List when mock Azure call.
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(azureList));
    when(userRetrofitService.getAzureUsers(argThat(new Contains(ALICE_USER_EMAIL))))
        .thenReturn(Calls.response(azureList));

    // Check if result is null when retrieve Users.
    List<UserProfiles> users = userProfileService.getUsers(currentUserId);
    // Second call test return values
    when(userProfileDao.getUsers(currentUserId)).thenReturn(users);
    List<UserProfiles> users2 = userProfileService.getUsers(currentUserId);
    Assert.assertNotNull(users2);
    Assert.assertFalse(users2.isEmpty());

    // Checking new user in cache - inserted after updated in Azure.
    Assert.assertNotNull(users2);
    for (int count = 0; count < users2.size(); count++) {
      // Check against user returned.
      UserProfiles user = users2.get(count);
      Assert.assertNotNull(user);
      Assert.assertTrue(user.isSsoUser());

      // Check against cache.
      UserProfiles cachedUsrPrf = (UserProfiles) cache.get(CacheConstants.PREFIX_USER_ID + user.getUserId()).get();
      Assert.assertNotNull(cachedUsrPrf);
      Assert.assertTrue(cachedUsrPrf.isSsoUser());
      LOGGER.info("User id: {}, SSO User : {}", user.getUserId(), user.isSsoUser());
    }
  }

  /**
   * Test Case for Caching New SSO Users.
   *
   * @throws Exception Object.
   */
  @Test
  public final void testCacheNewSsoUsers() throws Exception {
    Cache cache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    SortedMap<String, String> userMap = mockUserIds();
    List<UserProfiles> userList = mockUsers(userMap);

    // Reset DAO for User Profile as it still have reference from other test case.
    reset(userProfileDao);

    // Reinitialize DAO for User Profile.
    when(userProfileDao.getUsers(SUCCESS_USER_ID)).thenReturn(userList);

    List<LRIDUserDto> azureList = new ArrayList<>();
    userList.forEach(user -> {
      LRIDUserDto lridDto = getUpdatedAzureDto(user);
      azureList.add(lridDto);
    });

    // Populate registered emails.
    List<String> includedUserEmails = new ArrayList<>();
    userMap.entrySet().forEach(user -> {
      LOGGER.info("User Id: {}, User Email : {}", user.getKey(), user.getValue());
      includedUserEmails.add(user.getValue());
    });
    String formattedEmails = UserProfileServiceUtils.getFormattedEmail(includedUserEmails, externalEmailFilter);
    // Populate Azure List when mock Azure call.
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(azureList));
    // Check if result is null when retrieve Users.
    List<UserProfiles> users = userProfileService.getUsers(SUCCESS_USER_ID);
    Assert.assertNotNull(users);
    Assert.assertFalse(users.isEmpty());

    // Checking new user in cache - inserted after updated in Azure.
    Assert.assertNotNull(users);
    for (int count = 0; count < users.size(); count++) {
      // Check against user returned.
      UserProfiles user = users.get(count);
      Assert.assertNotNull(user);
      Assert.assertTrue(user.isSsoUser());

      // Check against cache.
      UserProfiles cachedUsrPrf = (UserProfiles) cache.get(CacheConstants.PREFIX_USER_ID + user.getUserId()).get();
      Assert.assertNotNull(cachedUsrPrf);
      Assert.assertTrue(cachedUsrPrf.isSsoUser());
      LOGGER.info("User id: {}, SSO User : {}", user.getUserId(), user.isSsoUser());
    }
  }

  /**
   * Tear Down.
   */
  @After
  public void tearDown() {
    SecurityContextHolder.setContext(securityCtx);
  }

  /**
   * Test Case for Null Requested User Id.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getUserTestRequestedIdNull() throws Exception {
    doReturn(securityCtx.getAuthentication()).when(spy(SecurityContextHolder.getContext())).getAuthentication();
    UserProfiles userProfiles = userProfileService.getUser(null);
    for (Roles role : userProfiles.getRoles()) {
      Assert.assertEquals("Role Name", Role.THETIS.toString(), role.getRoleName());
    }
  }

  /**
   * Test Case get user Requested User Id : positive test sso user available.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getUserTestRequestedId() throws Exception {
    final String currentUserId = SUCCESS_USER_ID_10;
    final String currentUserEmail = BOB_USER_EMAIL;
    doReturn(securityCtx.getAuthentication()).when(spy(SecurityContextHolder.getContext())).getAuthentication();
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(mockUser(currentUserId));

    LRIDUserDto lridDto = mockLRIDUserDto(currentUserId, currentUserEmail, "First", "Last");
    String formattedEmails1 =
        UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(currentUserEmail), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails1)).thenReturn(Calls.response(Arrays.asList(lridDto)));
    UserProfiles userProfiles = userProfileService.getUser(currentUserId);
    Assert.assertNotNull(userProfiles);
    Assert.assertEquals(userProfiles.getUserId(), currentUserId);
    Assert.assertEquals(userProfiles.getEmail(), currentUserEmail);

    UserProfiles user = new UserProfiles();
    user.setUserId(currentUserId);
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(user);
    UserProfiles userProfiles1 = userProfileService.getUser(currentUserId);
    Assert.assertNotNull(userProfiles1);
  }

  /**
   * Test Case get user Requested User Id : positive test sso user available.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getUserTestRequestedIdWithoutCompanyNameAndUserFullName() throws Exception {
    final String currentUserId = SUCCESS_USER_ID_10;
    final String currentUserEmail = BOB_USER_EMAIL;
    final UserProfiles userprofile = mockUser(currentUserId);
    doReturn(securityCtx.getAuthentication()).when(spy(SecurityContextHolder.getContext())).getAuthentication();
    when(userProfileDao.getUser(eq(currentUserId))).thenReturn(userprofile);

    LRIDUserDto lridDto = mockLRIDUserDto(currentUserId, currentUserEmail, "Bob", "Last");
    String formattedEmails1 =
        UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(currentUserEmail), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails1)).thenReturn(Calls.response(Arrays.asList(lridDto)));

    UserProfiles userProfiles = userProfileService.getUser(currentUserId);
    Assert.assertNotNull(userProfiles);
    Assert.assertEquals(userProfiles.getUserId(), currentUserId);
    Assert.assertEquals(userProfiles.getEmail(), currentUserEmail);
  }

  /**
   * Test Case get user Requested User Id : positive test sso user available.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getUserTestRequestedIdWithoutCompanyNameAndWithoutUserFirstName() throws Exception {
    final String currentUserId = SUCCESS_USER_ID_10;
    final String currentUserEmail = BOB_USER_EMAIL;
    doReturn(securityCtx.getAuthentication()).when(spy(SecurityContextHolder.getContext())).getAuthentication();
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(mockUser(currentUserId));

    LRIDUserDto lridDto = mockLRIDUserDto(currentUserId, currentUserEmail, "Bob", "Last");
    String formattedEmails1 =
        UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(currentUserEmail), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails1)).thenReturn(Calls.response(Arrays.asList(lridDto)));
    UserProfiles userProfiles = userProfileService.getUser(currentUserId);
    Assert.assertNotNull(userProfiles);
    Assert.assertEquals(userProfiles.getUserId(), currentUserId);
    Assert.assertEquals(userProfiles.getEmail(), currentUserEmail);

    UserProfiles user = new UserProfiles();
    user.setUserId(currentUserId);
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(user);
    UserProfiles userProfiles1 = userProfileService.getUser(currentUserId);
    Assert.assertNotNull(userProfiles1);
  }

  /**
   * Test Case get user Requested User Id : positive test sso user available.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getUserTestRequestedIdWithoutCompany() throws Exception {
    final String currentUserId = SUCCESS_USER_ID_10;
    final String currentUserEmail = BOB_USER_EMAIL;
    final UserProfiles userprofile = mockUser(currentUserId);
    doReturn(securityCtx.getAuthentication()).when(spy(SecurityContextHolder.getContext())).getAuthentication();
    when(userProfileDao.getUser(eq(currentUserId))).thenReturn(userprofile);

    List<LRIDUserDto> list1 = new ArrayList<>();
    LRIDUserDto lridDto = new LRIDUserDto();
    lridDto.setObjectId(currentUserId);
    lridDto.setSignInName(currentUserEmail);
    lridDto.setRcaCity("London");
    lridDto.setGivenName("First");
    lridDto.setSurname("Last");
    lridDto.setOrganisationalRole("Manager");
    lridDto.setRcaZipPostalCode(POSTAL_CODE);
    list1.add(lridDto);

    String formattedEmails1 =
        UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(currentUserEmail), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails1)).thenReturn(Calls.response(list1));
    UserProfiles userProfiles = userProfileService.getUser(currentUserId);
    Assert.assertNotNull(userProfiles);
    Assert.assertEquals(userProfiles.getUserId(), currentUserId);
    Assert.assertEquals(userProfiles.getEmail(), currentUserEmail);
  }


  /**
   * Test Case get user Requested User Id : negative test sso user null.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getUserTestRequestedIdFail() throws Exception {
    final String currentUserId = SUCCESS_USER_ID_10;
    final UserProfiles userprofile = mockUser(currentUserId);
    doReturn(securityCtx.getAuthentication()).when(spy(SecurityContextHolder.getContext())).getAuthentication();
    when(userProfileDao.getUser(currentUserId)).thenReturn(userprofile);

    String formattedEmails1 =
        UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(BOB_USER_EMAIL), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails1)).thenReturn(Calls.failure(new IOException()));
    UserProfiles userProfiles = userProfileService.getUser(currentUserId);
    Assert.assertNotNull(userProfiles);
  }

  /**
   * Test Case for successful User retrieval.
   *
   * @throws Exception exception.
   */
  @Test
  public final void getUserTestSuccess() throws Exception {
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(mockUser(SUCCESS_USER_ID));
    List<LRIDUserDto> list1 = mockLRIDUserDtoList();
    String formattedEmails = UserProfileServiceUtils
        .getFormattedEmail(Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL}), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(list1));
    UserProfiles userProfiles = userProfileService.getUser(SUCCESS_USER_ID);
    Assert.assertEquals("userId", SUCCESS_USER_ID, userProfiles.getUserId());
    Assert.assertEquals("status", STATUS_ID1, userProfiles.getStatus().getId());
    Assert.assertEquals("email", BOB_USER_EMAIL, userProfiles.getEmail());
    Assert.assertEquals("firstName", "First", userProfiles.getFirstName());
  }

  /**
   * Test Case for unsuccessful User retrieval.
   *
   * @throws Exception Object.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getUserTestFail() throws Exception {
    when(userProfileDao.getUser(Mockito.anyString())).thenThrow(new RecordNotFoundException(" "));
    userProfileService.getUser(FAIL_USER_ID);
  }

  /**
   * Test Case for successful Requested User retrieval.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getRequestedUserTestSuccess() throws Exception {
    when(userProfileDao.getRequestedUser(Mockito.anyString())).thenReturn(mockUser(SUCCESS_USER_ID_9));
    final String formattedEmails = UserProfileServiceUtils
        .getFormattedEmail(Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL}), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockLRIDUserDtoList()));
    UserProfiles userProfiles = userProfileService.getRequestedUser(SUCCESS_USER_ID_9);
    Assert.assertEquals("userId", SUCCESS_USER_ID_9, userProfiles.getUserId());
    Assert.assertEquals("status", STATUS_ID1, userProfiles.getStatus().getId());
    Assert.assertEquals("email", BOB_USER_EMAIL, userProfiles.getEmail());
    Assert.assertEquals("firstName", "First", userProfiles.getFirstName());
  }

  /**
   * Test Case for successful Users retrieval.
   *
   * @throws Exception Object.
   */
  @Test
  public final void getUsersTestSuccess() throws Exception {
    when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(mockUsers(INITIAL_RESULT_SIZE));
    List<UserProfiles> users = userProfileService.getUsers(CLIENT_ADMIN_USER_ID);
    Assert.assertFalse(users.isEmpty());
  }

  /**
   * Test Case for unsuccessful User retrieval.
   *
   * @throws Exception Object.
   */
  @Test(expected = UnauthorisedAccessException.class)
  public final void getUsersTestFail() throws Exception {
    when(userProfileDao.getUsers(Mockito.anyString())).thenThrow(new UnauthorisedAccessException(" "));
    userProfileService.getUsers(CLIENT_USER_ID);
  }

  /**
   * Tests update user status to archive.
   * <p>
   * When user status change to archive, the cache should refresh without need to trigger SSO API.
   * Test case will need to ensure all internal fields (especially the internalUser flag) got
   * refresh in the cache as well.
   * </p>
   *
   * @throws Exception if user related tables database operation fail.
   */
  @Test
  public final void archivedUsersTestSuccess() throws Exception {
    final String email = "internal@lr.org";
    final String username = "Mock Internal User";
    final String companyName = "Mock Company Name";

    reset(favouritesRepository);
    reset(subFleetDao);
    final UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(SUCCESS_USER_ID);
    userProfiles.setEmail(email);
    userProfiles.setFlagCode(MALAYSIA_FLAG_CODE);
    userProfiles.setStatus(mockUserAccountStatus(ACTIVE));
    userProfiles.setName(username);
    userProfiles.setCompanyName(companyName);
    userProfiles.setInternalUser(true);

    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(userProfiles);
    userProfiles.setStatus(mockUserAccountStatus(ARCHIVED));
    when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);
    when(favouritesService.getFavouritesForUser(SUCCESS_USER_ID)).thenReturn(Collections.singletonList("1"));
    Assert.assertNotNull(userProfiles);

    UserProfiles user = null;
    // Retrieve and put to cache
    user = userProfileService.getUser(SUCCESS_USER_ID);

    user = userProfileService.updateUser(CLIENT_ADMIN_USER_ID, SUCCESS_USER_ID, userProfiles, null);
    Assert.assertNotNull(user);
    Assert.assertEquals(user.getUserId(), SUCCESS_USER_ID);
    Assert.assertEquals(user.getStatus().getName(), ARCHIVED.getName());

    // Verify delete all favorites and save subfleet method call.
    verify(favouritesRepository, never()).deleteAllUserFavourites(SUCCESS_USER_ID);
    verify(subFleetDao, never()).saveSubFleet(SUCCESS_USER_ID, Collections.emptyList(), Collections.emptyList());

    // Retrieve from cache
    user = userProfileService.getUser(SUCCESS_USER_ID);

    // Make sure user name, company name and isInternalUser flag is copy to cache.
    Assert.assertNotNull(user);
    Assert.assertEquals(ARCHIVED.getName(), user.getStatus().getName());
    Assert.assertEquals(username, user.getName());
    Assert.assertEquals(companyName, user.getCompanyName());
    Assert.assertEquals(true, user.isInternalUser());
  }

  /**
   * Test Case for Successful User update for LR Internal User With Different Flag Code.
   *
   * @throws Exception Object.
   */
  @Test
  public final void updateLrInternalUserWithDifferentFlagCodeTestSuccess() throws Exception {
    reset(favouritesRepository);
    reset(subFleetDao);
    final UserAccountStatus activeAccountStatus = mockUserAccountStatus(ACTIVE);
    final Set<Roles> lrInternalRole = mockUserRole(LR_INTERNAL);

    final UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(SUCCESS_USER_ID);
    userProfiles.setFlagCode(MALAYSIA_FLAG_ID.toString());
    userProfiles.setRoles(lrInternalRole);
    userProfiles.setStatus(activeAccountStatus);

    final UserProfiles updatedUserProfiles = new UserProfiles();
    updatedUserProfiles.setUserId(SUCCESS_USER_ID);
    updatedUserProfiles.setFlagCode(ANGOLA_FLAG_ID.toString());
    updatedUserProfiles.setRoles(lrInternalRole);
    updatedUserProfiles.setStatus(activeAccountStatus);

    when(userProfileDao.getUser(eq(CLIENT_ADMIN_USER_ID))).thenReturn(userProfiles);
    when(userProfileDao.getUser(eq(SUCCESS_USER_ID))).thenReturn(userProfiles);
    when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(updatedUserProfiles);
    Assert.assertNotNull(userProfiles);

    UserProfiles user = userProfileService.updateUser(CLIENT_ADMIN_USER_ID, SUCCESS_USER_ID, updatedUserProfiles, null);
    // final Company company = user.getCompany();
    Assert.assertNotNull(user);
    Assert.assertEquals(user.getUserId(), SUCCESS_USER_ID);

    // Verify flag code.
    Assert.assertNotEquals(user.getFlagCode(), userProfiles.getFlagCode());

    // Verify delete all favorites and save subfleet method call.
    verify(favouritesRepository, times(ONE)).deleteAllUserFavourites(SUCCESS_USER_ID);
    verify(subFleetDao, times(ONE)).saveSubFleet(SUCCESS_USER_ID, Collections.emptyList(), Collections.emptyList());
  }

  /**
   * Test Case for Successful User update for LR Internal User With Different Flag Code.
   *
   * @throws Exception Object.
   */
  @Test
  public final void updateLrInternalUserFromFlagCodeToClientCodeTestSuccess() throws Exception {
    reset(favouritesRepository);
    reset(subFleetDao);
    final UserAccountStatus activeAccountStatus = mockUserAccountStatus(ACTIVE);
    final Set<Roles> lrInternalRole = mockUserRole(LR_INTERNAL);

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(CLIENT_USER_ID);
    userProfiles.setFlagCode(MALAYSIA_FLAG_ID.toString());
    userProfiles.setRoles(lrInternalRole);
    userProfiles.setStatus(activeAccountStatus);

    UserProfiles updatedUserProfiles = new UserProfiles();
    updatedUserProfiles.setUserId(CLIENT_USER_ID);
    updatedUserProfiles.setClientCode(CLIENT_IMO_NUMBER);
    updatedUserProfiles.setRoles(lrInternalRole);
    updatedUserProfiles.setStatus(activeAccountStatus);

    when(userProfileDao.getUser(eq(CLIENT_ADMIN_USER_ID))).thenReturn(userProfiles);
    when(userProfileDao.getUser(eq(CLIENT_USER_ID))).thenReturn(userProfiles);
    when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(updatedUserProfiles);
    Assert.assertNotNull(userProfiles);

    UserProfiles user = userProfileService.updateUser(CLIENT_ADMIN_USER_ID, CLIENT_USER_ID, updatedUserProfiles, null);
    // final Company company = user.getCompany();
    Assert.assertNotNull(user);
    Assert.assertEquals(user.getUserId(), CLIENT_USER_ID);

    // Verify flag code.
    Assert.assertNull(user.getFlagCode());
    Assert.assertNotNull(user.getClientCode());

    // Verify favorites and save subfleet method call.
    verify(favouritesRepository, times(ONE)).deleteAllUserFavourites(CLIENT_USER_ID);
    verify(subFleetDao, times(ONE)).saveSubFleet(CLIENT_USER_ID, Collections.emptyList(), Collections.emptyList());
  }

  /**
   * Test Case for successful Users update.
   *
   * @throws Exception Object.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public final void updateUsersTestFail() throws Exception {
    final UserProfiles userprofile = mockUser(CLIENT_USER_ID);
    when(userProfileDao.getUser(eq(CLIENT_USER_ID))).thenReturn(userprofile);
    when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenThrow(new DataIntegrityViolationException(""));
    UserProfiles user = userProfileService.updateUser(CLIENT_USER_ID, CLIENT_USER_ID, null, null);
    Assert.assertNotNull(user);
  }

  /**
   * Test Case for Cache invocation for User update.
   *
   * @throws Exception Object.
   */
  @Test
  public final void updateUsersCacheInvocationTest() throws Exception {
    reset(favouritesRepository);
    reset(subFleetDao);

    UserProfiles adminUserProfiles = new UserProfiles();
    adminUserProfiles.setUserId(CLIENT_ADMIN_USER_ID);
    adminUserProfiles.setFlagCode(MALAYSIA_FLAG_CODE);
    adminUserProfiles.setStatus(mockUserAccountStatus(ARCHIVED));
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(CLIENT_ADMIN_USER_ID);
    userProfiles.setFlagCode(MALAYSIA_FLAG_CODE);
    userProfiles.setStatus(mockUserAccountStatus(ARCHIVED));

    when(userProfileDao.getUser(eq(CLIENT_ADMIN_USER_ID))).thenReturn(adminUserProfiles);
    when(userProfileDao.getUser(eq(CLIENT_USER_ID))).thenReturn(userProfiles);
    when(userProfileDao.disableExpiredUsers()).thenReturn(1);
    when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);
    when(favouritesService.getFavouritesForUser(eq(CLIENT_USER_ID))).thenReturn(Collections.singletonList("1"));

    // Trigger getUser - cache admin user.
    userProfileService.getUser(CLIENT_ADMIN_USER_ID);

    // Retrieve user from cache.
    userProfileService.updateUser(CLIENT_ADMIN_USER_ID, CLIENT_USER_ID, userProfiles, null);

    // Verify getUser call as second time onwards, data is fetched from cache.
    verify(userProfileDao, times(ONE)).getUser(eq(CLIENT_ADMIN_USER_ID));
  }

  /**
   * Test to Create User.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void createUserTestSuccess() throws ClassDirectException {
    reset(userProfileDao);
    UserAccountStatus accountStatus = mockUserAccountStatus(ACTIVE);
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(SUCCESS_USER_ID);
    userProfiles.setStatus(accountStatus);

    when(userProfileDao.getUser(Mockito.eq(SUCCESS_USER_ID))).thenReturn(userProfiles);
    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId(TEST_USER);
    userDto.setEmail(ALICE_USER_EMAIL);
    final StatusDto status = new StatusDto(userSuccessfulCreate);
    when(userProfileDao.getUser(Mockito.eq(TEST_USER))).thenThrow(new RecordNotFoundException(""));
    doReturn(status).when(userProfileDao).createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class));
    StatusDto statusExpec = userProfileService.createUser(SUCCESS_USER_ID, userDto);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Test to Create User.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void createUserCacheInvocationTest() throws ClassDirectException {
    reset(userProfileDao);
    UserAccountStatus accountStatus = mockUserAccountStatus(ACTIVE);
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(SUCCESS_USER_ID);
    userProfiles.setStatus(accountStatus);

    when(userProfileDao.getUser(Mockito.eq(SUCCESS_USER_ID))).thenReturn(userProfiles);
    when(userProfileDao.getUser(Mockito.eq(TEST_USER))).thenThrow(new RecordNotFoundException(""));
    final StatusDto status = new StatusDto(userSuccessfulCreate);
    doReturn(status).when(userProfileDao).createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class));

    // The requester user is logged in user. So, the Test User has already been cached.
    userProfileService.getUser(SUCCESS_USER_ID);

    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId(TEST_USER);
    userDto.setEmail(ALICE_USER_EMAIL);
    StatusDto statusExpec = userProfileService.createUser(SUCCESS_USER_ID, userDto);
    Assert.assertEquals(statusExpec, status);

    // Verify getUser only called once as the user is already cached.
    verify(userProfileDao, times(ONE)).getUser(eq(SUCCESS_USER_ID));
  }

  /**
   * Test to Create User.
   *
   * @throws ClassDirectException when exception occurs.
   * @throws IOException IOException.
   */
  @Test
  public final void recreateArchivedUserTestSuccess() throws ClassDirectException, IOException {
    reset(userProfileDao);

    // Retrieve the respective cache for user and subfleet.
    final Cache userCache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    final Cache subfleetCache = cacheManager.getCache(CacheConstants.CACHE_SUBFLEET);

    // Target user
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(mockUser(TEST_USER));
    List<LRIDUserDto> list1 = mockLRIDUserDtoList();
    String formattedEmails =
        UserProfileServiceUtils.getFormattedEmail(Arrays.asList(new String[] {ALICE_USER_EMAIL}), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(list1));
    UserProfiles targetUser = userProfileService.getUser(TEST_USER);
    targetUser.setRestrictAll(false);
    targetUser.setStatus(mockUserAccountStatus(ARCHIVED));

    // Mock relevant assets for subfleet, accessible and restricted asset.
    // Mock relevant assets for subfleet, accessible and restricted asset.
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ASSET_ID_1, ASSET_ID_2}));
    final PaginationDto dto = new PaginationDto();
    dto.setTotalPages(1);
    dto.setTotalElements(2L);
    assetPageResource.setPagination(dto);
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));
    when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), Mockito.isNull(Integer.class),
        Mockito.isNull(Integer.class))).thenReturn(Calls.response(assetPageResource));

    List<Long> accessibleAssets = subfleetService.getAccessibleAssetIdsForUser(TEST_USER);

    Assert.assertNotNull(accessibleAssets);
    Assert.assertFalse(accessibleAssets.isEmpty());

    when(subFleetDao.getSubFleetById(eq(TEST_USER))).thenReturn(Collections.singletonList(ASSET_ID_2));
    when(assetService.populateAssetQuery(eq(targetUser), Mockito.any(MastQueryBuilder.class)))
        .thenReturn(MastQueryBuilder.create());

    final BaseDtoPageResource subfleetPageResource = new BaseDtoPageResource();
    subfleetPageResource.setContent(Collections.singletonList(mockBaseDto().getContent().get(1)));
    when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), Mockito.isNull(Integer.class),
        Mockito.isNull(Integer.class))).thenReturn(Calls.response(subfleetPageResource));
    when(assetService.executeMastIhsQuery(any(), eq(null), eq(null), eq(false)))
        .thenReturn(Collections.singletonList(mockAssets().get(1)));
    List<Long> subfleetAssets = subfleetService.getSubFleetIds(TEST_USER);

    // Before re-create archived user.
    Assert.assertNotNull(subfleetAssets);
    Assert.assertFalse(subfleetAssets.isEmpty());
    List<Long> restrictedAssets = subfleetService.getRestrictedAssetsIds(TEST_USER);

    Assert.assertNotNull(restrictedAssets);
    Assert.assertFalse(restrictedAssets.isEmpty());

    final ValueWrapper userCacheValue = userCache.get(CacheConstants.PREFIX_USER_ID + targetUser.getUserId());
    Assert.assertNotNull(userCacheValue);
    Assert.assertEquals(userCacheValue.get(), targetUser);

    final ValueWrapper subfleetCacheValue = subfleetCache.get("User:" + targetUser.getUserId() + ":SubfleetIds");
    Assert.assertNotNull(subfleetCacheValue);
    Assert.assertEquals(subfleetCacheValue.get(), subfleetAssets);

    final ValueWrapper restrictedAssetCacheValue =
        subfleetCache.get("User:" + targetUser.getUserId() + ":RestrictedAssetIds");
    Assert.assertNotNull(restrictedAssetCacheValue);
    Assert.assertEquals(restrictedAssetCacheValue.get(), restrictedAssets);

    final ValueWrapper accessibleAssetCacheValue =
        subfleetCache.get("User:" + targetUser.getUserId() + ":AccessibleAssetIds");
    Assert.assertNotNull(accessibleAssetCacheValue);
    Assert.assertEquals(accessibleAssetCacheValue.get(), accessibleAssets);

    // Requester user
    UserProfiles requesterUser = new UserProfiles();
    requesterUser.setUserId(SUCCESS_USER_ID);
    requesterUser.setStatus(mockUserAccountStatus(ACTIVE));

    when(userProfileDao.getUser(Mockito.eq(SUCCESS_USER_ID))).thenReturn(requesterUser);
    when(userProfileDao.getUser(Mockito.eq(TEST_USER))).thenReturn(targetUser);

    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId(TEST_USER);
    userDto.setEmail(ALICE_USER_EMAIL);

    final StatusDto status = new StatusDto(userSuccessfulCreate);
    doReturn(status).when(userProfileDao).recreateArchivedUser(Mockito.any(UserProfiles.class), Mockito.anyString(),
        Mockito.any(CreateNewUserDto.class));

    StatusDto statusExpec = userProfileService.createUser(SUCCESS_USER_ID, userDto);
    // After re-create archived user.
    Assert.assertNull(userCache.get(CacheConstants.PREFIX_USER_ID + targetUser.getUserId()));
    Assert.assertNull(subfleetCache.get("User:" + targetUser.getUserId() + ":SubfleetIds"));
    Assert.assertNull(subfleetCache.get("User:" + targetUser.getUserId() + ":RestrictedAssetIds"));
    Assert.assertNull(subfleetCache.get("User:" + targetUser.getUserId() + ":AccessibleAssetIds"));

    Assert.assertEquals(statusExpec, status);
  }

  /**
   * @throws Exception unexpected exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserTestforExistingUser() throws Exception {
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(SUCCESS_USER_ID_6);
    userProfiles.setStatus(mockUserAccountStatus(ACTIVE));
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(userProfiles);
    when(userProfileDao.createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class)))
        .thenThrow(new DataIntegrityViolationException(" "));
    CreateNewUserDto newUser = new CreateNewUserDto();
    newUser.setUserId(LR_ADMIN_USER_ID);
    newUser.setEmail("puppy@bae.com");
    newUser.setFlagCode("CFG");
    final StatusDto status = userProfileService.createUser(SUCCESS_USER_ID_6, newUser);
    Assert.assertEquals(String.format(userExists, newUser.getEmail()), status.getMessage());
  }

  /**
   * Test to Create User.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserTest() throws ClassDirectException {
    when(userProfileDao.createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class)))
        .thenThrow(new DataIntegrityViolationException(" "));
    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId(TEST_USER);
    userDto.setEmail(ALICE_USER_EMAIL);
    StatusDto statusExpec = userProfileService.createUser(TEST_USER, userDto);
    Assert.assertNull(statusExpec);
  }

  /**
   * Test to Update EOR.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void updateEORTestSuccess() throws ClassDirectException {
    final StatusDto status = new StatusDto(userEORSuccessfulUpdate);
    CreateNewUserDto createDto = new CreateNewUserDto();
    UserEORDto eorInfo = new UserEORDto();
    eorInfo.setImoNumber("1231234");
    eorInfo.setAssetExpiry(LocalDate.now());
    createDto.setEor(Arrays.asList(eorInfo));

    UserProfiles userProfiles = new UserProfiles();
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(userProfiles);
    doReturn(status).when(userProfileDao).updateEORs(Mockito.anyString(), Mockito.anyString(),
        Mockito.any(CreateNewUserDto.class));
    StatusDto statusExpec = userProfileService.updateEOR(TEST_USER, TEST_USER2, createDto);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Test to Update EOR.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void adminUpdateEORCacheInvocationTest() throws ClassDirectException {
    reset(userProfileDao);
    final StatusDto status = new StatusDto(userEORSuccessfulUpdate);
    CreateNewUserDto createDto = new CreateNewUserDto();
    createDto.setUserId(TEST_USER);

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(SUCCESS_USER_ID_10);
    UserProfiles updatedUserProfiles = new UserProfiles();
    updatedUserProfiles.setUserId(TEST_USER);

    when(userProfileDao.getUser(eq(SUCCESS_USER_ID_10))).thenReturn(userProfiles);
    when(userProfileDao.getUser(eq(TEST_USER))).thenReturn(updatedUserProfiles);
    doReturn(status).when(userProfileDao).updateEORs(anyString(), anyString(), any(CreateNewUserDto.class));

    // The requester user is logged in user. So, the Test User has already been cached.
    userProfileService.getUser(SUCCESS_USER_ID_10);

    // User is updating EOR information.
    StatusDto statusExpec = userProfileService.updateEOR(SUCCESS_USER_ID_10, TEST_USER, createDto);
    Assert.assertEquals(statusExpec, status);

    // Verify getUser (first time - when logged in and, second time, when retrieve user information
    // for audit).
    verify(userProfileDao, times(ONE)).getUser(eq(SUCCESS_USER_ID_10));
  }

  /**
   * Test to Update EOR.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void selfUpdateEORCacheInvocationTest() throws ClassDirectException {
    reset(userProfileDao);
    final StatusDto status = new StatusDto(userEORSuccessfulUpdate);
    CreateNewUserDto createDto = new CreateNewUserDto();
    createDto.setUserId(TEST_USER);

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(TEST_USER);

    when(userProfileDao.getUser(eq(TEST_USER))).thenReturn(userProfiles);
    doReturn(status).when(userProfileDao).updateEORs(anyString(), anyString(), any(CreateNewUserDto.class));

    // The requester user is logged in user. So, the Test User has already been cached.
    userProfileService.getUser(TEST_USER);

    // User is updating EOR information.
    StatusDto statusExpec = userProfileService.updateEOR(TEST_USER, TEST_USER, createDto);
    Assert.assertEquals(statusExpec, status);

    // Verify getUser (get logged in user, get target user)
    verify(userProfileDao, times(TWO)).getUser(eq(TEST_USER));
  }

  /**
   * Test Case for fetching All User status.
   *
   * @throws ClassDirectException Object.
   */
  @Test
  public final void fetchAllUserAccStatus() throws ClassDirectException {
    List<UserAccountStatus> userStatus = mockUserAccountStatusList(Arrays.asList(ACTIVE, ARCHIVED, DISABLED));
    Object[] expectedUserStatus = userStatus.toArray();
    when(userProfileDao.fetchAllUserAccStatus()).thenReturn(userStatus);
    List<UserAccountStatus> status = userProfileService.fetchAllUserAccStatus();
    Object[] statusArray = status.toArray();
    Assert.assertArrayEquals(expectedUserStatus, statusArray);
  }

  /**
   * Test Case for paginantion.
   *
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   * @throws IOException
   */
  @Test
  public final void paginationTest() throws ClassDirectException {
    Integer resultSize = INITIAL_RESULT_SIZE;
    final List<UserProfiles> users = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(users);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(users);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(users);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }

    when(userRetrofitService.getAzureUsers(Mockito.anyString()))
        .thenReturn(Calls.response(mockLRIDUserDtoList(resultSize)));

    final Integer size = DEFAULT_SIZE;
    Integer totalPage = 0;
    Integer pageNumber = INITIAL_PAGE_NUMBER;

    PageResource<UserProfiles> resource = userProfileService.getUsersById(SUCCESS_USER_ID, null, null);
    Assert.assertEquals(PaginationUtil.DEFAULT_SIZE_PER_PAGE, resource.getPagination().getSize());
    Assert.assertEquals(totalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(true, resource.getPagination().getFirst());
    Assert.assertEquals(INITIAL_RESULT_SIZE.intValue(), resource.getContent().size());

    pageNumber++;
    resource = userProfileService.getUsersById(SUCCESS_USER_ID, pageNumber, size);
    final Integer newTotalPage = THREE;
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    resource.getContent().forEach(user -> LOGGER.debug("ID: {} ", user.getUserId()));

    pageNumber++;
    resource = userProfileService.getUsersById(SUCCESS_USER_ID, pageNumber, size);
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    resource.getContent().forEach(asset -> LOGGER.debug("ID: {} ", asset.getUserId()));

    resultSize = INITIAL_RESULT_SIZE + 2;
    final List<UserProfiles> userspage2 = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(userspage2);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(userspage2);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(userspage2);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }
    pageNumber++;
    resource = userProfileService.getUsersById(SUCCESS_USER_ID, pageNumber, size);
    final Integer newTotalPageNewOpt = FOUR;
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPageNewOpt, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    resource.getContent().forEach(asset -> LOGGER.debug("ID: {} ", asset.getUserId()));
  }

  /**
   * Test case for different pagination scenarios.
   *
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   * @throws IOException
   */
  @Test
  public final void paginationUsersByDtoQueryTest() throws ClassDirectException {
    Integer resultSize = INITIAL_RESULT_SIZE;
    final List<UserProfiles> users = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(users);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(users);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(users);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }

    final List<LRIDUserDto> mockUserDtoList = mockLRIDUserDtoList(resultSize);
    when(userRetrofitService.getAzureUsers(Mockito.anyString())).thenReturn(Calls.response(mockUserDtoList));
    Integer totalPage = 0;
    Integer pageNumber = INITIAL_PAGE_NUMBER;
    List<String> includedUsers = new ArrayList<>();
    List<String> excludedUsers = new ArrayList<>();
    // testing results without pagination all users.
    UserProfileDto dto1 = mockQueryDto(includedUsers, excludedUsers);
    when(userProfileDao.getUsersCount(Mockito.any(UserProfileDto.class))).thenReturn(resultSize.longValue());
    PageResource<UserProfiles> resource1 = userProfileService.getUsersByDto(dto1, null, null, null, null);
    Assert.assertEquals(PaginationUtil.DEFAULT_SIZE_PER_PAGE, resource1.getPagination().getSize());
    Assert.assertEquals(totalPage, resource1.getPagination().getTotalPages());
    Assert.assertEquals(true, resource1.getPagination().getFirst());
    Assert.assertEquals(resultSize, resource1.getPagination().getNumberOfElements());
    Assert.assertEquals(resultSize.intValue(), resource1.getPagination().getTotalElements().intValue());
    Assert.assertEquals(INITIAL_RESULT_SIZE.intValue(), resource1.getContent().size());
    // testing results without pagination excluding one user.
    resultSize = resultSize - 1;
    final List<UserProfiles> userspage2 = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(userspage2);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(userspage2);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(userspage2);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }

    when(userRetrofitService.getAzureUsers(Mockito.anyString()))
        .thenReturn(Calls.response(mockLRIDUserDtoList(resultSize)));

    excludedUsers.add(CLIENT_USER_ID);
    UserProfileDto dto = mockQueryDto(includedUsers, excludedUsers);
    when(userProfileDao.getUsersCount(Mockito.any(UserProfileDto.class))).thenReturn(resultSize.longValue());
    PageResource<UserProfiles> resource = userProfileService.getUsersByDto(dto, null, null, null, null);
    Assert.assertEquals(PaginationUtil.DEFAULT_SIZE_PER_PAGE, resource.getPagination().getSize());
    Assert.assertEquals(totalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(true, resource.getPagination().getFirst());
    Assert.assertEquals(resultSize, resource.getPagination().getNumberOfElements());
    Assert.assertEquals(resultSize.intValue(), resource.getPagination().getTotalElements().intValue());
    Assert.assertEquals(resultSize.intValue(), resource.getContent().size());

    // testing results with pagination size 5 and page 2 query excluding 1 user
    pageNumber++;
    final Integer size = DEFAULT_SIZE;
    resource = userProfileService.getUsersByDto(dto, pageNumber, size, null, null);
    final Integer newTotalPage = THREE;
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    Assert.assertEquals(false, resource.getPagination().getLast());
    Assert.assertEquals(resultSize, resource.getPagination().getNumberOfElements());
    Assert.assertEquals(resultSize.intValue(), resource.getPagination().getTotalElements().intValue());
    resource.getContent().forEach(user -> LOGGER.debug("ID: {} ", user.getUserId()));

    // test with included users
    includedUsers.add(ALICE_USER_EMAIL);
    includedUsers.add(BOB_USER_EMAIL);
    resultSize = includedUsers.size();
    final List<UserProfiles> userspage3 = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(userspage3);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(userspage3);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(userspage3);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }

    when(userRetrofitService.getAzureUsers(Mockito.anyString()))
        .thenReturn(Calls.response(mockLRIDUserDtoList(resultSize)));

    pageNumber--;
    resource = userProfileService.getUsersByDto(dto, pageNumber, size, null, null);
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(true, resource.getPagination().getFirst());
    Assert.assertEquals(resultSize.intValue(), resource.getContent().size());
    resource.getContent().forEach(asset -> LOGGER.debug("ID: {} ", asset.getUserId()));

    resultSize = INITIAL_RESULT_SIZE + 1;
    final List<UserProfiles> usersPage4 = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(usersPage4);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(usersPage4);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(usersPage4);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }
    pageNumber++;
    when(userProfileDao.getUsersCount(Mockito.any(UserProfileDto.class))).thenReturn(resultSize.longValue());
    resource = userProfileService.getUsersByDto(dto, pageNumber, size, null, null);
    final Integer newTotalPageNewOpt = THREE;
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPageNewOpt, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    resource.getContent().forEach(asset -> LOGGER.debug("ID: {} ", asset.getUserId()));
  }

  /**
   * Test Case for User Query Pagination.
   *
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   * @throws IOException
   */
  @Test
  public final void userQueryPaginationTest() throws ClassDirectException, IOException {
    Integer resultSize = INITIAL_RESULT_SIZE;
    final List<UserProfiles> users = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(users);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(users);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(users);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }

    when(userRetrofitService.getAzureUsers(Mockito.anyString()))
        .thenReturn(Calls.response(mockLRIDUserDtoList(resultSize)));

    final List<String> emptyList = Collections.emptyList();
    final Integer size = DEFAULT_SIZE;
    Integer totalPage = 0;
    Integer pageNumber = INITIAL_PAGE_NUMBER;
    UserProfileDto dto = new UserProfileDto();
    dto.setUserId(GET_USERS_USER_ID);
    dto.setStatus(Collections.singletonList(ACTIVE.getName()));
    dto.setEmail("abc");
    dto.setExcludedUsers(emptyList);
    dto.setIncludedUsers(emptyList);
    dto.setSearchSSOIfNotExist(false);
    when(userProfileDao.getUsersCount(Mockito.any(UserProfileDto.class))).thenReturn(resultSize.longValue());
    PageResource<UserProfiles> resource = userProfileService.getUsersByDto(dto, null, null, null, null);
    Assert.assertEquals(PaginationUtil.DEFAULT_SIZE_PER_PAGE, resource.getPagination().getSize());
    Assert.assertEquals(totalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(true, resource.getPagination().getFirst());
    Assert.assertEquals(INITIAL_RESULT_SIZE.intValue(), resource.getContent().size());
    pageNumber++;
    resource = userProfileService.getUsersByDto(dto, pageNumber, size, null, null);
    final Integer newTotalPage = THREE;
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    resource.getContent().forEach(user -> LOGGER.debug("ID: {} ", user.getUserId()));
    pageNumber++;
    resource = userProfileService.getUsersByDto(dto, pageNumber, size, null, null);
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPage, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    resource.getContent().forEach(asset -> LOGGER.debug("ID: {} ", asset.getUserId()));
    resultSize = INITIAL_RESULT_SIZE + FOUR;

    final List<UserProfiles> usersPage2 = mockUsers(resultSize);
    try {
      when(userProfileDao.getUsers(Mockito.anyString())).thenReturn(usersPage2);
      Mockito
          .when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.isNull(Integer.class),
              Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
          .thenReturn(usersPage2);
      when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
          Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(usersPage2);
    } catch (final RecordNotFoundException | UnauthorisedAccessException exception) {
      exception.printStackTrace();
    }

    pageNumber++;
    when(userProfileDao.getUsersCount(Mockito.any(UserProfileDto.class))).thenReturn(resultSize.longValue());
    resource = userProfileService.getUsersByDto(dto, pageNumber, size, null, null);
    final Integer newTotalPageNewOpt = FOUR;
    Assert.assertEquals(size, resource.getPagination().getSize());
    Assert.assertEquals(newTotalPageNewOpt, resource.getPagination().getTotalPages());
    Assert.assertEquals(false, resource.getPagination().getFirst());
    resource.getContent().forEach(asset -> LOGGER.debug("ID: {} ", asset.getUserId()));
  }

  /**
   * Test Case for Multiple Non-SSO User retrieval.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUsersWithoutSSO() throws ClassDirectException {
    List<LRIDUserDto> mockLridDtoUserList = mockLRIDUserDtoList();
    final String formattedEmails = UserProfileServiceUtils
        .getFormattedEmail(Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL}), externalEmailFilter);
    List<UserProfiles> usersProfile = new ArrayList<>();
    final Set<Roles> roles = mockUserRole(LR_ADMIN);
    UserProfiles user = new UserProfiles();
    user.setEmail(ALICE_USER_EMAIL);
    user.setRoles(roles);
    usersProfile.add(user);
    UserProfiles user2 = new UserProfiles();
    user2.setEmail(BOB_USER_EMAIL);
    user2.setRoles(roles);
    usersProfile.add(user2);
    UserProfileDto dto = new UserProfileDto();
    dto.setUserId("");
    dto.setStatus(Collections.singletonList(ACTIVE.getName()));
    dto.setEmail("");
    dto.setSearchSSOIfNotExist(false);

    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockLridDtoUserList));
    when(userRetrofitService.getAzureUsers(argThat(new Contains(ALICE_USER_EMAIL))))
        .thenReturn(Calls.response(mockLridDtoUserList));
    when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(usersProfile);

    List<UserProfiles> result = userProfileService.getUsers(dto, PaginationUtil.DEFAULT_STARTING_PAGE,
        PaginationUtil.DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertNotNull(result);
    Assert.assertTrue(result.get(0).isCdUser());
    Assert.assertTrue(result.get(0).isSsoUser());
    Assert.assertEquals(result.get(0).getFirstName(), mockLridDtoUserList.get(0).getGivenName());
    Assert.assertEquals(result.get(0).getLastName(), mockLridDtoUserList.get(0).getSurname());
    Assert.assertEquals(result.get(0).getEmail(), usersProfile.get(0).getEmail());
    Assert.assertTrue(result.get(1).isCdUser());
    Assert.assertTrue(result.get(1).isSsoUser());
    Assert.assertEquals(result.get(1).getFirstName(), mockLridDtoUserList.get(1).getGivenName());
    Assert.assertEquals(result.get(1).getLastName(), mockLridDtoUserList.get(1).getSurname());
    Assert.assertEquals(result.get(1).getEmail(), usersProfile.get(1).getEmail());
    Assert.assertEquals(POSTAL_CODE, result.get(1).getCompany().getPostCode());
  }

  /**
   * Test Case for Multiple SSO User retrieval.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUsersWithSSO() throws ClassDirectException {
    List<LRIDUserDto> mockLridDtoUserList = mockLRIDUserDtoList();
    final List<String> includedUsers = Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL});
    final String formattedEmails = UserProfileServiceUtils.getFormattedEmail(includedUsers, externalEmailFilter);
    final Set<Roles> roles = mockUserRole(LR_ADMIN);
    List<UserProfiles> usersList = new ArrayList<>();
    UserProfiles user = new UserProfiles();
    user.setEmail(ALICE_USER_EMAIL);
    user.setRoles(roles);
    UserProfiles user2 = new UserProfiles();
    user2.setEmail(BOB_USER_EMAIL);
    user2.setRoles(roles);
    UserProfileDto dto = new UserProfileDto();
    dto.setUserId("1");
    dto.setStatus(Collections.singletonList(ACTIVE.getName()));
    dto.setEmail("");
    dto.setIncludedUsers(includedUsers);
    dto.setExcludedUsers(new ArrayList<>());
    dto.setSearchSSOIfNotExist(true);
    usersList.add(user);
    usersList.add(user2);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockLridDtoUserList));
    when(userRetrofitService.getAzureUsers(argThat(new Contains(ALICE_USER_EMAIL))))
        .thenReturn(Calls.response(mockLridDtoUserList));
    when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(usersList);

    List<UserProfiles> result = userProfileService.getUsers(dto, PaginationUtil.DEFAULT_STARTING_PAGE,
        PaginationUtil.DEFAULT_SIZE_PER_PAGE, null, null);

    Assert.assertNotNull(result);
    Assert.assertFalse(result.isEmpty());
    Assert.assertTrue(result.get(0).isCdUser());
    Assert.assertTrue(result.get(0).isSsoUser());
    Assert.assertTrue(result.get(1).isCdUser());
    Assert.assertTrue(result.get(1).isSsoUser());
    Assert.assertEquals(result.get(0).getFirstName(), mockLridDtoUserList.get(0).getGivenName());
    Assert.assertEquals(result.get(0).getLastName(), mockLridDtoUserList.get(0).getSurname());
    Assert.assertEquals(result.get(0).getEmail(), usersList.get(0).getEmail());
    Assert.assertEquals(result.get(1).getFirstName(), mockLridDtoUserList.get(1).getGivenName());
    Assert.assertEquals(result.get(1).getLastName(), mockLridDtoUserList.get(1).getSurname());
    Assert.assertEquals(result.get(1).getEmail(), usersList.get(1).getEmail());
  }

  /**
   * Test Case for get All Users with success scenario.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testGetAllUsers() throws ClassDirectException {
    final List<FlagStateHDto> flagList = mockFlagStateList();
    final List<String> emails = Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL, DAVID_USER_EMAIL});
    final List<Role> roles = Arrays.asList(new Role[] {Role.LR_ADMIN, Role.LR_INTERNAL, Role.LR_INTERNAL});
    final List<String> userIds = Arrays.asList(new String[] {SUCCESS_USER_ID_4, SUCCESS_USER_ID_5, SUCCESS_USER_ID_6});
    final List<UserProfiles> usersProfileList = mockUsersList(emails, roles, userIds, emails.size());
    final UserProfiles user = usersProfileList.get(0);
    final UserProfiles user2 = usersProfileList.get(1);
    final UserProfiles user3 = usersProfileList.get(2);
    String formattedEmails = UserProfileServiceUtils.getFormattedEmail(emails, externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockLRIDUserDtoList(emails)));
    when(userRetrofitService.getAzureUsers(argThat(new Contains(ALICE_USER_EMAIL))))
        .thenReturn(Calls.response(mockLRIDUserDtoList(emails)));
    when(userProfileDao.getUser(Mockito.anyString())).thenReturn(user);
    when(userProfileDao.getAllUsers()).thenReturn(usersProfileList);
    when(flagStateService.getFlagStates()).thenReturn(flagList);
    doReturn(user).when(userProfileDao).updateUser(user.getUserId(), user.getUserId(), user, null);
    doReturn(user2).when(userProfileDao).updateUser(user2.getUserId(), user2.getUserId(), user2, null);
    doReturn(user3).when(userProfileDao).updateUser(user3.getUserId(), user3.getUserId(), user3, null);

    userProfileService.updateAllUsersInformations();
    Cache cache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    for (UserProfiles userprofile : usersProfileList) {
      Assert.assertNotNull(cache.get(CacheConstants.PREFIX_USER_ID + userprofile.getUserId()));
    }
  }

  /**
   * Test Case for get All Users with Failed scenario With Interrupt Exception.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testGetAllUsersFailedWithInterruptException() throws ClassDirectException {
    final List<String> emails = Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL, DAVID_USER_EMAIL});
    final List<Role> roles = Arrays.asList(new Role[] {Role.LR_ADMIN, Role.LR_INTERNAL, Role.LR_INTERNAL});
    final List<String> userIds = Arrays.asList(new String[] {SUCCESS_USER_ID_4, SUCCESS_USER_ID_5, SUCCESS_USER_ID_6});
    final List<UserProfiles> usersProfileList = mockUsersList(emails, roles, userIds, emails.size());

    when(userProfileDao.getAllUsers()).thenThrow(new ClassDirectException(""));
    userProfileService.updateAllUsersInformations();
    Cache cache = cacheManager.getCache(CacheConstants.CACHE_USERS);
    for (UserProfiles userprofile : usersProfileList) {
      Assert.assertNull(cache.get(CacheConstants.PREFIX_USER_ID + userprofile.getUserId()));
    }
  }

  /**
   * Test case for retrieving EOR ids By user id.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  @Test
  public final void testGetEorsByUserId() throws ClassDirectException {
    final Set<String> result = new HashSet<>();
    result.add(GET_USERS_USER_ID);
    result.add(CLIENT_USER_ID);
    when(userProfileDao.getEorsByUserId(USER_ID.toString())).thenReturn(result);
    Assert.assertNotNull(userProfileService.getEorsByUserId(USER_ID.toString()));
  }

  /**
   * Test Case for Single Non-SSO User retrieval.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUserSingleWithoutSSO() throws ClassDirectException {
    when(userRetrofitService.getAzureUsers(Mockito.anyString())).thenReturn(Calls.response(new ArrayList<>()));

    final List<LRIDUserDto> mockList = mockLRIDUser();
    String formattedEmails1 =
        UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(BOB_USER_EMAIL), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails1))
        .thenReturn(Calls.response(Collections.singletonList(mockList.get(0))));

    final List<String> includedUsers = Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL});
    final String formattedEmails = UserProfileServiceUtils.getFormattedEmail(includedUsers, externalEmailFilter);

    List<LRIDUserDto> mockLridDtoUserList = mockLRIDUserDtoList();
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockLridDtoUserList));
    List<UserProfiles> usersProfile = new ArrayList<>();
    UserProfiles user = new UserProfiles();
    user.setEmail(BOB_USER_EMAIL);
    usersProfile.add(user);
    UserProfileDto dto = new UserProfileDto();
    dto.setUserId("");
    dto.setStatus(Collections.singletonList(ACTIVE.getName()));
    dto.setEmail(BOB_USER_EMAIL);
    dto.setSearchSSOIfNotExist(false);

    when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(usersProfile);

    List<UserProfiles> result = userProfileService.getUsers(dto, PaginationUtil.DEFAULT_STARTING_PAGE,
        PaginationUtil.DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertNotNull(result);
    Assert.assertFalse(result.isEmpty());
    Assert.assertTrue(result.get(0).isCdUser());
    Assert.assertTrue(result.get(0).isSsoUser());
    Assert.assertEquals(result.get(0).getEmail(), BOB_USER_EMAIL);
    Assert.assertEquals(result.get(0).getFirstName(), mockLridDtoUserList.get(1).getGivenName());
    Assert.assertEquals(result.get(0).getLastName(), mockLridDtoUserList.get(1).getSurname());
  }

  /**
   * Test Case for Single SSO User retrieval.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUserSingleWithSSO() throws ClassDirectException {
    final List<LRIDUserDto> mockList = mockLRIDUser();
    String formattedEmails =
        UserProfileServiceUtils.getFormattedEmail(Arrays.asList(new String[] {ALICE_USER_EMAIL}), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails))
        .thenReturn(Calls.response(Collections.singletonList(mockList.get(1))));

    List<LRIDUserDto> mockLridDtoUserList = mockLRIDUserDtoList();
    final LRIDUserDto aliceLridUserDto = mockLridDtoUserList.get(0);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockLridDtoUserList));

    List<UserProfiles> usersProfile = new ArrayList<>();
    UserProfileDto dto = new UserProfileDto();
    dto.setEmail(ALICE_USER_EMAIL);
    dto.setSearchSSOIfNotExist(true);

    when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
        Mockito.anyString(), Mockito.anyString())).thenReturn(usersProfile);

    List<UserProfiles> result = userProfileService.getUsers(dto, PaginationUtil.DEFAULT_STARTING_PAGE,
        PaginationUtil.DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertNotNull(result);
    Assert.assertFalse(result.get(0).isCdUser());
    Assert.assertTrue(result.get(0).isSsoUser());
    Assert.assertEquals(ALICE_USER_EMAIL, result.get(0).getEmail());
    Assert.assertEquals(aliceLridUserDto.getGivenName(), result.get(0).getFirstName());
    Assert.assertEquals(aliceLridUserDto.getSurname(), result.get(0).getLastName());
  }

  /**
   * Test Case for Multiple SSO User retrieval with Unknown Email.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUsersWithSSONotMatchWithMail() throws ClassDirectException {
    when(userRetrofitService.getAzureUsers(Mockito.anyString())).thenReturn(Calls.response(new ArrayList<>()));
    final List<LRIDUserDto> mockList = mockLRIDUser();
    String formattedEmails1 =
        UserProfileServiceUtils.getFormattedEmail(Collections.singletonList(BOB_USER_EMAIL), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails1))
        .thenReturn(Calls.response(Collections.singletonList(mockList.get(0))));

    String formattedEmails = UserProfileServiceUtils
        .getFormattedEmail(Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL}), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockList));

    List<UserProfiles> usersProfile = new ArrayList<>();
    UserProfileDto dto = new UserProfileDto();
    dto.setUserId("");
    dto.setStatus(Collections.emptyList());
    dto.setEmail("admin@lr.org");
    dto.setSearchSSOIfNotExist(true);

    when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
        Mockito.anyString(), Mockito.anyString())).thenReturn(usersProfile);

    List<UserProfiles> result = userProfileService.getUsers(dto, PaginationUtil.DEFAULT_STARTING_PAGE,
        PaginationUtil.DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertNotNull(result);
  }

  /**
   * Test Case for Empty User retrieval.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUsersWithEmpty() throws ClassDirectException {
    List<LRIDUserDto> mockLridDtoUserList = mockLRIDUserDtoList();
    String formattedEmails = UserProfileServiceUtils
        .getFormattedEmail(Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL}), externalEmailFilter);
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(mockLridDtoUserList));

    final Set<Roles> roles = mockUserRole(LR_ADMIN);
    List<UserProfiles> usersProfile = new ArrayList<>();
    UserProfiles user = new UserProfiles();
    user.setEmail(ALICE_USER_EMAIL);
    user.setRoles(roles);
    usersProfile.add(user);
    UserProfiles user2 = new UserProfiles();
    user2.setEmail(BOB_USER_EMAIL);
    user2.setRoles(roles);
    usersProfile.add(user2);

    UserProfileDto dto = new UserProfileDto();
    when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(usersProfile);

    List<UserProfiles> result = userProfileService.getUsers(dto, PaginationUtil.DEFAULT_STARTING_PAGE,
        PaginationUtil.DEFAULT_SIZE_PER_PAGE, null, null);
    Assert.assertTrue(result.size() > 0);
    Assert.assertEquals(usersProfile.get(0).getEmail(), result.get(0).getEmail());
    Assert.assertEquals(usersProfile.get(1).getEmail(), result.get(1).getEmail());
    Assert.assertTrue(result.get(0).isCdUser());
    Assert.assertTrue(result.get(0).isSsoUser());
  }

  /**
   * Test to get latest terms and conditions.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void getLatestTermsConditionsTest() throws ClassDirectException {
    TermsConditions tcLatest = new TermsConditions();
    tcLatest.setTcId(USER_ID);
    tcLatest.setTcDesc("test");
    when(userProfileDao.getLatestTermsConditions()).thenReturn(tcLatest);
    TermsConditions tc = userProfileService.getLatestTermsConditions();
    Assert.assertNotNull(tc);
    Assert.assertEquals(USER_ID, tcLatest.getTcId());
  }

  /**
   * Test to Create Terms and Conditions.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void createTermsandConditionsTestSuccess() throws ClassDirectException {
    final StatusDto status = new StatusDto(userTCSuccessfulCreate);
    doReturn(status).when(userProfileDao).createAcceptedTermsandConditions(Mockito.anyString(), Mockito.anyLong());
    StatusDto statusExpec = userProfileService.createAcceptedTermsandConditions(Mockito.anyString(), Mockito.anyLong());
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Test to get export summary of users having client and lr_admin roles, min and max login times
   * of users.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void userExportSummary() throws ClassDirectException {
    UserProfileDto userDto = new UserProfileDto();
    final List<String> includedUsers = Arrays.asList(new String[] {DAVID_USER_EMAIL, ALICE_USER_EMAIL, BOB_USER_EMAIL});
    userDto.setIncludedUsers(includedUsers);
    when(userProfileDao.getUsers(Mockito.any(UserProfileDto.class), Mockito.anyInt(), Mockito.anyInt(),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(mockUsersList());
    ExportSummaryDto exportSummary = userProfileService.exportSummary(userDto);
    Assert.assertNotNull(exportSummary);
    Assert.assertEquals("CLIENT", exportSummary.getAccountTypes().get(0));
    Assert.assertEquals("LR_ADMIN", exportSummary.getAccountTypes().get(1));
    Assert.assertEquals("2016-12-09T09:09:09", exportSummary.getLastLoginDate().getFrom());
    Assert.assertEquals("2017-12-09T09:09:09", exportSummary.getLastLoginDate().getTo());
  }

  /**
   * Test to Create User: approve user, negative test.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserApproveUserFail() throws ClassDirectException {
    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId(TEST_USER);
    userDto.setEmail(ALICE_USER_EMAIL);

    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final StatusDto status = new StatusDto(userSuccessfulCreate);
    doReturn(status).when(userProfileDao).createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class));
    userProfileService.createUser(CLIENT_USER_ID, userDto);
  }

  /**
   * Test to Disable Expired Users.
   *
   * @throws ClassDirectException when exception occurs.
   */
  @Test
  public final void disableExpiredUsers() throws ClassDirectException {
    when(userProfileDao.disableExpiredUsers()).thenReturn(0);
    userProfileService.disableExpiredUserAccounts();
  }

  /**
   * This method test for empty user id.
   *
   * @throws Exception unexpected exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserTestforEmptyUserId() throws Exception {
    final CreateNewUserDto newUser = new CreateNewUserDto();
    userProfileService.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * This method test for null create new user dto.
   *
   * @throws Exception unexpected exception.
   */
  @Test(expected = ClassDirectException.class)
  public final void createUserTestforNullCreateNewUserDto() throws Exception {
    final CreateNewUserDto newUser = null;
    userProfileService.createUser(LR_ADMIN_USER_ID, newUser);
  }

  /**
   * Tests that emails with external domains are routed to to {@link UserRetrofitService}.
   *
   * @throws ClassDirectException exceptions.
   */
  @Test
  public final void testExternalUser() throws ClassDirectException {
    reset(userRetrofitService);
    final UserProfiles internalUser = new UserProfiles();
    internalUser.setEmail(EXTERNAL_EMAIL);
    when(userProfileDao.getRequestedUser(CLIENT_USER_ID)).thenReturn(internalUser);
    when(userRetrofitService.getAzureUsers(anyString())).thenReturn(null);
    final UserProfiles userProfiles = userProfileService.getRequestedUser(CLIENT_USER_ID);
    Assert.assertFalse(userProfiles.isInternalUser());
    verify(userRetrofitService, times(1)).getAzureUsers(anyString());
  }

  /**
   * Tests success scenario to update last login time for user
   * {@link UserProfileService#updateUserLastLogin(String, java.time.LocalDateTime)}.
   *
   * @throws ClassDirectException when if any sql exception or error in getting user or any error
   *         execution flow.
   */
  @Test
  public final void testUpdateLastLoginSuccess() throws ClassDirectException {
    UserProfiles user = new UserProfiles();
    user.setUserId(CLIENT_USER_ID);
    when(userProfileDao.updateUserLastLogin(CLIENT_USER_ID, LocalDateTime.of(2017, 12, 9, 2, 20, 20))).thenReturn(1);
    when(userProfileDao.getUser(CLIENT_USER_ID)).thenReturn(user);
    userProfileService.updateUserLastLogin(CLIENT_USER_ID, LocalDateTime.of(2017, 12, 9, 2, 20, 20));
  }

  /**
   * Tests failure scenario to update last login time for user
   * {@link UserProfileService#updateUserLastLogin(String, java.time.LocalDateTime)} when fails to
   * update last login.
   *
   * @throws ClassDirectException if any sql exception or error in getting user or any error
   *         execution flow.
   */
  @Test(expected = ClassDirectException.class)
  public final void testUpdateLastLoginFail() throws ClassDirectException {
    UserProfiles user = new UserProfiles();
    user.setUserId(CLIENT_USER_ID);
    when(userProfileDao.updateUserLastLogin(CLIENT_USER_ID, LocalDateTime.of(2017, 12, 9, 2, 20, 20)))
        .thenThrow(new ClassDirectException("mock"));
    when(userProfileDao.getUser(CLIENT_USER_ID)).thenReturn(user);
    userProfileService.updateUserLastLogin(CLIENT_USER_ID, LocalDateTime.of(2017, 12, 9, 2, 20, 20));

  }

  /**
   * Mocks list of base dto with id's.
   *
   * @param ids the list of asset ids.
   * @return the mocked asset list.
   */
  private List<BaseDto> mockBaseDtoWithIds(final Long[] ids) {
    final List<BaseDto> assetIds = new ArrayList<>();

    for (Long id : ids) {
      BaseDto dto = new BaseDto();
      dto.setId(id);
      assetIds.add(dto);
    }

    return assetIds;
  }

  /**
   * @author sbollu
   */
  @Configuration
  @EnableCaching
  public static class Config extends BaseMockConfiguration {
    /**
     * @return UserProfile Service.
     */
    @Override
    @Bean
    public UserProfileService userProfileService() {
      return new UserProfileServiceImpl();
    }

    @Override
    @Bean
    public LREmailDomainsDao lrEmailDomainsDao() {
      final LREmailDomainsDao lrEmailDomainsDao = Mockito.mock(LREmailDomainsDao.class);
      final LREmailDomains domain = new LREmailDomains();
      domain.setDomain("lr.org");
      when(lrEmailDomainsDao.getLrEmailDomains()).thenReturn(Arrays.asList(domain));
      return lrEmailDomainsDao;
    }

    /**
     * Create subfleet service.
     *
     * @return service.
     */
    @Override
    @Bean
    public SubFleetService subFleetService() {
      return spy(new SubFleetServiceImpl());
    }

    /**
     * Create Favorites service.
     *
     * @return service.
     */
    @Override
    @Bean
    public FavouritesService favouritesService() {
      return spy(new FavouritesServiceImpl());
    }

    /**
     * Return cache.
     *
     * @return cache object.
     */
    @Bean
    public CacheManager cacheManager() {
      // Configure and Return implementation of Spring's CacheManager.
      SimpleCacheManager cacheManager = new SimpleCacheManager();
      List<Cache> caches = new ArrayList<Cache>();
      ConcurrentMapCache concurrentMapUserCache = new ConcurrentMapCache(CacheConstants.CACHE_USERS);
      ConcurrentMapCache concurrentMapSubFleetCache = new ConcurrentMapCache(CacheConstants.CACHE_SUBFLEET);
      caches.add(concurrentMapUserCache);
      caches.add(concurrentMapSubFleetCache);
      cacheManager.setCaches(caches);
      return cacheManager;
    }
  }
}
