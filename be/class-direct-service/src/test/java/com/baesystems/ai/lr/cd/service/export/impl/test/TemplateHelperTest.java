package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.service.export.impl.TemplateHelper;

/**
 * Unit test for template helper.
 *
 * @author yng
 *
 */
public class TemplateHelperTest {

  /**
   * seven.
   */
  private static final int SEVEN = 7;

  /**
   * Test format date.
   */
  @Test
  public final void testFormatDate() {
    final TemplateHelper helper = new TemplateHelper();

    final GregorianCalendar date = new GregorianCalendar();

    final String formattedDate = helper.formatDate(date.getTime());

    final Pattern pattern = Pattern.compile("\\d{2} \\D{3} \\d{4}");
    final Matcher matcher = pattern.matcher(formattedDate);

    Assert.assertTrue(matcher.matches());
  }

  /**
   * Test format print date.
   */
  @Test
  public final void testFormatPrintDate() {
    final TemplateHelper helper = new TemplateHelper();
    final GregorianCalendar date = new GregorianCalendar();
    final String formattedDate = helper.formatPrintDate(date.getTime());
    final Pattern pattern = Pattern.compile("\\D{3}+,\\s\\D{3}\\s\\d{2}+,\\s\\d{4}");
    final Matcher matcher = pattern.matcher(formattedDate);
    Assert.assertTrue(matcher.matches());
  }

  /**
   * Test left zero padding.
   */
  @Test
  public final void testleftZeroPaddingForJobId() {
    final TemplateHelper helper = new TemplateHelper();

    Assert.assertEquals(SEVEN, helper.leftZeroPaddingForJobId(1L).length());
  }

  /**
   * Test left zero padding, and concatenation.
   */
  @Test
  public final void testleftZeroPaddingForJobIdWithList() {
    final TemplateHelper helper = new TemplateHelper();

    final JobHDto job1 = new JobHDto();
    job1.setId(1L);
    job1.setJobNumber("167");
    final JobHDto job2 = new JobHDto();
    job2.setId(2L);
    job2.setJobNumber("277");

    final java.util.List<JobHDto> jobs = new ArrayList<>();
    jobs.add(job1);
    jobs.add(job2);

    final String output = helper.leftZeroPaddingForJobId(jobs);

    final Pattern pattern = Pattern.compile("\\d{3},\\s\\d{3}");
    final Matcher matcher = pattern.matcher(output);

    Assert.assertTrue(matcher.matches());
  }

 /**
  * test double value conversion.
  */
  @Test
  public final void testremoveDecimals() {
    Double randomValue  = Math.random();
    String convertedValue = String.valueOf(Math.round(randomValue));
    final Pattern pattern = Pattern.compile("\\d+");
    final Matcher matcher = pattern.matcher(convertedValue);

    Assert.assertTrue(matcher.matches());
  }

}
