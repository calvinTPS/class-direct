package com.baesystems.ai.lr.cd.service.attachment.reference.impl.test;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.AttachmentReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;
import com.baesystems.ai.lr.cd.service.attachment.reference.AttachmentReferenceService;
import com.baesystems.ai.lr.cd.service.attachment.reference.impl.AttachmentReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

import retrofit2.mock.Calls;

/**
 * Provides unit test for AttachmentReferenceService.
 *
 * @author syalavarthi.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AttachmentReferenceServiceTest.Config.class)
@DirtiesContext
public class AttachmentReferenceServiceTest {

  /**
   * The {@link ApplicationContext} is spring context.
   */
  @Autowired
  private ApplicationContext context;
  /**
   * The {@link AttachmentReferenceService} from spring context.
   *
   */
  @Autowired
  private AttachmentReferenceService attachmentReferenceService;
  /**
   * {@value #ATTACHMENT_NAME} is constant for attachment name.
   *
   */
  private static final String ATTACHMENT_NAME = "Picture";

  /**
   * Tests success scenario for {@link AttachmentReferenceService#getAttachmentTypes()}.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void successfullyRetrieveListOfAttachmentTypes() throws Exception {
    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAttachmentTypes()).thenReturn(Calls.response(mockAttachmentTypes()));

    final List<AttachmentTypeHDto> attachmentTypes = attachmentReferenceService.getAttachmentTypes();
    Assert.assertNotNull(attachmentTypes);
    Assert.assertNotEquals(0, attachmentTypes.size());
    Assert.assertEquals(attachmentTypes.get(0).getName(), ATTACHMENT_NAME);
  }

  /**
   * Tests failure scenario for {@link AttachmentReferenceService#getAttachmentTypes()}.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void failedToRetriveListOfAttachmentTypes() throws Exception {
    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);
    when(retrofitService.getAttachmentTypes()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<AttachmentTypeHDto> attachmentTypes = attachmentReferenceService.getAttachmentTypes();
    Assert.assertNotNull(attachmentTypes);
    Assert.assertEquals(0, attachmentTypes.size());
  }

  /**
   * Tests success scenario for {@link AttachmentReferenceService#getAttachmentType(Long)}.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void successfullyGetAttachmentType() throws Exception {
    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAttachmentTypes()).thenReturn(Calls.response(mockAttachmentTypes()));

    final AttachmentTypeHDto attachmentType = attachmentReferenceService.getAttachmentType(1L);
    Assert.assertNotNull(attachmentType);
    Assert.assertEquals(attachmentType.getName(), ATTACHMENT_NAME);
  }

  /**
   * Tests empty result for {@link AssetReferenceService#getAssetType(Long)}.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void emptyResultForGetAssetType() throws Exception {
    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAttachmentTypes()).thenReturn(Calls.failure(new IOException("Mock exception")));

    final AttachmentTypeHDto attachmentType = attachmentReferenceService.getAttachmentType(1L);
    Assert.assertNull(attachmentType);
  }

  /**
   * Tests success scenario for {@link AttachmentReferenceService#getAttachmentCategories()}.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void successfullyRetrieveListOfAttachmentCategories() throws Exception {
    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);

    Mockito.when(retrofitService.getAttachmentCategories()).thenReturn(Calls.response(mockAttachmentCategories()));

    final List<AttachmentCategoryHDto> attachmentCaterories = attachmentReferenceService.getAttachmentCategories();
    Assert.assertNotNull(attachmentCaterories);
    Assert.assertNotEquals(0, attachmentCaterories.size());
    Assert.assertEquals(attachmentCaterories.get(0).getName(), ATTACHMENT_NAME);
  }

  /**
   * Tests failure scenario for {@link AttachmentReferenceService#getAttachmentCategories()}.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void failedToRetriveListOfAttachmentCategories() throws Exception {

    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);
    when(retrofitService.getAttachmentCategories()).thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<AttachmentCategoryHDto> attachmentCaterories = attachmentReferenceService.getAttachmentCategories();
    Assert.assertNotNull(attachmentCaterories);
    Assert.assertEquals(0, attachmentCaterories.size());
  }

  /**
   * Tests success scenario for {@link AttachmentReferenceService#getAttachmentCategorie(Long)}.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void successfullyGetAttachmentCategorie() throws Exception {
    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAttachmentCategories()).thenReturn(Calls.response(mockAttachmentCategories()));

    final AttachmentCategoryHDto attachmentCaterory = attachmentReferenceService.getAttachmentCategory(1L);
    Assert.assertNotNull(attachmentCaterory);
    Assert.assertEquals(attachmentCaterory.getName(), ATTACHMENT_NAME);
  }

  /**
   * Tests empty result for {@link AssetReferenceService#getAssetCategorie(Long)} when invalid id provided.
   *
   * @throws Exception if mast API fails.
   *
   */
  @Test
  public void emptyResultForGetAssetCategorie() throws Exception {
    AttachmentReferenceRetrofitService retrofitService = context.getBean(AttachmentReferenceRetrofitService.class);
    Mockito.when(retrofitService.getAttachmentCategories())
        .thenReturn(Calls.failure(new IOException("Mock exception")));

    final AttachmentCategoryHDto attachmentCaterory = attachmentReferenceService.getAttachmentCategory(1L);
    Assert.assertNull(attachmentCaterory);
  }

  /**
   * mock list of attachment types.
   *
   * @return list of attachment types.
   */
  private List<AttachmentTypeHDto> mockAttachmentTypes() {
    List<AttachmentTypeHDto> attachments = new ArrayList<>();
    AttachmentTypeHDto attachment = new AttachmentTypeHDto();
    attachment.setId(1L);
    attachment.setName(ATTACHMENT_NAME);
    attachments.add(attachment);
    return attachments;
  }

  /**
   * mock list of attachment categories.
   *
   * @return list of attachment categories.
   */
  private List<AttachmentCategoryHDto> mockAttachmentCategories() {
    List<AttachmentCategoryHDto> attachments = new ArrayList<>();
    AttachmentCategoryHDto attachment = new AttachmentCategoryHDto();
    attachment.setId(1L);
    attachment.setName(ATTACHMENT_NAME);
    attachments.add(attachment);
    return attachments;
  }

  /**
   * Spring in-class configurations.
   *
   * @author syalavarthi.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create {@link AttachmentReferenceService} bean.
     *
     * @return attachment reference service.
     *
     */
    @Bean
    @Override
    public AttachmentReferenceService attachmentReferenceService() {
      return new AttachmentReferenceServiceImpl();
    }
  }
}
