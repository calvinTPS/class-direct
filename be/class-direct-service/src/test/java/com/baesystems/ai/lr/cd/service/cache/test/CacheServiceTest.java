package com.baesystems.ai.lr.cd.service.cache.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.service.cache.CacheService;
import com.baesystems.ai.lr.cd.service.cache.impl.CacheServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

import redis.clients.jedis.Jedis;


/**
 * Cache Factory related unit test.
 *
 * @author yng
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest(CacheServiceImpl.class)
@ContextConfiguration(classes = CacheServiceTest.Config.class)
public class CacheServiceTest {

  /**
   * Injected service.
   */
  @Autowired
  private CacheService cacheService;

  /**
   * flush cache.
   * @throws Exception  value.
   */
  @Test
  public final void testFlushCache() throws Exception {
    final CacheService service = Mockito.spy(new CacheServiceImpl());

    ReflectionTestUtils.setField(service, "redisHostname", "127.0.0.1");
    ReflectionTestUtils.setField(service, "redisPort", "6379");

    // create jedis mock
    final Jedis mockJedis = Mockito.mock(Jedis.class);
    PowerMockito.whenNew(Jedis.class).withAnyArguments().thenReturn(mockJedis);
    PowerMockito.when(mockJedis.ping()).thenReturn("PONG");

    //test for success connection
    PowerMockito.when(mockJedis.isConnected()).thenReturn(true);
    PowerMockito.when(mockJedis.flushAll()).thenReturn("OK");

    StringResponse message = cacheService.flushCache();

    Assert.assertEquals("Flush successful: OK", message.getResponse());

    //test for failed connection
    PowerMockito.when(mockJedis.isConnected()).thenReturn(false);

    message = cacheService.flushCache();

    Assert.assertEquals("Unable to connect to redis server.", message.getResponse());
  }

  /**
   *
   * Spring in-class configurations.
   *
   * @author yng
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * un-mock cache service.
     *
     * @return
     */
    @Override
    @Bean
    public CacheService cacheService() {
      return new CacheServiceImpl();
    }
  }
}
