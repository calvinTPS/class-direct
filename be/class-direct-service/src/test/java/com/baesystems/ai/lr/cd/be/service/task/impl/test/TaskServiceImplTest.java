package com.baesystems.ai.lr.cd.be.service.task.impl.test;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.GroupDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.HierarchyDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.SubgroupDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemModelItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.MastAPIException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.task.impl.TaskServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemModelItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPreviewScheduledServiceDto;
import com.baesystems.ai.lr.enums.ResolutionStatus;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.mock.Calls;

/**
 * Provides unit test methods for {@link TaskService}.
 *
 * @author VKolagutla
 * @author sBollu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TaskServiceImplTest.Config.class)
public class TaskServiceImplTest extends TaskServiceMockTestData {

  /**
   * Logger to print logs for class {@link TaskServiceTest}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(TaskServiceImplTest.class);

  /**
   * The {@link ApplicationContext} from Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link TaskService} from Spring context.
   */
  @Autowired
  private TaskService taskService;

  /**
   * The {@link TaskRetrofitService} from Spring context.
   */
  @Autowired
  private TaskRetrofitService taskRetrofit;

  /**
   * The {@link ServiceReferenceRetrofitService} from Spring context.
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceReference;

  /**
   * The directory test package path.
   */
  private static final String PATH = "data/";

  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * This methods is setup pre-requisite mocking.
   *
   * @throws Exception exception.
   */
  @Before
  public void setUp() throws Exception {
    final CDAuthToken auth = new CDAuthToken("test");
    auth.setPrincipal("101");
    final UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    user.setClientCode("100100");
    user.setShipBuilderCode("100100");
    user.setFlagCode("ALA");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success scenario for {@link TaskService#getTasksByAssetId(Long, Boolean)} with pmsAble
   * filter false.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testTasksByAssetId() throws ClassDirectException, IOException {
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockMixedDataTasks()));


    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, false, null, null);
    Assert.assertEquals("A1", workItemLightDto.getContent().get(0).getReferenceCode());
    Assert.assertEquals("task 1", workItemLightDto.getContent().get(0).getName());
    Assert.assertEquals(null, workItemLightDto.getContent().get(0).getResolutionStatus());
    Assert.assertEquals(null, workItemLightDto.getContent().get(1).getResolutionStatus());
    Assert.assertFalse(workItemLightDto.getContent().get(0).getPmsApplicable());
    Assert.assertTrue(workItemLightDto.getContent().get(1).getPmsApplicable());
  }

  /**
   * Tests success scenario for {@link TaskService#getTasksByAssetId(Long, Boolean)} with pmsAble
   * filter true.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testTasksAndPmsAbleByAssetId() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class))).thenReturn(Calls.response(mockTasks()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, true, null, null);
    Assert.assertNotNull(workItemLightDto);
    // taskList filter
    Assert.assertNotEquals(workItemLightDto.getContent().size(), mockTasks().size());
    Assert.assertTrue(workItemLightDto.getContent().size() == 3L);
    workItemLightDto.getContent().stream().forEach(pmsAble -> Assert.assertTrue(pmsAble.getPmsApplicable()));
  }

  /**
   * Tests success scenario for {@link TaskService#getTasksByAssetId(Long, Boolean)} with pmsAble
   * filter true. For PmsCreditabletasks, resolutionStatus shouldn't have value.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testPmsAbleTasksByAssetIdWithCreditStatusAndResolution() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockTasksWithCreditStatus()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, true, null, null);
    Assert.assertTrue(workItemLightDto.getContent().isEmpty());
  }

  /**
   * Tests success scenario for {@link TaskService#getTasksByAssetId(Long, Boolean)} with pmsAble
   * filter null. Default pmsAble filter is false.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testPmsAbleNullByAssetId() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockNonPmsTasks()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, null, null, null);
    Assert.assertTrue(workItemLightDto.getContent().size() == 2L);
    Assert.assertTrue(workItemLightDto.getContent().get(0).getPmsApplicable());
    Assert.assertFalse(workItemLightDto.getContent().get(1).getPmsApplicable());
  }

  /**
   * Tests success scenario for {@link TaskService#getTasksByAssetId(Long, Boolean)} with pmsAble
   * filter false.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testPmsAbleFalseByAssetId() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockNonPmsTasks()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, false, null, null);
    Assert.assertTrue(workItemLightDto.getContent().size() == 2L);
    Assert.assertTrue(workItemLightDto.getContent().get(0).getPmsApplicable());
    Assert.assertFalse(workItemLightDto.getContent().get(1).getPmsApplicable());
  }


  /**
   * Tests failure scenario for {@link TaskService#getTasksByAssetId(Long, Boolean)}, if nothing
   * found for given identifiers.
   *
   * @throws ClassDirectException if execution fails.
   * @throws RecordNotFoundException if object not found for given unique identifier.
   * @throws IOException if API call fails.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testTasksByAssetIdNull() throws ClassDirectException, RecordNotFoundException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class))).thenReturn(null);

    taskService.getTasksByAssetId(1L, false, null, null);
  }

  /**
   * Tests success scenario for {@link TaskService#calculateDueStatus(WorkItemLightHDto, LocalDate)}
   * , if postPonementDate has value.
   *
   * @throws RecordNotFoundException if object not found for given unique identifier.
   * @throws IOException if API call fails.
   */
  @Test
  public final void calculateDueStatusTest() throws RecordNotFoundException, IOException {
    final TaskRetrofitService retrofitService = context.getBean(TaskRetrofitService.class);
    final List<WorkItemLightHDto> tasks = new ArrayList<>();

    final LocalDate date = LocalDate.now();
    final LocalDate date1 = LocalDate.now().minusDays(2L);
    final LocalDate date3 = LocalDate.now().plusMonths(1L);
    final LocalDate date4 = LocalDate.now().minusMonths(1L);
    final LocalDate date5 = LocalDate.now().plusMonths(1L).minusDays(1L);
    final LocalDate date6 = LocalDate.now().plusMonths(2L);
    final LocalDate date7 = LocalDate.now().plusMonths(4L);
    final LocalDate date8 = LocalDate.now().plusMonths(3L).minusDays(1L);
    final LocalDate date9 = LocalDate.now().plusMonths(3L).plusDays(1L);

    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setPostponementDate(DateUtils.getLocalDateToDate(date));
    tasks.add(task);

    final WorkItemLightHDto service3 = new WorkItemLightHDto();
    service3.setPostponementDate(DateUtils.getLocalDateToDate(date1));
    tasks.add(service3);

    final WorkItemLightHDto service4 = new WorkItemLightHDto();
    service4.setPostponementDate(DateUtils.getLocalDateToDate(date3));
    tasks.add(service4);

    final WorkItemLightHDto service5 = new WorkItemLightHDto();
    service5.setPostponementDate(DateUtils.getLocalDateToDate(date4));
    tasks.add(service5);

    final WorkItemLightHDto service6 = new WorkItemLightHDto();
    service6.setPostponementDate(DateUtils.getLocalDateToDate(date5));
    tasks.add(service6);

    final WorkItemLightHDto service7 = new WorkItemLightHDto();
    service7.setPostponementDate(DateUtils.getLocalDateToDate(date6));
    tasks.add(service7);

    final WorkItemLightHDto service8 = new WorkItemLightHDto();
    service8.setPostponementDate(DateUtils.getLocalDateToDate(date7));
    tasks.add(service8);

    final WorkItemLightHDto service9 = new WorkItemLightHDto();
    service9.setPostponementDate(DateUtils.getLocalDateToDate(date8));
    tasks.add(service9);

    final WorkItemLightHDto service10 = new WorkItemLightHDto();
    service10.setPostponementDate(DateUtils.getLocalDateToDate(date9));
    tasks.add(service10);

    final WorkItemLightHDto service11 = new WorkItemLightHDto();
    service11.setPostponementDate(null);
    tasks.add(service11);

    Mockito.when(retrofitService.getTask(Mockito.any(WorkItemQueryDto.class))).thenReturn(Calls.response(tasks));

    final DueStatus status = taskService.calculateDueStatus(task);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    final DueStatus status3 = taskService.calculateDueStatus(service3);
    Assert.assertEquals(DueStatus.OVER_DUE, status3);

    final DueStatus status4 = taskService.calculateDueStatus(service4);
    Assert.assertEquals(DueStatus.DUE_SOON, status4);

    final DueStatus status5 = taskService.calculateDueStatus(service5);
    Assert.assertEquals(DueStatus.OVER_DUE, status5);

    final DueStatus status6 = taskService.calculateDueStatus(service6);
    Assert.assertEquals(DueStatus.IMMINENT, status6);

    final DueStatus status7 = taskService.calculateDueStatus(service7);
    Assert.assertEquals(DueStatus.DUE_SOON, status7);

    final DueStatus status8 = taskService.calculateDueStatus(service8);
    Assert.assertEquals(DueStatus.NOT_DUE, status8);

    final DueStatus status9 = taskService.calculateDueStatus(service9);
    Assert.assertEquals(DueStatus.DUE_SOON, status9);

    final DueStatus status10 = taskService.calculateDueStatus(service10);
    Assert.assertEquals(DueStatus.NOT_DUE, status10);

    final DueStatus status11 = taskService.calculateDueStatus(service11);
    Assert.assertEquals(DueStatus.NOT_DUE, status11);

  }


  /**
   * Tests success scenario for {@link TaskService#calculateDueStatus(WorkItemLightHDto, LocalDate)}
   * , if postPonementDate has no value.
   *
   * @throws RecordNotFoundException if object not found for given unique identifier.
   * @throws IOException if API call fails.
   */
  @Test
  public final void calculateDueStatusTestNull() throws RecordNotFoundException, IOException {
    final TaskRetrofitService retrofitService = context.getBean(TaskRetrofitService.class);
    final List<WorkItemLightHDto> tasks = new ArrayList<>();

    final LocalDate date = LocalDate.now();
    final LocalDate date1 = LocalDate.now().minusDays(2L);
    final LocalDate date3 = LocalDate.now().plusMonths(1L);
    final LocalDate date4 = LocalDate.now().minusMonths(1L);
    final LocalDate date5 = LocalDate.now().plusMonths(1L).minusDays(1L);
    final LocalDate date6 = LocalDate.now().plusMonths(2L);
    final LocalDate date7 = LocalDate.now().plusMonths(4L);
    final LocalDate date8 = LocalDate.now().plusMonths(3L).minusDays(1L);
    final LocalDate date9 = LocalDate.now().plusMonths(3L).plusDays(1L);

    final WorkItemLightHDto task = new WorkItemLightHDto();
    task.setPostponementDate(null);
    task.setDueDate(DateUtils.getLocalDateToDate(date));
    tasks.add(task);

    final WorkItemLightHDto task3 = new WorkItemLightHDto();
    task3.setPostponementDate(null);
    task3.setDueDate(DateUtils.getLocalDateToDate(date1));
    tasks.add(task3);

    final WorkItemLightHDto task4 = new WorkItemLightHDto();
    task4.setPostponementDate(null);
    task4.setDueDate(DateUtils.getLocalDateToDate(date3));
    tasks.add(task4);

    final WorkItemLightHDto task5 = new WorkItemLightHDto();
    task5.setPostponementDate(null);
    task5.setDueDate(DateUtils.getLocalDateToDate(date4));
    tasks.add(task5);

    final WorkItemLightHDto task6 = new WorkItemLightHDto();
    task6.setPostponementDate(null);
    task6.setDueDate(DateUtils.getLocalDateToDate(date5));
    tasks.add(task6);

    final WorkItemLightHDto task7 = new WorkItemLightHDto();
    task7.setPostponementDate(null);
    task7.setDueDate(DateUtils.getLocalDateToDate(date6));
    tasks.add(task7);

    final WorkItemLightHDto task8 = new WorkItemLightHDto();
    task8.setPostponementDate(null);
    task8.setDueDate(DateUtils.getLocalDateToDate(date7));
    tasks.add(task8);

    final WorkItemLightHDto task9 = new WorkItemLightHDto();
    task9.setPostponementDate(null);
    task9.setDueDate(DateUtils.getLocalDateToDate(date8));
    tasks.add(task9);

    final WorkItemLightHDto task10 = new WorkItemLightHDto();
    task10.setPostponementDate(null);
    task10.setDueDate(DateUtils.getLocalDateToDate(date9));
    tasks.add(task10);

    Mockito.when(retrofitService.getTask(Mockito.any(WorkItemQueryDto.class))).thenReturn(Calls.response(tasks));

    final DueStatus status = taskService.calculateDueStatus(task);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    final DueStatus status3 = taskService.calculateDueStatus(task3);
    Assert.assertEquals(DueStatus.OVER_DUE, status3);

    final DueStatus status4 = taskService.calculateDueStatus(task4);
    Assert.assertEquals(DueStatus.DUE_SOON, status4);

    final DueStatus status5 = taskService.calculateDueStatus(task5);
    Assert.assertEquals(DueStatus.OVER_DUE, status5);

    final DueStatus status6 = taskService.calculateDueStatus(task6);
    Assert.assertEquals(DueStatus.IMMINENT, status6);

    final DueStatus status7 = taskService.calculateDueStatus(task7);
    Assert.assertEquals(DueStatus.DUE_SOON, status7);

    final DueStatus status8 = taskService.calculateDueStatus(task8);
    Assert.assertEquals(DueStatus.NOT_DUE, status8);

    final DueStatus status9 = taskService.calculateDueStatus(task9);
    Assert.assertEquals(DueStatus.DUE_SOON, status9);

    final DueStatus status10 = taskService.calculateDueStatus(task10);
    Assert.assertEquals(DueStatus.NOT_DUE, status10);

  }

  /**
   * Tests continuousServices filter success scenario for
   * {@link TaskService#getTasksByService(Long, Long, Integer, Integer)}.
   *
   * @throws ClassDirectException if any exception occurred during execution.
   * @throws IOException if mast API call fails.
   */
  @Test
  public final void testTasksByService() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockTaskshasContinuousServices()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByService(1L, 1L, null, null);
    // taskList filter
    Assert.assertNotEquals(workItemLightDto.getContent().size(), mockTaskshasContinuousServices().size());
    Assert.assertTrue(workItemLightDto.getContent().size() == 6L);
  }

  /**
   * Tests to fetch all task for given list of services, see
   * {@link TaskService#getTasksForServices(List)}.
   *
   * @throws ClassDirectException if error on external api to fetch tasks.
   */
  @Test
  public final void testGetTasksForServices() throws ClassDirectException {
    final List<ScheduledServiceHDto> serviceList = new ArrayList<>(2);
    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setId(1L);
    serviceList.add(service1);
    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    service2.setId(2L);
    serviceList.add(service2);

    Mockito.when((taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class))))
        .thenReturn(Calls.response(mockTasksForMultiServices()));

    final Map<Long, List<WorkItemLightHDto>> actualResult = taskService.getTasksForServices(serviceList);

    Assert.assertNotNull(actualResult);
    // Service 1 should have 2 tasks
    final List<WorkItemLightHDto> svc1TaskList = actualResult.get(service1.getId());
    Assert.assertNotNull(svc1TaskList);
    Assert.assertEquals(2, svc1TaskList.size());

    // Service 2 should have 1 tasks
    final List<WorkItemLightHDto> svc2TaskList = actualResult.get(service2.getId());
    Assert.assertNotNull(svc2TaskList);
    Assert.assertEquals(1, svc2TaskList.size());
  }

  /**
   * Tests to fetch all task with external api error, see
   * {@link TaskService#getTasksForServices(List)}.
   *
   * @throws ClassDirectException if error on external api to fetch tasks.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetTasksForServicesWithApiError() throws ClassDirectException {
    final List<ScheduledServiceHDto> serviceList = new ArrayList<>(2);
    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setId(1L);
    serviceList.add(service1);
    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    service2.setId(2L);
    serviceList.add(service2);

    Mockito.when((taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class))))
        .thenReturn(Calls.failure(new IOException("Mock error.")));

    taskService.getTasksForServices(serviceList);

  }

  /**
   * Tests to fetch all task with a null service list, see
   * {@link TaskService#getTasksForServices(List)}.
   *
   * @throws ClassDirectException if error on external api to fetch tasks.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetTasksForServicesWithInvalidServices() throws ClassDirectException {
    final List<ScheduledServiceHDto> serviceList = null;

    Mockito.when((taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class))))
        .thenReturn(Calls.response(mockTasksForMultiServices()));

    taskService.getTasksForServices(serviceList);

  }

  /**
   * Tests non ContinuousServices success scenario for
   * {@link TaskService#getTasksByService(Long, Long, Integer, Integer)}.
   *
   * @throws ClassDirectException if any exception occurred during execution.
   * @throws IOException if mast API call fails.
   */
  @Test
  public final void shouldReturnNonFilterServices() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockNonContinuousServices()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByService(2L, 1L, null, null);
    Assert.assertEquals(workItemLightDto.getContent().size(), mockNonContinuousServices().size());
    Assert.assertTrue(workItemLightDto.getContent().size() == 3L);
  }


  /**
   * Tests success scenario for {@link TaskService#getCheckListByService(Long, Integer, Integer)}.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testCheckListByService() throws ClassDirectException, IOException {

    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockUnSortedChecklistData()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getCheckListByService(1L, null, null);
    Assert.assertEquals("A1", workItemLightDto.getContent().get(0).getReferenceCode());
    Assert.assertEquals("000001-XXXX-000004", workItemLightDto.getContent().get(0).getTaskNumber());
  }

  /**
   * Tests success scenario {@link TaskService#updateTasksAsList(WorkItemLightListHDto)}.
   *
   * @throws ClassDirectException if execution fails or API call fails.
   */
  @Test
  public final void testUpdateTasksAsList() throws ClassDirectException {

    final Date currentDate = DateUtils.getLocalDateToDate(LocalDate.now());

    WorkItemLightListHDto workItemLightList = new WorkItemLightListHDto();
    List<WorkItemLightHDto> workItemLightDtos = new ArrayList<>();
    WorkItemLightHDto workItemLightDto = new WorkItemLightHDto();
    workItemLightDto.setId(1L);
    workItemLightDto.setUpdatedBy("LONLBZ");
    workItemLightDto.setUpdatedDate(currentDate);
    workItemLightDto.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto);

    WorkItemLightHDto workItemLightDto1 = new WorkItemLightHDto();
    workItemLightDto1.setId(2L);
    workItemLightDto1.setUpdatedBy("LONLBZ");
    workItemLightDto1.setUpdatedDate(currentDate);
    workItemLightDto1.setPmsCreditDate(currentDate);
    workItemLightDtos.add(workItemLightDto1);

    workItemLightList.setTasks(workItemLightDtos);

    Mockito.when(taskRetrofit.updateTaskList(Mockito.any(WorkItemLightListDto.class)))
        .thenReturn(Calls.response(workItemList()));
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(new UserProfiles());
    // WorkItemLightListHDto
    WorkItemLightListHDto lightListDto = taskService.updateTasksAsList(workItemLightList);
    Assert.assertEquals(workItemLightList.getTasks().get(0).getUpdatedBy(),
        lightListDto.getTasks().get(0).getUpdatedBy());
    Assert.assertEquals(currentDate, lightListDto.getTasks().get(0).getPmsCreditDate());
    Assert.assertEquals(currentDate, lightListDto.getTasks().get(1).getPmsCreditDate());
  }


  /**
   * Tests success scenario to set pmsCreditDate to futureDate
   * {@link TaskService#updateTasksAsList(WorkItemLightListHDto)}.
   *
   * @throws ClassDirectException if execution fails or API call fails.
   */
  @Test
  public final void testUpdateTasksAsListforFutureDate() throws ClassDirectException {

    final Date currentDate = DateUtils.getLocalDateToDate(LocalDate.now());
    final Date futureDate = DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(1L));

    WorkItemLightListHDto workItemLightList = new WorkItemLightListHDto();
    List<WorkItemLightHDto> workItemLightHDtos = new ArrayList<>();
    WorkItemLightHDto workItemLightHDto = new WorkItemLightHDto();
    workItemLightHDto.setId(1L);
    workItemLightHDto.setUpdatedBy("LONLBZ");
    workItemLightHDto.setUpdatedDate(currentDate);
    workItemLightHDto.setPmsCreditDate(futureDate);
    workItemLightHDtos.add(workItemLightHDto);

    WorkItemLightHDto workItemLightHDto1 = new WorkItemLightHDto();
    workItemLightHDto1.setId(2L);
    workItemLightHDto1.setUpdatedBy("LONLBZ");
    workItemLightHDto1.setUpdatedDate(currentDate);
    workItemLightHDto1.setPmsCreditDate(futureDate);
    workItemLightHDtos.add(workItemLightHDto1);

    workItemLightList.setTasks(workItemLightHDtos);


    WorkItemLightListDto workItemLightListDto = new WorkItemLightListDto();
    List<WorkItemLightDto> workItemLightDtos = new ArrayList<>();
    WorkItemLightDto workItemLightDto = new WorkItemLightDto();
    workItemLightDto.setId(1L);
    workItemLightDto.setUpdatedBy("LONLBZ");
    workItemLightDto.setUpdatedDate(currentDate);
    workItemLightDto.setPmsCreditDate(futureDate);
    workItemLightDtos.add(workItemLightDto);

    WorkItemLightDto workItemLightDto1 = new WorkItemLightDto();
    workItemLightDto1.setId(2L);
    workItemLightDto1.setUpdatedBy("LONLBZ");
    workItemLightDto1.setUpdatedDate(currentDate);
    workItemLightDto1.setPmsCreditDate(futureDate);
    workItemLightDtos.add(workItemLightDto1);
    workItemLightListDto.setTasks(workItemLightDtos);

    workItemLightListDto.setTasks(workItemLightDtos);

    Mockito.when(taskRetrofit.updateTaskList(Mockito.any(WorkItemLightListDto.class)))
        .thenReturn(Calls.response(workItemLightListDto));
    when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUser());
    // WorkItemLightListHDto
    WorkItemLightListHDto lightListDto = taskService.updateTasksAsList(workItemLightList);
    Assert.assertEquals(workItemLightList.getTasks().get(0).getUpdatedBy(),
        lightListDto.getTasks().get(0).getUpdatedBy());
    Assert.assertEquals(futureDate, lightListDto.getTasks().get(0).getPmsCreditDate());
    Assert.assertEquals(futureDate, lightListDto.getTasks().get(1).getPmsCreditDate());
  }

  /**
   * Returns mock user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("LR_ADMIN");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }

  /**
   * Tests success scenario of throwing proper exception for
   * {@link TaskService#updateTasksAsList(WorkItemLightListHDto)} .
   */
  @Test
  public final void testUpdateTasksAsListErrorCode() {
    LocalDate date = LocalDate.now();
    WorkItemLightListHDto workItemLightList = new WorkItemLightListHDto();
    List<WorkItemLightHDto> workItemLightDtos = new ArrayList<>();
    WorkItemLightHDto workItemLightDto = new WorkItemLightHDto();
    workItemLightDto.setId(1L);
    workItemLightDto.setUpdatedBy("LONLBZ");
    workItemLightDto.setUpdatedDate(DateUtils.getLocalDateToDate(date));
    workItemLightDto.setPmsCreditDate(DateUtils.getLocalDateToDate(date));
    workItemLightDtos.add(workItemLightDto);

    WorkItemLightHDto workItemLightDto1 = new WorkItemLightHDto();
    workItemLightDto1.setId(2L);
    workItemLightDto1.setUpdatedBy("LONLBZ");
    workItemLightDto1.setUpdatedDate(DateUtils.getLocalDateToDate(date));
    workItemLightDto1.setPmsCreditDate(DateUtils.getLocalDateToDate(date));
    workItemLightDtos.add(workItemLightDto1);
    workItemLightList.setTasks(workItemLightDtos);

    Mockito.when(taskRetrofit.updateTaskList(Mockito.any(WorkItemLightListDto.class)))
        .thenReturn(Calls.failure(new IOException("Bad request")));
    try {
      WorkItemLightListHDto workItemLightListHDto = taskService.updateTasksAsList(workItemLightList);
    } catch (ClassDirectException exception) {
      Assert.assertEquals(0, exception.getErrorCode());
      Assert.assertNotNull(exception.getMessage());
    }

  }

  /**
   * Tests success scenario of throwing proper error code for
   * {@link TaskService#updateTasksAsList(WorkItemLightListHDto)} .
   */
  @Test
  public final void testUpdateTasksAsListMastErrorCode() {
    LocalDate date = LocalDate.now();
    WorkItemLightListHDto workItemLightList = new WorkItemLightListHDto();
    List<WorkItemLightHDto> workItemLightDtos = new ArrayList<>();
    WorkItemLightHDto workItemLightDto = new WorkItemLightHDto();
    workItemLightDto.setId(1L);
    workItemLightDto.setUpdatedBy("LONLBZ");
    workItemLightDto.setUpdatedDate(DateUtils.getLocalDateToDate(date));
    workItemLightDto.setPmsCreditDate(DateUtils.getLocalDateToDate(date));
    workItemLightDtos.add(workItemLightDto);

    WorkItemLightHDto workItemLightDto1 = new WorkItemLightHDto();
    workItemLightDto1.setId(2L);
    workItemLightDto1.setUpdatedBy("LONLBZ");
    workItemLightDto1.setUpdatedDate(DateUtils.getLocalDateToDate(date));
    workItemLightDto1.setPmsCreditDate(DateUtils.getLocalDateToDate(date));
    workItemLightDtos.add(workItemLightDto1);
    workItemLightList.setTasks(workItemLightDtos);

    Mockito.when(taskRetrofit.updateTaskList(Mockito.any(WorkItemLightListDto.class)))
        .thenReturn(Calls.failure(new MastAPIException(409, "Bad request")));
    try {
      WorkItemLightListHDto workItemLightListHDto = taskService.updateTasksAsList(workItemLightList);
    } catch (ClassDirectException exception) {
      Assert.assertEquals(409, exception.getErrorCode());
      Assert.assertNotNull(exception.getMessage());
    }
  }


  /**
   * Tests success scenario of continuousServices filter for
   * {@link TaskService#getTasksByAssetId(Long, Boolean)}. Filter is as follows,
   * <ol>
   * <li>tasks notStarted</li>
   * <li>tasks postponed, notCredited</li>
   * <li>tasks pms'ed, notCredited</li>
   * </ol>
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testTasksByAssetIdforContinuous() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockTaskshasContinuousServices()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, false, null, null);
    Assert.assertNotEquals(workItemLightDto.getContent().size(), mockTaskshasContinuousServices().size());
    Assert.assertTrue(workItemLightDto.getContent().size() == 6L);
  }

  /**
   * Tests success scenario of non continuousServices tasks for
   * {@link TaskService#getTasksByAssetId(Long, Boolean)}.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testTasksByAssetIdforNonContinuous() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockNonContinuousServices()));
    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, false, null, null);
    Assert.assertEquals(workItemLightDto.getContent().size(), mockNonContinuousServices().size());
    Assert.assertTrue(workItemLightDto.getContent().size() == 3L);
  }


  /**
   * Tests success scenario of continuousServices filter for
   * {@link TaskService#getTasksByAssetId(Long, Boolean)}. Filter is as follows,
   * <li>Exclude Continuous Services tasks with resolution status W, X, C.</li>
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testTasksByAssetIdforContinuousExcludedScenario() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockContinuousServices()));
    final List<WorkItemLightHDto> workItemLightDto1 = taskService.getTasksByAssetId(1L, false, null, null).getContent();
    Assert.assertNotNull(workItemLightDto1);
    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, false, null, null);
    Assert.assertTrue(workItemLightDto.getContent().isEmpty());
  }

  /**
   * Tests failure scenario for {@link TaskService#getTasksByAssetId(Long, Boolean)}.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testTasksByAssetIdFail() throws ClassDirectException, IOException {
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.failure(new IOException()));
    final List<WorkItemLightHDto> workItemLightDto1 = taskService.getTasksByAssetId(1L, false, null, null).getContent();
    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByAssetId(1L, false, null, null);
  }

  /**
   * Tests failure scenario for {@link TaskService#getCheckListByService(Long, Integer, Integer)}.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test(expected = IOException.class)
  public final void testCheckListByServiceThrowsException() throws ClassDirectException, IOException {

    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.failure(new IOException()));

    final List<WorkItemLightHDto> workItemLightDto1 = taskService.getCheckListByService(1L, null, null).getContent();
    Assert.assertNotNull(workItemLightDto1);

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getCheckListByService(1L, null, null);
  }

  /**
   * Tests failure scenario for {@link TaskService#getTasksByService(Long, Long, Integer, Integer)}.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testTaskListByServiceThrowsException() throws ClassDirectException, IOException {

    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.failure(new IOException()));

    final PageResource<WorkItemLightHDto> workItemLightDto = taskService.getTasksByService(1L, 1L, null, null);
  }

  /**
   * Returns mock hierarchical Non Continuous services.
   *
   * @return list of mock services.
   * @throws JsonParseException if json parse fails.
   * @throws JsonMappingException if json mapping fails.
   * @throws IOException if coping of stream fails.
   */
  public List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> mockNonContinuousScheduledService()
      throws JsonParseException, JsonMappingException, IOException {
    List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> serviceList = new ArrayList<>();
    serviceList.add(returnObject("Service"));
    return serviceList;
  }

  /**
   * Returns mock hierarchical Continuous services.
   *
   * @return list of mock services.
   * @throws JsonParseException if json parse fails.
   * @throws JsonMappingException if json mapping fails.
   * @throws IOException if coping of stream fails.
   */
  public List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> mockContinuousScheduledService()
      throws JsonParseException, JsonMappingException, IOException {
    List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> serviceList = new ArrayList<>();
    serviceList.add(returnObject("ContinuousService"));
    return serviceList;
  }

  /**
   * Returns mock hierarchical Non Continuous services.
   *
   * @return list of mock services.
   * @throws JsonParseException if json parse fails.
   * @throws JsonMappingException if json mapping fails.
   * @throws IOException if coping of stream fails.
   */
  public List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> mockCSSWithCompletedResolutionStatus()
      throws JsonParseException, JsonMappingException, IOException {
    List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> serviceList = new ArrayList<>();
    serviceList.add(returnObject("ContinuousService-CompletedResStatus"));
    return serviceList;
  }

  /**
   * Returns mock hierarchical Non Continuous services has PMS tasks.
   *
   * @return list of mock services.
   * @throws JsonParseException if json parse fails.
   * @throws JsonMappingException if json mapping fails.
   * @throws IOException if coping of stream fails.
   */
  public List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> mockPMSTaskService()
      throws JsonParseException, JsonMappingException, IOException {
    List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> serviceList = new ArrayList<>();
    serviceList.add(returnObject("PMSTask-Service"));
    return serviceList;
  }

  /**
   * Returns mock hierarchical Continuous services has PMS tasks.
   *
   * @return list of mock services.
   * @throws JsonParseException if json parse fails.
   * @throws JsonMappingException if json mapping fails.
   * @throws IOException if coping of stream fails.
   */
  public List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> mockPMSTaskContinuousService()
      throws JsonParseException, JsonMappingException, IOException {
    List<WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto>> serviceList = new ArrayList<>();
    serviceList.add(returnObject("PMSTask-ContinuousService"));
    return serviceList;
  }

  /**
   * Converts json to object.
   *
   * @param jsonName the json file name.
   * @return mapped object.
   * @throws IOException if coping of stream fails.
   */
  public WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto> returnObject(final String jsonName)
      throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto> content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(PATH + jsonName + ".json"), writer,
        Charset.defaultCharset());
    JavaType type = mapper.getTypeFactory().constructParametrizedType(WorkItemPreviewScheduledServiceDto.class,
        WorkItemPreviewScheduledServiceDto.class, WorkItemModelItemDto.class);
    content = mapper.readValue(writer.toString(), type);
    return content;

  }

  /**
   * Tests success of non continuousServices tasks scenario for
   * {@link TaskService#getHierarchicalTasksByService(Long, Long)}. No filter applied for non
   * continuousServices tasks,we will get data as it is with hydration.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testHierarchicalNonContinuousServiceTasksByService() throws ClassDirectException, IOException {
    WorkItemPreviewListHDto listDto = new WorkItemPreviewListHDto();
    // add non continuous service.
    listDto.setServices(mockNonContinuousScheduledService());
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getHierarchicalTask(Mockito.any(WorkItemQueryDto.class), Mockito.anyBoolean()))
        .thenReturn(Calls.response(listDto));

    final WorkItemPreviewListHDto hierarchialTasks = taskService.getHierarchicalTasksByService(1L, 1L);
    // Actual services will be null after process.
    Assert.assertTrue(hierarchialTasks.getServices() == null);
    // Hydrated services will have value after process.
    Assert.assertTrue(hierarchialTasks.getServicesH() != null);
    // Verification of Tasks,whether expected tasks and actual returned tasks are same or not.
    // Total expected tasks from mast API.
    List<WorkItemLightDto> expectedTasks = returnActualTaskList(listDto);
    // actual hydrated tasks returned from {@link
    // TaskService#getHierarchicalTasksByService(Long, Long)}
    List<WorkItemLightHDto> actualTasks = returnExpectedTaskList(hierarchialTasks);
    // actual tasks and expected tasks must be same.None of the filters applied.
    Assert.assertTrue(actualTasks.containsAll(expectedTasks));
    // Verification of Items.
    // Total expected Items from mast API.
    List<WorkItemModelItemDto> expectedItems = returnActualItemList(listDto);
    // actual hydrated Items returned from {@link
    // TaskService#getHierarchicalTasksByService(Long, Long)}
    List<WorkItemModelItemHDto> actualItems = returnExpectedItemList(hierarchialTasks);
    // actual Items and expected Items must be same.
    Assert.assertTrue(actualItems.containsAll(expectedItems));
  }

  /**
   * Tests success of non continuousServices tasks scenario for
   * {@link TaskService#getHierarchicalTasksByServiceIds(List, Long)}. No filter applied for non
   * continuousServices tasks,we will get data as it is with hydration.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testHierarchicalNonContinuousServiceTasksByServiceIds() throws ClassDirectException, IOException {
    WorkItemPreviewListHDto listDto = new WorkItemPreviewListHDto();
    // add non continuous service.
    listDto.setServices(mockNonContinuousScheduledService());
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getHierarchicalTask(Mockito.any(WorkItemQueryDto.class), Mockito.anyBoolean()))
        .thenReturn(Calls.response(listDto));

    List<Long> serviceIds = Arrays.asList(1L, 2L, 3L);
    final WorkItemPreviewListHDto hierarchialTasks = taskService.getHierarchicalTasksByServiceIds(serviceIds, 1L);
    // Actual services will be null after process.
    Assert.assertTrue(hierarchialTasks.getServices() == null);
    // Hydrated services will have value after process.
    Assert.assertTrue(hierarchialTasks.getServicesH() != null);
    // Verification of Tasks,whether expected tasks and actual returned tasks are same or not.
    // Total expected tasks from mast API.
    List<WorkItemLightDto> expectedTasks = returnActualTaskList(listDto);
    // actual hydrated tasks returned from {@link
    // TaskService#getHierarchicalTasksByService(Long, Long)}
    List<WorkItemLightHDto> actualTasks = returnExpectedTaskList(hierarchialTasks);
    // actual tasks and expected tasks must be same.None of the filters applied.
    Assert.assertTrue(actualTasks.containsAll(expectedTasks));
    // Verification of Items.
    // Total expected Items from mast API.
    List<WorkItemModelItemDto> expectedItems = returnActualItemList(listDto);
    // actual hydrated Items returned from {@link
    // TaskService#getHierarchicalTasksByService(Long, Long)}
    List<WorkItemModelItemHDto> actualItems = returnExpectedItemList(hierarchialTasks);
    // actual Items and expected Items must be same.
    Assert.assertTrue(actualItems.containsAll(expectedItems));
  }

  /**
   * Tests success continuousServices tasks filter scenario for
   * {@link TaskService#getHierarchicalTasksByService(Long, Long)}.
   * <ol>
   * Filter is as follows,
   * <li>tasks notStarted</li>
   * <li>tasks postponed, notCredited</li>
   * <li>tasks pms'ed, notCredited</li>
   * </ol>
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void testHierarchicalContinuousServiceTasksByService() throws ClassDirectException, IOException {
    WorkItemPreviewListHDto listDto = new WorkItemPreviewListHDto();
    // add service which has continuous indicator.
    listDto.setServices(mockContinuousScheduledService());
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getHierarchicalTask(Mockito.any(WorkItemQueryDto.class), Mockito.anyBoolean()))
        .thenReturn(Calls.response(listDto));

    final WorkItemPreviewListHDto hierarchialTasks = taskService.getHierarchicalTasksByService(1L, 1L);
    // Actual services will be null after process.
    Assert.assertTrue(hierarchialTasks.getServices() == null);
    // Hydrated services will have value after process.
    Assert.assertTrue(hierarchialTasks.getServicesH() != null);
    // Total expected tasks from mast API.
    List<WorkItemLightDto> expectedTasks = returnActualTaskList(listDto);
    // expected continuous tasks(tasks has service with continuous indicator true) after applying
    // filter to expected tasks from mast API.
    List<WorkItemLightDto> expectedContinuousTasks = expectedTasks.stream().filter(task -> {
      // exclude resolutionStatus(Waived (W),ConfirmatoryCheck (C),Complete(X))
      boolean excludedWCX = Optional.ofNullable(task.getResolutionStatus()).map(LinkResource::getId)
          .map(id -> ResolutionStatus.WAIVED.getValue().equals(id)
              || ResolutionStatus.CONFIRMATORY_CHECK.getValue().equals(id)
              || ResolutionStatus.COMPLETE.getValue().equals(id))
          .orElse(false);
      return (task.getResolutionStatus() == null && task.getPmsCreditDate() == null)
          || (task.getResolutionStatus() != null
              && task.getResolutionStatus().getId().longValue() == ResolutionStatus.POSTPONED.getValue()
              && task.getPmsCreditDate() == null)
          || (task.getPmsApplicable().booleanValue() == Boolean.TRUE && task.getPmsCreditDate() == null)
              && !excludedWCX;
    }).collect(Collectors.toList());
    // actual hydrated continuous tasks returned from {@link
    // TaskService#getHierarchicalTasksByService(Long, Long)}
    List<WorkItemLightHDto> actualContinuousTasks = returnExpectedTaskList(hierarchialTasks);
    // expected and actual should be same.
    Assert.assertTrue(expectedContinuousTasks.containsAll(actualContinuousTasks));
  }

  /**
   * Tests success continuousServices tasks filter scenario for
   * {@link TaskService#getHierarchicalTasksByService(Long, Long)}. Tests If the result Items and
   * Tasks are null or empty,Then there will not be a next level items.
   *
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void shouldRemoveNullItemsAndTasksInSecondLevelItemsList() throws ClassDirectException, IOException {
    WorkItemPreviewListHDto listDto = new WorkItemPreviewListHDto();
    // add service which has continuous indicator.
    listDto.setServices(mockCSSWithCompletedResolutionStatus());
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getHierarchicalTask(Mockito.any(WorkItemQueryDto.class), Mockito.anyBoolean()))
        .thenReturn(Calls.response(listDto));

    final WorkItemPreviewListHDto hierarchialTasks = taskService.getHierarchicalTasksByService(1L, 1L);
    // Actual services will be null after process.
    Assert.assertTrue(hierarchialTasks.getServices() == null);
    // Hydrated services will have value after process.
    Assert.assertTrue(hierarchialTasks.getServicesH() != null);
    // next level items is empty after the process(we apply continuous service filter)
    Assert.assertTrue(hierarchialTasks.getServicesH().get(0).getItemsH().get(0).getItemsH().isEmpty());
  }

  /**
   * Tests Mast API Call failure scenario for
   * {@link TaskService#getHierarchicalTasksByService(Long, Long)}.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testHierarchicalTasksByServiceFailure() throws ClassDirectException, IOException {

    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getHierarchicalTask(Mockito.any(WorkItemQueryDto.class), Mockito.anyBoolean()))
        .thenReturn(Calls.failure(new IOException()));

    final WorkItemPreviewListHDto hierarchialTasks = taskService.getHierarchicalTasksByService(1L, 1L);
  }

  /**
   * Tests success scenario for {@link TaskService#getHierarchicalTasksByAssetId(Long, Boolean)},
   * non continuous services with pmsAble filter true.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void shouldReturnOnlyPMSTasksForAssetId() throws ClassDirectException, IOException {
    WorkItemPreviewListHDto listDto = new WorkItemPreviewListHDto();
    // add non continuous service.
    listDto.setServices(mockPMSTaskService());
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getHierarchicalTask(Mockito.any(WorkItemQueryDto.class), Mockito.anyBoolean()))
        .thenReturn(Calls.response(listDto));

    final WorkItemPreviewListHDto hierarchialTasks = taskService.getHierarchicalTasksByAssetId(1L, true);
    // Actual services will be null after process.
    Assert.assertTrue(hierarchialTasks.getServices() == null);
    // Hydrated services will have value after process.
    Assert.assertTrue(hierarchialTasks.getServicesH() != null);
    // Verification of Tasks,whether expected tasks and actual returned tasks are same or not.
    // Total expected tasks from mast API.
    List<WorkItemLightDto> expectedTasks = returnActualTaskList(listDto);
    // expected PMS tasks from mast API.
    List<WorkItemLightDto> pmsTasks =
        expectedTasks.stream().filter(task -> task.getPmsApplicable()).collect(Collectors.toList());
    // actual PMS tasks returned from {@link
    // TaskService#getHierarchicalTasksByAssetId(Long, Boolean)}
    List<WorkItemLightHDto> actualTasks = returnExpectedTaskList(hierarchialTasks);
    // PMS Applicable is true for all PMS tasks.
    actualTasks.stream().forEach(task -> Assert.assertTrue(task.getPmsApplicable()));
    // Ids of PMS tasks and expected tasks must be same.
    Assert.assertTrue(pmsTasks.get(0).getId() == actualTasks.get(0).getId());
    // Verification of Items,Whether Items doesn't have PMS tasks are filtered or not.
    // Total expected items from mast API.
    List<WorkItemModelItemDto> expectedItems = returnActualItemList(listDto);
    // Items has PMS tasks
    List<WorkItemModelItemDto> pmsItems = new ArrayList<>();
    for (WorkItemModelItemDto pmsItem : expectedItems) {
      if (pmsItem.getTasks() != null) {
        for (WorkItemLightDto pTask : pmsItem.getTasks()) {
          if (pTask.getPmsApplicable()) {
            pmsItems.add(pmsItem);
          }
        }
      }
    }
    // actual hydrated items which has PMS tasks returned from {@link
    // TaskService#getHierarchicalTasksByAssetId(Long, Boolean)}.
    List<WorkItemModelItemHDto> actualItems = returnExpectedItemList(hierarchialTasks);
    // actualItems should be same with pmsItems(items has PMS tasks from mast API)
    Assert.assertTrue(actualItems.containsAll(pmsItems));
  }

  /**
   * Tests continuousServices filter success scenario for
   * {@link TaskService#getHierarchicalTasksByAssetId(Long, Boolean)} with pmsAble filter true.
   *
   * @throws ClassDirectException if execution fails.
   * @throws IOException if API call fails.
   */
  @Test
  public final void shouldReturnOnlyPMSTasksForAssetIdWhichHasContinuousService()
      throws ClassDirectException, IOException {
    WorkItemPreviewListHDto listDto = new WorkItemPreviewListHDto();
    // add non continuous service.
    listDto.setServices(mockPMSTaskContinuousService());
    Mockito.when(serviceReference.getAssetServices(Mockito.anyLong())).thenReturn(Calls.response(mockServices()));
    Mockito.when(taskRetrofit.getHierarchicalTask(Mockito.any(WorkItemQueryDto.class), Mockito.anyBoolean()))
        .thenReturn(Calls.response(listDto));

    final WorkItemPreviewListHDto hierarchialTasks = taskService.getHierarchicalTasksByAssetId(1L, true);
    // Actual services will be null after process.
    Assert.assertTrue(hierarchialTasks.getServices() == null);
    // Hydrated services will have value after process.
    Assert.assertTrue(hierarchialTasks.getServicesH() != null);
    // Verification of Tasks,whether expected PMS tasks and actual returned PMS tasks are same or
    // not.
    // Total expected tasks from mast API.
    List<WorkItemLightDto> expectedTasks = returnActualTaskList(listDto);
    // Total expected PMS tasks from mast API.
    List<WorkItemLightDto> expectedPMSTasks =
        expectedTasks.stream().filter(task -> task.getPmsApplicable()).collect(Collectors.toList());
    // actual PMS tasks returned from {@link
    // TaskService#getHierarchicalTasksByAssetId(Long, Boolean)}.
    List<WorkItemLightHDto> actualTasks = returnExpectedTaskList(hierarchialTasks);
    // PMS Applicable is true for all PMS tasks.
    actualTasks.stream().forEach(task -> Assert.assertTrue(task.getPmsApplicable()));
    Assert.assertTrue(expectedPMSTasks.containsAll(actualTasks));
    // Verification of Items,Whether Items doesn't have PMS tasks are filtered or not.
    // Actual Items from mast.
    List<WorkItemModelItemDto> expectedItems = returnActualItemList(listDto);
    // Items has PMS tasks
    List<WorkItemModelItemDto> pmsItems = new ArrayList<>();
    for (WorkItemModelItemDto pmsItem : expectedItems) {
      if (pmsItem.getTasks() != null) {
        for (WorkItemLightDto pTask : pmsItem.getTasks()) {
          if (pTask.getPmsApplicable()) {
            pmsItems.add(pmsItem);
          }
        }
      }
    }

    // actual hydrated items which has PMS tasks returned from {@link
    // TaskService#getHierarchicalTasksByAssetId(Long, Boolean)}.
    List<WorkItemModelItemHDto> actualItems = returnExpectedItemList(hierarchialTasks);
    // actualItems should be same with pmsItems(items has PMS tasks from mast API)
    Assert.assertTrue(actualItems.containsAll(pmsItems));
  }

  /**
   * Returns actual simple list of tasks from mast by removing hierarchy through recursive process.
   *
   * @param actualServiceList the actual service list from mast.
   * @return list of tasks.
   */
  private List<WorkItemLightDto> returnActualTaskList(final WorkItemPreviewListHDto actualServiceList) {
    List<WorkItemLightDto> actualTasks = new ArrayList<>();

    for (WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto> serviceList : actualServiceList.getServices()) {
      for (WorkItemModelItemDto item : serviceList.getItems()) {
        recursiveActualTask(item, actualTasks);
      }
    }
    return actualTasks;
  }

  /**
   * Collects list of tasks from mast by removing hierarchy through recursive process.
   *
   * @param actualItem the item used for recursive process.
   * @param actualList the total list of tasks (with no hierarchy).
   */
  private void recursiveActualTask(final WorkItemModelItemDto actualItem, final List<WorkItemLightDto> actualList) {
    for (WorkItemModelItemDto item : actualItem.getItems()) {
      recursiveActualTask(item, actualList);
    }
    if (actualItem.getTasks() != null) {
      actualList.addAll(actualItem.getTasks());
    }
  }

  /**
   * Returns list of hydrated tasks by removing hierarchy through recursive process.
   *
   * @param expectedServiceList the result service list.
   * @return the list of hydrated tasks (No hierarchy).
   */
  private List<WorkItemLightHDto> returnExpectedTaskList(final WorkItemPreviewListHDto expectedServiceList) {
    List<WorkItemLightHDto> expectedTasks = new ArrayList<>();

    for (WorkItemPreviewScheduledServiceHDto serviceList : expectedServiceList.getServicesH()) {
      for (WorkItemModelItemHDto item : serviceList.getItemsH()) {
        recursiveExpectedTask(item, expectedTasks);
      }
    }
    return expectedTasks;
  }

  /**
   * Collects list of hydrated tasks by removing hierarchy through recursive process.
   *
   * @param expectedItem the item used for recursive process.
   * @param expectedList the total list of hydrated tasks (with no hierarchy)
   */
  private void recursiveExpectedTask(final WorkItemModelItemHDto expectedItem,
      final List<WorkItemLightHDto> expectedList) {
    for (WorkItemModelItemHDto item : expectedItem.getItemsH()) {
      recursiveExpectedTask(item, expectedList);
    }
    if (expectedItem.getTasksH() != null) {
      expectedList.addAll(expectedItem.getTasksH());
    }
  }

  /**
   * Returns actual simple list of items from mast by removing hierarchy through recursive process.
   *
   * @param actualServiceList the actual service list from mast.
   * @return list of items.
   */
  private List<WorkItemModelItemDto> returnActualItemList(final WorkItemPreviewListHDto actualServiceList) {
    List<WorkItemModelItemDto> actualItems = new ArrayList<>();

    for (WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto> serviceList : actualServiceList.getServices()) {
      if (serviceList.getItems() != null) {
        actualItems.addAll(serviceList.getItems());
        for (WorkItemModelItemDto item : serviceList.getItems()) {
          recursiveActualItem(item, actualItems);
        }
      }
    }
    return actualItems;
  }

  /**
   * Collects list of items from mast by removing hierarchy through recursive process.
   *
   * @param actualItem the item used for recursive process.
   * @param actualList the total list of items (with no hierarchy).
   */
  private void recursiveActualItem(final WorkItemModelItemDto actualItem, final List<WorkItemModelItemDto> actualList) {
    if (actualItem.getItems() != null) {
      actualList.addAll(actualItem.getItems());
      for (WorkItemModelItemDto item : actualItem.getItems()) {
        recursiveActualItem(item, actualList);
      }
    }
  }

  /**
   * Returns simple list of hydrated items by removing hierarchy through recursive process.
   *
   * @param expectedServiceList the result service list.
   * @return the list of hydrated items (No hierarchy).
   */
  private List<WorkItemModelItemHDto> returnExpectedItemList(final WorkItemPreviewListHDto expectedServiceList) {
    List<WorkItemModelItemHDto> expectedItems = new ArrayList<>();

    for (WorkItemPreviewScheduledServiceHDto serviceList : expectedServiceList.getServicesH()) {
      if (serviceList.getItemsH() != null) {
        expectedItems.addAll(serviceList.getItemsH());
        for (WorkItemModelItemHDto item : serviceList.getItemsH()) {

          recursiveExpectedItem(item, expectedItems);
        }
      }
    }
    return expectedItems;
  }

  /**
   * Collects list of hydrated items by removing hierarchy through recursive process.
   *
   * @param expectedItem the item used for recursive process.
   * @param expectedList the total list of hydrated items (with no hierarchy).
   */
  private void recursiveExpectedItem(final WorkItemModelItemHDto expectedItem,
      final List<WorkItemModelItemHDto> expectedList) {
    if (expectedItem.getItemsH() != null) {
      expectedList.addAll(expectedItem.getItemsH());
      for (WorkItemModelItemHDto item : expectedItem.getItemsH()) {
        recursiveExpectedItem(item, expectedList);
      }
    }
  }

  /**
   * Tests success scenario for {@link TaskService#getCheckListHierarchyByService(Long)}.build
   * hierarchy and sorting of checklist groups,subgroups and items.
   *
   * @throws ClassDirectException if API call fails or execution fails.
   */
  @Test
  public final void testChecklistHierarchyForService() throws ClassDirectException {
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockUnSortedChecklistData()));

    Map<Long, Map<Long, List<Long>>> heirarchyMapBeforeProcess = mockUnSortedChecklistData().stream()
        .collect(Collectors.groupingBy(p -> p.getChecklistH().getChecklistGroup().getId(),
            Collectors.groupingBy(k -> k.getChecklistH().getChecklistSubgroup().getId(),
                Collectors.mapping(WorkItemLightHDto::getId, Collectors.toList()))));

    final HierarchyDto ckHierarchy = taskService.getCheckListHierarchyByService(1L);
    Assert.assertNotNull(ckHierarchy);
    List<Long> groupids = new ArrayList<>();
    List<Long> subgroupids = new ArrayList<>();
    List<Long> ckItemsList = new ArrayList<>();

    Map<Long, Map<Long, List<Long>>> heirarchyMapAfterProcess = new HashMap<>();

    for (GroupDto groupDto : ckHierarchy.getChecklistGroups()) {
      groupids.add(groupDto.getId());
      if (!heirarchyMapAfterProcess.containsKey(groupDto.getId())) {
        heirarchyMapAfterProcess.put(groupDto.getId(), new HashMap<>());
      }
      List<Long> ckItems = new ArrayList<>();
      for (SubgroupDto subgrpDto : groupDto.getChecklistSubgrouplist()) {
        subgroupids.add(subgrpDto.getId());
        for (WorkItemLightHDto ckList : subgrpDto.getChecklistItems()) {
          ckItemsList.add(ckList.getId());

          Map<Long, List<Long>> subGroupitemMap = heirarchyMapAfterProcess.get(groupDto.getId());

          if (!subGroupitemMap.containsKey(subgrpDto.getId())) {
            ckItems = new ArrayList<>();
            ckItems.add(ckList.getId());
            subGroupitemMap.put(subgrpDto.getId(), ckItems);
            heirarchyMapAfterProcess.put(groupDto.getId(), subGroupitemMap);
          } else {
            ckItems.add(ckList.getId());
          }
        }
      }
    }
    Assert.assertTrue(heirarchyMapBeforeProcess.entrySet().size() == ckHierarchy.getChecklistGroups().size());

    ckHierarchy.getChecklistGroups().forEach(t -> Assert.assertTrue(
        heirarchyMapBeforeProcess.entrySet().stream().anyMatch(s -> s.getKey().longValue() == t.getId().longValue())));

    heirarchyMapBeforeProcess.entrySet().stream().forEach(gp -> Assert.assertTrue(groupids.contains(gp.getKey())));

    heirarchyMapBeforeProcess.entrySet().stream().forEach(sub -> sub.getValue().entrySet().stream()
        .forEach(subgp -> Assert.assertTrue(subgroupids.contains(subgp.getKey()))));

    heirarchyMapBeforeProcess.entrySet().stream().forEach(sub -> sub.getValue().entrySet().stream()
        .forEach(subgp -> Assert.assertTrue(ckItemsList.containsAll(subgp.getValue()))));


    List<GroupDto> groups = ckHierarchy.getChecklistGroups();
    for (int i = 0; i < groups.size(); i++) {
      // expected groups sorting order should be Accommodation,Cargo Pump Room,Survey Preparation.
      // first group element.
      Assert.assertTrue(groups.get(0).getName().equalsIgnoreCase("Cargo Pump Room"));
      // second group element.
      Assert.assertTrue(groups.get(1).getName().equalsIgnoreCase("Survey Preparation"));
      // third group element.
      Assert.assertTrue(groups.get(2).getName().equalsIgnoreCase("Accommodation"));
    }

    for (int i = 0; i < groups.size(); i++) {
      List<SubgroupDto> subgroups = groups.get(0).getChecklistSubgrouplist();
      for (int j = 0; j < subgroups.size(); j++) {
        // expected sub groups sorting order under group Cargo Pump Room should be
        // Certificate-Naval,Certified Lifeboatmen and Documentation.
        // first subgroup element.
        Assert.assertTrue(subgroups.get(0).getName().equalsIgnoreCase("Documentation"));
        // second subgroup element.
        Assert.assertTrue(subgroups.get(1).getName().equalsIgnoreCase("Certificate-Naval"));
        // third subgroup element.
        Assert.assertTrue(subgroups.get(2).getName().equalsIgnoreCase("Certified Lifeboatmen"));
      }
    }

    for (int i = 0; i < groups.size(); i++) {
      List<SubgroupDto> subgroups = groups.get(2).getChecklistSubgrouplist();
      for (int j = 0; j < subgroups.size(); j++) {
        List<WorkItemLightHDto> checklistItems = subgroups.get(0).getChecklistItems();
        for (int k = 0; k < checklistItems.size(); k++) {
          // expected checklist items sorting order under subgroup Documentation under group
          // Accommodation should be
          // 000002-XXXX-000001,000002-XXXX-000001 and 000002-XXXX-000003.
          // first checklist items element.
          Assert.assertTrue(checklistItems.get(0).getReferenceCode().equalsIgnoreCase("A5"));
          // second checklist items element.
          Assert.assertTrue(checklistItems.get(1).getReferenceCode().equalsIgnoreCase("A7"));
          // third checklist items element.
          Assert.assertTrue(checklistItems.get(2).getReferenceCode().equalsIgnoreCase("A8"));
        }
      }
    }
  }



  /**
   * Tests failure scenario for {@link TaskService#getCheckListHierarchyByService(Long)}.
   *
   * @throws ClassDirectException if API call fails or execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testReturnOfExceptionWhenCallChecklistHierarchyForService() throws ClassDirectException {
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.failure(new IOException()));

    final HierarchyDto ckHierarchy = taskService.getCheckListHierarchyByService(1L);
  }

  /**
   * Tests failure scenario for {@link TaskService#getCheckListHierarchyByService(Long)}.if list of
   * checklist have a checklist with null checklist Id.
   *
   * @throws ClassDirectException if API call fails or execution fails.
   * @throws RecordNotFoundException if checklist Id is not found.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testChecklistHierarchyForServiceforNullCheckListId()
      throws RecordNotFoundException, ClassDirectException {
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockChecklistDataWithNullChecklistId()));

    final HierarchyDto ckHierarchy = taskService.getCheckListHierarchyByService(1L);
  }

  /**
   * Tests success scenario for {@link TaskService#getCheckListHierarchyByService(Long)},if mast API
   * returns empty list.
   *
   * @throws ClassDirectException if API call fails or execution fails.
   * @throws RecordNotFoundException when object not found for given unique identifier.
   */
  @Test
  public final void testChecklistHierarchyForServiceforEmptyList()
      throws RecordNotFoundException, ClassDirectException {
    Mockito.when(taskRetrofit.getTask(Mockito.any(WorkItemQueryDto.class)))
        .thenReturn(Calls.response(mockEmptyChecklist()));

    final HierarchyDto ckHierarchy = taskService.getCheckListHierarchyByService(1L);
    Assert.assertNull(ckHierarchy);
  }

  /**
   * Method updateTasksAsList will throws exception when MAST returns stale exception.
   *
   * @throws ClassDirectException exception object.
   */
  @Test(expected = ClassDirectException.class)
  public final void throwExceptionWhenUpdateTasksAsListFailed() throws ClassDirectException {
    final WorkItemLightListDto response = new WorkItemLightListDto();
    response.setStalenessState(Collections.singletonList("Test"));
    Mockito.when(taskRetrofit.updateTaskList(Mockito.any())).thenReturn(Calls.response(response));

    final WorkItemLightListHDto request = new WorkItemLightListHDto();
    request.setTasks(Collections.singletonList(new WorkItemLightHDto()));
    taskService.updateTasksAsList(request);
  }


  /**
   * Provides Spring in-class configurations.
   *
   * @author VKolagutla
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Creates {@link TaskRetrofitService}.
     *
     * @return mock task retrofit service.
     *
     */
    @Override
    @Bean
    public TaskRetrofitService taskRetrofitService() {
      return Mockito.mock(TaskRetrofitService.class);
    }

    @Override
    @Bean
    public TaskService taskService() {
      return new TaskServiceImpl();
    }

  }

}
