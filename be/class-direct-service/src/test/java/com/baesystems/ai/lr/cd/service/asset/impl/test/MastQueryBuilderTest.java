package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetEffectiveDate;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetImoNumber;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetLifecycleStatusId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetSearchWithFormerAssetName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetTypeCode;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetTypeId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.assetWildcardSearch;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.buildDate;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.buildServiceCodicilQuery;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.builderIdAndBuilderName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.builderName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.classDepartmentId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.classStatusId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.clientCode;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.clientName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.closedJobsWithRange;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.codicilsAndServicesDue;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.codicilsQuery;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.deadWeight;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.excludeInActiveAsset;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.flagStateId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.formerAssetName;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.grossTonnage;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.hasActiveServices;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.ihsAssetId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.linkedAssetId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.serviceWithMMSService;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.servicesCatalogueId;
import static com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder.yardNumber;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.JoinType;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.asset.impl.Predicate;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AbstractSubQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.baesystems.ai.lr.enums.ServiceStatus;

/**
 * Provides unit tests for {@link MastQueryBuilder}.
 *
 * @author Faizal Sidek
 * @author YWearn 2017-05-18
 */
public class MastQueryBuilderTest {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MastQueryBuilderTest.class);

  /**
   * Default imo number.
   */
  private static final String DEFAULT_IMO_NUMBER = "1000010";

  /**
   * Default stat5 code.
   */
  private static final String DEFAULT_STAT5_CODE = "MBMM12345";

  /**
   * Default flag state id.
   */
  private static final Long DEFAULT_FLAG_STATE_ID = 10001L;

  /**
   * Min double.
   */
  private static final Double GENERIC_MIN_DOUBLE = 1000d;

  /**
   * Max double.
   */
  private static final Double GENERIC_MAX_DOUBLE = 2000d;

  /**
   * Min date.
   */
  private static final Date GENERIC_START_DATE = new Date();

  /**
   * Max date.
   */
  private static final Date GENERIC_END_DATE = new Date();

  /**
   * Search wildcard.
   */
  private static final String SEARCH_WILDCARD_PARAMETER = "*Lady*";

  /**
   * Count of search by wildcard clause.
   */
  private static final int WILDCARD_CLAUSE_COUNT = 3;

  /**
   * Normal class status id.
   */
  private static final Long[] CLASS_STATUS_ID_NORMAL = new Long[] {1L, 2L, 3L, 4L};

  /**
   * Class status id with non lr.
   */
  private static final Long[] CLASS_STATUS_ID_NON_LR = new Long[] {1L, 2L, 3L, 4L, 10L};

  /**
   * The service catalogue id.
   */
  private static final Long[] SERVICE_CATALOGUE_ID = new Long[] {1L, 2L, 3L, 4L, 5L};

  /**
   * The service catalogue id.
   */
  private static final Long[] MMS_SERVICE_CATALOGUE_ID = new Long[] {3L, 4L, 5L};

  /**
   * The codicil type id.
   */
  private static final String[] CODICIL_TYPE = new String[] {"COC", "AI"};

  /**
   * The codicil type id.
   */
  private static final Long[] CODICIL_TYPE_ID = new Long[] {1L, 2L};

  /**
   * The codicil status id.
   */
  private static final Long[] CODICIL_STATUS_ID = new Long[] {3L, 4L, 6L, 14L};

  /**
   * The codicil confidentialityType id.
   */
  private static final Long[] CODICIL_CONFIDENTIALITY_TYPE_ID = new Long[] {3L, 4L, 6L, 14L};

  /**
   * Query builder should create mandatory only filters.
   */
  @Test
  public final void shouldCreateMandatoryOnlyQuery() {
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(assetImoNumber(QueryRelationshipType.EQ, "1000010"))
            .mandatory(assetTypeCode(QueryRelationshipType.EQ, DEFAULT_STAT5_CODE)).build();

    checkOnlyAndSet(query);
    query.getAnd().forEach(MastQueryBuilderTest::checkJoinFieldIsSet);
  }

  /**
   * Query builder should create optional filters.
   */
  @Test
  public final void shouldCreateOptionalOnlyQuery() {
    final AbstractQueryDto query =
        MastQueryBuilder.create().optional(assetImoNumber(QueryRelationshipType.EQ, "1000010"))
            .optional(assetTypeCode(QueryRelationshipType.EQ, DEFAULT_STAT5_CODE)).build();

    checkOnlyOrSet(query);
    query.getOr().forEach(MastQueryBuilderTest::checkJoinFieldIsSet);
  }

  /**
   * Should create mandatory and optional.
   */
  @Test
  public final void shouldCreateMandatoryMixedWithOptionals() {
    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(assetId(QueryRelationshipType.EQ, 1000010L))
        .optional(assetTypeCode(QueryRelationshipType.EQ, DEFAULT_STAT5_CODE)).build();

    checkOnlyAndSet(query);
    query.getAnd().stream().filter(dto -> dto.getOr() != null && !dto.getOr().isEmpty()).forEach(dto -> {
      checkJoinFieldIsSet(dto.getOr().get(0));
    });
  }

  /**
   * Combined unit test for fluent builders.
   */
  @Test
  public final void combinedTestsForFluentBuilder() {
    genericPredicateTest(assetTypeId(QueryRelationshipType.EQ, 1000010L));
    genericPredicateTest(flagStateId(QueryRelationshipType.EQ, DEFAULT_FLAG_STATE_ID));
    genericBetweenTest(grossTonnage(GENERIC_MIN_DOUBLE, GENERIC_MAX_DOUBLE));
    genericPredicateTest(assetId(QueryRelationshipType.EQ, 1000010L));
    genericPredicateTest(ihsAssetId(QueryRelationshipType.EQ, 1000010L));
    genericPredicateNonJoinTest(yardNumber(QueryRelationshipType.EQ, DEFAULT_STAT5_CODE));
    genericPredicateTest(assetTypeCode(QueryRelationshipType.EQ, DEFAULT_STAT5_CODE));
    genericPredicateTest(servicesCatalogueId(QueryRelationshipType.EQ, DEFAULT_FLAG_STATE_ID));

    genericPredicateTest(assetLifecycleStatusId(QueryRelationshipType.EQ, 1000010L));
    genericPredicateTest(assetImoNumber(QueryRelationshipType.EQ, "1000010"));
    genericBetweenTestJoined(assetEffectiveDate(GENERIC_START_DATE, GENERIC_END_DATE));
    genericPredicateTest(classDepartmentId(QueryRelationshipType.EQ, DEFAULT_FLAG_STATE_ID));
    genericPredicateTest(linkedAssetId(QueryRelationshipType.EQ, 1000010L));
    genericBetweenTestJoined(deadWeight(GENERIC_MIN_DOUBLE, GENERIC_MAX_DOUBLE));

  }

  /**
   * Unit test for query by gross tonnage.
   */
  @Test
  public final void queryByGrossTonnage() {
    genericBetweenTest(grossTonnage(GENERIC_MIN_DOUBLE, GENERIC_MAX_DOUBLE));

    final AbstractQueryDto queryMinOnly = grossTonnage(GENERIC_MIN_DOUBLE, null).evaluate();
    assertNotNull(queryMinOnly.getField());
    assertEquals(QueryRelationshipType.GE, queryMinOnly.getRelationship());
    assertEquals(GENERIC_MIN_DOUBLE, queryMinOnly.getValue());
    assertEquals("publishedVersion.grossTonnage", queryMinOnly.getField());

    final AbstractQueryDto queryMaxOnly = grossTonnage(null, GENERIC_MAX_DOUBLE).evaluate();
    assertNotNull(queryMaxOnly.getField());
    assertEquals(QueryRelationshipType.LE, queryMaxOnly.getRelationship());
    assertEquals(GENERIC_MAX_DOUBLE, queryMaxOnly.getValue());
    assertEquals("publishedVersion.grossTonnage", queryMaxOnly.getField());
  }

  /**
   * Should create query by client id.
   */
  @Test
  public final void queryByClientId() {
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(clientCode(QueryRelationshipType.EQ, DEFAULT_IMO_NUMBER)).build();

    checkOnlyAndSet(query);
    final AbstractQueryDto clause =
        query.getAnd().stream().filter(and -> and.getOr() != null && !and.getOr().isEmpty()).findFirst().get();
    assertNotNull(clause);
    checkOnlyOrSet(clause);
    clause.getOr().forEach(MastQueryBuilderTest::checkJoinFieldIsSet);
  }

  /**
   * Should create query by services with MMS Services only.
   */
  @Test
  public final void queryByIsMMSServices() {
    final List<Long> mmsServiceCatalogueIds = new ArrayList<>(1);
    mmsServiceCatalogueIds.add(1L);
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(serviceWithMMSService(mmsServiceCatalogueIds)).build();
    checkOnlyAndSet(query);
    final AbstractQueryDto clause = query.getAnd()
        .stream().filter(and -> and.getJoinField() != null && and.getJoinField().equals("services")
            && and.getJoinType() != null && and.getJoinType().equals(JoinType.INNER) && !and.getAnd().isEmpty())
        .findFirst().get();
    assertNotNull(clause);
    assertNotNull(clause.getJoinField());
    assertNotNull(clause.getJoinType());

    final List<AbstractQueryDto> subQueries = clause.getAnd();
    assertNotNull(subQueries);

    final AbstractQueryDto firstSubQuery = subQueries.get(0);
    assertNotNull(firstSubQuery.getField());
    assertEquals("serviceCatalogueReadOnlyDomain.id", firstSubQuery.getField());
    assertNotNull(firstSubQuery.getRelationship());
    assertEquals(QueryRelationshipType.IN, firstSubQuery.getRelationship());
    assertNotNull(firstSubQuery.getValue());
    final Long[] actualServiceCatalogueIds = (Long[]) firstSubQuery.getValue();
    assertArrayEquals(mmsServiceCatalogueIds.toArray(new Long[] {}), actualServiceCatalogueIds);

    final AbstractQueryDto secondSubQuery = subQueries.get(1);
    assertNotNull(secondSubQuery.getField());
    assertEquals(secondSubQuery.getField(), "serviceStatus.id");
    assertNotNull(secondSubQuery.getRelationship());
    assertEquals(secondSubQuery.getRelationship(), QueryRelationshipType.NE);
    assertNotNull(secondSubQuery.getValue());
    assertEquals(secondSubQuery.getValue(), ServiceStatus.COMPLETE.getValue());
  }

  /**
   * Should create query by client name.
   */
  @Test
  public final void queryByClientName() {
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(clientName(QueryRelationshipType.EQ, DEFAULT_STAT5_CODE)).build();

    checkOnlyAndSet(query);
    final AbstractQueryDto clause =
        query.getAnd().stream().filter(and -> and.getOr() != null && !and.getOr().isEmpty()).findFirst().get();
    assertNotNull(clause);
    checkOnlyOrSet(clause);
    clause.getOr().forEach(MastQueryBuilderTest::checkJoinFieldIsSet);
  }

  /**
   * Query by build date should return between or equals.
   */
  @Test
  public final void queryByBuildDateRange() {
    final Date from = new Date(0);
    final Date to = new Date();

    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(buildDate(from, to)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto clause = query.getAnd().get(0);
    checkOnlyAndSet(clause);

    final AbstractQueryDto fromClause = clause.getAnd().get(0);
    checkFieldIsSet(fromClause);
    assertField(fromClause, "publishedVersion.buildDate", QueryRelationshipType.GE, from);

    final AbstractQueryDto toClause = clause.getAnd().get(1);
    checkFieldIsSet(toClause);
    assertField(toClause, "publishedVersion.buildDate", QueryRelationshipType.LE, to);
  }

  /**
   * Query by single build date.
   */
  @Test
  public final void queryByBuildDateSingle() {
    final Date to = new Date();

    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(buildDate(to, to)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto clause = query.getAnd().get(0);
    checkOnlyAndSet(clause);

    // convert fromDate and toDate to normal date
    final Calendar fromDate = Calendar.getInstance();
    fromDate.setTime(to);
    fromDate.add(Calendar.SECOND, -1);
    fromDate.add(Calendar.MILLISECOND, fromDate.getActualMaximum(Calendar.MILLISECOND));

    final Calendar toDate = Calendar.getInstance();
    toDate.setTime(to);
    toDate.add(Calendar.DATE, 1);
    toDate.add(Calendar.SECOND, -1);
    toDate.add(Calendar.MILLISECOND, toDate.getActualMaximum(Calendar.MILLISECOND));

    final AbstractQueryDto fromClause = clause.getAnd().get(0);
    checkFieldIsSet(fromClause);
    assertField(fromClause, "publishedVersion.buildDate", QueryRelationshipType.GE, fromDate.getTime());

    final AbstractQueryDto toClause = clause.getAnd().get(1);
    checkFieldIsSet(toClause);
    assertField(toClause, "publishedVersion.buildDate", QueryRelationshipType.LE, toDate.getTime());
  }

  /**
   * Query by class status id.
   */
  @Test
  public final void queryByClassStatusId() {
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(classStatusId(QueryRelationshipType.IN, CLASS_STATUS_ID_NORMAL)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkJoinFieldIsSet(mainClause);
    assertJoinField(mainClause, "publishedVersion.classStatus", "id", QueryRelationshipType.IN, CLASS_STATUS_ID_NORMAL);
  }

  /**
   * Query by class status id with non LR.
   */
  @Test
  public final void queryByClassStatusIdNonLR() {
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(classStatusId(QueryRelationshipType.IN, CLASS_STATUS_ID_NON_LR)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);

    final AbstractQueryDto hiddenIdClause = mainClause.getOr().get(0);
    checkJoinFieldIsSet(hiddenIdClause);
    assertJoinField(hiddenIdClause, "publishedVersion", "hiddenId", QueryRelationshipType.GT, 0);

    final AbstractQueryDto classStatusClause = mainClause.getOr().get(1);
    checkJoinFieldIsSet(classStatusClause);
    assertJoinField(classStatusClause, "publishedVersion.classStatus", "id", QueryRelationshipType.IN,
        CLASS_STATUS_ID_NON_LR);
  }

  /**
   * Query by survey type.
   */
  @Test
  public final void queryBySurveyType() {

    AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setCodicilDueDateMin(GENERIC_START_DATE);
    assetQuery.setCodicilDueDateMax(GENERIC_END_DATE);
    assetQuery.setServiceCatalogueIds(Arrays.asList(SERVICE_CATALOGUE_ID));

    Arrays.asList(MMS_SERVICE_CATALOGUE_ID);
    final AbstractQueryDto query = MastQueryBuilder.create()
        .mandatory(buildServiceCodicilQuery(assetQuery, Arrays.asList(MMS_SERVICE_CATALOGUE_ID), null, null, null))
        .build();

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);

    final AbstractQueryDto serviceQuery = mainClause.getOr().get(0);
    assertEquals("id", serviceQuery.getField());
    assertNotNull(serviceQuery.getValue());

    AbstractSubQueryDto serviceQueryValue = (AbstractSubQueryDto) serviceQuery.getValue();
    assertEquals("services", serviceQueryValue.getFieldOnParentObject());

    AbstractQueryDto serviceSubquery = serviceQueryValue.getSubQuery();
    checkOnlyAndSet(serviceSubquery);

    AbstractQueryDto serviceSubqueryAnd = serviceSubquery.getAnd().get(0);
    assertEquals("active", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.EQ, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(1);
    assertEquals("serviceStatus.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.NE, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(3);
    assertEquals("serviceCatalogue.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.IN, serviceSubqueryAnd.getRelationship());

  }

  /**
   * Query by Codicil type.
   */
  @Test
  public final void queryByCodicilType() {
    AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setCodicilDueDateMin(GENERIC_START_DATE);
    assetQuery.setCodicilDueDateMax(GENERIC_END_DATE);
    assetQuery.setCodicilTypeId(Arrays.asList(CODICIL_TYPE));

    Arrays.asList(CODICIL_TYPE_ID);
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(buildServiceCodicilQuery(assetQuery, null, Arrays.asList(CODICIL_TYPE_ID),
            Arrays.asList(CODICIL_STATUS_ID), Arrays.asList(CODICIL_CONFIDENTIALITY_TYPE_ID))).build();

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);

    final AbstractQueryDto codicilsQuery = mainClause.getOr().get(0);
    assertEquals("id", codicilsQuery.getField());
    assertNotNull(codicilsQuery.getValue());

    AbstractSubQueryDto codicilsQueryValue = (AbstractSubQueryDto) codicilsQuery.getValue();
    assertEquals("codicils", codicilsQueryValue.getFieldOnParentObject());


    AbstractQueryDto codicilsSubquery = codicilsQueryValue.getSubQuery();
    checkOnlyAndSet(codicilsSubquery);

    AbstractQueryDto codicilsSubqueryAnd = codicilsSubquery.getAnd().get(0);
    assertEquals("dueDate", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.GE, codicilsSubqueryAnd.getRelationship());

    codicilsSubqueryAnd = codicilsSubquery.getAnd().get(1);
    assertEquals("dueDate", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.LE, codicilsSubqueryAnd.getRelationship());

    codicilsSubqueryAnd = codicilsSubquery.getAnd().get(2);
    assertEquals("typeId", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.IN, codicilsSubqueryAnd.getRelationship());

  }


  /**
   * Query by MMS service type.
   */
  @Test
  public final void queryByMMS() {
    AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setCodicilDueDateMin(GENERIC_START_DATE);
    assetQuery.setCodicilDueDateMax(GENERIC_END_DATE);
    assetQuery.setIsMMSService(Boolean.TRUE);

    Arrays.asList(MMS_SERVICE_CATALOGUE_ID);
    final AbstractQueryDto query = MastQueryBuilder.create()
        .mandatory(buildServiceCodicilQuery(assetQuery, Arrays.asList(MMS_SERVICE_CATALOGUE_ID), null, null, null))
        .build();

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);

    final AbstractQueryDto serviceQuery = mainClause.getOr().get(0);
    assertEquals("id", serviceQuery.getField());
    assertNotNull(serviceQuery.getValue());

    AbstractSubQueryDto serviceQueryValue = (AbstractSubQueryDto) serviceQuery.getValue();
    assertEquals("services", serviceQueryValue.getFieldOnParentObject());

    AbstractQueryDto serviceSubquery = serviceQueryValue.getSubQuery();
    checkOnlyAndSet(serviceSubquery);

    AbstractQueryDto serviceSubqueryAnd = serviceSubquery.getAnd().get(0);
    assertEquals("active", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.EQ, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(1);
    assertEquals("serviceStatus.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.NE, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(3);
    assertEquals("serviceCatalogueReadOnlyDomain.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.IN, serviceSubqueryAnd.getRelationship());

  }

  /**
   * Query by codicil and survey type.
   */
  @Test
  public final void queryByCodicilAndSurveyType() {
    AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setCodicilDueDateMin(GENERIC_START_DATE);
    assetQuery.setCodicilDueDateMax(GENERIC_END_DATE);
    assetQuery.setServiceCatalogueIds(Arrays.asList(SERVICE_CATALOGUE_ID));
    assetQuery.setCodicilTypeId(Arrays.asList(CODICIL_TYPE));

    Arrays.asList(CODICIL_TYPE_ID);
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(buildServiceCodicilQuery(assetQuery, null, Arrays.asList(CODICIL_TYPE_ID),
            Arrays.asList(CODICIL_STATUS_ID), Arrays.asList(CODICIL_CONFIDENTIALITY_TYPE_ID))).build();

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);

    final AbstractQueryDto serviceQuery = mainClause.getOr().get(0);
    assertNotNull(serviceQuery.getValue());

    final AbstractQueryDto codicilsQuery = mainClause.getOr().get(1);
    assertEquals("id", codicilsQuery.getField());
    assertNotNull(codicilsQuery.getValue());

    AbstractSubQueryDto serviceQueryValue = (AbstractSubQueryDto) serviceQuery.getValue();
    assertEquals("services", serviceQueryValue.getFieldOnParentObject());

    AbstractQueryDto serviceSubquery = serviceQueryValue.getSubQuery();
    checkOnlyAndSet(serviceSubquery);

    AbstractQueryDto serviceSubqueryAnd = serviceSubquery.getAnd().get(0);
    assertEquals("active", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.EQ, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(1);
    assertEquals("serviceStatus.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.NE, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(3);
    assertEquals("serviceCatalogue.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.IN, serviceSubqueryAnd.getRelationship());

    AbstractSubQueryDto codicilsQueryValue = (AbstractSubQueryDto) codicilsQuery.getValue();
    assertEquals("codicils", codicilsQueryValue.getFieldOnParentObject());

    AbstractQueryDto codicilsSubquery = codicilsQueryValue.getSubQuery();
    checkOnlyAndSet(codicilsSubquery);

    AbstractQueryDto codicilsSubqueryAnd = codicilsSubquery.getAnd().get(0);
    assertEquals("dueDate", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.GE, codicilsSubqueryAnd.getRelationship());

    codicilsSubqueryAnd = codicilsSubquery.getAnd().get(1);
    assertEquals("dueDate", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.LE, codicilsSubqueryAnd.getRelationship());

    codicilsSubqueryAnd = codicilsSubquery.getAnd().get(2);
    assertEquals("typeId", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.IN, codicilsSubqueryAnd.getRelationship());

  }

  /**
   * Query by survey type and MMS service.
   */
  @Test
  public final void queryBySurveyTypeAndMMS() {

    AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setCodicilDueDateMin(GENERIC_START_DATE);
    assetQuery.setCodicilDueDateMax(GENERIC_END_DATE);
    assetQuery.setIsMMSService(Boolean.TRUE);
    assetQuery.setServiceCatalogueIds(Arrays.asList(SERVICE_CATALOGUE_ID));

    Arrays.asList(MMS_SERVICE_CATALOGUE_ID);
    final AbstractQueryDto query = MastQueryBuilder.create()
        .mandatory(buildServiceCodicilQuery(assetQuery, Arrays.asList(MMS_SERVICE_CATALOGUE_ID), null, null, null))
        .build();

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);

    final AbstractQueryDto serviceQuery = mainClause.getOr().get(0);
    assertEquals("id", serviceQuery.getField());
    assertNotNull(serviceQuery.getValue());

    AbstractSubQueryDto serviceQueryValue = (AbstractSubQueryDto) serviceQuery.getValue();
    assertEquals("services", serviceQueryValue.getFieldOnParentObject());

    AbstractQueryDto serviceSubquery = serviceQueryValue.getSubQuery();
    checkOnlyAndSet(serviceSubquery);

    AbstractQueryDto serviceSubqueryAnd = serviceSubquery.getAnd().get(0);
    assertEquals("active", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.EQ, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(1);
    assertEquals("serviceStatus.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.NE, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(3);
    assertEquals("serviceCatalogue.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.IN, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(4);
    assertEquals("serviceCatalogueReadOnlyDomain.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.IN, serviceSubqueryAnd.getRelationship());

  }


  /**
   * Query by due date and postponement date.
   */
  @Test
  public final void queryByDateOnly() {
    AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setCodicilDueDateMin(GENERIC_START_DATE);
    assetQuery.setCodicilDueDateMax(GENERIC_END_DATE);

    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(buildServiceCodicilQuery(assetQuery, null, null, null, null)).build();

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);

    final AbstractQueryDto serviceQuery = mainClause.getOr().get(0);
    assertNotNull(serviceQuery.getValue());

    final AbstractQueryDto codicilsQuery = mainClause.getOr().get(1);
    assertEquals("id", codicilsQuery.getField());
    assertNotNull(codicilsQuery.getValue());

    AbstractSubQueryDto serviceQueryValue = (AbstractSubQueryDto) serviceQuery.getValue();
    assertEquals("services", serviceQueryValue.getFieldOnParentObject());

    AbstractQueryDto serviceSubquery = serviceQueryValue.getSubQuery();
    checkOnlyAndSet(serviceSubquery);

    AbstractQueryDto serviceSubqueryAnd = serviceSubquery.getAnd().get(0);
    assertEquals("active", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.EQ, serviceSubqueryAnd.getRelationship());

    serviceSubqueryAnd = serviceSubquery.getAnd().get(1);
    assertEquals("serviceStatus.id", serviceSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.NE, serviceSubqueryAnd.getRelationship());

    AbstractQueryDto serviceSubqueryOr = serviceSubquery.getAnd().get(2);
    assertNotNull(serviceSubqueryOr);

    AbstractQueryDto serviceSubqueryOrAnd = serviceSubqueryOr.getOr().get(0).getAnd().get(0);
    assertEquals("postponementDate", serviceSubqueryOrAnd.getField());
    assertEquals(QueryRelationshipType.GE, serviceSubqueryOrAnd.getRelationship());

    serviceSubqueryOrAnd = serviceSubqueryOr.getOr().get(0).getAnd().get(1);
    assertEquals("postponementDate", serviceSubqueryOrAnd.getField());
    assertEquals(QueryRelationshipType.LE, serviceSubqueryOrAnd.getRelationship());

    serviceSubqueryOrAnd = serviceSubqueryOr.getOr().get(1).getAnd().get(0);
    assertEquals("postponementDate", serviceSubqueryOrAnd.getField());
    assertEquals(QueryRelationshipType.NULL, serviceSubqueryOrAnd.getRelationship());


    serviceSubqueryOrAnd = serviceSubqueryOr.getOr().get(1).getAnd().get(1);
    assertEquals("dueDate", serviceSubqueryOrAnd.getField());
    assertEquals(QueryRelationshipType.GE, serviceSubqueryOrAnd.getRelationship());

    serviceSubqueryOrAnd = serviceSubqueryOr.getOr().get(1).getAnd().get(2);
    assertEquals("dueDate", serviceSubqueryOrAnd.getField());
    assertEquals(QueryRelationshipType.LE, serviceSubqueryOrAnd.getRelationship());

    AbstractSubQueryDto codicilsQueryValue = (AbstractSubQueryDto) codicilsQuery.getValue();
    assertEquals("codicils", codicilsQueryValue.getFieldOnParentObject());

    AbstractQueryDto codicilsSubquery = codicilsQueryValue.getSubQuery();
    checkOnlyAndSet(codicilsSubquery);

    AbstractQueryDto codicilsSubqueryAnd = codicilsSubquery.getAnd().get(0);
    assertEquals("dueDate", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.GE, codicilsSubqueryAnd.getRelationship());

    codicilsSubqueryAnd = codicilsSubquery.getAnd().get(1);
    assertEquals("dueDate", codicilsSubqueryAnd.getField());
    assertEquals(QueryRelationshipType.LE, codicilsSubqueryAnd.getRelationship());

  }


  /**
   * Tests {@link MastQueryBuilder#assetWildcardSearch(String)}.
   * <p>
   * The query should returns 3 conditions:
   * <ol>
   * <li>Query value asset name with name in allVersions and asset published version equal to asset
   * published version id</li>
   * <li>Query value match asset imo number</li>
   * <li>Query value match with ihs name</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryByWildcardSearch() {

    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(assetWildcardSearch(SEARCH_WILDCARD_PARAMETER)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);
    assertEquals(WILDCARD_CLAUSE_COUNT, mainClause.getOr().size());

    final AbstractQueryDto searchByNameClause = mainClause.getOr().get(0);
    checkOnlyAndSet(searchByNameClause);
    assertFalse(searchByNameClause.getSelectWhenFieldIsMissing());

    final AbstractQueryDto searchVersionClause = searchByNameClause.getAnd().get(0);
    checkJoinFieldIsSet(searchVersionClause);
    assertJoinField(searchVersionClause, "allVersions", "name", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));
    final AbstractQueryDto searchAssetVersionClause = searchByNameClause.getAnd().get(1);
    checkJoinFieldIsSet(searchAssetVersionClause);
    final AbstractQueryDto queryDto = new AbstractQueryDto();
    queryDto.setJoinField("asset");
    queryDto.setField("publishedVersionId");
    assertEquals("allVersions", searchAssetVersionClause.getJoinField());
    assertEquals("assetVersionId", searchAssetVersionClause.getField());
    assertEquals(QueryRelationshipType.EQ, searchAssetVersionClause.getRelationship());
    assertTrue(searchAssetVersionClause.getValue() instanceof AbstractQueryDto);
    assertEquals("asset", ((AbstractQueryDto) searchAssetVersionClause.getValue()).getJoinField());
    assertEquals("publishedVersionId", ((AbstractQueryDto) searchAssetVersionClause.getValue()).getField());

    final AbstractQueryDto searchByImoClause = mainClause.getOr().get(1);
    checkJoinFieldIsSet(searchByImoClause);
    assertJoinField(searchByImoClause, "publishedVersion", "imoNumber", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));

    final AbstractQueryDto searchByNameIhs = mainClause.getOr().get(2);
    checkJoinFieldIsSet(searchByNameIhs);
    assertJoinField(searchByNameIhs, "publishedVersion", "name", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));
  }

  /**
   * Tests create query by codicil and services by status, types, due date range and postponement
   * date. range
   * {@link MastQueryBuilder#codicilsAndServicesDue(List, List, List, Date, Date, Date, Date)}.
   * <p>
   * The query should returns 2 conditions:
   * <ol>
   * <li>codicils with status ids, types, min amd max due date range.</li>
   * <li>services with active and postponement dae range</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryBycodicilsAndServicesDue() {

    final Long[] codicilStatusIds = {3L, 5L, 6L, 14L, 23L};
    final Long[] codicilTypeIds = {1L, 2L, 5L};
    final Long[] serviceSatatusIds = {2L, 5L};
    final Long[] confidentialityTypeIds = {3L};

    final AbstractQueryDto query = MastQueryBuilder.create()
        .mandatory(codicilsAndServicesDue(Arrays.asList(codicilStatusIds), Arrays.asList(codicilTypeIds),
            Arrays.asList(serviceSatatusIds), GENERIC_START_DATE, GENERIC_END_DATE, GENERIC_START_DATE,
            GENERIC_END_DATE, Arrays.asList(confidentialityTypeIds)))
        .build();

    checkOnlyAndSet(query);

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);
    assertEquals(2, mainClause.getOr().size());

    final AbstractQueryDto searchByCodicilClause = mainClause.getOr().get(0);
    checkOnlyAndSet(searchByCodicilClause);
    assertEquals(5, searchByCodicilClause.getAnd().size());

    final AbstractQueryDto searchCodidilDueMinClause = searchByCodicilClause.getAnd().get(0);
    checkJoinFieldIsSet(searchCodidilDueMinClause);
    assertJoinField(searchCodidilDueMinClause, "codicils", "dueDate", QueryRelationshipType.GE, GENERIC_START_DATE);

    final AbstractQueryDto searchCodidilDueMaxClause = searchByCodicilClause.getAnd().get(1);
    checkJoinFieldIsSet(searchCodidilDueMaxClause);
    assertJoinField(searchCodidilDueMaxClause, "codicils", "dueDate", QueryRelationshipType.LE, GENERIC_END_DATE);

    final AbstractQueryDto searchCodidilTypeIdsClause = searchByCodicilClause.getAnd().get(2);
    checkJoinFieldIsSet(searchCodidilTypeIdsClause);

    final AbstractQueryDto searchCodidilStatusClause = searchByCodicilClause.getAnd().get(3);
    checkJoinFieldIsSet(searchCodidilStatusClause);

    final AbstractQueryDto searchCodidilConfidentialityClause = searchByCodicilClause.getAnd().get(4);
    checkJoinFieldIsSet(searchCodidilConfidentialityClause);

    final AbstractQueryDto searchByServicesClause = mainClause.getOr().get(1);
    checkOnlyAndSet(searchByServicesClause);
    assertEquals(3, searchByServicesClause.getAnd().size());

    final AbstractQueryDto searchCodidilPostponementMinClause = searchByServicesClause.getAnd().get(0);
    checkJoinFieldIsSet(searchCodidilPostponementMinClause);
    assertJoinField(searchCodidilPostponementMinClause, "services", "postponementDate", QueryRelationshipType.GE,
        GENERIC_START_DATE);

    final AbstractQueryDto searchCodidilPostponementMaxClause = searchByServicesClause.getAnd().get(1);
    checkJoinFieldIsSet(searchCodidilPostponementMaxClause);
    assertJoinField(searchCodidilPostponementMaxClause, "services", "postponementDate", QueryRelationshipType.LE,
        GENERIC_END_DATE);

    final AbstractQueryDto searchServicesStatusClause = searchByServicesClause.getAnd().get(2);
    checkJoinFieldIsSet(searchServicesStatusClause);
  }

  /**
   * Tests create query by closed jobs with min and max range. range
   * {@link MastQueryBuilder#closedJobsWithRange(Date, Date, List)}.
   * <p>
   * The query should returns 2 conditions:
   * <ol>
   * <li>jobs with status closed ids.</li>
   * <li>closed jobs with range</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryByClosedJobsWithRange() {

    final Long[] closedJobIds = {11L};

    final AbstractQueryDto query =
        closedJobsWithRange(GENERIC_START_DATE, GENERIC_END_DATE, Arrays.asList(closedJobIds));

    checkOnlyAndSet(query);
    assertEquals(3, query.getAnd().size());
    final AbstractQueryDto searchJobClosedMinClause = query.getAnd().get(0);
    checkOnlyFieldSet(searchJobClosedMinClause);
    assertJoinField(searchJobClosedMinClause, null, "completedOn", QueryRelationshipType.GE, GENERIC_START_DATE);

    final AbstractQueryDto searchJobClosedMaxClause = query.getAnd().get(1);
    checkOnlyFieldSet(searchJobClosedMaxClause);
    assertJoinField(searchJobClosedMaxClause, null, "completedOn", QueryRelationshipType.LE, GENERIC_END_DATE);

    final AbstractQueryDto searchjobTypeIdsClause = query.getAnd().get(2);
    checkOnlyFieldSet(searchjobTypeIdsClause);

  }

  /**
   * Tests {@link MastQueryBuilder#formerAssetName(String)}.
   * <p>
   * The query should returns 3 conditions:
   * <ol>
   * <li>Query value former asset name with name in allVersions</li>
   * <li>Query value former asset published version not equal to asset published version id</li>
   * <li>Query value former asset with ihs formerData name</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryByFormerAssetNameSearch() {

    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(formerAssetName(SEARCH_WILDCARD_PARAMETER)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);
    assertEquals(2, mainClause.getOr().size());

    final AbstractQueryDto searchByNameClause = mainClause.getOr().get(0);
    checkOnlyAndSet(searchByNameClause);
    assertFalse(searchByNameClause.getSelectWhenFieldIsMissing());

    final AbstractQueryDto searchVersionClause = searchByNameClause.getAnd().get(0);
    checkJoinFieldIsSet(searchVersionClause);
    assertJoinField(searchVersionClause, "allVersions", "name", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));

    final AbstractQueryDto searchAssetVersionClause = searchByNameClause.getAnd().get(1);
    checkJoinFieldIsSet(searchAssetVersionClause);
    final AbstractQueryDto queryDto = new AbstractQueryDto();
    queryDto.setJoinField("asset");
    queryDto.setField("publishedVersionId");
    assertEquals("allVersions", searchAssetVersionClause.getJoinField());
    assertEquals("assetVersionId", searchAssetVersionClause.getField());
    assertEquals(QueryRelationshipType.NE, searchAssetVersionClause.getRelationship());
    assertTrue(searchAssetVersionClause.getValue() instanceof AbstractQueryDto);
    assertEquals("asset", ((AbstractQueryDto) searchAssetVersionClause.getValue()).getJoinField());
    assertEquals("publishedVersionId", ((AbstractQueryDto) searchAssetVersionClause.getValue()).getField());

    final AbstractQueryDto searchByNameIhs = mainClause.getOr().get(1);
    checkJoinFieldIsSet(searchByNameIhs);
    assertJoinField(searchByNameIhs, "publishedVersion.formerData", "keyName", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));
  }

  /**
   * Tests {@link MastQueryBuilder#assetSearchWithFormerAssetName(String)}.
   * <p>
   * The query should returns 4 conditions:
   * <ol>
   * <li>Query value asset name with name in allVersions</li>
   * <li>Query value asset with asset imonumber</li>
   * <li>Query value asset with ihs formerData name</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryByWildcardAllVersionSearch() {
    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(assetSearchWithFormerAssetName(SEARCH_WILDCARD_PARAMETER)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto mainClause = query.getAnd().get(0);
    checkOnlyOrSet(mainClause);
    assertEquals(5, mainClause.getOr().size());

    final AbstractQueryDto searchByNameClause = mainClause.getOr().get(0);
    // checkOnlyAndSet(searchByNameClause);
    assertFalse(searchByNameClause.getSelectWhenFieldIsMissing());

    // final AbstractQueryDto searchVersionClause = searchByNameClause.getAnd().get(0);
    checkJoinFieldIsSet(searchByNameClause);
    assertJoinField(searchByNameClause, "publishedVersion", "name", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));

    final AbstractQueryDto searchAssetVersionClause = mainClause.getOr().get(1);
    checkJoinFieldIsSet(searchAssetVersionClause);

    assertEquals("allVersions", searchAssetVersionClause.getJoinField());
    assertEquals("name", searchAssetVersionClause.getField());
    assertEquals(QueryRelationshipType.LIKE, searchAssetVersionClause.getRelationship());

    final AbstractQueryDto searchByImoClause = mainClause.getOr().get(2);
    checkJoinFieldIsSet(searchByImoClause);
    assertJoinField(searchByImoClause, "publishedVersion", "imoNumber", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));

    final AbstractQueryDto searchByName = mainClause.getOr().get(3);
    checkJoinFieldIsSet(searchByName);
    assertJoinField(searchByName, "publishedVersion", "name", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));

    final AbstractQueryDto searchByNameIhs = mainClause.getOr().get(4);
    checkJoinFieldIsSet(searchByNameIhs);
    assertJoinField(searchByNameIhs, "publishedVersion.formerData", "keyName", QueryRelationshipType.LIKE,
        SEARCH_WILDCARD_PARAMETER.replace("*", "%"));
  }

  /**
   * Tests {@link MastQueryBuilder#builderIdAndBuilderName(String, String)}.
   * <p>
   * The query should returns 2 conditions:
   * <ol>
   * <li>Query IHS using builder code (IMO number).</li>
   * <li>Query MAST using builder name</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryByBuilder() {
    final String imoNumber = "1";
    final String builderName = "BUILDER_A";

    final AbstractQueryDto query =
        MastQueryBuilder.create().mandatory(builderIdAndBuilderName(imoNumber, builderName)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto ihsClause = query.getAnd().get(0).getOr().get(0);
    checkJoinFieldIsSet(ihsClause);
    assertJoinField(ihsClause, "publishedVersion.builderInfo", "code", QueryRelationshipType.EQ, imoNumber);

    final AbstractQueryDto mastClause = query.getAnd().get(0).getOr().get(1);
    checkJoinFieldIsSet(mastClause);
    assertJoinField(mastClause, "publishedVersion", "builder", QueryRelationshipType.EQ, builderName);
  }

  /**
   * Tests {@link MastQueryBuilder#builderName(QueryRelationshipType, String...)}.
   * <p>
   * The query should returns 2 conditions:
   * <ol>
   * <li>Query IHS on publishedVersion.builderInfo.name.</li>
   * <li>Query MAST on publishedVersion.builder.</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryByBuilderName() {
    final QueryRelationshipType cond = QueryRelationshipType.LIKE;
    final String builderName = "BUILDER_A";

    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(builderName(cond, builderName)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto ihsClause = query.getAnd().get(0).getOr().get(0);
    checkJoinFieldIsSet(ihsClause);
    assertJoinField(ihsClause, "publishedVersion.builderInfo", "name", cond, builderName);

    final AbstractQueryDto mastClause = query.getAnd().get(0).getOr().get(1);
    checkJoinFieldIsSet(mastClause);
    assertJoinField(mastClause, "publishedVersion", "builder", cond, builderName);
  }

  /**
   * Test for {@link MastQueryBuilder#hasActiveServices()}.
   */
  @Test
  public final void queryByHasActiveServices() {
    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(hasActiveServices()).build();
    checkOnlyAndSet(query);
    checkJoinFieldIsSet(query.getAnd().get(0));
    assertJoinField(query.getAnd().get(0), "services", "active", QueryRelationshipType.EQ, true);
  }

  /**
   * Test for {@link MastQueryBuilder#excludeInActiveAsset()}.
   */
  @Test
  public final void queryByExcludeInActiveAsset() {
    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(excludeInActiveAsset()).build();
    final AbstractQueryDto excludeInActiveAssetQuery = query.getAnd().get(0);
    checkOnlyOrSet(excludeInActiveAssetQuery);
    checkJoinFieldIsSet(excludeInActiveAssetQuery.getOr().get(0));
    assertJoinFieldWithoutValue(excludeInActiveAssetQuery.getOr().get(0), "publishedVersion.ssch2", "status",
        QueryRelationshipType.IN);
    final AbstractQueryDto subQuery = excludeInActiveAssetQuery.getOr().get(1);
    checkOnlyAndSet(subQuery);

    assertJoinFieldWithoutValue(subQuery.getAnd().get(0), "publishedVersion", "assetLifecycleStatus",
        QueryRelationshipType.IN);
    /**
     * the below code has been commented temporarily to map with the temporary change in actual
     * implementation method
     */
    // assertJoinField(subQuery.getAnd().get(1), "publishedVersion", "imoNumber",
    // QueryRelationshipType.NULL, true);
  }

  /**
   * Tests create query by codicil by codicil types and active statuses.
   * {@link MastQueryBuilder#codicilsQuery(QueryRelationshipType, List, Long...)}.
   * <p>
   * The query should returns 2 conditions:
   * <ol>
   * <li>codicils with active status ids.</li>
   * <li>codicils with COC and AI</li>
   * </ol>
   * </p>
   */
  @Test
  public final void queryBycodicilsTypes() {

    final Long[] codicilStatusIds = {3L, 5L, 6L, 14L};
    final Long[] codicilTypeIds = {1L, 2L};
    final Long[] codicilConfidentialityTypeIds = {3L};

    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(codicilsQuery(QueryRelationshipType.IN,
        Arrays.asList(codicilStatusIds), Arrays.asList(codicilConfidentialityTypeIds), codicilTypeIds)).build();
    checkOnlyAndSet(query);

    final AbstractQueryDto clause = query.getAnd().get(0);
    checkOnlyAndSet(clause);

    final List<AbstractQueryDto> andcaluse = clause.getAnd();
    assertNotNull(andcaluse);
    assertFalse(andcaluse.isEmpty());
    assertEquals(3, andcaluse.size());

    final AbstractQueryDto searchCodidilTypeIdsClause = andcaluse.get(0);
    checkJoinFieldIsSet(searchCodidilTypeIdsClause);
    assertJoinField(searchCodidilTypeIdsClause, "codicils", "typeId", QueryRelationshipType.IN, codicilTypeIds);

    final AbstractQueryDto searchCodidilStatusClause = andcaluse.get(1);
    checkJoinFieldIsSet(searchCodidilStatusClause);

    final AbstractQueryDto searchConfidentialityTypeClause = andcaluse.get(2);
    checkJoinFieldIsSet(searchConfidentialityTypeClause);
  }

  /**
   * Assert field.
   *
   * @param query query.
   * @param fieldName field name.
   * @param relationshipType relationship.
   * @param fieldValue field value.
   */
  private void assertField(final AbstractQueryDto query, final String fieldName,
      final QueryRelationshipType relationshipType, final Object fieldValue) {
    assertEquals(fieldName, query.getField());
    assertEquals(relationshipType, query.getRelationship());
    assertEquals(fieldValue, query.getValue());
  }

  /**
   * Assert join field.
   *
   * @param query query.
   * @param joinField join field name.
   * @param fieldName field name.
   * @param relationshipType relationship.
   * @param fieldValue field value.
   */
  public static void assertJoinField(final AbstractQueryDto query, final String joinField, final String fieldName,
      final QueryRelationshipType relationshipType, final Object fieldValue) {
    assertJoinFieldWithoutValue(query, joinField, fieldName, relationshipType);
    assertEquals(fieldValue, query.getValue());
  }

  /**
   * Assert join field without value.
   *
   * @param query query dto.
   * @param joinField join field.
   * @param fieldName field name.
   * @param relationshipType field relationship type.
   */
  public static void assertJoinFieldWithoutValue(final AbstractQueryDto query, final String joinField,
      final String fieldName, final QueryRelationshipType relationshipType) {
    assertEquals(joinField, query.getJoinField());
    assertEquals(fieldName, query.getField());
    assertEquals(relationshipType, query.getRelationship());
  }

  /**
   * Generic test.
   *
   * @param predicate predicate.
   */
  private void genericPredicateTest(final Predicate predicate) {
    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(predicate).build();

    checkOnlyAndSet(query);
    final AbstractQueryDto clause = query.getAnd().get(0);
    checkJoinFieldIsSet(clause);
  }

  /**
   * Generic test for non-joined.
   *
   * @param predicate predicate.
   */
  private void genericPredicateNonJoinTest(final Predicate predicate) {
    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(predicate).build();

    checkOnlyAndSet(query);
    final AbstractQueryDto clause = query.getAnd().get(0);
    checkFieldIsSet(clause);
  }

  /**
   * Generic test for between clause.
   *
   * @param predicate predicate to test.
   */
  private void genericBetweenTestJoined(final Predicate predicate) {
    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(predicate).build();

    checkOnlyAndSet(query);
    final AbstractQueryDto clause = query.getAnd().get(0);
    checkOnlyAndSet(clause);
    final List<AbstractQueryDto> between = clause.getAnd();
    assertNotNull(between);
    assertFalse(between.isEmpty());
    assertEquals(2, between.size());
    between.forEach(MastQueryBuilderTest::checkJoinFieldIsSet);
  }

  /**
   * Generic test for between clause.
   *
   * @param predicate predicate to test.
   */
  private void genericBetweenTest(final Predicate predicate) {
    final AbstractQueryDto query = MastQueryBuilder.create().mandatory(predicate).build();

    checkOnlyAndSet(query);
    final AbstractQueryDto clause = query.getAnd().get(0);
    checkOnlyAndSet(clause);
    final List<AbstractQueryDto> between = clause.getAnd();
    assertNotNull(between);
    assertFalse(between.isEmpty());
    assertEquals(2, between.size());
    between.forEach(this::checkFieldIsSet);
  }

  /**
   * Assert join fields is set.
   *
   * @param query query dto.
   */
  public static void checkJoinFieldIsSet(final AbstractQueryDto query) {
    assertNotNull(query);
    assertNull(query.getOr());
    assertNull(query.getAnd());
    assertNotNull(query.getJoinField());
    assertNotNull(query.getJoinType());
    assertNotNull(query.getField());
    assertNotNull(query.getValue());
    assertNotNull(query.getRelationship());
  }

  /**
   * Assert fields is set.
   *
   * @param query query dto.
   */
  private void checkFieldIsSet(final AbstractQueryDto query) {
    assertNotNull(query);
    assertNull(query.getOr());
    assertNull(query.getAnd());
    assertNull(query.getJoinField());
    assertNull(query.getJoinType());
    assertNotNull(query.getField());
    assertNotNull(query.getValue());
    assertNotNull(query.getRelationship());
  }

  /**
   * Assert only AND field is set.
   *
   * @param query query dto.
   */
  public static void checkOnlyAndSet(final AbstractQueryDto query) {
    assertNotNull(query);
    assertNull(query.getOr());
    assertNotNull(query.getAnd());
    assertFalse(query.getAnd().isEmpty());
    assertNull(query.getJoinField());
    assertNull(query.getJoinType());
    assertNull(query.getField());
    assertNull(query.getValue());
    assertNull(query.getRelationship());
  }

  /**
   * Assert only OR field is set.
   *
   * @param query query dto.
   */
  private void checkOnlyOrSet(final AbstractQueryDto query) {
    assertNotNull(query);
    assertNotNull(query.getOr());
    assertFalse(query.getOr().isEmpty());
    assertNull(query.getAnd());
    assertNull(query.getJoinField());
    assertNull(query.getJoinType());
    assertNull(query.getField());
    assertNull(query.getValue());
    assertNull(query.getRelationship());
  }

  /**
   * Assert only field and value is set.
   *
   * @param query query dto.
   */
  private void checkOnlyFieldSet(final AbstractQueryDto query) {
    assertNotNull(query);
    assertNull(query.getOr());
    assertNull(query.getAnd());
    assertNull(query.getJoinField());
    assertNotNull(query.getField());
    assertNull(query.getJoinType());
    assertNotNull(query.getValue());
    assertNotNull(query.getRelationship());
  }
}
