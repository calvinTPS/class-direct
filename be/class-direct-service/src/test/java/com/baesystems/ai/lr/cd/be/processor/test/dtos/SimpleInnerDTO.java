package com.baesystems.ai.lr.cd.be.processor.test.dtos;

/**
 * Created by PFauchon on 21/11/2016.
 */
public class SimpleInnerDTO {


  /**
   * Create a simple dto.
   *
   * @param idParam   the id
   * @param nameParam the name
   */
  public SimpleInnerDTO(final int idParam, final String nameParam) {
    this.id = idParam;
    this.name = nameParam;
  }

  /**
   * int field.
   */
  private int id;

  /**
   * String field.
   */
  private String name;

  /**
   * get id field.
   *
   * @return the id
   */
  public final int getId() {
    return id;
  }


  /**
   * set id field.
   *
   * @param idParam the id
   */
  public final void setId(final int idParam) {
    this.id = idParam;
  }


  /**
   * get name.
   *
   * @return the name
   */
  public final String getName() {
    return name;
  }


  /**
   * set field.
   *
   * @param nameParam the name
   */
  public final void setName(final String nameParam) {
    this.name = nameParam;
  }
}
