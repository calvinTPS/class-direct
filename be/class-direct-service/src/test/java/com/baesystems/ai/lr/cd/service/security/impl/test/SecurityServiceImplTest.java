package com.baesystems.ai.lr.cd.service.security.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.security.RolesDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.service.security.impl.SecurityServiceImpl;

/**
 * Test class for Port transactions.
 *
 * @author VKovuru
 */
@RunWith(MockitoJUnitRunner.class)
public class SecurityServiceImplTest {

  /**
   * Injected {@link PortService}.
   *
   */
  @InjectMocks
  private SecurityServiceImpl securityService = new SecurityServiceImpl();

  /**
   * SecurityDAO.
   */
  @Mock
  private SecurityDAO securityDAO;
  /**
   *
   * @throws Exception exception
   */
  @Test
  public final void testHasRoles() throws Exception {
    List<String> list = new ArrayList<>();
    RolesDto dto = new RolesDto();
    dto.setRoles(list);
    Mockito.when(securityDAO.hasRole(Mockito.anyString(), Mockito.anyList())).thenReturn(true);
    Assert.assertTrue(securityService.hasRoles("user", dto));
  }
}
