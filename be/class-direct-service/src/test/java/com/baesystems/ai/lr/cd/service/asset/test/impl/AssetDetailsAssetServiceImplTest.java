package com.baesystems.ai.lr.cd.service.asset.test.impl;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.LR_ADMIN;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.FlagStateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.EquipmentDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.IhsAssetClientDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.OwnershipDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.PrincipalDimensionDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RegistryInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RulesetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.CustomerRolesEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.reference.CustomerReferenceService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.BaseAssetDto;
import com.baesystems.ai.lr.dto.assets.IhsAssetLink;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetTypeDto;
import com.baesystems.ai.lr.dto.ihs.IhsCbsbDto;
import com.baesystems.ai.lr.dto.ihs.IhsCompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipment2Dto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipment4Dto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsGroupFleetDto;
import com.baesystems.ai.lr.dto.ihs.IhsHistDto;
import com.baesystems.ai.lr.dto.ihs.IhsOvnaDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;

import retrofit2.Call;
import retrofit2.mock.Calls;

/**
 * Additional Test Class for Asset Service Implementation Class.
 *
 * @author RKaneysan.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AssetDetailsAssetServiceImplTest.Config.class)
public class AssetDetailsAssetServiceImplTest {

  /**
   * Security context.
   */
  private SecurityContext securityContext;

  /**
   * Mocked {@link AssetRetrofitService}.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * Mocked {@link IhsRetrofitService}.
   */
  @Autowired
  private IhsRetrofitService ihsServiceDelegate;

  /**
   * Mocked {@link FlagStateRetrofitService.}.
   */
  @Autowired
  private FlagStateRetrofitService flagStateDelegate;

  /**
   * Injected {@link AssetService}.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Injected {@link UserProfileService}.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Injected {@link CustomerReferenceService}.
   */
  @Autowired
  private CustomerReferenceService customerReferenceService;

  /**
   * Injected {@link SubFleetService}.
   */
  @Autowired
  private SubFleetService subFleetService;

  /**
   * Constant LR asset id.
   */
  private static final Long LR_ASSET_ID = 1L;

  /**
   * Constant asset id.
   */
  private static final String LR_ASSET_CODE = "LRV1";

  /**
   * Alice's User Id.
   */
  private static final String USER_ID = "d5aac4be-2714-43b7-a18e-cc27c1dff473";

  /**
   * Constant port id.
   */
  private static final Long PORT_ID = 1L;

  /**
   * Constant asset id.
   */
  private static final Long COUNTRY_ID = 1L;

  /**
   * Date Pattern.
   */
  private static final String DATE_PATTERN_KEEL_AND_BUILD_DATE = "dd MMM yyyy";

  /**
   * Date Pattern constant (yyyyMMdd).
   */
  private static final String DATE_PATTERN_REVERSE = "yyyyMMdd";

  /**
   * Date Pattern .
   */
  private static final String DATE_PATTERN_YEAR_ONLY = "yyyy";

  /**
   * Constant doccode.
   */
  private static final String DOC_CODE = "1000191";
  /**
   * Constant general id.
   */
  private static final Long GENERAL_ID = 1L;
  /**
   * version id.
   */
  private static final Long LR_ASSET_VESRSION_ID = 3L;
  /**
   * Country name.
   */
  private static final String COUNTRY_NAME = "Malaysia";

  /**
   * Reset mock objects.
   */
  @Before
  public void resetMocks() {
    Mockito.reset(assetServiceDelegate);
  }

  /**
   * Mock Port Of Registry.
   *
   * @return PortOfRegistryDto.
   */
  public PortOfRegistryDto mockPortOfRegistry() {
    PortOfRegistryDto podDto = new PortOfRegistryDto();
    podDto.setId(1L);
    podDto.setName("A i Lofoten");
    return podDto;
  }

  /**
   * Mock for Flag State.
   *
   * @return FlagStateHDto
   */
  private FlagStateHDto mockFlagState() {
    FlagStateHDto flagStateHDto = new FlagStateHDto();
    flagStateHDto.setId(1L);
    flagStateHDto.setName("Albania");
    return flagStateHDto;
  }

  /**
   * Mock Assets for Mast IHS query.
   *
   * @param ihsAssetDto IHS Asset Details Dto.
   * @param mockedAsset Asset Hydrated Dto.
   * @return Caller response for List<MultiAssetDto>.
   */
  private Call<MultiAssetPageResourceDto> mockAssets(final IhsAssetDetailsDto ihsAssetDto,
      final AssetHDto mockedAsset) {
    final Boolean isEmptyAsset = ihsAssetDto == null && mockedAsset == null;

    if (!isEmptyAsset) {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber("1000019");
      multiAssetDto.setIhsAsset(ihsAssetDto);
      multiAssetDto.setMastAsset(mockedAsset);
      final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
      pageResource.setContent(Collections.singletonList(multiAssetDto));
      return Calls.response(pageResource);
    } else {
      final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
      pageResource.setContent(Collections.emptyList());
      return Calls.response(pageResource);
    }
  }

  /**
   * Mock for Port Of Registry Information.
   *
   * @return mocked port of registry dto.
   */
  private PortOfRegistryDto mockPortOfRegistryDto() {
    final PortOfRegistryDto portDto = new PortOfRegistryDto();
    portDto.setId(PORT_ID);
    portDto.setName("KL Port");
    return portDto;
  }

  /**
   * Mock for Registry Information.
   *
   * @return value.
   * @throws IOException exception.
   */
  private AssetHDto mockAssetHdtosforLrAssets() {
    Date currentDate = new Date();

    AssetTypeDto assetTypeDto = new AssetTypeDto();
    assetTypeDto.setName("General Cargo Barge, non propelled");

    FlagStateHDto flag = new FlagStateHDto();
    flag.setFlagCode("CH2");

    ClassStatusHDto clStatusDto = new ClassStatusHDto();
    clStatusDto.setId(10L);

    AssetHDto dto = new AssetHDto();
    dto.setId(LR_ASSET_ID);
    dto.setCode(LR_ASSET_CODE);
    dto.setRegisteredPort(new LinkResource(mockPortOfRegistryDto().getId()));
    dto.setAssetType(assetTypeDto);
    dto.setBuilder("Ership Internacional SA");
    dto.setYardNumber("Yard1");
    dto.setKeelLayingDate(currentDate);
    dto.setBuildDate(currentDate);
    dto.setCountryOfBuild(new LinkResource(COUNTRY_ID));
    dto.setFlagStateDto(flag);
    dto.setIhsAsset(new IhsAssetLink("1000019"));
    dto.setCallSign("Call Sign");
    dto.setAssetLifecycleStatus(new LinkResource(1L));
    dto.setClassStatusDto(clStatusDto);
    AssetLifeCycleStatusDto status = new AssetLifeCycleStatusDto();
    status.setName("Closed");
    dto.setAssetLifeCycleStatusDto(status);
    dto.setIsLead(true);
    dto.setVersionId(LR_ASSET_VESRSION_ID);
    return dto;
  }


  /**
   * Mock for CountryHDto Information.
   *
   * @return value.
   * @throws IOException exception.
   */
  private CountryHDto mockCountryHDto() {
    CountryHDto mockedHDto = new CountryHDto();
    mockedHDto.setId(COUNTRY_ID);
    mockedHDto.setName(COUNTRY_NAME);
    return mockedHDto;
  }

  /**
   * Mock for Ihs Asset Details.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsAssetDetailsDto mockIhsAssetDetailsForAssetDetails() {
    IhsHistDto ihsHistDto = new IhsHistDto();
    ihsHistDto.setKeelLayingDate("20160901");

    IhsGroupFleetDto ihsGroupFleetDto = new IhsGroupFleetDto();
    ihsGroupFleetDto.setRegdOwnerName("ROwnr1");
    ihsGroupFleetDto.setGroupOwnerName("GOwn1");

    IhsAssetTypeDto ihsAssetType = new IhsAssetTypeDto();
    ihsAssetType.setStatDeCode("LNG Tanker");

    List<IhsOvnaDto> formerAssetdata = new ArrayList<>();
    IhsOvnaDto ihsOvnaDto = new IhsOvnaDto();
    ihsOvnaDto.setG01Name("Test 1");
    formerAssetdata.add(ihsOvnaDto);
    IhsOvnaDto ihsOvnaDto2 = new IhsOvnaDto();
    ihsOvnaDto2.setG01Name("TEST 1");
    formerAssetdata.add(ihsOvnaDto2);

    IhsCbsbDto ihsCbsb = new IhsCbsbDto();
    ihsCbsb.setDecodeA("Decode A");
    ihsCbsb.setDecodeB("Decode B");

    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("1000019");
    ihsAssetDto.setFormerData(formerAssetdata);
    ihsAssetDto.setIhsCbsb(ihsCbsb);
    ihsAssetDto.setPortName("KL Port");
    ihsAssetDto.setCallSign("KL Call Sign");
    ihsAssetDto.setOfficialNo("010-987-001");
    ihsAssetDto.setName("TEST 1");
    ihsAssetDto.setBuilder("A builder");
    ihsAssetDto.setYardNumber("y1");
    ihsAssetDto.setDateOfBuild(new Date());
    ihsAssetDto.setCountryOfBuild("MY");
    ihsAssetDto.setFlag("1");
    ihsAssetDto.setIhsAssetType(ihsAssetType);
    ihsAssetDto.setShipManagerCode("3000107");
    ihsAssetDto.setShipManager("KL Ship Manager");
    ihsAssetDto.setOperatorCode("3000108");
    ihsAssetDto.setOperator("KL Operator");
    ihsAssetDto.setDocCode(DOC_CODE);
    ihsAssetDto.setDocCompany("Docking Company");
    ihsAssetDto.setTechManagerCode("3000109");
    ihsAssetDto.setTechManager("TMAN1");
    ihsAssetDto.setOwner("ROwnr1");
    ihsAssetDto.setOwnerCode("5000001");
    ihsAssetDto.setGbo("GOwn1");
    ihsAssetDto.setGboCode("6000001");
    ihsAssetDto.setMmsi("013456789");
    ihsAssetDetailsDto.setId("1000019");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    ihsAssetDetailsDto.setIhsHist(ihsHistDto);
    ihsAssetDetailsDto.setIhsGroupFleet(ihsGroupFleetDto);
    return ihsAssetDetailsDto;
  }

  /**
   * Mock for Ihs Personnel Contact Dto.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsPersonnelContactDto mockOperatorIhsPersonnelContactForAssetDetails() {
    IhsPersonnelContactDto ihsPersonnelContactDto = new IhsPersonnelContactDto();
    ihsPersonnelContactDto.setAddressLine1("ADDR LINE 1");
    ihsPersonnelContactDto.setAddressLine2("ADDR LINE 2");
    ihsPersonnelContactDto.setAddressLine3("ADDR LINE 3");
    ihsPersonnelContactDto.setStreet1("Street Line1");
    ihsPersonnelContactDto.setEmail("gary@hotmail.com");
    ihsPersonnelContactDto.setTelephone("7869879876");
    return ihsPersonnelContactDto;
  }

  /**
   * Mock for Ihs Personnel Contact Dto.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsPersonnelContactDto mockShipManagerIhsPersonnelContactForAssetDetails() {
    IhsPersonnelContactDto ihsPersonnelContactDto = new IhsPersonnelContactDto();
    ihsPersonnelContactDto.setAddressLine1("ADDR LINE 1");
    ihsPersonnelContactDto.setAddressLine2("ADDR LINE 2");
    ihsPersonnelContactDto.setAddressLine3("ADDR LINE 3");
    ihsPersonnelContactDto.setEmail("apple.id@hotmail.com");
    ihsPersonnelContactDto.setTelephone("7869879876");
    return ihsPersonnelContactDto;
  }

  /**
   * Mock for Ihs Personnel Contact Dto.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsPersonnelContactDto mockTechManagerIhsPersonnelContactForAssetDetails() {
    IhsPersonnelContactDto ihsPersonnelContactDto = new IhsPersonnelContactDto();
    ihsPersonnelContactDto.setAddressLine1("ADDR LINE 1");
    ihsPersonnelContactDto.setAddressLine2("ADDR LINE 2");
    ihsPersonnelContactDto.setAddressLine3("ADDR LINE 3");
    ihsPersonnelContactDto.setEmail("tom@hotmail.com");
    ihsPersonnelContactDto.setTelephone("7869879876");
    return ihsPersonnelContactDto;
  }

  /**
   * Mock for Ihs Company Contact Dto.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsCompanyContactDto mockDocIhsCompanyContactForAssetDetails() {
    IhsCompanyContactDto ihsCompanyContactDto = new IhsCompanyContactDto();
    ihsCompanyContactDto.setStreetNo("58");
    ihsCompanyContactDto.setStreetName("Street Name");
    ihsCompanyContactDto.setStreetName1("Street Name 1");
    ihsCompanyContactDto.setAddressLine1("ADDR LINE 1");
    ihsCompanyContactDto.setAddressLine3("ADDR LINE 3");
    ihsCompanyContactDto.setTownName1("Town Name1");
    ihsCompanyContactDto.setPrePostCode("34879");
    ihsCompanyContactDto.setCountryFullName("MALAYSIA");
    ihsCompanyContactDto.setEmail("email@lr.com");
    ihsCompanyContactDto.setTelephone("7869879876");
    ihsCompanyContactDto.setCompanyName("Company Name");
    return ihsCompanyContactDto;
  }

  /**
   * Mock for Principal Dimension.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsPrincipalDimensionsDto mockPrincipalDimensionsForAssetDetails() {
    final Double breadth = 101.99;
    final Double draught = 101.99;
    final Double depth = 101.99;
    final Integer grossTonnage = 101;
    final Double lengthOverall = 102.213;
    final Integer netTonnage = 2;
    final Integer deadWeight = 102;

    IhsPrincipalDimensionsDto dto = new IhsPrincipalDimensionsDto();
    dto.setLengthOverall(lengthOverall);
    dto.setLengthBtwPerpendiculars("20000");
    dto.setBreadthMoulded(breadth);
    dto.setBreadthExtreme(breadth);
    dto.setDraughtMax(draught);
    dto.setDeadWeight(deadWeight);
    dto.setDepthMoulded(depth);
    dto.setGrossTonnage(grossTonnage);
    dto.setNetTonnage(netTonnage);
    dto.setDecks("2");
    dto.setPropulsion("2");
    return dto;
  }

  /**
   * Mock for Equipment Details.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsEquipmentDetailsDto mockEquipmentDetailsForAssetDetails() {
    final Double cableLength = 10.4;
    final String cableGrade = "G1";
    final String cableDiameter = "20";
    final String equipmentLetter = "5";

    IhsEquipment2Dto equipment2Dto = new IhsEquipment2Dto();
    equipment2Dto.setCableLength(cableLength);
    equipment2Dto.setCableGrade(cableGrade);
    equipment2Dto.setCableDiameter(cableDiameter);

    IhsEquipment4Dto equipment4Dto = new IhsEquipment4Dto();
    equipment4Dto.setEquipmentLetter(equipmentLetter);

    IhsEquipmentDetailsDto dto = new IhsEquipmentDetailsDto();
    dto.setEquipment2Dto(equipment2Dto);
    dto.setEquipment4Dto(equipment4Dto);
    return dto;
  }

  /**
   * Test Non-LR Asset's Registry Information In Get Asset Details By AssetId without Former Asset
   * Information.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   * @throws ParseException ParseException.
   */
  @Test
  public void testRegistryInfoForNonLrAssetInGetAssetDetailsByAssetIdWithoutFormerAsset()
      throws IOException, ClassDirectException, InterruptedException, ParseException {
    mockNonEquasisThetisUser();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    ihsAssetDetailsDto.getIhsHist().setKeelLayingDate("");
    IhsPrincipalDimensionsDto ihsPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    IhsEquipmentDetailsDto ihsEquipmentDto = mockEquipmentDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, null);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber("1000019"))
        .thenReturn(Calls.response(ihsPrincipalDto));

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsEquipmentDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    RegistryInformationDto registryInfo = assetDto.getRegistryInformation();
    Date mockKeelLayDateIn = null;
    List<IhsOvnaDto> formerAssetnames = ihsAssetDetailsDto.getIhsAsset().getFormerData();
    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(registryInfo);
    Assert.assertEquals(mockKeelLayDateIn, registryInfo.getKeelLayingDate());
    Assert.assertEquals(2, formerAssetnames.size());
    Assert.assertEquals("", registryInfo.getFormerAssetNames());
  }

  /**
   * Test Non-LR Asset's Registry Information In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   * @throws ParseException ParseException.
   */
  @Test
  public void testRegistryInfoForNonLrAssetInGetAssetDetailsByAssetId()
      throws IOException, ClassDirectException, InterruptedException, ParseException {
    mockNonEquasisThetisUser();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto ihsPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    IhsEquipmentDetailsDto ihsEquipmentDto = mockEquipmentDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, null);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber("1000019"))
        .thenReturn(Calls.response(ihsPrincipalDto));

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsEquipmentDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    RegistryInformationDto registryInfo = assetDto.getRegistryInformation();
    IhsAssetDto ihsAssetDto = ihsAssetDetailsDto.getIhsAsset();
    IhsHistDto ihsHist = ihsAssetDetailsDto.getIhsHist();
    Date mockedBuildDateInput = ihsAssetDto.getDateOfBuild();
    String mockedBuildDate = DateUtils.getFormattedDatetoString(mockedBuildDateInput, DATE_PATTERN_KEEL_AND_BUILD_DATE);
    String mockedYearOfBuild = DateUtils.getFormattedDatetoString(mockedBuildDateInput, DATE_PATTERN_YEAR_ONLY);
    Date mockKeelLayDateIn = DateUtils.getFormattedStringDatetoDate(ihsHist.getKeelLayingDate(), DATE_PATTERN_REVERSE);
    String mockedKeelLayingDate =
        DateUtils.getFormattedDatetoString(mockKeelLayDateIn, DATE_PATTERN_KEEL_AND_BUILD_DATE);
    IhsCbsbDto mockIhsCbsb = ihsAssetDto.getIhsCbsb();
    String mockYard = mockIhsCbsb.getDecodeA() + mockIhsCbsb.getDecodeB();
    Set<String> assetnames = new HashSet<>();
    Set<String> formerAssetNamesList =
        ihsAssetDto.getFormerData().stream().map(IhsOvnaDto::getG01Name).distinct().collect(Collectors.toSet());
    formerAssetNamesList.removeIf(p -> !assetnames.add(p.toLowerCase()));
    formerAssetNamesList.remove(ihsAssetDetailsDto.getIhsAsset().getName());
    String formatAssetNames = formerAssetNamesList.stream().collect(Collectors.joining(","));

    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(registryInfo);
    Assert.assertEquals(ihsAssetDto.getPortName(), registryInfo.getPortOfRegistry());
    Assert.assertEquals(ihsAssetDto.getCallSign(), registryInfo.getCallSign());
    Assert.assertEquals(ihsAssetDto.getOfficialNo(), registryInfo.getOfficialNumber());
    Assert.assertEquals(ihsAssetDto.getIhsAssetType().getStatDeCode(), registryInfo.getAssetTypeDetails());
    Assert.assertEquals(formatAssetNames, registryInfo.getFormerAssetNames());
    Assert.assertEquals(ihsAssetDto.getBuilder(), registryInfo.getBuilder());
    Assert.assertEquals(mockYard, registryInfo.getYard());
    Assert.assertEquals(ihsAssetDto.getYardNumber(), registryInfo.getYardNumber());
    Assert.assertEquals(mockedKeelLayingDate, registryInfo.getKeelLayingDate());
    Assert.assertEquals(mockedBuildDate, registryInfo.getDateOfBuild());
    Assert.assertEquals(mockedYearOfBuild, registryInfo.getYearOfBuild());
    Assert.assertEquals(ihsAssetDto.getCountryOfBuild(), registryInfo.getCountryOfBuild());
    Assert.assertEquals(ihsAssetDto.getFlagName(), registryInfo.getFlag());
    Assert.assertEquals(ihsAssetDto.getMmsi(), registryInfo.getMmsiNumber());
  }

  /**
   * Test Non-LR Asset's Registry Information In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   * @throws ParseException ParseException.
   */
  @Test
  public void testRegistryInfoForLrAssetInGetAssetDetailsByAssetId()
      throws IOException, ClassDirectException, InterruptedException, ParseException {
    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();

    CountryHDto mockCountryHdto = mockCountryHDto();

    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto ihsPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    IhsEquipmentDetailsDto ihsEquipmentDto = mockEquipmentDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber("1000019"))
        .thenReturn(Calls.response(ihsPrincipalDto));

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsEquipmentDto));

    Mockito.when(customerReferenceService.getCountry(Mockito.anyLong())).thenReturn(mockCountryHdto);

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    RegistryInformationDto registryInfo = assetDto.getRegistryInformation();
    IhsAssetDto ihsAssetDto = ihsAssetDetailsDto.getIhsAsset();
    Date mockedBuildDateInput = mockedAsset.getBuildDate();
    String mockedBuildDate = DateUtils.getFormattedDatetoString(mockedBuildDateInput, DATE_PATTERN_KEEL_AND_BUILD_DATE);
    String mockedYearOfBuild = DateUtils.getFormattedDatetoString(mockedBuildDateInput, DATE_PATTERN_YEAR_ONLY);
    String mockedKeelLayingDate =
        DateUtils.getFormattedDatetoString(mockedAsset.getKeelLayingDate(), DATE_PATTERN_KEEL_AND_BUILD_DATE);
    IhsCbsbDto mockIhsCbsb = ihsAssetDto.getIhsCbsb();
    String mockYard = mockIhsCbsb.getDecodeA() + mockIhsCbsb.getDecodeB();
    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(registryInfo);
    Assert.assertEquals(ihsAssetDetailsDto.getIhsAsset().getPortName(), registryInfo.getPortOfRegistry());
    Assert.assertEquals(mockedAsset.getCallSign(), registryInfo.getCallSign());
    Assert.assertEquals(ihsAssetDto.getOfficialNo(), registryInfo.getOfficialNumber());
    Assert.assertEquals(mockedAsset.getAssetType().getName(), registryInfo.getAssetTypeDetails());
    Assert.assertEquals(mockedAsset.getBuilder(), registryInfo.getBuilder());
    Assert.assertEquals(mockYard, registryInfo.getYard());
    Assert.assertEquals(mockedAsset.getYardNumber(), registryInfo.getYardNumber());
    Assert.assertEquals(mockedKeelLayingDate, registryInfo.getKeelLayingDate());
    Assert.assertEquals(mockedBuildDate, registryInfo.getDateOfBuild());
    Assert.assertEquals(mockedYearOfBuild, registryInfo.getYearOfBuild());
    Assert.assertEquals(mockCountryHdto.getName(), registryInfo.getCountryOfBuild());
    Assert.assertEquals(mockedAsset.getFlagStateDto().getName(), registryInfo.getFlag());
    Assert.assertEquals(ihsAssetDto.getMmsi(), registryInfo.getMmsiNumber());
    Assert.assertEquals(registryInfo.getLead(), mockedAsset.getIsLead());
    Assert.assertNotNull(registryInfo.getAssetLifeCycleStatus());
  }

  /**
   * Test LR only Asset's Registry Information In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   * @throws ParseException ParseException.
   */
  @Test
  public void testRegistryInfoForLrAssetOnly()
      throws IOException, ClassDirectException, InterruptedException, ParseException {
    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    mockedAsset.setVersionId(LR_ASSET_VESRSION_ID);

    CountryHDto mockedCountryHdto = mockCountryHDto();

    List<BaseAssetDto> list = new ArrayList<>();
    BaseAssetDto baseAsset = new BaseAssetDto();
    baseAsset.setVersionId(GENERAL_ID);
    baseAsset.setName("Test 1");
    BaseAssetDto baseAsset2 = new BaseAssetDto();
    baseAsset2.setVersionId(2L);
    baseAsset2.setName("TEST 1");

    list.add(baseAsset);
    mockedAsset.setAllVersions(list);

    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto ihsPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    IhsEquipmentDetailsDto ihsEquipmentDto = mockEquipmentDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber("1000019"))
        .thenReturn(Calls.response(ihsPrincipalDto));

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsEquipmentDto));

    Mockito.when(customerReferenceService.getCountry(Mockito.anyLong())).thenReturn(mockedCountryHdto);

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    RegistryInformationDto registryInfo = assetDto.getRegistryInformation();
    IhsAssetDto ihsAssetDto = ihsAssetDetailsDto.getIhsAsset();
    Date mockedBuildDateInput = mockedAsset.getBuildDate();
    String mockedBuildDate = DateUtils.getFormattedDatetoString(mockedBuildDateInput, DATE_PATTERN_KEEL_AND_BUILD_DATE);
    String mockedYearOfBuild = DateUtils.getFormattedDatetoString(mockedBuildDateInput, DATE_PATTERN_YEAR_ONLY);
    String mockedKeelLayingDate =
        DateUtils.getFormattedDatetoString(mockedAsset.getKeelLayingDate(), DATE_PATTERN_KEEL_AND_BUILD_DATE);
    IhsCbsbDto mockIhsCbsb = ihsAssetDto.getIhsCbsb();
    String mockYard = mockIhsCbsb.getDecodeA() + mockIhsCbsb.getDecodeB();
    String formatAssetNames = mockedAsset.getAllVersions().stream()
        .collect(Collectors.mapping(BaseAssetDto::getName, Collectors.joining(",")));

    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(registryInfo);
    Assert.assertEquals(ihsAssetDetailsDto.getIhsAsset().getPortName(), registryInfo.getPortOfRegistry());
    Assert.assertEquals(mockedAsset.getCallSign(), registryInfo.getCallSign());
    Assert.assertEquals(ihsAssetDto.getOfficialNo(), registryInfo.getOfficialNumber());
    Assert.assertEquals(mockedAsset.getAssetType().getName(), registryInfo.getAssetTypeDetails());
    Assert.assertEquals(formatAssetNames, registryInfo.getFormerAssetNames());
    Assert.assertEquals(mockedAsset.getBuilder(), registryInfo.getBuilder());
    Assert.assertEquals(mockYard, registryInfo.getYard());
    Assert.assertEquals(mockedAsset.getYardNumber(), registryInfo.getYardNumber());
    Assert.assertEquals(mockedKeelLayingDate, registryInfo.getKeelLayingDate());
    Assert.assertEquals(mockedBuildDate, registryInfo.getDateOfBuild());
    Assert.assertEquals(mockedYearOfBuild, registryInfo.getYearOfBuild());
    Assert.assertEquals(mockedCountryHdto.getName(), registryInfo.getCountryOfBuild());
    Assert.assertEquals(mockedAsset.getFlagStateDto().getName(), registryInfo.getFlag());
    Assert.assertEquals(ihsAssetDto.getMmsi(), registryInfo.getMmsiNumber());
    Assert.assertEquals(registryInfo.getLead(), mockedAsset.getIsLead());
    Assert.assertNotNull(registryInfo.getAssetLifeCycleStatus());
    Assert.assertNotNull(registryInfo.getFormerAssetNames());
  }

  /**
   * Test when IhsDetails is empty in GetAssetDetails.
   *
   * @throws ClassDirectException ClassDirectException.
   * @throws IOException IOException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testGetAssetDetailsForIhsAndLrAsset() throws ClassDirectException, IOException, InterruptedException {
    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);
    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    Assert.assertNotNull(assetDto);
  }

  /**
   * Test when IhsDetails is empty in GetAssetDetails.
   *
   * @throws ClassDirectException ClassDirectException.
   * @throws IOException IOException.
   * @throws InterruptedException InterruptedException.
   */
  @Test(expected = RecordNotFoundException.class)
  public void testEmptyAssetInGetAssetDetails() throws ClassDirectException, IOException, InterruptedException {
    mockNonEquasisThetisUser();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(null, null);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    Assert.assertNotNull(assetDto);
  }

  /**
   * Test Principal Dimension In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testPrincipalDimensionInRegistryInfoInGetAssetDetailsByAssetId()
      throws IOException, ClassDirectException, InterruptedException {
    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto ihsPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber("1000019")).thenReturn(Calls.response(ihsPrincipalDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    PrincipalDimensionDto principalDto = assetDto.getPrincipalDimension();

    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(principalDto);
    Assert.assertEquals(ihsPrincipalDto.getLengthOverall(), principalDto.getLengthOverall());
    Assert.assertEquals(ihsPrincipalDto.getLengthBtwPerpendiculars(), principalDto.getLengthBetweenPerpendiculars());
    Assert.assertEquals(ihsPrincipalDto.getBreadthMoulded(), principalDto.getBreadthMoulded());
    Assert.assertEquals(ihsPrincipalDto.getBreadthExtreme(), principalDto.getBreadthExtreme());
    Assert.assertEquals(ihsPrincipalDto.getDraughtMax(), principalDto.getDraughtMax());

    // TODO "Air Draught" field is missing in mast. Will address in next PR.

    Assert.assertEquals(ihsPrincipalDto.getDepthMoulded(), principalDto.getDepthMoulded());
    Assert.assertEquals(ihsPrincipalDto.getGrossTonnage(), principalDto.getGrossTonnage());
    Assert.assertEquals(ihsPrincipalDto.getNetTonnage(), principalDto.getNetTonnage());
    Assert.assertEquals(ihsPrincipalDto.getDeadWeight(), principalDto.getDeadWeight());
    Assert.assertEquals(ihsPrincipalDto.getDecks(), principalDto.getDecks());
    Assert.assertEquals(ihsPrincipalDto.getPropulsion(), principalDto.getPropulsion());
  }

  /**
   * Test Principal Dimension In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testEquipmentDetailsInRegistryInfoInGetAssetDetailsByAssetId()
      throws IOException, ClassDirectException, InterruptedException {
    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();

    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsEquipmentDetailsDto ihsEquipmentDto = mockEquipmentDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);

    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsEquipmentDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    EquipmentDetailsDto equipmentDto = assetDto.getEquipmentDetails();
    IhsEquipment2Dto ihsEquipment2Dto = ihsEquipmentDto.getEquipment2Dto();

    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(equipmentDto);
    Assert.assertEquals(ihsEquipment2Dto.getCableLength(), equipmentDto.getCableLength());
    Assert.assertEquals(ihsEquipment2Dto.getCableGrade(), equipmentDto.getCableGrade());
    Assert.assertEquals(ihsEquipment2Dto.getCableDiameter(), equipmentDto.getCableDiameter());
    Assert.assertEquals(ihsEquipmentDto.getEquipment4Dto().getEquipmentLetter(), equipmentDto.getEquipmentLetter());
  }

  /**
   * Test Principal Dimension In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testEquasisThetisEquipmentDetailsInGetAssetDetailsByAssetId()
      throws IOException, ClassDirectException, InterruptedException {
    mockEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsEquipmentDetailsDto ihsEquipmentDto = mockEquipmentDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);

    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsEquipmentDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    EquipmentDetailsDto equipmentDto = assetDto.getEquipmentDetails();

    Assert.assertNotNull(assetDto);
    Assert.assertNull(equipmentDto);
  }

  /**
   * Test Ownership information In Get Asset Details for Equasis/Thetis user.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testEquasisThetisOwnershipInfoInGetAssetDetailsByAssetId() throws ClassDirectException {

    mockEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto mockedPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    IhsPersonnelContactDto mockOwner = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsCompanyContactDto mockedDocDto = mockDocIhsCompanyContactForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedPrincipalDto));

    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockOwner));

    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedDocDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    OwnershipDto ownershipDto = assetDto.getOwnership();
    IhsAssetDto ihsAssetDto = ihsAssetDetailsDto.getIhsAsset();

    List<String> addressList = Arrays.asList(mockOwner.getStreetNo(), mockOwner.getStreetName(),
        mockOwner.getAddressLine1(), mockOwner.getStreet1(), mockOwner.getStreet2(), mockOwner.getAddressLine2(),
        mockOwner.getAddressLine3(), mockOwner.getTownName1(), mockOwner.getTownName2(), mockOwner.getPrePostCode(),
        mockOwner.getPostPostCode(), mockOwner.getFullCountryName());
    final String ihsROAddress =
        addressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));

    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(ownershipDto);

    Assert.assertEquals(ihsAssetDto.getOwner(), ownershipDto.getRegisteredOwner().getName());
    Assert.assertEquals(ihsROAddress, ownershipDto.getRegisteredOwner().getAddress());
    Assert.assertEquals(mockOwner.getEmail(), ownershipDto.getRegisteredOwner().getEmailAddress());
    Assert.assertEquals(mockOwner.getTelephone(), ownershipDto.getRegisteredOwner().getPhoneNumber());
  }

  /**
   * Test Principal Dimension In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testOwnershipInfoInGetAssetDetailsByAssetId() throws ClassDirectException {

    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto mockedPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    IhsEquipmentDetailsDto mockedEquipmentDto = mockEquipmentDetailsForAssetDetails();
    IhsPersonnelContactDto mockOwner = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockGOwner = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockOperator = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockShipManager = mockShipManagerIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockTechManager = mockTechManagerIhsPersonnelContactForAssetDetails();
    IhsCompanyContactDto mockedDocDto = mockDocIhsCompanyContactForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedPrincipalDto));

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedEquipmentDto));

    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockOwner)).thenReturn(Calls.response(mockGOwner))
        .thenReturn(Calls.response(mockOperator)).thenReturn(Calls.response(mockShipManager))
        .thenReturn(Calls.response(mockTechManager));

    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedDocDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    OwnershipDto ownershipDto = assetDto.getOwnership();
    IhsAssetDto ihsAssetDto = ihsAssetDetailsDto.getIhsAsset();
    IhsGroupFleetDto ihsGroupDto = ihsAssetDetailsDto.getIhsGroupFleet();

    List<String> addressList = Arrays.asList(mockOwner.getStreetNo(), mockOwner.getStreetName(),
        mockOwner.getAddressLine1(), mockOwner.getStreet1(), mockOwner.getStreet2(), mockOwner.getAddressLine2(),
        mockOwner.getAddressLine3(), mockOwner.getTownName1(), mockOwner.getTownName2(), mockOwner.getPrePostCode(),
        mockOwner.getPostPostCode(), mockOwner.getFullCountryName());
    final String ihsROAddress =
        addressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));

    List<String> ihsGOAddressList = Arrays.asList(mockGOwner.getStreetNo(), mockGOwner.getStreetName(),
        mockGOwner.getAddressLine1(), mockGOwner.getStreet1(), mockGOwner.getStreet2(), mockGOwner.getAddressLine2(),
        mockGOwner.getAddressLine3(), mockGOwner.getTownName1(), mockGOwner.getTownName2(), mockGOwner.getPrePostCode(),
        mockGOwner.getPostPostCode(), mockGOwner.getFullCountryName());
    final String ihsGOAddress =
        ihsGOAddressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));

    List<String> ihsDocAddressList =
        Arrays.asList(mockedDocDto.getStreetNo(), mockedDocDto.getStreetName(), mockedDocDto.getAddressLine1(),
            mockedDocDto.getStreetName1(), mockedDocDto.getStreetName2(), mockedDocDto.getAddressLine2(),
            mockedDocDto.getAddressLine3(), mockedDocDto.getTownName1(), mockedDocDto.getTownName2(),
            mockedDocDto.getPrePostCode(), mockedDocDto.getPostPostCode(), mockedDocDto.getCountryFullName());
    final String ihsDocAddress =
        ihsDocAddressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));

    List<String> mockOperatorAddressList =
        Arrays.asList(mockOperator.getStreetNo(), mockOperator.getStreetName(), mockOperator.getAddressLine1(),
            mockOperator.getStreet1(), mockOperator.getStreet2(), mockOperator.getAddressLine2(),
            mockOperator.getAddressLine3(), mockOperator.getTownName1(), mockOperator.getTownName2(),
            mockOperator.getPrePostCode(), mockOperator.getPostPostCode(), mockOperator.getFullCountryName());
    final String mockOperatorAddress =
        mockOperatorAddressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));

    List<String> ihsTechManagerAddressList =
        Arrays.asList(mockTechManager.getStreetNo(), mockTechManager.getStreetName(), mockTechManager.getAddressLine1(),
            mockTechManager.getStreet1(), mockTechManager.getStreet2(), mockTechManager.getAddressLine2(),
            mockTechManager.getAddressLine3(), mockTechManager.getTownName1(), mockTechManager.getTownName2(),
            mockTechManager.getPrePostCode(), mockTechManager.getPostPostCode(), mockTechManager.getFullCountryName());
    final String ihsTechManagerAddress = ihsTechManagerAddressList.stream().filter(value -> !StringUtils.isEmpty(value))
        .collect(Collectors.joining(" "));

    List<String> ihsShipAddressList =
        Arrays.asList(mockShipManager.getStreetNo(), mockShipManager.getStreetName(), mockShipManager.getAddressLine1(),
            mockShipManager.getStreet1(), mockShipManager.getStreet2(), mockShipManager.getAddressLine2(),
            mockShipManager.getAddressLine3(), mockShipManager.getTownName1(), mockShipManager.getTownName2(),
            mockShipManager.getPrePostCode(), mockShipManager.getPostPostCode(), mockShipManager.getFullCountryName());
    final String ihsShipAddress =
        ihsShipAddressList.stream().filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.joining(" "));

    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(ownershipDto);

    Assert.assertEquals(ihsAssetDto.getOwner(), ownershipDto.getRegisteredOwner().getName());
    Assert.assertEquals(ihsROAddress, ownershipDto.getRegisteredOwner().getAddress());
    Assert.assertEquals(mockOwner.getEmail(), ownershipDto.getRegisteredOwner().getEmailAddress());
    Assert.assertEquals(mockOwner.getTelephone(), ownershipDto.getRegisteredOwner().getPhoneNumber());

    Assert.assertEquals(ihsAssetDto.getGbo(), ownershipDto.getGroupOwner().getName());
    Assert.assertEquals(ihsGOAddress, ownershipDto.getGroupOwner().getAddress());
    Assert.assertEquals(mockGOwner.getEmail(), ownershipDto.getGroupOwner().getEmailAddress());
    Assert.assertEquals(mockGOwner.getTelephone(), ownershipDto.getGroupOwner().getPhoneNumber());

    Assert.assertEquals(ihsGroupDto.getGroupOwnerName(), ownershipDto.getGroupOwner().getName());
    Assert.assertEquals(ihsAssetDto.getOperator(), ownershipDto.getOperator().getName());
    Assert.assertEquals(mockOperatorAddress, ownershipDto.getOperator().getAddress());
    Assert.assertEquals(mockOperator.getEmail(), ownershipDto.getOperator().getEmailAddress());
    Assert.assertEquals(mockOperator.getTelephone(), ownershipDto.getOperator().getPhoneNumber());
    Assert.assertEquals(ihsAssetDto.getDocCompany(), ownershipDto.getDoc().getName());
    Assert.assertEquals(ihsDocAddress, ownershipDto.getDoc().getAddress());
    Assert.assertEquals(mockedDocDto.getEmail(), ownershipDto.getDoc().getEmailAddress());
    Assert.assertEquals(mockedDocDto.getTelephone(), ownershipDto.getDoc().getPhoneNumber());
    Assert.assertEquals(ihsAssetDto.getTechManager(), ownershipDto.getTechnicalManager().getName());
    Assert.assertEquals(ihsTechManagerAddress, ownershipDto.getTechnicalManager().getAddress());
    Assert.assertEquals(mockTechManager.getEmail(), ownershipDto.getTechnicalManager().getEmailAddress());
    Assert.assertEquals(mockTechManager.getTelephone(), ownershipDto.getTechnicalManager().getPhoneNumber());
    Assert.assertEquals(ihsAssetDto.getShipManager(), ownershipDto.getShipManager().getName());
    Assert.assertEquals(ihsShipAddress, ownershipDto.getShipManager().getAddress());
    Assert.assertEquals(mockShipManager.getEmail(), ownershipDto.getShipManager().getEmailAddress());
    Assert.assertEquals(mockShipManager.getTelephone(), ownershipDto.getShipManager().getPhoneNumber());
  }

  /**
   * Tests success scenario to get Principal Dimension In Get Asset Details By AssetId if inner mast
   * api call fails to get personal information.
   *
   * @throws ClassDirectException error in executing mast-ihs-query/ error in execution flow/ any
   *         interrupted exception in executing threads.
   */
  @Test
  public void testOwnershipInfo() throws ClassDirectException {

    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto mockedPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    IhsEquipmentDetailsDto mockedEquipmentDto = mockEquipmentDetailsForAssetDetails();
    IhsPersonnelContactDto mockOwner = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockGOwner = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockOperator = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockShipManager = mockShipManagerIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockTechManager = mockTechManagerIhsPersonnelContactForAssetDetails();
    IhsCompanyContactDto mockedDocDto = mockDocIhsCompanyContactForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedPrincipalDto));

    Mockito.when(ihsServiceDelegate.getEquipmentDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedEquipmentDto));


    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(eq("5000001")))
        .thenReturn(Calls.failure(new IOException("mock")));
    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(eq("6000001")))
        .thenReturn(Calls.failure(new IOException("mock")));
    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(eq("3000109")))
        .thenReturn(Calls.failure(new IOException("mock")));
    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(eq("3000108")))
        .thenReturn(Calls.failure(new IOException("mock")));
    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(eq("3000107")))
        .thenReturn(Calls.failure(new IOException("mock")));

    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedDocDto));

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    OwnershipDto ownershipDto = assetDto.getOwnership();
    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(ownershipDto);
    Assert.assertNotNull(ownershipDto.getGroupOwner());
    Assert.assertNotNull(ownershipDto.getGroupOwner().getName());
    Assert.assertEquals(ownershipDto.getGroupOwner().getName(), "GOwn1");
    Assert.assertNotNull(ownershipDto.getOperator());
    Assert.assertNotNull(ownershipDto.getOperator().getName());
    Assert.assertEquals(ownershipDto.getOperator().getName(), "KL Operator");
    Assert.assertNotNull(ownershipDto.getRegisteredOwner());
    Assert.assertNotNull(ownershipDto.getRegisteredOwner().getName());
    Assert.assertEquals(ownershipDto.getRegisteredOwner().getName(), "ROwnr1");
    Assert.assertNotNull(ownershipDto.getShipManager());
    Assert.assertNotNull(ownershipDto.getShipManager());
    Assert.assertEquals(ownershipDto.getShipManager().getName(), "KL Ship Manager");
    Assert.assertNotNull(ownershipDto.getTechnicalManager());
    Assert.assertNotNull(ownershipDto.getTechnicalManager().getName());
    Assert.assertEquals(ownershipDto.getTechnicalManager().getName(), "TMAN1");
    Assert.assertNotNull(ownershipDto.getDoc());

    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockOwner)).thenReturn(Calls.response(mockGOwner))
        .thenReturn(Calls.response(mockOperator)).thenReturn(Calls.response(mockShipManager))
        .thenReturn(Calls.response(mockTechManager));

    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.failure(new IOException("mock")));

    AssetDetailsDto assetDto1 = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    OwnershipDto ownershipDto1 = assetDto.getOwnership();
    Assert.assertNotNull(assetDto1);
    Assert.assertNotNull(ownershipDto1);
    Assert.assertNotNull(ownershipDto1.getGroupOwner());
    Assert.assertNotNull(ownershipDto1.getGroupOwner().getName());
    Assert.assertEquals(ownershipDto1.getGroupOwner().getName(), "GOwn1");
    Assert.assertNotNull(ownershipDto1.getOperator());
    Assert.assertNotNull(ownershipDto1.getOperator().getName());
    Assert.assertEquals(ownershipDto1.getOperator().getName(), "KL Operator");
    Assert.assertNotNull(ownershipDto1.getRegisteredOwner());
    Assert.assertNotNull(ownershipDto1.getRegisteredOwner().getName());
    Assert.assertEquals(ownershipDto1.getRegisteredOwner().getName(), "ROwnr1");
    Assert.assertNotNull(ownershipDto1.getShipManager());
    Assert.assertNotNull(ownershipDto1.getShipManager());
    Assert.assertEquals(ownershipDto1.getShipManager().getName(), "KL Ship Manager");
    Assert.assertNotNull(ownershipDto1.getTechnicalManager());
    Assert.assertNotNull(ownershipDto1.getTechnicalManager().getName());
    Assert.assertEquals(ownershipDto1.getTechnicalManager().getName(), "TMAN1");
    Assert.assertNotNull(ownershipDto.getDoc());
    Assert.assertEquals(ownershipDto1.getDoc().getName(), "Docking Company");
  }

  /**
   * Test Principal Dimension In Get Asset Details By AssetId.
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testRulesetDetailsInGetAssetDetailsByAssetId()
      throws IOException, ClassDirectException, InterruptedException {
    mockNonEquasisThetisUser();
    AssetHDto mockedAsset = mockAssetHdtosforLrAssets();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, mockedAsset);

    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    AssetDetailsDto assetDto = assetService.getAssetDetailsByAssetId(LR_ASSET_ID, USER_ID, false);
    RulesetDetailsDto rulesetDetails = assetDto.getRulesetDetailsDto();

    Assert.assertNotNull(assetDto);
    Assert.assertEquals(mockedAsset.getMachineryClassNotation(), rulesetDetails.getMachineryNotation());
    Assert.assertEquals(mockedAsset.getDescriptiveNote(), rulesetDetails.getDescriptiveNote());
    Assert.assertEquals(mockedAsset.getHullIndicator(), rulesetDetails.getHullIndicator());
  }

  /**
   * Test Get Asset Details By AssetId for Only IHS Assets (Not LR Asset).
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testGetAssetDetailsForOnlyIhsAsset() throws IOException, ClassDirectException, InterruptedException {
    mockNonEquasisThetisUser();
    IhsAssetDetailsDto ihsAssetDetailsDto = mockIhsAssetDetailsForAssetDetails();
    IhsPrincipalDimensionsDto ihsPrincipalDto = mockPrincipalDimensionsForAssetDetails();
    FlagStateHDto flagStateHDto = mockFlagState();
    IhsPersonnelContactDto mockOwner = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockGOwner = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockOperator = mockOperatorIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockShipManager = mockShipManagerIhsPersonnelContactForAssetDetails();
    IhsPersonnelContactDto mockTechManager = mockTechManagerIhsPersonnelContactForAssetDetails();
    IhsCompanyContactDto mockedDocDto = mockDocIhsCompanyContactForAssetDetails();

    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(ihsAssetDetailsDto, null);

    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    Mockito.when(ihsServiceDelegate.getPrincipalDimensionsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsPrincipalDto));

    Mockito.when(flagStateDelegate.getFlagById(Mockito.anyLong())).thenReturn(Calls.response(flagStateHDto));

    Mockito.when(ihsServiceDelegate.getPersonnelContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockOwner)).thenReturn(Calls.response(mockGOwner))
        .thenReturn(Calls.response(mockOperator)).thenReturn(Calls.response(mockShipManager))
        .thenReturn(Calls.response(mockTechManager));

    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockedDocDto));

    AssetDetailsDto assetDetailsDto = assetService.getAssetDetailsByAssetId(1000019L, USER_ID, false);
    Assert.assertNotNull(assetDetailsDto.getRegistryInformation());
    Assert.assertNotNull(assetDetailsDto.getPrincipalDimension());
    Assert.assertNull(assetDetailsDto.getRulesetDetailsDto());
    Assert.assertNull(assetDetailsDto.getEquipmentDetails());
    Assert.assertNotNull(assetDetailsDto.getOwnership());
  }

  /**
   * Test Get Asset Details By AssetId for Only LR Assets (Not IHS Asset).
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testGetAssetDetailsForOnlyLrAsset() throws IOException, ClassDirectException, InterruptedException {
    mockNonEquasisThetisUser();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(null, mockAssetHdtosforLrAssets());
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    AssetDetailsDto assetDetailsDto = assetService.getAssetDetailsByAssetId(1000019L, USER_ID, false);
    Assert.assertNotNull(assetDetailsDto.getRegistryInformation());
    Assert.assertNull(assetDetailsDto.getPrincipalDimension());
    Assert.assertNotNull(assetDetailsDto.getRulesetDetailsDto());
    Assert.assertNull(assetDetailsDto.getEquipmentDetails());
    Assert.assertNull(assetDetailsDto.getOwnership());
  }

  /**
   * Tests get asset details by assetId for only LR Assets (Restricted).
   *
   * @throws IOException IOException.
   * @throws ClassDirectException ClassDirectException.
   * @throws InterruptedException InterruptedException.
   */
  @Test
  public void testGetAssetDetailsForOnlyLrAssetForRestricted()
      throws IOException, ClassDirectException, InterruptedException {
    mockNonEquasisThetisUser();
    Call<MultiAssetPageResourceDto> assetCaller = mockAssets(null, mockAssetHdtosforLrAssets());
    when(assetServiceDelegate.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(assetCaller);

    List<Long> assetIds = new ArrayList<>();
    assetIds.add(1000078L);
    Mockito.when(subFleetService.getSubFleetById(Mockito.anyString())).thenReturn(assetIds);

    AssetDetailsDto assetDetailsDto = assetService.getAssetDetailsByAssetId(1000019L, USER_ID, false);
    Assert.assertNotNull(assetDetailsDto.getRegistryInformation());
    Assert.assertNull(assetDetailsDto.getPrincipalDimension());
    Assert.assertNull(assetDetailsDto.getRulesetDetailsDto());
    Assert.assertNull(assetDetailsDto.getEquipmentDetails());
    Assert.assertNull(assetDetailsDto.getOwnership());
  }

  /**
   * @throws IOException exception.
   * @throws ClassDirectException exception.
   */
  @Test
  public void testGetCustomersByIMONumber() throws IOException, ClassDirectException {

    Mockito.when(ihsServiceDelegate.getAssetDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockIhsAssetDetailsForAssetDetails()));

    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockDocIhsCompanyContactForAssetDetails()));

    IhsAssetClientDto ihsAssetClients = assetService.getCustomersByImoNumber("1000019");
    Assert.assertNotNull(ihsAssetClients);
    Assert.assertEquals("TEST 1", ihsAssetClients.getIhsAssetDetailsDto().getIhsAsset().getName());
    Assert.assertEquals("LNG Tanker",
        ihsAssetClients.getIhsAssetDetailsDto().getIhsAsset().getIhsAssetType().getStatDeCode());
    Assert.assertEquals("1000019", ihsAssetClients.getIhsAssetDetailsDto().getIhsAsset().getId());
    Assert.assertNotNull(ihsAssetClients.getCustomers());
    Assert.assertEquals("Company Name", ihsAssetClients.getCustomers().get(0).getCompanyName());
    final IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    final IhsCompanyContactDto ihsCompanyContactDto = new IhsCompanyContactDto();
    Mockito.when(ihsServiceDelegate.getAssetDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsAssetDetailsDto));
    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsCompanyContactDto));
    IhsAssetClientDto ihsAssetClients1 = assetService.getCustomersByImoNumber("1000019");
    Assert.assertNotNull(ihsAssetClients1);
    Assert.assertNotNull(ihsAssetClients.getCustomers());
    // test with out customer codes
    Mockito.when(ihsServiceDelegate.getAssetDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockIhsAssetDetailsForAssetDetailsWithoutCodes()));
    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(ihsCompanyContactDto));
    IhsAssetClientDto ihsAssetClients2 = assetService.getCustomersByImoNumber("1000019");
    Assert.assertNotNull(ihsAssetClients2);
    Assert.assertNotNull(ihsAssetClients2.getCustomers());
  }

  /**
   * @throws IOException exception.
   * @throws ClassDirectException exception.
   */
  @Test
  public void testGetCustomersByIMONumberFail() throws IOException, ClassDirectException {

    Mockito.when(ihsServiceDelegate.getAssetDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.failure(new IOException()));
    IhsAssetClientDto ihsAssetClients = assetService.getCustomersByImoNumber("1000019");
    Assert.assertNotNull(ihsAssetClients);

    Mockito.when(ihsServiceDelegate.getAssetDetailsByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.response(mockIhsAssetDetailsForAssetDetails()));
    Mockito.when(ihsServiceDelegate.getCompanyContactByImoNumber(Mockito.anyString()))
        .thenReturn(Calls.failure(new IOException()));
    IhsAssetClientDto ihsAssetClients1 = assetService.getCustomersByImoNumber("1000019");
    Assert.assertNotNull(ihsAssetClients1);
    Assert.assertEquals("TEST 1", ihsAssetClients1.getIhsAssetDetailsDto().getIhsAsset().getName());
    Assert.assertEquals("LNG Tanker",
        ihsAssetClients1.getIhsAssetDetailsDto().getIhsAsset().getIhsAssetType().getStatDeCode());
    Assert.assertEquals("1000019", ihsAssetClients1.getIhsAssetDetailsDto().getIhsAsset().getId());
    Assert.assertNotNull(ihsAssetClients1.getCustomers());
    Assert.assertTrue(ihsAssetClients1.getCustomers().isEmpty());
  }

  /**
   * Mock for Ihs Asset Details without customer role codes.
   *
   * @return value.
   * @throws IOException exception.
   */
  private IhsAssetDetailsDto mockIhsAssetDetailsForAssetDetailsWithoutCodes() {
    IhsHistDto ihsHistDto = new IhsHistDto();
    IhsAssetTypeDto ihsAssetType = new IhsAssetTypeDto();
    ihsAssetType.setStatDeCode("LNG Tanker");
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("1000019");
    ihsAssetDto.setIhsAssetType(ihsAssetType);
    ihsAssetDto.setCallSign("KL Call Sign");
    ihsAssetDto.setOfficialNo("010-987-001");
    ihsAssetDto.setName("Alan Tank");
    ihsAssetDto.setBuilder("A builder");
    ihsAssetDto.setYardNumber("y1");
    ihsAssetDto.setDateOfBuild(new Date());
    ihsAssetDto.setCountryOfBuild("MY");
    ihsAssetDto.setFlag("1");
    ihsAssetDto.setShipManager("KL Ship Manager");
    ihsAssetDto.setOperator("KL Operator");
    ihsAssetDto.setDocCompany("Docking Company");
    ihsAssetDto.setTechManager("TMAN1");
    ihsAssetDto.setOwner("ROwnr1");
    ihsAssetDto.setGbo("GOwn1");
    ihsAssetDto.setMmsi("013456789");
    ihsAssetDetailsDto.setId("1000019");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    ihsAssetDetailsDto.setIhsHist(ihsHistDto);
    return ihsAssetDetailsDto;
  }

  /**
   * Mock Equasis User.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  private void mockNonEquasisThetisUser() throws ClassDirectException {
    mockUserProfile(USER_ID);
    doReturn(securityContext.getAuthentication()).when(Mockito.spy(SecurityContextHolder.getContext()))
        .getAuthentication();
  }

  /**
   * Mock Equasis User.
   *
   * @throws ClassDirectException ClassDirectException.
   */
  private void mockEquasisThetisUser() throws ClassDirectException {
    mockUserProfile(null);
    doReturn(securityContext.getAuthentication()).when(Mockito.spy(SecurityContextHolder.getContext()))
        .getAuthentication();
  }

  /**
   * Mock user profiles.
   *
   * @param userId user id.
   * @throws ClassDirectException when error.
   */
  private void mockUserProfile(final String userId) throws ClassDirectException {
    final UserProfiles user = new UserProfiles();
    user.setUserId(userId);
    user.setRestrictAll(false);
    user.setClientCode("3000107");
    user.setClientType(CustomerRolesEnum.DOC_COMPANY.toString());
    final Roles userRole = new Roles();
    if (userId != null) {
      userRole.setRoleId(LR_ADMIN.getId());
      userRole.setRoleName(LR_ADMIN.toString());
    } else {
      userRole.setRoleId(EQUASIS.getId());
      userRole.setRoleName(EQUASIS.toString());
    }
    Set<Roles> roles = new HashSet<Roles>();
    roles.add(userRole);
    user.setRoles(roles);

    when(userProfileService.getUser(anyString())).thenReturn(user);
  }

  /**
   * @throws Exception exception.
   */
  @Before
  public void setUp() throws Exception {
    securityContext = SecurityContextHolder.getContext();
    SecurityContext context = new SecurityContextImpl();
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    listOfRoles.add(new SimpleGrantedAuthority(LR_ADMIN.toString()));
    CDAuthToken token = new CDAuthToken(listOfRoles);
    token.setAuthenticated(true);
    UserProfiles user = new UserProfiles();
    token.setDetails(user);
    token.setPrincipal(USER_ID);
    context.setAuthentication(token);
    SecurityContextHolder.setContext(context);
  }

  /**
   *
   */
  @After
  public void tearDown() {
    SecurityContextHolder.setContext(securityContext);
    reset(userProfileService);
  }

  /**
   * @author RKaneysan.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create asset services.
     *
     * @return asset services.
     */
    @Override
    @Bean
    public AssetService assetService() {
      return new AssetServiceImpl();
    }
  }
}
