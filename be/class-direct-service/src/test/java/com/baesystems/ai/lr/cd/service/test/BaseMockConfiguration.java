package com.baesystems.ai.lr.cd.service.test;

import org.mockito.Mockito;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.certificate.CertificateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.customer.CustomerRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.employee.EmployeeRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.InternalUserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.UserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AttachmentReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.CertificateReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.FlagStateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.JobReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.PortRegistryService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ProductReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.tasks.TaskRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.LREmailDomainsDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.NotificationsDao;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SecurityDAO;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.processor.EnableMasqueradeProcessor;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.ClassDirectSecurityService;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.ServiceHelper;
import com.baesystems.ai.lr.cd.mail.MailService;
import com.baesystems.ai.lr.cd.service.ResourceProviderScanner;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.CodicilService;
import com.baesystems.ai.lr.cd.service.asset.DefectService;
import com.baesystems.ai.lr.cd.service.asset.DeficiencyService;
import com.baesystems.ai.lr.cd.service.asset.JobService;
import com.baesystems.ai.lr.cd.service.asset.MajorNCNService;
import com.baesystems.ai.lr.cd.service.asset.StatutoryFindingService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.asset.reference.ReferenceDataService;
import com.baesystems.ai.lr.cd.service.attachment.reference.AttachmentReferenceService;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.cache.CacheAspect;
import com.baesystems.ai.lr.cd.service.cache.CacheContainer;
import com.baesystems.ai.lr.cd.service.cache.CacheScheduler;
import com.baesystems.ai.lr.cd.service.cache.CacheService;
import com.baesystems.ai.lr.cd.service.certificate.CertificateReferenceService;
import com.baesystems.ai.lr.cd.service.certificate.CertificateService;
import com.baesystems.ai.lr.cd.service.countryfiles.CountryFileService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.export.ActionableItemExportService;
import com.baesystems.ai.lr.cd.service.export.AssetExportService;
import com.baesystems.ai.lr.cd.service.export.AssetNoteExportService;
import com.baesystems.ai.lr.cd.service.export.CertificateDownloadService;
import com.baesystems.ai.lr.cd.service.export.ChecklistExportService;
import com.baesystems.ai.lr.cd.service.export.CocExportService;
import com.baesystems.ai.lr.cd.service.export.EspExportService;
import com.baesystems.ai.lr.cd.service.export.MNCNAttachmentService;
import com.baesystems.ai.lr.cd.service.export.MajorNCNExportService;
import com.baesystems.ai.lr.cd.service.export.PmsTaskListExportService;
import com.baesystems.ai.lr.cd.service.export.ReportExportService;
import com.baesystems.ai.lr.cd.service.export.StatutoryDeficienciesExportService;
import com.baesystems.ai.lr.cd.service.export.StatutoryFindingsExportService;
import com.baesystems.ai.lr.cd.service.export.TMReportDownloadService;
import com.baesystems.ai.lr.cd.service.export.TaskListExportService;
import com.baesystems.ai.lr.cd.service.export.UsersExportService;
import com.baesystems.ai.lr.cd.service.favourites.FavouritesService;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.notifications.NotificationsService;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.cd.service.reference.CustomerReferenceService;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.survey.SurveyService;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.cd.service.userprofile.helper.ExternalSSOProvider;
import com.baesystems.ai.lr.cd.service.userprofile.helper.InternalSSOProvider;
import com.baesystems.ai.lr.cd.service.userprofile.helper.SSOProviderFactory;
import com.baesystems.ai.lr.cd.service.userprofile.helper.SSOProviderFactoryImpl;
import com.baesystems.ai.lr.cd.service.userprofile.helper.SSOUserProvider;


/**
 * Base Spring mock configuration.
 * <p>
 * All mock services goes here. Override the method to place real objects.
 *
 * @author MSidek
 */
@Configuration
public class BaseMockConfiguration {
  /**
   * sso provider factory.
   *
   * @return sso provider factory.
   */
  @Bean
  public SSOProviderFactory ssoProviderFactory() {
    return new SSOProviderFactoryImpl();
  }

  /**
   *
   * @return internal SSO user provider.
   */
  @Bean
  public SSOUserProvider internalSSOProvider() {
    return new InternalSSOProvider();
  }

  /**
   *
   * @return external SSO user provider.
   */
  @Bean
  public SSOUserProvider externalSSOProvider() {
    return new ExternalSSOProvider();
  }

  /**
   * @return Internal LR AD user retrofit service.
   */
  @Bean
  public InternalUserRetrofitService internalUserRetrofitService() {
    return Mockito.mock(InternalUserRetrofitService.class);
  }

  /**
   * @return LR email domains DAO.
   */
  @Bean
  public LREmailDomainsDao lrEmailDomainsDao() {
    return Mockito.mock(LREmailDomainsDao.class);
  }

  /**
   * Default cache version.
   */
  private final Long defaultVersion = 49L;

  /**
   * Mock {@link FlagsAssociationsService}.
   *
   * @return mock object.
   */
  @Bean
  public FlagsAssociationsService flagsAssociationsService() {
    return Mockito.mock(FlagsAssociationsService.class);
  }

  /**
   * Mock {@link AssetReferenceService}.
   *
   * @return mock object.
   */
  @Bean
  public AssetReferenceService assetReferenceService() {
    return Mockito.mock(AssetReferenceService.class);
  }

  /**
   * Mock {@link AssetReferenceRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public AssetReferenceRetrofitService assetReferenceDelegate() {
    return Mockito.mock(AssetReferenceRetrofitService.class);
  }

  /**
   * Mock {@link CustomerRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public CustomerRetrofitService customerRetrofitDelegate() {
    return Mockito.mock(CustomerRetrofitService.class);
  }

  /**
   * Create mail service.
   *
   * @return service.
   */
  @Bean
  public MailService mailService() {
    return Mockito.mock(MailService.class);
  }

  /**
   * Create asset services.
   *
   * @return asset services.
   */
  @Bean
  public AssetService assetService() {
    return Mockito.mock(AssetService.class);
  }

  /**
   * Mocked {@link AssetRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public AssetRetrofitService assetRetrofitService() {
    return Mockito.mock(AssetRetrofitService.class);
  }

  /**
   * Create {@link ActionableItemService} bean.
   *
   * @return actionable item service.
   */
  @Bean
  public ActionableItemService actionableItemService() {
    return Mockito.mock(ActionableItemService.class);
  }

  /**
   * Create {@link CoCService} bean.
   *
   * @return coc service.
   */
  @Bean
  public CoCService cocService() {
    return Mockito.mock(CoCService.class);
  }

  /**
   * Create {@link CodicilService} bean.
   *
   * @return codicil service.
   */
  @Bean
  public CodicilService codicilService() {
    return Mockito.mock(CodicilService.class);
  }

  /**
   * Create {@link AssetNoteService} bean.
   *
   * @return assetNote service.
   */
  @Bean
  public AssetNoteService assetNoteService() {
    return Mockito.mock(AssetNoteService.class);
  }

  /**
   * Spring bean for {@link FlagStateRetrofitService}.
   *
   * @return service.
   */
  @Bean
  public FlagStateRetrofitService flagStateRetrofitService() {
    return Mockito.mock(FlagStateRetrofitService.class);
  }

  /**
   * Create service for {@link FlagStateService}.
   *
   * @return service.
   */
  @Bean
  public FlagStateService flagStateService() {
    return Mockito.mock(FlagStateService.class);
  }

  /**
   * Create object for ServiceReferenceServiceImpl.
   *
   * @return ServiceReferenceServiceImpl
   */
  @Bean
  public ServiceReferenceService serviceReferenceService() {
    return Mockito.mock(ServiceReferenceService.class);
  }

  /**
   * @return serviceHelperImpl
   */
  @Bean
  public ServiceHelper serviceHelper() {
    return Mockito.mock(ServiceHelper.class);
  }

  /**
   * Create {@Link ServiceReferenceRetrofitService}.
   *
   * @return serviceReferenceRetrofitService.
   */
  @Bean
  public ServiceReferenceRetrofitService serviceReferenceRetrofitService() {
    return Mockito.mock(ServiceReferenceRetrofitService.class);
  }

  /**
   * Create favorites repository.
   *
   * @return favorites repository.
   */
  @Bean
  public FavouritesRepository favouritesRepository() {
    return Mockito.mock(FavouritesRepository.class);
  }

  /**
   * Favourite service.
   *
   * @return service.
   */
  @Bean
  public FavouritesService favouritesService() {
    return Mockito.mock(FavouritesService.class);
  }

  /**
   * Create survey service.
   *
   * @return service.
   */
  @Bean
  public SurveyService surveyService() {
    return Mockito.mock(SurveyService.class);
  }

  /**
   * Create PortRegistryService repository.
   *
   * @return PortRegistryService repository. Mock port registry service.
   */
  @Bean
  public PortRegistryService portRegistryService() {
    return Mockito.mock(PortRegistryService.class);
  }

  /**
   * Properties place holder.
   *
   * @return placeholder.
   */
  @Bean
  public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
    final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
    configurer.setLocations(new ClassPathResource("errormessage-config.properties"),
        new ClassPathResource("lr-ad-config.properties"), new ClassPathResource("sitekit-config.properties"),
        new ClassPathResource("classdirect-config.properties"),
        new ClassPathResource("classdirect-security-config.properties"),
        new ClassPathResource("export-config.properties"), new ClassPathResource("asset-services.properties"),
        new ClassPathResource("class-direct-s3.properties"), new ClassPathResource("test-credential.properties"),
        new ClassPathResource("test-asset-service.properties"));
    configurer.setSearchSystemEnvironment(true);
    configurer.setSystemPropertiesMode(PropertyPlaceholderConfigurer.SYSTEM_PROPERTIES_MODE_OVERRIDE);
    return configurer;
  }

  /**
   * Return cache.
   *
   * @return cache object.
   */
  @Bean
  public CacheContainer cache() {
    return new CacheContainer();
  }

  /**
   * Caching aspect.
   *
   * @return aspect.
   */
  @Bean
  public CacheAspect cacheAspect() {
    return Mockito.spy(new CacheAspect());
  }

  /**
   * Create portService repository.
   *
   * @return portService repository.
   */
  @Bean
  public PortService portService() {
    return Mockito.mock(PortService.class);
  }

  /**
   * Create classDirectSecurityService class-direct security service.
   *
   * @return classDirectSecurityService class-direct security service.
   */
  @Bean
  public ClassDirectSecurityService classDirectSecurityService() {
    return Mockito.mock(ClassDirectSecurityService.class);
  }

  /**
   * Create aesSecurityService AES security service.
   *
   * @return aesSecurityService AES security service.
   */
  @Bean
  public AesSecurityService aesSecurityService() {
    return Mockito.mock(AesSecurityService.class);
  }

  /**
   * Create securityDAO.
   *
   * @return securityDAO.
   */
  @Bean
  public SecurityDAO securityDAO() {
    return Mockito.mock(SecurityDAO.class);
  }

  /**
   * Create jobService repository.
   *
   * @return jobService repository.
   */
  @Bean
  public JobService jobService() {
    return Mockito.mock(JobService.class);
  }

  /**
   * Mock {@link JobRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public JobRetrofitService jobRetrofitService() {
    return Mockito.mock(JobRetrofitService.class);
  }

  /**
   * Create jobReferenceService repository.
   *
   * @return jobReferenceService repository.
   */
  @Bean
  public JobReferenceService jobReferenceService() {
    return Mockito.mock(JobReferenceService.class);
  }

  /**
   * Mock {@link JobReferenceRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public JobReferenceRetrofitService jobReferenceDelegate() {
    return Mockito.mock(JobReferenceRetrofitService.class);
  }

  /**
   * Resource provider scanner.
   *
   * @return resource provider scanner.
   */
  @Bean
  public ResourceProviderScanner scanner() {
    return new ResourceProviderScanner();
  }

  /**
   * Initialize {@link ReferenceDataService}.
   *
   * @return service.
   */
  @Bean
  public ReferenceDataService referenceDataService() {
    final ReferenceDataService service = Mockito.mock(ReferenceDataService.class);
    Mockito.when(service.getCurrentVersion()).thenReturn(defaultVersion);

    return service;
  }

  /**
   * Mock {@link NotificationsService}.
   *
   * @return mock object.
   */
  @Bean
  public NotificationsService notificationsService() {
    return Mockito.mock(NotificationsService.class);
  }

  /**
   * Mock {@link UserProfileDao}.
   *
   * @return mock object.
   */
  @Bean
  public UserProfileDao userProfileDao() {
    return Mockito.mock(UserProfileDao.class);
  }

  /**
   * Mock {@link UserProfileService}.
   *
   * @return mock object.
   */
  @Bean
  public UserProfileService userProfileService() {
    return Mockito.mock(UserProfileService.class);
  }

  /**
   * Mock {@link NotificationsDao}.
   *
   * @return mock object.
   */
  @Bean
  public NotificationsDao notificationsDao() {
    return Mockito.mock(NotificationsDao.class);
  }

  /**
   * Initialize cache scheduler.
   *
   * @return scheduler
   */
  @Bean
  public CacheScheduler scheduler() {
    return new CacheScheduler();
  }

  /**
   * Create subfleet service.
   *
   * @return service.
   */
  @Bean
  public SubFleetService subFleetService() {
    return Mockito.mock(SubFleetService.class);
  }

  /**
   * Create subfleetDAO.
   *
   * @return subfleetDAO.
   */
  @Bean
  public SubFleetDAO subfleetDAO() {
    return Mockito.mock(SubFleetDAO.class);
  }

  /**
   * Create {@link DefectService} bean.
   *
   * @return defect service.
   */
  @Bean
  public DefectService defectService() {
    return Mockito.mock(DefectService.class);
  }

  /**
   * Mock {@link TaskRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public TaskRetrofitService taskRetrofitService() {
    return Mockito.mock(TaskRetrofitService.class);
  }

  /**
   * Mock {@link IhsRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public IhsRetrofitService ihsRetrofitService() {
    return Mockito.mock(IhsRetrofitService.class);
  }

  /**
   * Mock {@link TaskService}.
   *
   * @return mock object.
   */
  @Bean
  public TaskService taskService() {
    return Mockito.mock(TaskService.class);
  }

  /**
   * Mock {@link TaskReferenceRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public TaskReferenceRetrofitService taskReferenceRetrofitService() {
    return Mockito.mock(TaskReferenceRetrofitService.class);
  }

  /**
   * Create TaskReferenceService repository.
   *
   * @return TaskReferenceService repository.
   */
  @Bean
  public TaskReferenceService taskReferenceService() {
    return Mockito.mock(TaskReferenceService.class);
  }

  /**
   * Mock {@link UserRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public UserRetrofitService userRetrofitService() {
    return Mockito.mock(UserRetrofitService.class);
  }

  /**
   * Mock {@link UsersExportService}.
   *
   * @return mock object.
   */
  @Bean
  public UsersExportService usersExportService() {
    return Mockito.mock(UsersExportService.class);
  }

  /**
   * Mock {@link AssetExportService}.
   *
   * @return mock object.
   */
  @Bean
  public AssetExportService assetExportService() {
    return Mockito.mock(AssetExportService.class);
  }

  /**
   * Mock {@link CertificateRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public CertificateRetrofitService certificateRetrofitService() {
    return Mockito.mock(CertificateRetrofitService.class);
  }

  /**
   * Mock {@link CertificateReferenceRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public CertificateReferenceRetrofitService certificateReferenceRetrofitService() {
    return Mockito.mock(CertificateReferenceRetrofitService.class);
  }

  /**
   * Mock {@link CertificateService}.
   *
   * @return mock object.
   */
  @Bean
  public CertificateService certificateService() {
    return Mockito.mock(CertificateService.class);
  }

  /**
   * Mock {@link CertificateReferenceService}.
   *
   * @return mock object.
   */
  @Bean
  public CertificateReferenceService certificateReferenceService() {
    return Mockito.mock(CertificateReferenceService.class);
  }

  /**
   * Mock {@link PmsTaskListExportService}.
   *
   * @return mock object.
   */
  @Bean
  public PmsTaskListExportService pmsTaskListExportService() {
    return Mockito.mock(PmsTaskListExportService.class);
  }


  /**
   * Mock {@link CocExportService}.
   *
   * @return mock object.
   */
  @Bean
  public CocExportService cocExportService() {
    return Mockito.mock(CocExportService.class);
  }

  /**
   * Mock {@link ActionableItemExportService}.
   *
   * @return mock object.
   */
  @Bean
  public ActionableItemExportService actionableItemExportService() {
    return Mockito.mock(ActionableItemExportService.class);
  }

  /**
   * Mock {@link TaskListExportService}.
   *
   * @return mock object.
   */
  @Bean
  public TaskListExportService taskListExportService() {
    return Mockito.mock(TaskListExportService.class);
  }

  /**
   * Mock {@link AssetNoteExportService}.
   *
   * @return mock object.
   */
  public AssetNoteExportService assetNoteExportService() {
    return Mockito.mock(AssetNoteExportService.class);
  }

  /**
   * Mock {@link ChecklistExportService}.
   *
   * @return mock object.
   */
  public ChecklistExportService checklistExportService() {
    return Mockito.mock(ChecklistExportService.class);
  }

  /**
   * Mock {@link AmazonStorageService}.
   *
   * @return mock object.
   */
  @Bean
  public AmazonStorageService amazonStorageService() {
    return Mockito.mock(AmazonStorageService.class);
  }

  /**
   * Mock (@Link {@link ProductReferenceService}.
   *
   * @return mock object.
   */
  @Bean
  public ProductReferenceService productReferenceService() {
    return Mockito.mock(ProductReferenceService.class);
  }

  /**
   * Mock (@Link {@link ProductReferenceRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public ProductReferenceRetrofitService productReferenceRetrofitService() {
    return Mockito.mock(ProductReferenceRetrofitService.class);
  }

  /**
   * Mock {@link CertificateDownloadService}.
   *
   * @return mock object.
   */
  @Bean
  public CertificateDownloadService certificateDownloadService() {
    return Mockito.mock(CertificateDownloadService.class);
  }

  /**
   * Mock {@link ReportExportService}.
   *
   * @return mock object.
   */
  @Bean
  public ReportExportService reportExportService() {
    return Mockito.mock(ReportExportService.class);
  }

  /**
   * Mock {@link EmployeeRetrofitService}.
   *
   * @return mock object.
   */
  @Bean
  public EmployeeRetrofitService employeeRetrofitService() {
    return Mockito.mock(EmployeeRetrofitService.class);
  }

  /**
   * Mock {@link AuditService}.
   *
   * @return mock object.
   */
  @Bean
  public AuditService auditService() {
    return Mockito.mock(AuditService.class);
  }

  /**
   * Mock {@link CacheService}.
   *
   * @return mock object.
   */
  @Bean
  public CacheService cacheService() {
    return Mockito.mock(CacheService.class);
  }

  /**
   * Mock {@link EnableMasqueradeProcessor}.
   *
   * @return mock object.
   */
  @Bean
  public EnableMasqueradeProcessor enableMasqueradeProcessor() {
    return Mockito.mock(EnableMasqueradeProcessor.class);
  }

  /**
   * Create AttachmentService repository.
   *
   * @return AttachmentService repository.
   */
  @Bean
  public AttachmentRetrofitService attachmentRetrofitService() {
    return Mockito.mock(AttachmentRetrofitService.class);
  }

  /**
   * Mock {@link EmployeeReferenceService}.
   *
   * @return mock object.
   */

  @Bean
  public EmployeeReferenceService employeeReferenceService() {
    return Mockito.mock(EmployeeReferenceService.class);
  }

  /**
   * Mock {@link AttachmentReferenceService}.
   *
   * @return mock Object.
   */
  public AttachmentReferenceService attachmentReferenceService() {
    return Mockito.mock(AttachmentReferenceService.class);
  }

  /**
   * Create AttachmentReferenceRetrofitService repository.
   *
   * @return AttachmentReferenceRetrofitService repository.
   */
  @Bean
  public AttachmentReferenceRetrofitService attachmentReferenceRetrofitService() {
    return Mockito.mock(AttachmentReferenceRetrofitService.class);
  }

  /**
   * Mock {@link TMReportDownloadService}.
   *
   * @return mock object.
   */
  @Bean
  public TMReportDownloadService tMReportDownloadService() {
    return Mockito.mock(TMReportDownloadService.class);
  }

  /**
   * Mock {@link CountryFileService}.
   *
   * @return mock object.
   */
  @Bean
  public CountryFileService countryFileService() {
    return Mockito.mock(CountryFileService.class);
  }

  /**
   * Mock {@link EspExportService}.
   *
   * @return mock object.
   */
  @Bean
  public EspExportService espExportService() {
    return Mockito.mock(EspExportService.class);
  }

  /**
   * Mock {@link DeficiencyService}.
   *
   * @return mock object.
   */
  @Bean
  public DeficiencyService deficiencyService() {
    return Mockito.mock(DeficiencyService.class);
  }

  /**
   * Mock {@link StatutoryFindingService}.
   *
   * @return mock object.
   */
  @Bean
  public StatutoryFindingService statutoryService() {
    return Mockito.mock(StatutoryFindingService.class);
  }

  /**
   * Mock {@link StatutoryDeficienciesExportService}.
   *
   * @return mock object.
   */
  @Bean
  public StatutoryDeficienciesExportService sdExportService() {
    return Mockito.mock(StatutoryDeficienciesExportService.class);
  }

  /**
   * Mock {@link StatutoryFindingsExportService}.
   *
   * @return mock object.
   */
  @Bean
  public StatutoryFindingsExportService sfExportService() {
    return Mockito.mock(StatutoryFindingsExportService.class);
  }

  /**
   * Returns mocked {@link CustomerReferenceService}.
   *
   * @return the mocked {@link CustomerReferenceService}.
   */
  @Bean
  public CustomerReferenceService customerReferenceService() {
    return Mockito.mock(CustomerReferenceService.class);
  }

  /**
   * Returns mocked {@link MajorNCNService}.
   *
   * @return the mocked {@link MajorNCNService}.
   */
  @Bean
  public MajorNCNService majorNCNService() {
    return Mockito.mock(MajorNCNService.class);
  }

  /**
   * Returns mocked {@link MajorNCNExportService}.
   *
   * @return the mocked {@link MajorNCNExportService}.
   */
  @Bean
  public MajorNCNExportService majorNCNExportService() {
    return Mockito.mock(MajorNCNExportService.class);
  }

  /**
   * Returns mocked {@link MNCNAttachmentService}.
   *
   * @return the mocked {@link MNCNAttachmentService}.
   */
  @Bean
  public MNCNAttachmentService mNCNAttachmentService() {
    return Mockito.mock(MNCNAttachmentService.class);
  }
}
