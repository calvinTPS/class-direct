package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.baesystems.ai.lr.cd.be.cs10.CS10Service;
import com.baesystems.ai.lr.cd.be.cs10.dto.CS10FileVersion;
import com.baesystems.ai.lr.cd.be.cs10.exception.CS10Exception;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.certificate.CertificateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.export.CertificateAttachDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.certificate.CertificateService;
import com.baesystems.ai.lr.cd.service.export.CertificateDownloadService;
import com.baesystems.ai.lr.cd.service.export.impl.CertificateDownloadServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;

import retrofit2.mock.Calls;


/**
 * Provides unit test methods for {@link CertificateDownloadService}.
 *
 * @author sbollu
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CertificateDownloadServiceTest.Config.class)
public class CertificateDownloadServiceTest {

  /**
   * The {@link CertificateRetrofitService} from Spring context.
   */
  @Autowired
  private CertificateRetrofitService certificateRetrofitService;

  /**
   * The {@link CertificateService} from Spring context.
   */
  @Autowired
  private CertificateService certificateService;

  /**
   * The {@link CertificateDownloadService} from Spring context.
   */
  @Autowired
  private CertificateDownloadService certificateDownLoadService;

  /**
   * CS10 service.
   */
  @Autowired
  private CS10Service cs10Service;


  /**
   * The {@link SecurityContext} from Spring context.
   */
  private SecurityContext securityCtx;

  /**
   * This method will provide prerequisite mocking of user authentication.
   */
  @Before
  public void setUp() {
    securityCtx = SecurityContextHolder.getContext();
    SecurityContext context = new SecurityContextImpl();
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    CDAuthToken token = new CDAuthToken(listOfRoles);

    token.setDetails(user);
    token.setAuthenticated(true);
    context.setAuthentication(token);
    SecurityContextHolder.setContext(context);
  }


  /**
   * Destroys context after all tests executed.
   */
  @After
  public void tearDown() {
    SecurityContextHolder.setContext(securityCtx);
  }



  /**
   * Tests success scenario for
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)}.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void testDownloadCertificateInfo() throws ClassDirectException {
    final CertificateAttachDto dto = new CertificateAttachDto();
    dto.setJobId(1L);
    dto.setCertId(8L);

    SupplementaryInformationPageResourceDto certAttachDto = new SupplementaryInformationPageResourceDto();
    List<SupplementaryInformationDto> attachListDto = new ArrayList<SupplementaryInformationDto>();
    SupplementaryInformationDto attachDto = new SupplementaryInformationDto();
    attachDto.setId(8L);
    attachDto.setAttachmentUrl("19876");
    attachDto.setDocumentVersion(1L);
    attachListDto.add(attachDto);
    certAttachDto.setContent(attachListDto);

    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(1L, 8L))
        .thenReturn(Calls.response(certAttachDto));
    Mockito.when(certificateService.getCertificateAttachment(1L, 8L)).thenReturn(certAttachDto);
    // call method
    final StringResponse response = certificateDownLoadService.downloadCertificateInfo(dto);

    Assert.assertNotNull(response);
  }

  /**
   * Tests success scenario for
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)},null document
   * version Id of CS10.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void testDownloadCertificateforDocumentVersionNullValues() throws ClassDirectException {
    final CertificateAttachDto dto = new CertificateAttachDto();
    dto.setJobId(1L);
    dto.setCertId(8L);

    SupplementaryInformationPageResourceDto certAttachDto = new SupplementaryInformationPageResourceDto();
    List<SupplementaryInformationDto> attachListDto = new ArrayList();
    SupplementaryInformationDto attachDto = new SupplementaryInformationDto();
    attachDto.setId(8L);
    attachDto.setAttachmentUrl("19876");
    attachDto.setDocumentVersion(null);
    attachListDto.add(attachDto);
    certAttachDto.setContent(attachListDto);

    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(1L, 8L))
        .thenReturn(Calls.response(certAttachDto));
    Mockito.when(certificateService.getCertificateAttachment(1L, 8L)).thenReturn(certAttachDto);
    // call method
    mockCS10FileVersion();
    final StringResponse response = certificateDownLoadService.downloadCertificateInfo(dto);

    Assert.assertNotNull(response);
  }

  /**
   * Tests failure scenario for
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)},null values of
   * node id and document version Id of CS10.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void testDownloadCertificateforNullValues() throws ClassDirectException {

    final CertificateAttachDto dto = new CertificateAttachDto();
    dto.setJobId(1L);
    dto.setCertId(8L);

    SupplementaryInformationPageResourceDto certAttachDto = new SupplementaryInformationPageResourceDto();
    List<SupplementaryInformationDto> attachListDto = new ArrayList();
    SupplementaryInformationDto attachDto = new SupplementaryInformationDto();
    attachDto.setId(8L);
    attachDto.setAttachmentUrl(null);
    attachDto.setDocumentVersion(null);
    attachListDto.add(attachDto);
    certAttachDto.setContent(attachListDto);

    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(1L, 8L))
        .thenReturn(Calls.response(certAttachDto));
    Mockito.when(certificateService.getCertificateAttachment(1L, 8L)).thenReturn(certAttachDto);
    // call method
    final StringResponse response = certificateDownLoadService.downloadCertificateInfo(dto);
  }


  /**
   * Tests failure scenario for
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)},result list is
   * null.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testDownloadCertificateforListNullValues() throws ClassDirectException {

    final CertificateAttachDto dto = new CertificateAttachDto();
    dto.setJobId(1L);
    dto.setCertId(8L);

    SupplementaryInformationPageResourceDto certAttachDto = new SupplementaryInformationPageResourceDto();
    List<SupplementaryInformationDto> attachListDto = null;

    certAttachDto.setContent(attachListDto);

    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(1L, 8L))
        .thenReturn(Calls.response(certAttachDto));
    Mockito.when(certificateService.getCertificateAttachment(1L, 8L)).thenReturn(certAttachDto);
    // call method
    final StringResponse response = certificateDownLoadService.downloadCertificateInfo(dto);
  }

  /**
   * Tests failure scenario for
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)},result list is
   * empty.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testDownloadCertificateforEmptyListValues() throws ClassDirectException {

    final CertificateAttachDto dto = new CertificateAttachDto();
    dto.setJobId(1L);
    dto.setCertId(8L);

    SupplementaryInformationPageResourceDto certAttachDto = new SupplementaryInformationPageResourceDto();
    List<SupplementaryInformationDto> attachListDto = Collections.emptyList();

    certAttachDto.setContent(attachListDto);

    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(1L, 8L))
        .thenReturn(Calls.response(certAttachDto));
    Mockito.when(certificateService.getCertificateAttachment(1L, 8L)).thenReturn(certAttachDto);
    // call method
    mockCS10FileVersion();
    final StringResponse response = certificateDownLoadService.downloadCertificateInfo(dto);
  }


  /**
   * Tests failure scenario for
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)},if any IO
   * exception.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testDownloadCertificateInfoFail() throws ClassDirectException {

    final CertificateAttachDto dto = new CertificateAttachDto();
    dto.setJobId(2L);
    dto.setCertId(9L);
    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(2L, 9L))
        .thenReturn(Calls.failure(new IOException("IO Exception")));
    Mockito.when(certificateService.getCertificateAttachment(2L, 9L))
        .thenThrow(new ClassDirectException("IO Exception"));

    // call method
    mockCS10FileVersion();
    certificateDownLoadService.downloadCertificateInfo(dto);
  }

  /**
   * Tests failure scenario for
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)},if any number
   * format exception.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testDownloadCertificateInfoNumberFormatException() throws ClassDirectException {

    final CertificateAttachDto dto = new CertificateAttachDto();
    dto.setJobId(14L);
    dto.setCertId(10L);

    SupplementaryInformationPageResourceDto certAttachDto = new SupplementaryInformationPageResourceDto();
    List<SupplementaryInformationDto> attachListDto = new ArrayList<SupplementaryInformationDto>();
    SupplementaryInformationDto attachDto = new SupplementaryInformationDto();
    attachDto.setId(10L);
    attachDto.setAttachmentUrl("XYZ");
    attachListDto.add(attachDto);
    certAttachDto.setContent(attachListDto);

    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(14L, 10L))
        .thenReturn(Calls.response(certAttachDto));
    Mockito.when(certificateService.getCertificateAttachment(14L, 10L)).thenReturn(certAttachDto);

    // call method
    mockCS10FileVersion();
    certificateDownLoadService.downloadCertificateInfo(dto);
  }

  /**
   * Mock cs10 file version.
   */
  private void mockCS10FileVersion() {
    try {
      final CS10FileVersion fileVersion = new CS10FileVersion();
      fileVersion.setFilename("test.docx");
      fileVersion.setFileType("type/xml");
      Mockito.when(cs10Service.getFileVersion(Mockito.anyInt(), Mockito.anyInt()))
          .thenReturn(fileVersion);
    } catch (CS10Exception exception) {
      exception.printStackTrace();
    }
  }

  /**
   * Provides Spring in-class configurations.
   *
   * @author sbollu
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Creates {@link CertificateDownloadService}.
     *
     * @return mock certificate download service.
     */
    @Override
    @Bean
    public CertificateDownloadService certificateDownloadService() {
      return new CertificateDownloadServiceImpl();
    }

    /**
     * Mock cs10 service.
     *
     * @return mock service.
     */
    @Bean
    public CS10Service cs10Service() {
      return Mockito.mock(CS10Service.class);
    }
  }

}
