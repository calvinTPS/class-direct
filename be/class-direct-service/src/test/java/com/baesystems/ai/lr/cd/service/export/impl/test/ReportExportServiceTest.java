package com.baesystems.ai.lr.cd.service.export.impl.test;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetItemHierarchyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetModelHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RegistryInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportContentHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.service.SurveyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedWorkItemAttributeValueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.ReportUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.export.ReportExportService;
import com.baesystems.ai.lr.cd.service.export.impl.ReportExportServiceImpl;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.certificates.CertificateDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.ncns.MajorNCNDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyWithTaskListDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.CodicilStatus;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.mock.Calls;


/**
 * Provides unit test for ReportExportService.
 *
 * @author yng
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = ReportExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class ReportExportServiceTest {

  /**
   * The {@link ReportExportService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private ReportExportService reportExportService;
  /**
   * The {@link AmazonStorageService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link JobRetrofitService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The {@link AssetService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link AssetReferenceService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link EmployeeReferenceService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;

  /**
   * The {@link ServiceReferenceService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private ServiceReferenceService serviceService;

  /**
   * The {@link JobReferenceService} from {@link ReportExportServiceTest.Config} class.
   */
  @Autowired
  private JobReferenceService jobReferenceService;
  /**
   * The {@link PortService} from Spring context.
   */
  @Autowired
  private PortService portService;

  /**
   * The {@link AssetRetrofitService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The directory test package path.
   */
  private static final String PATH = "data/FARAppendix2TestPackage/";
  /**
   * The {@link TaskReferenceService} from Spring context.
   */
  @Autowired
  private TaskReferenceService taskReferenceService;

  /**
   * Provides mock reset before any test class is executed.
   */
  @Before
  public void resetMock() {
    Mockito.reset(amazonStorageService, jobRetrofitService, assetService, serviceService, employeeReferenceService);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests FAR pdf generation with real test data obtained from MAST team.
   *
   * @throws IOException if json read or write fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws JsonParseException if json parsing fail.
   * @throws ClassDirectException if API call or executor fail.
   *
   */
  @Test
  public final void testRealTestCaseForAppendix2()
      throws JsonParseException, JsonMappingException, IOException, ClassDirectException {


    mockServiceCatalogueDataForAppendix2();

    // create query
    final ReportExportDto query = new ReportExportDto();
    query.setJobId(1L);
    query.setReportId(1L);

    // mock data
    when(taskReferenceService.getAllowedAttributeValues()).thenReturn(mockMaxAllowedAttributeValue());
    Mockito.when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobData()));
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAssetData());
    Mockito.when(assetService.getAssetDetailsByAssetId(Matchers.anyLong(), Matchers.anyString(), Matchers.anyBoolean()))
        .thenReturn(mockAssetDetailData());
    Mockito.when(jobRetrofitService.getSurveysByJobIdAndReportId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockFARDataForAppendix2()));

    Mockito.when(assetRetrofitService.getAssetItemsModel(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockAssetModel()));

    Mockito.when(serviceService.getServiceCreditStatuses()).thenReturn(mockServiceCreditStatusData());
    Mockito.when(employeeReferenceService.getEmployee(Matchers.anyLong())).thenReturn(mockEmployeeData());
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");
    Mockito.when(serviceService.getServiceCatalogues()).thenReturn(mockServiceCatalogueDataForAppendix2());
    Mockito.when(jobReferenceService.getResolutionStatuses()).thenReturn(mockResolutionStatusData());
    Mockito.when(assetReferenceService.getActionTakenList()).thenReturn(mockActionTakenData());

    final StringResponse response = reportExportService.downloadPdf(query);

    Assert.assertNotNull(response);
  }

  /**
   * Returns real reference data mocking for appendix 2 testing.
   *
   * @return the list of allowed attribute value mock reference data.
   * @throws IOException if json read or write fail.
   */
  public final List<AllowedWorkItemAttributeValueHDto> mockMaxAllowedAttributeValue() throws IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    List<AllowedWorkItemAttributeValueHDto> content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/AllowedAttributeValues.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), new TypeReference<List<AllowedWorkItemAttributeValueHDto>>() {

    });

    return content;
  }

  /**
   * Returns real reference data mocking for appendix 2 testing.
   *
   * @return the list of service catalogue mock reference data.
   * @throws IOException if json read or write fail.
   */
  public final List<ServiceCatalogueHDto> mockServiceCatalogueDataForAppendix2() throws IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    List<ServiceCatalogueHDto> content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(PATH + "serviceCatalogueRefData.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), new TypeReference<List<ServiceCatalogueHDto>>() {

    });

    return content;
  }

  /**
   * Creates FAR content mock data for appendix 2 testing.
   *
   * @return the mocked FAR content data.
   * @throws IOException if json read or write fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws JsonParseException if json parsing fail.
   */
  public final ReportDto mockFARDataForAppendix2() throws JsonParseException, JsonMappingException, IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    ReportContentHDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(PATH + "reportContent.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), ReportContentHDto.class);

    final ReportDto report = new ReportDto();
    report.setId(1L);
    StringBuffer classReco = new StringBuffer();
    classReco.append("Class Recommendation Text");
    classReco.append("\r\n");
    classReco.append("Class Recommendation Text");
    classReco.append("\r\n");
    classReco.append("Class Recommendation Text");
    report.setClassRecommendation(classReco.toString());
    report.setIssueDate(new GregorianCalendar(2017, 1, 1).getTime());
    report.setIssuedBy(new LinkResource(1L));
    report.setReportType(new LinkResource(1L));
    report.setReportVersion(1L);
    report.setAuthorisedBy(new LinkResource(1L));
    report.setAuthorisationDate(new GregorianCalendar(2017, 1, 1).getTime());

    final JsonNode contentNode = mapper.convertValue(content, JsonNode.class);

    report.setContent(contentNode);

    return report;
  }

  /**
   * Creates Asset model content mock data for appendix 2 testing.
   *
   * @return the mocked FAR content data.
   * @throws IOException if json read or write fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws JsonParseException if json parsing fail.
   */
  private AssetModelHDto mockAssetModel() throws JsonParseException, JsonMappingException, IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    AssetModelHDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(PATH + "assetModel.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), AssetModelHDto.class);

    return content;
  }


  /**
   * Tests success scenario for FAR pdf generation.
   *
   * @throws IOException if json read or write fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws JsonParseException if json parsing fail.
   * @throws ClassDirectException if API call or executor fail.
   */
  @Test
  public final void testGenerateFARPDF()
      throws JsonParseException, JsonMappingException, IOException, ClassDirectException {
    // create query
    final ReportExportDto query = new ReportExportDto();
    query.setJobId(1L);
    query.setReportId(1L);

    // mock data
    Mockito.when(assetRetrofitService.getAssetItemsModel(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockAssetModel()));
    Mockito.when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobData()));
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAssetData());
    Mockito.when(assetService.getAssetDetailsByAssetId(Matchers.anyLong(), Matchers.anyString(), Matchers.anyBoolean()))
        .thenReturn(mockAssetDetailData());
    Mockito.when(jobRetrofitService.getSurveysByJobIdAndReportId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockFARData()));
    Mockito.when(serviceService.getServiceCreditStatuses()).thenReturn(mockServiceCreditStatusData());
    Mockito.when(employeeReferenceService.getEmployee(Matchers.anyLong())).thenReturn(mockEmployeeData());
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");
    Mockito.when(serviceService.getServiceCatalogues()).thenReturn(mockServiceCatalogueData());
    Mockito.when(jobReferenceService.getResolutionStatuses()).thenReturn(mockResolutionStatusData());
    Mockito.when(assetReferenceService.getActionTakenList()).thenReturn(mockActionTakenData());
    Mockito.when(assetReferenceService.getFlagState(Matchers.anyLong())).thenReturn(mockFlagData());
    Mockito.when(portService.getPort(Matchers.anyLong())).thenReturn(mockRegistryData());

    final StringResponse response = reportExportService.downloadPdf(query);

    Assert.assertNotNull(response);
  }

  /**
   * Tests success scenario for FAR pdf generation when coc content null.
   *
   * @throws ClassDirectException if API call or executor fail.
   */
  @Test
  public final void testGenerateFARPDFWithCOCNull() throws ClassDirectException {
    // create query
    final ReportExportDto query = new ReportExportDto();
    query.setJobId(1L);
    query.setReportId(1L);

    // mock data
    Mockito.when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobData()));
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAssetData());
    Mockito.when(assetService.getAssetDetailsByAssetId(Matchers.anyLong(), Matchers.anyString(), Matchers.anyBoolean()))
        .thenReturn(mockAssetDetailData());
    Mockito.when(jobRetrofitService.getSurveysByJobIdAndReportId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockFARData()));
    Mockito.when(serviceService.getServiceCreditStatuses()).thenReturn(mockServiceCreditStatusData());
    Mockito.when(employeeReferenceService.getEmployee(Matchers.anyLong())).thenReturn(mockEmployeeData());
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");
    Mockito.when(serviceService.getServiceCatalogues()).thenReturn(mockServiceCatalogueData());
    Mockito.when(jobReferenceService.getResolutionStatuses()).thenReturn(mockResolutionStatusData());
    Mockito.when(assetReferenceService.getActionTakenList()).thenReturn(mockActionTakenData());
    Mockito.when(portService.getPort(Matchers.anyLong())).thenReturn(mockRegistryData());

    final StringResponse response = reportExportService.downloadPdf(query);

    Assert.assertNotNull(response);
  }

  /**
   * Tests success scenario for FSR pdf generation.
   *
   * @throws IOException if json read or write fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws JsonParseException if json parsing fail.
   * @throws ClassDirectException if API call or executor fail.
   */
  @Test
  public final void testGenerateFSRPDF()
      throws JsonParseException, JsonMappingException, IOException, ClassDirectException {
    // create query
    final ReportExportDto query = new ReportExportDto();
    query.setJobId(1L);
    query.setReportId(1L);

    // mock data
    Mockito.when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobData()));
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAssetData());
    Mockito.when(assetService.getAssetDetailsByAssetId(Matchers.anyLong(), Matchers.anyString(), Matchers.anyBoolean()))
        .thenReturn(mockAssetDetailData());
    Mockito.when(assetRetrofitService.getAssetItemsModel(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockAssetModel()));
    Mockito.when(jobRetrofitService.getSurveysByJobIdAndReportId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockFSRData()));
    Mockito.when(serviceService.getServiceCreditStatuses()).thenReturn(mockServiceCreditStatusData());
    Mockito.when(employeeReferenceService.getEmployee(Matchers.anyLong())).thenReturn(mockEmployeeData());
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");
    Mockito.when(serviceService.getServiceCatalogues()).thenReturn(mockServiceCatalogueData());
    Mockito.when(jobReferenceService.getResolutionStatuses()).thenReturn(mockResolutionStatusData());
    Mockito.when(assetReferenceService.getActionTakenList()).thenReturn(mockActionTakenData());
    Mockito.when(assetReferenceService.getFlagState(Matchers.anyLong())).thenReturn(mockFlagData());
    Mockito.when(portService.getPort(Matchers.anyLong())).thenReturn(mockRegistryData());

    final StringResponse response = reportExportService.downloadPdf(query);

    Assert.assertNotNull(response);
  }

  /**
   * Tests success scenario for
   * {@link ReportExportServiceImpl#getGroupedTask(ReportContentHDto, Map)}.
   *
   * @throws JsonParseException if json parsing fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws IOException if json read or write fail.
   */
  @Test
  public final void testMethodGetGroupedTask() throws JsonParseException, JsonMappingException, IOException {
    final ReportDto report = mockFARData();

    final ObjectMapper mapper = new ObjectMapper();
    final ReportContentHDto content = mapper.readValue(report.getContent(), ReportContentHDto.class);

    Map<String, Object> model = new HashMap<String, Object>();
    model.put("content", content);

    Mockito.when(serviceService.getServiceCatalogues()).thenReturn(mockServiceCatalogueData());

    final ReportExportServiceImpl impl = (ReportExportServiceImpl) reportExportService;

    final Map<ProductCatalogueDto, Map<SurveyHDto, Map<AssetItemHierarchyHDto, List<WorkItemHDto>>>> result =
        ReportUtils.getGroupedTask(model, impl.generateServiceCatalogueLookup(), taskReferenceService, null, false);

    // test not null
    Assert.assertNotNull(result);

    // check total primary key
    Assert.assertEquals(2, result.size());
  }

  /**
   * Returns service catalogue mock data.
   *
   * @return the service catalogue mock data.
   */
  public final List<ServiceCatalogueHDto> mockServiceCatalogueData() {
    final ServiceCatalogueHDto data1 = new ServiceCatalogueHDto();
    data1.setId(1L);
    data1.setCode("AAA");
    data1.setName("Service one");
    data1.setProductCatalogue(new ProductCatalogueDto());
    data1.getProductCatalogue().setId(1L);
    data1.getProductCatalogue().setName("MARPOL I (Oil)");
    data1.getProductCatalogue().setDisplayOrder(3);
    data1.setDisplayOrder(1);

    final ServiceCatalogueHDto data2 = new ServiceCatalogueHDto();
    data2.setId(2L);
    data2.setCode("BBB");
    data2.setName("Service two");
    data2.setProductCatalogue(new ProductCatalogueDto());
    data2.getProductCatalogue().setId(2L);
    data2.getProductCatalogue().setName("postponed");
    data2.getProductCatalogue().setDisplayOrder(Integer.MAX_VALUE);
    data2.setDisplayOrder(3);

    final ServiceCatalogueHDto data3 = new ServiceCatalogueHDto();
    data3.setId(3L);
    data3.setCode("CCC");
    data3.setName("Service three");
    data3.setProductCatalogue(new ProductCatalogueDto());
    data3.getProductCatalogue().setId(2L);
    data3.getProductCatalogue().setName("Product two");
    data3.getProductCatalogue().setDisplayOrder(1);
    data3.setDisplayOrder(2);

    final ServiceCatalogueHDto data4 = new ServiceCatalogueHDto();
    data4.setId(4L);
    data4.setCode("ddd");
    data4.setName("Service four");
    data4.setProductCatalogue(new ProductCatalogueDto());
    data4.getProductCatalogue().setId(1L);
    data4.getProductCatalogue().setName("Product one");
    data4.getProductCatalogue().setDisplayOrder(5);
    data4.setDisplayOrder(6);

    final ServiceCatalogueHDto data5 = new ServiceCatalogueHDto();
    data5.setId(5L);
    data5.setCode("CCC");
    data5.setName("Service five");
    data5.setProductCatalogue(new ProductCatalogueDto());
    data5.getProductCatalogue().setId(2L);
    data5.getProductCatalogue().setName("postponed");
    data5.getProductCatalogue().setDisplayOrder(4);
    data5.setDisplayOrder(5);

    final List<ServiceCatalogueHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);
    data.add(data4);
    data.add(data5);

    return data;

  }

  /**
   * Returns FAR content mock data.
   *
   * @return the FAR content mock data.
   */
  public final ReportDto mockFARData() {
    final ReportContentHDto content = new ReportContentHDto();
    content.setWipCocs(mockCocData());
    content.setWipActionableItems(mockAiData());
    content.setWipAssetNotes(mockAnData());
    content.setSurveys(mockSurveyData());
    content.setTasks(mockTaskData());
    content.setStatutoryFindings(mockStatutoryFindingData());
    content.setCertificates(mockCertificateData());
    content.setCertificateActions(mockCertificateActionData());
    content.setWipMNCNs(mockMajorNonConformities());
    content.setCurrentFlagH(mockCurrentFlagData());
    content.setProposedFlagH(mockProposedFlagData());

    final ReportDto report = new ReportDto();
    report.setId(1L);
    report.setClassRecommendation("Class Recommendation Text. Class Recommendation Text. Class Recommendation Text.");
    report.setIssueDate(new GregorianCalendar(2017, 1, 1).getTime());
    report.setIssuedBy(new LinkResource(1L));
    report.setReportType(new LinkResource(2L));
    report.setReportVersion(1L);
    report.setAuthorisedBy(new LinkResource(1L));
    report.setAuthorisationDate(new GregorianCalendar(2017, 1, 1).getTime());

    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode contentNode = mapper.convertValue(content, JsonNode.class);

    report.setContent(contentNode);

    return report;
  }

  /**
   * Returns FAR content mock data with null content in coc and tasks.
   *
   * @return the FAR content mock data.
   */
  public final ReportDto mockFARDataWithCOCNull() {
    final ReportContentHDto content = new ReportContentHDto();
    content.setWipCocs(null);
    content.setWipActionableItems(mockAiData());
    content.setWipAssetNotes(mockAnData());
    content.setSurveys(mockSurveyData());
    content.setTasks(null);
    content.setStatutoryFindings(mockStatutoryFindingData());
    content.setCertificates(mockCertificateData());
    content.setCertificateActions(mockCertificateActionData());
    content.setWipMNCNs(mockMajorNonConformities());

    final ReportDto report = new ReportDto();
    report.setId(1L);
    report.setClassRecommendation("Class Recommendation Text. Class Recommendation Text. Class Recommendation Text.");
    report.setIssueDate(new GregorianCalendar(2017, 1, 1).getTime());
    report.setIssuedBy(new LinkResource(1L));
    report.setReportType(new LinkResource(2L));
    report.setReportVersion(1L);
    report.setAuthorisedBy(new LinkResource(1L));
    report.setAuthorisationDate(new GregorianCalendar(2017, 1, 1).getTime());

    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode contentNode = mapper.convertValue(content, JsonNode.class);

    report.setContent(contentNode);

    return report;
  }

  /**
   * Returns FSR content mock data.
   *
   * @return the FSR content mock data.
   */
  public final ReportDto mockFSRData() {
    final ReportContentHDto content = new ReportContentHDto();
    content.setWipCocs(mockCocData());
    content.setWipActionableItems(mockAiData());
    content.setWipAssetNotes(mockAnData());
    content.setSurveys(mockSurveyData());
    content.setTasks(mockTaskData());
    content.setStatutoryFindings(mockStatutoryFindingData());
    content.setCertificates(mockCertificateData());
    content.setCertificateActions(mockCertificateActionData());
    content.setWipMNCNs(mockMajorNonConformities());
    content.setCurrentFlagH(mockCurrentFlagData());
    content.setProposedFlagH(mockProposedFlagData());

    final ReportDto report = new ReportDto();
    report.setId(1L);
    report.setClassRecommendation("Class Recommendation Text. Class Recommendation Text. Class Recommendation Text.");
    report.setIssueDate(new GregorianCalendar(2017, 1, 1).getTime());
    report.setIssuedBy(new LinkResource(1L));
    report.setReportType(new LinkResource(1L));
    report.setReportVersion(1L);
    report.setAuthorisedBy(new LinkResource(1L));
    report.setAuthorisationDate(new GregorianCalendar(2017, 1, 1).getTime());
    report.setJob(new LinkResource(1L));

    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode contentNode = mapper.convertValue(content, JsonNode.class);
    report.setContent(contentNode);

    return report;
  }

  /**
   * Returns certificate action mock data.
   *
   * @return the certificate action mock data.
   */
  public List<CertificateActionDto> mockCertificateActionData() {
    final CertificateActionDto data1 = new CertificateActionDto();
    data1.setId(1L);
    data1.setCertificate(new LinkVersionedResource(1L, 1L));
    data1.setActionTaken(new LinkResource(1L));

    final CertificateActionDto data2 = new CertificateActionDto();
    data2.setId(2L);
    data2.setCertificate(new LinkVersionedResource(2L, 1L));
    data2.setActionTaken(new LinkResource(2L));

    final List<CertificateActionDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);

    return data;
  }


  /**
   * Returns action taken mock data.
   *
   * @return the action taken mock data.
   */
  public List<ActionTakenHDto> mockActionTakenData() {
    final ActionTakenHDto data1 = new ActionTakenHDto();
    data1.setId(1L);
    data1.setName("Raised");

    final ActionTakenHDto data2 = new ActionTakenHDto();
    data2.setId(2L);
    data2.setName("Closed");

    final ActionTakenHDto data3 = new ActionTakenHDto();
    data3.setId(3L);
    data3.setName("Amended");

    final List<ActionTakenHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);

    return data;
  }

  /**
   * Returns flag state data.
   *
   * @return the flag state mock data.
   */
  public FlagStateHDto mockFlagData() {
    final FlagStateHDto data = new FlagStateHDto();
    data.setId(1L);
    data.setName("Ponama");
    return data;
  }

  /**
   * Returns port of registry mock data.
   *
   * @return the port of registry mock data.
   */
  public PortOfRegistryDto mockRegistryData() {
    final PortOfRegistryDto data = new PortOfRegistryDto();
    data.setId(1L);
    data.setName("London");
    return data;
  }

  /**
   * Returns action taken mock data.
   *
   * @return the action taken mock data.
   */
  public FlagStateHDto mockCurrentFlagData() {
    final FlagStateHDto data = new FlagStateHDto();
    data.setId(1L);
    data.setName("Belgium");
    return data;
  }

  /**
   * Returns action taken mock data.
   *
   * @return the action taken mock data.
   */
  public FlagStateHDto mockProposedFlagData() {
    final FlagStateHDto data = new FlagStateHDto();
    data.setId(2L);
    data.setName("Angola");
    return data;
  }

  /**
   * Returns service credit status mock data.
   *
   * @return the service credit status mock data.
   */
  public List<ServiceCreditStatusHDto> mockServiceCreditStatusData() {
    final ServiceCreditStatusHDto data1 = new ServiceCreditStatusHDto();
    data1.setId(1L);
    data1.setName("Not started");
    data1.setCode("NS");

    final ServiceCreditStatusHDto data2 = new ServiceCreditStatusHDto();
    data2.setId(2L);
    data2.setName("Part held");
    data2.setCode("P");

    final ServiceCreditStatusHDto data3 = new ServiceCreditStatusHDto();
    data3.setId(3L);
    data3.setName("Postponed");
    data3.setCode("E");

    final ServiceCreditStatusHDto data4 = new ServiceCreditStatusHDto();
    data4.setId(4L);
    data4.setName("Complete");
    data4.setCode("c");

    final ServiceCreditStatusHDto data5 = new ServiceCreditStatusHDto();
    data5.setId(5L);
    data5.setName("Finished");
    data5.setCode("F");

    final List<ServiceCreditStatusHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);
    data.add(data4);
    data.add(data5);

    return data;
  }

  /**
   * Returns resolution status mock data.
   *
   * @return the resolution status mock data.
   */
  public List<JobResolutionStatusHDto> mockResolutionStatusData() {
    final JobResolutionStatusHDto data1 = new JobResolutionStatusHDto();
    data1.setId(1L);
    data1.setName("Completed");
    data1.setCode("A");

    final JobResolutionStatusHDto data2 = new JobResolutionStatusHDto();
    data2.setId(2L);
    data2.setName("Confirmatory Check");
    data2.setCode("B");

    final JobResolutionStatusHDto data3 = new JobResolutionStatusHDto();
    data3.setId(3L);
    data3.setName("Waived");
    data3.setCode("C");

    final JobResolutionStatusHDto data4 = new JobResolutionStatusHDto();
    data4.setId(4L);
    data4.setName("Not applicable");
    data4.setCode("D");

    final JobResolutionStatusHDto data5 = new JobResolutionStatusHDto();
    data5.setId(6L);
    data5.setName("Postponed");
    data5.setCode("E");

    final List<JobResolutionStatusHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);
    data.add(data4);
    data.add(data5);

    return data;
  }

  /**
   * Returns employee mock data.
   *
   * @return the employee mock data.
   */
  public LrEmployeeDto mockEmployeeData() {
    final LrEmployeeDto employee = new LrEmployeeDto();
    employee.setId(1L);
    employee.setFirstName("John");
    employee.setLastName("Smith");
    employee.setName("John Smith");
    employee.setLegalEntity("Lloyd’s Register Group Limited");

    return employee;
  }

  /**
   * Returns survey mock data.
   *
   * @return the survey mock data.
   */
  public List<SurveyWithTaskListDto> mockSurveyData() {
    final SurveyWithTaskListDto survey1 = new SurveyWithTaskListDto();
    survey1.setId(1L);
    survey1.setServiceCatalogue(new LinkResource(1L));
    survey1.setName("First Survey");
    survey1.setDateOfCrediting(new GregorianCalendar(2017, 1, 1).getTime());
    survey1.setSurveyStatus(new LinkResource(1L));
    survey1.setServiceCatalogue(new LinkResource(1L));
    survey1.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    survey1.setOccurrenceNumber(2);

    final SurveyWithTaskListDto survey2 = new SurveyWithTaskListDto();
    survey2.setId(2L);
    survey2.setServiceCatalogue(new LinkResource(2L));
    survey2.setName("Second Survey");
    survey2.setSurveyStatus(new LinkResource(2L));
    survey2.setServiceCatalogue(new LinkResource(2L));
    survey2.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    survey2.setOccurrenceNumber(1);

    final SurveyWithTaskListDto survey3 = new SurveyWithTaskListDto();
    survey3.setId(3L);
    survey3.setServiceCatalogue(new LinkResource(3L));
    survey3.setName("Third Survey");
    survey3.setSurveyStatus(new LinkResource(2L));
    survey3.setServiceCatalogue(new LinkResource(3L));
    survey3.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    survey3.setOccurrenceNumber(5);

    final SurveyWithTaskListDto survey4 = new SurveyWithTaskListDto();
    survey4.setId(4L);
    survey4.setServiceCatalogue(new LinkResource(4L));
    survey4.setName("Fourth Survey");
    survey4.setSurveyStatus(new LinkResource(2L));
    survey4.setServiceCatalogue(new LinkResource(4L));
    survey4.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    survey4.setOccurrenceNumber(4);

    final SurveyWithTaskListDto survey5 = new SurveyWithTaskListDto();
    survey5.setId(5L);
    survey5.setServiceCatalogue(new LinkResource(5L));
    survey5.setName("Fifth Survey");
    survey5.setDateOfCrediting(new GregorianCalendar(2017, 1, 1).getTime());
    survey5.setSurveyStatus(new LinkResource(2L));
    survey5.setServiceCatalogue(new LinkResource(5L));
    survey5.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    survey5.setOccurrenceNumber(10);



    final List<SurveyWithTaskListDto> surveys = new ArrayList<>();
    surveys.add(survey1);
    surveys.add(survey2);
    surveys.add(survey3);
    surveys.add(survey4);
    surveys.add(survey5);

    return surveys;
  }

  /**
   * Returns asset detail mock data.
   *
   * @return the asset detail mock data.
   */
  public AssetDetailsDto mockAssetDetailData() {
    final AssetDetailsDto assetDetail = new AssetDetailsDto();
    assetDetail.setRegistryInformation(new RegistryInformationDto());
    assetDetail.getRegistryInformation().setPortOfRegistry("Port’s Side");
    return assetDetail;
  }

  /**
   * Returns asset mock data.
   *
   * @return the asset mock data.
   */
  public AssetHDto mockAssetData() {
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("My Ship");
    asset.setLeadImo("1000019");
    asset.setImoNumber("1000019");

    return asset;
  }

  /**
   * Returns job mock data.
   *
   * @return the job mock data.
   */
  public JobHDto mockJobData() {
    final JobHDto job = new JobHDto();
    job.setId(1L);
    job.setJobNumber("JOBNUMBER");
    job.setFirstVisitDate(new GregorianCalendar(2017, 1, 5).getTime());
    job.setLastVisitDate(new GregorianCalendar(2016, 1, 4).getTime());
    job.setAsset(new LinkVersionedResource(1L, 1L));
    job.setJobCategory(new LinkResource(1L));
    LocationHDto locationDto = new LocationHDto();
    locationDto.setId(1L);
    CountryHDto countryDto = new CountryHDto();
    countryDto.setName("Location Name");
    locationDto.setCountryDto(countryDto);
    job.setLocationDto(locationDto);
    job.setEmployees(new ArrayList<EmployeeLinkDto>());

    final EmployeeLinkDto employee1 = new EmployeeLinkDto();
    employee1.setId(1L);
    employee1.setEmployeeRole(new LinkResource(11L));
    employee1.setLrEmployee(new LinkResource(1L));

    final EmployeeLinkDto employee2 = new EmployeeLinkDto();
    employee2.setId(2L);
    employee2.setEmployeeRole(new LinkResource(6L));
    employee1.setLrEmployee(new LinkResource(2L));

    final EmployeeLinkDto employee3 = new EmployeeLinkDto();
    employee3.setId(3L);
    employee3.setEmployeeRole(new LinkResource(6L));
    employee1.setLrEmployee(new LinkResource(3L));

    job.getEmployees().add(employee1);
    job.getEmployees().add(employee2);
    job.getEmployees().add(employee3);

    return job;
  }

  /**
   * Returns CoC mock data.
   *
   * @return the coc mock data.
   */
  public final List<CoCDto> mockCocData() {
    final CoCDto coc1 = new CoCDto();
    coc1.setId(1L);
    coc1.setTitle("First coc");
    coc1.setActionTaken(new LinkResource(1L));
    coc1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    coc1.setRaisedOnJob(new LinkResource(1L));
    coc1.setDeleted(false);

    final CoCDto coc2 = new CoCDto();
    coc2.setId(2L);
    coc2.setTitle("Second coc");
    coc2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    coc2.setClosedOnJob(new LinkResource(1L));
    coc2.setDeleted(true);

    final List<CoCDto> cocs = new ArrayList<>();
    cocs.add(coc1);
    cocs.add(coc2);


    return cocs;
  }

  /**
   * Returns AI mock data.
   *
   * @return the AI mock data.
   */
  public final List<ActionableItemDto> mockAiData() {
    final ActionableItemDto item1 = new ActionableItemDto();
    item1.setId(1L);
    item1.setTitle("First ai");
    item1.setStatus(new LinkResource(CodicilStatus.AI_CHANGE_RECOMMENDED.getValue()));
    item1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    item1.setConfidentialityType(new LinkResource(1L));
    item1.setStatus(new LinkResource(1L));
    item1.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    item1.setRaisedOnJob(new LinkResource(1L));
    item1.setActionTaken(new LinkResource(1L));

    final ActionableItemDto item2 = new ActionableItemDto();
    item2.setId(2L);
    item2.setTitle("Second ai");
    item2.setStatus(new LinkResource(CodicilStatus.AI_OPEN.getValue()));
    item2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    item2.setConfidentialityType(new LinkResource(1L));
    item2.setStatus(new LinkResource(1L));
    item2.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    item2.setClosedOnJob(new LinkResource(1L));

    final List<ActionableItemDto> items = new ArrayList<>();
    items.add(item1);
    items.add(item2);

    return items;
  }

  /**
   * Returns AN mock data.
   *
   * @return the AN mock data.
   */
  public final List<AssetNoteDto> mockAnData() {
    final AssetNoteDto note1 = new AssetNoteDto();
    note1.setId(1L);
    note1.setTitle("First an");
    note1.setStatus(new LinkResource(CodicilStatus.AI_CHANGE_RECOMMENDED.getValue()));
    note1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    note1.setConfidentialityType(new LinkResource(1L));
    note1.setStatus(new LinkResource(1L));
    note1.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    note1.setRaisedOnJob(new LinkResource(1L));
    note1.setActionTaken(new LinkResource(1L));

    final AssetNoteDto note2 = new AssetNoteDto();
    note2.setId(2L);
    note2.setTitle("Second an");
    note2.setStatus(new LinkResource(CodicilStatus.AN_OPEN.getValue()));
    note2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    note2.setConfidentialityType(new LinkResource(1L));
    note2.setStatus(new LinkResource(1L));
    note2.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    note2.setClosedOnJob(new LinkResource(1L));

    final List<AssetNoteDto> notes = new ArrayList<>();
    notes.add(note1);
    notes.add(note2);

    return notes;
  }

  /**
   * Returns statutory finding mock data.
   *
   * @return the statutory finding mock data.
   */
  public final List<StatutoryFindingDto> mockStatutoryFindingData() {
    final StatutoryFindingDto data1 = new StatutoryFindingDto();
    data1.setId(1L);
    data1.setTitle("SF 1");
    data1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    data1.setActionTaken(new LinkResource(1L));

    final StatutoryFindingDto data2 = new StatutoryFindingDto();
    data2.setId(1L);
    data2.setTitle("SF 2");
    data2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());

    final List<StatutoryFindingDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);

    return data;
  }

  /**
   * Returns Mock Major Non-Conformities {@link MajorNCNDto}.
   *
   * @return Major Non-Conformities list.
   */
  public final List<MajorNCNDto> mockMajorNonConformities() {
    final MajorNCNDto data1 = new MajorNCNDto();
    data1.setId(1L);
    data1.setDescription("Mncn 1");
    data1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    data1.setActionTaken(new LinkResource(1L));

    final MajorNCNDto data2 = new MajorNCNDto();
    data2.setId(1L);
    data2.setDescription("Mncn 2");
    data2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());

    final List<MajorNCNDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);

    return data;
  }

  /**
   * Returns certificate mock data.
   *
   * @return the certificates mock data.
   */
  public final List<CertificateDto> mockCertificateData() {
    final CertificateDto data1 = new CertificateDto();
    data1.setId(1L);
    data1.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data1.setCertificateNumber("cert 1");
    data1.setName("test cert");

    final CertificateDto data2 = new CertificateDto();
    data2.setId(2L);
    data2.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data2.setCertificateNumber("cert 2");
    data2.setName("test cert2");

    final CertificateDto data3 = new CertificateDto();
    data3.setId(3L);
    data3.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data3.setCertificateNumber("cert 3");
    data3.setName("test cert3");

    final CertificateDto data4 = new CertificateDto();
    data4.setId(4L);
    data4.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data4.setCertificateNumber("cert 4");
    data4.setName("test cert4");

    final CertificateDto data5 = new CertificateDto();
    data5.setId(5L);
    data5.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data5.setCertificateNumber("cert 5");
    data5.setName("test cert5");

    final CertificateDto data6 = new CertificateDto();
    data6.setId(6L);
    data6.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data6.setCertificateNumber("cert 6");
    data6.setName("test cert6");

    final CertificateDto data7 = new CertificateDto();
    data7.setId(7L);
    data7.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data7.setCertificateNumber("cert 7");
    data7.setName("test cert7");

    final List<CertificateDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);
    data.add(data4);
    data.add(data5);
    data.add(data6);
    data.add(data7);

    return data;
  }


  /**
   * Returns task mock data.
   *
   * @return the task mock data.
   */
  public final List<WorkItemDto> mockTaskData() {
    final WorkItemDto task1 = new WorkItemDto();
    task1.setId(1L);
    task1.setTaskNumber("112-111-111");
    task1.setName("Task 1");
    SurveyDto survey = new SurveyDto();
    survey.setId(1L);
    task1.setSurvey(survey);
    task1.setWorkItemType(new LinkResource(1L));
    task1.setServiceCode("DS");
    task1.setLongDescription("long description");
    List<WorkItemAttributeDto> attributes = new ArrayList<>();
    WorkItemAttributeDto workItem = new WorkItemAttributeDto();
    workItem.setAttributeDataType(new LinkResource(5L));
    WorkItemConditionalAttributeDto workItemConditionalAttributeDto = new WorkItemConditionalAttributeDto();
    workItemConditionalAttributeDto.setReadingCategory(new LinkResource(3L));
    workItemConditionalAttributeDto.setId(1L);
    workItemConditionalAttributeDto.setComparativeAttributeCode("test");
    workItem.setId(1L);
    workItem.setWorkItemConditionalAttribute(workItemConditionalAttributeDto);
    WorkItemAttributeDto workItem2 = new WorkItemAttributeDto();
    workItem2.setId(2L);
    workItem2.setInternalId("2");
    workItem2.setDescription("test");
    workItem2.setParent(new LinkResource(1L));
    attributes.add(workItem);
    attributes.add(workItem2);
    task1.setAttributes(attributes);
    ItemLightDto assetItem = new ItemLightDto();
    assetItem.setId(1L);
    task1.setAssetItem(assetItem);

    final WorkItemDto task2 = new WorkItemDto();
    task2.setId(2L);
    task2.setTaskNumber("222-222-222");
    task2.setName("Task 2");
    task2.setResolutionStatus(new LinkResource(2L));
    task2.setWorkItemType(new LinkResource(1L));
    SurveyDto survey2 = new SurveyDto();
    survey2.setId(1L);
    task2.setSurvey(survey2);
    ItemLightDto assetItem2 = new ItemLightDto();
    assetItem2.setId(2L);
    task2.setAssetItem(assetItem2);

    final WorkItemDto task3 = new WorkItemDto();
    task3.setId(3L);
    task3.setTaskNumber("334-333-333");
    task3.setName("Task 3");
    task3.setResolutionStatus(new LinkResource(3L));
    SurveyDto survey3 = new SurveyDto();
    survey3.setId(2L);
    task3.setSurvey(survey3);
    task3.setWorkItemType(new LinkResource(1L));
    task3.setServiceCode("TS");
    task3.setLongDescription("long description");
    List<WorkItemAttributeDto> attributes1 = new ArrayList<>();
    WorkItemAttributeDto workItem1 = new WorkItemAttributeDto();
    workItem1.setAttributeDataType(new LinkResource(5L));
    WorkItemConditionalAttributeDto workItemConditionalAttributeDto1 = new WorkItemConditionalAttributeDto();
    workItemConditionalAttributeDto1.setReadingCategory(new LinkResource(2L));
    workItemConditionalAttributeDto1.setId(2L);
    workItemConditionalAttributeDto1.setReferenceCode("test");
    workItem1.setId(4L);
    workItem1.setWorkItemConditionalAttribute(workItemConditionalAttributeDto1);
    attributes1.add(workItem1);
    task3.setAttributes(attributes1);
    task3.setAssetItem(assetItem);

    WorkItemAttributeDto workItem3 = new WorkItemAttributeDto();
    WorkItemConditionalAttributeDto workItemConditionalAttributeDto2 = new WorkItemConditionalAttributeDto();
    workItemConditionalAttributeDto2.setReadingCategory(new LinkResource(4L));
    workItemConditionalAttributeDto2.setId(4L);
    workItem3.setId(5L);
    workItem3.setWorkItemConditionalAttribute(workItemConditionalAttributeDto1);
    attributes1.add(workItem3);
    task3.setAttributes(attributes1);

    final WorkItemDto task4 = new WorkItemDto();
    task4.setId(4L);
    task4.setTaskNumber("444-444-444");
    task4.setName("Task 4");
    task4.setResolutionStatus(new LinkResource(4L));
    SurveyDto survey4 = new SurveyDto();
    survey4.setId(3L);
    task4.setSurvey(survey4);
    task4.setWorkItemType(new LinkResource(1L));
    task4.setServiceCode("CC");
    task4.setLongDescription("long description");
    task4.setAssetItem(assetItem);

    final WorkItemDto task5 = new WorkItemDto();
    task5.setId(5L);
    task5.setTaskNumber("555-555-555");
    task5.setName("Task 5");
    SurveyDto survey5 = new SurveyDto();
    survey5.setId(5L);
    task5.setSurvey(survey4);
    task5.setWorkItemType(new LinkResource(1L));
    task5.setServiceCode("BB");
    task5.setLongDescription("long description 5");
    task5.setResolutionStatus(new LinkResource(4L));
    task5.setAssetItem(assetItem);

    final WorkItemDto task6 = new WorkItemDto();
    task6.setId(6L);
    task6.setTaskNumber("666-666-666");
    task6.setDescription("Task 6");
    task6.setResolutionStatus(new LinkResource(6L));
    task6.setWorkItemType(new LinkResource(2L));
    SurveyDto survey6 = new SurveyDto();
    survey6.setId(4L);
    task6.setSurvey(survey6);
    task6.setLongDescription("long description 6");
    task6.setAssetItem(assetItem);

    final WorkItemDto task7 = new WorkItemDto();
    task7.setId(7L);
    task7.setTaskNumber("777-777-777");
    task7.setDescription("Task 7");
    SurveyDto survey7 = new SurveyDto();
    survey7.setId(5L);
    task7.setSurvey(survey7);
    task7.setWorkItemType(new LinkResource(2L));
    task7.setLongDescription("long description 7");
    task7.setServiceCode("BB");
    task7.setReferenceCode("Task7 ref");
    task7.setAssetItem(assetItem);

    final List<WorkItemDto> tasks = new ArrayList<>();
    tasks.add(task1);
    tasks.add(task2);
    tasks.add(task3);
    tasks.add(task4);
    tasks.add(task5);
    tasks.add(task6);
    tasks.add(task7);

    return tasks;
  }

  /**
   *
   * Provides spring in-class configurations.
   *
   * @author yng
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Unmocks report export service.
     *
     * @return the {@link ReportExportService} instance.
     */
    @Override
    @Bean
    public ReportExportService reportExportService() {
      return new ReportExportServiceImpl();
    }
  }

}
