package com.baesystems.ai.lr.cd.service.test.bean;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;

/**
 * Test DTO extended {@link AssetLightDto}.
 *
 * @author msidek
 *
 */
public class Asset extends AssetLightDto {
  /**
   * Constant serial.
   *
   */
  private static final long serialVersionUID = -5366681148484516354L;

  /**
   * Linked DTO.
   *
   */
  @LinkedResource(referencedField = "registeredPort")
  private PortOfRegistryDto registeredPortDto;

  /**
   * Getter for registeredPortDto.
   *
   * @return registeredPortDto.
   *
   */
  public final PortOfRegistryDto getRegisteredPortDto() {
    return registeredPortDto;
  }

  /**
   * Setter for registeredPortDto.
   *
   * @param portDto port to be set.
   *
   */
  public final void setRegisteredPortDto(final PortOfRegistryDto portDto) {
    this.registeredPortDto = portDto;
  }


}
