package com.baesystems.ai.lr.cd.be.processor.duestatuscalculator.test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.impl.ActionableItemServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.enums.CodicilCategory;

import retrofit2.mock.Calls;

/**
 * @author VKolagutla
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ActionableItemsDueStatusCalculatorTest.Config.class)
public class ActionableItemsDueStatusCalculatorTest {


  /**
   * actionableItemService.
   */
  @Autowired
  private ActionableItemService actionableItemService;

  /**
   *
   * Mocked {@link AssetRetrofitService}.
   *
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * Constant asset id.
   *
   */
  private static final Long ASSET_ID = 1L;

  /**
   * months.
   */
  private static final Long MONTHS_11 = 11L;

  /**
   * three.
   */
  private static final Long THREE = 3L;


  /**
   * test case for ActionableItems dueStatus.
   */
  @Test
  public final void calculateDueStatusTest() {
    final List<ActionableItemHDto> actionableItems = new ArrayList<>();

    final LocalDate date = LocalDate.now();
    final LocalDate date1 = LocalDate.now().minusDays(2L);
    final LocalDate date2 = LocalDate.now().plusDays(2L);
    final LocalDate date3 = LocalDate.now().plusMonths(1L);
    final LocalDate date4 = LocalDate.now().plusMonths(1L).minusDays(1L);
    final LocalDate date5 = LocalDate.now().plusMonths(THREE);
    final LocalDate date6 = LocalDate.now().plusMonths(THREE).minusDays(1L);
    final LocalDate date7 = LocalDate.now().plusYears(1L);
    final LocalDate date8 = LocalDate.now().plusYears(1L).minusDays(1L);

    final ActionableItemHDto actionableItem = new ActionableItemHDto();
    actionableItem.setDueDate(DateUtils.getLocalDateToDate(date));

    final ActionableItemHDto actionableItem1 = new ActionableItemHDto();
    actionableItem1.setDueDate(DateUtils.getLocalDateToDate(date1));

    final ActionableItemHDto actionableItem2 = new ActionableItemHDto();
    actionableItem2.setDueDate(DateUtils.getLocalDateToDate(date2));

    final ActionableItemHDto actionableItem3 = new ActionableItemHDto();
    actionableItem3.setDueDate(DateUtils.getLocalDateToDate(date3));

    final ActionableItemHDto actionableItem4 = new ActionableItemHDto();
    actionableItem4.setDueDate(DateUtils.getLocalDateToDate(date4));

    final ActionableItemHDto actionableItem5 = new ActionableItemHDto();
    actionableItem5.setDueDate(null);

    final ActionableItemHDto actionableItem6 = new ActionableItemHDto();
    LinkResource category = new LinkResource(CodicilCategory.AI_CLASS.getValue());
    actionableItem6.setCategory(category);
    actionableItem6.setDueDate(DateUtils.getLocalDateToDate(date5));

    final ActionableItemHDto actionableItem7 = new ActionableItemHDto();
    LinkResource category1 = new LinkResource(CodicilCategory.AI_CLASS.getValue());
    actionableItem7.setCategory(category1);
    actionableItem7.setDueDate(DateUtils.getLocalDateToDate(date6));

    final ActionableItemHDto actionableItem8 = new ActionableItemHDto();
    LinkResource category2 = new LinkResource(CodicilCategory.AI_STATUTORY.getValue());
    actionableItem8.setCategory(category2);
    actionableItem8.setDueDate(DateUtils.getLocalDateToDate(date7));

    final ActionableItemHDto actionableItem9 = new ActionableItemHDto();
    LinkResource category3 = new LinkResource(CodicilCategory.AI_STATUTORY.getValue());
    actionableItem9.setCategory(category3);
    actionableItem9.setDueDate(DateUtils.getLocalDateToDate(date8));

    Mockito.when(assetServiceDelegate.getActionableItemDto(ASSET_ID)).thenReturn(Calls.response(actionableItems));

    final DueStatus status = actionableItemService.calculateDueStatus(actionableItem);
    Assert.assertEquals(DueStatus.IMMINENT, status);

    final DueStatus status1 = actionableItemService.calculateDueStatus(actionableItem1);
    Assert.assertEquals(DueStatus.OVER_DUE, status1);

    final DueStatus status2 = actionableItemService.calculateDueStatus(actionableItem2);
    Assert.assertEquals(DueStatus.IMMINENT, status2);

    final DueStatus status3 = actionableItemService.calculateDueStatus(actionableItem3);
    Assert.assertEquals(DueStatus.NOT_DUE, status3);

    final DueStatus status4 = actionableItemService.calculateDueStatus(actionableItem4);
    Assert.assertEquals(DueStatus.IMMINENT, status4);

    final DueStatus status5 = actionableItemService.calculateDueStatus(actionableItem5);
    Assert.assertEquals(DueStatus.NOT_DUE, status5);

    final DueStatus status6 = actionableItemService.calculateDueStatus(actionableItem6);
    Assert.assertEquals(DueStatus.NOT_DUE, status6);

    final DueStatus status7 = actionableItemService.calculateDueStatus(actionableItem7);
    Assert.assertEquals(DueStatus.DUE_SOON, status7);

    final DueStatus status8 = actionableItemService.calculateDueStatus(actionableItem8);
    Assert.assertEquals(DueStatus.NOT_DUE, status8);

    final DueStatus status9 = actionableItemService.calculateDueStatus(actionableItem9);
    Assert.assertEquals(DueStatus.DUE_SOON, status9);

  }

  /**
   * Test Case for Calculate Due Status With Previous Date.
   */
  @Test
  public final void testCalculateDueStatusWithPreviousDate() {
    final ActionableItemHDto actionableItem = new ActionableItemHDto();
    final CodicilCategoryHDto codicialCategory = new CodicilCategoryHDto();
    codicialCategory.setName("Statutory");
    actionableItem.setCategoryH(codicialCategory);
    actionableItem.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(1L)));

    final DueStatus status = actionableItemService.calculateDueStatus(actionableItem, LocalDate.now().minusDays(1L));
    Assert.assertEquals(DueStatus.IMMINENT, status);
  }

  /**
   * @author VKolagutla
   *
   *         Spring in-class configurations.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {


    /**
     * Create {@link AssetRetrofitService}.
     *
     * @return asset retrofit service.
     *
     */
    @Override
    @Bean
    public AssetRetrofitService assetRetrofitService() {
      return Mockito.mock(AssetRetrofitService.class);
    }

    /**
     * @return dueStatus.
     */
    @Override
    @Bean
    public ActionableItemService actionableItemService() {
      return new ActionableItemServiceImpl();
    }
  }
}
