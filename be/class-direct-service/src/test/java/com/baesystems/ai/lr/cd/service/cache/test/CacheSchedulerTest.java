package com.baesystems.ai.lr.cd.service.cache.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.service.asset.reference.ReferenceDataService;
import com.baesystems.ai.lr.cd.service.cache.CacheContainer;
import com.baesystems.ai.lr.cd.service.cache.CacheScheduler;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * Unit test for {@link CacheScheduler}.
 *
 * @author MSidek
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CacheSchedulerTest.Config.class)
public class CacheSchedulerTest {
  /**
   * Initial cache version.
   *
   */
  private static final Long INITIAL_VERSION = 41L;

  /**
   * Next cache version.
   *
   */
  private static final Long NEXT_VERSION = 42L;

  /**
   * Injected scheduler.
   *
   */
  @Autowired
  private CacheScheduler scheduler;

  /**
   * Injected cache.
   *
   */
  @Autowired
  private CacheContainer cache;

  /**
   * Injected ref data service.
   *
   */
  @Autowired
  private ReferenceDataService refDataService;

  /**
   * Unit test for {@link CacheScheduler#checkVersion()}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void scheduleRun() throws Exception {
    scheduler.checkVersion();
    Assert.assertEquals(INITIAL_VERSION, cache.getVersion());

    Mockito.when(refDataService.getCurrentVersion()).thenReturn(NEXT_VERSION);
    scheduler.checkVersion();
    Assert.assertEquals(NEXT_VERSION, cache.getVersion());
  }

  /**
   * Inner config class.
   *
   * @author MSidek
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Bean
    @Override
    public ReferenceDataService referenceDataService() {
      final ReferenceDataService service = super.referenceDataService();
      Mockito.when(service.getCurrentVersion()).thenReturn(INITIAL_VERSION);

      return service;
    }
  }
}
