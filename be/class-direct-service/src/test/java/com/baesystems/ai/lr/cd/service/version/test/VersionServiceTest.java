package com.baesystems.ai.lr.cd.service.version.test;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.version.VersionService;
import com.baesystems.ai.lr.cd.service.version.impl.VersionServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.servlet.ServletContext;
import java.lang.reflect.Proxy;

import static junit.framework.TestCase.assertNotNull;

/**
 * Unit tests for {@link com.baesystems.ai.lr.cd.service.version.VersionService}.
 *
 * @author fwijaya on 15/6/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
public class VersionServiceTest {
  /**
   * Version service.
   */
  @Autowired
  private VersionService versionService;
  /**
   * Spring injected context.
   */
  @Autowired
  private ServletContext servletContext;

  /**
   * Set manifest with build number.
   */
  private void setManifestWithBuildNumber() {
    ServletContext context = (ServletContext) Proxy
      .newProxyInstance(servletContext.getClass().getClassLoader(), servletContext.getClass().getInterfaces(),
                        new ServletContextProxy(servletContext, "manifestfiles/MANIFEST-1.MF"));
    ((VersionServiceImpl) versionService).setServletContext(context);
  }

  /**
   * Set manifest with no build number.
   */
  private void setManifestWithNoBuildNumber() {
    ServletContext context = (ServletContext) Proxy
      .newProxyInstance(servletContext.getClass().getClassLoader(), servletContext.getClass().getInterfaces(),
                        new ServletContextProxy(servletContext, "manifestfiles/MANIFEST-2.MF"));
    ((VersionServiceImpl) versionService).setServletContext(context);
  }

  /**
   * Test successfully getting build number from the menifest file.
   * @throws ClassDirectException when no build number is found.
   */
  @Test
  public final void testGetBuildNumber() throws ClassDirectException {
    setManifestWithBuildNumber();
    final String version = versionService.getVersion().getBuild();
    assertNotNull(version);
  }

  /**
   * Test getting build number from the menifest file without build number.
   * @throws ClassDirectException when no build number is found.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetNonExistentBuildNumber() throws ClassDirectException {
    setManifestWithNoBuildNumber();
    versionService.getVersion();
  }

  /**
   * Configuration inner-class.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create version service.
     *
     * @return service.
     */
    @Bean
    public VersionService versionService() {
      return new VersionServiceImpl();
    }
  }
}
