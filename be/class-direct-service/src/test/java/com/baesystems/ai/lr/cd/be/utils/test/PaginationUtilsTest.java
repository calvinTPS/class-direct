package com.baesystems.ai.lr.cd.be.utils.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.dto.paging.PageResource;

/**
 * Unit test for {@link PaginationUtil}.
 *
 * @author msidek
 *
 */
public class PaginationUtilsTest {
  /**
   * Default asset size.
   *
   */
  private static final Integer ASSET_SIZE = 30;

  /**
   * Unit test for {@link PaginationUtil#paginate(List, Integer, Integer)}.
   *
   */
  @Test
  public final void paginateList() {
    final List<AssetHDto> assets = mockAssets(ASSET_SIZE);
    final Integer pageNumber = 2;
    final Integer resultSize = 5;

    final Integer startingId = 5;
    final Integer finalId = 9;

    final PageResource<AssetHDto> assetPage = PaginationUtil.paginate(assets, pageNumber, resultSize);
    Assert.assertNotNull(assetPage);
    Assert.assertNotNull(assetPage.getContent());
    Assert.assertEquals(resultSize.intValue(), assetPage.getContent().size());
    Assert.assertEquals(Long.valueOf(startingId), assetPage.getContent().get(0).getId());
    Assert.assertEquals(Long.valueOf(finalId), assetPage.getContent().get(resultSize - 1).getId());

    final Integer pageNumberOne = 1;
    final Integer startingIdOne = 0;
    final Integer finalIdOne = 4;
    final PageResource<AssetHDto> assetPageOne = PaginationUtil.paginate(assets, pageNumberOne, resultSize);
    Assert.assertNotNull(assetPageOne);
    Assert.assertNotNull(assetPageOne.getContent());
    Assert.assertEquals(resultSize.intValue(), assetPageOne.getContent().size());
    Assert.assertEquals(Long.valueOf(startingIdOne), assetPageOne.getContent().get(0).getId());
    Assert.assertEquals(Long.valueOf(finalIdOne), assetPageOne.getContent().get(resultSize - 1).getId());

    final Integer pageNumberLast = 6;
    final Integer startingIdLast = 25;
    final Integer finalIdLast = 29;
    final PageResource<AssetHDto> assetPageLast = PaginationUtil.paginate(assets, pageNumberLast, resultSize);
    Assert.assertNotNull(assetPageLast);
    Assert.assertNotNull(assetPageLast.getContent());
    Assert.assertEquals(resultSize.intValue(), assetPageLast.getContent().size());
    Assert.assertEquals(Long.valueOf(startingIdLast), assetPageLast.getContent().get(0).getId());
    Assert.assertEquals(Long.valueOf(finalIdLast), assetPageLast.getContent().get(resultSize - 1).getId());

    final Integer pageNumberFirstLast = 1;
    final Integer startingIdFirstLast = 0;
    final Integer finalIdFirstLast = 29;
    final Integer resultSizeFirstLast = 30;
    final PageResource<AssetHDto> assetPageFirstLast =
        PaginationUtil.paginate(assets, pageNumberFirstLast, resultSizeFirstLast);
    Assert.assertNotNull(assetPageFirstLast);
    Assert.assertNotNull(assetPageFirstLast.getContent());
    Assert.assertEquals(resultSizeFirstLast.intValue(), assetPageFirstLast.getContent().size());
    Assert.assertEquals(Long.valueOf(startingIdFirstLast), assetPageFirstLast.getContent().get(0).getId());
    Assert.assertEquals(Long.valueOf(finalIdFirstLast),
        assetPageFirstLast.getContent().get(resultSizeFirstLast - 1).getId());

    final Integer pageOne = 1;
    final Integer sizeSeven = 7;
    final Integer expectedPage = 5;
    final PageResource<AssetHDto> remainderAssetAfterFullPage =
        PaginationUtil.paginate(mockAssets(ASSET_SIZE + 1), pageOne, sizeSeven);
    Assert.assertEquals(expectedPage, remainderAssetAfterFullPage.getPagination().getTotalPages());

  }


  /**
   * Unit test for {@link PaginationUtil#paginate(List, Integer, Integer)} - with page/size =
   * null/zero.
   */
  @Test
  public final void paginateWithPageAndSizeNullOrZero() {
    final List<AssetHDto> assets = mockAssets(ASSET_SIZE);
    PageResource<AssetHDto> assetPage = null;

    assetPage = PaginationUtil.paginate(assets, null, null);
    Assert.assertNotNull(assetPage);
  }

  /**
   * Negative unit test for {@link PaginationUtil#paginate(List, Integer, Integer)}.
   *
   */
  @Test(expected = IllegalArgumentException.class)
  public final void paginateThrowErrorOnInvalidNumberPage() {
    PaginationUtil.paginate(new ArrayList<>(), -1, 1);
  }

  /**
   * Negative unit test for {@link PaginationUtil#paginate(List, Integer, Integer)}.
   *
   */
  @Test(expected = IllegalArgumentException.class)
  public final void paginateThrowErrorOnInvalidNumberSize() {
    PaginationUtil.paginate(new ArrayList<>(), 1, -1);
  }

  /**
   * Create mock asset list.
   *
   * @param count count of objects.
   * @return list of mock object.
   *
   */
  private List<AssetHDto> mockAssets(final Integer count) {
    final List<AssetHDto> assets = new ArrayList<>();
    for (Integer i = 0; i < count; i++) {
      final AssetHDto asset = new AssetHDto();
      asset.setId(i.longValue());
      assets.add(asset);
    }

    return assets;
  }
}
