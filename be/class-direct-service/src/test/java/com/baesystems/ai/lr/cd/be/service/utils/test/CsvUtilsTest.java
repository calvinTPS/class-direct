package com.baesystems.ai.lr.cd.be.service.utils.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.service.utils.CsvUtils;

/**
 * Unit test case for {@link CsvUtils}.
 *
 * @author syalavarthi
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class CsvUtilsTest {


  /**
   * Tests success scenario to sanitise first character
   * {@link CsvUtils#sanitiseFirstCharacter(String)} if it start with =.
   */
  @Test
  public final void testSanitiseFirstCharacter() {
    String result = CsvUtils.sanitiseFirstCharacter("=testsanitize");
    Assert.assertNotNull(result);
    Assert.assertEquals("testsanitize", result);
  }

  /**
   * Tests success scenario to sanitise first character
   * {@link CsvUtils#sanitiseFirstCharacter(String)} value is null or empty.
   */
  @Test
  public final void testSanitiseFirstCharacterIsNull() {
    String result = CsvUtils.sanitiseFirstCharacter(null);
    Assert.assertNull(result);
    String resultEmpty = CsvUtils.sanitiseFirstCharacter("");
    Assert.assertNotNull(resultEmpty);
    Assert.assertTrue(resultEmpty.isEmpty());
  }

}
