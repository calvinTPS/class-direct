package com.baesystems.ai.lr.cd.service.cache.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.test.util.ReflectionTestUtils;

import com.baesystems.ai.lr.cd.service.cache.CacheFactory;

import redis.clients.jedis.Jedis;

/**
 * Provides test for {@link CacheFactory}.
 *
 * @author yng
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(CacheFactory.class)
@PowerMockIgnore("javax.management.*")
public class CacheFactoryTest {

  /**
   * Test postive scenario for {@link CacheFactory#createCacheManager()}.
   *
   * @throws Exception if mock fail.
   *
   */
  @Test
  public void testCacheFactory() throws Exception {
    final CacheFactory factory = Mockito.spy(new CacheFactory());

    ReflectionTestUtils.setField(factory, "defaultExpiration", "3600");
    ReflectionTestUtils.setField(factory, "assetsExpiration", "3600");
    ReflectionTestUtils.setField(factory, "assetsByUserExpiration", "3600");
    ReflectionTestUtils.setField(factory, "userProfileExpiration", "86400");
    ReflectionTestUtils.setField(factory, "assetsExportExpiration", "3600");
    ReflectionTestUtils.setField(factory, "redisHostname", "127.0.0.1");
    ReflectionTestUtils.setField(factory, "redisPort", "6379");

    // create jedis mock
    final Jedis mockJedis = Mockito.mock(Jedis.class);
    PowerMockito.whenNew(Jedis.class).withAnyArguments().thenReturn(mockJedis);
    PowerMockito.when(mockJedis.ping()).thenReturn("PONG");

    // test for redis connected is true
    PowerMockito.when(mockJedis.isConnected()).thenReturn(true);


    final CacheManager cacheManager = factory.createCacheManager();

    Assert.assertNotNull(cacheManager);
    Assert.assertTrue(cacheManager instanceof RedisCacheManager);
  }
}
