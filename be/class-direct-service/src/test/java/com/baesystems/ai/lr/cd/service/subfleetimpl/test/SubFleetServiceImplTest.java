package com.baesystems.ai.lr.cd.service.subfleetimpl.test;

import static com.baesystems.ai.lr.cd.be.constants.CDConstants.MAST_PREFIX;
import static com.baesystems.ai.lr.cd.service.test.UserId.ALICE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BaseDtoPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetDto;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetOptimizedDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.subfleet.impl.SubFleetServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for {@link SubFleetService}.
 *
 * @author syalavarthi
 * @author YWearn 2017-07-04
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SubFleetServiceImplTest.Config.class)
@DirtiesContext
public class SubFleetServiceImplTest {
  /**
   * The application logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(SubFleetServiceImplTest.class);
  /**
   * The {@link SubFleetService} injected from Spring context.
   */
  @Autowired
  private SubFleetService subfleetService;
  /**
   * The {@link AssetService} injected from Spring context.
   */
  @Autowired
  private AssetService assetService;
  /**
   * The constant test asset id.
   */
  private static final Long ID = 1L;
  /**
   * The constant test asset id.
   */
  private static final Long ID2 = 2L;
  /**
   * The constant test asset id.
   */
  private static final Long ID3 = 3L;

  /**
   * The constant test client user id.
   */
  private static final String CLIENT_USER_ID = "111";

  /**
   * The {@link SubFleetDAO} injected from Spring context.
   */
  @Autowired
  private SubFleetDAO subFleetDAO;
  /**
   * The {@link UserProfileService} injected from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;
  /**
   * The {@link AuditService} injected from Spring context.
   */
  @Autowired
  private AuditService auditService;
  /**
   * The {@link AssetRetrofitService} injected from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * The argument captor for accessible ids.
   */
  @Captor
  private ArgumentCaptor<List<Long>> accessibleIdsCaptor;

  /**
   * The argument captor for restricted ids.
   */
  @Captor
  private ArgumentCaptor<List<Long>> restrictedIdsCaptor;

  /**
   * Provides clean up of mocked services and daos before each test case.
   */
  @Before
  public final void cleanUp() {
    MockitoAnnotations.initMocks(this); // Activate use of @Captor

    reset(assetRetrofitService, userProfileService, assetService, auditService, subFleetDAO);
  }

  /**
   * Tests success scenario of getting sub fleet of a user.
   *
   * @throws Exception exception if there is error fetching user's subfleet.
   */
  @Test
  public final void testGetSubfleet() throws Exception {
    List<Long> assetIds = new ArrayList<>();
    assetIds.add(new Long(ID));
    Mockito.when(subFleetDAO.getSubFleetById(Mockito.anyString())).thenReturn(assetIds);
    Assert.assertTrue(subfleetService.getSubFleetById(ALICE.getId()).containsAll(assetIds));
  }

  /**
   * Tests success scenario to save sub fleet for a given user.
   *
   * @throws Exception exception if error on database insert operation.
   */
  @Test
  public final void testSaveSubFleet() throws Exception {
    List<Long> assetIds = new ArrayList<>();
    assetIds.add(new Long(ID));
    assetIds.add(new Long(ID2));
    SubFleetDto subfleet = new SubFleetDto();
    subfleet.setAccessibleAssetIds(assetIds);
    subfleet.setRestrictedAssetIds(new ArrayList<>());
    AssetHDto asset = new AssetHDto();
    asset.setId(ID);
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(CLIENT_USER_ID, Collections.emptyMap()));
    Mockito.when(assetService.assetByCode(ALICE.getId(), MAST_PREFIX + ID)).thenReturn(asset);
    Mockito.when(assetService.assetByCode(ALICE.getId(), MAST_PREFIX + ID2)).thenThrow(new RecordNotFoundException(""));
    try {
      Assert.assertNotNull(assetService.assetByCode(ALICE.getId(), MAST_PREFIX + ID));
      assetService.assetByCode(ALICE.getId(), MAST_PREFIX + ID2);
    } catch (RecordNotFoundException exception) {
      LOG.debug("Error while getting asset" + exception);
    }
    StatusDto status = new StatusDto();
    status.setMessage("subfleet updated.");
    Mockito
        .when(
            subFleetDAO.saveSubFleet(Mockito.anyString(), Mockito.anyListOf(Long.class), Mockito.anyListOf(Long.class)))
        .thenReturn(status);
    Assert.assertNotNull(subfleetService.saveSubFleet(ALICE.getId(), subfleet));
    Assert.assertEquals(status.getMessage(), subfleetService.saveSubFleet(ALICE.getId(), subfleet).getMessage());
    Mockito.when(assetService.assetByCode(null, MAST_PREFIX + ID)).thenReturn(null);

    final StatusDto actualStatus = subfleetService.saveSubFleet(ALICE.getId(), subfleet);

    Assert.assertNotNull(actualStatus);
    Assert.assertEquals(status.getMessage(), actualStatus.getMessage());
  }

  /**
   * Creates default pagination dto.
   *
   * @param numberOfElements the numberOfElements.
   * @return the pagination dto.
   */
  private PaginationDto getDefaultPagination(final Long numberOfElements) {
    final PaginationDto dto = new PaginationDto();
    dto.setTotalPages(1);
    dto.setTotalElements(numberOfElements);
    return dto;
  }

  /**
   * Tests the optimized save sub fleet methods which allow a partial asset id list specified for
   * moving asset from accessible to restricted using RESTRICTED, include option.
   *
   * @throws Exception exception if database operation fail.
   */
  @Test
  public final void testSaveSubFleetOptimizedMoveToRestrictedIncluded() throws Exception {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(ALICE.getId(), Collections.emptyMap()));
    Mockito.when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserWithId(CLIENT_USER_ID));
    // All the assets this user can theoretically access if nothing restricted.

    // EVERYTHING ACCESSIBLE
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(new ArrayList<Long>());

    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ID, ID2, ID3}));
    assetPageResource.setPagination(getDefaultPagination(3L));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));

    // (1) Move some to restricted using included
    List<Long> assetIds = new ArrayList<>();
    assetIds.add(new Long(ID));
    assetIds.add(new Long(ID2));
    SubFleetOptimizedDto subfleet = new SubFleetOptimizedDto();
    subfleet.setMoveTo("RESTRICTED");
    subfleet.setIncludedAssetIds(assetIds);
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // SOME ACCESSIBLE
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(Arrays.asList(ID2, ID3));

    // (2) Move some to restricted using included
    subfleet.setIncludedAssetIds(Arrays.asList(ID3));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // (3) Move all to restricted using included
    subfleet.setIncludedAssetIds(Arrays.asList(ID2, ID3));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    Mockito.verify(subFleetDAO, Mockito.times(3)).saveSubFleet(eq(CLIENT_USER_ID), accessibleIdsCaptor.capture(),
        restrictedIdsCaptor.capture());

    // (1)
    Assert.assertArrayEquals(new Long[] {ID3}, accessibleIdsCaptor.getAllValues().get(0).toArray());
    Assert.assertArrayEquals(new Long[] {ID, ID2}, restrictedIdsCaptor.getAllValues().get(0).toArray());

    // (2)
    Assert.assertArrayEquals(new Long[] {ID2}, accessibleIdsCaptor.getAllValues().get(1).toArray());
    Assert.assertArrayEquals(new Long[] {ID, ID3}, restrictedIdsCaptor.getAllValues().get(1).toArray());

    // (3)
    Assert.assertArrayEquals(new Long[] {}, accessibleIdsCaptor.getAllValues().get(2).toArray());
    Assert.assertArrayEquals(new Long[] {ID, ID2, ID3}, restrictedIdsCaptor.getAllValues().get(2).toArray());
  }

  /**
   * Tests the optimized save sub fleet methods which allow a partial asset id list specified for
   * moving asset from accessible to restricted using RESTRICTED, exclusion option.
   *
   * @throws Exception exception if database operation fail.
   */
  @Test
  public final void testSaveSubFleetOptimizedMoveToRestrictedExcluded() throws Exception {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(ALICE.getId(), Collections.emptyMap()));
    Mockito.when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserWithId(CLIENT_USER_ID));
    // All the assets this user can theoretically access if nothing restricted.
    Mockito.when(assetService.executeMastIhsQuery(any(), any(), any(), eq(false)))
        .thenReturn(mockAssetsWithIds(new Long[] {ID, ID2, ID3}));
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ID, ID2, ID3}));
    assetPageResource.setPagination(getDefaultPagination(3L));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));

    // EVERYTHING ACCESSIBLE
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(new ArrayList<Long>());

    // (1) Move some to restricted using excluded
    SubFleetOptimizedDto subfleet = new SubFleetOptimizedDto();
    subfleet.setMoveTo("RESTRICTED");
    subfleet.setExcludedAssetIds(Arrays.asList(ID));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // (2) Move all to restricted using excluded
    subfleet.setExcludedAssetIds(new ArrayList<>());
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // SOME ACCESSIBLE
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(Arrays.asList(ID2, ID3));

    // (3) Move some to restricted using excluded
    subfleet.setExcludedAssetIds(Arrays.asList(ID2));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // (4) Move all to restricted using excluded
    subfleet.setExcludedAssetIds(new ArrayList<>());
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    Mockito.verify(subFleetDAO, Mockito.times(4)).saveSubFleet(eq(CLIENT_USER_ID), accessibleIdsCaptor.capture(),
        restrictedIdsCaptor.capture());

    // (1)
    Assert.assertArrayEquals(new Long[] {ID}, accessibleIdsCaptor.getAllValues().get(0).toArray());
    Assert.assertArrayEquals(new Long[] {ID2, ID3}, restrictedIdsCaptor.getAllValues().get(0).toArray());

    // (2)
    Assert.assertArrayEquals(new Long[] {}, accessibleIdsCaptor.getAllValues().get(1).toArray());
    Assert.assertArrayEquals(new Long[] {ID, ID2, ID3}, restrictedIdsCaptor.getAllValues().get(1).toArray());

    // (3)
    Assert.assertArrayEquals(new Long[] {ID2}, accessibleIdsCaptor.getAllValues().get(2).toArray());
    Assert.assertArrayEquals(new Long[] {ID, ID3}, restrictedIdsCaptor.getAllValues().get(2).toArray());

    // (4)
    Assert.assertArrayEquals(new Long[] {}, accessibleIdsCaptor.getAllValues().get(3).toArray());
    Assert.assertArrayEquals(new Long[] {ID, ID2, ID3}, restrictedIdsCaptor.getAllValues().get(3).toArray());
  }

  /**
   * Tests the optimized save sub fleet methods which allow a partial asset id list specified for
   * moving asset from restricted to accessible using ACCESSIBLE, include option.
   *
   * @throws Exception exception if any database operation fail.
   */
  @Test
  public final void testSaveSubFleetOptimizedMoveToAccessibleIncluded() throws Exception {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(ALICE.getId(), Collections.emptyMap()));
    Mockito.when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserWithId(CLIENT_USER_ID));
    // All the assets this user can theoretically access if nothing restricted.

    // EVERYTHING RESTRICTED
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserRestrictAllWithId(CLIENT_USER_ID));
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(Arrays.asList());
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ID, ID2, ID3}));
    assetPageResource.setPagination(getDefaultPagination(3L));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));

    // (1) Move some to accessible using included
    List<Long> assetIds = new ArrayList<>();
    assetIds.add(new Long(ID));
    assetIds.add(new Long(ID2));
    SubFleetOptimizedDto subfleet = new SubFleetOptimizedDto();
    subfleet.setMoveTo("ACCESSIBLE");
    subfleet.setIncludedAssetIds(assetIds);
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // SOME RESTRICTED
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserWithId(CLIENT_USER_ID));
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(Arrays.asList(ID));

    // (2) Move some to accessible using included
    subfleet.setIncludedAssetIds(Arrays.asList(ID2));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // (3) Move all to accessible using included
    subfleet.setIncludedAssetIds(Arrays.asList(ID2, ID3));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    Mockito.verify(subFleetDAO, Mockito.times(3)).saveSubFleet(eq(CLIENT_USER_ID), accessibleIdsCaptor.capture(),
        restrictedIdsCaptor.capture());

    // (1)
    Assert.assertArrayEquals(new Long[] {ID, ID2}, accessibleIdsCaptor.getAllValues().get(0).toArray());
    Assert.assertArrayEquals(new Long[] {ID3}, restrictedIdsCaptor.getAllValues().get(0).toArray());

    // (2)
    Assert.assertArrayEquals(new Long[] {ID, ID2}, accessibleIdsCaptor.getAllValues().get(1).toArray());
    Assert.assertArrayEquals(new Long[] {ID3}, restrictedIdsCaptor.getAllValues().get(1).toArray());

    // (3)
    Assert.assertArrayEquals(new Long[] {ID, ID2, ID3}, accessibleIdsCaptor.getAllValues().get(2).toArray());
    Assert.assertArrayEquals(new Long[] {}, restrictedIdsCaptor.getAllValues().get(2).toArray());
  }

  /**
   * Tests the optimized save sub fleet methods which allow a partial asset id list specified for
   * moving asset from restricted to accessible using ACCESSIBLE, exclude option.
   *
   * @throws Exception exception if any database operation fail.
   */
  @Test
  public final void testSaveSubFleetOptimizedMoveToAccessibleExcluded() throws Exception {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken(ALICE.getId(), Collections.emptyMap()));
    Mockito.when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserWithId(CLIENT_USER_ID));
    // All the assets this user can theoretically access if nothing restricted.

    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ID, ID2, ID3}));
    assetPageResource.setPagination(getDefaultPagination(3L));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));

    // EVERYTHING RESTRICTED
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserRestrictAllWithId(CLIENT_USER_ID));
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(Arrays.asList());

    // (1) Move some to accessible using excluded
    SubFleetOptimizedDto subfleet = new SubFleetOptimizedDto();
    subfleet.setMoveTo("ACCESSIBLE");
    subfleet.setExcludedAssetIds(Arrays.asList(ID));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // (2) Move all to accessible using excluded
    subfleet.setExcludedAssetIds(new ArrayList<>());
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // SOME RESTRICTED
    Mockito.when(userProfileService.getUser(eq(CLIENT_USER_ID))).thenReturn(mockUserWithId(CLIENT_USER_ID));
    Mockito.when(subFleetDAO.getSubFleetById(eq(CLIENT_USER_ID))).thenReturn(Arrays.asList(ID3));

    // (3) Move some to accessible using excluded
    subfleet.setExcludedAssetIds(Arrays.asList(ID2));
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    // (4) Move all to accessible using excluded
    subfleet.setExcludedAssetIds(new ArrayList<>());
    subfleetService.saveSubFleetOptimized(CLIENT_USER_ID, subfleet);

    Mockito.verify(subFleetDAO, Mockito.times(4)).saveSubFleet(eq(CLIENT_USER_ID), accessibleIdsCaptor.capture(),
        restrictedIdsCaptor.capture());

    // (1)
    Assert.assertArrayEquals(new Long[] {ID2, ID3}, accessibleIdsCaptor.getAllValues().get(0).toArray());
    Assert.assertArrayEquals(new Long[] {ID}, restrictedIdsCaptor.getAllValues().get(0).toArray());

    // (2)
    Assert.assertArrayEquals(new Long[] {ID, ID2, ID3}, accessibleIdsCaptor.getAllValues().get(1).toArray());
    Assert.assertArrayEquals(new Long[] {}, restrictedIdsCaptor.getAllValues().get(1).toArray());

    // (3)
    Assert.assertArrayEquals(new Long[] {ID, ID3}, accessibleIdsCaptor.getAllValues().get(2).toArray());
    Assert.assertArrayEquals(new Long[] {ID2}, restrictedIdsCaptor.getAllValues().get(2).toArray());

    // (4)
    Assert.assertArrayEquals(new Long[] {ID, ID2, ID3}, accessibleIdsCaptor.getAllValues().get(3).toArray());
    Assert.assertArrayEquals(new Long[] {}, restrictedIdsCaptor.getAllValues().get(3).toArray());
  }


  /**
   * Tests the success scenario to return only accessible assets.
   *
   * @throws ClassDirectException if the user not found or sub fleet database operation fail.
   * @throws IOException if asset api fail.
   */
  @Test
  public final void testShouldReturnListOfAccessibleAssets() throws ClassDirectException, IOException {
    when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserWithId(ALICE.getId()));

    final List<Long> assetIds = new ArrayList<>();
    assetIds.add(ID);
    assetIds.add(ID2);
    when(subFleetDAO.getSubFleetById(eq(ALICE.getId()))).thenReturn(assetIds);
    when(assetService.executeMastIhsQuery(any(), eq(1), eq(1), eq(null), eq(null))).thenReturn(getAccesibleAssets());

    final PageResource<AssetHDto> assetPageResource = subfleetService.getSubfleet(ALICE.getId(), 1, 1);
    assertNotNull(assetPageResource);
    assertNotNull(assetPageResource.getContent());
    assertFalse(assetPageResource.getContent().isEmpty());
    assertNotNull(assetPageResource.getPagination());
  }



  /**
   * Mocks default asset pagination for test data.
   *
   * @return AssetPageResource the mocked asset pagination test data.
   */
  private AssetPageResource getDefaultPaginatedAssets() {
    final AssetPageResource assets = new AssetPageResource();
    assets.setContent(Collections.emptyList());
    final PaginationDto pagination = new PaginationDto();
    pagination.setSize(1);
    pagination.setNumber(1);
    pagination.setFirst(false);
    pagination.setLast(false);
    pagination.setTotalPages(1);
    pagination.setTotalElements(0L);
    pagination.setNumberOfElements(1);
    assets.setPagination(pagination);
    return assets;
  }

  /**
   * Tests success return user's sub fleet ids.
   *
   * @throws Exception exception if asset api fail or sub fleet database operation fail.
   */
  @Test
  public final void testGetSubFleetIds() throws Exception {
    List<Long> assetIds = new ArrayList<>();
    assetIds.add(new Long(ID));
    assetIds.add(ID2);

    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(subFleetDAO.getSubFleetById(Mockito.anyString())).thenReturn(assetIds);

    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ID, ID2}));
    assetPageResource.setPagination(getDefaultPagination(3L));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));
    List<Long> assets = subfleetService.getSubFleetIds(ALICE.getId());
    Assert.assertNotNull(assets);
    Assert.assertFalse(assets.isEmpty());

    // to check BaseDto empty.
    BaseDtoPageResource pageResource = new BaseDtoPageResource();
    pageResource.setContent(Collections.emptyList());
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(pageResource));
    when(subFleetDAO.getSubFleetById(eq(ALICE.getId()))).thenReturn(Collections.emptyList());
    List<Long> assets1 = subfleetService.getSubFleetIds(ALICE.getId());
    Assert.assertNotNull(assets1);
    Assert.assertTrue(assets1.isEmpty());
  }


  /**
   * Tests {@link SubFleetService#isSubFleetEnabled(String)} to return true if user restricted flag
   * is set to true.
   *
   * @throws ClassDirectException if user or sub fleet database operation fail.
   */
  @Test
  public final void testIsSubFleetEnabledWithRestrictedAll() throws ClassDirectException {
    Mockito.when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserRestrictAllWithId(ALICE.getId()));
    boolean actualResult = subfleetService.isSubFleetEnabled(ALICE.getId());
    Assert.assertTrue(actualResult);
  }

  /**
   * Tests {@link SubFleetService#isSubFleetEnabled(String)} to return true if user restricted flag
   * is set to false and with sub fleet defined.
   *
   * @throws ClassDirectException if user or sub fleet database operation fail.
   */
  @Test
  public final void testIsSubFleetEnabledWithSubfleet() throws ClassDirectException {
    Mockito.when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(subFleetDAO.getSubFleetCountByUserId(eq(ALICE.getId()))).thenReturn(1L);
    boolean actualResult = subfleetService.isSubFleetEnabled(ALICE.getId());
    Assert.assertTrue(actualResult);
  }

  /**
   * Tests {@link SubFleetService#isSubFleetEnabled(String)} to return false if user restricted flag
   * is set to false and with zero sub fleet defined.
   *
   * @throws ClassDirectException if user or sub fleet database operation fail.
   */
  @Test
  public final void testIsSubFleetEnabledWithoutSubfleet() throws ClassDirectException {
    Mockito.when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(subFleetDAO.getSubFleetCountByUserId(eq(ALICE.getId()))).thenReturn(0L);
    boolean actualResult = subfleetService.isSubFleetEnabled(ALICE.getId());
    Assert.assertFalse(actualResult);
  }

  /**
   * Tests success return the user's restricted ids.
   *
   * @throws Exception exception if the asset api fail or sub fleet database operation fail.
   */
  @Test
  public final void testGetRestrictedAssetsIds() throws Exception {
    // to test userRestrictAll false, Subfleet and assetQueryAsBaseDto having same results.
    List<Long> assetIds = new ArrayList<>();
    assetIds.add(new Long(ID));
    assetIds.add(ID2);
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUserWithId(ALICE.getId()));
    Mockito.when(subFleetDAO.getSubFleetById(Mockito.anyString())).thenReturn(assetIds);
    Mockito.when(assetService.populateAssetQuery(Mockito.any(UserProfiles.class), Mockito.any(MastQueryBuilder.class)))
        .thenReturn(MastQueryBuilder.create());

    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class),
        Mockito.isNull(Integer.class), Mockito.isNull(Integer.class))).thenReturn(Calls.response(mockBaseDto()));
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ID, ID2}));
    assetPageResource.setPagination(getDefaultPagination(2L));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));
    List<Long> assets = subfleetService.getRestrictedAssetsIds(ALICE.getId());
    Assert.assertNotNull(assets);
    Assert.assertTrue(assets.isEmpty());

    // to check assetRetrofitService assetQueryAsBaseDto call throw exception.
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class),
        Mockito.isNull(Integer.class), Mockito.isNull(Integer.class))).thenReturn(Calls.failure(new IOException()));
    List<Long> assets2 = subfleetService.getRestrictedAssetsIds(ALICE.getId());
    Assert.assertEquals(0, assets2.size());

    // to test userRestAll true and subfleet empty.
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUserRestrictAllWithId(ALICE.getId()));
    Mockito.when(subFleetDAO.getSubFleetById(Mockito.anyString())).thenReturn(Collections.emptyList());
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class),
        Mockito.isNull(Integer.class), Mockito.isNull(Integer.class))).thenReturn(Calls.response(mockBaseDto()));
    Mockito.when(assetService.executeMastIhsQuery(any(), eq(null), eq(null), eq(false))).thenReturn(mockAssets());
    List<Long> assets3 = subfleetService.getRestrictedAssetsIds(ALICE.getId());
    Assert.assertNotNull(assets3);
    Assert.assertFalse(assets3.isEmpty());
  }

  /**
   * Tests {@link SubFleetService#getSubFleetById(String)} should return empty if user is
   * restricted.
   *
   * @throws ClassDirectException if user or sub fleet database operation fail.
   * @throws IOException if mast asset api fail.
   */
  @Test
  public final void testSubfleetShouldReturnEmptyIfUserIsRestricted() throws ClassDirectException, IOException {
    final UserProfiles alice = new UserProfiles();
    alice.setRestrictAll(Boolean.TRUE);
    when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(alice);

    final List<Long> subfleets = new ArrayList<>();
    subfleets.add(ID);
    subfleets.add(ID2);
    subfleets.add(ID3);
    when(subFleetDAO.getSubFleetById(eq(ALICE.getId()))).thenReturn(subfleets);

    final List<Long> subfleetIds = subfleetService.getSubFleetIds(ALICE.getId());
    assertNotNull(subfleetIds);
    assertTrue(subfleetIds.isEmpty());
  }

  /**
   * Tests {@link SubFleetService#getAccessibleAssetIdsForUser(String)} should get list of
   * accessible assets from mast and ihs query.
   *
   * @throws ClassDirectException if user/subfleet database operation fail or asset api fail.
   */
  @Test
  public final void testGetAccessibleAssetIdsForUser() throws ClassDirectException {
    final UserProfiles alice = new UserProfiles();
    when(userProfileService.getUser(eq(ALICE.getId()))).thenReturn(alice);
    Mockito.when(assetService.executeMastIhsQuery(any(), eq(null), eq(null), eq(false))).thenReturn(mockAssets());
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(mockBaseDtoWithIds(new Long[] {ID, ID2}));
    assetPageResource.setPagination(getDefaultPagination(2L));
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), anyInt(), anyInt()))
        .thenReturn(Calls.response(assetPageResource));
    final List<Long> accessibleIds = subfleetService.getAccessibleAssetIdsForUser(ALICE.getId());
    assertTrue(accessibleIds.containsAll(Arrays.asList(ID, ID2)));
    assertEquals(accessibleIds.size(), 2);
  }

  /**
   * Mocks list of test assets.
   *
   * @return the test asset list.
   */
  private List<AssetHDto> mockAssets() {
    final List<AssetHDto> assets = new ArrayList<>();

    final AssetHDto asset1 = getAsset();
    asset1.setId(ID2);
    asset1.setCode(AssetCodeUtil.getMastCode(ID2));
    assets.add(asset1);

    final AssetHDto asset2 = getAsset();
    asset2.setId(ID);
    asset2.setCode(AssetCodeUtil.getMastCode(ID));
    assets.add(asset2);

    return assets;
  }

  /**
   * Mocks list of assets with asset code.
   *
   * @param ids the list of asset ids.
   * @return the mocked asset list.
   */
  private List<AssetHDto> mockAssetsWithIds(final Long[] ids) {
    final List<AssetHDto> assets = new ArrayList<>();

    for (Long id : ids) {
      AssetHDto asset = getAsset();
      asset.setId(id);
      asset.setCode(AssetCodeUtil.getMastCode(id));
      assets.add(asset);
    }

    return assets;
  }

  /**
   * Mocks the asset api result.
   *
   * @return the mocked asset api result.
   */
  private BaseDtoPageResource mockBaseDto() {
    BaseDtoPageResource pageResource = new BaseDtoPageResource();
    List<BaseDto> baseDtos = new ArrayList<>();
    BaseDto baseDto = new BaseDto();
    baseDto.setId(ID);
    BaseDto baseDto2 = new BaseDto();
    baseDto2.setId(ID2);
    baseDtos.add(baseDto);
    baseDtos.add(baseDto2);
    pageResource.setContent(baseDtos);
    return pageResource;
  }

  /**
   * Mocks a single asset model.
   *
   * @return the mocked single asset model.
   */
  private AssetHDto getAsset() {
    AssetHDto asset = new AssetHDto();
    asset.setBuilder("Ership Internacional SA");
    asset.setYardNumber("Yard1");
    asset.setId(ID);
    return asset;
  }

  /**
   * Mocks list of accessible assets.
   *
   * @return the mocked accessible assets.
   */
  private AssetPageResource getAccesibleAssets() {
    AssetPageResource pageResource = getDefaultPaginatedAssets();
    pageResource.getPagination().setTotalElements(ID2);
    List<AssetHDto> assets = new ArrayList<>();
    AssetHDto asset = new AssetHDto();
    asset.setBuilder("Ership Internacional SA");
    asset.setYardNumber("Yard1");
    asset.setId(ID);
    assets.add(asset);

    AssetHDto asset2 = new AssetHDto();
    asset2.setBuilder("Ership Internacional SA");
    asset2.setYardNumber("Yard1");
    asset2.setId(ID2);
    assets.add(asset2);
    pageResource.setContent(assets);
    return pageResource;
  }

  /**
   * Mocks test user profile with restricted all flag set to true.
   *
   * @param userId the user id.
   * @return user the mocked user profile with restricted all flag true.
   */
  private UserProfiles mockUserRestrictAllWithId(final String userId) {
    UserProfiles user = new UserProfiles();
    user.setUserId(userId);
    user.setRestrictAll(true);
    return user;
  }

  /**
   * Mocks test user profile with restricted all flag set to false.
   *
   * @param id the user id.
   * @return user the mocked user profile with restricted all flag false.
   */
  private UserProfiles mockUserWithId(final String id) {
    UserProfiles user = new UserProfiles();
    user.setUserId(id);
    user.setRestrictAll(false);
    return user;
  }

  /**
   * Mocks list of base dto with id's.
   *
   * @param ids the list of asset ids.
   * @return the mocked asset list.
   */
  private List<BaseDto> mockBaseDtoWithIds(final Long[] ids) {
    final List<BaseDto> assetIds = new ArrayList<>();

    for (Long id : ids) {
      BaseDto dto = new BaseDto();
      dto.setId(id);
      assetIds.add(dto);
    }

    return assetIds;
  }

  /**
   * Provides Spring context for unit test.
   *
   * @author SYalavarthi.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Creates the sub fleet service for test case.
     *
     * @return the sub fleet service.
     */
    @Override
    @Bean
    public SubFleetService subFleetService() {
      return Mockito.spy(new SubFleetServiceImpl());
    }

  }
}

