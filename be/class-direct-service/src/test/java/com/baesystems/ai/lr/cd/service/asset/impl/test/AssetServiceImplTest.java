package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetEarliestEntitiesHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BuilderHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.IhsAssetClientDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.RepeatOfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ServiceScheduleDto;
import com.baesystems.ai.lr.cd.be.domain.dto.favourites.FavouritesDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.repositories.Favourites;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.CustomerRolesEnum;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.enums.OrderOptionEnum;
import com.baesystems.ai.lr.cd.be.enums.SortOptionEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.BaseAssetDto;
import com.baesystems.ai.lr.dto.assets.IhsAssetLink;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetTypeDto;
import com.baesystems.ai.lr.dto.ihs.IhsCompanyContactDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.EarliestEntityQueryDto;
import com.baesystems.ai.lr.dto.query.IhsSisterQueryDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.CodicilStatus;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.baesystems.ai.lr.enums.ServiceStatus;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.mock.Calls;

/**
 * Unit test for AssetService.
 *
 * @author Faizal Sidek
 *  @author syalavarthi 02-07-2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AssetServiceImplTest {

  /**
   * Logger for AssetServiceImpl class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetServiceImplTest.class);

  /**
   * Default mast asset id.
   */
  private static final Long DEFAULT_MAST_ASSET_ID = 1000000L;

  /**
   * Mast asset id 1.
   */
  private static final Long MAST_ASSET_ID_1 = 1000001L;

  /**
   * Mast asset id 2.
   */
  private static final Long MAST_ASSET_ID_2 = 1000002L;

  /**
   * Mast asset id 3.
   */
  private static final Long MAST_ASSET_ID_3 = 1000003L;

  /**
   * Mast asset id 4.
   */
  private static final Long MAST_ASSET_ID_4 = 1000004L;

  /**
   * Mast asset id 5.
   */
  private static final Long MAST_ASSET_ID_5 = 1000005L;

  /**
   * Default user id.
   */
  private static final String DEFAULT_USER_ID = "1000";

  /**
   * User id 1001.
   */
  private static final String USER_ID_1001 = "1001";

  /**
   * Initial page.
   */
  private static final Integer INITIAL_PAGE = 1;

  /**
   * Default result per page.
   */
  private static final Integer DEFAULT_PAGE_SIZE = 100;

  /**
   * Integer 16.
   */
  private static final Integer SIXTEEN = 16;

  /**
   * Integer 0.
   */
  private static final Integer ZERO = 0;

  /**
   * Integer 1.
   */
  private static final Integer ONE = 1;

  /**
   * Integer 2.
   */
  private static final Integer TWO = 2;

  /**
   * Integer 3.
   */
  private static final Integer THREE = 3;

  /**
   * Integer 4.
   */
  private static final Integer FOUR = 4;


  /**
   * Integer 5.
   */
  private static final Integer FIVE = 5;

  /**
   * Integer 6.
   */
  private static final Integer SIX = 6;

  /**
   * Integer 7.
   */
  private static final Integer SEVEN = 7;

  /**
   * Integer 8.
   */
  private static final Integer EIGHT = 8;

  /**
   * Integer 9.
   */
  private static final Integer NINE = 9;

  /**
   * Integer 10.
   */
  private static final Integer TEN = 10;
  /**
   * Integer 11.
   */
  private static final Integer ELEVEN = 11;

  /**
   * Integer 12.
   */
  private static final Integer TWELVE = 12;
  /**
   * Default flag code.
   */
  private static final String DEFAULT_FLAG_CODE = "MYY";
  /**
   * Secondary flag code 1.
   */
  private static final String SECONDARY_FLAG_CODE_1 = "DEF";
  /**
   * Secondary flag code 2.
   */
  private static final String SECONDARY_FLAG_CODE_2 = "GHI";

  /**
   * Default client code.
   */
  private static final String DEFAULT_CLIENT_CODE = "1000";

  /**
   * Default ship builder code.
   */
  private static final String DEFAULT_SHIP_BUILDER_CODE = "1000";

  /**
   * Default builder name.
   */
  private static final String DEFAULT_BUILDER_NAME = "Test builder";

  /**
   * Default min double.
   */
  private static final Double DEFAULT_MIN_DOUBLE = 1000d;

  /**
   * Default max double.
   */
  private static final Double DEFAULT_MAX_DOUBLE = 2000d;

  /**
   * Default min date.
   */
  private static final Date DEFAULT_MIN_DATE = new Date();

  /**
   * Default max date.
   */
  private static final Date DEFAULT_MAX_DATE = new Date();

  /**
   * Default yard number.
   */
  private static final String DEFAULT_YARD_NUMBER = "YARD10234";

  /**
   * Default asset name.
   */
  private static final String DEFAULT_ASSET_NAME = "Skele";

  /**
   * Default asset type.
   */
  private static final String DEFAULT_ASSET_TYPE = "Default5";

  /**
   * Default IHS doc company.
   */
  private static final String DEFAULT_IHS_DOC_COMPANY = "DocCompanyInc";

  /**
   * Default company name.
   */
  private static final String DEFAULT_COMPANY_NAME = "MyCompanyInc";

  /**
   * Default doc code.
   */
  private static final String DEFAULT_DOC_CODE = "12345";

  /**
   * Default IMO number.
   */
  private static final String DEFAULT_IMO_NUMBER = "123456";

  /**
   * Injected asset service.
   */
  @Autowired
  private AssetService assetService;

  /**
   * App context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * serviceReference.
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceReference;

  /**
   * userService.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * flags service.
   */
  @Autowired
  private FlagsAssociationsService flagsAssociationsService;

  /**
   * The argument captor for mast and ihs AbstractQueryDto.
   */
  @Captor
  private ArgumentCaptor<PrioritisedAbstractQueryDto> abstractQueryCaptor;

  /**
   * The argument captor for mast and ihs AbstractQueryDto.
   */
  @Captor
  private ArgumentCaptor<PrioritisedAbstractQueryDto> prioritytQueryCaptor;

  /**
   * The {@link EmployeeReferenceService} from spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;

  /**
   * The {@link AssetRetrofitService} from spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * The {@link IhsRetrofitService} from spring context.
   */
  @Autowired
  private IhsRetrofitService ihsServiceDelegate;

  /**
   * The {@link FavouritesRepository} from spring context.
   */
  @Autowired
  private FavouritesRepository favouritesRepository;


  /**
   * The {@link assetReferenceService} from spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link assetServiceDelegate} from spring context.
   *
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;
  /**
   *
   */
  @Value("${favourite.successful.update}")
  private String successfulUpdate;
  /**
   * The {@link ProductReferenceService} from spring context.
   */
  @Autowired
  private ProductReferenceService productReferenceService;
  /**
   * The sorting priority as IHS.
   */
  private static final String SORTING_PRIORITY_IHS = "IHS";
  /**
   * Maximum page size.
   */
  private static final Integer MAX_PAGE_SIZE = 16;

  /**
   * Setup field injection.
   *
   * @throws NoSuchFieldException when error.
   * @throws IllegalAccessException when error.
   */
  @Before
  public final void setup() throws NoSuchFieldException, IllegalAccessException {
    final Field maxPageSizeField = assetService.getClass().getDeclaredField("maxPageSize");
    maxPageSizeField.setAccessible(true);
    maxPageSizeField.set(assetService, MAX_PAGE_SIZE);
  }

  /**
   * Reset mock behaviours.
   */
  @Before
  public final void cleanUp() {
    MockitoAnnotations.initMocks(this); // Activate use of @Captor
    final SubFleetService subFleetService = context.getBean(SubFleetService.class);
    reset(favouritesRepository, subFleetService, assetRetrofitService);
  }

  /**
   * Test restMastPagination.
   *
   * @throws NoSuchMethodException exception.
   * @throws SecurityException exception.
   * @throws IllegalAccessException exception.
   * @throws IllegalArgumentException exception.
   * @throws InvocationTargetException exception.
   */
  @Test
  public final void testResetMastPagination() throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {
    // mock mast pagination data
    final AssetPageResource pageResource = new AssetPageResource();
    final PaginationDto page = new PaginationDto();
    page.setNumber(1);
    page.setTotalPages(SIXTEEN);
    pageResource.setPagination(page);

    // test resetMastPagination
    final AssetServiceImpl impl = new AssetServiceImpl();

    final Method method = AssetServiceImpl.class.getDeclaredMethod("resetMastPagination", AssetPageResource.class);
    method.setAccessible(true);
    final AssetPageResource returnedPageResource = (AssetPageResource) method.invoke(impl, pageResource);

    // total pages should remain the same
    Assert.assertEquals(SIXTEEN, returnedPageResource.getPagination().getTotalPages());

    // number should incremented by 1
    Assert.assertEquals(TWO, returnedPageResource.getPagination().getNumber());
  }

  /**
   * Should return asset page resource with no favourite set.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnResultListWithNoFavourite() throws ClassDirectException, IOException {
    mockUserProfiles(DEFAULT_USER_ID);
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockSubFleet(DEFAULT_USER_ID, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockOffice();
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(false);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(DEFAULT_USER_ID, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery)
        .getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    assets.forEach(asset -> assertFalse(asset.getIsFavourite()));
    assets.forEach(asset -> assertNotNull(asset.getCfoOfficeH()));
    assets.forEach(asset -> Assert.assertEquals("Siri@baesystems.com", asset.getCfoOfficeH().getEmailAddress()));
  }

  /**
   * Tests to return user subfleet asset if whitelist is enabled. The mast query should specify the
   * subfleet assets id.
   *
   * @throws IOException if asset query fail.
   * @throws ClassDirectException if user and subfleet database operation fail.
   */
  @Test
  public final void testShouldReturnSubfleetOnlyAssetWithWhitelist() throws ClassDirectException, IOException {
    mockUserProfiles(DEFAULT_USER_ID);
    mockMastAssets(true, MAST_ASSET_ID_1);
    mockSubFleetIds(DEFAULT_USER_ID, true, MAST_ASSET_ID_1);

    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsSubfleet(true);
    assetQuery.setIncludeInactiveAssets(Boolean.TRUE);

    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(DEFAULT_USER_ID, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery)
        .getContent();

    // Capture the argument used to call the mock.
    Mockito.verify(retrofitService).mastIhsQueryUnsorted(abstractQueryCaptor.capture(), any(), any());

    // Verify mock retrofit was called with "publishedVersion.id": [MAST_ASSET_ID_1]
    final AbstractQueryDto firstAnd = abstractQueryCaptor.getValue().getMastPriorityQuery().getAnd().get(0);
    final Long[] expectedSubfleetIds = (Long[]) firstAnd.getValue();
    assertEquals("publishedVersion", firstAnd.getJoinField());
    assertEquals("id", firstAnd.getField());
    assertEquals(QueryRelationshipType.IN, firstAnd.getRelationship());
    assertEquals(MAST_ASSET_ID_1.longValue(), expectedSubfleetIds[0].longValue());

    assertNotNull(assets);
    assertEquals(assets.size(), 1);
    assets.forEach(asset -> assertFalse(asset.getIsFavourite()));
  }

  /**
   * Tests to return user subfleet asset if whitelist is disabled. The mast query should specify
   * general condition of asset id > 0.
   *
   * @throws IOException if asset query fail.
   * @throws ClassDirectException if user and subfleet database operation fail.
   */
  @Test
  public final void testShouldReturnSubfleetOnlyAssetWithoutWhitelist() throws ClassDirectException, IOException {
    final UserProfiles user = new UserProfiles();
    user.setUserId(DEFAULT_USER_ID);
    user.setRoles(new HashSet<>(1));
    final Roles adminRole = new Roles();
    adminRole.setRoleName("LR_ADMIN");
    user.getRoles().add(adminRole);
    when(userProfileService.getUser(anyString())).thenReturn(user);

    mockMastAssets(true, MAST_ASSET_ID_1);
    mockSubFleetIds(DEFAULT_USER_ID, false, MAST_ASSET_ID_1);

    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsSubfleet(true);
    assetQuery.setIncludeInactiveAssets(Boolean.TRUE);

    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(DEFAULT_USER_ID, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery)
        .getContent();

    // Capture the argument used to call the mock.
    Mockito.verify(retrofitService).mastIhsQueryUnsorted(abstractQueryCaptor.capture(), any(), any());

    // Verify mock retrofit was called with "publishedVersion.id": [MAST_ASSET_ID_1]
    final AbstractQueryDto firstAnd = abstractQueryCaptor.getValue().getMastPriorityQuery().getAnd().get(0);
    final Long actualSubfleetId = (Long) firstAnd.getValue();
    assertEquals("publishedVersion", firstAnd.getJoinField());
    assertEquals("id", firstAnd.getField());
    assertEquals(QueryRelationshipType.GT, firstAnd.getRelationship());
    assertEquals(new Long(0), actualSubfleetId);

    assertNotNull(assets);
    assertEquals(assets.size(), 1);
    assets.forEach(asset -> assertFalse(asset.getIsFavourite()));
  }

  /**
   * should test whether the input is a IMONumber.
   */
  @Test
  public final void testSearchInputForImoNumber() {
    String[] searchInput = new String[]{"1234567", "A123456", "12345678"};
    assertTrue(ServiceUtils.isImoNumber(searchInput[0]));
    assertFalse(ServiceUtils.isImoNumber(searchInput[1]));
    assertFalse(ServiceUtils.isImoNumber(searchInput[2]));
  }

  /**
   * Should return list with sort and order.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void shouldReturnResultListWithSortAndOrder() throws ClassDirectException, IOException {
    mockUserProfiles(DEFAULT_USER_ID);
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockSubFleet(DEFAULT_USER_ID, MAST_ASSET_ID_1, MAST_ASSET_ID_2);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(false);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(DEFAULT_USER_ID, INITIAL_PAGE, DEFAULT_PAGE_SIZE, "AssetName", "ASC", assetQuery)
        .getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    assets.forEach(asset -> assertFalse(asset.getIsFavourite()));
    assets.forEach(asset -> assertTrue(asset.getIsMMSService()));
  }

  /**
   * {@link AssetService#assetByCode(String, String)} should handle empty result set.
   *
   * @throws IOException when error.
   * @throws RecordNotFoundException when error.
   * @throws ClassDirectException when error.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void shouldHandleEmptyAssetList() throws IOException, RecordNotFoundException, ClassDirectException {
    mockEmptyMastIhsResults();
    assertNotNull(assetService.assetByCode(null, CDConstants.MAST_PREFIX + DEFAULT_MAST_ASSET_ID));
    Mockito.verify(assetRetrofitService, atLeast(2)).mastIhsQuery(any());
  }

  /**
   * Service should not return empty if fleet is restricted and user has favourite.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldNotReturnEmptyIfFleetIsRestricted() throws ClassDirectException, IOException {
    mockUserProfileRestrictAll(USER_ID_1001);
    mockIhsAndMast(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4, MAST_ASSET_ID_5);
    mockUserFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(false);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertFalse(assets.get(ZERO).getIsFavourite());
    Assert.assertTrue(assets.get(ONE).getIsFavourite());
    Assert.assertTrue(assets.get(TWO).getIsFavourite());
    Assert.assertFalse(assets.get(THREE).getIsFavourite());
    Assert.assertFalse(assets.get(FOUR).getIsFavourite());
  }

  /**
   * Should return list of assets with favourites.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnListOfFavouriteAssets() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4, MAST_ASSET_ID_5);
    mockUserProfiles(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4, MAST_ASSET_ID_5);
    mockUserFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(true);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    assets.stream().filter(asset -> asset.getId().equals(MAST_ASSET_ID_1) && asset.getId().equals(MAST_ASSET_ID_2))
        .forEach(asset -> assertTrue(asset.getIsFavourite()));
    assets.stream().filter(asset -> asset.getId().equals(MAST_ASSET_ID_3) && asset.getId().equals(MAST_ASSET_ID_4))
        .forEach(asset -> assertTrue(asset.getIsFavourite()));
  }

  /**
   * Should return list of unfavored assets.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnListOfUnfavouriteAssets() throws ClassDirectException, IOException {
    mockMastAssets(false, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3);
    mockUserProfiles(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(false);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    assets.forEach(asset -> assertFalse(asset.getIsFavourite()));
    assets.forEach(asset -> assertFalse(asset.getIsMMSService()));
  }

  /**
   * Should return populated asset query.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnPopulatedAssetQuery() throws ClassDirectException, IOException {
    mockUserProfiles(USER_ID_1001);

    final AssetReferenceService assetReferenceService1 = context.getBean(AssetReferenceService.class);
    final FlagStateHDto flagState = new FlagStateHDto();
    flagState.setFlagCode(DEFAULT_FLAG_CODE);
    when(assetReferenceService1.getFlagStates()).thenReturn(Collections.singletonList(flagState));
    final UserProfileService profileService = context.getBean(UserProfileService.class);
    final UserProfiles userProfile = profileService.getUser(USER_ID_1001);

    final MastQueryBuilder assetQuery = assetService.populateAssetQuery(userProfile, MastQueryBuilder.create());
    assertNotNull(assetQuery);
  }

  /**
   * Tests success scenario to get one asset unique by id
   * {@link AssetService#assetByCode(String, String)}.
   *
   *
   * @throws ClassDirectException when error in execution flow or any error in mast api call fail.
   */
  @Test
  public final void testReturnSingleAsset() throws ClassDirectException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockUserFavourites(USER_ID_1001, MAST_ASSET_ID_1);
    mockOffice();
    final AssetHDto asset = assetService.assetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertEquals(MAST_ASSET_ID_1, asset.getId());
    assertTrue(asset.getIsFavourite());
    assertNotNull(asset.getCfoOfficeH());
    Assert.assertEquals("Siri@baesystems.com", asset.getCfoOfficeH().getEmailAddress());
  }


  /**
   * Tests failure scenario to get single asset {@link AssetService#assetByCode(String, String)}
   * when internal api fail.
   *
   * @throws ClassDirectException when error in execution flow or any error in mast api call fail.
   */
  @Test
  public final void testReturnSingleAssetFail() throws ClassDirectException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockUserFavourites(USER_ID_1001, MAST_ASSET_ID_1);
    when(employeeReferenceService.getOffice(eq(1L))).thenReturn(null);

    final AssetHDto asset = assetService.assetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertEquals(MAST_ASSET_ID_1, asset.getId());
    assertTrue(asset.getIsFavourite());
    Assert.assertNull(asset.getCfoOfficeH());
  }

  /**
   * Should create a favourite.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldCreateFavourite() throws ClassDirectException {
    // TODO
    when(favouritesRepository.verifyFavourite(eq(USER_ID_1001), eq(String.valueOf(MAST_ASSET_ID_1)))).thenReturn(null);

    // TODO
    doNothing().when(favouritesRepository).createUserFavourite(eq(USER_ID_1001), eq(String.valueOf(MAST_ASSET_ID_1)));
    final StatusDto status = assetService.createFavourite(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertNotNull(status);
  }

  /**
   * Should delete favourite.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldDeleteFavourite() throws ClassDirectException {
    when(favouritesRepository.verifyFavourite(eq(USER_ID_1001), eq(String.valueOf(MAST_ASSET_ID_1))))
        .thenReturn(new Favourites());
    doNothing().when(favouritesRepository).deleteUserFavourite(eq(USER_ID_1001), eq(String.valueOf(MAST_ASSET_ID_1)));
    final StatusDto status = assetService.deleteFavourite(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertNotNull(status);
  }

  /**
   * Tests success scenario of {@link AssetService#updateFavouriteList(String, FavouritesDto)}.
   *
   * @throws ClassDirectException if any execution fails.
   */
  @Test
  public final void shouldCreateFavouriteList() throws ClassDirectException {
    FavouritesDto fav = new FavouritesDto();
    doNothing().when(favouritesRepository).updateUserFavouriteList(eq(USER_ID_1001), any(FavouritesDto.class));
    final String status = assetService.updateFavouriteList(USER_ID_1001, fav);
    assertNotNull(status);
    assertTrue(status.equals(String.format(successfulUpdate)));
  }

  /**
   * Tests failure scenario of {@link AssetService#updateFavouriteList(String, FavouritesDto)} for
   * null userId.
   *
   * @throws ClassDirectException if any execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void shouldCreateFavouriteListforNullUserId() throws ClassDirectException {
    FavouritesDto fav = new FavouritesDto();
    final String status = assetService.updateFavouriteList(null, fav);
  }

  /**
   * Tests failure scenario of {@link AssetService#updateFavouriteList(String, FavouritesDto)} for
   * null input DTO.
   *
   * @throws ClassDirectException if any execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void shouldCreateFavouriteListforNullFavouriteDto() throws ClassDirectException {
    FavouritesDto fav = null;
    final String status = assetService.updateFavouriteList(USER_ID_1001, fav);
  }

  /**
   * Tests failure scenario of {@link AssetService#updateFavouriteList(String, FavouritesDto)} for
   * null userId.
   *
   * @throws ClassDirectException if any execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void shouldCreateFavouriteListforEmptyUserId() throws ClassDirectException {
    FavouritesDto fav = null;
    final String status = assetService.updateFavouriteList("", fav);
  }

  /**
   * Tests success scenario of {@link AssetService#updateFavouriteList(String, FavouritesDto)} when
   * exception occurs.
   *
   * @throws ClassDirectException if any execution fails.
   */
  @Test
  public final void testCreateFavouriteListforFail() throws ClassDirectException {
    FavouritesDto fav = new FavouritesDto();
    doThrow(new DataIntegrityViolationException("")).when(favouritesRepository)
        .updateUserFavouriteList(eq(USER_ID_1001), any(FavouritesDto.class));
    final String status = assetService.updateFavouriteList(USER_ID_1001, fav);
    assertNull(status);
  }

  /**
   * Should return page resource of technical sisters.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void shouldReturnTechnicalSisters() throws ClassDirectException, IOException {
    final UserProfiles user = new UserProfiles();
    user.setClientCode("1");
    user.setClientType("DOC_COMPANY");
    when(userProfileService.getUser(eq(USER_ID_1001))).thenReturn(user);

    final AssetHDto parent = new AssetHDto();
    parent.setLeadImo(DEFAULT_IMO_NUMBER);
    when(assetRetrofitService.getAssetById(MAST_ASSET_ID_1)).thenReturn(Calls.response(parent));
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    final AssetPageResource assetPageResource = assetService.getTechnicalSister(DEFAULT_IMO_NUMBER, USER_ID_1001,
        AssetCodeUtil.getMastCode(MAST_ASSET_ID_1), 1, 1);
    assertNotNull(assetPageResource);
    assertNotNull(assetPageResource.getContent());
    assertFalse(assetPageResource.getContent().isEmpty());
    assetPageResource.getContent().forEach(asset -> assertTrue(asset.getIsMMSService()));
  }

  /**
   * Tests success scenario to get technical sisters count
   * {@link AssetService#getTechnicalSistersCount(String, String, String)}.
   *
   * @throws ClassDirectException when mast api call fail or any invalid input parameters.
   */
  @Test
  public final void testGetTechnicalSistersCount() throws ClassDirectException {
    MultiAssetPageResourceDto assetPageResource = new MultiAssetPageResourceDto();
    PaginationDto pagination = new PaginationDto();
    pagination.setTotalElements(4L);
    pagination.setNumber(0);
    assetPageResource.setPagination(pagination);

    when(assetRetrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), eq(0), eq(1)))
        .thenReturn(Calls.response(assetPageResource));
    final Long count = assetService.getTechnicalSistersCount(DEFAULT_IMO_NUMBER, DEFAULT_USER_ID,
        AssetCodeUtil.getMastCode(MAST_ASSET_ID_1));

    assertNotNull(count);
    assertEquals(pagination.getTotalElements(), count);
  }

  /**
   * Tests failure scenario to get technical sisters count
   * {@link AssetService#getTechnicalSistersCount(String, String, String)} with input parameters
   * null.
   *
   * @throws ClassDirectException when mast api call fail or any invalid input parameters.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetTechnicalSistersCountFail() throws ClassDirectException {
    assetService.getTechnicalSistersCount(null, null, null);
  }


  /**
   * Tests failure scenario to get technical sisters count
   * {@link AssetService#getTechnicalSistersCount(String, String, String)} with empty input
   * parameters.
   *
   * @throws ClassDirectException when mast API call fail or any invalid input parameters.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetTechnicalSistersCountFailForEmpty() throws ClassDirectException {
    assetService.getTechnicalSistersCount("", null, "");
  }

  /**
   * Tests failure scenario to get technical sisters count
   * {@link AssetService#getTechnicalSister(String, String, String, Integer, Integer)} with input
   * parameters null.
   *
   * @throws ClassDirectException when any invalid input parameters.
   * @throws IOException if API call fails
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetTechnicalSistersFail() throws ClassDirectException, IOException {
    assetService.getTechnicalSister(null, null, null, null, null);
  }

  /**
   * Tests failure scenario to get technical sisters count
   * {@link AssetService#getTechnicalSistersCount(String, String, String)} with empty & null input
   * parameters.
   *
   * @throws ClassDirectException when any invalid input parameters.
   * @throws IOException if API call fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetTechnicalSistersFailForEmpty() throws ClassDirectException, IOException {
    assetService.getTechnicalSister("", null, "", null, null);
  }

  /**
   * Tests success scenario to get registry book sisters count
   * {@link AssetService#getRegisterBookSistersCount(String, String, String)}.
   *
   * @throws ClassDirectException when mast api call fail or any invalid input parameters.
   */
  @Test
  public final void testGetRegisterBookSistersCount() throws ClassDirectException {
    MultiAssetPageResourceDto assetPageResource = new MultiAssetPageResourceDto();
    PaginationDto pagination = new PaginationDto();
    pagination.setTotalElements(4L);
    pagination.setNumber(0);
    assetPageResource.setPagination(pagination);

    when(ihsServiceDelegate.registerBookSistersIhsQuery(any(IhsSisterQueryDto.class), any(), any(),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(Calls.response(assetPageResource));
    final Long count = assetService.getRegisterBookSistersCount(DEFAULT_IMO_NUMBER, DEFAULT_USER_ID,
        AssetCodeUtil.getMastCode(MAST_ASSET_ID_1));
    assertNotNull(count);
    assertEquals(pagination.getTotalElements(), count);
  }

  /**
   * Tests success scenario to get registry book sisters count as Zero for lead IMO as null
   * {@link AssetService#getTechnicalSistersCount(String, String, String)} with input parameters
   * null.
   *
   * @throws ClassDirectException when mast API call fail or any invalid input parameters.
   */
  @Test
  public final void testGetRegistryBookSistersCountZero() throws ClassDirectException {
      Long count = assetService.getRegisterBookSistersCount(null, null, null);
      Long expectedCount = 0L;
      assertEquals(count, expectedCount);
  }

  /**
   * Tests success scenario to get registry book sisters count as Zero for lead IMO as empty
   * {@link AssetService#getRegisterBookSistersCount(String, String, String)} with empty input
   * parameters.
   *
   * @throws ClassDirectException when mast API call fail or any invalid input parameters.
   */
  @Test
  public final void testGetRegistryBookSistersCountZeroForEmpty() throws ClassDirectException {
      Long count = assetService.getRegisterBookSistersCount("", "", "");
      Long expectedCount = 0L;
      assertEquals(count, expectedCount);
  }

  /**
   * Tests failure scenario to get registry book sisters
   * {@link AssetService#getRegisterBookSister(String, String, String, Integer, Integer)} with input
   * parameters null.
   *
   * @throws ClassDirectException when any invalid input parameters.
   * @throws IOException if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetRegistryBookSistersFail() throws ClassDirectException, IOException {
    assetService.getRegisterBookSister(null, null, null, null, null);
  }

  /**
   * Tests failure scenario to get registry book sisters
   * {@link AssetService#getRegisterBookSister(String, String, String, Integer, Integer)} when API
   * call fails.
   *
   *
   * @throws ClassDirectException when mast API call fail or any invalid input parameters.
   * @throws IOException if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetRegisterBookSistersFailForAPICallFailure() throws ClassDirectException, IOException {
    when(ihsServiceDelegate.registerBookSistersIhsQuery(any(IhsSisterQueryDto.class), eq(0), eq(1),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(Calls.failure(new IOException()));

    final PageResource<AssetHDto> assets = assetService.getRegisterBookSister(DEFAULT_IMO_NUMBER, DEFAULT_USER_ID,
        AssetCodeUtil.getMastCode(MAST_ASSET_ID_1), 1, 1);
  }

  /**
   * Should return page resource of registry book sister.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void shouldReturnRegisterBookSister() throws ClassDirectException, IOException {

    MultiAssetPageResourceDto assetPageResource = new MultiAssetPageResourceDto();
    PaginationDto pagination = new PaginationDto();
    pagination.setTotalElements(4L);
    pagination.setNumber(0);
    assetPageResource.setPagination(pagination);
    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();

    final MultiAssetDto multiAssetDto = new MultiAssetDto();
    multiAssetDto.setIhsAsset(mockIhsAsset(MAST_ASSET_ID_1));
    multiAssetDtos.add(multiAssetDto);

    assetPageResource.setContent(multiAssetDtos);


    when(ihsServiceDelegate.registerBookSistersIhsQuery(any(IhsSisterQueryDto.class), eq(0), eq(1),
        Mockito.isNull(String.class), Mockito.isNull(String.class))).thenReturn(Calls.response(assetPageResource));

    final PageResource<AssetHDto> assets = assetService.getRegisterBookSister(DEFAULT_IMO_NUMBER, DEFAULT_USER_ID,
        AssetCodeUtil.getMastCode(MAST_ASSET_ID_1), 1, 1);
    assertNotNull(assets);
    assertNotNull(assets.getContent());
    assertFalse(assets.getContent().isEmpty());
  }


  /**
   * Tests failure scenario to get registry book sisters count
   * {@link AssetService#getRegisterBookSister(String, String, String, Integer, Integer)} with empty
   * input parameters.
   *
   * @throws ClassDirectException when mast API call fail or any invalid input parameters.
   * @throws IOException if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetRegisterBookSistersFailForEmpty() throws ClassDirectException, IOException {
    assetService.getRegisterBookSister("", "", "", null, null);
  }

  /**
   * Should return single codicil for asset.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnSingleCodicil() throws IOException, ClassDirectException {
    when(assetRetrofitService.getCoCQueryDto(any(), anyLong()))
        .thenReturn(Calls.response(Collections.singletonList(new CoCHDto())));
    when(assetRetrofitService.getActionableItemQueryDto(any(), anyLong()))
        .thenReturn(Calls.response(Collections.singletonList(new ActionableItemHDto())));

    final CodicilDefectQueryHDto query = new CodicilDefectQueryHDto();
    final CodicilActionableHDto codicil = assetService.getCodicils(MAST_ASSET_ID_1, query);
    assertNotNull(codicil);
  }

  /**
   * Mock data for coc.
   *
   * @return cocs.
   */
  public List<CoCHDto> mockCoC() {
    final List<CoCHDto> cocs = new ArrayList<>();
    final CoCHDto coc1 = new CoCHDto();
    coc1.setId(1L);
    coc1.setJob(new LinkResource(1L));
    coc1.setStatusH(new CodicilStatusHDto());
    coc1.getStatusH().setId(CodicilStatus.COC_OPEN.getValue());
    cocs.add(coc1);

    final CoCHDto coc2 = new CoCHDto();
    coc2.setId(2L);
    coc2.setJob(new LinkResource(1L));
    coc2.setStatusH(new CodicilStatusHDto());
    coc2.getStatusH().setId(CodicilStatus.COC_CANCELLED.getValue());
    cocs.add(coc2);
    return cocs;
  }

  /**
   * Mock data for AI.
   *
   * @return ais.
   */
  // mockAi data
  public List<ActionableItemHDto> mockAi() {
    final List<ActionableItemHDto> ais = new ArrayList<>();
    final ActionableItemHDto actionableitem1 = new ActionableItemHDto();
    actionableitem1.setId(1L);
    actionableitem1.setJob(new LinkResource(1L));
    actionableitem1.setStatusH(new CodicilStatusHDto());
    actionableitem1.getStatusH().setId(CodicilStatus.AI_OPEN.getValue());
    ais.add(actionableitem1);

    final ActionableItemHDto actionableitem2 = new ActionableItemHDto();
    actionableitem2.setId(1L);
    actionableitem2.setJob(new LinkResource(1L));
    actionableitem2.setStatusH(new CodicilStatusHDto());
    actionableitem2.getStatusH().setId(CodicilStatus.AI_CANCELLED.getValue());
    ais.add(actionableitem2);

    return ais;

  }

  /**
   * Test for hydrating job data.
   *
   * @throws IOException when if there is error fetching services related to codicil.
   * @throws ClassDirectException when there is error hydrating codicil related details.
   */
  @Test
  public final void shouldReturnWithHydratedJob() throws IOException, ClassDirectException {
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);


    final List<CoCHDto> cocs = new ArrayList<>();
    final CoCHDto coc1 = new CoCHDto();
    cocs.add(coc1);

    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    final ActionableItemHDto actionableItem1 = new ActionableItemHDto();
    actionableItems.add(actionableItem1);

    final JobHDto job1 = new JobHDto();
    job1.setId(1L);

    final CodicilDefectQueryHDto query = new CodicilDefectQueryHDto();
    List<Long> statusIds = Arrays.asList(14L, 23L, 3L, 4L);
    query.setStatusList(statusIds);

    // test when job is not null
    when(jobRetrofitService.getJobsByJobId(anyLong())).thenReturn(Calls.response(job1));
    when(assetReferenceService.getCodicilStatuses()).thenReturn(getCodicilStatus());
    when(assetServiceDelegate.getCoCQueryDto(any(), anyLong())).thenReturn(Calls.response(mockCoC()));
    when(assetServiceDelegate.getActionableItemQueryDto(any(), anyLong())).thenReturn(Calls.response(mockAi()));

    CodicilActionableHDto codicil = assetService.getCodicils(MAST_ASSET_ID_1, query);
    assertEquals(job1.getId(), codicil.getCocHDtos().get(0).getJobH().getId());
    assertEquals(codicil.getCocHDtos().get(0).getStatusH().getId(), CodicilStatus.COC_OPEN.getValue());
    assertEquals(codicil.getActionableItemHDtos().get(0).getStatusH().getId(), CodicilStatus.AI_OPEN.getValue());
    assertEquals(codicil.getCocHDtos().get(1).getStatusH().getId(), CodicilStatus.COC_CANCELLED.getValue());
    assertEquals(codicil.getActionableItemHDtos().get(1).getStatusH().getId(), CodicilStatus.AI_CANCELLED.getValue());
    assertEquals(job1.getId(), codicil.getActionableItemHDtos().get(0).getJobH().getId());

    // test exception logger flow
    when(jobRetrofitService.getJobsByJobId(anyLong())).thenReturn(Calls.failure(new IOException()));
    CodicilActionableHDto codicil4 = assetService.getCodicils(MAST_ASSET_ID_1, query);
    // test when job id is null
    job1.setId(null);
    when(jobRetrofitService.getJobsByJobId(anyLong())).thenReturn(Calls.response(job1));

    CodicilActionableHDto codicil2 = assetService.getCodicils(MAST_ASSET_ID_1, query);
    Assert.assertNull(codicil2.getCocHDtos().get(0).getJobH().getId());
    Assert.assertNull(codicil2.getActionableItemHDtos().get(0).getJobH().getId());
    // test when job is null
    cocs.removeAll(cocs);
    actionableItems.removeAll(actionableItems);

    cocs.add(new CoCHDto());
    actionableItems.add(new ActionableItemHDto());
  }

  /**
   * Test for hydrating job data when job is null.
   *
   * @throws IOException when if there is error fetching services related to codicil.
   * @throws ClassDirectException when there is error hydrating codicil related details when job is
   *         null.
   */
  @Test
  public final void shouldReturnWithHydratedJobFail() throws IOException, ClassDirectException {
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);


    final List<CoCHDto> cocs = new ArrayList<>();
    final CoCHDto coc1 = new CoCHDto();
    cocs.add(coc1);

    final List<ActionableItemHDto> actionableItems = new ArrayList<>();
    final ActionableItemHDto actionableItem1 = new ActionableItemHDto();
    actionableItems.add(actionableItem1);

    final JobHDto job1 = new JobHDto();
    job1.setId(1L);

    final CodicilDefectQueryHDto query = new CodicilDefectQueryHDto();
    List<Long> statusIds = Arrays.asList(14L, 23L, 3L, 4L);
    query.setStatusList(statusIds);

    // test when job is not null
    when(assetReferenceService.getCodicilStatuses()).thenReturn(getCodicilStatus());
    when(jobRetrofitService.getJobsByJobId(anyLong())).thenReturn(Calls.failure(new IOException()));
    when(assetServiceDelegate.getCoCQueryDto(any(), anyLong())).thenReturn(Calls.response(mockCoC()));
    when(assetServiceDelegate.getActionableItemQueryDto(any(), anyLong())).thenReturn(Calls.response(mockAi()));

    CodicilActionableHDto codicil3 = assetService.getCodicils(MAST_ASSET_ID_1, query);
    Assert.assertNull(codicil3.getCocHDtos().get(0).getJobH());
    Assert.assertNull(codicil3.getActionableItemHDtos().get(0).getJobH());

  }



  /**
   * Should return asset id for imo number.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnAssetIdByImoNumber() throws ClassDirectException {
    mockMastAssets(true, MAST_ASSET_ID_1);

    final Long assetId = assetService.getAssetIdByImoNumber(DEFAULT_IMO_NUMBER);
    assertNotNull(assetId);
    assertFalse(assetId < 0);
  }

  /**
   * Should return asset for imo number.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnAssetByImoNumber() throws ClassDirectException {
    mockMastAssets(true, MAST_ASSET_ID_1);
    final AssetHDto asset = assetService.getAssetByImoNumber(DEFAULT_IMO_NUMBER);
    assertNotNull(asset);
    assertFalse(asset.getId() < 0);
    assertEquals("AssetName", "Test Asset", asset.getName());
  }

  /**
   * Tests to return exception if asset not found.
   *
   * @throws ClassDirectException if asset not found for Imo number or more than one asset found or
   *         exception occured.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void shouldReturnRecordNotFoundExceptionAssetByImoNumber() throws ClassDirectException {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();

    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));

    final AssetHDto asset = assetService.getAssetByImoNumber(DEFAULT_IMO_NUMBER);
  }

  /**
   * Tests to return exception if more than one asset found.
   *
   * @throws ClassDirectException if asset not found for Imo number or more than one asset found or
   *         exception occured.
   */
  @Test(expected = ClassDirectException.class)
  public final void shouldReturnClassDirectExceptionMoreThanOneAssetByImoNumber() throws ClassDirectException {
    mockMastMultipleAssets(true, MAST_ASSET_ID_1);
    final AssetHDto asset = assetService.getAssetByImoNumber(DEFAULT_IMO_NUMBER);
  }

  /**
   * Tests to return exception if imo number is invalid.
   *
   * @throws ClassDirectException if asset not found for Imo number or more than one asset found or
   *         exception occured.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void shouldReturnRecordNotFoundExceptionForInavlidAssetByImoNumber() throws ClassDirectException {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();

    final MultiAssetDto multiAssetDto = new MultiAssetDto();
    multiAssetDto.setMastAsset(mockMastAsset(MAST_ASSET_ID_2));
    multiAssetDto.setFlagMMS(true);
    multiAssetDtos.add(multiAssetDto);
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    final AssetHDto asset = assetService.getAssetByImoNumber(DEFAULT_IMO_NUMBER);
  }

  /**
   * Should return page resource of schedule services.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   * @throws CloneNotSupportedException when error.
   */
  @Test
  public final void shouldReturnScheduleServicesForAsset()
      throws ClassDirectException, IOException, CloneNotSupportedException {
    when(serviceReference.getAssetServicesQuery(anyLong(), anyMapOf(String.class, String.class)))
        .thenReturn(Calls.response(mockServiceData()));
    Mockito.when(productReferenceService.getProductGroup(anyLong())).thenReturn(null);

    final ServiceQueryDto query = new ServiceQueryDto();
    final PageResource<ScheduledServiceHDto> scheduledServices =
        assetService.getServices(INITIAL_PAGE, DEFAULT_PAGE_SIZE, MAST_ASSET_ID_1, query);
    assertNotNull(scheduledServices);
    assertNotNull(scheduledServices.getContent());
    assertFalse(scheduledServices.getContent().isEmpty());
    scheduledServices.getContent().stream().forEach(ser -> Assert.assertTrue(ser.getActive().equals(Boolean.TRUE)));
    scheduledServices.getContent().stream()
        .forEach(ser -> Assert.assertNull(ser.getServiceCatalogueH().getProductGroupH()));
  }

  /**
   * Query should return mast and ihs results.
   *
   * @throws ClassDirectException when exception occurs while executing mast and ihs query.
   */
  @Test
  public final void shouldReturnMastAndIhsResults() throws ClassDirectException {
    mockIhs(MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3);

    final MastQueryBuilder mastQueryBuilder =
        MastQueryBuilder.create().mandatory(MastQueryBuilder.assetId(QueryRelationshipType.EQ, 1L));
    List<AssetHDto> assets = assetService.mastIhsQuery(mastQueryBuilder, null, null);
    assets.forEach(asset -> {
      assertNotNull(asset);
      assertTrue(asset.getCode().startsWith("IHS"));
    });

    mockIhsAndMast(true, MAST_ASSET_ID_1, MAST_ASSET_ID_3);
    final MastQueryBuilder queryBuilder =
        MastQueryBuilder.create().mandatory(MastQueryBuilder.assetId(QueryRelationshipType.GT, 1L));
    assets = assetService.mastIhsQuery(queryBuilder, null, null);
    assets.forEach(asset -> {
      assertNotNull(asset);
      assertTrue(asset.getCode().startsWith("LRV"));
    });
  }

  /**
   * Query should return mast and ihs results, ordered and paginated.
   *
   * @throws ClassDirectException when exception occurs while executing mast and ihs query.
   */
  @Test
  public final void shouldReturnOrderedAndPaginatedMast() throws ClassDirectException {
    mockIhsAndMastOrderedByYardNumber(MAST_ASSET_ID_1, MAST_ASSET_ID_3);

    AssetPageResource assetPageResource =
        assetService.mastIhsQuery(MastQueryBuilder.create(), 1, 2, SortOptionEnum.YardNumber, OrderOptionEnum.ASC);
    assertTrue(assetPageResource.getContent().get(0).getYardNumber()
        .compareTo(assetPageResource.getContent().get(1).getYardNumber()) < 0);

    assetPageResource =
        assetService.mastIhsQuery(MastQueryBuilder.create(), 1, 2, SortOptionEnum.YardNumber, OrderOptionEnum.DESC);
    assertTrue(assetPageResource.getContent().get(0).getYardNumber()
        .compareTo(assetPageResource.getContent().get(1).getYardNumber()) > 0);
  }

  /**
   * Query should return mast and ihs results sorted by classification society.
   *
   * @throws ClassDirectException when exception occurs while executing mast and ihs query.
   */
  @Test
  public final void testMastIhsAssetSortedByClassSociety() throws ClassDirectException {
    mockMastIhsQueryAssetSortByClassSociety(MAST_ASSET_ID_1, MAST_ASSET_ID_3);

    AssetPageResource assetPageResource =
        assetService.mastIhsQuery(MastQueryBuilder.create(), 1, 2, SortOptionEnum.ClassSociety, OrderOptionEnum.ASC);
    assertTrue(assetPageResource.getContent().get(0).getIhsAssetDto().getIhsAsset().getClassList()
        .compareTo(assetPageResource.getContent().get(1).getIhsAssetDto().getIhsAsset().getClassList()) > 0);

    assetPageResource =
        assetService.mastIhsQuery(MastQueryBuilder.create(), 1, 2, SortOptionEnum.ClassSociety, OrderOptionEnum.DESC);
    assertTrue(assetPageResource.getContent().get(0).getIhsAssetDto().getIhsAsset().getClassList()
        .compareTo(assetPageResource.getContent().get(1).getIhsAssetDto().getIhsAsset().getClassList()) < 0);
  }

  /**
   * Query should return mast and ihs results sorted by dead weight.
   *
   * @throws ClassDirectException when exception occurs while executing mast and ihs query.
   */
  @Test
  public final void testMastIhsAssetSortedByDeadWeight() throws ClassDirectException {
    mockMastIhsQueryAssetSortByDeadWeight(MAST_ASSET_ID_1, MAST_ASSET_ID_3);

    AssetPageResource assetPageResource =
        assetService.mastIhsQuery(MastQueryBuilder.create(), 1, 2, SortOptionEnum.DeadWeight, OrderOptionEnum.ASC);
    assertTrue(assetPageResource.getContent().get(0).getDeadWeight()
        .compareTo(assetPageResource.getContent().get(1).getDeadWeight()) > 0);

    assetPageResource =
        assetService.mastIhsQuery(MastQueryBuilder.create(), 1, 2, SortOptionEnum.DeadWeight, OrderOptionEnum.DESC);
    assertTrue(assetPageResource.getContent().get(0).getDeadWeight()
        .compareTo(assetPageResource.getContent().get(1).getDeadWeight()) < 0);
  }

  /**
   * Return list of assets with favourites.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void returnListOfFavouriteAssets() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(true);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertTrue(assets.get(ZERO).getIsFavourite());
    Assert.assertTrue(assets.get(ONE).getIsFavourite());
    Assert.assertFalse(assets.get(TWO).getIsFavourite());
    Assert.assertFalse(assets.get(THREE).getIsFavourite());
  }

  /**
   * Return empty list of assets without favourites.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void returnEmptyListOfFavouriteAssets() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(true);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertTrue(assets.isEmpty());
  }

  /**
   * Return list of ihs assets with favourites.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void returnListOfIhsFavouriteAssets() throws ClassDirectException, IOException {
    mockIhsAndMast(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserIhsFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(true);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertTrue(assets.get(ZERO).getIsFavourite());
    Assert.assertFalse(assets.get(ONE).getIsFavourite());
    Assert.assertFalse(assets.get(TWO).getIsFavourite());
    Assert.assertFalse(assets.get(THREE).getIsFavourite());
  }

  /**
   * Tests success scenario to get list of assets by paginate them internally
   * {@link AssetService#getAssetsByPagination(String, AssetQueryHDto, MastQueryBuilder)} with mast
   * query builder.
   *
   * @throws ClassDirectException when error in execution flow.
   * @throws IOException when mast api call fail.
   */
  @Test
  public final void testGetAssetsByPagination() throws ClassDirectException, IOException {
    mockMastAssetsWithPagination(33L, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserIhsFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    final MastQueryBuilder queryBuilder = new MastQueryBuilder();
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(true);
    final List<AssetHDto> assets = assetService.getAssetsByPagination(USER_ID_1001, assetQuery, queryBuilder);
    verify(assetRetrofitService, times(3)).mastIhsQueryUnsorted(Mockito.any(PrioritisedAbstractQueryDto.class),
        Mockito.anyInt(), Mockito.anyInt());
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
  }

  /**
   * Tests success scenario to get list of assets by paginate them internally
   * {@link AssetService#getAssetsByPagination(String, AssetQueryHDto, MastQueryBuilder)} with out
   * mast query builder.
   *
   * @throws ClassDirectException when error in execution flow.
   * @throws IOException when mast api call fail.
   */
  @Test
  public final void testGetAssetsByPaginationWithOutQueryBuilder() throws ClassDirectException, IOException {
    mockMastAssetsWithPagination(80L, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserIhsFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    final MastQueryBuilder builder = MastQueryBuilder.create();
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(false);
    final List<AssetHDto> assets = assetService.getAssetsByPagination(USER_ID_1001, assetQuery, builder);
    verify(assetRetrofitService, times(5)).mastIhsQueryUnsorted(Mockito.any(PrioritisedAbstractQueryDto.class),
        Mockito.anyInt(), Mockito.anyInt());
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
  }

  /**
   * Tests success scenario to get list of assets by paginate them internally
   * {@link AssetService#getAssetsByPagination(String, AssetQueryHDto, MastQueryBuilder)} when
   * mast-and-ihs-query fail.
   *
   * @throws ClassDirectException when error in execution flow.
   * @throws IOException when mast api call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetAssetsByPaginationFail() throws ClassDirectException, IOException {
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserIhsFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    final MastQueryBuilder builder = MastQueryBuilder.create();
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(false);
    Mockito.when(assetRetrofitService.mastIhsQueryUnsorted(Mockito.any(PrioritisedAbstractQueryDto.class),
        Mockito.anyInt(), Mockito.anyInt())).thenReturn(Calls.failure(new IOException()));
    assetService.getAssetsByPagination(USER_ID_1001, assetQuery, builder);
  }

  /**
   * Return list of assets search by former asset
   * {@link AssetService#getAssetsQueryPageResource(String, Integer, Integer, String, String, AssetQueryHDto)}
   * .
   *
   * @throws ClassDirectException any error in execution flow/ preparing query builder.
   * @throws IOException when mast api call fails.
   */
  @Test
  public final void testSearchByFormerAssetName() throws ClassDirectException, IOException {
    mockMastAssetsWithFormerAssets(true, MAST_ASSET_ID_1);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserIhsFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setFormerAssetName("former");
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertEquals(assets.get(ZERO).getName(), "Test Asset");
    Assert.assertEquals(assets.get(ZERO).getAllVersions().get(ZERO).getName(), "Former asset");
    Assert.assertNotEquals(assets.get(ZERO).getAllVersions().get(ZERO).getVersionId(),
        assets.get(ZERO).getParentPublishVersionId());
  }

  /**
   * Return list of assets by asset name
   * {@link AssetService#getAssetsQueryPageResource(String, Integer, Integer, String, String, AssetQueryHDto)}
   * .
   *
   * @throws ClassDirectException any error in execution flow/ preparing query builder.
   * @throws IOException when mast api call fails.
   */
  @Test
  public final void testSearchByAssetName() throws ClassDirectException, IOException {
    mockMastAssetsWithFormerAssets(true, MAST_ASSET_ID_1);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserIhsFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setSearch("Test");
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertEquals(assets.get(ZERO).getName(), "Test Asset");
    Assert.assertEquals(assets.get(ZERO).getAllVersions().get(ZERO).getName(), "Former asset");
    Assert.assertNotEquals(assets.get(ZERO).getAllVersions().get(ZERO).getVersionId(),
        assets.get(ZERO).getParentPublishVersionId());
  }

  /**
   * Return list of ihs assets with favourites.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void returnListOfIhsandMastFavouriteAssets() throws ClassDirectException, IOException {
    mockIhsAndMast(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfileRestrictAll(USER_ID_1001);
    mockSubFleet(USER_ID_1001, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);
    mockUserIhsandMastFavourites(USER_ID_1001, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setIsFavourite(true);
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertTrue(assets.get(ZERO).getIsFavourite());
    Assert.assertTrue(assets.get(ONE).getIsFavourite());
    Assert.assertTrue(assets.get(TWO).getIsFavourite());
    Assert.assertFalse(assets.get(THREE).getIsFavourite());
  }

  /**
   * Return list of assets.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void returnListOfAssets() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfiles(USER_ID_1001);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertFalse(assets.get(ZERO).getIsFavourite());
    Assert.assertFalse(assets.get(ONE).getIsFavourite());
    Assert.assertFalse(assets.get(TWO).getIsFavourite());
    Assert.assertFalse(assets.get(THREE).getIsFavourite());
  }

  /**
   * Return list of assets.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void returnListOfAssetsWithIMONumber() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1);
    mockUserProfiles(USER_ID_1001);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    assertNotNull(assets.get(ZERO).getImoNumber());
  }

  /**
   * Return list of assets.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void returnListOfAssetsForClientUserWithClientType() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2, MAST_ASSET_ID_3, MAST_ASSET_ID_4);
    mockUserProfilesWithClientType(USER_ID_1001);

    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    final List<AssetHDto> assets = assetService
        .getAssetsQueryPageResource(USER_ID_1001, INITIAL_PAGE, DEFAULT_PAGE_SIZE, null, null, assetQuery).getContent();
    assertNotNull(assets);
    assertFalse(assets.isEmpty());
    Assert.assertFalse(assets.get(ZERO).getIsFavourite());
    Assert.assertFalse(assets.get(ONE).getIsFavourite());
    Assert.assertFalse(assets.get(TWO).getIsFavourite());
    Assert.assertFalse(assets.get(THREE).getIsFavourite());
  }

  /**
   * AssetQuery should be able to be converted to AbstractQuery.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void conversionBetweenAssetQueryToAbstractQuery() throws ClassDirectException, IOException {
    final AssetQueryHDto assetQuery = new AssetQueryHDto();
    assetQuery.setAssetTypeId(Collections.singletonList(1000L));
    assetQuery.setFlagStateId(Collections.singletonList(1000L));
    assetQuery.setGrossTonnageMin(DEFAULT_MIN_DOUBLE);
    assetQuery.setGrossTonnageMax(DEFAULT_MAX_DOUBLE);
    assetQuery.setClassStatusId(Collections.singletonList(1000L));
    assetQuery.setBuildDateMin(DEFAULT_MIN_DATE);
    assetQuery.setBuildDateMax(DEFAULT_MAX_DATE);
    assetQuery.setIdList(Collections.singletonList(1000L));
    assetQuery.setClientIds(Collections.singletonList(1000L));
    assetQuery.setClientName(DEFAULT_BUILDER_NAME);
    assetQuery.setYardNumber(DEFAULT_YARD_NUMBER);
    assetQuery.setAssetTypeCodes(Collections.singletonList(DEFAULT_IMO_NUMBER));
    assetQuery.setCodicilDueDateMin(DEFAULT_MIN_DATE);
    assetQuery.setCodicilDueDateMax(DEFAULT_MAX_DATE);
    assetQuery.setSearch(DEFAULT_BUILDER_NAME);
    assetQuery.setLifecycleStatusId(Collections.singletonList(1000L));
    assetQuery.setImoNumber(Collections.singletonList("1000"));
    assetQuery.setFlagStateCodes(Collections.singletonList(DEFAULT_FLAG_CODE));
    mockIhsAndMast(true, MAST_ASSET_ID_3, MAST_ASSET_ID_1, MAST_ASSET_ID_4);
    mockUserProfiles("1000");
    final AssetPageResource assets = assetService.getAssets(DEFAULT_USER_ID, assetQuery, null, null);
    assertNotNull(assets);
  }

  /**
   * AssetQuery should return customer by imo number.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void shouldReturnCustomersByImoNumber() throws ClassDirectException {
    final IhsCompanyContactDto companyContact = new IhsCompanyContactDto();
    companyContact.setCompanyName(DEFAULT_COMPANY_NAME);
    when(ihsServiceDelegate.getCompanyContactByImoNumber(anyString())).thenReturn(Calls.response(companyContact));

    final IhsAssetDetailsDto ihsAsset = new IhsAssetDetailsDto();
    final IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setName(DEFAULT_ASSET_NAME);
    ihsAssetDto.setId("1000019");
    final IhsAssetTypeDto assetType = new IhsAssetTypeDto();
    assetType.setStatDeCode(DEFAULT_ASSET_TYPE);
    ihsAssetDto.setIhsAssetType(assetType);
    ihsAssetDto.setDocCompany(DEFAULT_IHS_DOC_COMPANY);
    ihsAssetDto.setDocCode(DEFAULT_DOC_CODE);
    // TODO mock ihs asset
    ihsAsset.setIhsAsset(ihsAssetDto);
    when(ihsServiceDelegate.getAssetDetailsByImoNumber(eq("1000019"))).thenReturn(Calls.response(ihsAsset));

    final IhsAssetClientDto dto = assetService.getCustomersByImoNumber("1000019");
    assertNotNull(dto);
    assertEquals("1000019", dto.getIhsAssetDetailsDto().getIhsAsset().getId());
    assertEquals(DEFAULT_ASSET_NAME, dto.getIhsAssetDetailsDto().getIhsAsset().getName());
    assertEquals(DEFAULT_ASSET_TYPE, dto.getIhsAssetDetailsDto().getIhsAsset().getIhsAssetType().getStatDeCode());

  }

  /**
   * Tests success scenario to calculate asset overall status and asset has postponement services
   * {@link AssetService#calculateOverAllStatus(AssetHDto)} when having postponement service and no
   * postponement service.
   *
   * @throws ClassDirectException if mast api call fails or any execution flow error.
   */
  @Test
  public final void testAssetOverAllStatusOverDue() throws ClassDirectException {

    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);
    final List<AssetEarliestEntitiesHDto> earliestEntitiesList = new ArrayList<>();
    final AssetEarliestEntitiesHDto earliestEntities1 = new AssetEarliestEntitiesHDto();
    earliestEntities1.setAssetId(ONE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities2 = new AssetEarliestEntitiesHDto();
    earliestEntities2.setAssetId(TWO.longValue());
    final AssetEarliestEntitiesHDto earliestEntities3 = new AssetEarliestEntitiesHDto();
    earliestEntities3.setAssetId(THREE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities4 = new AssetEarliestEntitiesHDto();
    earliestEntities4.setAssetId(FOUR.longValue());
    final AssetEarliestEntitiesHDto earliestEntities5 = new AssetEarliestEntitiesHDto();
    earliestEntities5.setAssetId(FIVE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities6 = new AssetEarliestEntitiesHDto();
    earliestEntities6.setAssetId(SIX.longValue());
    final AssetEarliestEntitiesHDto earliestEntities7 = new AssetEarliestEntitiesHDto();
    earliestEntities7.setAssetId(SEVEN.longValue());
    final AssetEarliestEntitiesHDto earliestEntities8 = new AssetEarliestEntitiesHDto();
    earliestEntities8.setAssetId(EIGHT.longValue());
    final AssetEarliestEntitiesHDto earliestEntities9 = new AssetEarliestEntitiesHDto();
    earliestEntities8.setAssetId(NINE.longValue());

    // CoC.
    final CoCDto coc = new CoCDto();
    coc.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(1L)));
    earliestEntities1.setEarliestCoCByDueDate(coc);
    // ActionableItems nonStatuActionableItem.
    final ActionableItemDto nonStatuactionableItem = new ActionableItemDto();
    nonStatuactionableItem.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    earliestEntities2.setEarliestNonStatutoryActionableItemByDueDate(nonStatuactionableItem);
    // ActionableItems statuActionableItem.
    final ActionableItemDto statuactionableItem = new ActionableItemDto();
    statuactionableItem.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    earliestEntities3.setEarliestStatutoryActionableItemByDueDate(statuactionableItem);
    // ServiceStatus.
    final ScheduledServiceDto upperRangeService = new ScheduledServiceDto();
    upperRangeService.setUpperRangeDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    earliestEntities4.setEarliestServiceByUpperRangeDateOrDueDate(upperRangeService);
    final ScheduledServiceDto upperRangeServiceDueDate = new ScheduledServiceDto();
    upperRangeServiceDueDate.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    earliestEntities5.setEarliestServiceByUpperRangeDateOrDueDate(upperRangeServiceDueDate);
    final ScheduledServiceDto postponementDate = new ScheduledServiceDto();
    postponementDate.setPostponementDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    earliestEntities6.setEarliestServiceByPostponementDate(postponementDate);
    // Task.
    final WorkItemDto nonPmsTask = new WorkItemDto();
    nonPmsTask.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    nonPmsTask.setPmsApplicable(false);
    earliestEntities7.setEarliestNonPMSTaskByDueDate(nonPmsTask);
    final WorkItemDto nonPmsTaskPostpone = new WorkItemDto();
    nonPmsTaskPostpone.setPostponementDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    nonPmsTaskPostpone.setPmsApplicable(false);
    earliestEntities8.setEarliestNonPMSTaskByPostponementDate(nonPmsTaskPostpone);
    // Statutory finding
    final StatutoryFindingDto statutoryFinding = new StatutoryFindingDto();
    statutoryFinding.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(ONE.longValue())));
    earliestEntities9.setEarliestStatutoryFindingByDueDate(statutoryFinding);

    earliestEntitiesList.add(earliestEntities1);
    earliestEntitiesList.add(earliestEntities2);
    earliestEntitiesList.add(earliestEntities3);
    earliestEntitiesList.add(earliestEntities4);
    earliestEntitiesList.add(earliestEntities5);
    earliestEntitiesList.add(earliestEntities6);
    earliestEntitiesList.add(earliestEntities7);
    earliestEntitiesList.add(earliestEntities8);
    earliestEntitiesList.add(earliestEntities9);

    // List of Assets.
    final List<AssetHDto> assetDtos = IntStream.rangeClosed(ONE, NINE).mapToObj(runNo -> {
      final AssetHDto assetDto = new AssetHDto();
      assetDto.setId(Integer.toUnsignedLong(runNo));
      return assetDto;
    }).collect(Collectors.toList());

    Mockito.when(retrofitService.assetEarliestEntities(Mockito.any(EarliestEntityQueryDto.class)))
        .thenReturn(Calls.response(earliestEntitiesList));

    try {
      final AssetServiceImpl assetServImpl = (AssetServiceImpl) assetService;
      final Method testMethod = assetServImpl.getClass().getDeclaredMethod("calculateOverallStatus", List.class);
      testMethod.setAccessible(true);
      testMethod.invoke(assetServImpl, assetDtos);

      assetDtos.forEach(singleAsset -> {
        LOGGER.debug("AssetId : {}, DueStatus: {}", singleAsset.getId(), singleAsset.getHasPostponedService());
        if (singleAsset.getId() == Long.valueOf(6)) {
          Assert.assertTrue(singleAsset.getHasPostponedService());
        } else {
          Assert.assertFalse(singleAsset.getHasPostponedService());
        }
      });

    } catch (SecurityException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      LOGGER.error("Fail invoke method. ErrorMessage : {}", e.getMessage(), e);
      Assert.fail("Fail to invoke method calculateOverallStatus()");
    }
  }

  /**
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  // @Test
  public final void testAssetOverAllStatusImminent() throws ClassDirectException, IOException {

    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);
    final List<AssetEarliestEntitiesHDto> earliestEntitiesList = new ArrayList<>();
    final AssetEarliestEntitiesHDto earliestEntities1 = new AssetEarliestEntitiesHDto();
    earliestEntities1.setAssetId(ONE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities2 = new AssetEarliestEntitiesHDto();
    earliestEntities2.setAssetId(TWO.longValue());
    final AssetEarliestEntitiesHDto earliestEntities3 = new AssetEarliestEntitiesHDto();
    earliestEntities3.setAssetId(THREE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities4 = new AssetEarliestEntitiesHDto();
    earliestEntities4.setAssetId(FOUR.longValue());
    final AssetEarliestEntitiesHDto earliestEntities5 = new AssetEarliestEntitiesHDto();
    earliestEntities5.setAssetId(FIVE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities6 = new AssetEarliestEntitiesHDto();
    earliestEntities6.setAssetId(SIX.longValue());
    final AssetEarliestEntitiesHDto earliestEntities7 = new AssetEarliestEntitiesHDto();
    earliestEntities7.setAssetId(SEVEN.longValue());
    final AssetEarliestEntitiesHDto earliestEntities8 = new AssetEarliestEntitiesHDto();
    earliestEntities8.setAssetId(EIGHT.longValue());
    final AssetEarliestEntitiesHDto earliestEntities9 = new AssetEarliestEntitiesHDto();
    earliestEntities9.setAssetId(NINE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities10 = new AssetEarliestEntitiesHDto();
    earliestEntities10.setAssetId(TEN.longValue());
    final AssetEarliestEntitiesHDto earliestEntities11 = new AssetEarliestEntitiesHDto();
    earliestEntities11.setAssetId(ELEVEN.longValue());

    // CoC.
    final CoCDto coc = new CoCDto();
    coc.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities1.setEarliestCoCByDueDate(coc);
    // ActionableItems NonStatutoryItems.
    final ActionableItemDto nonStatuactionableItem = new ActionableItemDto();
    nonStatuactionableItem.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities2.setEarliestNonStatutoryActionableItemByDueDate(nonStatuactionableItem);
    // ActionableItems StatutoryItems.
    final ActionableItemDto statuactionableItem = new ActionableItemDto();
    statuactionableItem.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities3.setEarliestStatutoryActionableItemByDueDate(statuactionableItem);
    // ServiceStatus UpperRangeDate.
    final ScheduledServiceDto upperRangeService = new ScheduledServiceDto();
    upperRangeService.setUpperRangeDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities4.setEarliestServiceByUpperRangeDateOrDueDate(upperRangeService);
    // ServiceStatus DueDate.
    final ScheduledServiceDto upperRangeServiceDueDate = new ScheduledServiceDto();
    upperRangeServiceDueDate.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities5.setEarliestServiceByUpperRangeDateOrDueDate(upperRangeServiceDueDate);
    // ServiceStatus PostponementDate.
    final ScheduledServiceDto postponementDate = new ScheduledServiceDto();
    postponementDate.setPostponementDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities6.setEarliestServiceByPostponementDate(postponementDate);
    // Task nonPms.
    final WorkItemDto nonPmsTask = new WorkItemDto();
    nonPmsTask.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    nonPmsTask.setPmsApplicable(false);
    earliestEntities7.setEarliestNonPMSTaskByDueDate(nonPmsTask);
    // Task nonPmsPostponement.
    final WorkItemDto nonPmsTaskPostpone = new WorkItemDto();
    nonPmsTaskPostpone.setPostponementDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    nonPmsTaskPostpone.setPmsApplicable(false);
    earliestEntities8.setEarliestNonPMSTaskByPostponementDate(nonPmsTaskPostpone);
    // Task pmsTaskDueDate.
    final WorkItemDto pmsTaskDueDate = new WorkItemDto();
    pmsTaskDueDate.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(TWO.longValue())));
    pmsTaskDueDate.setPmsApplicable(true);
    earliestEntities9.setEarliestPMSTaskByDueDate(pmsTaskDueDate);
    // Task pmsTaskPostponementDate.
    final WorkItemDto pmsTaskPostponement = new WorkItemDto();
    pmsTaskPostponement.setPostponementDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(TWO.longValue())));
    pmsTaskPostponement.setPmsApplicable(true);
    earliestEntities10.setEarliestPMSTaskByPostponementDate(pmsTaskPostponement);
    // Statutory finding
    final StatutoryFindingDto statutoryFinding = new StatutoryFindingDto();
    statutoryFinding.setDueDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities11.setEarliestStatutoryFindingByDueDate(statutoryFinding);

    earliestEntitiesList.add(earliestEntities1);
    earliestEntitiesList.add(earliestEntities2);
    earliestEntitiesList.add(earliestEntities3);
    earliestEntitiesList.add(earliestEntities4);
    earliestEntitiesList.add(earliestEntities5);
    earliestEntitiesList.add(earliestEntities6);
    earliestEntitiesList.add(earliestEntities7);
    earliestEntitiesList.add(earliestEntities8);
    earliestEntitiesList.add(earliestEntities9);
    earliestEntitiesList.add(earliestEntities10);
    earliestEntitiesList.add(earliestEntities11);

    // List of Assets.
    final List<AssetHDto> assetDtos = IntStream.rangeClosed(ONE, ELEVEN).mapToObj(runNo -> {
      final AssetHDto assetDto = new AssetHDto();
      assetDto.setId(Integer.toUnsignedLong(runNo));
      return assetDto;
    }).collect(Collectors.toList());
    Mockito.when(retrofitService.assetEarliestEntities(Mockito.any(EarliestEntityQueryDto.class)))
        .thenReturn(Calls.response(earliestEntitiesList));

    try {
      final AssetServiceImpl assetServImpl = (AssetServiceImpl) assetService;
      final Method testMethod = assetServImpl.getClass().getDeclaredMethod("calculateOverallStatus", List.class);
      testMethod.setAccessible(true);
      testMethod.invoke(assetServImpl, assetDtos);
      assetDtos.forEach(singleAsset -> {
        LOGGER.debug("AssetId : {}, DueStatus: {}", singleAsset.getId(), singleAsset.getDueStatusH());
        Assert.assertTrue(DueStatus.IMMINENT.equals(singleAsset.getDueStatusH()));
      });
    } catch (SecurityException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      LOGGER.error("Fail invoke method. ErrorMessage : {}", e.getMessage(), e);
      Assert.fail("Fail to invoke method calculateOverallStatus()");
    }
  }

  /**
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  // @Test
  public final void testAssetOverAllStatusDueSoon() throws ClassDirectException, IOException {

    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<AssetEarliestEntitiesHDto> earliestEntitiesList = new ArrayList<>();
    final AssetEarliestEntitiesHDto earliestEntities1 = new AssetEarliestEntitiesHDto();
    earliestEntities1.setAssetId(ONE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities2 = new AssetEarliestEntitiesHDto();
    earliestEntities2.setAssetId(TWO.longValue());
    final AssetEarliestEntitiesHDto earliestEntities3 = new AssetEarliestEntitiesHDto();
    earliestEntities3.setAssetId(THREE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities4 = new AssetEarliestEntitiesHDto();
    earliestEntities4.setAssetId(FOUR.longValue());
    final AssetEarliestEntitiesHDto earliestEntities5 = new AssetEarliestEntitiesHDto();
    earliestEntities5.setAssetId(FIVE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities6 = new AssetEarliestEntitiesHDto();
    earliestEntities6.setAssetId(SIX.longValue());
    final AssetEarliestEntitiesHDto earliestEntities7 = new AssetEarliestEntitiesHDto();
    earliestEntities7.setAssetId(SEVEN.longValue());
    final AssetEarliestEntitiesHDto earliestEntities8 = new AssetEarliestEntitiesHDto();
    earliestEntities8.setAssetId(EIGHT.longValue());
    final AssetEarliestEntitiesHDto earliestEntities9 = new AssetEarliestEntitiesHDto();
    earliestEntities9.setAssetId(NINE.longValue());

    // CoC.
    final CoCDto coc = new CoCDto();
    coc.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(TWO.longValue()).plusDays(THREE.longValue())));
    earliestEntities1.setEarliestCoCByDueDate(coc);
    earliestEntities2.setEarliestCoCByDueDate(coc);
    earliestEntities3.setEarliestCoCByDueDate(coc);

    // ServiceStatus DueDate.
    final ScheduledServiceDto lowerRangeDateService = new ScheduledServiceDto();
    lowerRangeDateService.setLowerRangeDate(DateUtils.getLocalDateToDate(LocalDate.now().minusDays(TWO.longValue())));
    earliestEntities4.setEarliestServiceByLowerRangeDate(lowerRangeDateService);
    // Task nonPms.
    final WorkItemDto nonPmsTask = new WorkItemDto();
    nonPmsTask.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(TWO.longValue()).plusDays(THREE.longValue())));
    nonPmsTask.setPmsApplicable(false);
    earliestEntities5.setEarliestNonPMSTaskByDueDate(nonPmsTask);
    // Task nonPmsPostponement.
    final WorkItemDto nonPmsTaskPostpone = new WorkItemDto();
    nonPmsTaskPostpone.setPostponementDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(TWO.longValue()).plusDays(THREE.longValue())));
    nonPmsTaskPostpone.setPmsApplicable(false);
    earliestEntities6.setEarliestNonPMSTaskByPostponementDate(nonPmsTaskPostpone);
    // Task pmsTaskDueDate.
    final WorkItemDto pmsTaskDueDate = new WorkItemDto();
    pmsTaskDueDate.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(TWO.longValue()).plusDays(THREE.longValue())));
    pmsTaskDueDate.setPmsApplicable(true);
    earliestEntities7.setEarliestPMSTaskByDueDate(pmsTaskDueDate);
    // Task pmsTaskPostponementDate.
    final WorkItemDto pmsTaskPostponement = new WorkItemDto();
    pmsTaskPostponement.setPostponementDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(TWO.longValue()).plusDays(THREE.longValue())));
    pmsTaskPostponement.setPmsApplicable(true);
    earliestEntities8.setEarliestPMSTaskByPostponementDate(pmsTaskPostponement);
    // Statutory finding
    final StatutoryFindingDto statutoryFinding = new StatutoryFindingDto();
    statutoryFinding.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(TWO.longValue()).plusDays(THREE.longValue())));
    earliestEntities9.setEarliestStatutoryFindingByDueDate(statutoryFinding);

    earliestEntitiesList.add(earliestEntities1);
    earliestEntitiesList.add(earliestEntities2);
    earliestEntitiesList.add(earliestEntities3);
    earliestEntitiesList.add(earliestEntities4);
    earliestEntitiesList.add(earliestEntities5);
    earliestEntitiesList.add(earliestEntities6);
    earliestEntitiesList.add(earliestEntities7);
    earliestEntitiesList.add(earliestEntities8);
    earliestEntitiesList.add(earliestEntities9);

    // List of Assets.
    final List<AssetHDto> assetDtos = IntStream.rangeClosed(ONE, NINE).mapToObj(runNo -> {
      final AssetHDto assetDto = new AssetHDto();
      assetDto.setId(Integer.toUnsignedLong(runNo));
      return assetDto;
    }).collect(Collectors.toList());
    Mockito.when(retrofitService.assetEarliestEntities(Mockito.any(EarliestEntityQueryDto.class)))
        .thenReturn(Calls.response(earliestEntitiesList));

    try {
      final AssetServiceImpl assetServImpl = (AssetServiceImpl) assetService;
      final Method testMethod = assetServImpl.getClass().getDeclaredMethod("calculateOverallStatus", List.class);
      testMethod.setAccessible(true);
      testMethod.invoke(assetServImpl, assetDtos);
      assetDtos.forEach(singleAsset -> {
        LOGGER.debug("AssetId : {}, DueStatus: {}", singleAsset.getId(), singleAsset.getDueStatusH());
        Assert.assertTrue(DueStatus.DUE_SOON.equals(singleAsset.getDueStatusH()));
      });

    } catch (SecurityException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      LOGGER.error("Fail invoke method. ErrorMessage : {}", e.getMessage(), e);
      Assert.fail("Fail to invoke method calculateOverallStatus()");
    }

  }

  /**
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  // @Test
  public final void testAssetOverAllStatusNotDue() throws ClassDirectException, IOException {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);
    final List<AssetEarliestEntitiesHDto> earliestEntitiesList = new ArrayList<>();
    final AssetEarliestEntitiesHDto earliestEntities1 = new AssetEarliestEntitiesHDto();
    earliestEntities1.setAssetId(ONE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities2 = new AssetEarliestEntitiesHDto();
    earliestEntities2.setAssetId(TWO.longValue());
    final AssetEarliestEntitiesHDto earliestEntities3 = new AssetEarliestEntitiesHDto();
    earliestEntities3.setAssetId(THREE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities4 = new AssetEarliestEntitiesHDto();
    earliestEntities4.setAssetId(FOUR.longValue());
    final AssetEarliestEntitiesHDto earliestEntities5 = new AssetEarliestEntitiesHDto();
    earliestEntities5.setAssetId(FIVE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities6 = new AssetEarliestEntitiesHDto();
    earliestEntities6.setAssetId(SIX.longValue());
    final AssetEarliestEntitiesHDto earliestEntities7 = new AssetEarliestEntitiesHDto();
    earliestEntities7.setAssetId(SEVEN.longValue());
    final AssetEarliestEntitiesHDto earliestEntities8 = new AssetEarliestEntitiesHDto();
    earliestEntities8.setAssetId(EIGHT.longValue());
    final AssetEarliestEntitiesHDto earliestEntities9 = new AssetEarliestEntitiesHDto();
    earliestEntities9.setAssetId(NINE.longValue());
    final AssetEarliestEntitiesHDto earliestEntities10 = new AssetEarliestEntitiesHDto();
    earliestEntities10.setAssetId(TEN.longValue());
    final AssetEarliestEntitiesHDto earliestEntities11 = new AssetEarliestEntitiesHDto();
    earliestEntities11.setAssetId(ELEVEN.longValue());
    final AssetEarliestEntitiesHDto earliestEntities12 = new AssetEarliestEntitiesHDto();
    earliestEntities12.setAssetId(TWELVE.longValue());

    // CoC.
    final CoCDto coc = new CoCDto();
    coc.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    earliestEntities1.setEarliestCoCByDueDate(coc);
    // ActionableItems NonStatutoryItems.
    final ActionableItemDto nonStatuactionableItem = new ActionableItemDto();
    nonStatuactionableItem.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    earliestEntities2.setEarliestNonStatutoryActionableItemByDueDate(nonStatuactionableItem);
    // ActionableItems StatutoryItems.
    final ActionableItemDto statuactionableItem = new ActionableItemDto();
    statuactionableItem.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(TWELVE.longValue()).plusDays(TWO.longValue())));
    earliestEntities3.setEarliestStatutoryActionableItemByDueDate(statuactionableItem);
    // ServiceStatus UpperRangeDate.
    final ScheduledServiceDto upperRangeService = new ScheduledServiceDto();
    upperRangeService.setUpperRangeDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    earliestEntities4.setEarliestServiceByUpperRangeDateOrDueDate(upperRangeService);
    // ServiceStatus DueDate.
    final ScheduledServiceDto upperRangeServiceDueDate = new ScheduledServiceDto();
    upperRangeServiceDueDate.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    earliestEntities5.setEarliestServiceByUpperRangeDateOrDueDate(upperRangeServiceDueDate);
    // ServiceStatus DueDate.
    final ScheduledServiceDto lowerRangeDateService = new ScheduledServiceDto();
    lowerRangeDateService.setLowerRangeDate(DateUtils.getLocalDateToDate(LocalDate.now().plusDays(TWO.longValue())));
    earliestEntities6.setEarliestServiceByLowerRangeDate(lowerRangeDateService);
    // ServiceStatus PostponementDate.
    final ScheduledServiceDto postponementDate = new ScheduledServiceDto();
    postponementDate.setPostponementDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    earliestEntities7.setEarliestServiceByPostponementDate(postponementDate);
    // Task nonPms.
    final WorkItemDto nonPmsTask = new WorkItemDto();
    nonPmsTask.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    nonPmsTask.setPmsApplicable(false);
    earliestEntities8.setEarliestNonPMSTaskByDueDate(nonPmsTask);
    // Task nonPmsPostponement.
    final WorkItemDto nonPmsTaskPostpone = new WorkItemDto();
    nonPmsTaskPostpone.setPostponementDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    nonPmsTaskPostpone.setPmsApplicable(false);
    earliestEntities9.setEarliestNonPMSTaskByPostponementDate(nonPmsTaskPostpone);
    // Task pmsTaskDueDate.
    final WorkItemDto pmsTaskDueDate = new WorkItemDto();
    pmsTaskDueDate.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    pmsTaskDueDate.setPmsApplicable(true);
    earliestEntities10.setEarliestPMSTaskByDueDate(pmsTaskDueDate);
    // Task pmsTaskPostponementDate.
    final WorkItemDto pmsTaskPostponement = new WorkItemDto();
    pmsTaskPostponement.setPostponementDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    pmsTaskPostponement.setPmsApplicable(true);
    earliestEntities11.setEarliestPMSTaskByPostponementDate(pmsTaskPostponement);
    // Statutory finding
    final StatutoryFindingDto statutoryFinding = new StatutoryFindingDto();
    statutoryFinding.setDueDate(
        DateUtils.getLocalDateToDate(LocalDate.now().plusMonths(THREE.longValue()).plusDays(TWO.longValue())));
    earliestEntities12.setEarliestStatutoryFindingByDueDate(statutoryFinding);


    earliestEntitiesList.add(earliestEntities1);
    earliestEntitiesList.add(earliestEntities2);
    earliestEntitiesList.add(earliestEntities3);
    earliestEntitiesList.add(earliestEntities4);
    earliestEntitiesList.add(earliestEntities5);
    earliestEntitiesList.add(earliestEntities6);
    earliestEntitiesList.add(earliestEntities7);
    earliestEntitiesList.add(earliestEntities8);
    earliestEntitiesList.add(earliestEntities9);
    earliestEntitiesList.add(earliestEntities10);
    earliestEntitiesList.add(earliestEntities11);
    earliestEntitiesList.add(earliestEntities12);

    // List of Assets.
    final List<AssetHDto> assetDtos = IntStream.rangeClosed(ONE, TWELVE).mapToObj(runNo -> {
      final AssetHDto assetDto = new AssetHDto();
      assetDto.setId(Integer.toUnsignedLong(runNo));
      return assetDto;
    }).collect(Collectors.toList());
    Mockito.when(retrofitService.assetEarliestEntities(Mockito.any(EarliestEntityQueryDto.class)))
        .thenReturn(Calls.response(earliestEntitiesList));
    try {
      final AssetServiceImpl assetServImpl = (AssetServiceImpl) assetService;
      final Method testMethod = assetServImpl.getClass().getDeclaredMethod("calculateOverallStatus", List.class);
      testMethod.setAccessible(true);
      testMethod.invoke(assetServImpl, assetDtos);
      assetDtos.forEach(singleAsset -> {
        LOGGER.debug("AssetId : {}, DueStatus: {}", singleAsset.getId(), singleAsset.getDueStatusH());
        Assert.assertTrue(DueStatus.NOT_DUE.equals(singleAsset.getDueStatusH()));
      });

    } catch (SecurityException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      LOGGER.error("Fail invoke method. ErrorMessage : {}", e.getMessage(), e);
      Assert.fail("Fail to invoke method calculateOverallStatus()");
    }
  }

  /**
   * Should return one eor asset unique by id.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void shouldReturnSingleEorAsset() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockOffice();
    when(userProfileService.getEorsByUserId(any(String.class)))
        .thenReturn(mockEorIMO(USER_ID_1001, String.valueOf(MAST_ASSET_ID_1)));
    when(serviceReference.getAssetServicesQuery(anyLong(), anyMapOf(String.class, String.class)))
        .thenReturn(Calls.response(Collections.singletonList(new ScheduledServiceHDto())));
    final AssetHDto asset = assetService.eorAssetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);

    assertEquals(MAST_ASSET_ID_1, asset.getId());
  }

  /**
   * Should return exception.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void shouldReturnExceptionforNonEorAsset() throws ClassDirectException, IOException {
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockOffice();
    when(userProfileService.getEorsByUserId(any(String.class)))
        .thenReturn(mockEorIMO(USER_ID_1001, DEFAULT_IMO_NUMBER));
    when(serviceReference.getAssetServicesQuery(anyLong(), anyMapOf(String.class, String.class)))
        .thenReturn(Calls.response(Collections.singletonList(new ScheduledServiceHDto())));
    final AssetHDto asset = assetService.eorAssetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_2);
  }

  /**
   * Should return exception.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test(expected = ClassDirectException.class)
  public final void shouldReturnExceptionforEmptyIMOList() throws ClassDirectException, IOException {
    final Set<String> eorImo = new HashSet<>();
    mockMastAssets(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    when(userProfileService.getEorsByUserId(any(String.class))).thenReturn(eorImo);
    when(serviceReference.getAssetServicesQuery(anyLong(), anyMapOf(String.class, String.class)))
        .thenReturn(Calls.response(Collections.singletonList(new ScheduledServiceHDto())));
    final AssetHDto asset = assetService.eorAssetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_2);
  }

  /**
   * Mock multiple mast-asset.
   *
   * @param isMMSService indicator for MMS Service for the given assets.
   * @param assetIds id of assets.
   */
  private void mockMastAssets(final boolean isMMSService, final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber(String.valueOf(assetId));
      multiAssetDto.setMastAsset(mockMastAsset(assetId));
      multiAssetDto.setFlagMMS(isMMSService);
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
  }

  /**
   * Mock multiple mast-asset with pagination.
   *
   * @param numberOfElements the total number of elements in pagination.
   * @param assetIds id of assets.
   */
  private void mockMastAssetsWithPagination(final Long numberOfElements, final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber(String.valueOf(assetId));
      multiAssetDto.setMastAsset(mockMastAsset(assetId));
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);
    PaginationDto pagination = new PaginationDto();
    pagination.setTotalElements(numberOfElements);
    pagination.setNumber(1);
    pageResource.setPagination(pagination);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    multiAssetPageResourceDto.setPagination(pagination);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
  }

  /**
   * Mock multiple mast-asset.
   *
   * @param assetIds id of assets.
   */
  private void mockIhs(final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setIhsAsset(mockIhsAsset(assetId));
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
  }

  /**
   * Mock multiple mast-asset.
   *
   * @param isMMSService indicator for MMS Service for the given assets.
   * @param assetIds id of assets.
   */
  private void mockIhsAndMast(final boolean isMMSService, final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber(String.valueOf(assetId));
      if (assetId.equals(MAST_ASSET_ID_1)) {
           multiAssetDto.setIhsAsset(mockIhsAsset(assetId));
      }
      multiAssetDto.setMastAsset(mockMastAsset(assetId));
      multiAssetDto.setFlagMMS(isMMSService);
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
  }

  /**
   * Mock multiple ihs and mast-asset which are ordered asc or desc by the yard number.
   *
   * @param assetIds id of assets.
   */
  private void mockIhsAndMastOrderedByYardNumber(final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtosAsc = new ArrayList<>();
    final List<MultiAssetDto> multiAssetDtosDesc = new ArrayList<>();
    int i = 1;
    for (final long assetId : assetIds) {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      final IhsAssetDetailsDto ihsAssetDto = mockIhsAsset(assetId);
      ihsAssetDto.getIhsAsset().setYardNumber("ANYNUMBER" + i);
      multiAssetDto.setIhsAsset(ihsAssetDto);

      final AssetLightDto assetLightDto = mockMastAsset(assetId);
      assetLightDto.setYardNumber("ANYNUMBER" + i);
      multiAssetDto.setMastAsset(assetLightDto);

      multiAssetDtosAsc.add(multiAssetDto);
      multiAssetDtosDesc.add(multiAssetDto);
      ++i;
    }
    multiAssetDtosAsc.sort((e1, e2) -> e1.getMastAsset().getYardNumber().compareTo(e2.getMastAsset().getYardNumber()));
    multiAssetDtosDesc.sort((e1, e2) -> e2.getMastAsset().getYardNumber().compareTo(e1.getMastAsset().getYardNumber()));

    final MultiAssetPageResourceDto multiAssetPageResourceDtoAsc = new MultiAssetPageResourceDto();
    multiAssetPageResourceDtoAsc.setContent(multiAssetDtosAsc);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        eq(SortOptionEnum.YardNumber.getField()), eq(OrderOptionEnum.ASC.name())))
            .thenReturn(Calls.response(multiAssetPageResourceDtoAsc));

    final MultiAssetPageResourceDto multiAssetPageResourceDtoDesc = new MultiAssetPageResourceDto();
    multiAssetPageResourceDtoDesc.setContent(multiAssetDtosDesc);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        eq(SortOptionEnum.YardNumber.getField()), eq(OrderOptionEnum.DESC.name())))
            .thenReturn(Calls.response(multiAssetPageResourceDtoDesc));
  }

  /**
   * Mock multiple ihs and mast-asset which are ordered asc or desc by the ihs classList.
   *
   * @param assetIds id of assets.
   */
  private void mockMastIhsQueryAssetSortByClassSociety(final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtosAsc = new ArrayList<>();
    final List<MultiAssetDto> multiAssetDtosDesc = new ArrayList<>();
    int i = 1;
    for (final long assetId : assetIds) {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      final IhsAssetDetailsDto ihsAssetDto = mockIhsAsset(assetId);
      ihsAssetDto.getIhsAsset().setClassList("ClassList" + i);
      multiAssetDto.setIhsAsset(ihsAssetDto);

      multiAssetDtosAsc.add(multiAssetDto);
      multiAssetDtosDesc.add(multiAssetDto);
      ++i;
    }
    multiAssetDtosAsc.sort((e1, e2) -> e1.getIhsAsset().getIhsAsset().getClassList()
        .compareTo(e2.getIhsAsset().getIhsAsset().getClassList()));
    multiAssetDtosAsc.sort((e1, e2) -> e2.getIhsAsset().getIhsAsset().getClassList()
        .compareTo(e1.getIhsAsset().getIhsAsset().getClassList()));

    final MultiAssetPageResourceDto multiAssetPageResourceDtoAsc = new MultiAssetPageResourceDto();
    multiAssetPageResourceDtoAsc.setContent(multiAssetDtosAsc);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        eq(SortOptionEnum.ClassSociety.getField()), eq(OrderOptionEnum.ASC.name()), eq(SORTING_PRIORITY_IHS)))
            .thenReturn(Calls.response(multiAssetPageResourceDtoAsc));

    final MultiAssetPageResourceDto multiAssetPageResourceDtoDesc = new MultiAssetPageResourceDto();
    multiAssetPageResourceDtoDesc.setContent(multiAssetDtosDesc);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        eq(SortOptionEnum.ClassSociety.getField()), eq(OrderOptionEnum.DESC.name()), eq(SORTING_PRIORITY_IHS)))
            .thenReturn(Calls.response(multiAssetPageResourceDtoDesc));
  }

  /**
   * Mock multiple ihs and mast-asset which are ordered asc or desc by the dead weight.
   *
   * @param assetIds id of assets.
   */
  private void mockMastIhsQueryAssetSortByDeadWeight(final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtosAsc = new ArrayList<>();
    final List<MultiAssetDto> multiAssetDtosDesc = new ArrayList<>();
    int i = 1;
    for (final long assetId : assetIds) {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();

      final AssetLightDto assetLightDto = mockMastAsset(assetId);
      assetLightDto.setDeadWeight(new Double(1 + i));
      multiAssetDto.setMastAsset(assetLightDto);
      multiAssetDtosAsc.add(multiAssetDto);
      multiAssetDtosDesc.add(multiAssetDto);
      ++i;
    }
    multiAssetDtosAsc.sort((e1, e2) -> e1.getMastAsset().getDeadWeight().compareTo(e2.getMastAsset().getDeadWeight()));
    multiAssetDtosAsc.sort((e1, e2) -> e2.getMastAsset().getDeadWeight().compareTo(e1.getMastAsset().getDeadWeight()));

    final MultiAssetPageResourceDto multiAssetPageResourceDtoAsc = new MultiAssetPageResourceDto();
    multiAssetPageResourceDtoAsc.setContent(multiAssetDtosAsc);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        eq(SortOptionEnum.DeadWeight.getField()), eq(OrderOptionEnum.ASC.name()), eq(SORTING_PRIORITY_IHS)))
            .thenReturn(Calls.response(multiAssetPageResourceDtoAsc));

    final MultiAssetPageResourceDto multiAssetPageResourceDtoDesc = new MultiAssetPageResourceDto();
    multiAssetPageResourceDtoDesc.setContent(multiAssetDtosDesc);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        eq(SortOptionEnum.DeadWeight.getField()), eq(OrderOptionEnum.DESC.name()), eq(SORTING_PRIORITY_IHS)))
            .thenReturn(Calls.response(multiAssetPageResourceDtoDesc));
  }

  /**
   * Mock single asset.
   *
   * @param assetId asset id.
   * @return asset.
   */
  private AssetLightDto mockMastAsset(final Long assetId) {
    final AssetLightDto asset = new AssetLightDto();
    asset.setId(assetId);
    asset.setYardNumber(DEFAULT_YARD_NUMBER);
    asset.setBuilder(DEFAULT_BUILDER_NAME);
    asset.setName("Test Asset");
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      asset.setLeadImo("1234567");
      asset.setDeadWeight(new Double(2.5));
    } else {
      asset.setLeadImo("123456");
      asset.setDeadWeight(new Double(1.5));
    }
    final AssetTypeDto assetType = new AssetTypeDto();
    assetType.setId(DEFAULT_MAST_ASSET_ID);
    asset.setAssetType(assetType);
    final IhsAssetLink ihsAsset = new IhsAssetLink();
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      ihsAsset.setId("1234567");
    } else {
      ihsAsset.setId("123456");
    }
    asset.setIhsAsset(ihsAsset);
    List<OfficeLinkDto> officeList = new ArrayList<>();
    OfficeLinkDto office = new OfficeLinkDto();
    office.setOffice(new LinkResource(1L));
    office.setOfficeRole(new LinkResource(2L));
    officeList.add(office);
    asset.setOffices(officeList);
    asset.setParentPublishVersionId(1L);

    return asset;
  }

  /**
   * Mock single asset with former assets.
   *
   * @param assetId asset id.
   * @return asset.
   */
  private AssetLightDto mockMastAssetWithFormerAssets(final Long assetId) {
    final AssetLightDto asset = new AssetLightDto();
    asset.setId(assetId);
    asset.setYardNumber(DEFAULT_YARD_NUMBER);
    asset.setBuilder(DEFAULT_BUILDER_NAME);
    asset.setName("Test Asset");
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      asset.setLeadImo("1234567");
    } else {
      asset.setLeadImo("123456");
    }
    final AssetTypeDto assetType = new AssetTypeDto();
    assetType.setId(DEFAULT_MAST_ASSET_ID);
    asset.setAssetType(assetType);
    final IhsAssetLink ihsAsset = new IhsAssetLink();
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      ihsAsset.setId("1234567");
    } else {
      ihsAsset.setId("123456");
    }
    asset.setIhsAsset(ihsAsset);
    List<OfficeLinkDto> officeList = new ArrayList<>();
    OfficeLinkDto office = new OfficeLinkDto();
    office.setOffice(new LinkResource(1L));
    office.setOfficeRole(new LinkResource(2L));
    officeList.add(office);
    asset.setOffices(officeList);
    asset.setParentPublishVersionId(1L);

    List<BaseAssetDto> allVersions = new ArrayList<>();
    BaseAssetDto assetVesrion1 = new BaseAssetDto();
    assetVesrion1.setId(assetId);
    assetVesrion1.setName("Former asset");
    assetVesrion1.setVersionId(2L);
    allVersions.add(assetVesrion1);

    asset.setAllVersions(allVersions);

    return asset;
  }

  /**
   * Mock single asset.
   *
   * @param userId userId.
   * @param imo mocked imo number.
   * @return asset.
   */
  private Set<String> mockEorIMO(final String userId, final String imo) {
    final Set<String> eorImo = new HashSet<>();
    eorImo.add(imo);

    return eorImo;
  }

  /**
   * Mock single asset.
   *
   * @param assetId asset id.
   * @return asset.
   */
  private IhsAssetDetailsDto mockIhsAsset(final Long assetId) {
    final IhsAssetDto asset = new IhsAssetDto();
    asset.setId(String.valueOf(assetId));
    asset.setYardNumber(DEFAULT_YARD_NUMBER);
    asset.setBuilder(DEFAULT_BUILDER_NAME);

    final IhsAssetDetailsDto details = new IhsAssetDetailsDto();
    details.setId(String.valueOf(assetId));
    details.setIhsAsset(asset);

    return details;
  }

  /**
   * Mock empty result on mast-ihs query.
   */
  private void mockEmptyMastIhsResults() {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(Collections.emptyList());
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
  }

  /**
   * Mock user profiles with null client code. This is used for testing the flag codes because flag
   * codes are ignored if client code is not null.
   *
   * @param userIds id of users.
   */
  private void mockUserProfilesWithNullClientCode(final String... userIds) {
    Stream.of(userIds).forEach(userId -> {
      final UserProfiles user = createUserProfile(userId);
      user.setClientCode(null);
      try {
        when(userProfileService.getUser(anyString())).thenReturn(user);
      } catch (final ClassDirectException exception) {
        exception.printStackTrace();
      }
    });
  }

  /**
   * Mock user profiles.
   *
   * @param userIds id of users.
   */
  private void mockUserProfiles(final String... userIds) {
    Stream.of(userIds).forEach(userId -> {
      final UserProfiles user = createUserProfile(userId);
      try {
        when(userProfileService.getUser(anyString())).thenReturn(user);
      } catch (final ClassDirectException exception) {
        exception.printStackTrace();
      }
    });
  }

  /**
   * Mock user profiles.
   *
   * @param userIds id of users.
   */
  private void mockUserProfilesWithClientType(final String... userIds) {
    Stream.of(userIds).forEach(userId -> {
      final UserProfiles user = createUserProfileWithClientType(userId);
      try {
        when(userProfileService.getUser(anyString())).thenReturn(user);
      } catch (final ClassDirectException exception) {
        exception.printStackTrace();
      }
    });
  }

  /**
   * Create user profiles with restrict all.
   *
   * @param userIds user ids.
   */
  private void mockUserProfileRestrictAll(final String... userIds) {
    Stream.of(userIds).forEach(userId -> {
      final UserProfiles user = createUserProfile(userId);
      user.setRestrictAll(true);
      try {
        when(userProfileService.getUser(anyString())).thenReturn(user);
      } catch (final ClassDirectException exception) {
        exception.printStackTrace();
      }
    });
  }

  /**
   * Create default user profile.
   *
   * @param userId user id.
   * @return user profile.
   */
  private UserProfiles createUserProfile(final String userId) {
    final AssetReferenceService assetReferenceService2 = context.getBean(AssetReferenceService.class);
    final BuilderHDto builder = new BuilderHDto();
    builder.setName(DEFAULT_BUILDER_NAME);
    when(assetReferenceService2.getBuilderById(eq(1000L))).thenReturn(builder);

    final UserProfiles profiles = new UserProfiles();
    profiles.setUserId(userId);
    profiles.setFlagCode(DEFAULT_FLAG_CODE);
    profiles.setClientCode(DEFAULT_CLIENT_CODE);
    profiles.setShipBuilderCode(DEFAULT_SHIP_BUILDER_CODE);
    profiles.setRestrictAll(false);
    return profiles;
  }

  /**
   * Create default user profile.
   *
   * @param userId user id.
   * @return user profile.
   */
  private UserProfiles createUserProfileWithClientType(final String userId) {
    final AssetReferenceService assetReferenceService2 = context.getBean(AssetReferenceService.class);
    final BuilderHDto builder = new BuilderHDto();
    builder.setName(DEFAULT_BUILDER_NAME);
    when(assetReferenceService2.getBuilderById(eq(1000L))).thenReturn(builder);

    final UserProfiles profiles = new UserProfiles();
    profiles.setUserId(userId);
    profiles.setFlagCode(DEFAULT_FLAG_CODE);
    profiles.setClientCode(DEFAULT_CLIENT_CODE);
    profiles.setClientType(CustomerRolesEnum.DOC_COMPANY.toString());
    profiles.setShipBuilderCode(DEFAULT_SHIP_BUILDER_CODE);
    profiles.setRestrictAll(false);
    return profiles;
  }

  /**
   * Mock getting subfleet by ID (DAO).
   *
   * @param userId id of user.
   * @param subfleetIds id of subfleets.
   */
  private void mockSubFleet(final String userId, final Long... subfleetIds) {
    final SubFleetService subFleetService = context.getBean(SubFleetService.class);
    try {
      when(subFleetService.getSubFleetById(eq(userId)))
          .thenReturn(Stream.of(subfleetIds).map(subFleetId -> subFleetId).collect(Collectors.toList()));
    } catch (final RecordNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * Mocks user's subfleet IDs.
   *
   * @param userId the user identifier
   * @param whitelist the flag to determine if whitelist enabled.
   * @param subfleetIds the list of user's subfleet ids.
   */
  private void mockSubFleetIds(final String userId, final boolean whitelist, final Long... subfleetIds) {
    final SubFleetService subFleetService = context.getBean(SubFleetService.class);
    try {
      when(subFleetService.getSubFleetIds(eq(userId))).thenReturn(Stream.of(subfleetIds).collect(Collectors.toList()));
      when(subFleetService.isSubFleetEnabled(eq(userId))).thenReturn(whitelist);
    } catch (final RecordNotFoundException e) {
      e.printStackTrace();
    } catch (final ClassDirectException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Mock user favourites.
   *
   * @param userId user id.
   * @param assetIds list of asset ids.
   * @throws RecordNotFoundException when error.
   */
  private void mockUserFavourites(final String userId, final Long... assetIds) throws RecordNotFoundException {
    final List<Favourites> favouritesList = Stream.of(assetIds).map(assetId -> {
      final Favourites favourites = new Favourites();
      favourites.setAssetId(assetId);
      favourites.setSource(CDConstants.MAST_PREFIX);
      when(favouritesRepository.isUserFavourite(eq(userId), eq(assetId))).thenReturn(true);
      return favourites;
    }).collect(Collectors.toList());
    when(favouritesRepository.getUserFavourites(eq(userId))).thenReturn(favouritesList);
  }

  /**
   * Mock user favourites.
   *
   * @param userId user id.
   * @param assetIds list of asset ids.
   * @throws RecordNotFoundException when error.
   */
  private void mockUserIhsFavourites(final String userId, final Long... assetIds) throws RecordNotFoundException {
    final List<Favourites> favouritesList = Stream.of(assetIds).map(assetId -> {
      final Favourites favourites = new Favourites();
      favourites.setAssetId(assetId);
      favourites.setSource(CDConstants.IHS_PREFIX);
      when(favouritesRepository.isUserFavourite(eq(userId), eq(assetId))).thenReturn(true);
      return favourites;
    }).collect(Collectors.toList());
    when(favouritesRepository.getUserFavourites(eq(userId))).thenReturn(favouritesList);
  }

  /**
   * Mock user favourites.
   *
   * @param userId user id.
   * @param assetIds list of asset ids.
   * @throws RecordNotFoundException when error.
   */
  private void mockUserIhsandMastFavourites(final String userId, final Long... assetIds)
      throws RecordNotFoundException {
    final List<Favourites> favouritesList = Stream.of(assetIds).map(assetId -> {
      final Favourites favourites = new Favourites();
      favourites.setAssetId(assetId);
      if (assetId.equals(MAST_ASSET_ID_1)) {
        favourites.setSource(CDConstants.IHS_PREFIX);
      } else {
        favourites.setSource(CDConstants.MAST_PREFIX);
      }
      when(favouritesRepository.isUserFavourite(eq(userId), eq(assetId))).thenReturn(true);
      return favourites;
    }).collect(Collectors.toList());
    when(favouritesRepository.getUserFavourites(eq(userId))).thenReturn(favouritesList);
  }

  /**
   * Test get services with future due date support.
   *
   * @throws Exception value.
   *
   */
  @Test
  public void testGetServicesWithFutureDueDateSupport() throws Exception {

    // mock service data
    final List<ScheduledServiceHDto> services = new ArrayList<>();
    services.add(new ScheduledServiceHDto());

    // mock repeatOf data
    final List<RepeatOfDto> repeatedServices = new ArrayList<>();
    repeatedServices.add(new RepeatOfDto());

    // sample query
    final ServiceQueryDto query = new ServiceQueryDto();
    query.setIncludeFutureDueDates(false);

    when(serviceReference.getAssetServicesQuery(anyLong(), anyMapOf(String.class, String.class)))
        .thenReturn(Calls.response(mockServiceData()));
    Mockito.when(productReferenceService.getProductGroup(anyLong())).thenReturn(mockProductGroup());

    ServiceScheduleDto result = assetService.getServicesWithFutureDueDateSupport(1L, query);

    Assert.assertNotNull(result);
    Assert.assertNull(result.getRepeatedServices());

    query.setIncludeFutureDueDates(true);
    result = assetService.getServicesWithFutureDueDateSupport(1L, query);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getRepeatedServices());
    result.getServices().stream().forEach(ser -> Assert.assertTrue(ser.getActive().equals(Boolean.TRUE)));
    result.getServices().stream().forEach(ser -> Assert.assertNotNull(ser.getServiceCatalogueH().getProductGroupH()));
  }

  /**
   * Test catching interrupted exception.
   *
   * @throws IOException when error.
   * @throws ClassDirectException when error.
   */
  @Test
  public final void testInterruptedExceptionWhenGettingCodicils() throws IOException, ClassDirectException {
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);
    final Long jobId = 1234352323253L;
    final CoCHDto coCHDto = new CoCHDto();
    coCHDto.setJob(new LinkResource(jobId));

    when(assetRetrofitService.getCoCQueryDto(any(), anyLong()))
        .thenReturn(Calls.response(Collections.singletonList(coCHDto)));
    when(assetRetrofitService.getActionableItemQueryDto(any(), anyLong()))
        .thenReturn(Calls.response(Collections.singletonList(new ActionableItemHDto())));
    when(jobRetrofitService.getJobsByJobId(jobId)).thenThrow(InterruptedException.class);

    final CodicilDefectQueryHDto query = new CodicilDefectQueryHDto();
    final CodicilActionableHDto codicil = assetService.getCodicils(MAST_ASSET_ID_1, query);
    assertNotNull(codicil);
  }

  /**
   * Test that associated flag codes are added correctly to the query.
   *
   * @throws ClassDirectException exceptions.
   */
  @Test
  public final void testFlagCodes() throws ClassDirectException {
    mockUserProfilesWithNullClientCode(USER_ID_1001);
    final MastQueryBuilder query = MastQueryBuilder.create();
    final List<String> secondaryFlagCodes = new ArrayList<>();
    secondaryFlagCodes.add(SECONDARY_FLAG_CODE_1);
    secondaryFlagCodes.add(SECONDARY_FLAG_CODE_2);
    when(flagsAssociationsService.getAssociatedFlags(DEFAULT_FLAG_CODE)).thenReturn(secondaryFlagCodes);
    assetService.filterByUser(query, userProfileService.getUser(USER_ID_1001));
    final Object ihsMandatory = Whitebox.getInternalState(query, "ihsPriorityMandatory");
    final AbstractQueryDto queryDto = (AbstractQueryDto) ((List) ihsMandatory).get(0);
    final String[] values = (String[]) queryDto.getValue();
    final List<String> flagCodes = Arrays.asList(values);
    Collections.sort(flagCodes);
    // The flag codes should contain DEFAULT_FLAG_CODE, SECONDARY_FLAG_CODE_1 and
    // SECONDARY_FLAG_CODE_2
    Assert.assertEquals(flagCodes.get(0), SECONDARY_FLAG_CODE_1);
    Assert.assertEquals(flagCodes.get(1), SECONDARY_FLAG_CODE_2);
    Assert.assertEquals(flagCodes.get(2), DEFAULT_FLAG_CODE);
  }

  /**
   * Mocks multiple mast-asset.
   *
   * @param isMMSService indicator for MMS Service for the given assets.
   * @param assetIds id of assets.
   */
  private void mockMastMultipleAssets(final boolean isMMSService, final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber(String.valueOf(assetId));
      multiAssetDto.setMastAsset(mockMastAsset(assetId));
      multiAssetDto.setFlagMMS(isMMSService);
      multiAssetDtos.add(multiAssetDto);

      final MultiAssetDto multiAssetDto1 = new MultiAssetDto();
      multiAssetDto.setImoNumber("1000002");
      multiAssetDto1.setMastAsset(mockMastAsset(MAST_ASSET_ID_2));
      multiAssetDto1.setFlagMMS(isMMSService);
      multiAssetDtos.add(multiAssetDto1);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
  }


  /**
   * Tests success scenario to get single asset has no office list
   * {@link AssetService#assetByCode(String, String)}.
   *
   *
   * @throws ClassDirectException when error in execution flow or any error in mast api call fail.
   */
  @Test
  public final void testReturnSingleAssetHavingNullOfficeList() throws ClassDirectException {
    mockMastAssetsNoOffice(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockUserFavourites(USER_ID_1001, MAST_ASSET_ID_1);
    mockDefaultOffice();
    final AssetHDto asset = assetService.assetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertEquals(MAST_ASSET_ID_1, asset.getId());
    assertTrue(asset.getIsFavourite());
    assertNotNull(asset.getCfoOfficeH());
    Assert.assertEquals("officeCFO@baesystems.com", asset.getCfoOfficeH().getEmailAddress());
  }

  /**
   * Tests success scenario to get single asset has multiple office list
   * {@link AssetService#assetByCode(String, String)}.
   *
   *
   * @throws ClassDirectException when error in execution flow or any error in mast API call fail.
   */
  @Test
  public final void testReturnSingleAssetHavingMultipleOfficeList() throws ClassDirectException {
    mockMastAssetsMultipleCFOOffice(true, MAST_ASSET_ID_1, MAST_ASSET_ID_2);
    mockUserFavourites(USER_ID_1001, MAST_ASSET_ID_1);
    mockDefaultOffice();
    final AssetHDto asset = assetService.assetByCode(USER_ID_1001, CDConstants.MAST_PREFIX + MAST_ASSET_ID_1);
    assertEquals(MAST_ASSET_ID_1, asset.getId());
    assertTrue(asset.getIsFavourite());
    assertNotNull(asset.getCfoOfficeH());
    Assert.assertEquals("officeCFO@baesystems.com", asset.getCfoOfficeH().getEmailAddress());
  }


  /**
   * Mocks office details.
   */
  private void mockOffice() {
    OfficeDto office = new OfficeDto();
    office.setId(1L);
    office.setEmailAddress("Siri@baesystems.com");
    office.setOfficeRole(new LinkResource(2L));
    when(employeeReferenceService.getOffice(eq(1L))).thenReturn(office);
  }

  /**
   * Mocks default CFO office details.
   */
  private void mockDefaultOffice() {
    OfficeDto office = new OfficeDto();
    office.setId(422L);
    office.setEmailAddress("officeCFO@baesystems.com");
    office.setOfficeRole(new LinkResource(2L));
    when(employeeReferenceService.getOffice(eq(422L))).thenReturn(office);
  }

  /**
   * Mocks asset has no office details.
   *
   * @param assetId the unique identifier of asset.
   * @return asset.
   */
  private AssetLightDto mockMastAssetWithAssetOfficeNull(final Long assetId) {
    final AssetLightDto asset = new AssetLightDto();
    asset.setId(assetId);
    asset.setYardNumber(DEFAULT_YARD_NUMBER);
    asset.setBuilder(DEFAULT_BUILDER_NAME);
    asset.setName("Test Asset");
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      asset.setLeadImo("1234567");
    } else {
      asset.setLeadImo("123456");
    }
    final AssetTypeDto assetType = new AssetTypeDto();
    assetType.setId(DEFAULT_MAST_ASSET_ID);
    asset.setAssetType(assetType);
    final IhsAssetLink ihsAsset = new IhsAssetLink();
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      ihsAsset.setId("1234567");
    } else {
      ihsAsset.setId("123456");
    }
    asset.setIhsAsset(ihsAsset);
    asset.setOffices(null);

    return asset;
  }

  /**
   * Mocks multiple mast-asset with former assets.
   *
   * @param isMMSService the indicator for MMS Service for the given assets.
   * @param assetIds the id's of assets.
   */
  private void mockMastAssetsWithFormerAssets(final boolean isMMSService, final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber(String.valueOf(assetId));
      multiAssetDto.setMastAsset(mockMastAssetWithFormerAssets(assetId));
      multiAssetDto.setFlagMMS(isMMSService);
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
  }


  /**
   * Mocks multiple mast-asset has no office.
   *
   * @param isMMSService the indicator for MMS Service for the given assets.
   * @param assetIds the id's of assets.
   */
  private void mockMastAssetsNoOffice(final boolean isMMSService, final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber(String.valueOf(assetId));
      multiAssetDto.setMastAsset(mockMastAssetWithAssetOfficeNull(assetId));
      multiAssetDto.setFlagMMS(isMMSService);
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
  }

  /**
   * Mocks multiple mast-asset has multiple CFO office.
   *
   * @param isMMSService the indicator for MMS Service for the given assets.
   * @param assetIds the id's of assets.
   */
  private void mockMastAssetsMultipleCFOOffice(final boolean isMMSService, final Long... assetIds) {
    final AssetRetrofitService retrofitService = context.getBean(AssetRetrofitService.class);

    final List<MultiAssetDto> multiAssetDtos = new ArrayList<>();
    Stream.of(assetIds).forEach(assetId -> {
      final MultiAssetDto multiAssetDto = new MultiAssetDto();
      multiAssetDto.setImoNumber(String.valueOf(assetId));
      multiAssetDto.setMastAsset(mockMastAssetWithMultipleCFOoffice(assetId));
      multiAssetDto.setFlagMMS(isMMSService);
      multiAssetDtos.add(multiAssetDto);
    });
    final MultiAssetPageResourceDto pageResource = new MultiAssetPageResourceDto();
    pageResource.setContent(multiAssetDtos);

    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class))).thenReturn(Calls.response(pageResource));
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), anyString(), anyString()))
        .thenReturn(Calls.response(pageResource));
    final MultiAssetPageResourceDto multiAssetPageResourceDto = new MultiAssetPageResourceDto();
    multiAssetPageResourceDto.setContent(multiAssetDtos);
    when(retrofitService.mastIhsQuery(any(PrioritisedAbstractQueryDto.class), any(Integer.class), any(Integer.class),
        any(String.class), any(String.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
    when(retrofitService.mastIhsQueryUnsorted(any(PrioritisedAbstractQueryDto.class), any(Integer.class),
        any(Integer.class))).thenReturn(Calls.response(multiAssetPageResourceDto));
  }

  /**
   * Mock single asset has multiple CFO office.
   *
   * @param assetId the unique identifier of asset.
   * @return asset.
   */
  private AssetLightDto mockMastAssetWithMultipleCFOoffice(final Long assetId) {
    final AssetLightDto asset = new AssetLightDto();
    asset.setId(assetId);
    asset.setYardNumber(DEFAULT_YARD_NUMBER);
    asset.setBuilder(DEFAULT_BUILDER_NAME);
    asset.setName("Test Asset");
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      asset.setLeadImo("1234567");
    } else {
      asset.setLeadImo("123456");
    }
    final AssetTypeDto assetType = new AssetTypeDto();
    assetType.setId(DEFAULT_MAST_ASSET_ID);
    asset.setAssetType(assetType);
    final IhsAssetLink ihsAsset = new IhsAssetLink();
    if (assetId.longValue() == MAST_ASSET_ID_2) {
      ihsAsset.setId("1234567");
    } else {
      ihsAsset.setId("123456");
    }
    asset.setIhsAsset(ihsAsset);
    List<OfficeLinkDto> officeList = new ArrayList<>();
    OfficeLinkDto office1 = new OfficeLinkDto();
    office1.setOffice(new LinkResource(1L));
    office1.setOfficeRole(new LinkResource(2L));
    officeList.add(office1);
    OfficeLinkDto office2 = new OfficeLinkDto();
    office2.setOffice(new LinkResource(2L));
    office2.setOfficeRole(new LinkResource(2L));
    officeList.add(office2);
    asset.setOffices(officeList);

    return asset;
  }


  /**
   * Returns the mocked list of services data.
   *
   * @return the mocked list of services data.
   */
  public final List<ScheduledServiceHDto> mockServiceData() {
    final List<ScheduledServiceHDto> services = new ArrayList<>();

    // data1
    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setId(1L);
    service1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setLowerRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setUpperRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setDueStatusH(DueStatus.DUE_SOON);
    service1.setServiceCatalogue(new LinkResource(1L));
    service1.setServiceCatalogueH(new ServiceCatalogueHDto());
    service1.getServiceCatalogueH().setId(1L);
    service1.getServiceCatalogueH().setName("Service");
    service1.getServiceCatalogueH().setDisplayOrder(1);
    service1.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service1.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service1.getServiceCatalogueH().getProductCatalogue().setProductGroup(new LinkResource());
    service1.getServiceCatalogueH().getProductCatalogue().getProductGroup().setId(1L);
    service1.setServiceStatus(new LinkResource(ServiceStatus.NOT_STARTED.getValue()));
    service1.setActive(Boolean.TRUE);
    service1.setOccurrenceNumber(1);
    services.add(service1);

    // data2
    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    service2.setId(2L);
    service2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    service2.setLowerRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service2.setUpperRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service2.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    service2.setDueStatusH(DueStatus.DUE_SOON);
    service2.setServiceCatalogueH(new ServiceCatalogueHDto());
    service2.getServiceCatalogueH().setName("Service 2");
    service2.getServiceCatalogueH().setDisplayOrder(3);
    service2.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service2.getServiceCatalogueH().getProductCatalogue().setName("Other");
    service2.getServiceCatalogueH().getProductCatalogue().setProductGroup(new LinkResource());
    service2.getServiceCatalogueH().getProductCatalogue().getProductGroup().setId(2L);
    service2.setServiceStatus(new LinkResource(ServiceStatus.PART_HELD.getValue()));
    service2.setActive(Boolean.TRUE);
    services.add(service2);

    // data3
    final ScheduledServiceHDto service3 = new ScheduledServiceHDto();
    service3.setId(3L);
    service3.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    service3.setLowerRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service3.setUpperRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service3.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    service3.setDueStatusH(DueStatus.DUE_SOON);
    service3.setServiceCatalogueH(new ServiceCatalogueHDto());
    service3.getServiceCatalogueH().setName("Service");
    service3.getServiceCatalogueH().setId(1L);
    service3.setServiceCatalogue(new LinkResource(1L));
    service3.getServiceCatalogueH().setDisplayOrder(6);
    service3.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service3.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service3.getServiceCatalogueH().getProductCatalogue().setProductGroup(new LinkResource());
    service3.getServiceCatalogueH().getProductCatalogue().getProductGroup().setId(3L);
    service3.setPostponementTypeH(new PostponementTypeHDto());
    service3.getPostponementTypeH().setName("Conditionally Approved");
    service3.setServiceStatus(new LinkResource(ServiceStatus.PART_HELD.getValue()));
    service3.setActive(Boolean.TRUE);
    service3.setOccurrenceNumber(3);
    services.add(service3);


    // data4
    final ScheduledServiceHDto service4 = new ScheduledServiceHDto();
    service4.setId(4L);
    service4.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    service4.setLowerRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service4.setUpperRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service4.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    service4.setDueStatusH(DueStatus.DUE_SOON);
    service4.setServiceCatalogueH(new ServiceCatalogueHDto());
    service4.setServiceCatalogue(new LinkResource(1L));
    service4.getServiceCatalogueH().setName("Service");
    service4.getServiceCatalogueH().setId(1L);
    service4.getServiceCatalogueH().setDisplayOrder(2);
    service4.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service4.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service4.getServiceCatalogueH().getProductCatalogue().setProductGroup(new LinkResource());
    service4.getServiceCatalogueH().getProductCatalogue().getProductGroup().setId(4L);
    service4.setPostponementTypeH(new PostponementTypeHDto());
    service4.getPostponementTypeH().setName("Conditionally Approved");
    service4.setServiceStatus(new LinkResource(ServiceStatus.NOT_STARTED.getValue()));
    service4.setActive(Boolean.FALSE);
    service4.setOccurrenceNumber(2);
    services.add(service4);


    // data5
    final ScheduledServiceHDto service5 = new ScheduledServiceHDto();
    service5.setId(5L);
    service5.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    service5.setLowerRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service5.setUpperRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service5.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    service5.setDueStatusH(DueStatus.DUE_SOON);
    service5.setServiceCatalogueH(new ServiceCatalogueHDto());
    service5.getServiceCatalogueH().setName("Service");
    service5.getServiceCatalogueH().setId(1L);
    service5.setServiceCatalogue(new LinkResource(1L));
    service5.getServiceCatalogueH().setDisplayOrder(4);
    service5.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service5.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service5.getServiceCatalogueH().getProductCatalogue().setProductGroup(new LinkResource());
    service5.getServiceCatalogueH().getProductCatalogue().getProductGroup().setId(5L);
    service5.setPostponementTypeH(new PostponementTypeHDto());
    service5.getPostponementTypeH().setName("Conditionally Approved");
    service5.setServiceStatus(new LinkResource(ServiceStatus.NOT_STARTED.getValue()));
    service5.setActive(Boolean.FALSE);
    service5.setOccurrenceNumber(4);
    services.add(service5);
    return services;
  }

  /**
   * Mockdata for codicil.
   *
   * @return mockCoC.
   */

  private List<CodicilStatusHDto> getCodicilStatus() {
    // create mock data
    final List<CodicilStatusHDto> mockData = new ArrayList<CodicilStatusHDto>();
    final CodicilStatusHDto status1 = new CodicilStatusHDto();
    status1.setId(1L);
    status1.setTypeId(1L);
    mockData.add(status1);
    final CodicilStatusHDto status2 = new CodicilStatusHDto();
    status2.setId(2L);
    status2.setTypeId(2L);
    mockData.add(status2);
    final CodicilStatusHDto status5 = new CodicilStatusHDto();
    status5.setId(5L);
    status5.setTypeId(5L);
    mockData.add(status5);
    return mockData;
  }

  /**
   * The directory test package path.
   */
  private static final String PATH = "data/";


  /**
   * Converts json to object.
   *
   * @throws IOException if coping of stream fails.
   */
  @Test
  public final void testMultiAssetPageResourceMapping() throws IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    MultiAssetPageResourceDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(PATH + "multiAssetPageResource.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), MultiAssetPageResourceDto.class);

    Assert.assertNotNull(content);
  }

  /**
   * Mocks product group.
   *
   * @return the list of product grou.
   */
  private ProductGroupDto mockProductGroup() {
    ProductGroupDto dto = new ProductGroupDto();
    dto.setId(1L);
    dto.setName("Group1");
    dto.setDisplayOrder(1);

    return dto;
  }

  /**
   * Inner config.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Mock.
     *
     * @return mock object.
     */

    @Override
    public AssetService assetService() {
      return new AssetServiceImpl();
    }

    /**
     * @return resource.
     */
    @Bean
    public Resources resources() {
      final Resources resource = new Resources();
      Resources.setInjector(resource);
      return resource;
    }
  }

}
