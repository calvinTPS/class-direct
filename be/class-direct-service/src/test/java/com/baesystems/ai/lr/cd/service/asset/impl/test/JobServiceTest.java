package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CustomerLinkHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.AttachmentsCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobWithFlagsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportCDDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AttachmentCategoryEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.JobService;
import com.baesystems.ai.lr.cd.service.asset.impl.JobServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.BaseAssetDto;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobWithFlagsQueryDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.enums.PartyRole;

import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 *
 * Provides unit test methods for job service.
 *
 * @author yng
 * @author syalavarthi
 * @author SBollu.
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@PrepareForTest({Response.class, Resources.class, SecurityUtils.class})
@ContextConfiguration(classes = JobServiceTest.Config.class)
public class JobServiceTest extends JobServiceMockTestData {
  /**
   *
   * The Logger to display logs.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(JobServiceTest.class);

  /**
   * The {@link ApplicationContext} from Spring context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link JobService} from Spring context.
   */
  @Autowired
  private JobService jobService;

  /**
   * The {@link JobRetrofitService} from Spring context.
   */
  @Autowired
  private JobRetrofitService jobServiceDelegate;

  /**
   * The {@link AttachmentRetrofitService} from Spring context.
   */
  @Autowired
  private AttachmentRetrofitService attachmentService;
  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;
  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;
  /**
   * The {@link EmployeeReferenceService} from Spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public final void init() {
    reset(context.getBean(JobRetrofitService.class));
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    final CDAuthToken auth = new CDAuthToken(listOfRoles);
    final UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    user.setClientCode("100100");
    user.setShipBuilderCode("100100");
    user.setFlagCode("ALA");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);


    PowerMockito.mockStatic(Resources.class, invocation -> {
      return null;
    });
  }

  /**
   * Tests success scenario for internal user
   * {@link JobService#getSurveyReports(Integer, Integer, Long)} should return only 4 reports.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test
  public final void testGetSurveyReports() throws Exception {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);

    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    Mockito.when(assetRetrofitService.getJobsById(1)).thenReturn(Calls.response(mockJobData()));
    Mockito.when(jobRetrofitService.getReportsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(mockSurveyReportData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(1)).thenReturn(Calls.response(mockJobHDto()));
    List<JobWithFlagsHDto> jobsList = mockJobWithFlagsList();
    JobWithFlagsHDto job2 = mockJobWithFlagsDto(2L);
    job2.setFlagTM(false);
    jobsList.add(job2);
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(jobsList));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));

    final PageResource<ReportHDto> result = jobService.getSurveyReports(null, null, 1L);

    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

    Assert.assertNotNull(result);

    // Should return 4 reports - Latest version of FAR and FSR, ESP and TM.
    Assert.assertEquals(4, result.getContent().size());

    final ReportHDto farReport = getReport(result.getContent(), "FAR");
    Assert.assertNotNull("FAR report is missing.", farReport);
    Assert.assertEquals("2016/05/05", formatter.format(farReport.getIssueDate()));

    final ReportHDto fsrReport = getReport(result.getContent(), "FSR");
    Assert.assertNotNull("FSR report is missing.", fsrReport);
    Assert.assertEquals("2016/06/06", formatter.format(fsrReport.getIssueDate()));

    // TM Report should have attachments, and issue date that is the latest attachment creation
    // date.
    final ReportHDto tmReport = getReport(result.getContent(), "TM");
    Assert.assertNotNull("TM report is missing.", tmReport);
    Assert.assertEquals("2016/01/02", formatter.format(tmReport.getIssueDate()));
    Assert.assertNotNull("TM report missing attachment.", tmReport.getAttachments());
    Assert.assertEquals(true, tmReport.getJobH().getHasTMReport());

    // ESP report will be generated only if FSR Report is available.
    final ReportHDto espReport = getReport(result.getContent(), "FSR");
    Assert.assertNotNull("ESP report Can't be generated since FSR report is missing. ", espReport);
    Assert.assertEquals("2016/06/06", formatter.format(espReport.getIssueDate()));
  }

  /**
   * Tests success scenario for client role if jobs with flags query return result
   * {@link JobService#getSurveyReports(Integer, Integer, Long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test
  public final void testGetSurveyReportsClientRole() throws Exception {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);

    mockUserRoleClient();

    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100");

    Mockito.when(assetRetrofitService.getJobsById(1)).thenReturn(Calls.response(mockJobData()));
    Mockito.when(jobRetrofitService.getReportsByJobId(1)).thenReturn(Calls.response(mockSurveyReportData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(1)).thenReturn(Calls.response(mockJobHDto()));

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));

    final PageResource<ReportHDto> result = jobService.getSurveyReports(null, null, 1L);

    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

    Assert.assertNotNull(result);

    // Should return 4 reports - Latest version of FAR and FSR, ESP and TM.
    Assert.assertEquals(4, result.getContent().size());

    final ReportHDto farReport = getReport(result.getContent(), "FAR");
    Assert.assertNotNull("FAR report is missing.", farReport);
    Assert.assertEquals("2016/05/05", formatter.format(farReport.getIssueDate()));

    final ReportHDto fsrReport = getReport(result.getContent(), "FSR");
    Assert.assertNotNull("FSR report is missing.", fsrReport);
    Assert.assertEquals("2016/06/06", formatter.format(fsrReport.getIssueDate()));

    // TM Report should have attachments, and issue date that is the latest attachment creation
    // date.
    final ReportHDto tmReport = getReport(result.getContent(), "TM");
    Assert.assertNotNull("TM report is missing.", tmReport);
    Assert.assertEquals("2016/01/02", formatter.format(tmReport.getIssueDate()));
    Assert.assertNotNull("TM report missing attachment.", tmReport.getAttachments());

    // ESP report will only be a holder.
    final ReportHDto espReport = getReport(result.getContent(), "ESP");
    Assert.assertNotNull("ESP report is missing.", espReport);
  }

  /**
   * Filters the list of report and return the first report that match the report type, if not found
   * return null.
   *
   * @param reportList the list of reports.
   * @param reportType the report type.
   * @return the first matching report for the given type. Otherwise null if not found.
   */
  private ReportHDto getReport(final List<ReportHDto> reportList, final String reportType) {
    return reportList.stream().filter(report -> reportType.equalsIgnoreCase(report.getReportTypeDto().getName()))
        .findFirst().orElse(null);
  }

  /**
   * Tests success scenario for client if client user is not DOC_COMPANY role, then should get mms
   * job reports {@link JobService#getSurveyReports(Integer, Integer, Long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test
  public final void testGetSurveyReportsClientRoleSuccess() throws Exception {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);

    mockUserRoleClient();
    final AssetHDto asset = mockAsset();
    List<CustomerLinkHDto> mastClients = new ArrayList<>();

    CustomerLinkHDto secondMastClient = new CustomerLinkHDto();
    secondMastClient.setPartyRoles(new PartyRoleHDto());
    secondMastClient.getPartyRoles().setId(PartyRole.SHIP_MANAGER.getValue());
    secondMastClient.getPartyRoles().setName(PartyRole.SHIP_MANAGER.name());

    secondMastClient.setParty(new CustomerLinkDto());
    secondMastClient.getParty().setCustomer(new PartyDto());
    secondMastClient.getParty().getCustomer().setImoNumber("100102");

    mastClients.add(secondMastClient);

    asset.setCustomersH(mastClients);

    Mockito.when(assetRetrofitService.getJobsById(1)).thenReturn(Calls.response(mockJobData()));
    Mockito.when(jobRetrofitService.getReportsByJobId(1)).thenReturn(Calls.response(mockSurveyReportData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(1)).thenReturn(Calls.response(mockJobHDto()));

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(Collections.emptyList()));
    final PageResource<ReportHDto> result = jobService.getSurveyReports(null, null, 1L);

    Assert.assertNotNull(result);
    Assert.assertFalse(result.getContent().isEmpty());
  }

  /**
   * Tests success scenario for shipbuilder role
   * {@link JobService#getSurveyReports(Integer, Integer, Long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test
  public final void testGetSurveyReportsShipBuilder() throws Exception {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);

    mockUserRoleShip();

    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100");

    Mockito.when(assetRetrofitService.getJobsById(1)).thenReturn(Calls.response(mockJobData()));
    Mockito.when(jobRetrofitService.getReportsByJobId(1)).thenReturn(Calls.response(mockSurveyReportData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(1)).thenReturn(Calls.response(mockJobHDto()));

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(serviceReferenceService.getServiceCreditStatus(Mockito.anyLong()))
        .thenReturn(mockServiceCreditStatusData());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(Collections.emptyList()));
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.anyLong())).thenReturn(mockServiceCatalogueData());
    final PageResource<ReportHDto> result = jobService.getSurveyReports(null, null, 1L);

    Assert.assertNotNull(result);
    Assert.assertTrue(result.getContent().isEmpty());
  }

  /**
   * Tests success and Failure scenarios for flag role,
   * {@link JobService#getSurveyReports(Integer, Integer, Long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test
  public final void testGetSurveyReportsFlag() throws Exception {

    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);

    mockUserRoleFlag();

    final AssetHDto asset = mockAsset();
    asset.setFlagStateDto(new FlagStateHDto());
    asset.getFlagStateDto().setFlagCode("ALA");

    Mockito.when(assetRetrofitService.getJobsById(1)).thenReturn(Calls.response(mockJobData()));
    Mockito.when(jobRetrofitService.getReportsByJobId(1)).thenReturn(Calls.response(mockSurveyReportData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(1)).thenReturn(Calls.response(mockJobHDto()));

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockRefrerenceDataFlags());
    final PageResource<ReportHDto> result = jobService.getSurveyReports(null, null, 1L);

    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

    Assert.assertNotNull(result);

    // Should return 4 reports - Latest version of FAR and FSR, ESP and TM.
    Assert.assertEquals(4, result.getContent().size());

    final ReportHDto farReport = getReport(result.getContent(), "FAR");
    Assert.assertNotNull("FAR report is missing.", farReport);
    Assert.assertEquals("2016/05/05", formatter.format(farReport.getIssueDate()));

    final ReportHDto fsrReport = getReport(result.getContent(), "FSR");
    Assert.assertNotNull("FSR report is missing.", fsrReport);
    Assert.assertEquals("2016/06/06", formatter.format(fsrReport.getIssueDate()));

    // TM Report should have attachments, and issue date that is the latest attachment creation
    // date.
    final ReportHDto tmReport = getReport(result.getContent(), "TM");
    Assert.assertNotNull("TM report is missing.", tmReport);
    Assert.assertEquals("2016/01/02", formatter.format(tmReport.getIssueDate()));
    Assert.assertNotNull("TM report missing attachment.", tmReport.getAttachments());
    Assert.assertEquals(true, tmReport.getJobH().getHasTMReport());

    // ESP report will only be a holder.
    final ReportHDto espReport = getReport(result.getContent(), "ESP");
    Assert.assertNotNull("ESP report is missing.", espReport);
    Assert.assertEquals(true, espReport.getJobH().getHasESPReport());

    // negative test.
    asset.getFlagStateDto().setFlagCode("AAA");
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    final PageResource<ReportHDto> result1 = jobService.getSurveyReports(null, null, 1L);
    Assert.assertNotNull(result1);
    Assert.assertFalse(result1.getContent().isEmpty());
  }

  /**
   * Tests success scenario for internal user
   * {@link JobService#getSurveyReports(Integer, Integer, Long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test
  public final void testGetSurveyReportsPagination() throws Exception {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);
    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);

    Mockito.when(assetRetrofitService.getJobsById(1)).thenReturn(Calls.response(mockJobData()));
    Mockito.when(jobRetrofitService.getReportsByJobId(1)).thenReturn(Calls.response(mockSurveyReportData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(1)).thenReturn(Calls.response(mockJobHDto()));

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));

    final PageResource<ReportHDto> result = jobService.getSurveyReports(1, 2, 1L);

    Assert.assertNotNull(result);
    Assert.assertEquals(String.valueOf(result.getContent().size()), "2");
  }

  /**
   * Tests failure scenario for internal user
   * {@link JobService#getSurveyReports(Integer, Integer, Long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetSurveyReportsIOException() throws Exception {

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.failure(new IOException("Mock Exception.")));

    jobService.getSurveyReports(null, null, 1L);
  }

  /**
   * Tests failure scenario for internal user internal API's
   * {@link JobService#getSurveyReports(Integer, Integer, Long)}.
   *
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetSurveyReportsIOExceptionInner() throws Exception {
    final AssetRetrofitService assetRetrofitService = context.getBean(AssetRetrofitService.class);
    final JobRetrofitService jobRetrofitService = context.getBean(JobRetrofitService.class);

    Mockito.when(assetRetrofitService.getJobsById(1)).thenReturn(Calls.response(mockJobData()));

    when(jobRetrofitService.getJobsWithFlags(Mockito.any(JobWithFlagsQueryDto.class)))
        .thenReturn(Calls.failure(new IOException("Mock Exception.")));

    final PageResource<ReportHDto> result = jobService.getSurveyReports(null, null, 1L);

    Assert.assertNotNull(result);
  }

  /**
   * Returns mocked job list.
   *
   * @return the list of mocked job.
   */
  private List<JobHDto> mockJobData() {
    final List<JobHDto> jobs = new ArrayList<>();
    final JobHDto job = new JobHDto();
    job.setId(1L);
    job.setAsset(new LinkVersionedResource(1L, 1L));
    job.setJobCategory(new LinkResource(1L));
    jobs.add(job);

    return jobs;
  }

  /**
   * Returns mock survey report list.
   *
   * @return the list of mocked {@link ReportHDto}.
   * @throws ParseException if fail to parse the date string otherwise will always success.
   */
  private List<ReportHDto> mockSurveyReportData() throws ParseException {
    final List<ReportHDto> reports = new ArrayList<>();


    final ReportHDto report1 = new ReportHDto();
    report1.setId(1L);
    report1.setReportVersion(1L);
    report1.setReportType(new LinkResource(1L));
    report1.setReportTypeDto(new ReportTypeHDto());
    report1.getReportTypeDto().setName("FSR");
    report1.setContent(mockServiceReportDto().getContent());
    reports.add(report1);

    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

    final ReportHDto report2 = new ReportHDto();
    report2.setId(2L);
    report2.setIssueDate(formatter.parse("2016/05/05"));
    report2.setReportVersion(1L);
    report2.setReportType(new LinkResource(2L));
    report2.setReportTypeDto(new ReportTypeHDto());
    report2.getReportTypeDto().setName("FAR");
    report2.setContent(mockServiceReportDto().getContent());
    reports.add(report2);

    final ReportHDto report3 = new ReportHDto();
    report3.setId(3L);
    report3.setReportVersion(1L);
    report3.setReportType(new LinkResource(3L));
    report3.setReportTypeDto(new ReportTypeHDto());
    report3.getReportTypeDto().setName("DSR");
    report3.setContent(mockServiceReportDto().getContent());
    reports.add(report3);

    final ReportHDto report4 = new ReportHDto();
    report4.setId(4L);
    report4.setReportVersion(1L);
    report4.setReportType(new LinkResource(4L));
    report4.setReportTypeDto(new ReportTypeHDto());
    report4.getReportTypeDto().setName("DAR");
    report4.setIssueDate(formatter.parse("2016/07/07"));
    report4.setContent(mockServiceReportDto().getContent());
    reports.add(report4);

    final ReportHDto report5 = new ReportHDto();
    report5.setId(5L);
    report5.setReportVersion(2L);
    report5.setIssueDate(formatter.parse("2016/06/06"));
    report5.setReportType(new LinkResource(1L));
    report5.setReportTypeDto(new ReportTypeHDto());
    report5.getReportTypeDto().setName("FSR");
    report5.setContent(mockServiceReportDto().getContent());
    reports.add(report5);

    return reports;
  }

  /**
   * Tests success scenario for admin role {@link JobService#getJobsByAssetIdCall(Long)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsByAssetIdCallTest() throws ClassDirectException, ParseException {
    mockUserRoles();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Assert.assertFalse(jobService.getJobsByAssetIdCall(1L, false).isEmpty());
  }

  /**
   * Tests success scenario for MMS jobs for internalUser : LR_ADMIN, LR_INTERNAL and LR_SUPPORT
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSTest() throws ClassDirectException, ParseException {
    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertFalse(response.isEmpty());
    Assert.assertFalse(response.get(0).getServices().isEmpty());

    // To check if the job is MMS or not
    Assert.assertEquals(response.get(0).getIsMMS(), true);
  }

  /**
   * Tests success scenario for MMS jobs for CLIENT user : if jobs with flags api return jobs.
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSTestClientUser() throws ClassDirectException, ParseException {
    mockUserRoleClient();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100");
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertFalse(response.isEmpty());
    Assert.assertFalse(response.get(0).getServices().isEmpty());
    Assert.assertNotNull(response.get(0).getHasTMReport());
    Assert.assertNotNull(response.get(0).getHasESPReport());
  }


  /**
   * Tests success scenario for MMS jobs for CLIENT user :If client user with doc role can able to
   * get jobs from mast. {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSTestClientCodeFromMast() throws ClassDirectException, ParseException {
    mockUserRoleClient();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    final AssetHDto asset = mockAsset();
    List<CustomerLinkHDto> mastClients = new ArrayList<>();
    CustomerLinkHDto firstMastClient = new CustomerLinkHDto();
    firstMastClient.setPartyRoles(new PartyRoleHDto());
    firstMastClient.getPartyRoles().setId(PartyRole.DOC_COMPANY.getValue());
    firstMastClient.getPartyRoles().setName(PartyRole.DOC_COMPANY.name());

    firstMastClient.setParty(new CustomerLinkDto());
    firstMastClient.getParty().setCustomer(new PartyDto());
    firstMastClient.getParty().getCustomer().setImoNumber("100100");

    CustomerLinkHDto secondMastClient = new CustomerLinkHDto();
    secondMastClient.setPartyRoles(new PartyRoleHDto());
    secondMastClient.getPartyRoles().setId(PartyRole.SHIP_MANAGER.getValue());
    secondMastClient.getPartyRoles().setName(PartyRole.SHIP_MANAGER.name());

    secondMastClient.setParty(new CustomerLinkDto());
    secondMastClient.getParty().setCustomer(new PartyDto());
    secondMastClient.getParty().getCustomer().setImoNumber("100102");

    CustomerLinkHDto thirdMastClient = new CustomerLinkHDto();
    thirdMastClient.setPartyRoles(new PartyRoleHDto());
    thirdMastClient.getPartyRoles().setId(PartyRole.SHIP_OWNER.getValue());
    thirdMastClient.getPartyRoles().setName(PartyRole.SHIP_OWNER.name());

    thirdMastClient.setParty(new CustomerLinkDto());
    thirdMastClient.getParty().setCustomer(new PartyDto());
    thirdMastClient.getParty().getCustomer().setImoNumber("100103");

    CustomerLinkHDto fourthMastClient = new CustomerLinkHDto();
    fourthMastClient.setPartyRoles(new PartyRoleHDto());
    fourthMastClient.getPartyRoles().setId(PartyRole.SHIP_BUILDER.getValue());
    fourthMastClient.getPartyRoles().setName(PartyRole.SHIP_BUILDER.name());

    fourthMastClient.setParty(new CustomerLinkDto());
    fourthMastClient.getParty().setCustomer(new PartyDto());
    fourthMastClient.getParty().getCustomer().setImoNumber("100104");

    mastClients.add(firstMastClient);
    mastClients.add(secondMastClient);
    mastClients.add(thirdMastClient);
    mastClients.add(fourthMastClient);

    asset.setCustomersH(mastClients);
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertFalse(response.isEmpty());
    Assert.assertFalse(response.get(0).getServices().isEmpty());
  }

  /**
   * Tests success scenario for MMS jobs for CLIENT user : when user is doc role
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSTestDiffClientCodeFromMast() throws ClassDirectException, ParseException {
    mockUserRoleClient();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    final AssetHDto asset = mockAsset();
    List<CustomerLinkHDto> mastClients = new ArrayList<>();
    CustomerLinkHDto firstMastClient = new CustomerLinkHDto();
    firstMastClient.setPartyRoles(new PartyRoleHDto());
    firstMastClient.getPartyRoles().setId(PartyRole.DOC_COMPANY.getValue());
    firstMastClient.getPartyRoles().setName(PartyRole.DOC_COMPANY.name());

    firstMastClient.setParty(new CustomerLinkDto());
    firstMastClient.getParty().setCustomer(new PartyDto());
    firstMastClient.getParty().getCustomer().setImoNumber("100101");

    CustomerLinkHDto secondMastClient = new CustomerLinkHDto();
    secondMastClient.setPartyRoles(new PartyRoleHDto());
    secondMastClient.getPartyRoles().setId(PartyRole.SHIP_MANAGER.getValue());
    secondMastClient.getPartyRoles().setName(PartyRole.SHIP_MANAGER.name());

    secondMastClient.setParty(new CustomerLinkDto());
    secondMastClient.getParty().setCustomer(new PartyDto());
    secondMastClient.getParty().getCustomer().setImoNumber("100102");

    CustomerLinkHDto thirdMastClient = new CustomerLinkHDto();
    thirdMastClient.setPartyRoles(new PartyRoleHDto());
    thirdMastClient.getPartyRoles().setId(PartyRole.SHIP_OWNER.getValue());
    thirdMastClient.getPartyRoles().setName(PartyRole.SHIP_OWNER.name());

    thirdMastClient.setParty(new CustomerLinkDto());
    thirdMastClient.getParty().setCustomer(new PartyDto());
    thirdMastClient.getParty().getCustomer().setImoNumber("100103");

    CustomerLinkHDto fourthMastClient = new CustomerLinkHDto();
    fourthMastClient.setPartyRoles(new PartyRoleHDto());
    fourthMastClient.getPartyRoles().setId(PartyRole.SHIP_BUILDER.getValue());
    fourthMastClient.getPartyRoles().setName(PartyRole.SHIP_BUILDER.name());

    fourthMastClient.setParty(new CustomerLinkDto());
    fourthMastClient.getParty().setCustomer(new PartyDto());
    fourthMastClient.getParty().getCustomer().setImoNumber("100104");

    mastClients.add(firstMastClient);
    mastClients.add(secondMastClient);
    mastClients.add(thirdMastClient);
    mastClients.add(fourthMastClient);

    asset.setCustomersH(mastClients);
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertFalse(response.isEmpty());
  }

  /**
   * Tests success scenario for MMS jobs for CLIENT user : If client user with doc role can able to
   * get jobs from mast. LRCD-3649. {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSClientUserTestSuccess() throws ClassDirectException, ParseException {
    mockUserRoleClient();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());

    final List<JobWithFlagsHDto> mockMMSJobList = mockMMSJobWithFlagsList();
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockMMSJobList));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100");
    asset.setVersionId(2L);

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertEquals(mockMMSJobList.size(), response.size());
  }

  /**
   * Tests failure scenario for MMS jobs for CLIENT user if there is no MMS jobs for client user
   * from jobs with flags query api. {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSClientUserDocCodeFail() throws ClassDirectException, ParseException {
    mockUserRoleClient();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(Collections.emptyList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100111");

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertTrue(response.isEmpty());
  }

  /**
   * Tests success scenario for MMS jobs for FLAG user : If current flag from generated content
   * should match with user flag code and praposed flag is equal to current user flag or praposed
   * flag equal to null {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSFlagUser() throws ClassDirectException, ParseException {
    mockUserRoleFlag();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockRefrerenceDataFlags());

    final AssetHDto asset = mockAsset();
    asset.setFlagStateDto(new FlagStateHDto());
    asset.getFlagStateDto().setFlagCode("ALA");

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(serviceReferenceService.getServiceCreditStatus(Mockito.anyLong()))
        .thenReturn(mockServiceCreditStatusData());
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.anyLong())).thenReturn(mockServiceCatalogueData());
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertFalse(response.isEmpty());
    Assert.assertFalse(response.get(0).getServices().isEmpty());
  }

  /**
   * Tests failure scenario for MMS jobs for FLAG user :proposed flag does not match with current
   * flag {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSFlagUserFalgcodeFail() throws ClassDirectException, ParseException {
    mockUserRoleFlag();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceProposedFlag());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockRefrerenceDataFlags());

    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertTrue(response.isEmpty());
  }

  /**
   * Tests success scenario for MMS jobs for FLAG user If current flag from generated content should
   * match with user flag code and praposed flag is equal to current user flag or praposed flag
   * equal to null {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSFlagUserSuccess() throws ClassDirectException, ParseException {
    mockUserRoleFlag();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());

    final List<JobWithFlagsHDto> mmsJobList = mockMMSJobWithFlagsList();
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mmsJobList));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    final FlagStateHDto flagStateALA = new FlagStateHDto();
    flagStateALA.setId(1L);
    flagStateALA.setFlagCode("ALA");

    Mockito.when(assetReferenceService.getFlagState(Mockito.eq(flagStateALA.getId()))).thenReturn(flagStateALA);

    final AssetHDto asset = mockAsset();
    // Current published asset FLAG is different than the user flag of ALA
    asset.setFlagStateDto(new FlagStateHDto());
    asset.getFlagStateDto().setFlagCode("ALA2");
    asset.setVersionId(2L);

    final BaseAssetDto verAsset = new BaseAssetDto();
    verAsset.setVersionId(1L);
    verAsset.setFlagState(new LinkResource());
    verAsset.getFlagState().setId(flagStateALA.getId());
    asset.setAllVersions(new ArrayList<>(1));
    asset.getAllVersions().add(verAsset);

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertEquals(mmsJobList.size(), response.size());
  }


  /**
   * Tests success scenario for MMS jobs for SHIPBUILER user :should not display job if it is MMS
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsMMSSHIPUserTest() throws ClassDirectException, ParseException {
    mockUserRoleShip();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    final AssetHDto asset = mockAsset();
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertTrue(response.isEmpty());
  }

  /**
   * Tests success scenario for MMS jobs for SHIPBUILER user :should display job if it is not MMS
   * job. {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getJobsSHIPUserTest() throws ClassDirectException, ParseException {
    mockUserRoleShip();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    final AssetHDto asset = mockAsset();
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(new ReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertFalse(response.isEmpty());
  }

  /**
   * Tests success scenario of filter migrated MMS jobs
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success..
   */
  @Test
  public final void shouldReturnEmptyForMigratedMMSJobs() throws ClassDirectException, ParseException {
    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMigratedJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    final List<JobCDDto> response = jobService.getJobsByAssetIdCall(1L, false);
    Assert.assertNotNull(response);
    Assert.assertTrue(response.isEmpty());
  }

  /**
   * Tests failure scenario for MMS jobs {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test
  public final void getJobsByAssetIdCallFail() throws ClassDirectException, ParseException {
    mockUserRoles();
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.failure(new IOException()));
    Assert.assertTrue(jobService.getJobsByAssetIdCall(1L, false).isEmpty());
  }

  /**
   * Tests success scenario for Admin role {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test
  public final void getJobsByAssetIdCallTestSuccess() throws ClassDirectException, ParseException {
    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Assert.assertFalse(jobService.getJobsByAssetIdCall(1L, false).isEmpty());
  }

  /**
   * Tests failure scenario for Admin role {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test(expected = ClassDirectException.class)
  public final void getJobsByAssetIdCallTestFailJob() throws ClassDirectException, ParseException {
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.failure(new IOException()));
    jobService.getJobsByAssetIdCall(1L, false);
  }

  /**
   * Tests failure scenario for Admin role {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test
  public final void getJobsByAssetIdCallTestFail() throws ClassDirectException, ParseException {
    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(null);
    Assert.assertFalse(jobService.getJobsByAssetIdCall(1L, false).isEmpty());
  }

  /**
   * Tests success scenario for internal user role
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)}. gets services from survey object from
   * generated content not from generated content scheduledService.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test
  public final void getJobTest() throws ParseException, ClassDirectException {
    mockUserRoles();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    String reportContent = reports.get(0).getContent();
    final JSONObject obj = new JSONObject(reportContent);
    final JSONArray surveyObj = obj.getJSONArray("surveys");

    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(serviceReferenceService.getServiceCreditStatus(Mockito.anyLong()))
        .thenReturn(mockServiceCreditStatusData());
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.anyLong())).thenReturn(mockServiceCatalogueData());
    final JobCDDto job = jobService.getJob(1L);
    Assert.assertNotNull(job);
    Assert.assertEquals("2016-07-05", job.getLastVisitDate());
    Assert.assertEquals(Long.valueOf(1), job.getLeadSurveyor().getId());
    Assert.assertEquals("BILL", job.getLeadSurveyor().getName());
    Assert.assertFalse(job.getServices().isEmpty());
    Assert.assertEquals("TestStatus", job.getServices().get(0).getStatus());
    Assert.assertEquals("screwshaft", job.getServices().get(0).getName());
    Assert.assertEquals(true, job.getHasESPReport());
    Assert.assertEquals(true, job.getHasTMReport());
    Assert.assertEquals("1234", job.getJobNumber());

    for (int i = 0, size = surveyObj.length(); i < size; i++) {
      final JSONObject objectInArray = surveyObj.getJSONObject(i);
      Assert.assertEquals(surveyObj.length(), job.getServices().size());
      if (i == 1) {
        Assert.assertTrue(objectInArray.isNull("scheduledService"));
      } else {
        Assert.assertFalse(objectInArray.isNull("scheduledService"));
      }
    }

    Assert.assertEquals(job.getServices().size(), 2);
  }

  /**
   * Tests success scenario for internal user role
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)}.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test
  public final void getJobTestWithNoEmployee() throws ParseException, ClassDirectException {
    mockUserRoles();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    List<JobWithFlagsHDto> jobWithFlagsHDtosMock = mockJobWithFlagsList();
    jobWithFlagsHDtosMock.iterator().next().setEmployees(null);
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(jobWithFlagsHDtosMock));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(serviceReferenceService.getServiceCreditStatus(Mockito.anyLong()))
        .thenReturn(mockServiceCreditStatusData());
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.anyLong())).thenReturn(mockServiceCatalogueData());
    final JobCDDto job = jobService.getJob(1L);
    Assert.assertNotNull(job);
    Assert.assertEquals("2016-07-05", job.getLastVisitDate());
    Assert.assertEquals(null, job.getLeadSurveyor());
    Assert.assertFalse(job.getServices().isEmpty());
    Assert.assertEquals("TestStatus", job.getServices().get(0).getStatus());
    Assert.assertEquals("screwshaft", job.getServices().get(0).getName());
    Assert.assertEquals(true, job.getHasESPReport());
    Assert.assertEquals(true, job.getHasTMReport());
  }

  /**
   * Tests success scenario for internal user role
   * {@link JobService#getJobsByAssetIdCall(Long, Boolean)} job with FSR report type.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test
  public final void getJobWithFSRReportTypeTest() throws ParseException, ClassDirectException {
    mockUserRoles();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    reports.add(mockFSRReportDto());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(serviceReferenceService.getServiceCreditStatus(Mockito.anyLong()))
        .thenReturn(mockServiceCreditStatusData());
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.anyLong())).thenReturn(mockServiceCatalogueData());
    final JobCDDto job = jobService.getJob(1L);
    Assert.assertNotNull(job);
    Assert.assertEquals("2016-07-05", job.getLastVisitDate());
    Assert.assertEquals(Long.valueOf(1), job.getLeadSurveyor().getId());
    Assert.assertEquals("BILL", job.getLeadSurveyor().getName());
    Assert.assertFalse(job.getServices().isEmpty());
    Assert.assertEquals("TestStatus", job.getServices().get(0).getStatus());
    Assert.assertEquals("screwshaft", job.getServices().get(0).getName());
    Assert.assertEquals(true, job.getHasESPReport());
    Assert.assertEquals(true, job.getHasTMReport());
  }

  /**
   * Tests success scenario to get filtered services for job.
   *
   * @throws ClassDirectException when Mast call fails otherwise should always success.
   * @throws ParseException if the setLastVisitDate is null or can not able to convert to required
   *         format otherwise should always success.
   */
  @Test
  public final void getJobWithFilteredServices() throws ParseException, ClassDirectException {
    mockUserRoles();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockDupliacteServiceReportDto());
    reports.add(mockFSRReportDto());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(serviceReferenceService.getServiceCreditStatus(Mockito.anyLong()))
        .thenReturn(mockServiceCreditStatusData());
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.anyLong())).thenReturn(mockServiceCatalogueData());
    final JobCDDto job = jobService.getJob(1L);
    Assert.assertNotNull(job);
    // filter out service with duplicate id
    Assert.assertEquals(job.getServices().size(), 2);
  }

  /**
   * Tests success scenario of filter migrated MMS jobs {@link JobService#getJob(Long)}.
   *
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   *
   **/
  @Test
  public final void getJobTestToFilterMMSMigratedJobs() throws ParseException, ClassDirectException {
    mockUserRoles();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMigratedJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(serviceReferenceService.getServiceCreditStatus(Mockito.anyLong()))
        .thenReturn(mockServiceCreditStatusData());
    Mockito.when(serviceReferenceService.getServiceCatalogue(Mockito.anyLong())).thenReturn(mockServiceCatalogueData());
    final JobCDDto job = jobService.getJob(1L);
    Assert.assertNull(job);
  }

  /**
   * Tests failure scenario get single job {@link JobService#getJob(Long)}.
   *
   * @throws ClassDirectException any of Mast call fail otherwise should always success.
   *
   */
  @Test
  public final void getJobTestFail() throws ClassDirectException {
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.failure(new IOException()));
    Assert.assertNull(jobService.getJob(1L));
  }

  /**
   * regex test.
   */
  @Test
  public final void getJobRegexTest() {
    final String pattern = ".+\\/job\\/\\d+\\/report\\/\\d+$";
    final String s = "/mast/api/v2/job/1/report/1";
    Assert.assertTrue(s.matches(pattern));
  }

  /**
   * Tests success scenario get completed jobs internal user role
   * {@link JobService#getCompletedJobs(String, Date, Date)}.
   *
   * @throws Exception any of Mast call fail otherwise should always success.
   */
  @Test
  public final void getCompletedJobsTest() throws Exception {
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    List<JobWithFlagsHDto> jobs = new ArrayList<>();
    Mockito.when(jobServiceDelegate.getJobsByAbstarctQuery(Mockito.any(AbstractQueryDto.class)))
        .thenReturn(Calls.response(mockClosedJobWithFlagsList()));
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUser());
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(mockSurveyReportData()));
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    jobs = jobService.getCompletedJobs("101", formatter.parse("2016/05/05"), formatter.parse("2016/08/08"));
    Assert.assertFalse(jobs.isEmpty());
    Assert.assertFalse(mockJobCopmpletedOnData().size() == jobs.size());
  }

  /**
   * Tests failure scenario get completed jobs internal user role
   * {@link JobService#getCompletedJobs(String, Date, Date)}.
   *
   * @throws Exception any of Mast call fail otherwise should always success.
   */
  @Test(expected = IllegalArgumentException.class)
  public final void getCompletedJobsNullDateTest() throws Exception {
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    jobService.getCompletedJobs("101", null, null);
  }

  /**
   * Tests success scenario get completed jobs CLIENT user role
   * {@link JobService#getCompletedJobs(String, Date, Date)}.
   *
   * @throws Exception any of Mast call fail otherwise should always success.
   */
  @Test
  public final void getCompletedJobsClient() throws Exception {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100");
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(jobServiceDelegate.getJobsByAbstarctQuery(Mockito.any(AbstractQueryDto.class)))
        .thenReturn(Calls.response(mockClosedJobWithFlagsList()));
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockClientUser());
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    List<JobWithFlagsHDto> jobs = new ArrayList<>();
    jobs = jobService.getCompletedJobs("101", formatter.parse("2016/05/05"), formatter.parse("2016/08/08"));
    Assert.assertFalse(jobs.isEmpty());
  }

  /**
   * Tests success scenario get completed jobs CLIENT user role
   * {@link JobService#getCompletedJobs(String, Date, Date)} with valid date range.
   *
   * @throws Exception any of Mast call fail otherwise should always success.
   */
  @Test
  public final void getCompletedJobsClientValidDateRange() throws Exception {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100");
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(jobServiceDelegate.getJobsByAbstarctQuery(Mockito.any(AbstractQueryDto.class)))
        .thenReturn(Calls.response(mockClosedJobWithFlagsList()));
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockClientUser());
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    List<JobWithFlagsHDto> jobs = new ArrayList<>();
    jobs = jobService.getCompletedJobs("101", formatter.parse("2016/05/05"), formatter.parse("2016/07/07"));
    Assert.assertFalse(jobs.isEmpty());
  }

  /**
   * Tests failure scenario get completed jobs CLIENT user role
   * {@link JobService#getCompletedJobs(String, Date, Date)} with in valid date range.
   *
   * @throws Exception any of Mast call fail otherwise should always success.
   */
  @Test
  public final void getCompletedJobsClientInValidDateRange() throws Exception {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode("100100");
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(jobServiceDelegate.getJobsByAbstarctQuery(Mockito.any(AbstractQueryDto.class)))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockClientUser());
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    List<JobWithFlagsHDto> jobs = new ArrayList<>();
    jobs = jobService.getCompletedJobs("101", formatter.parse("2017/04/06"), formatter.parse("2017/05/07"));

    jobs = jobService.filterJobsBasedOnUserRole("101", asset.getId(), jobs);
    Assert.assertTrue(jobs.isEmpty());
  }

  /**
   * Tests failure scenario get completed jobs when jobs with flags query did not return anythong
   * with search criteria {@link JobService#getCompletedJobs(String, Date, Date)}.
   *
   * @throws Exception any of Mast call fail otherwise should always success.
   */
  @Test
  public final void getCompletedJobsClientFail() throws Exception {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getJobsByAbstarctQuery(Mockito.any(AbstractQueryDto.class)))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    final AssetHDto asset = mockAsset();
    asset.setIhsAssetDto(new IhsAssetDetailsDto());
    asset.getIhsAssetDto().setIhsAsset(new IhsAssetDto());
    asset.getIhsAssetDto().getIhsAsset().setDocCode(null);
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(jobServiceDelegate.getSurveysByJobIdAndReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(mockServiceReportDto()));
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockClientUser());
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    List<JobWithFlagsHDto> jobs = new ArrayList<>();
    jobs = jobService.getCompletedJobs("101", formatter.parse("2016/05/05"), formatter.parse("2016/08/08"));

    jobs = jobService.filterJobsBasedOnUserRole("101", asset.getId(), jobs);

    Assert.assertTrue(jobs.isEmpty());
  }

  /**
   * Tests failure scenario get completed jobs
   * {@link JobService#getCompletedJobs(String, Date, Date)}.
   *
   * @throws Exception any of Mast call fail otherwise should always success.
   */
  @Test(expected = ClassDirectException.class)
  public final void getCompletedJobsFail() throws Exception {
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    Mockito.when(jobServiceDelegate.getJobsByAbstarctQuery(Mockito.any(AbstractQueryDto.class)))
        .thenReturn(Calls.failure(new IOException()));

    jobService.getCompletedJobs("101", formatter.parse("2016/05/05"), formatter.parse("2016/08/08"));
  }


  /**
   * Tests success scenario get getReports {@link JobService#getReports(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getReportsTest() throws IOException, ClassDirectException, ParseException {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    reports.add(mockFSRReportDto());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(attachmentService.getAttachmentsByJobIdReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    mockUserRoles();
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(userProfileService.getEOR(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUserEOR());
    Mockito.when(SecurityUtils.isEORUser(Mockito.any())).thenReturn(false);

    final List<com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportCDDto> reportDto = jobService.getReports(1L);
    Assert.assertNotNull(reportDto);
    Assert.assertFalse(reportDto.isEmpty());
    Assert.assertEquals("FSR", reportDto.get(0).getReportType());
    Assert.assertEquals("FAR", reportDto.get(1).getReportType());
    Assert.assertEquals("ESP", reportDto.get(2).getReportType());
    Assert.assertEquals("TM", reportDto.get(3).getReportType());
  }

  /**
   * Tests success scenario get getReports with mms attachment {@link JobService#getReports(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getReportsWithMMSAttachmentTest() throws IOException, ClassDirectException, ParseException {
    mockUserRoleFlag();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockServiceReportDto());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any()))
        .thenReturn(Calls.response(mockMMSJobWithFlagsList()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getMMSSupplementaryInfo()));
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockRefrerenceDataFlags());
    final AssetHDto asset = mockAsset();
    asset.setFlagStateDto(new FlagStateHDto());
    asset.getFlagStateDto().setFlagCode("ALA");
    asset.setVersionId(1L);
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(userProfileService.getEOR(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUserEOR());
    Mockito.when(SecurityUtils.isEORUser(Mockito.any())).thenReturn(false);

    final List<ReportCDDto> reportDto = jobService.getReports(1L);
    Assert.assertNotNull(reportDto);
    Assert.assertFalse(reportDto.isEmpty());
    Assert.assertEquals("FSR", reportDto.get(0).getReportType());
    Assert.assertEquals("ESP", reportDto.get(1).getReportType());
    Assert.assertEquals("TM", reportDto.get(2).getReportType());
    Assert.assertEquals("MMS", reportDto.get(3).getReportType());
    Assert.assertEquals(AttachmentCategoryEnum.MMS_REPORT.getId(),
        reportDto.get(3).getAttachments().get(0).getAttachmentCategoryH().getId());
    Assert.assertEquals(AttachmentCategoryEnum.MMS_JOB_NOTE.getId(),
        reportDto.get(3).getAttachments().get(1).getAttachmentCategoryH().getId());
  }

  /**
   * Tests success scenario get getReports with mms attachment for survey job
   * {@link JobService#getReports(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void testSurveyJobWithMMSReportAttachement() throws IOException, ClassDirectException, ParseException {
    mockUserRoleFlag();
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockFSRReportDto());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getMMSSupplementaryInfo()));
    final AssetHDto asset = mockAsset();
    asset.setFlagStateDto(new FlagStateHDto());
    asset.getFlagStateDto().setFlagCode("ALA");
    asset.setVersionId(1L);
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(userProfileService.getEOR(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUserEOR());
    Mockito.when(SecurityUtils.isEORUser(Mockito.any())).thenReturn(false);

    final List<ReportCDDto> reportDto = jobService.getReports(1L);
    Assert.assertNotNull(reportDto);
    Assert.assertFalse(reportDto.isEmpty());
    Assert.assertEquals("FSR", reportDto.get(0).getReportType());
    Assert.assertEquals("ESP", reportDto.get(1).getReportType());
    Assert.assertEquals("TM", reportDto.get(2).getReportType());
    Assert.assertEquals("MMS", reportDto.get(3).getReportType());
    Assert.assertEquals(AttachmentCategoryEnum.MMS_REPORT.getId(),
        reportDto.get(3).getAttachments().get(0).getAttachmentCategoryH().getId());
    Assert.assertEquals(AttachmentCategoryEnum.MMS_JOB_NOTE.getId(),
        reportDto.get(3).getAttachments().get(1).getAttachmentCategoryH().getId());
  }

  /**
   * Tests TM report is not returning if an EOR user do not has TM report access enabled.
   * getReports{@link JobService#getReports(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void testEorUserGetReportsWithoutTMAccess() throws IOException, ClassDirectException, ParseException {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(attachmentService.getAttachmentsByJobIdReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    mockUserRoles();
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());

    Mockito.when(SecurityUtils.isEORUser(Mockito.any())).thenReturn(true);
    Mockito.when(userProfileService.getEOR(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUserEORTMFalse());

    final List<com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportCDDto> reportDto = jobService.getReports(1L);
    Assert.assertNotNull(reportDto);
    Assert.assertFalse(reportDto.isEmpty());
    reportDto.forEach(report -> {
      Assert.assertNotEquals("TM", report.getReportType());
    });
  }

  /**
   * Tests TM report is returning if an EOR user has TM report access enabled. getReports
   * {@link JobService#getReports(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void testEorUserGetReportsWithTMAccess() throws IOException, ClassDirectException, ParseException {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());
    reports.add(mockFSRReportDto());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(attachmentService.getAttachmentsByJobIdReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    mockUserRoles();
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());

    Mockito.when(SecurityUtils.isEORUser(Mockito.any())).thenReturn(true);
    Mockito.when(userProfileService.getEOR(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUserEOR());

    final List<ReportCDDto> reportDto = jobService.getReports(1L);
    Assert.assertNotNull(reportDto);
    Assert.assertFalse(reportDto.isEmpty());
    Assert.assertEquals("FSR", reportDto.get(0).getReportType());
    Assert.assertEquals("FAR", reportDto.get(1).getReportType());
    Assert.assertEquals("ESP", reportDto.get(2).getReportType());
    Assert.assertEquals("TM", reportDto.get(3).getReportType());
  }

  /**
   * Tests failure scenario get getReports {@link JobService#getReports(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getReportsTestFail() throws IOException, ParseException {
    Mockito.when(assetReferenceService.getFlagStates()).thenReturn(mockRefrerenceDataFlags());
    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.failure(new IOException()));
    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());
    Assert.assertTrue(jobService.getReports(1L).isEmpty());
  }

  /**
   * Tests failure scenario get getAttachments {@link JobService#getAttachments(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getAttachmentFail() throws IOException, ClassDirectException, ParseException {
    final List<ReportHDto> reports = new ArrayList<>();
    reports.add(mockReportDto());

    Mockito.when(jobServiceDelegate.getJobsWithFlags(Mockito.any())).thenReturn(Calls.response(mockJobWithFlagsList()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    mockUserRoles();
    Mockito.when(employeeReferenceService.getEmployee(Mockito.anyLong())).thenReturn(mockLrEmployeeDto());

    Mockito.when(jobServiceDelegate.getReportsByJobId(Mockito.anyLong())).thenReturn(Calls.response(reports));
    Mockito.when(attachmentService.getAttachmentsByJobIdReportId(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.failure(new IOException()));
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    Mockito.when(userProfileService.getEOR(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUserEOR());

    List<ReportCDDto> response = jobService.getReports(1L);
    response.forEach(res -> {
      Assert.assertNull(res.getAttachments());
    });
  }

  /**
   * Tests success scenario get getAttachments {@link JobService#getAttachments(Long)}.
   *
   * @throws IOException any of Mast call fail otherwise should always success.
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   * @throws ParseException if the issueDate is null or invalid issueDate otherwise should always
   *         success.
   */
  @Test
  public final void getAttachmentsTest() throws ClassDirectException, ParseException {
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.response(getSupplementaryInfo()));

    final AttachmentsCDDto attachments = jobService.getAttachments(1L);
    Assert.assertNotNull(attachments);
    Assert.assertFalse(attachments.getAttachments().isEmpty());
    Assert.assertEquals("PDF", attachments.getAttachments().get(0).getAttachmentTypeH().getName());
    Assert.assertEquals("PDF", attachments.getAttachments().get(0).getAttachmentCategoryH().getName());
    Assert.assertNotNull(attachments.getAttachments().get(0).getToken());
  }

  /**
   * Tests failure scenario get getAttachments {@link JobService#getAttachments(Long)}.
   *
   * @throws ClassDirectException if there is error on the execution/verification otherwise should
   *         always success.
   */
  @Test(expected = ClassDirectException.class)
  public final void getAttachmentsTestFail() throws ClassDirectException {
    Mockito.when(attachmentService.getAttachmentsByJobId(Mockito.anyLong()))
        .thenReturn(Calls.failure(new IOException()));

    final AttachmentsCDDto attachments = jobService.getAttachments(1L);
    Assert.assertNotNull(attachments);
    Assert.assertTrue(attachments.getAttachments().isEmpty());
  }

  /**
   * Spring in-class configurations.
   *
   * @author yng
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create {@link JobService} bean.
     *
     * @return job service.
     */
    @Override
    @Bean
    public JobService jobService() {
      return new JobServiceImpl();
    }
  }
}
