package com.baesystems.ai.lr.cd.service.asset.impl.test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.mockito.BDDMockito;
import org.powermock.api.mockito.PowerMockito;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobWithFlagsHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AttachmentCategoryEnum;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.enums.AttachmentCategory;

/**
 * Provides mock test data for job service .
 *
 * @author sbollu
 *
 */
public class JobServiceMockTestData {

  /**
   * Returns mock object {@link JobHDto}.
   *
   * @return List<JobHDto> as mock object.
   * @throws Exception if there is error on the execution/verification otherwise should always
   *         success.
   */
  protected List<JobWithFlagsHDto> mockJobCopmpletedOnData() throws Exception {
    final List<JobWithFlagsHDto> jobs = new ArrayList<>();
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    final JobWithFlagsHDto job1 = new JobWithFlagsHDto();
    job1.setAsset(new LinkVersionedResource(1L, 1L));
    job1.setId(1L);
    job1.setCompletedOn(formatter.parse("2016/05/05"));
    job1.setMigrated(Boolean.TRUE);
    job1.setJobCategory(new LinkResource(2L));
    jobs.add(job1);

    final JobWithFlagsHDto job2 = new JobWithFlagsHDto();
    job2.setId(Long.valueOf("2"));
    job2.setAsset(new LinkVersionedResource(1L, 1L));
    job2.setCompletedOn(formatter.parse("2016/07/05"));
    job2.setMigrated(Boolean.FALSE);
    job2.setJobCategory(new LinkResource(2L));
    jobs.add(job2);

    final JobWithFlagsHDto job3 = new JobWithFlagsHDto();
    job3.setId(Long.valueOf("3"));
    job3.setAsset(new LinkVersionedResource(1L, 1L));
    job3.setCompletedOn(formatter.parse("2016/07/07"));
    job3.setMigrated(Boolean.FALSE);
    job3.setJobCategory(new LinkResource(2L));
    jobs.add(job3);

    final JobWithFlagsHDto job4 = new JobWithFlagsHDto();
    job4.setId(Long.valueOf("4"));
    job4.setAsset(new LinkVersionedResource(1L, 1L));
    job4.setCompletedOn(formatter.parse("2016/08/08"));
    job4.setMigrated(Boolean.FALSE);
    job4.setJobCategory(new LinkResource(2L));
    jobs.add(job4);

    final JobWithFlagsHDto job5 = new JobWithFlagsHDto();
    job5.setId(Long.valueOf("5"));
    job5.setAsset(new LinkVersionedResource(1L, 1L));
    job5.setCompletedOn(formatter.parse("2016/06/06"));
    job5.setMigrated(Boolean.TRUE);
    job5.setJobCategory(new LinkResource(2L));
    jobs.add(job5);

    final JobWithFlagsHDto job6 = new JobWithFlagsHDto();
    job6.setId(Long.valueOf("6"));
    job6.setAsset(new LinkVersionedResource(1L, 1L));
    job6.setCompletedOn(formatter.parse("2017/06/06"));
    job6.setMigrated(Boolean.FALSE);
    job6.setJobCategory(new LinkResource(2L));
    jobs.add(job6);

    return jobs;
  }

  /**
   * mock user roles as Admin.
   */
  protected void mockUserRoles() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("LR_ADMIN");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * mock user role CLIENT.
   */
  protected void mockUserRoleClient() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("CLIENT");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * mock user role SHIP.
   */
  protected void mockUserRoleShip() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("SHIPBUILDER");
    roles.add("EOR");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * mock user role FLAG.
   */
  protected void mockUserRoleFlag() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("FLAG");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * Returns mock asset {@link AssetHDto}.
   *
   * @return assetHdto.
   */
  public AssetHDto mockAsset() {
    // data1
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("v2 LADY K II");
    asset.setIsLead(false);
    asset.setLeadImo(String.valueOf(1L));
    asset.setVersionId(1L);
    asset.setImoNumber("100");
    return asset;
  }

  /**
   * Returns mock UserEOR {@link UserEOR}.
   *
   * @return userEor.
   */
  protected UserEOR mockUserEOR() {
    UserEOR userEor = new UserEOR();
    userEor.setAccessTmReport(true);
    return userEor;
  }

  /**
   * Returns mock UserEOR {@link UserEOR}.
   *
   * @return userEor.
   */
  protected UserEOR mockUserEORTMFalse() {
    UserEOR userEor = new UserEOR();
    userEor.setAccessTmReport(false);
    return userEor;
  }


  /**
   * Returns mock user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("LR_ADMIN");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }

  /**
   * Returns mock client user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockClientUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    user.setClientCode("100100");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("CLIENT");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }


  /**
   * Returns mock object {@link LrEmployeeDto}.
   *
   * @return LrEmployeeDto.
   */
  protected LrEmployeeDto mockLrEmployeeDto() {
    final LrEmployeeDto dto = new LrEmployeeDto();
    dto.setId(1L);
    dto.setName("BILL");
    return dto;
  }

  /**
   * Returns mock object {@link ReportHDto}.
   *
   * @return ReportHDto.
   */
  protected ReportHDto mockReportDto() {
    final ReportHDto dto = new ReportHDto();
    dto.setId(1L);
    final LinkResource reportType = new LinkResource();
    reportType.setId(2L);
    dto.setReportType(reportType);
    final ReportTypeHDto reportTypeDto = new ReportTypeHDto();
    reportTypeDto.setName("FAR");
    dto.setReportTypeDto(reportTypeDto);
    return dto;
  }

  /**
   * Returns mock object {@link ReportHDto} with report type FSR.
   *
   * @return ReportHDto.
   */
  protected ReportHDto mockFSRReportDto() {
    final ReportHDto dto = new ReportHDto();
    dto.setId(1L);
    final LinkResource reportType = new LinkResource();
    reportType.setId(1L);
    dto.setReportType(reportType);
    final ReportTypeHDto reportTypeDto = new ReportTypeHDto();
    reportTypeDto.setName("FSR");
    dto.setReportTypeDto(reportTypeDto);
    dto.setReportVersion(null);
    return dto;
  }

  /**
   * Returns mock object {@link ReportHDto}.
   *
   * @return ReportHDto.
   */
  protected ReportHDto mockServiceReportDto() {
    final ReportHDto dto = mockFSRReportDto();
    dto.setId(1L);
    final String json = "{\"wipActionableItems\":[],\"wipAssetNotes\":[],\"wipCocs\":[],"
        + "\"proposedFlag\":null,\"currentFlag\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"surveys\":[{\"id\": 1,\"deleted\": false,\"updatedBy\": null,"
        + "\"updatedDate\": null,\"actionTakenDate\": null,\"stalenessHash\": "
        + "\"a5363954e493cf6a838f5997d88735fe3b37bd7d\",\"name\": screwshaft,"
        + "\"jobScopeConfirmed\": true,\"autoGenerated\": null,\"actionRequired\": null,"
        + "\"narrative\": null,\"job\": {\"id\": 1,\"_id\": \"1\"},\"dateOfCrediting\": null,"
        + "\"surveyStatus\": {\"id\": 1,\"_id\": \"1\"},\"approved\": false,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"scheduledService\": {\"id\": 1,\"stalenessHash\": null,"
        + "\"parentService\": null,\"asset\":{\"id\": 1,\"_id\": \"1\"},"
        + "\"assetItem\": null,\"cyclePeriodicity\": 0,"
        + "\"postponementDate\": null,\"assignedDate\": \"2015-11-05\","
        + "\"assignedDateManual\": null,\"dueDate\": \"2017-11-05\","
        + "\"dueStatus\": {\"id\": 1,\"_id\": \"1\"},\"dueDateManual\": null,\"lowerRangeDate\": \"2017-11-02\","
        + "\"lowerRangeDateManual\": null,\"upperRangeDate\": \"2017-11-08\",\"upperRangeDateManual\": null,"
        + "\"serviceStatus\": {\"id\": 1,\"_id\": \"1\"},\"serviceCreditStatus\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"completionDate\": null,\"provisionalDates\": false,\"creditingDate\": null,"
        + "\"serviceCycleNumber\": null,\"lastCreditedJob\": null," + "\"lastPartheldJob\": null,\"provisional\": null,"
        + "\"active\": true,\"_id\": \"1\"},\"creditedBy\": null,\"scheduleDatesUpdated\": false,\"_id\": \"1\"},"
        + "{\"id\": 2,\"deleted\": false,\"updatedBy\": null,"
        + "\"updatedDate\": null,\"actionTakenDate\": null,\"stalenessHash\": "
        + "\"a5363954e493cf6a838f5997d88735fe3b37bd7d\",\"name\": screwshaft2,"
        + "\"jobScopeConfirmed\": true,\"autoGenerated\": null,\"actionRequired\": null,"
        + "\"narrative\": null,\"job\": {\"id\": 1,\"_id\": \"1\"},\"dateOfCrediting\": null,"
        + "\"surveyStatus\": {\"id\": 1,\"_id\": \"1\"},\"approved\": false,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"scheduledService\": null,\"creditedBy\": null,\"scheduleDatesUpdated\": false,\"_id\": \"2\"}],"
        + "\"tasks\":[]}";
    dto.setContent(json);
    return dto;
  }

  /**
   * Returns mock object {@link ReportHDto}.
   *
   * @return ReportHDto.
   */
  protected ReportHDto mockServiceProposedFlag() {
    final ReportHDto dto = mockFSRReportDto();
    dto.setId(1L);
    final String json = "{\"wipActionableItems\":[],\"wipAssetNotes\":[],\"wipCocs\":[],"
        + "\"proposedFlag\": {\"id\": 2,\"_id\": \"2\"}," + "\"currentFlag\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"surveys\":[{\"id\": 1,\"deleted\": false,\"updatedBy\": null,"
        + "\"updatedDate\": null,\"actionTakenDate\": null,\"stalenessHash\": "
        + "\"a5363954e493cf6a838f5997d88735fe3b37bd7d\",\"name\": screwshaft,"
        + "\"jobScopeConfirmed\": true,\"autoGenerated\": null,\"actionRequired\": null,"
        + "\"narrative\": null,\"job\": {\"id\": 1,\"_id\": \"1\"},\"dateOfCrediting\": null,"
        + "\"surveyStatus\": {\"id\": 1,\"_id\": \"1\"},\"approved\": false,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"scheduledService\": {\"id\": 1,\"stalenessHash\": null,"
        + "\"parentService\": null,\"asset\":{\"id\": 1,\"_id\": \"1\"},"
        + "\"assetItem\": null,\"cyclePeriodicity\": 0,"
        + "\"postponementDate\": null,\"assignedDate\": \"2015-11-05\","
        + "\"assignedDateManual\": null,\"dueDate\": \"2017-11-05\","
        + "\"dueStatus\": {\"id\": 1,\"_id\": \"1\"},\"dueDateManual\": null,\"lowerRangeDate\": \"2017-11-02\","
        + "\"lowerRangeDateManual\": null,\"upperRangeDate\": \"2017-11-08\",\"upperRangeDateManual\": null,"
        + "\"serviceStatus\": {\"id\": 1,\"_id\": \"1\"},\"serviceCreditStatus\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"completionDate\": null,\"provisionalDates\": false,\"creditingDate\": null,"
        + "\"serviceCycleNumber\": null,\"lastCreditedJob\": null," + "\"lastPartheldJob\": null,\"provisional\": null,"
        + "\"active\": true,\"_id\": \"1\"},\"creditedBy\": null,\"scheduleDatesUpdated\": false,\"_id\": \"1\"},"
        + "{\"id\": 2,\"deleted\": false,\"updatedBy\": null,"
        + "\"updatedDate\": null,\"actionTakenDate\": null,\"stalenessHash\": "
        + "\"a5363954e493cf6a838f5997d88735fe3b37bd7d\",\"name\": screwshaft2,"
        + "\"jobScopeConfirmed\": true,\"autoGenerated\": null,\"actionRequired\": null,"
        + "\"narrative\": null,\"job\": {\"id\": 1,\"_id\": \"1\"},\"dateOfCrediting\": null,"
        + "\"surveyStatus\": {\"id\": 1,\"_id\": \"1\"},\"approved\": false,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"scheduledService\": null,\"creditedBy\": null,\"scheduleDatesUpdated\": false,\"_id\": \"2\"}],"
        + "\"tasks\":[]}";
    dto.setContent(json);
    return dto;
  }

  /**
   * Returns mock object {@link ReportHDto}.
   *
   * @return ReportHDto.
   */
  protected ReportHDto mockDupliacteServiceReportDto() {
    final ReportHDto dto = mockFSRReportDto();
    dto.setId(1L);
    final String json = "{\"wipActionableItems\":[],\"wipAssetNotes\":[],\"wipCocs\":[],"
        + "\"proposedFlag\": {\"id\": 2,\"_id\": \"2\"}," + "\"currentFlag\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"surveys\":[{\"id\": 1,\"deleted\": false,\"updatedBy\": null,"
        + "\"updatedDate\": null,\"actionTakenDate\": null,\"stalenessHash\": "
        + "\"a5363954e493cf6a838f5997d88735fe3b37bd7d\",\"name\": screwshaft,"
        + "\"jobScopeConfirmed\": true,\"autoGenerated\": null,\"actionRequired\": null,"
        + "\"narrative\": null,\"job\": {\"id\": 1,\"_id\": \"1\"},\"dateOfCrediting\": null,"
        + "\"surveyStatus\": {\"id\": 1,\"_id\": \"1\"},\"approved\": false,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"scheduledService\": {\"id\": 1,\"stalenessHash\": null,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"parentService\": null,\"asset\":{\"id\": 1,\"_id\": \"1\"},"
        + "\"assetItem\": null,\"cyclePeriodicity\": 0,"
        + "\"postponementDate\": null,\"assignedDate\": \"2015-11-05\","
        + "\"assignedDateManual\": null,\"dueDate\": \"2017-11-05\","
        + "\"dueStatus\": {\"id\": 1,\"_id\": \"1\"},\"dueDateManual\": null,\"lowerRangeDate\": \"2017-11-02\","
        + "\"lowerRangeDateManual\": null,\"upperRangeDate\": \"2017-11-08\",\"upperRangeDateManual\": null,"
        + "\"serviceStatus\": {\"id\": 1,\"_id\": \"1\"},\"serviceCreditStatus\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"completionDate\": null,\"provisionalDates\": false,\"creditingDate\": null,"
        + "\"serviceCycleNumber\": null,\"lastCreditedJob\": null," + "\"lastPartheldJob\": null,\"provisional\": null,"
        + "\"active\": true,\"_id\": \"1\"},\"creditedBy\": null,\"scheduleDatesUpdated\": false,\"_id\": \"1\"},"
        + "{\"id\": 2,\"deleted\": false,\"updatedBy\": null,"
        + "\"updatedDate\": null,\"actionTakenDate\": null,\"stalenessHash\": "
        + "\"a5363954e493cf6a838f5997d88735fe3b37bd7d\",\"name\": screwshaft2,"
        + "\"jobScopeConfirmed\": true,\"autoGenerated\": null,\"actionRequired\": null,"
        + "\"narrative\": null,\"job\": {\"id\": 1,\"_id\": \"1\"},\"dateOfCrediting\": null,"
        + "\"surveyStatus\": {\"id\": 1,\"_id\": \"1\"},\"approved\": false,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"scheduledService\": null,\"creditedBy\": null,\"scheduleDatesUpdated\": false,\"_id\": \"2\"},"
        + "{\"id\": 2,\"deleted\": false,\"updatedBy\": null,"
        + "\"updatedDate\": null,\"actionTakenDate\": null,\"stalenessHash\": "
        + "\"a5363954e493cf6a838f5997d88735fe3b37bd7d\",\"name\": screwshaft3,"
        + "\"jobScopeConfirmed\": true,\"autoGenerated\": null,\"actionRequired\": null,"
        + "\"narrative\": null,\"job\": {\"id\": 1,\"_id\": \"1\"},\"dateOfCrediting\": null,"
        + "\"surveyStatus\": {\"id\": 1,\"_id\": \"1\"},\"approved\": false,"
        + "\"serviceCatalogue\": {\"id\": 1,\"_id\": \"1\"},"
        + "\"scheduledService\": null,\"creditedBy\": null,\"scheduleDatesUpdated\": false,\"_id\": \"2\"}],"
        + "\"tasks\":[]}";

    dto.setContent(json);
    final ReportTypeHDto reportTypeDto = new ReportTypeHDto();
    reportTypeDto.setName("FSR");
    dto.setReportTypeDto(reportTypeDto);
    return dto;
  }

  /**
   * Returns mock object{@link JobWithFlagsHDto}.
   *
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @return job with flags page resource dto.
   */
  protected List<JobWithFlagsHDto> mockJobWithFlagsList() throws ParseException {
    final List<JobWithFlagsHDto> content = new ArrayList<>();
    content.add(mockJobWithFlagsDto(1L));
    return content;
  }

  /**
   * Returns mock object{@link JobWithFlagsHDto}.
   *
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @return job with flags page resource dto.
   */
  protected List<JobWithFlagsHDto> mockMMSJobWithFlagsList() throws ParseException {
    final List<JobWithFlagsHDto> content = new ArrayList<>();
    content.add(mockMMSJobWithFlagsDto(1L));
    return content;
  }

  /**
   * Returns mock object migrated job{@link JobWithFlagsHDto}.
   *
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @return job with flags page resource dto.
   */
  protected List<JobWithFlagsHDto> mockMigratedJobWithFlagsList() throws ParseException {
    final List<JobWithFlagsHDto> content = new ArrayList<>();
    content.add(mockMigratedJobWithFlagsDto());
    return content;
  }

  /**
   * Returns mock object with migrated job {@link JobWithFlagsHDto}.
   *
   * @return Mock JobWithFlagsDto.
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @throws IOException if there is error on the execution/verification otherwise should always
   *         success.
   */
  private JobWithFlagsHDto mockMigratedJobWithFlagsDto() throws ParseException {
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    final JobWithFlagsHDto dto = new JobWithFlagsHDto();
    dto.setId(1L);
    dto.setAsset(new LinkVersionedResource(1L, 1L));
    dto.setLastVisitDate(formatter.parse("2016/07/05"));
    dto.setFirstVisitDate(formatter.parse("2015/07/05"));
    dto.setWorkOrderNumber(1L);
    dto.setFlagTM(false);
    dto.setFlagESP(false);
    final List<EmployeeLinkDto> emp = new ArrayList<EmployeeLinkDto>();
    final EmployeeLinkDto leadSurveyor = new EmployeeLinkDto();
    leadSurveyor.setId(1L);
    final LinkResource employeeRole = new LinkResource();
    employeeRole.setId(11L);
    leadSurveyor.setEmployeeRole(employeeRole);
    final LinkResource lrEmployee = new LinkResource();
    lrEmployee.setId(1L);
    leadSurveyor.setLrEmployee(lrEmployee);
    emp.add(leadSurveyor);
    dto.setEmployees(emp);
    dto.setLocationDto(new LocationHDto());
    dto.setMigrated(Boolean.TRUE);
    dto.setJobCategory(new LinkResource(2L));
    return dto;
  }

  /**
   * Returns mock object closed job{@link JobWithFlagsHDto}.
   *
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @return job with flags page resource dto.
   */
  protected List<JobWithFlagsHDto> mockClosedJobWithFlagsList() throws ParseException {
    final List<JobWithFlagsHDto> content = new ArrayList<>();
    content.add(mockClosedJobWithFlagsDto());
    return content;
  }

  /**
   * Returns mock object with closed job {@link JobWithFlagsHDto}.
   *
   * @return Mock JobWithFlagsDto.
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @throws IOException if there is error on the execution/verification otherwise should always
   *         success.
   */
  private JobWithFlagsHDto mockClosedJobWithFlagsDto() throws ParseException {
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    final JobWithFlagsHDto dto = new JobWithFlagsHDto();
    dto.setId(1L);
    dto.setAsset(new LinkVersionedResource(1L, 1L));
    dto.setLastVisitDate(formatter.parse("2016/07/05"));
    dto.setFirstVisitDate(formatter.parse("2015/07/05"));
    dto.setWorkOrderNumber(1L);
    dto.setFlagTM(false);
    dto.setFlagESP(false);
    dto.setCompletedOn(formatter.parse("2016/05/05"));
    final List<EmployeeLinkDto> emp = new ArrayList<EmployeeLinkDto>();
    final EmployeeLinkDto leadSurveyor = new EmployeeLinkDto();
    leadSurveyor.setId(1L);
    final LinkResource employeeRole = new LinkResource();
    employeeRole.setId(11L);
    leadSurveyor.setEmployeeRole(employeeRole);
    final LinkResource lrEmployee = new LinkResource();
    lrEmployee.setId(1L);
    leadSurveyor.setLrEmployee(lrEmployee);
    emp.add(leadSurveyor);
    dto.setEmployees(emp);
    dto.setLocationDto(new LocationHDto());
    dto.setMigrated(Boolean.TRUE);
    dto.setJobCategory(new LinkResource(2L));
    return dto;
  }

  /**
   * Returns mock object {@link jobHdo}.
   *
   * @return Mock JobHDto.
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @throws IOException if there is error on the execution/verification otherwise should always
   *         success.
   */
  protected JobHDto mockJobHDto() throws IOException, ParseException {
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    final JobHDto dto = new JobHDto();
    dto.setId(1L);
    dto.setAsset(new LinkVersionedResource(1L, 1L));
    dto.setLastVisitDate(formatter.parse("2016/07/05"));
    dto.setFirstVisitDate(formatter.parse("2015/07/05"));
    dto.setWorkOrderNumber(1L);
    final List<EmployeeLinkDto> emp = new ArrayList<EmployeeLinkDto>();
    final EmployeeLinkDto leadSurveyor = new EmployeeLinkDto();
    leadSurveyor.setId(1L);
    final LinkResource employeeRole = new LinkResource();
    employeeRole.setId(11L);
    leadSurveyor.setEmployeeRole(employeeRole);
    final LinkResource lrEmployee = new LinkResource();
    lrEmployee.setId(1L);
    leadSurveyor.setLrEmployee(lrEmployee);
    emp.add(leadSurveyor);
    dto.setEmployees(emp);
    dto.setLocationDto(new LocationHDto());
    return dto;
  }


  /**
   * Returns list of SupplementaryInformationHDto.
   *
   * @return list of {@link SupplementaryInformationHDto}.
   * @throws ParseException if the createionDate is null or invalid issueDate otherwise should
   *         always success.
   */
  protected List<SupplementaryInformationHDto> getSupplementaryInfo() throws ParseException {
    final List<SupplementaryInformationHDto> dtos = new ArrayList<>();

    final SupplementaryInformationHDto supplementaryDto = new SupplementaryInformationHDto();
    supplementaryDto.setAttachmentType(new LinkResource(1L));
    supplementaryDto.setAttachmentCategory(new LinkResource(1L));

    // Set attachment creation date.
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    supplementaryDto.setCreationDate(formatter.parse("2016/01/01"));

    // mock attachment type
    final AttachmentTypeHDto attachmentType = new AttachmentTypeHDto();
    attachmentType.setId(1L);
    attachmentType.setName("PDF");
    attachmentType.setDescription("PDF");
    supplementaryDto.setAttachmentTypeH(attachmentType);

    // mock attachmnet category
    final AttachmentCategoryHDto category = new AttachmentCategoryHDto();
    category.setId(1L);
    category.setName("PDF");
    supplementaryDto.setAttachmentCategoryH(category);
    supplementaryDto.setAttachmentCategory(new LinkResource(1L));

    supplementaryDto.setId(1L);
    dtos.add(supplementaryDto);

    //
    // Supp. DTO 2 (TM Report)
    //
    final SupplementaryInformationHDto supplementaryDto2 = new SupplementaryInformationHDto();
    supplementaryDto2.setId(2L);
    supplementaryDto2.setCreationDate(formatter.parse("2016/01/02"));
    supplementaryDto2
        .setAttachmentCategory(new LinkResource(AttachmentCategory.THICKNESS_MEASUREMENT_REPORT_ARGONAUT.getValue()));
    dtos.add(supplementaryDto2);
    return dtos;
  }

  /**
   * Returns list of MMS SupplementaryInformationHDto.
   *
   * @return list of {@link SupplementaryInformationHDto}.
   * @throws ParseException if the createionDate is null or invalid issueDate otherwise should
   *         always success.
   */
  protected List<SupplementaryInformationHDto> getMMSSupplementaryInfo() throws ParseException {
    final List<SupplementaryInformationHDto> dtos = new ArrayList<>();

    // mock mms report attachment category.
    final AttachmentCategoryHDto mmsReportCategory = new AttachmentCategoryHDto();
    mmsReportCategory.setId(AttachmentCategoryEnum.MMS_REPORT.getId());
    mmsReportCategory.setName("MMS Report");

    // mock mms job note attachment category.
    final AttachmentCategoryHDto mmsJobNotecategory = new AttachmentCategoryHDto();
    mmsJobNotecategory.setId(AttachmentCategoryEnum.MMS_JOB_NOTE.getId());
    mmsJobNotecategory.setName("MMS Job Note");

    // The MMS report supplementary dto.
    final SupplementaryInformationHDto supplementaryDto = new SupplementaryInformationHDto();
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    supplementaryDto.setCreationDate(formatter.parse("2016/01/01"));

    // mock attachment type.
    final AttachmentTypeHDto attachmentType = new AttachmentTypeHDto();
    attachmentType.setId(1L);
    attachmentType.setName("PDF");
    attachmentType.setDescription("PDF");
    supplementaryDto.setAttachmentTypeH(attachmentType);

    supplementaryDto.setAttachmentCategoryH(mmsReportCategory);
    supplementaryDto.setAttachmentCategory(new LinkResource(AttachmentCategoryEnum.MMS_REPORT.getId()));

    supplementaryDto.setId(1L);
    dtos.add(supplementaryDto);

    // The MMS Job note supplementary dto.
    final SupplementaryInformationHDto supplementaryDto2 = new SupplementaryInformationHDto();
    supplementaryDto2.setId(2L);
    supplementaryDto2.setCreationDate(formatter.parse("2016/01/02"));
    supplementaryDto2.setAttachmentCategoryH(mmsJobNotecategory);
    supplementaryDto2.setAttachmentCategory(new LinkResource(AttachmentCategoryEnum.MMS_JOB_NOTE.getId()));
    dtos.add(supplementaryDto2);
    return dtos;
  }


  /**
   * Returns jobs dto {@link JobWithFlagsHDto}.
   *
   * @param id the job id.
   * @return Mock JobWithFlagsDto.
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @throws IOException if there is error on the execution/verification otherwise should always
   *         success.
   */
  protected JobWithFlagsHDto mockMMSJobWithFlagsDto(final Long id) throws ParseException {
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    final JobWithFlagsHDto dto = new JobWithFlagsHDto();
    dto.setId(id);
    dto.setAsset(new LinkVersionedResource(1L, 1L));
    dto.setLastVisitDate(formatter.parse("2016/07/05"));
    dto.setFirstVisitDate(formatter.parse("2015/07/05"));
    dto.setWorkOrderNumber(1L);
    dto.setFlagTM(true);
    dto.setFlagESP(true);
    final List<EmployeeLinkDto> emp = new ArrayList<EmployeeLinkDto>();
    final EmployeeLinkDto leadSurveyor = new EmployeeLinkDto();
    leadSurveyor.setId(1L);
    final LinkResource employeeRole = new LinkResource();
    employeeRole.setId(11L);
    leadSurveyor.setEmployeeRole(employeeRole);
    final LinkResource lrEmployee = new LinkResource();
    lrEmployee.setId(1L);
    leadSurveyor.setLrEmployee(lrEmployee);
    emp.add(leadSurveyor);
    dto.setEmployees(emp);
    dto.setLocationDto(new LocationHDto());
    dto.setMigrated(Boolean.FALSE);
    dto.setJobNumber("1234");
    dto.setJobCategory(new LinkResource(2L));
    return dto;
  }

  /**
   * Returns jobs dto {@link JobWithFlagsHDto}.
   *
   * @param id the job id.
   * @return Mock JobWithFlagsDto.
   * @throws ParseException if the LastVisitDate or FirstVisitDate is null or invalid issueDate
   *         otherwise should always success.
   * @throws IOException if there is error on the execution/verification otherwise should always
   *         success.
   */
  protected JobWithFlagsHDto mockJobWithFlagsDto(final Long id) throws ParseException {
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    final JobWithFlagsHDto dto = new JobWithFlagsHDto();
    dto.setId(id);
    dto.setAsset(new LinkVersionedResource(1L, 1L));
    dto.setLastVisitDate(formatter.parse("2016/07/05"));
    dto.setFirstVisitDate(formatter.parse("2015/07/05"));
    dto.setCompletedOn(formatter.parse("2016/05/05"));
    dto.setWorkOrderNumber(1L);
    dto.setFlagTM(true);
    dto.setFlagESP(true);
    final List<EmployeeLinkDto> emp = new ArrayList<EmployeeLinkDto>();
    final EmployeeLinkDto leadSurveyor = new EmployeeLinkDto();
    leadSurveyor.setId(1L);
    final LinkResource employeeRole = new LinkResource();
    employeeRole.setId(11L);
    leadSurveyor.setEmployeeRole(employeeRole);
    final LinkResource lrEmployee = new LinkResource();
    lrEmployee.setId(1L);
    leadSurveyor.setLrEmployee(lrEmployee);
    emp.add(leadSurveyor);
    dto.setEmployees(emp);
    dto.setLocationDto(new LocationHDto());
    dto.setMigrated(Boolean.FALSE);
    dto.setJobNumber("1234");
    dto.setJobCategory(new LinkResource(1L));
    return dto;
  }

  /**
   * Returns service credit status mock data.
   *
   * @return the service credit status mock data.
   */
  public ServiceCreditStatusHDto mockServiceCreditStatusData() {
    final ServiceCreditStatusHDto serviceCreditStatus = new ServiceCreditStatusHDto();
    serviceCreditStatus.setId(1L);
    serviceCreditStatus.setName("TestStatus");
    serviceCreditStatus.setCode("N");
    return serviceCreditStatus;
  }

  /**
   * Returns service catalogue mock data.
   *
   * @return the service catalogue mock data.
   */
  public ServiceCatalogueHDto mockServiceCatalogueData() {
    final ServiceCatalogueHDto dto = new ServiceCatalogueHDto();
    dto.setId(1L);
    dto.setCode("12324");
    dto.setName("Catalogue");
    ProductCatalogueDto productCatalogue = new ProductCatalogueDto();
    productCatalogue.setId(1L);
    productCatalogue.setName("MMS");
    productCatalogue.setProductType(new LinkResource());
    productCatalogue.getProductType().setId(2L);
    dto.setProductCatalogue(productCatalogue);
    return dto;
  }

  /**
   * Mocks reference data flags.
   *
   * @return flags.
   */
  public List<FlagStateHDto> mockRefrerenceDataFlags() {
    List<FlagStateHDto> flags = new ArrayList<>();
    FlagStateHDto flag = new FlagStateHDto();
    flag.setFlagCode("ALA");
    flag.setId(1L);
    flags.add(flag);
    FlagStateHDto flag2 = new FlagStateHDto();
    flag2.setFlagCode("IND");
    flag2.setId(2L);
    flags.add(flag2);
    return flags;
  }

}
