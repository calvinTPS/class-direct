package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetLifeCycleStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ChecklistExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.GroupDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.HierarchyDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.SubgroupDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.ChecklistExportService;
import com.baesystems.ai.lr.cd.service.export.impl.ChecklistExportServiceImpl;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;


/**
 * Provides unit test for {@link ChecklistExportService}.
 *
 * @author yng
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = ChecklistExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class ChecklistExportServiceTest {

  /**
   * The {@link TaskService} from {@link ChecklistExportServiceTest.Config} class.
   */
  @Autowired
  private TaskService taskService;

  /**
   * The {@link AssetService} from {@link ChecklistExportServiceTest.Config} class.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link AmazonStorageService} from {@link ChecklistExportServiceTest.Config} class.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link ServiceReferenceService} from {@link ChecklistExportServiceTest.Config} class.
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * The {@link ChecklistExportService} from {@link ChecklistExportServiceTest.Config} class.
   */
  @Autowired
  private ChecklistExportService checklistExportService;

  /**
   * Provides mock reset before any test class is executed.
   */
  @Before
  public void resetMock() {
    Mockito.reset(taskService, assetService, serviceReferenceService);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Test positive case scenario for checklist pdf generation.
   *
   * @throws ClassDirectException if API call fail.
   * @throws IOException if API call fail.
   */
  @Test
  public final void testGenerateChecklistPDF() throws ClassDirectException, IOException {

    Mockito.when(taskService.getCheckListHierarchyByService(Matchers.anyLong())).thenReturn(mockHierarchyDto());
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAsset());
    Mockito.when(serviceReferenceService.getServicesForAsset(Mockito.anyLong())).thenReturn(mockServices());
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ChecklistExportDto query = new ChecklistExportDto();
    query.setAssetCode("LRV1");
    query.setServiceId(1L);

    final StringResponse response = checklistExportService.downloadPdf(query);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }


  /**
   * Returns mock checklist hierarchy DTO.
   *
   * @return mock checklist hierarchy DTO which holds group ,sub group and item list.
   */
  private HierarchyDto mockHierarchyDto() {

    HierarchyDto hierarchyDto = new HierarchyDto();
    // group1 and subgroup1
    final WorkItemLightHDto firstCK = new WorkItemLightHDto();
    firstCK.setId(1L);
    firstCK.setReferenceCode("A1");
    firstCK.setTaskNumber("000001-XXXX-000004");
    firstCK.setChecklist(new LinkResource(1L));
    firstCK.setWorkItemType(new LinkResource(2L));
    firstCK.setLongDescription("Long description1");


    // group1 and subgroup2
    final WorkItemLightHDto secondCK = new WorkItemLightHDto();
    secondCK.setId(2L);
    secondCK.setReferenceCode("A2");
    secondCK.setName("SSSS");
    secondCK.setTaskNumber("000001-XXXX-000003");
    secondCK.setChecklist(new LinkResource(2L));
    secondCK.setWorkItemType(new LinkResource(2L));
    secondCK.setLongDescription("Long description2");


    // group2 and subgroup2
    final WorkItemLightHDto thirdCK = new WorkItemLightHDto();
    thirdCK.setId(3L);
    thirdCK.setReferenceCode("A3");
    thirdCK.setName("EEE");
    thirdCK.setTaskNumber("000001-XXXX-000001");
    thirdCK.setChecklist(new LinkResource(3L));
    thirdCK.setWorkItemType(new LinkResource(2L));


    // group2 and subgroup2
    final WorkItemLightHDto fourthCK = new WorkItemLightHDto();
    fourthCK.setId(4L);
    fourthCK.setReferenceCode("A4");
    fourthCK.setTaskNumber("000002-XXXX-000005");
    fourthCK.setChecklist(new LinkResource(3L));
    fourthCK.setWorkItemType(new LinkResource(2L));


    // group3 and subgroup1
    final WorkItemLightHDto fifthCK = new WorkItemLightHDto();
    fifthCK.setId(5L);
    fifthCK.setReferenceCode("A4");
    fifthCK.setTaskNumber("000002-XXXX-000003");
    fifthCK.setChecklist(new LinkResource(3L));
    fifthCK.setWorkItemType(new LinkResource(2L));


    // group1 and subgroup4
    final WorkItemLightHDto sixthCK = new WorkItemLightHDto();
    sixthCK.setId(6L);
    sixthCK.setReferenceCode("A6");
    sixthCK.setTaskNumber("000001-XXXX-000008");
    sixthCK.setChecklist(new LinkResource(2L));
    sixthCK.setWorkItemType(new LinkResource(2L));
    sixthCK.setLongDescription("Long description6");


    // group3 and subgroup1
    final WorkItemLightHDto seventhCK = new WorkItemLightHDto();
    seventhCK.setId(5L);
    seventhCK.setReferenceCode("A4");
    seventhCK.setTaskNumber("000002-XXXX-000001");
    seventhCK.setChecklist(new LinkResource(3L));
    seventhCK.setWorkItemType(new LinkResource(2L));


    // group3 and subgroup1
    final WorkItemLightHDto eighthCK = new WorkItemLightHDto();
    eighthCK.setId(5L);
    eighthCK.setReferenceCode("A4");
    eighthCK.setTaskNumber("000002-XXXX-000001");
    eighthCK.setChecklist(new LinkResource(3L));
    eighthCK.setWorkItemType(new LinkResource(2L));


    final List<WorkItemLightHDto> ckList1 = new ArrayList<>();
    ckList1.add(firstCK);
    ckList1.add(fourthCK);
    ckList1.add(seventhCK);

    final List<WorkItemLightHDto> ckList2 = new ArrayList<>();
    ckList2.add(secondCK);
    ckList2.add(fifthCK);
    ckList2.add(eighthCK);

    final List<WorkItemLightHDto> ckList3 = new ArrayList<>();
    ckList3.add(thirdCK);
    ckList3.add(sixthCK);


    // checklist subgroup mock data
    SubgroupDto subgrOneDto = new SubgroupDto();
    subgrOneDto.setId(1L);
    subgrOneDto.setName("Documentation");
    subgrOneDto.setChecklistItems(ckList1);

    SubgroupDto subgrTwoDto = new SubgroupDto();
    subgrTwoDto.setId(2L);
    subgrTwoDto.setName("Certificate-Naval");
    subgrTwoDto.setChecklistItems(ckList2);

    SubgroupDto subgrFourDto = new SubgroupDto();
    subgrFourDto.setId(4L);
    subgrFourDto.setName("Certified Lifeboatmen");
    subgrFourDto.setChecklistItems(ckList3);

    List<SubgroupDto> subgroups1 = new ArrayList<>();
    subgroups1.add(subgrOneDto);
    subgroups1.add(subgrTwoDto);

    List<SubgroupDto> subgroups2 = new ArrayList<>();
    subgroups2.add(subgrTwoDto);

    List<SubgroupDto> subgroups3 = new ArrayList<>();
    subgroups3.add(subgrOneDto);
    subgroups3.add(subgrFourDto);

    List<GroupDto> groups = new ArrayList<>();

    // checklist group mock data
    GroupDto grOneDto = new GroupDto();
    grOneDto.setId(1L);
    grOneDto.setName("Cargo Pump Room");
    grOneDto.setChecklistSubgrouplist(subgroups1);

    GroupDto grTwoDto = new GroupDto();
    grTwoDto.setId(2L);
    grTwoDto.setName("Survey Preparation");
    grTwoDto.setChecklistSubgrouplist(subgroups2);

    GroupDto grThreeDto = new GroupDto();
    grThreeDto.setId(3L);
    grThreeDto.setName("Accommodation");
    grThreeDto.setChecklistSubgrouplist(subgroups3);

    groups.add(grOneDto);
    groups.add(grTwoDto);
    groups.add(grThreeDto);

    hierarchyDto.setChecklistGroups(groups);

    return hierarchyDto;
  }

  /**
   * Returns mock asset.
   *
   * @return mock asset.
   */
  private AssetHDto mockAsset() {
    // mock asset data
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("v2 LADY K II");
    asset.setIsLead(false);
    asset.setLeadImo("1000019");
    asset.setAssetType(new AssetTypeDto());
    asset.getAssetType().setName("ABC TYPE");
    asset.setClassStatusDto(new ClassStatusHDto());
    asset.getClassStatusDto().setName("In Class (Laid Up)");

    asset.setBuildDate(new GregorianCalendar(2017, 1, 1).getTime());
    asset.setFlagStateDto(new FlagStateHDto());
    asset.getFlagStateDto().setName("Jersey");

    asset.setAssetLifeCycleStatusDto(new AssetLifeCycleStatusDto());
    asset.getAssetLifeCycleStatusDto().setName("In service");

    asset.setGrossTonnage(2.1);

    asset.setEffectiveDate(new GregorianCalendar(2017, 1, 1).getTime());

    asset.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    asset.getClassMaintenanceStatusDto().setName("Dual");

    asset.setCoClassificationSocietyDto(new ClassificationSocietyHDto());
    asset.getCoClassificationSocietyDto().setName("American Bureau Of Shipping");
    asset.getCoClassificationSocietyDto().setId(2L);
    asset.setHasPostponedService(true);
    asset.setDueStatusH(DueStatus.NOT_DUE);
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("AB");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    asset.setIhsAssetDto(ihsAssetDetailsDto);

    return asset;
  }

  /**
   * Returns list of mock services.
   *
   * @return list of mock services.
   */
  private List<ScheduledServiceHDto> mockServices() {
    // mock service data
    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setId(1L);
    service1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setLowerRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setUpperRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());
    service1.setDueStatusH(DueStatus.DUE_SOON);
    service1.setServiceCatalogueH(new ServiceCatalogueHDto());
    service1.getServiceCatalogueH().setName("Service 1");
    service1.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service1.getServiceCatalogueH().getProductCatalogue().setName("Machinery");
    service1.getServiceCatalogueH().setCode("MS");
    service1.setServiceCatalogueName("Service catalogue name");

    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    service2.setId(2L);
    service2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    service2.setLowerRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service2.setUpperRangeDate(new GregorianCalendar(2017, 1, 1).getTime());
    service2.setDueStatusH(DueStatus.DUE_SOON);
    service2.setServiceCatalogueH(new ServiceCatalogueHDto());
    service2.getServiceCatalogueH().setName("Service 2");
    service2.getServiceCatalogueH().setProductCatalogue(new ProductCatalogueDto());
    service2.getServiceCatalogueH().getProductCatalogue().setName("Other");
    service2.getServiceCatalogueH().setCode("AS");
    service2.setServiceCatalogueName("Service catalogue name2");

    final List<ScheduledServiceHDto> services = new ArrayList<>();
    services.add(service1);
    services.add(service2);

    return services;
  }

  /**
   * Returns mock checklist hierarchy DTO with null group list.
   *
   * @return mock checklist hierarchy DTO which holds null group list.
   */
  private HierarchyDto mockHierarchyDtoWithNullGrouplist() {
    HierarchyDto hierarchyDto = new HierarchyDto();
    hierarchyDto.setChecklistGroups(null);
    return hierarchyDto;
  }

  /**
   * Returns mock checklist hierarchy DTO with null subgroup list.
   *
   * @return mock checklist hierarchy DTO which holds null subgroup list.
   */
  private HierarchyDto mockHierarchyDtoWithNullSubgrouplist() {
    HierarchyDto hierarchyDto = new HierarchyDto();

    List<GroupDto> groups = new ArrayList<>();

    // checklist group mock data
    GroupDto grOneDto = new GroupDto();
    grOneDto.setId(1L);
    grOneDto.setName("Cargo Pump Room");
    grOneDto.setChecklistSubgrouplist(null);

    GroupDto grTwoDto = new GroupDto();
    grTwoDto.setId(2L);
    grTwoDto.setName("Survey Preparation");
    grTwoDto.setChecklistSubgrouplist(null);

    GroupDto grThreeDto = new GroupDto();
    grThreeDto.setId(3L);
    grThreeDto.setName("Accommodation");
    grThreeDto.setChecklistSubgrouplist(null);

    groups.add(grOneDto);
    groups.add(grTwoDto);
    groups.add(grThreeDto);

    hierarchyDto.setChecklistGroups(groups);
    return hierarchyDto;
  }

  /**
   * Returns mock checklist hierarchy DTO with null checklist item list.
   *
   * @return mock checklist hierarchy DTO which holds null checklist item list.
   */
  private  HierarchyDto mockHierarchyDtoWithNullChecklistItems() {

    HierarchyDto hierarchyDto = new HierarchyDto();

    // checklist subgroup mock data
    SubgroupDto subgrOneDto = new SubgroupDto();
    subgrOneDto.setId(1L);
    subgrOneDto.setName("Documentation");
    subgrOneDto.setChecklistItems(null);

    SubgroupDto subgrTwoDto = new SubgroupDto();
    subgrTwoDto.setId(2L);
    subgrTwoDto.setName("Certificate-Naval");
    subgrTwoDto.setChecklistItems(null);

    SubgroupDto subgrFourDto = new SubgroupDto();
    subgrFourDto.setId(4L);
    subgrFourDto.setName("Certified Lifeboatmen");
    subgrFourDto.setChecklistItems(null);

    List<SubgroupDto> subgroups1 = new ArrayList<>();
    subgroups1.add(subgrOneDto);
    subgroups1.add(subgrTwoDto);

    List<SubgroupDto> subgroups2 = new ArrayList<>();
    subgroups2.add(subgrTwoDto);

    List<SubgroupDto> subgroups3 = new ArrayList<>();
    subgroups3.add(subgrOneDto);
    subgroups3.add(subgrFourDto);

    List<GroupDto> groups = new ArrayList<>();

    // checklist group mock data
    GroupDto grOneDto = new GroupDto();
    grOneDto.setId(1L);
    grOneDto.setName("Cargo Pump Room");
    grOneDto.setChecklistSubgrouplist(subgroups1);

    GroupDto grTwoDto = new GroupDto();
    grTwoDto.setId(2L);
    grTwoDto.setName("Survey Preparation");
    grTwoDto.setChecklistSubgrouplist(subgroups2);

    GroupDto grThreeDto = new GroupDto();
    grThreeDto.setId(3L);
    grThreeDto.setName("Accommodation");
    grThreeDto.setChecklistSubgrouplist(subgroups3);

    groups.add(grOneDto);
    groups.add(grTwoDto);
    groups.add(grThreeDto);

    hierarchyDto.setChecklistGroups(groups);

    return hierarchyDto;
  }

  /**
   * LRCD-3215 as per new template checklist should always be tehre
   * Tests success scenario for checklist pdf generation if null group list passed to pug file.
   *
   * @throws ClassDirectException if API call fail.
   * @throws IOException if API call fail.
   */
//  @Test
  public final void testGenerateChecklistPDFforNullGrouplist() throws ClassDirectException, IOException {
    Mockito.when(taskService.getCheckListHierarchyByService(Matchers.anyLong()))
        .thenReturn(mockHierarchyDtoWithNullGrouplist());
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAsset());
    List<ScheduledServiceHDto> services = mockServices();
    services.get(0).setPostponementDate(null);
    Mockito.when(serviceReferenceService.getServicesForAsset(Mockito.anyLong())).thenReturn(services);
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ChecklistExportDto query = new ChecklistExportDto();
    query.setAssetCode("LRV1");
    query.setServiceId(1L);

    final StringResponse response = checklistExportService.downloadPdf(query);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   * Tests success scenario for checklist pdf generation if null subgroup list passed to pug file.
   *
   * @throws ClassDirectException if API call fail.
   * @throws IOException if API call fail.
   */
  @Test
  public final void testGenerateChecklistPDFforNullSubgrouplist() throws ClassDirectException, IOException {
    Mockito.when(taskService.getCheckListHierarchyByService(Matchers.anyLong()))
        .thenReturn(mockHierarchyDtoWithNullSubgrouplist());
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAsset());
    Mockito.when(serviceReferenceService.getServicesForAsset(Mockito.anyLong())).thenReturn(mockServices());
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ChecklistExportDto query = new ChecklistExportDto();
    query.setAssetCode("LRV1");
    query.setServiceId(1L);

    final StringResponse response = checklistExportService.downloadPdf(query);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   * Tests success scenario for checklist pdf generation if null checklist item list passed to pug
   * file.
   *
   * @throws ClassDirectException if API call fail.
   * @throws IOException if API call fail.
   */
  @Test
  public final void testGenerateChecklistPDFforNullChecklistItemlist() throws ClassDirectException, IOException {
    Mockito.when(taskService.getCheckListHierarchyByService(Matchers.anyLong()))
        .thenReturn(mockHierarchyDtoWithNullChecklistItems());
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAsset());
    Mockito.when(serviceReferenceService.getServicesForAsset(Mockito.anyLong())).thenReturn(mockServices());
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ChecklistExportDto query = new ChecklistExportDto();
    query.setAssetCode("LRV1");
    query.setServiceId(1L);

    final StringResponse response = checklistExportService.downloadPdf(query);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   *
   * Provides Spring in-class configurations.
   *
   * @author yng
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public ChecklistExportService checklistExportService() {
      return new ChecklistExportServiceImpl();
    }
  }

}
