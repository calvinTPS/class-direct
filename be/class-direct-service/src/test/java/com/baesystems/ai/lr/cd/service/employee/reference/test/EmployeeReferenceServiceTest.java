package com.baesystems.ai.lr.cd.service.employee.reference.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.employee.EmployeeRetrofitService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.employee.reference.impl.EmployeeReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for employee reference service {@link EmployeeReferenceService}.
 *
 * @author syalavarthi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EmployeeReferenceServiceTest.Config.class)
@DirtiesContext
public class EmployeeReferenceServiceTest {

  /**
   * The EmployeeReferenceService from spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;
  /**
   * The EmployeeRetrofitService from spring context.
   */
  @Autowired
  private EmployeeRetrofitService employeeRetrofitService;

  /**
   * Tests success scenario to get employee list {@link EmployeeReferenceService#getEmployeeList()}.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testGetEmployeeList() throws IOException {
    Mockito.when(employeeRetrofitService.getEmployee()).thenReturn(Calls.response(mockEmployeeList()));
    List<LrEmployeeDto> employeeList = employeeReferenceService.getEmployeeList();
    Assert.assertNotNull(employeeList);
    Assert.assertEquals(employeeList.get(0).getFirstName(), "david");
    Assert.assertEquals(employeeList.get(1).getFirstName(), "bob");
    Assert.assertEquals(employeeList.get(0).getName(), "UNKNOWN_MIGRATED_FNAME");
    Assert.assertEquals(employeeList.get(1).getName(), "UNKNOWN_MIGRATED_FNAME");
  }

  /**
   * Tests failure scenario to get employee list {@link EmployeeReferenceService#getEmployeeList()}
   * when mast api call fails.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testFailGetEmployeeList() throws IOException {
    Mockito.when(employeeRetrofitService.getEmployee()).thenReturn(Calls.failure(new IOException("mock")));
    List<LrEmployeeDto> employeeList = employeeReferenceService.getEmployeeList();
    Assert.assertNotNull(employeeList);
    Assert.assertTrue(employeeList.isEmpty());
  }

  /**
   * Tests success scenario to get employee by id {@link EmployeeReferenceService#getEmployee()}.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testGetEmployee() throws IOException {
    Mockito.when(employeeRetrofitService.getEmployee()).thenReturn(Calls.response(mockEmployeeList()));
    LrEmployeeDto employee = employeeReferenceService.getEmployee(1L);
    Assert.assertNotNull(employee);
    Assert.assertEquals(employee.getFirstName(), "david");
    Assert.assertEquals(employee.getName(), "UNKNOWN_MIGRATED_FNAME");
  }

  /**
   * Tests failure scenario to get employee by id {@link EmployeeReferenceService#getEmployee()}
   * when requested employee not found.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testfailGetEmployee() throws IOException {
    Mockito.when(employeeRetrofitService.getEmployee()).thenReturn(Calls.response(mockEmployeeList()));
    OfficeDto office = employeeReferenceService.getOffice(5L);
    Assert.assertNull(office);
  }

  /**
   * Tests success scenario to get office list {@link EmployeeReferenceService#getOfficeList()}.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testGetOfficeList() throws IOException {
    Mockito.when(employeeRetrofitService.getOffice()).thenReturn(Calls.response(mockOfficeList()));
    List<OfficeDto> officeList = employeeReferenceService.getOfficeList();
    Assert.assertNotNull(officeList);
    Assert.assertEquals(officeList.get(0).getCode(), "ALG");
    Assert.assertEquals(officeList.get(1).getCode(), "LDA");
    Assert.assertEquals(officeList.get(0).getName(), "Algiers");
    Assert.assertEquals(officeList.get(1).getName(), "Launda");
  }

  /**
   * Tests failure scenario to get office list {@link EmployeeReferenceService#getOfficeList()} when
   * mast api call fails.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testFailGetOfficeList() throws IOException {
    Mockito.when(employeeRetrofitService.getOffice()).thenReturn(Calls.failure(new IOException()));
    List<OfficeDto> officeList = employeeReferenceService.getOfficeList();
    Assert.assertNotNull(officeList);
    Assert.assertTrue(officeList.isEmpty());
  }

  /**
   * Tests success scenario to get office by id {@link EmployeeReferenceService#getOffice()}.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testGetOffie() throws IOException {
    Mockito.when(employeeRetrofitService.getOffice()).thenReturn(Calls.response(mockOfficeList()));
    OfficeDto office = employeeReferenceService.getOffice(1L);
    Assert.assertNotNull(office);
    Assert.assertEquals(office.getCode(), "ALG");
    Assert.assertEquals(office.getName(), "Algiers");
  }

  /**
   * Tests failure scenario to get office by id {@link EmployeeReferenceService#getOffice()} when
   * requested office not found.
   *
   * @throws IOException if mast api fail.
   */
  @Test
  public void testfailGetOffice() throws IOException {
    Mockito.when(employeeRetrofitService.getOffice()).thenReturn(Calls.response(mockOfficeList()));
    OfficeDto office = employeeReferenceService.getOffice(5L);
    Assert.assertNull(office);
  }

  /**
   * Mocks employee list.
   *
   * @return list of employee.
   */
  private List<LrEmployeeDto> mockEmployeeList() {
    List<LrEmployeeDto> employeeList = new ArrayList<LrEmployeeDto>();
    LrEmployeeDto employee = new LrEmployeeDto();
    employee.setId(1L);
    employee.setFirstName("david");
    employee.setEmailAddress("david.classdirect@gmail.com");
    employee.setLastName("classdirect");
    employee.setLegalEntity("legal entity");
    employee.setName("UNKNOWN_MIGRATED_FNAME");
    employeeList.add(employee);

    LrEmployeeDto employee2 = new LrEmployeeDto();
    employee2.setId(2L);
    employee2.setFirstName("bob");
    employee2.setEmailAddress("bob.classdirect@gmail.com");
    employee2.setLastName("classdirect");
    employee2.setLegalEntity("legal entity");
    employee2.setName("UNKNOWN_MIGRATED_FNAME");
    employeeList.add(employee2);

    return employeeList;
  }

  /**
   * Mocks office list.
   *
   * @return list of office list.
   */
  private List<OfficeDto> mockOfficeList() {
    List<OfficeDto> officeList = new ArrayList<>();

    OfficeDto office = new OfficeDto();
    office.setId(1L);
    office.setCode("ALG");
    office.setName("Algiers");
    office.setOfficeRole(new LinkResource(1L));
    officeList.add(office);

    OfficeDto office2 = new OfficeDto();
    office2.setId(2L);
    office2.setCode("LDA");
    office2.setName("Launda");
    office2.setOfficeRole(new LinkResource(1L));
    officeList.add(office2);
    return officeList;
  }

  /**
   * Spring bean config.
   *
   * @author SYalavarthi.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create service for {@link EmployeeReferenceService}.
     *
     * @return service.
     */
    @Override
    @Bean
    public EmployeeReferenceService employeeReferenceService() {
      return new EmployeeReferenceServiceImpl();
    }
  }
}
