package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.PmsTaskListExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.PmsTaskListExportService;
import com.baesystems.ai.lr.cd.service.export.impl.PmsTaskListExportServiceImpl;
import com.baesystems.ai.lr.cd.service.task.TaskService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author VKolagutla
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = PmsTaskListExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class PmsTaskListExportServiceTest {


  /**
   * Constant value task.
   */
  private static final String TASK = "task";

  /**
   * pmsTaskListExportService.
   */
  @Autowired
  private PmsTaskListExportService pmsTaskListExportService;

  /**
   * Injected asset service.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Inject task service.
   */
  @Autowired
  private TaskService taskService;

  /**
   * amazonStorageService.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * inject login user.
   */
  @Before
  public final void injectLoginUser() {
    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Test case for success download PMS PDF.
   * @throws ClassDirectException exception.
   * @throws IOException exception.
   */
  @Test
  public final void testDownloadPmsTaskListInfo() throws ClassDirectException, IOException {
    LocalDate date = LocalDate.now();

    final PmsTaskListExportDto dto  = new PmsTaskListExportDto();
    dto.setAssetCode("LRV1");

    final WorkItemTypeHDto type = new WorkItemTypeHDto();
    type.setName(TASK);
    final JobResolutionStatusHDto status = new JobResolutionStatusHDto();
    status.setName("Not Credited");

    final List<WorkItemLightHDto> pmsTasks = new ArrayList<WorkItemLightHDto>();
    final WorkItemLightHDto  pmsTask1 = new WorkItemLightHDto();
    pmsTask1.setName("Task1");
    pmsTask1.setDueDate(DateUtils.getLocalDateToDate(date));
    pmsTask1.setTaskNumber("1234567");
    pmsTask1.setAssignedDate(DateUtils.getLocalDateToDate(date));
    pmsTask1.setNotes("Testing Note 123.");
    pmsTask1.setWorkItemTypeH(type);
    pmsTask1.setPmsCreditDate(DateUtils.getLocalDateToDate(date));
    pmsTasks.add(pmsTask1);

    final WorkItemLightHDto  pmsTask2 = new WorkItemLightHDto();
    pmsTask2.setName("Task2 with a very long name");
    pmsTask2.setDueDate(DateUtils.getLocalDateToDate(date));
    pmsTask2.setTaskNumber("9999999");
    pmsTask2.setAssignedDate(DateUtils.getLocalDateToDate(date));
    pmsTasks.add(pmsTask2);

    final AssetHDto asset = new AssetHDto();
    asset.setName("Test Asset");
    asset.setLeadImo("9999999");
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("AB");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    asset.setIhsAssetDto(ihsAssetDetailsDto);
    asset.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    asset.getClassMaintenanceStatusDto().setName("Single");

    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(asset);
    Mockito.when(taskService.getHierarchicalTasksByAssetId(Mockito.anyLong(), Matchers.eq(true)))
    .thenReturn(mockHierarchicalTasks());
    Mockito.when(amazonStorageService.uploadFile(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
        Matchers.anyString()))
    .thenReturn("response");

    // call method
    final StringResponse response = pmsTaskListExportService.downloadPmsTaskListInfo(dto);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }
  /**
   * Converts json to object.
   *
   * @return mapped object.
   * @throws IOException if coping of stream fails.
   */
  public WorkItemPreviewListHDto mockHierarchicalTasks() throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    WorkItemPreviewListHDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/HierarchicalTasks.json"), writer,
        Charset.defaultCharset());
    content = mapper.readValue(writer.toString(), WorkItemPreviewListHDto.class);
    return content;

  }
  /**
   * Sprint in-class configurations.
   *
   * @author VKolagutla
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * un-mock pms export service.
     *
     * @return
     */
    @Override
    @Bean
    public PmsTaskListExportService pmsTaskListExportService() {
      return new PmsTaskListExportServiceImpl();
    }
  }

}
