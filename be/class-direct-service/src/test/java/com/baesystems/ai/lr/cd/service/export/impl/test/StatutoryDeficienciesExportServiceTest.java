package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DeficiencyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.DeficiencyService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.StatutoryDeficienciesExportService;
import com.baesystems.ai.lr.cd.service.export.impl.StatutoryDeficienciesExportServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.references.FaultCategoryDto;

/**
 * Provides unit test methods for {@link StatutoryDeficienciesExportService}.
 *
 * @author sbollu
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = StatutoryDeficienciesExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class StatutoryDeficienciesExportServiceTest {

  /**
   * The {@link DeficiencyService} from {@link StatutoryDeficienciesExportServiceTest.Config} class.
   */
  @Autowired
  private DeficiencyService defiService;


  /**
   * The {@link AssetService} from {@link StatutoryDeficienciesExportServiceTest.Config} class.
   */
  @Autowired
  private AssetService assetService;


  /**
   * The {@link StatutoryDeficienciesExportService} from
   * {@link StatutoryDeficienciesExportServiceTest.Config} class.
   */
  @Autowired
  private StatutoryDeficienciesExportService sdExportService;


  /**
   * The {@link AmazonStorageService} from {@link StatutoryDeficienciesExportServiceTest.Config}
   * class.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * Provides mock reset before any test class is executed.
   */
  @Before
  public void resetMock() {
    Mockito.reset(defiService);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }


  /**
   * Tests success scenario of generating PDF for
   * {@link StatutoryDeficienciesExportService#downloadPdf(ExportPdfDto)}.
   *
   * @throws ClassDirectException if IO exception occurred or Jade Exception occurred or document
   *         Exception occurred.
   */
  @Test
  public void testGenerateSDPDF() throws ClassDirectException {
    // mock asset data
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("My asset");
    asset.setLeadImo("1000019");
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("AB");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    asset.setIhsAssetDto(ihsAssetDetailsDto);
    asset.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    asset.getClassMaintenanceStatusDto().setName("Single");

    // mock sd data
    final DeficiencyHDto data1 = new DeficiencyHDto();
    data1.setId(1L);
    data1.setTitle("sd 1");
    data1.setIncidentDate(new GregorianCalendar(2017, 1, 2).getTime());
    data1.setCategory(new FaultCategoryDto());
    data1.getCategory().setName("Draft");


    final DeficiencyHDto data2 = new DeficiencyHDto();
    data2.setId(2L);
    data2.setTitle("sd 2");
    data2.setIncidentDate(new GregorianCalendar(2017, 3, 5).getTime());
    data2.setCategory(new FaultCategoryDto());
    data2.getCategory().setName("Draft");


    final DeficiencyHDto data3 = new DeficiencyHDto();
    data3.setId(3L);
    data3.setTitle("coc 3");
    data3.setIncidentDate(new GregorianCalendar(2017, 3, 1).getTime());
    data3.setCategory(new FaultCategoryDto());
    data3.getCategory().setName("Draft");

    final List<DeficiencyHDto> sdMock = new ArrayList<>();
    sdMock.add(data1);
    sdMock.add(data2);
    sdMock.add(data3);


    Mockito.when(defiService.getDeficiencies(Matchers.anyLong(), Mockito.any(DeficiencyQueryHDto.class)))
        .thenReturn(sdMock);
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(asset);
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ExportPdfDto query = new ExportPdfDto();
    query.setCode("LRV1");
    Set<Long> itemsToExport = new HashSet<Long>();
    itemsToExport.add(1L);
    itemsToExport.add(2L);
    query.setItemsToExport(itemsToExport);

    final StringResponse response = sdExportService.downloadPdf(query);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   *
   * Provides Spring in-class configurations.
   *
   * @author sbollu
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * un-mock StatutoryDeficiencies export service.
     *
     * @return
     */
    @Override
    @Bean
    public StatutoryDeficienciesExportService sdExportService() {
      return new StatutoryDeficienciesExportServiceImpl();
    }
  }

}
