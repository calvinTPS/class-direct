package com.baesystems.ai.lr.cd.service.userprofile.impl.test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BaseDtoPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.PartyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.AssetCodeUtil;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.ihs.IhsCompanyContactDto;

/**
 * Userprofile Service Mock Test Data.
 *
 * @author RKaneysan.
 */
public class UserProfileServiceMockTestData {
  /**
   * Constant one time.
   */
  protected static final int ONE = 1;
  /**
   * Constant two time.
   */
  protected static final int TWO = 2;
  /**
   * Constant three.
   */
  protected static final int THREE = 3;
  /**
   * Constant four.
   */
  protected static final int FOUR = 4;
  /**
   * LR_ADMIN_USER_ID.
   */
  protected static final String LR_ADMIN_USER_ID = "106";
  /**
   * Client Admin User Id.
   */
  protected static final String CLIENT_ADMIN_USER_ID = "107";
  /**
   * Client User Id.
   */
  protected static final String CLIENT_USER_ID = "111";
  /**
   * Builder Code.
   */
  protected static final String CLIENT_CODE = "1000003";

  /**
   * Client imo number.
   */
  protected static final String CLIENT_IMO_NUMBER = "1000191";

  /**
   * Client imo number.
   */
  protected static final String SHIP_BUILDER_IMO_NUMBER = "1000003";

  /**
   * flag code.
   */
  protected static final Long ANGOLA_FLAG_ID = 4L;
  /**
   * flag code.
   */
  protected static final String ANGOLA_FLAG_CODE = "ALA";
  /**
   * flag code.
   */
  protected static final Long AUSTRALIA_FLAG_ID = 9L;
  /**
   * flag code.
   */
  protected static final String AUSTRALIA_FLAG_CODE = "AST";
  /**
   * country id for Malaysia constant.
   */
  protected static final Long MALAYSIA_ID = 134L;
  /**
   * The Malaysia country name constant.
   */
  protected static final String MALAYSIA_NAME = "Malaysia";
  /**
   * flag code.
   */
  protected static final Long MALAYSIA_FLAG_ID = 117L;
  /**
   * flag code.
   */
  protected static final String MALAYSIA_FLAG_CODE = "MYS";
  /**
   * Postal code.
   */
  protected static final String POSTAL_CODE = "50770";
  /**
   * Default Year 1.
   */
  protected static final Integer DEFAULT_YEAR = 2016;
  /**
   * Default Year 2.
   */
  protected static final Integer DEFAULT_YEAR2 = 2017;
  /**
   * Month.
   */
  protected static final Integer DEFAULT_MONTH = 12;
  /**
   * Date.
   */
  protected static final Integer DEFAULT_DATE = 9;
  /**
   * Success User Id 1.
   */
  protected static final String SUCCESS_USER_ID = "101";
  /**
   * Success User Id 2.
   */
  protected static final String SUCCESS_USER_ID_2 = "102";
  /**
   * Success User Id 3.
   */
  protected static final String SUCCESS_USER_ID_3 = "104";
  /**
   * Success User Id 4.
   */
  protected static final String SUCCESS_USER_ID_4 = "d0835759-e2f5-4d0c-b445-c90a20d89466";
  /**
   * Success User Id 5.
   */
  protected static final String SUCCESS_USER_ID_5 = "2618aed6-3d12-41e5-9f24-c29f2c7ea4c1";
  /**
   * Success User Id 6.
   */
  protected static final String SUCCESS_USER_ID_6 = "32b345b9-a8cf-4534-a979-87f7b23ce8e2";
  /**
   * Success User Id 7.
   */
  protected static final String SUCCESS_USER_ID_7 = "f09f4de5-814c-4733-9482-0ee7104e30ae";
  /**
   * Success User Id 8.
   */
  protected static final String SUCCESS_USER_ID_8 = "103";
  /**
   * Success User Id 9.
   */
  protected static final String SUCCESS_USER_ID_9 = "48999039-cde0-4234-8225-0a6469c6b701";
  /**
   * Success User Id 10.
   */
  protected static final String SUCCESS_USER_ID_10 = "105";
  /**
   * Success User Id 11.
   */
  protected static final String SUCCESS_USER_ID_11 = "14943820-eb7d-4955-8c2c-1c9215633a40";
  /**
   * builderCode.
   */
  protected static final String BUILDER_CODE = "5";
  /**
   * User Email for Bob.
   */
  protected static final String BOB_USER_EMAIL = "bob.classdirect@gmail.com";
  /**
   * User Email for Alice.
   */
  protected static final String ALICE_USER_EMAIL = "alice.classdirect@gmail.com";
  /**
   * User Email for Charlie.
   */
  protected static final String CHARLIE_USER_EMAIL = "charlie.classdirect@gmail.com";
  /**
   * User Email for David.
   */
  protected static final String DAVID_USER_EMAIL = "david.classdirect@gmail.com";
  /**
   * User Email for Admin Joe.
   */
  protected static final String ADMIN_JOE_USER_EMAIL = "admin@lr.org";
  /**
   * User Email for Flag User.
   */
  protected static final String FLAG_USR_USER_EMAIL = "flag@flaguser.com";
  /**
   * User Email for Ship Builder.
   */
  protected static final String SHP_BUILDER_USER_EMAIL = "ship@shipbuilder.com";
  /**
   * User Email for Oscar.
   */
  protected static final String OSCAR_USER_EMAIL = "oscar.classdirect@gmail.com";
  /**
   * User Email for Client User.
   */
  protected static final String USR_CLIENT_USER_EMAIL = "user@client.com";
  /**
   * User Email for Ship Builder.
   */
  protected static final String CLIENT_USR_2_USER_EMAIL = "client@client.org";
  /**
   * User Email for ABC.
   */
  protected static final String ABC_USER_EMAIL = "abc.classdirect@gmail.com";
  /**
   * User id for ABC.
   */
  protected static final String TEST_USER = "052d354b-096a-407c-bf5e-34a38a646527";
  /**
   * User id for ABC.
   */
  protected static final String TEST_USER2 = "11111111-096a-407c-bf5e-34a38a646527";
  /**
   * Status Id 1.
   */
  protected static final Long STATUS_ID1 = 1L;
  /**
   * Status Id 2.
   */
  protected static final Long STATUS_ID2 = 2L;
  /**
   * Status Id 3.
   */
  protected static final Long STATUS_ID3 = 3L;
  /**
   * Initial result result.
   */
  public static final Integer INITIAL_RESULT_SIZE = 8;
  /**
   * Default size.
   */
  protected static final Integer DEFAULT_SIZE = 3;
  /**
   * Initial page number.
   *
   */
  protected static final Integer INITIAL_PAGE_NUMBER = 1;
  /**
   * User Id.
   */
  protected static final Long USER_ID = 1L;
  /**
   * Fail User Id.
   */
  protected static final String FAIL_USER_ID = "120";
  /**
   * Asset Id 1.
   */
  protected static final Long ASSET_ID_1 = 1L;
  /**
   * Asset Id 2.
   */
  protected static final Long ASSET_ID_2 = 2L;
  /**
   * Asset Id 3.
   */
  protected static final Long ASSET_ID_3 = 3L;
  /**
   * Internal LR domain.
   */
  protected static final String LR_DOMAIN = "lr.org";
  /**
   * Email with internal LR domain.
   */
  protected static final String INTERNAL_EMAIL = "whatever@lr.org";
  /**
   * Email with externel domain.
   */
  protected static final String EXTERNAL_EMAIL = "whatever@google.com";

  /**
   * User Profile Service Mock Test Data.
   */
  protected UserProfileServiceMockTestData() {
    super();
  }

  /**
   * Mock client information.
   *
   * @param clientImo client imo number.
   * @param isClient indicator is it client user or not.
   * @return List of Client Information.
   * @throws ClassDirectException ClassDirectException.
   */
  protected final List<PartyHDto> mockClientInfo(final String clientImo, final boolean isClient)
      throws ClassDirectException {

    final List<PartyHDto> partyList = new ArrayList<>();
    final PartyHDto partyDto = new PartyHDto();
    partyDto.setId(1L);
    partyDto.setImoNumber(clientImo);

    if (isClient) {
      partyDto.setName("Ership Internacional SA");
    } else {
      partyDto.setName("A builder");
    }

    partyDto.setAddressLine1("Menara Binjai, Level 28,");
    partyDto.setAddressLine2("No. 2,");
    partyDto.setAddressLine3("Jalan Binjai,");
    partyDto.setCity("Kuala Lumpur");
    partyDto.setPostcode("50450");
    partyDto.setCountry("Malaysia");
    partyList.add(partyDto);

    return partyList;
  }

  /**
   * Mock Base Dto.
   *
   * @return value.
   */
  protected final BaseDtoPageResource mockBaseDto() {
    BaseDtoPageResource pageResource = new BaseDtoPageResource();
    List<BaseDto> baseDtos = new ArrayList<>();
    BaseDto baseDto = new BaseDto();
    baseDto.setId(ASSET_ID_1);
    BaseDto baseDto2 = new BaseDto();
    baseDto2.setId(ASSET_ID_2);
    baseDtos.add(baseDto);
    baseDtos.add(baseDto2);
    pageResource.setContent(baseDtos);
    return pageResource;
  }

  /**
   * Return getAsset.
   *
   * @return value.
   */
  private AssetHDto getAsset() {
    AssetHDto asset = new AssetHDto();
    asset.setBuilder("Ership Internacional SA");
    asset.setYardNumber("Yard1");
    asset.setId(ASSET_ID_1);
    return asset;
  }

  /**
   * Mock list of assets.
   *
   * @return asset list.
   */
  protected final List<AssetHDto> mockAssets() {
    final List<AssetHDto> assets = new ArrayList<>();

    final AssetHDto asset1 = getAsset();
    asset1.setId(ASSET_ID_1);
    asset1.setCode(AssetCodeUtil.getMastCode(ASSET_ID_1));
    assets.add(asset1);

    final AssetHDto asset2 = getAsset();
    asset2.setId(ASSET_ID_2);
    asset2.setCode(AssetCodeUtil.getMastCode(ASSET_ID_2));
    assets.add(asset2);

    return assets;
  }

  /**
   * GET_USERS_USER_ID.
   */
  protected static final String GET_USERS_USER_ID = "108";

  /**
   * Mock User Ids.
   *
   * @return mocked User Maps.
   */
  protected final SortedMap<String, String> mockUserIds() {
    final SortedMap<String, String> userMap = new TreeMap<>();
    userMap.put(SUCCESS_USER_ID_2, ALICE_USER_EMAIL);
    userMap.put(SUCCESS_USER_ID_3, CHARLIE_USER_EMAIL);
    userMap.put(SUCCESS_USER_ID_4, DAVID_USER_EMAIL);
    return userMap;
  }

  /**
   * Mock multiple users from azure.
   *
   * @return list of lrid users.
   */
  protected final List<LRIDUserDto> mockLRIDUserDtoList() {
    final List<LRIDUserDto> list1 = new ArrayList<LRIDUserDto>();
    final LRIDUserDto lridDto1 = mockLRIDUserDto(null, ALICE_USER_EMAIL, "Alice", "Joe");
    list1.add(lridDto1);
    final LRIDUserDto lridDto2 = mockLRIDUserDto(null, BOB_USER_EMAIL, "Bob", "Joe");
    list1.add(lridDto2);
    return list1;
  }

  /**
   * Mock single user from azure.
   *
   * @param userId user id.
   * @param userEmail user email.
   * @param firstName user first name.
   * @param lastName user last name.
   * @return lrid user.
   */
  protected final LRIDUserDto mockLRIDUserDto(final String userId, final String userEmail, final String firstName,
      final String lastName) {
    final LRIDUserDto lridDto1 = new LRIDUserDto();
    lridDto1.setSignInName(userEmail);
    lridDto1.setObjectId(userId);
    lridDto1.setRcaCity("London");
    lridDto1.setGivenName(firstName);
    lridDto1.setSurname(lastName);
    lridDto1.setOrganisationalRole("Manager");
    lridDto1.setRcaZipPostalCode(POSTAL_CODE);
    return lridDto1;
  }

  /**
   * Mock LRID User Dto List.
   *
   * @param resultSize count.
   * @return list of LRIDUserDto.
   */
  protected final List<LRIDUserDto> mockLRIDUserDtoList(final Integer resultSize) {

    final List<String> emails = Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL, CHARLIE_USER_EMAIL,
        DAVID_USER_EMAIL, OSCAR_USER_EMAIL, SHP_BUILDER_USER_EMAIL, FLAG_USR_USER_EMAIL, USR_CLIENT_USER_EMAIL,
        CLIENT_USR_2_USER_EMAIL, ABC_USER_EMAIL, ADMIN_JOE_USER_EMAIL});

    final List<String> userIds = Arrays.asList(new String[] {SUCCESS_USER_ID, SUCCESS_USER_ID_2, SUCCESS_USER_ID_3,
        SUCCESS_USER_ID_4, SUCCESS_USER_ID_5, SUCCESS_USER_ID_6, SUCCESS_USER_ID_7, SUCCESS_USER_ID_8,
        SUCCESS_USER_ID_9, SUCCESS_USER_ID_10, SUCCESS_USER_ID_11});

    return mockLRIDUserDtoList(userIds, emails, resultSize);
  }

  /**
   * Mock multiple users from azure.
   *
   * @param emails list of users email.
   * @return list of lrid users.
   */
  protected final List<LRIDUserDto> mockLRIDUserDtoList(final List<String> emails) {
    final List<LRIDUserDto> list1 = new ArrayList<LRIDUserDto>();

    for (int index = 0; index < emails.size(); index++) {
      final LRIDUserDto lridDto1 = new LRIDUserDto();
      lridDto1.setSignInName(emails.get(index));
      lridDto1.setRcaCity("London");
      lridDto1.setGivenName("FirstName" + index);
      lridDto1.setSurname("LastName" + index);
      lridDto1.setOrganisationalRole("Manager");
      lridDto1.setRcaZipPostalCode(POSTAL_CODE);
      list1.add(lridDto1);
    }

    return list1;
  }

  /**
   * to get single user from azure.
   *
   * @return list of Azure user.
   */
  protected final List<LRIDUserDto> mockLRIDUser() {
    final List<LRIDUserDto> mockLRIDUserList = new ArrayList<>();

    final LRIDUserDto bobLridDto = new LRIDUserDto();
    bobLridDto.setObjectId(SUCCESS_USER_ID);
    bobLridDto.setSignInName(BOB_USER_EMAIL);
    bobLridDto.setRcaCity("London");
    bobLridDto.setGivenName("Bob");
    bobLridDto.setSurname("Joe");
    bobLridDto.setOrganisationalRole("Manager");
    bobLridDto.setRcaZipPostalCode(POSTAL_CODE);

    final LRIDUserDto aliceLridDto = new LRIDUserDto();
    aliceLridDto.setSignInName(ALICE_USER_EMAIL);
    aliceLridDto.setRcaCity("London");
    aliceLridDto.setGivenName("Alice");
    aliceLridDto.setSurname("Joe");
    aliceLridDto.setOrganisationalRole("Manager");
    aliceLridDto.setRcaZipPostalCode(POSTAL_CODE);

    mockLRIDUserList.add(bobLridDto);
    mockLRIDUserList.add(aliceLridDto);

    return mockLRIDUserList;
  }

  /**
   * Mock LRID User Dto List.
   *
   * @param userIds users id.
   * @param emails users email.
   * @param count count.
   * @return list of LRIDUserDto.
   */
  private List<LRIDUserDto> mockLRIDUserDtoList(final List<String> userIds, final List<String> emails,
      final Integer count) {
    List<String> includedUsers = new ArrayList<>();
    List<LRIDUserDto> azureList = new ArrayList<>();

    // Populate all respective user profile as it would have been when retrieved from DB.
    for (int index = 0; index < count; index++) {
      final UserProfiles up = mockUserprofile(userIds.get(index), emails.get(index));
      LRIDUserDto lridDto = getUpdatedAzureDto(up);
      azureList.add(lridDto);
    }
    includedUsers.addAll(emails);
    return azureList.parallelStream().limit(count).collect(Collectors.toList());
  }

  /**
   * Mock multiple users.
   *
   * @param emails list of users email.
   * @param roles list of users role.
   * @param userIds list of users id.
   * @param count number of users.
   * @return list of user profiles.
   */
  protected static final List<UserProfiles> mockUsersList(final List<String> emails, final List<Role> roles,
      final List<String> userIds, final int count) {
    final UserAccountStatus status = mockUserAccountStatus(AccountStatusEnum.ACTIVE);

    List<UserProfiles> userProfiles = new ArrayList<>();
    for (int index = 0; index < emails.size(); index++) {
      String email = emails.get(index);
      Role roleEnum = roles.get(index);
      String userId = userIds.get(index);
      Roles role = new Roles();
      role.setRoleId(roleEnum.getId());
      role.setRoleName(roleEnum.toString());
      Set<Roles> rolesObject = new HashSet<Roles>();
      rolesObject.add(role);
      final UserProfiles userProfile = new UserProfiles();
      userProfile.setUserId(userId);
      userProfile.setEmail(email);
      userProfile.setLastLogin(LocalDateTime.now());
      userProfile.setStatus(status);
      userProfile.setRoles(rolesObject);
      userProfile.setDateOfModification(LocalDateTime.now());
      userProfiles.add(userProfile);
    }
    return userProfiles;
  }

  /**
   * Mock multiple users.
   *
   * @return value.
   */
  protected static final List<UserProfiles> mockUsersList() {
    final UserAccountStatus status = mockUserAccountStatus(AccountStatusEnum.ACTIVE);
    List<UserProfiles> userProfiles = new ArrayList<>();
    final UserProfiles userProfile = new UserProfiles();
    userProfile.setUserId(TEST_USER);
    userProfile.setFirstName("alice");
    userProfile.setLastName("Marsh");
    userProfile.setFlagCode("ALA");
    userProfile.setEmail(ALICE_USER_EMAIL);
    userProfile.setLastLogin(
        LocalDateTime.of(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DATE, DEFAULT_DATE, DEFAULT_DATE, DEFAULT_DATE));
    userProfile.setStatus(status);
    Roles role = new Roles();
    role.setRoleId(Role.CLIENT.getId());
    role.setRoleName(Role.CLIENT.toString());
    Set<Roles> rolesObject = new HashSet<Roles>();
    rolesObject.add(role);
    userProfile.setRoles(rolesObject);
    userProfile.setDateOfModification(LocalDateTime.now());
    userProfiles.add(userProfile);
    final UserProfiles userProfile1 = new UserProfiles();
    userProfile1.setUserId("052d354b-096a-407c-bf5e-34a38a646526");
    userProfile1.setFirstName("bob");
    userProfile1.setLastName("Marsh");
    userProfile1.setClientCode(BUILDER_CODE);
    userProfile1.setEmail(BOB_USER_EMAIL);
    userProfile1.setLastLogin(
        LocalDateTime.of(DEFAULT_YEAR2, DEFAULT_MONTH, DEFAULT_DATE, DEFAULT_DATE, DEFAULT_DATE, DEFAULT_DATE));
    userProfile1.setStatus(status);
    Roles role1 = new Roles();
    role1.setRoleId(Role.LR_ADMIN.getId());
    role1.setRoleName(Role.LR_ADMIN.toString());
    Set<Roles> rolesObject1 = new HashSet<Roles>();
    rolesObject1.add(role1);
    userProfile1.setRoles(rolesObject1);
    userProfile1.setDateOfModification(LocalDateTime.now());
    userProfiles.add(userProfile1);
    final UserProfiles userProfile2 = new UserProfiles();
    userProfile2.setUserId("052d354b-096a-407c-bf5e-34a38a646528");
    userProfile2.setFirstName("david");
    userProfile2.setLastName("Marsh");
    userProfile2.setClientCode(BUILDER_CODE);
    userProfile2.setEmail(DAVID_USER_EMAIL);
    userProfile2.setStatus(status);
    userProfile2.setDateOfModification(LocalDateTime.now());
    userProfiles.add(userProfile2);
    return userProfiles;
  }

  /**
   * Mock User Account Status.
   *
   * @return UserAccountStatus updated user account status.
   * @param statusEnum account status enumerator.
   */
  protected static final UserAccountStatus mockUserAccountStatus(final AccountStatusEnum statusEnum) {
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(statusEnum.getId());
    status.setName(statusEnum.getName());
    return status;
  }

  /**
   * Mock List of User Account Status.
   *
   * @param statusEnumList list of account status enumerator.
   * @return UserAccountStatus updated user account status.
   */
  protected static final List<UserAccountStatus> mockUserAccountStatusList(
      final List<AccountStatusEnum> statusEnumList) {
    List<UserAccountStatus> userAcctStatusList = new ArrayList<>();
    for (AccountStatusEnum statusEnum : statusEnumList) {
      final UserAccountStatus status = new UserAccountStatus();
      status.setId(statusEnum.getId());
      status.setName(statusEnum.getName());
      userAcctStatusList.add(status);
    }
    return userAcctStatusList;
  }

  /**
   * Mock query dto.
   *
   * @param includedUsers users.
   * @param excludedUsers users.
   * @return dto.
   */
  protected static final UserProfileDto mockQueryDto(final List<String> includedUsers,
      final List<String> excludedUsers) {
    UserProfileDto dto = new UserProfileDto();
    dto.setUserId("");
    dto.setRoles(null);
    dto.setStatus(Collections.emptyList());
    dto.setEmail("");
    dto.setExcludedUsers(includedUsers);
    dto.setIncludedUsers(includedUsers);
    dto.setSearchSSOIfNotExist(false);
    return dto;
  }

  /**
   * Update mock Azure data.
   *
   * @param up user profile.
   * @return updated user profile.
   */
  protected static final LRIDUserDto getUpdatedAzureDto(final UserProfiles up) {

    LRIDUserDto lridDto = new LRIDUserDto();
    lridDto.setSignInName(up.getEmail());
    lridDto.setRcaCity("London");
    lridDto.setGivenName("First");
    lridDto.setSurname("Last");
    lridDto.setOrganisationalRole("Manager");
    lridDto.setRcaZipPostalCode(POSTAL_CODE);
    lridDto.setAccountEnabled(true);
    lridDto.setRcaAddressLine1("street no 1");
    lridDto.setRcaAddressLine2("marthalli");
    lridDto.setRcaAddressLine3("BNG");

    return lridDto;
  }

  /**
   * Mock Users.
   *
   * @param count count.
   * @return list of users.
   */
  protected static final List<UserProfiles> mockUsers(final Integer count) {
    final List<String> emails = Arrays.asList(new String[] {ALICE_USER_EMAIL, BOB_USER_EMAIL, CHARLIE_USER_EMAIL,
        DAVID_USER_EMAIL, OSCAR_USER_EMAIL, SHP_BUILDER_USER_EMAIL, FLAG_USR_USER_EMAIL, USR_CLIENT_USER_EMAIL,
        CLIENT_USR_2_USER_EMAIL, ABC_USER_EMAIL, ADMIN_JOE_USER_EMAIL});

    final List<String> userIds = Arrays.asList(new String[] {SUCCESS_USER_ID, SUCCESS_USER_ID_2, SUCCESS_USER_ID_3,
        SUCCESS_USER_ID_4, SUCCESS_USER_ID_5, SUCCESS_USER_ID_6, SUCCESS_USER_ID_7, SUCCESS_USER_ID_8,
        SUCCESS_USER_ID_9, SUCCESS_USER_ID_10, SUCCESS_USER_ID_11});

    return mockUsers(userIds, emails, count);
  }

  /**
   * Mock Users.
   *
   * @param userIds users id.
   * @param emails users email.
   * @param count count.
   * @return list of users.
   */
  protected static final List<UserProfiles> mockUsers(final List<String> userIds, final List<String> emails,
      final Integer count) {
    List<UserProfiles> users = new ArrayList<>();
    // Populate all respective user profile as it would have been when retrieved from DB.
    for (int index = 0; index < count; index++) {
      final UserProfiles up1 = mockUserprofile(SUCCESS_USER_ID, ALICE_USER_EMAIL);
      up1.setCdUser(true);
      up1.setSsoUser(true);
      users.add(up1);
    }
    return users.parallelStream().limit(count).collect(Collectors.toList());
  }

  /**
   * Mock Userprofile.
   *
   * @param id id.
   * @return value.
   */
  protected static final UserProfiles mockUser(final String id) {
    return mockUserprofile(id, BOB_USER_EMAIL);
  }

  /**
   * Mock User Role.
   *
   * @param roleEnum user role enum.
   * @return mocked user role.
   */
  protected static final Set<Roles> mockUserRole(final Role roleEnum) {
    Roles role = new Roles();
    role.setRoleId(roleEnum.getId());
    role.setRoleName(roleEnum.toString());

    Set<Roles> lrInternalRolesObject = new HashSet<Roles>();
    return lrInternalRolesObject;
  }

  /**
   * Mock flag state list.
   *
   * @return value.
   */
  protected static final List<FlagStateHDto> mockFlagStateList() {
    final List<FlagStateHDto> flagList = new ArrayList<>();
    FlagStateHDto flagDto = new FlagStateHDto();
    flagDto.setId(ANGOLA_FLAG_ID);
    flagDto.setFlagCode(ANGOLA_FLAG_CODE);
    flagDto.setName("Angola");
    flagDto.setDeleted(true);
    flagList.add(flagDto);
    FlagStateHDto flagDto2 = new FlagStateHDto();
    flagDto2.setId(AUSTRALIA_FLAG_ID);
    flagDto2.setFlagCode(AUSTRALIA_FLAG_CODE);
    flagDto2.setName("Australia");
    flagDto2.setDeleted(false);
    flagList.add(flagDto2);
    FlagStateHDto flagDto3 = new FlagStateHDto();
    flagDto3.setId(MALAYSIA_FLAG_ID);
    flagDto3.setFlagCode(MALAYSIA_FLAG_CODE);
    flagDto3.setName("Malaysia");
    flagDto3.setDeleted(false);
    flagList.add(flagDto3);
    return flagList;
  }

  /**
   * Mock User.
   *
   * @param userId user id.
   * @param email user email.
   * @return mocked User Profile.
   */
  protected static final UserProfiles mockUserprofile(final String userId, final String email) {
    final UserAccountStatus status = new UserAccountStatus();
    status.setId(AccountStatusEnum.ACTIVE.getId());
    status.setName("Active");
    return mockUserprofile(userId, email, status);
  }

  /**
   * Mock Users.
   *
   * @param userIds userIds.
   * @return users list of userprofiles.
   */
  protected static final List<UserProfiles> mockUsers(final Map<String, String> userIds) {
    List<UserProfiles> userList = new ArrayList<>();

    // Populate all respective user profile as it would have been when retrieved from DB.
    userIds.entrySet().forEach(user -> {
      final UserProfiles up = mockUserprofile(user.getKey(), user.getValue());
      userList.add(up);
    });

    return userList;
  }

  /**
   * Mock User.
   *
   * @param userId user id.
   * @param email user email.
   * @param usrStatus user status.
   * @return mocked User Profile.
   */
  protected static final UserProfiles mockUserprofile(final String userId, final String email,
      final UserAccountStatus usrStatus) {
    final UserAccountStatus active = new UserAccountStatus();
    active.setId(AccountStatusEnum.ACTIVE.getId());
    active.setName("Active");
    final Roles lrAdmin = new Roles();
    lrAdmin.setRoleName(Role.LR_ADMIN.toString());

    final UserProfiles up = new UserProfiles();
    up.setUserId(userId);
    up.setEmail(email);
    up.setRoles(Collections.singleton(lrAdmin));
    up.setStatus(usrStatus);
    up.setSsoUser(false);
    up.setFirstName("First");
    return up;
  }

  /**
   * Provides mocked country reference data for testing.
   * @return list of mocked country reference data.
   */
  protected static final List<CountryHDto> mockCountryList() {
    final List<CountryHDto> countryList = new ArrayList<>(1);
    final CountryHDto country = new CountryHDto();
    country.setId(MALAYSIA_ID);
    country.setName(MALAYSIA_NAME);
    countryList.add(country);

    return countryList;
  }

  /**
   * Provides mocked IHS company contact details.
   * @return the mocked IHS company contact details.
   */
  protected static final IhsCompanyContactDto mockCompanyContact() {
    final IhsCompanyContactDto contact = new IhsCompanyContactDto();
    contact.setAddressLine1("Address line 1");
    contact.setAddressLine2("Address line 2");
    contact.setAddressLine3("Address line 3");
    contact.setCompanyName("BAE");
    contact.setCountryFullName("Malaysia");
    contact.setPrePostCode("PREPOSTCODE");
    contact.setPostPostCode("POSTPOSTCODE");
    contact.setTelephone("603-1234567");
    contact.setTownName1("Town 1");
    contact.setTownName2("Town 2");

    return contact;
  }
}
