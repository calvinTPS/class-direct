package com.baesystems.ai.lr.cd.be.service.security.impl.test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;
import com.baesystems.ai.lr.cd.be.service.security.ClassDirectSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.impl.ClassDirectSecurityServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * Provides unit tests for class direct security service {@link ClassDirectSecurityService}.
 *
 * @author RKaneysan
 * @author syalavarthi 21-06-2017.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ClassDirectSecurityServiceImplTest.Config.class)
@DirtiesContext
public class ClassDirectSecurityServiceImplTest {

  /**
   * The equasis shared key.
   */
  @Value("${equasis.shared.key}")
  private String equasisSharedKey;

  /**
   * The french time zone id.
   */
  private static final String FRENCH_TIME_ZONE = "Europe/Paris";

  /**
   * The date pattern for date formatting.
   */
  private static final String DATE_PATTERN = "yyyyMMddHHmmss";

  /**
   * The format date time in time stamp format.
   */
  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);

  /**
   * The {@link ClassDirectSecurityService} from spring context.
   */
  @Autowired
  private ClassDirectSecurityService classDirectSecurityService;

  /**
   * Tests success scenario to validate equasis user with valid hash key
   * {@link ClassDirectSecurityService#authenticateHash(String, String, String)}.
   *
   * @throws ClassDirectSecurityException if the given hash (shared key) does not match with the
   *         newly generated hash key..
   */
  @Test
  public final void testEquasisValidHash() throws ClassDirectSecurityException {
    final String imoNumberParam = "1000021";
    final String timeParam = "20160926134115";
    classDirectSecurityService.authenticateHash(imoNumberParam, timeParam, "6746eb1ac9bb07d10cacd729b126c6ee");
  }

  /**
   * Tests failure scenario to validate equasis user with invalid hash key
   * {@link ClassDirectSecurityService#authenticateHash(String, String, String)}.
   *
   * @throws ClassDirectSecurityException if the given hash (shared key) does not match with the
   *         newly generated hash key..
   */
  @Test(expected = ClassDirectSecurityException.class)
  public final void testEquasisInvalidHash() throws ClassDirectSecurityException {
    final String imoNumberParam = "1000021";
    final String timeParam = "2016-09-26 13:41:15.518";
    classDirectSecurityService.authenticateHash(imoNumberParam, timeParam, "TBbYmE2wbSUNk3VvCMHQkBtHmq8=");
  }

  /**
   * Tests failure scenario for validate time
   * {@link ClassDirectSecurityService#validateTime(String)} with invalid timestamp format.
   *
   * @throws ClassDirectSecurityException if expiration period exceeds or error in date conversions.
   */
  @Test(expected = ClassDirectSecurityException.class)
  public final void testInvalidTime() throws ClassDirectSecurityException {
    classDirectSecurityService.validateTime("23423sd234234234");
  }

  /**
   * Tests failure scenario for validate time
   * {@link ClassDirectSecurityService#validateTime(String)} with exceeded expiration period.
   *
   * @throws ClassDirectSecurityException if expiration period exceeds or error in date conversions.
   */
  @Test(expected = ClassDirectSecurityException.class)
  public final void testExceededExpirationPeriod() throws ClassDirectSecurityException {
    final Long offSetTimeInSeconds = 5400L;
    final LocalDateTime frenchLocalDateTime =
        ZonedDateTime.now(ZoneId.of(FRENCH_TIME_ZONE)).toLocalDateTime().minusSeconds(offSetTimeInSeconds);
    classDirectSecurityService.validateTime(frenchLocalDateTime.format(DATE_TIME_FORMATTER));
  }

  /**
   * Tests failure scenario for validate time
   * {@link ClassDirectSecurityService#validateTime(String)} with exceeded expiration period of
   * future date.
   *
   * @throws ClassDirectSecurityException if expiration period exceeds or error in date conversions.
   */
  @Test(expected = ClassDirectSecurityException.class)
  public final void testFutureDateRequest() throws ClassDirectSecurityException {
    final Long offSetTimeInSeconds = 5400L;
    final LocalDateTime frenchLocalDateTime =
        ZonedDateTime.now(ZoneId.of(FRENCH_TIME_ZONE)).toLocalDateTime().plusSeconds(offSetTimeInSeconds);
    classDirectSecurityService.validateTime(frenchLocalDateTime.format(DATE_TIME_FORMATTER));
  }

  /**
   * Tests success scenario for validate time
   * {@link ClassDirectSecurityService#validateTime(String)}.
   *
   * @throws ClassDirectSecurityException if expiration period exceeds or error in date conversions.
   */
  @Test
  public final void testValidTime() throws ClassDirectSecurityException {
    final Long offSetTimeInSeconds = 900L;
    final LocalDateTime frenchLocalDateTime =
        ZonedDateTime.now(ZoneId.of(FRENCH_TIME_ZONE)).toLocalDateTime().minusSeconds(offSetTimeInSeconds);
    classDirectSecurityService.validateTime(frenchLocalDateTime.format(DATE_TIME_FORMATTER));
  }

  /**
   * Spring bean config.
   *
   * @author rkaneysan
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Create hmacSecurityService for {@link hmacSecurityService}.
     *
     * @return hmacSecurityService.
     */
    @Override
    @Bean
    public ClassDirectSecurityService classDirectSecurityService() {
      return new ClassDirectSecurityServiceImpl();
    }

    /**
     * Create asset services for {@link AssetService}.
     *
     * @return asset services.
     */
    @Override
    @Bean
    public AssetService assetService() {
      return new AssetServiceImpl();
    }
  }
}
