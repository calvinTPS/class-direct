package com.baesystems.ai.lr.cd.service.export.impl.test;

import com.baesystems.ai.lr.cd.be.cs10.CS10Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

import com.baesystems.ai.lr.cd.be.domain.dto.export.CertificateAttachDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.service.export.CertificateDownloadService;
import com.baesystems.ai.lr.cd.service.export.impl.CertificateDownloadServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * Provides unit test methods to test authentication of {@link CertificateDownloadService}.
 *
 * @author syalavarthi.
 * @author sbollu.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestExecutionListeners(listeners = {ServletTestExecutionListener.class, DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class,
    WithSecurityContextTestExecutionListener.class})
public class CertificateDownloadSecurityTest {
  /**
   * The {@link CertificateDownloadService} from Spring context.
   *
   */
  @Autowired
  private CertificateDownloadService certificateDownloadService;

  /**
   * Tests success scenario of authentication
   * {@link CertificateDownloadService#downloadCertificateInfo(CertificateAttachDto)}, API should
   * throw exception for any unauthorized access.
   *
   * @throws Exception if user has no authorities to the API.
   */
  @Test(expected = AccessDeniedException.class)
  @WithMockUser(authorities = {"EQUASIS", "THESIS"})
  public final void shouldReturnExceptionForEquasis() throws Exception {
    StringResponse response = certificateDownloadService.downloadCertificateInfo(new CertificateAttachDto());
  }

  /**
   * Provides Spring in-class configurations.
   *
   * @author syalavarthi
   *
   */
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  public static class Config extends BaseMockConfiguration {


    @Override
    @Bean
    public CertificateDownloadService certificateDownloadService() {
      return new CertificateDownloadServiceImpl();
    }

    /**
     * Mock cs10 service.
     * @return mock service.
     */
    @Bean
    public CS10Service cs10Service() {
      return Mockito.mock(CS10Service.class);
    }

    /**
     * @param auth The authentication builder.
     * @throws Exception if authentication build fails with given user,password and authorities.
     */
    @Autowired
    public final void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
      auth.inMemoryAuthentication().withUser(" ").password(" ").authorities("EQUASIS", "THESIS");
    }

  }
}
