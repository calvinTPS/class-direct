package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.service.export.impl.Node;

/**
 * Unit test for node.
 *
 * @author yng
 *
 */
public class NodeTest {

  /**
   * 4.
   */
  private static final int FOUR = 4;

  /**
   * Test node.
   */
  @Test
  public void testNode() {
    final Node<String> parent = new Node<>("Parent");

    final Node<String> child1 = new Node<>("child1");
    final Node<String> child2 = new Node<>("child2");
    final Node<String> child3 = new Node<>("child3");

    final List<Node<String>> childList = new ArrayList<>();
    childList.add(child2);
    childList.add(child3);

    parent.addChild("child String");

    Assert.assertEquals("Parent", parent.getData());

    parent.setData("Parent is root");
    Assert.assertEquals("Parent is root", parent.getData());

    parent.addChild(child1);
    parent.addChildren(childList);
    Assert.assertEquals(FOUR, parent.getChildren().size());

    final Node<String> child4 = new Node<>("child4");
    child4.setParent(parent);
    Assert.assertEquals(parent.getData(), child4.getParent().getData());

  }
}
