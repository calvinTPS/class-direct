package com.baesystems.ai.lr.cd.service.countryfiles.test;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.context.SecurityContextHolder;

import com.baesystems.ai.lr.cd.be.domain.dao.attachments.AttachmentRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.utils.FileTokenGenerator;
import com.baesystems.ai.lr.cd.service.countryfiles.CountryFileService;
import com.baesystems.ai.lr.cd.service.countryfiles.CountryFileServiceImpl;
import com.baesystems.ai.lr.dto.LinkResource;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for {@link com.baesystems.ai.lr.cd.service.countryfiles.CountryFileService}.
 *
 * @author Faizal Sidek
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({FileTokenGenerator.class})
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
public class CountryFileServiceTest {
  /**
   * The {@value #TOKEN} is to set token to download attachment
   * {@link SupplementaryInformationHDto#getToken()}.
   */
  private static final String TOKEN = "eyJ0eXBlIjoiQ1MxMCIsImNzMTBUb2tlbiI6";

  /**
   * The {@link CountryFileService} from Spring context.
   */
  @InjectMocks
  private CountryFileService countryFileService = new CountryFileServiceImpl();

  /**
   * The {@link AttachmentRetrofitService} from Spring context.
   */
  @Mock
  private AttachmentRetrofitService attachmentRetrofitService;

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public final void init() {
    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success scenario for {@link CountryFileService#getAttachmentsCountryFile(Long)}.
   *
   * @throws Exception if mast api call fails.
   */
  @Test
  public final void getAttachmentsCountryFile() throws Exception {
    PowerMockito.mockStatic(FileTokenGenerator.class, invocation -> {
      return TOKEN;
    });

    when(attachmentRetrofitService.getAttachmentsByFlagId(eq(1L))).thenReturn(Calls.response(getSupplementaryInfo()));

    final List<SupplementaryInformationHDto> supplemtaryInfo = countryFileService.getAttachmentsCountryFile(1L);
    Assert.assertNotNull(supplemtaryInfo);
    Assert.assertFalse(supplemtaryInfo.isEmpty());
    Assert.assertNotNull(supplemtaryInfo.get(0).getToken());
    Assert.assertEquals(supplemtaryInfo.get(0).getTitle(), "SINGAPORE (47)");
    Assert.assertEquals(supplemtaryInfo.get(1).getTitle(), "SINGAPORE (46)");
    Assert.assertEquals(supplemtaryInfo.get(2).getTitle(), "SINGAPORE (43)");
    Assert.assertEquals(supplemtaryInfo.get(3).getTitle(), "SINGAPORE (42)");
  }

  /**
   * Tests failure scenario for {@link CountryFileService#getAttachmentsCountryFile(Long)} when
   * external API return error.
   *
   * @throws Exception if mast api call fails.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getAttachmentsCountryFileFail() throws Exception {
    when(attachmentRetrofitService.getAttachmentsByFlagId(eq(1L)))
        .thenReturn(Calls.failure(new IOException("mock exception")));

    final List<SupplementaryInformationHDto> supplemtaryInfo = countryFileService.getAttachmentsCountryFile(1L);
    Assert.assertNull(supplemtaryInfo);
  }

  /**
   * mock list of SupplementaryInformationHDto.
   *
   * @return the list of SupplementaryInformationHDto.
   * @throws Exception if error occur in date conversion.
   */
  private List<SupplementaryInformationHDto> getSupplementaryInfo() throws Exception {
    List<SupplementaryInformationHDto> dtos = new ArrayList<>();
    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

    SupplementaryInformationHDto supplemtaryDto = new SupplementaryInformationHDto();
    supplemtaryDto.setAttachmentType(new LinkResource(1L));
    supplemtaryDto.setAttachmentCategory(new LinkResource(1L));
    supplemtaryDto.setAttachmentUrl("123");
    supplemtaryDto.setDocumentVersion(1L);
    supplemtaryDto.setToken(TOKEN);
    supplemtaryDto.setTitle("SINGAPORE (43)");
    supplemtaryDto.setUpdatedDate(dateformat.parse("22/07/2017"));
    supplemtaryDto.setUpdatedDateWithTime("2017-10-09T17:08:36Z");
    supplemtaryDto.setAttachmentDateWithTime("2016-12-22T15:53:14Z");


    // mock attachment type
    AttachmentTypeHDto attachmentType = new AttachmentTypeHDto();
    attachmentType.setId(1L);
    supplemtaryDto.setAttachmentTypeH(attachmentType);

    // mock attachment category
    AttachmentCategoryHDto category = new AttachmentCategoryHDto();
    category.setId(1L);
    supplemtaryDto.setAttachmentCategoryH(category);

    supplemtaryDto.setId(1L);
    dtos.add(supplemtaryDto);

    SupplementaryInformationHDto supplemtaryDto2 = new SupplementaryInformationHDto();
    supplemtaryDto2.setTitle("SINGAPORE (47)");
    supplemtaryDto2.setId(2L);
    supplemtaryDto2.setAttachmentUrl(null);
    supplemtaryDto2.setDocumentVersion(null);
    supplemtaryDto2.setUpdatedDateWithTime("2017-09-01T17:08:06Z");
    supplemtaryDto2.setAttachmentDateWithTime("2017-01-31T12:14:19Z");
    supplemtaryDto.setUpdatedDate(dateformat.parse("17/08/2017"));
    AttachmentTypeHDto attachment2 = new AttachmentTypeHDto();
    attachment2.setId(1L);
    supplemtaryDto2.setAttachmentTypeH(attachment2);
    dtos.add(supplemtaryDto2);
    SupplementaryInformationHDto supplemtaryDto3 = new SupplementaryInformationHDto();
    supplemtaryDto3.setTitle("SINGAPORE (42)");
    supplemtaryDto3.setId(3L);
    supplemtaryDto3.setUpdatedDate(null);
    supplemtaryDto3.setUpdatedDateWithTime("2017-10-09T17:13:21Z");
    supplemtaryDto3.setAttachmentDateWithTime("2016-12-19T11:10:18Z");
    dtos.add(supplemtaryDto3);
    SupplementaryInformationHDto supplemtaryDto4 = new SupplementaryInformationHDto();
    supplemtaryDto4.setTitle("SINGAPORE (46)");
    supplemtaryDto4.setId(4L);
    supplemtaryDto4.setUpdatedDate(null);
    supplemtaryDto4.setUpdatedDateWithTime("2017-10-09T17:14:44Z");
    supplemtaryDto4.setAttachmentDateWithTime("2017-01-11T15:12:31Z");
    dtos.add(supplemtaryDto4);
    return dtos;
  }

}
