package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.MajorNCNHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.MajorNCNService;
import com.baesystems.ai.lr.cd.service.asset.impl.MajorNCNServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;

import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Provides unit test for {@link majorNCNService}.
 *
 * @author pfauchon
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Response.class, Resources.class, SecurityUtils.class})
@ContextConfiguration(classes = MajorNCNServiceTest.Config.class)
public class MajorNCNServiceTest {
  /**
   * The logger instantiated for {@link MajorNCNServiceTest}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MajorNCNServiceTest.class);

  /**
   * The {@link MajorNCNService} from from spring context.
   */
  @Autowired
  private MajorNCNService majorNCNService;

  /**
   * The {@link AssetRetrofitService} from spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;
  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;
  /**
   * The {@link UserProfileService} from Spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public final void init() {
    final CDAuthToken auth = new CDAuthToken("test");
    auth.setPrincipal("101");
    final UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    user.setClientCode("100100");
    user.setShipBuilderCode("100100");
    user.setFlagCode("ALA");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success scenario to get major non confirmative notes by asset id
   * {@link MajorNCNService#getMajorNCNs(Integer, Integer, Long).} for admin role.
   *
   * @throws ClassDirectException if mast API call fail.
   */
  @Test
  public final void testGetMajorNCNs() throws ClassDirectException {

    when(assetRetrofitService.getMajorNCNdto(anyLong())).thenReturn(Calls.response(mockMajorNCNs()));
    when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockUser());
    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);
    final PageResource<MajorNCNHDto> result = majorNCNService.getMajorNCNs(null, null, 1L);
    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getContent());
    Assert.assertFalse(result.getContent().isEmpty());
    assertEquals(2, result.getPagination().getTotalElements().longValue());
    assertEquals(mockMajorNCNs().size(), result.getContent().size());
    assertEquals(new GregorianCalendar(2017, 1, 1).getTime(), result.getContent().get(0).getDueDate());
    assertEquals("result", result.getContent().get(0).getDescription());
    assertEquals("100100", result.getContent().get(0).getDocCompanyImo());
    assertEquals("result2", result.getContent().get(1).getDescription());
    assertEquals("5678", result.getContent().get(1).getDocCompanyImo());
    Assert.assertNotNull(result.getContent().get(0).getDueDate());
    Assert.assertNotNull(result.getContent().get(1).getDueDate());

  }

  /**
   * Tests success scenario to get major non confirmative notes by asset id
   * {@link MajorNCNService#getMajorNCNs(Integer, Integer, Long).}.
   * <li>for client role one mncn with doc company imo match with user client code.</li>
   * <li>one mncn with doc company imo not match with client code</li>
   *
   * @throws ClassDirectException if mast API call fail.
   */
  @Test
  public final void testGetMajorNCNsClientRole() throws ClassDirectException {

    when(assetRetrofitService.getMajorNCNdto(anyLong())).thenReturn(Calls.response(mockMajorNCNs()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockClientUser());
    mockUserRoleClient();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(false);

    final PageResource<MajorNCNHDto> result = majorNCNService.getMajorNCNs(null, null, 1L);
    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getContent());
    Assert.assertFalse(result.getContent().isEmpty());
    assertEquals(1, result.getPagination().getTotalElements().longValue());
    assertEquals(1, result.getContent().size());
    assertEquals(new GregorianCalendar(2017, 1, 1).getTime(), result.getContent().get(0).getDueDate());
    assertEquals("result", result.getContent().get(0).getDescription());
    assertEquals("100100", result.getContent().get(0).getDocCompanyImo());

  }
  /**
   * Tests success scenario to get major non confirmative notes by asset id
   * {@link MajorNCNService#getMajorNCNs(Integer, Integer, Long).}.
   * <li>for flag role asset flag code match with user flag code.</li>
   * <li>for flag role asset flag code does not match with user flag code.</li>
   *
   * @throws ClassDirectException if mast API call fail.
   */
  @Test
  public final void testGetMajorNCNsFlagRole() throws ClassDirectException {

    when(assetRetrofitService.getMajorNCNdto(anyLong())).thenReturn(Calls.response(mockMajorNCNs()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    UserProfiles user = mockFlagUser();
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(user);
    mockUserRoleFlag();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(false);

    final PageResource<MajorNCNHDto> result = majorNCNService.getMajorNCNs(null, null, 1L);
    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getContent());
    Assert.assertFalse(result.getContent().isEmpty());
    assertEquals(2, result.getPagination().getTotalElements().longValue());
    assertEquals(2, result.getContent().size());
    assertEquals(new GregorianCalendar(2017, 1, 1).getTime(), result.getContent().get(0).getDueDate());
    assertEquals("result", result.getContent().get(0).getDescription());
    assertEquals("100100", result.getContent().get(0).getDocCompanyImo());
    assertEquals("result2", result.getContent().get(1).getDescription());
    assertEquals("5678", result.getContent().get(1).getDocCompanyImo());

    // user flag code does not match with asset flag state.
    user.setFlagCode("AAA");
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(user);
    final PageResource<MajorNCNHDto> result2 = majorNCNService.getMajorNCNs(null, null, 1L);
    Assert.assertNotNull(result2);
    Assert.assertNotNull(result2.getContent());
    Assert.assertTrue(result2.getContent().isEmpty());
  }

  /**
   * Tests success scenario to get major non confirmative notes by asset id
   * {@link MajorNCNService#getMajorNCNs(Integer, Integer, Long).} for ship builder role should not
   * return anything.
   *
   *
   * @throws ClassDirectException if mast API call fail.
   */
  @Test
  public final void testGetMajorNCNsBuilderRole() throws ClassDirectException {

    when(assetRetrofitService.getMajorNCNdto(anyLong())).thenReturn(Calls.response(mockMajorNCNs()));
    Mockito.when(assetService.assetByCode(Mockito.anyString(), Mockito.anyString())).thenReturn(mockAsset());
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockShipBuilderUser());
    mockUserRoleShip();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(false);

    final PageResource<MajorNCNHDto> result = majorNCNService.getMajorNCNs(null, null, 1L);
    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getContent());
    Assert.assertTrue(result.getContent().isEmpty());

  }

  /**
   * Tests failure scenario to get major non confirmative notes by asset id
   * {@link MajorNCNService#getMajorNCNs(Integer, Integer, Long).} when mast api call fails.
   *
   * @throws ClassDirectException if mast API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetMajorNCNsFail() throws ClassDirectException {
    when(assetRetrofitService.getMajorNCNdto(anyLong())).thenReturn(Calls.failure(new IOException()));

    majorNCNService.getMajorNCNs(null, null, 1L);

  }

  /**
   * Test success scenario get mncn by asset Id and mncn Id
   * {@link MajorNCNService#getMajorNCNByMNCNId(Long, long)} for admin role.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetMajorNCNsByAssetIdMncnId() throws Exception {

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    when(assetRetrofitService.getMajorNCNById(anyLong(), anyLong())).thenReturn(Calls.response(mockMajorNCNs().get(0)));
    mockUserRoles();
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(true);

    final MajorNCNHDto result = majorNCNService.getMajorNCNByMNCNId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getDocCompanyImo());
    Assert.assertEquals(result.getDocCompanyImo(), "100100");
  }

  /**
   * Test success scenario get mncn by asset Id and mncn Id
   * {@link MajorNCNService#getMajorNCNByMNCNId(Long, long)} for client role.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetMajorNCNsByIdClinetRole() throws Exception {

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    when(assetRetrofitService.getMajorNCNById(anyLong(), anyLong())).thenReturn(Calls.response(mockMajorNCNs().get(0)));
    mockUserRoleClient();
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockClientUser());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(false);

    final MajorNCNHDto result = majorNCNService.getMajorNCNByMNCNId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getDocCompanyImo());
    Assert.assertEquals(result.getDocCompanyImo(), "100100");
  }

  /**
   * Test success scenario get mncn by asset Id and mncn Id
   * {@link MajorNCNService#getMajorNCNByMNCNId(Long, long)} for flag role.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetMajorNCNsByIdFlagRole() throws Exception {

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    when(assetRetrofitService.getMajorNCNById(anyLong(), anyLong())).thenReturn(Calls.response(mockMajorNCNs().get(0)));
    mockUserRoleFlag();
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockFlagUser());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(false);

    final MajorNCNHDto result = majorNCNService.getMajorNCNByMNCNId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getDocCompanyImo());
    Assert.assertEquals(result.getDocCompanyImo(), "100100");
  }

  /**
   * Test success scenario get mncn by asset Id and mncn Id
   * {@link MajorNCNService#getMajorNCNByMNCNId(Long, long)} for ship builder role should not return
   * anything.
   *
   * @throws Exception if API call fail.
   */
  @Test
  public final void testGetMajorNCNsByIdShipRole() throws Exception {

    PowerMockito.mockStatic(Resources.class, invocation -> {
      LOGGER.debug("Skip invocation for inject.");
      return null;
    });
    when(assetRetrofitService.getMajorNCNById(anyLong(), anyLong())).thenReturn(Calls.response(mockMajorNCNs().get(0)));
    mockUserRoleShip();
    Mockito.when(userProfileService.getUser(Mockito.anyString())).thenReturn(mockShipBuilderUser());
    BDDMockito.given(SecurityUtils.isInternalUser()).willReturn(false);

    final MajorNCNHDto result = majorNCNService.getMajorNCNByMNCNId(1L, 1L);

    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getDocCompanyImo());
    Assert.assertEquals(result.getDocCompanyImo(), "100100");
  }

  /**
   * Test positive case scenario for get asset note by asset Id and asset note Id with null
   * condition.
   *
   * @throws Exception if API call fail.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetMajorNCNsByAssetIdMncnIdFail() throws Exception {

    when(assetRetrofitService.getMajorNCNById(anyLong(), anyLong())).thenReturn(Calls.failure(new IOException()));

    majorNCNService.getMajorNCNByMNCNId(1L, 1L);
  }

  /**
   * Returns list of Major NCNs.
   *
   * @return list.
   */
  private List<MajorNCNHDto> mockMajorNCNs() {
    List<MajorNCNHDto> mncns = new ArrayList<>();

    MajorNCNHDto mncn = new MajorNCNHDto();
    mncn.setId(1L);
    mncn.setActionTaken(new LinkResource(1L));
    mncn.setDocCompanyCountry("US");
    mncn.setDocCompanyImo("100100");
    mncn.setDocCompanyName("ABC");
    mncn.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    mncn.setJob(new LinkResource(1L));
    mncn.setStatus(new LinkResource(1L));
    mncn.setDescription("result");
    mncn.setObjectiveEvidence("evidence");
    mncns.add(mncn);

    MajorNCNHDto mncn2 = new MajorNCNHDto();
    mncn2.setId(2L);
    mncn2.setActionTaken(new LinkResource(2L));
    mncn2.setDocCompanyCountry("MY");
    mncn2.setDocCompanyImo("5678");
    mncn2.setDocCompanyName("ABC");
    mncn2.setDueDate(new GregorianCalendar(2017, 1, 2).getTime());
    mncn2.setJob(new LinkResource(2L));
    mncn2.setStatus(new LinkResource(2L));
    mncn2.setDescription("result2");
    mncn2.setObjectiveEvidence("evidence2");
    mncns.add(mncn2);

    return mncns;
  }

  /**
   * Returns mock asset {@link AssetHDto}.
   *
   * @return assetHdto.
   */
  public AssetHDto mockAsset() {
    // data1
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("v2 LADY K II");
    asset.setIsLead(false);
    asset.setLeadImo(String.valueOf(1L));
    asset.setVersionId(1L);
    asset.setImoNumber("100");
    FlagStateHDto flag = new FlagStateHDto();
    flag.setFlagCode("ALA");
    asset.setFlagStateDto(flag);
    return asset;
  }

  /**
   * Returns mock user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("LR_ADMIN");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }

  /**
   * Returns mock client user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockClientUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    user.setClientCode("100100");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("CLIENT");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }

  /**
   * Returns mock ship builder user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockShipBuilderUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    user.setShipBuilderCode("100100");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("SHIP_BUILDER");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }

  /**
   * Returns mock flag user {@link UserProfiles}.
   *
   * @return user.
   */
  protected UserProfiles mockFlagUser() {
    UserProfiles user = new UserProfiles();
    user.setUserId("101");
    user.setFlagCode("ALA");
    final Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("FLAG");
    roles.add(role);
    user.setRoles(roles);
    return user;
  }

  /**
   * mock user roles as Admin.
   */
  protected void mockUserRoles() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("LR_ADMIN");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * mock user role CLIENT.
   */
  protected void mockUserRoleClient() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("CLIENT");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * mock user role SHIP.
   */
  protected void mockUserRoleShip() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("SHIPBUILDER");
    roles.add("EOR");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * mock user role FLAG.
   */
  protected void mockUserRoleFlag() {
    PowerMockito.mockStatic(SecurityUtils.class);
    final List<String> roles = new ArrayList<>();
    roles.add("FLAG");
    BDDMockito.given(SecurityUtils.getRoles()).willReturn(roles);
  }

  /**
   * Provides Spring in-class configurations.
   *
   * @author syalavarthi.
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Bean
    @Override
    public MajorNCNService majorNCNService() {
      return new MajorNCNServiceImpl();
    }
  }
}
