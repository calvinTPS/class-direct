package com.baesystems.ai.lr.cd.be.controller.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.controller.UserProfileController;
import com.baesystems.ai.lr.cd.be.controller.UserProfileController.SitekitError;

/**
 * Unit test for UserProfileController.
 *
 * @author YWearn
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UserProfileControllerTest.Config.class)
public class UserProfileControllerTest {

  /**
   * Default redirect url after sitekit policy redirection.
   */
  @Value("${sitekit.default.redirect}")
  private String redirectUrl;

  /**
   * Redirect url if user cancel login.
   */
  @Value("${sitekit.cancel.login.redirect}")
  private String cancelLoginRedirectUrl;

  /**
   * Sitekit login policy name.
   */
  @Value("${sitekit.login.policyname}")
  private String sitekitLoginPolicyName;

  /**
   * The key in the referrer that indicates user cancellation if present.
   */
  @Value("${sitekit.cancel.key}")
  private String cancelKey;

  /**
   * Test UserProfileController.
   */
  @Autowired
  private UserProfileController controller;

  /**
   * Test success edit redirect to ClassDirect landing with delay.
   */
  @Test
  public final void testPerformSuccessEditRedirect() {
    Assert.assertEquals("redirect:" + redirectUrl, controller.performSuccessEditRedirect("DummyUserId"));
  }

  /**
   * Test success reset password redirect to ClassDirect landing.
   */
  @Test
  public final void testPerformSuccessResetPasswordRedirect() {
    Assert.assertEquals("redirect:" + redirectUrl, controller.performSuccessResetPasswordRedirect("DummyUserId"));
  }

  /**
   * Test error redirection scenarios: 1. Cancel login -> redirect to cdlive 2. Edit/reset error ->
   * redirect to ClassDirect landing (trigger relogin)
   */
  @Test
  public final void testPerformErrorRedirect() {
    final SitekitError error = new SitekitError();
    error.setErrorCode("ERRORCODE");
    error.setErrorMessage("ERRORMESSGE");

    // Test cancel login error
    error.setReferrer(cancelKey + " " + sitekitLoginPolicyName);
    Assert.assertEquals("redirect:" + cancelLoginRedirectUrl, controller.performErrorRedirect("DummyUserId", error));

    // Test cancel from non-login error
    error.setReferrer(sitekitLoginPolicyName);
    Assert.assertEquals("redirect:" + redirectUrl, controller.performErrorRedirect("DummyUserId", error));

    // Test other errors
    error.setReferrer("");
    Assert.assertEquals("redirect:" + redirectUrl, controller.performErrorRedirect("DummyUserId", error));

    // Test without user id, should not be error.
    error.setReferrer("");
    Assert.assertEquals("redirect:" + redirectUrl, controller.performErrorRedirect(null, error));
  }

  /**
   * Spring context.
   *
   * @author YWearn
   */
  @Configuration
  public static class Config {

    /**
     * Properties place holder.
     *
     * @return placeholder.
     */
    @Bean
    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
      final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
      configurer.setLocations(new ClassPathResource("sitekit-config.properties"));
      return configurer;
    }

    /**
     * Return cache.
     *
     * @return cache object.
     */
    @Bean
    public CacheManager cacheManager() {
      // Configure and Return implementation of Spring's CacheManager.
      SimpleCacheManager cacheManager = new SimpleCacheManager();
      List<Cache> caches = new ArrayList<Cache>();
      ConcurrentMapCache concurrentMapUserCache = new ConcurrentMapCache(CacheConstants.CACHE_USERS);
      ConcurrentMapCache concurrentMapSubFleetCache = new ConcurrentMapCache(CacheConstants.CACHE_SUBFLEET);
      caches.add(concurrentMapUserCache);
      caches.add(concurrentMapSubFleetCache);
      cacheManager.setCaches(caches);
      return cacheManager;
    }

    /**
     * Controller class bean.
     *
     * @return controller class.
     */
    @Bean
    public UserProfileController userProfileController() {
      return new UserProfileController();
    }

  }
}
