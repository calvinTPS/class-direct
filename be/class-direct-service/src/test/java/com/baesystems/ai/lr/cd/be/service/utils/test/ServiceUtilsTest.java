package com.baesystems.ai.lr.cd.be.service.utils.test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.service.utils.ServiceUtils;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;

/**
 * @author Stephen Chew
 */
public class ServiceUtilsTest {
  /**
   * Provide test to sort by due date.
   *
   * @throws Exception when error.
   */
  @Test
  public void codicilDefaultComparatorSortByDueDate() throws Exception {

    // arrange
    Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    Date yesterday = Date.from(LocalDate.now().minusDays(1L).atStartOfDay(ZoneId.systemDefault()).toInstant());

    ActionableItemDto ai1 = new ActionableItemDto();
    ai1.setId(1L);
    ai1.setDueDate(today);

    ActionableItemDto ai2 = new ActionableItemDto();
    ai2.setId(2L);
    ai2.setDueDate(yesterday);

    // action
    List<ActionableItemDto> list = Arrays.asList(ai1, ai2);
    list.sort(ServiceUtils.codicilDefaultComparator());

    // assert
    assertEquals((Long) 2L, list.get(0).getId());
    assertEquals((Long) 1L, list.get(1).getId());
  }

  /**
   * Provide test to sort by Id.
   *
   * @throws Exception when error.
   */
  @Test
  public void codicilDefaultComparatorSortById() throws Exception {

    // arrange
    Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    Date yesterday = Date.from(LocalDate.now().minusDays(1L).atStartOfDay(ZoneId.systemDefault()).toInstant());

    ActionableItemDto ai1 = new ActionableItemDto();
    ai1.setId(1L);
    ai1.setDueDate(today);
    ai1.setReferenceCode("ref51");

    ActionableItemDto ai2 = new ActionableItemDto();
    ai2.setId(2L);
    ai2.setDueDate(today);
    ai2.setReferenceCode("ref31");

    ActionableItemDto ai3 = new ActionableItemDto();
    ai3.setId(3L);
    ai3.setDueDate(yesterday);
    ai3.setReferenceCode("ref21");

    // action
    List<ActionableItemDto> list = Arrays.asList(ai1, ai2, ai3);
    list.sort(ServiceUtils.codicilDefaultComparator());

    // assert
    assertEquals((Long) 3L, list.get(0).getId());
    assertEquals((Long) 1L, list.get(2).getId());
    assertEquals((Long) 2L, list.get(1).getId());
  }

  /**
   * Test for null check of due date.
   *
   * @throws Exception when error.
   */
  @Test
  public void codicilDefaultComparatorSortByEmptyDate() throws Exception {

    // arrange
    Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());

    ActionableItemDto ai1 = new ActionableItemDto();
    ai1.setId(1L);
    ai1.setDueDate(today);

    ActionableItemDto ai2 = new ActionableItemDto();
    ai2.setId(2L);
    ai2.setDueDate(null);

    // action
    List<ActionableItemDto> list = Arrays.asList(ai1, ai2);
    list.sort(ServiceUtils.codicilDefaultComparator());

    // assert
    assertEquals((Long) 1L, list.get(0).getId());
    assertEquals((Long) 2L, list.get(1).getId());
  }

  /**
   * Tests success scenario to sort asset notes by imposed date and reference code.
   *
   * @throws Exception id any error in sorting.
   */
  @Test
  public void assetNotesComparatorTest() throws Exception {

    Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    Date yesterday = Date.from(LocalDate.now().minusDays(1L).atStartOfDay(ZoneId.systemDefault()).toInstant());

    AssetNoteHDto assetNotes1 = new AssetNoteHDto();
    assetNotes1.setId(1L);
    assetNotes1.setImposedDate(today);
    assetNotes1.setReferenceCode("333");

    AssetNoteHDto assetNotes2 = new AssetNoteHDto();
    assetNotes2.setId(2L);
    assetNotes2.setImposedDate(yesterday);
    assetNotes2.setReferenceCode("111");

    AssetNoteHDto assetNotes3 = new AssetNoteHDto();
    assetNotes3.setId(3L);
    assetNotes3.setImposedDate(today);
    assetNotes3.setReferenceCode("222");

    AssetNoteHDto assetNotes4 = new AssetNoteHDto();
    assetNotes4.setId(4L);
    assetNotes4.setReferenceCode("444");

    List<AssetNoteHDto> list = Arrays.asList(assetNotes1, assetNotes2, assetNotes3, assetNotes4);
    list.sort(ServiceUtils.assetNotesDefaultComparator());

    assertEquals((Long) 2L, list.get(0).getId());
    assertEquals((Long) 3L, list.get(1).getId());
    assertEquals((Long) 1L, list.get(2).getId());
    assertEquals((Long) 4L, list.get(3).getId());
  }
}
