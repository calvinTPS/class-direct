package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.mockito.Mockito.reset;

import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilStatusesHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.ActionableItemService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.CodicilService;
import com.baesystems.ai.lr.cd.service.asset.impl.CodicilServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.Response;

/**
 * Provides unit test methods for {@link CodicilService}.
 *
 * @author yng
 * @author sbollu
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Response.class, Resources.class, ExecutorService.class})
@ContextConfiguration(classes = CodicilServiceTest.Config.class)
public class CodicilServiceTest {
  /**
   * Logger initialization.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CodicilServiceTest.class);

  /**
   * The {@link ApplicationContext} from Spring context.
   *
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link CodicilService} from Spring context.
   */
  @Autowired
  private CodicilService codicilService;

  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private CoCService cocService;
  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private ActionableItemService actionableItemService;
  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * Initialize mock object.
   *
   */
  @Before
  public final void init() {
    reset(context.getBean(AssetRetrofitService.class));
  }

  /**
   * Tests success scenario for {@link CodicilService#getCodicil(Integer, Integer, Long)} without
   * pagination attributes.
   *
   * @throws Exception if API call or execution fails.
   */
  @Test
  public final void testGetCodicil() throws Exception {

    Mockito.when(cocService.getCoC(1L)).thenReturn(mockCoCData().getContent());
    Mockito.when(actionableItemService.getActionableItem(null, null, 1L)).thenReturn(mockActionableItemData());

    final PageResource<Object> result = codicilService.getCodicil(null, null, 1L);

    Assert.assertNotNull(result.getContent());
    Assert.assertEquals(7, result.getContent().size());

    Assert.assertTrue(result.getContent().get(0) instanceof ActionableItemHDto);
    Assert.assertTrue(result.getContent().get(7 - 1) instanceof CoCHDto);

    Assert.assertEquals(result.getPagination().getTotalElements(), Long.valueOf(7));
    Assert.assertEquals(result.getPagination().getTotalPages(), Integer.valueOf(0));
  }

  /**
   * Tests success scenario for {@link CodicilService#getCodicil(Integer, Integer, Long)} with
   * pagination attributes.
   *
   * @throws Exception if API call or execution fails.
   */
  @Test
  public final void testGetCodicilPageResource() throws Exception {
    Mockito.when(cocService.getCoC(1L)).thenReturn(mockCoCData().getContent());
    Mockito.when(actionableItemService.getActionableItem(null, null, 1L)).thenReturn(mockActionableItemData());

    final PageResource<Object> result = codicilService.getCodicil(1, 1, 1L);

    Assert.assertEquals(result.getContent().size(), 1);
  }

  /**
   * Tests success scenario for {@link CodicilService#getCodicilStatuses()} when mast api return
   * data.
   *
   * @throws Exception if mast API call or execution fails.
   */
  @Test
  public final void testGetCodicilStatuses() throws Exception {
    Mockito.when(assetReferenceService.getDefectStatuses()).thenReturn(mockDefectStatuses());
    Mockito.when(assetReferenceService.getCodicilStatuses()).thenReturn(mockCodicilStatuses());
    Mockito.when(assetReferenceService.getMncnStatusList()).thenReturn(mockMajorNcns());

    final CodicilStatusesHDto result = codicilService.getCodicilStatuses();

    Assert.assertEquals(result.getAnStatuses().size(), 1);
    Assert.assertEquals(result.getAiStatuses().size(), 1);
    Assert.assertEquals(result.getCocStatuses().size(), 1);
    Assert.assertEquals(result.getMncnStatuses().size(), 2);
    Assert.assertEquals(result.getSfStatuses().size(), 0);
    Assert.assertEquals(result.getDefectStatuses().size(), 3);
    Assert.assertEquals(result.getDeficiencyStatuses().size(), 3);
  }

  /**
   * Tests failure scenario for {@link CodicilService#getCodicilStatuses()} when mast api return
   * empty.
   *
   * @throws Exception if mast API call or execution fails.
   */
  @Test
  public final void testGetCodicilStatusesEmpty() throws Exception {
    Mockito.when(assetReferenceService.getDefectStatuses()).thenReturn(Collections.emptyList());
    Mockito.when(assetReferenceService.getCodicilStatuses()).thenReturn(Collections.emptyList());
    Mockito.when(assetReferenceService.getMncnStatusList()).thenReturn(Collections.emptyList());

    final CodicilStatusesHDto result = codicilService.getCodicilStatuses();

    Assert.assertNull(result.getAiStatuses());
    Assert.assertNull(result.getAnStatuses());
    Assert.assertNull(result.getCocStatuses());
    Assert.assertNull(result.getMncnStatuses());
    Assert.assertNull(result.getDeficiencyStatuses());
    Assert.assertNull(result.getDefectStatuses());
  }

  /**
   * Tests success scenario for {@link CodicilService#getCodicil(Integer, Integer, Long)}.Should
   * return empty list without any exception ,if COC and actionable Item list's are empty.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @SuppressWarnings("unchecked")
  @Test
  public final void testIfCodicilListIsEmpty() throws ClassDirectException {
    // test codicil for empty list
    final ActionableItemPageResource aiResource = new ActionableItemPageResource();
    aiResource.equals(new ArrayList<>());

    Mockito.when(cocService.getCoC(1L)).thenReturn(new ArrayList<>());
    Mockito.when(actionableItemService.getActionableItem(null, null, 1L)).thenReturn(aiResource);

    final PageResource<Object> result = codicilService.getCodicil(1, 1, 1L);

    Assert.assertTrue(result.getContent().isEmpty());
  }


  /**
   * Mocks CoC Data.
   *
   * @return mock COC.
   * @throws Exception if execution fails.
   */
  @SuppressWarnings("unchecked")
  private PageResource<CoCHDto> mockCoCData() throws Exception {
    final ObjectMapper mapper = new ObjectMapper();
    PageResource<CoCHDto> response = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/Codicil-CoCData.json"), writer,
        Charset.defaultCharset());

    response = mapper.readValue(writer.toString(), CoCPageResource.class);

    return response;
  }

  /**
   * Mocks Actionable Item Data.
   *
   * @return mock Items need to be take actions.
   * @throws Exception if execution fails.
   */
  @SuppressWarnings("unchecked")
  private PageResource<ActionableItemHDto> mockActionableItemData() throws Exception {
    final ObjectMapper mapper = new ObjectMapper();
    PageResource<ActionableItemHDto> response = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/Codicil-ActionableItemData.json"), writer,
        Charset.defaultCharset());

    response = mapper.readValue(writer.toString(), ActionableItemPageResource.class);

    return response;
  }

  /**
   * Mocks codicil statuses.
   *
   * @return mock object.
   */
  private List<DefectStatusHDto> mockDefectStatuses() {
    List<DefectStatusHDto> statuses = new ArrayList<>();

    DefectStatusHDto status1 = new DefectStatusHDto();
    status1.setId(1L);
    status1.setName("Open");
    status1.setDeleted(false);
    statuses.add(status1);

    DefectStatusHDto status2 = new DefectStatusHDto();
    status2.setId(2L);
    status2.setName("Closed");
    status2.setDeleted(false);
    statuses.add(status2);

    DefectStatusHDto status3 = new DefectStatusHDto();
    status3.setId(3L);
    status3.setName("Cancelled");
    status3.setDeleted(false);
    statuses.add(status3);
    return statuses;

  }


  /**
   * Mocks defct statuses.
   *
   * @return mock object.
   */
  private List<CodicilStatusHDto> mockCodicilStatuses() {
    List<CodicilStatusHDto> statuses = new ArrayList<>();

    // mock coc statuses
    CodicilStatusHDto cocStatus1 = new CodicilStatusHDto();
    cocStatus1.setId(1L);
    cocStatus1.setName("Open");
    cocStatus1.setDeleted(false);
    cocStatus1.setTypeId(CodicilType.COC.getId());
    statuses.add(cocStatus1);

    CodicilStatusHDto cocStatus2 = new CodicilStatusHDto();
    cocStatus2.setId(1L);
    cocStatus2.setName("Closed");
    cocStatus2.setDeleted(true);
    cocStatus2.setTypeId(CodicilType.COC.getId());
    statuses.add(cocStatus2);

    // mock ai statuses
    CodicilStatusHDto aiStatus1 = new CodicilStatusHDto();
    aiStatus1.setId(3L);
    aiStatus1.setName("Open");
    aiStatus1.setTypeId(CodicilType.AI.getId());
    aiStatus1.setDeleted(true);
    statuses.add(aiStatus1);

    CodicilStatusHDto aiStatus2 = new CodicilStatusHDto();
    aiStatus2.setId(4L);
    aiStatus2.setName("Closed");
    aiStatus2.setTypeId(CodicilType.AI.getId());
    aiStatus2.setDeleted(false);
    statuses.add(aiStatus2);

    // mock an statuses
    CodicilStatusHDto anStatus1 = new CodicilStatusHDto();
    anStatus1.setId(5L);
    anStatus1.setName("Open");
    anStatus1.setTypeId(CodicilType.AN.getId());
    anStatus1.setDeleted(false);
    statuses.add(anStatus1);

    CodicilStatusHDto anStatus2 = new CodicilStatusHDto();
    anStatus2.setId(6L);
    anStatus2.setName("Closed");
    anStatus2.setDeleted(true);
    anStatus2.setTypeId(CodicilType.AN.getId());
    statuses.add(anStatus2);

    return statuses;
  }

  /**
   * Mocks major non confirmitive notes list.
   *
   * @return list of major non confirmitive notes.
   */
  private List<MajorNCNStatusHDto> mockMajorNcns() {
    // create mock data
    final List<MajorNCNStatusHDto> mockData = new ArrayList<MajorNCNStatusHDto>();
    MajorNCNStatusHDto mncn1 = new MajorNCNStatusHDto();
    mncn1.setId(1L);
    mncn1.setName("Open");
    mncn1.setDeleted(false);
    mockData.add(mncn1);
    MajorNCNStatusHDto mncn2 = new MajorNCNStatusHDto();
    mncn2.setId(2L);
    mncn2.setName("Open - Downgraded");
    mncn2.setDeleted(false);
    mockData.add(mncn2);
    MajorNCNStatusHDto mncn3 = new MajorNCNStatusHDto();
    mncn3.setId(3L);
    mncn3.setName("Closed");
    mncn3.setDeleted(true);
    mockData.add(mncn3);
    return mockData;
  }

  /**
   * Provides spring in-class configurations.
   *
   * @author msidek
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Creates {@link CodicilService} bean.
     *
     * @return codicil service.
     *
     */
    @Override
    @Bean
    public CodicilService codicilService() {
      return new CodicilServiceImpl();
    }

  }
}
