package com.baesystems.ai.lr.cd.service.product.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.ProductReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.ProductCatalogueHDto;
import com.baesystems.ai.lr.cd.service.product.ProductReferenceService;
import com.baesystems.ai.lr.cd.service.product.impl.ProductReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;

import retrofit2.mock.Calls;

/**
 * Created by PFauchon on 24/2/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ProductReferenceServiceTest.Config.class)
@DirtiesContext
public class ProductReferenceServiceTest {
  /**
   * Invalid value to test null result.
   */
  private static final long NON_EXISTANT_PRODUCT_CATALOGUE = 3;
  /**
   * Product reference service.
   */
  @Autowired
  private ProductReferenceService productReferenceService;
  /**
   * Retrofit service.
   */
  @Autowired
  private ProductReferenceRetrofitService productReferenceRetrofitService;

  /**
   * Tests the service to get a list and a single product catalogue.
   */
  @Test
  public final void testProductCatalogue() {

    final ProductCatalogueHDto cat1 = new ProductCatalogueHDto();
    cat1.setId(1L);
    cat1.setDeleted(false);
    cat1.setName("Catalogue1");


    final ProductCatalogueHDto cat2 = new ProductCatalogueHDto();
    cat2.setId(0L);
    cat2.setDeleted(false);
    cat2.setName("Catalogue2");

    final ProductCatalogueHDto cat3 = new ProductCatalogueHDto();
    cat3.setId(2L);
    cat3.setDeleted(false);
    cat3.setName("Catalogue3");

    final List<ProductCatalogueHDto> catalogues = new ArrayList<>(Arrays.asList(cat1, cat2, cat3));

    Mockito.when(productReferenceRetrofitService.getProductCataloguesDto()).thenReturn(Calls.response(catalogues));

    final List<ProductCatalogueHDto> cataloguesFromService = productReferenceService.getProductCatalogues();


    Assert.assertEquals(catalogues, cataloguesFromService);
    Assert.assertEquals(cat2, productReferenceService.getProductCatalogue(cat2.getId()));
    Assert.assertNull(productReferenceService.getProductCatalogue(NON_EXISTANT_PRODUCT_CATALOGUE));
  }


  /**
   * Tests success scenario to get list of product groups
   * {@link ProductReferenceService#getProductGroups()()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testProductGroups() throws Exception {
    Mockito.when(productReferenceRetrofitService.getProductGroupsDto()).thenReturn(Calls.response(mockProductGroups()));

    final List<ProductGroupDto> productGroups = productReferenceService.getProductGroups();
    Assert.assertNotNull(productGroups);
    Assert.assertNotEquals(0, productGroups.size());
  }

  /**
   * Tests failure scenario to get list of product groups
   * {@link ProductReferenceService#getProductGroups()()}. when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testProductGroupsFail() throws Exception {
    Mockito.when(productReferenceRetrofitService.getProductGroupsDto())
        .thenReturn(Calls.failure(new IOException("Mock exception.")));

    final List<ProductGroupDto> productGroups = productReferenceService.getProductGroups();
    Assert.assertNotNull(productGroups);
    Assert.assertTrue(productGroups.isEmpty());
  }

  /**
   * Tests success scenario to get list of product groups
   * {@link ProductReferenceService#getProductGroups()()}.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testProductGroup() throws Exception {
    Mockito.when(productReferenceRetrofitService.getProductGroupsDto()).thenReturn(Calls.response(mockProductGroups()));

    final ProductGroupDto productGroup = productReferenceService.getProductGroup(1L);
    Assert.assertNotNull(productGroup);
    Assert.assertEquals(productGroup.getName(), "Group1");
  }

  /**
   * Tests failure scenario to get list of product groups
   * {@link ProductReferenceService#getProductGroups()()}. when mast api call fails.
   *
   * @throws Exception if mast api call fail.
   */
  @Test
  public void testProductGroupFail() throws Exception {
    Mockito.when(productReferenceRetrofitService.getProductGroupsDto())
        .thenReturn(Calls.failure(new IOException("Mock exception.")));

    final ProductGroupDto productGroup = productReferenceService.getProductGroup(4L);
    Assert.assertNull(productGroup);
  }

  /**
   * Mocks list product groups.
   *
   * @return the list of product groups.
   * @throws IOException if mast api call fails.
   */
  private List<ProductGroupDto> mockProductGroups() throws IOException {
    List<ProductGroupDto> dtos = new ArrayList<>();
    ProductGroupDto dto = new ProductGroupDto();
    dto.setId(1L);
    dto.setName("Group1");
    dto.setDisplayOrder(1);
    dtos.add(dto);

    ProductGroupDto dto2 = new ProductGroupDto();
    dto2.setId(2L);
    dto2.setName("Group2");
    dto2.setDisplayOrder(2);
    dtos.add(dto2);
    return dtos;
  }

  /**
   * Spring bean config.
   *
   * @author msidek
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {
    /**
     * Create service for {@link ProductReferenceService}.
     *
     * @return service.
     */
    @Override
    @Bean
    public ProductReferenceService productReferenceService() {
      return new ProductReferenceServiceImpl();
    }
  }
}
