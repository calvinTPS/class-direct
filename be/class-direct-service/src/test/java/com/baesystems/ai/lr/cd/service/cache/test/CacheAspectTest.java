package com.baesystems.ai.lr.cd.service.cache.test;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.ServiceReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.service.service.reference.impl.ServiceReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.asset.reference.impl.AssetReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.cache.CacheAspect;
import com.baesystems.ai.lr.cd.service.cache.CacheContainer;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;

import retrofit2.mock.Calls;

/**
 * Unit test for {@link CacheAspect}.
 *
 * @author MSidek
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CacheAspectTest.Config.class)
public class CacheAspectTest {

  /**
   * Injected asset reference service.
   *
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * Injected cache aspect.
   *
   */
  @Autowired
  private CacheAspect cacheAspect;

  /**
   * Injected asset reference delegate.
   *
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceDelegate;

  /**
   * Injected service reference delegate.
   *
   */
  @Autowired
  private ServiceReferenceRetrofitService serviceReferenceDelegate;

  /**
   * Injected service reference service.
   *
   */
  @Autowired
  private ServiceReferenceService serviceReferenceService;

  /**
   * Unit test on {@link CacheAspect#cacheResult(org.aspectj.lang.JoinPoint, Object)}.
   *
   * @throws Throwable when error.
   *
   */
  @Test
  public void invokeCache() throws Throwable {
    final Integer defaultCount = 15;
    final Long defaultId = 5L;

    Mockito.when(assetReferenceDelegate.getAssetCategories())
        .thenReturn(Calls.response(mockAssetCategories(defaultCount)));
    Mockito.when(assetReferenceDelegate.getAssetTypeDtos()).thenReturn(Calls.response(mockAssetTypes(defaultCount)));
    Mockito.when(serviceReferenceDelegate.getServiceCreditStatuses())
        .thenReturn(Calls.response(mockServiceCreditStatuses(defaultCount)));

    assetReferenceService.getAssetTypes();
    AssetTypeHDto assetType = assetReferenceService.getAssetType(2L);
    Assert.assertNotNull(assetType);

    AssetCategoryHDto assetCategory = assetReferenceService.getAssetCategory(1L);
    Assert.assertNotNull(assetCategory);

    serviceReferenceService.getServiceCreditStatuses();
    ServiceCreditStatusHDto creditStatus = serviceReferenceService.getServiceCreditStatus(defaultId);
    Assert.assertNotNull(creditStatus);

    Mockito.verify(cacheAspect, Mockito.atLeastOnce()).checkAndGetFromCache(Mockito.any());
  }

  /**
   * Mock service credit status.
   *
   * @param count object count.
   * @return list of mock objects.
   *
   */
  private List<ServiceCreditStatusHDto> mockServiceCreditStatuses(final Integer count) {
    List<ServiceCreditStatusHDto> creditStatuses = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      ServiceCreditStatusHDto creditStatus = new ServiceCreditStatusHDto();
      creditStatus.setId(Long.valueOf(i));
      creditStatus.setName(generateRandomString());
      creditStatuses.add(creditStatus);
    }

    return creditStatuses;
  }

  /**
   * Mock asset categories.
   *
   * @param count object counts.
   * @return list of mock objects.
   *
   */
  private List<AssetCategoryHDto> mockAssetCategories(final Integer count) {
    List<AssetCategoryHDto> assetCategories = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      AssetCategoryHDto assetCategory = new AssetCategoryHDto();
      assetCategory.setId(Long.valueOf(i));
      assetCategory.setName(generateRandomString());
      assetCategories.add(assetCategory);
    }

    return assetCategories;
  }

  /**
   * Mock asset types.
   *
   * @param count object counts.
   * @return list of asset types.
   *
   */
  private List<AssetTypeHDto> mockAssetTypes(final Integer count) {
    List<AssetTypeHDto> assetTypes = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      AssetTypeHDto assetType = new AssetTypeHDto();
      assetType.setId(Long.valueOf(i));
      assetType.setCategory(new LinkResource(Long.valueOf(i)));
      assetType.setName(generateRandomString());
      assetTypes.add(assetType);
    }

    return assetTypes;
  }

  /**
   * Length of string.
   *
   */
  private static final Integer CHARACTER_SIZE = 32;

  /**
   * Random salt.
   *
   */
  private static final Integer RANDOM_SALT = 130;

  /**
   * Generate random 32 chars string.
   *
   * @return string.
   *
   */
  private String generateRandomString() {
    final SecureRandom random = new SecureRandom();
    return new BigInteger(RANDOM_SALT, random).toString(CHARACTER_SIZE);
  }

  /**
   * Config inner class.
   *
   * @author MSidek
   *
   */
  @Configuration
  @EnableAspectJAutoProxy
  public static class Config extends BaseMockConfiguration {

    @Bean
    @Override
    public AssetReferenceService assetReferenceService() {
      return Mockito.spy(new AssetReferenceServiceImpl());
    }

    @Bean
    @Override
    public CacheContainer cache() {
      return new CacheContainer();
    }

    @Bean
    @Override
    public ServiceReferenceService serviceReferenceService() {
      return new ServiceReferenceServiceImpl();
    }

    /**
     * Initialize resources.
     *
     * @return resources.
     *
     */
    @Bean
    public Resources resources() {
      return new Resources();
    }
  }
}
