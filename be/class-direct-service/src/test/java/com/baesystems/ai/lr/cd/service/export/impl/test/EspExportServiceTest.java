package com.baesystems.ai.lr.cd.service.export.impl.test;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.certificate.CertificateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.jobs.JobRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetDetailsDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetModelHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RegistryInformationDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionPageResourceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ESPReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportContentHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCreditStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.AllowedWorkItemAttributeValueHDto;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.service.reference.ServiceReferenceService;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetNoteService;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.asset.reference.AssetReferenceService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.employee.reference.EmployeeReferenceService;
import com.baesystems.ai.lr.cd.service.export.EspExportService;
import com.baesystems.ai.lr.cd.service.export.impl.EspExportServiceImpl;
import com.baesystems.ai.lr.cd.service.job.reference.JobReferenceService;
import com.baesystems.ai.lr.cd.service.port.PortService;
import com.baesystems.ai.lr.cd.service.task.reference.TaskReferenceService;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.certificates.CertificateDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FaultCategoryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.CodicilCategory;
import com.baesystems.ai.lr.enums.CodicilStatus;
import com.baesystems.ai.lr.enums.ReportType;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.Response;
import retrofit2.mock.Calls;


/**
 * Provides unit test methods for EspExportService.
 *
 * @author yng
 * @author SBollu.
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = EspExportServiceTest.Config.class)
@PrepareForTest({Response.class, Resources.class, SecurityUtils.class})
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class EspExportServiceTest {

  /**
   * The {@link EspExportService} from Spring context.
   */
  @Autowired
  private EspExportService espExportService;

  /**
   * The {@link AmazonStorageService} from Spring context.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * The {@link CoCService} from Spring context.
   */
  @Autowired
  private CoCService cocService;

  /**
   * The {@link AssetNoteService} from Spring context.
   */
  @Autowired
  private AssetNoteService assetNoteService;

  /**
   * The {@link AssetRetrofitService} from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * The {@link JobRetrofitService} from Spring context.
   */
  @Autowired
  private JobRetrofitService jobRetrofitService;

  /**
   * The {@link AssetService} from Spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link CertificateRetrofitService} from Spring context.
   */
  @Autowired
  private CertificateRetrofitService certificateRetrofitService;

  /**
   * The {@link AssetReferenceService} from Spring context.
   */
  @Autowired
  private AssetReferenceService assetReferenceService;

  /**
   * The {@link ServiceReferenceService} from Spring context.
   */
  @Autowired
  private ServiceReferenceService serviceService;

  /**
   * The {@link JobReferenceService} from Spring context.
   */
  @Autowired
  private JobReferenceService jobReferenceService;

  /**
   * The {@link EmployeeReferenceService} from Spring context.
   */
  @Autowired
  private EmployeeReferenceService employeeReferenceService;

  /**
   * The {@link TaskReferenceService} from Spring context.
   */
  @Autowired
  private TaskReferenceService taskReferenceService;
  /**
   * The {@link PortService} from Spring context.
   */
  @Autowired
  private PortService portService;
  /**
   * The directory test package path.
   */
  private static final String PATH = "data/FARAppendix2TestPackage/";

  /**
   * Provides prerequisite mocking of user authentication and services.
   */
  @Before
  public void resetMock() {
    Mockito.reset(amazonStorageService, cocService, assetNoteService, assetRetrofitService, certificateRetrofitService,
        serviceService, jobRetrofitService, jobReferenceService, assetService);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Tests success scenario for {@link EspExportService#downloadPdf(ESPReportExportDto)}.
   *
   * @throws ClassDirectException if any parsing or mapping fails.
   * @throws IOException if any API fails.
   */
  @Test
  public final void testEspPdfGeneration() throws ClassDirectException, IOException {
    // create query
    final ESPReportExportDto query = new ESPReportExportDto();
    query.setJobId(1L);

    // mock data
    Mockito.when(assetRetrofitService.getAssetItemsModel(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockAssetModel()));
    when(taskReferenceService.getAllowedAttributeValues()).thenReturn(mockMaxAllowedAttributeValue());
    Mockito.when(jobRetrofitService.getReportsByJobId(Matchers.anyLong()))
        .thenReturn(Calls.response(mockFsrReportsData()));
    Mockito.when(serviceService.getServiceCatalogues()).thenReturn(mockServiceCatalogueDataForAppendix2());
    Mockito.when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobData()));
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAssetData());
    Mockito.when(assetService.getAssetDetailsByAssetId(Matchers.anyLong(), Matchers.anyString(), Matchers.anyBoolean()))
        .thenReturn(mockAssetDetailData());
    Mockito.when(assetService.getServices(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class),
        Matchers.anyLong(), Matchers.any(ServiceQueryDto.class))).thenReturn(mockServiceData());

    Mockito.when(cocService.getCoC(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(mockCocData());
    Mockito.when(assetRetrofitService.getAssetDefectDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockDefectData()));

    Mockito.when(
        assetNoteService.getAssetNote(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(mockAssetNoteData());

    Mockito.when(assetReferenceService.getActionTakenList()).thenReturn(mockActionTakenData());

    Mockito.when(certificateRetrofitService.certificateActionQuery(Matchers.anyLong()))
        .thenReturn(Calls.response(mockCertificateActionData()));

    Mockito.when(jobRetrofitService.getSurveysByJobIdAndReportId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockFARDataForAppendix2()));
    Mockito.when(employeeReferenceService.getEmployee(Matchers.anyLong())).thenReturn(mockEmployeeData());
    Mockito.when(jobReferenceService.getResolutionStatuses()).thenReturn(mockResolutionStatusData());
    Mockito.when(serviceService.getServiceCreditStatuses()).thenReturn(mockServiceCreditStatusData());
    Mockito.when(portService.getPort(Matchers.anyLong())).thenReturn(mockRegistryData());

    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final StringResponse response = espExportService.downloadPdf(query);

    Assert.assertNotNull(response);
  }

  /**
   * Tests success scenario for {@link EspExportService#downloadPdf(ESPReportExportDto)} with out
   * appendix.
   *
   * @throws ClassDirectException if any parsing or mapping fails.
   * @throws IOException if any API fails.
   */
  @Test
  public final void testEspPdfGenerationWithEmptyServices() throws ClassDirectException, IOException {
    // create query
    final ESPReportExportDto query = new ESPReportExportDto();
    query.setJobId(1L);

    // mock data
    Mockito.when(assetRetrofitService.getAssetItemsModel(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockAssetModel()));
    Mockito.when(jobRetrofitService.getReportsByJobId(Matchers.anyLong()))
        .thenReturn(Calls.response(mockFsrReportsData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobData()));
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(mockAssetData());
    Mockito.when(assetService.getAssetDetailsByAssetId(Matchers.anyLong(), Matchers.anyString(), Matchers.anyBoolean()))
        .thenReturn(mockAssetDetailData());
    Mockito.when(assetService.getServices(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class),
        Matchers.anyLong(), Matchers.any(ServiceQueryDto.class))).thenReturn(mockServiceData());

    Mockito.when(cocService.getCoC(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(mockCocData());
    Mockito.when(assetRetrofitService.getAssetDefectDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockDefectData()));

    Mockito.when(
        assetNoteService.getAssetNote(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(mockAssetNoteData());

    Mockito.when(assetReferenceService.getActionTakenList()).thenReturn(mockActionTakenData());

    Mockito.when(certificateRetrofitService.certificateActionQuery(Matchers.anyLong()))
        .thenReturn(Calls.response(mockCertificateActionData()));

    Mockito.when(jobRetrofitService.getSurveysByJobIdAndReportId(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(mockReportData()));
    Mockito.when(employeeReferenceService.getEmployee(Matchers.anyLong())).thenReturn(mockEmployeeData());
    Mockito.when(serviceService.getServiceCatalogues()).thenReturn(mockServiceCatalogueData());
    Mockito.when(jobReferenceService.getResolutionStatuses()).thenReturn(mockResolutionStatusData());
    Mockito.when(serviceService.getServiceCreditStatuses()).thenReturn(mockServiceCreditStatusData());
    Mockito.when(portService.getPort(Matchers.anyLong())).thenReturn(mockRegistryData());

    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final StringResponse response = espExportService.downloadPdf(query);

    Assert.assertNotNull(response);
  }

  /**
   * Tests failure scenario for {@link EspExportService#downloadPdf(ESPReportExportDto)} fail to get
   * asset model.
   *
   * @throws ClassDirectException if any parsing or mapping fails or mast api call fails.
   * @throws IOException if any API fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testEspPdfFail() throws ClassDirectException, IOException {
    // create query
    final ESPReportExportDto query = new ESPReportExportDto();
    query.setJobId(1L);

    // mock data
    Mockito.when(assetRetrofitService.getAssetItemsModel(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.failure(new IOException()));
    Mockito.when(jobRetrofitService.getReportsByJobId(Matchers.anyLong()))
        .thenReturn(Calls.response(mockFsrReportsData()));
    Mockito.when(jobRetrofitService.getJobsByJobId(Matchers.anyLong())).thenReturn(Calls.response(mockJobData()));

    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    espExportService.downloadPdf(query);
  }

  /**
   * Tests success scenario for {@link EspExportServiceImpl#extractCocInfo(Long, Long)}.
   *
   * @throws ClassDirectException if API call fails..
   */
  @Test
  public final void testExtractCocInfo() throws ClassDirectException {

    // coc test data
    final CoCHDto coc1 = new CoCHDto();
    coc1.setId(1L);
    coc1.setRaisedOnJob(new LinkResource(1L));
    coc1.setDefect(new LinkResource(1L));

    final CoCHDto coc2 = new CoCHDto();
    coc2.setId(2L);
    coc2.setClosedOnJob(new LinkResource(1L));
    coc2.setDefect(new LinkResource(1L));

    final CoCHDto coc3 = new CoCHDto();
    coc3.setId(3L);
    coc3.setClosedOnJob(new LinkResource(1L));
    coc3.setDefect(new LinkResource(1L));

    final CoCHDto coc4 = new CoCHDto();
    coc4.setId(4L);

    final List<CoCHDto> cocs = new ArrayList<>();
    cocs.add(coc1);
    cocs.add(coc2);
    cocs.add(coc3);
    cocs.add(coc4);

    final PageResource<CoCHDto> cocResource = PaginationUtil.paginate(cocs, null, null);

    // defect test data
    final DefectHDto defect1 = new DefectHDto();
    defect1.setId(1L);
    defect1.setCategory(new FaultCategoryDto());
    defect1.getCategory().setId(1L);

    final DefectHDto defect2 = new DefectHDto();
    defect2.setId(2L);
    defect2.setCategory(new FaultCategoryDto());
    defect2.getCategory().setId(2L);

    final DefectHDto defect3 = new DefectHDto();
    defect3.setId(3L);
    defect3.setCategory(new FaultCategoryDto());

    // Mock data
    Mockito.when(cocService.getCoC(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(cocResource);
    Mockito.when(assetRetrofitService.getAssetDefectDto(Matchers.anyLong(), Matchers.anyLong()))
        .thenReturn(Calls.response(defect1), Calls.response(defect2), Calls.response(defect3));

    final EspExportServiceImpl impl = (EspExportServiceImpl) espExportService;

    final List<CoCHDto> result = impl.extractCocInfo(1L, 1L);

    Assert.assertEquals(1, result.size());
    Assert.assertEquals(coc2.getId(), result.get(0).getId());
  }

  /**
   * Tests success scenario for {@link EspExportServiceImpl#extractAssetNoteInfo(Long, Long)}.
   *
   * @throws ClassDirectException if API call fails.
   */
  @Test
  public final void testExtractAssetNoteInfo() throws ClassDirectException {

    // asset note test data
    final AssetNoteHDto note1 = new AssetNoteHDto();
    note1.setId(1L);
    note1.setRaisedOnJob(new LinkResource(1L));

    final AssetNoteHDto note2 = new AssetNoteHDto();
    note2.setId(2L);
    note2.setClosedOnJob(new LinkResource(1L));
    note2.setCategory(new LinkResource(CodicilCategory.AN_CLASS.getValue()));
    note2.setConfidentialityType(new LinkResource(2L));

    final AssetNoteHDto note3 = new AssetNoteHDto();
    note3.setId(3L);

    final AssetNoteHDto note4 = new AssetNoteHDto();
    note4.setId(4L);
    note4.setClosedOnJob(new LinkResource(1L));
    note4.setCategory(new LinkResource(CodicilCategory.AN_CLASS.getValue()));
    note4.setConfidentialityType(new LinkResource(1L));

    final List<AssetNoteHDto> notes = new ArrayList<>();
    notes.add(note1);
    notes.add(note2);
    notes.add(note3);
    notes.add(note4);

    final PageResource<AssetNoteHDto> resource = PaginationUtil.paginate(notes, null, null);

    // Mock data
    Mockito.when(
        assetNoteService.getAssetNote(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(resource);

    final EspExportServiceImpl impl = (EspExportServiceImpl) espExportService;

    final List<AssetNoteHDto> result = impl.extractAssetNoteInfo(1L, 1L);

    Assert.assertEquals(1, result.size());
  }

  /**
   * Returns mock asset data.
   *
   * @return asset data.
   */
  public AssetHDto mockAssetData() {
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("My Ship");
    asset.setLeadImo("1000019");

    return asset;
  }

  /**
   * Returns mock job data.
   *
   * @return job data.
   */
  public JobHDto mockJobData() {
    final JobHDto job = new JobHDto();
    job.setId(1L);
    job.setFirstVisitDate(new GregorianCalendar(2017, 1, 1).getTime());
    job.setLastVisitDate(new GregorianCalendar(2016, 1, 1).getTime());
    job.setAsset(new LinkVersionedResource(1L, 1L));
    job.setJobCategory(new LinkResource(1L));
    LocationHDto locationDto = new LocationHDto();
    locationDto.setName("malaysia");
    job.setLocationDto(locationDto);

    job.setEmployees(new ArrayList<EmployeeLinkDto>());

    final EmployeeLinkDto employee1 = new EmployeeLinkDto();
    employee1.setId(1L);
    employee1.setEmployeeRole(new LinkResource(11L));
    employee1.setLrEmployee(new LinkResource(1L));

    final EmployeeLinkDto employee2 = new EmployeeLinkDto();
    employee2.setId(2L);
    employee2.setEmployeeRole(new LinkResource(6L));
    employee1.setLrEmployee(new LinkResource(2L));

    final EmployeeLinkDto employee3 = new EmployeeLinkDto();
    employee3.setId(3L);
    employee3.setEmployeeRole(new LinkResource(6L));
    employee1.setLrEmployee(new LinkResource(3L));

    job.getEmployees().add(employee1);
    job.getEmployees().add(employee2);
    job.getEmployees().add(employee3);

    return job;
  }

  /**
   * Returns mock asset detail data.
   *
   * @return asset detail data.
   */
  public AssetDetailsDto mockAssetDetailData() {
    final AssetDetailsDto assetDetail = new AssetDetailsDto();
    assetDetail.setRegistryInformation(new RegistryInformationDto());
    assetDetail.getRegistryInformation().setPortOfRegistry("Port Side");

    return assetDetail;
  }

  /**
   * Returns list of mock services.
   *
   * @return list of mock services.
   */
  public PageResource<ScheduledServiceHDto> mockServiceData() {

    final ServiceCatalogueHDto catalogue1 = new ServiceCatalogueHDto();
    catalogue1.setId(1L);
    catalogue1.setCode("AAA");
    catalogue1.setEspIndicator(Boolean.TRUE);
    catalogue1.setDisplayOrder(2);

    final ServiceCatalogueHDto catalogue2 = new ServiceCatalogueHDto();
    catalogue2.setId(2L);
    catalogue2.setCode("AAAA");
    catalogue2.setEspIndicator(Boolean.TRUE);
    catalogue1.setDisplayOrder(1);

    final ServiceCatalogueHDto catalogue3 = new ServiceCatalogueHDto();
    catalogue3.setId(3L);
    catalogue3.setCode("AAAA");
    catalogue3.setEspIndicator(Boolean.TRUE);
    catalogue3.setDisplayOrder(1);

    final ServiceCatalogueHDto catalogue4 = new ServiceCatalogueHDto();
    catalogue4.setId(2L);
    catalogue4.setCode("AAAA");
    catalogue4.setEspIndicator(Boolean.FALSE);
    catalogue4.setDisplayOrder(1);

    final ServiceCatalogueHDto catalogue5 = new ServiceCatalogueHDto();
    catalogue5.setId(2L);
    catalogue5.setCode("AAAA");
    catalogue5.setEspIndicator(Boolean.TRUE);
    catalogue5.setDisplayOrder(1);

    final List<ScheduledServiceHDto> serviceList = new ArrayList<>();

    final ScheduledServiceHDto service1 = new ScheduledServiceHDto();
    service1.setId(3726960L);
    service1.setServiceCatalogueH(catalogue1);

    final ScheduledServiceHDto service2 = new ScheduledServiceHDto();
    service2.setId(3726961L);
    service2.setServiceCatalogueH(catalogue2);

    final ScheduledServiceHDto service3 = new ScheduledServiceHDto();
    service2.setId(3726962L);
    service2.setServiceCatalogueH(catalogue2);

    final ScheduledServiceHDto service4 = new ScheduledServiceHDto();
    service2.setId(3726993L);
    service2.setServiceCatalogueH(catalogue2);

    final ScheduledServiceHDto service5 = new ScheduledServiceHDto();
    service2.setId(3726996L);
    service2.setServiceCatalogueH(catalogue2);

    serviceList.add(service1);
    serviceList.add(service2);
    serviceList.add(service3);
    serviceList.add(service4);
    serviceList.add(service5);

    final PageResource<ScheduledServiceHDto> scResource = PaginationUtil.paginate(serviceList, null, null);

    return scResource;
  }

  /**
   * Returns mock COC data.
   *
   * @return COC data.
   */
  public PageResource<CoCHDto> mockCocData() {
    final CoCHDto coc1 = new CoCHDto();
    coc1.setId(1L);
    coc1.setTitle("First coc");
    coc1.setRaisedOnJob(new LinkResource(1L));
    coc1.setDefect(new LinkResource(1L));
    coc1.setActionTaken(new LinkResource(1L));
    coc1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());

    final CoCHDto coc2 = new CoCHDto();
    coc2.setId(1L);
    coc2.setTitle("Second coc");
    coc2.setRaisedOnJob(new LinkResource(1L));
    coc2.setDefect(new LinkResource(1L));
    coc2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());

    final List<CoCHDto> cocs = new ArrayList<>();
    cocs.add(coc1);
    cocs.add(coc2);

    final PageResource<CoCHDto> cocResource = PaginationUtil.paginate(cocs, null, null);

    return cocResource;
  }

  /**
   * Returns mock defect data.
   *
   * @return defect data.
   */
  public DefectHDto mockDefectData() {
    final DefectHDto defect = new DefectHDto();
    defect.setId(1L);
    defect.setCategory(new FaultCategoryDto());
    defect.getCategory().setId(2L);

    return defect;
  }

  /**
   * Returns mock asset note data.
   *
   * @return asset note data.
   */
  public final PageResource<AssetNoteHDto> mockAssetNoteData() {
    final AssetNoteHDto note1 = new AssetNoteHDto();
    note1.setId(1L);
    note1.setTitle("First an");
    note1.setReferenceCode("AAA");
    note1.setDescription("Asset Note1 Decsription");
    note1.setRaisedOnJob(new LinkResource(1L));
    note1.setActionTaken(new LinkResource(1L));
    note1.setCategory(new LinkResource(CodicilCategory.AN_CLASS.getValue()));
    note1.setConfidentialityType(new LinkResource(2L));

    final AssetNoteHDto note2 = new AssetNoteHDto();
    note2.setId(2L);
    note2.setTitle("Second an");
    note2.setReferenceCode("BBB");
    note2.setDescription("Asset Note2 Decsription");
    note2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    note2.setClosedOnJob(new LinkResource(1L));
    note2.setCategory(new LinkResource(CodicilCategory.AN_CLASS.getValue()));
    note2.setConfidentialityType(new LinkResource(2L));

    final List<AssetNoteHDto> notes = new ArrayList<>();
    notes.add(note1);
    notes.add(note2);

    final PageResource<AssetNoteHDto> resource = PaginationUtil.paginate(notes, null, null);

    return resource;
  }

  /**
   * Returns mock action taken data.
   *
   * @return action taken data.
   */
  public List<ActionTakenHDto> mockActionTakenData() {
    final ActionTakenHDto data1 = new ActionTakenHDto();
    data1.setId(1L);
    data1.setName("Raised");

    final ActionTakenHDto data2 = new ActionTakenHDto();
    data2.setId(2L);
    data2.setName("Closed");

    final ActionTakenHDto data3 = new ActionTakenHDto();
    data3.setId(3L);
    data3.setName("Amended");

    final List<ActionTakenHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);

    return data;
  }

  /**
   * Returns mock certificate data.
   *
   * @return certificate data.
   */
  public CertificateActionPageResourceHDto mockCertificateActionData() {
    final CertificateActionHDto data = new CertificateActionHDto();
    data.setId(1L);
    data.setActionTaken(new LinkResource(2L));

    final List<CertificateActionHDto> certActions = new ArrayList<>();
    certActions.add(data);

    final CertificateActionPageResourceHDto resource = new CertificateActionPageResourceHDto();
    resource.setContent(certActions);

    return resource;
  }

  /**
   * Returns mock report content data.
   *
   * @return content data.
   */
  public final ReportDto mockReportData() {
    final ReportContentHDto content = new ReportContentHDto();
    content.setWipCocs(mockCocListData());
    content.setWipActionableItems(mockAiData());
    content.setWipAssetNotes(mockAnData());
    // content.setSurveys(mockSurveyData());
    content.setTasks(mockTaskData());
    content.setStatutoryFindings(mockStatutoryFindingData());
    content.setCertificates(mockCertificateData());
    content.setCertificateActions(mockCertificateActionListData());

    final ReportDto report = new ReportDto();
    report.setId(1L);
    report.setClassRecommendation("Class Recommendation Text. Class Recommendation Text. Class Recommendation Text.");
    report.setIssueDate(new GregorianCalendar(2017, 1, 1).getTime());
    report.setIssuedBy(new LinkResource(1L));
    report.setReportType(new LinkResource(1L));
    report.setReportVersion(1L);
    report.setAuthorisedBy(new LinkResource(1L));
    report.setAuthorisationDate(new GregorianCalendar(2017, 1, 1).getTime());

    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode contentNode = mapper.convertValue(content, JsonNode.class);

    report.setContent(contentNode);

    return report;
  }

  /**
   * Creates FAR content mock data for appendix 2 testing.
   *
   * @return the mocked FAR content data.
   * @throws IOException if json read or write fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws JsonParseException if json parsing fail.
   */
  public final ReportDto mockFARDataForAppendix2() throws JsonParseException, JsonMappingException, IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    ReportContentHDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/EspAppendixData.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), ReportContentHDto.class);

    final ReportDto report = new ReportDto();
    report.setId(1L);
    StringBuffer classReco = new StringBuffer();
    classReco.append("Class Recommendation Text");
    classReco.append("\r\n");
    classReco.append("Class Recommendation Text");
    classReco.append("\r\n");
    classReco.append("Class Recommendation Text");
    report.setClassRecommendation(classReco.toString());
    report.setIssueDate(new GregorianCalendar(2017, 1, 1).getTime());
    report.setIssuedBy(new LinkResource(1L));
    report.setReportType(new LinkResource(2L));
    report.setReportVersion(1L);
    report.setAuthorisedBy(new LinkResource(1L));
    report.setAuthorisationDate(new GregorianCalendar(2017, 1, 1).getTime());

    final JsonNode contentNode = mapper.convertValue(content, JsonNode.class);

    report.setContent(contentNode);

    return report;
  }

  /**
   * Returns mock list of reports has FAR.
   *
   * @return list of reports.
   */
  public final List<ReportHDto> mockReportsData() {
    List<ReportHDto> reports = new ArrayList<>();
    ReportHDto report3 = new ReportHDto();
    report3.setId(3L);
    report3.setReportType(new LinkResource(ReportType.FAR.value()));
    report3.setReportVersion(3L);
    reports.add(report3);
    ReportHDto report = new ReportHDto();
    report.setId(1L);
    report.setReportType(new LinkResource(ReportType.FAR.value()));
    report.setReportVersion(1L);
    reports.add(report);
    ReportHDto report2 = new ReportHDto();
    report2.setId(2L);
    report2.setReportType(new LinkResource(ReportType.FAR.value()));
    report2.setReportVersion(2L);
    reports.add(report2);

    return reports;
  }

  /**
   * Returns mock list of reports has FSR.
   *
   * @return list of reports.
   */
  public final List<ReportHDto> mockFsrReportsData() {
    List<ReportHDto> reports = new ArrayList<>();
    ReportHDto report3 = new ReportHDto();
    report3.setId(3L);
    report3.setReportType(new LinkResource(ReportType.FSR.value()));
    report3.setReportVersion(3L);
    reports.add(report3);
    ReportHDto report = new ReportHDto();
    report.setId(1L);
    report.setReportType(new LinkResource(ReportType.FSR.value()));
    report.setReportVersion(1L);
    reports.add(report);
    ReportHDto report2 = new ReportHDto();
    report2.setId(2L);
    report2.setReportType(new LinkResource(ReportType.FSR.value()));
    report2.setReportVersion(2L);
    reports.add(report2);

    return reports;
  }

  /**
   * Returns mock CoC list.
   *
   * @return list of mock COCs.
   */
  public final List<CoCDto> mockCocListData() {
    final CoCDto coc1 = new CoCDto();
    coc1.setId(1L);
    coc1.setTitle("First coc");
    coc1.setActionTaken(new LinkResource(1L));
    coc1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    coc1.setRaisedOnJob(new LinkResource(1L));
    coc1.setDeleted(false);

    final CoCDto coc2 = new CoCDto();
    coc2.setId(2L);
    coc2.setTitle("Second coc");
    coc2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    coc2.setClosedOnJob(new LinkResource(1L));
    coc2.setDeleted(true);

    final List<CoCDto> cocs = new ArrayList<>();
    cocs.add(coc1);
    cocs.add(coc2);


    return cocs;
  }

  /**
   * Returns mocks actionableItem list.
   *
   * @return mock actionableItem list.
   */
  public final List<ActionableItemDto> mockAiData() {
    final List<LinkResource> services = new ArrayList<>();

    final LinkResource service1 = new LinkResource();
    service1.setId(1L);

    final LinkResource service2 = new LinkResource();
    service2.setId(2L);

    services.add(service1);
    services.add(service2);

    final ActionableItemDto item1 = new ActionableItemDto();
    item1.setId(1L);
    item1.setTitle("First ai");
    item1.setDescription("Asset Item1 Description");
    item1.setReferenceCode("AAA");
    item1.setStatus(new LinkResource(CodicilStatus.AI_CHANGE_RECOMMENDED.getValue()));
    item1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    item1.setConfidentialityType(new LinkResource(1L));
    item1.setStatus(new LinkResource(1L));
    item1.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    item1.setRaisedOnJob(new LinkResource(1L));
    item1.setActionTaken(new LinkResource(1L));
    item1.setScheduledServices(services);

    final ActionableItemDto item2 = new ActionableItemDto();
    item2.setId(2L);
    item2.setTitle("Second ai");
    item2.setDescription("Asset Item 2 Description");
    item2.setReferenceCode("BBB");
    item2.setStatus(new LinkResource(CodicilStatus.AI_OPEN.getValue()));
    item2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    item2.setConfidentialityType(new LinkResource(1L));
    item2.setStatus(new LinkResource(1L));
    item2.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    item2.setClosedOnJob(new LinkResource(1L));

    final List<ActionableItemDto> items = new ArrayList<>();
    items.add(item1);
    items.add(item2);

    return items;
  }


  /**
   * Returns mock asset note list.
   *
   * @return list of mock asset note.
   */
  public final List<AssetNoteDto> mockAnData() {
    final AssetNoteDto note1 = new AssetNoteDto();
    note1.setId(1L);
    note1.setTitle("First an");
    note1.setDescription("Asset Note1 Description");
    note1.setReferenceCode("AAA");
    note1.setStatus(new LinkResource(CodicilStatus.AI_CHANGE_RECOMMENDED.getValue()));
    note1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    note1.setConfidentialityType(new LinkResource(1L));
    note1.setStatus(new LinkResource(1L));
    note1.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    note1.setRaisedOnJob(new LinkResource(1L));
    note1.setActionTaken(new LinkResource(1L));

    final AssetNoteDto note2 = new AssetNoteDto();
    note2.setId(2L);
    note2.setTitle("Second an");
    note2.setDescription("Asset Note2 Description");
    note2.setReferenceCode("BBB");
    note2.setStatus(new LinkResource(CodicilStatus.AN_OPEN.getValue()));
    note2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    note2.setConfidentialityType(new LinkResource(1L));
    note2.setStatus(new LinkResource(1L));
    note2.setActionTakenDate(new GregorianCalendar(2016, 1, 1).getTime());
    note2.setClosedOnJob(new LinkResource(1L));

    final List<AssetNoteDto> notes = new ArrayList<>();
    notes.add(note1);
    notes.add(note2);

    return notes;
  }


  /**
   * Returns mock statutory finding.
   *
   * @return statutory finding list.
   */
  public final List<StatutoryFindingDto> mockStatutoryFindingData() {
    final StatutoryFindingDto data1 = new StatutoryFindingDto();
    data1.setId(1L);
    data1.setTitle("SF 1");
    data1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    data1.setActionTaken(new LinkResource(1L));

    final StatutoryFindingDto data2 = new StatutoryFindingDto();
    data2.setId(1L);
    data2.setTitle("SF 2");
    data2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());

    final List<StatutoryFindingDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);

    return data;
  }

  /**
   * Returns mock certificates.
   *
   * @return mock certificates.
   */
  public final List<CertificateDto> mockCertificateData() {
    final CertificateDto data1 = new CertificateDto();
    data1.setId(1L);
    data1.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data1.setCertificateNumber("cert 1");
    data1.setService(new LinkResource(1L));

    final CertificateDto data2 = new CertificateDto();
    data2.setId(2L);
    data2.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data2.setCertificateNumber("cert 2");

    final CertificateDto data3 = new CertificateDto();
    data3.setId(3L);
    data3.setExpiryDate(new GregorianCalendar(2017, 1, 1).getTime());
    data3.setCertificateNumber("cert 3");
    data3.setService(new LinkResource(2L));

    final List<CertificateDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);

    return data;
  }


  /**
   * Returns mock task data.
   *
   * @return task list data.
   */
  public final List<WorkItemDto> mockTaskData() {
    final WorkItemDto task1 = new WorkItemDto();
    task1.setId(1L);
    task1.setTaskNumber("112-111-111");
    task1.setName("Task 1");
    SurveyDto survey = new SurveyDto();
    survey.setId(1L);
    task1.setSurvey(survey);
    task1.setWorkItemType(new LinkResource(1L));
    task1.setServiceCode("DS");
    task1.setLongDescription("long description");
    List<WorkItemAttributeDto> attributes = new ArrayList<>();
    WorkItemAttributeDto workItem = new WorkItemAttributeDto();
    workItem.setAttributeDataType(new LinkResource(5L));
    WorkItemConditionalAttributeDto workItemConditionalAttributeDto = new WorkItemConditionalAttributeDto();
    workItemConditionalAttributeDto.setReadingCategory(new LinkResource(3L));
    workItemConditionalAttributeDto.setId(1L);
    workItemConditionalAttributeDto.setComparativeAttributeCode("test");
    workItem.setId(1L);
    workItem.setWorkItemConditionalAttribute(workItemConditionalAttributeDto);
    WorkItemAttributeDto workItem2 = new WorkItemAttributeDto();
    workItem2.setId(2L);
    workItem2.setInternalId("2");
    workItem2.setDescription("test");
    workItem2.setParent(new LinkResource(1L));
    attributes.add(workItem);
    attributes.add(workItem2);
    task1.setAttributes(attributes);

    final WorkItemDto task2 = new WorkItemDto();
    task2.setId(2L);
    task2.setTaskNumber("222-222-222");
    task2.setName("Task 2");
    task2.setResolutionStatus(new LinkResource(2L));
    task2.setWorkItemType(new LinkResource(1L));
    SurveyDto survey2 = new SurveyDto();
    survey2.setId(1L);
    task2.setSurvey(survey2);

    final WorkItemDto task3 = new WorkItemDto();
    task3.setId(3L);
    task3.setTaskNumber("334-333-333");
    task3.setName("Task 3");
    task3.setResolutionStatus(new LinkResource(3L));
    SurveyDto survey3 = new SurveyDto();
    survey3.setId(2L);
    task3.setSurvey(survey3);
    task3.setWorkItemType(new LinkResource(1L));
    task3.setServiceCode("TS");
    task3.setLongDescription("long description");
    List<WorkItemAttributeDto> attributes1 = new ArrayList<>();
    WorkItemAttributeDto workItem1 = new WorkItemAttributeDto();
    workItem1.setAttributeDataType(new LinkResource(5L));
    WorkItemConditionalAttributeDto workItemConditionalAttributeDto1 = new WorkItemConditionalAttributeDto();
    workItemConditionalAttributeDto1.setReadingCategory(new LinkResource(2L));
    workItemConditionalAttributeDto1.setId(2L);
    workItemConditionalAttributeDto1.setReferenceCode("test");
    workItem1.setId(4L);
    workItem1.setWorkItemConditionalAttribute(workItemConditionalAttributeDto1);
    attributes1.add(workItem1);
    task3.setAttributes(attributes1);

    WorkItemAttributeDto workItem3 = new WorkItemAttributeDto();
    WorkItemConditionalAttributeDto workItemConditionalAttributeDto2 = new WorkItemConditionalAttributeDto();
    workItemConditionalAttributeDto2.setReadingCategory(new LinkResource(4L));
    workItemConditionalAttributeDto2.setId(4L);
    workItem3.setId(5L);
    workItem3.setWorkItemConditionalAttribute(workItemConditionalAttributeDto1);
    attributes1.add(workItem3);
    task3.setAttributes(attributes1);

    final WorkItemDto task4 = new WorkItemDto();
    task4.setId(4L);
    task4.setTaskNumber("444-444-444");
    task4.setName("Task 4");
    task4.setResolutionStatus(new LinkResource(4L));
    SurveyDto survey4 = new SurveyDto();
    survey4.setId(3L);
    task4.setSurvey(survey4);
    task4.setWorkItemType(new LinkResource(1L));
    task4.setServiceCode("CC");
    task4.setLongDescription("long description");

    final WorkItemDto task5 = new WorkItemDto();
    task5.setId(5L);
    task5.setTaskNumber("555-555-555");
    task5.setName("Task 5");
    SurveyDto survey5 = new SurveyDto();
    survey5.setId(5L);
    task5.setSurvey(survey5);
    task5.setWorkItemType(new LinkResource(1L));
    task5.setServiceCode("BB");
    task5.setLongDescription("long description 5");

    final WorkItemDto task6 = new WorkItemDto();
    task6.setId(6L);
    task6.setTaskNumber("666-666-666");
    task6.setDescription("Task 6");
    task6.setResolutionStatus(new LinkResource(6L));
    task6.setWorkItemType(new LinkResource(2L));
    SurveyDto survey6 = new SurveyDto();
    survey6.setId(4L);
    task6.setSurvey(survey6);
    task6.setLongDescription("long description 6");

    final WorkItemDto task7 = new WorkItemDto();
    task7.setId(7L);
    task7.setTaskNumber("777-777-777");
    task7.setDescription("Task 7");
    SurveyDto survey7 = new SurveyDto();
    survey7.setId(5L);
    task7.setSurvey(survey7);
    task7.setWorkItemType(new LinkResource(2L));
    task7.setLongDescription("long description 7");
    task7.setServiceCode("BB");
    task7.setReferenceCode("Task7 ref");

    final List<WorkItemDto> tasks = new ArrayList<>();
    tasks.add(task1);
    tasks.add(task2);
    tasks.add(task3);
    tasks.add(task4);
    tasks.add(task5);
    tasks.add(task6);
    tasks.add(task7);

    return tasks;
  }

  /**
   * Returns mock survey data.
   *
   * @return survey data.
   */
  public List<SurveyDto> mockSurveyData() {
    final SurveyDto survey1 = new SurveyDto();
    survey1.setId(1L);
    survey1.setName("First Survey");
    survey1.setSurveyStatus(new LinkResource(1L));
    survey1.setServiceCatalogue(new LinkResource(1L));
    survey1.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());

    final SurveyDto survey2 = new SurveyDto();
    survey2.setId(2L);
    survey2.setName("Second Survey");
    survey2.setSurveyStatus(new LinkResource(2L));
    survey2.setServiceCatalogue(new LinkResource(2L));
    survey2.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());

    final SurveyDto survey3 = new SurveyDto();
    survey3.setId(3L);
    survey3.setName("Third Survey");
    survey3.setSurveyStatus(new LinkResource(2L));
    survey3.setServiceCatalogue(new LinkResource(3L));
    survey3.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());

    final SurveyDto survey4 = new SurveyDto();
    survey4.setId(4L);
    survey4.setName("Fourth Survey");
    survey4.setDateOfCrediting(new GregorianCalendar(2017, 1, 1).getTime());
    survey4.setSurveyStatus(new LinkResource(2L));
    survey4.setServiceCatalogue(new LinkResource(2L));
    survey4.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());

    final SurveyDto survey5 = new SurveyDto();
    survey5.setId(5L);
    survey5.setName("Fifth Survey");
    survey5.setDateOfCrediting(new GregorianCalendar(2017, 1, 1).getTime());
    survey5.setSurveyStatus(new LinkResource(2L));
    survey5.setServiceCatalogue(new LinkResource(3L));
    survey5.setPostponementDate(new GregorianCalendar(2017, 1, 1).getTime());



    final List<SurveyDto> surveys = new ArrayList<>();
    surveys.add(survey1);
    surveys.add(survey2);
    surveys.add(survey3);
    surveys.add(survey4);
    surveys.add(survey5);

    return surveys;
  }


  /**
   * Returns mock certificate action data.
   *
   * @return certificate action data.
   */
  public List<CertificateActionDto> mockCertificateActionListData() {
    final CertificateActionDto data1 = new CertificateActionDto();
    data1.setId(1L);
    data1.setCertificate(new LinkVersionedResource(1L, 1L));
    data1.setActionTaken(new LinkResource(1L));

    final CertificateActionDto data2 = new CertificateActionDto();
    data2.setId(2L);
    data2.setCertificate(new LinkVersionedResource(2L, 1L));
    data2.setActionTaken(new LinkResource(2L));

    final List<CertificateActionDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);

    return data;
  }

  /**
   * Returns mock serviceCatalogue data.
   *
   * @return serviceCatalogue data.
   */
  public final List<ServiceCatalogueHDto> mockServiceCatalogueData() {
    final ServiceCatalogueHDto data1 = new ServiceCatalogueHDto();
    data1.setId(1L);
    data1.setCode("AAA");
    data1.setProductCatalogue(new ProductCatalogueDto());
    data1.getProductCatalogue().setId(1L);
    data1.getProductCatalogue().setName("Product one");
    data1.setEspIndicator(Boolean.TRUE);
    data1.setDisplayOrder(2);

    final ServiceCatalogueHDto data2 = new ServiceCatalogueHDto();
    data2.setId(2L);
    data2.setCode("BBB");
    data2.setProductCatalogue(new ProductCatalogueDto());
    data2.getProductCatalogue().setId(2L);
    data2.getProductCatalogue().setName("Product two");
    data2.setEspIndicator(Boolean.FALSE);
    data2.setDisplayOrder(1);

    final ServiceCatalogueHDto data3 = new ServiceCatalogueHDto();
    data3.setId(3L);
    data3.setCode("CCC");
    data3.setProductCatalogue(new ProductCatalogueDto());
    data3.getProductCatalogue().setId(2L);
    data3.getProductCatalogue().setName("Product two");
    data3.setEspIndicator(Boolean.TRUE);
    data3.setDisplayOrder(3);

    final List<ServiceCatalogueHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);

    return data;

  }

  /**
   * Returns mock resolution status data.
   *
   * @return resolution status data.
   */
  public List<JobResolutionStatusHDto> mockResolutionStatusData() {
    final JobResolutionStatusHDto data1 = new JobResolutionStatusHDto();
    data1.setId(1L);
    data1.setName("Completed");
    data1.setCode("A");

    final JobResolutionStatusHDto data2 = new JobResolutionStatusHDto();
    data2.setId(2L);
    data2.setName("Confirmatory Check");
    data2.setCode("B");

    final JobResolutionStatusHDto data3 = new JobResolutionStatusHDto();
    data3.setId(3L);
    data3.setName("Waived");
    data3.setCode("C");

    final JobResolutionStatusHDto data4 = new JobResolutionStatusHDto();
    data4.setId(4L);
    data4.setName("Not applicable");
    data4.setCode("D");

    final JobResolutionStatusHDto data5 = new JobResolutionStatusHDto();
    data5.setId(6L);
    data5.setName("Postponed");
    data5.setCode("E");

    final List<JobResolutionStatusHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);
    data.add(data4);
    data.add(data5);

    return data;
  }

  /**
   * Returns service credit status mock data.
   *
   * @return the service credit status mock data.
   */
  public List<ServiceCreditStatusHDto> mockServiceCreditStatusData() {
    final ServiceCreditStatusHDto data1 = new ServiceCreditStatusHDto();
    data1.setId(1L);
    data1.setName("Not started");
    data1.setCode("N");

    final ServiceCreditStatusHDto data2 = new ServiceCreditStatusHDto();
    data2.setId(2L);
    data2.setName("Part held");
    data2.setCode("P");

    final ServiceCreditStatusHDto data3 = new ServiceCreditStatusHDto();
    data3.setId(3L);
    data3.setName("Posteponed");
    data3.setCode("P");

    final ServiceCreditStatusHDto data4 = new ServiceCreditStatusHDto();
    data4.setId(4L);
    data4.setName("Complete");
    data4.setCode("C");

    final ServiceCreditStatusHDto data5 = new ServiceCreditStatusHDto();
    data5.setId(5L);
    data5.setName("Finished");
    data5.setCode("F");

    final List<ServiceCreditStatusHDto> data = new ArrayList<>();
    data.add(data1);
    data.add(data2);
    data.add(data3);
    data.add(data4);
    data.add(data5);

    return data;
  }

  /**
   * Returns employee mock data.
   *
   * @return the employee mock data.
   */
  public LrEmployeeDto mockEmployeeData() {
    final LrEmployeeDto employee = new LrEmployeeDto();
    employee.setId(1L);
    employee.setFirstName("John");
    employee.setLastName("Smith");
    employee.setName("John Smith");
    employee.setLegalEntity("Lloyd’s Register Group Limited");

    return employee;
  }

  /**
   * Returns real reference data mocking for appendix 2 testing.
   *
   * @return the list of service catalogue mock reference data.
   * @throws IOException if json read or write fail.
   */
  public final List<ServiceCatalogueHDto> mockServiceCatalogueDataForAppendix2() throws IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    List<ServiceCatalogueHDto> content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(
        getClass().getClassLoader().getResourceAsStream("data/FARAppendix2TestPackage/serviceCatalogueRefData.json"),
        writer, Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), new TypeReference<List<ServiceCatalogueHDto>>() {

    });

    return content;
  }

  /**
   * Returns real reference data mocking for appendix 2 testing.
   *
   * @return the list of allowed attribute value mock reference data.
   * @throws IOException if json read or write fail.
   */
  public final List<AllowedWorkItemAttributeValueHDto> mockMaxAllowedAttributeValue() throws IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    List<AllowedWorkItemAttributeValueHDto> content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream("data/AllowedAttributeValues.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), new TypeReference<List<AllowedWorkItemAttributeValueHDto>>() {

    });

    return content;
  }

  /**
   * Returns port of registry mock data.
   *
   * @return the port of registry mock data.
   */
  public PortOfRegistryDto mockRegistryData() {
    final PortOfRegistryDto data = new PortOfRegistryDto();
    data.setId(1L);
    data.setName("London");
    return data;
  }

  /**
   * Creates Asset model content mock data for appendix 2 testing.
   *
   * @return the mocked FAR content data.
   * @throws IOException if json read or write fail.
   * @throws JsonMappingException if json to object mapping.
   * @throws JsonParseException if json parsing fail.
   */
  private AssetModelHDto mockAssetModel() throws JsonParseException, JsonMappingException, IOException {
    // read generated content file
    final ObjectMapper mapper = new ObjectMapper();
    AssetModelHDto content = null;

    final StringWriter writer = new StringWriter();
    IOUtils.copy(getClass().getClassLoader().getResourceAsStream(PATH + "assetModel.json"), writer,
        Charset.defaultCharset());

    content = mapper.readValue(writer.toString(), AssetModelHDto.class);

    return content;
  }

  /**
   *
   * Provides Spring in-class configurations.
   *
   * @author yng
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * un-mock esp export service.
     *
     * @return espExportService.
     */
    @Override
    @Bean
    public EspExportService espExportService() {
      return new EspExportServiceImpl();
    }
  }

}
