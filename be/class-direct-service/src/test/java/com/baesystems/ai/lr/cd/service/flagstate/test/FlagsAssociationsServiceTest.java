package com.baesystems.ai.lr.cd.service.flagstate.test;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FlagsAssociationsDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.FlagsAssociations;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.flagstate.FlagsAssociationsService;
import com.baesystems.ai.lr.cd.service.flagstate.impl.FlagsAssociationsServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the service class {@link FlagsAssociationsService}.
 * Coverage: Class: 100%, Method: 100%, Line: 93%.
 *
 * @author fwijaya on 20/4/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class FlagsAssociationsServiceTest {

  /**
   * The flags associations dao.
   */
  @Mock
  private FlagsAssociationsDao flagsAssociationsDao;
  /**
   * The flags association service.
   */
  @InjectMocks
  private FlagsAssociationsService flagsAssociationsService = new FlagsAssociationsServiceImpl();
  /**
   * Just a constant.
   */
  private static final int ONE = 1;
  /**
   * Flag code 1.
   */
  private static final String FLAG_CODE_1 = "ABC";
  /**
   * Secondary flag code 1.
   */
  private static final String SECONDARY_FLAG_CODE_1 = "DEF";
  /**
   * A random flag code.
   */
  private static final String RANDOM_FLAG_CODE = "You are ugly!";
  /**
   * FlagsAssociations instance.
   */
  private FlagsAssociations flagsAssociations;

  /**
   * Setup test instance before unit tests are run.
   */
  @Before
  public final void setup() {
    flagsAssociations = new FlagsAssociations();
    flagsAssociations.setFlagCode(FLAG_CODE_1);
    flagsAssociations.setSecondaryFlagCode(SECONDARY_FLAG_CODE_1);
  }

  /**
   * Tests removing associations.
   *
   * @throws ClassDirectException if unchecked exception occurs.
   */
  @Test
  public final void testRemoveAssociation() throws ClassDirectException {
    when(flagsAssociationsDao.removeAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1)).thenReturn(ONE);
    final int deletedAssociations = flagsAssociationsService.removeAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    Assert.assertEquals(deletedAssociations, ONE);
  }

  /**
   * Tests adding associations.
   *
   * @throws ClassDirectException if unchecked exception occurs.
   */
  @Test
  public final void testAddAssociation() throws ClassDirectException {
    doNothing().when(flagsAssociationsDao).addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    flagsAssociationsService.addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
    verify(flagsAssociationsDao, times(ONE)).addAssociation(FLAG_CODE_1, SECONDARY_FLAG_CODE_1);
  }

  /**
   * Tests adding a flag code which is too long.
   *
   * @throws ClassDirectException if unchecked exception occurs.
   */
  @Test(expected = Exception.class)
  public final void testAddLongFlagCode() throws ClassDirectException {
    doThrow(Exception.class).when(flagsAssociationsDao).addAssociation(RANDOM_FLAG_CODE, SECONDARY_FLAG_CODE_1);
    flagsAssociationsService.addAssociation(RANDOM_FLAG_CODE, SECONDARY_FLAG_CODE_1);
  }

  /**
   * Tests getting associated flags.
   *
   * @throws ClassDirectException if unchecked exception occurs.
   */
  @Test
  public final void testGetAssociatedFlags() throws ClassDirectException {
    final List<FlagsAssociations> result = new ArrayList<>();
    result.add(flagsAssociations);
    when(flagsAssociationsDao.getFlagAssocisationsForFlagCode(FLAG_CODE_1)).thenReturn(result);
    when(flagsAssociationsDao.getAssociatedFlag(flagsAssociations, FLAG_CODE_1)).thenReturn(SECONDARY_FLAG_CODE_1);
    final List<String> associatedFlags = flagsAssociationsService.getAssociatedFlags(FLAG_CODE_1);
    Assert.assertNotNull(associatedFlags);
    Assert.assertEquals(associatedFlags.get(0), SECONDARY_FLAG_CODE_1);
  }
}
