package com.baesystems.ai.lr.cd.service.certificate.impl.test;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.certificate.CertificateRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateActionPageResourceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificatePageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.CertificateTemplateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.certificate.ProductCatalogueHDto;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.certificate.CertificateReferenceService;
import com.baesystems.ai.lr.cd.service.certificate.CertificateService;
import com.baesystems.ai.lr.cd.service.certificate.impl.CertificateServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.query.CertificateQueryDto;
import com.baesystems.ai.lr.dto.references.CertificateActionTakenDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

import retrofit2.Response;
import retrofit2.mock.Calls;

/**
 * Provides unit test methods for {@link CertificateService}.
 *
 * @author fwijaya on 25/1/2017.
 * @author sbollu
 *
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({Response.class, Resources.class, SecurityUtils.class})
@ContextConfiguration(classes = CertificateServiceImplTest.Config.class)
public class CertificateServiceImplTest {

  /**
   * The {@link ApplicationContext} from Spring context.
   */
  @Autowired
  private ApplicationContext context;

  /**
   * The {@link CertificateService} from Spring context.
   */
  @Autowired
  private CertificateService certService;

  /**
   * The {@link CertificateRetrofitService} from Spring context.
   */
  @Autowired
  private CertificateRetrofitService certificateRetrofitService;

  /**
   * The {@link CertificateReferenceService} from Spring context.
   */
  @Autowired
  private CertificateReferenceService certificateReferenceService;

  /**
   * Certificate action date 1.
   */
  private static final Date ACTION_DATE_1 = Date.from(Instant.EPOCH);

  /**
   * Certificate action date 2.
   */
  private static final Date ACTION_DATE_2 = Date.from(Instant.now());

  /**
   * Sets job id.
   */
  @SuppressWarnings("serial")
  private CertificateQueryHDto getAllQuery = new CertificateQueryHDto() {
    {
      super.setJobId(15L);
    }
  };

  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer, Integer, String, String, CertificateQueryHDto)}.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void shouldReturnCertificateWithLatestActionDate() throws ClassDirectException {
    mockAllCertificates(1L, 2L);
    CertificatePageResource pageResource =
        certService.getCertificateQueryPageResource(null, null, null, null, getAllQuery);
    pageResource.getContent().stream().forEach(certificate -> {
      Assert.assertEquals(certificate.getEndorsedDate(), ACTION_DATE_2);
    });
  }

  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer,Integer, <br>
   * String, String, CertificateQueryHDto)},if certificate page resource is null.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void shouldReturnZeroCertification() throws ClassDirectException {
    mockZeroCertificates();
    CertificatePageResource pageResource =
        certService.getCertificateQueryPageResource(null, null, null, null, getAllQuery);
    Assert.assertTrue(pageResource.getContent().isEmpty());
  }

  /**
   * Tests failure scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer, Integer, String, String, CertificateQueryHDto)}.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void failGetCertificate() throws ClassDirectException {
    mockAllCertificates(1L, 2L);
    certService.getCertificateQueryPageResource(0, 16, "", "ASC", getAllQuery);
  }

  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer,Integer, <br>
   * String, String, CertificateQueryHDto)},should return certificates has invalid certificate
   * action.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void shouldNotMatchEndorsedCertificateAction() throws ClassDirectException {
    mockInvalidCertificateAction(1L);
    CertificatePageResource pageResource =
        certService.getCertificateQueryPageResource(null, null, null, null, getAllQuery);
    Assert.assertNull(pageResource.getContent().get(0).getEndorsedDate());
  }

  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer,Integer,<br>
   * String, String, CertificateQueryHDto)},should return certificates successfully, if any
   * exception happens while calling certificate action API.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void failGetCertificateAction() throws ClassDirectException {
    mockExceptionCertificateAction(1L);
    CertificatePageResource pageResource =
        certService.getCertificateQueryPageResource(null, null, null, null, getAllQuery);
    Assert.assertNull(pageResource.getContent().get(0).getEndorsedDate());
  }

  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer,Integer, <br>
   * String, String, CertificateQueryHDto)},should return certificates has empty certificate action.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void failGetEmptyCertificateAction() throws ClassDirectException {
    mockEmptyCertificateAction(1L);
    CertificatePageResource pageResource =
        certService.getCertificateQueryPageResource(null, null, null, null, getAllQuery);
    Assert.assertNull(pageResource.getContent().get(0).getEndorsedDate());
  }

  /**
   * Returns certificate page resource object has null content(size of certificate list is zero).
   */
  private void mockZeroCertificates() {
    final CertificateRetrofitService certRetrofitService = context.getBean(CertificateRetrofitService.class);
    final CertificatePageResource pageResource = new CertificatePageResource();
    pageResource.setContent(null);

    Mockito.when(certRetrofitService.certificateQuery(getAllQuery, null, null, null, null))
        .thenReturn(Calls.response(pageResource));
  }

  /**
   * Returns mock certificate has invalid certificateAction.
   *
   * @param certId the unique identifier of certificate.
   */
  private void mockInvalidCertificateAction(final Long certId) {
    final CertificateRetrofitService certRetrofitService = context.getBean(CertificateRetrofitService.class);
    final CertificatePageResource pageResource = new CertificatePageResource();
    final List<CertificateHDto> list = new ArrayList<>();
    final CertificateHDto dto = new CertificateHDto();
    dto.setId(certId);
    list.add(dto);
    pageResource.setContent(list);

    CertificateActionPageResourceHDto actionPageResourceHDto = new CertificateActionPageResourceHDto();
    List<CertificateActionHDto> actionHDtos = new ArrayList<>();
    // Date is null
    actionHDtos.add(createCertificateActionHDto(null, "Endorsed"));
    // Action taken is null
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_1, null));
    // Action taken is empty
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_1, ""));
    actionPageResourceHDto.setContent(actionHDtos);

    Mockito.when(certRetrofitService.certificateQuery(getAllQuery, null, null, null, null))
        .thenReturn(Calls.response(pageResource));

    Mockito.when(certRetrofitService.certificateActionQuery(certId)).thenReturn(Calls.response(actionPageResourceHDto));
  }

  /**
   * Returns mock certificate has exception when we call certificateAction API.
   *
   * @param certId the unique identifier of certificate.
   */
  private void mockExceptionCertificateAction(final Long certId) {
    final CertificateRetrofitService certRetrofitService = context.getBean(CertificateRetrofitService.class);
    final CertificatePageResource pageResource = new CertificatePageResource();
    final List<CertificateHDto> list = new ArrayList<>();
    final CertificateHDto dto = new CertificateHDto();
    dto.setId(certId);
    list.add(dto);
    pageResource.setContent(list);

    Mockito.when(certRetrofitService.certificateQuery(getAllQuery, null, null, null, null))
        .thenReturn(Calls.response(pageResource));

    Mockito.when(certRetrofitService.certificateActionQuery(certId)).thenReturn(Calls.failure(new IOException()));
  }

  /**
   * Returns mock certificate has empty certificateAction.
   *
   * @param certId the unique identifier of certificate.
   */
  private void mockEmptyCertificateAction(final Long certId) {
    final CertificateRetrofitService certRetrofitService = context.getBean(CertificateRetrofitService.class);
    final CertificatePageResource pageResource = new CertificatePageResource();
    final List<CertificateHDto> list = new ArrayList<>();
    final CertificateHDto dto = new CertificateHDto();
    dto.setId(certId);
    list.add(dto);
    pageResource.setContent(list);

    CertificateActionPageResourceHDto actionPageResourceHDto = new CertificateActionPageResourceHDto();
    actionPageResourceHDto.setContent(null);

    Mockito.when(certRetrofitService.certificateQuery(getAllQuery, null, null, null, null))
        .thenReturn(Calls.response(pageResource));

    Mockito.when(certRetrofitService.certificateActionQuery(certId)).thenReturn(Calls.response(actionPageResourceHDto));
  }

  /**
   * Returns mock certificates.
   *
   * @param certIds the unique identifier of certificates.
   */
  private void mockAllCertificates(final Long... certIds) {
    CertificateRetrofitService certRetrofitService = context.getBean(CertificateRetrofitService.class);
    CertificatePageResource pageResource = new CertificatePageResource();
    List<CertificateHDto> list = new ArrayList<>();
    for (Long id : certIds) {
      CertificateHDto dto = new CertificateHDto();
      dto.setId(id);
      list.add(dto);
    }
    pageResource.setContent(list);

    CertificateActionPageResourceHDto actionPageResourceHDto = new CertificateActionPageResourceHDto();
    List<CertificateActionHDto> actionHDtos = new ArrayList<>();
    actionPageResourceHDto.setContent(actionHDtos);
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_2, "Endorsed"));
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_1, "Endorsed"));

    Mockito.when(certRetrofitService.certificateQuery(Mockito.any(CertificateQueryDto.class), Mockito.anyInt(),
        Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(Calls.failure(new IOException()));
    Mockito.when(certRetrofitService.certificateQuery(getAllQuery, null, null, null, null))
        .thenReturn(Calls.response(pageResource));
    Mockito.when(certRetrofitService.certificateActionQuery(Mockito.anyLong()))
        .thenReturn(Calls.response(actionPageResourceHDto));
  }

  /**
   * Returns mock certificate action DTO.
   *
   * @param actionDate the action date.
   * @param actionTaken the action taken.
   * @return CertificateActionHDto object.
   */
  private CertificateActionHDto createCertificateActionHDto(final Date actionDate, final String actionTaken) {
    CertificateActionHDto actionHDto = new CertificateActionHDto();
    actionHDto.setActionDate(actionDate);
    if (actionTaken != null) {
      CertificateActionTakenDto dto = new CertificateActionTakenDto();
      dto.setName(actionTaken);
      actionHDto.setActionTakenDto(dto);
    }
    return actionHDto;
  }


  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateAttachment(Long, Long)},should return certificate
   * attachment.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void testGetCertificateAttachmentSuccess() throws ClassDirectException {

    final String attachURL = "19876";
    SupplementaryInformationPageResourceDto certAttachDto = new SupplementaryInformationPageResourceDto();
    List<SupplementaryInformationDto> attachListDto = new ArrayList<SupplementaryInformationDto>();
    SupplementaryInformationDto attachDto = new SupplementaryInformationDto();
    attachDto.setId(1L);
    attachDto.setAttachmentUrl(attachURL);
    attachListDto.add(attachDto);
    certAttachDto.setContent(attachListDto);

    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.response(certAttachDto));
    // call method
    final SupplementaryInformationPageResourceDto response = certService.getCertificateAttachment(15L, 1L);
    Assert.assertEquals(Long.valueOf(1), response.getContent().get(0).getId());
    Assert.assertEquals(attachURL, response.getContent().get(0).getAttachmentUrl());
  }

  /**
   * Tests failure scenario for {@link CertificateService#getCertificateAttachment(Long, Long)}.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test(expected = ClassDirectException.class)
  public final void testGetCertificateAttachmentFail() throws ClassDirectException {
    Mockito.when(certificateRetrofitService.certificateAttachmentQuery(Mockito.anyLong(), Mockito.anyLong()))
        .thenReturn(Calls.failure(new IOException("THere is no Certificates")));
    // call method
    final SupplementaryInformationPageResourceDto response = certService.getCertificateAttachment(15L, 1L);
  }

  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer,Integer,<br>
   * String, String, CertificateQueryHDto)},get all certificates.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void shouldReturnDefault() throws ClassDirectException {
    CertificateQueryHDto query = null;
    CertificatePageResource pageResource = certService.getCertificateQueryPageResource(null, null, null, null, query);
    Assert.assertNotNull(pageResource);
    Assert.assertTrue(pageResource.getContent().isEmpty());
  }

  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer,Integer, <br>
   * String, String, CertificateQueryHDto)},should return certificates with Status isDeleted false.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void shouldReturnDeletedStatusFalseCertificates() throws ClassDirectException {
    mockCertificates(1L, 2L, 3L, 4L, 5L, 6L);
    CertificatePageResource pageResource =
        certService.getCertificateQueryPageResource(null, null, null, null, getAllQuery);
    Assert.assertNotNull(pageResource);
    Assert.assertFalse(pageResource.getContent().isEmpty());
    Assert.assertTrue(pageResource.getContent().size() == 3L);
  }

  /**
   * Returns mock certificates has different statuses.
   *
   * @param certIds the unique identifier of certificates.
   */
  private void mockCertificates(final Long... certIds) {
    CertificateRetrofitService certRetrofitService = context.getBean(CertificateRetrofitService.class);
    CertificatePageResource pageResource = new CertificatePageResource();
    List<CertificateHDto> list = new ArrayList<>();
    for (Long id : certIds) {
      CertificateHDto dto = new CertificateHDto();
      dto.setId(id);
      if (id % 2 == 0) {
        CertificateStatusDto statusDto = new CertificateStatusDto();
        statusDto.setId(id);
        dto.setCertificateStatusDto(statusDto);
        list.add(dto);
      }
    }
    pageResource.setContent(list);

    CertificateActionPageResourceHDto actionPageResourceHDto = new CertificateActionPageResourceHDto();
    List<CertificateActionHDto> actionHDtos = new ArrayList<>();
    actionPageResourceHDto.setContent(actionHDtos);
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_2, "Endorsed"));
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_1, "Endorsed"));

    Mockito.when(certRetrofitService.certificateQuery(Mockito.any(CertificateQueryDto.class), Mockito.anyInt(),
        Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(Calls.failure(new IOException()));
    Mockito.when(certificateReferenceService.getCertificateStatuses()).thenReturn(mockStatus());
    Mockito.when(certRetrofitService.certificateQuery(getAllQuery, null, null, null, null))
        .thenReturn(Calls.response(pageResource));
    Mockito.when(certRetrofitService.certificateActionQuery(Mockito.anyLong()))
        .thenReturn(Calls.response(actionPageResourceHDto));
  }


  /**
   * Returns mock certificates has productType.
   *
   * @param certIds the unique identifier of certificates.
   */
  private void mockProductTypeCertificates(final Long... certIds) {
    CertificateQueryHDto qHdto = new CertificateQueryHDto();
    qHdto.setJobId(15L);
    qHdto.setIsStatutoryOnly(Boolean.TRUE);
    CertificateRetrofitService certRetrofitService = context.getBean(CertificateRetrofitService.class);
    CertificatePageResource pageResource = new CertificatePageResource();
    PaginationDto pageDto = new PaginationDto();
    pageResource.setPagination(pageDto);
    List<CertificateHDto> list = new ArrayList<>();
    for (Long id : certIds) {
      CertificateHDto dto = new CertificateHDto();
      dto.setId(id);
      if (id % 2 == 0) {
        CertificateStatusDto statusDto = new CertificateStatusDto();
        statusDto.setId(id);
        dto.setCertificateStatusDto(statusDto);
        CertificateTemplateHDto tempHdto = new CertificateTemplateHDto();
        tempHdto.setId(id);
        ProductCatalogueHDto pHdto = new ProductCatalogueHDto();
        pHdto.setId(id);
        pHdto.setProductType(new LinkResource(3L));
        tempHdto.setProductCatalogueH(pHdto);
        tempHdto.setProductCatalogue(new LinkResource(id));
        dto.setCertificateTemplateHDto(tempHdto);
        dto.setEmployee(new LinkResource(1L));
      } else {
        CertificateStatusDto statusDto = new CertificateStatusDto();
        statusDto.setId(id);
        dto.setCertificateStatusDto(statusDto);
        CertificateTemplateHDto tempHdto = new CertificateTemplateHDto();
        tempHdto.setId(id);
        ProductCatalogueHDto pHdto = new ProductCatalogueHDto();
        pHdto.setId(id);
        pHdto.setProductType(new LinkResource(4L));
        tempHdto.setProductCatalogueH(pHdto);
        tempHdto.setProductCatalogue(new LinkResource(id));
        dto.setCertificateTemplateHDto(tempHdto);
      }
      list.add(dto);
    }
    pageResource.setContent(list);

    CertificateActionPageResourceHDto actionPageResourceHDto = new CertificateActionPageResourceHDto();
    List<CertificateActionHDto> actionHDtos = new ArrayList<>();
    actionPageResourceHDto.setContent(actionHDtos);
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_2, "Endorsed"));
    actionHDtos.add(createCertificateActionHDto(ACTION_DATE_1, "Endorsed"));

    Mockito.when(certRetrofitService.certificateActionQuery(Mockito.anyLong()))
        .thenReturn(Calls.response(actionPageResourceHDto));
    Mockito.when(certificateReferenceService.getCertificateStatuses()).thenReturn(mockStatus());
    Mockito.when(certificateReferenceService.getCertificateStatuses()).thenReturn(mockStatus());
    PowerMockito.mockStatic(SecurityUtils.class);
    BDDMockito.given(SecurityUtils.isEquasisThetisUser()).willReturn(Boolean.TRUE);
    Mockito
        .when(
            certRetrofitService.certificateQuery(Mockito.any(CertificateQueryHDto.class), Mockito.isNull(Integer.class),
                Mockito.isNull(Integer.class), Mockito.isNull(String.class), Mockito.isNull(String.class)))
        .thenReturn(Calls.response(pageResource));

  }


  /**
   * Tests success scenario for
   * {@link CertificateService#getCertificateQueryPageResource(Integer,Integer, <br>
   * String, String, CertificateQueryHDto)},should return certificates with productType is
   * Statutory.
   * <p>
   * should return surveyor details if api {@link CertificateReferenceService return data </p>.
   *
   * @throws ClassDirectException if API call or execution fails.
   */
  @Test
  public final void shouldReturnProductTypeCertificates() throws ClassDirectException {
    CertificateQueryHDto qHdto = new CertificateQueryHDto();
    qHdto.setJobId(15L);
    qHdto.setIsStatutoryOnly(Boolean.TRUE);
    mockProductTypeCertificates(1L, 2L, 3L, 4L, 5L, 6L);
    LrEmployeeDto employee = new LrEmployeeDto();
    employee.setId(1L);
    employee.setName("test name");
    Mockito.when(certificateReferenceService.getSurveyor(Mockito.anyLong())).thenReturn(employee);
    CertificatePageResource pageResource = certService.getCertificateQueryPageResource(null, null, null, null, qHdto);
    Assert.assertNotNull(pageResource);
    Assert.assertFalse(pageResource.getContent().isEmpty());
    Assert.assertTrue(pageResource.getContent().size() == 3L);
    Assert.assertNotNull(pageResource.getContent().get(0).getSurveyor());
    Assert.assertNotNull(pageResource.getContent().get(0).getSurveyor().getName());
    Assert.assertEquals(pageResource.getContent().get(0).getSurveyor().getName(), "test name");
  }

  /**
   * Returns mock certificate statuses.
   *
   * @return mock statuses.
   */
  private List<CertificateStatusDto> mockStatus() {
    List<CertificateStatusDto> list = new ArrayList<>();
    long[] statusIds = {1L, 2L, 3L, 4L, 5L, 6L};
    for (Long id : statusIds) {
      CertificateStatusDto dto = new CertificateStatusDto();
      dto.setId(id);
      if (id % 2 == 0) {
        dto.setDeleted(false);
      } else {
        dto.setDeleted(true);
      }

      list.add(dto);
    }
    return list;

  }

  /**
   * Provides Spring in-class configurations.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Mocks CertificateService.
     *
     * @return mock object.
     */
    @Override
    public CertificateService certificateService() {
      return new CertificateServiceImpl();
    }
  }
}
