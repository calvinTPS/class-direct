package com.baesystems.ai.lr.cd.service.asset.impl.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.TooManyResultsException;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.impl.AssetServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.impl.MastQueryBuilder;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AbstractSubQueryDto;
import com.baesystems.ai.lr.dto.query.PrioritisedAbstractQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;

import retrofit2.mock.Calls;

/**
 * Separate unit test for mast-and-ihs-query.
 *
 * @author Faizal Sidek
 * @author syalavarthi 02-07-2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AssetServiceImplMastIhsQueryTest {

  /**
   * The max page size.
   */
  private static final Integer MAX_SIZE = 16;

  /**
   * The {@link AssetRetrofitService} from spring context.
   */
  @Autowired
  private AssetRetrofitService assetServiceDelegate;

  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * The {@link FavouritesRepository} from spring context.
   */
  @Autowired
  private FavouritesRepository favouritesRepository;

  /**
   * The {@link AssetService} from spring context.
   */
  @Autowired
  private AssetService assetService;

  /**
   * Execute mast-ihs-query without filter.
   *
   * @throws ClassDirectException when error.
   */
  @Test(expected = TooManyResultsException.class)
  public final void throwsExceptionWhenExecuteQueryWithoutFilter() throws ClassDirectException {
    final MastQueryBuilder queryBuilder = MastQueryBuilder.create();

    assetService.executeMastIhsQuery(queryBuilder, null, null, false);
  }

  /**
   * Execute mast-ihs-query filtered by client id.
   *
   * @throws ClassDirectException when error
   */
  @Test
  public final void successfullyExecuteQueryFilteredByClientId() throws ClassDirectException {
    final MastQueryBuilder queryBuilder =
        MastQueryBuilder.create().mandatory(MastQueryBuilder.clientCode(QueryRelationshipType.IN, "1", "2", "3"));
    when(assetServiceDelegate.mastIhsQuery(any())).thenReturn(Calls.response(new MultiAssetPageResourceDto()));

    assetService.executeMastIhsQuery(queryBuilder, null, null, false);
    verify(assetServiceDelegate, atMost(1)).mastIhsQuery(any(PrioritisedAbstractQueryDto.class));
  }

  /**
   * Execute mast-ihs-query with pagination and empty query.
   *
   * @throws ClassDirectException when error.
   */
  @Test
  public final void successfullyExecuteQueryWithPagination() throws ClassDirectException {
    when(assetServiceDelegate.mastIhsQueryUnsorted(any(), anyInt(), anyInt()))
        .thenReturn(Calls.response(new MultiAssetPageResourceDto()));
    assetService.executeMastIhsQuery(MastQueryBuilder.create(), 0, MAX_SIZE, null, null);
    verify(assetServiceDelegate, atMost(1)).mastIhsQuery(any(PrioritisedAbstractQueryDto.class));
  }

  /**
   * Resend mast-ihs-query with hasActiveServices flag if not indicated.
   *
   * @throws ClassDirectException when error.
   * @throws IOException when error.
   */
  @Test
  public final void checkForServicesIfHasActiveServicesSpecified() throws ClassDirectException, IOException {
    when(assetServiceDelegate.mastIhsQueryUnsorted(any(), anyInt(), anyInt()))
        .thenReturn(Calls.response(new MultiAssetPageResourceDto()));

    final UserProfiles user = new UserProfiles();
    user.setRoles(Collections.emptySet());
    when(userProfileService.getUser(anyString())).thenReturn(user);
    when(favouritesRepository.getUserFavourites(anyString())).thenReturn(Collections.emptyList());

    final AssetQueryHDto query = new AssetQueryHDto();
    query.setCodicilDueDateMin(new Date());
    query.setCodicilDueDateMax(new Date());
    query.setHasActiveServices(Boolean.TRUE);
    query.setIncludeInactiveAssets(Boolean.TRUE);
    assetService.getAssets("test-123", query, 1, MAX_SIZE);

    final ArgumentCaptor<PrioritisedAbstractQueryDto> captor =
        ArgumentCaptor.forClass(PrioritisedAbstractQueryDto.class);
    verify(assetServiceDelegate, atMost(2)).mastIhsQueryUnsorted(captor.capture(), anyInt(), anyInt());
    final AbstractQueryDto sentQuery = captor.getValue().getMastPriorityQuery();
    assertNotNull(sentQuery);
    assertNotNull(sentQuery.getAnd());
    assertEquals(1, sentQuery.getAnd().size());
    final AbstractQueryDto mainClause = sentQuery.getAnd().get(0);
    assertNotNull(mainClause);

    final AbstractQueryDto serviceQuery = mainClause.getOr().get(1);
    AbstractSubQueryDto serviceQueryValue = (AbstractSubQueryDto) serviceQuery.getValue();
    AbstractQueryDto serviceSubquery = serviceQueryValue.getSubQuery();

    AbstractQueryDto queryDto = serviceSubquery.getAnd().get(0);
    assertEquals("dueDate", queryDto.getField());
    queryDto = serviceSubquery.getAnd().get(1);
    assertNotNull(queryDto);

  }

  /**
   * Inner config.
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Mocks Asset service.
     *
     * @return mock object.
     */

    @Override
    public AssetService assetService() {
      return new AssetServiceImpl();
    }

    /**
     * @return resource.
     */
    @Bean
    public Resources resources() {
      final Resources resource = new Resources();
      Resources.setInjector(resource);
      return resource;
    }
  }
}
