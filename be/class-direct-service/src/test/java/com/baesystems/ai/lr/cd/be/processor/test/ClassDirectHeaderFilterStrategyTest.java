package com.baesystems.ai.lr.cd.be.processor.test;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.camel.Exchange;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.baesystems.ai.lr.cd.be.processor.ClassDirectHeaderFilterStrategy;

/**
 * Provides unit test on {@link ClassDirectHeaderFilterStrategy}.
 *
 * @author YWearn
 *
 */
public class ClassDirectHeaderFilterStrategyTest {

  /**
   * Tests against camel filter for out headers, no filter apply to in headers.
   * Only white list out headers should allow to pass through, filter = false.
   */
  @Test
  public final void testHeaderWhitelist() {
    final ClassDirectHeaderFilterStrategy filter = new ClassDirectHeaderFilterStrategy();
    final Exchange exchange = Mockito.mock(Exchange.class);

    String headerName = null;
    Object headerValue = null;
    boolean actualResult = false;

    // Test null whitelist
    headerName = "OTHER_HEADER";
    headerValue = new String("Dummy");
    actualResult = filter.applyFilterToCamelHeaders(headerName, headerValue, exchange);
    Assert.assertTrue("All headers should be filtered.", actualResult);


    // Test empty whitelist
    filter.setWhitelist(new ArrayList<String>(0));
    headerName = "OTHER_HEADER";
    headerValue = new String("Dummy");
    actualResult = filter.applyFilterToCamelHeaders(headerName, headerValue, exchange);
    Assert.assertTrue("All headers should be filtered.", actualResult);


    filter.setWhitelist(Arrays.asList("UUID"));

    // Test whitelist
    headerName = "UUID";
    headerValue = new String("Dummy");
    actualResult = filter.applyFilterToCamelHeaders(headerName, headerValue, exchange);
    Assert.assertFalse("Whitelist should not be filtered.", actualResult);

    // Test non-whitelist
    headerName = "OTHER_HEADER";
    headerValue = new String("Dummy");
    actualResult = filter.applyFilterToCamelHeaders(headerName, headerValue, exchange);
    Assert.assertTrue("Non whitelist should be filtered.", actualResult);

    // Test IN header - filter should not apply even if non-whitelist value.
    headerName = "IN_HEADER";
    headerValue = new String("Dummy");
    actualResult = filter.applyFilterToExternalHeaders(headerName, headerValue, exchange);
    Assert.assertFalse("IN header should not be filtered.", actualResult);

  }

}
