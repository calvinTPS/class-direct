package com.baesystems.ai.lr.cd.service.favourites.impl.test;


import static com.baesystems.ai.lr.cd.service.test.UserId.ALICE;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.FavouritesRepository;
import com.baesystems.ai.lr.cd.be.domain.repositories.Favourites;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.favourites.FavouritesService;
import com.baesystems.ai.lr.cd.service.favourites.impl.FavouritesServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;

/**
 * Provides unit test class for favourite service {@link FavouritesService}.
 *
 * @author NAvula.
 * @author syalavarthi 23-05-2017.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FavouritesServiceImplTest.Config.class)
public class FavouritesServiceImplTest {
  /**
   * The {@link FavouritesService} from spring context.
   */
  @Autowired
  private FavouritesService favouritesService;

  /**
   * The {@link FavoritesRepository} from spring context.
   */
  @Autowired
  private FavouritesRepository favouritesRepository;

  /**
   * Tests success scenario to get favourite assets of user with valid user id
   * {@link FavouritesService#getFavouritesForUser(String)}.
   *
   * @throws RecordNotFoundException if the requested user not available.
   */
  @Test
  public final void getFavouritesForUserTest() throws RecordNotFoundException {

    List<Favourites> favourites = new ArrayList<>();
    Favourites favourite = new Favourites();
    favourite.setAssetId(1L);
    favourites.add(favourite);
    Mockito.when(favouritesRepository.getUserFavourites(ALICE.getId())).thenReturn(favourites);

    List<String> userFavourites = favouritesService.getFavouritesForUser(ALICE.getId());
    Assert.assertNotNull(userFavourites);
  }

  /**
   * Tests failure scenario to get favourite assets of user when internal call fails
   * {@link FavouritesService#getFavouritesForUser(String)}.
   *
   * @throws RecordNotFoundException if the requested user not available.
   */
  @Test(expected = RecordNotFoundException.class)
  public final void getFavouritesForUserTestFail() throws RecordNotFoundException {

    List<Favourites> favourites = new ArrayList<>();
    Favourites favourite = new Favourites();
    favourite.setAssetId(1L);
    favourites.add(favourite);
    Mockito.when(favouritesRepository.getUserFavourites(ALICE.getId()))
        .thenThrow(new RecordNotFoundException("exception"));
    favouritesService.getFavouritesForUser(ALICE.getId());
  }

  /**
   * Tests success scenario to delete favourite assets of user with valid user id
   * {@link FavouritesService#getFavouritesForUser(String)}.
   *
   * @throws RecordNotFoundException if the requested user not available.
   * @throws ClassDirectException if error in deleting favourite assets of user.
   */
  @Test
  public final void deleteFavouritesForUserTest() throws RecordNotFoundException, ClassDirectException {

    FavouritesRepository favoriteRepositoryMock = Mockito.mock(FavouritesRepository.class);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        return null;
      }
    }).when(favoriteRepositoryMock).deleteAllUserFavourites(ALICE.getId());

    favouritesService.deleteAllUserFavourites(ALICE.getId());

  }

  /**
   * Spring base config.
   *
   * @author NAvula
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * Favorite service {@link FavouritesService}.
     *
     * @return favourite service.
     *
     */
    @Override
    @Bean
    public FavouritesService favouritesService() {
      return new FavouritesServiceImpl();
    }
  }
}
