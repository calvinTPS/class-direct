package com.baesystems.ai.lr.cd.be.processor.test;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.processor.CustomJsonSerializerModule;
import com.baesystems.ai.lr.cd.be.processor.SlimJsonDataFormat;
import com.baesystems.ai.lr.cd.be.processor.test.dtos.SimpleDTO;
import com.baesystems.ai.lr.cd.be.processor.test.dtos.SimpleInnerDTO;
import com.baesystems.ai.lr.cd.be.processor.test.dtos.SimpleTestDto;
import com.baesystems.ai.lr.cd.be.utils.PaginationUtil;
import com.baesystems.ai.lr.dto.paging.BasePageResource;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by PFauchon on 21/11/2016.
 */
public class SlimJsonDataFormatTest {

  /**
   * Header field name.
   */
  private static final String HEADER_NAME = "Fields";
  /**
   * Header field value.
   */
  private static final String HEADER_VALUE = "inner.*,name,innerList.name";

  /**
   * Header field value.
   */
  private static final String HEADER_VALUE_1 = "dueStatus.*";

  /**
   * What we expect from filtered DTO1 from test fitlered fields.
   */
  private static final String DTO_1_FILTERED_JSON = "{\"inner\":{\"id\":2,\"name\":\"My Fancy name\"},"
      + "\"name\":\"SimpleDTO\"," + "\"innerList\":[{\"name\":\"My not so fancy name...\"},{\"name\":\"Whooo\"}]}";

  /**
   * What we expect from full DTO1 from test fitlered fields.
   */
  private static final String DTO_1_FULL_JSON = "{\"inner\":{\"id\":2,\"name\":\"My Fancy name\"},\"id\":1,"
      + "\"name\":\"SimpleDTO\",\"innerList\":[{\"id\":1,\"name\":\"My not so fancy name...\"},{\"id\":3,"
      + "\"name\":\"Whooo\"}]}";

  /**
   * What we expect from filtered DTO2 from test fitlered fields.
   */
  private static final String DTO_2_FILTERED_JSON =
      "{\"inner\":{\"id\":2," + "\"name\":\"My Fancy name for the second DTO\"}," + "\"name\":\"SimpleDTO2\","
          + "\"innerList\":[{\"name\":\"My not so fancy name this time...\"},{\"name\":\"nope\"}]}";

  /**
   * What we expect from filtered DTO2 from test filtered fields.
   */
  private static final String DTO_2_FULL_JSON =
      "{\"inner\":{\"id\":2,\"name\":\"My Fancy name for the second DTO\"}," + "\"id\":2,"
          + "\"name\":\"SimpleDTO2\",\"innerList\":[{\"id\":1,\"name\":\"My not so fancy name this time...\"},"
          + "{\"id\":3," + "\"name\":\"nope\"}]}";

  /**
   * List of filtered data.
   */
  private static final String LIST_DTO1_DTO2_FILTERED = "[" + DTO_1_FILTERED_JSON + "," + DTO_2_FILTERED_JSON + "]";

  /**
   * List of full data.
   */
  private static final String LIST_DTO1_DTO2_FULL = "[" + DTO_1_FULL_JSON + "," + DTO_2_FULL_JSON + "]";

  /**
   * Pagination wrapper.
   */
  private static final String PAGINATION_WRAPPER_FILTERED =
      "{\"content\":%s,\"pagination\":{\"size\":2147483647," + "\"number\":1,"
          + "\"first\":true,\"last\":false,\"totalPages\":0,\"totalElements\":2,\"numberOfElements\":2,\"sort\":null,"
          + "\"link\":null}}";


  /**
   * Kill magic numbers !.
   */
  private static final int ONE = 1, TWO = 2, THREE = 3;

  /**
   * Pagination wrapper.
   */
  private static final String PAGINATION_WRAPPER_FULL = "{\"pagination\":{\"size\":2147483647,\"number\":1,"
      + "\"first\":true,\"last\":false,\"totalPages\":0,\"totalElements\":2,\"numberOfElements\":2,\"sort\":null,"
      + "\"link\":null},\"content\":%s}";


  /**
   * Empty json list.
   */
  private static final String EMPTY_LIST = "[]";

  /**
   * Enum Stream.
   */
  private static final String ENUM_FILTER_STREAM =
      "{\"dueStatus\":{\"name\":\"Due soon\",\"id\":2,\"priority\":3,\"icon\":\"duesoon\","
      + "\"assetIcon\":\"overall_due_soon\"}}";

  /**
   * Test pagination with header filters.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testPaginationFiltered() throws Exception {


    final Exchange exchange = mockExchange();
    Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn(HEADER_VALUE);

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);


    final List<SimpleDTO> list = new ArrayList<>();
    list.add(getSimpleDTO1());
    list.add(getSimpleDTO2());


    final BasePageResource<SimpleDTO> paginated = PaginationUtil.paginate(list, 1, Integer.MAX_VALUE);

    final OutputStream stream = new SimpleOutputStream();


    dataFormat.marshal(exchange, paginated, stream);
    Assert.assertEquals(String.format(PAGINATION_WRAPPER_FILTERED, LIST_DTO1_DTO2_FILTERED), stream.toString());
  }

  /**
   * Tests on paginated resource without filter.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testPaginationUnFiltered() throws Exception {


    final Exchange exchange = mockExchange();

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);


    final List<SimpleDTO> list = new ArrayList<>();
    list.add(getSimpleDTO1());
    list.add(getSimpleDTO2());


    final BasePageResource<SimpleDTO> paginated = PaginationUtil.paginate(list, 1, Integer.MAX_VALUE);

    final OutputStream stream = new SimpleOutputStream();


    dataFormat.marshal(exchange, paginated, stream);
    // we need to steal the id from generated pagination resource because it's randomly generated.
    Assert.assertEquals(String.format(PAGINATION_WRAPPER_FULL, LIST_DTO1_DTO2_FULL), stream.toString());
  }

  /**
   * Tests list without filter.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testListUnfiltered() throws Exception {

    final Exchange exchange = mockExchange();

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);


    final List<SimpleDTO> list = new ArrayList<>();
    list.add(getSimpleDTO1());
    list.add(getSimpleDTO2());

    final OutputStream stream = new SimpleOutputStream();


    dataFormat.marshal(exchange, list, stream);
    Assert.assertEquals(LIST_DTO1_DTO2_FULL, stream.toString());
  }


  /**
   * Test list with filter.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testListFiltered() throws Exception {


    final Exchange exchange = mockExchange();
    Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn(HEADER_VALUE);

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);


    final List<SimpleDTO> list = new ArrayList<>();
    list.add(getSimpleDTO1());
    list.add(getSimpleDTO2());

    final OutputStream stream = new SimpleOutputStream();


    dataFormat.marshal(exchange, list, stream);
    Assert.assertEquals(LIST_DTO1_DTO2_FILTERED, stream.toString());
  }

  /**
   * Test single object with filter.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testEmptyList() throws Exception {
    final Exchange exchange = mockExchange();
    Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn(HEADER_VALUE);

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);

    final OutputStream stream = new SimpleOutputStream();

    dataFormat.marshal(exchange, new ArrayList<SimpleDTO>(), stream);
    Assert.assertEquals(EMPTY_LIST, stream.toString());
  }


  /**
   * Test with null content, this will default back to Camel Jackson data format behavior, which is
   * NullPointerException let's not change that.
   *
   * @throws Exception if something goes wrong.
   */
  @Test(expected = NullPointerException.class)
  public final void testNullObject() throws Exception {


    final Exchange exchange = mockExchange();
    Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn(HEADER_VALUE);

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);

    final OutputStream stream = new SimpleOutputStream();

    dataFormat.marshal(null, new ArrayList<SimpleDTO>(), stream);
  }


  /**
   * Test sngle object without filter.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testSingleObjectUnfiltered() throws Exception {

    final Exchange exchange = mockExchange();
    // Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn("*");

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);


    OutputStream stream = new SimpleOutputStream();

    dataFormat.marshal(exchange, getSimpleDTO1(), stream);
    Assert.assertEquals(DTO_1_FULL_JSON, stream.toString());


    // Trying with empty headers
    stream = new SimpleOutputStream();
    Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn("");
    dataFormat.marshal(exchange, getSimpleDTO1(), stream);
    Assert.assertEquals(DTO_1_FULL_JSON, stream.toString());
  }


  /**
   * Test single object with filter.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testSingleObjectFiltered() throws Exception {


    final Exchange exchange = mockExchange();
    Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn(HEADER_VALUE);

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);


    final OutputStream stream = new SimpleOutputStream();

    dataFormat.marshal(exchange, getSimpleDTO1(), stream);
    Assert.assertEquals(DTO_1_FILTERED_JSON, stream.toString());
  }

  /**
   * Test single object with filter.
   *
   * @throws Exception if something goes wrong.
   */
  @Test
  public final void testSingleObjectFilteredForEnum() throws Exception {


    final Exchange exchange = mockExchange();
    Mockito.when(exchange.getIn().getHeader(HEADER_NAME, String.class)).thenReturn(HEADER_VALUE_1);

    final SlimJsonDataFormat dataFormat = new SlimJsonDataFormat();
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new CustomJsonSerializerModule());
    dataFormat.setObjectMapper(mapper);


    final OutputStream stream = new SimpleOutputStream();

    dataFormat.marshal(exchange, getSimpleTestDTO1(), stream);
    Assert.assertEquals(ENUM_FILTER_STREAM, stream.toString());
  }

  /**
   * Mock an exchange object with it's getIn message set.
   *
   * @return a mocked exchange object
   */
  private Exchange mockExchange() {
    final Exchange exchange = Mockito.mock(Exchange.class);
    final Message in = Mockito.mock(Message.class);


    Mockito.when(exchange.getIn()).thenReturn(in);

    return exchange;
  }


  /**
   * Creates a simple DTO.
   *
   * @return a dto
   */
  private SimpleDTO getSimpleDTO1() {
    final SimpleDTO simpleDTO = new SimpleDTO();
    simpleDTO.setName("SimpleDTO");
    simpleDTO.setId(ONE);
    simpleDTO.setInner(new SimpleInnerDTO(TWO, "My Fancy name"));
    simpleDTO.getInnerList().add(new SimpleInnerDTO(ONE, "My not so fancy name..."));
    simpleDTO.getInnerList().add(new SimpleInnerDTO(THREE, "Whooo"));
    return simpleDTO;
  }

  /**
   * Creates a simple DTO.
   *
   * @return a dto
   */
  private SimpleTestDto getSimpleTestDTO1() {
    final SimpleTestDto simpleTestDto = new SimpleTestDto();
    simpleTestDto.setName("SimpleTestDTO");
    simpleTestDto.setId(ONE);
    simpleTestDto.setDueStatus(DueStatus.DUE_SOON);

    return simpleTestDto;
  }

  /**
   * Creates a simple dto.
   *
   * @return a dto
   */
  private SimpleDTO getSimpleDTO2() {
    final SimpleDTO simpleDTO2 = new SimpleDTO();
    simpleDTO2.setName("SimpleDTO2");
    simpleDTO2.setId(TWO);
    simpleDTO2.setInner(new SimpleInnerDTO(TWO, "My Fancy name for the second DTO"));
    simpleDTO2.getInnerList().add(new SimpleInnerDTO(ONE, "My not so fancy name this time..."));
    simpleDTO2.getInnerList().add(new SimpleInnerDTO(THREE, "nope"));

    return simpleDTO2;
  }

  /**
   * Simple output stream that concatenates to a string.
   */
  private final class SimpleOutputStream extends OutputStream {

    /**
     * String builder where we'll happend tuff into it.
     */
    private final StringBuilder builder = new StringBuilder();

    /**
     * Writes a bite to the variable string.
     *
     * @param b a byte
     * @throws IOException when something goes wrong
     */
    @Override
    public void write(final int b) throws IOException {
      builder.append((char) b);
    }

    /**
     * return the string.
     *
     * @return the String
     */
    @Override
    public String toString() {
      return builder.toString();
    }
  }


}
