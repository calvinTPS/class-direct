package com.baesystems.ai.lr.cd.service.export.impl.test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.asset.CoCService;
import com.baesystems.ai.lr.cd.service.aws.AmazonStorageService;
import com.baesystems.ai.lr.cd.service.export.CocExportService;
import com.baesystems.ai.lr.cd.service.export.impl.CocExportServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;


/**
 * Provides unit test for {@link CocExportService}.
 *
 * @author yng
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.crypto.*"})
@ContextConfiguration(classes = CocExportServiceTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class CocExportServiceTest {

  /**
   * The {@link CoCService} from {@link CocExportServiceTest.Config} class.
   */
  @Autowired
  private CoCService cocService;

  /**
   * The {@link AssetService} from {@link CocExportServiceTest.Config} class.
   */
  @Autowired
  private AssetService assetService;

  /**
   * The {@link CocExportService} from {@link CocExportServiceTest.Config} class.
   */
  @Autowired
  private CocExportService cocExportService;

  /**
   * The {@link AmazonStorageService} from {@link CocExportServiceTest.Config} class.
   */
  @Autowired
  private AmazonStorageService amazonStorageService;

  /**
   * Provides mock reset before any test class is executed.
   */
  @Before
  public void resetMock() {
    Mockito.reset(cocService);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  /**
   * Test positive case scenario for coc pdf generation.
   *
   * @throws ClassDirectException if API call fail.
   */
  @Test
  public void testGenerateCoCPDF() throws ClassDirectException {

    // mock asset data
    final AssetHDto asset = new AssetHDto();
    asset.setId(1L);
    asset.setName("My asset");
    asset.setLeadImo("1000019");
    IhsAssetDetailsDto ihsAssetDetailsDto = new IhsAssetDetailsDto();
    ihsAssetDetailsDto.setId("1000019");
    IhsAssetDto ihsAssetDto = new IhsAssetDto();
    ihsAssetDto.setId("100014");
    ihsAssetDto.setClassList("AB");
    ihsAssetDetailsDto.setIhsAsset(ihsAssetDto);
    asset.setIhsAssetDto(ihsAssetDetailsDto);
    asset.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    asset.getClassMaintenanceStatusDto().setName("Single");

    // mock coc data
    final CoCHDto data1 = new CoCHDto();
    data1.setId(6L);
    data1.setTitle("coc 1");
    data1.setImposedDate(new GregorianCalendar(2017, 1, 1).getTime());
    data1.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    data1.setCategoryH(new CodicilCategoryHDto());
    data1.getCategoryH().setName("Draft");
    data1.getCategoryH().setId(12L);
    data1.setStatusH(new CodicilStatusHDto());
    data1.getStatusH().setName("Open");
    data1.setReferenceCode("111");

    final CoCHDto data2 = new CoCHDto();
    data2.setId(2L);
    data2.setTitle("coc 2");
    data2.setImposedDate(new GregorianCalendar(2017, 1, 1).getTime());
    data2.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    data2.setCategoryH(new CodicilCategoryHDto());
    data2.getCategoryH().setName("Draft");
    data2.getCategoryH().setId(12L);
    data2.setStatusH(new CodicilStatusHDto());
    data2.getStatusH().setName("Open");
    data2.setReferenceCode("err");

    final CoCHDto data3 = new CoCHDto();
    data3.setId(3L);
    data3.setTitle("coc 3");
    data3.setImposedDate(new GregorianCalendar(2017, 1, 1).getTime());
    data3.setDueDate(new GregorianCalendar(2017, 1, 1).getTime());
    data3.setCategoryH(new CodicilCategoryHDto());
    data3.getCategoryH().setName("Draft");
    data3.getCategoryH().setId(18L);
    data3.setStatusH(new CodicilStatusHDto());
    data3.getStatusH().setName("Open");
    data3.setReferenceCode("123");

    final List<CoCHDto> cocsMock = new ArrayList<>();
    cocsMock.add(data1);
    cocsMock.add(data2);
    cocsMock.add(data3);

    final CoCPageResource cocResource = new CoCPageResource();
    cocResource.setContent(cocsMock);

    Mockito.when(cocService.getCoC(Mockito.isNull(Integer.class), Mockito.isNull(Integer.class), Matchers.anyLong()))
        .thenReturn(cocResource);
    Mockito.when(assetService.assetByCode(Matchers.anyString(), Matchers.anyString())).thenReturn(asset);
    Mockito.when(amazonStorageService.uploadFile(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
        Matchers.anyString()))
        .thenReturn("token");

    final ExportPdfDto query = new ExportPdfDto();
    query.setCode("LRV1");
    Set<Long> itemsToExport = new HashSet<Long>();
    itemsToExport.add(2L);
    itemsToExport.add(6L);
    query.setItemsToExport(itemsToExport);

    final StringResponse response = cocExportService.downloadPdf(query);

    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getResponse());
  }

  /**
   *
   * Provides Spring in-class configurations.
   *
   * @author yng
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    @Override
    @Bean
    public CocExportService cocExportService() {
      return new CocExportServiceImpl();
    }
  }

}
