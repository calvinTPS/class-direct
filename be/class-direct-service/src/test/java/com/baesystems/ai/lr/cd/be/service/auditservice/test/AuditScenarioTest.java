package com.baesystems.ai.lr.cd.be.service.auditservice.test;

import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ACTIVE;
import static com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum.ARCHIVED;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.asset.AssetRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.UserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.BaseDtoPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetDto;
import com.baesystems.ai.lr.cd.be.domain.dto.subfleet.SubFleetOptimizedDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.CreateNewUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserEORDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.SubFleetDAO;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.enums.SubfleetMoveOperationEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.service.audit.AuditService;
import com.baesystems.ai.lr.cd.service.audit.impl.AuditServiceImpl;
import com.baesystems.ai.lr.cd.service.subfleet.SubFleetService;
import com.baesystems.ai.lr.cd.service.subfleet.impl.SubFleetServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.cd.service.userprofile.impl.UserProfileServiceImpl;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.fasterxml.jackson.core.JsonProcessingException;

import retrofit2.mock.Calls;

/**
 * Provides unit tests for audit service {@link AuditService}.
 *
 * @author VKolagutla
 * @author syalavarthi 15-06-2017.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AuditScenarioTest.Config.class)
@DirtiesContext
public class AuditScenarioTest {

  /**
   * The logger for user audit service {@link AuditScenarioTest}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AuditScenarioTest.class);

  /**
   * The {@link UserProfileService} from spring context.
   */
  @Autowired
  private UserProfileService userProfileService;
  /**
   * The {@link UserProfileDao} from spring context.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  /**
   * The {@link UserRetrofitService} from spring context.
   */
  @Autowired
  private UserRetrofitService userRetrofitService;

  /**
   * The {@link SubFleetService} from spring context.
   */
  @Autowired
  private SubFleetService subFleetService;

  /**
   * The {@link SubFleetDAO} from spring context.
   */
  @Autowired
  private SubFleetDAO subFleetDAO;
  /**
   * The {@link AssetRetrofitService} injected from Spring context.
   */
  @Autowired
  private AssetRetrofitService assetRetrofitService;

  /**
   * The user email for alice.
   */
  private static final String ALICE_USER_EMAIL = "alice.classdirect@gmail.com";

  /**
   * The user email for bob.
   */
  private static final String BOB_USER_EMAIL = "bob.classdirect@gmail.com";

  /**
   * The message for successful creation.
   */
  @Value("${user.successful.create}")
  private String userSuccessfulCreate;

  /**
   * The message for successful deletion of Eor.
   */
  @Value("${eor.successful.delete}")
  private String successfulDelete;

  /**
   * Tests success audit scenario to re-create archived user
   * {@link UserProfileService#recreateArchivedUser(UserProfiles, String, CreateNewUserDto)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public void testRecreateArchivedUser() throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId("105");
    userDto.setEmail(ALICE_USER_EMAIL);

    LocalDate date = LocalDate.now();
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("101");
    userProfiles.setEmail(BOB_USER_EMAIL);
    userProfiles.setUserAccExpiryDate(date);

    UserAccountStatus accountStatusActive = new UserAccountStatus();
    accountStatusActive.setId(ACTIVE.getId());
    accountStatusActive.setName(ACTIVE.getName());
    userProfiles.setStatus(accountStatusActive);

    // Archived User
    UserProfiles archiverUser = new UserProfiles();
    archiverUser.setUserId("105");
    archiverUser.setEmail(ALICE_USER_EMAIL);
    archiverUser.setUserAccExpiryDate(date);

    UserAccountStatus accountStatusArchived = new UserAccountStatus();
    accountStatusArchived.setId(ARCHIVED.getId());
    accountStatusArchived.setName(ARCHIVED.getName());
    archiverUser.setStatus(accountStatusArchived);

    final StatusDto status = new StatusDto(userSuccessfulCreate);
    when(userProfileDao.getUser(eq("101"))).thenReturn(userProfiles);
    when(userProfileDao.getUser(eq("105"))).thenReturn(archiverUser);
    doReturn(status).when(userProfileDao).recreateArchivedUser(any(UserProfiles.class), anyString(),
        any(CreateNewUserDto.class));
    when(userRetrofitService.approveAzureUser(any(LRIDUserStatusDto.class), anyString()))
        .thenReturn(Calls.response(status));
    StatusDto statusExpec = userProfileService.createUser("101", userDto);
    Assert.assertNotNull(statusExpec);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Tests success audit scenario to create user
   * {@link UserProfileService#createUser(String, CreateNewUserDto)}.
   *
   * @throws RecordNotFoundException if no record admin user performs request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public void testCreateUser() throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    Mockito.reset(userProfileDao);
    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId("105");
    userDto.setEmail(ALICE_USER_EMAIL);

    LocalDate date = LocalDate.now();
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("101");
    userProfiles.setEmail(BOB_USER_EMAIL);
    userProfiles.setUserAccExpiryDate(date);

    UserAccountStatus accountStatus = new UserAccountStatus();
    accountStatus.setName(AccountStatusEnum.ACTIVE.getName());
    userProfiles.setStatus(accountStatus);

    final StatusDto status = new StatusDto(userSuccessfulCreate);
    when(userProfileDao.getUser(eq("101"))).thenReturn(userProfiles);
    when(userProfileDao.getUser(eq("105"))).thenThrow(new RecordNotFoundException(""));
    doReturn(status).when(userProfileDao).createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class));

    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));
    StatusDto statusExpec = userProfileService.createUser("101", userDto);

    Assert.assertNotNull(statusExpec);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Tests success audit scenario to create user with eor permissions
   * {@link UserProfileService#createUser(String, CreateNewUserDto)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public void testCreateUserWithEor() throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    List<UserEORDto> eorList = new ArrayList<>();
    UserEORDto eor = new UserEORDto();
    eor.setTmReport(true);
    eorList.add(eor);

    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId("105");
    userDto.setEmail(ALICE_USER_EMAIL);
    userDto.setEor(eorList);

    LocalDate date = LocalDate.now();
    LOGGER.debug("Date {}", date);
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("101");
    userProfiles.setEmail(BOB_USER_EMAIL);
    userProfiles.setUserAccExpiryDate(date);

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    final StatusDto status = new StatusDto(userSuccessfulCreate);


    Mockito.when(userProfileDao.getUser(Mockito.eq("101"))).thenReturn(userProfiles);
    Mockito.when(userProfileDao.getUser(Mockito.eq("105"))).thenThrow(new RecordNotFoundException(""));
    doReturn(status).when(userProfileDao).createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class));
    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));
    StatusDto statusExpec = userProfileService.createUser("101", userDto);

    Assert.assertNotNull(statusExpec);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Tests success audit scenario to create user with TM permissions
   * {@link UserProfileService#createUser(String, CreateNewUserDto)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public void testCreateUserWithTm() throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    List<UserEORDto> eorList = new ArrayList<>();
    UserEORDto eor = new UserEORDto();
    eor.setTmReport(true);
    eorList.add(eor);

    CreateNewUserDto userDto = new CreateNewUserDto();
    userDto.setUserId("105");
    userDto.setEmail(ALICE_USER_EMAIL);
    userDto.setEor(eorList);

    LocalDate date = LocalDate.now();
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("101");
    userProfiles.setEmail(BOB_USER_EMAIL);
    userProfiles.setUserAccExpiryDate(date);

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    final StatusDto status = new StatusDto(userSuccessfulCreate);

    Mockito.when(userProfileDao.getUser(Mockito.eq("101"))).thenReturn(userProfiles);
    Mockito.when(userProfileDao.getUser(Mockito.eq("105"))).thenThrow(new RecordNotFoundException(""));
    doReturn(status).when(userProfileDao).createUser(Mockito.anyString(), Mockito.any(CreateNewUserDto.class));
    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));
    StatusDto statusExpec = userProfileService.createUser("101", userDto);

    Assert.assertNotNull(statusExpec);
    Assert.assertEquals(statusExpec, status);
  }

  /**
   * Tests success audit scenario to update user with EOR permissions
   * {@link UserProfileService#updateUser(String, String, UserProfiles, String)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public final void updateUsersEORChanged()
      throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("111", Collections.emptyMap()));

    Set<UserEOR> eors = new HashSet<>();
    UserEOR eor = new UserEOR();
    eor.setEorId(123L);
    eor.setAccessTmReport(true);
    eor.setImoNumber("123");
    eors.add(eor);

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("107");
    userProfiles.setEors(eors);

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    Set<UserEOR> eors1 = new HashSet<>();
    UserEOR eor1 = new UserEOR();
    eor1.setEorId(124L);
    eor1.setAccessTmReport(false);
    eor1.setImoNumber("12");
    eors1.add(eor1);

    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("111");
    userProfiles1.setEors(eors1);

    UserAccountStatus accountSatatus1 = new UserAccountStatus();
    accountSatatus1.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles1.setStatus(accountSatatus1);

    final StatusDto status = new StatusDto(userSuccessfulCreate);

    Mockito.when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("107"))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles1);

    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));

    Assert.assertNotNull(userProfiles);
    UserProfiles user = userProfileService.updateUser("107", "111", userProfiles, null);

    Assert.assertNotNull(user);
  }

  /**
   * Tests success audit scenario to update user with TM permissions
   * {@link UserProfileService#updateUser(String, String, UserProfiles, String)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public final void updateUsersTMChanged()
      throws RecordNotFoundException, ClassDirectException, JsonProcessingException {

    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("111", Collections.emptyMap()));
    Set<UserEOR> eors = new HashSet<>();
    UserEOR eor = new UserEOR();
    eor.setEorId(123L);
    eor.setAccessTmReport(true);
    eor.setImoNumber("123");
    eors.add(eor);

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("107");
    userProfiles.setEors(eors);
    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    Set<UserEOR> eors1 = new HashSet<>();
    UserEOR eor1 = new UserEOR();
    eor1.setEorId(124L);
    eor1.setAccessTmReport(false);
    eor1.setImoNumber("124");
    eors1.add(eor1);

    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("111");
    userProfiles1.setEors(eors1);

    UserAccountStatus accountSatatus1 = new UserAccountStatus();
    accountSatatus1.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles1.setStatus(accountSatatus1);

    final StatusDto status = new StatusDto(userSuccessfulCreate);

    Mockito.when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("107"))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles1);

    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));

    Assert.assertNotNull(userProfiles);
    UserProfiles user = userProfileService.updateUser("107", "111", userProfiles, null);

    Assert.assertNotNull(user);
  }

  /**
   * Tests success audit scenario to update user with status changes
   * {@link UserProfileService#updateUser(String, String, UserProfiles, String)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public final void updateUsersStatusChanged()
      throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("111", Collections.emptyMap()));
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("107");

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);


    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("111");

    UserAccountStatus accountSatatus1 = new UserAccountStatus();
    accountSatatus1.setName(AccountStatusEnum.ACTIVE.getName());
    userProfiles1.setStatus(accountSatatus1);

    final StatusDto status = new StatusDto(userSuccessfulCreate);

    Mockito.when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("107"))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles1);

    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));

    Assert.assertNotNull(userProfiles);
    UserProfiles user = userProfileService.updateUser("107", "111", userProfiles, null);
    Assert.assertNotNull(user);
  }


  /**
   * Tests success audit scenario to update user with expiry date changes
   * {@link UserProfileService#updateUser(String, String, UserProfiles, String)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public final void updateUsersExpiryDateChanged()
      throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("111", Collections.emptyMap()));
    LocalDate date = LocalDate.now();
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("107");
    userProfiles.setEmail(BOB_USER_EMAIL);
    userProfiles.setUserAccExpiryDate(date);

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    LocalDate date1 = LocalDate.now().plusDays(1L);
    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("111");
    userProfiles1.setEmail(BOB_USER_EMAIL);
    userProfiles1.setUserAccExpiryDate(date1);

    UserAccountStatus accountSatatus1 = new UserAccountStatus();
    accountSatatus1.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles1.setStatus(accountSatatus1);

    final StatusDto status = new StatusDto(userSuccessfulCreate);
    when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);
    when(userProfileDao.getUser(Mockito.eq("107"))).thenReturn(userProfiles);
    when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles1);
    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));

    Assert.assertNotNull(userProfiles);
    UserProfiles user = userProfileService.updateUser("107", "111", userProfiles, null);
    Assert.assertNotNull(user);
  }

  /**
   * Tests success audit scenario to update user with client code changes
   * {@link UserProfileService#updateUser(String, String, UserProfiles, String)} and delete all user
   * favourites.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public final void testUpdateUsersChanged()
      throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("107");
    userProfiles.setEmail(BOB_USER_EMAIL);
    userProfiles.setClientCode("123");
    userProfiles.setRestrictAll(true);
    userProfiles.setLastLogin(LocalDateTime.now());

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("111");
    userProfiles1.setEmail(ALICE_USER_EMAIL);
    userProfiles1.setClientCode("456");
    userProfiles1.setLastLogin(LocalDateTime.now());

    UserAccountStatus activeUserAccountStatus = new UserAccountStatus();
    activeUserAccountStatus.setId(AccountStatusEnum.ARCHIVED.getId());
    activeUserAccountStatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles1.setStatus(activeUserAccountStatus);

    final StatusDto status = new StatusDto(userSuccessfulCreate);
    when(userProfileDao.updateUser(anyString(), anyString(), any(UserProfiles.class), Mockito.isNull(String.class)))
        .thenReturn(userProfiles);
    when(userProfileDao.getUser(eq("107"))).thenReturn(userProfiles);
    when(userProfileDao.getUser(eq("111"))).thenReturn(userProfiles1);
    when(userRetrofitService.approveAzureUser(any(LRIDUserStatusDto.class), anyString()))
        .thenReturn(Calls.response(status));
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("111", Collections.emptyMap()));
    Assert.assertNotNull(userProfiles);
    UserProfiles user = userProfileService.updateUser("107", "111", userProfiles, null);
    Assert.assertNotNull(user);
  }

  /**
   * Tests success audit scenario to update user with company changes
   * {@link UserProfileService#updateUser(String, String, UserProfiles, String)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public final void testUpdateUsersCompanyChanged()
      throws RecordNotFoundException, ClassDirectException, JsonProcessingException {
    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("111", Collections.emptyMap()));
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("111");
    userProfiles.setEmail(ALICE_USER_EMAIL);
    userProfiles.setCompanyName("Name");
    userProfiles.setName("first");

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("111");
    userProfiles1.setEmail(ALICE_USER_EMAIL);
    userProfiles1.setCompanyName("companyName");

    UserAccountStatus accountSatatus1 = new UserAccountStatus();
    accountSatatus1.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles1.setStatus(accountSatatus1);

    final StatusDto status = new StatusDto(userSuccessfulCreate);

    Mockito.when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("107"))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles1);


    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.response(status));

    Assert.assertNotNull(userProfiles);
    UserProfiles user = userProfileService.updateUser("107", "111", userProfiles, null);

    Assert.assertNotNull(user);
  }

  /**
   * Tests success audit scenario to update user with subfleet changes
   * {@link SubFleetService#saveSubFleet(String, SubFleetDto)}.
   *
   * @throws SQLException error in getting subfleet for target user.
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   */
  @Test
  public final void testSaveSubFleet() throws SQLException, RecordNotFoundException, ClassDirectException {

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("111");
    userProfiles.setEmail(BOB_USER_EMAIL);

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    SubFleetDto subfleet = new SubFleetDto();

    List<Long> oldIds = new ArrayList<>();
    oldIds.add(1L);
    oldIds.add(2L);

    List<Long> accAssetIds = new ArrayList<>();
    accAssetIds.add(3L);
    accAssetIds.add(4L);

    subfleet.setAccessibleAssetIds(accAssetIds);

    StatusDto status = new StatusDto();
    status.setMessage("subfleet updated.");

    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("111", Collections.emptyMap()));

    Mockito.when(subFleetDAO.getSubFleetById(Mockito.eq("111"))).thenReturn(oldIds);

    Mockito
        .when(
            subFleetDAO.saveSubFleet((Mockito.eq("111")), Mockito.anyListOf(Long.class), Mockito.anyListOf(Long.class)))
        .thenReturn(status);

    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles);

    StatusDto statusChanged = subFleetService.saveSubFleet("111", subfleet);
  }

  /**
   * Tests success audit scenario change user subfleet move all accessableIds to restricted
   * {@link SubFleetService#saveSubFleet(String, SubFleetDto)}.
   *
   * @throws SQLException error in getting subfleet for target user.
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   */
  @Test
  public final void testSaveSubFleetAll() throws SQLException, RecordNotFoundException, ClassDirectException {

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("111");
    userProfiles.setEmail(BOB_USER_EMAIL);

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);
    userProfiles.setRestrictAll(false);

    UserProfiles userProfilesNew = new UserProfiles();
    userProfilesNew.setUserId("111");
    userProfilesNew.setRestrictAll(true);
    userProfilesNew.setEmail(BOB_USER_EMAIL);

    SubFleetOptimizedDto subfleet = new SubFleetOptimizedDto();

    List<Long> oldIds = new ArrayList<>();

    List<Long> accAssetIds = new ArrayList<>();
    accAssetIds.add(3L);
    accAssetIds.add(4L);

    subfleet.setMoveTo(SubfleetMoveOperationEnum.RESTRICTED.getName());

    subfleet.setIncludedAssetIds(accAssetIds);

    StatusDto status = new StatusDto();
    status.setMessage("subfleet updated.");

    UserProfiles adminUser = new UserProfiles();
    adminUser.setUserId("112");
    adminUser.setEmail("admin.classdirect@gmail.com");

    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("112", Collections.emptyMap()));
    Mockito.when(userProfileDao.getUser(Mockito.eq("112"))).thenReturn(adminUser);

    Mockito.when(subFleetDAO.getSubFleetById(Mockito.eq("111"))).thenReturn(oldIds);

    Mockito
        .when(
            subFleetDAO.saveSubFleet((Mockito.eq("111")), Mockito.anyListOf(Long.class), Mockito.anyListOf(Long.class)))
        .thenReturn(status);

    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(Collections.emptyList());
    final PaginationDto pagination = new PaginationDto();
    pagination.setTotalPages(1);
    pagination.setTotalElements(2L);
    assetPageResource.setPagination(pagination);
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), any(), any()))
        .thenReturn(Calls.response(assetPageResource));

    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles, userProfilesNew);
    StatusDto statusChanged = subFleetService.saveSubFleetOptimized("111", subfleet);
  }

  /**
   * Tests success audit scenario change user subfleet move all restricted to accessable
   * {@link SubFleetService#saveSubFleet(String, SubFleetDto)}.
   *
   * @throws SQLException error in getting subfleet for target user.
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   */
  @Test
  public final void testSaveSubFleetAllToAccessible()
      throws SQLException, RecordNotFoundException, ClassDirectException {

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("111");
    userProfiles.setEmail(BOB_USER_EMAIL);

    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);
    userProfiles.setRestrictAll(false);

    UserProfiles userProfilesNew = new UserProfiles();
    userProfilesNew.setUserId("111");
    userProfilesNew.setRestrictAll(true);
    userProfilesNew.setEmail(BOB_USER_EMAIL);

    SubFleetOptimizedDto subfleet = new SubFleetOptimizedDto();

    List<Long> oldIds = new ArrayList<>();
    oldIds.add(1L);
    oldIds.add(2L);

    List<Long> accAssetIds = new ArrayList<>();

    subfleet.setMoveTo(SubfleetMoveOperationEnum.ACCESSIBLE.getName());

    subfleet.setIncludedAssetIds(accAssetIds);

    StatusDto status = new StatusDto();
    status.setMessage("subfleet updated.");

    UserProfiles adminUser = new UserProfiles();
    adminUser.setUserId("112");
    adminUser.setEmail("admin.classdirect@gmail.com");

    SecurityContextHolder.getContext().setAuthentication(new CDAuthToken("112", Collections.emptyMap()));
    Mockito.when(userProfileDao.getUser(Mockito.eq("112"))).thenReturn(adminUser);

    Mockito.when(subFleetDAO.getSubFleetById(Mockito.eq("111"))).thenReturn(oldIds);

    Mockito
        .when(
            subFleetDAO.saveSubFleet((Mockito.eq("111")), Mockito.anyListOf(Long.class), Mockito.anyListOf(Long.class)))
        .thenReturn(status);
    final BaseDtoPageResource assetPageResource = new BaseDtoPageResource();
    assetPageResource.setContent(Collections.emptyList());
    final PaginationDto pagination = new PaginationDto();
    pagination.setTotalPages(1);
    pagination.setTotalElements(2L);
    assetPageResource.setPagination(pagination);
    Mockito.when(assetRetrofitService.assetQueryAsBaseDto(Mockito.any(AssetQueryHDto.class), any(), any()))
        .thenReturn(Calls.response(assetPageResource));
    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles, userProfilesNew);

    StatusDto statusChanged = subFleetService.saveSubFleetOptimized("111", subfleet);
  }

  /**
   * Tests success audit scenario to update user EOR
   * {@link UserProfileService#updateUser(String, String, UserProfiles, String)} when delete EOR
   * permissions.
   *
   * @throws ClassDirectException if any error in execution flow.
   */
  @Test
  public final void deleteEORTest() throws ClassDirectException {

    Set<UserEOR> eors = new HashSet<>();
    UserEOR eor = new UserEOR();
    eor.setEorId(123L);
    eor.setAccessTmReport(true);
    eor.setImoNumber("123");
    eor.setUserId("111");
    eors.add(eor);

    UserEOR eorToDelete = new UserEOR();
    eorToDelete.setEorId(124L);
    eorToDelete.setAccessTmReport(true);
    eorToDelete.setImoNumber("124");
    eorToDelete.setUserId("111");
    eors.add(eorToDelete);

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("111");
    userProfiles.setEors(eors);

    Set<UserEOR> eors1 = new HashSet<>();
    UserEOR eor1 = new UserEOR();
    eor1.setEorId(123L);
    eor1.setAccessTmReport(true);
    eor1.setImoNumber("123");
    eor1.setUserId("111");
    eors1.add(eor1);

    UserProfiles userProfilesNew = new UserProfiles();
    userProfilesNew.setUserId("111");
    userProfilesNew.setEors(eors1);

    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("107");

    final StatusDto status = new StatusDto(successfulDelete);

    Mockito.when(userProfileDao.getUser(Mockito.eq("107"))).thenReturn(userProfiles1);

    Mockito.when(userProfileDao.getUser(Mockito.eq("111"))).thenReturn(userProfiles, userProfilesNew);


    Mockito.when(userProfileDao.removeEOR(Mockito.eq("111"), Mockito.eq("124L"))).thenReturn(status);

    StatusDto deleteStatus = userProfileService.deleteEOR("107", "111", "124L");

    Assert.assertEquals(String.format(successfulDelete), deleteStatus.getMessage());
  }

  /**
   * Tests success audit scenario to update user Last login
   * {@link UserProfileService#updateUserLastLogin(String, LocalDateTime)}.
   *
   * @throws RecordNotFoundException if no record found for target user or admin user performs
   *         request.
   * @throws ClassDirectException if any error in execution flow.
   * @throws JsonProcessingException if any error in parsing user object in logging.
   */
  @Test
  public final void updateUsersLastLoginChanged()
      throws RecordNotFoundException, ClassDirectException, JsonProcessingException {

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("107");
    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);

    Mockito.when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);

    Mockito.when(userProfileDao.getUser(Mockito.eq("107"))).thenReturn(userProfiles);
    when(userProfileDao.updateUserLastLogin("107", LocalDateTime.of(2017, 12, 9, 2, 20, 20))).thenReturn(1);

    userProfileService.updateUserLastLogin("107", LocalDateTime.of(2017, 12, 9, 2, 20, 20));
  }

  /**
   * @author VKolagutla
   *
   */
  @Configuration
  public static class Config extends BaseMockConfiguration {

    /**
     * @return AuditService Service.
     */
    @Bean
    @Override
    public AuditService auditService() {
      return new AuditServiceImpl();
    }

    /**
     * @return UserProfile Service.
     */
    @Bean
    @Override
    public UserProfileService userProfileService() {
      return new UserProfileServiceImpl();
    }

    /**
     * Return cache.
     *
     * @return cache object.
     */
    @Bean
    public CacheManager cacheManager() {
      // Configure and Return implementation of Spring's CacheManager.
      SimpleCacheManager cacheManager = new SimpleCacheManager();
      ConcurrentMapCache concurrentMapCache = new ConcurrentMapCache(CacheConstants.CACHE_USERS);
      ConcurrentMapCache concurrentSubfleeMapCache = new ConcurrentMapCache(CacheConstants.CACHE_SUBFLEET);
      cacheManager.setCaches(Arrays.asList(concurrentMapCache));
      return cacheManager;
    }

    /**
     * Create subfleet service.
     *
     * @return service.
     */
    @Override
    @Bean
    public SubFleetService subFleetService() {
      return new SubFleetServiceImpl();
    }

  }
}
