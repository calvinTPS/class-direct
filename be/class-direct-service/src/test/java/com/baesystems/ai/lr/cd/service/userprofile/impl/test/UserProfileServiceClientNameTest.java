package com.baesystems.ai.lr.cd.service.userprofile.impl.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.constants.CacheConstants;
import com.baesystems.ai.lr.cd.be.domain.dao.customer.CustomerRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.ihs.IhsRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.lrid.UserRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dao.reference.AssetReferenceRetrofitService;
import com.baesystems.ai.lr.cd.be.domain.dto.StatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserDto;
import com.baesystems.ai.lr.cd.be.domain.dto.azure.LRIDUserStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.PartyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.user.PartyQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserEOR;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.service.utils.UserProfileServiceUtils;
import com.baesystems.ai.lr.cd.service.Resources;
import com.baesystems.ai.lr.cd.service.reference.CustomerReferenceService;
import com.baesystems.ai.lr.cd.service.reference.FlagStateService;
import com.baesystems.ai.lr.cd.service.reference.impl.CustomerReferenceServiceImpl;
import com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.baesystems.ai.lr.cd.service.userprofile.impl.UserProfileServiceImpl;
import com.baesystems.ai.lr.dto.query.PartyQueryDto;

import retrofit2.mock.Calls;

/**
 * @author RKaneysan.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UserProfileServiceClientNameTest.Config.class)
@DirtiesContext
public class UserProfileServiceClientNameTest extends UserProfileServiceMockTestData {

  /**
   * Injected user profile dao.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  /**
   * Injected user profile service.
   */
  @Autowired
  private UserProfileService userProfileService;

  /**
   * Injected customer retrofit service.
   */
  @Autowired
  private CustomerRetrofitService customerRetrofitService;

  /**
   * Injects the {@link AssetReferenceRetrofitService} for testing.
   */
  @Autowired
  private AssetReferenceRetrofitService assetReferenceRetrofitService;

  /**
   * Injected flag state retrofit service.
   */
  @Autowired
  private FlagStateService flagStateService;
  /**
   * Injected userRetrofitService.
   */
  @Autowired
  private UserRetrofitService userRetrofitService;

  /**
   * The {@link IhsRetrofitService} from Spring context.
   */
  @Autowired
  private IhsRetrofitService ihsServiceDelegate;

  /**
   * External email filter.
   */
  @Value("${sitekit.email.filter}")
  private String externalEmailFilter;

  /**
   * The message for successful deletion of Eor.
   */
  @Value("${eor.successful.delete}")
  private String successfulDelete;

  /**
   * Test Case for Non-SSO User retrieval with client information.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUsersWithClientInformation() throws ClassDirectException {
    // Mock client information.
    List<PartyHDto> mockedPartyList = mockClientInfo(CLIENT_IMO_NUMBER, true);
    final PartyHDto mockedPartyDto = mockedPartyList.get(0);

    // Mock role for user profile.
    final Roles mockedRole = new Roles();
    mockedRole.setRoleId(Role.CLIENT.getId());
    mockedRole.setRoleName(Role.CLIENT.toString());

    final Set<Roles> mockedRoles = new HashSet<>();
    mockedRoles.add(mockedRole);

    UserProfiles mockedUser = new UserProfiles();
    mockedUser.setUserId(SUCCESS_USER_ID_10);
    mockedUser.setEmail(USR_CLIENT_USER_EMAIL);
    mockedUser.setClientCode(CLIENT_IMO_NUMBER);
    mockedUser.setRoles(mockedRoles);

    LRIDUserDto lridDto = getUpdatedAzureDto(mockedUser);
    String formattedEmails =
        UserProfileServiceUtils.getFormattedEmail(Arrays.asList(USR_CLIENT_USER_EMAIL), externalEmailFilter);
    // Populate Azure List when mock Azure call.
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(Arrays.asList(lridDto)));
    doReturn(mockedUser).when(userProfileDao).getUser(Mockito.anyString());
    when(assetReferenceRetrofitService.getCountryList()).thenReturn(Calls.response(mockCountryList()));
    when(customerRetrofitService.getClientInformationByClientImo(Mockito.any(Integer.class), Mockito.any(Integer.class),
        Mockito.any(PartyQueryHDto.class))).thenReturn(Calls.response(mockedPartyList));

    UserProfiles userprofile = userProfileService.getUser(mockedUser.getUserId());
    final Company company = userprofile.getCompany();

    assertNotNull(userprofile);
    assertEquals(userprofile.getUserId(), mockedUser.getUserId());
    assertEquals(userprofile.getClientCode(), mockedUser.getClientCode());
    assertEquals(userprofile.getEmail(), mockedUser.getEmail());
    assertEquals(userprofile.getClientCode(), mockedPartyDto.getImoNumber());
    assertEquals(company.getName(), mockedPartyDto.getName());
    assertEquals(company.getTelephone(), mockedPartyDto.getPhoneNumber());
    assertEquals(company.getAddressLine1(), mockedPartyDto.getAddressLine1());
    assertEquals(company.getAddressLine2(), mockedPartyDto.getAddressLine2());
    assertEquals(company.getAddressLine3(), mockedPartyDto.getAddressLine3());
    assertEquals(company.getPostCode(), mockedPartyDto.getPostcode());
    assertEquals(company.getCity(), mockedPartyDto.getCity());
    assertEquals(MALAYSIA_NAME, company.getCountry());
  }

  /**
   * Test Case for Non-SSO User retrieval with client information.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testUsersWithMutipleRoleWithClientInformation() throws ClassDirectException {
    // Mock client information.
    List<PartyHDto> mockedPartyList = mockClientInfo(CLIENT_IMO_NUMBER, true);
    final PartyHDto mockedPartyDto = mockedPartyList.get(0);

    // Mock role for user profile.
    final Roles mockedClientRole = new Roles();
    mockedClientRole.setRoleId(Role.CLIENT.getId());
    mockedClientRole.setRoleName(Role.CLIENT.toString());

    // Mock role for user profile.
    final Roles mockedEORRole = new Roles();
    mockedEORRole.setRoleId(Role.EOR.getId());
    mockedEORRole.setRoleName(Role.EOR.toString());

    final Set<Roles> mockedRoles = new HashSet<>();
    mockedRoles.add(mockedEORRole);
    mockedRoles.add(mockedClientRole);

    UserProfiles mockedUser = new UserProfiles();
    mockedUser.setUserId(SUCCESS_USER_ID_9);
    mockedUser.setEmail(OSCAR_USER_EMAIL);
    mockedUser.setClientCode(CLIENT_IMO_NUMBER);
    mockedUser.setRoles(mockedRoles);

    LRIDUserDto lridDto = getUpdatedAzureDto(mockedUser);
    String formattedEmails =
        UserProfileServiceUtils.getFormattedEmail(Arrays.asList(OSCAR_USER_EMAIL), externalEmailFilter);
    // Populate Azure List when mock Azure call.
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(Arrays.asList(lridDto)));
    doReturn(mockedUser).when(userProfileDao).getUser(Mockito.anyString());
    when(assetReferenceRetrofitService.getCountryList()).thenReturn(Calls.response(mockCountryList()));
    when(customerRetrofitService.getClientInformationByClientImo(Mockito.any(Integer.class), Mockito.any(Integer.class),
        Mockito.any(PartyQueryHDto.class))).thenReturn(Calls.response(mockedPartyList));

    UserProfiles userprofile = userProfileService.getUser(mockedUser.getUserId());
    final Company company = userprofile.getCompany();

    assertNotNull(userprofile);
    assertEquals(userprofile.getUserId(), mockedUser.getUserId());
    assertEquals(userprofile.getClientCode(), mockedUser.getClientCode());
    assertEquals(userprofile.getEmail(), mockedUser.getEmail());
    assertEquals(userprofile.getClientCode(), mockedPartyDto.getImoNumber());
    assertEquals(company.getName(), mockedPartyDto.getName());
    assertEquals(company.getTelephone(), mockedPartyDto.getPhoneNumber());
    assertEquals(company.getAddressLine1(), mockedPartyDto.getAddressLine1());
    assertEquals(company.getAddressLine2(), mockedPartyDto.getAddressLine2());
    assertEquals(company.getAddressLine3(), mockedPartyDto.getAddressLine3());
    assertEquals(company.getPostCode(), mockedPartyDto.getPostcode());
    assertEquals(company.getCity(), mockedPartyDto.getCity());
    assertEquals(MALAYSIA_NAME, company.getCountry());
  }

  /**
   * Tests on {@link UserProfileService#getUser(String)} method to retrieve ship builder
   * information.
   *
   * @throws ClassDirectException if there is error on external API to fetch ship builder
   *         information.
   */
  @Test
  public final void testGetUsersWithShipBuilderInfo() throws ClassDirectException {

    // Mock role for user profile.
    final Roles mockedRole = new Roles();
    mockedRole.setRoleId(Role.SHIP_BUILDER.getId());
    mockedRole.setRoleName(Role.SHIP_BUILDER.toString());

    final Set<Roles> mockedRoles = new HashSet<>();
    mockedRoles.add(mockedRole);

    UserProfiles mockedUser = new UserProfiles();
    mockedUser.setUserId(SUCCESS_USER_ID_7);
    mockedUser.setEmail(DAVID_USER_EMAIL);
    mockedUser.setShipBuilderCode("1000003");
    mockedUser.setRoles(mockedRoles);

    LRIDUserDto lridDto = getUpdatedAzureDto(mockedUser);
    String formattedEmails =
        UserProfileServiceUtils.getFormattedEmail(Arrays.asList(DAVID_USER_EMAIL), externalEmailFilter);
    // Populate Azure List when mock Azure call.
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(Arrays.asList(lridDto)));
    doReturn(mockedUser).when(userProfileDao).getUser(Mockito.anyString());
    when(assetReferenceRetrofitService.getCountryList()).thenReturn(Calls.response(mockCountryList()));
    List<PartyHDto> mockedPartyList = mockClientInfo("1000003", true);
    final PartyHDto mockedPartyDto = mockedPartyList.get(0);
    when(customerRetrofitService.getClientInformationByClientImo(Mockito.any(Integer.class), Mockito.any(Integer.class),
        Mockito.any(PartyQueryHDto.class))).thenReturn(Calls.response(mockedPartyList));

    UserProfiles userprofile = userProfileService.getUser(mockedUser.getUserId());
    final Company company = userprofile.getCompany();

    assertNotNull(userprofile);
    assertEquals(mockedUser.getUserId(), userprofile.getUserId());
    assertEquals(mockedUser.getShipBuilderCode(), userprofile.getShipBuilderCode());
    assertEquals(mockedUser.getEmail(), userprofile.getEmail());
    assertEquals(mockedPartyDto.getName(), company.getName());
    assertEquals(lridDto.getTelephoneNumber(), company.getTelephone());
    assertEquals("marthalli", company.getAddressLine2());
    assertEquals("street no 1", company.getAddressLine1());
    assertEquals("BNG", company.getAddressLine3());
    assertEquals(lridDto.getRcaAddressLine1(), company.getAddressLine1());
    assertEquals(lridDto.getRcaAddressLine2(), company.getAddressLine2());
    assertEquals(lridDto.getRcaAddressLine3(), company.getAddressLine3());
    assertEquals(lridDto.getRcaCity(), company.getCity());
    assertEquals(lridDto.getRcaCountry(), company.getCountry());
    assertEquals(lridDto.getRcaZipPostalCode(), company.getPostCode());
  }

  /**
   * Test Case for Non-SSO User retrieval with flag information.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUsersWithFlagInformation() throws ClassDirectException {
    // Mock client information.
    final List<FlagStateHDto> flagList = mockFlagStateList();
    final FlagStateHDto flagDto =
        flagList.parallelStream().filter(flag -> flag.getFlagCode().equals(MALAYSIA_FLAG_CODE)).findFirst().get();

    // Mock role for user profile.
    final Roles mockedRole = new Roles();
    mockedRole.setRoleId(Role.FLAG.getId());
    mockedRole.setRoleName(Role.FLAG.toString());

    final Set<Roles> mockedRoles = new HashSet<>();
    mockedRoles.add(mockedRole);

    UserProfiles mockedUser = new UserProfiles();
    mockedUser.setUserId(SUCCESS_USER_ID_6);
    mockedUser.setEmail(CHARLIE_USER_EMAIL);
    mockedUser.setFlagCode(MALAYSIA_FLAG_CODE);
    mockedUser.setRoles(mockedRoles);

    LRIDUserDto lridDto = getUpdatedAzureDto(mockedUser);
    String formattedEmails =
        UserProfileServiceUtils.getFormattedEmail(Arrays.asList(CHARLIE_USER_EMAIL), externalEmailFilter);
    // Populate Azure List when mock Azure call.
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(Arrays.asList(lridDto)));
    doReturn(mockedUser).when(userProfileDao).getUser(Mockito.anyString());
    when(flagStateService.getFlagStates()).thenReturn(flagList);

    UserProfiles userprofile = userProfileService.getUser(mockedUser.getUserId());
    final Company company = userprofile.getCompany();

    assertNotNull(userprofile);
    assertEquals(userprofile.getUserId(), mockedUser.getUserId());
    assertEquals(userprofile.getFlagCode(), mockedUser.getFlagCode());
    assertEquals(userprofile.getEmail(), mockedUser.getEmail());
    assertEquals(userprofile.getFlagCode(), flagDto.getFlagCode());
    assertEquals(company.getName(), flagDto.getName());
  }

  /**
   * Test Case for Non-SSO User retrieval with flag information.
   *
   * @throws ClassDirectException exception.
   */
  @Test
  public final void testGetUsersWithoutFlagInformation() throws ClassDirectException {
    // Mock client information.
    final List<FlagStateHDto> flagList = mockFlagStateList();

    // Mock role for user profile.
    final Roles mockedRole = new Roles();
    mockedRole.setRoleId(Role.FLAG.getId());
    mockedRole.setRoleName(Role.FLAG.toString());

    final Set<Roles> mockedRoles = new HashSet<>();
    mockedRoles.add(mockedRole);

    UserProfiles mockedUser = new UserProfiles();
    mockedUser.setUserId(SUCCESS_USER_ID_8);
    mockedUser.setEmail(ABC_USER_EMAIL);
    mockedUser.setRoles(mockedRoles);
    mockedUser.setLastLogin(LocalDateTime.now());

    LRIDUserDto lridDto = getUpdatedAzureDto(mockedUser);
    String formattedEmails =
        UserProfileServiceUtils.getFormattedEmail(Arrays.asList(ABC_USER_EMAIL), externalEmailFilter);
    // Populate Azure List when mock Azure call.
    when(userRetrofitService.getAzureUsers(formattedEmails)).thenReturn(Calls.response(Arrays.asList(lridDto)));
    doReturn(mockedUser).when(userProfileDao).getUser(Mockito.anyString());
    when(flagStateService.getFlagStates()).thenReturn(flagList);

    UserProfiles userprofile = userProfileService.getUser(mockedUser.getUserId());
    assertNotNull(userprofile.getCompany());
    assertNull(userprofile.getCompany().getName());
  }

  /**
   * Test Case for successful User retrieval.
   *
   * @throws Exception exception.
   */
  @Test
  public final void getClientInformation() throws Exception {
    // Mock client information.
    List<PartyHDto> mockedPartyList = mockClientInfo(CLIENT_IMO_NUMBER, true);
    final PartyHDto mockedPartyDto = mockedPartyList.get(0);

    when(customerRetrofitService.getClientInformationByClientImo(Mockito.any(Integer.class), Mockito.any(Integer.class),
        Mockito.any(PartyQueryDto.class))).thenReturn(Calls.response(mockedPartyList));
    final PartyQueryHDto query = new PartyQueryHDto();
    query.setImoNumber(CLIENT_CODE.toString());
    final List<PartyHDto> partyDtos = userProfileService.retrieveClientInformation(null, null, query);
    final PartyHDto partyDto = partyDtos.get(0);
    assertNotNull(partyDto);
    assertEquals(partyDto.getName(), mockedPartyDto.getName());
    assertEquals(partyDto.getPhoneNumber(), mockedPartyDto.getPhoneNumber());
    assertEquals(partyDto.getAddressLine1(), mockedPartyDto.getAddressLine1());
    assertEquals(partyDto.getAddressLine2(), mockedPartyDto.getAddressLine2());
    assertEquals(partyDto.getAddressLine3(), mockedPartyDto.getAddressLine3());
    assertEquals(partyDto.getPostcode(), mockedPartyDto.getPostcode());
    assertEquals(partyDto.getCity(), mockedPartyDto.getCity());
    assertEquals(partyDto.getCountry(), mockedPartyDto.getCountry());
  }

  /**
   * Test Case for successful User retrieval.
   *
   * @throws Exception exception.
   */
  @Test
  public final void getClientInformationFailedWithIOException() throws Exception {
    // Mock client information.
    when(customerRetrofitService.getClientInformationByClientImo(Mockito.any(Integer.class), Mockito.any(Integer.class),
        Mockito.any(PartyQueryDto.class)))
            .thenReturn(Calls.failure(new IOException("Mock Exception for Client information")));
    final PartyQueryHDto query = new PartyQueryHDto();
    query.setImoNumber(CLIENT_CODE.toString());
    userProfileService.retrieveClientInformation(null, null, query);
  }

  /**
   * Test Case for successful User update.
   *
   * @throws Exception Object.
   */
  @Test
  public final void updateUsersTestAzureFail() throws Exception {
    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(CLIENT_USER_ID);
    UserAccountStatus accountSatatus = new UserAccountStatus();
    accountSatatus.setName(AccountStatusEnum.ARCHIVED.getName());
    userProfiles.setStatus(accountSatatus);
    when(userProfileDao.updateUser(Mockito.anyString(), Mockito.anyString(), Mockito.any(UserProfiles.class),
        Mockito.isNull(String.class))).thenReturn(userProfiles);
    when(userProfileDao.getUser(CLIENT_ADMIN_USER_ID)).thenReturn(userProfiles);
    when(userRetrofitService.approveAzureUser(Mockito.any(LRIDUserStatusDto.class), Mockito.anyString()))
        .thenReturn(Calls.failure(new IOException("Mock exception")));
    UserProfiles user = userProfileService.updateUser(CLIENT_ADMIN_USER_ID, CLIENT_USER_ID, userProfiles, null);
    assertNotNull(userProfiles);
    assertNotNull(user);
    assertEquals(userProfiles.getUserId(), CLIENT_USER_ID);
    assertEquals(userProfiles.getStatus().getName(), AccountStatusEnum.ARCHIVED.getName());
  }


  /**
   * Tests success scenario to remove expired eor's
   * {@link UserProfileService#removeExpiredEorUserAccounts()}.
   *
   * @throws ClassDirectException if no user records for EOR user with imo number is found or error
   *         in delete eor.
   */
  @Test
  public final void removeExpiredUsers() throws ClassDirectException {
    when(userProfileDao.getExpiredEorUsers()).thenReturn(mockUserEors());

    UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId("130");
    userProfiles.setEors(new HashSet<UserEOR>(mockUserEors()));
    Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleName("CLIENT");
    roles.add(role);
    Roles role2 = new Roles();
    role2.setRoleName("EOR");
    roles.add(role2);
    userProfiles.setRoles(roles);

    UserProfiles userProfilesNew = new UserProfiles();
    userProfilesNew.setUserId("130");
    Set<Roles> rolesNew = new HashSet<>();
    Roles roleNew = new Roles();
    roleNew.setRoleName("CLIENT");
    rolesNew.add(roleNew);
    userProfilesNew.setRoles(rolesNew);

    final CDAuthToken auth = new CDAuthToken("test");
    final UserProfiles user = new UserProfiles();
    user.setUserId("101");
    auth.setDetails(user);
    SecurityContextHolder.getContext().setAuthentication(auth);
    UserProfiles userProfiles1 = new UserProfiles();
    userProfiles1.setUserId("101");

    final StatusDto status = new StatusDto(successfulDelete);

    Mockito.when(userProfileDao.getUser(Mockito.eq("101"))).thenReturn(userProfiles1);
    Mockito.when(userProfileDao.getUser(Mockito.eq("130"))).thenReturn(userProfiles, userProfilesNew);
    Mockito.when(userProfileDao.removeEOR(Mockito.eq("130"), Mockito.eq("7653986"))).thenReturn(status);
    Mockito.when(userProfileDao.removeEOR(Mockito.eq("130"), Mockito.eq("7653987"))).thenReturn(status);

    userProfileService.removeExpiredEorUserAccounts();

    Mockito.verify(userProfileDao, Mockito.times(2)).removeEOR(Mockito.any(), Mockito.any());
  }

  /**
   * Mocks list of expired user eors.
   *
   * @return list of expired user eor's.
   */
  public final List<UserEOR> mockUserEors() {
    List<UserEOR> eors = new ArrayList<>();

    UserEOR eor1 = new UserEOR();
    eor1.setEorId(1L);
    eor1.setAssetExpiryDate(LocalDate.now());
    eor1.setUserId("130");
    eor1.setImoNumber("7653986L");
    eors.add(eor1);
    UserEOR eor2 = new UserEOR();
    eor2.setEorId(2L);
    eor2.setAssetExpiryDate(LocalDate.now());
    eor2.setUserId("130");
    eor2.setImoNumber("7653986");
    eors.add(eor2);

    return eors;
  }

  /**
   * @author RKaneysan.
   */
  @Configuration
  @EnableCaching
  public static class Config extends BaseMockConfiguration {

    /**
     * @return UserProfile Service.
     */
    @Bean
    @Override
    public UserProfileService userProfileService() {
      return new UserProfileServiceImpl();
    }


    /**
     * @see com.baesystems.ai.lr.cd.service.test.BaseMockConfiguration#customerReferenceService()
     */
    @Override
    public CustomerReferenceService customerReferenceService() {
      return new CustomerReferenceServiceImpl();
    }

    /**
     * Returns the resource injector implementation for reference data lookup.
     *
     * @return the resource injector implementation.
     */
    @Bean
    public Resources resources() {
      return new Resources();
    }

    /**
     * Returns mocked userProfile and subfleet cache.
     *
     * @return cache object.
     */
    @Bean
    public CacheManager cacheManager() {
      // Configure and Return implementation of Spring's CacheManager.
      SimpleCacheManager cacheManager = new SimpleCacheManager();
      ConcurrentMapCache concurrentUserMapCache = new ConcurrentMapCache(CacheConstants.CACHE_USERS);
      ConcurrentMapCache concurrentSubfleetMapCache = new ConcurrentMapCache(CacheConstants.CACHE_SUBFLEET);
      cacheManager.setCaches(Arrays.asList(concurrentUserMapCache, concurrentSubfleetMapCache));
      return cacheManager;
    }
  }
}

