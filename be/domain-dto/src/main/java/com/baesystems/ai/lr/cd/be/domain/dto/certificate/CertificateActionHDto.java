package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.references.CertificateActionTakenDto;

/**
 * Created by fwijaya on 24/1/2017.
 */
public class CertificateActionHDto extends CertificateActionDto {

  /**
   * Certificate action taken.
   */
  @LinkedResource(referencedField = "actionTaken")
  private CertificateActionTakenDto actionTakenDto;

  /**
   * Getter.
   *
   * @return certificateactiontakendto object.
   */
  public final CertificateActionTakenDto getActionTakenDto() {
    return actionTakenDto;
  }

  /**
   * Setter.
   *
   * @param actionTakenDtoArg object.
   */
  public final void setActionTakenDto(final CertificateActionTakenDto actionTakenDtoArg) {
    this.actionTakenDto = actionTakenDtoArg;
  }
}
