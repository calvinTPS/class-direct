package com.baesystems.ai.lr.cd.be.domain.dto.export;

/**
 * checklist export dto.
 *
 * @author yng
 *
 */
public class ChecklistExportDto {

  /**
   * asset code.
   */
  private String assetCode;

  /**
   * service id.
   */
  private Long serviceId;

  /**
   * Getter for asset code.
   *
   * @return asset code.
   */
  public final String getAssetCode() {
    return assetCode;
  }

  /**
   * Setter for asset code.
   *
   * @param assetCodeObj value.
   */
  public final void setAssetCode(final String assetCodeObj) {
    this.assetCode = assetCodeObj;
  }

  /**
   * Getter for service id.
   *
   * @return service id.
   */
  public final Long getServiceId() {
    return serviceId;
  }

  /**
   * Setter for service id.
   *
   * @param serviceIdObj id.
   */
  public final void setServiceId(final Long serviceIdObj) {
    this.serviceId = serviceIdObj;
  }


}
