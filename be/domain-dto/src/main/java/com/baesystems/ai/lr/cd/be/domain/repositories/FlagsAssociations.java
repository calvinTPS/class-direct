package com.baesystems.ai.lr.cd.be.domain.repositories;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * DO for table FLAGS_ASSOCIATIONS.
 *
 * @author fwijaya on 19/4/2017.
 */
@Entity
@Table(name = "FLAGS_ASSOCIATIONS")
public class FlagsAssociations implements Serializable {

  /**
   * Intellij automatically generated serial version.
   */
  private static final long serialVersionUID = -5247605948080967352L;

  /**
   * Primary key.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;

  /**
   * Flag code.
   */
  @Column(name = "FLAG_CODE")
  private String flagCode;

  /**
   * Secondary flag code.
   */
  @Column(name = "SECONDARY_FLAG_CODE")
  private String secondaryFlagCode;

  /**
   * Returns the id.
   *
   * @return The id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param theId the Id.
   */
  public final void setId(final Long theId) {
    this.id = theId;
  }

  /**
   * Returns the flag code.
   *
   * @return Flag code.
   */
  public final String getFlagCode() {
    return flagCode;
  }

  /**
   * Sets the flag code.
   *
   * @param theFlagCode Flag code.
   */
  public final void setFlagCode(final String theFlagCode) {
    this.flagCode = theFlagCode;
  }

  /**
   * Returns the secondary flag code.
   *
   * @return The secondary flag code.
   */
  public final String getSecondaryFlagCode() {
    return secondaryFlagCode;
  }

  /**
   * Sets the secondary flag code.
   *
   * @param theSecondaryFlagCode The secondary flag code.
   */
  public final void setSecondaryFlagCode(final String theSecondaryFlagCode) {
    this.secondaryFlagCode = theSecondaryFlagCode;
  }

  /**
   * Returns String representation of this FlagsAssociations.
   *
   * @return String representation.
   */
  @Override
  public final String toString() {
    return "FlagsAssociations{" + "id=" + id + ", flagCode='" + flagCode + '\'' + ", secondaryFlagCode='"
      + secondaryFlagCode + '\'' + '}';
  }
}
