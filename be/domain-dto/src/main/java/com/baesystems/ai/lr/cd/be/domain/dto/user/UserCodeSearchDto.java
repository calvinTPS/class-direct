package com.baesystems.ai.lr.cd.be.domain.dto.user;

/**
 * @author syalavarthi
 *
 */
public class UserCodeSearchDto {
  /**
   * type.
   */
  private String type;
  /**
   * code.
   */
  private String code;

  /**
   * @return the type value.
   */
  public final String getType() {
    return type;
  }

  /**
   * @param typeObject to set.
   */
  public final void setType(final String typeObject) {
    this.type = typeObject;
  }

  /**
   * @return the code value.
   */
  public final String getCode() {
    return code;
  }

  /**
   * @param codeObject to set.
   */
  public final void setCode(final String codeObject) {
    this.code = codeObject;
  }

}
