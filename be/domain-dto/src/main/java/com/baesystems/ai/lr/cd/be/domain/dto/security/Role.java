package com.baesystems.ai.lr.cd.be.domain.dto.security;

import java.util.Locale;

import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;

/**
 * Enumerator class for Role.
 *
 * @author RKaneysan
 */
public enum Role {

  /**
   * EQUASIS Role values.
   */
  EQUASIS("EQUASIS", Role.PUBLIC_ACCESS_USER, -1L),
  /**
   * THETIS Role values.
   */
  THETIS("THETIS", Role.PUBLIC_ACCESS_USER, -1L),
  /**
   * CLIENT Role values.
   */
  CLIENT("CLIENT", Role.NORMAL_USER, 101L),
  /**
   * EXTERNAL_NON_CLIENT Role values.
   */
  EXTERNAL_NON_CLIENT("EXTERNAL_NON_CLIENT", Role.NORMAL_USER, 102L),
  /**
   * SHIP_BUILDER Role values.
   */
  SHIP_BUILDER("SHIP_BUILDER", Role.NORMAL_USER, 103L),
  /**
   * FLAG Role values.
   */
  FLAG("FLAG", Role.NORMAL_USER, 104L),
  /**
   * EOR Role values.
   */
  EOR("EOR", Role.NORMAL_USER, 105L),
  /**
   * LR_INTERNAL Role values.
   */
  LR_INTERNAL("LR_INTERNAL", Role.NORMAL_USER, 108L),
  /**
   * LR_SUPPORT Role values.
   */
  LR_SUPPORT("LR_SUPPORT", Role.PRIVILEGED_USER, 106L),
  /**
   * LR_ADMIN Role values.
   */
  LR_ADMIN("LR_ADMIN", Role.OVERALL_ADMIN, 107L);

  /**
   * Constants for the ranking of roles.
   */
  // TODO privileges of LR_SUPPORT after clarification.
  private static final int PUBLIC_ACCESS_USER = 0, NORMAL_USER = 1, PRIVILEGED_USER = 2, OVERALL_ADMIN = 3;

  /**
   * Role field.
   */
  private final String role;


  /**
   * Role rank, the higher the more power the role has.
   */
  private final int rank;

  /**
   * id of Role.
   */
  private Long id;

  /**
   * private constructor for role.
   *
   * @param roleIn role input parameter.
   * @param rankIn rank input parameter.
   * @param value value input parameter.
   */
  Role(final String roleIn, final int rankIn, final Long value) {
    this.role = roleIn;
    this.rank = rankIn;
    this.id = value;
  }


  /**
   * Get role rank.
   *
   * @return int role rank.
   */
  public int getRank() {
    return rank;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return value of id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * Maps a DB role to an enum role.
   *
   * @param dbRole the DB rolw we want to map.
   * @return the enum role matching.
   */
  public static Role mapFromRoles(final Roles dbRole) {
    Role role = null;
    switch (dbRole.getRoleName()) {
      case "CLIENT":
        role = CLIENT;
        break;
      case "EXTERNAL_NON_CLIENT":
        role = EXTERNAL_NON_CLIENT;
        break;
      case "SHIP_BUILDER":
        role = SHIP_BUILDER;
        break;
      case "FLAG":
        role = FLAG;
        break;
      case "EOR":
        role = EOR;
        break;
      case "LR_SUPPORT":
        role = LR_SUPPORT;
        break;
      case "LR_ADMIN":
        role = LR_ADMIN;
        break;
      case "LR_INTERNAL":
        role = LR_INTERNAL;
        break;
      default:
        role = null;
        break;
    }

    return role;
  }

  /**
   * toString for {@link #role}.
   *
   * @return role in String format.
   */
  public final String toString() {
    return role;
  }

  /**
   * toLowerCase for {@link #role}.
   *
   * @param locale Locale for Case.
   * @return role in String format.
   */
  public final String toLowerCase(final Locale locale) {
    return role.toLowerCase(locale);
  }

}
