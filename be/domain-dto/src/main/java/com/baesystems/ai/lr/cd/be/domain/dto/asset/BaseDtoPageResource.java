package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.paging.BasePageResource;


/**
 * @author syalavarthi
 *
 */
public class BaseDtoPageResource extends BasePageResource<BaseDto> {

  /**
   * List of assets.
   *
   */
  private List<BaseDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<BaseDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<BaseDto> assetList) {
    this.content = assetList;
  }
}
