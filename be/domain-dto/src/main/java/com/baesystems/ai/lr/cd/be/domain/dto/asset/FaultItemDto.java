package com.baesystems.ai.lr.cd.be.domain.dto.asset;

/**
 * @author syalavarthi.
 *
 */
public class FaultItemDto {
  /**
   * item name.
   */
  private String itemName;

  /**
   * Getter for itemName.
   *
   * @return itemName.
   */
  public final String getItemName() {
    return itemName;
  }

  /**
   * Setter for itemName.
   *
   * @param itemNameObj value.
   */
  public final void setItemName(final String itemNameObj) {
    this.itemName = itemNameObj;
  }

}
