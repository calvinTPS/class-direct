package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * @author RKaneysan
 *
 */
public class IhsEquipmentInfoDto implements Serializable {

  /**
   * Default serial version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Cable Length.
   */
  private String cableLength;

  /**
   * Cable Grade.
   */
  private String cableGrade;

  /**
   * Cable Diameter.
   */
  private String cableDiameter;

  /**
   * Equipment Letter.
   */
  private String equipmentLetter;

  /**
   * @return the cableLength.
   */
  public final String getCableLength() {
    return cableLength;
  }

  /**
   * @param newCableLength the cableLength to set.
   */
  public final void setCableLength(final String newCableLength) {
    this.cableLength = newCableLength;
  }

  /**
   * @return the cableGrade.
   */
  public final String getCableGrade() {
    return cableGrade;
  }

  /**
   * @param newCableGrade the cableGrade to set.
   */
  public final void setCableGrade(final String newCableGrade) {
    this.cableGrade = newCableGrade;
  }

  /**
   * @return the cableDiameter.
   */
  public final String getCableDiameter() {
    return cableDiameter;
  }

  /**
   * @param newCableDiameter the cableDiameter to set.
   */
  public final void setCableDiameter(final String newCableDiameter) {
    this.cableDiameter = newCableDiameter;
  }

  /**
   * @return the equipmentLetter.
   */
  public final String getEquipmentLetter() {
    return equipmentLetter;
  }

  /**
   * @param newEquipmentLetter the equipmentLetter to set.
   */
  public final void setEquipmentLetter(final String newEquipmentLetter) {
    this.equipmentLetter = newEquipmentLetter;
  }
}
