package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author RKaneysan
 */
public class AssetHeaderDto implements Serializable {

  /**
   * Serial Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * DATE_FORMAT_PERIOD_SEPARATED.
   */
  private static final String DATE_FORMAT_SEPARATED_BY_COMMA = "dd.MM.yyyy";

  /**
   * BLANK.
   */
  public static final String BLANK = "";

  /**
   * Asset Name.
   */
  private String name;

  /**
   * IMO Number for Asset.
   */
  private String imo;

  /**
   * Due Status for Asset.
   */
  private DueStatus dueStatus;

  /**
   * Asset Type.
   */
  private String type;

  /**
   * The asset emblem image.
   */
  private String emblemImage;

  /**
   * Date Of Build Asset.
   */
  private String dateOfBuild;

  /**
   * Asset Gross Tonnage.
   */
  private String grossTonnage;

  /**
   * Flag for Asset.
   */
  private String flag;

  /**
   * Asset Class Status.
   */
  private String classStatus;

  /**
   * Asset details link.
   */
  private String link;

  /**
   * Asset details client name.
   */
  private String clientName;
  /**
   * The asset has postponed service.
   */
  private Boolean hasPostponedService;

  /**
   * Due item list.
   */
  private final List<DueItemDto> list = new ArrayList<>();

  /**
   * Job report list.
   */
  private final List<JobItemDto> jobs = new ArrayList<>();

  /**
   * Actionable items list.
   */
  @Getter
  @Setter
  private List<DueItemDto> listActionableItem = new ArrayList<>();

  /**
   * Condition of class List.
   */
  @Getter
  @Setter
  private List<DueItemDto> listConditionOfClass = new ArrayList<>();

  /**
   * Statutory finding list.
   */
  @Getter
  @Setter
  private List<DueItemDto> listStatutoryFinding = new ArrayList<>();

  /**
   * Service list.
   */
  @Getter
  @Setter
  private List<DueItemDto> listService = new ArrayList<>();

  /**
   * @param date date.
   * @return formatted date.
   */
  public final String getDate(final Date date) {
    String stringDate = BLANK;
    if (Objects.nonNull(date)) {
      stringDate = DateUtils.getFormattedDatetoString(date, DATE_FORMAT_SEPARATED_BY_COMMA);
    }
    return stringDate;
  }

  /**
   * @return the name.
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameParam the name to set.
   */
  public final void setName(final String nameParam) {
    this.name = nameParam;
  }

  /**
   * @return the imo.
   */
  public final String getImo() {
    return imo;
  }

  /**
   * @param imoParam the imo to set.
   */
  public final void setImo(final String imoParam) {
    this.imo = imoParam;
  }

  /**
   * @return the status.
   */
  public final DueStatus getDueStatus() {
    return dueStatus;
  }

  /**
   * @param statusParam the status to set.
   */
  public final void setDueStatus(final DueStatus statusParam) {
    this.dueStatus = statusParam;
  }

  /**
   * @return the type.
   */
  public final String getType() {
    return type;
  }

  /**
   * @param typeParam the type to set.
   */
  public final void setType(final String typeParam) {
    this.type = typeParam;
  }

  /**
   * @return the emblem.
   */
  public final String getEmblemImage() {
    return emblemImage;
  }

  /**
   * @param emblemParam the emblem to set.
   */
  public final void setEmblemImage(final String emblemParam) {
    this.emblemImage = emblemParam;
  }

  /**
   * @return the dateOfBuild.
   */
  public final String getDateOfBuild() {
    return dateOfBuild;
  }

  /**
   * @param dateOfBuildParam the dateOfBuild to set.
   */
  public final void setDateOfBuild(final String dateOfBuildParam) {
    this.dateOfBuild = dateOfBuildParam;
  }

  /**
   * @return the grossTonnage.
   */
  public final String getGrossTonnage() {
    return grossTonnage;
  }

  /**
   * @param grossTonnageParam the grossTonnage to set.
   */
  public final void setGrossTonnage(final String grossTonnageParam) {
    this.grossTonnage = grossTonnageParam;
  }

  /**
   * @return the flag.
   */
  public final String getFlag() {
    return flag;
  }

  /**
   * @param flagParam the flag to set.
   */
  public final void setFlag(final String flagParam) {
    this.flag = flagParam;
  }

  /**
   * @return the classStatus.
   */
  public final String getClassStatus() {
    return classStatus;
  }

  /**
   * @param classStatusParam the classStatus to set
   */
  public final void setClassStatus(final String classStatusParam) {
    this.classStatus = classStatusParam;
  }

  /**
   * @return the link
   */
  public final String getLink() {
    return link;
  }

  /**
   * @param linkParam the link to set.
   */
  public final void setLink(final String linkParam) {
    this.link = linkParam;
  }

  /**
   * @return the clientName.
   */
  public final String getClientName() {
    return clientName;
  }

  /**
   * @param clientNameParam the clientName to set.
   */
  public final void setClientName(final String clientNameParam) {
    this.clientName = clientNameParam;
  }

  /**
   * Getter for {@link #list}.
   *
   * @return value of list.
   */
  public final List<DueItemDto> getList() {
    return list;
  }

  /**
   * Getter for {@link #jobs}.
   *
   * @return value of jobs.
   */
  public final List<JobItemDto> getJobs() {
    return jobs;
  }

  /**
   * Gets hasPostponedService for asset.
   *
   * @return hasPostponedService.
   */
  public final Boolean getHasPostponedService() {
    return hasPostponedService;
  }

  /**
   * Sets hasPostponedService for asset.
   *
   * @param hasPostponedServiceParam The flag for hasPostponedService.
   */
  public final void setHasPostponedService(final Boolean hasPostponedServiceParam) {
    this.hasPostponedService = hasPostponedServiceParam;
  }

}
