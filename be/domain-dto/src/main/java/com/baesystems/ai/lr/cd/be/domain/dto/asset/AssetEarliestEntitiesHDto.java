package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.assets.AssetEarliestEntitiesDto;

/**
 * @author VKolagutla
 */
public class AssetEarliestEntitiesHDto extends AssetEarliestEntitiesDto {

  /**
   * Default serial version UID.
   */
  private static final long serialVersionUID = -5356324571441182835L;

}

