package com.baesystems.ai.lr.cd.be.domain.dto.port;

/**
 * @author vkovuru
 *
 */
public class PortOfRegistryInputDto {

  /**
   * search name of the port.
   */
  private String search;

  /**
   * limit no.of ports.
   */
  private Integer limit;

  /**
   * @return search
   */
  public final String getSearch() {
    return search;
  }

  /**
   * @param searchval port name string
   */
  public final void setSearch(final String searchval) {
    this.search = searchval;
  }

  /**
   * @return limit value.
   */
  public final Integer getLimit() {
    return limit;
  }

  /**
   * @param limitval no.of ports.
   */
  public final void setLimit(final Integer limitval) {
    this.limit = limitval;
  }

}
