package com.baesystems.ai.lr.cd.be.utils;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.ServiceQueryDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;

/**
 * @author Faizal Sidek
 */
public final class AssetCodeUtil {

  /**
   * Privatized constructor.
   */
  private AssetCodeUtil() {

  }

  /**
   * The logger for ServiceReferenceService {@link ServiceReferenceServiceImpl}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AssetCodeUtil.class);

  /**
   * Parse id from code.
   *
   * @param code asset code.
   * @return id.
   */
  public static Long getId(final String code) {
    final Long id;
    if (code.startsWith(CDConstants.MAST_PREFIX) || code.startsWith(CDConstants.IHS_PREFIX)) {
      id = Long.parseLong(code.substring(CDConstants.PREFIX_LENGTH));
    } else {
      id = Long.parseLong(code);
    }

    return id;
  }

  /**
   * Generate MAST asset code.
   *
   * @param id asset id.
   * @return asset code.
   */
  public static String getMastCode(final Long id) {
    return CDConstants.MAST_PREFIX + id;
  }

  /**
   * Generate IHS asset code.
   *
   * @param id asset id.
   * @return asset code.
   */
  public static String getIhsCode(final Long id) {
    return CDConstants.IHS_PREFIX + id;
  }

  /**
   * Parse source from code.
   *
   * @param code asset code.
   * @return source of asset.
   */
  public static String getAssetSource(final String code) {
    final String source;
    if (code.startsWith(CDConstants.MAST_PREFIX) || code.startsWith(CDConstants.IHS_PREFIX)) {
      source = code.substring(CDConstants.LIMIT_ZERO, CDConstants.PREFIX_LENGTH);
    } else {
      source = null;
    }

    return source;
  }

  /**
   * Converts mast DTO to hydrated CD DTO.
   *
   * @param assetTypeDto the assetTypeDto.
   * @return hydrated asset type object.
   */
  public static AssetTypeHDto convertDtoToHDto(final AssetTypeDto assetTypeDto) {

    final AssetTypeHDto assetTypeHDto = new AssetTypeHDto();
    BeanUtils.copyProperties(assetTypeDto, assetTypeHDto);

    return assetTypeHDto;
  }

  /**
   * Returns the asset IMO number from asset model.
   *
   * @param asset the asset model.
   * @return the imo number for the asset or else null;
   */
  public static String getAssetImo(final AssetHDto asset) {
    String imo = asset.getImoNumber();
    if (StringUtils.isEmpty(imo)) {
      imo = Optional.ofNullable(asset.getIhsAssetDto()).map(IhsAssetDetailsDto::getId).orElse("");
    }
    return imo;
  }

  /**
   * Reads all the fields from ServiceQueryDto Class and put it in map and return it.
   *
   * @param queryDto queryDto.
   * @return query to map.
   */
  public static Map<String, String> serviceQuerytoQueryMap(final ServiceQueryDto queryDto) {
    final Map<String, String> query = new HashMap<>();
    final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    final Class<ServiceQueryDto> clazz = ServiceQueryDto.class;
    Stream.of(clazz.getDeclaredFields()).forEach(field -> {
      try {
        LOGGER.debug("Trying to create query String from field {}", field.getName());
        final String fieldString = field.getName();
        final String methodName = "get" + fieldString.substring(0, 1).toUpperCase() + fieldString.substring(1);
        String value = "";
        final Method getMethod = clazz.getMethod(methodName);
        final Object methodResult = getMethod.invoke(queryDto);
        if (methodResult instanceof List) {
          final List<Object> list = (List<Object>) methodResult;
          value = list.stream().map(Object::toString).collect(Collectors.joining(","));
        } else if (methodResult instanceof Date) {
          value = dateFormatter.format((Date) methodResult);
        } else {
          value = methodResult.toString();
        }
        query.put(fieldString, value);
      } catch (NullPointerException | NoSuchMethodException | SecurityException | IllegalAccessException
          | IllegalArgumentException | InvocationTargetException | ClassCastException e) {
        LOGGER.debug("Can't  create query String from field {}", field.getName());
      }
    });
    LOGGER.debug("Query string : {}", query);
    return query;
  }
}
