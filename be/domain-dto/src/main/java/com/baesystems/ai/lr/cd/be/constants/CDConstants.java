package com.baesystems.ai.lr.cd.be.constants;

/**
 * Class-Direct Constants.
 *
 * @author vkovuru
 */
public final class CDConstants {

  /**
   * Private Constructor for Class-Direct Constants.
   */
  private CDConstants() {

  }

  /**
   * Prefix for mast asset.
   */
  public static final String MAST_PREFIX = "LRV";

  /**
   * Prefix for IHS asset.
   */
  public static final String IHS_PREFIX = "IHS";

  /**
   * Default prefix size.
   */
  public static final Integer PREFIX_LENGTH = 3;

  /**
   * default limit no.of ports returned per call.
   */
  public static final Integer DEFAULT_LIMIT = 20;
  /**
   * limit zero.
   */
  public static final Integer LIMIT_ZERO = 0;

  /**
   * email pattern.
   */
  public static final String EMAIL_PATTERN = "^(?:(?:[\\w`~!#$%^&*\\-=+;:{}\'|,?\\/]+(?:(?:\\.(?:\"(?:\\\\?[\\w`"
      + "~!#$%^&*\\-=+;:{}\'|,?\\/\\.()<>\\[\\] @]|\\\\\"|\\\\\\\\)*\"|["
      + "\\w`~!#$%^&*\\-=+;:{}\'|,?\\/]+))*\\.[\\w`~!#$%^&*\\-=+;:{}\'|,?"
      + "\\/]+)?)|(?:\"(?:\\\\?[\\w`~!#$%^&*\\-=+;:{}\'|,?\\/\\.()<>\\[\\]"
      + " @]|\\\\\"|\\\\\\\\)+\"))@(?:[a-zA-Z\\d\\-]+(?:\\.[a-zA-Z\\d\\-]+)*"
      + "|\\[\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\])$";

  /**
   * Delimiter to split roles.
   */
  public static final String ROLE_DELIMITER = ",";
  /**
   * due status.
   */
  public static final String DUE_STATUS = "Due";

  /**
   * Role constant.
   */
  public static final String ROLE = "role";

  /**
   * Asset Hub path.
   */
  public static final String EQUASIS_THETIS_URL = "EquasisThetisUrl";

  /**
   * Equasis thetis asset id request attribute marker.
   */
  public static final String EQUASIS_THETIS_ASSET_ID = "AssetID";

  /**
   * Fixed delay.
   */
  public static final Long CHECK_AVAILABILITY_DELAY = 60000L;

  /**
   * availableProcessors.
   */
  public static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();
  /**
   * Masquerade Id header.
   */
  public static final String HEADER_MASQ_ID = "x-masq-id";
  /**
   * Request id for logger.
   */
  public static final String REQUEST_ID = "UUID";

  /**
   * Maximum uuid length.
   */
  public static final Integer MAX_UUID_LENGTH = 8;

  /**
   * The header Id .
   */
  public static final String HEADER_USER_ID = "userId";

  /**
   * The asset model hierarchy level limit to show parent items in FAR/FSR and ESP report.
   */
  public static final Integer LEVEL_TWO_HIERARCHY = 2;

}
