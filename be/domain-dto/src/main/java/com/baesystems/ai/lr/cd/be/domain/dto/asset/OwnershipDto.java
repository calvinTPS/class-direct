package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * Contact information Dto for Registered Owner, Group Owner, Operator, Ship Manager, Technical
 * Manager and Doc Company.
 *
 * @author RKaneysan
 */
public class OwnershipDto implements Serializable {

  /**
   * Default Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Registered Owner.
   */
  private UserPersonalInformationDto registeredOwner;

  /**
   * Group Owner.
   */
  private UserPersonalInformationDto groupOwner;

  /**
   * Operator.
   */
  private UserPersonalInformationDto operator;

  /**
   * Ship Manager.
   */
  private UserPersonalInformationDto shipManager;

  /**
   * Technical Manager.
   */
  private UserPersonalInformationDto technicalOwner;

  /**
   * DOC Company.
   */
  private UserPersonalInformationDto doc;

  /**
   * @return the registeredOwner
   */
  public final UserPersonalInformationDto getRegisteredOwner() {
    return registeredOwner;
  }

  /**
   * @param registeredOwnerParam the registeredOwner to set
   */
  public final void setRegisteredOwner(final UserPersonalInformationDto registeredOwnerParam) {
    this.registeredOwner = registeredOwnerParam;
  }

  /**
   * @return the groupOwner.
   */
  public final UserPersonalInformationDto getGroupOwner() {
    return groupOwner;
  }

  /**
   * @param groupOwnerParam the groupOwner to set.
   */
  public final void setGroupOwner(final UserPersonalInformationDto groupOwnerParam) {
    this.groupOwner = groupOwnerParam;
  }

  /**
   * @return the operator.
   */
  public final UserPersonalInformationDto getOperator() {
    return operator;
  }

  /**
   * @param operatorParam the operator to set
   */
  public final void setOperator(final UserPersonalInformationDto operatorParam) {
    this.operator = operatorParam;
  }

  /**
   * @return the shipManager
   */
  public final UserPersonalInformationDto getShipManager() {
    return shipManager;
  }

  /**
   * @param shipManagerParam the shipManagerDto to set
   */
  public final void setShipManager(final UserPersonalInformationDto shipManagerParam) {
    this.shipManager = shipManagerParam;
  }

  /**
   * @return the technicalOwner.
   */
  public final UserPersonalInformationDto getTechnicalManager() {
    return technicalOwner;
  }

  /**
   * @param technicalOwnerParam the technicalOwner to set.
   */
  public final void setTechnicalManager(final UserPersonalInformationDto technicalOwnerParam) {
    this.technicalOwner = technicalOwnerParam;
  }

  /**
   * @return the doc.
   */
  public final UserPersonalInformationDto getDoc() {
    return doc;
  }

  /**
   * @param docParam the doc to set.
   */
  public final void setDoc(final UserPersonalInformationDto docParam) {
    this.doc = docParam;
  }

}
