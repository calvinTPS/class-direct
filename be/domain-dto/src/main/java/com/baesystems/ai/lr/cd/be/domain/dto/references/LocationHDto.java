package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CountryHDto;
import com.baesystems.ai.lr.dto.references.LocationDto;
import lombok.Getter;
import lombok.Setter;

/**
 * Location HDTO.
 *
 * @author fwijaya on 5/5/2017.
 */
public class LocationHDto extends LocationDto {

  /**
   * Linked to {@link CountryHDto}.
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "country")
  private CountryHDto countryDto;

}
