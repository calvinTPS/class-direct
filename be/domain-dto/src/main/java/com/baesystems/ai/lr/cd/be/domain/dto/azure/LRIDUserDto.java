package com.baesystems.ai.lr.cd.be.domain.dto.azure;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author VKolagutla
 *
 */
public class LRIDUserDto implements Serializable {

  /**
   * Generated serial UID.
   */
  private static final long serialVersionUID = 456522339971221639L;

  /**
   * objectId.
   */
  @Getter
  @Setter
  private String objectId;
  /**
   * password.
   */
  @Getter
  @Setter
  private PasswordProfile passwordProfile;
  /**
   * Account enable flag.
   */
  @Getter
  @Setter
  private Boolean accountEnabled = Boolean.TRUE;

  /**
   * creationType (Used LocalAccount).
   */
  @Getter
  @Setter
  private String creationType;

  /**
   * email.
   */
  @Getter
  @Setter
  private String signInName;

  /**
   * given name.
   */
  @Getter
  @Setter
  private String givenName;

  /**
   * surname.
   */
  @Getter
  @Setter
  private String surname;
  /**
   * mobile.
   */
  @Getter
  @Setter
  private String mobile;
  /**
   * title.
   */
  @Getter
  @Setter
  private String title;
  /**
   * displayName.
   */
  @Getter
  @Setter
  private String displayName;

  /**
   * userPrincipalName.
   */
  @Getter
  @Setter
  private String userPrincipalName;

  /**
   * organisationalRole.
   */
  @Getter
  @Setter
  private String organisationalRole;

  /**
   * rca_organisationName.
   */
  @JsonProperty("rca_organisationName")
  @Getter
  @Setter
  private String rcaOrganisationName;
  /**
   * rca_addressLine1.
   */
  @JsonProperty("rca_addressLine1")
  @Getter
  @Setter
  private String rcaAddressLine1;
  /**
   * rca_addressLine2.
   */
  @JsonProperty("rca_addressLine2")
  @Getter
  @Setter
  private String rcaAddressLine2;
  /**
   * rca_addressLine3.
   */
  @JsonProperty("rca_addressLine3")
  @Getter
  @Setter
  private String rcaAddressLine3;
  /**
   * rca_city.
   */
  @JsonProperty("rca_city")
  @Getter
  @Setter
  private String rcaCity;
  /**
   * rca_stateProvinceRegion.
   */
  @JsonProperty("rca_stateProvinceRegion")
  @Getter
  @Setter
  private String rcaStateProvinceRegion;
  /**
   * rca_zipPostalCode.
   */
  @JsonProperty("rca_zipPostalCode")
  @Getter
  @Setter
  private String rcaZipPostalCode;
  /**
   * rca_country.
   */
  @JsonProperty("rca_country")
  @Getter
  @Setter
  private String rcaCountry;
  /**
   * sa_addressLine1.
   */
  @JsonProperty("sa_addressLine1")
  @Getter
  @Setter
  private String saAddressLine1;
  /**
   * sa_addressLine2.
   */
  @JsonProperty("sa_addressLine2")
  @Getter
  @Setter
  private String saAddressLine2;
  /**
   * sa_addressLine3.
   */
  @JsonProperty("sa_addressLine3")
  @Getter
  @Setter
  private String saAddressLine3;
  /**
   * sa_city.
   */
  @JsonProperty("sa_city")
  @Getter
  @Setter
  private String saCity;
  /**
   * sa_stateProvinceRegion.
   */
  @JsonProperty("sa_stateProvinceRegion")
  @Getter
  @Setter
  private String saStateProvinceRegion;
  /**
   * sa_zipPostalCode.
   */
  @JsonProperty("sa_zipPostalCode")
  @Getter
  @Setter
  private String saZipPostalCode;
  /**
   * sa_country.
   */
  @JsonProperty("sa_country")
  @Getter
  @Setter
  private String saCountry;

  /**
   * telephoneNumber.
   */
  @Getter
  @Setter
  private String telephoneNumber;

  /**
   * Flag to indicate whether this user is internal user.
   */
  @Getter
  @Setter
  private boolean isInternalUser;

  @Override
  public final String toString() {
    return "LRIDUserDto [objectId=" + objectId + ", accountEnabled=" + accountEnabled + ", passwordProfile="
        + passwordProfile + ", creationType=" + creationType + ", signInName=" + signInName + ", givenName=" + givenName
        + ", surname=" + surname + ", mobile=" + mobile + ", title=" + title + ", displayName=" + displayName
        + ", userPrincipalName=" + userPrincipalName + ", organisationalRole=" + organisationalRole
        + ", rcaOrganisationName=" + rcaOrganisationName + ", rcaAddressLine1=" + rcaAddressLine1 + ", rcaAddressLine2="
        + rcaAddressLine2 + ", rcaAddressLine3=" + rcaAddressLine3 + ", rcaCity=" + rcaCity
        + ", rcaStateProvinceRegion=" + rcaStateProvinceRegion + ", rcaZipPostalCode=" + rcaZipPostalCode
        + ", rcaCountry=" + rcaCountry + ", saAddressLine1=" + saAddressLine1 + ", saAddressLine2=" + saAddressLine2
        + ", saAddressLine3=" + saAddressLine3 + ", saCity=" + saCity + ", saStateProvinceRegion="
        + saStateProvinceRegion + ", saZipPostalCode=" + saZipPostalCode + ", saCountry=" + saCountry
        + ", telephoneNumber=" + telephoneNumber + ", isInternalUser=" + isInternalUser + "]";
  }

  /**
   * @author YWearn
   *
   */
  public static class PasswordProfile {

    /**
     * Flag for change password on next login.
     */
    @Getter
    @Setter
    private Boolean forceChangePasswordNextLogin = Boolean.FALSE;

    /**
     * User password.
     */
    @Getter
    @Setter
    private String password;

    @Override
    public final String toString() {
      return "PasswordProfile [forceChangePasswordNextLogin=" + forceChangePasswordNextLogin + ", password=" + password
          + "]";
    }

  }
}
