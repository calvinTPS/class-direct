package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * The MajorNCNStatusHDto extends ReferenceDataDto.
 *
 * @author syalavarthi.
 *
 */
public class MajorNCNStatusHDto extends ReferenceDataDto {
  /**
   * The static serialVersionUID.
   */
  private static final long serialVersionUID = -2338351860284373556L;
}
