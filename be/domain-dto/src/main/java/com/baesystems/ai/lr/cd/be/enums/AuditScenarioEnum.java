package com.baesystems.ai.lr.cd.be.enums;

/**
 * Scenarios that can be used in audit user management.
 *
 * @author VKolagutla
 * @author syalavarthi 15-06-2017.
 *
 */
public enum AuditScenarioEnum {

  /**
   * Create User.
   */
  CREATE_USER("Create User."),
  /**
   * Create User.
   */
  RECREATE_ARCHIVED_USER("Recreate Archived User."),
  /**
   * Create User, EOR_Enabled.
   */
  CREATE_USER_WITH_EOR("Create User, EOR Enabled."),
  /**
   * Create User with TM Report.
   */
  CREATE_USER_WITH_TM("Create User with TM Report Permission."),
  /**
   * Update User.
   */
  UPDATE_USER("Update User."),
  /**
   * Update User, EOR Changed.
   */
  UPDATE_USER_EOR_CHANGED("Update User, EOR Changed."),
  /**
   * Update User, TM Changed.
   */
  UPDATE_USER_TM_CHANGED("Update User, TM Report Permission Changed."),
  /**
   * Update User, Status Changed.
   */
  UPDATE_USER_STATUS_CHANGED("Update User, Status Changed."),
  /**
   * Update User, ExpiryDate Changed.
   */
  UPDATE_USER_EXPIRYDATE_CHANGED("Update User, Account Expiry Date Changed."),
  /**
   * Save Subfleet.
   */
  SAVE_SUBFLEET("Save Subfleet."),
  /**
   * Save Subfleet.
   */
  SAVE_SUBFLEET_ALL("Save Subfleet."),

  /**
   * Update User last login.
   */
  UPDATE_USER_LAST_LOGIN("Update User last login."),

  /**
   * Update mpms tasks.
   */
  UPDATE_MPMS_TASKS("Credit MPMS task.");

  /**
   * The audit value.
   */
  private final String value;

  /**
   * @param valueObj the auditScenario.
   */
  AuditScenarioEnum(final String valueObj) {
    this.value = valueObj;
  }

  /**
   * @return auditScenario.
   */
  public final String getValue() {
    return value;
  }

}
