package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.Date;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.MajorNCNStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.dto.ncns.MajorNCNDto;

import lombok.Getter;
import lombok.Setter;

/**
 * MajorNCNHDto extends from MajorNCNDto.
 *
 * @author syalavarthi.
 *
 */
public class MajorNCNHDto extends MajorNCNDto {
  /**
   * Codicil type.
   */
  @Getter
  @Setter
  private String codicilType = CodicilType.MNCN.getName();
  /**
   * The static serialVersionUID.
   */
  private static final long serialVersionUID = 1L;
  /**
   * The Due date epoch.
   */
  @Getter
  @Setter
  private Long dueDateEpoch;
  /**
   * The Closure date epoch.
   */
  @Getter
  @Setter
  private Long closureDateEpoch;
  /**
   * mncn status.
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "status")
  private MajorNCNStatusHDto statusH;

  /**
   * Overrides setter for due date to calculate due date epoch and Sets due date epoch.
   *
   * @param dueDate value.
   */
  @Override
  public final void setDueDate(final Date dueDate) {
    super.setDueDate(dueDate);
    Long epoch = null;
    if (dueDate != null) {
      epoch = DateUtils.getDateToEpoch(dueDate);
      setDueDateEpoch(epoch);
    }
  }

  /**
   * Overrides setter for due date to calculate closure date epoch and Sets due date epoch.
   *
   * @param closureDate value.
   */
  @Override
  public final void setClosureDate(final Date closureDate) {
    super.setClosureDate(closureDate);
    Long epoch = null;
    if (closureDate != null) {
      epoch = DateUtils.getDateToEpoch(closureDate);
      setClosureDateEpoch(epoch);
    }
  }

}
