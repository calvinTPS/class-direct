package com.baesystems.ai.lr.cd.be.domain.dto.attachments;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;

/**
 * @author syalavarthi
 *
 */
public class SupplementaryInformationHDto extends SupplementaryInformationDto {

  /**
   *
   */
  private static final long serialVersionUID = 2914071954651428734L;

  /**
   * hydrate attachmentType.
   */
  @LinkedResource(referencedField = "attachmentType")
  private AttachmentTypeHDto attachmentTypeH;

  /**
   * hydrate attachmentType.
   */
  @LinkedResource(referencedField = "attachmentCategory")
  private AttachmentCategoryHDto attachmentCategoryH;
  /**
   * file token.
   */
  private String token;

  /**
   * Getter for {@link #token}.
   *
   * @return value of token.
   */
  public final String getToken() {
    return token;
  }

  /**
   * Setter for {@link #token}.
   *
   * @param fileToken value to be set.
   */
  public final void setToken(final String fileToken) {
    this.token = fileToken;
  }

  /**
   * @return value.
   */
  public final AttachmentTypeHDto getAttachmentTypeH() {
    return attachmentTypeH;
  }

  /**
   * @param attachmentTypeObj attachmentType.
   */
  public final void setAttachmentTypeH(final AttachmentTypeHDto attachmentTypeObj) {
    this.attachmentTypeH = attachmentTypeObj;
  }

  /**
   * @return value.
   */
  public final AttachmentCategoryHDto getAttachmentCategoryH() {
    return attachmentCategoryH;
  }

  /**
   * @param attachmentCategoryHObj attachmentCategory.
   */
  public final void setAttachmentCategoryH(final AttachmentCategoryHDto attachmentCategoryHObj) {
    this.attachmentCategoryH = attachmentCategoryHObj;
  }

}
