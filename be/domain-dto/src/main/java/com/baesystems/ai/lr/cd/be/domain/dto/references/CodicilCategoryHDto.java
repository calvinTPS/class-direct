package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.dto.references.CodicilCategoryDto;

/**
 * Asset category extending AssetCategoryDto.
 *
 * @author yng
 *
 */
public class CodicilCategoryHDto extends CodicilCategoryDto {
  /**
   * Static serial version.
   *
   */
  private static final long serialVersionUID = 2942305200107277185L;

  /**
   * Generate string representation.
   *
   */
  @Override
  public final String toString() {
    return "AssetCategory [getName()=" + getName() + ", getDescription()=" + getDescription() + "]";
  }
}

