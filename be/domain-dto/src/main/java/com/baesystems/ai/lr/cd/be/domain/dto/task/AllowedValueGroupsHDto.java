package com.baesystems.ai.lr.cd.be.domain.dto.task;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * Extended Regference data DTO.
 *
 * @author sYalavarthi.
 *
 */
public class AllowedValueGroupsHDto extends ReferenceDataDto {
  /**
   * the static serialVersionUID.
   */
  private static final long serialVersionUID = 1L;
}
