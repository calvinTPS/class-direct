package com.baesystems.ai.lr.cd.be.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum containing sorting options available on MAST-IHS query.
 *
 * @author Faizal Sidek
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SortOptionEnum {
  /**
   * Sort by class status name.
   */
  ClassStatusName("publishedVersion.classStatus.name"),

  /**
   * Sort by class status id.
   */
  ClassStatusId("publishedVersion.classStatus.id"),

  /**
   * Sort by asset type code.
   */
  AssetTypeCode("publishedVersion.assetType.code"),

  /**
   * Sort by asset type name.
   */
  AssetTypeName("publishedVersion.assetType.name"),

  /**
   * Sort by asset build date.
   */
  BuildDate("publishedVersion.buildDate"),

  /**
   * Sort by yard number.
   */
  YardNumber("publishedVersion.yardNumber"),

  /**
   * Sort by Flag code.
   */
  FlagCode("publishedVersion.flagState.flagCode"),

  /**
   * Sort by flag name.
   */
  FlagName("publishedVersion.flagState.name"),

  /**
   * Sort by asset name.
   */
  AssetName("publishedVersion.name"),

  /**
   * Sort by gross tonnage.
   */
  GrossTonnage("publishedVersion.grossTonnage"),

  /**
   * Sort by imo number.
   */
  IMONumber("publishedVersion.imoNumber"),

  /**
   * Sort by dead weight.
   */
  DeadWeight("publishedVersion.deadWeight"),
  /**
   * Sort by dead weight.
   */
  ClassSociety("publishedVersion.classList");

  /**
   * Value of sort field.
   */
  private String field;

  /**
   * Default constructor.
   *
   * @param sortField sort field.
   */
  SortOptionEnum(final String sortField) {
    this.field = sortField;
  }

  /**
   * Helper method to deserialize json.
   *
   * @param value enum value.
   * @return json.
   */
  @JsonCreator
  public static SortOptionEnum forValue(final String value) {
    for (final SortOptionEnum option : SortOptionEnum.values()) {
      if (option.name().equalsIgnoreCase(value)) {
        return option;
      }
    }

    return null;
  }

  /**
   * Getter for {@link #field}.
   *
   * @return value of field.
   */
  @JsonValue
  public final String getField() {
    return field;
  }
}
