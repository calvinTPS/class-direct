package com.baesystems.ai.lr.cd.be.domain.dto.security;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * MAST signature object.
 *
 * @author Faizal Sidek
 */
@Entity
@Table(name = "MAST_SIGNATURES")
public class MastSignature implements Serializable {

  /**
   * Serial version.
   */
  private static final long serialVersionUID = -8648984116882825556L;

  /**
   * Id for the signature.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "SIGNATURE_ID")
  private Integer id;

  /**
   * ID on mast. Default is cdf.
   */
  @Column(name = "MAST_ID", nullable = false)
  private String mastId;

  /**
   * Description for the signature.
   */
  @Column(name = "DESCRIPTION")
  private String description;

  /**
   * OpenSSL formatted private key.
   */
  @Column(name = "PRIVATE_KEY")
  private String privateKey;

  /**
   * OpenSSL formatted public key.
   */
  @Column(name = "PUBLIC_KEY")
  private String publicKey;

  /**
   * Encryption algorithm.
   */
  @Column(name = "ALGORITHM")
  private String algorithm;

  /**
   * Signature status.
   */
  @Column(name = "IS_ENABLED")
  private Boolean enabled;

  /**
   * Getter for {@link #id}.
   *
   * @return value of id.
   */
  public final Integer getId() {
    return id;
  }

  /**
   * Setter for {@link #id}.
   *
   * @param signatureId value to be set.
   */
  public final void setId(final Integer signatureId) {
    this.id = signatureId;
  }

  /**
   * Getter for {@link #description}.
   *
   * @return value of description.
   */
  public final String getDescription() {
    return description;
  }

  /**
   * Setter for {@link #description}.
   *
   * @param signatureDescription value to be set.
   */
  public final void setDescription(final String signatureDescription) {
    this.description = signatureDescription;
  }

  /**
   * Getter for {@link #privateKey}.
   *
   * @return value of privateKey.
   */
  public final String getPrivateKey() {
    return privateKey;
  }

  /**
   * Setter for {@link #privateKey}.
   *
   * @param signaturePrivateKey value to be set.
   */
  public final void setPrivateKey(final String signaturePrivateKey) {
    this.privateKey = signaturePrivateKey;
  }

  /**
   * Getter for {@link #publicKey}.
   *
   * @return value of publicKey.
   */
  public final String getPublicKey() {
    return publicKey;
  }

  /**
   * Setter for {@link #publicKey}.
   *
   * @param signaturePublicKey value to be set.
   */
  public final void setPublicKey(final String signaturePublicKey) {
    this.publicKey = signaturePublicKey;
  }

  /**
   * Getter for {@link #algorithm}.
   *
   * @return value of algorithm.
   */
  public final String getAlgorithm() {
    return algorithm;
  }

  /**
   * Setter for {@link #algorithm}.
   *
   * @param signatureAlgorithm value to be set.
   */
  public final void setAlgorithm(final String signatureAlgorithm) {
    this.algorithm = signatureAlgorithm;
  }

  /**
   * Getter for {@link #enabled}.
   *
   * @return value of enabled.
   */
  public final Boolean getEnabled() {
    return enabled;
  }

  /**
   * Setter for {@link #enabled}.
   *
   * @param signatureFlag value to be set.
   */
  public final void setEnabled(final Boolean signatureFlag) {
    this.enabled = signatureFlag;
  }

  /**
   * Getter for {@link #mastId}.
   *
   * @return value of mastId.
   */
  public final String getMastId() {
    return mastId;
  }

  /**
   * Setter for {@link #mastId}.
   *
   * @param mastSignatureId value to be set.
   */
  public final void setMastId(final String mastSignatureId) {
    this.mastId = mastSignatureId;
  }
}
