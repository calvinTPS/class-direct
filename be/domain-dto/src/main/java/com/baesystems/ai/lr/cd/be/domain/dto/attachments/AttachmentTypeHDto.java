package com.baesystems.ai.lr.cd.be.domain.dto.attachments;

import com.baesystems.ai.lr.dto.references.AttachmentTypeDto;

/**
 * @author syalavarthi
 *
 */
public class AttachmentTypeHDto extends AttachmentTypeDto {

  /**
   * serialVersionUID.
   */
  private static final long serialVersionUID = -6340624316627054629L;

}
