package com.baesystems.ai.lr.cd.be.enums;

/**
 * Enumeration for file download type.
 *
 * @author Faizal Sidek
 */
public enum DownloadTypeEnum {
  /**
   * S3 file download.
   */
  S3,

  /**
   * CS10 file download.
   */
  CS10,

  /**
   * HTTP file download.
   */
  HTTP
}
