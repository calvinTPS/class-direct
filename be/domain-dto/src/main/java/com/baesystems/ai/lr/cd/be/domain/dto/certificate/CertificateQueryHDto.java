package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import com.baesystems.ai.lr.dto.query.CertificateQueryDto;

/**
 * @author sbollu
 *
 */
public class CertificateQueryHDto extends CertificateQueryDto {

  /**
   *
   */
  private static final long serialVersionUID = -1649819571023813465L;


  /**
   * Flag for Statutory.
   *
   */
  private Boolean isStatutoryOnly = Boolean.FALSE;


  /**
   * @return the isStatutoryOnly.
   */
  public final Boolean getIsStatutoryOnly() {
    return isStatutoryOnly;
  }


  /**
   * @param isStatutoryOnlyArg to set.
   */
  public final void setIsStatutoryOnly(final Boolean isStatutoryOnlyArg) {
    this.isStatutoryOnly = isStatutoryOnlyArg;
  }


}
