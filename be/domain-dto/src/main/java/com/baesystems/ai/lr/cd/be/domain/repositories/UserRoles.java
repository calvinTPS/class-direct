package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author vkovuru
 *
 */
@Entity
@Table(name = "USER_ROLES", uniqueConstraints = {@UniqueConstraint(columnNames = {"USER_ID", "ROLE_ID"})})
public class UserRoles implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 2304921434484004000L;

  /**
   * Id of the entity.
   *
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "USER_ROLES_ID")
  private Long id;

  /**
  *
  */
  @ManyToOne
  @JoinColumn(name = "ROLE_ID", nullable = false)
  private Roles role;

  /**
   *
   */
  @ManyToOne
  @JoinColumn(name = "USER_ID", nullable = false)
  private UserProfiles user;

  /**
   * Getter for {@link #id}.
   *
   * @return id
   *
   */
  public final Long getId() {
    return id;
  }

  /**
   * Setter for {@link #id}.
   *
   * @param idToBeSet id to be set.
   *
   */
  public final void setId(final Long idToBeSet) {
    this.id = idToBeSet;
  }

  /**
   * @return value.
   */
  public final Roles getRole() {
    return role;
  }

  /**
   * @param roleObject value.
   */
  public final void setRole(final Roles roleObject) {
    this.role = roleObject;
  }

  /**
   * @return value.
   */
  public final UserProfiles getUser() {
    return user;
  }

  /**
   * @param userObject value.
   */
  public final void setUser(final UserProfiles userObject) {
    this.user = userObject;
  }
}
