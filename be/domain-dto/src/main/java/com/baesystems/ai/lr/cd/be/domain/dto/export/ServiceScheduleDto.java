package com.baesystems.ai.lr.cd.be.domain.dto.export;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;

/**
 * ServiceSchedule Dto is mainly use to future due date related calculation.
 *
 * @author yng
 *
 */
public class ServiceScheduleDto {

  /**
   * List of services.
   */
  private List<ScheduledServiceHDto> services;

  /**
   * List of future due date calculated from above services.
   */
  private List<RepeatOfDto> repeatedServices;

  /**
   * Getter for services.
   *
   * @return services value.
   */
  public final List<ScheduledServiceHDto> getServices() {
    return services;
  }

  /**
   * Setter for services.
   *
   * @param servicesArg value.
   */
  public final void setServices(final List<ScheduledServiceHDto> servicesArg) {
    this.services = servicesArg;
  }

  /**
   * Setter for repeat of.
   *
   * @return repeat of value.
   */
  public final List<RepeatOfDto> getRepeatedServices() {
    return repeatedServices;
  }

  /**
   * Getter for repeat of.
   *
   * @param repeatedServicesArg value.
   */
  public final void setRepeatedServices(final List<RepeatOfDto> repeatedServicesArg) {
    this.repeatedServices = repeatedServicesArg;
  }
}
