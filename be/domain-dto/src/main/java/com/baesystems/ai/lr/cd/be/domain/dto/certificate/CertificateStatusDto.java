package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import com.baesystems.ai.lr.dto.base.DeletableDto;

/**
 * Created by fwijaya on 24/1/2017.
 */
public class CertificateStatusDto extends DeletableDto {
  /**
   * Name of the certificate status.
   */
  private String name;
  /**
   * Description of the certificate status.
   */
  private String description;

  /**
   * Get certificate status name.
   * @return certificate status name.
   */
  public final String getName() {
    return name;
  }

  /**
   * Set certificate status name.
   * @param nameArg Cetificate status name.
   */
  public final void setName(final String nameArg) {
    this.name = nameArg;
  }

  /**
   * Get certificate description.
   * @return Certificate description.
   */
  public final String getDescription() {
    return description;
  }

  /**
   * Set certificate status description.
   * @param descriptionArg Certificate status description.
   */
  public final void setDescription(final String descriptionArg) {
    this.description = descriptionArg;
  }
}
