package com.baesystems.ai.lr.cd.be.domain.dto.export;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.reports.ReportContentDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * Mapping for generated content from mast_job_report.generated_content column.
 *
 * @author yng
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportContentHDto extends ReportContentDto {
  /**
   * The default serialVersionUID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * json field {@link FlagStateHDto}.
   */
  @LinkedResource(referencedField = "currentFlag")
  @Getter
  @Setter
  private FlagStateHDto currentFlagH;

  /**
   * json field {@link FlagStateHDto}.
   */
  @LinkedResource(referencedField = "proposedFlag")
  @Getter
  @Setter
  private FlagStateHDto proposedFlagH;

  /**
   * json field {@link LinkResource}.
   */
  @Getter
  @Setter
  private PortOfRegistryDto registeredPortH;

}
