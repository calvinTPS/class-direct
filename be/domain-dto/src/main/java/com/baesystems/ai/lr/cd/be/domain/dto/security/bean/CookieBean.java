package com.baesystems.ai.lr.cd.be.domain.dto.security.bean;

/**
 * @author RKaneysan
 *
 */
public class CookieBean {

  /**
   * Domain for Cookie.
   */
  private String domain;

  /**
   * isHttpOnly Cookie.
   */
  private boolean httpOnly;

  /**
   * Maximum Age for Cookie.
   */
  private int maxAge;

  /**
   * Cookie Path.
   */
  private String path;

  /**
   * isSecure Cookie.
   */
  private boolean secure;

  /**
   * Comments for Cookie.
   */
  private String comments;

  /**
   * Cookie version.
   */
  private int version;

  /**
   * @return the domain.
   */
  public final String getDomain() {
    return domain;
  }

  /**
   * @param domainInput the domain to set.
   */
  public final void setDomain(final String domainInput) {
    this.domain = domainInput;
  }

  /**
   * @return the httpOnly.
   */
  public final boolean isHttpOnly() {
    return httpOnly;
  }

  /**
   * @param httpOnlyInput the httpOnly to set.
   */
  public final void setHttpOnly(final boolean httpOnlyInput) {
    this.httpOnly = httpOnlyInput;
  }

  /**
   * @return the maxAge.
   */
  public final int getMaxAge() {
    return maxAge;
  }

  /**
   * @param maxAgeInput the maxAge to set.
   */
  public final void setMaxAge(final int maxAgeInput) {
    this.maxAge = maxAgeInput;
  }

  /**
   * @return the path.
   */
  public final String getPath() {
    return path;
  }

  /**
   * @param pathInput the path to set.
   */
  public final void setPath(final String pathInput) {
    this.path = pathInput;
  }

  /**
   * @return the secure.
   */
  public final boolean isSecure() {
    return secure;
  }

  /**
   * @param secureInput the secure to set.
   */
  public final void setSecure(final boolean secureInput) {
    this.secure = secureInput;
  }

  /**
   * @return the comments.
   */
  public final String getComments() {
    return comments;
  }

  /**
   * @param commentsInput the comments to set.
   */
  public final void setComments(final String commentsInput) {
    this.comments = commentsInput;
  }

  /**
   * @return the version.
   */
  public final int getVersion() {
    return version;
  }

  /**
   * @param versionInput the version to set.
   */
  public final void setVersion(final int versionInput) {
    this.version = versionInput;
  }

  /**
   * Override toString to return all field value.
   *
   * @see java.lang.Object#toString()
   * @return all field value in String format.
   */
  public final String toString() {
    return "domain: " + domain + ", httpOnly: " + httpOnly + ", maxAge: " + maxAge + ", path: " + path + ", secure: "
        + secure + ", comments: " + comments + ", version: " + version;
  }

}
