package com.baesystems.ai.lr.cd.be.enums;

/**
 * @author sbollu
 *
 */
public enum ResponseOptionEnum {

  /**
   *
   */
  Cached("C", false),
  /**
  *
  */
  Internal("I", false),
  /**
   *
   */
  External("E", false);

  /**
   * Flag.
   */
  private final boolean responseFlag;

  /**
   * name.
   */
  private final String flagName;

  /**
   * @return the flagName
   */
  public final String getFlagName() {
    return flagName;
  }

  /**
   * @return the responseFlag.
   */
  public final boolean isResponseFlag() {
    return responseFlag;
  }

  /**
   * default constructor.
   *
   * @param flagNameObj flagNameObj.
   * @param responseFlagObj responseFlagObj.
   */
  private ResponseOptionEnum(final String flagNameObj, final boolean responseFlagObj) {
    this.responseFlag = responseFlagObj;
    this.flagName = flagNameObj;
  }

}
