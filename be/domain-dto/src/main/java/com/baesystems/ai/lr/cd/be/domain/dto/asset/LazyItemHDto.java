package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;

/**
 * LazyItemHDto extending LazyItemDto (referred as AssetItem in general term).
 *
 * @author yng
 *
 */
public class LazyItemHDto extends LazyItemDto {

  /**
   * Auto generated serial.
   */
  private static final long serialVersionUID = 5998787680518280632L;



}
