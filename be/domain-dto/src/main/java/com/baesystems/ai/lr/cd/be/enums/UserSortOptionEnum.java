package com.baesystems.ai.lr.cd.be.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumerator for User profiles sorting option.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum UserSortOptionEnum {

  /**
   * Sort by email.
   */
  Email("email");
  /**
   * Value of sort field.
   */
  private String field;

  /**
   * Default constructor.
   *
   * @param sortField sort field.
   */
  UserSortOptionEnum(final String sortField) {
    this.field = sortField;
  }

  /**
   * Helper method to deserialize json.
   *
   * @param value enum value.
   * @return json.
   */
  @JsonCreator
  public static UserSortOptionEnum forValue(final String value) {
    for (final UserSortOptionEnum option : UserSortOptionEnum.values()) {
      if (option.name().equalsIgnoreCase(value)) {
        return option;
      }
    }

    return null;
  }

  /**
   * Getter for {@link #field}.
   *
   * @return value of field.
   */
  @JsonValue
  public final String getField() {
    return field;
  }

}

