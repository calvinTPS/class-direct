package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * Provides customer role reference data details.
 *
 * @author sbollu
 *
 */
public class PartyRoleHDto extends ReferenceDataDto {

  /**
   * Auto generated serial version UID.
   */
  private static final long serialVersionUID = -1529982621744049074L;



}
