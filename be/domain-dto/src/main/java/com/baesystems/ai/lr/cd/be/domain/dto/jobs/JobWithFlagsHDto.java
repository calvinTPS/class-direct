package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.dto.jobs.JobWithFlagsDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fwijaya on 5/5/2017.
 */
public class JobWithFlagsHDto extends JobWithFlagsDto {
  /**
   * Linked to {@link LocationHDto}.
   */
  @LinkedResource(referencedField = "location")
  @Getter
  @Setter
  private LocationHDto locationDto;

  /**
   * Linked to {@link JobHDto}.
   */
  @LinkedResource(referencedField = "jobStatus")
  @Getter
  @Setter
  private JobStatusHDto jobStatusDto;
}
