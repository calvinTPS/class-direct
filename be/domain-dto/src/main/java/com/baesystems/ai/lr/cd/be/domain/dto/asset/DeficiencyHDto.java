package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;

/**
 * @author syalavarthi.
 *
 */
public class DeficiencyHDto extends DeficiencyDto {

  /**
   * static serialVersionUID.
   */
  private static final long serialVersionUID = 5307033630099175317L;

  /**
   * hydrate defect status.
   */
  @LinkedResource(referencedField = "status")
  private DefectStatusHDto statusH;

  /**
   * list of rectifications.
   */
  private List<RectificationHDto> rectificationsH;

  /**
   * list of statutoryFindings.
   */
  private List<StatutoryFindingHDto> statutoryFindings;

  /**
   * Getter for defect status.
   *
   * @return defect status dto.
   */
  public final DefectStatusHDto getStatusH() {
    return statusH;
  }

  /**
   * Setter for defect status.
   *
   * @param statusHObj value.
   */
  public final void setStatusH(final DefectStatusHDto statusHObj) {
    this.statusH = statusHObj;
  }

  /**
   * Getter for rectifications list.
   *
   * @return rectifications.
   */
  public final List<RectificationHDto> getRectificationsH() {
    return rectificationsH;
  }

  /**
   * Setter for rectifications list.
   *
   * @param rectificationsObj value.
   */
  public final void setRectificationsH(final List<RectificationHDto> rectificationsObj) {
    this.rectificationsH = rectificationsObj;
  }

  /**
   * Getter for statutoryFindings list.
   *
   * @return statutoryFindings.
   */
  public final List<StatutoryFindingHDto> getStatutoryFindings() {
    return statutoryFindings;
  }

  /**
   * Setter for statutoryFindings list.
   *
   * @param statutoryFindingsObj value.
   */
  public final void setStatutoryFindings(final List<StatutoryFindingHDto> statutoryFindingsObj) {
    this.statutoryFindings = statutoryFindingsObj;
  }

}
