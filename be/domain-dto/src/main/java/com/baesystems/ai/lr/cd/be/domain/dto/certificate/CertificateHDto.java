package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import java.util.Date;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.certificates.CertificateDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

/**
 * Extended certificate DTO. Created by fwijaya on 23/1/2017.
 */
public class CertificateHDto extends CertificateDto {

  /**
   * Certificate template DTO.
   */
  @LinkedResource(referencedField = "certificateTemplate")
  private CertificateTemplateHDto certificateTemplateDto;

  /**
   * Certificate Type DTO.
   */
  @LinkedResource(referencedField = "certificateType")
  private CertificateTypeDto certificateTypeDto;

  /**
   * Certificate Status DTO.
   */
  @LinkedResource(referencedField = "certificateStatus")
  private CertificateStatusDto certificateStatusDto;

  /**
   * LR Employee DTO.
   */
  private LrEmployeeDto surveyor;

  /**
   * Latest endorsed date from a list of action taken.
   */
  private Date endorsedDate;

  /**
   * Get endorsed date.
   *
   * @return endorsed date.
   */
  public final Date getEndorsedDate() {
    return endorsedDate;
  }

  /**
   * Set endorsed date.
   *
   * @param endorsedDateArg endorsed date.
   */
  public final void setEndorsedDate(final Date endorsedDateArg) {
    this.endorsedDate = endorsedDateArg;
  }

  /**
   * Get surveyor.
   *
   * @return LR Employee DTO.
   */
  public final LrEmployeeDto getSurveyor() {
    return surveyor;
  }

  /**
   * Set surveyor.
   *
   * @param surveyorArg Surveyor object.
   */
  public final void setSurveyor(final LrEmployeeDto surveyorArg) {
    this.surveyor = surveyorArg;
  }

  /**
   * get certificate status dto.
   *
   * @return certificate status dto object.
   */
  public final CertificateStatusDto getCertificateStatusDto() {
    return certificateStatusDto;
  }

  /**
   * Set certificate status dto.
   *
   * @param certificateStatusDtoArg certificate status dto object.
   */
  public final void setCertificateStatusDto(final CertificateStatusDto certificateStatusDtoArg) {
    this.certificateStatusDto = certificateStatusDtoArg;
  }

  /**
   * get certificate template dto.
   *
   * @return certificate template dto object.
   */
  public final CertificateTemplateHDto getCertificateTemplateHDto() {
    return certificateTemplateDto;
  }

  /**
   * set certificate template dto.
   *
   * @param certificateTemplateDtoArg certificate template dto object.
   */
  public final void setCertificateTemplateHDto(final CertificateTemplateHDto certificateTemplateDtoArg) {
    this.certificateTemplateDto = certificateTemplateDtoArg;
  }

  /**
   * Get certificate template dto.
   *
   * @return certificate template dto object.
   */
  public final CertificateTypeDto getCertificateTypeDto() {
    return certificateTypeDto;
  }

  /**
   * Set certificate type dto.
   *
   * @param certificateTypeDtoArg certificate type dto object.
   */
  public final void setCertificateTypeDto(final CertificateTypeDto certificateTypeDtoArg) {
    this.certificateTypeDto = certificateTypeDtoArg;
  }
}
