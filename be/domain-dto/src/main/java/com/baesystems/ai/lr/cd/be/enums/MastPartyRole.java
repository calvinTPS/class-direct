package com.baesystems.ai.lr.cd.be.enums;

/**
 * Provides a reference to MAST - PartyRole with all role that CD need.
 * @author YWearn
 */
public enum MastPartyRole {
  /**
   * The ship operator role.
   */
  SHIP_OPERATOR(2L),
  /**
   * The ship manager role.
   */
  SHIP_MANAGER(3L),
  /**
   * The ship registered owner role.
   */
  SHIP_OWNER(4L),
  /**
   * The ship group owner role.
   */
  GROUP_OWNER(6L),
  /**
   * The docking company role.
   */
  DOC_COMPANY(10L);

  /**
   * The unique identifier for party role.
   */
  private final Long value;

  /**
   * Constructs a party role enum using unique identifier.
   * @param roleId the party unique identifier.
   */
  MastPartyRole(final Long roleId) {
      this.value = roleId;
  }

  /**
   * Gets the party role enum unique identifier.
   * @return the party role enum unique identifier.
   */
  public final Long getValue() {
      return value;
  }

}
