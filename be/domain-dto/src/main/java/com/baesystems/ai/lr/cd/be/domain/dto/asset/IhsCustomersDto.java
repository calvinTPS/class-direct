package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.cd.be.enums.CustomerRolesEnum;

/**
 * @author syalavarthi.
 *
 */
public class IhsCustomersDto {

  /**
   * imoNumber.
   */
  private String imoNumber;
  /**
   * company name.
   */
  private String companyName;
  /**
   * relationship.
   */
  private CustomerRolesEnum relationShip;

  /**
   * to get imoNumber.
   *
   * @return impoNumber.
   */
  public final String getImoNumber() {
    return imoNumber;
  }

  /**
   * to set imoNumber.
   *
   * @param imoNumberObj imoNumber.
   */
  public final void setImoNumber(final String imoNumberObj) {
    this.imoNumber = imoNumberObj;
  }

  /**
   * to get company name.
   *
   * @return companyName.
   */
  public final String getCompanyName() {
    return companyName;
  }

  /**
   * to set company name.
   *
   * @param companyNameObj companyName.
   */
  public final void setCompanyName(final String companyNameObj) {
    this.companyName = companyNameObj;
  }

  /**
   * to get relation.
   *
   * @return relation.
   */
  public final CustomerRolesEnum getRelationShip() {
    return relationShip;
  }

  /**
   * to set relationship.
   *
   * @param relationShipObj relationship.
   */
  public final void setRelationShip(final CustomerRolesEnum relationShipObj) {
    this.relationShip = relationShipObj;
  }


}
