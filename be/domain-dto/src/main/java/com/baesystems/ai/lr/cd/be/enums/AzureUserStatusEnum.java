package com.baesystems.ai.lr.cd.be.enums;

/**
 * approval status.
 *
 * @author syalavarthi.
 *
 */
public enum AzureUserStatusEnum {
  /**
   * approval none.
   */
  APPROVAL_NONE,

  /**
   * approval pending.
   */
  APPROVAL_PENDING,

  /**
   * approval approved.
   */
  APPROVAL_APPROVED;
}


