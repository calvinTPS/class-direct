package com.baesystems.ai.lr.cd.be.domain.dto.employee;

/**
 * @author sbollu
 *
 */
public class EmployeeDto {

  /**
   *
   */
  private Long id;
  /**
   *
   */
  private String name;

  /**
   * @return the id value.
   */
  public final Long getId() {
    return id;
  }

  /**
   * @param idObject to set.
   */
  public final void setId(final Long idObject) {
    this.id = idObject;
  }

  /**
   * @return the name value.
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameObject set.
   */
  public final void setName(final String nameObject) {
    this.name = nameObject;
  }


}
