package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.Date;

import com.baesystems.ai.lr.cd.be.domain.annotation.DueStatusField;
import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.IDueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author syalavarthi.
 *
 */
public class StatutoryFindingHDto extends StatutoryFindingDto implements IDueStatusDto {

  /**
   * static serialVersionUID.
   */
  private static final long serialVersionUID = -5039284907166961319L;

  /**
   * Asset code.
   */
  private String code;

  /**
   * Linked to {@link CodicilStatusHDto}.
   *
   */
  @LinkedResource(referencedField = "status")
  private CodicilStatusHDto statusH;

  /**
   * Linked to {@link ActionTakenHDto}.
   *
   */
  @LinkedResource(referencedField = "actionTaken")
  private ActionTakenHDto actionTakenH;

  /**
   * injecting deficiencyH.
   */
  private DeficiencyHDto deficiencyH;

  /**
   * dueStatus.
   */
  @DueStatusField
  private DueStatus dueStatusH;

  /**
   * statutory finding category.
   */
  @Getter
  @Setter
  private CodicilCategoryHDto categoryH;
  /**
   * The Job for sattutory finding.
   */
  @Getter
  @Setter
  private JobHDto jobH;

  /**
   * The Due date epoch.
   */
  @Getter
  @Setter
  private Long dueDateEpoch;

  /**
   * The Imposed date epoch.
   */
  @Getter
  @Setter
  private Long imposedDateEpoch;

  /**
   * getter for statusH.
   *
   * @return statusH.
   */
  public final CodicilStatusHDto getStatusH() {
    return statusH;
  }

  /**
   * set statusHObj to statusH.
   *
   * @param statusHObj statusH.
   */
  public final void setStatusH(final CodicilStatusHDto statusHObj) {
    this.statusH = statusHObj;
  }

  /**
   * getter for actionTakenH.
   *
   * @return actionTakenH.
   */
  public final ActionTakenHDto getActionTakenH() {
    return actionTakenH;
  }

  /**
   * set actionTakenHObj to actionTakenH.
   *
   * @param actionTakenHObj actionTakenH.
   */
  public final void setActionTakenH(final ActionTakenHDto actionTakenHObj) {
    this.actionTakenH = actionTakenHObj;
  }

  /**
   * getter for deficiencyH.
   *
   * @return deficiencyH.
   */
  public final DeficiencyHDto getDeficiencyH() {
    return deficiencyH;
  }

  /**
   * set deficiencyHObj to deficiencyH.
   *
   * @param deficiencyHObj deficiencyH.
   */
  public final void setDeficiencyH(final DeficiencyHDto deficiencyHObj) {
    this.deficiencyH = deficiencyHObj;
  }

  /**
   * Gets {@link #dueStatusH}.
   *
   * @return the dueStatusH.
   */
  @Override
  public final DueStatus getDueStatusH() {
    return dueStatusH;
  }

  /**
   * Sets {@link #dueStatusH}.
   *
   * @param dueStatusHObj to set.
   */
  public final void setDueStatusH(final DueStatus dueStatusHObj) {
    this.dueStatusH = dueStatusHObj;
  }

  /**
   * Getter for {@link #code}.
   *
   * @return value of code.
   */
  public final String getCode() {
    return code;
  }

  /**
   * Setter for {@link #code}.
   *
   * @param assetCode value to be set.
   */
  public final void setCode(final String assetCode) {
    this.code = assetCode;
  }

  /**
   * Overrides setter for due date to calculate due date epoch and Sets due date epoch.
   *
   * @param dueDate value.
   */
  @Override
  public final void setDueDate(final Date dueDate) {
    super.setDueDate(dueDate);
    Long epoch = null;
    if (dueDate != null) {
      epoch = DateUtils.getDateToEpoch(dueDate);
      setDueDateEpoch(epoch);
    }
  }

  /**
   * Overrides setter for imposed date to calculate imposed date epoch and Sets imposed date epoch.
   *
   * @param imposedDate value.
   */
  @Override
  public final void setImposedDate(final Date imposedDate) {
    super.setImposedDate(imposedDate);
    Long epoch = null;
    if (imposedDate != null) {
      epoch = DateUtils.getDateToEpoch(imposedDate);
      setImposedDateEpoch(epoch);
    }
  }

}
