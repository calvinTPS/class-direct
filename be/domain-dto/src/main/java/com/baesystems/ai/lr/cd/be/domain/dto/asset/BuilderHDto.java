package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.assets.BuilderDto;

/**
 * Hydrated Dto for BuilderDto.
 *
 * @author VMandalapu
 */
public class BuilderHDto extends BuilderDto {

  /**
   * Generated Serial Id.
   */
  private static final long serialVersionUID = -8138690732750402884L;

}
