package com.baesystems.ai.lr.cd.be.domain.dto.version;

import lombok.Getter;
import lombok.Setter;

/**
 * Version DTO.
 *
 * @author fwijaya on 15/6/2017.
 */
public class VersionDto {
  /**
   * Constructs a Version Dto.
   *
   * @param versionArg The versionArg number.
   */
  public VersionDto(final String versionArg) {
    this.build = versionArg;
  }

  /**
   * The build version.
   */
  @Getter
  @Setter
  private String build;
}
