package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.query.FaultQueryDto;

/**
 * @author syalavarthi.
 *
 */
public class FaultQueryHDto extends FaultQueryDto {

  /**
   * static serial id.
   */
  private static final long serialVersionUID = 674390711207602156L;

}
