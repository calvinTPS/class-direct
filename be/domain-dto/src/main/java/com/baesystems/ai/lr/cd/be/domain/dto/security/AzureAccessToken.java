package com.baesystems.ai.lr.cd.be.domain.dto.security;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Azure access token.
 *
 * @author Faizal Sidek
 */
public class AzureAccessToken implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * tokenType.
   */
  @JsonProperty("token_type")
  private String tokenType;

  /**
   * expiresIn.
   */
  @JsonProperty("expires_in")
  private Long expiresIn;

  /**
   * extExpiresIn.
   */
  @JsonProperty("ext_expires_in")
  private Long extExpiresIn;

  /**
   * expiresOn.
   */
  @JsonProperty("expires_on")
  private Long expiresOn;

  /**
   * notBefore.
   */
  @JsonProperty("not_before")
  private Long notBefore;

  /**
   * resource.
   */
  @JsonProperty("resource")
  private String resource;

  /**
   * accessToken.
   */
  @JsonProperty("access_token")
  private String accessToken;

  /**
   * Getter for {@link #tokenType}.
   *
   * @return value of tokenType.
   */
  public final String getTokenType() {
    return tokenType;
  }

  /**
   * Setter for {@link #tokenType}.
   *
   * @param tokenTypeObj value to be set.
   */
  public final void setTokenType(final String tokenTypeObj) {
    this.tokenType = tokenTypeObj;
  }

  /**
   * Getter for {@link #expiresIn}.
   *
   * @return value of expiresIn.
   */
  public final Long getExpiresIn() {
    return expiresIn;
  }

  /**
   * Setter for {@link #expiresIn}.
   *
   * @param expiresInObj value to be set.
   */
  public final void setExpiresIn(final Long expiresInObj) {
    this.expiresIn = expiresInObj;
  }

  /**
   * Getter for {@link #extExpiresIn}.
   *
   * @return value of extExpiresIn.
   */
  public final Long getExtExpiresIn() {
    return extExpiresIn;
  }

  /**
   * Setter for {@link #extExpiresIn}.
   *
   * @param extExpiresInObj value to be set.
   */
  public final void setExtExpiresIn(final Long extExpiresInObj) {
    this.extExpiresIn = extExpiresInObj;
  }

  /**
   * Getter for {@link #expiresOn}.
   *
   * @return value of expiresOn.
   */
  public final Long getExpiresOn() {
    return expiresOn;
  }

  /**
   * Setter for {@link #expiresOn}.
   *
   * @param expiresOnObj value to be set.
   */
  public final void setExpiresOn(final Long expiresOnObj) {
    this.expiresOn = expiresOnObj;
  }

  /**
   * Getter for {@link #notBefore}.
   *
   * @return value of notBefore.
   */
  public final Long getNotBefore() {
    return notBefore;
  }

  /**
   * Setter for {@link #notBefore}.
   *
   * @param notBeforeObj value to be set.
   */
  public final void setNotBefore(final Long notBeforeObj) {
    this.notBefore = notBeforeObj;
  }

  /**
   * Getter for {@link #resource}.
   *
   * @return value of resource.
   */
  public final String getResource() {
    return resource;
  }

  /**
   * Setter for {@link #resource}.
   *
   * @param resourceObj value to be set.
   */
  public final void setResource(final String resourceObj) {
    this.resource = resourceObj;
  }

  /**
   * Getter for {@link #accessToken}.
   *
   * @return value of accessToken.
   */
  public final String getAccessToken() {
    return accessToken;
  }

  /**
   * Setter for {@link #accessToken}.
   *
   * @param accessTokenObj value to be set.
   */
  public final void setAccessToken(final String accessTokenObj) {
    this.accessToken = accessTokenObj;
  }

  @Override
  public final String toString() {
    return "AzureAccessToken{" + "tokenType='" + tokenType + '\'' + ", expiresIn=" + expiresIn + ", extExpiresIn="
      + extExpiresIn + ", expiresOn=" + expiresOn + ", notBefore=" + notBefore + ", resource='" + resource + '\''
      + ", accessToken='" + accessToken + '\'' + '}';
  }
}
