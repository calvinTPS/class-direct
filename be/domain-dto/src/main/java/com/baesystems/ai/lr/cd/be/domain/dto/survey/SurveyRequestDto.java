package com.baesystems.ai.lr.cd.be.domain.dto.survey;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.port.PortAgentDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author NAvula
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyRequestDto {

  /**
   *
   */
  private LocalDate estimatedDateOfArrival;
  /**
   *
   */
  private LocalTime estimatedTimeOfArrival;
  /**
   *
   */
  private LocalDate surveyStartDate;
  /**
   *
   */
  private LocalDate estimatedDateOfDeparture;
  /**
   *
   */
  private LocalTime estimatedTimeOfDeparture;

  /**
   *
   */
  private PortAgentDto portAgent;


  /**
   *
   */
  private String berthAnchorage;
  /**
   *
   */
  private String shipContactDetails;
  /**
   *
   */
  private String additionalComments;
  /**
   *
   */
  private String[] otherEmails;
  // private ScheduledServiceListDto services;
  /**
   *
   */
  private String assetId;
  /**
   *
   */
  private List<Long> serviceIds;
  /**
   *
   */
  private long portId;


  /**
   * @return value.
   */
  public final LocalDate getEstimatedDateOfArrival() {
    return estimatedDateOfArrival;
  }

  /**
   * @param estimatedDateOfArrivalObject value.
   */
  public final void setEstimatedDateOfArrival(final LocalDate estimatedDateOfArrivalObject) {
    this.estimatedDateOfArrival = estimatedDateOfArrivalObject;
  }

  /**
   * @return value.
   */
  public final LocalTime getEstimatedTimeOfArrival() {
    return estimatedTimeOfArrival;
  }

  /**
   * @param estimatedTimeOfArrivalObject value.
   */
  public final void setEstimatedTimeOfArrival(final LocalTime estimatedTimeOfArrivalObject) {
    this.estimatedTimeOfArrival = estimatedTimeOfArrivalObject;
  }

  /**
   * @return value.
   */
  public final LocalDate getSurveyStartDate() {
    return surveyStartDate;
  }

  /**
   * @param surveyStartDateObject value.
   */
  public final void setSurveyStartDate(final LocalDate surveyStartDateObject) {
    this.surveyStartDate = surveyStartDateObject;
  }

  /**
   * @return value.
   */
  public final LocalDate getEstimatedDateOfDeparture() {
    return estimatedDateOfDeparture;
  }

  /**
   * @param estimatedDateOfDepartureObject value.
   */
  public final void setEstimatedDateOfDeparture(final LocalDate estimatedDateOfDepartureObject) {
    this.estimatedDateOfDeparture = estimatedDateOfDepartureObject;
  }

  /**
   * @return value.
   */
  public final LocalTime getEstimatedTimeOfDeparture() {
    return estimatedTimeOfDeparture;
  }

  /**
   * @param estimatedTimeOfDepartureObject value.
   */
  public final void setEstimatedTimeOfDeparture(final LocalTime estimatedTimeOfDepartureObject) {
    this.estimatedTimeOfDeparture = estimatedTimeOfDepartureObject;
  }

  /**
   * @return value.
   */
  public final PortAgentDto getPortAgent() {
    return portAgent;
  }

  /**
   * @param portAgentObject value.
   */
  public final void setPortAgent(final PortAgentDto portAgentObject) {
    this.portAgent = portAgentObject;
  }

  /**
   * @return value.
   */
  public final String getBerthAnchorage() {
    return berthAnchorage;
  }

  /**
   * @param berthAnchorageObject value.
   */
  public final void setBerthAnchorage(final String berthAnchorageObject) {
    this.berthAnchorage = berthAnchorageObject;
  }

  /**
   * @return value.
   */
  public final String getShipContactDetails() {
    return shipContactDetails;
  }

  /**
   * @param shipContactDetailsObject value.
   */
  public final void setShipContactDetails(final String shipContactDetailsObject) {
    this.shipContactDetails = shipContactDetailsObject;
  }

  /**
   * @return value.
   */
  public final String getAdditionalComments() {
    return additionalComments;
  }

  /**
   * @param additionalCommentsObject value.
   */
  public final void setAdditionalComments(final String additionalCommentsObject) {
    this.additionalComments = additionalCommentsObject;
  }

  /**
   * @return value.
   */
  public final String[] getOtherEmails() {
    return otherEmails;
  }

  /**
   * @param otherEmailsObject value.
   */
  public final void setOtherEmails(final String[] otherEmailsObject) {
    this.otherEmails = otherEmailsObject;
  }

  /**
   * @return value.
   */
  public final String getAssetId() {
    return assetId;
  }

  /**
   * @param assetIdObject value.
   */
  public final void setAssetId(final String assetIdObject) {
    this.assetId = assetIdObject;
  }

  /**
   * @return value.
   */
  public final List<Long> getServiceIds() {
    return serviceIds;
  }

  /**
   * @param serviceIdsObject value.
   */
  public final void setServiceIds(final List<Long> serviceIdsObject) {
    this.serviceIds = serviceIdsObject;
  }

  /**
   * @return value.
   */
  public final long getPortId() {
    return portId;
  }

  /**
   * @param portIdObject value.
   */
  public final void setPortId(final long portIdObject) {
    this.portId = portIdObject;
  }
}
