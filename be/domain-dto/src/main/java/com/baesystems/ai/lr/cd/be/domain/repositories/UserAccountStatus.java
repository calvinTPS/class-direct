package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author sbollu
 *
 */
@Entity
@Table(name = "USER_ACCOUNT_STATUS")
public class UserAccountStatus implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 4271314743040084467L;

  /**
   * Id of the entity.
   */
  @Id
  @Column(name = "ID")
  private Long id;

  /**
  *
  */
  @Column(name = "NAME")
  private String name;

  /**
   * @return the id
   */
  public final Long getId() {
    return id;
  }

  /**
   * @param idObject to set.
   */
  public final void setId(final Long idObject) {
    this.id = idObject;
  }

  /**
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameObject to set
   */
  public final void setName(final String nameObject) {
    this.name = nameObject;
  }

  /**
   * (non-Javadoc).
   *
   * @see java.lang.Object#hashCode().
   */
  @Override
  public final int hashCode() {
    final HashCodeBuilder hcb = new HashCodeBuilder();
    hcb.append(name);
    return hcb.toHashCode();
  }

  /**
   * (non-Javadoc).
   *
   * @see java.lang.Object#equals(java.lang.Object).
   */
  @Override
  public final boolean equals(final Object obj) {
    boolean isEqual;

    if (this == obj) {
      isEqual = true;
    } else if (!(obj instanceof UserAccountStatus)) {
      isEqual = false;
    } else {
      final UserAccountStatus userAccStatus = (UserAccountStatus) obj;
      final EqualsBuilder eqlbuilder = new EqualsBuilder();
      eqlbuilder.append(name, userAccStatus.name);
      isEqual =  eqlbuilder.isEquals();
    }

    return isEqual;
  }
}
