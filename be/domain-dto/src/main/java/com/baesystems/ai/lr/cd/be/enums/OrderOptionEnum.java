package com.baesystems.ai.lr.cd.be.enums;

/**
 * Order ascending or descending.
 * @author Faizal Sidek
 */
public enum OrderOptionEnum {
  /**
   * Ascending order.
   */
  ASC,

  /**
   * Descending order.
   */
  DESC;
}
