package com.baesystems.ai.lr.cd.be.domain.dto.subfleet;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * @author Jonathan Lin
 *
 */
public class SubFleetOptimizedDto implements Serializable {

  /**
   * serialVersionUID.
   */
  private static final long serialVersionUID = 2795924405147554544L;

  /**
   * Asset IDs to be included in the operation.
   */
  private List<Long> includedAssetIds;

  /**
   * Asset IDs to be excluded from the operation.
   */
  private List<Long> excludedAssetIds;

  /**
   * "RESTRICTED" OR "ACCESSIBLE".
   */
  @NotNull
  private String moveTo;

  /**
   * @return included IDs.
   */
  public final List<Long> getIncludedAssetIds() {
    return includedAssetIds;
  }

  /**
   * Set included IDs.
   *
   * @param includedIds The list of included IDs.
   */
  public final void setIncludedAssetIds(final List<Long> includedIds) {
    this.includedAssetIds = includedIds;
  }

  /**
   * @return Excluded IDs.
   */
  public final List<Long> getExcludedAssetIds() {
    return excludedAssetIds;
  }

  /**
   * Set excluded IDs.
   *
   * @param excludedIds The list of excluded IDs.
   */
  public final void setExcludedAssetIds(final List<Long> excludedIds) {
    this.excludedAssetIds = excludedIds;
  }

  /**
   * @return The name of the move operation.
   */
  public final String getMoveTo() {
    return moveTo;
  }

  /**
   * Set the operation name.
   *
   * @param operation The operation name.
   */
  public final void setMoveTo(final String operation) {
    this.moveTo = operation;
  }
}
