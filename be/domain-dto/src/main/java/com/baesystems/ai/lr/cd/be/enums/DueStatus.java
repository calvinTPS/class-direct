package com.baesystems.ai.lr.cd.be.enums;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * @author VKolagutla
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = DueStatus.DueStatusDeserializer.class)
public enum DueStatus {
  /**
   * not due.
   */
  NOT_DUE(1L, "Not due", 4, "notdue", "overall_not_due"),
  /**
   * due soon.
   */
  DUE_SOON(2L, "Due soon", 3, "duesoon", "overall_due_soon"),
  /**
   * imminent.
   */
  IMMINENT(3L, "Imminent", 2, "imminent", "overall_imminent"),
  /**
   * overdue.
   */
  OVER_DUE(4L, "Overdue", 1, "overdue", "overall_overdue");

  /**
   * name.
   */
  private String name;
  /**
   * long id.
   */
  private Long id;

  /**
   * int priority.
   */
  private int priority;
  /**
   * The due status icon.
   */
  private String icon;
  /**
   * The over all due status icon.
   */
  private String assetIcon;

  /**
   * Constructs due status with id, name , priority, due status icon and asset overall status icon.
   *
   * @param idObj the id.
   * @param nameObj the due status name.
   * @param priorityObj the due statuses priority.
   * @param iconObj the icon for due status item.
   * @param overallIconObj the icon for asset overall status.
   */
  DueStatus(final Long idObj, final String nameObj, final int priorityObj, final String iconObj,
      final String overallIconObj) {
    this.name = nameObj;
    this.id = idObj;
    this.priority = priorityObj;
    this.icon = iconObj;
    this.assetIcon = overallIconObj;
  }

  /**
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * @return the id
   */
  public final Long getId() {
    return id;
  }

  /**
   * @return the priority
   */
  public final int getPriority() {
    return priority;
  }

  /**
   * Gets due status icon.
   *
   * @return the icon.
   */
  public final String getIcon() {
    return icon;
  }

  /**
   * Gets asset overall due status icon.
   *
   * @return the icon.
   */
  public final String getAssetIcon() {
    return assetIcon;
  }

  /**
   * Find due status from value.
   *
   * @param node json node.
   * @return enum value.
   */
  @JsonCreator
  public static DueStatus fromValue(final JsonNode node) {
    final Optional<DueStatus> optionals =
        Arrays.stream(DueStatus.values()).filter(type -> type.id.equals(node.get("id").asLong())).findAny();

    DueStatus status = null;
    if (optionals.isPresent()) {
      status = optionals.get();
    }

    return status;
  }

  /**
   * Deserializer for DueStatus.
   */
  public static class DueStatusDeserializer extends JsonDeserializer<DueStatus> {
    @Override
    public final DueStatus deserialize(final JsonParser parser, final DeserializationContext context)
        throws IOException, JsonProcessingException {
      final JsonNode jsonNode = parser.readValueAsTree();

      return DueStatus.fromValue(jsonNode);
    }
  }

  /**
   * Serializer for DueStatus.
   */
  public static class DueStatusSerializer extends JsonSerializer<DueStatus> {
    /**
     * Due status name.
     */
    private static final String DUE_STATUS_NAME = "name";

    /**
     * Due status asset icon.
     */
    private static final String DUE_STATUS_ASSET_ICON = "assetIcon";

    /**
     * Due status icon.
     */
    private static final String DUE_STATUS_ICON = "icon";

    /**
     * Due status priority.
     */
    private static final String DUE_STATUS_PRIORITY = "priority";

    /**
     * Due Status id.
     */
    private static final String DUE_STATUS_ID = "id";

    @Override
    public final void serialize(final DueStatus dueStatus, final JsonGenerator jsonGenerator,
        final SerializerProvider serializerProvider) throws IOException {
      jsonGenerator.writeStartObject();
      jsonGenerator.writeStringField(DUE_STATUS_NAME, dueStatus.getName());
      jsonGenerator.writeNumberField(DUE_STATUS_ID, dueStatus.getId());
      jsonGenerator.writeNumberField(DUE_STATUS_PRIORITY, dueStatus.getPriority());
      jsonGenerator.writeStringField(DUE_STATUS_ICON, dueStatus.getIcon());
      jsonGenerator.writeStringField(DUE_STATUS_ASSET_ICON, dueStatus.getAssetIcon());
      jsonGenerator.writeEndObject();
    }
  }
}
