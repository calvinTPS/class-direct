package com.baesystems.ai.lr.cd.be.domain.dto.cs10;

/**
 * CS10 Output Dto.
 *
 * @author yng
 *
 */
public class CS10OutputDto {

  /**
   * CS10 file name.
   */
  private String fileName;

  /**
   * CS10 file size.
   */
  private String fileSize;

  /**
   * CS10 file stream.
   */
  private byte[] fileStream;

  /**
   * Getter for file name.
   *
   * @return file name.
   */
  public final String getFileName() {
    return fileName;
  }

  /**
   * Setter for file name.
   *
   * @param fileNameParam file name.
   */
  public final void setFileName(final String fileNameParam) {
    this.fileName = fileNameParam;
  }

  /**
   * Getter for file size.
   *
   * @return file size.
   */
  public final String getFileSize() {
    return fileSize;
  }

  /**
   * Setter for file size.
   *
   * @param fileSizeParam file size.
   */
  public final void setFileSize(final String fileSizeParam) {
    this.fileSize = fileSizeParam;
  }

  /**
   * Getter for file stream.
   *
   * @return file stream.
   */
  public final byte[] getFileStream() {
    return fileStream;
  }

  /**
   * Setter for file stream.
   *
   * @param fileStreamParam file stream.
   */
  public final void setFileStream(final byte[] fileStreamParam) {
    this.fileStream = fileStreamParam;
  }


}
