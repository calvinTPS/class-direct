package com.baesystems.ai.lr.cd.be.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Mast Attachment Category Enumerator.
 */
public enum AttachmentCategoryEnum {

  /**
   * Certificate.
   */
  CERTIFICATE(1L, "Certificate"),
  /**
   * Thickness Measurement Report - Argonaut.
   */
  THICKNESS_MEASUREMENT_REPORT_ARGONAUT(2L, "Thickness Measurement Report - Argonaut"),
  /**
   * Thickness Measurement Report - Long.
   */
  THICKNESS_MEASUREMENT_REPORT_LONG(3L, "Thickness Measurement Report - Long"),
  /**
   * Thickness Measurement Report - Short.
   */
  THICKNESS_MEASUREMENT_REPORT_SHORT(4L, "Thickness Measurement Report - Short"),
  /**
   * Statutory Record.
   */
  STATUTORY_RECORD(5L, "Statutory Record"),
  /**
   * Executive Hull Summary.
   */
  EXECUTIVE_HULL_SUMMARY(6L, "Executive Hull Summary"),
  /**
   * Repair.
   */
  REPAIR(7L, "Repair"),
  /**
   * Meeting Minutes.
   */
  MEETING_MINUTES(8L, "Meeting Minutes"),
  /**
   * PR-17.
   */
  PR_17(9L, "PR-17"),
  /**
   * Survey Report.
   */
  SURVEY_REPORT(10L, "Survey Report"),
  /**
   * TOC/COF Instructions.
   */
  TOC_OR_COF_INSTRUCTIONS(11L, "TOC/COF Instructions"),
  /**
   * Other Correspondence.
   */
  OTHER_CORRESPONDENCE(12L, "Other Correspondence"),
  /**
   * Other.
   */
  OTHER(13L, "Other"),
  /**
   * The MMS report.
   */
  MMS_REPORT(14L, "MMS Report"),
  /**
   * The MMS Job Note.
   */
  MMS_JOB_NOTE(15L, "MMS Job Note");

  /**
   * attachment category id.
   */
  private final Long id;

  /**
   * attachment category name.
   */
  private final String name;

  /**
   * @param idObj attachment category id.
   * @param nameObj attachment category id.
   */
  AttachmentCategoryEnum(final Long idObj, final String nameObj) {
    this.id = idObj;
    this.name = nameObj;
  }

  /**
   * @return attachment category id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * @return attachment category name.
   */
  public final String getName() {
    return name;
  }

  /**
   * Gets list of mms attachments.
   *
   * @return list of mms attachments.
   */
  public static List<Long> getMMSCategories() {
    final List<Long> mmsIds = new ArrayList<Long>();
    mmsIds.add(MMS_REPORT.getId());
    mmsIds.add(MMS_JOB_NOTE.getId());
    return mmsIds;
  }
}

