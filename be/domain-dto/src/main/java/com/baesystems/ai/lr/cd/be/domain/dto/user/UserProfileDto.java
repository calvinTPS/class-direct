package com.baesystems.ai.lr.cd.be.domain.dto.user;

import java.util.List;

/**
 * @author SBollu
 *
 */
public class UserProfileDto {

  /**
   * user Id.
   */
  private String userId;
  /**
   * Status.
   */
  private List<String> status;
  /**
   * Role.
   */
  private List<String> roles;
  /**
   * Email.
   */
  private String email;

  /**
   * CD User.
   */
  private Boolean searchSSOIfNotExist;
  /**
   * users to be included.
   */
  private List<String> includedUsers;
  /**
   * users to be excluded.
   */
  private List<String> excludedUsers;
  /**
   * dateOfLastLoginMin.
   */
  private String dateOfLastLoginMin;
  /**
   * dateOfLastLoginMax.
   */
  private String dateOfLastLoginMax;
  /**
   * users codeSearch.
   */
  private UserCodeSearchDto codeSearch;

  /**
   * user company name.
   */
  private String companyName;

  /**
   * user full name.
   */
  private String name;

  /**
   * get includedUsers.
   *
   * @return includedUsers.
   */
  public final List<String> getIncludedUsers() {
    return includedUsers;
  }

  /**
   * setter for includedUsers.
   *
   * @param includedUsersObj value to includedUsers.
   */
  public final void setIncludedUsers(final List<String> includedUsersObj) {
    this.includedUsers = includedUsersObj;
  }

  /**
   * get excludedUsers.
   *
   * @return excludedUsers
   */
  public final List<String> getExcludedUsers() {
    return excludedUsers;
  }

  /**
   * setter for excludedUsers.
   *
   * @param excludedUsersObj value to excludedUsers.
   */
  public final void setExcludedUsers(final List<String> excludedUsersObj) {
    this.excludedUsers = excludedUsersObj;
  }

  /**
   * @return the userId value.
   */
  public final String getUserId() {
    return userId;
  }

  /**
   * @param userIdObject to set.
   */
  public final void setUserId(final String userIdObject) {
    this.userId = userIdObject;
  }

  /**
   * @return the status.
   */
  public final List<String> getStatus() {
    return status;
  }

  /**
   * @param statusObject the status to set.
   */
  public final void setStatus(final List<String> statusObject) {
    this.status = statusObject;
  }

  /**
   * @return the role value.
   */
  public final List<String> getRoles() {
    return roles;
  }

  /**
   * @param roleObject to set.
   */
  public final void setRoles(final List<String> roleObject) {
    this.roles = roleObject;
  }

  /**
   * @return the email value.
   */
  public final String getEmail() {
    return email;
  }

  /**
   * @param emailObject to set.
   */
  public final void setEmail(final String emailObject) {
    this.email = emailObject;
  }

  /**
   * @return the searchSSOIfNotExist
   */
  public final Boolean getSearchSSOIfNotExist() {
    return searchSSOIfNotExist;
  }

  /**
   * @param searchSSOIfNotExistObj the searchSSOIfNotExist to set
   */
  public final void setSearchSSOIfNotExist(final Boolean searchSSOIfNotExistObj) {
    this.searchSSOIfNotExist = searchSSOIfNotExistObj;
  }

  /**
   * @return the dateOfLastLoginMin value.
   */
  public final String getDateOfLastLoginMin() {
    return dateOfLastLoginMin;
  }

  /**
   * @param dateOfLastLoginMinObject to set.
   */
  public final void setDateOfLastLoginMin(final String dateOfLastLoginMinObject) {
    this.dateOfLastLoginMin = dateOfLastLoginMinObject;
  }

  /**
   * @return the dateOfLastLoginMax value.
   */
  public final String getDateOfLastLoginMax() {
    return dateOfLastLoginMax;
  }

  /**
   * @param dateOfLastLoginMaxObject to set.
   */
  public final void setDateOfLastLoginMax(final String dateOfLastLoginMaxObject) {
    this.dateOfLastLoginMax = dateOfLastLoginMaxObject;
  }

  /**
   * @return the codeSearch value.
   */
  public final UserCodeSearchDto getCodeSearch() {
    return codeSearch;
  }

  /**
   * @param codeSearchObject to set.
   */
  public final void setCodeSearch(final UserCodeSearchDto codeSearchObject) {
    this.codeSearch = codeSearchObject;
  }

  /**
   * @return the companyName.
   */
  public final String getCompanyName() {
    return companyName;
  }

  /**
   * @param companyNameObject the companyName to set.
   */
  public final void setCompanyName(final String companyNameObject) {
    this.companyName = companyNameObject;
  }

  /**
   * @return the name.
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameObject the name to set.
   */
  public final void setName(final String nameObject) {
    this.name = nameObject;
  }

}
