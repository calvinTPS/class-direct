package com.baesystems.ai.lr.cd.be.domain.dto.favourites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author NAvula.
 * @author sbollu.
 *
 */
public class FavouritesDto {

  /**
   * List of assetCodes to insert FAVOURITES table.
   */
  @Getter
  @Setter
  private List<String> add;
  /**
   * List of assetCodes to delete/remove from FAVOURITES table.
   */
  @Getter
  @Setter
  private List<String> remove;

}
