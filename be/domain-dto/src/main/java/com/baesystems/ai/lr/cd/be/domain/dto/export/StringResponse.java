package com.baesystems.ai.lr.cd.be.domain.dto.export;

import lombok.Getter;
import lombok.Setter;

/**
 * Standard class for string response.
 *
 * @author yng
 *
 */
public class StringResponse {

  /**
   * Response string value.
   */
  private String response;

  /**
   * File Name.
   */
  @Getter
  @Setter
  private String fileName;

  /**
   * Constructor.
   *
   * @param responseObj string.
   * @param fName String
   */
  public StringResponse(final String responseObj, final String fName) {
    super();
    this.response = responseObj;
    this.fileName = fName;
  }

  /**
   * Constructor.
   *
   * @param responseObj string.
   */
  public StringResponse(final String responseObj) {
    this.response = responseObj;
  }

  /**
   * Getter for response.
   *
   * @return response string.
   */
  public final String getResponse() {
    return response;
  }

  /**
   * Setter for response.
   *
   * @param responseObj string.
   */
  public final void setResponse(final String responseObj) {
    this.response = responseObj;
  }


}
