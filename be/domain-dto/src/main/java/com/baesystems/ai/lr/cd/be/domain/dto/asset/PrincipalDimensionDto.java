package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * @author RKaneysan
 */
public class PrincipalDimensionDto implements Serializable {

  /**
   * Default Serial Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * length Overall.
   */
  private Double lengthOverall;

  /**
   * Length Between Perpendiculars.
   */
  private String lengthBetweenPerpendiculars;

  /**
   * Load line Length.
   */
  private String loadlineLength;

  /**
   * Breadth Moulded.
   */
  private Double breadthMoulded;

  /**
   * Breadth Extreme.
   */
  private Double breadthExtreme;

  /**
   * Draught Max.
   */
  private Double draughtMax;

  /**
   * Air Draught.
   */
  private String airDraught;

  /**
   * Depth Moulded.
   */
  private Double depthMoulded;

  /**
   * Gross Tonnage.
   */
  private Integer grossTonnage;

  /**
   * Net Tonnage.
   */
  private Integer netTonnage;

  /**
   * Dead weight.
   */
  private Integer deadWeight;

  /**
   * Decks.
   */
  private String decks;

  /**
   * Propulsion.
   */
  private String propulsion;

  /**
   *
   * @return the lengthOverall
   */
  public final Double getLengthOverall() {
    return lengthOverall;
  }

  /**
   *
   * @param lengthOverallParam the lengthOverall to set
   */
  public final void setLengthOverall(final Double lengthOverallParam) {
    this.lengthOverall = lengthOverallParam;
  }

  /**
   *
   * @return the lengthBetweenPerpendiculars
   */
  public final String getLengthBetweenPerpendiculars() {
    return lengthBetweenPerpendiculars;
  }

  /**
   *
   * @param lengthBetweenPerpendicularsParam the lengthBetweenPerpendiculars to set.
   */
  public final void setLengthBetweenPerpendiculars(final String lengthBetweenPerpendicularsParam) {
    this.lengthBetweenPerpendiculars = lengthBetweenPerpendicularsParam;
  }

  /**
   *
   * @return the loadlineLength
   */
  public final String getLoadlineLength() {
    return loadlineLength;
  }

  /**
   *
   * @param loadlineLengthParam the loadlineLength to set.
   */
  public final void setLoadlineLength(final String loadlineLengthParam) {
    this.loadlineLength = loadlineLengthParam;
  }

  /**
   *
   * @return the breadthMoulded
   */
  public final Double getBreadthMoulded() {
    return breadthMoulded;
  }

  /**
   *
   * @param breadthMouldedParam the breadthMoulded to set.
   */
  public final void setBreadthMoulded(final Double breadthMouldedParam) {
    this.breadthMoulded = breadthMouldedParam;
  }

  /**
   *
   * @return the breadthExtreme
   */
  public final Double getBreadthExtreme() {
    return breadthExtreme;
  }

  /**
   *
   * @param breadthExtremeParam the breadthExtreme to set.
   */
  public final void setBreadthExtreme(final Double breadthExtremeParam) {
    this.breadthExtreme = breadthExtremeParam;
  }

  /**
   *
   * @return the draughtMax
   */
  public final Double getDraughtMax() {
    return draughtMax;
  }

  /**
   *
   * @param draughtMaxParam the draughtMax to set.
   */
  public final void setDraughtMax(final Double draughtMaxParam) {
    this.draughtMax = draughtMaxParam;
  }

  /**
   *
   * @return the airDraught.
   */
  public final String getAirDraught() {
    return airDraught;
  }

  /**
   *
   * @param airDraughtParam the airDraught to set.
   */
  public final void setAirDraught(final String airDraughtParam) {
    this.airDraught = airDraughtParam;
  }

  /**
   *
   * @return the depthMoulded.
   */
  public final Double getDepthMoulded() {
    return depthMoulded;
  }

  /**
   *
   * @param depthMouldedParam the depthMoulded to set.
   */
  public final void setDepthMoulded(final Double depthMouldedParam) {
    this.depthMoulded = depthMouldedParam;
  }

  /**
   *
   * @return the grossTonnage.
   */
  public final Integer getGrossTonnage() {
    return grossTonnage;
  }

  /**
   *
   * @param grossTonnageParam the grossTonnage to set.
   */
  public final void setGrossTonnage(final Integer grossTonnageParam) {
    this.grossTonnage = grossTonnageParam;
  }

  /**
   *
   * @return the netTonnage.
   */
  public final Integer getNetTonnage() {
    return netTonnage;
  }

  /**
   *
   * @param netTonnageParam the netTonnage to set.
   */
  public final void setNetTonnage(final Integer netTonnageParam) {
    this.netTonnage = netTonnageParam;
  }

  /**
   *
   * @return the deadWeight.
   */
  public final Integer getDeadWeight() {
    return deadWeight;
  }

  /**
   *
   * @param deadWeightParam the deadWeight to set.
   */
  public final void setDeadWeight(final Integer deadWeightParam) {
    this.deadWeight = deadWeightParam;
  }

  /**
   *
   * @return the decks
   */
  public final String getDecks() {
    return decks;
  }

  /**
   *
   * @param decksParam the decks to set.
   */
  public final void setDecks(final String decksParam) {
    this.decks = decksParam;
  }

  /**
   *
   * @return the propulsion.
   */
  public final String getPropulsion() {
    return propulsion;
  }

  /**
   *
   * @param propulsionParam the propulsion to set.
   */
  public final void setPropulsion(final String propulsionParam) {
    this.propulsion = propulsionParam;
  }
}
