package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.employee.EmployeeDto;
import com.baesystems.ai.lr.cd.be.domain.dto.service.JobSurveyDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sbollu
 * @author syalavarthi.
 *
 */
public class JobCDDto {
  /**
   * job id.
   */
  @Getter
  @Setter
  private Long id;
  /**
   * job location.
   */
  @Getter
  @Setter
  private String location;
  /**
   * Last visit date.
   */
  @Getter
  @Setter
  private String lastVisitDate;
  /**
   * First visit date.
   */
  @Getter
  @Setter
  private String firstVisitDate;
  /**
   * job employee role as leadSurveyor.
   */
  @Getter
  @Setter
  private EmployeeDto leadSurveyor;
  /**
   * job Services.
   */
  @Getter
  @Setter
  private List<JobSurveyDto> services;
  /**
   * is MMS service.
   */
  @Getter
  @Setter
  private Boolean isMMS;
  /**
   * Has ESP Reports.
   */
  @Getter
  @Setter
  private Boolean hasESPReport;
  /**
   * Has TM Reports.
   */
  @Getter
  @Setter
  private Boolean hasTMReport;
  /**
   * asset id.
   */
  @Getter
  @Setter
  private Long assetId;
  /**
   * job report type FAR or FSR.
   */
  @Getter
  @Setter
  private Boolean isFARorFSRReportType;
  /**
   * reports associated with job.
   */
  @Getter
  @Setter
  private List<ReportHDto> reports;
  /**
   * The job number.
   */
  @Getter
  @Setter
  private String jobNumber;
  /**
   * The current flag on this job.
   */
  @Getter
  @Setter
  private Long currentFlag;
  /**
   * The proposed flag on this job.
   */
  @Getter
  @Setter
  private Long proposedFlag;
  /**
   * The migrated flag.
   */
  @Getter
  @Setter
  private Boolean isMigrated;
}
