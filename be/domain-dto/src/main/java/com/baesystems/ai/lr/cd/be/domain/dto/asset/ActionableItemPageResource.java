package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * ActionableItemPageResourceHDto object extending ActionableItemPageResourceDto from MAST.
 *
 * @author yng
 *
 */
public class ActionableItemPageResource extends BasePageResource<ActionableItemHDto> {

  /**
   * List of asset note.
   */
  private List<ActionableItemHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<ActionableItemHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<ActionableItemHDto> actionableItemList) {
    this.content = actionableItemList;
  }

}
