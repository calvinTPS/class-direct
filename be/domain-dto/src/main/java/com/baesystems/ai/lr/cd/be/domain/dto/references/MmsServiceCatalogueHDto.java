package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;

/**
 * Provides an object to store MMS related service catalogues.
 * Used as a container to cache MMS service catalogue to be used in asset query upon reference data refresh.
 *
 * @author YWearn on 2017-10-02.
 */
public class MmsServiceCatalogueHDto extends ServiceCatalogueDto {

  /**
   * The generated UUID.
   */
  private static final long serialVersionUID = -3073382328321095322L;


}
