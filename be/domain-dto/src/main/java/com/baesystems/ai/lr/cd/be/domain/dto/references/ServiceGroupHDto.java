package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.dto.references.ServiceGroupDto;

/**
 * ServiceGroup Dto extension.
 *
 * @author VMandalapu
 *
 */
public class ServiceGroupHDto extends ServiceGroupDto {

  /**
   *
   */
  private static final long serialVersionUID = -6590277883083953579L;

}
