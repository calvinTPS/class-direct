package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;

/**
 * @author syalavarthi.
 *
 */
public class DeficiencyQueryHDto extends CodicilDefectQueryDto {

  /**
   * static serialVersionUID.
   */
  private static final long serialVersionUID = -4461098440066362347L;

}
