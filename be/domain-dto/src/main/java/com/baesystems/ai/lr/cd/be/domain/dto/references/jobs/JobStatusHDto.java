package com.baesystems.ai.lr.cd.be.domain.dto.references.jobs;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * Job Status HDTO.
 *
 * @author yng
 *
 */
public class JobStatusHDto extends ReferenceDataDto {

  /**
   * Generated UID.
   */
  private static final long serialVersionUID = -6742372443640918142L;

}
