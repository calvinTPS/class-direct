package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * @author VKolagutla
 *
 */
public class TaskPageResource extends BasePageResource<WorkItemLightHDto> {

  /**
   * list of tasks.
   */
  private List<WorkItemLightHDto> content;

  /**
   * @return the content
   */
  @Override
  public final List<WorkItemLightHDto> getContent() {
    return content;
  }

  /**
   * @param contentObj the content to set
   */
  @Override
  public final void setContent(final List<WorkItemLightHDto> contentObj) {
    this.content = contentObj;
  }



}
