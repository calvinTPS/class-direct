package com.baesystems.ai.lr.cd.be.domain.dto.security;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * @author vkovuru
 *
 */
public class RolesDto implements Serializable {

  /**
   * serialVersionID.
   */
  private static final long serialVersionUID = -848497892439268606L;

  /**
   * roles.
   */
  @NotNull
  private List<String> roles;

  /**
   * @return List<String> values.
   */
  public final List<String> getRoles() {
    return roles;
  }

  /**
   * @param rolesList roles.
   */
  public final void setRoles(final List<String> rolesList) {
    this.roles = rolesList;
  }
}
