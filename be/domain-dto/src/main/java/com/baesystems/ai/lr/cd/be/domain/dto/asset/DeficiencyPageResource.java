package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * @author syalavarthi.
 *
 */
public class DeficiencyPageResource extends BasePageResource<DeficiencyHDto> {
  /**
   * List of deficiencies.
   */
  private List<DeficiencyHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<DeficiencyHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<DeficiencyHDto> contentObj) {
    this.content = contentObj;
  }

}
