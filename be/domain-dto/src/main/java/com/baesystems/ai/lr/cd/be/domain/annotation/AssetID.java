package com.baesystems.ai.lr.cd.be.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Companion annotation to {@link RestrictedAsset} annotation. Will be use to mark param as asset identifier.
 *
 * @author Faizal Sidek
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface AssetID {
}
