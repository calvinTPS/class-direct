package com.baesystems.ai.lr.cd.be.domain.dto.user;

/**
 * User Dto.
 *
 * @author vmandalapu
 */
public class UserDto {

  /**
   * equasis variable.
   */
  private boolean equasis;

  /**
   * thetis variable.
   */
  private boolean thetis;

  /**
   * @return the equasis
   */
  public final boolean isEquasis() {
    return equasis;
  }

  /**
   * @param isEquasisObject the equasis to set
   */
  public final void setEquasis(final boolean isEquasisObject) {
    this.equasis = isEquasisObject;
  }

  /**
   * @return the thetis
   */
  public final boolean isThetis() {
    return thetis;
  }

  /**
   * @param isThetisObject the thetis to set
   */
  public final void setThetis(final boolean isThetisObject) {
    this.thetis = isThetisObject;
  }


}
