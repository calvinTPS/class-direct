package com.baesystems.ai.lr.cd.be.domain.dto.user;

/**
 * @author syalavarthi.
 *
 */
public class LastLoginRangeDto {
  /**
   * min login from.
   */
  private String from;
  /**
   * max login to.
   */
  private String to;

  /**
   * @return the from value.
   */
  public final String getFrom() {
    return from;
  }

  /**
   * @param fromObject to set.
   */
  public final void setFrom(final String fromObject) {
    this.from = fromObject;
  }

  /**
   * @return the to value.
   */
  public final String getTo() {
    return to;
  }

  /**
   * @param toObject to set.
   */
  public final void setTo(final String toObject) {
    this.to = toObject;
  }

}
