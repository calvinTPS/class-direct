/**
 * Root package for reference DTO.
 *
 * @author Faizal Sidek
 *
 */
package com.baesystems.ai.lr.cd.be.domain.dto.references;
