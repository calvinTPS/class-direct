package com.baesystems.ai.lr.cd.be.domain.dto.download;


import com.baesystems.ai.lr.cd.be.enums.DownloadTypeEnum;
import java.io.Serializable;

/**
 * Token for file download.
 *
 * @author Faizal Sidek
 */
public class FileToken implements Serializable {

  /**
   * Serial uuid.
   */
  private static final long serialVersionUID = 4849476717497615517L;

  /**
   * Type of download.
   */
  private DownloadTypeEnum type;

  /**
   * S3 file token if type is {@link DownloadTypeEnum#S3}.
   */
  private S3FileToken s3Token;

  /**
   * CS10 file token if type is {@link DownloadTypeEnum#CS10}.
   */
  private CS10FileToken cs10Token;

  /**
   * Getter for {@link #type}.
   *
   * @return value of type.
   */
  public final DownloadTypeEnum getType() {
    return type;
  }

  /**
   * Setter for {@link #type}.
   *
   * @param name value to be set.
   */
  public final void setType(final DownloadTypeEnum name) {
    this.type = name;
  }

  /**
   * Getter for {@link #s3Token}.
   *
   * @return value of s3Token.
   */
  public final S3FileToken getS3Token() {
    return s3Token;
  }

  /**
   * Setter for {@link #s3Token}.
   *
   * @param token value to be set.
   */
  public final void setS3Token(final S3FileToken token) {
    this.s3Token = token;
  }

  /**
   * Getter for {@link #cs10Token}.
   *
   * @return value of cs10Token.
   */
  public final CS10FileToken getCs10Token() {
    return cs10Token;
  }

  /**
   * Setter for {@link #cs10Token}.
   *
   * @param token value to be set.
   */
  public final void setCs10Token(final CS10FileToken token) {
    this.cs10Token = token;
  }
}
