package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.assets.AssetModelDto;

/**
 * Extends asset model {@link AssetModelDto}.
 *
 * @author syalavarthi.
 *
 */
public class AssetModelHDto extends AssetModelDto {

  /**
   * The default serial version id.
   */
  private static final long serialVersionUID = 1L;

}
