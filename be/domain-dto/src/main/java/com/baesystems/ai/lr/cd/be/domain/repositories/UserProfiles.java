package com.baesystems.ai.lr.cd.be.domain.repositories;

import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateConverter;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * @author VMandalapu (added new columns username, email, telephone, city, postcode and status on
 *         23/09/2016,Sushma)
 */
@Entity
@Table(name = "USERS")
public class UserProfiles implements Serializable {
  /**
   *
   */
  private static final long serialVersionUID = 8919641980755736329L;
  /**
   *
   */
  @Id
  @Column(name = "USER_ID", unique = true)
  @Getter
  @Setter
  private String userId;

  /**
   * Flag Id.
   */
  @Column(name = "FLAG_CODE", nullable = true)
  @Getter
  @Setter
  private String flagCode;

  /**
   * Ship Builder Code.
   */
  @Column(name = "SHIP_CODE", nullable = true)
  @Getter
  @Setter
  private String shipBuilderCode;

  /**
   * Client Code.
   */
  @Column(name = "CLIENT_CODE", nullable = true)
  @Getter
  @Setter
  private String clientCode;

  /**
   * firstName.
   */
  @Transient
  @Getter
  @Setter
  private String firstName;
  /**
   * LastName.
   */
  @Transient
  @Getter
  @Setter
  private String lastName;

  /**
   * User Account ExpiryDate.
   */
  @Column(name = "ACC_EXPIRY_DATE", nullable = true)
  @Convert(converter = LocalDateConverter.class)
  @Getter
  @Setter
  private LocalDate userAccExpiryDate;

  /**
   * User Account ExpiryDate.
   */
  @Column(name = "DATE_OF_MODIFICATION", nullable = true)
  @Convert(converter = LocalDateTimeConverter.class)
  @Getter
  @Setter
  private LocalDateTime dateOfModification;

  /**
   * Email.
   */
  @Column(name = "EMAIL", nullable = true)
  @Getter
  @Setter
  private String email;
  /**
   * status.
   */
  @ManyToOne
  @JoinColumn(name = "STATUS", referencedColumnName = "ID", nullable = false)
  @Getter
  @Setter
  private UserAccountStatus status;

  /**
   * restrictAll.
   */
  @Column(name = "RESTRICTALL", nullable = false, columnDefinition = "tinyint(1) default 0")
  @Getter
  @Setter
  private Boolean restrictAll;

  /**
   * Last login timestamp.
   */
  @Column(name = "LAST_LOGIN", nullable = true)
  @Convert(converter = LocalDateTimeConverter.class)
  @Getter
  @Setter
  private LocalDateTime lastLogin;

  /**
   * Roles.
   */
  @ManyToMany
  @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "USER_ID"),
      inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID"))
  @Getter
  @Setter
  private Set<Roles> roles;

  /**
   * Notifications.
   */
  @OneToMany
  @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
  @Getter
  @Setter
  private Set<Notifications> notifications;

  /**
   * eor.
   */
  @OneToMany
  @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
  @Getter
  @Setter
  private Set<UserEOR> eors;

  /**
   * Terms and Conditions.
   */
  @OneToMany
  @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
  @Getter
  @Setter
  private Set<UserTermsConditions> userTCs;

  /**
   * companyDetails.
   */
  @Transient
  @Getter
  @Setter
  private Company company;

  /**
   * cdUser.
   */
  @Transient
  @Getter
  @Setter
  private boolean cdUser;

  /**
   * ssoUser.
   */
  @Transient
  @Getter
  @Setter
  private boolean ssoUser;

  /**
   * companyPosition.
   */
  @Transient
  @Getter
  @Setter
  private String companyPosition;
  /**
   * CustomerType.
   */
  @Column(name = "CLIENT_TYPE", nullable = true)
  @Getter
  @Setter
  private String clientType;

  /**
   * Company Name.
   */
  @Column(name = "COMPANY_NAME", nullable = true)
  @Getter
  @Setter
  private String companyName;

  /**
   * Name of the user.
   */
  @Column(name = "NAME", nullable = true)
  @Getter
  @Setter
  private String name;

  /**
   * True if user is internal LR AD user.
   */
  @Transient
  @Getter
  @Setter
  private boolean internalUser;

  @Override
  public final String toString() {
    return "UserProfiles [userId=" + userId + ", flagCode=" + flagCode + ", shipBuilderCode=" + shipBuilderCode
        + ", clientCode=" + clientCode + ", firstName=" + firstName + ", lastName=" + lastName + ", userAccExpiryDate="
        + userAccExpiryDate + ", dateOfModification=" + dateOfModification + ", email=" + email + ", status=" + status
        + ", restrictAll=" + restrictAll + ", roles=" + roles + ", notifications=" + notifications + ", eors=" + eors
        + ", userTCs=" + userTCs + ", company=" + company + ", cdUser=" + cdUser + ", ssoUser=" + ssoUser
        + ", companyPosition=" + companyPosition + ", clientType=" + clientType + ", companyName=" + companyName
        + ", name=" + name + ", lastLogin=" + lastLogin + "]";
  }

}
