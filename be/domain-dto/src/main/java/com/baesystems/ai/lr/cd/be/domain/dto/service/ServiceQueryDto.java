package com.baesystems.ai.lr.cd.be.domain.dto.service;

import java.util.Date;
import java.util.List;

/**
 * @author VKolagutla
 *
 */
public class ServiceQueryDto {

  /**
   * statusId.
   */
  private List<Long> statusIdList;

  /**
   * productFamilyIdList.
   */
  private List<Long> productFamilyIdList;

  /**
   * minDueDate.
   */
  private Date minDueDate;

  /**
   * maxDueDate.
   */
  private Date maxDueDate;

  /**
   * Future due date flag.
   */
  private boolean includeFutureDueDates;


  /**
   * Getter for future due date.
   *
   * @return future due date flag.
   */
  public final boolean isIncludeFutureDueDates() {
    return includeFutureDueDates;
  }

  /**
   * Setter for future due date.
   *
   * @param includeFutureDueDatesArg due date flag.
   */
  public final void setIncludeFutureDueDates(final boolean includeFutureDueDatesArg) {
    this.includeFutureDueDates = includeFutureDueDatesArg;
  }

  /**
   * @return the statusIdList
   */
  public final List<Long> getStatusIdList() {
    return statusIdList;
  }

  /**
   * @param statusIdListObj the statusIdList to set
   */
  public final void setStatusIdList(final List<Long> statusIdListObj) {
    this.statusIdList = statusIdListObj;
  }

  /**
   * @return the productFamilyIdList
   */
  public final List<Long> getProductFamilyIdList() {
    return productFamilyIdList;
  }

  /**
   * @param productFamilyIdListObj the productFamilyIdList to set
   */
  public final void setProductFamilyIdList(final List<Long> productFamilyIdListObj) {
    this.productFamilyIdList = productFamilyIdListObj;
  }

  /**
   * @return the minDueDate
   */
  public final Date getMinDueDate() {
    return minDueDate;
  }

  /**
   * @param minDueDateObj the minDueDate to set
   */
  public final void setMinDueDate(final Date minDueDateObj) {
    this.minDueDate = minDueDateObj;
  }

  /**
   * @return the maxDueDate
   */
  public final Date getMaxDueDate() {
    return maxDueDate;
  }

  /**
   * @param maxDueDateObj the maxDueDate to set
   */
  public final void setMaxDueDate(final Date maxDueDateObj) {
    this.maxDueDate = maxDueDateObj;
  }


}
