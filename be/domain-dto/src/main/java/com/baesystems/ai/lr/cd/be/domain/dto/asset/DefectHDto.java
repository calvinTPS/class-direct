package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.dto.defects.DefectDto;

import lombok.Getter;
import lombok.Setter;

/**
 * DefectHDto extending DefectDto.
 *
 * @author yng
 *
 */
public class DefectHDto extends DefectDto {

  /**
   * Auto generated serial.
   */
  private static final long serialVersionUID = 883650090030641576L;

  /**
   * hydrate defect status.
   */
  @LinkedResource(referencedField = "status")
  private DefectStatusHDto defectStatusH;

  /**
   * Append jobs data.
   */
  private List<JobHDto> jobsH;

  /**
   * Append items data.
   */
  private List<LazyItemHDto> itemsH;

  /**
   * Append data for coc.
   */
  private List<CoCHDto> cocH;

  /**
   * Hydrate data from job field.
   */
  @Getter
  @Setter
  private JobHDto jobH;

  /**
   * Hydrate data from raisedOnJob field.
   */
  @Getter
  @Setter
  private JobHDto raisedOnJobH;

  /**
   * Hydrate data from closedOnJob field.
   */
  @Getter
  @Setter
  private JobHDto closedOnJobH;
  /**
   * The incident date epoch.
   */
  @Getter
  @Setter
  private Long incidentDateEpoch;

  /**
   * Getter for cocH.
   *
   * @return return list of coc.
   */
  public final List<CoCHDto> getCocH() {
    return cocH;
  }

  /**
   * Setter for cocH.
   *
   * @param cocHObj object.
   */
  public final void setCocH(final List<CoCHDto> cocHObj) {
    this.cocH = cocHObj;
  }

  /**
   * Getter for jobsH.
   *
   * @return return list of job.
   */
  public final List<JobHDto> getJobsH() {
    return jobsH;
  }

  /**
   * Setter for jobsH.
   *
   * @param jobsHObj object.
   */
  public final void setJobsH(final List<JobHDto> jobsHObj) {
    this.jobsH = jobsHObj;
  }

  /**
   * Getter for itemsH.
   *
   * @return return list of item.
   */
  public final List<LazyItemHDto> getItemsH() {
    return itemsH;
  }

  /**
   * Setter for itemsH.
   *
   * @param itemsHObj object.
   */
  public final void setItemsH(final List<LazyItemHDto> itemsHObj) {
    this.itemsH = itemsHObj;
  }

  /**
   * Getter for defect status.
   *
   * @return defect status dto.
   */
  public final DefectStatusHDto getDefectStatusH() {
    return defectStatusH;
  }

  /**
   * Setter for defect status.
   *
   * @param defectStatusHObj value.
   */
  public final void setDefectStatusH(final DefectStatusHDto defectStatusHObj) {
    this.defectStatusH = defectStatusHObj;
  }
  /**
   * Overrides setter for due date to calculate incident date epoch and Sets incidentDate epoch.
   *
   * @param incidentDate value.
   */
  @Override
  public final void setIncidentDate(final Date incidentDate) {
    super.setIncidentDate(incidentDate);
    Long epoch = null;
    if (incidentDate != null) {
      epoch = DateUtils.getDateToEpoch(incidentDate);
      setIncidentDateEpoch(epoch);
    }
  }

}
