package com.baesystems.ai.lr.cd.be.domain.dto.export;

/**
 * @author VKolagutla
 *
 */
public class PmsTaskListExportDto {


  /**
   * assetCode.
   */
  private String assetCode;

  /**
   * @return the assetCode
   */
  public final String getAssetCode() {
    return assetCode;
  }

  /**
   * @param assetCodeStr the assetCode to set
   */
  public final void setAssetCode(final String assetCodeStr) {
    this.assetCode = assetCodeStr;
  }


}
