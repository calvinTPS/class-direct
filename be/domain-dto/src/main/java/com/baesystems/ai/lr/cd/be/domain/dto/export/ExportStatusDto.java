package com.baesystems.ai.lr.cd.be.domain.dto.export;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The model to store export status.
 *
 * @author YWearn 2018-02-27
 */
@ToString
public class ExportStatusDto implements Serializable {

  /**
   * The unique export id.
   */
  @Setter
  @Getter
  private String jobId;

  /**
   * The timestamp that the export process is invoke.
   */
  @Setter
  @Getter
  private Date startTime;

  /**
   * The timestamp when the export process is completed.
   */
  @Setter
  @Getter
  private Date endTime;

  /**
   * The unique token used for actual download.
   */
  @Setter
  @Getter
  private String downloadToken;

  /**
   * The export status code. Possible value would be:
   *
   * <ul>
   *   <li>202 - Export is accepted and in progress.</li>
   *   <li>200 - Export is completed.</li>
   *   <li>404 - Export job id record not found.</li>
   *   <li>500 - Error in the export operation</li>
   * </ul>
   */
  @Setter
  @Getter
  private int statusCode;

  /**
   * The export error description if any.
   */
  @Setter
  @Getter
  private String statusDesc;

  /**
   * File Name.
   */
  @Setter
  @Getter
  private String fileName;
}
