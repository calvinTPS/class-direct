package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * @author VKolagutla
 *
 */
public class DueStatusDto extends ReferenceDataDto {

  /**
   *
   */
  private static final long serialVersionUID = 2916337094734722867L;

}
