package com.baesystems.ai.lr.cd.be.enums;

/**
 * Enumeration for subfleet update operation.
 *
 * @author Jonathan Lin
 */
public enum SubfleetMoveOperationEnum {
  /**
   * Move to accessible.
   */
  ACCESSIBLE("ACCESSIBLE"),
  /**
   * Move to restricted.
   */
  RESTRICTED("RESTRICTED");

  /**
   * Name of operation.
   */
  private String name;

  /**
   * Default constructor.
   *
   * @param opName Name of the operation.
   */
  private SubfleetMoveOperationEnum(final String opName) {
    this.name = opName;
  }

  /**
   * Getter for {@link #name}.
   *
   * @return Name of the operation.
   */
  public final String getName() {
    return name;
  }
}
