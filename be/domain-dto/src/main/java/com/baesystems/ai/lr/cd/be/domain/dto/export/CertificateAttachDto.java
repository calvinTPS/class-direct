package com.baesystems.ai.lr.cd.be.domain.dto.export;

/**
 * @author sbollu
 *
 */
public class CertificateAttachDto {
  /**
   * CertificateId.
   */
  private Long certId;
  /**
   * jobId.
   */
  private Long jobId;

  /**
   * @return the certId.
   */
  public final Long getCertId() {
    return certId;
  }

  /**
   * @param certIdObject to set.
   */
  public final void setCertId(final Long certIdObject) {
    this.certId = certIdObject;
  }

  /**
   * @return the jobId.
   */
  public final Long getJobId() {
    return jobId;
  }

  /**
   * @param jobIdObject to set.
   */
  public final void setJobId(final Long jobIdObject) {
    this.jobId = jobIdObject;
  }
}
