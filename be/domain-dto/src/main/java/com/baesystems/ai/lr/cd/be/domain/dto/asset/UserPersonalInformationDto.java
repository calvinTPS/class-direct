package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * @author RKaneysan
 */
public class UserPersonalInformationDto implements Serializable {

  /**
   * Default Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Name.
   */
  private String name;

  /**
   * Address.
   */
  private String address;

  /**
   * Email Address.
   */
  private String emailAddress;

  /**
   * Phone Number.
   */
  private String phoneNumber;

  /**
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameParam the name to set
   */
  public final void setName(final String nameParam) {
    this.name = nameParam;
  }

  /**
   * @return the address
   */
  public final String getAddress() {
    return address;
  }

  /**
   * @param addressParam the address to set
   */
  public final void setAddress(final String addressParam) {
    this.address = addressParam;
  }

  /**
   * @return the emailAddress
   */
  public final String getEmailAddress() {
    return emailAddress;
  }

  /**
   * @param emailAddressParam the emailAddress to set
   */
  public final void setEmailAddress(final String emailAddressParam) {
    this.emailAddress = emailAddressParam;
  }

  /**
   * @return the phoneNumber
   */
  public final String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * @param phoneNumberParam the phoneNumber to set
   */
  public final void setPhoneNumber(final String phoneNumberParam) {
    this.phoneNumber = phoneNumberParam;
  }
}
