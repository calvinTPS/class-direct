package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.CodicilStatusDto;

/**
 * Class status HDTO.
 *
 * @author msidek
 *
 */
public class CodicilStatusHDto extends CodicilStatusDto {

  /**
   * Auto generated serial.
   */
  private static final long serialVersionUID = -2338351860284373556L;

}
