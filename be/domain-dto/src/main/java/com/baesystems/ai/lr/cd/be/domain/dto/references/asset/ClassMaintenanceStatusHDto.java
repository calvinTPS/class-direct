package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * Class maintainence status HDTO.
 *
 * @author yng
 *
 */
public class ClassMaintenanceStatusHDto extends ReferenceDataDto {

  /**
   * Generated serial id.
   */
  private static final long serialVersionUID = 8661389646471972689L;


}
