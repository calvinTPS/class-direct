package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.Date;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;

import lombok.Getter;
import lombok.Setter;

/**
 * AssetNoteHDto extending AssetNoteDto.
 *
 * @author yng
 *
 */
public class AssetNoteHDto extends AssetNoteDto {

  /**
   * Codicil type.
   */
  private final String codicilType = CodicilType.AN.getName();

  /**
   * hydrate codicilCategory.
   */
  @LinkedResource(referencedField = "category")
  private CodicilCategoryHDto categoryH;

  /**
   * codicil status.
   */
  @LinkedResource(referencedField = "status")
  private CodicilStatusHDto statusH;

  /**
   * append data for asset item.
   */
  private LazyItemHDto assetItemH;
  /**
   * hydrate job.
   */
  @Getter
  @Setter
  private JobHDto jobH;
  /**
   * The Due date epoch.
   */
  @Getter
  @Setter
  private Long dueDateEpoch;
  /**
   * The Imposed date epoch.
   */
  @Getter
  @Setter
  private Long imposedDateEpoch;

  /**
   * Getter for codicil type.
   *
   * @return codicil type.
   */
  public final String getCodicilType() {
    return codicilType;
  }

  /**
   * Getter for statusH.
   *
   * @return codicil status.
   */
  public final CodicilStatusHDto getStatusH() {
    return statusH;
  }

  /**
   * Setter for statusH.
   *
   * @param statusHObject object.
   */
  public final void setStatusH(final CodicilStatusHDto statusHObject) {
    this.statusH = statusHObject;
  }

  /**
   * Getter for assetItemH.
   *
   * @return asset item.
   */
  public final LazyItemHDto getAssetItemH() {
    return assetItemH;
  }

  /**
   * Setter for assetItemH.
   *
   * @param assetItemHObj object.
   */
  public final void setAssetItemH(final LazyItemHDto assetItemHObj) {
    this.assetItemH = assetItemHObj;
  }

  /**
   * Getter for codicilCategoryH.
   *
   * @return category object.
   *
   */
  public final CodicilCategoryHDto getCategoryH() {
    return categoryH;
  }

  /**
   * Setter for codicilCategoryH.
   *
   * @param categoryHObject to be set.
   *
   */
  public final void setCategoryH(final CodicilCategoryHDto categoryHObject) {
    this.categoryH = categoryHObject;
  }

  /**
   * Overrides setter for due date to calculate due date epoch and Sets due date epoch.
   *
   * @param dueDate value.
   */
  @Override
  public final void setDueDate(final Date dueDate) {
    super.setDueDate(dueDate);
    Long epoch = null;
    if (dueDate != null) {
      epoch = DateUtils.getDateToEpoch(dueDate);
      setDueDateEpoch(epoch);
    }
  }

  /**
   * Overrides setter for imposed date to calculate imposed date epoch and Sets imposed date epoch.
   *
   * @param imposedDate value.
   */
  @Override
  public final void setImposedDate(final Date imposedDate) {
    super.setImposedDate(imposedDate);
    Long epoch = null;
    if (imposedDate != null) {
      epoch = DateUtils.getDateToEpoch(imposedDate);
      setImposedDateEpoch(epoch);
    }
  }
}
