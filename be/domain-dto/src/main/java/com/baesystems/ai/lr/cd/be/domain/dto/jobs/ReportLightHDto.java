package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;

import lombok.Getter;
import lombok.Setter;

/**
 * ReportLightHDto extending ReportLightDto.
 *
 * @author yng
 *
 */
public class ReportLightHDto extends ReportLightDto {
  /**
   * Linked to {@link ReportLightHDto}.
   */
  @LinkedResource(referencedField = "reportType")
  @Getter
  @Setter
  private ReportTypeHDto reportTypeDto;

  /**
   * Dto for {@link #job}.
   */
  @Getter
  @Setter
  private JobCDDto jobH;

  /**
   * Attachments associated with this report.
   */
  @Getter
  @Setter
  private List<SupplementaryInformationHDto> attachments;

}
