package com.baesystems.ai.lr.cd.be.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Enum for codicil type.
 *
 * @author yng
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CodicilType {
  /**
   * coc.
   */
  COC(1L, "COC"),
  /**
   * actionable item.
   */
  AI(2L, "AI"),
  /**
   * asset note.
   */
  AN(3L, "AN"),
  /**
   * statutory finding.
   */
  SF(5L, "SF"),
  /**
   * Major non confirmitive note.
   */
  MNCN(6L, "MNCN");

  /**
   * name.
   */
  private String name;
  /**
   * long id.
   */
  private Long id;

  /**
   * CodicilType constructor.
   *
   * @param idObj id.
   * @param nameObj name.
   */
  CodicilType(final Long idObj, final String nameObj) {
    this.name = nameObj;
    this.id = idObj;
  }

  /**
   * Getter for name.
   *
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * Getter for id.
   *
   * @return the id
   */
  public final Long getId() {
    return id;
  }
}
