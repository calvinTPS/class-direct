package com.baesystems.ai.lr.cd.be.constants;

/**
 * Class Direct Cache Constants.
 *
 * @author Jonathan Lin
 */
public final class CacheConstants {

  /**
   * Private constructor for CacheConstants.
   */
  private CacheConstants() {

  }

  /**
   * Cache Name for Assets.
   */
  public static final String CACHE_ASSETS = "assets";

  /**
   * Cache Name for Assets by User.
   */
  public static final String CACHE_ASSETS_BY_USER = "assetsByUser";

  /**
   * Cache Name for User.
   */
  public static final String CACHE_USERS = "users";

  /**
   * Cache Name for Subfleet.
   */
  public static final String CACHE_SUBFLEET = "subfleet";

  /**
   * Cache Name for Asset Export Status.
   */
  public static final String CACHE_ASSETS_EXPORT = "assetsExport";

  /**
   * Prefix for asset in cache key.
   */
  public static final String PREFIX_ASSET = "Asset:";

  /**
   * Prefix for user ID in cache key.
   */
  public static final String PREFIX_USER_ID = "UserId:";

  /**
   * Prefix for user in cache key.
   */
  public static final String PREFIX_USER = "User:";

  /**
   * Postfix for subfleet ids.
   */
  public static final String POSTFIX_SUBFLEET_IDS = ":SubfleetIds";

  /**
   * Postfix for restricted asset ids.
   */
  public static final String POSTFIX_RESTRICTED_ASSET_IDS = ":RestrictedAssetIds";

  /**
   * Postfix for accessible asset ids.
   */
  public static final String POSTFIX_ACCESSIBLE_ASSET_IDS = ":AccessibleAssetIds";

  /**
   * Appender for assets by user.
   */
  public static final String INFIX_ASSETS = ":Asset:";

  /**
   * The appender for assets export.
   */
  public static final String INFIX_EXPORT = ":Export:";

}
