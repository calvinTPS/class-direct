package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;

/**
 * AssetType dto object extending AssetTypeDto from MAST.
 *
 * @author msidek
 *
 */
public class AssetTypeHDto extends AssetTypeDto {
  /**
   * static serial id.
   */
  private static final long serialVersionUID = 7896156791826230756L;

  /**
   * Linked resource to AssetCategoryDto.
   */
  @LinkedResource(referencedField = "category")
  private AssetCategoryHDto categoryDto;

  /**
   * Get AssetCategoryDto.
   *
   * @return asset category
   *
   */
  public final AssetCategoryHDto getCategoryDto() {
    return categoryDto;
  }

  /**
   * Set Category DTO.
   *
   * @param dto to be set.
   *
   */
  public final void setCategoryDto(final AssetCategoryHDto dto) {
    this.categoryDto = dto;
  }

  @Override
  public final String toString() {
    return "AssetType [categoryDto=" + categoryDto + ", getCode()=" + getCode() + ", getName()=" + getName()
        + ", getParentId()=" + getParentId() + ", getLevelIndication()=" + getLevelIndication() + ", getId()=" + getId()
        + ", getInternalId()=" + getInternalId() + "]";
  }
}
