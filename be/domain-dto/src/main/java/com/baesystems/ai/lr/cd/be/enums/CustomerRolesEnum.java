package com.baesystems.ai.lr.cd.be.enums;

/**
 * @author syalavarthi.
 *
 */
public enum CustomerRolesEnum {
  /**
   * ship builder role.
   */
  SHIP_BUILDER("publishedVersion.shipBuilderInfo"),
  /**
   * Ship manager role..
   */
  SHIP_MANAGER("publishedVersion.shipManagerInfo"),
  /**
   * ship operator role.
   */
  SHIP_OPERATOR("publishedVersion.operatorInfo"),
  /**
   * technical manager role.
   */
  TECHNICAL_MANAGER("publishedVersion.techManagerInfo"),
  /**
   * company role..
   */
  DOC_COMPANY("publishedVersion.docCompanyInfo"),
  /**
   * registered owner role..
   */
  REGISTERED_OWNER("publishedVersion.registeredOwnerInfo"),
  /**
   * group role.
   */
  GROUP_BENEFICIAL_OWNER("publishedVersion.groupOwnerInfo");

  /**
   * Path mapping to Mast DTO.
   */
  private String path;

  /**
   * Default constructor.
   * @param dtoPath path of dto.
   */
  CustomerRolesEnum(final String dtoPath) {
    this.path = dtoPath;
  }

  /**
   * Getter for {@link #path}.
   *
   * @return value of path.
   */
  public final String getPath() {
    return path;
  }
}
