package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.List;

import com.baesystems.ai.lr.dto.tasks.WorkItemModelItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPreviewListDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sbollu
 *
 */
public class WorkItemPreviewListHDto extends WorkItemPreviewListDto<WorkItemModelItemDto> {
  /**
  *
  */
  @Getter
  @Setter
  private List<WorkItemPreviewScheduledServiceHDto> servicesH;
}
