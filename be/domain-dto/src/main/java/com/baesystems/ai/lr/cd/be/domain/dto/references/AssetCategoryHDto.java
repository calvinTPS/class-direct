package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.dto.references.AssetCategoryDto;

/**
 * Asset category extending AssetCategoryDto.
 *
 * @author Faizal Sidek
 *
 */
public class AssetCategoryHDto extends AssetCategoryDto {
  /**
   * Static serial version.
   *
   */
  private static final long serialVersionUID = 2942305200107277185L;

  /**
   * Generate string representation.
   *
   */
  @Override
  public final String toString() {
    return "AssetCategory [getName()=" + getName() + ", getDescription()=" + getDescription() + "]";
  }
}
