package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.DueStatusField;
import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.IDueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ActionTakenHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.CodicilType;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.dto.codicils.CoCDto;

/**
 * CocHDto extending CoCDto.
 *
 * @author yng
 *
 */
@SuppressWarnings("CPD-START")
public class CoCHDto extends CoCDto implements IDueStatusDto {

  /**
   * Codicil type.
   */
  private final String codicilType = CodicilType.COC.getName();

  /**
   * hydrate codicilCategory.
   */
  @LinkedResource(referencedField = "category")
  private CodicilCategoryHDto categoryH;

  /**
   * dueStatus.
   */
  @DueStatusField
  private DueStatus dueStatusH;

  /**
   * codicil status.
   */
  @LinkedResource(referencedField = "status")
  private CodicilStatusHDto statusH;

  /**
   * Action taken.
   */
  @LinkedResource(referencedField = "actionTaken")
  private ActionTakenHDto actionsTakenH;

  /**
   * hydrate job.
   */
  private JobHDto jobH;

  /**
   * append data for coc defect.
   */
  private DefectHDto defectH;


  /**
   * append data for coc repair.
   */
  private List<RepairHDto> repairH;

  /**
   * append data for asset item.
   */
  private LazyItemHDto assetItemH;

  /**
   * Asset code.
   */
  private String code;

  /**
   * Getter for {@link #code}.
   *
   * @return value of code.
   */
  public final String getCode() {
    return code;
  }

  /**
   * Setter for {@link #code}.
   *
   * @param assetCode value to be set.
   */
  public final void setCode(final String assetCode) {
    this.code = assetCode;
  }

  /**
   * Getter for codicil type.
   *
   * @return codicil type.
   */
  public final String getCodicilType() {
    return codicilType;
  }

  /**
   * Getter for statusH.
   *
   * @return codicil status.
   */
  public final CodicilStatusHDto getStatusH() {
    return statusH;
  }

  /**
   * Setter for statusH.
   *
   * @param statusHObject object.
   */
  public final void setStatusH(final CodicilStatusHDto statusHObject) {
    this.statusH = statusHObject;
  }

  /**
   * Getter for assetItemH.
   *
   * @return asset item.
   */
  public final LazyItemHDto getAssetItemH() {
    return assetItemH;
  }

  /**
   * Setter for assetItemH.
   *
   * @param assetItemHObject object.
   */
  public final void setAssetItemH(final LazyItemHDto assetItemHObject) {
    this.assetItemH = assetItemHObject;
  }

  /**
   * Getter for defectH.
   *
   * @return defect.
   */
  public final DefectHDto getDefectH() {
    return defectH;
  }

  /**
   * Setter for defectH.
   *
   * @param defectHObject object.
   */
  public final void setDefectH(final DefectHDto defectHObject) {
    this.defectH = defectHObject;
  }

  /**
   * Getter for repairH.
   *
   * @return list of repair.
   */
  public final List<RepairHDto> getRepairH() {
    return repairH;
  }

  /**
   * Setter for repairH.
   *
   * @param repairHObject list.
   */
  public final void setRepairH(final List<RepairHDto> repairHObject) {
    this.repairH = repairHObject;
  }

  /**
   * @return the jobH
   */
  public final JobHDto getJobH() {
    return jobH;
  }

  /**
   * @param jobHObject the jobH to set
   */
  public final void setJobH(final JobHDto jobHObject) {
    this.jobH = jobHObject;
  }

  /**
   * Getter for codicilCategoryH.
   *
   * @return category object.
   *
   */
  public final CodicilCategoryHDto getCategoryH() {
    return categoryH;
  }

  /**
   * @return the dueStatusH.
   */
  public final DueStatus getDueStatusH() {
    return dueStatusH;
  }

  /**
   * @param dueStatusHObj the dueStatusH to set.
   */
  public final void setDueStatusH(final DueStatus dueStatusHObj) {
    this.dueStatusH = dueStatusHObj;
  }

  /**
   * Setter for codicilCategoryH.
   *
   * @param categoryHObject to be set.
   *
   */
  public final void setCategoryH(final CodicilCategoryHDto categoryHObject) {
    this.categoryH = categoryHObject;
  }

  /**
   * @return the actionsTakenH.
   */
  public final ActionTakenHDto getActionsTakenH() {
    return actionsTakenH;
  }

  /**
   * Setter for actionsTakenH.
   *
   * @param actionsTakenHObject to be set.
   *
   */
  public final void setActionsTakenH(final ActionTakenHDto actionsTakenHObject) {
    this.actionsTakenH = actionsTakenHObject;
  }

  /**
   * @return dueDate in Epoch.
   */
  public final Long getDueDateEpoch() {
    Long epoch = null;
    if (null != getDueDate()) {
      epoch = DateUtils.getDateToEpoch(getDueDate());
    }
    return epoch;
  }

  /**
   * @return imposedDate in Epoch.
   */
  public final Long getImposedDateEpoch() {
    Long epoch = null;
    if (null != getImposedDate()) {
      epoch = DateUtils.getDateToEpoch(getImposedDate());
    }
    return epoch;
  }
}
