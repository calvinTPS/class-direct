package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

/**
 * @author sbollu
 *
 */
@Entity
@Table(name = "TERMS_CONDITIONS")
public class TermsConditions implements Serializable {


  /**
   *
   */
  private static final long serialVersionUID = 7931514977097806525L;

  /**
   */
  private static final int MYSQL_TEXT_MAX_LENGTH = 65365;

  /**
   * Terms and Conditions Id.
   */
  @Id
  @Column(name = "ID")
  private Long tcId;

  /**
   * Terms and Conditions Description.
   */
  @Lob
  @Column(columnDefinition = "TEXT", name = "TC_DESCRIPTION", length = MYSQL_TEXT_MAX_LENGTH)
  private String tcDesc;


  /**
   * Terms and Conditions Date.
   */
  @Column(name = "TC_DATE", nullable = false)
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime tcDate;

  /**
   * Terms and Conditions version.
   */
  @Column(name = "TC_VERSION")
  private Long tcVersion;

  /**
   * Terms and Conditions author.
   */
  @Column(name = "TC_AUTHOR")
  private String tcAuthor;

  /**
   * @return the tcId.
   */
  public final Long getTcId() {
    return tcId;
  }

  /**
   * @param tcIdObject to set.
   */
  public final void setTcId(final Long tcIdObject) {
    this.tcId = tcIdObject;
  }

  /**
   * @return the tcDesc.
   */
  public final String getTcDesc() {
    return tcDesc;
  }

  /**
   * @param tcDescObject to set.
   */
  public final void setTcDesc(final String tcDescObject) {
    this.tcDesc = tcDescObject;
  }

  /**
   * @return the tcDate.
   */
  public final LocalDateTime getTcDate() {
    return tcDate;
  }

  /**
   * @param tcDateObject to set.
   */
  public final void setTcDate(final LocalDateTime tcDateObject) {
    this.tcDate = tcDateObject;
  }

  /**
   * @return the tcVersion.
   */
  public final Long getTcVersion() {
    return tcVersion;
  }

  /**
   * @param tcVersionObject to set.
   */
  public final void setTcVersion(final Long tcVersionObject) {
    this.tcVersion = tcVersionObject;
  }

  /**
   * @return the tcAuthor.
   */
  public final String getTcAuthor() {
    return tcAuthor;
  }

  /**
   * @param tcAuthorObject to set.
   */
  public final void setTcAuthor(final String tcAuthorObject) {
    this.tcAuthor = tcAuthorObject;
  }

  @Override
  public final String toString() {
    return "TermsConditions [tcId=" + tcId + ", tcDesc=" + tcDesc + ", tcDate=" + tcDate + ", tcVersion=" + tcVersion
        + ", tcAuthor=" + tcAuthor + "]";
  }


}
