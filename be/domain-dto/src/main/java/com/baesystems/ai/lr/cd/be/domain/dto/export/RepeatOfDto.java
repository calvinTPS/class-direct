package com.baesystems.ai.lr.cd.be.domain.dto.export;

import java.util.Date;

import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.dto.LinkResource;

/**
 * This Dto is use in service query with future due date calculation purpose.
 *
 * @author yng
 *
 */
public class RepeatOfDto {

  /**
   * due date.
   */
  private Date dueDate;

  /**
   * due status.
   */
  private LinkResource dueStatus;

  /**
   * hydrate due status.
   */
  private DueStatus dueStatusH;

  /**
   * lower range date.
   */
  private Date lowerRangeDate;

  /**
   * upper range date.
   */
  private Date upperRangeDate;

  /**
   * repeat of (service id).
   */
  private Long repeatOf;

  /**
   * Getter for due date.
   *
   * @return due date.
   */
  public final Date getDueDate() {
    return dueDate;
  }

  /**
   * Setter for due date.
   *
   * @param dueDateArg value.
   */
  public final void setDueDate(final Date dueDateArg) {
    this.dueDate = dueDateArg;
  }

  /**
   * Getter for due status.
   *
   * @return due status.
   */
  public final LinkResource getDueStatus() {
    return dueStatus;
  }

  /**
   * Setter for due status.
   *
   * @param dueStatusArg value.
   */
  public final void setDueStatus(final LinkResource dueStatusArg) {
    this.dueStatus = dueStatusArg;
  }

  /**
   * Getter for hydrated due status.
   *
   * @return hydrated due status.
   */
  public final DueStatus getDueStatusH() {
    return dueStatusH;
  }

  /**
   * Setter for hydrated due status.
   *
   * @param dueStatusHArg value.
   */
  public final void setDueStatusH(final DueStatus dueStatusHArg) {
    this.dueStatusH = dueStatusHArg;
  }

  /**
   * Getter for lower range date.
   *
   * @return lower range date.
   */
  public final Date getLowerRangeDate() {
    return lowerRangeDate;
  }

  /**
   * Setter for lower range date.
   *
   * @param lowerRangeDateArg value.
   */
  public final void setLowerRangeDate(final Date lowerRangeDateArg) {
    this.lowerRangeDate = lowerRangeDateArg;
  }

  /**
   * Getter for upper range date.
   *
   * @return upper range date.
   */
  public final Date getUpperRangeDate() {
    return upperRangeDate;
  }

  /**
   * Setter for upper range date.
   *
   * @param upperRangeDateArg value.
   */
  public final void setUpperRangeDate(final Date upperRangeDateArg) {
    this.upperRangeDate = upperRangeDateArg;
  }

  /**
   * Getter for repeat of service id.
   *
   * @return repeat of service id.
   */
  public final Long getRepeatOf() {
    return repeatOf;
  }

  /**
   * Setter for repeat of service id.
   *
   * @param repeatOfArg value.
   */
  public final void setRepeatOf(final Long repeatOfArg) {
    this.repeatOf = repeatOfArg;
  }



}
