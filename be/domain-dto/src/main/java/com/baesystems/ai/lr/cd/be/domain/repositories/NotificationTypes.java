package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author RKaneysan
 */
@Entity
@Table(name = "NOTIFICATION_TYPES")
public class NotificationTypes implements Serializable {

  /**
   * Default serial version id for Notification Types Entity Object.
   */
  private static final long serialVersionUID = 1L;

  /**
   * id column for notification types table.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;

  /**
   * name column for notification types table.
   */
  @Column(name = "NAME")
  private String name;

  /**
   * @return the id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * @param idParam the id to set.
   */
  public final void setId(final Long idParam) {
    id = idParam;
  }

  /**
   * @return the name.
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameParam the name to set.
   */
  public final void setName(final String nameParam) {
    this.name = nameParam;
  }
}
