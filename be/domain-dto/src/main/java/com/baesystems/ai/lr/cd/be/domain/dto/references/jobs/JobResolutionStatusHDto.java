package com.baesystems.ai.lr.cd.be.domain.dto.references.jobs;

import com.baesystems.ai.lr.dto.references.ResolutionStatusDto;

/**
 * @author VKolagutla
 *
 */
public class JobResolutionStatusHDto extends ResolutionStatusDto {

  /**
   * Auto generated serial.
   */
  private static final long serialVersionUID = 7909226692626137741L;

}
