package com.baesystems.ai.lr.cd.be.domain.dto.port;

/**
 * Created by PFauchon on 17/8/2016.
 */
public class PortAgentDto {
  /**
   *
   */
  private String name;
  /**
   *
   */
  private String phoneNumber;
  /**
   *
   */
  private String email;

  /**
   * @return value.
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameObject value.
   */
  public final void setName(final String nameObject) {
    this.name = nameObject;
  }

  /**
   * @return value.
   */
  public final String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * @param phoneNumberObject value.
   */
  public final void setPhoneNumber(final String phoneNumberObject) {
    this.phoneNumber = phoneNumberObject;
  }

  /**
   * @return value.
   */
  public final String getEmail() {
    return email;
  }

  /**
   * @param emailObject value.
   */
  public final void setEmail(final String emailObject) {
    this.email = emailObject;
  }
}
