package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.List;

import com.baesystems.ai.lr.dto.tasks.WorkItemDto;

import lombok.Getter;
import lombok.Setter;
/**
 * Extends from WorkItemDto.
 *
 * @author SYalavarthi.
 *
 */
public class WorkItemHDto extends WorkItemDto {

  /**
   * The defalut serialVersionUID.
   */
  private static final long serialVersionUID = 1L;
  /**
   * The workItemQAModel for esp appendix 1.
   */
  @Getter
  @Setter
  private List<WorkItemQAModel> workItemQAModel;
  /**
   * The task narrativeslist.
   */
  @Getter
  @Setter
  private String[] taskNarratives;
}
