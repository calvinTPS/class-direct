package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * DefectPageResourceHDto object extending DefectPageResourceDto from MAST.
 *
 * @author yng
 *
 */
public class DefectPageResource extends BasePageResource<DefectHDto> {

  /**
   * List of defect.
   */
  private List<DefectHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<DefectHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<DefectHDto> defectList) {
    this.content = defectList;
  }

}
