package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * StatutoryFindingPageResource object extending StatutoryFindingPageResourceDto from MAST.
 *
 * @author syalavarthi.
 *
 */
public class StatutoryFindingPageResource extends BasePageResource<StatutoryFindingHDto> {
  /**
   * List of statutoryFinding.
   */
  private List<StatutoryFindingHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<StatutoryFindingHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<StatutoryFindingHDto> contentObj) {
    this.content = contentObj;
  }

}
