package com.baesystems.ai.lr.cd.be.domain.dto.user;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * Page resource for {@link Userprofiles}.
 *
 * @author sbollu
 *
 */
public class UserProfilesPageResource extends BasePageResource<UserProfiles> {

  /**
   * List of users.
   *
   */
  private List<UserProfiles> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<UserProfiles> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<UserProfiles> usersList) {
    this.content = usersList;
  }
}
