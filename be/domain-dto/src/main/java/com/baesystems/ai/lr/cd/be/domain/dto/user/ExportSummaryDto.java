package com.baesystems.ai.lr.cd.be.domain.dto.user;

import java.util.List;

/**
 * @author syalavarthi.
 *
 */
public class ExportSummaryDto {
  /**
   * accountTypes.
   */
  private List<String> accountTypes;
  /**
   * lastLoginDate.
   */
  private LastLoginRangeDto lastLoginDate;

  /**
   * get accountTypes.
   *
   * @return accountTypes.
   */
  public final List<String> getAccountTypes() {
    return accountTypes;
  }

  /**
   * setter for accountTypes.
   *
   * @param accountTypesObj value to accountTypes.
   */
  public final void setAccountTypes(final List<String> accountTypesObj) {
    this.accountTypes = accountTypesObj;
  }

  /**
   * @return the role value.
   */

  public final LastLoginRangeDto getLastLoginDate() {
    return lastLoginDate;
  }

  /**
   * @param lastLoginDateObject to set.
   */
  public final void setLastLoginDate(final LastLoginRangeDto lastLoginDateObject) {
    this.lastLoginDate = lastLoginDateObject;
  }
}
