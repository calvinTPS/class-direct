package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * @author RKaneysan
 */
public class EquipmentDetailsDto implements Serializable {

  /**
   * Default Serial Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Cable Length.
   */
  private Double cableLength;

  /**
   * Cable Grade.
   */
  private String cableGrade;

  /**
   * Cable Diameter.
   */
  private String cableDiameter;

  /**
   * Equipment Letter.
   */
  private String equipmentLetter;

  /**
   * @return the cableLength.
   */
  public final Double getCableLength() {
    return cableLength;
  }

  /**
   *
   * @param cableLengthParam the cableLength to set.
   */
  public final void setCableLength(final Double cableLengthParam) {
    this.cableLength = cableLengthParam;
  }

  /**
   *
   * @return the cableGrade.
   */
  public final String getCableGrade() {
    return cableGrade;
  }

  /**
   *
   * @param cableGradeParam the cableGrade to set.
   */
  public final void setCableGrade(final String cableGradeParam) {
    this.cableGrade = cableGradeParam;
  }

  /**
   * @return the cableDiameter.
   */
  public final String getCableDiameter() {
    return cableDiameter;
  }

  /**
   * @param cableDiameterParam the cableDiameter to set.
   */
  public final void setCableDiameter(final String cableDiameterParam) {
    this.cableDiameter = cableDiameterParam;
  }

  /**
   * @return the equipmentLetter.
   */
  public final String getEquipmentLetter() {
    return equipmentLetter;
  }

  /**
   * @param equipmentLetterParam the equipmentLetter to set.
   */
  public final void setEquipmentLetter(final String equipmentLetterParam) {
    this.equipmentLetter = equipmentLetterParam;
  }
}
