package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;

/**
 * @author syalavarthi.
 *
 */
public class IhsAssetClientDto {

  /**
   * IhsAssetDetailsDto.
   */
  private IhsAssetDetailsDto ihsAssetDetailsDto;
  /**
   * list of customers.
   */
  private List<IhsCustomersDto> customers;

  /**
   * to get IhsAssetDetailsDto.
   *
   * @return ihsAssetDetailsDto.
   */
  public final IhsAssetDetailsDto getIhsAssetDetailsDto() {
    return ihsAssetDetailsDto;
  }

  /**
   * to set IhsAssetDetailsDto.
   *
   * @param ihsAssetDetailsDtoObj IhsAssetDetailsDto.
   */
  public final void setIhsAssetDetailsDto(final IhsAssetDetailsDto ihsAssetDetailsDtoObj) {
    this.ihsAssetDetailsDto = ihsAssetDetailsDtoObj;
  }

  /**
   * to get customers.
   *
   * @return customers.
   */
  public final List<IhsCustomersDto> getCustomers() {
    return customers;
  }

  /**
   * to set customers.
   *
   * @param customersObj customers.
   */
  public final void setCustomers(final List<IhsCustomersDto> customersObj) {
    this.customers = customersObj;
  }
}
