package com.baesystems.ai.lr.cd.be.domain.dto.security;

/**
 * Masquerading user dto.
 *
 * @author yng
 *
 */
public class MasqueradeUserDto {

  /**
   * User Id.
   */
  private String userId;

  /**
   * Getter for user id.
   *
   * @return user id.
   */
  public final String getUserId() {
    return userId;
  }

  /**
   * Setter for user id.
   *
   * @param userIdArg user id.
   */
  public final void setUserId(final String userIdArg) {
    this.userId = userIdArg;
  }
}
