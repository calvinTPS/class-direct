package com.baesystems.ai.lr.cd.be.domain.dto.task;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.references.AllowedWorkItemAttributeValueDto;

import lombok.Getter;
import lombok.Setter;

/**
 * Extended Allowed WorkItem Attribute Value DTO.
 *
 * @author syalavarthi.
 *
 */
public class AllowedWorkItemAttributeValueHDto extends AllowedWorkItemAttributeValueDto {

  /**
   * The static serialVersionUID.
   */
  private static final long serialVersionUID = 1L;
  /**
   * Linked to {@link AllowedValueGroupsHDto}.
   *
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "allowedValueGroup")
  private AllowedValueGroupsHDto allowedValueGroupH;

}
