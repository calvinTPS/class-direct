package com.baesystems.ai.lr.cd.be.domain.dto.asset;

/**
 * @author syalavarthi.
 *
 */
public class FaultGroupDto {
  /**
   * fault group id.
   */
  private Long faultGroupId;
  /**
   * fault group name.
   */
  private String faultGroupName;

  /**
   * Getter for faultGroupId.
   *
   * @return faultGroupId.
   */
  public final Long getFaultGroupId() {
    return faultGroupId;
  }

  /**
   * Setter for faultGroupId.
   *
   * @param faultGroupIdObj value.
   */
  public final void setFaultGroupId(final Long faultGroupIdObj) {
    this.faultGroupId = faultGroupIdObj;
  }

  /**
   * Getter for faultGroupName.
   *
   * @return faultGroupName.
   */
  public final String getFaultGroupName() {
    return faultGroupName;
  }

  /**
   * Setter for faultGroupName.
   *
   * @param faultGroupNameObj value.
   */
  public final void setFaultGroupName(final String faultGroupNameObj) {
    this.faultGroupName = faultGroupNameObj;
  }

}
