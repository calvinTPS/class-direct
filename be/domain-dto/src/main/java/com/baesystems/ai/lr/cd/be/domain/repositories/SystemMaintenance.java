package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

/**
 * Entity for system maintenance.
 *
 * @author Faizal Sidek
 */
@Entity
@Table(name = "SYSTEM_MAINTENANCE")
public class SystemMaintenance implements Serializable {

  /**
   * Serial uuid.
   */
  private static final long serialVersionUID = -3880824065370302210L;

  /**
   * Logical id.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "MAINTENANCE_ID", nullable = false)
  private Integer id;

  /**
   * Is maintenance active flag.
   */
  @Column(name = "IS_ACTIVE")
  private boolean active;

  /**
   * Maintenance message.
   */
  @Column(name = "MESSAGE")
  private String message;

  /**
   * Maintenance start window.
   */
  @Column(name = "START_DATE")
  @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
  private LocalDateTime startDate;

  /**
   * Maintenance end window.
   */
  @Column(name = "END_DATE")
  @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
  private LocalDateTime endDate;

  /**
   * Getter for {@link #id}.
   *
   * @return value of id.
   */
  public final Integer getId() {
    return id;
  }

  /**
   * Setter for {@link #id}.
   *
   * @param maintenanceId value to be set.
   */
  public final void setId(final Integer maintenanceId) {
    this.id = maintenanceId;
  }

  /**
   * Getter for {@link #active}.
   *
   * @return value of active.
   */
  public final boolean isActive() {
    return active;
  }

  /**
   * Setter for {@link #active}.
   *
   * @param activeFlag value to be set.
   */
  public final void setActive(final boolean activeFlag) {
    this.active = activeFlag;
  }

  /**
   * Getter for {@link #message}.
   *
   * @return value of message.
   */
  public final String getMessage() {
    return message;
  }

  /**
   * Setter for {@link #message}.
   *
   * @param msg value to be set.
   */
  public final void setMessage(final String msg) {
    this.message = msg;
  }

  /**
   * Getter for {@link #startDate}.
   *
   * @return value of startDate.
   */
  public final LocalDateTime getStartDate() {
    return startDate;
  }

  /**
   * Setter for {@link #startDate}.
   *
   * @param start value to be set.
   */
  public final void setStartDate(final LocalDateTime start) {
    this.startDate = start;
  }

  /**
   * Getter for {@link #endDate}.
   *
   * @return value of endDate.
   */
  public final LocalDateTime getEndDate() {
    return endDate;
  }

  /**
   * Setter for {@link #endDate}.
   *
   * @param end value to be set.
   */
  public final void setEndDate(final LocalDateTime end) {
    this.endDate = end;
  }
}
