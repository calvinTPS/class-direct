package com.baesystems.ai.lr.cd.be.domain.dto.export;

/**
 * Report export configuration.
 *
 * @author yng
 *
 */
public class ReportExportDto {

  /**
   * report id.
   */
  private long reportId;

  /**
   * job id.
   */
  private long jobId;

  /**
   * Getter for job id.
   *
   * @return job id.
   */
  public final long getJobId() {
    return jobId;
  }

  /**
   * Setter for job id.
   *
   * @param jobIdArg job id.
   */
  public final void setJobId(final long jobIdArg) {
    this.jobId = jobIdArg;
  }

  /**
   * Getter for report id.
   *
   * @return report id.
   */
  public final long getReportId() {
    return reportId;
  }

  /**
   * Setter for report id.
   *
   * @param reportIdArg report id.
   */
  public final void setReportId(final long reportIdArg) {
    this.reportId = reportIdArg;
  }


}
