package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;

import lombok.Getter;
import lombok.Setter;

/**
 * The ReportHDto extending ReportDto.
 *
 * @author syalavarthi.
 *
 */
public class ReportHDto extends ReportDto {

  /**
   * The default serial version id.
   */
  private static final long serialVersionUID = 1L;
  /**
   * Linked to {@link ReportLightHDto}.
   */
  @LinkedResource(referencedField = "reportType")
  @Getter
  @Setter
  private ReportTypeHDto reportTypeDto;

  /**
   * Dto for {@link #job}.
   */
  @Getter
  @Setter
  private JobCDDto jobH;

  /**
   * Attachments associated with this report.
   */
  @Getter
  @Setter
  private List<SupplementaryInformationHDto> attachments;
}
