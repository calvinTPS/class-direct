/**
 * This package contains ClassDirect-specific DTO.
 *
 * @author Faizal Sidek
 */
package com.baesystems.ai.lr.cd.be.domain.dto.survey;
