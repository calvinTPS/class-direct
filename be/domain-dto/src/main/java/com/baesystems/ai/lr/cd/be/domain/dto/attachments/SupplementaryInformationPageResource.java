package com.baesystems.ai.lr.cd.be.domain.dto.attachments;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * @author syalavarthi
 *
 */
public class SupplementaryInformationPageResource extends BasePageResource<SupplementaryInformationHDto> {

  /**
   * List of SupplementaryInformation.
   *
   */
  private List<SupplementaryInformationHDto> content;

  /**
   * Getter for {@link #content}.
   * @return value.
   */
  @Override
  public final List<SupplementaryInformationHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   * @param contentObj content.
   */
  @Override
  public final void setContent(final List<SupplementaryInformationHDto> contentObj) {
    this.content = contentObj;
  }



}
