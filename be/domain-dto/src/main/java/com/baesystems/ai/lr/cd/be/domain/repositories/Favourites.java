package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author VMandalapu
 *
 */
@Entity
@Table(name = "FAVOURITES", uniqueConstraints = {@UniqueConstraint(columnNames = {"USER_ID", "ASSET_ID"})})
public class Favourites implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -336146411431652817L;

  /**
   * Id of the entity.
   *
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "FAVOURITES_ID", nullable = false)
  private Long id;

  /**
  *
  */
  @Column(name = "ASSET_ID", nullable = false)
  private Long assetId;

  /**
  *
  */
  @Column(name = "SOURCE", nullable = true)
  private String source;

  /**
   *
   */
  @ManyToOne
  @JoinColumn(name = "USER_ID", nullable = false)
  private UserProfiles user;

  /**
   * Getter for {@link #id}.
   *
   * @return id
   *
   */
  public final Long getId() {
    return id;
  }

  /**
   * Setter for {@link #id}.
   *
   * @param idToBeSet id to be set.
   *
   */
  public final void setId(final Long idToBeSet) {
    this.id = idToBeSet;
  }

  /**
   * @return value.
   */
  public final Long getAssetId() {
    return assetId;
  }

  /**
   * @param assetIdObject value.
   */
  public final void setAssetId(final Long assetIdObject) {
    this.assetId = assetIdObject;
  }

  /**
   * @return the source
   */
  public final String getSource() {
    return source;
  }

  /**
   * @param sourceObject to set
   */
  public final void setSource(final String sourceObject) {
    this.source = sourceObject;
  }

  /**
   * @return value.
   */
  public final UserProfiles getUser() {
    return user;
  }

  /**
   * @param userObject value.
   */
  public final void setUser(final UserProfiles userObject) {
    this.user = userObject;
  }
}
