package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;

/**
 * @author sbollu
 *
 */
public class ProductCatalogueHDto extends ProductCatalogueDto {

  /**
   *
   */
  private static final long serialVersionUID = 645315724581974407L;
}
