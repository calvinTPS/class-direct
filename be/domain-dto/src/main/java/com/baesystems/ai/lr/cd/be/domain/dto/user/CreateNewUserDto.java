package com.baesystems.ai.lr.cd.be.domain.dto.user;

import java.time.LocalDate;
import java.util.List;

/**
 * @author sbollu
 *
 */
public class CreateNewUserDto {

  /**
   * new user id.
   */
  private String userId;

  /**
   * new user email.
   */
  private String email;
  /**
   * new user role.
   */
  private String role;
  /**
   * new user flagCode.
   */
  private String flagCode;
  /**
   * new user clientCode.
   */
  private String clientCode;
  /**
   * new user shipBuilderCode.
   */
  private String shipBuilderCode;
  /**
   * new user accountExpiry.
   */
  private LocalDate accountExpiry;
  /**
   * new user assigned eor's.
   */
  private List<UserEORDto> eor;
  /**
   * CustomerType.
   */
  private String clientType;

  /**
   * @return the userId.
   */
  public final String getUserId() {
    return userId;
  }

  /**
   * @param userIdObject the userId to set.
   */
  public final void setUserId(final String userIdObject) {
    this.userId = userIdObject;
  }

  /**
   * @return the email.
   */
  public final String getEmail() {
    return email;
  }

  /**
   * @param emailObject to set.
   */
  public final void setEmail(final String emailObject) {
    this.email = emailObject;
  }

  /**
   * @return the role.
   */
  public final String getRole() {
    return role;
  }

  /**
   * @param roleObject to set.
   */
  public final void setRole(final String roleObject) {
    this.role = roleObject;
  }

  /**
   * @return the flagCode.
   */
  public final String getFlagCode() {
    return flagCode;
  }

  /**
   * @param flagCodeObject to set.
   */
  public final void setFlagCode(final String flagCodeObject) {
    this.flagCode = flagCodeObject;
  }

  /**
   * @return the clientCode.
   */
  public final String getClientCode() {
    return clientCode;
  }

  /**
   * @param clientCodeObject to set.
   */
  public final void setClientCode(final String clientCodeObject) {
    this.clientCode = clientCodeObject;
  }

  /**
   * @return the shipBuilderCode.
   */
  public final String getShipBuilderCode() {
    return shipBuilderCode;
  }

  /**
   * @param shipBuilderCodeObject to set.
   */
  public final void setShipBuilderCode(final String shipBuilderCodeObject) {
    this.shipBuilderCode = shipBuilderCodeObject;
  }

  /**
   * @return the accountExpiry.
   */
  public final LocalDate getAccountExpiry() {
    return accountExpiry;
  }

  /**
   * @param accountExpiryObject to set.
   */
  public final void setAccountExpiry(final LocalDate accountExpiryObject) {
    this.accountExpiry = accountExpiryObject;
  }

  /**
   * @return the eor.
   */
  public final List<UserEORDto> getEor() {
    return eor;
  }

  /**
   * @param eorObject to set.
   */
  public final void setEor(final List<UserEORDto> eorObject) {
    this.eor = eorObject;
  }

  /**
   * Getter for clientType.
   *
   * @return clientType.
   */
  public final String getClientType() {
    return clientType;
  }

  /**
   * Setter for clientType.
   *
   * @param clientTypeObject clientType.
   */
  public final void setCustomerType(final String clientTypeObject) {
    this.clientType = clientTypeObject;
  }

}
