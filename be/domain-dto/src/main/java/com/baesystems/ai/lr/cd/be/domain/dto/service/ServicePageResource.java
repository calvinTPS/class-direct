package com.baesystems.ai.lr.cd.be.domain.dto.service;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * @author VKolagutla
 *
 */
public class ServicePageResource extends BasePageResource<ScheduledServiceHDto> {

  /**
   * List of services.
   */
  private List<ScheduledServiceHDto> content;

  /**
   * @return the content
   */
  @Override
  public final List<ScheduledServiceHDto> getContent() {
    return content;
  }

  /**
   * @param contentObj the content to set
   */
  @Override
  public final void setContent(final List<ScheduledServiceHDto> contentObj) {
    this.content = contentObj;
  }


}
