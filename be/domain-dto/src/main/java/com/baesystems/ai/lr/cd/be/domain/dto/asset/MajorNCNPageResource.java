package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;
import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * Page resource for {@link MajorNCNHDto}.
 *
 * @author syalavarthi.
 *
 */
public class MajorNCNPageResource extends BasePageResource<MajorNCNHDto> implements Serializable {

  /**
   * Auto generated serial ID.
   */
  private static final long serialVersionUID = -3691599102967374529L;

  /**
   * List of major non confirmitive notes.
   *
   */
  private List<MajorNCNHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<MajorNCNHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<MajorNCNHDto> mncnsList) {
    this.content = mncnsList;
  }
}
