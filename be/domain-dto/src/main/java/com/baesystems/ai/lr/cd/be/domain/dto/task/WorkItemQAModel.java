package com.baesystems.ai.lr.cd.be.domain.dto.task;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides question and answer model for work item.
 *
 * @author syalavarthi.
 *
 */
public class WorkItemQAModel {
  /**
   * The question.
   */
  @Getter
  @Setter
  private String question;
  /**
   * The answer.
   */
  @Getter
  @Setter
  private String answer;

}
