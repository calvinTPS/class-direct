package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.List;

import com.baesystems.ai.lr.dto.tasks.WorkItemModelItemDto;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides methods for hydrated list of tasks and items.
 *
 * @author sbollu
 *
 */
public class WorkItemModelItemHDto extends WorkItemModelItemDto {

  /**
   * Generated serialVersionUID.
   */
  private static final long serialVersionUID = 8795656676510601788L;

  /**
   * Hydrated tasks.
   */
  @Getter
  @Setter
  private List<WorkItemLightHDto> tasksH;

  /**
   * Next level hydrated items.
   */
  @Getter
  @Setter
  private List<WorkItemModelItemHDto> itemsH;

}
