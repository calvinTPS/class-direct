package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * @author RKaneysan.
 */
public class AssetDetailsDto implements Serializable {

  /**
   * Default serial version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Registry Information Dto.
   */
  private RegistryInformationDto registryInformation;

  /**
   * Principal Dimension Dto.
   */
  private PrincipalDimensionDto principalDimension;

  /**
   * Ruleset Details Dto.
   */
  private RulesetDetailsDto rulesetDetailsDto;

  /**
   * Equipment Details Dto.
   */
  private EquipmentDetailsDto equipmentDetails;

  /**
   * Ownership Dto.
   */
  private OwnershipDto ownership;

  /**
   * @return the registryInformation.
   */
  public final RegistryInformationDto getRegistryInformation() {
    return registryInformation;
  }

  /**
   * @param registryInformationParam the registryInformation to set.
   */
  public final void setRegistryInformation(final RegistryInformationDto registryInformationParam) {
    this.registryInformation = registryInformationParam;
  }

  /**
   * @return the principalDimension.
   */
  public final PrincipalDimensionDto getPrincipalDimension() {
    return principalDimension;
  }

  /**
   * @param newPrincipalDimension the principalDimension to set.
   */
  public final void setPrincipalDimension(final PrincipalDimensionDto newPrincipalDimension) {
    this.principalDimension = newPrincipalDimension;
  }

  /**
   *
   * @return the rulesetDetailsDto.
   */
  public final RulesetDetailsDto getRulesetDetailsDto() {
    return rulesetDetailsDto;
  }

  /**
   *
   * @param rulesetDetailsDtoParam the rulesetDetailsDto to set.
   */
  public final void setRulesetDetailsDto(final RulesetDetailsDto rulesetDetailsDtoParam) {
    this.rulesetDetailsDto = rulesetDetailsDtoParam;
  }

  /**
   * @return the equipmentDetails.
   */
  public final EquipmentDetailsDto getEquipmentDetails() {
    return equipmentDetails;
  }

  /**
   * @param equipmentDetailsParam the equipmentDetails to set.
   */
  public final void setEquipmentDetails(final EquipmentDetailsDto equipmentDetailsParam) {
    this.equipmentDetails = equipmentDetailsParam;
  }

  /**
   * @return the ownership.
   */
  public final OwnershipDto getOwnership() {
    return ownership;
  }

  /**
   * @param ownershipParam the ownership to set.
   */
  public final void setOwnership(final OwnershipDto ownershipParam) {
    this.ownership = ownershipParam;
  }

}
