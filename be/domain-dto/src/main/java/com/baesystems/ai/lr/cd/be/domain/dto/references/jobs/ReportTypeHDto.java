package com.baesystems.ai.lr.cd.be.domain.dto.references.jobs;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * Report Type HDTO.
 *
 * @author yng
 *
 */
public class ReportTypeHDto extends ReferenceDataDto {

  /**
   * Generated UID.
   */
  private static final long serialVersionUID = -7211620299574901135L;

}
