/**
 * Base package for asset reference HDto.
 *
 */
package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;
