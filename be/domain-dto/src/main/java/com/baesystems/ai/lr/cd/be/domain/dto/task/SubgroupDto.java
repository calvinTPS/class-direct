package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sbollu
 *
 */
public class SubgroupDto extends ReferenceDataDto {


  /**
   * The unique generated serial version UID.
   */
  private static final long serialVersionUID = 8173800148227108327L;

  /**
   * Constructs DTO.
   */
  public SubgroupDto() {
    super();
  }

  /**
   * Constructs DTO with check list subgroup {@link ChecklistSubgroupDto}.
   *
   * @param subgroup the checklist subgroup.
   */
  public SubgroupDto(final ChecklistSubgroupDto subgroup) {
    this();
    this.setId(subgroup.getId());
    this.setName(subgroup.getName());
    this.setChecklistItems(new ArrayList<>());
  }

  /**
   * The list of checklist items {@link WorkItemLightHDto}.
   */
  @Getter
  @Setter
  private List<WorkItemLightHDto> checklistItems;

}
