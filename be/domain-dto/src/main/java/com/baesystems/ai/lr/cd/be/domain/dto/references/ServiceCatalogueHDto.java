package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author VMandalapu
 *
 */
public class ServiceCatalogueHDto extends ServiceCatalogueDto {

  /**
   *
   */
  private static final long serialVersionUID = -2759973806766021839L;

  /**
   * LinkedResource for ServiceGroupDto.
   */
  @LinkedResource(referencedField = "serviceGroup")
  private ServiceGroupHDto serviceGroupH;
  /**
   * The product group of service catalogue.
   */
  @Getter
  @Setter
  private ProductGroupDto productGroupH;

  /**
   * Getter for ServiceGroupH.
   *
   * @return dto
   */
  public final ServiceGroupHDto getServiceGroupH() {
    return serviceGroupH;
  }

  /**
   * Set for ServiceGroupH.
   *
   * @param dto value
   */
  public final void setServiceGroupH(final ServiceGroupHDto dto) {
    this.serviceGroupH = dto;
  }
}
