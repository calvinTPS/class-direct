package com.baesystems.ai.lr.cd.be.domain.dto.subfleet;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * @author syalavarthi
 *
 */
public class SubFleetDto implements Serializable {

  /**
   * serialVersionUID.
   */
  private static final long serialVersionUID = 2795924405147554544L;
  /**
   * accessibleAssetIds.
   */
  @NotNull
  private List<Long> accessibleAssetIds;
  /**
   * restrictedAssetIds.
   */
  @NotNull
  private List<Long> restrictedAssetIds;

  /**
   * @return value.
   */
  public final List<Long> getAccessibleAssetIds() {
    return accessibleAssetIds;
  }

  /**
   * @param accessibleAssetIdsObject assetIds.
   */
  public final void setAccessibleAssetIds(final List<Long> accessibleAssetIdsObject) {
    this.accessibleAssetIds = accessibleAssetIdsObject;
  }

  /**
   * @return value.
   */
  public final List<Long> getRestrictedAssetIds() {
    return restrictedAssetIds;
  }

  /**
   * @param restrictedAssetIdsObject ids.
   */
  public final void setRestrictedAssetIds(final List<Long> restrictedAssetIdsObject) {
    this.restrictedAssetIds = restrictedAssetIdsObject;
  }



}
