package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairTypeHDto;

/**
 * @author syalavarthi.
 *
 */
public class FixDto {
  /**
   * defect title.
   */
  private String defectTitle;
  /**
   * defect repairType.
   */
  private List<RepairTypeHDto> repairTypes;
  /**
   * defect repairAction.
   */
  private String repairAction;
  /**
   * list of items.
   */
  private List<FaultItemDto> items;
  /**
   * defect dateAdded.
   */
  private Date dateAdded;
  /**
   * defect description.
   */
  private String description;

  /**
   * Getter for jobs list.
   *
   * @return defect status dto.
   */
  public final List<FaultItemDto> getItems() {
    return items;
  }

  /**
   * Setter for items.
   *
   * @param itemsObj value.
   */
  public final void setItems(final List<FaultItemDto> itemsObj) {
    this.items = itemsObj;
  }

  /**
   * Getter for defectTitle.
   *
   * @return defectTitle.
   */
  public final String getDefectTitle() {
    return defectTitle;
  }

  /**
   * Setter for defectTitle.
   *
   * @param defectTitleObj value.
   */
  public final void setDefectTitle(final String defectTitleObj) {
    this.defectTitle = defectTitleObj;
  }

  /**
   * Getter for repairTypes.
   *
   * @return repairTypes.
   */
  public final List<RepairTypeHDto> getRepairType() {
    return repairTypes;
  }

  /**
   * Setter for repairType.
   *
   * @param repairTypesObj value.
   */
  public final void setRepairType(final List<RepairTypeHDto> repairTypesObj) {
    this.repairTypes = repairTypesObj;
  }

  /**
   * Getter for repairAction.
   *
   * @return repairAction.
   */
  public final String getRepairAction() {
    return repairAction;
  }

  /**
   * Setter for repairAction.
   *
   * @param repairActionObj value.
   */
  public final void setRepairAction(final String repairActionObj) {
    this.repairAction = repairActionObj;
  }

  /**
   * Getter for dateAdded.
   *
   * @return dateAdded.
   */
  public final Date getDateAdded() {
    return dateAdded;
  }

  /**
   * Setter for dateAdded.
   *
   * @param dateAddedObj value.
   */
  public final void setDateAdded(final Date dateAddedObj) {
    this.dateAdded = dateAddedObj;
  }

  /**
   * Getter for description.
   *
   * @return description.
   */
  public final String getDescription() {
    return description;
  }

  /**
   * Setter for description.
   *
   * @param descriptionObj value.
   */
  public final void setDescription(final String descriptionObj) {
    this.description = descriptionObj;
  }

}
