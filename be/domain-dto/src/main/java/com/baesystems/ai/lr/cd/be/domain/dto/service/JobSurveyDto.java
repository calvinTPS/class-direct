package com.baesystems.ai.lr.cd.be.domain.dto.service;

import lombok.Getter;
import lombok.Setter;

/**
 * @author SBollu
 *
 */
public class JobSurveyDto {

  /**
   * id.
   */
  @Getter
  @Setter
  private Long id;
  /**
   * The service name.
   */
  @Getter
  @Setter
  private String name;
  /**
   * The service status.
   */
  @Getter
  @Setter
  private String status;
  /**
   * The MMS service.
   */
  @Getter
  @Setter
  private Boolean isMMS;

}
