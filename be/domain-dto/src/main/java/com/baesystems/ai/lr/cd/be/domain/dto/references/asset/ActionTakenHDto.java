package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * @author syalavarthi.
 *
 */
public class ActionTakenHDto extends ReferenceDataDto {

  /**
   * static serialVersionUID.
   */
  private static final long serialVersionUID = -3648527091429760891L;

}
