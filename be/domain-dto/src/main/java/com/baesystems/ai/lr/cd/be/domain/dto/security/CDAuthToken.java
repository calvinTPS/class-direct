package com.baesystems.ai.lr.cd.be.domain.dto.security;

import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Authentication token for class direct.
 * This class have token in form of AES cookie, B2C claims, user id.
 *
 * @author Faizal Sidek
 */
public class CDAuthToken implements Authentication {

  /**
   * serialVersionID.
   */
  private static final long serialVersionUID = -848497892439268606L;

  /**
   * Map of claims.
   */
  private Map<String, String> claims = new HashMap<>();

  /**
   * Encrypted AES cookie value.
   */
  private String aesCookie;

  /**
   * User id with type of {@link String}.
   */
  private String principal;

  /**
   * Details about principal.
   */
  private UserProfiles details;

  /**
   * List of authorities user can perform.
   */
  private List<SimpleGrantedAuthority> authorities;

  /**
   * Flag to represent user logged in status.
   */
  private boolean authenticated;

  /**
   * Accessible asset id for equasis thetis.
   */
  private Long equasisThetisAccessibleAssetId;

  /**
   * Initialise token with map of claims and user id passed down by Azure B2C.
   *
   * @param b2cClaims map of claims.
   * @param userId    id of user.
   */
  public CDAuthToken(final String userId, final Map<String, String> b2cClaims) {
    this.claims = b2cClaims;
    this.principal = userId;
    this.authorities = Collections.unmodifiableList(Collections.emptyList());
  }

  /**
   * Initialise token with AES cookie value.
   *
   * @param aesCookieValue value of AES cookie.
   */
  public CDAuthToken(final String aesCookieValue) {
    this.aesCookie = aesCookieValue;
  }

  /**
   * Initialise token with list of authorities.
   *
   * @param grantedAuthorities list of authorities.
   */
  public CDAuthToken(final List<SimpleGrantedAuthority> grantedAuthorities) {
    this.authorities = Collections.unmodifiableList(grantedAuthorities);
  }

  /**
   * Getter for {@link #authenticated}.
   *
   * @return value of authenticated.
   */
  public final boolean isAuthenticated() {
    return authenticated;
  }

  /**
   * Setter for {@link #authenticated}.
   *
   * @param flag value to be set.
   */
  public final void setAuthenticated(final boolean flag) {
    this.authenticated = flag;
  }

  /**
   * Returns user id of the user.
   *
   * @return id of logged in user.
   */
  public final String getPrincipal() {
    return principal;
  }

  /**
   * Getter for {@link #details}.
   *
   * @return value of details.
   */
  @Override
  public final UserProfiles getDetails() {
    return details;
  }

  @Override
  public final String getName() {
    String name = "";
    if (null != details) {
      name = details.getUserId();
    } else {
      name = principal;
    }

    return name;
  }

  /**
   * Getter for {@link #authorities}.
   *
   * @return value of authorities.
   */
  @Override
  public final List<SimpleGrantedAuthority> getAuthorities() {
    return authorities;
  }

  /**
   * Map of credentials.
   *
   * @return credentials.
   */
  @Override
  public final Map<String, String> getCredentials() {
    return claims;
  }

  /**
   * Getter for {@link #aesCookie}.
   *
   * @return value of aesCookie.
   */
  public final String getAesCookie() {
    return aesCookie;
  }

  /**
   * Setter for {@link #aesCookie}.
   *
   * @param cookie value to be set.
   */
  public final void setAesCookie(final String cookie) {
    this.aesCookie = cookie;
  }

  /**
   * Setter for {@link #claims}.
   *
   * @param claimMap value to be set.
   */
  public final void setClaims(final Map<String, String> claimMap) {
    this.claims = claimMap;
  }

  /**
   * Getter for {@link #claims}.
   *
   * @return value of claims.
   */
  public final Map<String, String> getClaims() {
    return claims;
  }

  /**
   * Setter for {@link #details}.
   *
   * @param userProfiles value to be set.
   */
  public final void setDetails(final UserProfiles userProfiles) {
    this.details = userProfiles;
  }

  /**
   * Setter for {@link #principal}.
   *
   * @param userId value to be set.
   */
  public final void setPrincipal(final String userId) {
    this.principal = userId;
  }

  /**
   * Getter for {@link #equasisThetisAccessibleAssetId}.
   *
   * @return value of equasisThetisAccessibleAssetId.
   */
  public final Long getEquasisThetisAccessibleAssetId() {
    return equasisThetisAccessibleAssetId;
  }

  /**
   * Setter for {@link #equasisThetisAccessibleAssetId}.
   *
   * @param assetId value to be set.
   */
  public final void setEquasisThetisAccessibleAssetId(final Long assetId) {
    this.equasisThetisAccessibleAssetId = assetId;
  }

  @Override
  public final String toString() {
    return "CDAuthToken{" + "claims=" + claims + ", aesCookie='" + aesCookie + '\'' + ", principal='" + principal + '\''
      + ", details=" + details + ", authorities=" + authorities + ", authenticated=" + authenticated + '}';
  }
}
