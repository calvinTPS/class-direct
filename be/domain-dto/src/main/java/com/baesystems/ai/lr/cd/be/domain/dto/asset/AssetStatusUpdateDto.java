package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;
import java.util.List;

/**
 * @author RKaneysan
 *
 */
public class AssetStatusUpdateDto implements Serializable {

  /**
   * Default Serial Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Asset Header Dto.
   */
  private AssetHDto asset;

  /**
   * Due Item List.
   */
  private List<DueItemDto> dueItemList;

  /**
   * @return the asset
   */
  public final AssetHDto getAsset() {
    return asset;
  }

  /**
   * @param assetParam the asset to set
   */
  public final void setAsset(final AssetHDto assetParam) {
    this.asset = assetParam;
  }

  /**
   * @return the dueItemList.
   */
  public final List<DueItemDto> getDueItemList() {
    return dueItemList;
  }

  /**
   * @param dueItemListParam the dueItemList to set.
   */
  public final void setDueItemList(final List<DueItemDto> dueItemListParam) {
    this.dueItemList = dueItemListParam;
  }

}
