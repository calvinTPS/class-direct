package com.baesystems.ai.lr.cd.be.utils;

import java.io.IOException;
import java.util.Optional;

import com.baesystems.ai.lr.cd.be.exception.MastAPIException;


/**
 * @author SBollu
 *
 */
public final class ExceptionUtil {

  /**
   * Privatized constructor.
   */
  private ExceptionUtil() {

  }

  /**
   * @param ioe ioe.
   * @return ClassDirectException.
   */
  public static MastAPIException toMastException(final IOException ioe) {
    return Optional.ofNullable(ioe).filter(ex -> ex instanceof MastAPIException).map(ex -> (MastAPIException) ex)
        .orElse(null);
  }
}
