package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.io.Serializable;
import java.util.List;

/**
 * Page resource for {@link CertificateHDto}.
 */
public class CertificatePageResource extends BasePageResource<CertificateHDto> implements Serializable {

  /**
   * Serial version ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of assets.
   */
  private List<CertificateHDto> content;

  /**
   * Getter for {@link #content}.
   */
  @Override
  public final List<CertificateHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   */
  @Override
  public final void setContent(final List<CertificateHDto> assetList) {
    this.content = assetList;
  }

}
