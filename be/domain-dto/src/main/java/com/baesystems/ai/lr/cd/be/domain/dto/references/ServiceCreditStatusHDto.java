package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.dto.references.ServiceCreditStatusDto;

/**
 * @author VMandalapu
 *
 */
public class ServiceCreditStatusHDto extends ServiceCreditStatusDto {

  /**
   * serial version.
   *
   */
  private static final long serialVersionUID = 4471060455949849229L;

}
