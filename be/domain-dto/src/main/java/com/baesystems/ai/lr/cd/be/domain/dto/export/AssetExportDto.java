package com.baesystems.ai.lr.cd.be.domain.dto.export;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;

import lombok.Getter;
import lombok.Setter;

/**
 * Asset export configuration.
 *
 * @author yng
 *
 */
public class AssetExportDto {

  /**
   * List of included asset codes.
   */
  private List<String> includes;

  /**
   * List of excluded asset codes.
   */
  private List<String> excludes;

  /**
   * Asset query object.
   */
  private AssetQueryHDto filter;

  /**
   * Information printing option.
   */
  private Long info;

  /**
   * Service printing option.
   */
  private Long service;

  /**
   * Codicils printing option.
   */
  private List<Long> codicils;

  /**
   * Query parameter to differ export.
   */
  @Setter
  @Getter
  private String fileType;

  /**
   * Getter for query.
   *
   * @return filter value.
   */
  public final AssetQueryHDto getFilter() {
    return filter;
  }

  /**
   * Setter for query.
   *
   * @param filterObj value.
   */
  public final void setAssetQuery(final AssetQueryHDto filterObj) {
    this.filter = filterObj;
  }

  /**
   * Getter for includes.
   *
   * @return includes id.
   */
  public final List<String> getIncludes() {
    return includes;
  }

  /**
   * Setter for includes.
   *
   * @param includesObj id.
   */
  public final void setIncludes(final List<String> includesObj) {
    this.includes = includesObj;
  }

  /**
   * Getter for excludes.
   *
   * @return excludes id.
   */
  public final List<String> getExcludes() {
    return excludes;
  }

  /**
   * Setter for excludes.
   *
   * @param excludesObj id.
   */
  public final void setExcludes(final List<String> excludesObj) {
    this.excludes = excludesObj;
  }

  /**
   * Getter for info option.
   *
   * @return info option enum value.
   */
  public final Long getInfo() {
    return info;
  }

  /**
   * Setter for info option.
   *
   * @param infoValue option enum value.
   */
  public final void setInfo(final Long infoValue) {
    this.info = infoValue;
  }

  /**
   * Getter for service option.
   *
   * @return service option num value.
   */
  public final Long getService() {
    return service;
  }

  /**
   * Setter for service option.
   *
   * @param serviceValue option num value.
   */
  public final void setService(final Long serviceValue) {
    this.service = serviceValue;
  }

  /**
   * Getter for list of codicils.
   *
   * @return list of selected codicil enum value.
   */
  public final List<Long> getCodicils() {
    return codicils;
  }

  /**
   * Setter for list of codicils.
   *
   * @param codicilsObject list of selected codicil enum value.
   */
  public final void setCodicils(final List<Long> codicilsObject) {
    this.codicils = codicilsObject;
  }


}
