package com.baesystems.ai.lr.cd.be.enums;

/**
 * Enum for asset service export.
 *
 * @author yng
 *
 */
public enum AssetServiceExportEnum {
  /**
   * No service information.
   */
  NO_SERVICE_INFO(1L),

  /**
   * Basic service information.
   */
  BASIC_SERVICE_INFO(2L),

  /**
   * Full service information.
   */
  FULL_SERVICE_INFO(3L);

  /**
   * enum id.
   */
  private Long id;

  /**
   * Constructor.
   *
   * @param value id.
   */
  AssetServiceExportEnum(final Long value) {
    this.id = value;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return value of id.
   */
  public final Long getId() {
    return id;
  }
}
