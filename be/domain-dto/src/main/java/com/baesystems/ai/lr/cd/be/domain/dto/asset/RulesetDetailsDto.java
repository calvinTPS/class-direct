package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * @author RKaneysan
 */
public class RulesetDetailsDto implements Serializable {

  /**
   * Default Serial Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Machinery Notation.
   */
  private String machineryNotation;

  /**
   * Descriptive Note.
   */
  private String descriptiveNote;

  /**
   * Hull Indicator.
   */
  private Integer hullIndicator;

  /**
   * @return the machineryNotation
   */
  public final String getMachineryNotation() {
    return machineryNotation;
  }

  /**
   * @param machineryNotationParam the machineryNotation to set
   */
  public final void setMachineryNotation(final String machineryNotationParam) {
    this.machineryNotation = machineryNotationParam;
  }

  /**
   * @return the descriptiveNote
   */
  public final String getDescriptiveNote() {
    return descriptiveNote;
  }

  /**
   * @param descriptiveNoteParam the descriptiveNote to set
   */
  public final void setDescriptiveNote(final String descriptiveNoteParam) {
    this.descriptiveNote = descriptiveNoteParam;
  }

  /**
   * @return the hullIndicator.
   */
  public final Integer getHullIndicator() {
    return hullIndicator;
  }

  /**
   * @param hullIndicatorParam the hullIndicator to set.
   */
  public final void setHullIndicator(final Integer hullIndicatorParam) {
    this.hullIndicator = hullIndicatorParam;
  }
}
