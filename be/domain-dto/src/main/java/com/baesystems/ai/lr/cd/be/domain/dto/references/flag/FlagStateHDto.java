package com.baesystems.ai.lr.cd.be.domain.dto.references.flag;

import com.baesystems.ai.lr.dto.references.FlagStateDto;

/**
 * Hydrated dto for {@link FlagStateDto}.
 *
 * @author msidek
 *
 */
public class FlagStateHDto extends FlagStateDto {
  /**
   * Generated UID.
   *
   */
  private static final long serialVersionUID = -3278639501290242174L;

  // TODO link resource to Ports.
}
