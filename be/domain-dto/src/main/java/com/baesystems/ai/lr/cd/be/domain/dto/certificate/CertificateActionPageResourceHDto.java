package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

import java.util.List;

/**
 * Created by fwijaya on 24/1/2017.
 */
public class CertificateActionPageResourceHDto extends BasePageResource<CertificateActionHDto> {

  /**
   * Content.
   */
  private List<CertificateActionHDto> content;

  /**
   * Getter.
   *
   * @return List of {@link CertificateActionHDto}.
   */
  @Override
  public final List<CertificateActionHDto> getContent() {
    return this.content;
  }

  /**
   * Setter.
   *
   * @param contentArg object.
   */
  @Override
  public final void setContent(final List<CertificateActionHDto> contentArg) {
    this.content = contentArg;
  }
}
