package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.dto.references.ProductTypeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemModelItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPreviewScheduledServiceDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sbollu
 *
 */
public class WorkItemPreviewScheduledServiceHDto extends WorkItemPreviewScheduledServiceDto<WorkItemModelItemDto> {
  /**
   * Generated serial version UID.
   */
  private static final long serialVersionUID = 1273455370045391533L;

  /**
   * Hydrated Service Catalogue.
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "serviceCatalogue")
  private ServiceCatalogueHDto serviceCatalogueH;

  /**
   * Hydrated product Type.
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "productType")
  private ProductTypeDto productTypeH;

  /***
   * Hydrated Items.
   */
  @Getter
  @Setter
  private List<WorkItemModelItemHDto> itemsH;
}
