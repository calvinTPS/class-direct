package com.baesystems.ai.lr.cd.be.enums;
/**
 * Enumeration for Product family(type).
 *
 * @author Shajahan
 */
public enum ProductTypeEnum {
     /**
      * Classification product.
      */
     CLASSIFICATION("Classification", 1L),
     /**
      * MMS Product.
      */
     MMS("MMS", 2L),
     /**
      * Statutory product.
      */
     STATUTORY("Statutory", 3L);

     /**
      * Name of Product type.
      */
     private String name;

     /**
      * Product type id.
      */
     private Long id;

     /**
      * Default constructor.
      *
      * @param productTypeName name of product type.
      * @param value id of object.
      */
     private ProductTypeEnum(final String productTypeName, final Long value) {
       this.name = productTypeName;
       this.id = value;
     }

     /**
      * Getter for {@link #id}.
      *
      * @return value of id.
      */
     public final Long getId() {
       return id;
     }

     /**
      * Getter for {@link #name}.
      *
      * @return name of status.
      */
     public final String getName() {
       return name;
     }

     /**
      * To get the ProductTypeEnum object by id.
      *
      * @param id product type id.
      * @return productEnum object.
      */
     public static ProductTypeEnum getById(final Long id) {
        for (ProductTypeEnum productEnum: values()) {
           if (productEnum.id.equals(id)) {
              return productEnum;
           }
        }
        return null;
     }
   }
