package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.LocationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import lombok.Getter;
import lombok.Setter;

/**
 * JobHDto extending JobDto.
 *
 * @author yng
 *
 */
public class JobHDto extends JobDto {
  /**
   * Linked to {@link LocationHDto}.
   */
  @LinkedResource(referencedField = "location")
  @Getter
  @Setter
  private LocationHDto locationDto;
  /**
   * Linked to {@link JobHDto}.
   */
  @LinkedResource(referencedField = "jobStatus")
  private JobStatusHDto jobStatusDto;

  /**
   * Setter for {@link #jobStatusDto}.
   *
   * @return job status dto.
   */
  public final JobStatusHDto getJobStatusDto() {
    return jobStatusDto;
  }

  /**
   * Getter for {@link #jobStatusDto}.
   *
   * @param dto to be set.
   */
  public final void setJobStatusDto(final JobStatusHDto dto) {
    this.jobStatusDto = dto;
  }
}
