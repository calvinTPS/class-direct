package com.baesystems.ai.lr.cd.be.domain.dto.service;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.dto.services.SurveyWithTaskListDto;

import lombok.Getter;
import lombok.Setter;

/**
 * Extends survey dto.
 *
 * @author syalavarthi.
 *
 */
public class SurveyHDto extends SurveyWithTaskListDto {

  /**
   * The default serial version id.
   */
  private static final long serialVersionUID = 1L;

  /**
   * The service narratives list.
   */
  @Getter
  @Setter
  private String[] serviceNarratives;

  /**
   * The service catalogue.
   */
  @Getter
  @Setter
  private ServiceCatalogueHDto serviceCatalogueH;

}
