package com.baesystems.ai.lr.cd.be.utils;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EOR;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.LR_ADMIN;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.LR_INTERNAL;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.LR_SUPPORT;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;

/**
 * Provides utilities to get user roles and user role checking.
 *
 * @author RKaneysan
 * @author SYalavarthi 29-06-2017.
 * @author SBollu 14-07-2017.
 */
public final class SecurityUtils {

  /**
   * Private constructor SecurityUtils.
   */
  private SecurityUtils() {
    super();
  }

  /**
   * The message digest encryption algorithm.
   */
  public static final String MD5_ALGO = "MD5";

  /**
   * The UTF character encoding.
   */
  public static final String UTF_8 = "UTF-8";

  /**
   * The specified radix of BigInteger to which the string representation of BigInteger should
   * translate.
   */
  private static final int HEX = 16;

  /**
   * The maximum limit to generate milliseconds.
   */
  private static final int MAX_LIMIT = 100;

  /**
   * The AES algorithm.
   */
  private static final String AES_ALGO = "AES";

  /**
   * Initial Vector for Download.
   */
  private static String downloadInitVector = "DownLoadRandomIV";

  /**
   * The message digest encryption algorithm.
   */
  private static final String AES_INSTANCE = "AES/CBC/PKCS5Padding";

  /**
   * The Logger for {@link SecurityUtils}.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtils.class);

  /**
   * Provide MD5 encryption for given data and shared key.
   *
   * @param input the input for encryption.
   * @return encrypted hash value of the input.
   * @throws ClassDirectSecurityException class-direct security exception.
   */
  public static String getMessageDigestEncyption(final String input) throws ClassDirectSecurityException {
    StringBuilder encryptedValue = new StringBuilder();
    try {
      MessageDigest messageDigest = MessageDigest.getInstance(MD5_ALGO);
      final byte[] encryptedByte = messageDigest.digest(input.getBytes(UTF_8));
      // Convert byte array to hexadecimal string.
      for (byte eachByte : encryptedByte) {
        encryptedValue.append(String.format("%02x", eachByte));
      }
      LOGGER.debug("Generated encrypted value is {}. ", encryptedValue);
    } catch (final NoSuchAlgorithmException | UnsupportedEncodingException exception) {
      LOGGER.error("General Security Exception: Error in generating hash for given data and security key. ", exception);
      throw new ClassDirectSecurityException(exception);
    }
    return encryptedValue.toString();
  }

  /**
   * Gets all the role and populate into the list.
   *
   * @return list of roles.
   */
  public static List<String> getRoles() {
    final CDAuthToken token = (CDAuthToken) SecurityContextHolder.getContext().getAuthentication();
    final List<String> roles = new ArrayList<>();
    if (token != null) {
      roles.addAll(
          token.getAuthorities().stream().map(SimpleGrantedAuthority::getAuthority).collect(Collectors.toList()));
    }

    return roles;
  }

  /**
   * Returns true if user is equsis or thetis role.
   *
   * @return true if user is equsis or thetis role.
   */
  public static boolean isEquasisThetisUser() {
    final boolean equasisThetisUser = getRoles().stream().filter(authority -> {
      return authority.equalsIgnoreCase(EQUASIS.toString()) || authority.equalsIgnoreCase(THETIS.toString());
    }).findAny().isPresent();

    return equasisThetisUser;
  }

  /**
   * Returns true if the user with any of the roles LR_INTERNAL, LR_ADMIN and LR_SUPPORT.
   *
   * @param roles the user roles.
   * @return true if the user with any of the roles LR_INTERNAL, LR_ADMIN and LR_SUPPORT otherwise
   *         false.
   */
  public static boolean isInternalUser(final List<String> roles) {
    final boolean isInternalUser = roles.stream().filter(authority -> {
      return authority.equalsIgnoreCase(LR_INTERNAL.toString()) || authority.equalsIgnoreCase(LR_ADMIN.toString())
          || authority.equalsIgnoreCase(LR_SUPPORT.toString());
    }).findAny().isPresent();

    return isInternalUser;
  }

  /**
   * Returns true if the user with any of the roles LR_INTERNAL, LR_ADMIN and LR_SUPPORT.
   *
   * @return true if the user with any of the roles LR_INTERNAL, LR_ADMIN and LR_SUPPORT otherwise
   *         false.
   */
  public static boolean isInternalUser() {
    final boolean isInternalUser = getRoles().stream().filter(authority -> {
      return authority.equalsIgnoreCase(LR_INTERNAL.toString()) || authority.equalsIgnoreCase(LR_ADMIN.toString())
          || authority.equalsIgnoreCase(LR_SUPPORT.toString());
    }).findAny().isPresent();

    return isInternalUser;
  }

  /**
   * Returns true if the user with EOR role.
   *
   * @param roles the user roles.
   * @return true if the user with EOR role otherwise false.
   */
  public static boolean isEORUser(final List<String> roles) {
    final boolean isEORUser = roles.stream().filter(authority -> {
      return authority.equalsIgnoreCase(EOR.toString());
    }).findAny().isPresent();

    return isEORUser;
  }

  /**
   * Returns flag to indicate if the user credential from security context has EOR role.
   *
   * @return true if the user credential from security context with EOR role.
   */
  public static boolean isEORUser() {
    return isEORUser(getRoles());
  }



  /**
   * Returns flag to indicate if the user has administration credential.
   *
   * @param roles the user roles.
   * @return true if the user has administration credential else false.
   */
  public static boolean isAdmin(final List<String> roles) {
    boolean isAdmin = roles.stream().filter(authority -> authority.equalsIgnoreCase(LR_ADMIN.toString())
        || authority.equalsIgnoreCase(LR_SUPPORT.toString())).filter(Objects::nonNull).findAny().isPresent();
    return isAdmin;
  }


  /**
   * Returns flag to indicate if the user from security context has administration credential.
   *
   * @return true if the user has administration credential else false.
   */
  public static boolean isAdmin() {
    return isAdmin(getRoles());
  }

  /**
   * Provides AES encryption for given input string.
   *
   * @param input the string to be encrypted.
   * @param userId the userId to authenticate as salt.
   * @return encrypted and encoded base64 string.
   * @throws ClassDirectSecurityException if any IO, padding ,invalid key, invalid algorithm
   *         exception occurs.
   * @throws InterruptedException if any thread interruption exception occurs.
   */
  public static String encrypt(final String input, final String userId)
      throws ClassDirectSecurityException, InterruptedException {
    try {
      // get salt.
      byte[] saltBytes = generateSalt(userId);
      LOGGER.debug("Key Length: " + saltBytes.length);
      IvParameterSpec ivParamSpec = new IvParameterSpec(downloadInitVector.getBytes(UTF_8));
      final SecretKeySpec skeySpec = new SecretKeySpec(saltBytes, AES_ALGO);
      final Cipher cipher = Cipher.getInstance(AES_INSTANCE);
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParamSpec);
      final byte[] encrypted = cipher.doFinal(input.getBytes(UTF_8));
      return Base64.encodeBase64URLSafeString(encrypted);

    } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
        | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException
        | ArrayIndexOutOfBoundsException | IllegalArgumentException excep) {
      LOGGER.error("Unable to write file token for input: {} for userId  : {} | ErrorMessage:  {}", input, userId,
          excep.getMessage(), excep);
      Random rand = new Random(System.currentTimeMillis());
      int sleepMilliSecondsValue = rand.nextInt(MAX_LIMIT);
      Thread.sleep(sleepMilliSecondsValue);
      throw new ClassDirectSecurityException("Invalid Token.");
    }
  }

  /**
   * generates salt for security encryption.
   *
   * @param userId the string to be convert into bytes.
   * @return userId.
   * @throws IllegalArgumentException if the userId contains invalid hex character.
   * @throws ArrayIndexOutOfBoundsException if length more than sixteen bytes.
   */
  private static byte[] generateSalt(final String userId)
      throws ArrayIndexOutOfBoundsException, IllegalArgumentException {
    // to avoid NumberFormatException("Illegal embedded sign character")
    String userIdAfterReplace = userId.replaceAll("-", "");
    final byte[] result = DatatypeConverter.parseHexBinary(userIdAfterReplace);
    return Arrays.copyOfRange(result, 0, HEX);
  }

  /**
   * Provides AES decryption for given input string.
   *
   * @param input the string to be decrypted.
   * @param userId the userId to authenticate as salt.
   * @return decrypted and decoded string.
   * @throws ClassDirectSecurityException if any IO, padding ,invalid key, invalid algorithm
   *         exception occurs.
   * @throws InterruptedException if any thread interruption exception occurs.
   */
  public static String decrypt(final String input, final String userId)
      throws ClassDirectSecurityException, InterruptedException {
    try {
      // get salt.
      byte[] saltBytes = generateSalt(userId);
      LOGGER.debug("Key Length: " + saltBytes.length);
      byte[] encryptedTextBytes = Base64.decodeBase64(input.getBytes(UTF_8));
      final IvParameterSpec ivParamSpec = new IvParameterSpec(downloadInitVector.getBytes(UTF_8));
      final SecretKeySpec skeySpec = new SecretKeySpec(saltBytes, AES_ALGO);
      final Cipher cipher = Cipher.getInstance(AES_INSTANCE);
      cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParamSpec);

      byte[] decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
      return new String(decryptedTextBytes, UTF_8);

    } catch (BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | NoSuchPaddingException
        | InvalidKeyException | InvalidAlgorithmParameterException | UnsupportedEncodingException
        | IllegalArgumentException excep) {
      LOGGER.error("Failed to decode encrypted token:  {} for userId : {} | ErrorMessage:  {}",
          new Object[] {input, userId, excep.getMessage(), excep});
      Random rand = new Random(System.currentTimeMillis());
      int sleepMilliSecondsValue = rand.nextInt(MAX_LIMIT);
      Thread.sleep(sleepMilliSecondsValue);
      throw new ClassDirectSecurityException("Invalid Token");
    }

  }

}
