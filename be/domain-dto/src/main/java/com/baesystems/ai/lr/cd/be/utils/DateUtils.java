package com.baesystems.ai.lr.cd.be.utils;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * @author VKolagutla
 *
 */
public final class DateUtils {

  /**
   * MILLIES_1000.
   */
  private static final Long MILLISECOND_IN_MINUTE = 1000L;

  /**
   *
   */
  private DateUtils() {
    super();
  }

  /**
   * @param date to LocalDateTime.
   * @return LocalDateTime.
   */
  public static LocalDateTime getDateToLocalDate(final Date date) {
    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
  }

  /**
   * @param date to LocalDate.
   * @return LocalDate.
   */
  public static LocalDate getLocalDateFromDate(final Date date) {
    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
  }

  /**
   * @param localDateTime to Epoch.
   * @return Epoch.
   */
  public static Long getLocalDateToEpoch(final LocalDateTime localDateTime) {
    return localDateTime.atZone(ZoneId.systemDefault()).toEpochSecond() * MILLISECOND_IN_MINUTE;
  }

  /**
   * @param date to Epoch.
   * @return Epoch.
   */
  public static Long getDateToEpoch(final Date date) {
    return date.toInstant().atZone(ZoneId.systemDefault()).toEpochSecond() * MILLISECOND_IN_MINUTE;
  }

  /**
   * @param localDate to Date.
   * @return Date.
   */
  public static Date getLocalDateToDate(final LocalDate localDate) {
    return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
  }

  /**
   * @param offSetTimeInSeconds OffSet Time from Current Date.
   * @return offSet local date.
   */
  public static LocalDateTime getLocalDateOffset(final Long offSetTimeInSeconds) {
    final Long currentEpochDate = LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond();
    return Instant.ofEpochSecond(currentEpochDate - offSetTimeInSeconds).atZone(ZoneId.systemDefault())
        .toLocalDateTime();
  }

  /**
   * @param offSetTimeInSeconds String OffSet Time from Current Date.
   * @return offSet local date.
   */
  public static LocalDate getLocalDateOffset(final String offSetTimeInSeconds) {
    return getLocalDateOffset(Long.parseLong(offSetTimeInSeconds)).toLocalDate();
  }

  /**
   * @param datetoFormat date to be formatted.
   * @param pattern date pattern to returned.
   * @return formatted String Date.
   */
  public static String getFormattedDatetoString(final Date datetoFormat, final String pattern) {
    final Format formatter = new SimpleDateFormat(pattern, Locale.getDefault());
    return formatter.format(datetoFormat);
  }

  /**
   * @param strDatetoFormat string date to be formatted.
   * @param pattern date pattern to returned.
   * @return formatted String Date.
   * @throws ParseException ParseException.
   */
  public static Date getFormattedStringDatetoDate(final String strDatetoFormat, final String pattern)
      throws ParseException {
    final DateFormat formatter = new SimpleDateFormat(pattern, Locale.getDefault());
    return formatter.parse(strDatetoFormat);
  }

  /**
   * @param offSetTimeInSeconds offset time in seconds.
   * @param datePattern date pattern to be formatted.
   * @return formatted String Date.
   */
  public static String getOffSetDateTimeString(final Long offSetTimeInSeconds, final String datePattern) {
    final LocalDateTime localDateTime = getLocalDateOffset(offSetTimeInSeconds);
    return localDateTime.format(DateTimeFormatter.ofPattern(datePattern));
  }

  /**
   * @param strDatetoFormat string date to be formatted.
   * @param pattern date pattern to returned.
   * @return formatted String Date.
   * @throws ParseException ParseException.
   */
  public static String getFormattedLocaleDatetoString(final LocalDate strDatetoFormat, final String pattern)
      throws ParseException {
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
    return strDatetoFormat.format(formatter);
  }

}
