package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides all types of codicil statuses list.
 *
 * @author syalavarthi.
 *
 */
public class CodicilStatusesHDto {


  /**
   * The codicil statuses list.
   */
  @Getter
  @Setter
  private List<String> cocStatuses;
  /**
   * The actionable item statuses list.
   */
  @Getter
  @Setter
  private List<String> aiStatuses;
  /**
   * The asset notes statuses list.
   */
  @Getter
  @Setter
  private List<String> anStatuses;
  /**
   * The statutory findings statuses list.
   */
  @Getter
  @Setter
  private List<String> sfStatuses;
  /**
   * The major non confirmative statuses list.
   */
  @Getter
  @Setter
  private List<String> mncnStatuses;
  /**
   * The defect statuses list.
   */
  @Getter
  @Setter
  private List<String> defectStatuses;
  /**
   * The deficiency statuses list.
   */
  @Getter
  @Setter
  private List<String> deficiencyStatuses;
}
