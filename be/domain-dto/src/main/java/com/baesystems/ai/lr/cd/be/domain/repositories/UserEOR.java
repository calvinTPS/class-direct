package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateConverter;

/**
 * @author sbollu
 *
 */
@Entity
@Table(name = "USER_EOR")
public class UserEOR implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -1950057651983206929L;

  /**
   * primary key for this entity.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "USER_EOR_ID")
  private Long eorId;

  /**
   * imo number.
   */
  @Column(name = "IMO_NUM", nullable = false)
  private String imoNumber;

  /**
   * userId.
   */

  @Column(name = "USER_ID", nullable = false)
  private String userId;

  /**
   * Eor asset expiry date.
   */
  @Column(name = "ASSET_EXPIRY_DATE", nullable = true)
  @Convert(converter = LocalDateConverter.class)
  private LocalDate assetExpiryDate;

  /**
   * accessTmReport.
   */
  @Column(name = "ACCESS_TM_REPORT", nullable = false)
  private Boolean accessTmReport;

  /**
   * @return the eorId
   */
  public final Long getEorId() {
    return eorId;
  }

  /**
   * @param eorIdObject to set
   */
  public final void setEorId(final Long eorIdObject) {
    this.eorId = eorIdObject;
  }

  /**
   * @return the userId.
   */
  public final String getUserId() {
    return userId;
  }

  /**
   * @param userIdObject to set.
   */
  public final void setUserId(final String userIdObject) {
    this.userId = userIdObject;
  }

  /**
   * @return the assetExpiryDate
   */
  public final LocalDate getAssetExpiryDate() {
    return assetExpiryDate;
  }

  /**
   * @param assetExpiryDateObject to set
   */
  public final void setAssetExpiryDate(final LocalDate assetExpiryDateObject) {
    this.assetExpiryDate = assetExpiryDateObject;
  }

  /**
   * @return the accessTmReport
   */
  public final Boolean getAccessTmReport() {
    return accessTmReport;
  }

  /**
   * @param accessTmReportObject to set
   */
  public final void setAccessTmReport(final Boolean accessTmReportObject) {
    this.accessTmReport = accessTmReportObject;
  }

  /**
   * @return the imoNumber.
   */
  public final String getImoNumber() {
    return imoNumber;
  }

  /**
   * @param imoNumberObject to set.
   */
  public final void setImoNumber(final String imoNumberObject) {
    this.imoNumber = imoNumberObject;
  }

}
