package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * SurveyReportsPageResourceHDto extending SurveyReportsPageResourceHDto from MAST.
 *
 * @author yng
 *
 */
public class SurveyReportsPageResource extends BasePageResource<ReportHDto> {

  /**
   * List of ReportLightHDto.
   */
  private List<ReportHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<ReportHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<ReportHDto> objectList) {
    this.content = objectList;
  }

}
