package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * AssetNotePageResourceHDto object extending AssetNotePageResourceDto from MAST.
 *
 * @author yng
 *
 */
public class AssetNotePageResource extends BasePageResource<AssetNoteHDto> {

  /**
   * List of asset note.
   */
  private List<AssetNoteHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<AssetNoteHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<AssetNoteHDto> assetNoteList) {
    this.content = assetNoteList;
  }

}
