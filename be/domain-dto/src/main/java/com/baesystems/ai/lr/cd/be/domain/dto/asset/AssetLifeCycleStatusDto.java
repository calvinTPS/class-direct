package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * Hydrated dto for {@link AssetLightDto}.
 *
 * @author sirisha
 *
 */
public class AssetLifeCycleStatusDto extends ReferenceDataDto {
  /**
   * Generated UID.
   *
   */
  private static final long serialVersionUID = -7715853397804288284L;
}
