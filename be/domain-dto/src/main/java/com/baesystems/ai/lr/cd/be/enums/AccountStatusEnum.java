package com.baesystems.ai.lr.cd.be.enums;

/**
 * Enumeration of {@link com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus}.
 *
 * @author Faizal Sidek
 */
public enum AccountStatusEnum {
  /**
   * Active status.
   */
  ACTIVE("Active", 1L),
  /**
   * Disable status.
   */
  DISABLED("Disabled", 2L),
  /**
   * Archived status.
   */
  ARCHIVED("Archived", 3L);

  /**
   * Name of status.
   */
  private String name;

  /**
   * Name of status.
   */
  private Long id;

  /**
   * Default constructor.
   *
   * @param statusName name of status.
   * @param value id of object.
   */
  private AccountStatusEnum(final String statusName, final Long value) {
    this.name = statusName;
    this.id = value;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return value of id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * Getter for {@link #name}.
   *
   * @return name of status.
   */
  public final String getName() {
    return name;
  }
}
