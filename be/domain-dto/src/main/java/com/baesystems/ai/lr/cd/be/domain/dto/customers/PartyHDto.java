package com.baesystems.ai.lr.cd.be.domain.dto.customers;

import com.baesystems.ai.lr.dto.customers.PartyDto;

/**
 * Provides POJO to capture customer information return from customer query API.
 *
 * @author YWearn
 */
public class PartyHDto extends PartyDto {

  /**
   * The unique serial version UID.
   */
  private static final long serialVersionUID = 313220106287760220L;
}
