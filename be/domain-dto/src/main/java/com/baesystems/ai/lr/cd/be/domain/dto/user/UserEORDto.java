package com.baesystems.ai.lr.cd.be.domain.dto.user;

import java.time.LocalDate;

/**
 * @author sbollu
 *
 */
public class UserEORDto {


  /**
   * imoNumber.
   */
  private String imoNumber;
  /**
   * assetExpiry.
   */
  private LocalDate assetExpiry;
  /**
   * tmReport.
   */
  private Boolean tmReport;



  /**
   * @return the imoNumber
   */
  public final String getImoNumber() {
    return imoNumber;
  }

  /**
   * @param imoNumberObject to set
   */
  public final void setImoNumber(final String imoNumberObject) {
    this.imoNumber = imoNumberObject;
  }

  /**
   * @return the assetExpiry.
   */
  public final LocalDate getAssetExpiry() {
    return assetExpiry;
  }

  /**
   * @param assetExpiryObject to set.
   */
  public final void setAssetExpiry(final LocalDate assetExpiryObject) {
    this.assetExpiry = assetExpiryObject;
  }

  /**
   * @return the tmReport.
   */
  public final Boolean getTmReport() {
    return tmReport;
  }

  /**
   * @param tmReportObject to set.
   */
  public final void setTmReport(final Boolean tmReportObject) {
    this.tmReport = tmReportObject;
  }


}
