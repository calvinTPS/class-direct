/**
 * This package contains DTO for Azure User.
 * @author YWearn
 *
 */
package com.baesystems.ai.lr.cd.be.domain.dto.azure;
