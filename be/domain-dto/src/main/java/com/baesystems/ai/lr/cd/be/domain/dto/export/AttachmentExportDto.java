package com.baesystems.ai.lr.cd.be.domain.dto.export;

/**
 * @author syalavarthi.
 *
 */
public class AttachmentExportDto {
  /**
   * attachment id.
   */
  private long attachmentId;

  /**
   * job id.
   */
  private long jobId;

  /**
   * Getter for job id.
   *
   * @return job id.
   */
  public final long getJobId() {
    return jobId;
  }

  /**
   * Setter for job id.
   *
   * @param jobIdArg job id.
   */
  public final void setJobId(final long jobIdArg) {
    this.jobId = jobIdArg;
  }

  /**
   * Getter for attachment id.
   *
   * @return attachment id.
   */
  public final long getAttachmentId() {
    return attachmentId;
  }

  /**
   * Setter for attachment id.
   *
   * @param attachmentIdArg attachment id.
   */
  public final void setAttachmentId(final long attachmentIdArg) {
    this.attachmentId = attachmentIdArg;
  }

}
