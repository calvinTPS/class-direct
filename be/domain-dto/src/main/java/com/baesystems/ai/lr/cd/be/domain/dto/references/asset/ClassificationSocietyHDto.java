package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import lombok.Getter;
import lombok.Setter;

/**
 * Classification society HDTO.
 *
 * @author yng
 *
 */
/**
 * @author syalavarthi
 *
 */
public class ClassificationSocietyHDto extends ReferenceDataDto {

  /**
   * generated serial id.
   */
  private static final long serialVersionUID = 3641628763693989617L;
  /**
   * The code for classification society.
   */
  @Getter
  @Setter
  private String code;
}
