package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * Used in audit purpose of pmsable tasks.
 *
 * @author syalavarthi.
 *
 */
public class AuditPmsAbleTaskDto {

  /**
   * The task id.
   */
  @Getter
  @Setter
  private Long id;
  /**
   * The task name.
   */
  @Getter
  @Setter
  private String taskName;

  /**
   * The task number.
   */
  @Getter
  @Setter
  private String taskNumber;

  /**
   * The task date credited.
   */
  @Getter
  @Setter
  private Date dateCredited;

}
