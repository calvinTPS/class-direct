package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;

/**
 * @author syalavarthi.
 *
 */
public class DetailedDefectDto {
  /**
   * defect category.
   */
  private String defectCategory;
  /**
   * defect title.
   */
  private String defectTitle;
  /**
   * list of items.
   */
  private List<FaultItemDto> items;
  /**
   * status.
   */
  private String status;
  /**
   * defect narrative.
   */
  private String defectNarrative;
  /**
   * date occured.
   */
  private Date dateOccured;

  /**
   * Append jobs data.
   */
  private List<JobHDto> jobsH;
  /**
   * Append data for coc.
   */
  private List<CoCHDto> cocH;

  /**
   * Getter for cocH.
   *
   * @return return list of coc.
   */
  public final List<CoCHDto> getCocH() {
    return cocH;
  }

  /**
   * Setter for cocH.
   *
   * @param cocHObj object.
   */
  public final void setCocH(final List<CoCHDto> cocHObj) {
    this.cocH = cocHObj;
  }

  /**
   * Getter for defectNarrative.
   *
   * @return defectNarrative.
   */
  public final String getDefectNarrative() {
    return defectNarrative;
  }

  /**
   * Setter for defectNarrative.
   *
   * @param defectNarrativeObj value.
   */
  public final void setDefectNarrative(final String defectNarrativeObj) {
    this.defectNarrative = defectNarrativeObj;
  }

  /**
   * Getter for defectCategory.
   *
   * @return defectCategory.
   */
  public final String getDefectCategory() {
    return defectCategory;
  }

  /**
   * Setter for defectCategory.
   *
   * @param defectCategoryObj value.
   */
  public final void setDefectCategory(final String defectCategoryObj) {
    this.defectCategory = defectCategoryObj;
  }

  /**
   * Getter for defectTitle.
   *
   * @return defectTitle.
   */
  public final String getDefectTitle() {
    return defectTitle;
  }

  /**
   * Setter for defectTitle.
   *
   * @param defectTitleObj value.
   */
  public final void setDefectTitle(final String defectTitleObj) {
    this.defectTitle = defectTitleObj;
  }

  /**
   * Getter for dateOccured.
   *
   * @return dateOccured.
   */
  public final Date getDateOccured() {
    return dateOccured;
  }

  /**
   * Setter for dateOccured.
   *
   * @param dateOccuredObj value.
   */
  public final void setDateOccured(final Date dateOccuredObj) {
    this.dateOccured = dateOccuredObj;
  }

  /**
   * Getter for jobs list.
   *
   * @return defect status dto.
   */
  public final List<FaultItemDto> getItems() {
    return items;
  }

  /**
   * Setter for items.
   *
   * @param itemsObj value.
   */
  public final void setItems(final List<FaultItemDto> itemsObj) {
    this.items = itemsObj;
  }

  /**
   * Getter for defect status.
   *
   * @return defect status dto.
   */
  public final String getStatus() {
    return status;
  }

  /**
   * Setter for status.
   *
   * @param statusObj value.
   */
  public final void setStatus(final String statusObj) {
    this.status = statusObj;
  }

  /**
   * @return the jobsH
   */
  public final List<JobHDto> getJobsH() {
    return jobsH;
  }

  /**
   * @param jobsHArg to set
   */
  public final void setJobsH(final List<JobHDto> jobsHArg) {
    this.jobsH = jobsHArg;
  }
}
