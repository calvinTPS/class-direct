package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sbollu
 *
 */
public class GroupDto extends ReferenceDataDto {
  /**
   * The unique generated serial version UID.
   */
  private static final long serialVersionUID = 4821828530767400495L;


  /**
   * Constructs Dto.
   */
  public GroupDto() {
    super();
  }

  /**
   * Constructs DTO with check list group {@link ChecklistGroupDto}.
   *
   * @param group the checklist group.
   */
  public GroupDto(final ChecklistGroupDto group) {
    this();
    this.setId(group.getId());
    this.setName(group.getName());
    this.setChecklistSubgrouplist(new ArrayList<>());
  }

  /**
   * The list of checklist subgroups {@link SubgroupDto}.
   */
  @Getter
  @Setter
  private List<SubgroupDto> checklistSubgrouplist;
  /**
   * lookup object for checklist subgroup.
   */
  @JsonIgnoreProperties
  private Map<Long, SubgroupDto> lookupMap = new HashMap<>();

  /**
   * @param subgroup the checklist group.
   * @return checklist subgroup model object.
   */
  public final SubgroupDto get(final ChecklistSubgroupDto subgroup) {
    SubgroupDto result = lookupMap.get(subgroup.getId());
    if (result == null) {
      result = new SubgroupDto(subgroup);
      lookupMap.put(subgroup.getId(), result);
      checklistSubgrouplist.add(result);
    }
    return result;
  }


}
