package com.baesystems.ai.lr.cd.be.domain.dto.customers;

import lombok.Getter;
import lombok.Setter;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

/**
 * Provides POJO object to capture {@link PartyDto} country information.
 *
 * @author YWearn
 *
 */
public class CountryHDto extends BaseReferenceDataDto {

  /**
   * The unique serial version UID.
   */
  private static final long serialVersionUID = 203505976182021401L;


  /**
   * The country name.
   */
  @Getter
  @Setter
  private String name;

  /**
   * The detail information on a country.
   */
  @Getter
  @Setter
  private String description;

}
