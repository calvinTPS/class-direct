package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import lombok.Getter;
import lombok.Setter;

/**
 * Used to create parent-child model for FAR/FSR and ESP.
 *
 * @author syalavarthi.
 *
 */
public class AssetItemHierarchyHDto {
  /**
   * The asset item id.
   */
  @Getter
  @Setter
  private Long id;

  /**
   * The asset item name.
   */
  @Getter
  @Setter
  private String name;

  /**
   * The parent asset item name with asset item name.
   */
  @Getter
  @Setter
  private String parentAssetItemName;

  /**
   * The asset item display order.
   */
  @Getter
  @Setter
  private String dispalyOrder;

  /**
   * Objects point to the same reference and therefore the same object -> result = true, method
   * returns true<br>
   * Objects are not the same object -> result = false
   * <p>
   * Other objects is not a BaseDTO -> result is unchanged i.e.false, method returns false<br>
   * ID of other object equals ID of this object -> result = true, method returns true<br>
   * ID of other object does NOT equal ID of this object -> result is unchanged i.e. false, method
   * returns false
   * <p>
   * Therefore, equals only if exact same object, or if other is a BaseDto and has the same ID.
   */
  @Override
  public final boolean equals(final Object other) {
    boolean result = this == other;

    if (!result && other instanceof AssetItemHierarchyHDto) {
      final AssetItemHierarchyHDto itemDto = (AssetItemHierarchyHDto) other;

      if (itemDto.getId() != null && itemDto.getId().equals(this.getId())) {
        result = true;
      }
    }

    return result;
  }

  @Override
  public final int hashCode() {
    final int result;

    if (this.getId() != null) {
      result = this.getId().hashCode();
    } else {
      result = super.hashCode();
    }

    return result;
  }

}
