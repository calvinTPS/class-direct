package com.baesystems.ai.lr.cd.be.domain.repositories;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * DO for file archived in AWS Glacier.
 *
 * @author fwijaya on 6/3/2017.
 */
@Entity
@Table(name = "AWS_GLACIER_AUDIT")
public class AwsGlacierAudit {

  /**
   * Id.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private int id;

  /**
   * Archive id of the archived file.
   */
  @Column(name = "GLACIER_ARCHIVE_ID", nullable = false)
  private String glacierArchiveId;

  /**
   * Vault name of the AWS glacier where file is archived.
   */
  @Column(name = "GLACIER_VAULT_NAME", nullable = false)
  private String glacierVaultName;

  /**
   * Url to download the archived file.
   */
  @Column(name = "DOWNLOAD_REQ_URL", nullable = false)
  private String downloadRequestUrl;

  /**
   * Timestamp of the export.
   */
  @Column(name = "EXPORT_TIME", nullable = false)
  @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
  private LocalDateTime exportTime;

  /**
   * The id of the user doing the export.
   */
  @Column(name = "USER_ID", nullable = false)
  private String userId;

  /**
   * Getter for id.
   *
   * @return the id.
   */
  public final int getId() {
    return id;
  }

  /**
   * Setter for id.
   *
   * @param idArg id.
   */
  public final void setId(final int idArg) {
    this.id = idArg;
  }

  /**
   * Getter for glacier archive id.
   *
   * @return glacier archive id.
   */
  public final String getGlacierArchiveId() {
    return glacierArchiveId;
  }

  /**
   * Setter for glacier archive id.
   *
   * @param glacierArchiveIdArg archive id.
   */
  public final void setGlacierArchiveId(final String glacierArchiveIdArg) {
    this.glacierArchiveId = glacierArchiveIdArg;
  }

  /**
   * Getter for glacier vault name.
   *
   * @return AWS glacier vault name.
   */
  public final String getGlacierVaultName() {
    return glacierVaultName;
  }

  /**
   * Setter for glacier vault name.
   *
   * @param glacierVaultNameArg vault name.
   */
  public final void setGlacierVaultName(final String glacierVaultNameArg) {
    this.glacierVaultName = glacierVaultNameArg;
  }

  /**
   * Getter for download request url.
   *
   * @return download request url.
   */
  public final String getDownloadRequestUrl() {
    return downloadRequestUrl;
  }

  /**
   * Setter for download request url.
   *
   * @param downloadRequestUrlArg download url.
   */
  public final void setDownloadRequestUrl(final String downloadRequestUrlArg) {
    this.downloadRequestUrl = downloadRequestUrlArg;
  }

  /**
   * Getter for export time.
   *
   * @return export time.
   */
  public final LocalDateTime getExportTime() {
    return exportTime;
  }

  /**
   * Setter for export time.
   *
   * @param exportTimeArg export time.
   */
  public final void setExportTime(final LocalDateTime exportTimeArg) {
    this.exportTime = exportTimeArg;
  }

  /**
   * Getter for user id.
   *
   * @return user id.
   */
  public final String getUserId() {
    return userId;
  }

  /**
   * Setter for user id.
   *
   * @param userIdArg user id.
   */
  public final void setUserId(final String userIdArg) {
    this.userId = userIdArg;
  }
}
