package com.baesystems.ai.lr.cd.be.domain.dto.customers;

import java.io.Serializable;

import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.PartyRoleHDto;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sbollu
 *
 */
public class CustomerLinkHDto implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 7143310421715513273L;

  /**
   * Linked to {@link PartyRoleHDto}.
   *
   */
  @Getter
  @Setter
  private PartyRoleHDto partyRoles;

  /**
   * Linked to {@link CustomerLinkDto}.
   */
  @Getter
  @Setter
  private CustomerLinkDto party;

}
