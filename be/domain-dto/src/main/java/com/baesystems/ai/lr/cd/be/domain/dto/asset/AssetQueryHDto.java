package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Function;

import com.baesystems.ai.lr.cd.be.enums.OrderOptionEnum;
import com.baesystems.ai.lr.cd.be.enums.SortOptionEnum;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;

import lombok.Getter;
import lombok.Setter;

/**
 * Extended AssetQuery with favorite.
 *
 * @author msidek
 *
 */
public class AssetQueryHDto extends AssetQueryDto {

  /**
   * Constant for hashcode.
   */
  private static final int THIRTY_ONE = 31;

  /**
   * List of codicil types.
   */
  private List<String> codicilTypeId = new ArrayList<>();

  /**
   * Flag for favourite.
   *
   */
  private Boolean isFavourite;

  /**
   * List of client IDs.
   */
  private List<Long> clientIds = new ArrayList<>();

  /**
   * List of asset types.
   */
  private List<String> assetTypeCodes = new ArrayList<>();

  /**
   * Client name.
   */
  private String clientName;

  /**
   * Codicils min due date.
   */
  private Date codicilDueDateMin;

  /**
   * Codicils max due date.
   */
  private Date codicilDueDateMax;

  /**
   * Sort query results.
   */
  private SortOptionEnum sort;

  /**
   * Order option.
   */
  private OrderOptionEnum order;

  /**
   * Asset code.
   */
  private String code;

  /**
   * List of service catalogue.
   */
  private List<Long> serviceCatalogueIds;

  /**
   * Flag for EOR.
   *
   */
  private Boolean isEOR = Boolean.FALSE;

  /**
   * Flag state codes.
   */
  private List<String> flagStateCodes;

  /**
   * True if querying for user's subfleet only.
   */
  private Boolean isSubfleet;

  /**
   * True if querying for user's MMS Service only.
   */
  private Boolean isMMSService;

  /**
   * The former asset name.
   */
  @Getter
  @Setter
  private String formerAssetName;

  /**
   * If true will filter only assets with active services.
   */
  @Getter
  @Setter
  private Boolean hasActiveServices;

  /**
   * If true search including former asset name.
   */
  @Getter
  @Setter
  private Boolean includeFormerAssetNames;

  /**
   * If true include in-active asset.
   */
  @Getter
  @Setter
  private Boolean includeInactiveAssets;
  /**
   * Getter for {@link #flagStateCodes}.
   *
   * @return value of flagStateCodes.
   */
  public final List<String> getFlagStateCodes() {
    return flagStateCodes;
  }

  /**
   * Setter for {@link #flagStateCodes}.
   *
   * @param codes value to be set.
   */
  public final void setFlagStateCodes(final List<String> codes) {
    this.flagStateCodes = codes;
  }

  /**
   * Getter for {@link #serviceCatalogueIds}.
   *
   * @return value of serviceCatalogueIds.
   */
  public final List<Long> getServiceCatalogueIds() {
    return serviceCatalogueIds;
  }

  /**
   * Setter for {@link #serviceCatalogueIds}.
   *
   * @param ids value to be set.
   */
  public final void setServiceCatalogueIds(final List<Long> ids) {
    this.serviceCatalogueIds = ids;
  }

  /**
   * Getter for {@link #code}.
   *
   * @return value of code.
   */
  public final String getCode() {
    return code;
  }

  /**
   * Setter for {@link #code}.
   *
   * @param assetCode value to be set.
   */
  public final void setCode(final String assetCode) {
    this.code = assetCode;
  }

  /**
   * Getter for {@link #order}.
   *
   * @return value of order.
   */
  public final OrderOptionEnum getOrder() {
    return order;
  }

  /**
   * Setter for {@link #order}.
   *
   * @param orderOption value to be set.
   */
  public final void setOrder(final OrderOptionEnum orderOption) {
    this.order = orderOption;
  }

  /**
   * Getter for {@link #sort}.
   *
   * @return value of sort.
   */
  public final SortOptionEnum getSort() {
    return sort;
  }

  /**
   * Setter for {@link #sort}.
   *
   * @param sortOptionEnum value to be set.
   */
  public final void setSort(final SortOptionEnum sortOptionEnum) {
    this.sort = sortOptionEnum;
  }

  /**
   * Getter for {@link #codicilDueDateMin}.
   *
   * @return value of codicilDueDateMin.
   */
  public final Date getCodicilDueDateMin() {
    return codicilDueDateMin;
  }

  /**
   * Setter for {@link #codicilDueDateMin}.
   *
   * @param dueDateMin value to be set.
   */
  public final void setCodicilDueDateMin(final Date dueDateMin) {
    this.codicilDueDateMin = dueDateMin;
  }

  /**
   * Getter for {@link #codicilDueDateMax}.
   *
   * @return value of codicilDueDateMax.
   */
  public final Date getCodicilDueDateMax() {
    return codicilDueDateMax;
  }

  /**
   * Setter for {@link #codicilDueDateMax}.
   *
   * @param dueDateMax value to be set.
   */
  public final void setCodicilDueDateMax(final Date dueDateMax) {
    this.codicilDueDateMax = dueDateMax;
  }

  /**
   * Setter for {@link #assetTypeCodes}.
   *
   * @param codes value to be set.
   */
  public final void setAssetTypeCodes(final List<String> codes) {
    this.assetTypeCodes = codes;
  }

  /**
   * Getter for {@link #assetTypeCodes}.
   *
   * @return value of assetTypeCodes.
   */
  public final List<String> getAssetTypeCodes() {
    return assetTypeCodes;
  }

  /**
   * Getter for {@link #clientName}.
   *
   * @return value of clientName.
   */
  public final String getClientName() {
    return clientName;
  }

  /**
   * Setter for {@link #clientName}.
   *
   * @param name value to be set.
   */
  public final void setClientName(final String name) {
    this.clientName = name;
  }

  /**
   * Getter for {@link #clientIds}.
   *
   * @return value of clientIds.
   */
  public final List<Long> getClientIds() {
    return clientIds;
  }

  /**
   * Setter for {@link #clientIds}.
   *
   * @param ids value to be set.
   */
  public final void setClientIds(final List<Long> ids) {
    this.clientIds = ids;
  }

  /**
   * Getter for {@link #isFavourite}.
   *
   * @return favourite flag.
   *
   */
  public final Boolean getIsFavourite() {
    return isFavourite;
  }

  /**
   * Setter for {@link #isFavourite}.
   *
   * @param favourite to be set.
   *
   */
  public final void setIsFavourite(final Boolean favourite) {
    this.isFavourite = favourite;
  }

  /**
   * Getter for {@link #codicilTypeId}.
   *
   * @return A list of codicil type id.
   */
  public final List<String> getCodicilTypeId() {
    return codicilTypeId;
  }

  /**
   * Getter for {@link #codicilTypeId}.
   *
   * @param codicilType Codicil type id.
   */
  public final void setCodicilTypeId(final List<String> codicilType) {
    this.codicilTypeId = codicilType;
  }

  /**
   * @return the isEOR.
   */
  public final Boolean getIsEOR() {
    return isEOR;
  }

  /**
   * @param eOR to set.
   */
  public final void setIsEOR(final Boolean eOR) {
    this.isEOR = eOR;
  }

  /**
   * @return True if getting subfleet assets only.
   */
  public final Boolean getIsSubfleet() {
    return isSubfleet;
  }

  /**
   * @param subfleet Boolean to set.
   */
  public final void setIsSubfleet(final Boolean subfleet) {
    this.isSubfleet = subfleet;
  }

  /**
   * @return the isMMSService.
   */
  public final Boolean getIsMMSService() {
    return isMMSService;
  }

  /**
   * @param mmsService the isMMSService to set.
   */
  public final void setIsMMSService(final Boolean mmsService) {
    this.isMMSService = mmsService;
  }

  @Override
  public final int hashCode() {

    final Function<Object, Integer> getHash = obj -> {
      if (obj != null) {
        return obj.hashCode();
      } else {
        return 0;
      }
    };

    int result = getHash.apply(getSearch());
    result = THIRTY_ONE * result + getHash.apply(getLifecycleStatusId());
    result = THIRTY_ONE * result + getHash.apply(getImoNumber());
    result = THIRTY_ONE * result + getHash.apply(getAssetTypeId());
    result = THIRTY_ONE * result + getHash.apply(getYardNumber());
    result = THIRTY_ONE * result + getHash.apply(getBuildDateMin());
    result = THIRTY_ONE * result + getHash.apply(getBuildDateMax());
    result = THIRTY_ONE * result + getHash.apply(getEffectiveDateMin());
    result = THIRTY_ONE * result + getHash.apply(getEffectiveDateMax());
    result = THIRTY_ONE * result + getHash.apply(getClassStatusId());
    result = THIRTY_ONE * result + getHash.apply(getClassDepartmentId());
    result = THIRTY_ONE * result + getHash.apply(getLinkedAssetId());
    result = THIRTY_ONE * result + getHash.apply(getFlagStateId());
    result = THIRTY_ONE * result + getHash.apply(getGrossTonnageMin());
    result = THIRTY_ONE * result + getHash.apply(getGrossTonnageMax());
    result = THIRTY_ONE * result + getHash.apply(getDeadWeightMin());
    result = THIRTY_ONE * result + getHash.apply(getDeadWeightMax());
    result = THIRTY_ONE * result + getHash.apply(getPartyId());
    result = THIRTY_ONE * result + getHash.apply(getPartyRoleId());
    result = THIRTY_ONE * result + getHash.apply(getPartyRoleId());
    result = THIRTY_ONE * result + getHash.apply(getBuilder());
    result = THIRTY_ONE * result + getHash.apply(getCategoryId());
    result = THIRTY_ONE * result + getHash.apply(getIdList());
    result = THIRTY_ONE * result + getHash.apply(getItemTypeId());
    result = THIRTY_ONE * result + getHash.apply(getAttributeTypeId());
    result = THIRTY_ONE * result + getHash.apply(getOperator());
    result = THIRTY_ONE * result + getHash.apply(getAttributeValue());
    result = THIRTY_ONE * result + getHash.apply(getIsFavourite());
    result = THIRTY_ONE * result + getHash.apply(getIsEOR());
    return result;
  }

  @Override
  public final boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final BiPredicate<Object, Object> isEqual = (obj1, obj2) -> {
      if (obj1 != null) {
        return !obj1.equals(obj2);
      } else {
        return obj2 != null;
      }
    };

    final AssetQueryHDto queryDto = (AssetQueryHDto) o;

    if (isEqual.test(getSearch(), queryDto.getSearch())) {
      return false;
    }
    if (isEqual.test(getLifecycleStatusId(), queryDto.getLifecycleStatusId())) {
      return false;
    }
    if (isEqual.test(getImoNumber(), queryDto.getImoNumber())) {
      return false;
    }
    if (isEqual.test(getAssetTypeId(), queryDto.getAssetTypeId())) {
      return false;
    }
    if (isEqual.test(getYardNumber(), queryDto.getYardNumber())) {
      return false;
    }
    if (isEqual.test(getBuildDateMin(), queryDto.getBuildDateMin())) {
      return false;
    }
    if (isEqual.test(getBuildDateMax(), queryDto.getBuildDateMax())) {
      return false;
    }
    if (isEqual.test(getEffectiveDateMin(), queryDto.getEffectiveDateMin())) {
      return false;
    }
    if (isEqual.test(getEffectiveDateMax(), queryDto.getEffectiveDateMax())) {
      return false;
    }
    if (isEqual.test(getClassStatusId(), queryDto.getClassStatusId())) {
      return false;
    }
    if (isEqual.test(getClassDepartmentId(), queryDto.getClassDepartmentId())) {
      return false;
    }
    if (isEqual.test(getLinkedAssetId(), queryDto.getLinkedAssetId())) {
      return false;
    }
    if (isEqual.test(getFlagStateId(), queryDto.getFlagStateId())) {
      return false;
    }
    if (isEqual.test(getGrossTonnageMin(), queryDto.getGrossTonnageMin())) {
      return false;
    }
    if (isEqual.test(getGrossTonnageMax(), queryDto.getGrossTonnageMax())) {
      return false;
    }
    if (isEqual.test(getDeadWeightMin(), queryDto.getDeadWeightMin())) {
      return false;
    }
    if (isEqual.test(getDeadWeightMax(), queryDto.getDeadWeightMax())) {
      return false;
    }
    if (isEqual.test(getPartyId(), queryDto.getPartyId())) {
      return false;
    }
    if (isEqual.test(getPartyRoleId(), queryDto.getPartyRoleId())) {
      return false;
    }
    if (isEqual.test(getPartyRoleId(), queryDto.getPartyRoleId())) {
      return false;
    }
    if (isEqual.test(getBuilder(), queryDto.getBuilder())) {
      return false;
    }
    if (isEqual.test(getCategoryId(), queryDto.getCategoryId())) {
      return false;
    }
    if (isEqual.test(getIdList(), queryDto.getIdList())) {
      return false;
    }
    if (isEqual.test(getItemTypeId(), queryDto.getItemTypeId())) {
      return false;
    }
    if (isEqual.test(getAttributeTypeId(), queryDto.getAttributeTypeId())) {
      return false;
    }

    if (isEqual.test(getIsFavourite(), queryDto.getIsFavourite())) {
      return false;
    }

    if (isEqual.test(getIsEOR(), queryDto.getIsEOR())) {
      return false;
    }

    if (getOperator() != queryDto.getOperator()) {
      return false;
    }

    return !isEqual.test(getAttributeValue(), queryDto.getAttributeValue());

  }
}
