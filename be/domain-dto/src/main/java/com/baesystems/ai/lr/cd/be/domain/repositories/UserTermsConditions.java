package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

/**
 * @author sbollu
 *
 */
@Entity
@Table(name = "USER_TERMS_CONDITIONS")
public class UserTermsConditions implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 8772259304297581410L;

  /**
   * Id of the entity.
   *
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;

  /**
   * userId.
   */

  @Column(name = "USER_ID", nullable = false)
  private String userId;

  /**
   * TermsConditions id.
   */
  @Column(name = "TC_ID", nullable = false)
  private Long tcId;

  /**
   * TermsConditions accepted date.
   */
  @Column(name = "ACCEPTED_DATE", nullable = true)
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime acceptedDate;



  /**
   * @return the id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * @param idObject to set.
   */
  public final void setId(final Long idObject) {
    this.id = idObject;
  }

  /**
   * @return the userId.
   */
  public final String getUserId() {
    return userId;
  }

  /**
   * @param userIdObject to set.
   */
  public final void setUserId(final String userIdObject) {
    this.userId = userIdObject;
  }

  /**
   * @return the tcId.
   */
  public final Long getTcId() {
    return tcId;
  }

  /**
   * @param tcIdObject to set.
   */
  public final void setTcId(final Long tcIdObject) {
    this.tcId = tcIdObject;
  }

  /**
   * @return the acceptedDate.
   */
  public final LocalDateTime getAcceptedDate() {
    return acceptedDate;
  }

  /**
   * @param acceptedDateObject to set.
   */
  public final void setAcceptedDate(final LocalDateTime acceptedDateObject) {
    this.acceptedDate = acceptedDateObject;
  }

}
