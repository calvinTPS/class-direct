package com.baesystems.ai.lr.cd.be.domain.repositories;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author vkovuru
 */
@Entity
@Table(name = "ROLES")
public class Roles implements Serializable {
  /**
   *
   */
  private static final long serialVersionUID = 7625830302496172115L;
  /**
   *
   */
  @Id
  @Column(name = "ROLE_ID")
  private Long roleId;
  /**
   *
   */
  @Column(name = "ROLE_NAME")
  private String roleName;

  /**
   *
   */
  @Transient
  private Boolean islrSupport;

  /**
   *
   */
  @Transient
  private Boolean islrAdmin;

  /**
   *
   */
  @Transient
  private Boolean isAdmin;

  /**
   * @return value.
   */
  public final Long getRoleId() {
    return roleId;
  }

  /**
   * @param roleIdObject value.
   */
  public final void setRoleId(final Long roleIdObject) {
    this.roleId = roleIdObject;
  }

  /**
   * @return value.
   */
  public final String getRoleName() {
    return roleName;
  }

  /**
   * @param roleNameObject value.
   */
  public final void setRoleName(final String roleNameObject) {
    this.roleName = roleNameObject;
  }

  /**
   * @return the isClientadmin.
   */
  public final Boolean getIslrSupport() {
    return islrSupport;
  }

  /**
   * @param lrSupport to set
   */
  public final void setIslrSupport(final Boolean lrSupport) {
    this.islrSupport = lrSupport;
  }

  /**
   * @return the islrAdmin.
   */
  public final Boolean getIslrAdmin() {
    return islrAdmin;
  }

  /**
   * @param lrAdmin to set.
   */
  public final void setIslrAdmin(final Boolean lrAdmin) {
    this.islrAdmin = lrAdmin;
  }

  /**
   * @return the isAdmin.
   */
  public final Boolean getIsAdmin() {
    return isAdmin;
  }

  /**
   * @param admin to set.
   */
  public final void setIsAdmin(final Boolean admin) {
    this.isAdmin = admin;
  }
}
