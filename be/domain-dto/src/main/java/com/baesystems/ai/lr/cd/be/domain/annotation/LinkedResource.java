package com.baesystems.ai.lr.cd.be.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Linked resource annotation.
 *
 * @author msidek
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface LinkedResource {
  /**
   * Provides meta information about target DTO. Optional for non-collection resource.
   *
   */
  Class<?> target() default Object.class;

  /**
   * Field name of id from extended DTO.
   *
   */
  String referencedField();

  /**
   * Lazy initialize resource.
   *
   */
  boolean lazy() default false;
}
