package com.baesystems.ai.lr.cd.be.enums;

/**
 *
 * Enum for asset information export.
 *
 * @author yng
 *
 */
public enum AssetInformationExportEnum {

  /**
   * Basic asset information.
   */
  BASIC_ASSET_INFO(1L),

  /**
   * Full asset information.
   */
  FULL_ASSET_INFO(2L);

  /**
   * enum id.
   */
  private Long id;

  /**
   * Constructor.
   *
   * @param value id.
   */
  AssetInformationExportEnum(final Long value) {
    this.id = value;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return value of id.
   */
  public final Long getId() {
    return id;
  }

}
