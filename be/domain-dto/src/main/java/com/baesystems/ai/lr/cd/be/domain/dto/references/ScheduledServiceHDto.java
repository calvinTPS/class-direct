package com.baesystems.ai.lr.cd.be.domain.dto.references;

import java.util.Date;

import com.baesystems.ai.lr.cd.be.domain.annotation.DueStatusField;
import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.IDueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.PostponementTypeHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.cd.be.utils.DateUtils;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

/**
 * ScheduledServiceHDTO.
 *
 * @author VMandalapu
 *
 */
public class ScheduledServiceHDto extends ScheduledServiceDto implements IDueStatusDto, Cloneable {

  /**
   *
   */
  private static final long serialVersionUID = 6238135996358918221L;

  /**
   * Linked resource to ServiceCreditStatusHDto.
   */
  @LinkedResource(referencedField = "serviceCreditStatus")
  private ServiceCreditStatusHDto serviceCreditStatusH;

  /**
   * Linked resource to service catalogue dto.
   *
   */
  @LinkedResource(referencedField = "serviceCatalogue")
  private ServiceCatalogueHDto serviceCatalogueH;

  /**
   * Linked resource to postponement type.
   */
  @LinkedResource(referencedField = "postponementType")
  private PostponementTypeHDto postponementTypeH;

  /**
   * dueStatus.
   */
  @DueStatusField
  private DueStatus dueStatusH;
  /**
   * Service catalogue name along with occurence number.
   */
  @Getter
  @Setter
  private String serviceCatalogueName;

  /**
   * Asset code.
   */
  private String code;

  /**
   * Future Dates related field.
   */
  private Long repeatOf;

  /**
   * The Due date epoch.
   */
  @Getter
  @Setter
  private Long dueDateEpoch;

  /**
   * Getter for repeatOf.
   *
   * @return value.
   */
  public final Long getRepeatOf() {
    return repeatOf;
  }

  /**
   * Setter for repeatOf.
   *
   * @param repeatOfArg value.
   */
  public final void setRepeatOf(final Long repeatOfArg) {
    this.repeatOf = repeatOfArg;
  }

  /**
   * Getter for {@link #code}.
   *
   * @return value of code.
   */
  public final String getCode() {
    return code;
  }

  /**
   * Setter for {@link #code}.
   *
   * @param assetCode value to be set.
   */
  public final void setCode(final String assetCode) {
    this.code = assetCode;
  }

  /**
   * @return dto
   */
  public final ServiceCreditStatusHDto getServiceCreditStatusH() {
    return serviceCreditStatusH;
  }

  /**
   * @param serviceCreditStatusHObject dto
   */
  public final void setServiceCreditStatusH(final ServiceCreditStatusHDto serviceCreditStatusHObject) {
    this.serviceCreditStatusH = serviceCreditStatusHObject;
  }

  /**
   * Getter for catalogue dto.
   *
   * @return dto
   *
   */
  public final ServiceCatalogueHDto getServiceCatalogueH() {
    return serviceCatalogueH;
  }

  /**
   * Set catalogue dto.
   *
   * @param serviceCatalogueHObject to be set.
   *
   */
  public final void setServiceCatalogueH(final ServiceCatalogueHDto serviceCatalogueHObject) {
    this.serviceCatalogueH = serviceCatalogueHObject;
  }

  /**
   * @return the dueStatusH
   */
  @Override
  public final DueStatus getDueStatusH() {
    return dueStatusH;
  }

  /**
   * @param dueStatusHObj the dueStatusH to set
   */
  public final void setDueStatusH(final DueStatus dueStatusHObj) {
    this.dueStatusH = dueStatusHObj;
  }

  /**
   * @return lowRangeDate in Epoch.
   */
  public final Long getLowerRangeDateEpoch() {
    Long epoch = null;
    if (null != getLowerRangeDate()) {
      epoch = DateUtils.getDateToEpoch(getLowerRangeDate());
    }
    return epoch;
  }

  /**
   * @return upperRangeDate in Epoch.
   */
  public final Long getUpperRangeDateEpoch() {
    Long epoch = null;
    if (null != getUpperRangeDate()) {
      epoch = DateUtils.getDateToEpoch(getUpperRangeDate());
    }
    return epoch;
  }

  /**
   * @return assignedDate in Epoch.
   */
  public final Long getAssignedDateEpoch() {
    Long epoch = null;
    if (null != getAssignedDate()) {
      epoch = DateUtils.getDateToEpoch(getAssignedDate());
    }
    return epoch;
  }

  /**
   * Getter for hydrated postponement type.
   *
   * @return PostponementTypeH value.
   */
  public final PostponementTypeHDto getPostponementTypeH() {
    return postponementTypeH;
  }

  /**
   * Setter for hydrated postponement type.
   *
   * @param postponementTypeHObj value.
   */
  public final void setPostponementTypeH(final PostponementTypeHDto postponementTypeHObj) {
    postponementTypeH = postponementTypeHObj;
  }

  @Override
  public final ScheduledServiceHDto clone() throws CloneNotSupportedException {
    return (ScheduledServiceHDto) super.clone();
  }

  /**
   * Overrides active flag of parent class.
   *
   * @param active the boolean flag.
   */
  @JsonIgnore(false)
  public final void setActive(final Boolean active) {
    super.setActive(active);
  }

  /**
   * Overrides setter for due date to calculate due date epoch and Sets due date epoch. previous
   * logic (getter field set up for due date epoch and calculating due date epoch in get method)
   * failed, due to java refelection API did not recognize due date epoch field.
   *
   * @param dueDate value.
   */
  @Override
  public final void setDueDate(final Date dueDate) {
    super.setDueDate(dueDate);
    Long epoch = null;
    if (dueDate != null) {
      epoch = DateUtils.getDateToEpoch(dueDate);
      setDueDateEpoch(epoch);
    }
  }

}
