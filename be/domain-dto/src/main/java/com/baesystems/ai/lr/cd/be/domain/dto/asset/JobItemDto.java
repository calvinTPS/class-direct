package com.baesystems.ai.lr.cd.be.domain.dto.asset;

/**
 * @author RKaneysan
 *
 */
public class JobItemDto {

  /**
   * Job Number.
   */
  private String jobNumber;

  /**
   * Name.
   */
  private String name;

  /**
   * Last Visit.
   */
  private String lastVisit;

  /**
   * Location.
   */
  private String location;

  /**
   * Job Icon.
   */
  private String icon;

  /**
   * Job Status Label.
   */
  private String statusLabel;

  /**
   * Job Link.
   */
  private String link;

  /**
   * @return the jobNumber.
   */
  public final String getJobNumber() {
    return jobNumber;
  }

  /**
   * @param jobNumberParam the jobNumber to set.
   */
  public final void setJobNumber(final String jobNumberParam) {
    this.jobNumber = jobNumberParam;
  }

  /**
   * @return the name.
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameParam the name to set.
   */
  public final void setName(final String nameParam) {
    this.name = nameParam;
  }

  /**
   * @return the lastVisit.
   */
  public final String getLastVisit() {
    return lastVisit;
  }

  /**
   * @param lastVisitParam the lastVisit to set.
   */
  public final void setLastVisit(final String lastVisitParam) {
    this.lastVisit = lastVisitParam;
  }

  /**
   * @return the location.
   */
  public final String getLocation() {
    return location;
  }

  /**
   * @param locationParam the location to set.
   */
  public final void setLocation(final String locationParam) {
    this.location = locationParam;
  }

  /**
   * @return the icon.
   */
  public final String getIcon() {
    return icon;
  }

  /**
   * @param iconParam the icon to set.
   */
  public final void setIcon(final String iconParam) {
    this.icon = iconParam;
  }

  /**
   * @return the statusLabel.
   */
  public final String getStatusLabel() {
    return statusLabel;
  }

  /**
   * @param statusLabelParam the status to set.
   */
  public final void setStatusLabel(final String statusLabelParam) {
    this.statusLabel = statusLabelParam;
  }

  /**
   * @return the link.
   */
  public final String getLink() {
    return link;
  }

  /**
   * @param linkParam the link to set.
   */
  public final void setLink(final String linkParam) {
    this.link = linkParam;
  }

  /**
   * Override Object toString(). @see java.lang.Object#toString()
   *
   * @return all fields separated by comma in String format.
   */
  public final String toString() {
    return "jobNumber : " + this.jobNumber + ", name : " + this.name + ", lastVisit : " + this.lastVisit
        + ", location : " + this.location + ", icon : " + this.icon + ", status : " + this.statusLabel + ", link : "
        + this.link;
  }

}
