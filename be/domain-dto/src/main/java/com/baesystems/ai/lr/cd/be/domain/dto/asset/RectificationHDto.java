package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairActionHDto;
import com.baesystems.ai.lr.dto.defects.RectificationDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author syalavarthi.
 *
 */
public class RectificationHDto extends RectificationDto {

  /**
   * static serialVersionUID.
   */
  private static final long serialVersionUID = -8196726766021924547L;
  /**
   * hydrate repair action.
   */
  @LinkedResource(referencedField = "repairAction")
  @Getter
  @Setter
  private RepairActionHDto repairActionH;
}
