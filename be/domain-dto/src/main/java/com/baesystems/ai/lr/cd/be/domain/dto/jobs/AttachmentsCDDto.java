package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;

/**
 * @author syalavarthi.
 *
 */
public class AttachmentsCDDto {
  /**
   * List of SupplementaryInformation.
   *
   */
  private List<SupplementaryInformationHDto> attachments;

  /**
   * Getter for {@link #content}.
   *
   * @return attachments.
   */
  public final List<SupplementaryInformationHDto> getAttachments() {
    return attachments;
  }

  /**
   * @param attachmentsObj attachments. Setter for {@link #content}.
   */
  public final void setAttachments(final List<SupplementaryInformationHDto> attachmentsObj) {
    this.attachments = attachmentsObj;
  }
}
