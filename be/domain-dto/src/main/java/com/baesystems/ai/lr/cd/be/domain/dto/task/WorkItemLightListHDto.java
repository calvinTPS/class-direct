package com.baesystems.ai.lr.cd.be.domain.dto.task;

import static com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto.ENTITY_NAME;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.annotations.EntityName;
import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.dto.services.StalenessListDto;

/**
 * @author VKolagutla
 *
 */
@EntityName(ENTITY_NAME)
public class WorkItemLightListHDto extends StalenessListDto implements Dto, Serializable {
  /**
  *
  */
  private static final long serialVersionUID = 5745841228511280480L;

  /**
   * Task.
   */
  public static final String ENTITY_NAME = "Task";

  /**
   * list of tasks.
   */
  @NotNull
  @Valid
  private List<WorkItemLightHDto> tasks;

  /**
   * @return the tasks
   */
  public final List<WorkItemLightHDto> getTasks() {
    return tasks;
  }

  /**
   * @param tasksObj the tasks to set
   */
  public final void setTasks(final List<WorkItemLightHDto> tasksObj) {
    this.tasks = tasksObj;
  }

  @Override
  protected final String entityName() {
    return ENTITY_NAME;
  }
}
