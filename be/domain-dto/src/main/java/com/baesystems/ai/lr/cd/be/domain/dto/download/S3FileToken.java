package com.baesystems.ai.lr.cd.be.domain.dto.download;

import java.io.Serializable;

/**
 * S3 file token.
 *
 * @author Faizal Sidek
 */
public class S3FileToken implements Serializable {

  /**
   * Serial uuid.
   */
  private static final long serialVersionUID = 1444587571985827917L;

  /**
   * S3 region.
   */
  private String region;

  /**
   * S3 bucket name.
   */
  private String bucketName;

  /**
   * Key path to file.
   */
  private String fileKey;

  /**
   * File etag for web cache validation.
   */
  private String etag;

  /**
   * Getter for {@link #bucketName}.
   *
   * @return value of bucketName.
   */
  public final String getBucketName() {
    return bucketName;
  }

  /**
   * Setter for {@link #bucketName}.
   *
   * @param name value to be set.
   */
  public final void setBucketName(final String name) {
    this.bucketName = name;
  }

  /**
   * Getter for {@link #region}.
   *
   * @return value of region.
   */
  public final String getRegion() {
    return region;
  }

  /**
   * Setter for {@link #region}.
   *
   * @param regionName value to be set.
   */
  public final void setRegion(final String regionName) {
    this.region = regionName;
  }

  /**
   * Getter for {@link #fileKey}.
   *
   * @return value of fileKey.
   */
  public final String getFileKey() {
    return fileKey;
  }

  /**
   * Setter for {@link #fileKey}.
   *
   * @param path value to be set.
   */
  public final void setFileKey(final String path) {
    this.fileKey = path;
  }

  /**
   * Getter for {@link #etag}.
   *
   * @return value of etag.
   */
  public final String getEtag() {
    return etag;
  }

  /**
   * Setter for {@link #etag}.
   *
   * @param value value to be set.
   */
  public final void setEtag(final String value) {
    this.etag = value;
  }
}
