package com.baesystems.ai.lr.cd.be.domain.dto.azure;

import java.io.Serializable;

/**
 * @author syalavarthi.
 *
 */
public class LRIDUserStatusDto implements Serializable {

  /**
   * static serialVersionUID.
   */
  private static final long serialVersionUID = 1277757927320974293L;
  /**
   * state.
   */
  private String state;

  /**
   * @return the state
   */
  public final String getState() {
    return state;
  }

  /**
   * @param stateObj the state to set
   */
  public final void setState(final String stateObj) {
    this.state = stateObj;
  }

}
