package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author RKaneysan.
 */
@Entity
@Table(name = "NOTIFICATIONS")
public class Notifications implements Serializable {

  /**
   * Default serial version.
   */
  private static final long serialVersionUID = 1L;

  /**
   * id column for notifications table.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  @Getter
  @Setter
  private Long id;

  /**
   * User id column for notifications table.
   *
   * Foreign key for {@link UserProfiles} table.
   */
  @Getter
  @Setter
  @Column(name = "USER_ID")
  private String userId;

  /**
   * Notifications types.
   */
  @OneToOne
  @JoinColumn(name = "TYPE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  @Getter
  @Setter
  private NotificationTypes notificationTypes;

  /**
   * Type id column for notifications table.
   *
   * Foreign key for {@link NotificationTypes} table.
   */
  @Getter
  @Setter
  @Column(name = "TYPE_ID")
  private Long typeId;

  /**
   * The favouritesOnly column for notifications table.
   *
   */
  @Getter
  @Setter
  @Column(name = "FAVOURITES_ONLY")
  private boolean favouritesOnly;

  @Override
  public final String toString() {
    return "userId: " + userId + ", typeId: " + typeId + "notificationTypes:" + notificationTypes + "favouritesOnly:"
        + favouritesOnly;
  }
}
