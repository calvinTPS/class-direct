package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import lombok.Getter;
import lombok.Setter;

/**
 * Used to create model to store asset item fields.
 *
 * @author syalavarthi.
 *
 */
public class ItemHDto {

  /**
   * The asset item id.
   */
  @Getter
  @Setter
  private Long id;

  /**
   * The asset item name.
   */
  @Getter
  @Setter
  private String name;

  /**
   * The asset item display order.
   */
  @Getter
  @Setter
  private String dispalyOrder;

  /**
   * The parent asset item.
   */
  @Getter
  @Setter
  private Long parentAssetItem;


  @Override
  public final String toString() {
    return "ItemHdto [id=" + id + ", name=" + name + ", dispalyOrder=" + dispalyOrder + ", parentAssetItem="
        + parentAssetItem + "]";
  }
}
