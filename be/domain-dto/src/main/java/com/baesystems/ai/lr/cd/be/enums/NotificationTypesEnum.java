package com.baesystems.ai.lr.cd.be.enums;

/**
 * Enumerators for Notification Types.
 *
 * @author RKaneysan
 */
public enum NotificationTypesEnum {

  /**
   * Asset Listing.
   */
  ASSET_LISTING(1L, "Asset Listing"),
  /**
   * Due Status.
   */
  DUE_STATUS(2L, "Due Status"),
  /**
   * Job Reports.
   */
  JOB_REPORTS(3L, "Job Reports");

  /**
   * name.
   */
  private String name;
  /**
   * long id.
   */
  private Long id;

  /**
   * @param idObj id.
   * @param nameObj name.
   */
  NotificationTypesEnum(final Long idObj, final String nameObj) {
    this.name = nameObj;
    this.id = idObj;
  }

  /**
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * @return the id
   */
  public final Long getId() {
    return id;
  }

}
