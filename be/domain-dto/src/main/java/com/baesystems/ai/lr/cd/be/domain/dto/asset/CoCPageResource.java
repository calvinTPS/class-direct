package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * CoCPageResourceHDto object extending CoCPageResourceDto from MAST.
 *
 * @author yng
 *
 */
public class CoCPageResource extends BasePageResource<CoCHDto> {

  /**
   * List of coc.
   */
  private List<CoCHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<CoCHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<CoCHDto> coCList) {
    this.content = coCList;
  }

}
