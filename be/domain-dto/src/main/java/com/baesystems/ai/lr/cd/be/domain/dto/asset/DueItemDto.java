package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.baesystems.ai.lr.cd.be.utils.DateUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * Asset Header Dto for Asset Update Status Notification Email.
 *
 * @author RKaneysan
 */
public class DueItemDto implements Serializable {

  /**
   * BLANK.
   */
  public static final String BLANK = "";

  /**
   * Regex string without white spaces.
   */
  private static final String STRING_WITHOUT_WHITE_SPACE_REGEX = "\\s+";

  /**
   * Regex string without white spaces.
   */
  private static final String DUE_STATUS_ICON_PREFIX = "overall-";

  /**
   * DATE_FORMAT_PERIOD_SEPARATED.
   */
  private static final String DATE_FORMAT_DD_MMM_YYYY = "dd MMM yyyy";

  /**
   * Default Serial Version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Title.
   */
  private String title;

  /**
   * Category.
   */
  private String category;


  /**
   * Imposed Date.
   */
  private String imposedDate;

  /**
   * Due Date.
   */
  private String dueDate;

  /**
   * Due Status.
   */
  private String dueStatusIcon;

  /**
   * Due Status Label.
   */
  private String dueStatusLabel;

  /**
   * Link.
   */
  private String link;

  /**
   * Service Name.
   */
  private String serviceName;

  /**
   * Service Code.
   */
  @Getter
  @Setter
  private String serviceCode;

  /**
   * Occurrence number.
   */
  @Getter
  @Setter
  private Integer occurrenceNumber;

  /**
   * Product.
   */
  private String product;

  /**
   * Lower Range Date.
   */
  private String lowerRangeDate;

  /**
   * Upper Range Date.
   */
  private String upperRangeDate;

  /**
   * Postponement Date.
   */
  private String postponementDate;

  /**
   * Getter for {@link #link}.
   *
   * @return value of link.
   */
  public final String getLink() {
    return link;
  }

  /**
   * Setter for {@link #link}.
   *
   * @param itemLink value to be set.
   */
  public final void setLink(final String itemLink) {
    this.link = itemLink;
  }

  /**
   * @param dateParam date string.
   * @return formatted date.
   */
  public final String getDate(final Date dateParam) {
    String date = BLANK;
    if (Objects.nonNull(dateParam)) {
      date = DateUtils.getFormattedDatetoString(dateParam, DATE_FORMAT_DD_MMM_YYYY);
    }
    return date;
  }

  /**
   * @param dueStatusParam Due Status.
   * @return trimmed string.
   */
  public final String getDueStatusIcon(final String dueStatusParam) {
    return DUE_STATUS_ICON_PREFIX + dueStatusParam.replaceAll(STRING_WITHOUT_WHITE_SPACE_REGEX, BLANK);
  }

  /**
   * @return the title.
   */
  public final String getTitle() {
    return title;
  }

  /**
   * @param titleParam the title to set.
   */
  public final void setTitle(final String titleParam) {
    this.title = titleParam;
  }

  /**
   * @return the category.
   */
  public final String getCategory() {
    return category;
  }

  /**
   * @param categoryParam the category to set.
   */
  public final void setCategory(final String categoryParam) {
    this.category = categoryParam;
  }

  /**
   * @return the imposedDate.
   */
  public final String getImposedDate() {
    return imposedDate;
  }

  /**
   * @param imposedDateParam the imposedDate to set.
   */
  public final void setImposedDate(final String imposedDateParam) {
    this.imposedDate = imposedDateParam;
  }

  /**
   * @return the dueDate.
   */
  public final String getDueDate() {
    return dueDate;
  }

  /**
   * @param dueDateParam the dueDate to set.
   */
  public final void setDueDate(final String dueDateParam) {
    this.dueDate = dueDateParam;
  }

  /**
   * @return the dueStatus.
   */
  public final String getDueStatusIcon() {
    return dueStatusIcon;
  }

  /**
   * @param dueStatusParam the dueStatus to set.
   */
  public final void setDueStatusIcon(final String dueStatusParam) {
    this.dueStatusIcon = dueStatusParam;
  }

  /**
   * @return the dueStatusLabel.
   */
  public final String getDueStatusLabel() {
    return dueStatusLabel;
  }

  /**
   * @param dueStatusLabelParam the dueStatusLabel to set.
   */
  public final void setDueStatusLabel(final String dueStatusLabelParam) {
    this.dueStatusLabel = dueStatusLabelParam;
  }

  /**
   * @return the serviceName.
   */
  public final String getServiceName() {
    return serviceName;
  }

  /**
   * @param serviceNameParam the serviceName to set.
   */
  public final void setServiceName(final String serviceNameParam) {
    this.serviceName = serviceNameParam;
  }

  /**
   * @return the product.
   */
  public final String getProduct() {
    return product;
  }

  /**
   * @param productParam the product to set.
   */
  public final void setProduct(final String productParam) {
    this.product = productParam;
  }

  /**
   * @return the lowerRangeDate.
   */
  public final String getLowerRangeDate() {
    return lowerRangeDate;
  }

  /**
   * @param lowerRangeDateParam the lowerRangeDate to set.
   */
  public final void setLowerRangeDate(final String lowerRangeDateParam) {
    this.lowerRangeDate = lowerRangeDateParam;
  }

  /**
   * @return the upperRangeDate.
   */
  public final String getUpperRangeDate() {
    return upperRangeDate;
  }

  /**
   * @param upperRangeDateParam the upperRangeDate to set.
   */
  public final void setUpperRangeDate(final String upperRangeDateParam) {
    this.upperRangeDate = upperRangeDateParam;
  }

  /**
   * @return the postponementDate.
   */
  public final String getPostponementDate() {
    return postponementDate;
  }

  /**
   * @param postponementDateParam the postponementDate to set.
   */
  public final void setPostponementDate(final String postponementDateParam) {
    this.postponementDate = postponementDateParam;
  }

}
