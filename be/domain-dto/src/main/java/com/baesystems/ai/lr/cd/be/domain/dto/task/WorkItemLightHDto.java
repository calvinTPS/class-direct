package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.DueStatusField;
import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.IDueStatusDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CheckListHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobResolutionStatusHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author VKolagutla
 *
 */
public class WorkItemLightHDto extends WorkItemLightDto implements IDueStatusDto {

  /**
   * dueStatus.
   */
  @DueStatusField
  private DueStatus dueStatusH;

  /**
   * workItemType for Task and Checklist.
   */
  @LinkedResource(referencedField = "workItemType")
  private WorkItemTypeHDto workItemTypeH;

  /**
   * postponementType.
   */
  @LinkedResource(referencedField = "postponementType")
  private PostponementTypeHDto postponementTypeH;

  /**
   * Linked resource to JobResolutionStatusHDto.
   */
  @LinkedResource(referencedField = "resolutionStatus")
  private JobResolutionStatusHDto resolutionStatusH;

  /**
   * The workItemQAModel for esp appendix 1.
   */
  @Getter
  @Setter
  private List<WorkItemQAModel> workItemQAModel;

  /**
   * Linked to CheckListDto.
   *
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "checklist")
  private CheckListHDto checklistH;

  /**
   * @return the dueStatusH
   */
  @Override
  public final DueStatus getDueStatusH() {
    return dueStatusH;
  }

  /**
   * @param dueStatusHObj the dueStatusH to set
   */
  public final void setDueStatusH(final DueStatus dueStatusHObj) {
    this.dueStatusH = dueStatusHObj;
  }

  /**
   * @return the workItemTypeH
   */
  public final WorkItemTypeHDto getWorkItemTypeH() {
    return workItemTypeH;
  }

  /**
   * @param workItemTypeHObj the workItemTypeH to set
   */
  public final void setWorkItemTypeH(final WorkItemTypeHDto workItemTypeHObj) {
    this.workItemTypeH = workItemTypeHObj;
  }

  /**
   * @return the postponementTypeH
   */
  public final PostponementTypeHDto getPostponementTypeH() {
    return postponementTypeH;
  }

  /**
   * @param postponementTypeHObj the postponementTypeH to set
   */
  public final void setPostponementTypeH(final PostponementTypeHDto postponementTypeHObj) {
    this.postponementTypeH = postponementTypeHObj;
  }

  /**
   * @return the resolutionStatusH
   */
  public final JobResolutionStatusHDto getResolutionStatusH() {
    return resolutionStatusH;
  }

  /**
   * @param resolutionStatusHObj the resolutionStatusH to set
   */
  public final void setResolutionStatusH(final JobResolutionStatusHDto resolutionStatusHObj) {
    this.resolutionStatusH = resolutionStatusHObj;
  }


}
