package com.baesystems.ai.lr.cd.be.domain.dto.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sbollu
 *
 */
public class HierarchyDto implements Serializable {
  /**
   * The unique generated serial version UID.
   */
  private static final long serialVersionUID = 4821828530767400495L;


  /**
   * The list of checklist groups {@link GroupDto}.
   */
  @Getter
  @Setter
  private List<GroupDto> checklistGroups;


  /**
   * lookup object for checklist group.
   */
  @JsonIgnore
  private Map<Long, GroupDto> lookupMap = new HashMap<>();


  /**
   * @param group the checklist group.
   * @return checklist group model object.
   */
  public final GroupDto get(final ChecklistGroupDto group) {
    GroupDto result = lookupMap.get(group.getId());
    if (result == null) {
      result = new GroupDto(group);
      lookupMap.put(group.getId(), result);
      if (checklistGroups != null && !checklistGroups.isEmpty()) {
        checklistGroups.add(result);
      } else {
        this.setChecklistGroups(new ArrayList<>());
        checklistGroups.add(result);
      }
    }
    return result;
  }


}
