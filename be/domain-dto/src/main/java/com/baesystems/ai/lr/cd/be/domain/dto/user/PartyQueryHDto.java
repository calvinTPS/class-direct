package com.baesystems.ai.lr.cd.be.domain.dto.user;

import com.baesystems.ai.lr.dto.query.PartyQueryDto;

/**
 * Party Query Hydrated Dto.
 */
public class PartyQueryHDto extends PartyQueryDto {

  /**
   * serial Version UID.
   */
  private static final long serialVersionUID = 602653722194650599L;

}

