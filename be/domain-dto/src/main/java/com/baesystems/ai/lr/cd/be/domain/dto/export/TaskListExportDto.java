package com.baesystems.ai.lr.cd.be.domain.dto.export;

/**
 * @author VKolagutla
 *
 */
public class TaskListExportDto {

  /**
   * assetId.
   */
  private String assetCode;

  /**
   * itemType.
   */
  private String itemType;

  /**
   * serviceId.
   */
  private Long serviceId;

  /**
   * @return the assetCode
   */
  public final String getAssetCode() {
    return assetCode;
  }

  /**
   * @param assetCodeObj the assetCode to set
   */
  public final void setAssetCode(final String assetCodeObj) {
    this.assetCode = assetCodeObj;
  }

  /**
   * @return the itemType
   */
  public final String getItemType() {
    return itemType;
  }

  /**
   * @param itemTypeObj the itemType to set
   */
  public final void setItemType(final String itemTypeObj) {
    this.itemType = itemTypeObj;
  }

  /**
   * @return the serviceId
   */
  public final Long getServiceId() {
    return serviceId;
  }

  /**
   * @param serviceIdObj the serviceId to set
   */
  public final void setServiceId(final Long serviceIdObj) {
    this.serviceId = serviceIdObj;
  }



}
