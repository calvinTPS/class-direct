package com.baesystems.ai.lr.cd.be.domain.dto.company;

import java.io.Serializable;

/**
 * @author SBollu
 *
 */
public class Company implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -4012976724526082464L;
  /**
   * name.
   */
  private String name;
  /**
   * addressLine1.
   */
  private String addressLine1;
  /**
   * addressLine2.
   */
  private String addressLine2;
  /**
   * addressLine3.
   */
  private String addressLine3;
  /**
   * City.
   */
  private String city;
  /**
   * state.
   */
  private String state;
  /**
   * postCode.
   */
  private String postCode;
  /**
   * country.
   */
  private String country;
  /**
   * Telephone.
   */
  private String telephone;

  /**
   * Getter for telephone.
   *
   * @return telephone.
   */

  public final String getTelephone() {
    return telephone;
  }

  /**
   * @param telephoneObject telephone
   */
  public final void setTelephone(final String telephoneObject) {
    this.telephone = telephoneObject;
  }

  /**
   * Getter for postCode.
   *
   * @return postCode.
   */
  public final String getPostCode() {
    return postCode;
  }

  /**
   * @param postCodeObject postCode.
   */
  public final void setPostCode(final String postCodeObject) {
    this.postCode = postCodeObject;
  }

  /**
   * @return the addressLine1.
   */
  public final String getAddressLine1() {
    return addressLine1;
  }

  /**
   * @param addressLine1Object to set.
   */
  public final void setAddressLine1(final String addressLine1Object) {
    this.addressLine1 = addressLine1Object;
  }

  /**
   * @return the addressLine2
   */
  public final String getAddressLine2() {
    return addressLine2;
  }

  /**
   * @param addressLine2Object to set.
   */
  public final void setAddressLine2(final String addressLine2Object) {
    this.addressLine2 = addressLine2Object;
  }

  /**
   * @return the addressLine3.
   */
  public final String getAddressLine3() {
    return addressLine3;
  }

  /**
   * @param addressLine3Object to set.
   */
  public final void setAddressLine3(final String addressLine3Object) {
    this.addressLine3 = addressLine3Object;
  }

  /**
   * Getter for city.
   *
   * @return city.
   */

  public final String getCity() {
    return city;
  }

  /**
   * @param cityObject city
   */
  public final void setCity(final String cityObject) {
    this.city = cityObject;
  }

  /**
   * @return the state.
   */
  public final String getState() {
    return state;
  }

  /**
   * @param stateObject to set.
   */
  public final void setState(final String stateObject) {
    this.state = stateObject;
  }

  /**
   * @return the country.
   */
  public final String getCountry() {
    return country;
  }

  /**
   * @param countryObject to set.
   */
  public final void setCountry(final String countryObject) {
    this.country = countryObject;
  }

  /**
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * @param nameObject the name to set
   */
  public final void setName(final String nameObject) {
    this.name = nameObject;
  }


}
