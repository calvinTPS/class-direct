package com.baesystems.ai.lr.cd.be.domain.dto.export;

import java.util.Set;

/**
 * Codicil and defect export configuration.
 *
 * @author yng
 *
 */
public class ExportPdfDto {

  /**
   * asset code.
   */
  private String code;

  /**
   * Filtered Ids.
   */
  private Set<Long> itemsToExport;


  /**
   * Getter for code.
   *
   * @return code.
   */
  public final String getCode() {
    return code;
  }

  /**
   * Setter for code.
   *
   * @param codeObj value.
   */
  public final void setCode(final String codeObj) {
    this.code = codeObj;
  }

  /**
   * Setter for itemsToExport.
   * @return the itemsToExport.
   */
  public final Set<Long> getItemsToExport() {
    return itemsToExport;
  }

  /**
   * Getter for itemsToExport.
   * @param itemsToExportObj the itemsToExport to set.
   */
  public final void setItemsToExport(final Set<Long> itemsToExportObj) {
    this.itemsToExport = itemsToExportObj;
  }

}
