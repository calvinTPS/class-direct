package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairTypeHDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;

/**
 * RepairHDtoHDto extending RepairHDtoDto.
 *
 * @author yng
 *
 */
public class RepairHDto extends RepairDto {

  /**
   * Auto generated serial.
   */
  private static final long serialVersionUID = -4150965637764750537L;

  /**
   * hydrate repair action.
   */
  @LinkedResource(referencedField = "repairAction")
  private RepairActionHDto repairActionH;

  /**
   * append repair types.
   */
  private List<RepairTypeHDto> repairTypesH;

  /**
   * append repairs data.
   */
  private List<LazyItemHDto> repairsH;



  /**
   * Getter for list of repair type.
   *
   * @return list of repair type.
   */
  public final List<RepairTypeHDto> getRepairTypesH() {
    return repairTypesH;
  }

  /**
   * Setter for list of repair type.
   *
   * @param repairTypesHObj value.
   */
  public final void setRepairTypesH(final List<RepairTypeHDto> repairTypesHObj) {
    this.repairTypesH = repairTypesHObj;
  }

  /**
   * Getter for list of repair item.
   *
   * @return return list of repair item.
   */
  public final List<LazyItemHDto> getRepairsH() {
    return repairsH;
  }

  /**
   * Setter for list of repair item.
   *
   * @param repairsHObj value.
   */
  public final void setRepairsH(final List<LazyItemHDto> repairsHObj) {
    this.repairsH = repairsHObj;
  }

  /**
   * Getter for repair action.
   *
   * @return repair action dto.
   */
  public final RepairActionHDto getRepairActionH() {
    return repairActionH;
  }

  /**
   * Setter for repair action.
   *
   * @param repairActionHObj value.
   */
  public final void setRepairActionH(final RepairActionHDto repairActionHObj) {
    this.repairActionH = repairActionHObj;
  }


}
