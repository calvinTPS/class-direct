package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * service credit status HDTO.
 *
 * @author yng
 *
 */
public class ServiceCreditStatusRefHDto extends ReferenceDataDto {

  /**
   * Auto generated serial.
   */
  private static final long serialVersionUID = 546627256079877711L;



}
