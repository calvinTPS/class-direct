package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.cd.be.domain.dto.customers.CustomerLinkHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;

import lombok.Getter;
import lombok.Setter;

/**
 * Extended Asset DTO.
 *
 * @author msidek
 *
 */
public class AssetHDto extends AssetLightDto {
  /**
   * Serial id.
   *
   */
  private static final long serialVersionUID = -8316877274479530872L;

  /**
   * IMO number.
   */
  @Getter
  @Setter
  private String imoNumber;

  /**
   * Linked to {@link AssetTypeHDto}.
   *
   */
  private AssetTypeHDto assetTypeHDto;

  /**
   * Linked to {@link AssetCategoryHDto}.
   *
   */
  @LinkedResource(referencedField = "assetCategory")
  private AssetCategoryHDto assetCategoryDto;

  /**
   * Linked to {@link ClassStatusHDto}.
   *
   */
  @LinkedResource(referencedField = "classStatus")
  private ClassStatusHDto classStatusDto;

  /**
   * Linked to {@link FlagStateHDto}.
   *
   */
  @LinkedResource(referencedField = "flagState")
  private FlagStateHDto flagStateDto;

  /**
   * IHS asset.
   */
  private IhsAssetDetailsDto ihsAssetDto;

  /**
   * Global asset code.
   */
  private String code;

  /**
   * isMMSService.
   */
  private Boolean isMMSService;

  /**
   * Is restricted asset.
   */
  private Boolean restricted = Boolean.FALSE;

  /**
   * Is EOR Asset.
   *
   */
  private Boolean isEOR = Boolean.FALSE;

  /**
   * hdyrated Classification society.
   */
  @LinkedResource(referencedField = "coClassificationSociety")
  private ClassificationSocietyHDto coClassificationSocietyDto;

  /**
   * hydrated class maintenance status.
   */
  @LinkedResource(referencedField = "classMaintenanceStatus")
  private ClassMaintenanceStatusHDto classMaintenanceStatusDto;

  /**
   * Hydrated customers.
   */
  @Getter
  @Setter
  private List<CustomerLinkHDto> customersH;

  /**
   * The CFO office details.
   */
  @Getter
  @Setter
  private OfficeDto cfoOfficeH;

  /**
   * The flag to check any service with postponement date.
   */
  @Getter
  @Setter
  private Boolean hasPostponedService = Boolean.FALSE;;

  /**
   * True will indicates this asset has one or more services.
   */
  @Getter
  @Setter
  private Boolean hasActiveServices = Boolean.FALSE;

  /**
   * Getter for {@link #classMaintenanceStatusDto}.
   *
   * @return value of class maintenance status.
   */
  public final ClassMaintenanceStatusHDto getClassMaintenanceStatusDto() {
    return classMaintenanceStatusDto;
  }

  /**
   * Setter for {@link #classMaintenanceStatusDto}.
   *
   * @param classMaintenanceStatusDtoObj value.
   */
  public final void setClassMaintenanceStatusDto(final ClassMaintenanceStatusHDto classMaintenanceStatusDtoObj) {
    this.classMaintenanceStatusDto = classMaintenanceStatusDtoObj;
  }

  /**
   * Getter for {@link #coClassificationSocietyDto}.
   *
   * @return value of classification society.
   */
  public final ClassificationSocietyHDto getCoClassificationSocietyDto() {
    return coClassificationSocietyDto;
  }

  /**
   * Setter for {@link #coClassificationSocietyDto}.
   *
   * @param coClassificationSocietyDtoObj value.
   */
  public final void setCoClassificationSocietyDto(final ClassificationSocietyHDto coClassificationSocietyDtoObj) {
    this.coClassificationSocietyDto = coClassificationSocietyDtoObj;
  }

  /**
   * Getter for {@link #restricted}.
   *
   * @return value of restricted.
   */
  public final Boolean getRestricted() {
    return restricted;
  }

  /**
   * Setter for {@link #restricted}.
   *
   * @param isRestricted value to be set.
   */
  public final void setRestricted(final Boolean isRestricted) {
    this.restricted = isRestricted;
  }

  /**
   * Getter for isMMSService.
   *
   * @return isMMSService.
   */
  public final Boolean getIsMMSService() {
    return isMMSService;
  }

  /**
   * Setter for isMMSService.
   *
   * @param isMMSServiceObj isMMSService.
   */
  public final void setIsMMSService(final Boolean isMMSServiceObj) {
    this.isMMSService = isMMSServiceObj;
  }


  /**
   * Getter for {@link #code}.
   *
   * @return value of code.
   */
  public final String getCode() {
    return code;
  }

  /**
   * Setter for {@link #code}.
   *
   * @param assetCode value to be set.
   */
  public final void setCode(final String assetCode) {
    this.code = assetCode;
  }

  /**
   * Getter for {@link #ihsAssetDto}.
   *
   * @return value of ihsAssetDto.
   */
  public final IhsAssetDetailsDto getIhsAssetDto() {
    return ihsAssetDto;
  }

  /**
   * Setter for {@link #ihsAssetDto}.
   *
   * @param ihsAsset value to be set.
   */
  public final void setIhsAssetDto(final IhsAssetDetailsDto ihsAsset) {
    this.ihsAssetDto = ihsAsset;
  }

  /**
   *
   */
  private Boolean isFavourite = Boolean.FALSE;
  /**
   * Linked to {@link AssetHLifeCycleDto}.
   *
   */
  @LinkedResource(referencedField = "assetLifecycleStatus")
  private AssetLifeCycleStatusDto assetLifeCycleStatusDto;

  /**
   * dueStatusH for the overAllStatus.
   */
  private DueStatus dueStatusH;

  /**
   * Getter for {@link #assetCategoryDto}.
   *
   * @return asset category dto.
   *
   */
  public final AssetCategoryHDto getAssetCategoryDto() {
    return assetCategoryDto;
  }

  /**
   * Setter for {@link #assetCategoryDto}.
   *
   * @param dto to be set.
   *
   */
  public final void setAssetCategoryDto(final AssetCategoryHDto dto) {
    this.assetCategoryDto = dto;
  }

  /**
   * Getter for {@link #classStatusDto}.
   *
   * @return class status dto.
   *
   */
  public final ClassStatusHDto getClassStatusDto() {
    return classStatusDto;
  }

  /**
   * Setter for {@link #classStatusDto}.
   *
   * @param dto to be set.
   *
   */
  public final void setClassStatusDto(final ClassStatusHDto dto) {
    this.classStatusDto = dto;
  }

  /**
   * Getter for {@link #flagStateDto}.
   *
   * @return {@link #flagStateDto}.
   *
   */
  public final FlagStateHDto getFlagStateDto() {
    return flagStateDto;
  }

  /**
   * Setter for {@link #flagStateDto}.
   *
   * @param dto to be set.
   *
   */
  public final void setFlagStateDto(final FlagStateHDto dto) {
    this.flagStateDto = dto;
  }

  /**
   * @return boolean.
   */
  public final Boolean getIsFavourite() {
    return isFavourite;
  }

  /**
   * @param isFavouriteObject boolean.
   */
  public final void setIsFavourite(final Boolean isFavouriteObject) {
    this.isFavourite = isFavouriteObject;
  }

  /**
   * Getter for {@link #assetLifecycleDto}.
   *
   * @return {@link #assetLifecycleDto}.
   *
   */
  public final AssetLifeCycleStatusDto getAssetLifeCycleStatusDto() {
    return assetLifeCycleStatusDto;
  }

  /**
   * Setter for {@link #assetLifecycleDto}.
   *
   * @param dto to be set.
   *
   */
  public final void setAssetLifeCycleStatusDto(final AssetLifeCycleStatusDto dto) {
    this.assetLifeCycleStatusDto = dto;
  }



  /**
   * @return the dueStatusH
   */
  public final DueStatus getDueStatusH() {
    return dueStatusH;
  }

  /**
   * @param dueStatusHObj the dueStatusH to set
   */
  public final void setDueStatusH(final DueStatus dueStatusHObj) {
    this.dueStatusH = dueStatusHObj;
  }

  /**
   * @return the isEOR
   */
  public final Boolean getIsEOR() {
    return isEOR;
  }

  /**
   * @param eOR to set
   */
  public final void setIsEOR(final Boolean eOR) {
    this.isEOR = eOR;
  }

  /**
   * Gets hydrated asset type dto.
   *
   * @return the assetTypeHDto
   */
  public final AssetTypeHDto getAssetTypeHDto() {
    return assetTypeHDto;
  }

  /**
   * Sets hydrated asset type dto.
   *
   * @param assetTypeHObj the assetTypeHDto to set
   */
  public final void setAssetTypeHDto(final AssetTypeHDto assetTypeHObj) {
    this.assetTypeHDto = assetTypeHObj;
  }
}
