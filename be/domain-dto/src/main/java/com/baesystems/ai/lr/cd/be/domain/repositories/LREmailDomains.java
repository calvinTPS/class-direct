package com.baesystems.ai.lr.cd.be.domain.repositories;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * DO for table LR_EMAIL_DOMAINS.
 *
 * @author fwijaya on 28/4/2017.
 */
@Entity
@Table(name = "LR_EMAIL_DOMAINS")
public class LREmailDomains implements Serializable {
  /**
   * Domain name length.
   */
  private static final int DOMAIN_NAME_LENGTH = 253;
  /**
   * Serial version unique id.
   */
  private static final long serialVersionUID = 8933211369127108219L;
  /**
   * Primary key.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;
  /**
   * Domain name.
   */
  @Column(name = "DOMAIN", length = DOMAIN_NAME_LENGTH)
  private String domain;

  /**
   * Returns the id.
   *
   * @return The id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param theId the Id.
   */
  public final void setId(final Long theId) {
    this.id = theId;
  }

  /**
   * Returns the domain name.
   *
   * @return The domain name.
   */
  public final String getDomain() {
    return domain;
  }

  /**
   * Sets the domain name.
   *
   * @param domainArgument The domain name.
   */
  public final void setDomain(final String domainArgument) {
    this.domain = domainArgument;
  }
}
