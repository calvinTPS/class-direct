package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;

/**
 * @author RKaneysan
 */
public class RegistryInformationDto implements Serializable {

  /**
   * Default serial version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Port Of Registry.
   */
  private String portOfRegistry;

  /**
   * Call Sign.
   */
  private String callSign;

  /**
   * Official Number.
   */
  private String officialNumber;

  /**
   * Asset Type Details.
   */
  private String assetTypeDetails;

  /**
   * Former Asset Names.
   */
  private String formerAssetNames;

  /**
   * Builder.
   */
  private String builder;

  /**
   * Yard.
   */
  private String yard;

  /**
   * Yard Number.
   */
  private String yardNumber;

  /**
   * Keel Laying Date.
   */
  private String keelLayingDate;

  /**
   * Date Of Build.
   */
  private String dateOfBuild;

  /**
   * Year Of Build.
   */
  private String yearOfBuild;

  /**
   * Country Of Build.
   */
  private String countryOfBuild;

  /**
   * Flag.
   */
  private String flag;

  /**
   * Maritime Mobile Security Identity (MMSI) Number.
   */
  private String mmsiNumber;
  /**
   * asset life cycle status.
   */
  private String assetLifeCycleStatus;
  /**
   * lead.
   */
  private Boolean lead;

  /**
   * @return the portOfRegistry
   */
  public final String getPortOfRegistry() {
    return portOfRegistry;
  }

  /**
   * @param portOfRegistryParam the portOfRegistry to set.
   */
  public final void setPortOfRegistry(final String portOfRegistryParam) {
    this.portOfRegistry = portOfRegistryParam;
  }

  /**
   * @return the callSign.
   */
  public final String getCallSign() {
    return callSign;
  }

  /**
   * @param callSignParam the callSign to set.
   */
  public final void setCallSign(final String callSignParam) {
    this.callSign = callSignParam;
  }

  /**
   * @return the officialNumber.
   */
  public final String getOfficialNumber() {
    return officialNumber;
  }

  /**
   * @param officialNumberParam the officialNumber to set.
   */
  public final void setOfficialNumber(final String officialNumberParam) {
    this.officialNumber = officialNumberParam;
  }

  /**
   * @return the assetTypeDetails.
   */
  public final String getAssetTypeDetails() {
    return assetTypeDetails;
  }

  /**
   * @param assetTypeDetailsParam the assetTypeDetails to set.
   */
  public final void setAssetTypeDetails(final String assetTypeDetailsParam) {
    this.assetTypeDetails = assetTypeDetailsParam;
  }

  /**
   * @return the formerAssetNames.
   */
  public final String getFormerAssetNames() {
    return formerAssetNames;
  }

  /**
   * @param formerAssetNamesParam the formerAssetNames to set.
   */
  public final void setFormerAssetNames(final String formerAssetNamesParam) {
    this.formerAssetNames = formerAssetNamesParam;
  }

  /**
   * @return the builder.
   */
  public final String getBuilder() {
    return builder;
  }

  /**
   * @param builderParam the builder to set.
   */
  public final void setBuilder(final String builderParam) {
    this.builder = builderParam;
  }

  /**
   * @return the yard.
   */
  public final String getYard() {
    return yard;
  }

  /**
   * @param yardParam the yard to set.
   */
  public final void setYard(final String yardParam) {
    this.yard = yardParam;
  }

  /**
   * @return the yardNumber.
   */
  public final String getYardNumber() {
    return yardNumber;
  }

  /**
   * @param yardNumberParam the yardNumber to set.
   */
  public final void setYardNumber(final String yardNumberParam) {
    this.yardNumber = yardNumberParam;
  }

  /**
   * @return the keelLayingDate.
   */
  public final String getKeelLayingDate() {
    return keelLayingDate;
  }

  /**
   * @param keelLayingDateParam the keelLayingDate to set.
   */
  public final void setKeelLayingDate(final String keelLayingDateParam) {
    this.keelLayingDate = keelLayingDateParam;
  }

  /**
   * @return the dateOfBuild.
   */
  public final String getDateOfBuild() {
    return dateOfBuild;
  }

  /**
   * @param dateOfBuildParam the dateOfBuild to set.
   */
  public final void setDateOfBuild(final String dateOfBuildParam) {
    this.dateOfBuild = dateOfBuildParam;
  }

  /**
   * @return the yearOfBuild.
   */
  public final String getYearOfBuild() {
    return yearOfBuild;
  }

  /**
   * @param yearOfBuildParam the yearOfBuild to set.
   */
  public final void setYearOfBuild(final String yearOfBuildParam) {
    this.yearOfBuild = yearOfBuildParam;
  }

  /**
   * @return the countryOfBuild.
   */
  public final String getCountryOfBuild() {
    return countryOfBuild;
  }

  /**
   * @param countryOfBuildParam the countryOfBuild to set.
   */
  public final void setCountryOfBuild(final String countryOfBuildParam) {
    this.countryOfBuild = countryOfBuildParam;
  }

  /**
   * @return the flag.
   */
  public final String getFlag() {
    return flag;
  }

  /**
   * @param flagParam the flag to set.
   */
  public final void setFlag(final String flagParam) {
    this.flag = flagParam;
  }

  /**
   * @return the mmsiNumber.
   */
  public final String getMmsiNumber() {
    return mmsiNumber;
  }

  /**
   * @param mmsiNumberParam the mmsiNumber to set.
   */
  public final void setMmsiNumber(final String mmsiNumberParam) {
    this.mmsiNumber = mmsiNumberParam;
  }

  /**
   * @return the assetLifeCycleStatus.
   */
  public final String getAssetLifeCycleStatus() {
    return assetLifeCycleStatus;
  }

  /**
   * @param assetLifeCycleStatusObj the assetLifeCycleStatus to set.
   */
  public final void setAssetLifeCycleStatus(final String assetLifeCycleStatusObj) {
    this.assetLifeCycleStatus = assetLifeCycleStatusObj;
  }

  /**
   * @return the lead.
   */
  public final Boolean getLead() {
    return lead;
  }

  /**
   * @param leadObj the lead to set.
   */
  public final void setLead(final Boolean leadObj) {
    this.lead = leadObj;
  }

}
