package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;
import java.util.List;
import com.baesystems.ai.lr.cd.be.domain.dto.export.RepeatOfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author RKaneysan
 */
public class CoCServiceRangeHDto implements Serializable {

  /**
   * Default serial version Unique ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Service Schedule Higher Range Date.
   */
  @Getter
  @Setter
  private Long highRange;

  /**
   * Service Schedule Lower Range Date.
   */
  @Getter
  @Setter
  private Long lowRange;

  /**
   * List of CoC Hydrated Dto.
   */
  @Getter
  @Setter
  private List<CoCHDto> cocHDtos;

  /**
   * List of Actionable Item Hydrated Dto.
   */
  @Getter
  @Setter
  private List<ActionableItemHDto> actionableItemHDtos;

  /**
   * List of Statutory Finding Hydrated Dto.
   */
  @Getter
  @Setter
  private List<StatutoryFindingHDto> statutoryFindingHDtos;

  /**
   * List of services.
   */
  @Getter
  @Setter
  private List<ScheduledServiceHDto> services;

  /**
   * List of future due date calculated from above services.
   */
  @Getter
  @Setter
  private List<RepeatOfDto> repeatedServices;
}
