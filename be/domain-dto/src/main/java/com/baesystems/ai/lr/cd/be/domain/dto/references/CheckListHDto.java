package com.baesystems.ai.lr.cd.be.domain.dto.references;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.references.CheckListDto;
import com.baesystems.ai.lr.dto.references.ChecklistGroupDto;
import com.baesystems.ai.lr.dto.references.ChecklistSubgroupDto;

import lombok.Getter;
import lombok.Setter;


/**
 * @author sbollu
 *
 */
public class CheckListHDto extends CheckListDto {

  /**
   * The generated serial version of UID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * hydrated checklist group {@link ChecklistGroupDto}.
   *
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "checklistGroup")
  private ChecklistGroupDto groupDto;

  /**
   * hydrated checklist subgroup {@link ChecklistSubgroupDto}.
   *
   */
  @Getter
  @Setter
  @LinkedResource(referencedField = "checklistSubgroup")
  private ChecklistSubgroupDto subGroupDto;
}
