package com.baesystems.ai.lr.cd.be.domain.repositories;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author syalavarthi
 *
 */
@Entity
@Table(name = "SUB_FLEET")
public class SubFleet implements Serializable {

  /**
   * serialVersionUID.
   */
  private static final long serialVersionUID = -4096895044536847130L;

  /**
  *
  */
  @Column(name = "ASSET_ID", nullable = false)
  @Id
  private Long assetId;

  /**
   *
   */
  @ManyToOne
  @JoinColumn(name = "USER_ID", nullable = false)
  @EmbeddedId
  private UserProfiles user;


  /**
   * @return value.
   */
  public final Long getAssetId() {
    return assetId;
  }

  /**
   * @param assetIdObject value.
   */
  public final void setAssetId(final Long assetIdObject) {
    this.assetId = assetIdObject;
  }

  /**
   * @return value.
   */
  public final UserProfiles getUser() {
    return user;
  }

  /**
   * @param userObject value.
   */
  public final void setUser(final UserProfiles userObject) {
    this.user = userObject;
  }
}
