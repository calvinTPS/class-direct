package com.baesystems.ai.lr.cd.be.domain.dto.attachments;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * @author syalavarthi.
 *
 */
public class AttachmentCategoryHDto extends ReferenceDataDto {
  /**
   * static serialVersionUID.
   */
  private static final long serialVersionUID = -8172243136245658286L;

}
