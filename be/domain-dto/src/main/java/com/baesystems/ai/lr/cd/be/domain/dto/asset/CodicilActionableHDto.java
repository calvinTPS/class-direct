package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.util.List;

/**
 * @author VKolagutla
 *
 */
public class CodicilActionableHDto {

  /**
   *
   */
  private List<CoCHDto> cocHDtos;

  /**
   *
   */
  private List<ActionableItemHDto> actionableItemHDtos;

  /**
   *
   */
  private List<StatutoryFindingHDto> statutoryFindingHDtos;

  /**
   * @return the list of statutory finading dto
   */
  public final List<StatutoryFindingHDto> getStatutoryFindingHDtos() {
    return statutoryFindingHDtos;
  }

  /**
   * @param statutoryFindingHDtoList The staturory findings to set
   */
  public final void setStatutoryFindingHDtos(final List<StatutoryFindingHDto> statutoryFindingHDtoList) {
    this.statutoryFindingHDtos = statutoryFindingHDtoList;
  }

  /**
   * @return the cocHDtos
   */
  public final List<CoCHDto> getCocHDtos() {
    return cocHDtos;
  }

  /**
   * @param cocHDtosObj the cocHDtos to set
   */
  public final void setCocHDtos(final List<CoCHDto> cocHDtosObj) {
    this.cocHDtos = cocHDtosObj;
  }

  /**
   * @return the actionableItemHDtos
   */
  public final List<ActionableItemHDto> getActionableItemHDtos() {
    return actionableItemHDtos;
  }

  /**
   * @param actionableItemHDtosObj the actionableItemHDtos to set
   */
  public final void setActionableItemHDtos(final List<ActionableItemHDto> actionableItemHDtosObj) {
    this.actionableItemHDtos = actionableItemHDtosObj;
  }


}
