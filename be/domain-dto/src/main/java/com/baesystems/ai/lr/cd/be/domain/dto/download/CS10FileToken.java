package com.baesystems.ai.lr.cd.be.domain.dto.download;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * CS10 file token.
 *
 * @author Faizal Sidek
 */
public class CS10FileToken implements Serializable {

  /**
   * Serial UUID.
   */
  private static final long serialVersionUID = 3553106556923147681L;

  /**
   * Node id.
   */
  private Integer nodeId;

  /**
   * Version number.
   */
  private Integer versionNumber;

  /**
   * Flag for zipped pdf format.
   */
  @Getter
  @Setter
  private Boolean zipFormat = Boolean.FALSE;

  /**
   * Getter for {@link #nodeId}.
   *
   * @return value of nodeId.
   */
  public final Integer getNodeId() {
    return nodeId;
  }

  /**
   * Setter for {@link #nodeId}.
   *
   * @param id value to be set.
   */
  public final void setNodeId(final Integer id) {
    this.nodeId = id;
  }

  /**
   * Getter for {@link #versionNumber}.
   *
   * @return value of versionNumber.
   */
  public final Integer getVersionNumber() {
    return versionNumber;
  }

  /**
   * Setter for {@link #versionNumber}.
   *
   * @param number value to be set.
   */
  public final void setVersionNumber(final Integer number) {
    this.versionNumber = number;
  }
}
