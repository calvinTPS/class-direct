package com.baesystems.ai.lr.cd.be.domain.dto.user;

/**
 * @author syalavarthi.
 *
 */
public class PaginationValidationDto {
  /**
   * page.
   */
  private Integer page;
  /**
   * size.
   */
  private Integer size;

  /**
   * @return the page value.
   */
  public final Integer getPage() {
    return page;
  }

  /**
   * @param pageObject to set.
   */
  public final void setPage(final Integer pageObject) {
    this.page = pageObject;
  }

  /**
   * @return the size value.
   */
  public final Integer getSize() {
    return size;
  }

  /**
   * @param sizeObject to set.size
   */
  public final void setSize(final Integer sizeObject) {
    this.size = sizeObject;
  }

}
