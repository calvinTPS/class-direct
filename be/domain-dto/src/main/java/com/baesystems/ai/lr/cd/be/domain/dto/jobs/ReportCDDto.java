package com.baesystems.ai.lr.cd.be.domain.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;

/**
 * @author syalavarthi
 *
 */
public class ReportCDDto {
  /**
   * report id.
   */
  private Long id;
  /**
   * reportType.
   */
  private String reportType;
  /**
   * reportVersion.
   */
  private Long reportVersion;
  /**
   * List of SupplementaryInformation.
   *
   */
  private List<SupplementaryInformationHDto> attachments;

  /**
   * job id.
   */
  private Long jobId;

  /**
   * Getter for {@link #content}.
   * @return attachments.
   */
  public final List<SupplementaryInformationHDto> getAttachments() {
    return attachments;
  }

  /**
   * @param attachmentsObj attachments.
   * Setter for {@link #content}.
   */
  public final void setAttachments(final List<SupplementaryInformationHDto> attachmentsObj) {
    this.attachments = attachmentsObj;
  }

  /**
   * Getter for job.
   *
   * @return job id.
   */
  public final Long getJobId() {
    return jobId;
  }

  /**
   * Setter for job.
   *
   * @param jobIdArg job id.
   */
  public final void setJobId(final Long jobIdArg) {
    this.jobId = jobIdArg;
  }

  /**
   * @return id.
   */
  public final Long getId() {
    return id;
  }

  /**
   * @param idObj id.
   */
  public final void setId(final Long idObj) {
    this.id = idObj;
  }

  /**
   * @return reportType.
   */
  public final String getReportType() {
    return reportType;
  }

  /**
   * @param reportTypeObj reportType.
   */
  public final void setReportType(final String reportTypeObj) {
    this.reportType = reportTypeObj;
  }


  /**
   * @return reportVersion.
   */
  public final Long getReportVersion() {
    return reportVersion;
  }

  /**
   * @param reportVersionObj reportVersion.
   */
  public final void setReportVersion(final Long reportVersionObj) {
    this.reportVersion = reportVersionObj;
  }

}
