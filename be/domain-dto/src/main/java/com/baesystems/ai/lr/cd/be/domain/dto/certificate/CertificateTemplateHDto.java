package com.baesystems.ai.lr.cd.be.domain.dto.certificate;

import com.baesystems.ai.lr.cd.be.domain.annotation.LinkedResource;
import com.baesystems.ai.lr.dto.references.CertificateTemplateDto;

/**
 * Holds info about a certificate template with hydrated product catalogue.
 */
public class CertificateTemplateHDto extends CertificateTemplateDto {

  /**
   * Hydrated product catalogue of the certificate template.
   */
  @LinkedResource(referencedField = "productCatalogue")
  private ProductCatalogueHDto productCatalogueH;

  /**
   * @return the productCatalogueH
   */
  public final ProductCatalogueHDto getProductCatalogueH() {
    return productCatalogueH;
  }

  /**
   * @param productCatalogueHArg to set
   */
  public final void setProductCatalogueH(final ProductCatalogueHDto productCatalogueHArg) {
    this.productCatalogueH = productCatalogueHArg;
  }

}
