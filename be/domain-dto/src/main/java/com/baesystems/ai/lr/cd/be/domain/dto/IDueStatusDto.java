package com.baesystems.ai.lr.cd.be.domain.dto;

import com.baesystems.ai.lr.cd.be.enums.DueStatus;

/**
 * @author VKolagutla
 *
 */
public interface IDueStatusDto {

  /**
   * Returns the due status for a given DTO.
   * @return dueStatus the due status value.
   */
  DueStatus getDueStatusH();

  /**
   * Sets due status for a given DTO.
   * @param dueStatusHObj the due status value.
   */
  void setDueStatusH(final DueStatus dueStatusHObj);

}
