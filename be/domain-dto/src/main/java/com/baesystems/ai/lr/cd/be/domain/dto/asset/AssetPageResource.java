package com.baesystems.ai.lr.cd.be.domain.dto.asset;

import java.io.Serializable;
import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

/**
 * Page resource for {@link AssetHDto}.
 *
 * @author msidek
 *
 */
public class AssetPageResource extends BasePageResource<AssetHDto> implements Serializable {

  /**
   * Auto generated serial ID.
   */
  private static final long serialVersionUID = -3691599102967374529L;

  /**
   * List of assets.
   *
   */
  private List<AssetHDto> content;

  /**
   * Getter for {@link #content}.
   *
   */
  @Override
  public final List<AssetHDto> getContent() {
    return content;
  }

  /**
   * Setter for {@link #content}.
   *
   */
  @Override
  public final void setContent(final List<AssetHDto> assetList) {
    this.content = assetList;
  }

}
