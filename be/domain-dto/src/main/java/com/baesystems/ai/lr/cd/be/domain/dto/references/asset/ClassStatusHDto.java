package com.baesystems.ai.lr.cd.be.domain.dto.references.asset;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

/**
 * Class status HDTO.
 *
 * @author msidek
 *
 */
public class ClassStatusHDto extends ReferenceDataDto {
  /**
   * Generated UID.
   *
   */
  private static final long serialVersionUID = -4536617520811064805L;

}
