package com.baesystems.ai.lr.cd.be.domain.dto.export;

import lombok.Getter;
import lombok.Setter;

/**
 * ESP Report Export DTO.
 *
 * @author Jonathan Lin
 *
 */
public class ESPReportExportDto {

  /**
   * Job ID.
   */
  @Getter
  @Setter
  private long jobId;

}
