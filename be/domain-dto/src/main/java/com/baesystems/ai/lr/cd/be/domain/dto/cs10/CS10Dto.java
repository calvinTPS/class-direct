package com.baesystems.ai.lr.cd.be.domain.dto.cs10;

/**
 * CS10 Dto.
 *
 * @author yng
 *
 */
public class CS10Dto {
  /**
   * cs10 file id.
   */
  private String fileId;

  /**
   * cs10 file version.
   */
  private String version;

  /**
   * Getter for file id.
   *
   * @return file id.
   */
  public final String getFileId() {
    return fileId;
  }

  /**
   * Setter for file id.
   *
   * @param fileIdParam value.
   */
  public final void setFileId(final String fileIdParam) {
    this.fileId = fileIdParam;
  }

  /**
   * Getter for file version.
   *
   * @return file version.
   */
  public final String getVersion() {
    return version;
  }

  /**
   * Setter for file version.
   *
   * @param versionParam value.
   */
  public final void setVersion(final String versionParam) {
    this.version = versionParam;
  }
}
