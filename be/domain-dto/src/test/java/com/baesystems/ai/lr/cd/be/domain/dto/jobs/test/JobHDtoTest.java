package com.baesystems.ai.lr.cd.be.domain.dto.jobs.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.JobStatusHDto;

/**
 * Unit Test for {@link JobHDto}.
 *
 * @author yng
 *
 */
public class JobHDtoTest {
  /**
   * Constant job id.
   */
  private static final Long JOB_ID = 1234L;

  /**
   * Unit test for {@link JobHDto}.
   *
   */
  @Test
  public final void jobHDtoTest() {
    final JobHDto jobHDto = new JobHDto();
    jobHDto.setId(JOB_ID);
    Assert.assertNotNull(jobHDto);

    jobHDto.setJobStatusDto(new JobStatusHDto());
    Assert.assertNotNull(jobHDto.getJobStatusDto());
  }
}
