package com.baesystems.ai.lr.cd.be.domain;

/**
 * @author Faizal Sidek
 */
public enum UserId {
  /**
   * Reference to Alice user.
   */
  ALICE("86d2c161-cc9f-458b-b10d-7476166affa6");

  /**
   * Id of the user.
   */
  private String id;

  /**
   * Private constructor.
   *
   * @param userId id of user.
   */
  UserId(final String userId) {
    this.id = userId;
  }

  /**
   * Getter for {@link #id}.
   *
   * @return id.
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for {@link #id}.
   *
   * @param userId id of the user.
   */
  public void setId(final String userId) {
    this.id = userId;
  }
}
