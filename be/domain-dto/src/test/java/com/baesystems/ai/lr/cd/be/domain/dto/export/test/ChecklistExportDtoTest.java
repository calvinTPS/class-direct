package com.baesystems.ai.lr.cd.be.domain.dto.export.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ChecklistExportDto;


/**
 * Unit Test for {@link ChecklistExportDto}.
 *
 * @author yng
 *
 */
public class ChecklistExportDtoTest {

  /**
   * Unit test for {@link ChecklistExportDto}.
   *
   */
  @Test
  public final void checklistExportDtoTest() {
    final ChecklistExportDto dto = new ChecklistExportDto();

    dto.setAssetCode("LRV1");
    dto.setServiceId(1L);

    Assert.assertEquals("LRV1", dto.getAssetCode());
    Assert.assertEquals(Long.valueOf(1L), dto.getServiceId());
  }
}
