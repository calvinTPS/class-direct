package com.baesystems.ai.lr.cd.be.domain.dto.export.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ServiceScheduleDto;


/**
 * Unit Test for {@link ServiceScheduleDto}.
 *
 * @author yng
 *
 */
public class ServiceScheduleDtoTest {
  /**
   * Unit test for {@link ServiceScheduleDto}.
   *
   */
  @Test
  public final void testServiceScheduleDto() {
    final ServiceScheduleDto dto = new ServiceScheduleDto();
    dto.setRepeatedServices(new ArrayList<>());
    dto.setServices(new ArrayList<>());

    Assert.assertNotNull(dto);
    Assert.assertNotNull(dto.getRepeatedServices());
    Assert.assertNotNull(dto.getServices());
  }
}
