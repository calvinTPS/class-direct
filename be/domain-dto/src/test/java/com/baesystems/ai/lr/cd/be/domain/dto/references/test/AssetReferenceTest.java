package com.baesystems.ai.lr.cd.be.domain.dto.references.test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Combined unit test.
 * {@link com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto},
 * {@link AssetCategoryHDto} and {@link AssetTypeHDto}
 *
 * @author msidek
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class AssetReferenceTest {
  /**
   * asset type id constant.
   *
   */
  private static final Long ASSET_TYPE_ID = 1234L;

  /**
   * Unit test for {@link AssetTypeHDto}.
   *
   * @throws IOException when mapper error.
   *
   */
  @Test
  public final void assetTypeTest() throws IOException {
    AssetTypeHDto assetType = new AssetTypeHDto();
    Assert.assertNotNull(assetType);

    assetType.setCategoryDto(new AssetCategoryHDto());
    Assert.assertNotNull(assetType.getCategoryDto());
    Assert.assertTrue(assetType.getCategoryDto() instanceof AssetCategoryHDto);
    Assert.assertNotNull(assetType.toString());
    Assert.assertFalse(assetType.toString().isEmpty());

    assetType.setId(ASSET_TYPE_ID);
    ObjectMapper mapper = new ObjectMapper();
    String assetTypeJson = mapper.writeValueAsString(assetType);
    Assert.assertNotNull(assetTypeJson);
    Assert.assertFalse(assetTypeJson.isEmpty());

    AssetTypeHDto fromJson = mapper.readValue(assetTypeJson, AssetTypeHDto.class);
    Assert.assertNotNull(fromJson);
    Assert.assertEquals(ASSET_TYPE_ID, fromJson.getId());
  }
}
