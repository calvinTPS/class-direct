package com.baesystems.ai.lr.cd.be.domain.dto.asset.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNotePageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CoCPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilPageResource;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.DefectHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.LazyItemHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.RepairHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.JobHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.CodicilCategoryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassMaintenanceStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassificationSocietyHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.CodicilStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.DefectStatusHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.RepairActionHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.flag.FlagStateHDto;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Combined unit test for {@link AssetHDto}, {@link AssetPageResource}.
 *
 * @author msidek
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class AssetDtoTest {
  /**
   * Constant asset id.
   *
   */
  private static final Long ASSET_ID = 1234L;

  /**
   * Constant coc id.
   */
  private static final Long COC_ID = 1234L;

  /**
   * Constant actionable item id.
   */
  private static final Long ACTIONABLE_ITEM_ID = 1234L;

  /**
   * Constant asset note id.
   */
  private static final Long ASSET_NOTE_ID = 1234L;

  /**
   * Unit test for {@link AssetHDto}.
   *
   * @throws IOException when mapper error.
   *
   */
  @Test
  public final void assetHDtoTest() throws IOException {
    final AssetHDto assetDto = new AssetHDto();
    assetDto.setId(ASSET_ID);
    Assert.assertNotNull(assetDto);
    Assert.assertNotNull(assetDto.toString());
    Assert.assertFalse(assetDto.toString().isEmpty());

    assetDto.setAssetCategoryDto(new AssetCategoryHDto());
    Assert.assertNotNull(assetDto.getAssetCategoryDto());
    Assert.assertTrue(assetDto.getAssetCategoryDto() instanceof AssetCategoryHDto);

    // assetDto.setAssetTypeDto(new AssetTypeDto());
    // Assert.assertNotNull(assetDto.getAssetTypeDto());
    // Assert.assertTrue(assetDto.getAssetTypeDto() instanceof AssetTypeHDto);

    assetDto.setClassStatusDto(new ClassStatusHDto());
    Assert.assertNotNull(assetDto.getClassStatusDto());
    Assert.assertTrue(assetDto.getClassStatusDto() instanceof ClassStatusHDto);

    assetDto.setFlagStateDto(new FlagStateHDto());
    Assert.assertNotNull(assetDto.getFlagStateDto());
    Assert.assertTrue(assetDto.getFlagStateDto() instanceof FlagStateHDto);

    final ObjectMapper mapper = new ObjectMapper();
    final String assetDtoJson = mapper.writeValueAsString(assetDto);
    Assert.assertNotNull(assetDtoJson);
    Assert.assertFalse(assetDtoJson.isEmpty());

    final AssetHDto assetDtoFromJson = mapper.readValue(assetDtoJson, AssetHDto.class);
    Assert.assertNotNull(assetDtoFromJson);
    Assert.assertEquals(ASSET_ID, assetDtoFromJson.getId());

    assetDto.setClassMaintenanceStatusDto(new ClassMaintenanceStatusHDto());
    Assert.assertNotNull(assetDto.getClassMaintenanceStatusDto());
    Assert.assertTrue(assetDto.getClassMaintenanceStatusDto() instanceof ClassMaintenanceStatusHDto);

    assetDto.setCoClassificationSocietyDto(new ClassificationSocietyHDto());
    Assert.assertNotNull(assetDto.getCoClassificationSocietyDto());
    Assert.assertTrue(assetDto.getCoClassificationSocietyDto() instanceof ClassificationSocietyHDto);
  }

  /**
   * Unit test for {@link AssetPageResource}.
   *
   * @throws IOException when mapper error.
   *
   */
  @Test
  public final void assetPageResourceTest() throws IOException {
    final AssetPageResource resource = new AssetPageResource();
    Assert.assertNotNull(resource);

    resource.setContent(mockAssets());
    Assert.assertNotNull(resource.getContent());
    Assert.assertFalse(resource.getContent().isEmpty());
    Assert.assertTrue(resource.getContent() instanceof List);

    resource.setPagination(new PaginationDto());
    Assert.assertNotNull(resource.getPagination());
    Assert.assertTrue(resource.getPagination() instanceof PaginationDto);

    final ObjectMapper mapper = new ObjectMapper();
    final String resourceJson = mapper.writeValueAsString(resource);
    Assert.assertNotNull(resourceJson);
    Assert.assertFalse(resourceJson.isEmpty());

    final AssetPageResource resourceFromJson = mapper.readValue(resourceJson, AssetPageResource.class);
    Assert.assertNotNull(resourceFromJson);
    Assert.assertFalse(resourceFromJson.getContent().isEmpty());
    Assert.assertEquals(ASSET_ID, resourceFromJson.getContent().get(0).getId());
  }

  /**
   * Unit test for {@link AssetQueryHDto}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void assetQueryHDtoTest() throws Exception {
    final AssetQueryHDto dto = new AssetQueryHDto();
    Assert.assertNotNull(dto);

    dto.setIsFavourite(true);
    Assert.assertTrue(dto.getIsFavourite());
  }

  /**
   * Unit test for {@link CoCPageResource}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void cocPageResourceHDtoTest() throws Exception {
    final CoCPageResource resource = new CoCPageResource();
    Assert.assertNotNull(resource);

    resource.setContent(mockCoCHDto());
    Assert.assertEquals(COC_ID, resource.getContent().get(0).getId());
  }

  /**
   * Unit test for {@link actionableItemPageResourceHDto}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void actionableItemPageResourceHDtoTest() throws Exception {
    final ActionableItemPageResource resource = new ActionableItemPageResource();
    Assert.assertNotNull(resource);

    resource.setContent(mockActionableItemHDto());
    Assert.assertEquals(ACTIONABLE_ITEM_ID, resource.getContent().get(0).getId());
  }

  /**
   * Unit test for {@link AssetNotePageResource}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void assetNotePageResourceHDtoTest() throws Exception {
    final AssetNotePageResource resource = new AssetNotePageResource();
    Assert.assertNotNull(resource);

    resource.setContent(mockAssetNoteHDto());
    Assert.assertEquals(ASSET_NOTE_ID, resource.getContent().get(0).getId());
  }

  /**
   * Unit test for {@link CoCHDto}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void coCHDtoTest() throws Exception {
    final CoCHDto resource = new CoCHDto();

    resource.setCategoryH(mockCoCHDto().get(0).getCategoryH());
    Assert.assertTrue(resource.getCategoryH() instanceof CodicilCategoryHDto);
    Assert.assertNotNull(resource.getCategoryH());

    resource.setAssetItemH(new LazyItemHDto());
    Assert.assertNotNull(resource.getAssetItemH());

    resource.setDefectH(new DefectHDto());
    Assert.assertNotNull(resource.getDefectH());

    resource.setRepairH(new ArrayList<RepairHDto>() {
      {
        add(new RepairHDto());
      }
    });
    Assert.assertNotNull(resource.getRepairH());

    resource.setStatusH(new CodicilStatusHDto());
    Assert.assertNotNull(resource.getStatusH());

    Assert.assertEquals("COC", resource.getCodicilType());
  }

  /**
   * Unit test for {@link ActionableItemHDto}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void actionableItemHDtoTest() throws Exception {
    final ActionableItemHDto resource = new ActionableItemHDto();

    resource.setCategoryH(mockActionableItemHDto().get(0).getCategoryH());
    Assert.assertTrue(resource.getCategoryH() instanceof CodicilCategoryHDto);
    Assert.assertNotNull(resource.getCategoryH());

    resource.setAssetItemH(new LazyItemHDto());
    Assert.assertNotNull(resource.getAssetItemH());

    resource.setStatusH(new CodicilStatusHDto());
    Assert.assertNotNull(resource.getStatusH());

    Assert.assertEquals("AI", resource.getCodicilType());
  }

  /**
   * Unit test for {@link CoCHDto}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void assetNoteHDtoTest() throws Exception {
    final AssetNoteHDto resource = new AssetNoteHDto();

    resource.setCategoryH(mockAssetNoteHDto().get(0).getCategoryH());
    Assert.assertTrue(resource.getCategoryH() instanceof CodicilCategoryHDto);
    Assert.assertNotNull(resource.getCategoryH());

    resource.setAssetItemH(new LazyItemHDto());
    Assert.assertNotNull(resource.getAssetItemH());

    resource.setStatusH(new CodicilStatusHDto());
    Assert.assertNotNull(resource.getStatusH());

    Assert.assertEquals("AN", resource.getCodicilType());
  }

  /**
   * Unit test for {@link CodicilPageResource}.
   *
   * @throws Exception when error.
   *
   */
  @Test
  public final void codicilResourceHDtoTest() throws Exception {
    final CodicilPageResource resource = new CodicilPageResource();
    Assert.assertNotNull(resource);

    final List<Object> mockList = new ArrayList<Object>();
    mockList.add(mockCoCHDto().get(0));

    resource.setContent(mockList);
    Assert.assertTrue(resource.getContent() instanceof Object);
    Assert.assertNotNull(resource.getContent());
  }

  /**
   * Unit test for {@link CodicilActionableHDto}.
   *
   * @throws Exception when error.
   */
  @Test
  public final void codicilActionableHDtoTest() throws Exception {
    final CodicilActionableHDto resource = new CodicilActionableHDto();
    final List<ActionableItemHDto> actionableItemHDtos = new ArrayList<>();
    final ActionableItemHDto actionableItemHDto = new ActionableItemHDto();
    actionableItemHDtos.add(actionableItemHDto);

    final List<CoCHDto> chDtos = new ArrayList<>();
    final CoCHDto chDto = new CoCHDto();
    chDtos.add(chDto);

    resource.setActionableItemHDtos(actionableItemHDtos);
    resource.setCocHDtos(chDtos);

    Assert.assertNotNull(resource);
    Assert.assertNotNull(resource.getActionableItemHDtos());
    Assert.assertNotNull(resource.getCocHDtos());
  }

  /**
   * Unit test for {@link DefectHDto}.
   *
   */
  @Test
  public final void defectHDtoTest() {
    final DefectHDto resource = new DefectHDto();
    resource.setDefectStatusH(new DefectStatusHDto());
    Assert.assertNotNull(resource.getDefectStatusH());

    resource.setJobsH(new ArrayList<JobHDto>() {
      {
        new JobHDto();
      }
    });
    Assert.assertNotNull(resource.getJobsH());

    resource.setItemsH(new ArrayList<LazyItemHDto>() {
      {
        new LazyItemHDto();
      }
    });
    Assert.assertNotNull(resource.getItemsH());

    resource.setCocH(new ArrayList<CoCHDto>() {
      {
        new CoCHDto();
      }
    });
    Assert.assertNotNull(resource.getCocH());
  }

  /**
   * Unit test for {@link RepairHDto}.
   *
   */
  @Test
  public final void repairHDtoTest() {
    final RepairHDto resource = new RepairHDto();
    resource.setRepairActionH(new RepairActionHDto());
    Assert.assertNotNull(resource.getRepairActionH());

    resource.setRepairsH(new ArrayList<LazyItemHDto>() {
      {
        new LazyItemHDto();
      }
    });
    Assert.assertNotNull(resource.getRepairsH());
  }



  /**
   * Create assets.
   *
   * @return list of mocked assets.
   *
   */
  private List<AssetHDto> mockAssets() {
    final List<AssetHDto> assets = new ArrayList<>();

    final AssetHDto assetDto = new AssetHDto();
    assetDto.setId(ASSET_ID);
    assetDto.setAssetCategoryDto(new AssetCategoryHDto());
    assetDto.setAssetType(new AssetTypeDto());
    assetDto.setClassStatusDto(new ClassStatusHDto());
    assetDto.setFlagStateDto(new FlagStateHDto());

    assets.add(assetDto);

    return assets;
  }

  /**
   * Mock create coc.
   *
   * @return list of mocked coc
   *
   */
  private List<CoCHDto> mockCoCHDto() {
    final List<CoCHDto> cocs = new ArrayList<>();

    final CoCHDto cocHDto = new CoCHDto();
    cocHDto.setId(COC_ID);
    cocHDto.setTitle("Test CoC");
    cocHDto.setCategoryH(new CodicilCategoryHDto());

    cocs.add(cocHDto);
    return cocs;
  }

  /**
   * Mock create coc.
   *
   * @return list of mocked coc
   *
   */
  private List<ActionableItemHDto> mockActionableItemHDto() {
    final List<ActionableItemHDto> actionableItems = new ArrayList<>();

    final ActionableItemHDto actionableItemHDto = new ActionableItemHDto();
    actionableItemHDto.setId(ACTIONABLE_ITEM_ID);
    actionableItemHDto.setTitle("Test Actionable Item");
    actionableItemHDto.setCategoryH(new CodicilCategoryHDto());

    actionableItems.add(actionableItemHDto);
    return actionableItems;
  }

  /**
   * Mock create coc.
   *
   * @return list of mocked coc
   *
   */
  private List<AssetNoteHDto> mockAssetNoteHDto() {
    final List<AssetNoteHDto> assetNotes = new ArrayList<>();

    final AssetNoteHDto assetNoteHDto = new AssetNoteHDto();
    assetNoteHDto.setId(ASSET_NOTE_ID);
    assetNoteHDto.setTitle("Test Asset Note");
    assetNoteHDto.setCategoryH(new CodicilCategoryHDto());

    assetNotes.add(assetNoteHDto);
    return assetNotes;
  }
}
