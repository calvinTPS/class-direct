package com.baesystems.ai.lr.cd.be.domain.dto.user.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.user.UserDto;

/**
 * test class for UserDto.
 *
 * @author vmandalapu
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class UserDtoTest {

  /**
   * test method.
   */
  @Test
  public final void userDtoTest() {
    UserDto dto = new UserDto();
    Assert.assertNotNull(dto);
    dto.setEquasis(true);
    dto.setThetis(true);
    Assert.assertTrue(dto.isEquasis());
    Assert.assertTrue(dto.isThetis());

    dto.setEquasis(false);
    dto.setThetis(false);
    Assert.assertFalse(dto.isEquasis());
    Assert.assertFalse(dto.isThetis());

    dto.setEquasis(true);
    dto.setThetis(false);
    Assert.assertTrue(dto.isEquasis());
    Assert.assertFalse(dto.isThetis());

    dto.setEquasis(false);
    dto.setThetis(true);
    Assert.assertFalse(dto.isEquasis());
    Assert.assertTrue(dto.isThetis());
  }
}
