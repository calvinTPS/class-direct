package com.baesystems.ai.lr.cd.be.domain.dto.cs10.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.cs10.CS10Dto;


/**
 * Unit Test for {@link CS10Dto}.
 *
 * @author yng
 *
 */
public class CS10DtoTest {
  /**
   * Unit test for {@link CS10Dto}.
   *
   */
  @Test
  public final void cs10Test() {
    final CS10Dto dto = new CS10Dto();
    dto.setFileId("id");
    dto.setVersion("1");

    Assert.assertEquals("id", dto.getFileId());
    Assert.assertEquals("1", dto.getVersion());

  }
}
