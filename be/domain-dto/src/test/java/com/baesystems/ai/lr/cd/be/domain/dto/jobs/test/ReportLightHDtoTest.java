package com.baesystems.ai.lr.cd.be.domain.dto.jobs.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportLightHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.jobs.ReportTypeHDto;

/**
 * Unit Test for {@link ReportLightHDto}.
 *
 * @author yng
 *
 */
public class ReportLightHDtoTest {
  /**
   * Constant report id.
   */
  private static final Long REPORT_ID = 1234L;

  /**
   * Unit test for {@link ReportLightHDto}.
   *
   */
  @Test
  public final void reportLightHDtoTest() {
    final ReportLightHDto reportLightHDto = new ReportLightHDto();
    reportLightHDto.setId(REPORT_ID);
    Assert.assertNotNull(reportLightHDto);

    reportLightHDto.setReportTypeDto(new ReportTypeHDto());
    Assert.assertNotNull(reportLightHDto.getReportTypeDto());
  }
}
