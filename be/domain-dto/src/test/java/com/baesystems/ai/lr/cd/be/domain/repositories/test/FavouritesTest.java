package com.baesystems.ai.lr.cd.be.domain.repositories.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.repositories.Favourites;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;

import static com.baesystems.ai.lr.cd.be.domain.UserId.ALICE;

/**
 * Favourites Test Class.
 *
 * @author VMandalapu
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class FavouritesTest {

  /**
   * ClientCode constant variable.
   */
  private static final String CLIENT_CODE = "45";

  /**
   * FlagCode constant variable.
   */
  private static final String FLAG_CODE = "ALA";

  /**
   * ShipBuilder constant variable.
   */
  private static final String SHIP_BUILDER_CODE = "20";

  /**
   * AssetId constant variable.
   */
  private static final Long ASSET_ID = 45L;

  /**
   * Test method for Favourites.
   */
  @Test
  public final void favouritesTest() {
    Favourites favourites = new Favourites();
    Assert.assertNotNull(favourites);

    UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);

    userProfiles.setUserId(ALICE.getId());
    userProfiles.setClientCode(CLIENT_CODE);
    userProfiles.setFlagCode(FLAG_CODE);
    userProfiles.setShipBuilderCode(SHIP_BUILDER_CODE);

    Assert.assertNotNull(userProfiles.getUserId());
    Assert.assertNotNull(userProfiles.getClientCode());
    Assert.assertNotNull(userProfiles.getFlagCode());
    Assert.assertNotNull(userProfiles.getShipBuilderCode());

    Assert.assertEquals(ALICE.getId(), userProfiles.getUserId());
    Assert.assertEquals(CLIENT_CODE, userProfiles.getClientCode());
    Assert.assertEquals(FLAG_CODE, userProfiles.getFlagCode());
    Assert.assertEquals(SHIP_BUILDER_CODE, userProfiles.getShipBuilderCode());

    favourites.setUser(userProfiles);
    favourites.setAssetId(ASSET_ID);

    Assert.assertNotNull(favourites.getUser());
    Assert.assertNotNull(favourites.getUser().getUserId());
    Assert.assertNotNull(favourites.getUser().getClientCode());
    Assert.assertNotNull(favourites.getUser().getFlagCode());
    Assert.assertNotNull(favourites.getUser().getShipBuilderCode());
    Assert.assertNotNull(favourites.getAssetId());

    Assert.assertEquals(ASSET_ID, favourites.getAssetId());
  }

}
