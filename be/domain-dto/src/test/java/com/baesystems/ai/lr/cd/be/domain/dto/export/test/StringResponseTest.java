package com.baesystems.ai.lr.cd.be.domain.dto.export.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.StringResponse;



/**
 * Test for StringResponse class.
 *
 * @author yng
 *
 */
public class StringResponseTest {

  /**
   * Test String 1.
   */
  private static final String TEST_STRING_1 = "Hello World";

  /**
   * Test String 2.
   */
  private static final String TEST_STRING_2 = "World Hello";

  /**
   * File Name.
   */
  private static final String FILE_NAME = "fName.pdf";

  /**
   * Test StringResponse class.
   */
  @Test
  public final void testStringResponse() {

    final StringResponse object = new StringResponse(TEST_STRING_1);

    Assert.assertEquals(TEST_STRING_1, object.getResponse());

    object.setResponse(TEST_STRING_2);

    Assert.assertEquals(TEST_STRING_2, object.getResponse());

    final StringResponse object1 = new StringResponse(TEST_STRING_1, FILE_NAME);

    Assert.assertEquals(TEST_STRING_1, object1.getResponse());

    Assert.assertEquals(FILE_NAME, object1.getFileName());

  }
}
