package com.baesystems.ai.lr.cd.be.domain.repositories.test;

import static com.baesystems.ai.lr.cd.be.domain.UserId.ALICE;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.company.Company;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;

/**
 * Test class for UserProfiles Dao.
 *
 * @author VMandalapu
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class UserProfilesTest {

  /**
   * ClientCode constant variable.
   */
  private static final String CLIENT_CODE = "45";
  /**
   * FlagCode constant id variable.
   */
  private static final Long FLAG_ID = 1L;

  /**
   * FlagCode constant variable.
   */
  private static final String FLAG_CODE = "ALA";

  /**
   * ShipBuilder constant variable.
   */
  private static final String SHIP_BUILDER_CODE = "20";

  /**
   * ShipBuilder constant variable.
   */
  private static final Long ROLE_ID = 109L;

  /**
   * UserName variable.
   */
  private static final String USER_NAME = "ABCD";
  /**
   * FIRST_NAME variable.
   */
  private static final String FIRST_NAME = "Hello";
  /**
   * LAST_NAME variable.
   */
  private static final String LAST_NAME = "World";
  /**
   * UserName variable.
   */
  private static final String EMAIL = "ABCD@baesystems.com";
  /**
   * RESTRICTALL variable.
   */
  private static final Boolean RESTRICTALL = true;

  /**
   * Test method to UserProfiles.
   */
  @Test
  public final void userProfilesTest() {
    UserProfiles userProfiles = new UserProfiles();
    Assert.assertNotNull(userProfiles);
    Company company = new Company();
    company.setName("BAE");
    company.setAddressLine1("addressLine1");
    company.setAddressLine2("addressLine2");


    UserAccountStatus status = new UserAccountStatus();
    status.setId(FLAG_ID);
    status.setName("Active");

    Set<Roles> roles = new HashSet<>();
    Roles role = new Roles();
    role.setRoleId(ROLE_ID);
    role.setRoleName("LR_ADMIN");
    roles.add(role);

    userProfiles.setUserId(ALICE.getId());
    userProfiles.setClientCode(CLIENT_CODE);
    userProfiles.setFlagCode(FLAG_CODE);
    userProfiles.setShipBuilderCode(SHIP_BUILDER_CODE);
    userProfiles.setEmail(EMAIL);
    userProfiles.setStatus(status);
    userProfiles.setRestrictAll(RESTRICTALL);
    userProfiles.setRoles(roles);
    userProfiles.setCompany(company);
    userProfiles.setCompanyName(company.getName());
    userProfiles.setName(FIRST_NAME + " " + LAST_NAME);
    userProfiles.setFirstName(FIRST_NAME);
    userProfiles.setLastName(LAST_NAME);


    Assert.assertNotNull(userProfiles.getUserId());
    Assert.assertNotNull(userProfiles.getRestrictAll());
    Assert.assertNotNull(userProfiles.getStatus());
    Assert.assertNotNull(userProfiles.getRoles());

    Assert.assertEquals(ALICE.getId(), userProfiles.getUserId());
    Assert.assertEquals(CLIENT_CODE, userProfiles.getClientCode());
    Assert.assertEquals(FLAG_CODE, userProfiles.getFlagCode());
    Assert.assertEquals(SHIP_BUILDER_CODE, userProfiles.getShipBuilderCode());


    Assert.assertEquals(EMAIL, userProfiles.getEmail());
    Assert.assertEquals(status, userProfiles.getStatus());
    Assert.assertEquals(RESTRICTALL, userProfiles.getRestrictAll());
    Assert.assertEquals(roles, userProfiles.getRoles());
    Assert.assertEquals(FIRST_NAME, userProfiles.getFirstName());
    Assert.assertEquals(LAST_NAME, userProfiles.getLastName());
    Assert.assertEquals(company, userProfiles.getCompany());



    Assert.assertNotNull(userProfiles.toString());
  }

}
