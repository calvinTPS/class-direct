package com.baesystems.ai.lr.cd.be.domain.dto.export.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;


/**
 * Unit Test for {@link AssetExportDtoDto}.
 *
 * @author yng
 *
 */
public class AssetExportDtoTest {
  /**
   * Unit test for {@link AssetExportDto}.
   *
   */
  @Test
  public final void assetExportDtoTest() {
    final AssetExportDto dto = new AssetExportDto();

    final List<String> testList = new ArrayList<>();
    testList.add("LRV1");
    testList.add("LRV2");

    final List<Long> testList2 = new ArrayList<>();
    testList2.add(1L);

    dto.setIncludes(testList);
    dto.setExcludes(testList);
    dto.setInfo(1L);
    dto.setService(1L);
    dto.setCodicils(testList2);

    final AssetQueryHDto query = new AssetQueryHDto();
    query.setIsFavourite(true);

    dto.setAssetQuery(query);

    Assert.assertEquals(testList.size(), dto.getIncludes().size());
    Assert.assertEquals(testList.size(), dto.getExcludes().size());
    Assert.assertEquals(Long.valueOf(1L), dto.getInfo());
    Assert.assertEquals(Long.valueOf(1L), dto.getService());
    Assert.assertEquals(testList2.size(), dto.getCodicils().size());
    Assert.assertNotNull(dto.getFilter());
    Assert.assertEquals(true, dto.getFilter().getIsFavourite());
  }
}
