package com.baesystems.ai.lr.cd.be.domain.dto.jobs.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.jobs.ReportHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.jobs.SurveyReportsPageResource;

/**
 * Unit Test for {@link SurveyReportsPageResource}.
 *
 * @author yng
 *
 */
public class SurveyReportsPageResourceTest {
  /**
   * Constant report id.
   */
  private static final Long REPORT_ID = 1234L;

  /**
   * Unit test for {@link SurveyReportsPageResource}.
   *
   */
  @Test
  public final void surveyReportsPageResourceTest() {
    final SurveyReportsPageResource resource = new SurveyReportsPageResource();
    Assert.assertNotNull(resource);

    resource.setContent(mockReportLightHDto());
    Assert.assertNotNull(resource.getContent());
    Assert.assertNotNull(resource.getContent().get(0));
  }

  /**
   * Mock report list.
   *
   * @return list of mocked report.
   */
  private List<ReportHDto> mockReportLightHDto() {
    final List<ReportHDto> reports = new ArrayList<>();

    final ReportHDto reportHDto = new ReportHDto();
    reportHDto.setId(REPORT_ID);
    reports.add(reportHDto);
    return reports;
  }
}
