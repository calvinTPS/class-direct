package com.baesystems.ai.lr.cd.be.domain.dto.security.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.security.MasqueradeUserDto;

/**
 * Masquerade user dto test.
 *
 * @author yng
 *
 */
public class MasqueradeUserDtoTest {

  /**
   * Test dto.
   */
  @Test
  public final void testMasqueradeUserDto() {
    final MasqueradeUserDto dto = new MasqueradeUserDto();
    dto.setUserId("ABC");

    Assert.assertNotNull(dto.getUserId());
    Assert.assertEquals("ABC", dto.getUserId());
  }

}
