package com.baesystems.ai.lr.cd.be.domain.dto.export.test;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.springframework.util.Assert;

import com.baesystems.ai.lr.cd.be.domain.dto.export.RepeatOfDto;
import com.baesystems.ai.lr.cd.be.enums.DueStatus;
import com.baesystems.ai.lr.dto.LinkResource;


/**
 * Unit Test for {@link RepeatOfDto}.
 *
 * @author yng
 *
 */
public class RepeatOfDtoTest {
  /**
   * Unit test for {@link RepeatOfDto}.
   *
   */
  @Test
  public final void testRepeatOfDto() {

    final Date date = Calendar.getInstance().getTime();

    final RepeatOfDto dto = new RepeatOfDto();
    dto.setDueDate(date);
    dto.setLowerRangeDate(date);
    dto.setUpperRangeDate(date);
    dto.setDueStatus(new LinkResource(1L));
    dto.setDueStatusH(DueStatus.NOT_DUE);
    dto.setRepeatOf(1L);

    Assert.notNull(dto);
    Assert.notNull(dto.getDueDate());
    Assert.notNull(dto.getLowerRangeDate());
    Assert.notNull(dto.getUpperRangeDate());
    Assert.notNull(dto.getDueStatus());
    Assert.notNull(dto.getDueStatusH());
    Assert.notNull(dto.getRepeatOf());

  }
}
