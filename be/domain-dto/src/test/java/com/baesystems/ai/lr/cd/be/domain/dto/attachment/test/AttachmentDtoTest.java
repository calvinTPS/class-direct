package com.baesystems.ai.lr.cd.be.domain.dto.attachment.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.AttachmentTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationPageResource;

/**
 * Attachment dto related test.
 *
 * @author yng
 *
 */
public class AttachmentDtoTest {

  /**
   * Test SupplementaryInformationHDto.
   */
  @Test
  public final void testSupplementaryInformationHDto() {
    final SupplementaryInformationHDto dto = new SupplementaryInformationHDto();
    dto.setAttachmentTypeH(new AttachmentTypeHDto());
    dto.getAttachmentTypeH().setId(1L);

    Assert.assertNotNull(dto.getAttachmentTypeH());
    Assert.assertEquals(Long.valueOf(1L), dto.getAttachmentTypeH().getId());
  }

  /**
   * Test SupplementaryInformationPageResource.
   */
  @Test
  public final void testSupplementaryInformationPageResource() {
    final List<SupplementaryInformationHDto> dtos = new ArrayList<>();
    dtos.add(new SupplementaryInformationHDto());
    dtos.add(new SupplementaryInformationHDto());

    final SupplementaryInformationPageResource resource = new SupplementaryInformationPageResource();
    resource.setContent(dtos);

    Assert.assertNotNull(resource.getContent());
    Assert.assertEquals(2L, resource.getContent().size());

  }

}
