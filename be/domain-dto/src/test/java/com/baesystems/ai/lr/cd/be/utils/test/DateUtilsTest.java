package com.baesystems.ai.lr.cd.be.utils.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.utils.DateUtils;

/**
 * @author VKolagutla
 *
 */
public class DateUtilsTest {

  /**
   * MILLIES_1000.
   */
  private static final Long MILLIES_1000 = 1000L;

  /**
   * Date Pattern.
   */
  private static final String DATE_PATTERN_WITHOUT_DOT = "dd MMM YYYY";

  /**
   * Date Pattern.
   */
  private static final String DATE_PATTERN_REVERSE = "yyyyMMdd";

  /**
   * Date Time Pattern.
   */
  private static final String DATE_PATTERN_WITH_TIME = "yyyy-MM-dd HH";

  /**
   * test to convert Date to LocalDateTime.
   */
  @Test
  public final void testGetDateToLocalDate() {
    final Date date = new Date();
    final LocalDateTime local = LocalDateTime.now();
    final LocalDateTime localDateTime = DateUtils.getDateToLocalDate(date);
    Assert.assertNotNull(localDateTime);
    Assert.assertEquals(local.getYear(), localDateTime.getYear());
    Assert.assertEquals(local.getMonth(), localDateTime.getMonth());
  }

  /**
   * test to convert LocalDateTime to Epoch.
   */
  @Test
  public final void testGetLocalDateToEpoch() {
    final LocalDateTime localDateTime = LocalDateTime.now();
    final Long epoch = localDateTime.atZone(ZoneId.systemDefault()).toEpochSecond() * MILLIES_1000;
    final Long epoch1 = DateUtils.getLocalDateToEpoch(localDateTime);
    Assert.assertNotNull(epoch);
    Assert.assertEquals(epoch, epoch1);
  }

  /**
   * test to convert Date to Epoch.
   */
  @Test
  public final void testGetDateToEpoch() {
    final Date date = new Date();
    final Long epoch1 = date.toInstant().atZone(ZoneId.systemDefault()).toEpochSecond() * MILLIES_1000;
    final Long epoch = DateUtils.getDateToEpoch(date);
    Assert.assertNotNull(epoch);
    Assert.assertEquals(epoch1, epoch);
  }

  /**
   * test to convert Date to LocalDateTime.
   */
  @Test
  public final void testGetLocalDateFromDate() {
    final Date date = new Date();
    final LocalDate local = LocalDate.now();
    final LocalDate localDateTime = DateUtils.getLocalDateFromDate(date);
    Assert.assertNotNull(localDateTime);
    Assert.assertEquals(local.getYear(), localDateTime.getYear());
    Assert.assertEquals(local.getMonth(), localDateTime.getMonth());
  }

  /**
   * test to convert Date to LocalDateTime.
   */
  @Test
  public final void testGetLocalDateOffsetString() {
    final String offset = "604800";
    final LocalDate localDate = DateUtils.getLocalDateOffset(offset);
    final LocalDateTime localDateTime = LocalDateTime.now().minusSeconds(Long.valueOf(offset));
    Assert.assertEquals(localDate.getYear(), localDateTime.getYear());
    Assert.assertEquals(localDate.getYear(), localDateTime.getYear());
    Assert.assertEquals(localDate.getMonth(), localDateTime.getMonth());
  }

  /**
   * test to convert Date to LocalDateTime.
   */
  @Test
  public final void testGetLocalDateOffsetLong() {
    final Long offset = 604800L;
    final LocalDateTime localDateTime = DateUtils.getLocalDateOffset(offset);
    final LocalDateTime localDateTime2 =
        LocalDateTime.now().minusSeconds(offset).atZone(ZoneId.systemDefault()).toLocalDateTime();
    Assert.assertEquals(localDateTime.toLocalDate(), localDateTime2.toLocalDate());
  }

  /**
   * test to convert Date to LocalDateTime.
   */
  @Test
  public final void testGetFormattedDatetoString() {
    Date date = new Date();
    final String formattedDate = DateUtils.getFormattedDatetoString(date, DATE_PATTERN_WITHOUT_DOT);
    String dateformatter = new SimpleDateFormat(DATE_PATTERN_WITHOUT_DOT).format(date);
    Assert.assertEquals(dateformatter, formattedDate);
  }

  /**
   * test to convert Date to LocalDateTime.
   *
   * @throws ParseException ParseException.
   */
  @Test
  public final void testGetFormattedStringDatetoDate() throws ParseException {
    Date currentDate = new Date();
    SimpleDateFormat dateformatter = new SimpleDateFormat(DATE_PATTERN_REVERSE, Locale.getDefault());
    String dateformatToCompare = dateformatter.format(currentDate);
    final Date formattedDate = DateUtils.getFormattedStringDatetoDate(dateformatToCompare, DATE_PATTERN_REVERSE);

    final String originalDate = DateUtils.getFormattedDatetoString(currentDate, DATE_PATTERN_WITHOUT_DOT);
    final String targetDate = DateUtils.getFormattedDatetoString(formattedDate, DATE_PATTERN_WITHOUT_DOT);

    Assert.assertEquals(originalDate, targetDate);
  }

  /**
   * test to get Offset Datetime.
   *
   * @throws ParseException ParseException.
   */
  @Test
  public final void testGetOffSetDateTimeString() {
    final Long offSetTimeInSeconds = 5400L;
    LocalDateTime localDateTime = LocalDateTime.now().minusSeconds(offSetTimeInSeconds);
    String dateformatter =
        localDateTime.format(DateTimeFormatter.ofPattern(DATE_PATTERN_WITH_TIME, Locale.getDefault()));
    final String formattedDate = DateUtils.getOffSetDateTimeString(offSetTimeInSeconds, DATE_PATTERN_WITH_TIME);
    // ignoring milliseconds, minutes and seconds -due to code execution.
    Assert.assertEquals(dateformatter, formattedDate);
  }

  /**
   * test to get string formatted date from locale date.
   *
   * @throws ParseException ParseException.
   *
   */
  @Test
  public final void testGetFormattedLocaleDatetoString() throws ParseException {
    LocalDate localDate = LocalDate.now();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
    String formattedString = localDate.format(formatter);
    final String formattedDate = DateUtils.getFormattedLocaleDatetoString(localDate, DATE_PATTERN_WITHOUT_DOT);
    Assert.assertEquals(formattedString, formattedDate);
  }

}
