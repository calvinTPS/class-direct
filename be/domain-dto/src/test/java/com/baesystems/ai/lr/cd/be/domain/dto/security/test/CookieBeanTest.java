package com.baesystems.ai.lr.cd.be.domain.dto.security.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.security.bean.CookieBean;

/**
 * @author RKaneysan
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class CookieBeanTest {

  /**
   * Max Age constant.
   */
  private static final int MAX_AGE = 86400;

  /**
   * Cookie Version constant.
   */
  private static final int VERSION = 1;

  /**
   * @return Cookie Bean.
   */
  private CookieBean populateCookieBean() {
    CookieBean cookieBean = new CookieBean();
    cookieBean.setDomain("localhost");
    cookieBean.setHttpOnly(true);
    cookieBean.setMaxAge(MAX_AGE);
    cookieBean.setPath("/");
    cookieBean.setSecure(false);
    cookieBean.setComments("Test");
    cookieBean.setVersion(VERSION);
    return cookieBean;
  }

  /**
   * Check if the Cookie Bean is Empty.
   */
  @Test
  public final void cookieBeanNoEmptyTest() {
    final CookieBean cookieBean = populateCookieBean();
    Assert.assertNotNull(cookieBean);
    Assert.assertNotNull(cookieBean.toString());
    Assert.assertFalse(cookieBean.toString().isEmpty());

    Assert.assertNotNull(cookieBean.getDomain());
    Assert.assertNotNull(cookieBean.isHttpOnly());
    Assert.assertTrue(cookieBean.isHttpOnly());
    Assert.assertNotNull(cookieBean.getMaxAge());
    Assert.assertNotNull(cookieBean.getPath());
    Assert.assertNotNull(cookieBean.isSecure());
    Assert.assertFalse(cookieBean.isSecure());
    Assert.assertNotNull(cookieBean.getComments());
    Assert.assertNotNull(cookieBean.getVersion());
  }

}
