package com.baesystems.ai.lr.cd.be.utils.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import com.baesystems.ai.lr.cd.be.domain.dto.download.CS10FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.download.FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.download.S3FileToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.DownloadTypeEnum;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectSecurityException;
import com.baesystems.ai.lr.cd.be.utils.SecurityUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provides unit test methods {@link SecurityUtils}.
 *
 * @author SBollu
 *
 */
public class SecurityUtilsTest {

  /**
   * The default s3 bucket.
   */
  private static final String DEFAULT_BUCKET = "lr-classdirect";

  /**
   * The default file key.
   */
  private static final String DEFAULT_KEY = "test/sample.pdf";

  /**
   * The default node id.
   */
  private static final Integer DEFAULT_NODE = 10001;

  /**
   * The default version.
   */
  private static final Integer DEFAULT_VERSION = 1;

  /**
   * The default sample s3 token.
   */
  private static final String SAMPLE_ENCODED_S3_TOKEN =
      "MOEUt56Egwx0czcon3549J_ZmpHHcKpOQzFKC6KMtaCzR-lXB5t0jcLCkDckr8hOhY27NTszRK9H14WAAiY8m3li3za7KvPFNONvOeRbWB9AdZK"
          + "QufkB82RkWy2brxof";

  /**
   * The default sample CS10 token.
   */
  private static final String SAMPLE_ENCODED_CS10_TOKEN =
      "XzqvbktgzIeCKD131jY9eCVpcaKEp-1w7CIIqdCKsu8-M-NQpFKIbTm507rJSJmNBOhvcJ3IuO5Y2Ddt0dGYbQ";

  /**
   * The default sample s3 token for failure scenario.
   */
  private static final String SAMPLE_ENCODED_S3_TOKEN_FAIL =
      "BlzECXFUQiuB9b_81WKSPn5nrFRbo9qM0mvXlJ5TBKtjAMuwokx0SpchKcVT7mq8ggsOSl93PvQhSU4C2ml3"
          + "QLIIG4PbwfcC9tZKsYabP91d0VfkXBl4mXvYtAs2TJMNIp1fFdxNQ-wyxwkZl4MTRQDcgBEF9ZLhF1Xj5t3fQ";

  /**
   * The default object mapper.
   */
  private static ObjectMapper objectMapper;

  static {
    objectMapper = new ObjectMapper();
    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
  }

  /**
   * The {@link SecurityContext} from test Spring context.
   */
  private SecurityContext securityCtx;

  /**
   * Provides prerequisite mocking of user authentication.
   */
  @Before
  public final void setUp() {
    securityCtx = SecurityContextHolder.getContext();
    SecurityContext context = new SecurityContextImpl();
    List<SimpleGrantedAuthority> listOfRoles = new ArrayList<SimpleGrantedAuthority>();
    UserProfiles user = new UserProfiles();
    user.setUserId("37be2b5b-63ee-4535-bca4-202d556d626e");
    CDAuthToken token = new CDAuthToken(listOfRoles);

    token.setDetails(user);
    token.setAuthenticated(true);
    context.setAuthentication(token);
    SecurityContextHolder.setContext(context);
  }


  /**
   * Destroys context after all tests executed.
   */
  @After
  public final void tearDown() {
    SecurityContextHolder.setContext(securityCtx);
  }



  /**
   * Tests success scenario for {@link SecurityUtils#encrypt(String)}.encrypts and encodes to a S3
   * string token.
   *
   * @throws ClassDirectSecurityException if any padding, algorithm and invalid key exception
   *         occurs.
   * @throws IOException if any API fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws JsonGenerationException if any json generation fails.
   * @throws InterruptedException if any thread interrupt exception.
   */
  @Test
  public final void testS3TokenEncrptionSuccess() throws ClassDirectSecurityException, JsonGenerationException,
      JsonMappingException, IOException, InterruptedException {
    final S3FileToken fileToken = new S3FileToken();
    fileToken.setBucketName(DEFAULT_BUCKET);
    fileToken.setFileKey(DEFAULT_KEY);
    final FileToken token = new FileToken();
    token.setType(DownloadTypeEnum.S3);
    token.setS3Token(fileToken);

    final String s3Token = SecurityUtils.encrypt(generateString(token), "37be2b5b-63ee-4535-bca4-202d556d626e");
    assertNotNull(s3Token);
    assertFalse(s3Token.isEmpty());

    final String fileTokenString = SecurityUtils.decrypt(s3Token, "37be2b5b-63ee-4535-bca4-202d556d626e");
    final FileToken decryptfileToken = objectMapper.readValue(fileTokenString, FileToken.class);
    assertNotNull(decryptfileToken);
    assertEquals(DownloadTypeEnum.S3, decryptfileToken.getType());
    assertNotNull(decryptfileToken.getS3Token());
    // decrypted bucketName should be equal to DEFAULT_BUCKET
    assertEquals(DEFAULT_BUCKET, decryptfileToken.getS3Token().getBucketName());
    // decrypted key should be equal to DEFAULT_KEY
    assertEquals(DEFAULT_KEY, decryptfileToken.getS3Token().getFileKey());

  }



  /**
   * Tests success scenario for {@link SecurityUtils#encrypt(String)}.encrypts and encodes to a S3
   * string token.
   *
   * @throws ClassDirectSecurityException if any padding, algorithm and invalid key exception
   *         occurs.
   * @throws IOException if any API fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws JsonGenerationException if any json generation fails.
   * @throws InterruptedException if any thread interrupt exception.
   */
  @Test
  public final void testCS10TokenEncrptionSuccess() throws ClassDirectSecurityException, JsonGenerationException,
      JsonMappingException, IOException, InterruptedException {
    final CS10FileToken token = new CS10FileToken();
    token.setNodeId(DEFAULT_NODE);
    token.setVersionNumber(DEFAULT_VERSION);
    final FileToken fileToken = new FileToken();
    fileToken.setType(DownloadTypeEnum.CS10);
    fileToken.setCs10Token(token);

    final String cs10Token = SecurityUtils.encrypt(generateString(fileToken), "37be2b5b-63ee-4535-bca4-202d556d626e");
    assertNotNull(cs10Token);
    assertFalse(cs10Token.isEmpty());

    final String fileTokenString = SecurityUtils.decrypt(cs10Token, "37be2b5b-63ee-4535-bca4-202d556d626e");
    final FileToken decryptfileToken = objectMapper.readValue(fileTokenString, FileToken.class);
    assertNotNull(decryptfileToken);
    assertEquals(DownloadTypeEnum.CS10, decryptfileToken.getType());
    assertNotNull(decryptfileToken.getCs10Token());
    // decrypted Node should be equal to DEFAULT_NODE.
    assertEquals(DEFAULT_NODE, fileToken.getCs10Token().getNodeId());
    // decrypted Node should be equal to DEFAULT_VERSION.
    assertEquals(DEFAULT_VERSION, fileToken.getCs10Token().getVersionNumber());
  }


  /**
   * Converts object to String.
   *
   * @param token the token to be converted to string.
   * @return converted string.
   * @throws JsonGenerationException if any json generation fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws IOException f any API fails.
   */
  private String generateString(final FileToken token)
      throws JsonGenerationException, JsonMappingException, IOException {
    final StringWriter writer = new StringWriter();
    objectMapper.writeValue(writer, token);
    return writer.toString();
  }


  /**
   * Tests failure scenario for {@link SecurityUtils#encrypt(String)}.if salt is more than sixteen
   * bytes length(128 bit).
   *
   * @throws ClassDirectSecurityException if any padding, algorithm and invalid key exception
   *         occurs.
   * @throws IOException if any API fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws JsonGenerationException if any json generation fails.
   * @throws InterruptedException if any thread interrupt exception.
   */
  @Test(expected = ClassDirectSecurityException.class)
  public final void testS3TokenEncrptionFailure() throws ClassDirectSecurityException, JsonGenerationException,
      JsonMappingException, IOException, InterruptedException {
    final S3FileToken fileToken = new S3FileToken();
    fileToken.setBucketName(DEFAULT_BUCKET);
    fileToken.setFileKey(DEFAULT_KEY);
    final FileToken token = new FileToken();
    token.setType(DownloadTypeEnum.S3);
    token.setS3Token(fileToken);

    final String s3Token = SecurityUtils.encrypt(generateString(token), "37be2b5b-63ee-4535-bca4-202d556d626e-12345");
  }


  /**
   * Tests failure scenario for {@link SecurityUtils#encrypt(String)}.if salt is more than sixteen
   * bytes length(128 bit).
   *
   * @throws ClassDirectSecurityException if any padding, algorithm and invalid key exception
   *         occurs.
   * @throws IOException if any API fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws JsonGenerationException if any json generation fails.
   * @throws InterruptedException if any thread interrupt exception.
   */
  @Test(expected = ClassDirectSecurityException.class)
  public final void testS3TokenEncrptionFailureforInvalidNumber() throws ClassDirectSecurityException,
      JsonGenerationException, JsonMappingException, IOException, InterruptedException {
    final S3FileToken fileToken = new S3FileToken();
    fileToken.setBucketName(DEFAULT_BUCKET);
    fileToken.setFileKey(DEFAULT_KEY);
    final FileToken token = new FileToken();
    token.setType(DownloadTypeEnum.S3);
    token.setS3Token(fileToken);

    final String s3Token =
        SecurityUtils.encrypt(generateString(token), "37be2b5b-63ee-4535-bca4-202d556d626e-wqrsfyt-12345");
  }

  /**
   * Tests success scenario for {@link SecurityUtils#decrypt(String)}.decrypts and decodes the
   * string to proper S3 file token.
   *
   * @throws ClassDirectException if any padding, algorithm and invalid key exception occurs.
   * @throws IOException if any API fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws JsonGenerationException if any json generation fails.
   * @throws JsonParseException if any json parse fails.
   * @throws InterruptedException if any thread interrupt exception.
   */
  @Test
  public final void testS3TokenDecryptionSuccess()
      throws ClassDirectException, JsonParseException, JsonMappingException, IOException, InterruptedException {

    final String fileTokenString =
        SecurityUtils.decrypt(SAMPLE_ENCODED_S3_TOKEN, "37be2b5b-63ee-4535-bca4-202d556d626e");
    final FileToken fileToken = objectMapper.readValue(fileTokenString, FileToken.class);
    assertNotNull(fileToken);
    assertEquals(DownloadTypeEnum.S3, fileToken.getType());
    assertNotNull(fileToken.getS3Token());
    // decrypted bucketName should be equal to DEFAULT_BUCKET
    assertEquals(DEFAULT_BUCKET, fileToken.getS3Token().getBucketName());
    // decrypted key should be equal to DEFAULT_KEY
    assertEquals(DEFAULT_KEY, fileToken.getS3Token().getFileKey());
  }

  /**
   * Tests success scenario for {@link SecurityUtils#decrypt(String)}.decrypts and decodes the
   * string to proper CS10 file token.
   *
   * @throws ClassDirectException if any padding, algorithm and invalid key exception occurs.
   * @throws IOException if any API fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws JsonGenerationException if any json generation fails.
   * @throws JsonParseException if any json parse fails.
   * @throws InterruptedException if any thread interrupt exception
   */
  @Test
  public final void testCS10TokenDecryptionSuccess()
      throws ClassDirectException, JsonParseException, JsonMappingException, IOException, InterruptedException {
    final String fileTokenString =
        SecurityUtils.decrypt(SAMPLE_ENCODED_CS10_TOKEN, "37be2b5b-63ee-4535-bca4-202d556d626e");
    final FileToken fileToken = objectMapper.readValue(fileTokenString, FileToken.class);
    assertNotNull(fileToken);
    assertEquals(DownloadTypeEnum.CS10, fileToken.getType());
    assertNotNull(fileToken.getCs10Token());
    // decrypted Node should be equal to DEFAULT_NODE.
    assertEquals(DEFAULT_NODE, fileToken.getCs10Token().getNodeId());
    // decrypted Node should be equal to DEFAULT_VERSION.
    assertEquals(DEFAULT_VERSION, fileToken.getCs10Token().getVersionNumber());
  }


  /**
   * Tests failure scenario for {@link SecurityUtils#decrypt(String)}.if salt is more than sixteen
   * bytes length(128 bit).
   *
   * @throws ClassDirectSecurityException if any padding, algorithm and invalid key exception
   *         occurs.
   * @throws JsonGenerationException if any json generation fails.
   * @throws JsonMappingException if any json mapping fails.
   * @throws IOException if any API fails.
   * @throws InterruptedException if any thread interrupt exception
   */
  @Test(expected = ClassDirectSecurityException.class)
  public final void testS3TokenDEcryptionFailureforInvalidKey() throws ClassDirectSecurityException,
      JsonGenerationException, JsonMappingException, IOException, InterruptedException {
    final String fileTokenString =
        SecurityUtils.decrypt(SAMPLE_ENCODED_S3_TOKEN_FAIL, "37be2b5b-63ee-4535-bca4-202d556d626e-12345");
    final FileToken fileToken = objectMapper.readValue(fileTokenString, FileToken.class);
    assertNotNull(fileToken);
    assertEquals(DownloadTypeEnum.S3, fileToken.getType());
    assertNotNull(fileToken.getS3Token());
    // decrypted bucketName should be equal to DEFAULT_BUCKET
    assertEquals(DEFAULT_BUCKET, fileToken.getS3Token().getBucketName());
    // decrypted key should be equal to DEFAULT_KEY
    assertEquals(DEFAULT_KEY, fileToken.getS3Token().getFileKey());
  }


}


