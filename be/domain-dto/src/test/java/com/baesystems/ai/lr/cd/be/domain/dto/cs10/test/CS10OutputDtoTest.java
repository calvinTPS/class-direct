package com.baesystems.ai.lr.cd.be.domain.dto.cs10.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.cs10.CS10OutputDto;


/**
 * Unit Test for {@link CS10OutputDto}.
 *
 * @author yng
 *
 */
public class CS10OutputDtoTest {
  /**
   * Unit test for {@link CS10OutputDto}.
   *
   */
  @Test
  public final void cs10Test() {
    final CS10OutputDto dto = new CS10OutputDto();
    dto.setFileName("file.txt");
    dto.setFileSize("10");
    dto.setFileStream("VGVzdCBtZXNzYWdlLg==".getBytes());

    Assert.assertEquals("file.txt", dto.getFileName());
    Assert.assertEquals("10", dto.getFileSize());
    Assert.assertNotNull(dto.getFileStream());

  }
}
