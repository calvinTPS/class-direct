package com.baesystems.ai.lr.cd.be.domain.dto.export.test;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;


/**
 * Unit Test for {@link ExportPdfDto}.
 *
 * @author yng
 *
 */
public class ExportPdfDtoTest {

  /**
   * Unit test for {@link ExportPdfDto}.
   *
   */
  @Test
  public final void exportPdfDtoTest() {
    final ExportPdfDto dto = new ExportPdfDto();

    dto.setCode("LRV1");
    Set<Long> itemsToExport = new HashSet<Long>();
    itemsToExport.add(1L);
    itemsToExport.add(2L);
    dto.setItemsToExport(itemsToExport);
    Assert.assertSame(2, dto.getItemsToExport().size());
    Assert.assertEquals("LRV1", dto.getCode());
  }
}
