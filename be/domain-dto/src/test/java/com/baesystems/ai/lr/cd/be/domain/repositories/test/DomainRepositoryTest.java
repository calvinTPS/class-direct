package com.baesystems.ai.lr.cd.be.domain.repositories.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.PojoField;
import com.openpojo.reflection.PojoMethod;
import com.openpojo.reflection.filters.FilterChain;
import com.openpojo.reflection.filters.FilterSyntheticClasses;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.reflection.impl.PojoClassImpl;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsRule;
import com.openpojo.validation.rule.impl.NoStaticExceptFinalRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.DefaultValuesNullTester;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

/**
 * @author vkovuru
 *
 */
public class DomainRepositoryTest {

  /**
   * package name to scan.
   */
  private static final String POJO_PACKAGE = "com.baesystems.ai.lr.cd.be.domain.repositories";

  /**
   * test all the classes in the POJO_PACKAGE.
   */
  @Test
  public final void testPojoStructureAndBehavior() {
    final Validator validator = ValidatorBuilder.create()
        // Add Rules to validate structure for POJO_PACKAGE
        // See com.openpojo.validation.rule.impl for more ...
        .with(new GetterMustExistRule()).with(new SetterMustExistRule())
        // Add Testers to validate behaviour for POJO_PACKAGE
        // See com.openpojo.validation.test.impl for more ...
        .with(new NoPublicFieldsRule()).with(new NoStaticExceptFinalRule()).with(new GetterMustExistRule())
        .with(new SetterMustExistRule())

        // Create Testers to validate behaviour for POJO_PACKAGE
        .with(new DefaultValuesNullTester()).with(new SetterTester()).with(new GetterTester()).build();

    final PojoClassFilter pojoClassFilter = new FilterChain(new FilterSyntheticClasses());
    List<PojoClass> pojoClasses = PojoClassFactory.getPojoClassesRecursively(POJO_PACKAGE, pojoClassFilter);
    pojoClasses = filterClasses(pojoClasses);
    validator.validate(pojoClasses);
  }

  /**
   * @param pojoClasses pojo classes.
   * @return list of pojo classes.
   */
  private List<PojoClass> filterClasses(final List<PojoClass> pojoClasses) {
    final List<PojoClass> results = new ArrayList<PojoClass>();
    for (final PojoClass clazz : pojoClasses) {
      final List<PojoField> fields = new ArrayList<PojoField>(0);
      final List<PojoMethod> methods = new ArrayList<PojoMethod>(0);

      for (final PojoField field : clazz.getPojoFields()) {
        if (!field.getName().contains("$")) {
          fields.add(field);
        }
      }

      for (final PojoMethod method : clazz.getPojoMethods()) {
        if (!method.getName().contains("$")) {
          methods.add(method);
        }
      }

      results.add(new PojoClassImpl(clazz.getClazz(), fields, methods));
    }
    return results;
  }
}
