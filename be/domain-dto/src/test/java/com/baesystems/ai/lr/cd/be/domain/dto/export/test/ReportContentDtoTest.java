package com.baesystems.ai.lr.cd.be.domain.dto.export.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportContentHDto;
import com.baesystems.ai.lr.dto.certificates.CertificateActionDto;
import com.baesystems.ai.lr.dto.certificates.CertificateDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.StatutoryFindingDto;
import com.baesystems.ai.lr.dto.services.SurveyWithTaskListDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;



/**
 * Unit Test for {@link ReporContentDto}.
 *
 * @author yng
 *
 */
public class ReportContentDtoTest {

  /**
   * Unit test for {@link ReporContentDto}.
   *
   */
  @Test
  public final void assetExportDtoTest() {
    final ReportContentHDto dto = new ReportContentHDto();

    dto.setWipActionableItems(new ArrayList<>());
    dto.getWipActionableItems().add(new ActionableItemDto());

    dto.setWipAssetNotes(new ArrayList<>());
    dto.getWipAssetNotes().add(new AssetNoteDto());
    dto.setWipCocs(new ArrayList<>());
    dto.getWipCocs().add(new CoCDto());

    dto.setSurveys(new ArrayList<>());
    dto.getSurveys().add(new SurveyWithTaskListDto());

    dto.setTasks(new ArrayList<>());
    dto.getTasks().add(new WorkItemDto());

    dto.setStatutoryFindings(new ArrayList<>());
    dto.getStatutoryFindings().add(new StatutoryFindingDto());

    dto.setCertificates(new ArrayList<>());
    dto.getCertificates().add(new CertificateDto());

    dto.setCertificateActions(new ArrayList<>());
    dto.getCertificateActions().add(new CertificateActionDto());

    Assert.assertEquals(1, dto.getWipActionableItems().size());
    Assert.assertEquals(1, dto.getWipAssetNotes().size());
    Assert.assertEquals(1, dto.getWipCocs().size());
    Assert.assertEquals(1, dto.getSurveys().size());
    Assert.assertEquals(1, dto.getTasks().size());
    Assert.assertEquals(1, dto.getStatutoryFindings().size());
    Assert.assertEquals(1, dto.getCertificates().size());
    Assert.assertEquals(1, dto.getCertificateActions().size());
  }
}
