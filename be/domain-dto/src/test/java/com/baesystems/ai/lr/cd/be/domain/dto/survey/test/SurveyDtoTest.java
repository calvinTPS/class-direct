package com.baesystems.ai.lr.cd.be.domain.dto.survey.test;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.port.PortAgentDto;
import com.baesystems.ai.lr.cd.be.domain.dto.survey.SurveyRequestDto;

/**
 * Combined unit test.
 *
 * @author NAvula
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class SurveyDtoTest {

  /**
   * int array.
   */
  private static final Long[] ARRAY = {1L, 2L, 3L};

  /**
   * Unit test for AssetTypeDto.
   *
   * @throws IOException when mapper error.
   *
   */
  @Test
  public final void assetTypeTest() throws IOException {
    SurveyRequestDto surveyDto = new SurveyRequestDto();
    Assert.assertNotNull(surveyDto);
    surveyDto.setAdditionalComments("Additional Comments");
    Assert.assertNotNull(surveyDto.getAdditionalComments());
    surveyDto.setAssetId("LRV1");
    Assert.assertEquals("LRV1", surveyDto.getAssetId());

    surveyDto.setBerthAnchorage("berthAnchorage");
    Assert.assertNotNull(surveyDto.getBerthAnchorage());

    surveyDto.setEstimatedDateOfArrival(LocalDate.now());
    Assert.assertNotNull(surveyDto.getEstimatedDateOfArrival());

    surveyDto.setEstimatedDateOfDeparture(LocalDate.now());
    Assert.assertNotNull(surveyDto.getEstimatedDateOfDeparture());

    surveyDto.setEstimatedTimeOfArrival(LocalTime.now());
    Assert.assertNotNull(surveyDto.getEstimatedTimeOfArrival());

    surveyDto.setEstimatedTimeOfDeparture(LocalTime.now());
    Assert.assertNotNull(surveyDto.getEstimatedTimeOfDeparture());

    surveyDto.setOtherEmails(new String[] {"one@comp.com", "two@comp.com"});
    Assert.assertNotNull(surveyDto.getOtherEmails());

    surveyDto.setPortAgent(new PortAgentDto());
    Assert.assertNotNull(surveyDto.getPortAgent());

    surveyDto.setPortId(1L);
    Assert.assertNotNull(surveyDto.getPortId());

    surveyDto.setServiceIds(Arrays.asList(ARRAY));
    Assert.assertNotNull(surveyDto.getServiceIds());

    surveyDto.setShipContactDetails("shipContactDetails");
    Assert.assertNotNull(surveyDto.getShipContactDetails());

    surveyDto.setSurveyStartDate(LocalDate.now());
    Assert.assertNotNull(surveyDto.getSurveyStartDate());

  }
}
