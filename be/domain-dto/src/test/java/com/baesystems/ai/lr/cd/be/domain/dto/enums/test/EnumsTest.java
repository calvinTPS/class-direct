package com.baesystems.ai.lr.cd.be.domain.dto.enums.test;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.cd.be.enums.AssetInformationExportEnum;
import com.baesystems.ai.lr.cd.be.enums.AssetServiceExportEnum;

/**
 * Enums related test.
 *
 * @author yng
 *
 */
public class EnumsTest {

  /**
   * Test enum AssetInformationExportEnum.
   */
  @Test
  public final void testAssetInformationExportEnum() {
    final Long test1 = AssetInformationExportEnum.BASIC_ASSET_INFO.getId();
    Assert.assertEquals(Long.valueOf(1L), test1);
  }

  /**
   * Test AssetServiceExportEnum.
   */
  @Test
  public final void testAssetServiceExportEnum() {
    final Long test1 = AssetServiceExportEnum.NO_SERVICE_INFO.getId();
    Assert.assertEquals(Long.valueOf(1L), test1);
  }
}
