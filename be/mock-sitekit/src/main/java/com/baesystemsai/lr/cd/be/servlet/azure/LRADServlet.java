package com.baesystemsai.lr.cd.be.servlet.azure;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Endpoint to handle request to internal LR AD.
 *
 * @author fwijaya on 3/5/2017.
 */
@WebServlet("/graph/*")
public class LRADServlet extends HttpServlet {
  /**
   * Handles get request.
   */
  public void doGet(final HttpServletRequest request, final HttpServletResponse response)
    throws IOException, ServletException {
    String pathInfo = request.getPathInfo() == null ? "" : request.getPathInfo();
    response.setContentType("application/json");
    if (pathInfo.startsWith("/user")) {
      IOUtils.copy(getServletContext().getResourceAsStream("WEB-INF/internaluserlist.json"), response
        .getOutputStream());
    } else if (pathInfo.startsWith("/token")) {
      IOUtils.copy(getServletContext().getResourceAsStream("WEB-INF/azuretoken.json"), response.getOutputStream());
    }
  }

  /**
   * Handles post request.
   */
  public void doPost(final HttpServletRequest request, final HttpServletResponse response)
    throws ServletException, IOException {
    doGet(request, response);
  }
}
