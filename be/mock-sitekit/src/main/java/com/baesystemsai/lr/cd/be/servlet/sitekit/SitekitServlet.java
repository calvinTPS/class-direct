package com.baesystemsai.lr.cd.be.servlet.sitekit;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Endpoint to handle sitekit request.
 *
 * Value for sitekit-config.properties should be set as follows:
 * sitekit.api.endpoint=http://localhost/mock-sitekit/sitekit
 * sitekit.oauth2.tokenurl=http://localhost/mock-sitekit/sitekit/token
 *
 * @author fwijaya on 3/5/2017.
 */
@WebServlet("/sitekit/*")
public class SitekitServlet extends HttpServlet {

  /**
   * Handles get request.
   */
  public void doGet(final HttpServletRequest request, final HttpServletResponse response)
    throws IOException, ServletException {
    String pathInfo = request.getPathInfo() == null ? "" : request.getPathInfo();
    response.setContentType("application/json");
    if (pathInfo.contains("user")) {
      IOUtils.copy(getServletContext().getResourceAsStream("WEB-INF/userlist.json"), response.getOutputStream());
    } else if (pathInfo.contains("token")) {
      IOUtils.copy(getServletContext().getResourceAsStream("WEB-INF/azuretoken.json"), response.getOutputStream());
    } else if (pathInfo.contains("approve")) {
      IOUtils.copy(getServletContext().getResourceAsStream("WEB-INF/userstatus.json"), response.getOutputStream());
    }
  }

  /**
   * Handles post request.
   */
  public void doPost(final HttpServletRequest request, final HttpServletResponse response)
    throws ServletException, IOException {
    doGet(request, response);
  }

}
