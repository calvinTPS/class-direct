import Transform from 'readable-stream/transform';
import rs from 'replacestream';
import istextorbinary from 'istextorbinary';
import he from 'he';

export default (options = {}) => new Transform({
  objectMode: true,
  transform(file, enc, callback) {
    if (file.isNull()) {
      return callback(null, file);
    }

    String.prototype.lpad = function (padString, length) {
      let str = this;
      while (str.length < length)
        str = padString + str;
      return str;
    }

    if (typeof _replacement === 'function') {
      // Pass the vinyl file object as this.file
      replacement = _replacement.bind({ file });
    }

    function doReplace() {
      if (file.isBuffer()) {
        const lines = String(file.contents).split('\n');
        const newFile = [];
        lines.map((text, index) => {

          const numberOfSpaces = text.search(/\S/);
          const leadingSpaces = ''.lpad(' ', numberOfSpaces);
          let childLines = [];
          let skip = false;
          if (text.indexOf('dontcompile') === -1) {
            if (!options.reverse && text.indexOf('///') ===-1) {
              if (text.indexOf('mixin') > -1 ||
                text.trim().indexOf('if ') === 0 ||
                text.trim().indexOf('if(') === 0 ||
                text.trim().indexOf('else') === 0 ||
                text.trim().indexOf('a(href') > -1) {
                // find out home many leading spaces...
                newFile.push(''.lpad(' ', numberOfSpaces) + 'safeblock.display-none');
                text = ' '.lpad(' ', numberOfSpaces) + '  ///' + text.trim();
              }
              if (text.trim().indexOf('- var') > -1) {
                text = text.replace('- var', '/// - var');
              }
              if (text.trim().indexOf('for') === 0) {
                text = text.replace('for', '///for');
                let linesBelowCounter = 1;
                // debugger;
                while(
                  lines[index + linesBelowCounter].search(/\S/)
                  > numberOfSpaces
                ) {
                  const numberOfChildSpaces = lines[index + linesBelowCounter].search(/\S/);

                  childLines.push(
                    leadingSpaces /// add leading spaces
                    + '///' // add the ///
                    + ' '.lpad(' ', numberOfChildSpaces - numberOfSpaces)    // add extra spaces more than parent
                    + lines[index + linesBelowCounter].trim() // add line
                  );

                  lines.splice(index + linesBelowCounter, 1);
                  linesBelowCounter++;
                }
              }
              if (text.indexOf('#{') > -1) {
                text = leadingSpaces + '///' + text.trim()
              }

            } else {
              if (text.indexOf('safeblock') > -1) {
                text = lines.splice(index + 1, 1)[0].replace('  ///', '');
              }
              if (text.trim().indexOf('/') === 0 && text.indexOf('for') > -1) {
                text = leadingSpaces + text.trim().split('//')[1].trim().split('/')[1];
                let linesBelowCounter = 1;
                while (
                  lines[index + linesBelowCounter].search(/\S/)
                  > numberOfSpaces
                ) {
                  const numberOfChildSpaces = numberOfSpaces + 2;

                  childLines.push(
                    leadingSpaces /// add leading spaces
                    + '  ' // add the ///
                    + lines[index + linesBelowCounter].trim() // add line
                  );

                  lines.splice(index + linesBelowCounter, 1);
                  linesBelowCounter++;
                }
              }
              if(text.indexOf('style ') > -1) {
                text = leadingSpaces + 'style.\n' + leadingSpaces + '  ' + text.split('style ')[1]
              }

              text = text.replace('/// ', '');
              text = text.replace('///', '');

            };
          }
          if (!skip) {
            newFile.push(
              he.encode(text, {
                'allowUnsafeSymbols': true
              })
            );
            if (childLines.length > 0) newFile.push(childLines.join('|||'));
          }
          skip = false;
        });

        file.contents = new Buffer(newFile.join('\n'));
        return callback(null, file);
      }

      callback(null, file);
    }

    if (options && options.skipBinary && !options.send) {
      istextorbinary.isText(file.path, file.contents, (err, result) => {
        if (err) {
          return callback(err, file);
        }

        if (!result) {
          callback(null, file);
        } else {
          doReplace();
        }
      });

      return;
    }

    doReplace();
  }
});
