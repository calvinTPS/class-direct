require('babel-polyfill');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var minify = require('html-minifier').minify;
var parse5 = require('parse5');
var Parser = require('./parser');

module.exports = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(sourceHtml) {
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var html, document, parser;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            html = minify(sourceHtml, {
              removeEmptyAttributes: false,
              collapseWhitespace: false,
              collapseBooleanAttributes: false,
              collapseInlineTagWhitespace: true,
              caseSensitive: true
            });
            // html = sourceHtml;

            // Server-side

            document = opts.fragment ? parse5.parseFragment(html) : parse5.parse(html);
            parser = new Parser(document);
            return _context.abrupt('return', parser.parse());

          case 4:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();
