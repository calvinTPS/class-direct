import gulp from 'gulp';
import browserSync from 'browser-sync';
const reload = browserSync.reload;
import svgConvert from 'gulp-svg2png';
import pug from 'gulp-pug';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import inlineCss from 'gulp-inline-css';
import inlineSource from 'gulp-inline-source';
import inky from 'inky';
import rename from 'gulp-rename';
import imagemin from 'gulp-imagemin';
import htmlmin from 'gulp-htmlmin';
import html2pug from './gulp-html2pug';
import { argv } from 'yargs';
import gulpif from 'gulp-if';
import replace from 'gulp-replace';
import sanitizePug from './lib/sanitize-pug';
import lazypipe from 'lazypipe';
import fs from 'fs';
import siphon from 'siphon-media-query';

// Inlines CSS into HTML, adds media query CSS into the <style> tag of the email, and compresses the HTML
const inliner = (css) => {
  const cssText = fs.readFileSync(css).toString();
  const mqCss = siphon(cssText);

  const fontsFilePath = './app/styles/scss/fonts.css';
  const fonts = fs.readFileSync(fontsFilePath).toString();

  const pipe = lazypipe()
    .pipe(inlineCss, {
      applyStyleTags: false,
      removeStyleTags: false,
      removeLinkTags: false
    })

    // inject css
    // add ${fonts} in front of ${mqCss} if you need to inject custom fonts
    .pipe(replace, '<!-- <style> -->', `<style>${mqCss}</style>`)
    .pipe(replace,'<link rel="stylesheet" type="text/css" href="styles/style.css">', '' )
    .pipe(htmlmin,{
      collapseWhitespace: true,
      minifyCSS: true
    });

  return pipe();
}

gulp.task('styles', () => gulp.src('app/styles/scss/main.scss')
  .pipe(sass({
      includePaths: ['node_modules/foundation-emails/scss']
    }).on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(rename('style.css'))
  .pipe(gulp.dest('./app/styles'))
  .pipe(reload({ stream: true })));


const execBack2Pug = () => {
  console.log('Inlined & Sanitized - Converting back to pug...');
  const puts = (error, stdout, stderr) => {
    console.log(stdout);
  };

  const exec = require('child_process').exec;
  exec('yarn run back2pug', puts);

  console.log('Inlined & Sanitized - Converting back to pug...DONE');
}

gulp.task('inline', ['styles', 'pug'], () => gulp.src('app/*.html')
  // .pipe(replace())
  .pipe(inliner('./app/styles/style.css'))
  .pipe(gulp.dest('dist/'))
  .on('end', execBack2Pug) // Had to hack it... Sigh.
);

gulp.task('pug', () => gulp.src('app/template/*.pug')
  .pipe(sanitizePug())
  .pipe(pug({
    pretty: true,
    compileDebug: true
  }))
  .pipe(inky())
  .pipe(gulp.dest('app/')))
  .on('error', (error) => {
    console.error('' + error);
  });

//- I translate the files from the 'gulp serve' command to the pug files that is used by the backend
//- I am used locally to help developers troubleshoot pug files
gulp.task('back2pug', () =>
  gulp.src('dist/**/*.html')
    .pipe(html2pug())
    .pipe(sanitizePug({reverse : true}))
    .pipe(replace('  \/\/\/mixin', 'mixin'))
    .pipe(replace('\/\/\/ - var', '- var'))
    .pipe(replace('\/\/\/', ''))
    .pipe(replace(`safeblock.display-none(align='center')`, ``))
    // .pipe(sanitizePug({ send: true }))
    .pipe(gulp.dest('dist/pug/'))
);

gulp.task('imagemin', () => {
  gulp.src('app/assets/img/**/*.{png,jpg,jpeg,gif,webp}')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/assets/img'));
});

gulp.task('svg2png', () => {
  gulp.src('app/assets/img/**/*.svg')
    .pipe(svgConvert({ height: 800 }))
    .pipe(imagemin())
    .pipe(rename({extname: '.png'}))
    .pipe(gulp.dest('./dist/assets/img'));
});

gulp.task('clean', require('del').bind(null, 'dist'));

gulp.task('build', ['inline', 'imagemin', 'svg2png']);

gulp.task('serve', ['inline', 'imagemin', 'svg2png'], () => {
  browserSync({
    server: './dist',
    notify: false,
    debugInfo: false,
    host: 'localhost'
  });

  gulp.watch('app/styles/**/*.scss', ['inline']);
  gulp.watch('app/template/**/*.pug', ['inline']);

  gulp.watch('dist/*.html').on('change', reload);
  gulp.watch('app/assets/img/**/*.{png,jpg,jpeg,gif,webp,svg}', ['imagemin', 'svg2png']);
});

