package com.baesystemsai.lr.cd.be.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author RKaneysan
 */
public final class SecurityUtils {

  /**
   * Private constructor SecurityUtils.
   */
  private SecurityUtils() {
    super();
  }

  /**
   * Message Digest Encryption Algorithm.
   */
  public static final String MD5_ALGO = "MD5";

  /**
   * UTF Character Encoding.
   */
  public static final String UTF_8 = "UTF-8";

  /**
   * Provide MD5 encryption for given data and shared key.
   *
   * @param input input for encryption.
   * @return encrypted hash value of the input.
   * @throws UnsupportedEncodingException unsupported exception.
   * @throws NoSuchAlgorithmException No Such Algorithm Exception.
   */
  public static String getMessageDigestEncyption(final String input)
      throws UnsupportedEncodingException, NoSuchAlgorithmException {
    StringBuilder encryptedValue = new StringBuilder();
    MessageDigest messageDigest = MessageDigest.getInstance(MD5_ALGO);
    final byte[] encryptedByte = messageDigest.digest(input.getBytes(UTF_8));
    // Convert byte array to hexadecimal string.
    for (byte eachByte : encryptedByte) {
      encryptedValue.append(String.format("%02x", eachByte));
    }
    return encryptedValue.toString();
  }
}
