package com.baesystemsai.lr.cd.be.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baesystemsai.lr.cd.be.utils.SecurityUtils;

/**
 * Servlet implementation class DemoServlet
 */
@WebServlet("/DemoServlet")
public class DemoServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * French Time Zone Id.
   */
  private static final String FRENCH_TIME_ZONE = "Europe/Paris";

  /**
   * Root path.
   */
  private static final String ROOT_PATH = "/";

  /**
   * @see HttpServlet#HttpServlet()
   */
  public DemoServlet() {
    super();
  }

  /**
   * @throws IOException
   * @throws ServletException
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    final String datePattern = "yyyyMMddHHmmss";
    // String equasisSharedKey = "equasisCl@ssDir3ct39";
    String type = request.getParameter("type");
    String sharedKey = request.getParameter("sharedKey");
    Long imoNumber = Long.valueOf(request.getParameter("imoNumber"));
    String url = null;

    if (type.equalsIgnoreCase("equasis")) {
      final LocalDateTime localDateTime = ZonedDateTime.now(ZoneId.of(FRENCH_TIME_ZONE)).toLocalDateTime();
      String time = localDateTime.format(DateTimeFormatter.ofPattern(datePattern));
      final String data = imoNumber + time + sharedKey;
      try {
        String hash = SecurityUtils.getMessageDigestEncyption(data);
        url = ROOT_PATH + type.toLowerCase() + "?IMO=" + imoNumber + "&d=" + time + "&k=" + hash;
        request.setAttribute("hash", hash);
      } catch (NoSuchAlgorithmException e) {
        System.out.println(e.getMessage());
      }
      request.setAttribute("time", time);
    } else if (type.equalsIgnoreCase("thetis")) {
      url = ROOT_PATH + type.toLowerCase() + "?lrno=" + imoNumber + "&client=0016139&user=dave";
    }

    request.setAttribute("type", type.toUpperCase());
    request.setAttribute("imoNumber", imoNumber);
    request.setAttribute("url", url);

    request.getServletContext().getRequestDispatcher("/home.jsp").forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }

}
