<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index Page</title>
<script type="text/javascript">
		/* window.onload=function(){
		 document.form1.submit();
		 } */
	</script>
<title>Equasis/Thetis Demo</title>
<link rel="stylesheet" href="assets/demo.css">
<style>
body {
	width: 800px;
	margin: 60px auto;
	font-family: 'trebuchet MS', 'Lucida sans', Arial;
	font-size: 14px;
	color: #444;
}

h1 {
	font-family: 'trebuchet MS', 'Lucida sans', Arial;
	font-size: 21px;
}
</style>
<script src="js/prefixfree.min.js"></script>
<script>
    // Toggle Shared Key Field display based User type.
	function hideSharedKey() {
	    var userList = document.getElementById("user");
	    var selUser = userList.options[userList.selectedIndex].value;
	    var sharedKeyVar = document.getElementById('shared-key');
		if(selUser === 'equasis'){
			sharedKeyVar.style.display = 'block';
		} else{
			sharedKeyVar.style.display = 'none';
		}
	}
</script>
</head>
<body onload="hideSharedKey()">
	<h1>Equasis / Thetis Demo</h1>
	<div id="login">
		<form id="login_form" name="form1" action="DemoServlet">
			<div class="field_container">
				<select id="user" name="type" onchange="hideSharedKey()">
					<option value="equasis">Equasis</option>
					<option value="thetis">Thetis</option>
				</select>
			</div>
			
			<div class="field_container" id="shared-key">
				<input type="text" name="sharedKey" value="equasisCl@ssDir3ct39" />
			</div>

			<div class="field_container">
				<input type="text" name="imoNumber" placeholder="IMO Number">
				<button id="sign_in_button" type="submit">
					<span class="button_text">Generate URL</span>
				</button>
			</div>
		</form>
	</div>
</body>
</html>
