#!/bin/sh

CMD="mvn clean -P env_dev compile install package"
SKIP_TEST="false"
QUICK="false"
SKIP_INTEGRATION="false"
SKIP_OWASP="true"
LAST=""
PROVISIONING_PATH="../../class-direct-provisioning"
CLASS_DIRECT_WAR_WEBAPP_DIR="class-direct-war/src/main/webapp"

echo "This script is to help with the maven commands to make the development faster by skipping compiling module you
don't need, just choose the module you want to compile, switch test and pmd flags on and off. Commands can be space
separated ex: '12 8 d'  will build the DTO the services and deploy to tomcat."

function options {
    echo ""
    echo ""
    echo "====================="
    echo "CHOOSE MODULE TO COMPILE"
    echo "1) ALL"
    echo "2) class-direct-camel"
    echo "3) class-direct-config"
    echo "4) class-direct-it"
    echo "5) class-direct-mail"
    echo "6) class-direct-rest"
    echo "7) class-direct-security"
    echo "8) class-direct-service"
    echo "9) class-direct-war"
    echo "10) cs10"
    echo "11) domain-dao"
    echo "12) domain-dto"
    echo "13) domain-exception"
    echo "14) mast-domain-dto"
    echo "15) class-direct-s3"
    echo "====================="
    echo "SWITCH MODE"
    echo "t) Skip Test                           = $SKIP_TEST"
    echo "q) Quick (skip PMD, Checkstyle etc...) = $QUICK"
    echo "o) Skip OWASP                          = $SKIP_OWASP"
    echo "i) Skip Integration                    = $SKIP_INTEGRATION"
    echo "e) Enable All SWITCH"
    echo "E) Disable ALL SWITCH"
    echo "====================="
    echo "OTHER"
    echo "d) Deploy to tomcat"
    echo "D) Deploy to tomcat with FE (from provisioning path: $PROVISIONING_PATH)"
    echo "r) Redo: $LAST"
    echo "====================="
    echo "STANDALONE COMMANDS"
    echo "W) Copy FE files to class-direct-war"
    echo "   Current provisioning path: $PROVISIONING_PATH"
    echo "   Usage:"
    echo "     W                                <- use current path"
    echo "     W  <path_to_provisioning_dir>    <- use defined path"
    echo "n) Run a small live server when editing your pug files at /static-file-generator/**"
    echo "N) Build static files and move the compiled pug/imgs to  "
    echo "   their respective folders                              "
    echo "====================="
    echo "Input:"

    read INPUT

    if [ "$INPUT" = "r" ] || [[ "$INPUT" =~ ^\s*$ ]];
        then INPUT="$LAST"
    else
        LAST="$INPUT"
    fi


    if [[ "$INPUT" =~ ^W[[:space:]]+(.*)\s*$ ]]
      then 
      PROVISIONING_PATH="${BASH_REMATCH[1]}"
       INPUT="W"
    fi

    if [[ "$INPUT" =~ ^D[[:space:]]+(.*)\s*$ ]]
      then
      PROVISIONING_PATH="${BASH_REMATCH[1]}"
      INPUT="D"
    fi
    

    SKIP="-DskipTests=$SKIP_TEST -Dquick=$QUICK -Dskip.owasp=$SKIP_OWASP -Dskip.integration.test=$SKIP_INTEGRATION"

    for word in $INPUT
    do
        PROJECT=""
        case $word in
        1)
            PROJECT="all"
            ;;
        2)
            PROJECT="class-direct-camel"
            ;;
        3)
            PROJECT="class-direct-config"
            ;;
        4)
            PROJECT="class-direct-it"
            ;;
        5)
            PROJECT="class-direct-mail"
            ;;
        6)
            PROJECT="class-direct-rest"
            ;;
        7)
            PROJECT="class-direct-security"
            ;;
        8)
            PROJECT="class-direct-service"
            ;;
        9)
            PROJECT="class-direct-war"
            ;;
        10)
            PROJECT="cs10"
            ;;
        11)
            PROJECT="domain-dao"
            ;;
        12)
            PROJECT="domain-dto"
            ;;
        13)
            PROJECT="domain-exception"
            ;;
        14)
            PROJECT="mast-domain-dto"
            ;;
        15)
            PROJECT="class-direct-s3"
            ;;
        d)
            $CMD tomcat:redeploy $SKIP -pl class-direct-war
            ;;
        D)
            rm -rf war_merge
            mkdir war_merge
            cp $PROVISIONING_PATH/local/frontend/archive/class-direct-fe.war war_merge
            cp ./class-direct-war/target/class-direct.war war_merge
            cd war_merge
            unzip -o class-direct.war >/dev/null 2>&1
            mv index.jsp swagger.jsp
            unzip -o class-direct-fe.war > /dev/null 2>&1
            rm *.war
            jar cvf ROOT.war * >/dev/null 2>&1
            curl --noproxy '*' --upload-file ROOT.war "http://admin:admin@localhost:8080/manager/text/deploy?path=/&update=true"
            cd ..
            rm -rf war_merge
            ;;
        t)
            if [ $SKIP_TEST = "false" ]
                then SKIP_TEST="true"
            else
                SKIP_TEST="false"
            fi
            clear
            ;;
        q)
            if [ $QUICK = "false" ]
                then QUICK="true"
            else
                QUICK="false"
            fi
            clear
            ;;
        i)
            if [ $SKIP_INTEGRATION = "false" ]
                then SKIP_INTEGRATION="true"
            else
                SKIP_INTEGRATION="false"
            fi
            clear
            ;;
        o)
            if [ $SKIP_OWASP = "false" ]
                then SKIP_OWASP="true"
            else
                SKIP_OWASP="false"
            fi
            clear
            ;;
        e)
            SKIP_TEST="true"
            QUICK="true"
            SKIP_INTEGRATION="true"
            SKIP_OWASP="true"
            ;;
        E)
            SKIP_TEST="false"
            QUICK="false"
            SKIP_INTEGRATION="false"
            SKIP_OWASP="false"
            ;;
        W)
            rm -rf fe_extract
            mkdir fe_extract
            cp $PROVISIONING_PATH/local/frontend/archive/class-direct-fe.war fe_extract
                    unzip fe_extract/class-direct-fe.war -d fe_extract > /dev/null 2>&1
            cp -r fe_extract/*.ttf fe_extract/*.js fe_extract/*.html fe_extract/*.svg fe_extract/*.jpg fe_extract/*.woff fe_extract/*.pdf $CLASS_DIRECT_WAR_WEBAPP_DIR
            rm -rf fe_extract
            ;;
        n)
            pushd ./static-file-generator
            gulp clean
            gulp serve
            popd
            break
            ;;

        N)
            pushd ./static-file-generator
            gulp clean
            gulp build && gulp back2pug
            rm ../class-direct-mail/src/main/resources/*.pug
            mv ./dist/pug/* ../class-direct-mail/src/main/resources/
            popd
            break
            ;;

        *)
        echo "Invalid command"
        break
            esac

            if [ -n "$PROJECT" ]
            then
                echo "$PROJECT -> $SKIP"
                if [ "$PROJECT" = "all" ]
                then
                    $CMD $SKIP
                else
                    $CMD $SKIP -pl $PROJECT
                fi
            fi

            #Stopping if the build fails
            rc=$?
        if [[ $rc -ne 0 ]] ; then
            break
        fi
    done
    options
}
options
