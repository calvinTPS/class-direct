const asset = {
    "id": 1,
    "name": "v2 LADY K II",
    "buildDate": 1485878400000,
    "grossTonnage": 2.1,
    "assetType": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "code": null,
      "name": "ABC TYPE",
      "category": null,
      "parentId": null,
      "levelIndication": null,
      "defaultClassDepartment": null,
      "assetTypeGroup": null,
      "_id": "b947e300-d1c1-4e10-a789-9983f771ddfa"
    },
    "assetCategory": null,
    "classStatus": null,
    "ihsAsset": null,
    "flagState": null,
    "stalenessHash": null,
    "classStatusReason": null,
    "isWatched": null,
    "jobCount": null,
    "caseCount": null,
    "reviewDate": null,
    "targetShip": null,
    "isLead": false,
    "leadImo": "1000019",
    "classMaintenanceStatus": null,
    "classNotation": "\"&'<>¡¢£¤¥ ¦§¨©ª«¬®¯°± ²³´µ¶·¸¹º»¼½¾¿À ÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ ×ØÙÚÛÜÝÞßàáâãäåæ çèéêëìíîïðñòóôõö÷øùúûüýþÿŒœŠš Ÿƒˆ˜ΑΒΓΔΕΖΗΘΙΚΛΜΝΞ ΟΠΡΣΤΥΦΧΨΩαβγδ εζ ηθικλμνξοπρ ςστυφχψ ωϑϒϖ–—‘’‚“”„†‡•…‰′ ″‹›‾⁄€ℑ℘ℜ™ℵ←↑→ ↓↔↵⇐⇑⇒⇓⇔∀∂ ∃∅∇∈∉∋∏∑−∗√∝∞∠∧∨∩ ∪∫∴∼≅≈≠≡≤≥⊂⊃⊄⊆⊇⊕⊗⊥⋅⌈⌉⌊ ⌋⟨⟩◊♠♣♥♦¦¬|",
    "machineryClassNotation": null,
    "estimatedBuildDate": null,
    "keelLayingDate": null,
    "builder": null,
    "yardNumber": null,
    "ruleSet": null,
    "previousRuleSet": null,
    "productRuleSet": null,
    "harmonisationDate": null,
    "registeredPort": null,
    "dateOfRegistry": null,
    "assetLifecycleStatus": null,
    "coClassificationSociety": null,
    "customers": null,
    "offices": null,
    "countryOfBuild": null,
    "descriptiveNote": null,
    "callSign": null,
    "hullIndicator": null,
    "classDepartment": null,
    "linkedAsset": null,
    "effectiveDate": 1485878400000,
    "deadWeight": null,
    "serviceRestriction": null,
    "parentPublishVersionId": null,
    "serviceRuleSet": null,
    "pmsApplicable": null,
    "allVersions": null,
    "source": null,
    "mmsiNumber": null,
    "newConstructionContractDate": null,
    "cfoProvenanceIndicator": null,
    "officialNumber": null,
    "breadth": null,
    "loa": null,
    "depth": null,
    "sourceJobLastVisitDate": null,
    "sourceJobCreationDateTime": null,
    "parentHash": null,
    "deleted": false,
    "imoNumber": "1000019",
    "assetTypeHDto": null,
    "assetCategoryDto": null,
    "classStatusDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "In Class (Laid Up)",
      "description": null,
      "_id": "c9b2228b-8480-473e-bf72-0e22756fabb0"
    },
    "flagStateDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "Jersey",
      "flagCode": null,
      "schedulingRegime": null,
      "ports": null,
      "_id": "8e27c1f0-04e0-4f12-ae3e-836c5f3a2a99"
    },
    "ihsAssetDto": {
      "id": "1000019",
      "ihsAsset": null,
      "ihsGroupFleet": null,
      "ihsHist": null,
      "registeredOwner": null,
      "shipManager": null,
      "docCompany": null,
      "technicalManager": null,
      "_id": "1000019"
    },
    "code": null,
    "isMMSService": null,
    "restricted": false,
    "isEOR": false,
    "coClassificationSocietyDto": {
      "id": 2,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "American Bureau Of Shipping",
      "description": null,
      "_id": "2"
    },
    "classMaintenanceStatusDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "Dual",
      "description": null,
      "_id": "fb120c0e-226e-4042-ba74-e8dec6c53cab"
    },
    "customersH": [
      {
        "partyRoles": {
          "id": 4,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "4"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Registered Owner",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": null,
            "addressLine3": "Address Line 3",
            "city": "New York",
            "postcode": "13654",
            "country": "US",
            "emailAddress": "registeredowner@mast.com",
            "website": null,
            "_id": "5e7174a5-e70c-4cdd-9df7-b3f3381be2a2"
          },
          "relationship": null,
          "_id": "cec47bbc-d7f1-44dc-9ba8-5fe1a1e56807"
        }
      },
      {
        "partyRoles": {
          "id": 6,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "6"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Group Owner",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "groupowner@mast.com",
            "website": null,
            "_id": "0712f69e-a265-4dba-af25-34c96c9838b1"
          },
          "relationship": null,
          "_id": "1d4aa7b0-510c-4d7f-a765-a6cd1806637c"
        }
      },
      {
        "partyRoles": {
          "id": 3,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "3"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Ship Manager",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "shipmanager@mast.com",
            "website": null,
            "_id": "462f120a-8428-49b4-a22c-2a67ae2eb5eb"
          },
          "relationship": null,
          "_id": "7d5e1743-5771-45ec-8c37-426fb319d566"
        }
      },
      {
        "partyRoles": {
          "id": 2,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "2"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Ship Operator",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "shipoperator@mast.com",
            "website": null,
            "_id": "991fe005-f19e-4243-86bf-7a30aa8beac6"
          },
          "relationship": null,
          "_id": "e097ce43-0b7d-45fd-8429-b63401ba449c"
        }
      },
      {
        "partyRoles": {
          "id": 10,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "10"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Doc Company",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "doccompany@mast.com",
            "website": null,
            "_id": "206858d5-3bd3-4124-aef5-48fe6df1f51f"
          },
          "relationship": null,
          "_id": "556b8b65-d995-4588-aab5-bb1c3d580b95"
        }
      }
    ],
    "cfoOfficeH": null,
    "hasPostponedService": true,
    "isFavourite": false,
    "assetLifeCycleStatusDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "In service",
      "description": null,
      "_id": "04f9ccbf-6c4f-4b0c-83cc-203c091c3ce4"
    },
    "dueStatusH": {
      "name": "Due soon",
      "id": 2,
      "priority": 3,
      "icon": "duesoon",
      "assetIcon": "overall_due_soon"
    },
    "_id": "1",
    "assetVersionId": null
};

const pmsTasks = {
    "services": null,
    "servicesH": [
      {
        "id": 528,
        "serviceCatalogue": {
          "id": 31,
          "_id": "31"
        },
        "productType": {
          "id": 1,
          "_id": "1"
        },
        "occurrenceNumber": null,
        "items": null,
        "serviceCatalogueH": {
          "id": 31,
          "deleted": false,
          "lastUpdateVersion": 1501811663,
          "name": "Boiler 1",
          "code": "BS",
          "serviceType": {
            "id": 10,
            "_id": "10"
          },
          "surveyType": null,
          "defectCategory": null,
          "serviceRuleset": {
            "id": 2,
            "_id": "2"
          },
          "serviceGroup": {
            "id": 15,
            "_id": "15"
          },
          "schedulingType": {
            "id": 4,
            "_id": "4"
          },
          "continuousIndicator": false,
          "harmonisationType": null,
          "displayOrder": 3,
          "cyclePeriodicity": 36,
          "cyclePeriodicityEditable": true,
          "lowerRangeDateOffsetMonths": 0,
          "upperRangeDateOffsetMonths": 0,
          "approvedPostponementMonths": 0,
          "conditionalPostponementMonths": 3,
          "description": null,
          "categoryLetter": "M",
          "multipleIndicator": true,
          "schedulingRegime": {
            "id": 8,
            "_id": "8"
          },
          "privateIndicator": null,
          "coastalIndicator": null,
          "workItemType": {
            "id": 1,
            "_id": "1"
          },
          "partHeldApplicable": true,
          "espIndicator": null,
          "nameEditable": false,
          "autoCertificate": null,
          "productCatalogue": {
            "id": 24,
            "deleted": false,
            "lastUpdateVersion": 1501811663,
            "name": "Machinery",
            "description": null,
            "displayOrder": 2,
            "productGroup": {
              "id": 3,
              "_id": "3"
            },
            "productType": {
              "id": 1,
              "_id": "1"
            },
            "_id": "24"
          },
          "flags": [],
          "relationships": [],
          "serviceGroupH": null,
          "_id": "31"
        },
        "productTypeH": null,
        "itemsH": [
          {
            "id": 6120,
            "name": "SKEG",
            "uniqueItemId": null,
            "items": null,
            "displayOrder": 6120,
            "attachmentCount": null,
            "tasks": null,
            "tasksH": [
              {
                "id": 1,
                "deleted": false,
                "updatedBy": null,
                "updatedDate": null,
                "actionTakenDate": null,
                "referenceCode": "A1",
                "name": "Task 1",
                "description": "Task 1 description",
                "longDescription": "Task 1 long description",
                "serviceCode": "1",
                "workItemType": {
                  "id": 1,
                  "_id": "1"
                },
                "checklist": null,
                "workItemAction": {
                  "id": 1386,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Examine Draught Marks Draught marks or reliable indicating system",
                  "description": null,
                  "referenceCode": "T1386",
                  "taskCategory": null,
                  "weighting": 4,
                  "creditAllowedX": true,
                  "creditAllowedW": false,
                  "creditAllowedE": true,
                  "creditAllowedC": true,
                  "setsCompletion": false,
                  "_id": "1386"
                },
                "attributes": [],
                "attributeMandatory": false,
                "dueDate": "2016-05-23T00:00:00Z",
                "dueDateManual": null,
                "notes": "Task 1 notes",
                "codicil": null,
                "attachmentRequired": false,
                "itemOrder": null,
                "resolutionDate": null,
                "resolutionStatus": null,
                "classSociety": {
                  "id": 1,
                  "_id": "1"
                },
                "assignedTo": {
                  "id": 1,
                  "deleted": null,
                  "lastUpdateVersion": null,
                  "oneWorldNumber": null,
                  "firstName": null,
                  "lastName": null,
                  "name": null,
                  "emailAddress": null,
                  "networkUserId": null,
                  "legalEntity": null,
                  "_id": "1"
                },
                "assignedDate": "2016-06-21T00:00:00Z",
                "assignedDateManual": null,
                "resolvedBy": null,
                "taskNumber": "000001-ADFGOS-000002",
                "parent": null,
                "dueStatus": {
                  "id": 3,
                  "_id": "3"
                },
                "creditedByJob": {
                  "id": 110021,
                  "_id": "110021"
                },
                "postponementDate": "2016-06-25T00:00:00Z",
                "postponementType": null,
                "postponedOnJob": null,
                "pmsApplicable": true,
                "pmsCreditDate": null,
                "pmsCredited": false,
                "workItemScopeStatus": null,
                "approvedByJob": null,
                "expiryDate": null,
                "parentHash": null,
                "sourceJobLastVisitDate": null,
                "sourceJobCreationDateTime": null,
                "attachmentCount": null,
                "stalenessHash": "0e93fabdda4ac4812c7e2727ea0a422b8e2658bc",
                "assetItem": {
                  "id": 6120,
                  "_id": "6120"
                },
                "survey": null,
                "scheduledService": {
                  "id": 528,
                  "_id": "528"
                },
                "conditionalParent": null,
                "workItem": null,
                "dueStatusH": {
                  "name": "Overdue",
                  "id": 4,
                  "priority": 1,
                  "icon": "overdue",
                  "assetIcon": "overall_overdue"
                },
                "workItemTypeH": {
                  "id": 1,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Task",
                  "description": null,
                  "_id": "1"
                },
                "postponementTypeH": null,
                "resolutionStatusH": null,
                "checklistH": null,
                "_id": "1",
                "_dmID": null
              },
              {
                "id": 2,
                "deleted": false,
                "updatedBy": null,
                "updatedDate": null,
                "actionTakenDate": null,
                "referenceCode": "A2",
                "name": "Task 2",
                "description": "Task 2 description",
                "longDescription": "Task 2 long description",
                "serviceCode": "1",
                "workItemType": {
                  "id": 1,
                  "_id": "1"
                },
                "checklist": null,
                "workItemAction": {
                  "id": 1386,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Examine Draught Marks Draught marks or reliable indicating system",
                  "description": null,
                  "referenceCode": "T1386",
                  "taskCategory": null,
                  "weighting": 4,
                  "creditAllowedX": true,
                  "creditAllowedW": false,
                  "creditAllowedE": true,
                  "creditAllowedC": true,
                  "setsCompletion": false,
                  "_id": "1386"
                },
                "attributes": [],
                "attributeMandatory": false,
                "dueDate": "2016-05-23T00:00:00Z",
                "dueDateManual": null,
                "notes": "Task 2 notes",
                "codicil": null,
                "attachmentRequired": false,
                "itemOrder": null,
                "resolutionDate": null,
                "resolutionStatus": null,
                "classSociety": {
                  "id": 1,
                  "_id": "1"
                },
                "assignedTo": {
                  "id": 1,
                  "deleted": null,
                  "lastUpdateVersion": null,
                  "oneWorldNumber": null,
                  "firstName": null,
                  "lastName": null,
                  "name": null,
                  "emailAddress": null,
                  "networkUserId": null,
                  "legalEntity": null,
                  "_id": "1"
                },
                "assignedDate": "2016-06-21T00:00:00Z",
                "assignedDateManual": null,
                "resolvedBy": null,
                "taskNumber": "000001-BKL72R-000003",
                "parent": null,
                "dueStatus": {
                  "id": 3,
                  "_id": "3"
                },
                "creditedByJob": {
                  "id": 110021,
                  "_id": "110021"
                },
                "postponementDate": null,
                "postponementType": null,
                "postponedOnJob": null,
                "pmsApplicable": true,
                "pmsCreditDate": null,
                "pmsCredited": false,
                "workItemScopeStatus": null,
                "approvedByJob": null,
                "expiryDate": null,
                "parentHash": null,
                "sourceJobLastVisitDate": null,
                "sourceJobCreationDateTime": null,
                "attachmentCount": null,
                "stalenessHash": "7ca0d33bbc10f15f8c9489f89a40015f8306d453",
                "assetItem": {
                  "id": 6120,
                  "_id": "6120"
                },
                "survey": null,
                "scheduledService": {
                  "id": 528,
                  "_id": "528"
                },
                "conditionalParent": null,
                "workItem": null,
                "dueStatusH": {
                  "name": "Overdue",
                  "id": 4,
                  "priority": 1,
                  "icon": "overdue",
                  "assetIcon": "overall_overdue"
                },
                "workItemTypeH": {
                  "id": 1,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Task",
                  "description": null,
                  "_id": "1"
                },
                "postponementTypeH": null,
                "resolutionStatusH": null,
                "checklistH": null,
                "_id": "2",
                "_dmID": null
              },
              {
                "id": 3,
                "deleted": false,
                "updatedBy": null,
                "updatedDate": null,
                "actionTakenDate": null,
                "referenceCode": "A3",
                "name": "Task 3",
                "description": "Task 3 description",
                "longDescription": "Task 3 long description",
                "serviceCode": "1",
                "workItemType": {
                  "id": 1,
                  "_id": "1"
                },
                "checklist": null,
                "workItemAction": {
                  "id": 1386,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Examine Draught Marks Draught marks or reliable indicating system",
                  "description": null,
                  "referenceCode": "T1386",
                  "taskCategory": null,
                  "weighting": 4,
                  "creditAllowedX": true,
                  "creditAllowedW": false,
                  "creditAllowedE": true,
                  "creditAllowedC": true,
                  "setsCompletion": false,
                  "_id": "1386"
                },
                "attributes": [],
                "attributeMandatory": false,
                "dueDate": "2016-05-23T00:00:00Z",
                "dueDateManual": null,
                "notes": "Task 3 notes",
                "codicil": null,
                "attachmentRequired": false,
                "itemOrder": null,
                "resolutionDate": null,
                "resolutionStatus": null,
                "classSociety": {
                  "id": 1,
                  "_id": "1"
                },
                "assignedTo": {
                  "id": 1,
                  "deleted": null,
                  "lastUpdateVersion": null,
                  "oneWorldNumber": null,
                  "firstName": null,
                  "lastName": null,
                  "name": null,
                  "emailAddress": null,
                  "networkUserId": null,
                  "legalEntity": null,
                  "_id": "1"
                },
                "assignedDate": "2016-06-21T00:00:00Z",
                "assignedDateManual": null,
                "resolvedBy": null,
                "taskNumber": "000001-RRWX54-000003",
                "parent": null,
                "dueStatus": {
                  "id": 3,
                  "_id": "3"
                },
                "creditedByJob": {
                  "id": 110021,
                  "_id": "110021"
                },
                "postponementDate": null,
                "postponementType": null,
                "postponedOnJob": null,
                "pmsApplicable": true,
                "pmsCreditDate": null,
                "pmsCredited": false,
                "workItemScopeStatus": null,
                "approvedByJob": null,
                "expiryDate": null,
                "parentHash": null,
                "sourceJobLastVisitDate": null,
                "sourceJobCreationDateTime": null,
                "attachmentCount": null,
                "stalenessHash": "139d6c45f8294a1a7175f7d95c2b8f6b94f6d0b1",
                "assetItem": {
                  "id": 6120,
                  "_id": "6120"
                },
                "survey": null,
                "scheduledService": {
                  "id": 528,
                  "_id": "528"
                },
                "conditionalParent": null,
                "workItem": null,
                "dueStatusH": {
                  "name": "Overdue",
                  "id": 4,
                  "priority": 1,
                  "icon": "overdue",
                  "assetIcon": "overall_overdue"
                },
                "workItemTypeH": {
                  "id": 1,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Task",
                  "description": null,
                  "_id": "1"
                },
                "postponementTypeH": null,
                "resolutionStatusH": null,
                "checklistH": null,
                "_id": "3",
                "_dmID": null
              },
              {
                "id": 4,
                "deleted": false,
                "updatedBy": null,
                "updatedDate": null,
                "actionTakenDate": null,
                "referenceCode": "A4",
                "name": "Task 4",
                "description": "Task 4 description",
                "longDescription": "Task 4 long description",
                "serviceCode": "1",
                "workItemType": {
                  "id": 1,
                  "_id": "1"
                },
                "checklist": null,
                "workItemAction": {
                  "id": 1386,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Examine Draught Marks Draught marks or reliable indicating system",
                  "description": null,
                  "referenceCode": "T1386",
                  "taskCategory": null,
                  "weighting": 4,
                  "creditAllowedX": true,
                  "creditAllowedW": false,
                  "creditAllowedE": true,
                  "creditAllowedC": true,
                  "setsCompletion": false,
                  "_id": "1386"
                },
                "attributes": [],
                "attributeMandatory": false,
                "dueDate": "2016-05-23T00:00:00Z",
                "dueDateManual": null,
                "notes": "Task 4 notes",
                "codicil": null,
                "attachmentRequired": false,
                "itemOrder": null,
                "resolutionDate": null,
                "resolutionStatus": null,
                "classSociety": {
                  "id": 1,
                  "_id": "1"
                },
                "assignedTo": {
                  "id": 1,
                  "deleted": null,
                  "lastUpdateVersion": null,
                  "oneWorldNumber": null,
                  "firstName": null,
                  "lastName": null,
                  "name": null,
                 "emailAddress": null,
                  "networkUserId": null,
                  "legalEntity": null,
                  "_id": "1"
                },
                "assignedDate": "2017-01-05T00:00:00Z",
                "assignedDateManual": null,
                "resolvedBy": null,
                "taskNumber": "000001-RRWX54-000003",
                "parent": null,
                "dueStatus": {
                  "id": 3,
                  "_id": "3"
                },
                "creditedByJob": {
                  "id": 110021,
                  "_id": "110021"
                },
                "postponementDate": null,
                "postponementType": null,
                "postponedOnJob": null,
                "pmsApplicable": true,
                "pmsCreditDate": null,
                "pmsCredited": false,
                "workItemScopeStatus": null,
                "approvedByJob": null,
                "expiryDate": null,
                "parentHash": null,
                "sourceJobLastVisitDate": null,
                "sourceJobCreationDateTime": null,
                "attachmentCount": null,
                "stalenessHash": "d49ee5103578ea286dd91ea0805bcf6dee90f6a1",
                "assetItem": {
                  "id": 6120,
                  "_id": "6120"
                },
                "survey": null,
                "scheduledService": {
                  "id": 528,
                  "_id": "528"
                },
                "conditionalParent": null,
                "workItem": null,
                "dueStatusH": {
                  "name": "Overdue",
                  "id": 4,
                  "priority": 1,
                  "icon": "overdue",
                  "assetIcon": "overall_overdue"
                },
                "workItemTypeH": {
                  "id": 1,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Task",
                  "description": null,
                  "_id": "1"
                },
                "postponementTypeH": null,
                "resolutionStatusH": null,
                "checklistH": null,
                "_id": "4",
                "_dmID": null
              },
              {
                "id": 5,
                "deleted": false,
                "updatedBy": null,
                "updatedDate": null,
                "actionTakenDate": null,
                "referenceCode": "A5",
                "name": "Task 5",
                "description": "Task 5 description",
                "longDescription": "Task 5 long description",
                "serviceCode": "1",
                "workItemType": {
                  "id": 1,
                  "_id": "1"
                },
                "checklist": null,
                "workItemAction": {
                  "id": 1386,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Examine Draught Marks Draught marks or reliable indicating system",
                  "description": null,
                  "referenceCode": "T1386",
                  "taskCategory": null,
                  "weighting": 4,
                  "creditAllowedX": true,
                  "creditAllowedW": false,
                  "creditAllowedE": true,
                  "creditAllowedC": true,
                  "setsCompletion": false,
                  "_id": "1386"
                },
                "attributes": [],
                "attributeMandatory": false,
                "dueDate": "2016-05-23T00:00:00Z",
                "dueDateManual": null,
                "notes": "Task 5 notes",
                "codicil": null,
                "attachmentRequired": false,
                "itemOrder": null,
                "resolutionDate": null,
                "resolutionStatus": null,
                "classSociety": {
                  "id": 1,
                  "_id": "1"
                },
                "assignedTo": {
                  "id": 1,
                  "deleted": null,
                  "lastUpdateVersion": null,
                  "oneWorldNumber": null,
                  "firstName": null,
                  "lastName": null,
                  "name": null,
                  "emailAddress": null,
                  "networkUserId": null,
                  "legalEntity": null,
                  "_id": "1"
                },
                "assignedDate": "2017-01-05T00:00:00Z",
                "assignedDateManual": null,
                "resolvedBy": null,
                "taskNumber": "000001-RRWX55-000004",
                "parent": null,
                "dueStatus": {
                  "id": 3,
                  "_id": "3"
                },
                "creditedByJob": {
                  "id": 110021,
                  "_id": "110021"
                },
                "postponementDate": null,
                "postponementType": null,
                "postponedOnJob": null,
                "pmsApplicable": true,
                "pmsCreditDate": null,
                "pmsCredited": false,
                "workItemScopeStatus": null,
                "approvedByJob": null,
                "expiryDate": null,
                "parentHash": null,
                "sourceJobLastVisitDate": null,
                "sourceJobCreationDateTime": null,
                "attachmentCount": null,
                "stalenessHash": "c01b182337e57b410f5f8407860b1ca093497fc1",
                "assetItem": {
                  "id": 6120,
                  "_id": "6120"
                },
                "survey": null,
                "scheduledService": {
                  "id": 528,
                  "_id": "528"
                },
                "conditionalParent": null,
                "workItem": null,
                "dueStatusH": {
                  "name": "Overdue",
                  "id": 4,
                  "priority": 1,
                  "icon": "overdue",
                  "assetIcon": "overall_overdue"
                },
                "workItemTypeH": {
                  "id": 1,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Task",
                  "description": null,
                  "_id": "1"
                },
                "postponementTypeH": null,
                "resolutionStatusH": null,
                "checklistH": null,
                "_id": "5",
                "_dmID": null
              },
              {
                "id": 6,
                "deleted": false,
                "updatedBy": null,
                "updatedDate": null,
                "actionTakenDate": null,
                "referenceCode": "A6",
                "name": "Task 6",
                "description": "Task 6 description",
                "longDescription": "Task 6 long description",
                "serviceCode": "1",
                "workItemType": {
                  "id": 1,
                  "_id": "1"
                },
                "checklist": null,
                "workItemAction": {
                  "id": 1386,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Examine Draught Marks Draught marks or reliable indicating system",
                  "description": null,
                  "referenceCode": "T1386",
                  "taskCategory": null,
                  "weighting": 4,
                  "creditAllowedX": true,
                  "creditAllowedW": false,
                  "creditAllowedE": true,
                  "creditAllowedC": true,
                  "setsCompletion": false,
                  "_id": "1386"
                },
                "attributes": [],
                "attributeMandatory": false,
                "dueDate": "2016-05-23T00:00:00Z",
                "dueDateManual": null,
                "notes": "Task 6 notes",
                "codicil": null,
                "attachmentRequired": false,
                "itemOrder": null,
                "resolutionDate": null,
                "resolutionStatus": null,
                "classSociety": {
                  "id": 1,
                  "_id": "1"
                },
                "assignedTo": {
                  "id": 1,
                  "deleted": null,
                  "lastUpdateVersion": null,
                  "oneWorldNumber": null,
                  "firstName": null,
                  "lastName": null,
                  "name": null,
                  "emailAddress": null,
                  "networkUserId": null,
                  "legalEntity": null,
                  "_id": "1"
                },
                "assignedDate": "2017-01-05T00:00:00Z",
                "assignedDateManual": null,
                "resolvedBy": null,
                "taskNumber": "000001-RRWX56-000005",
                "parent": null,
                "dueStatus": {
                  "id": 3,
                  "_id": "3"
                },
                "creditedByJob": {
                  "id": 110021,
                  "_id": "110021"
                },
                "postponementDate": null,
                "postponementType": null,
                "postponedOnJob": null,
                "pmsApplicable": true,
                "pmsCreditDate": null,
                "pmsCredited": false,
                "workItemScopeStatus": null,
                "approvedByJob": null,
                "expiryDate": null,
                "parentHash": null,
                "sourceJobLastVisitDate": null,
                "sourceJobCreationDateTime": null,
                "attachmentCount": null,
                "stalenessHash": "db52e3d63de20f73102088c9d0a5a108172369a9",
                "assetItem": {
                  "id": 6120,
                  "_id": "6120"
                },
                "survey": null,
                "scheduledService": {
                  "id": 528,
                  "_id": "528"
                },
                "conditionalParent": null,
                "workItem": null,
                "dueStatusH": {
                  "name": "Overdue",
                  "id": 4,
                  "priority": 1,
                  "icon": "overdue",
                  "assetIcon": "overall_overdue"
                },
                "workItemTypeH": {
                  "id": 1,
                  "deleted": false,
                  "lastUpdateVersion": 1501811663,
                  "name": "Task",
                  "description": null,
                  "_id": "1"
                },
                "postponementTypeH": null,
                "resolutionStatusH": null,
                "checklistH": null,
                "_id": "6",
                "_dmID": null
              }
            ],
            "itemsH": [],
            "_id": "6120"
          }
        ],
        "_id": "528"
      }
    ]
};

module.exports = {
  asset: asset,
  pmsTasks: pmsTasks,
}
