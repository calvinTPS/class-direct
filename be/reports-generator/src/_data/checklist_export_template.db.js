const asset = {
    "id": 1,
    "name": "v2 LADY K II",
    "buildDate": 1485878400000,
    "grossTonnage": 2.1,
    "assetType": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "code": null,
      "name": "ABC TYPE",
      "category": null,
      "parentId": null,
      "levelIndication": null,
      "defaultClassDepartment": null,
      "assetTypeGroup": null,
      "_id": "b947e300-d1c1-4e10-a789-9983f771ddfa"
    },
    "assetCategory": null,
    "classStatus": null,
    "ihsAsset": null,
    "flagState": null,
    "stalenessHash": null,
    "classStatusReason": null,
    "isWatched": null,
    "jobCount": null,
    "caseCount": null,
    "reviewDate": null,
    "targetShip": null,
    "isLead": false,
    "leadImo": "1000019",
    "classMaintenanceStatus": null,
    "classNotation": "\"&'<>¡¢£¤¥ ¦§¨©ª«¬®¯°± ²³´µ¶·¸¹º»¼½¾¿À ÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ ×ØÙÚÛÜÝÞßàáâãäåæ çèéêëìíîïðñòóôõö÷øùúûüýþÿŒœŠš Ÿƒˆ˜ΑΒΓΔΕΖΗΘΙΚΛΜΝΞ ΟΠΡΣΤΥΦΧΨΩαβγδ εζ ηθικλμνξοπρ ςστυφχψ ωϑϒϖ–—‘’‚“”„†‡•…‰′ ″‹›‾⁄€ℑ℘ℜ™ℵ←↑→ ↓↔↵⇐⇑⇒⇓⇔∀∂ ∃∅∇∈∉∋∏∑−∗√∝∞∠∧∨∩ ∪∫∴∼≅≈≠≡≤≥⊂⊃⊄⊆⊇⊕⊗⊥⋅⌈⌉⌊ ⌋⟨⟩◊♠♣♥♦¦¬|",
    "machineryClassNotation": null,
    "estimatedBuildDate": null,
    "keelLayingDate": null,
    "builder": null,
    "yardNumber": null,
    "ruleSet": null,
    "previousRuleSet": null,
    "productRuleSet": null,
    "harmonisationDate": null,
    "registeredPort": null,
    "dateOfRegistry": null,
    "assetLifecycleStatus": null,
    "coClassificationSociety": null,
    "customers": null,
    "offices": null,
    "countryOfBuild": null,
    "descriptiveNote": null,
    "callSign": null,
    "hullIndicator": null,
    "classDepartment": null,
    "linkedAsset": null,
    "effectiveDate": 1485878400000,
    "deadWeight": null,
    "serviceRestriction": null,
    "parentPublishVersionId": null,
    "serviceRuleSet": null,
    "pmsApplicable": null,
    "allVersions": null,
    "source": null,
    "mmsiNumber": null,
    "newConstructionContractDate": null,
    "cfoProvenanceIndicator": null,
    "officialNumber": null,
    "breadth": null,
    "loa": null,
    "depth": null,
    "sourceJobLastVisitDate": null,
    "sourceJobCreationDateTime": null,
    "parentHash": null,
    "deleted": false,
    "imoNumber": "1000019",
    "assetTypeHDto": null,
    "assetCategoryDto": null,
    "classStatusDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "In Class (Laid Up)",
      "description": null,
      "_id": "c9b2228b-8480-473e-bf72-0e22756fabb0"
    },
    "flagStateDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "Jersey",
      "flagCode": null,
      "schedulingRegime": null,
      "ports": null,
      "_id": "8e27c1f0-04e0-4f12-ae3e-836c5f3a2a99"
    },
    "ihsAssetDto": {
      "id": "1000019",
      "ihsAsset": null,
      "ihsGroupFleet": null,
      "ihsHist": null,
      "registeredOwner": null,
      "shipManager": null,
      "docCompany": null,
      "technicalManager": null,
      "_id": "1000019"
    },
    "code": null,
    "isMMSService": null,
    "restricted": false,
    "isEOR": false,
    "coClassificationSocietyDto": {
      "id": 2,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "American Bureau Of Shipping",
      "description": null,
      "_id": "2"
    },
    "classMaintenanceStatusDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "Dual",
      "description": null,
      "_id": "fb120c0e-226e-4042-ba74-e8dec6c53cab"
    },
    "customersH": [
      {
        "partyRoles": {
          "id": 4,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "4"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Registered Owner",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": null,
            "addressLine3": "Address Line 3",
            "city": "New York",
            "postcode": "13654",
            "country": "US",
            "emailAddress": "registeredowner@mast.com",
            "website": null,
            "_id": "5e7174a5-e70c-4cdd-9df7-b3f3381be2a2"
          },
          "relationship": null,
          "_id": "cec47bbc-d7f1-44dc-9ba8-5fe1a1e56807"
        }
      },
      {
        "partyRoles": {
          "id": 6,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "6"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Group Owner",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "groupowner@mast.com",
            "website": null,
            "_id": "0712f69e-a265-4dba-af25-34c96c9838b1"
          },
          "relationship": null,
          "_id": "1d4aa7b0-510c-4d7f-a765-a6cd1806637c"
        }
      },
      {
        "partyRoles": {
          "id": 3,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "3"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Ship Manager",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "shipmanager@mast.com",
            "website": null,
            "_id": "462f120a-8428-49b4-a22c-2a67ae2eb5eb"
          },
          "relationship": null,
          "_id": "7d5e1743-5771-45ec-8c37-426fb319d566"
        }
      },
      {
        "partyRoles": {
          "id": 2,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "2"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Ship Operator",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "shipoperator@mast.com",
            "website": null,
            "_id": "991fe005-f19e-4243-86bf-7a30aa8beac6"
          },
          "relationship": null,
          "_id": "e097ce43-0b7d-45fd-8429-b63401ba449c"
        }
      },
      {
        "partyRoles": {
          "id": 10,
          "deleted": null,
          "lastUpdateVersion": null,
          "name": null,
          "description": null,
          "_id": "10"
        },
        "party": {
          "id": null,
          "customer": {
            "id": null,
            "deleted": null,
            "lastUpdateVersion": null,
            "name": "MAST Doc Company",
            "imoNumber": null,
            "cdhCustomerId": null,
            "jdeReferenceNumber": null,
            "phoneNumber": "1111111111",
            "faxNumber": null,
            "office": null,
            "addressLine1": "Address Line 1",
            "addressLine2": "Address Line 2",
            "addressLine3": null,
            "city": null,
            "postcode": null,
            "country": null,
            "emailAddress": "doccompany@mast.com",
            "website": null,
            "_id": "206858d5-3bd3-4124-aef5-48fe6df1f51f"
          },
          "relationship": null,
          "_id": "556b8b65-d995-4588-aab5-bb1c3d580b95"
        }
      }
    ],
    "cfoOfficeH": null,
    "hasPostponedService": true,
    "isFavourite": false,
    "assetLifeCycleStatusDto": {
      "id": null,
      "deleted": null,
      "lastUpdateVersion": null,
      "name": "In service",
      "description": null,
      "_id": "04f9ccbf-6c4f-4b0c-83cc-203c091c3ce4"
    },
    "dueStatusH": {
      "name": "Due soon",
      "id": 2,
      "priority": 3,
      "icon": "duesoon",
      "assetIcon": "overall_due_soon"
    },
    "_id": "1",
    "assetVersionId": null
};

const checkLists = [
  {
    "id": 1,
    "deleted": null,
    "lastUpdateVersion": null,
    "name": "Survey Preparation",
    "description": null,
    "checklistSubgrouplist": [
      {
        "id": 4,
        "deleted": null,
        "lastUpdateVersion": null,
        "name": "Documentation - Are type approval certificates onboard (Oil)",
        "description": null,
        "checklistItems": [
          {
            "id": 10009,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A100",
            "name": "CHECKLIST GROUP 2",
            "description": "CHECKLIST GROUP 2 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 2 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 2,
              "_id": "2"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "a2d74d7beb73f150749e7aba9f92e5c627a9b762",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": null,
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10009",
            "_dmID": null
          },
          {
            "id": 10010,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A100",
            "name": "CHECKLIST 4",
            "description": "CHECKLIST 4 DESCRIPTION",
            "longDescription": "CHECKLIST 4 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 3,
              "_id": "3"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "21ef985aef45ca491e27d93b40564a317ee216fd",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10009,
              "_id": "10009"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10010",
            "_dmID": null
          },
          {
            "id": 10011,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A101",
            "name": "CHECKLIST GROUP 3",
            "description": "CHECKLIST GROUP 3 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 3 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 4,
              "_id": "4"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "22e1383ac0485aaaf5fb54bea7e59a271a305031",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10009,
              "_id": "10009"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10011",
            "_dmID": null
          },
          {
            "id": 10012,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A101",
            "name": "CHECKLIST 5",
            "description": "CHECKLIST 5 DESCRIPTION",
            "longDescription": "CHECKLIST 5 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 5,
              "_id": "5"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "fc70c9837286618bfb663af41f86ac46a0189d16",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10011,
              "_id": "10011"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10012",
            "_dmID": null
          },
          {
            "id": 10013,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A102",
            "name": "CHECKLIST GROUP 4",
            "description": "CHECKLIST GROUP 4 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 4 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 6,
              "_id": "6"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "e48ab78adf8fc5d620fea41fdbf664b19b42c4c9",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10011,
              "_id": "10011"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10013",
            "_dmID": null
          },
          {
            "id": 10014,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A102",
            "name": "CHECKLIST 6",
            "description": "CHECKLIST 6 DESCRIPTION",
            "longDescription": "CHECKLIST 6 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 7,
              "_id": "7"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "ac2dfa368f639ce69d94804ae445e41f05b453e1",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10013,
              "_id": "10013"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10014",
            "_dmID": null
          },
          {
            "id": 10006,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A99",
            "name": "CHECKLIST GROUP 1",
            "description": "CHECKLIST GROUP 1 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 1 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 1,
              "_id": "1"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "222f03df58923a73603f84edeacab73362ebdf43",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": null,
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10006",
            "_dmID": null
          },
          {
            "id": 10007,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A99",
            "name": "CHECKLIST 2",
            "description": "CHECKLIST 2 DESCRIPTION",
            "longDescription": "CHECKLIST 2 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 1,
              "_id": "1"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "8b25c9b457344b3154b658510b767ea7ec042227",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10006,
              "_id": "10006"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10007",
            "_dmID": null
          },
          {
            "id": 10008,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A99",
            "name": "CHECKLIST 3",
            "description": "CHECKLIST 3 DESCRIPTION",
            "longDescription": "CHECKLIST 3 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 1,
              "_id": "1"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "1546730153554e2cc36049b4415e3e83a9325efd",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10006,
              "_id": "10006"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10008",
            "_dmID": null
          }
        ],
        "_id": "4"
      }
    ],
    "_id": "1"
  },
  {
    "id": 1,
    "deleted": null,
    "lastUpdateVersion": null,
    "name": "Survey Preparation",
    "description": null,
    "checklistSubgrouplist": [
      {
        "id": 4,
        "deleted": null,
        "lastUpdateVersion": null,
        "name": "Documentation - Are type approval certificates onboard (Oil)",
        "description": null,
        "checklistItems": [
          {
            "id": 10009,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A100",
            "name": "CHECKLIST GROUP 2",
            "description": "CHECKLIST GROUP 2 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 2 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 2,
              "_id": "2"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "a2d74d7beb73f150749e7aba9f92e5c627a9b762",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": null,
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10009",
            "_dmID": null
          },
          {
            "id": 10010,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A100",
            "name": "CHECKLIST 4",
            "description": "CHECKLIST 4 DESCRIPTION",
            "longDescription": "CHECKLIST 4 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 3,
              "_id": "3"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "21ef985aef45ca491e27d93b40564a317ee216fd",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10009,
              "_id": "10009"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10010",
            "_dmID": null
          },
          {
            "id": 10011,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A101",
            "name": "CHECKLIST GROUP 3",
            "description": "CHECKLIST GROUP 3 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 3 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 4,
              "_id": "4"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "22e1383ac0485aaaf5fb54bea7e59a271a305031",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10009,
              "_id": "10009"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10011",
            "_dmID": null
          },
          {
            "id": 10012,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A101",
            "name": "CHECKLIST 5",
            "description": "CHECKLIST 5 DESCRIPTION",
            "longDescription": "CHECKLIST 5 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 5,
              "_id": "5"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "fc70c9837286618bfb663af41f86ac46a0189d16",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10011,
              "_id": "10011"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10012",
            "_dmID": null
          },
          {
            "id": 10013,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A102",
            "name": "CHECKLIST GROUP 4",
            "description": "CHECKLIST GROUP 4 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 4 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 6,
              "_id": "6"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "e48ab78adf8fc5d620fea41fdbf664b19b42c4c9",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10011,
              "_id": "10011"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10013",
            "_dmID": null
          },
          {
            "id": 10014,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A102",
            "name": "CHECKLIST 6",
            "description": "CHECKLIST 6 DESCRIPTION",
            "longDescription": "CHECKLIST 6 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 7,
              "_id": "7"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "ac2dfa368f639ce69d94804ae445e41f05b453e1",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10013,
              "_id": "10013"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10014",
            "_dmID": null
          },
          {
            "id": 10006,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A99",
            "name": "CHECKLIST GROUP 1",
            "description": "CHECKLIST GROUP 1 DESCRIPTION",
            "longDescription": "CHECKLIST GROUP 1 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 1,
              "_id": "1"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "222f03df58923a73603f84edeacab73362ebdf43",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": null,
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10006",
            "_dmID": null
          },
          {
            "id": 10007,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A99",
            "name": "CHECKLIST 2",
            "description": "CHECKLIST 2 DESCRIPTION",
            "longDescription": "CHECKLIST 2 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 1,
              "_id": "1"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "8b25c9b457344b3154b658510b767ea7ec042227",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10006,
              "_id": "10006"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10007",
            "_dmID": null
          },
          {
            "id": 10008,
            "deleted": false,
            "updatedBy": null,
            "updatedDate": null,
            "actionTakenDate": null,
            "referenceCode": "A99",
            "name": "CHECKLIST 3",
            "description": "CHECKLIST 3 DESCRIPTION",
            "longDescription": "CHECKLIST 3 LONG DESCRIPTION",
            "serviceCode": "1",
            "workItemType": {
              "id": 2,
              "_id": "2"
            },
            "checklist": {
              "id": 1,
              "_id": "1"
            },
            "workItemAction": {
              "id": 1,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Migration Only",
              "description": null,
              "referenceCode": "T1",
              "taskCategory": null,
              "weighting": 0,
              "creditAllowedX": true,
              "creditAllowedW": false,
              "creditAllowedE": true,
              "creditAllowedC": false,
              "setsCompletion": false,
              "_id": "1"
            },
            "attributes": [

            ],
            "attributeMandatory": false,
            "dueDate": "2016-05-23T00:00:00Z",
            "dueDateManual": null,
            "notes": "CHECKLIST 1 NOTES",
            "codicil": null,
            "attachmentRequired": false,
            "itemOrder": null,
            "resolutionDate": null,
            "resolutionStatus": null,
            "classSociety": {
              "id": 1,
              "_id": "1"
            },
            "assignedTo": {
              "id": 1,
              "deleted": null,
              "lastUpdateVersion": null,
              "oneWorldNumber": null,
              "firstName": null,
              "lastName": null,
              "name": null,
              "emailAddress": null,
              "networkUserId": null,
              "legalEntity": null,
              "_id": "1"
            },
            "assignedDate": "2016-06-21T00:00:00Z",
            "assignedDateManual": null,
            "resolvedBy": null,
            "taskNumber": "000001-XXXX-000004",
            "parent": null,
            "dueStatus": {
              "id": 3,
              "_id": "3"
            },
            "creditedByJob": {
              "id": 4,
              "_id": "4"
            },
            "postponementDate": null,
            "postponementType": null,
            "postponedOnJob": null,
            "pmsApplicable": false,
            "pmsCreditDate": null,
            "pmsCredited": false,
            "workItemScopeStatus": null,
            "approvedByJob": null,
            "expiryDate": null,
            "parentHash": null,
            "sourceJobLastVisitDate": null,
            "sourceJobCreationDateTime": null,
            "attachmentCount": null,
            "stalenessHash": "1546730153554e2cc36049b4415e3e83a9325efd",
            "assetItem": {
              "id": 86,
              "_id": "86"
            },
            "survey": null,
            "scheduledService": {
              "id": 325,
              "_id": "325"
            },
            "conditionalParent": {
              "id": 10006,
              "_id": "10006"
            },
            "workItem": null,
            "dueStatusH": {
              "name": "Overdue",
              "id": 4,
              "priority": 1,
              "icon": "overdue",
              "assetIcon": "overall_overdue"
            },
            "workItemTypeH": {
              "id": 2,
              "deleted": false,
              "lastUpdateVersion": 1501811663,
              "name": "Checklist",
              "description": null,
              "_id": "2"
            },
            "postponementTypeH": null,
            "resolutionStatusH": null,
            "checklistH": null,
            "_id": "10008",
            "_dmID": null
          }
        ],
        "_id": "4"
      }
    ],
    "_id": "1"
  }
];

const service = {
  "serviceCatalogueH": {
    "name": "Service Name",
    "code": "ABCDEFG"
  }
};

module.exports = {
  asset: asset,
  checkLists: checkLists,
  service: service,
}
