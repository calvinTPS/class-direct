const typeOf = require('kind-of');
const isObject = require('isobject');

function rename(obj, fn) {
  if (!isObject(obj)) {
    throw new TypeError('expected an object');
  }

  if (typeof fn !== 'function') {
    return obj;
  }

  const keys = Object.keys(obj);
  const result = {};

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const val = obj[key];
    // console.log(i, key);
    const str = fn(key, val);
    let newKey = '';
    if (typeof str === 'string' && str !== '') {
      newKey = str;
    }
    const funcval = function () {
      this.isObject = isObject(val);
      return val;
    };

    // console.log(newKey, funcval());
    result[newKey] = funcval;
  }
  // console.log(result);
  return result;
};

function renameDeep(obj, cb) {
  const type = typeOf(obj);

  if (type !== 'object' && type !== 'array') {
    throw new Error('expected an object');
  }

  let res = [];
  if (type === 'object') {
    obj = rename(obj, cb);
    res = {};
  }

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const val = obj[key];
      const shouldProceed = isObject(val());
      if (shouldProceed || typeOf(val) === 'object' || (typeof(val) === 'function' && typeOf(val()) === 'array')) {
        const tempArray = [];
        if (typeOf(val()) === 'array') {
          val().forEach(function(item, index) {
            tempArray.push(renameDeep(item, cb));
          })
          res[key] = () => tempArray;
        } else res[key] = () => renameDeep(val(), cb);
      } else {
        res[key] = val;
      }
    }
  }
  return res;
};

/**
 *
 * Expose `renameDeep`
 */

const asset = {
  "id": 1,
  "name": "v2 LADY K II",
  "buildDate": 1485878400000,
  "grossTonnage": 2.1,
  "assetType": {
    "id": null,
    "deleted": null,
    "lastUpdateVersion": null,
    "code": null,
    "name": "ABC TYPE",
    "category": null,
    "parentId": null,
    "levelIndication": null,
    "defaultClassDepartment": null,
    "assetTypeGroup": null,
    "_id": "b947e300-d1c1-4e10-a789-9983f771ddfa"
  },
}


const testData = {
  "title": "Pug-Sass Starter",
  "welcome": "LR Class Direct Reports Engine | ¦ ¬",
  "link": "https://classdirect.lr.org",
  "assets": [
    {
      "id": 1,
      "name": "v2 LADY K II",
      "buildDate": 1485878400000,
      "grossTonnage": 2.1,
      "assetType": {
        "id": null,
        "deleted": null,
        "lastUpdateVersion": null,
        "code": null,
        "name": "ABC TYPE",
        "category": null,
        "parentId": null,
        "levelIndication": null,
        "defaultClassDepartment": null,
        "assetTypeGroup": null,
        "_id": "b947e300-d1c1-4e10-a789-9983f771ddfa"
      },
    },
    {
      "id": 2,
      "name": "v2 LADY K III",
      "buildDate": 1485878400000,
      "grossTonnage": 3.1,
      "assetType": {
        "id": null,
        "deleted": null,
        "lastUpdateVersion": null,
        "code": null,
        "name": "ABC TYPE",
        "category": null,
        "parentId": null,
        "levelIndication": null,
        "defaultClassDepartment": null,
        "assetTypeGroup": null,
        "_id": "XXb947e300-d1c1-4e10-a789-9983f771ddfa"
      },
    }

  ]
};

// const prettyjson = require('prettyjson');

// const result = renameDeep(testData, (key) => 'get' + key[0].toUpperCase() + key.slice(1))


// console.log(prettyjson.render(result))

module.exports = renameDeep;