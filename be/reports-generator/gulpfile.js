/*global require*/
"use strict";

var gulp = require('gulp'),
  path = require('path'),
  data = require('gulp-data'),
  fs = require('fs'),
  pug = require('gulp-pug'),
  prefix = require('gulp-autoprefixer'),
  sass = require('gulp-sass'),
  refactorPayload = require('./test'),
  browserSync = require('browser-sync'),
  replace = require('gulp-replace');

/*
 * Directories here
 */
var paths = {
  public: './public/',
  sass: './src/sass/',
  css: './public/css/',
  data: './src/_data/',
  resources: './resources/',
  destination: '../class-direct-service/src/main/resources/',
};

var reports;

fs.readdir('./src', function(err, items) {
    // console.log(items);
    reports = items.filter(filename =>
      filename.indexOf('index') !== 0 &&
      filename.split('.').pop() === 'pug' ? true : false);
});

/**
 * Compile .pug files and pass in data from json file
 * matching file name. index.pug - index.pug.json
 */
gulp.task('pug', function () {
  return gulp.src('./src/*.pug')
    .pipe(data(function (file) {
      const jsonData = require( paths.data + path.basename(file.path).split('.pug')[0] + '.db.js');
      // jsonData.files = reports;

      const newRefactoredPayload = refactorPayload(
        jsonData,
        (key) => 'get' + key[0].toUpperCase() + key.slice(1)
      );
      console.log('In Gulp file', newRefactoredPayload);
      return newRefactoredPayload;
    }))
    .pipe(data(function() {
      return { useDummyDataAndFunctions: true };
    }))
    .pipe(pug())
    .on('error', function (err) {
      process.stderr.write(err.message + '\n');
      this.emit('end');
    })
    .pipe(gulp.dest(paths.public));
});

/**
 * Recompile .pug files and live reload the browser
 */
gulp.task('rebuild', ['pug'], function () {
  browserSync.reload();
});

/**
 * Wait for pug and sass tasks, then launch the browser-sync Server
 */
gulp.task('browser-sync', ['sass', 'pug'], function () {
  browserSync({
    server: {
      baseDir: paths.public
    },
    notify: false
  });
});

/**
 * Compile .scss files into public css directory With autoprefixer no
 * need for vendor prefixes then live reload the browser.
 */
gulp.task('sass', function () {
  return gulp.src(paths.sass + '*.scss')
    .pipe(sass({
      includePaths: [paths.sass],
      outputStyle: 'compressed'
    }))
    .on('error', sass.logError)
    .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
      cascade: true
    }))
    .pipe(gulp.dest(paths.css))
    .pipe(browserSync.reload({
      stream: true
    }));
});

/**
 * Watch scss files for changes & recompile
 * Watch .pug files run pug-rebuild then reload BrowserSync
 */
gulp.task('watch', function () {
  gulp.watch(paths.sass + '**/*.scss', ['sass']);
  gulp.watch('./src/**/*.pug', ['rebuild']);
});

// Rename and Move the files to pdf creation folder
gulp.task('export-pug-files', function () {
  console.log('Exporting....');
  gulp.src('./src/**/*.pug')
    .pipe(replace('//--- ', ''))
    .pipe(gulp.dest(paths.destination));
});

gulp.task('export-ttf-fonts', () => {
  gulp.src(paths.resources + '**/*.ttf')
  .pipe(gulp.dest(paths.destination));
})

gulp.task('export', ['export-pug-files', 'export-ttf-fonts']);

// Build task compile sass and pug.
gulp.task('build', ['sass', 'pug']);

/**
 * Default task, running just `gulp` will compile the sass,
 * compile the jekyll site, launch BrowserSync then watch
 * files for changes
 */
gulp.task('default', ['browser-sync', 'watch']);
