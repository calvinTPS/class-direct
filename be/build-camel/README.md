# build-camel

This module keeps the dependencies related to Camel.
Import wherever it's needed using type *pom*

```xml
<dependency>
    <groupId>com.baesystemsai.lr.cd.be</groupId>
    <artifactId>build-camel</artifactId>
    <version>${project.version}</version>
    <type>pom</type>
</dependency>
```