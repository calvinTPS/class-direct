package com.baesystems.ai.lr.cd.be.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Default authentication entry point. Will do nothing.
 *
 * @author Faizal Sidek
 */
public class DefaultAuthenticationEntryPoint implements AuthenticationEntryPoint {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAuthenticationEntryPoint.class);

  @Override
  public final void commence(final HttpServletRequest request, final HttpServletResponse response,
    final AuthenticationException authException) throws IOException, ServletException {
    LOGGER.debug("Should do nothing.");
  }
}
