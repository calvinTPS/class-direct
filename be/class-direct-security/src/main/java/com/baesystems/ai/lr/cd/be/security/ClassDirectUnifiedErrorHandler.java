package com.baesystems.ai.lr.cd.be.security;

import com.baesystems.ai.lr.cd.be.exception.ClassDirectExceptionDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.StringWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * Unified error handler for both access denied and authentication failure.
 *
 * @author Faizal Sidek
 */
public class ClassDirectUnifiedErrorHandler implements AuthenticationFailureHandler, AccessDeniedHandler {

  @Override
  public final void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
    final AuthenticationException exception) throws IOException, ServletException {
    doHandle(response, exception);
  }

  @Override
  public final void handle(final HttpServletRequest request, final HttpServletResponse response,
    final AccessDeniedException accessDeniedException) throws IOException, ServletException {
    doHandle(response, accessDeniedException);
  }

  /**
   * Handle error with proper messages.
   *
   * @param response  response body.
   * @param exception exception body.
   * @throws IOException      when error.
   * @throws ServletException when error.
   */
  private void doHandle(final HttpServletResponse response, final Exception exception)
    throws IOException, ServletException {
    final ClassDirectExceptionDTO dto =
      new ClassDirectExceptionDTO(exception.getMessage(), HttpStatus.UNAUTHORIZED.value());

    final ObjectMapper mapper = new ObjectMapper();
    final StringWriter writer = new StringWriter();
    mapper.writeValue(writer, dto);

    response.setStatus(HttpStatus.UNAUTHORIZED.value());
    response.getWriter().write(writer.toString());
  }
}
