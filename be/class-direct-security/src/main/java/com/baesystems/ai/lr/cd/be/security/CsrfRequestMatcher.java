package com.baesystems.ai.lr.cd.be.security;

import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * Will read properties and provides flag for CSRF protection.
 *
 * @author Faizal Sidek
 */
public class CsrfRequestMatcher implements RequestMatcher {

  /**
   * Enable CSRF flag.
   */
  @Value("${csrf.enabled:false}")
  private boolean csrfEnabled;

  /**
   * Excluded patterns.
   */
  private List<String> excludedPatterns = Collections.emptyList();

  @Override
  public final boolean matches(final HttpServletRequest request) {
    Boolean matches = csrfEnabled;
    if (csrfEnabled && checkForPattern(request)) {
      matches = Boolean.FALSE;
    } else if (csrfEnabled) {
      matches = Boolean.TRUE;
    }

    return matches;
  }

  /**
   * Check for pattern and exclude from checks.
   *
   * @param request request object.
   * @return flag.
   */
  private boolean checkForPattern(final HttpServletRequest request) {
    return excludedPatterns.stream().anyMatch(pattern -> request.getRequestURI().contains(pattern));
  }

  /**
   * Setter for {@link #excludedPatterns}.
   *
   * @param patterns value to be set.
   */
  public final void setExcludedPatterns(final List<String> patterns) {
    this.excludedPatterns = patterns;
  }
}
