package com.baesystems.ai.lr.cd.be.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.InactiveAccountException;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;

/**
 * Authentication provider for class direct.
 * Will handle both equasis/thetis and b2c login.
 *
 * @author Faizal Sidek
 */
public class ClassDirectAuthenticationProvider implements AuthenticationProvider {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ClassDirectAuthenticationProvider.class);

  /**
   * Injected {@link AesSecurityService}.
   */
  @Autowired
  private AesSecurityService aesSecurityService;

  /**
   * Injected {@link UserProfileDao}.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  @Override
  public final Authentication authenticate(final Authentication authentication) throws AuthenticationException {
    final CDAuthToken token = (CDAuthToken) authentication;
    UserProfiles profiles = null;

    final List<SimpleGrantedAuthority> authorities = new ArrayList<>();
    if (token.getAuthorities() != null && !token.getAuthorities().isEmpty()) {
      authorities.addAll(token.getAuthorities());
    }

    if (null != token.getAesCookie() && !token.getAesCookie().isEmpty()) {
      LOGGER.debug("Decrypting AES cookie and getting the list of authorities.");
      final String roles = aesSecurityService.decryptRoleName(token.getAesCookie());
      Stream.of(roles.split(CDConstants.ROLE_DELIMITER))
          .forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
    }

    if (null != token.getPrincipal() && !token.getPrincipal().isEmpty()) {
      LOGGER.debug("Principal is set with {}, using this to search for user details.", token.getPrincipal());
      final List<Roles> roles = new ArrayList<>();
      try {
        profiles = userProfileDao.getActiveUser(token.getPrincipal());
        roles.addAll(profiles.getRoles());
      } catch (final InactiveAccountException exception) {
        throw new UsernameNotFoundException(exception.getMessage(), exception);
      } catch (final ClassDirectException exception) {
        throw new UsernameNotFoundException("User " + token.getPrincipal() + " not found.", exception);
      }

      if (authorities.isEmpty() && !roles.isEmpty()) {
        LOGGER.debug("Authorities is empty, populate it with roles from db.");
        roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRoleName())));
      }
    }

    final CDAuthToken newToken;
    if (authorities.isEmpty()) {
      LOGGER.debug("User has no authority.");
      throw new InsufficientAuthenticationException("No authorities or role found.");
    } else {
      LOGGER.debug("Creating token with {} authorities.", authorities.size());
      newToken = new CDAuthToken(authorities);
    }
    newToken.setAuthenticated(true);
    newToken.setAesCookie(token.getAesCookie());
    newToken.setClaims(token.getClaims());
    newToken.setEquasisThetisAccessibleAssetId(token.getEquasisThetisAccessibleAssetId());

    if (token.getPrincipal() != null && !token.getPrincipal().isEmpty()) {
      LOGGER.debug("Setting with original token principal: {}", token.getPrincipal());
      newToken.setPrincipal(token.getPrincipal());
      newToken.setDetails(profiles);
    }

    LOGGER.debug("Return new token: {} ", newToken);
    return newToken;
  }

  @Override
  public final boolean supports(final Class<?> authentication) {
    return CDAuthToken.class.isAssignableFrom(authentication);
  }
}
