/**
 * Root package for ClassDirect Security module.
 *
 * @author Faizal Sidek
 *
 */
package com.baesystems.ai.lr.cd.be.security;
