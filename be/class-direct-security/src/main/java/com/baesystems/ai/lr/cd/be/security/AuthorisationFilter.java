package com.baesystems.ai.lr.cd.be.security;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Stream;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.security.bean.CookieBean;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.ClassDirectSecurityService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;



/**
 * Class Direct authorisation filter.
 *
 * @author Faizal Sidek
 */
@SuppressWarnings("PMD.CyclomaticComplexity")
public class AuthorisationFilter extends OncePerRequestFilter {

  /**
   * Logger object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AuthorisationFilter.class);

  /**
   * Default HTTP header name is userId.
   */
  private static final String DEFAULT_USER_ID_HEADER = "userId";

  /**
   * Default claim header prefix.
   */
  private static final String DEFAULT_B2C_CLAIM_PREFIX = "ClassDirect_";

  /**
   * Default User's last login header name.
   */
  private static final String USER_LAST_LOGIN = DEFAULT_B2C_CLAIM_PREFIX + "auth_time";

  /**
   * Prefix for claims headers.
   */
  private String b2cClaimsPrefix = DEFAULT_B2C_CLAIM_PREFIX;

  /**
   * HTTP header containing user id.
   */
  private String userIdHeaderName = DEFAULT_USER_ID_HEADER;

  /**
   * Auth manager.
   */
  private AuthenticationManager authenticationManager;

  /**
   * Handle authentication failure.
   */
  private AuthenticationFailureHandler failureHandler;

  /**
   * Injected aes security service.
   */
  @Autowired
  private AesSecurityService aesSecurityService;

  /**
   * Injected Class-Direct security service.
   */
  @Autowired
  private ClassDirectSecurityService classDirectSecurityService;

  /**
   * Injected user profile service.
   */
  @Autowired
  private UserProfileService userProfileService;

  @Override
  protected final void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
      final FilterChain chain) throws ServletException, IOException {
    LOGGER.debug("Attempting to authenticate.");
    Authentication token = null;
    String userId = request.getHeader(userIdHeaderName);
    // check if masquerade id exist, assign masquerade id to user id if exist
    if (request.getHeader(CDConstants.HEADER_MASQ_ID) != null) {
      userId = aesSecurityService.decryptValue(request.getHeader("x-masq-id"));
    }

    boolean hasCookie = false;
    Optional<Cookie> cookieStream = null;
    if (request.getCookies() != null) {
      Stream.of(request.getCookies()).forEach((cookie) -> LOGGER.debug(cookie.getName() + "=" + cookie.getValue()));
      cookieStream =
          Stream.of(request.getCookies()).filter(cookie -> cookie.getName().equals(CDConstants.ROLE)).findAny();
      hasCookie =
          cookieStream.isPresent() && cookieStream.get().getValue() != null && !cookieStream.get().getValue().isEmpty();
    }

    final String requestRole = (String) request.getAttribute(CDConstants.ROLE);
    final Long equasisThetisAssetId = (Long) request.getAttribute(CDConstants.EQUASIS_THETIS_ASSET_ID);

    if (userId != null && !userId.isEmpty()) {
      try {
        checkLastLogin(userId, request);
      } catch (final RecordNotFoundException exp) {
        LOGGER.debug(" Authentication filter doFilterInternal: " + exp);
        failureHandler.onAuthenticationFailure(request, response, new ProviderNotFoundException(exp.getMessage()));
        return;
      }
      final Map<String, String> claims = new HashMap<>();
      Collections.list(request.getHeaderNames()).stream().filter(name -> name.startsWith(b2cClaimsPrefix))
          .forEach(name -> {
            LOGGER.debug("Adding {} header to claims", name);
            claims.put(name, request.getHeader(name));
          });
      token = new CDAuthToken(userId, claims);
    } else if (!StringUtils.isEmpty(requestRole)
        && (Role.THETIS.toString().equals(requestRole) || Role.EQUASIS.toString().equals(requestRole))) {
      LOGGER.debug("Creating token for Equasis/Thetis");

      // check if cookie is empty, if true -> create cookie
      if (!hasCookie) {
        final CookieBean cookieBean = classDirectSecurityService.getCookieConfig();
        final Cookie cookie =
            new Cookie(CDConstants.ROLE, aesSecurityService.encrypt(requestRole, equasisThetisAssetId));

        cookie.setHttpOnly(cookieBean.isHttpOnly());
        cookie.setMaxAge(cookieBean.getMaxAge());
        cookie.setPath(cookieBean.getPath());
        cookie.setSecure(cookieBean.isSecure());
        cookie.setComment(cookieBean.getComments());
        cookie.setVersion(cookieBean.getVersion());

        response.addCookie(cookie);
      }

      // redirect page
      final String redirectUrl = (String) request.getAttribute(CDConstants.EQUASIS_THETIS_URL);
      response.sendRedirect(redirectUrl);
      return;
    } else if (hasCookie) {
      final String decryptedRole = aesSecurityService.decryptRoleName(cookieStream.get().getValue());
      final Long assetId = aesSecurityService.decryptAssetId(cookieStream.get().getValue());
      token = new CDAuthToken(Collections.singletonList(new SimpleGrantedAuthority(decryptedRole)));
      ((CDAuthToken) token).setEquasisThetisAccessibleAssetId(assetId);
    } else {
      LOGGER.debug("No cookie or userId header found.");
      handleFailure(request, response);
    }

    LOGGER.debug("Authenticating with token {}", token);
    if (null != token) {
      try {
        final Authentication authResult = authenticationManager.authenticate(token);
        LOGGER.debug("Authentication success.");
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
      } catch (final AuthenticationException exception) {
        SecurityContextHolder.clearContext();
        LOGGER.debug("Authentication failed: " + exception);
        failureHandler.onAuthenticationFailure(request, response,
            new ProviderNotFoundException(exception.getMessage()));
      }
    } else {
      handleFailure(request, response);
    }
  }

  /**
   * Checks user's last login and saves to db.
   *
   * @param userId User id.
   * @param request Http request.
   * @throws ServletException When something's wrong.
   * @throws RecordNotFoundException exception.
   */
  private void checkLastLogin(final String userId, final HttpServletRequest request)
      throws ServletException, RecordNotFoundException {
    try {
      final String lastLoginHeader = request.getHeader(USER_LAST_LOGIN);
      long lastLogin = 0;
      if (lastLoginHeader != null) {
        lastLogin = Long.parseLong(lastLoginHeader);
      }
      final LocalDateTime lastLoginDateTime =
          LocalDateTime.ofInstant(Instant.ofEpochSecond(lastLogin), TimeZone.getDefault().toZoneId());
      final UserProfiles userProfiles = userProfileService.getUser(userId);
      final LocalDateTime lastLoginDb = userProfiles.getLastLogin();
      if (lastLoginDb == null || lastLoginDb.isBefore(lastLoginDateTime)) {
        userProfileService.updateUserLastLogin(userId, lastLoginDateTime);
      }
    } catch (final ClassDirectException e) {
      LOGGER.debug(" Authentication filter checkLastLogin: " + e);
      throw new RecordNotFoundException(e.getMessage());
    }
  }

  /**
   * Delegate failure handling.
   *
   * @param request http request object.
   * @param response http response object.
   * @throws IOException when error.
   * @throws ServletException when error.
   */
  private void handleFailure(final HttpServletRequest request, final HttpServletResponse response)
      throws IOException, ServletException {
    failureHandler.onAuthenticationFailure(request, response,
        new ProviderNotFoundException("No provider for the request."));
  }


  /**
   * Setter for {@link #failureHandler}.
   *
   * @param handler value to be set.
   */
  public final void setFailureHandler(final AuthenticationFailureHandler handler) {
    this.failureHandler = handler;
  }

  /**
   * Setter for {@link #userIdHeaderName}.
   *
   * @param headerName value to be set.
   */
  public final void setUserIdHeaderName(final String headerName) {
    this.userIdHeaderName = headerName;
  }

  /**
   * Setter for {@link #b2cClaimsPrefix}.
   *
   * @param claimsPrefix value to be set.
   */
  public final void setB2cClaimsPrefix(final String claimsPrefix) {
    this.b2cClaimsPrefix = claimsPrefix;
  }

  /**
   * Setter for {@link #authenticationManager}.
   *
   * @param manager value to be set.
   */
  public final void setAuthenticationManager(final AuthenticationManager manager) {
    this.authenticationManager = manager;
  }

  /**
   * Setter for {@link #classDirectSecurityService}.
   *
   * @param service value.
   */
  public final void setClassDirectSecurityService(final ClassDirectSecurityService service) {
    this.classDirectSecurityService = service;
  }
}
