package com.baesystems.ai.lr.cd.be.security.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Dummy controller for unit testing purpose.
 *
 * @author Faizal Sidek
 */
@Controller
public class SecuredController {
  /**
   * Secured endpoint.
   */
  public static final String SECURED_ENDPOINT = "/secured";

  /**
   * Endpoint accessible for all.
   */
  public static final String PUBLIC_ENDPOINT = "/public";

  /**
   * Dummy secured endpoint.
   * @return response.
   */
  @RequestMapping(value = SECURED_ENDPOINT, method = RequestMethod.GET)
  public final String securedTest() {
    return "ok";
  }

  /**
   * Dummy insecure endpoint.
   * @return response
   */
  @RequestMapping(value = PUBLIC_ENDPOINT, method = RequestMethod.GET)
  public final String publicEndpoint() {
    return "ok";
  }
}
