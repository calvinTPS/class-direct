package com.baesystems.ai.lr.cd.be.security.test;

import com.baesystems.ai.lr.cd.be.security.DefaultAuthenticationEntryPoint;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.security.DefaultAuthenticationEntryPoint}.
 *
 * @author Faizal Sidek
 */
public class DefaultAuthenticationEntryPointTest {

  /**
   * Entry point should do nothing.
   * @throws Exception when error.
   */
  @Test
  public final void commenceTest() throws Exception {
    AuthenticationEntryPoint entryPoint = new DefaultAuthenticationEntryPoint();
    entryPoint.commence(Mockito.mock(HttpServletRequest.class), Mockito.mock(HttpServletResponse.class),
      Mockito.mock(AuthenticationException.class));
  }
}
