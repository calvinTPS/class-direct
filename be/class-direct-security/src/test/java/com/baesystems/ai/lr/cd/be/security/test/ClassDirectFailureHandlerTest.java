package com.baesystems.ai.lr.cd.be.security.test;

import com.baesystems.ai.lr.cd.be.security.ClassDirectUnifiedErrorHandler;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Unit test {@link com.baesystems.ai.lr.cd.be.security.ClassDirectUnifiedErrorHandler}.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ClassDirectFailureHandlerTest {

  /**
   * Injected error handler.
   */
  @Autowired
  private ClassDirectUnifiedErrorHandler errorHandler;

  /**
   * Unit test for onAuthenticationFailure.
   *
   * @throws Exception when error.
   */
  @Test
  public final void handleAuthenticationFailure() throws Exception {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
    AuthenticationException exception = Mockito.mock(AuthenticationException.class);
    Mockito.when(exception.getMessage()).thenReturn("Error message.");

    Mockito.when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));

    errorHandler.onAuthenticationFailure(request, response, exception);
    Mockito.verify(response, Mockito.atMost(1)).setStatus(HttpStatus.UNAUTHORIZED.value());
  }

  /**
   * Unit test for handle.
   *
   * @throws Exception when error.
   */
  @Test
  public final void handleAccessDenied() throws Exception {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
    AccessDeniedException exception = Mockito.mock(AccessDeniedException.class);
    Mockito.when(exception.getMessage()).thenReturn("Error message.");

    Mockito.when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));

    errorHandler.handle(request, response, exception);
    Mockito.verify(response, Mockito.atMost(1)).setStatus(HttpStatus.UNAUTHORIZED.value());
  }

  /**
   * Inner class configuration.
   */
  @Configuration
  public static class Config {

    /**
     * Create new ClassDirectSuccessHandler.
     * @return handler.
     */
    @Bean
    public ClassDirectUnifiedErrorHandler classDirectUnifiedErrorHandler() {
      return new ClassDirectUnifiedErrorHandler();
    }
  }
}
