package com.baesystems.ai.lr.cd.be.security.test;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.hibernate.dao.UserProfileDao;
import com.baesystems.ai.lr.cd.be.domain.repositories.Roles;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserAccountStatus;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.enums.AccountStatusEnum;
import com.baesystems.ai.lr.cd.be.exception.InactiveAccountException;
import com.baesystems.ai.lr.cd.be.exception.RecordNotFoundException;
import com.baesystems.ai.lr.cd.be.security.ClassDirectAuthenticationProvider;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.impl.AesSecurityServiceImpl;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.security.ClassDirectAuthenticationProvider}.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ClassDirectAuthenticationProviderTest {

  /**
   * Constant active user id.
   */
  private static final String ACTIVE_USER_ID = "alice";

  /**
   * Constant inactive user id.
   */
  private static final String INACTIVE_USER_ID = "bob";

  /**
   * Constant for new user id without any authorities.
   */
  private static final String NEW_USER_ID = "charlie";

  /**
   * Injected auth provider.
   */
  @Autowired
  private ClassDirectAuthenticationProvider authenticationProvider;

  /**
   * Inject user profile dao.
   */
  @Autowired
  private UserProfileDao userProfileDao;

  /**
   * Aes security service.
   */
  @Autowired
  private AesSecurityService aesSecurityService;

  /**
   * Auth provider should only support token of type {@link CDAuthToken}.
   */
  @Test
  public final void supportsTest() {
    Assert.assertTrue(authenticationProvider.supports(CDAuthToken.class));
    Assert.assertFalse(authenticationProvider.supports(UsernamePasswordAuthenticationToken.class));
  }

  /**
   * Auth provider should authenticate value token with only principal set.
   *
   * @throws AuthenticationException when error.
   * @throws RecordNotFoundException when no record found.
   * @throws InactiveAccountException when the user is not active
   */
  @Test
  public final void authenticateValidUserId()
      throws AuthenticationException, RecordNotFoundException, InactiveAccountException {
    mockActiveUser();

    final CDAuthToken authToken = new CDAuthToken(ACTIVE_USER_ID, Collections.emptyMap());
    final CDAuthToken returnToken = (CDAuthToken) authenticationProvider.authenticate(authToken);

    Assert.assertNotNull(returnToken);
    Assert.assertTrue(returnToken.isAuthenticated());
    Assert.assertEquals(ACTIVE_USER_ID, returnToken.getPrincipal());
  }

  /**
   * Auth provider should deny any user with inactive user id.
   *
   * @throws AuthenticationException when error.
   * @throws RecordNotFoundException when no record found.
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  @Test(expected = UsernameNotFoundException.class)
  public final void authenticateWithInactiveUserId()
      throws AuthenticationException, RecordNotFoundException, InactiveAccountException {
    mockInactiveUser();

    final CDAuthToken authToken = new CDAuthToken(INACTIVE_USER_ID, Collections.emptyMap());
    authenticationProvider.authenticate(authToken);
  }

  /**
   * Default asset id.
   */
  private static final Long DEFAULT_ASSET_ID = 1000019L;

  /**
   * Auth provider should authenticate token with AES cookie only. This is for Equasis/Thetis user.
   * In addition, the cookie should be converted to list of
   * {@link org.springframework.security.core.GrantedAuthority}.
   *
   * @throws AuthenticationException when error.
   */
  @Test
  public final void authenticateValidAesCookieOnly() throws AuthenticationException {
    final CDAuthToken authToken = new CDAuthToken(aesSecurityService.encrypt(EQUASIS.toString(), DEFAULT_ASSET_ID));
    authToken.setEquasisThetisAccessibleAssetId(DEFAULT_ASSET_ID);

    final CDAuthToken returnToken = (CDAuthToken) authenticationProvider.authenticate(authToken);
    Assert.assertNotNull(returnToken);
    Assert.assertTrue(returnToken.isAuthenticated());
    Assert.assertNull(returnToken.getPrincipal());
    Assert.assertFalse(returnToken.getAuthorities().isEmpty());
    Assert.assertEquals(EQUASIS.toString(), returnToken.getAuthorities().get(0).getAuthority());
    Assert.assertNotNull(returnToken.getEquasisThetisAccessibleAssetId());
    Assert.assertEquals(DEFAULT_ASSET_ID, returnToken.getEquasisThetisAccessibleAssetId());
  }

  /**
   * Auth provider should authenticate token with principal set and AES cookie. In addition, the
   * cookie should be converted to list of
   * {@link org.springframework.security.core.GrantedAuthority}.
   *
   * @throws AuthenticationException when error.
   * @throws RecordNotFoundException when error.
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  @Test
  public final void authenticateWithUserIdAndCookie()
      throws AuthenticationException, RecordNotFoundException, InactiveAccountException {
    mockActiveUser();

    final String encryptedRole = aesSecurityService.encrypt(Role.LR_ADMIN.toString(), DEFAULT_ASSET_ID);

    final CDAuthToken token = new CDAuthToken(ACTIVE_USER_ID, Collections.emptyMap());
    token.setAesCookie(encryptedRole);
    token.setEquasisThetisAccessibleAssetId(DEFAULT_ASSET_ID);
    final CDAuthToken returnToken = (CDAuthToken) authenticationProvider.authenticate(token);

    Assert.assertNotNull(returnToken);
    Assert.assertTrue(returnToken.isAuthenticated());
    Assert.assertNotNull(returnToken.getPrincipal());
    Assert.assertFalse(returnToken.getAuthorities().isEmpty());
    Assert.assertEquals(Role.LR_ADMIN.toString(), returnToken.getAuthorities().get(0).getAuthority());
    Assert.assertNotNull(returnToken.getEquasisThetisAccessibleAssetId());
    Assert.assertEquals(DEFAULT_ASSET_ID, returnToken.getEquasisThetisAccessibleAssetId());
  }

  /**
   * Auth provider should deny token without any authorities.
   *
   * @throws AuthenticationException when auth error.
   * @throws RecordNotFoundException when error.
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  @Test(expected = InsufficientAuthenticationException.class)
  public final void authenticateWithUserWithoutAnyRoles()
      throws AuthenticationException, RecordNotFoundException, InactiveAccountException {
    mockNewUser();

    final CDAuthToken token = new CDAuthToken(NEW_USER_ID, Collections.emptyMap());
    authenticationProvider.authenticate(token);
  }

  /**
   * Mock active user.
   *
   * @throws RecordNotFoundException when no record.
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  private void mockActiveUser() throws RecordNotFoundException, InactiveAccountException {
    final UserAccountStatus active = new UserAccountStatus();
    active.setId(AccountStatusEnum.ACTIVE.getId());
    active.setName("Active");

    final Roles lrAdmin = new Roles();
    lrAdmin.setRoleName(Role.LR_ADMIN.toString());

    final UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(ACTIVE_USER_ID);
    userProfiles.setRoles(Collections.singleton(lrAdmin));
    userProfiles.setStatus(active);
    when(userProfileDao.getActiveUser(ACTIVE_USER_ID)).thenReturn(userProfiles);
  }

  /**
   * Mock new user.
   *
   * @throws RecordNotFoundException when no record
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  private void mockNewUser() throws RecordNotFoundException, InactiveAccountException {
    final UserAccountStatus active = new UserAccountStatus();
    active.setId(AccountStatusEnum.ACTIVE.getId());
    active.setName("Active");

    final UserProfiles userProfiles = new UserProfiles();
    userProfiles.setUserId(NEW_USER_ID);
    userProfiles.setRoles(Collections.emptySet());
    userProfiles.setStatus(active);
    when(userProfileDao.getActiveUser(NEW_USER_ID)).thenReturn(userProfiles);
  }

  /**
   * Mock inactive user.
   *
   * @throws RecordNotFoundException when no record.
   * @throws InactiveAccountException trying to get an active user that is inactive.
   */
  private void mockInactiveUser() throws RecordNotFoundException, InactiveAccountException {
    when(userProfileDao.getActiveUser(INACTIVE_USER_ID)).thenThrow(new RecordNotFoundException("No record found."));
  }

  /**
   * Inner class config.
   */
  @Configuration
  public static class Config {

    /**
     * Initialize placeholders.
     *
     * @return property placeholders.
     */
    @Bean
    public PropertyPlaceholderConfigurer configurer() {
      final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
      configurer.setLocation(new ClassPathResource("test-config.properties"));
      return configurer;
    }

    /**
     * Mock {@link AesSecurityService}.
     *
     * @return mock service.
     * @throws Exception when error.
     */
    @Bean
    public AesSecurityService aesSecurityService() throws Exception {
      return new AesSecurityServiceImpl();
    }

    /**
     * Create mock {@link UserProfileDao}.
     *
     * @return mock.
     */
    @Bean
    public UserProfileDao mockUserProfileDao() {
      return Mockito.mock(UserProfileDao.class);
    }

    /**
     * Create real object.
     *
     * @return class direct provider.
     */
    @Bean
    public ClassDirectAuthenticationProvider authenticationProvider() {
      return new ClassDirectAuthenticationProvider();
    }
  }
}
