package com.baesystems.ai.lr.cd.be.security.test;

import com.baesystems.ai.lr.cd.be.security.CsrfRequestMatcher;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.security.CsrfRequestMatcher}.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class CsrfRequestMatcherTest {

  /**
   * Injected csrf request matcher.
   */
  @Autowired
  private CsrfRequestMatcher csrfRequestMatcher;

  /**
   * Matcher should return false by default.
   *
   */
  @Test
  public final void matcherTest() {
    final HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getRequestURI()).thenReturn("/test?1=1").thenReturn("/logout?login=1");

    assertTrue(csrfRequestMatcher.matches(request));
    assertFalse(csrfRequestMatcher.matches(request));
  }

  /**
   * Inner config.
   */
  @Configuration
  public static class Config {

    /**
     * Read config file.
     *
     * @return property placeholder.
     */
    @Bean
    public PropertyPlaceholderConfigurer configurer() {
      PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
      configurer.setLocation(new ClassPathResource("test-config.properties"));

      return configurer;
    }

    /**
     * Real object.
     * @return object.
     */
    @Bean
    public CsrfRequestMatcher csrfRequestMatcher() {
      final CsrfRequestMatcher requestMatcher = new CsrfRequestMatcher();
      requestMatcher.setExcludedPatterns(Collections.singletonList("/logout"));

      return requestMatcher;
    }
  }
}
