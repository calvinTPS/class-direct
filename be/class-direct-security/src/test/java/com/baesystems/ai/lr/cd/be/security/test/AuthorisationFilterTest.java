package com.baesystems.ai.lr.cd.be.security.test;

import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.EQUASIS;
import static com.baesystems.ai.lr.cd.be.domain.dto.security.Role.THETIS;
import static com.baesystems.ai.lr.cd.be.security.test.SecuredController.SECURED_ENDPOINT;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringWriter;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.baesystems.ai.lr.cd.be.constants.CDConstants;
import com.baesystems.ai.lr.cd.be.domain.dto.security.CDAuthToken;
import com.baesystems.ai.lr.cd.be.domain.dto.security.Role;
import com.baesystems.ai.lr.cd.be.domain.dto.security.bean.CookieBean;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectException;
import com.baesystems.ai.lr.cd.be.exception.ClassDirectExceptionDTO;
import com.baesystems.ai.lr.cd.be.service.security.AesSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.ClassDirectSecurityService;
import com.baesystems.ai.lr.cd.be.service.security.impl.AesSecurityServiceImpl;
import com.baesystems.ai.lr.cd.service.asset.AssetService;
import com.baesystems.ai.lr.cd.service.userprofile.UserProfileService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit test for {@link com.baesystems.ai.lr.cd.be.security.AuthorisationFilter}.
 *
 * @author Faizal Sidek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AuthorisationFilterTest.Config.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
@WebAppConfiguration
public class AuthorisationFilterTest {
  /**
   * Log object.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AuthorisationFilterTest.class);

  /**
   * Azure object id for Alice.
   */
  private static final String ALICE_OID = "052d354b-096a-407c-bf5e-34a38a646525";

  /**
   * User id for active user.
   */
  private static final String ACTIVE_USER_ID = "104";

  /**
   * User id header name.
   */
  private static final String USER_ID_HEADER_NAME = "userId";

  /**
   * status 302.
   */
  private static final int THREE_ZERO_TWO = 302;

  /**
   * Injected app context.
   */
  @Autowired
  private ApplicationContext applicationContext;

  /**
   * Web app context.
   */
  @Autowired
  private WebApplicationContext context;

  /**
   * Injected filter chain proxy.
   */
  @Autowired
  private FilterChainProxy filterChainProxy;

  /**
   * Injected mock aes security service.
   */
  @Autowired
  private AesSecurityService aesSecurityService;

  /**
   * Mock mvc.
   */
  private MockMvc mock;

  /**
   * Initialize test cases.
   *
   * @throws Exception when error.
   */
  @Before
  public final void setup() throws Exception {
    mock = MockMvcBuilders.webAppContextSetup(context)
        .apply(SecurityMockMvcConfigurers.springSecurity(filterChainProxy)).build();
    mockFailureHandler();
  }

  /**
   * Filter should deny any request without userId header or AES cookie.
   *
   * @throws Exception when error.
   */
  @Test
  public final void executeFilterWithInvalidRequest() throws Exception {
    mockAuthenticationProvider(true);
    mock.perform(get(SECURED_ENDPOINT).header("Test", "Test")).andExpect(status().is(HttpStatus.SC_UNAUTHORIZED));
  }

  /**
   * Filter should accept any request with userId header.
   *
   * @throws Exception when error.
   */
  @Test
  public final void executeDoFilterInternal() throws Exception {
    mockAuthenticationProvider(true);

    mock.perform(get(SECURED_ENDPOINT).header(USER_ID_HEADER_NAME, ACTIVE_USER_ID)).andExpect(status().isOk());
  }

  /**
   * Filter should accept any request with AES cookie and userId header with B2C claims.
   *
   * @throws Exception when error.
   */
  @Test
  public final void executeFilterWithCookiesAndClaims() throws Exception {
    mockAuthenticationProvider(true);

    mock.perform(get(SECURED_ENDPOINT).header("userId", ACTIVE_USER_ID).header("ClassDirect_OID", ALICE_OID))
        .andExpect(status().isOk());

    final AuthenticationProvider authenticationProvider =
        applicationContext.getBeansOfType(AuthenticationProvider.class).get("authenticationProvider");

    final ArgumentCaptor<CDAuthToken> argumentCaptor = ArgumentCaptor.forClass(CDAuthToken.class);
    verify(authenticationProvider, atLeast(1)).authenticate(argumentCaptor.capture());

    Assert.assertNotEquals(0, argumentCaptor.getAllValues().size());
    final Optional<CDAuthToken> cdAuthToken =
        argumentCaptor.getAllValues().stream().filter(token -> !token.getClaims().isEmpty()).findAny();

    Assert.assertNotNull(cdAuthToken);
    Assert.assertTrue(cdAuthToken.isPresent());
  }

  /**
   * Filter should accept request with only AES cookie.
   *
   * @throws Exception when error.
   */
  @Test
  public final void executeFilterWithCookieOnly() throws Exception {
    mockAuthenticationProvider(true);
    final String encryptedCookies = aesSecurityService.encrypt(Role.THETIS.toString(), DEFAULT_ASSET_ID);
    mock.perform(get(SECURED_ENDPOINT).cookie(new Cookie(CDConstants.ROLE, encryptedCookies)))
        .andExpect(status().isOk());
  }

  /**
   * Filter should send 401 if auth provider deny the user.
   *
   * @throws Exception when error.
   */
  @Test
  public final void executeFilterWithFailedAuthentication() throws Exception {
    mockAuthenticationProvider(false);

    mock.perform(get(SECURED_ENDPOINT).header(USER_ID_HEADER_NAME, ACTIVE_USER_ID))
        .andExpect(status().is(HttpStatus.SC_UNAUTHORIZED));
  }

  /**
   * Default asset id.
   */
  private static final Long DEFAULT_ASSET_ID = 1000019L;

  /**
   * Filter should handle request for Equasis.
   *
   * @throws Exception when error
   */
  @Test
  public final void executeFilterWithEquasisRequest() throws Exception {
    mockAuthenticationProvider(true);

    mock.perform(get(SECURED_ENDPOINT).requestAttr(CDConstants.ROLE, EQUASIS.toString())
        .requestAttr(CDConstants.EQUASIS_THETIS_ASSET_ID, DEFAULT_ASSET_ID)
        .requestAttr(CDConstants.EQUASIS_THETIS_URL, "test/test")).andExpect(status().is(THREE_ZERO_TWO));
  }

  /**
   * Filter should handle request for Thetis.
   *
   * @throws Exception when error
   */
  @Test
  public final void executeFilterWithThetisRequest() throws Exception {
    mockAuthenticationProvider(true);

    mock.perform(get(SECURED_ENDPOINT).requestAttr(CDConstants.ROLE, THETIS.toString())
        .requestAttr(CDConstants.EQUASIS_THETIS_URL, "test/test")).andExpect(status().is(THREE_ZERO_TWO));
  }

  /**
   * Mock authentication provider.
   *
   * @param success success flag.
   */
  private void mockAuthenticationProvider(final boolean success) {
    final AuthenticationProvider authProvider =
        applicationContext.getBean("authenticationProvider", AuthenticationProvider.class);

    when(authProvider.supports(any())).then(invocation -> {
      LOGGER.debug("Testing matchers with {} ", invocation.getArguments()[0]);
      return true;
    });
    if (success) {
      doReturn(new CDAuthToken("")).when(authProvider).authenticate(any());
    } else {
      doThrow(new BadCredentialsException("Failed login")).when(authProvider).authenticate(any());
    }
  }

  /**
   * Mock failure handler.
   *
   * @throws Exception when error.
   */
  private void mockFailureHandler() throws Exception {
    final AuthenticationFailureHandler failureHandler = applicationContext.getBean(AuthenticationFailureHandler.class);
    doAnswer(invocation -> {
      final AuthenticationException exception = invocation.getArgumentAt(2, AuthenticationException.class);
      final HttpServletResponse response = invocation.getArgumentAt(1, HttpServletResponse.class);
      final ClassDirectExceptionDTO dto =
          new ClassDirectExceptionDTO(exception.getMessage(), org.springframework.http.HttpStatus.UNAUTHORIZED.value());

      final ObjectMapper mapper = new ObjectMapper();
      final StringWriter writer = new StringWriter();
      mapper.writeValue(writer, dto);

      response.setStatus(org.springframework.http.HttpStatus.UNAUTHORIZED.value());
      response.getWriter().write(writer.toString());
      return null;
    }).when(failureHandler).onAuthenticationFailure(any(), any(), any());
  }

  /**
   * Mock config.
   */
  @Configuration
  @Import(SecuredController.class)
  @ImportResource("classpath:mock-authfilter-context.xml")
  public static class Config {

    /**
     * Initialize placeholders.
     *
     * @return property placeholders.
     */
    @Bean
    public PropertyPlaceholderConfigurer configurer() {
      final PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
      configurer.setLocation(new ClassPathResource("test-config.properties"));
      return configurer;
    }

    /**
     * Mock {@link AesSecurityService}.
     *
     * @return mock service.
     */
    @Bean
    public AesSecurityService aesSecurityService() {
      return new AesSecurityServiceImpl();
    }

    /**
     * Mock {@link AssetService}.
     *
     * @return AssetService object.
     * @throws ClassDirectException value.
     */
    @Bean
    public AssetService assetService() throws ClassDirectException {
      final AssetService assetService = Mockito.mock(AssetService.class);

      when(assetService.getAssetIdByImoNumber(Matchers.anyString())).thenReturn(1L);


      return assetService;
    }

    /**
     * Real service for Class-Direct security service.
     *
     * @return Class-Direct service.
     */
    @Bean
    public ClassDirectSecurityService classDirectSecurityService() {
      final ClassDirectSecurityService classDirectSecurityService = Mockito.mock(ClassDirectSecurityService.class);

      final CookieBean config = new CookieBean();
      config.setHttpOnly(true);
      config.setPath("/");
      config.setDomain("localhost");
      when(classDirectSecurityService.getCookieConfig()).thenReturn(config);

      when(classDirectSecurityService.getInvalidInputParameters()).thenReturn("Mock error message.");

      return classDirectSecurityService;
    }

    /**
     * Mock {@link UserProfileService}.
     *
     * @return user profile service.
     * @throws ClassDirectException ClassDirectException.
     */
    @Bean
    public UserProfileService userProfileService() throws ClassDirectException {
      final UserProfileService userProfileService = Mockito.mock(UserProfileService.class);
      UserProfiles userProfiles = new UserProfiles();
      when(userProfileService.getUser(Matchers.anyString())).thenReturn(userProfiles);
      return userProfileService;
    }
  }
}
