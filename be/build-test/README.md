# build-test

This module keeps the dependencies related to testing.
Import wherever it's needed using type *pom*

```xml
<dependency>
    <groupId>com.baesystemsai.lr.cd.be</groupId>
    <artifactId>build-test</artifactId>
    <version>${project.version}</version>
    <type>pom</type>
</dependency>
```