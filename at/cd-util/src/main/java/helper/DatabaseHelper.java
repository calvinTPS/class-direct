package helper;

import com.google.common.io.Resources;
import config.Config;
import org.testng.Assert;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.*;

public class DatabaseHelper
{

    private final static String USERNAME = "testUser";
    private final static String PASSWORD = "s3curePa55word";

    public static ResultSet executeQuery(String sql)
    {
        return executeQuery(sql, Database.MAST);
    }

    public static void insertQuery(String sql)
    {
        insertQuery(sql, Database.MAST);
    }

    public static ResultSet executeQuery(String sql, Database database)
    {
        try
        {
            return database.getConnection().createStatement().executeQuery(sql);
        }
        catch (SQLException e)
        {
            Assert.fail("Test Failed due to SQL Error: " + e.toString());
        }
        return null;
    }

    public static void insertQuery(String sql, Database database)
    {
        try
        {
            Statement statement = database.getConnection().createStatement();
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
        }
        catch (SQLException e)
        {
            Assert.fail("Test Failed due to SQL Error: " + e.toString());
        }
    }

    public static int getRowCount(ResultSet resultSet)
    {
        int rowCount = 0;
        try
        {
            if (resultSet.last())
            {
                rowCount = resultSet.getRow();
                resultSet.beforeFirst();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return rowCount;
    }

    public static void runSqlFile(String resourceFilePath, String... values)
    {
        runSqlFile(resourceFilePath, Database.MAST, values);
    }

    public static void runSqlFile(String resourceFilePath, Database database, String... values)
    {
        URL sqlFileURL = Resources.getResource(resourceFilePath);
        ScriptRunner scriptRunner = new ScriptRunner(database.getConnection(), false, true);
        try
        {
            String sqlScript =
                    String.format(Resources.toString(sqlFileURL, Charset.defaultCharset()), (Object[]) values);
            scriptRunner.runScript(new StringReader(sqlScript));
        }
        catch (IOException | SQLException e)
        {
            Assert.fail(e.getMessage());
        }
    }

    public enum Database
    {

        IHS("ihs_db"),
        MAST("mast_db");

        private Connection connection;

        Database(String db)
        {
            String dbNumber = System.getProperty("dbNumber");
            try
            {
                connection = DriverManager.getConnection(
                        "jdbc:mysql://" + Config.getDataBaseIp() + "/" +
                                db + ((dbNumber != null) ? dbNumber : "") + "?characterEncoding=utf-8",
                        USERNAME,
                        PASSWORD
                );
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        public Connection getConnection()
        {
            return connection;
        }
    }

}
