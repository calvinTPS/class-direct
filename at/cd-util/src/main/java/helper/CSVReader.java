package helper;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.Sleeper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CSVReader
{
    private static int attempt = 0;
    private static final String DOWNLOAD_PATH = String.format("%s/Downloads/", System.getProperty("user.home"));
    private static final int MAX_ATTEMPT = 10;

    public static List<CSVRecord> parseCSV(String fileName, CSVFormat csvFormat)
    {
        String csvFile = String.format("%s%s", DOWNLOAD_PATH, fileName);
        File f = new File(csvFile);

        return findCSVFile(f, csvFormat);
    }

    private static List<CSVRecord> findCSVFile(File file, CSVFormat csvFormat)
    {
        List<CSVRecord> csvRecordList = new ArrayList<>();
        try
        {
            if (attempt > MAX_ATTEMPT)
            {
                attempt = 0;
                throw new TimeoutException(String.format("Unable to find the CSV file %s after %s attempts.", file.getAbsolutePath(), MAX_ATTEMPT));
            }
            if(file.exists())
            {
                CSVParser csvParser = new CSVParser(new FileReader(file), csvFormat);
                csvRecordList = csvParser.getRecords();
                if(csvRecordList.size() <= 2)
                {
                    attempt++;
                    csvRecordList = findCSVFile(file, csvFormat);
                }
            }
            else
            {
                TimeUnit.SECONDS.sleep(2); //needs time to create the CSV file.
                attempt++;
                csvRecordList = findCSVFile(file, csvFormat);
            }
        }
        catch (IOException | InterruptedException e)
        {
             e.printStackTrace();
        }
        finally
        {
            attempt = 0;
        }

        return csvRecordList;
    }

    public enum UserColumn
    {
        USER_ID,
        FIRST_NAME,
        LAST_NAME,
        LAST_LOGIN_DATE,
        CLIENT_CODE,
        SHIP_BUILDER_CODE,
        FLAG_CODE,
        CLIENT_NAME,
        ACCOUNT_TYPE,
        EMAIL_ADDRESS,
        MODIFICATION_DATE,
        ACCOUNT_STATUS
    }
}
