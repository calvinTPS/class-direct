package helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.List;
import java.util.function.Function;

import static constant.Config.*;

public class ExtraExpectedConditions
{

    public static ExpectedCondition<Boolean> invisibilityOfSpinner()
    {
        return expectedCondition(
                driver ->
                {
                    List<WebElement> e = driver.findElements(SPINNER);
                    return e.size() <= 0 ||
                            e.parallelStream().noneMatch(WebElement::isDisplayed);
                },
                "Spinner to not be present or be invisible"
        );
    }

    public static ExpectedCondition<Boolean> visibilityOfSpinner()
    {
        return expectedCondition(
                driver ->
                {
                    List<WebElement> e = driver.findElements(SPINNER);
                    return e.size() > 0 &&
                            e.parallelStream().anyMatch(WebElement::isDisplayed);
                },
                "Spinner to be present or visible"
        );
    }

    public static ExpectedCondition<Boolean> invisibilityOfLoadingBar()
    {
        return expectedCondition(
                driver ->
                {
                    List<WebElement> e = driver.findElements(LOADING_BAR);
                    return e.size() <= 0 ||
                            e.parallelStream().noneMatch(WebElement::isDisplayed);
                },
                "Loading bar to not be present or be invisible"
        );
    }

    public static ExpectedCondition<Boolean> invisibilityOfDialog()
    {
        return expectedCondition(
                driver -> driver.findElements(DIALOG).size() <= 0,
                "Modal dialog to not be present or be invisible"
        );
    }

    public static ExpectedCondition<Boolean> visibilityOfDialog()
    {
        return expectedCondition(
                driver -> driver.findElements(DIALOG).size() > 0,
                "Modal dialog to be present or be visible"
        );
    }

    private static <T> ExpectedCondition<T> expectedCondition(
            Function<WebDriver, T> function, String string)
    {

        return new ExpectedCondition<T>()
        {
            @Override
            public T apply(WebDriver driver)
            {
                return function.apply(driver);
            }

            @Override
            public String toString()
            {
                return string;
            }
        };
    }
}

