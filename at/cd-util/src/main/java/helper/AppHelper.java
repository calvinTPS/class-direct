package helper;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.frameworkium.core.ui.tests.BaseTest.getDriver;
import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;

public class AppHelper {
    private static ThreadLocal<Integer> attempt = ThreadLocal.withInitial(() -> 0);

    private static final int headerHeight = 50; //50px header height
    private static final int mobileWidth = 425; //425px Mobile L's width

    // main scrolling pane CSS
    private static final String scrollpaneCSS = "body";
    // maximum attempts to try scrolling before giving up
    private static final int MAX_ATTEMPT = 3;

    public static void scrollIntoView(WebElement element) {
        scrollIntoView(element, false);
    }

    public static void scrollToMiddle(WebElement element) {
        // element could be in a child container or modal window, don't scroll and return
        // the overflow-y is disabled at html level when modal comes on screen
        if (!isScrollable(getDriver().findElement(By.cssSelector("html")))) {
            return;
        }

        // ---------DO THE SCROLL TO MIDDLE---------
        // the distance that the pane has already been scrolled
        Long paneTop = (Long) getDriver().executeScript("return document.querySelector('" + scrollpaneCSS + "').scrollTop");
        // the distance between top of the element from the top of the viewport
        Object obj = getDriver().executeScript("return arguments[0].getBoundingClientRect().top;", element);
        Long top = obj.getClass() == Long.class ? (Long) obj : ((Double) obj).longValue();
        // the total height of the viewport
        Long viewportHeight = (Long) getDriver().executeScript("return window.innerHeight;");

        if (top < headerHeight) { // the element is hidden and above the viewport or hidden by the header
            if (attempt.get() > MAX_ATTEMPT) { //element could be fixed in place, last ditch effort to scroll and give up
                scrollIntoView(element, true);
                return;
            }
            getDriver().executeScript("document.querySelector('" + scrollpaneCSS + "').scrollTop = 0"); //scroll to very top
            attempt.set(attempt.get() + 1);
            scrollToMiddle(element); //reassess the situation
        } else if (top > viewportHeight / 2) { // element is below the 2nd half of the viewport
            Long scrollLength = paneTop + top - viewportHeight / 2;  //distance that we have to scroll to get the element to middle
            getDriver().executeScript("document.querySelector('" + scrollpaneCSS + "').scrollTop = " + scrollLength.toString()); // scroll till its in the middle
        }
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        attempt.set(0); //reset
    }

    public static void scrollToBottom() {
        getDriver().executeScript("document.querySelector('" + scrollpaneCSS + "').scrollTop = 10000");
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
    }

    public static void scrollToTop(){
        getDriver().executeScript("document.querySelector('" + scrollpaneCSS + "').scrollTop = 0");
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
    }

    public static void shrinkIntoMobileView() {
        WebDriver.Window window = getDriver().manage().window();
        window.setSize(new Dimension(mobileWidth, window.getSize().getHeight()));
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
    }

    private static void scrollIntoView(WebElement element, boolean align) {
        getDriver().executeScript("arguments[0].scrollIntoView(" + String.valueOf(align) + ");", element);
    }

    private static boolean isScrollable(WebElement element) {
        return element.getCssValue("overflow-y").equals("auto");
    }
}
