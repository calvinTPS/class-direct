package helper;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;
import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;

public class HtmlElement extends ru.yandex.qatools.htmlelements.element.HtmlElement
{
    protected WebDriver driver = BaseTest.getDriver();
    protected Wait<WebDriver> wait = BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT);
    private Wait<WebDriver> waitForStale = new FluentWait<>(driver)
            .withTimeout(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS)
            .ignoring(NoSuchElementException.class)
            .ignoring(StaleElementReferenceException.class);

    protected void waitForSpinnerToFinish()
    {
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
    }

    public void waitForStaleness()
    {
        try
        {
            waitForStale.until(ExpectedConditions.stalenessOf(this));
        }
        catch (TimeoutException t)
        {
            //
        }
    }
}
