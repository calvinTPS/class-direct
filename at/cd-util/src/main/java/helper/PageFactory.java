package helper;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static constant.Config.GLOBAL_PAGE_LOAD_TIMEOUT;
import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;

public class PageFactory extends com.frameworkium.core.ui.pages.PageFactory {
    public static <T extends BasePage<T>> T newInstance(Class<T> clazz, long timeoutInSeconds) {
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT)
                .until(ExpectedConditions.and(
                ExtraExpectedConditions.invisibilityOfSpinner(),
                ExtraExpectedConditions.invisibilityOfLoadingBar()));
        return com.frameworkium.core.ui.pages.PageFactory.newInstance(clazz, timeoutInSeconds);
    }

    public static <T extends BasePage<T>> T newInstance(Class<T> clazz) {
        return newInstance(clazz, GLOBAL_PAGE_LOAD_TIMEOUT);
    }
}
