package helper;

import config.Config;
import io.restassured.RestAssured;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

import static constant.ClassDirect.Credentials.LR_ADMIN;

public class DownloadReportHelper {

    public static String getDownloadFileChecksum(String token)
    {
        String assetExportUrl = String.format("http://%s/api/v1-spring/download-file?token=%s", Config.getBaseAPI(), token);

        return getFileCheckSum(assetExportUrl);
    }

    private static String getFileCheckSum(String url)
    {
        String responseBody = RestAssured
                .given()
                .header("userId", LR_ADMIN.getId())
                .when()
                .get(url)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response()
                .asString();

        return DigestUtils.md5Hex(responseBody).toUpperCase();
    }
}
