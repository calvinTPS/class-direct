package constant;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

//TODO - this is getting gigantic - needs refactor to break into smaller relevant classes
public class ClassDirect
{

    // DateFormat
    public static final SimpleDateFormat BACKEND_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat FRONTEND_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    public static final SimpleDateFormat FRONTEND_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");
    public static final DateTimeFormatter FRONTEND_TIME_FORMAT_DTS = DateTimeFormatter.ofPattern("dd MMM yyyy");
    public static final SimpleDateFormat ASSET_DETAILS_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    // Local Date Object
    public static final LocalDate now = LocalDate.now();

    public static final String ALL_SEARCH_TERM = "****";

    // Assets
    public enum Asset{
        LAURA("Laura Test Asset", "7653895", "LRV1", 1),   //use for most common tests
        LR_ASSET_WITH_IHS_DATA("Zuiderdam", "7654321", "LRV427", 427), //use for tests dealing with lr assets with ihs data
        WITH_CERTIFICATES("Adonia","7653979", "LRV86", 86), //use for tests dealing with certificates
        NON_LR_ASSET("BLUE ISLAND","9209922", "IHS9209922", -1), //use for tests dealing with non-lr-asset/ihs
        FOR_RESTRICTED_TEST("Caly","7653901","LRV8", 8); //use for tests dealing with asset restriction

        private String name;
        private String imoNumber;
        private String assetCode;
        private Integer id;

        Asset(String name, String imoNumber, String assetCode, Integer id)
        {
            this.name=name;
            this.imoNumber=imoNumber;
            this.assetCode=assetCode;
            this.id=id;
        }

        public String getName(){ return this.name; }

        public String getImoNumber(){ return this.imoNumber; }

        public String getAssetCode(){return this.assetCode;}

        public Integer getId(){return this.id;}
    }

    // REGEX Format
    public static final String SHOWING_REGEX = "([0-9]{0,3}) of";
    public static final String TOTAL_REGEX = "of ([0-9]{0,5})";
    public static final String NUMBER_REGEX = "[0-9]*$";

    //Login Credentials
    public enum Credentials
    {
        //TODO: Password to be set here for each user
        LR_ADMIN("lr_admin", "", "LR Admin", "alice.classdirect@gmail.com", "37be2b5b-63ee-4535-bca4-202d556d626e"),
        FLAG_USER("flag_user", "", "Flag", "flag@flaguser.com", "fb69e49c-61ba-4332-99b7-d9913aac5d12"),
        SHIP_BUILDER_USER("ship_builder_user", "", "Ship Builder", "ship@shipbuilder.com", "c0615f39-227c-4406-b2ad-367f0da6461c"),
        LR_SUPPORT("", "", "LR Support", "bob.classdirect@gmail.com", "1b1e62ba-2923-46e0-90c2-23524904ce70"),
        LR_INTERNAL_USER("", "", "LR Internal", "idstestuser14@lr.org", "b597f7ce-6a09-4eaf-b387-54a36d91063f"),
        CLIENT_USER("client_user", "", "Client", "user@client.com", "e9999d2e-c043-4189-b6b2-6e3d907a5bdd");

        private final String userName;
        private final String password;
        private final String role;
        private final String email;
        private final String id;

        Credentials(String userName, String password, String role, String email, String id)
        {
            this.userName = userName;
            this.password = password;
            this.role = role;
            this.email = email;
            this.id = id;
        }

        public String getUserName()
        {
            return this.userName;
        }

        public String getPassword()
        {
            return this.password;
        }

        public String getRole()
        {
            return this.role;
        }

        public String getEmail()
        {
            return this.email;
        }

        public String getId()
        {
            return this.id;
        }
    }

    public static final class RequestSurveyData
    {
        public static final String portName = "Zamboanga";
        public static final LocalDate arrivalDate = now.plusDays(1);
        public static final LocalDate surveyStartDate = now.plusDays(2);
        public static final LocalDate departureDate = now.plusDays(5);
        public static final String arrivalTime = "01:00";
        public static final String departureTime = "20:00";
        public static final String agentName = "John Smith";
        public static final String agentPhoneNumber = "0178889900";
        public static final String agentEmailAddress = "test@baesystems.com";
        public static final String berthAnchorage = "Berth anchorage test";
        public static final String shipContactDetails = "Lloyds Register";
        public static final String comments = "test comment";
        public static final String emailForShareToOthers = "test@baesystems.com";

        public static final String emailHelperText = "E.g. email@example.com";
        public static final String commentHelperText = "Please add your comments here";
        public static final String requestSuccessfulMessage = "Request sent successfully";
    }

    //Filter By Asset Type Table Names
    public enum AssetTableName
    {
        NON_SHIP_STRUCTURES("Non Ship Structures"),
        BUILD_CARRIERS("Bulk Carriers"),
        DRY_CARGO_PASSENGER("Dry Cargo/Passenger"),
        TANKERS("Tankers"),
        NON_MERCHANTS("Non Merchant"),
        NON_PROPELLED("Non Propelled"),
        INLAND_WATERWAYS("Inland Waterways"),
        FISHING("Fishing"),
        MISCELLANEOUS("Miscellaneous"),
        OFFSHORE("Offshore");

        private final String tableName;

        AssetTableName(String tableName)
        {
            this.tableName = tableName;
        }

        public String getTableName()
        {
            return tableName;
        }

    }

    //Service Status of Job
    public enum ServiceStatus
    {
        Complete("Complete"),
        PartHeld("Part held"),
        Finished("Finished"),
        Not_Started("Not started"),
        Unknown("Unknown"),
        Postponed("Postponed");

        private final String serviceStatus;

        ServiceStatus(String serviceStatus)
        {
            this.serviceStatus = serviceStatus;
        }

        public String getServiceStatus()
        {
            return serviceStatus;
        }
    }

    //Class Statuses
    public enum ClassStatus
    {
        ALL("All"),
        CLASSED_PENDING_CLASS_EXEC_APPROVAL("Classed (Pending Class Exec Approval)"),
        CLASSED("Classed"),
        CLASSED_LAID_UP("Classed (Laid Up)"),
        CLASSED_SUSPENDED("Class Suspended"),
        CANCELLED("Cancelled"),
        CLASS_WITHDRAWN("Class Withdrawn"),
        NOT_LR_CLASSED("Not LR Classed"),
        TOC_CONTEMPLATED("TOC Contemplated"),
        AIC_CONTEMPLATED("AIC Contemplated"),
        RECLASSIFICATION_CONTEMPLATED("Reclassification Contemplated"),
        HELD("Held"),
        FE_CONTEMPLATED("FE Contemplated");

        private final String classStatus;

        ClassStatus(String classStatus)
        {
            this.classStatus = classStatus;
        }

        @Override
        public String toString(){
            return classStatus;
        }
    }

    //Due Statuses
    public enum DueStatus
    {
        OVERDUE("Overdue"),
        IMMINENT("Imminent"),
        DUE_SOON("Due soon"),
        NOT_DUE("Not due");

        private final String dueStatus;

        DueStatus(String dueStatus)
        {
            this.dueStatus = dueStatus;
        }

        public String getDueStatus()
        {
            return dueStatus;
        }
    }

    //Asset search options
    public enum AssetSearchOptions
    {
        ASSET_NAME_IMO_NUMBER("Asset name / IMO number"),
        CLIENT("Client"),
        SHIP_BUILDER("Ship Builder"),
        YARD_NUMBER("Yard number");

        private final String option;

        AssetSearchOptions(String option)
        {
            this.option = option;
        }

        public String toString()
        {
            return this.option;
        }
    }

    //Asset sort by options
    public enum AssetSortByOptions
    {
        ASSET_NAME("Asset name"),
        CLASS_STATUS("Asset class status"),
        ASSET_TYPE("Asset type"),
        DATE_OF_BUILD("Date of build"),
        YARD_NUMBER("Yard number"),
        FLAG("Flag");

        private final String option;

        AssetSortByOptions(String option)
        {
            this.option = option;
        }

        public String toString()
        {
            return this.option;
        }
    }

    // Job Reports
    public enum JobReport{
        FAR("000110020", 20),
        FSR("000100001", 1),
        ESP("000000004", 4);

        private String jobId;
        private long reportId;

        JobReport(String jobId, long reportId)
        {
            this.jobId=jobId;
            this.reportId=reportId;
        }

        public String getJobId(){ return this.jobId; }

        public long getReportId(){ return this.reportId; }
    }

    //Jobs search options
    public enum JobSearchOptions
    {
        SURVEYS("Surveys"),
        JOB_NUMBER("Job Number"),
        LEAD_SURVEYOR("Lead Surveyor"),
        LOCATION("Location");

        private final String option;

        JobSearchOptions(String option)
        {
            this.option = option;
        }

        public String toString()
        {
            return this.option;
        }
    }

    //Jobs sort by options
    public enum JobSortByOptions
    {
        JOB_NUMBER("Job Number"),
        LAST_VISIT_DATE("Last Visit Date");

        private final String option;

        JobSortByOptions(String option)
        {
            this.option = option;
        }

        public String toString()
        {
            return this.option;
        }
    }

    //Codicil search options
    //TODO - enum this so that it's type safe
    public static final String[] codicilSearchOptions = {"Name", "Item"};

    //Default codicil status
    //TODO - enum this so that it's type safe
    public static final String defaultCodicilStatus = "Open";

    //Certificate search options
    public enum CertificateSearchOptions
    {
        CERTIFICATE_NAME("Certificate Name"),
        CERTIFICATE_NUMBER("Certificate Number"),
        FORM_NUMBER("Form Number"),
        VERSION("Version"),
        OFFICE("Office");

        private final String option;

        CertificateSearchOptions(String option)
        {
            this.option = option;
        }

        public String toString()
        {
            return this.option;
        }
    }

    //Certificate sort options
    public enum CertificateSortOptions
    {
        CERTIFICATE_NAME("Certificate Name"),
        CERTIFICATE_NUMBER("Certificate Number"),
        FORM_NUMBER("Form Number"),
        VERSION("Version"),
        OFFICE("Office"),
        CERTIFICATE_STATUS("Certificate Status"),
        ISSUE_DATE("Issue Date"),
        ENDORSED_DATE("Endorsed Date"),
        EXPIRY_DATE("Expiry Date"),
        ISSUING_SURVEYOR("Issuing Surveyor");

        private final String option;

        CertificateSortOptions(String option)
        {
            this.option = option;
        }

        public String toString()
        {
            return this.option;
        }
    }

    //Certificate Status
    public enum CertificateStatus
    {
        ALL("All"),
        ISSUED("Issued"),
        WITHDRAWN("Withdrawn"),
        EXPIRED("Expired");

        private final String certStatus;

        CertificateStatus(String certStatus)
        {
            this.certStatus = certStatus;
        }

        public String toString()
        {
            return certStatus;
        }
    }
}
