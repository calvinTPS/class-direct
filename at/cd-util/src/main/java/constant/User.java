package constant;

public class User
{
    // emails
    public static final String NON_LR_EMAIL = "oscar3.classdirect@hotmail.com";
    public static final String CLIENT_EMAIL = "oscar.classdirect@gmail.com";
    public static final String LR_INTERNAL_EMAIL = "martin.williams@lr.org";
    public static final String EOR_EMAIL = "eor.classdirect@gmail.com";
    public static final String FLAG_EMAIL = "ella.classdirect@gmail.com";
    public static final String SHIP_BUILDER_EMAIL = "oscar.classdirect@hotmail.com";

    // This account type is not in the list (LR Internal or LR Application Support)
    public static final String NON_ADMIN_ACCOUNT_TYPE = "Non-Administrator";

    //User Account Types
    public enum UserAccountTypes
    {
        ALL("All"),
        CLIENT("Client"),
        EOR("Exhibition of Records"),
        SHIP_BUILDER("Ship builder"),
        FLAG("Flag"),
        EXTERNAL_NON_CLIENT("External Non-Client"),
        LR_ADMIN("LR Admin"),
        LR_INTERNAL("LR Internal"),
        LR_SUPPORT("LR Support");

        private final String accountType;

        UserAccountTypes(String accountType)
        {
            this.accountType = accountType;
        }

        public String toString()
        {
            return this.accountType;
        }
    }

    //User search options
    public enum UserSearchOptions
    {
        USER_EMAIL_ADDRESS("User Email Address"),
        USER_NAME("User Name"),
        COMPANY_NAME("Company Name"),
        CLIENT_CODE("Client Code"),
        SHIP_BUILDER_CODE("Ship Builder Code"),
        FLAG_CODE("Flag Code");

        private final String option;

        UserSearchOptions(String option)
        {
            this.option = option;
        }

        public String toString()
        {
            return this.option;
        }
    }

    //Account Status under Administration
    public enum AccountStatus
    {
        Active("Active"),
        Disabled("Disabled"),
        Archived("Archived");

        private final String accountStatus;

        AccountStatus(String accountStatus)
        {
            this.accountStatus = accountStatus;
        }

        public String getAccountStatus()
        {
            return accountStatus;
        }

    }

    public enum Client
    {
        CLIENT("100100"),
        SHIP_BUILDER("100101"),
        SHIP_BUILDER_DSME("3");

        private final String code;

        Client(String code)
        {
            this.code = code;
        }

        public String getCode()
        {
            return code;
        }
    }

    public enum Flag
    {
        ANGOLA("ALA", "Angola", 4),
        BANGLADESH("BNG", "Bangladesh", 14),
        JAPAN("JPN", "Japan", 99),
        JERSEY("CH2", "Jersey", 100),
        MEXICO("MEX", "Mexico", 126);

        private final String code;
        private final String name;
        private final int id;

        Flag(String code, String name, int id)
        {
            this.code = code;
            this.name = name;
            this.id = id;
        }

        public String getCode()
        {
            return code;
        }
        public String getName()
        {
            return name;
        }
        public int getId()
        {
            return id;
        }
    }
}
