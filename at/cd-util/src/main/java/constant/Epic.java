package constant;

public class Epic
{
    public static final String FLEET_DASHBOARD = "Fleet Dashboard";
    public static final String REQUEST_SURVEY = "Request Survey";
    public static final String NOTIFICATIONS = "Notifications";
    public static final String CERTIFICATES = "Certificates";
    public static final String GENERATE_AND_PRINT = "Generate and Print";
    public static final String PMS = "PMS";
    public static final String TAKSLISTS_AND_CHECKLISTS = "Tasklists and Checklists";
    public static final String ACCOUNT_REPORTING = "Account and Reporting";
    public static final String SINGLE_ASSET_CODICILS_AND_DEFECTS = "Single asset codicils and defects";
    public static final String USER_MANAGEMENT = "User Management";
    public static final String COUNTRY_FILES = "Country Files";
    public static final String ACCESS_CONTROL = "Access Control";
    public static final String ASSET_HUB = "Asset Hub";
    public static final String SERVICE_HISTORY = "Service History";
    public static final String EQUASIS_THESIS = "Equasis/Thesis";
    public static final String LOGIN_AND_LOGOUT = "Login and Logout";
    public static final String MILITARY_ACCESS = "Military Access";
    public static final String DUE_STATUS = "Due Status";
    public static final String SERVICE_SCHEDULE = "Service Schedule";
    public static final String STATUTORY_FINDINGS = "Statutory Findings";
}
