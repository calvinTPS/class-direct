package constant;

import org.openqa.selenium.By;

public class Config {
    //timeouts
    public static final long GLOBAL_SCRIPT_TIMEOUT = 120; //duration allowance for the spinner to continue in SECONDS
    public static final long GLOBAL_PAGE_LOAD_TIMEOUT = 25; //duration allowance for the page to finish loading in SECONDS
    public static final long GLOBAL_SCRIPT_THINK_TIME = 500; //duration for interacted elements to think before automatically triggering further automated actions in MILLISECONDS
    public static final long GLOBAL_ANIMATION_TRANSITION_TIME = 400; //duration for element to finish animation in MILLISECONDS
    public static final long GLOBAL_SPINNER_SPAWN_TIME = 1; //duration for delay in spinner to spawn in SECONDS

    //common URL
    public static final String HTTP = "http://";
    public static final String MAIN_URL = "/main";
    public static final String VESSEL_LIST_URL = "/fleet/vessel-list";
    public static final String TOC_URL = "/accept-terms-and-conditions";

    //common regex
    public static final String HTTP_OR_HTTPS_REGEX = "^https?://";

    //common css
    public static final By SPINNER = By.cssSelector(".loading-bar .spinner, "
            + ".loading-message .spinner, "
            + "spinner.top-spinner .spinner");//these are the list of all possible spinner css which has changed, unchanged, re-changed over the
    // course development of the application. Pray to the C'thulhu that one day they stop touching it. Stop please!
    public static final By DIALOG = By.cssSelector("md-dialog"); //modal dialog
    public static final By LOADING_BAR = By.cssSelector("#loading-bar"); //slim loading bar at the top of the page
}
