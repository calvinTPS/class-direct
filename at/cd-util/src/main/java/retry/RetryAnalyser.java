package retry;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Config;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import java.lang.reflect.Method;

public class RetryAnalyser implements IRetryAnalyzer
{
    private static int currentRetry = 0;
    private static Boolean isBlockedByTOC = false;

    @Override public boolean retry(ITestResult result)
    {
        String failureCause = result.getThrowable().toString();
        MaxRetryCount testRetryValue = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(MaxRetryCount.class);
        int maxTestRetry = (testRetryValue == null) ? getDefaultMaxRetry() : testRetryValue.value();
        return ++currentRetry <= maxTestRetry && performTestRetry(failureCause);
    }

    public static int getCurrentRetry()
    {
        return currentRetry;
    }

    public static Boolean wasCausedByTOC()
    {
        return isBlockedByTOC;
    }

    private Integer getDefaultMaxRetry()
    {
        Class<?> clazz = MaxRetryCount.class;
        try
        {
            Method method = clazz.getDeclaredMethod("value");
            return (Integer) method.getDefaultValue();
        }
        catch (NoSuchMethodException e)
        {
            //Not going to happen. Promise.
        }
        return 0;
    }

    private Boolean performTestRetry(String failureCause)
    {
        isBlockedByTOC = BaseTest.getDriver().getCurrentUrl().contains(Config.TOC_URL) &&
                failureCause.contains(TimeoutException.class.getName());
        return failureCause.contains(StaleElementReferenceException.class.getName()) ||
                isBlockedByTOC;
    }

}
