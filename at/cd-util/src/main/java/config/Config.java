package config;

public class Config
{
    public static String getBaseURL()
    {
        String s = System.getProperty("environmentKey");
        if (s != null) //get the environment IP if one is defined
        {
            return s;
        }
        else //else assume it is localhost:8080
        {
            return "localhost:8080";
        }
    }

    // this is typically used when the API endpoints uses different URI from the UI
    public static String getBaseAPI()
    {
        String s = System.getProperty("apiKey");
        if(s != null) //get the api URL if one is defined
        {
            return s;
        }
        else //else assume it is the same with getBaseURL()
        {
            return Config.getBaseURL();
        }
    }

    public static String getDataBaseIp()
    {
        String s = System.getProperty("dbKey");
        //get the environment IP if one is defined
        if (s != null)
        {
            return s;
        }
        //else assume it is localhost
        else
        {
            return "localhost:3306";
        }
    }
}
