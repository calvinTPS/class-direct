package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.base.Predicate;
import helper.AppHelper;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;

public class Datepicker extends TypifiedElement
{

    private static final String BUTTON_CSSSELECTOR = "button";
    private static final String INPUT_CSSSELECTOR = "input.md-datepicker-input";
    private static final String CALENDARPANE_CSSSELECTOR = "div.md-calendar-scroll-mask";
    private static final String SEARCH_DATE_CSSSELECTOR = "td[data-timestamp='%s'][role='gridcell']";
    private static final String FOCUS_DATE_CSSSELECTOR = "td.md-focus";
    private static final String ANY_DATE_CSSSELECTOR = "td[data-timestamp][role='gridcell']";
    private static final String ERROR_MESSAGE_XPATH = "following-sibling::div[contains(@data-ng-messages, 'error')]";
    private static final String SUBMIT_BUTTON_CSSSELECTOR = ".filter button[aria-label='Submit']";
    private static final int MAX_LOOPS = 10;
    private WebDriver driver = BaseTest.getDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 5);

    public Datepicker(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getDatepicker()
    {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::md-datepicker"));
    }

    public String getSelectedDate()
    {
        return getDatepicker().findElement(By.cssSelector(INPUT_CSSSELECTOR)).getAttribute("value");
    }

    public void selectByCalendarPane(LocalDate date)
    {
        getDatepicker().findElement(By.cssSelector(BUTTON_CSSSELECTOR)).click();
        WebElement calendarElement = driver.findElement(By.cssSelector(CALENDARPANE_CSSSELECTOR));
        wait.until(ExpectedConditions.visibilityOf(calendarElement));

        Timestamp timestamp = Timestamp.valueOf(date.atStartOfDay());
        String dateCssSelector = String.format(SEARCH_DATE_CSSSELECTOR, timestamp.getTime());
        List<WebElement> dateElements = getCalendarDateElements(dateCssSelector);
        if (dateElements.size() == 0)
        {
            Long focusDateTimestamp = Long.parseLong(driver.findElement(By.cssSelector(FOCUS_DATE_CSSSELECTOR)).getAttribute("data-timestamp"));
            LocalDate focusDate = new Timestamp(focusDateTimestamp).toLocalDateTime().toLocalDate();
            int counter = 0;
            if (date.isBefore(focusDate))
            {
                do
                {
                    WebElement firstLoadedDate = getCalendarDateElements(ANY_DATE_CSSSELECTOR).get(0);
                    AppHelper.scrollToMiddle(firstLoadedDate);
                    dateElements = getCalendarDateElements(dateCssSelector);
                    counter++;
                }
                while (dateElements.size() == 0 && counter < MAX_LOOPS);
            }
            else
            {
                do
                {
                    List<WebElement> allLoadedDates = getCalendarDateElements(ANY_DATE_CSSSELECTOR);
                    WebElement lastLoadedDate = allLoadedDates.get(allLoadedDates.size() - 1);
                    AppHelper.scrollToMiddle(lastLoadedDate);
                    dateElements = getCalendarDateElements(dateCssSelector);
                    counter++;
                }
                while (dateElements.size() == 0 && counter < MAX_LOOPS);
            }
        }

        AppHelper.scrollToMiddle(dateElements.get(0));
        dateElements.get(0).click();
        wait.until(ExpectedConditions.stalenessOf(calendarElement));
    }

    public void selectByCalendarPane(String dateString)
    {
        LocalDate dateTime = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("dd MMM yyyy"));
        selectByCalendarPane(dateTime);
    }

    @Override
    public void sendKeys(CharSequence... keysToSend)
    {
        typifiedelement.WebElement input = new typifiedelement.WebElement(getDatepicker().findElement(By.cssSelector(INPUT_CSSSELECTOR)));
        input.sendKeys(keysToSend);

        if (driver.findElements(By.cssSelector(SUBMIT_BUTTON_CSSSELECTOR)).size() > 0)
            wait.until((Predicate<WebDriver>) webDriver -> webDriver.findElement(By.cssSelector(SUBMIT_BUTTON_CSSSELECTOR)).isEnabled());
        try
        {
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
        }
        catch (InterruptedException i)
        {
            i.printStackTrace();
        }
    }

    @Override
    public void clear()
    {
        WebElement inputElement = getDatepicker().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        inputElement.sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.DELETE);
        try
        {
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public String getErrorMessage()
    {
        try
        {
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        return getDatepicker().findElement(By.xpath(ERROR_MESSAGE_XPATH)).getText().trim();
    }

    @Override
    public boolean isDisplayed()
    {
        return getDatepicker().isDisplayed();
    }

    private List<WebElement> getCalendarDateElements(String cssSelector)
    {
        List<WebElement> dateElements;

        try
        {
            dateElements = driver.findElements(By.cssSelector(cssSelector));
            dateElements.parallelStream().forEach(s -> wait.withTimeout(200, TimeUnit.MILLISECONDS).until(ExpectedConditions.stalenessOf(s)));
        }
        catch (StaleElementReferenceException | TimeoutException e)
        {
            dateElements = driver.findElements(By.cssSelector(cssSelector));
        }

        return dateElements;
    }
}
