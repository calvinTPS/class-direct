package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;

public class Dropdown extends TypifiedElement {

    private final String OPTION_CSSSELECTOR = "[class*='md-select-menu-container'][aria-hidden='false'] md-option";
    private WebDriver driver = BaseTest.getDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 5);

    public Dropdown(WebElement wrappedElement) {
        super(wrappedElement);
    }

    private WebElement getSelect() {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::md-input-container"));
    }

    private List<WebElement> getOptions() {
        WebElement e = getSelect().findElement(By.cssSelector("md-select"));
        AppHelper.scrollToMiddle(e);
        e.click();
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                By.cssSelector(OPTION_CSSSELECTOR)));
        return driver.findElements(By.cssSelector(OPTION_CSSSELECTOR));
    }
    public void closeOptionList() {
        List<WebElement> options = driver.findElements(By.cssSelector(OPTION_CSSSELECTOR));
        options.get(0).sendKeys(Keys.ESCAPE);
        wait.until(ExpectedConditions.invisibilityOfAllElements(options));
    }

    public void selectByText(String text) {
        List<WebElement> options = getOptions();
        WebElement o = options.stream()
                .filter(e -> Objects.equals(e.getText(), text))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Option with text: " + text + " not found"));
        AppHelper.scrollToMiddle(o);
        o.click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(options));
    }

    public void selectByIndex(int index) {
        List<WebElement> options = getOptions();
        AppHelper.scrollToMiddle(options.get(index));
        options.get(index).click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(options));
    }

    public String getSelectedOption() {
        return getSelect().findElement(By.cssSelector("md-select-value")).getText();
    }

    public List<String> getOptionList()
    {
        return getOptions().stream().map(WebElement::getText).collect(Collectors.toList());
    }
}
