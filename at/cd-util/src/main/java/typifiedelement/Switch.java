package typifiedelement;

import helper.AppHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class Switch extends TypifiedElement {
    public Switch(WebElement wrappedElement) {
        super(wrappedElement);
    }

    private WebElement getSwitch(){
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::md-switch"));
    }

    public boolean isSelected(){
        return getSwitch().getAttribute("aria-checked").equals("true");
    }

    public void select(){
        WebElement e = getSwitch();
        if(!isSelected()){
            AppHelper.scrollToMiddle(e);
            e.click();
        }
    }

    public void deselect(){
        WebElement e = getSwitch();
        if(isSelected()){
            AppHelper.scrollToMiddle(e);
            e.click();
        }
    }
}
