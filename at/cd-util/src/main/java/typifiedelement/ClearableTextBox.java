package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import helper.ExtraExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import static constant.Config.GLOBAL_SCRIPT_THINK_TIME;


public class ClearableTextBox extends TypifiedElement {

    private final String CLEAR_BUTTON_XPATH = "./following-sibling::div[@role='button'] | " +
            "./following-sibling::div[contains(@class,'clear-btn')]";

    public ClearableTextBox(WebElement wrappedElement) {
        super(wrappedElement);
    }

    private WebElement getTextBox() {
        String tag = this.getWrappedElement().getTagName();

        String xpath;
        if (!tag.equals("input"))
            xpath = "./descendant::input | ./preceding-sibling::input";
        else
            xpath = "self::input";
        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    public void clickClear() {
        WebElement e = getTextBox().findElement(By.xpath(CLEAR_BUTTON_XPATH));
        AppHelper.scrollToMiddle(e);
        e.click();
        BaseTest.getWait().until(ExpectedConditions.not(ExpectedConditions.attributeContains(e, "class", "show")));
    }

    @Override
    public void sendKeys(CharSequence... var) {
        getTextBox().sendKeys(var);
        //the input box anticipates input and queries after fixed amount of time has been lapsed
        FluentWait<WebDriver> wait = new FluentWait<>(BaseTest.getDriver());
        Predicate<WebDriver> p = driver -> !getTextBox().isEnabled();
        try {
            wait.withTimeout(GLOBAL_SCRIPT_THINK_TIME, TimeUnit.MILLISECONDS).until((com.google.common.base.Predicate<WebDriver>) p::test);
        } catch (TimeoutException ignore) {

        }
        BaseTest.getWait().until(driver -> getTextBox().isEnabled());
        BaseTest.getWait().until(ExtraExpectedConditions.invisibilityOfSpinner());
    }

    @Override
    public String getText() {
        return getTextBox().getAttribute("value");
    }
}
