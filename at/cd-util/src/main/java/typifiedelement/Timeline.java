package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.frameworkium.core.ui.tests.BaseTest.getDriver;
import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;

public class Timeline extends TypifiedElement
{
    private static final String TIMELINE_PIPS = "timeline-pips-offset span";
    private static final String TIMELINE_PIP_LABEL = "span.hide-sm";

    private static final String TODAY_CSSSELECTOR = ".timeline-bar div.today";
    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd MMM yy");

    private static final String BUTTON_CSSSELECTOR = "button";
    private static final String CALENDARPANE_CSSSELECTOR = "div.md-calendar-scroll-mask";
    private static final String SEARCH_DATE_CSSSELECTOR = "td[data-timestamp='%s'][role='gridcell']";

    private WebDriver driver = BaseTest.getDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 5);

    public Timeline(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getTimeline()
    {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::div[@class='date-range-sticky']"));
    }

    private WebElement getDatePickerFrom()
    {
        return getTimeline().findElement(By.cssSelector("md-datepicker.date-from"));
    }

    private WebElement getDatePickerTo()
    {
        return getTimeline().findElement(By.cssSelector("md-datepicker.date-to"));
    }

    private void selectByCalendarPane(LocalDate date, WebElement datepicker)
    {
        //click datepicker button
        WebElement datepickerButton = datepicker.findElement(By.cssSelector(BUTTON_CSSSELECTOR));
        AppHelper.scrollToMiddle(datepickerButton);
        datepickerButton.click();

        WebElement calendarElement = driver.findElement(By.cssSelector(CALENDARPANE_CSSSELECTOR));
        wait.until(ExpectedConditions.visibilityOf(calendarElement));

        try {
            // click first or last visible month year
            List<WebElement> monthLabels = calendarElement.findElements(By.cssSelector("td.md-calendar-month-label:not(.md-calendar-month-label-disabled)"));
            int index = datepicker.getAttribute("class").contains("date-from") ? 0 : monthLabels.size() -1;
            WebElement monthLabel = monthLabels.get(index);
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
            new Actions(getDriver()).moveToElement(monthLabel).build().perform();
            monthLabel.click();

            // click month
            String dateCssSelector = String.format(SEARCH_DATE_CSSSELECTOR, Timestamp.valueOf(date.atStartOfDay()).getTime());
            WebElement monthToSelect = driver.findElement(By.cssSelector(dateCssSelector));
            new Actions(getDriver()).moveToElement(monthToSelect).build().perform();
            monthToSelect.click();
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));

            // click day
            driver.findElement(By.cssSelector(dateCssSelector)).click();
            wait.until(ExpectedConditions.stalenessOf(calendarElement));
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
        } catch (TimeoutException ignore) {

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public LocalDate getSelectedStartDate()
    {
        List<WebElement> timelinePips = getTimeline().findElements(By.cssSelector(TIMELINE_PIPS));
        for (int i = 0; i < timelinePips.size(); i++)
        {
            List<WebElement> timelinePipLabel = timelinePips.get(i).findElements(By.cssSelector(TIMELINE_PIP_LABEL));
            if(timelinePipLabel.size() > 0)
            {
                return convertStringToDate(timelinePipLabel.get(0).getText().replace("'", "")).minusMonths(i);
            }
        }
        throw new NoSuchElementException("No date label found in the timeline.");
    }

    public LocalDate getSelectedEndDate()
    {
        List<WebElement> timelinePips = getTimeline().findElements(By.cssSelector(TIMELINE_PIPS));
        int size = timelinePips.size() -1;
        for (int i = size; i > 0; i--)
        {
            List<WebElement> timelinePipLabel = timelinePips.get(i).findElements(By.cssSelector(TIMELINE_PIP_LABEL));
            if(timelinePipLabel.size() > 0)
            {
                return convertStringToDate(timelinePipLabel.get(0).getText().replace("'", "")).plusMonths(size-i-2);
            }
        }
        throw new NoSuchElementException("No date label found in the timeline.");
    }

    public boolean isTodayDisplayed()
    {
        WebElement today = getTimeline().findElement(By.cssSelector(TODAY_CSSSELECTOR));
        AppHelper.scrollToMiddle(today);
        return today.isDisplayed();
    }

    public Timeline selectStartDate(LocalDate startDate)
    {
        if(startDate.isBefore(getSelectedEndDate()))
        {
            selectByCalendarPane(startDate, getDatePickerFrom());
        }
        else
        {
            throw new IllegalArgumentException(String.format("Start date '%s' should be less than the end date '%s'.",
                    startDate, getSelectedEndDate()));
        }
        return this;
    }

    public Timeline selectEndDate(LocalDate endDate)
    {
        if(endDate.isAfter(getSelectedStartDate()))
        {
            selectByCalendarPane(endDate, getDatePickerTo());
        }
        else
        {
            throw new IllegalArgumentException(String.format("End date '%s' should be greater than the start date '%s'.",
                    endDate, getSelectedStartDate()));
        }
        return this;
    }

    private LocalDate convertStringToDate(String dateString)
    {
        return LocalDate.parse(String.format("01 %s", dateString), DATETIME_FORMATTER);
    }
}
