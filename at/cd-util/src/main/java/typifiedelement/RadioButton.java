package typifiedelement;

import helper.AppHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class RadioButton extends TypifiedElement
{

    private final String CLICKABLE_XPATH = "./descendant::div[contains(@class,'md-container')]";
    private final String LABEL_XPATH = "./descendant::div[@class='md-label']/span";

    public RadioButton(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getButton()
    {
        String tag = this.getWrappedElement().getTagName();

        String xpath;
        if (!tag.equals("md-radio-button"))
            xpath = "./ancestor::md-radio-button";
        else
            xpath = "self::md-radio-button";
        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    public void select()
    {
        if (!this.isSelected())
        {
            WebElement e = getButton().findElement(By.xpath(CLICKABLE_XPATH));
            AppHelper.scrollToMiddle(e);
            e.click();
        }
    }

    public void deselect()
    {
        if (this.isSelected())
        {
            WebElement e = getButton().findElement(By.xpath(CLICKABLE_XPATH));
            AppHelper.scrollToMiddle(e);
            e.click();
        }
    }

    @Override
    public boolean isSelected()
    {
        String b = getButton().getAttribute("aria-checked");
        return b.equals("true");
    }

    @Override
    public String getText()
    {
        return getButton().findElement(By.xpath(LABEL_XPATH)).getText();
    }
}
