package typifiedelement;

import helper.AppHelper;
import helper.ExtraExpectedConditions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.frameworkium.core.ui.tests.BaseTest.getDriver;
import static com.frameworkium.core.ui.tests.BaseTest.newWaitWithTimeout;
import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;
import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;

public class WebElement extends TypifiedElement implements Locatable
{
    public WebElement(org.openqa.selenium.WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private final int MAX_ATTEMPT = 4;
    private ThreadLocal<Integer> attempt = ThreadLocal.withInitial(() -> 0);
    boolean lockScroll = false;

    @Override
    public void click()
    {
        if (!lockScroll)
        {
            AppHelper.scrollToMiddle(this);
        }
        newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        try
        {
            super.click();
        }
        catch (WebDriverException e)
        { //possible occlusion by animation
            if (attempt.get() < MAX_ATTEMPT - 2)
            {
                try
                {
                    Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
                }
                catch (InterruptedException e1)
                {
                    e1.printStackTrace();
                }
                attempt.set(attempt.get() + 1);
                AppHelper.scrollToBottom(); // jostle the position to bottom in case its permanently occluded by a bottom element
                this.click(); //re-assess the situation
            }
            else if (attempt.get() < MAX_ATTEMPT - 1)
            {
                attempt.set(attempt.get() + 1);
                AppHelper.scrollToTop(); //jostle the position to top in case its permanently occluded by a top element
                this.click(); //re-assess the situation
            }
            else
            {
                AppHelper.scrollIntoView(this);
                super.click(); //try final time, if fail throws WebDriverException and give up
            }
        }
        finally
        {
            attempt.set(0); //reset
        }
    }

    @Override
    public void submit()
    {
        if (!lockScroll)
        {
            AppHelper.scrollToMiddle(this);
        }
        newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        super.submit();
    }

    @Override
    public void sendKeys(CharSequence... charSequences)
    {
        if (!lockScroll)
        {
            AppHelper.scrollToMiddle(this);
        }
        newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());

        // If its a special character then don't use the retry mechanism
        if (Arrays.stream(charSequences)
                .anyMatch(charSequence -> charSequence.getClass().isEnum()))
        {
            super.sendKeys(charSequences);
        }
        else
        {
            sendKeysWithRetry(charSequences);
        }
    }

    /***
     * Set {@code charSequences} into input element while taking into consideration pre-existing string already in the element
     *
     * <p>
     *     Character sequence to be type in will be appended to the pre-existing string in the element
     *     This works with both text-based input or attribute-value based inputs
     *     Not compatible with Keys special character enum set and should be excluded before invoking this
     * </p>
     * @param charSequences
     */
    private void sendKeysWithRetry(CharSequence[] charSequences)
    {
        String preExisting = this.getAttribute("value") != null ? this.getAttribute("value") : this.getText();
        String expected = preExisting + String.join(",", charSequences);
        String missing = expected;
        String observed = "";
        final int MAX_RETRY = 10;
        ThreadLocal<Integer> attempt = ThreadLocal.withInitial(() -> 0);

        //do sendKey and retry if its interrupted
        do
        {
            //find what characters were NOT previously successfully typed in
            int diff = expected.compareTo(observed);
            if (diff < 0)
            { //characters typed in has been mutated beyond recognition by external means, leave it and return
                return;
            }
            else
            {
                missing = expected.substring(expected.length() - diff);
            }
            //attempt to type the rest of the missed characters
            super.sendKeys(missing);
            //verify what has successfully typed in
            observed = this.getAttribute("value") != null ? this.getAttribute("value") : this.getText();
            attempt.set(attempt.get() + 1);
        }
        while (!observed.equals(expected) && attempt.get() <= MAX_RETRY);
    }

    @Override
    public void clear()
    {
        if (!lockScroll)
        {
            AppHelper.scrollToMiddle(this);
        }
        newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        super.clear();
    }

    @Override
    public String getTagName()
    {
        return super.getTagName();
    }

    @Override
    public String getAttribute(String s)
    {
        return super.getAttribute(s);
    }

    @Override
    public boolean isSelected()
    {
        newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        return super.isSelected();
    }

    @Override
    public boolean isEnabled()
    {
        newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        return super.isEnabled();
    }

    @Override
    public String getText()
    {
        newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        if (!lockScroll)
        {
            this.scrollToMiddle();
        }
        return super.getText();
    }

    @Override
    public List<org.openqa.selenium.WebElement> findElements(By by)
    {
        return super.findElements(by);
    }

    @Override
    public org.openqa.selenium.WebElement findElement(By by)
    {
        return super.findElement(by);
    }

    @Override
    public boolean isDisplayed()
    {
        return super.isDisplayed();
    }

    public boolean isExists()
    {
        try
        {
            this.isDisplayed();
            return true;
        }
        catch (NoSuchElementException var2)
        {
            return false;
        }
    }

    @Override
    public Point getLocation()
    {
        return super.getLocation();
    }

    @Override
    public Dimension getSize()
    {
        return super.getSize();
    }

    @Override
    public Rectangle getRect()
    {
        return super.getRect();
    }

    @Override
    public String getCssValue(String s)
    {
        return super.getCssValue(s);
    }

    public org.openqa.selenium.WebElement getWrappedElement()
    {
        return super.getWrappedElement();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException
    {
        return super.getScreenshotAs(outputType);
    }

    @Override
    public Coordinates getCoordinates()
    {
        return null;
    }

    public WebElement scrollToMiddle()
    {
        AppHelper.scrollToMiddle(this);
        return this;
    }

    public WebElement moveToElement()
    {
        new Actions(getDriver()).moveToElement(this.getWrappedElement()).build().perform();
        return this;
    }

    /***
     * Disables the default scrolling to element behaviour
     *
     * <p>Useful when scrolling to the element interferes interactions with it</p>
     * @return this element
     */

    public WebElement lockScroll()
    {
        lockScroll = true;
        return this;
    }
}
