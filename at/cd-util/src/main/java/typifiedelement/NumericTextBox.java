package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class NumericTextBox extends TypifiedElement implements Locatable
{

    private final String INPUT_CSSSELECTOR = "input";
    private final String LABEL_CSSSELECTOR = "label";

    public NumericTextBox(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getTextBox()
    {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::md-input-container"));
    }

    public String getLabelText()
    {
        return this.getTextBox().findElement(By.cssSelector(LABEL_CSSSELECTOR)).getText();
    }

    public void clickCountUp()
    {
        WebElement inputTag = getTextBox().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        AppHelper.scrollToMiddle(inputTag);
        Actions actions = new Actions(BaseTest.getDriver());
        actions.moveToElement(inputTag,
                inputTag.getSize().getWidth() - 22,
                (inputTag.getSize().getHeight() / 2) - 4)
                .click()
                .build()
                .perform();
    }

    public void clickCountDown()
    {
        WebElement inputTag = getTextBox().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        AppHelper.scrollToMiddle(inputTag);
        Actions actions = new Actions(BaseTest.getDriver());
        actions.moveToElement(inputTag,
                inputTag.getSize().getWidth() - 22,
                (inputTag.getSize().getHeight() / 2) + 4)
                .click()
                .build()
                .perform();
    }

    @Override
    public String getText()
    {
        WebElement inputTag = getTextBox().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        return inputTag.getAttribute("value");
    }

    @Override
    public void sendKeys(CharSequence... keysToSend)
    {
        WebElement inputTag = getTextBox().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        inputTag.sendKeys(keysToSend);
    }

    @Override
    public void clear()
    {
        WebElement inputTag = getTextBox().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        AppHelper.scrollToMiddle(inputTag);
        inputTag.clear();
    }

    @Override
    public Coordinates getCoordinates()
    {
        return null;
    }
}
