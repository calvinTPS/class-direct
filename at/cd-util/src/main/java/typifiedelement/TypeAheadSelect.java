package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import helper.ExtraExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;
import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;

public class TypeAheadSelect extends TypifiedElement
{

    private final String DROPDOWN_ANCESTOR_TAG = "md-virtual-repeat-container";
    private final String OPTIONS_CSS = "span";
    private final String INPUT_TAG = "input";
    private final String INPUT_ANCESTOR_TAG_1 = "md-autocomplete-wrap";
    private final String INPUT_ANCESTOR_TAG_2 = "md-autocomplete";
    private final String DROPDOWN_CHILD_TAG_1 = "li";
    private final String DROPDOWN_CHILD_TAG_2 = "ul";

    public TypeAheadSelect(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    /**
     * This will be used as a reference point for all TypeAheadSelect method interactions
     *
     * @return Input element of the TypeAheadSelect
     */

    //normalize the selected element
    private WebElement getInput()
    {
        String tag = this.getWrappedElement().getTagName();
        String id;
        WebElement element;

        switch (tag)
        {
            case INPUT_TAG: //at input and/or its ancestors
            case INPUT_ANCESTOR_TAG_1:
            case INPUT_ANCESTOR_TAG_2:
                element = BaseTest.getDriver().findElement(By.xpath("./descendant-or-self::input"));
                break;
            case DROPDOWN_CHILD_TAG_1: //at the dropdown's children
            case DROPDOWN_CHILD_TAG_2:
                id = this.getWrappedElement().findElement(By.xpath("./self-or-ancestor::ul")).getAttribute("id");
                element = BaseTest.getDriver().findElement(By.cssSelector("input[id='" + id + "']"));
                break;
            case DROPDOWN_ANCESTOR_TAG: //at the dropdown's parent
                id = this.getWrappedElement().findElement(By.xpath("./self-or-descendant::ul")).getAttribute("id");
                element = BaseTest.getDriver().findElement(By.cssSelector("input[id='" + id + "']"));
                break;
            default: // don't know where it is/ user using TypeAheadSelect erroneously - return self
                element = BaseTest.getDriver().findElement(By.xpath("self"));
        }
        return element;
    }

    /**
     * This will be used as the reference point for all dropdown method interactions
     *
     * @return dropdown element controlled by the corresponding {@code getInput()}
     */
    private WebElement getSelect()
    {
        String id = getInput().getAttribute("aria-owns");
        return BaseTest.getDriver()
                .findElement(By.cssSelector(DROPDOWN_ANCESTOR_TAG + " ul[id='" + id + "']"));
    }

    /**
     * Returns all options belonging to this select tag.
     *
     * @return A list of {@code WebElements} representing options.
     */
    private List<WebElement> getOptions()
    {
        return getSelect().findElements(By.cssSelector(OPTIONS_CSS));
    }

    /**
     * Select the option at the given index. This is done by examining the "index" attribute of an
     * element, and not merely by counting.
     *
     * @param index The option at this index will be selected
     */
    public void selectByIndex(int index)
    {
        if (index < this.getOptions().size())
        {
            WebElement e = getOptions().get(index);
            AppHelper.scrollToMiddle(e);
            e.click();
        }
        else
            throw new IndexOutOfBoundsException("No options were available at index " + index);
    }

    /**
     * Select all options that display text matching the argument. That is, when given "Bar" this
     * would select an option like:
     * <p>
     * &lt;option value="foo"&gt;Bar&lt;/option&gt;
     *
     * @param text The visible text to match against
     */
    public void selectByVisibleText(String text)
    {
        WebElement e = getOptions().stream()
                .filter(o -> o.getText().equals(text))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Option not found: " + text));
        AppHelper.scrollToMiddle(e);
        e.click();
    }

    /**
     * Select the option based on the given text. If the given text is not present(since it is displayed based on the entries in DB), the element in first index will be selected
     *
     * @param text The option with this text will be selected
     */
    public void selectByVisibleOrFirstText(String text)
    {
        WebElement e = getOptions().stream()
                .filter(o -> o.getText().equals(text))
                .findFirst()
                .orElse(getOptions().get(0));
        AppHelper.scrollToMiddle(e);
        e.click();
    }

    /**
     * Check if the type ahead dropdown list is visible
     *
     * @return true if the choices are visible
     */
    public boolean isTypeAheadDropdownVisible()
    {
        return !getSelect().getAttribute("class").contains("ng-hide");
    }

    /**
     * Input characaters into the input box
     *
     * @param charSequence input to be typed in
     */
    public void sendKeys(CharSequence... charSequence)
    {
        super.sendKeys(charSequence);
        try
        {
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
    }
}
