package typifiedelement;

import helper.AppHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RadioGroup extends TypifiedElement
{
    private static final String RADIOBUTTON_ENABLED_CSSSELECTOR = "md-radio-button[aria-disabled='false']";
    private static final String RADIOBUTTON_DISABLED_CSSSELECTOR = "md-radio-button[aria-disabled='true']";
    private static final String RADIOBUTTON_CHECKED_CSSSELECTOR = "md-radio-button[aria-checked='true']";

    public RadioGroup(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getRadioGroup()
    {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::md-radio-group"));
    }

    public List<typifiedelement.WebElement> getOptions()
    {
        List<typifiedelement.WebElement> options = new ArrayList<>();
        Stream.of(getEnabled(), getDisabled()).forEach(options::addAll);
        return options;
    }

    public List<typifiedelement.WebElement> getEnabled()
    {
        return getRadioGroup()
                .findElements(By.cssSelector(RADIOBUTTON_ENABLED_CSSSELECTOR))
                .stream()
                .map(typifiedelement.WebElement::new)
                .collect(Collectors.toList());
    }

    public List<typifiedelement.WebElement> getDisabled()
    {
        return getRadioGroup()
                .findElements(By.cssSelector(RADIOBUTTON_DISABLED_CSSSELECTOR))
                .stream()
                .map(typifiedelement.WebElement::new)
                .collect(Collectors.toList());
    }

    public typifiedelement.WebElement getSelected()
    {
        return new typifiedelement.WebElement(getRadioGroup().findElement(By.cssSelector(RADIOBUTTON_CHECKED_CSSSELECTOR)));
    }

    public void selectByIndex(int index)
    {
        List<typifiedelement.WebElement> enabledOptions = getEnabled();
        if (index >= enabledOptions.size())
        {
            throw new NoSuchElementException(String.format("Option with index '%s' is either disabled or does not exist.", index));
        }
        AppHelper.scrollToMiddle(enabledOptions.get(index));
        enabledOptions.get(index).click();
    }

    public void selectByName(String name)
    {
        WebElement r = getEnabled().stream()
                .filter(e -> e.getText().equals(name))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Option with name '%s' is either disabled or does not exist.", name)));
        AppHelper.scrollToMiddle(r);
        r.click();

    }
}
