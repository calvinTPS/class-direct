package typifiedelement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class Star extends TypifiedElement
{

    private final String STATUS_CSS = ".star-container";

    public Star(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getStar()
    {
        String tag = this.getWrappedElement().getTagName();

        String xpath;
        if (!tag.equals("star"))
            xpath = "./ancestor::star";
        else
            xpath = "self::star";
        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    public boolean isFavourited()
    {
        return !getStar().findElement(By.cssSelector(STATUS_CSS)).getAttribute("class").contains("inactive");
    }

    public void select()
    {
        WebElement star = getStar();
        if (!this.isFavourited())
        {
            new typifiedelement.WebElement(star.findElement(By.cssSelector(STATUS_CSS))).click();
        }
    }

    public void deselect()
    {
        WebElement star = getStar();
        if (this.isFavourited())
        {
            new typifiedelement.WebElement(star.findElement(By.cssSelector(STATUS_CSS))).click();
        }
    }

}
