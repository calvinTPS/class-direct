package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import org.apache.commons.lang.NotImplementedException;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static constant.Config.GLOBAL_SCRIPT_THINK_TIME;

public class TimePicker extends TypifiedElement
{
    private static final String LABEL_CSSSELECTOR = "label";
    private static final String INPUT_CSSSELECTOR = "input";
    private static final String OPTION_LIST_CSSSELECTOR = "md-virtual-repeat-container[aria-hidden='false']";
    private static final String OPTION_CSSSELECTOR = OPTION_LIST_CSSSELECTOR + " ul li";
    private static final String SELECTED_OPTION_CSSSELECTOR = "span[data-md-highlight-text='%s'] span.highlight";
    private static final String ERROR_MESSAGE_CSSSELECTOR = "div[data-ng-messages*=error]";
    private static final int MAX_LOOPS = 100;
    private WebDriver driver = BaseTest.getDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 5);

    public TimePicker(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getTimePicker()
    {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::md-autocomplete"));
    }

    public String getLabelText()
    {
        return getTimePicker().findElement(By.cssSelector(LABEL_CSSSELECTOR)).getText();
    }

    public String getErrorMessage()
    {
        WebElement errMessage = getTimePicker().findElement(By.cssSelector(ERROR_MESSAGE_CSSSELECTOR));
        if (errMessage.findElements(By.cssSelector("div")).size() < 0)
        {
            //wait for it to appear with timeout
            try
            {
                wait.withTimeout(GLOBAL_SCRIPT_THINK_TIME, TimeUnit.MILLISECONDS).until(ExpectedConditions.visibilityOf(errMessage.findElement(By.cssSelector("div"))));
            }
            catch (Exception ignored)
            {
            }
        }
        else
        {
            try //wait for it to disappear with timeout
            {
                wait.withTimeout(GLOBAL_SCRIPT_THINK_TIME, TimeUnit.MILLISECONDS).until(ExpectedConditions.invisibilityOfElementLocated(By
                        .cssSelector(ERROR_MESSAGE_CSSSELECTOR + " div")));
            }
            catch (Exception ignored)
            {
            }
        }
        return getTimePicker().findElement(By.cssSelector(ERROR_MESSAGE_CSSSELECTOR)).getText();
    }

    @Override
    public String getText()
    {
        String searchText = getTimePicker().getAttribute("data-md-search-text");
        return driver.findElement(By.cssSelector(String.format(SELECTED_OPTION_CSSSELECTOR, searchText))).getAttribute("innerHTML");
    }

    @Override
    public void clear()
    {
        WebElement e = getTimePicker().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        AppHelper.scrollToMiddle(e);
        e.sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.DELETE, Keys.ESCAPE);
    }

    @Override
    public void click(){
        WebElement e = getTimePicker().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        AppHelper.scrollToMiddle(e);
        e.click();
    }

    @Override
    public void sendKeys(CharSequence... keysToSend)
    {
        WebElement e = getTimePicker().findElement(By.cssSelector(INPUT_CSSSELECTOR));
        AppHelper.scrollToMiddle(e);
        e.sendKeys(keysToSend);
    }

    public TimePicker selectFromList(String time)
    {
        if (Integer.parseInt(time.split(":")[1]) % 10 == 0) // if no text entered, options have 10 minutes interval by default
        {
            // Open the list with options
            WebElement e = getTimePicker().findElement(By.cssSelector(INPUT_CSSSELECTOR));
            AppHelper.scrollToMiddle(e);
            e.click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(OPTION_LIST_CSSSELECTOR)));

            // Check if the requested time is loaded
            List<WebElement> searchedTime = getOptions().stream().filter(o -> o.getText().equals(time)).collect(Collectors.toList());
            if (searchedTime.size() == 0) // Need to scroll down/up to find the time
            {
                int timeInMinutes = convertToMinutes(time);
                WebElement firstLoadedTime = getOptions().get(0);
                int firstLoadedTimeInMinutes = convertToMinutes(firstLoadedTime.getText());
                int counter = 0;
                if (timeInMinutes < firstLoadedTimeInMinutes) // Scroll down
                {
                    do
                    {
                        firstLoadedTime = getOptions().get(0);
                        AppHelper.scrollToMiddle(firstLoadedTime);
                        searchedTime = getOptions().stream()
                                .filter(o -> o.getText().equals(time))
                                .collect(Collectors.toList());
                        counter++;
                    }
                    while (searchedTime.size() == 0 && counter < MAX_LOOPS);
                }
                else // Scroll up
                {
                    do
                    {
                        List<WebElement> allLoadedTimes = getOptions();
                        WebElement lastLoadedTime = allLoadedTimes.get(allLoadedTimes.size() - 1);
                        AppHelper.scrollToMiddle(lastLoadedTime);
                        searchedTime = getOptions().stream()
                                .filter(o -> o.getText().equals(time))
                                .collect(Collectors.toList());
                        counter++;
                    }
                    while (searchedTime.size() == 0 && counter < MAX_LOOPS);
                }
            }

            // Select the requested time
            AppHelper.scrollToMiddle(searchedTime.get(0));
            searchedTime.get(0).click();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(OPTION_LIST_CSSSELECTOR)));
        }
        else
        {
            //TODO
            throw new NotImplementedException("Not yet implemented.");
        }

        return this;
    }

    private List<WebElement> getOptions()
    {
        List<WebElement> options;

        try
        {
            options = driver.findElements(By.cssSelector(OPTION_CSSSELECTOR));
            options.parallelStream().forEach(s -> wait.withTimeout(200,
                    TimeUnit.MILLISECONDS).until(ExpectedConditions.stalenessOf(s)));
        }
        catch (StaleElementReferenceException | TimeoutException e)
        {
            options = driver.findElements(By.cssSelector(OPTION_CSSSELECTOR));
        }

        return options;
    }

    private int convertToMinutes(String time)
    {
        String[] hoursMinutes = time.split(":");
        return Integer.parseInt(hoursMinutes[0]) * 60 + Integer.parseInt(hoursMinutes[1]);
    }

    public boolean isDropdownVisible()
    {
        return  driver.findElement(By.cssSelector(OPTION_LIST_CSSSELECTOR)).isDisplayed();
    }
}
