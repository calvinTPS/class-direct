package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import helper.ExtraExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;

public class CheckBox extends TypifiedElement {

    private final String CLICKABLE_XPATH = "./descendant::div[contains(@class,'md-container')]";
    private final String LABEL_XPATH = "./descendant::div[@class='md-label']/span";

    public CheckBox(WebElement wrappedElement) {
        super(wrappedElement);
    }

        private WebElement getCheckBox() {
        String tag = this.getWrappedElement().getTagName();

        String xpath;
        if (!tag.equals("md-checkbox"))
            xpath = "./ancestor::md-checkbox";
        else
            xpath = "self::md-checkbox";
        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    public void select() {
        if (!this.isSelected()) {
            WebElement checkbox = getCheckBox().findElement(By.xpath(CLICKABLE_XPATH));
            typifiedelement.WebElement cb = new typifiedelement.WebElement(checkbox);
            cb.click();
            BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());

        }
    }

    public void deselect() {
        if (this.isSelected()) {
            WebElement checkbox = getCheckBox().findElement(By.xpath(CLICKABLE_XPATH));
            typifiedelement.WebElement cb = new typifiedelement.WebElement(checkbox);
            cb.click();
            BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        }
    }

    @Override
    public boolean isSelected() {
        String b = getCheckBox().getAttribute("class");
        return b.contains("md-checked");
    }

    @Override
    public String getText() {
        return getCheckBox().findElement(By.xpath(LABEL_XPATH)).getText();
    }
}
