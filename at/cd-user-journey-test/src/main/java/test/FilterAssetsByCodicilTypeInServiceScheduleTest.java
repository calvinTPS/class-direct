package test;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterCodicilsPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.sub.actionableitems.ActionableItemsPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.ConditionsOfClassPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.ConditionOfClassDetailsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;

public class FilterAssetsByCodicilTypeInServiceScheduleTest extends BaseTest
{
    @Test(description = "User can filter their asset list by active codicil type in the Service Schedule view.")
    @Issue("LRCDT-2109")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void filterAssetsByCodicilTypeInServiceScheduleTest()
    {
        final String conditionOfClassTitle = "Missing ship's cat";
        final LocalDate dueDateFrom = LocalDate.of(2017, 5, 1);
        final LocalDate dueDateTo = LocalDate.of(2017, 6, 26);

        ConditionsOfClassPage conditionsOfClassPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .setSearchTextBox(LAURA.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickConditionsOfClassTab();

        // STEP 2 - View detailed info
        ConditionOfClassDetailsPage conditionOfClassDetailsPage = conditionsOfClassPage.getCodicilElements()
                .stream()
                .filter(c -> c.getTitle().equals(conditionOfClassTitle))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("No such coc with title %s is found", conditionOfClassTitle)))
                .clickArrowButton(ConditionOfClassDetailsPage.class);

        assert_().withFailureMessage("CoC does not have Title displayed.")
                .that(conditionOfClassDetailsPage.getTitle())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Status displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getStatus())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Category displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getCategory())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Imposed Date displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getImposedDate())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Due Date displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getDueDate())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Job Number displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getJobNumber())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Description displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getDescription())
                .isNotEmpty();

        assert_().withFailureMessage("No Associated Defects are displayed.")
                .that(conditionOfClassDetailsPage.getAssociatedClassDefects().size())
                .isGreaterThan(0);

        assert_().withFailureMessage("No Associated Repairs are displayed.")
                .that(conditionOfClassDetailsPage.getAssociatedRepairs().size())
                .isGreaterThan(0);

        // STEP 3 - Associated Defects
        conditionOfClassDetailsPage.getAssociatedClassDefects()
                .forEach(e ->
                {
                    assert_().withFailureMessage("Defect title should be displayed.")
                            .that(e.getDefectTitle())
                            .isNotEmpty();

                    assert_().withFailureMessage("Defect category should be displayed.")
                            .that(e.getDefectCategory())
                            .isNotEmpty();

                    assert_().withFailureMessage("Date Occurred should be displayed.")
                            .that(e.getDateOccurred())
                            .isNotEmpty();

                    assert_().withFailureMessage("Status should be displayed.")
                            .that(e.getStatus())
                            .isNotEmpty();
                });

        // STEP 4 - Associated Repairs
        conditionOfClassDetailsPage.getAssociatedRepairs()
                .forEach(e ->
                {
                    assert_().withFailureMessage("Repair Title should be displayed.")
                            .that(e.getRepairTitle())
                            .isNotEmpty();
                    assert_().withFailureMessage(String.format("Repair '%s' does not have Repair Type displayed.", e.getRepairTitle()))
                            .that(e.getRepairType())
                            .isNotEmpty();
                    assert_().withFailureMessage(String.format("Repair '%s' does not have 'Date Added' displayed.", e.getRepairTitle()))
                            .that(e.getDateAdded())
                            .isNotEmpty();
                });

        // STEP 5 - Navigate to Service Schedule page
        ServiceSchedulePage serviceSchedulePage = LandingPage.getTopBar()
                .clickClientTab()
                .getLandingPage()
                .clickServiceScheduleTab();

        // STEP 6 - Filter by codicil type (Actionable Item)
        ActionableItemsPage actionableItemsPage = serviceSchedulePage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deselectCodicilTypeAll()
                .selectCodicilTypeActionableItem()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset table not found."))
                .getAssetCard()
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickActionableItemsTab();

        assert_().withFailureMessage("There should be at least one Actionable item with status Open.")
                .that(actionableItemsPage.getCodicilElements().stream()
                        .filter(e -> e.getStatus().equals(defaultCodicilStatus)).count())
                .isGreaterThan(0L);

        // STEP 7-9 - Filter by codicil type (Condition of Class)
        conditionsOfClassPage = LandingPage.getTopBar()
                .clickClientTab()
                .getLandingPage()
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deselectCodicilTypeAll()
                .selectCodicilTypeConditionOfClass()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset table not found."))
                .getAssetCard()
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickConditionsOfClassTab();

        Long cocCount = conditionsOfClassPage.getCodicilElements().stream()
                .filter(e -> e.getStatus().equals(defaultCodicilStatus)).count();

        assert_().withFailureMessage("There should be at least one Condition of Class with status Open.")
                .that(cocCount.intValue())
                .isGreaterThan(0);

        // STEP 10 - Search options
        assert_().withFailureMessage("Search options are not complete.")
                .that(conditionsOfClassPage.getFilterBarPage().getSearchOptionList())
                .containsAllIn(Arrays.asList(codicilSearchOptions));

        // STEP 16: Verify searching by Name
        conditionsOfClassPage.getFilterBarPage()
                .closeSearchOptionList()
                .selectSearchDropDown(codicilSearchOptions[0])
                .setSearchTextBox(conditionOfClassTitle);

        assert_().withFailureMessage(String.format("No CoC has been found for '%s' search keyword.", conditionOfClassTitle))
                .that(conditionsOfClassPage.getCodicilElements().size())
                .isGreaterThan(0);

        // STEP 17: Verify clearing the search text box
        conditionsOfClassPage.getFilterBarPage()
                .clickClearButtonInSearchTextBox();

        assert_().withFailureMessage("Not all CoCs are displayed after clearing Search Text Box.")
                .that(conditionsOfClassPage.getCodicilElements().size())
                .isEqualTo(cocCount.intValue());

        // STEP 24-25: Filter by Due date
        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setDueDateFromDatepicker(dueDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDateToDatepicker(dueDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .forEach(e-> assert_().withFailureMessage(String.format("Due Date of %s should be in between %s and %s.", e.getTitle(), dueDateFrom, dueDateTo))
                        .that(LocalDate.parse(e.getDueDate(), FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(dueDateFrom, dueDateTo)));
    }
}
