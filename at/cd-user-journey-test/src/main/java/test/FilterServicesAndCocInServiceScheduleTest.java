package test;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import helper.TestDateHelper;
import main.common.element.CodicilElement;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterCodicilsPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.serviceschedule.details.TaskListPage;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import main.viewasset.codicils.codicilsanddefects.sub.actionableitems.ActionableItemsPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.ConditionOfClassDetailsPage;
import main.viewasset.schedule.ServiceSchedulePage;
import main.viewasset.schedule.graphical.element.DataElement;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import typifiedelement.Timeline;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.SERVICE_SCHEDULE;

public class FilterServicesAndCocInServiceScheduleTest extends BaseTest
{
    private LocalDate newEarliestDate, newLatestDate;

    @Test(description = "Verify that the user can switch to either graphical/tabular view and filter services/codicils for a single asset")
    @Issue("LRCDT-1325")
    @Features(SERVICE_SCHEDULE)
    public void filterServicesAndCocInServiceScheduleTest()
    {
        AssetHubPage assetHubPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton();

        ServiceSchedulePage serviceSchedulePage = assetHubPage.getSurveyPlannerPage().clickViewFullScheduleButton();

        //STEP 3 - Graphical View
        serviceSchedulePage.clickGraphicalViewTab()
                .getServiceTables()
                .forEach(e -> e.getDataElements().forEach(this::assertServiceAndCodicils));

        serviceSchedulePage.getGraphicalView()
                .getCodicilTables()
                .forEach(e -> e.getDataElements().forEach(this::assertServiceAndCodicils));

        //STEP 4 to 6 - Condition of Class
        List<String> codicilsWithCoc = serviceSchedulePage
                .clickFilterButton()
                .selectConditionOfClass()
                .deselectActionableItem()
                .deselectStatutoryFinding()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getGraphicalView()
                .getCodicilTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected codicil table not found."))
                .getDataElements()
                .stream()
                .map(DataElement::getServiceName)
                .collect(Collectors.toList());

        List<String> conditionOfClassList = serviceSchedulePage.clickBackButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickConditionsOfClassTab()
                .getCodicilElements()
                .stream()
                .map(CodicilElement::getTitle)
                .collect(Collectors.toList());

        assertCodicilList(codicilsWithCoc, conditionOfClassList);

        //STEP 7 - Actionable Items
        serviceSchedulePage = LandingPage.getTopBar()
                .clickClientTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deselectCodicilTypeAll()
                .selectCodicilTypeActionableItem()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        List<String> codicilsWithActionableItems = serviceSchedulePage
                .clickFilterButton()
                .selectActionableItem()
                .deselectConditionOfClass()
                .deselectStatutoryFinding()
                .clickSubmitButton(ServiceSchedulePage.class)
                .clickGraphicalViewTab()
                .getCodicilTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected codicil table not found."))
                .getDataElements()
                .stream()
                .map(DataElement::getServiceName)
                .collect(Collectors.toList());

        ActionableItemsPage actionableItemsPage = serviceSchedulePage.clickBackButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickActionableItemsTab();

        // Open Status
        List<String> actionableItemList = actionableItemsPage.getCodicilElements()
                .stream()
                .map(CodicilElement::getTitle)
                .collect(Collectors.toList());

        // Change Recommended
        actionableItemsPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .selectChangeRecommendedRadioButton()
                .clickSubmitButton(ActionableItemsPage.class)
                .getCodicilElements()
                .forEach(e -> actionableItemList.add(e.getTitle()));

        // Continues Satisfactory
        actionableItemsPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .selectContinuesSatisfactoryRadioButton()
                .clickSubmitButton(ActionableItemsPage.class)
                .getCodicilElements()
                .forEach(e -> actionableItemList.add(e.getTitle()));

        assertCodicilList(codicilsWithActionableItems, actionableItemList);

        //STEP 9 - Timescale
        serviceSchedulePage = LandingPage.getTopBar()
                .clickClientTab()
                .getLandingPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        Timeline timeline = serviceSchedulePage.getScheduleElement()
                .getServiceTimeline();

        newEarliestDate = timeline.getSelectedStartDate().plusMonths(9);
        newLatestDate = timeline.getSelectedEndDate().minusMonths(5);
        timeline.selectStartDate(newEarliestDate); // It might fail here due to this bug: LRCD-1708
        timeline.selectEndDate(newLatestDate);

        serviceSchedulePage.clickGraphicalViewTab()
                .getServiceTables()
                .forEach(serviceTable -> serviceTable.getDataElements()
                        .forEach(this::assertDueDatesInGraphicalView));

        serviceSchedulePage.getGraphicalView()
                .getCodicilTables()
                .forEach(codicilTable -> codicilTable.getDataElements()
                        .forEach(this::assertDueDatesInGraphicalView));

        //STEP 10 to 13 - Back button
        serviceSchedulePage.getGraphicalView()
                .getCodicilTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected codicil table not found."))
                .getDataElements()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Codicil list is empty"))
                .clickViewDetailsButton(ConditionOfClassDetailsPage.class)
                .getAssetHeaderPage()
                .clickBackButton(ServiceSchedulePage.class)
                .clickBackButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .clickGraphicalViewTab()
                .getServiceTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected service table not found."))
                .getDataElements()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Service list is empty"))
                .clickViewDetailsButton(TaskListPage.class)
                .getAssetHeaderPage()
                .clickBackButton(ServiceSchedulePage.class)
                .clickBackButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        //STEP 14 - Due status tooltip
        String dueStatusTooltip = serviceSchedulePage.getTabularView()
                .getServiceTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected service table not found."))
                .getServices()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Service list is empty"))
                .getDueStatusTooltip();

        assert_().withFailureMessage("Due status tooltip is expected to be displayed")
                .that(dueStatusTooltip)
                .isNotEmpty();
    }

    private void assertServiceAndCodicils(DataElement dataElement)
    {
        assert_().withFailureMessage("Service/Codicil name is expected to be displayed")
                .that(dataElement.getServiceName())
                .isNotEmpty();
        assert_().withFailureMessage("Due Date is expected to be displayed")
                .that(dataElement.getDueDate())
                .isNotEmpty();
        assert_().withFailureMessage("Due Status is expected to be displayed")
                .that(dataElement.getDueStatus())
                .isNotEmpty();
    }

    private void assertCodicilList(List<String> codicilNames, List<String> codicilElementList)
    {
        assert_().withFailureMessage("Count of Codicils should match the count in the list.")
                .that(codicilNames.size())
                .isEqualTo(codicilElementList.size());

        codicilElementList.forEach(e ->
                assert_().withFailureMessage(String.format("This codicil: %s is expected to be in the list.", e))
                        .that(codicilNames)
                        .contains(e)
        );
    }

    private void assertDueDatesInGraphicalView(DataElement dataElement)
    {
        LocalDate dueDate = LocalDate.parse(dataElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS);
        assert_().withFailureMessage("Due dates of codicils/services should be in between the earliest and latest date.")
                .that(TestDateHelper.toDate(dueDate))
                .isIn(Range.closed(TestDateHelper.toDate(newEarliestDate), TestDateHelper.toDate(newLatestDate)));
    }
}
