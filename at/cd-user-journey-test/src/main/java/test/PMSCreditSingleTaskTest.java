package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.PmsTaskListExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightHDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import helper.TestDateHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.mpms.crediting.MpmsCreditingPage;
import main.viewasset.mpms.crediting.element.TaskCard;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Assets;
import service.Export;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.WITH_CERTIFICATES;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;
import static constant.Epic.PMS;

public class PMSCreditSingleTaskTest extends BaseTest
{
    @Test(description = "User can credit a task. Proper error message is shown on submission.")
    @Issue("LRCDT-1685")
    @Features(PMS)
    public void pMSCreditSingleTaskTest()
    {
        final String expectedSuccessfulMessage = "The MPMS report submission was successful";
        MpmsCreditingPage mpmsCreditingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(WITH_CERTIFICATES.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(
                        () -> new NoSuchElementException(String.format("Expected asset card for %s not found.", WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getMpmsCard()
                .clickCreditingServiceButton();

        // STEP 4 - Export PMS
        mpmsCreditingPage.clickExportButton();
        PmsTaskListExportDto pmsTaskListExportDto = new PmsTaskListExportDto();
        pmsTaskListExportDto.setAssetCode(WITH_CERTIFICATES.getAssetCode());

        assert_().withFailureMessage("List of PMS tasks should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(new Export().getExportMpmsToken(pmsTaskListExportDto)))
                .isNotEmpty();

        // STEP 5 - Verify High level details
        List<WorkItemLightHDto> workItemLightHDtos =
                new Assets().getAssetTasks(WITH_CERTIFICATES.getId(), true)
                        .getServicesH()
                        .stream()
                        .flatMap(workItemPreviewScheduledServiceHDto -> workItemPreviewScheduledServiceHDto.getItemsH().stream())
                        .flatMap(workItemModelItemHDto -> workItemModelItemHDto.getTasksH().stream())
                        .collect(Collectors.toList());

        mpmsCreditingPage.getTaskCards()
                .forEach(taskCard ->
                {
                    assert_().withFailureMessage("Task Name is expected to be displayed ")
                            .that(taskCard.getTaskName())
                            .isNotEmpty();
                    assert_().withFailureMessage("Task Number is expected to be displayed ")
                            .that(taskCard.getTaskNumber())
                            .isNotEmpty();
                    assert_().withFailureMessage("Assigned Date is expected to be displayed ")
                            .that(taskCard.getAssignedDate())
                            .isNotEmpty();
                    assert_().withFailureMessage("Due/Postponed Date is expected to be displayed ")
                            .that(taskCard.getDuePostponedDate())
                            .isNotEmpty();

                    WorkItemLightDto taskBE = workItemLightHDtos.stream()
                            .filter(e -> e.getName().equals(taskCard.getTaskName()))
                            .findFirst()
                            .orElseThrow(() -> new java.util.NoSuchElementException("Expected task not found in the backend."));
                    LocalDate duePostponedDateBE = TestDateHelper
                            .toLocalDate(taskBE.getPostponementDate() != null ? taskBE.getPostponementDate() : taskBE.getDueDate());
                    assert_().withFailureMessage("If postponed date exists, it should be displayed or else due date should be displayed.")
                            .that(taskCard.getDuePostponedDate())
                            .isEqualTo(duePostponedDateBE.format(FRONTEND_TIME_FORMAT_DTS));
                });

        // STEP 6
        TaskCard taskCard = mpmsCreditingPage.getTaskCards().stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected task card not found"));

        taskCard.clickCreditingTaskButton();
        assert_().withFailureMessage("Date picker icon is expected to be displayed")
                .that(taskCard.isDateCreditedDatePickerDisplayed())
                .isTrue();

        // STEP 7-8 - Enter invalid date
        taskCard.setDateCredited("30 Feb 2019");
        assert_().withFailureMessage("Error message for invalid date is expected to be displayed.")
                .that(taskCard.isDateCreditedErrorTextDisplayed())
                .isTrue();

        // STEP 9 - Enter a valid date
        taskCard.setDateCredited(now.format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Error message for invalid date should NOT be displayed.")
                .that(taskCard.isDateCreditedErrorTextDisplayed())
                .isFalse();

        assert_().withFailureMessage("Submission successful message is expected to be displayed ")
                .that(mpmsCreditingPage.getFooterPage()
                        .clickSubmitReportButton(MpmsCreditingPage.class)
                        .getTaskReportSuccessfulSubmissionText())
                .isEqualTo(expectedSuccessfulMessage);
    }
}
