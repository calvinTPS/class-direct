package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import main.viewasset.mpms.crediting.MpmsCreditingPage;
import main.viewasset.mpms.crediting.element.TaskCard;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;
import static constant.Epic.PMS;

public class PMSCancelCreditMultipleTasksTest extends BaseTest
{
    @Test(description = "User can Cancel crediting a task. User is able to credit multiple tasks.")
    @Issue("LRCDT-1686")
    @Features(PMS)
    public void pMSCancelCreditMultipleTasksTest()
    {
        final String expectedSuccessfulMessage = "The MPMS report submission was successful";
        MpmsCreditingPage mpmsCreditingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s not found.", Asset.WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getMpmsCard()
                .clickCreditingServiceButton();

        // STEP 4 - Enter date credited
        TaskCard taskCard = mpmsCreditingPage.getTaskCards().stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected task card not found"))
                .clickCreditingTaskButton()
                .setDateCredited(now.format(FRONTEND_TIME_FORMAT_DTS));

        String taskCardName = taskCard.getTaskName();
        assert_().withFailureMessage("Date credited should be set successfully.")
                .that(taskCard.getDateCredited())
                .isEqualTo(now.format(FRONTEND_TIME_FORMAT_DTS));

        // STEP 5-6 - click cancel button
        List<TaskCard> taskCardList = mpmsCreditingPage.getFooterPage()
                .clickCancelButton(AssetHubPage.class)
                .getMpmsCard()
                .clickCreditingServiceButton()
                .getTaskCards();

        assert_().withFailureMessage("The previously selected task card should still exist.")
                .that(taskCardList.stream().map(TaskCard::getTaskName).collect(Collectors.toList()))
                .contains(taskCardName);

        // STEP 7 - Credit multiple tasks
        for (int i = 0; i < 2; i++)
        {
            // credit first 2 tasks
            taskCardList.get(i).clickCreditingTaskButton().setDateCredited(now.format(FRONTEND_TIME_FORMAT_DTS));
        }
        assert_().withFailureMessage("Submission successful message is expected to be displayed ")
                .that(mpmsCreditingPage.getFooterPage()
                        .clickSubmitReportButton(MpmsCreditingPage.class)
                        .getTaskReportSuccessfulSubmissionText())
                .isEqualTo(expectedSuccessfulMessage);
    }
}
