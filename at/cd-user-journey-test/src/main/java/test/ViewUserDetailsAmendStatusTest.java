package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.element.CompanyDetailsCard;
import main.administration.detail.element.EmulateUserCard;
import main.administration.detail.element.PersonalDetailsCard;
import main.administration.detail.sub.modalwindow.AmendAccountExpiryDatePage;
import main.administration.detail.sub.modalwindow.EditCodeModalPage;
import main.administration.element.DataElement;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Users;

import static com.google.common.truth.Truth.assert_;
import static constant.User.AccountStatus.Active;
import static constant.User.AccountStatus.Disabled;
import static constant.ClassDirect.Credentials.*;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.Flag.JAPAN;

public class ViewUserDetailsAmendStatusTest extends BaseTest
{
    @Test(description = "As an Admin user, the user can view an associated user's details and amend their status.")
    @Issue("LRCDT-1096")
    @Features(USER_MANAGEMENT)
    public void viewUserDetailsAmendStatusTest()
    {
        final String accountExpiryDate = "30 Dec 2025";
        final String newFlagCode = JAPAN.getCode();

        //STEP 1 to 3 - Visibility of details will be handled by @Visible annotation
        AdministrationPage administrationPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();
        AdministrationDetailPage administrationDetailPage = getUserByEmail(administrationPage, LR_SUPPORT.getEmail()).clickFurtherMoreDetailsButton();

        // STEP 4 - Personal Details
        assert_().withFailureMessage("User Name is expected to be displayed.")
                .that(administrationDetailPage.getUserName())
                .isNotEmpty();

        PersonalDetailsCard personalDetailsCard = administrationDetailPage.getPersonalDetailsCard();
        assert_().withFailureMessage("Account type is expected to be displayed.")
                .that(personalDetailsCard.getAccountType())
                .isNotEmpty();
        assert_().withFailureMessage("Email address is expected to be displayed.")
                .that(personalDetailsCard.getEmailAddress())
                .isEqualTo(LR_SUPPORT.getEmail());

        // STEP 5 -  Account Settings
        assert_().withFailureMessage("Account settings section is expected to be displayed.")
                .that(administrationDetailPage.getAccountSettingsCard().exists())
                .isTrue();

        // STEP 6 - Company Details
        CompanyDetailsCard companyDetailsCard = administrationDetailPage.getCompanyDetailsCard();
        assert_().withFailureMessage("Company name is expected to be displayed.")
                .that(companyDetailsCard.getCompanyName())
                .isNotEmpty();
        assert_().withFailureMessage("Address line 1 is expected to be displayed.")
                .that(companyDetailsCard.getAddressLine1())
                .isNotEmpty();
        assert_().withFailureMessage("City is expected to be displayed.")
                .that(companyDetailsCard.getCity())
                .isNotEmpty();
        assert_().withFailureMessage("State/Province/Region is expected to be displayed.")
                .that(companyDetailsCard.getStateProvinceRegion())
                .isNotEmpty();
        assert_().withFailureMessage("Zip/Postal Code is expected to be displayed.")
                .that(companyDetailsCard.getPostalCode())
                .isNotEmpty();
        assert_().withFailureMessage("Country is expected to be displayed.")
                .that(companyDetailsCard.getCountry())
                .isNotEmpty();

        // STEP 7 - See Class Direct as this User
        EmulateUserCard emulateUserCard = administrationDetailPage.getEmulateUserCard();
        assert_().withFailureMessage("'Emulate Class Direct as this user' button is expected to be displayed.")
                .that(companyDetailsCard.getPostalCode())
                .isNotEmpty();
        assert_().withFailureMessage("Country is expected to be displayed.")
                .that(companyDetailsCard.getCountry())
                .isNotEmpty();

        // STEP 8 - CD Exhibition of Records (only visible for Client or Shipbuilder users)
        administrationDetailPage.clickBackButton();
        administrationDetailPage = getUserByEmail(administrationPage, SHIP_BUILDER_USER.getEmail()).clickFurtherMoreDetailsButton();

        assert_().withFailureMessage("Country is expected to be displayed.")
                .that(administrationDetailPage.getExhibitionOfRecordsCard().isAddEorAccessButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Ship builder code is expected to be displayed.")
                .that(personalDetailsCard.getShipBuilderCode())
                .isNotEmpty();

        // STEP 9 - Edit Account Status
        if (!administrationDetailPage.getAccountStatus().equals(Active.getAccountStatus()))
        {
            administrationDetailPage.setAccountStatus(Active.getAccountStatus())
                    .clickConfirmButton();
        }
        assert_().withFailureMessage("Account status is expected to be Active")
                .that(administrationDetailPage.getAccountStatus())
                .isEqualTo(Active.getAccountStatus());

        // STEP 10 - Cancel disabling the account status
        assert_().withFailureMessage("Account status is expected to be still Active")
                .that(administrationDetailPage.setAccountStatus(Disabled.getAccountStatus())
                        .clickCancelButton()
                        .getAccountStatus())
                .isEqualTo(Active.getAccountStatus());

        // STEP 11 - Confirm disabling the account status
        assert_().withFailureMessage("Account status is expected to be Disabled")
                .that(administrationDetailPage.setAccountStatus(Disabled.getAccountStatus())
                        .clickConfirmButton()
                        .getAccountStatus())
                .isEqualTo(Disabled.getAccountStatus());

        // STEP 12
        administrationPage = administrationDetailPage.clickBackButton();
        assert_().withFailureMessage("Count should match the number of users displayed.")
                .that(administrationPage.getShowingXofYPage().getShown())
                .isEqualTo(administrationPage.getAdministrationTable().getDataElements().size());

        // STEP 13 - Verify Account status is Disabled
        assert_().withFailureMessage("Account status is expected to be Disabled")
                .that(getUserByEmail(administrationPage, SHIP_BUILDER_USER.getEmail()).getAccountStatus())
                .isEqualTo(Disabled.getAccountStatus());

        // STEP 14 - Edit Account expiry
        AmendAccountExpiryDatePage amendAccountExpiryDatePage = getUserByEmail(administrationPage, LR_INTERNAL_USER.getEmail())
                .clickFurtherMoreDetailsButton()
                .getAccountSettingsCard()
                .clickAmendAccountExpiryButton();
        // Visibility of Confirm and Cancel button is handled by @Visible annotation
        assert_().withFailureMessage("Amend Account Expiry date title is expected to be Disabled")
                .that(amendAccountExpiryDatePage.getAmendAccountExpiryDateTitle())
                .isNotEmpty();

        // STEP 15-16 - Cancel and Confirm Account expiry
        amendAccountExpiryDatePage.clickCancelButton()
                .getAccountSettingsCard()
                .clickAmendAccountExpiryButton()
                .setAccountExpiryDate(accountExpiryDate)
                .clickConfirmButton();

        assert_().withFailureMessage("New Account expiry date should be updated correctly.")
                .that(administrationDetailPage.getAccountSettingsCard().getAssetExpiryDate())
                .isEqualTo(accountExpiryDate);

        // STEP 17
        personalDetailsCard = administrationDetailPage.getPersonalDetailsCard();
        assert_().withFailureMessage("Email address is expected to be displayed.")
                .that(personalDetailsCard.getEmailAddress())
                .isEqualTo(LR_INTERNAL_USER.getEmail());
        String oldFlagCode = personalDetailsCard.getFlagCode();

        // STEP 18 - Visibility of elements in the modal is handled by @Visible annotation
        EditCodeModalPage editCodeModalPage = personalDetailsCard.clickFlagCodeButton();
        assert_().withFailureMessage("Modal Title is expected to be displayed.")
                .that(editCodeModalPage.getModalTitle())
                .isNotEmpty();

        // STEP 19 - Cancel and verify
        assert_().withFailureMessage("There should be no changes saved.")
                .that(editCodeModalPage.clickCancelButton().getPersonalDetailsCard().getFlagCode())
                .isEqualTo(oldFlagCode);

        // STEP 20 - Save and verify
        administrationDetailPage = personalDetailsCard.clickFlagCodeButton()
                .setCodeTextBox(newFlagCode)
                .clickConfirmButton();
        assert_().withFailureMessage("New flag code should be saved.")
                .that(administrationDetailPage.getPersonalDetailsCard().getFlagCode())
                .isEqualTo(newFlagCode);

        // STEP 21 - Accessible and Restricted assets relevant to the new code should be assigned.
        // This is expected to fail due to LRCD-2417
        Users users = new Users();
        users.getAccessibleAssets(LR_INTERNAL_USER.getId())
                .forEach(e -> assert_().withFailureMessage(String.format("Flag code of this accessible asset '%s' should be %s.", e.getName(), newFlagCode))
                        .that(e.getFlagStateDto().getFlagCode())
                        .isEqualTo(newFlagCode));

        users.getRestrictedAssets(LR_INTERNAL_USER.getId())
                .forEach(e -> assert_().withFailureMessage(String.format("Flag code of this accessible asset '%s' should be %s.", e.getName(), newFlagCode))
                        .that(e.getFlagStateDto().getFlagCode())
                        .isEqualTo(newFlagCode));
    }

    private DataElement getUserByEmail(AdministrationPage administrationPage, String email)
    {
        return administrationPage.getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(email))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User with email %s is not found", email)));
    }
}
