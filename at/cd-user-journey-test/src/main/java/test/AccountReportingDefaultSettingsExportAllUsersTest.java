package test;

import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.administration.export.ExportAccountReportPage;
import main.administration.export.element.InformationTab;
import main.administration.export.element.UsersTab;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ACCOUNT_REPORTING;

public class AccountReportingDefaultSettingsExportAllUsersTest extends BaseTest
{
    @Test(description = "As an LR Admin user, I'm able to export the information of one or more users listed on my User List page.")
    @Issue("LRCDT-1753")
    @Features(ACCOUNT_REPORTING)
    public void accountReportingDefaultSettingsExportAllUsersTest()
    {
        ExportAccountReportPage exportAccountReportPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickExportAccountReportButton()
                .clickCancelButton()
                .clickExportAccountReportButton();

        // STEP 6 - By default, all users are selected
        UsersTab usersTab = exportAccountReportPage.getUsersTab();
        usersTab.getUserCards()
                .forEach(e ->
                {
                    assert_().withFailureMessage(String.format("This user: %s is expected to be selected, by default.", e.getName()))
                            .that(e.isUserCardSelected())
                            .isTrue();

                    //STEP 7 - Verify each User card's content
                    assert_().withFailureMessage("Full name should be displayed.")
                            .that(e.getFullName())
                            .isNotEmpty();
                    assert_().withFailureMessage("Email address should be displayed.")
                            .that(e.getUserEmail())
                            .isNotEmpty();
                    assert_().withFailureMessage("User id should be displayed.")
                            .that(e.getUserId())
                            .isNotEmpty();
                    assert_().withFailureMessage("Account type should be displayed.")
                            .that(e.getAccountType())
                            .isNotEmpty();
                });

        assert_().withFailureMessage("By default, Remove All button should be enabled.")
                .that(usersTab.isRemoveAllButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("By default, Select All button should be disabled.")
                .that(usersTab.isSelectAllButtonEnabled())
                .isFalse();

        assert_().withFailureMessage("By default, Export button should be enabled.")
                .that(exportAccountReportPage.isExportButtonEnabled())
                .isTrue();

        // STEP 8 - Click Remove All button
        usersTab.clickRemoveAllButton()
                .getUserCards()
                .forEach(e -> assert_().withFailureMessage(String.format("This user: %s is expected to be deselected.", e.getName()))
                        .that(e.isUserCardSelected())
                        .isFalse());

        assert_().withFailureMessage("Remove All button should be disabled.")
                .that(usersTab.isRemoveAllButtonEnabled())
                .isFalse();

        assert_().withFailureMessage("Select All button should be enabled.")
                .that(usersTab.isSelectAllButtonEnabled())
                .isTrue();

        // STEP 9 - Click Select All button
        usersTab.clickSelectAllButton()
                .getUserCards()
                .forEach(e -> assert_().withFailureMessage(String.format("This user: %s is expected to be selected.", e.getName()))
                        .that(e.isUserCardSelected())
                        .isTrue());

        // Get all available account types
        List<String> accountTypes = usersTab
                .getUserCards()
                .stream()
                .map(e -> e.getAccountType().toLowerCase())
                .distinct()
                .collect(Collectors.toList());

        // STEP 10 - Information Tab
        InformationTab informationTab = exportAccountReportPage.clickInformationTab();

        List<String> accountTypesInformationTab = informationTab.getAccountTypes()
                .stream()
                .map(e -> e.getText().toLowerCase())
                .collect(Collectors.toList());

        assert_().withFailureMessage("All available account types should be displayed.")
                .that(accountTypesInformationTab)
                .containsAllIn(accountTypes);

        assert_().withFailureMessage("Last login Date from should be displayed.")
                .that(informationTab.getLastLoginDateFrom())
                .isNotEmpty();

        assert_().withFailureMessage("Last login Date To should be displayed.")
                .that(informationTab.getLastLoginDateTo())
                .isNotEmpty();

        // STEP 11- Export
        String fileName = String
                .format("user_listing_export_%s.csv", LocalDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("ddMMyyyyHHmm")));
        exportAccountReportPage.clickExportButton()
                .clickUsersTab();

        String exportUserToken = new Export().getExportUserToken(new UserProfileDto());
        assert_().withFailureMessage("User listing should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(exportUserToken))
                .isNotEmpty();

        // STEP 12 -Verify content - will not verify content
    }
}
