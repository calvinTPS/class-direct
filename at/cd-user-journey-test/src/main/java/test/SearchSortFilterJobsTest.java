package test;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterJobsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.surveys.servicehistory.ServiceHistoryPage;
import main.viewasset.surveys.servicehistory.element.JobElement;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.JobSearchOptions.JOB_NUMBER;
import static constant.ClassDirect.JobSearchOptions.SURVEYS;
import static constant.ClassDirect.JobSortByOptions.LAST_VISIT_DATE;
import static constant.Epic.SERVICE_HISTORY;
import static helper.TestDateHelper.toDate;

public class SearchSortFilterJobsTest extends BaseTest {
    private LocalDate fromLastViewDate = LocalDate.of(2016, 1, 1);
    private LocalDate toLastViewDate = LocalDate.of(2016, 12, 31);

    @Test(description = "Automation_UJ_DO_SearchSortFilterJobs")
    @Issue("LRCDT-1249")
    @Features(SERVICE_HISTORY)

    public void searchSortFilterJobsTest() {

        // Step 1-2
        ServiceHistoryPage serviceHistoryPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveysCard()
                .clickViewAllButton();

        // Step 3-4 & Step 7
        List<String> defaultJobs = getJobIds(serviceHistoryPage);
        serviceHistoryPage.getFilterBarPage()
                .clickFilterButton(FilterJobsPage.class)
                .setLastVisitDateFromDatepicker(fromLastViewDate.format(FRONTEND_TIME_FORMAT_DTS))
                .setLastVisitDateToDatepicker(toLastViewDate.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ServiceHistoryPage.class);

        // Step 5
        serviceHistoryPage.getJobElements()
                .forEach(e -> assert_().withFailureMessage(String.format("All Date on the jobs are expected to be in the range of %s and %s", fromLastViewDate, toLastViewDate))
                        .that(e.getLastVisitDate())
                        .isIn(Range.closed(toDate(fromLastViewDate), toDate(toLastViewDate))));

        //Step 6
        FilterJobsPage filterJobsPage = serviceHistoryPage.getFilterBarPage()
                .clickFilterButton(FilterJobsPage.class)
                .clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterJobsPage.class);

        assert_().withFailureMessage("From Last View Date Is expected to be Cleared")
                .that(filterJobsPage.getFromLastVisitedDateText())
                .isEmpty();

        assert_().withFailureMessage("To Last View Date Is expected to be Cleared ")
                .that(filterJobsPage.getToLastVisitedDateText())
                .isEmpty();

        filterJobsPage.clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterJobsPage.class)
                .clickSubmitButton(ServiceHistoryPage.class);

        // Step 8
        assert_().withFailureMessage("Default Sort By option is expected to be " + LAST_VISIT_DATE.toString())
                .that(serviceHistoryPage
                        .getFilterBarPage()
                        .getSelectedSortByOption()
                        .equals(LAST_VISIT_DATE.toString()))
                .isTrue();

        List<String> actualSortByOptions = serviceHistoryPage
                .getFilterBarPage()
                .getSortByOptionList();

        serviceHistoryPage.getFilterBarPage().closeSearchOptionList();

        assert_().withFailureMessage("Sort by options are expected to be displayed")
                .that(actualSortByOptions)
                .isEqualTo(Arrays.stream(ClassDirect.JobSortByOptions.values()).map(ClassDirect.JobSortByOptions::toString).collect(Collectors.toList()));

        // Step 9
        serviceHistoryPage.getFilterBarPage()
                .selectSortByDropDown(JOB_NUMBER.toString());

        List<String> actualJobId = getJobIds(serviceHistoryPage);

        //Get Job ID in separate List, Sorting will alter the actual order of list
        List<String> sortedJobId = getJobIds(serviceHistoryPage);
        sortedJobId.sort(Collections.reverseOrder());
        assert_().withFailureMessage("Jobs are expected to be sorted in descending order ")
                .that(actualJobId.equals(sortedJobId))
                .isTrue();

        // Step 10
        assert_().withFailureMessage("Default Search option is expected to be " + SURVEYS.toString())
                .that(serviceHistoryPage
                        .getFilterBarPage()
                        .getSelectedSearchOption())
                .isEqualTo(SURVEYS.toString());

        List<String> actualSearchOptions = serviceHistoryPage
                .getFilterBarPage()
                .getSearchOptionList();

        //Step 11-14
        serviceHistoryPage
                .getFilterBarPage().closeSearchOptionList()
                .selectSearchDropDown(JOB_NUMBER.toString());

        assert_().withFailureMessage("Search options are expected to be displayed")
                .that(actualSearchOptions)
                .isEqualTo(Arrays.stream(ClassDirect.JobSearchOptions.values()).map(ClassDirect.JobSearchOptions::toString).collect(Collectors.toList()));

        serviceHistoryPage.getFilterBarPage()
                .setSearchTextBox(defaultJobs.get(0))
                .selectSearchDropDown(JOB_NUMBER.toString())
                .getPageReference(ServiceHistoryPage.class)
                .getJobElements()
                .forEach(e ->
                        assert_().withFailureMessage("Searched Job ID is expected to be displayed")
                                .that(e.getJobId().equals(defaultJobs.get(0)))
                                .isTrue());

        serviceHistoryPage.getFilterBarPage().clickClearButtonInSearchTextBox();
        assert_().withFailureMessage("Search text box is expected to be cleared")
                .that(serviceHistoryPage
                        .getFilterBarPage()
                        .getSearchText()
                        .isEmpty())
                .isTrue();

        serviceHistoryPage.getFilterBarPage()
                .selectSortByDropDown(LAST_VISIT_DATE.toString());
        List<String> jobsAfterReset = getJobIds(serviceHistoryPage);

        assert_().withFailureMessage("All Jobs are expected to be displayed is default view after reset")
                .that(defaultJobs.equals(jobsAfterReset)).isTrue();
    }

    private List<String> getJobIds(ServiceHistoryPage serviceHistoryPage) {
        return serviceHistoryPage
                .getJobElements()
                .stream()
                .map(JobElement::getJobId)
                .collect(Collectors.toList());
    }
}
