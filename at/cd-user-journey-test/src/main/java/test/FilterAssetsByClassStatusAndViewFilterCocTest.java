package test;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterCodicilsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.ConditionsOfClassPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.ConditionOfClassDetailsPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.RepairDetailsOverlayPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FilterAssetsByClassStatusAndViewFilterCocTest extends BaseTest
{
    @Test(description = "Automation_UJ_Filter assets by Class status_View and filter CoCs for Asset")
    @Issue("LRCDT-1870")
    @Features(FLEET_DASHBOARD)
    public void filterAssetsByClassStatusAndViewFilterCocTest()
    {
        final String conditionOfClassTitle = "Missing ship's cat";
        final LocalDate filterByDueDateFrom = LocalDate.of(2016, 5, 1);
        final LocalDate filterByDueDateTo = LocalDate.of(2016, 5, 7);

        FilterBarPage filterBarPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class);

        FilterAssetsPage filterAssetsPage = filterBarPage.clickFilterButton(FilterAssetsPage.class);

        // STEP 4 - Verify List of Class Status
        assert_().withFailureMessage("All Class Status are expected to be displayed")
                .that(Arrays.stream(ClassStatus.values()).map(ClassStatus::toString).collect(Collectors.toList()))
                .containsAllIn(filterAssetsPage.getClassStatusList());

        // STEP 5 - By default, All is selected
        filterAssetsPage.getClassStatusCheckBox()
                .forEach(
                        e -> assert_().withFailureMessage(String.format("This class status: %s is expected to be selected, by default.", e.getName()))
                                .that(e.isSelected())
                                .isTrue());

        // STEP 6 - Select a specific class status
        VesselListPage vesselListPage = filterAssetsPage.deSelectClassStatusCheckBoxByStatusName(ClassStatus.ALL.toString())
                .selectClassStatusCheckBoxByStatusName(ClassStatus.CLASSED_PENDING_CLASS_EXEC_APPROVAL.toString())
                .clickSubmitButton(VesselListPage.class);

        vesselListPage.getAssetCards()
                .forEach(e -> assert_()
                        .withFailureMessage("Assets with the selected filter " + ClassStatus.CLASSED_PENDING_CLASS_EXEC_APPROVAL.toString() +
                                " should be displayed")
                        .that(e.getClassStatus())
                        .isEqualTo(ClassStatus.CLASSED_PENDING_CLASS_EXEC_APPROVAL.toString()));

        // STEP 7 - Showing X of Y
        assert_().withFailureMessage("Pagination total count should be displayed")
                .that(vesselListPage.getShowingXofYPage().getTotal())
                .isAtLeast(1);
        assert_().withFailureMessage("Pagination shown count should be displayed")
                .that(vesselListPage.getShowingXofYPage().getShown())
                .isAtLeast(1);

        //STEP 8-10 - Select an asset
        ConditionsOfClassPage conditionsOfClassPage = filterBarPage.setSearchTextBox(LAURA.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickConditionsOfClassTab();

        // STEP 11 - Verify high level details
        conditionsOfClassPage.getCodicilElements()
                .forEach(e ->
                {
                    assert_().withFailureMessage("Title should be displayed")
                            .that(e.getTitle())
                            .isNotEmpty();

                    assert_().withFailureMessage("Category should be displayed")
                            .that(e.getCategory())
                            .isNotEmpty();

                    assert_().withFailureMessage("Imposed Date should be displayed")
                            .that(e.getImposedDate())
                            .isNotEmpty();

                    assert_().withFailureMessage("Due Date should be displayed")
                            .that(e.getDueDate())
                            .isNotEmpty();

                    assert_().withFailureMessage("Status should be displayed")
                            .that(e.getStatus())
                            .isNotEmpty();
                });

        // STEP 12 - View detailed info
        ConditionOfClassDetailsPage conditionOfClassDetailsPage = conditionsOfClassPage.getCodicilElements()
                .stream()
                .filter(c -> c.getTitle().equals(conditionOfClassTitle))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("No such coc with title %s is found", conditionOfClassTitle)))
                .clickArrowButton(ConditionOfClassDetailsPage.class);

        assert_().withFailureMessage("CoC does not have Title displayed.")
                .that(conditionOfClassDetailsPage.getTitle())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Status displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getStatus())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Category displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getCategory())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Imposed Date displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getImposedDate())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Due Date displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getDueDate())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Job Number displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getJobNumber())
                .isNotEmpty();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Description displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getDescription())
                .isNotEmpty();

        assert_().withFailureMessage("No Associated Defects are displayed.")
                .that(conditionOfClassDetailsPage.getAssociatedClassDefects().size())
                .isGreaterThan(0);

        assert_().withFailureMessage("No Associated Repairs are displayed.")
                .that(conditionOfClassDetailsPage.getAssociatedRepairs().size())
                .isGreaterThan(0);

        // STEP 13 - Associated Defects
        conditionOfClassDetailsPage.getAssociatedClassDefects()
                .forEach(e ->
                {
                    assert_().withFailureMessage("Defect title should be displayed.")
                            .that(e.getDefectTitle())
                            .isNotEmpty();

                    assert_().withFailureMessage("Defect category should be displayed.")
                            .that(e.getDefectCategory())
                            .isNotEmpty();

                    assert_().withFailureMessage("Date Occurred should be displayed.")
                            .that(e.getDateOccurred())
                            .isNotEmpty();

                    assert_().withFailureMessage("Status should be displayed.")
                            .that(e.getStatus())
                            .isNotEmpty();
                });

        // STEP 14-15 - Select an associated defect
        conditionOfClassDetailsPage.getAssociatedClassDefects()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected associated defect is not found."))
                .clickArrowButton()
                .getAssetHeaderPage()
                .clickBackButton(ConditionOfClassDetailsPage.class);

        // STEP 16 - Associated Repairs
        conditionOfClassDetailsPage.getAssociatedRepairs()
                .forEach(e ->
                {
                    assert_().withFailureMessage("Repair Title should be displayed.")
                            .that(e.getRepairTitle())
                            .isNotEmpty();
                    assert_().withFailureMessage(String.format("Repair '%s' does not have Repair Type displayed.", e.getRepairTitle()))
                            .that(e.getRepairType())
                            .isNotEmpty();
                    assert_().withFailureMessage(String.format("Repair '%s' does not have 'Date Added' displayed.", e.getRepairTitle()))
                            .that(e.getDateAdded())
                            .isNotEmpty();
                });

        // STEP 17-18 - Overlay for Associated Repairs
        RepairDetailsOverlayPage repairDetailsOverlayPage = conditionOfClassDetailsPage.getAssociatedRepairs()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Associated Repair not found"))
                .clickArrowButton();
        assert_().withFailureMessage("Repair Title should be displayed in the overlay page.")
                .that(repairDetailsOverlayPage.getRepairTitle())
                .isNotEmpty();
        assert_().withFailureMessage(
                String.format("Repair '%s' does not have Repair Type displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getRepairType())
                .isNotEmpty();
        assert_().withFailureMessage(
                String.format("Repair '%s' does not have Repair Action displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getRepairAction())
                .isNotEmpty();
        assert_().withFailureMessage(
                String.format("Repair '%s' does not have Items displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getItems())
                .isNotEmpty();
        assert_().withFailureMessage(
                String.format("Repair '%s' does not have Description displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getDescription())
                .isNotEmpty();
        assert_().withFailureMessage(
                String.format("Repair '%s' does not have Date Added displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getDateAdded())
                .isNotEmpty();
        repairDetailsOverlayPage.clickCloseButton();

        // STEP 19-20 - Filter by Due Date
        conditionOfClassDetailsPage.getAssetHeaderPage().clickBackButton(ConditionsOfClassPage.class)
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setDueDateFromDatepicker(filterByDueDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDateToDatepicker(filterByDueDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .forEach(e -> assert_().withFailureMessage(
                        String.format("Due date of %s should be in between %s and %s", e.getTitle(), filterByDueDateFrom, filterByDueDateTo))
                        .that(LocalDate.parse(e.getDueDate(), FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(filterByDueDateFrom, filterByDueDateTo)));
    }
}
