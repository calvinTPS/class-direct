package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Assets;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Range.closed;
import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.ClassStatus;
import static constant.ClassDirect.ALL_SEARCH_TERM;
import static constant.Epic.FLEET_DASHBOARD;

public class FilterByClassStatusAndGrossTonnageTest extends BaseTest
{
    private List<String> classStatus = Arrays.stream(ClassStatus.values()).map(ClassStatus::toString).collect(Collectors.toList());

    private final static int minGrossTonnage = 10,
            maxGrossTonnage = 100;

    @Test(description = "Automation_UJ_Fleet dashboard_Filter assets_by_Class status and Gross Tonnage")
    @Issue("LRCDT-601")
    @Features(FLEET_DASHBOARD)
    public void filterByClassStatusAndGrossTonnageTest()
    {
        AssetQueryHDto assetQueryHDto = new AssetQueryHDto();
        assetQueryHDto.setSearch(ALL_SEARCH_TERM);
        List<AssetHDto> assetHDtoList = new Assets().getAssets(assetQueryHDto);

        //Open App and Go to Filter Page
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class);

        FilterAssetsPage filterAssetsPage = vesselListPage
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class);

        // STEP 4 - Class Status filter options
        assert_().withFailureMessage("All Class status should be displayed as filter option.")
                .that(filterAssetsPage.getClassStatusList())
                .containsAllIn(classStatus);

        // STEP 5 - All Class Status should be selected by default
        filterAssetsPage.getClassStatusCheckBox()
                .forEach(e -> assert_().withFailureMessage("By default, All class status should be selected.")
                        .that(e.isSelected())
                        .isTrue());
        filterAssetsPage.clickSubmitButton(VesselListPage.class);
        // STEPS 6-12 - Filter by each class status
        for (int i = 1; i < classStatus.size(); i++)
        {
            // This will fail in "Not LR Classed" due to this bug: LRCD-754
            setAndVerifyClassStatus(assetHDtoList, vesselListPage, i - 1, i);
        }

        // STEP 13 - Select All
        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectClassStatusCheckBoxByStatusName(ClassStatus.ALL.toString());

        // STEP 14 - Gross Tonnage Filter
        assert_().withFailureMessage("The Gross Tonnage field is expected to be present in the Filter page.")
                .that(filterAssetsPage.isGrossTonnageFieldDisplayed())
                .isTrue();

        // STEP 15 - Enter Max and Min gross tonnage
        filterAssetsPage.clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .setMinGrossTonnage(String.valueOf(minGrossTonnage))
                .setMaxGrossTonnage(String.valueOf(maxGrossTonnage))
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .forEach(e -> assert_().withFailureMessage(
                        String.format("Gross tonnage of displayed assets should range from %s to %s.", minGrossTonnage, maxGrossTonnage))
                        .that(Integer.parseInt(e.getGrossTonnage()))
                        .isIn(closed(minGrossTonnage, maxGrossTonnage)));

        // STEP 16 - Max gross tonnage is Empty
        vesselListPage.getFilterBarPage().clickFilterButton(FilterAssetsPage.class)
                .setMaxGrossTonnage("")
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .forEach(e -> assert_()
                        .withFailureMessage(String.format("Gross tonnage of displayed assets should not be less than %s.", minGrossTonnage))
                        .that(Integer.parseInt(e.getGrossTonnage()))
                        .isAtLeast(minGrossTonnage));

        // STEP 17 - Min gross tonnage is Empty
        vesselListPage.getFilterBarPage().clickFilterButton(FilterAssetsPage.class)
                .setMinGrossTonnage("")
                .setMaxGrossTonnage(String.valueOf(maxGrossTonnage))
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .forEach(e -> assert_()
                        .withFailureMessage(String.format("Gross tonnage of displayed assets should not be greater than %s.", maxGrossTonnage))
                        .that(Integer.parseInt(e.getGrossTonnage()))
                        .isAtMost(maxGrossTonnage));

        // STEP 18 - Showing X of Y
        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .setMaxGrossTonnage("")
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(ALL_SEARCH_TERM);

        assert_().withFailureMessage("Pagination count 'X' should be equal to count of assets displayed.")
                .that(vesselListPage.getShowingXofYPage().getShown())
                .isEqualTo(vesselListPage.getAssetCards().size());

        // This is expected to fail due to bug LRCD-754
        assert_().withFailureMessage("Pagination count 'Y' should be equal to total number of assets.")
                .that(vesselListPage.getShowingXofYPage().getTotal())
                .isEqualTo(assetHDtoList.size());
    }

    private void setAndVerifyClassStatus(List<AssetHDto> assetHDtoList, VesselListPage vesselListPage, int j, int k)
    {
        List<AssetCard> assetCards = vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .deSelectClassStatusCheckBoxByStatusName(classStatus.get(j))
                .selectClassStatusCheckBoxByStatusName(classStatus.get(k))
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards();

        long assetCountBackEnd;
        if (classStatus.get(k).equals(ClassStatus.NOT_LR_CLASSED.toString()))
        {
            // Non-LR assets don't have Class Status
            assetCards.forEach(e ->
                    assert_().withFailureMessage(String.format("%s assets are expected to be displayed.", classStatus.get(k)))
                            .that(e.getClassStatus())
                            .isEqualTo(ClassStatus.NOT_LR_CLASSED.toString()));

            assetCountBackEnd = assetHDtoList.stream()
                    .filter(e -> e.getClassStatusDto() == null)
                    .count();
        }
        else
        {
            assetCards.forEach(e ->
                    assert_().withFailureMessage(String.format("Assets with the selected filter %s should be displayed", classStatus.get(k)))
                            .that(e.getClassStatus())
                            .isEqualTo(classStatus.get(k)));

            assetCountBackEnd = assetHDtoList.stream()
                    .filter(e -> e.getClassStatusDto() != null && e.getClassStatusDto().getName().equals(classStatus.get(k)))
                    .count();
        }

        if (assetCountBackEnd > 0)
        {
            assert_().withFailureMessage("Pagination count 'X' should match the number of asset cards displayed.")
                    .that(assetCards.size())
                    .isEqualTo(vesselListPage.getShowingXofYPage().getShown());
            assert_().withFailureMessage(
                    String.format("Pagination count 'Y' should match the total number of assets with Class status %s.", classStatus.get(k)))
                    .that(assetCountBackEnd)
                    .isEqualTo(vesselListPage.getShowingXofYPage().getTotal());
        }
    }
}
