package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.administration.adduser.eor.AddEorPermissionPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.EOR_EMAIL;
import static constant.User.UserAccountTypes;

public class CreateEorUserTest extends BaseTest
{
    @Test(description = "Login as an LR Administrator and create an EoR user")
    @Issue("LRCDT-2215")
    @Features(USER_MANAGEMENT)
    public void createEorUserTest()
    {
        AdministrationPage administrationPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickAddUserButton()
                .setEmailAddress(EOR_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User card for %s was not found.", EOR_EMAIL)))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.EOR.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Account type %s was not found.", UserAccountTypes.EOR.toString())))
                .clickSelectButton(AddEorPermissionPage.class)
                .setAssetExpiryDate(now.format(FRONTEND_TIME_FORMAT_DTS))
                .setImoNumber(Asset.LAURA.getImoNumber())
                .clickAddExhibitionRecordsAssetButton()
                .getFooterPage()
                .clickConfirmButton(AdministrationPage.class);

        assert_().withFailureMessage("Newly created user should be displayed in the list.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(EOR_EMAIL))
                        .count())
                .isGreaterThan(0L);
    }
}
