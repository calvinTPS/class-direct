package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetNoteHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.common.element.CodicilElement;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterCodicilsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.assetnotes.AssetNoteDetailsPage;
import main.viewasset.codicils.codicilsanddefects.sub.assetnotes.AssetNotesPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Assets;
import service.Export;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;

public class AssetNotesSearchFilterSortReportTest extends BaseTest
{
    private final String assetNoteTitle = "Cancelled";
    private final String assetNoteCategory = "Class";
    private final String assetNoteStatus = "Recommended for change";
    private final Export export = new Export();

    @Test(description = "User can view Asset Notes for an asset and perform the following operations - sort, search, filter and download a Final Survey Report")
    @Issue("LRCDT-1021")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void assetNotesSearchFilterSortReportTest()
    {
        CodicilsAndDefectsPage codicilsAndDefectsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s is not found.", Asset.LAURA.getName())))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton();
        AssetNotesPage assetNotesPage = codicilsAndDefectsPage.clickAssetNotesTab();

        // STEP 4 - Asset note tab is already selected
        assert_().withFailureMessage("Asset note tab should be selected.")
                .that(codicilsAndDefectsPage.isAssetNotesTabSelected())
                .isTrue();

        // STEP 5 - Verify that only asset notes with Open status are displayed
        assert_().withFailureMessage("By default, asset notes with Open status should be displayed.")
                .that(assetNotesPage.getCodicilElements().stream().allMatch(i -> i.getStatus().equals(defaultCodicilStatus)))
                .isTrue();

        //STEP 6 - Verify high level details
        assetNotesPage.getCodicilElements()
                .forEach(this::assertCodicilElements);

        //STEP 7 - View detailed info
        AssetNoteDetailsPage assetNoteDetailsPage = assetNotesPage
                .getCodicilElements()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset note not found."))
                .clickArrowButton(AssetNoteDetailsPage.class);

        assert_().withFailureMessage("Title is expected to be displayed.")
                .that(assetNoteDetailsPage.getTitle())
                .isNotEmpty();
        assert_().withFailureMessage("Category is expected to be displayed.")
                .that(assetNoteDetailsPage.getCategory())
                .isNotEmpty();
        assert_().withFailureMessage("Imposed date is expected to be displayed.")
                .that(assetNoteDetailsPage.getImposedDate())
                .isNotEmpty();
        assert_().withFailureMessage("Status is expected to be displayed.")
                .that(assetNoteDetailsPage.getStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Job number is expected to be displayed.")
                .that(assetNoteDetailsPage.getJobNumber())
                .isNotEmpty();
        assert_().withFailureMessage("Description is expected to be displayed.")
                .that(assetNoteDetailsPage.getDescription())
                .isNotEmpty();

        //STEP 8 - Final Survey Report
        assetNoteDetailsPage.clickDownloadFinalServiceReportIcon();
        ReportExportDto reportExportDto = new ReportExportDto();
        reportExportDto.setJobId(JobReport.FSR.getReportId());
        reportExportDto.setReportId(JobReport.FSR.getReportId());

        assert_().withFailureMessage("Final Service Report should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(export.getExportReportToken(reportExportDto)))
                .isNotEmpty();

        //STEP 9-10 - Back button and Search Category
        assert_().withFailureMessage("Search options are not complete.")
                .that(assetNoteDetailsPage.getAssetHeaderPage().clickBackButton(AssetNotesPage.class)
                        .getFilterBarPage()
                        .getSearchOptionList()
                        .stream()
                        .allMatch(o -> Arrays.asList(codicilSearchOptions).contains(o)))
                .isTrue();

        //STEP 11 - Search by Asset note's name
        assetNotesPage.getFilterBarPage()
                .closeSearchOptionList()
                .selectSearchDropDown(codicilSearchOptions[0])
                .setSearchTextBox(assetNoteTitle)
                .getPageReference(AssetNotesPage.class)
                .getCodicilElements()
                .forEach(e -> assert_().withFailureMessage(String.format("Title should contain %s", assetNoteTitle))
                                .that(e.getTitle())
                                .contains(assetNoteTitle));

        //STEP 12 - clear text box & STEP 15 - Filter by Change Recommended
        assetNotesPage.getFilterBarPage()
                .clickClearButtonInSearchTextBox()
                .clickFilterButton(FilterCodicilsPage.class)
                .selectChangeRecommendedRadioButton()
                .clickSubmitButton(AssetNotesPage.class)
                .getCodicilElements()
                .forEach(e -> assert_().withFailureMessage(String.format("Status should be %s", assetNoteStatus))
                        .that(e.getStatus())
                        .isEqualTo(assetNoteStatus));

        //STEP 13 - by default, asset notes should be in descending order by Imposed Date
        List<LocalDate> imposedDateList = assetNotesPage.getFilterBarPage()
                .clickClearButtonInSearchTextBox()
                .getPageReference(AssetNotesPage.class)
                .getCodicilElements()
                .stream()
                .map(e -> LocalDate.parse(e.getImposedDate(), FRONTEND_TIME_FORMAT_DTS))
                .collect(Collectors.toList());

        Collections.reverse(imposedDateList); // reverse for descending order
        assert_().withFailureMessage("Imposed dates should be in descending order.")
                .that(imposedDateList)
                .isOrdered();

        //STEP 14 - Filter by Status
        List<AssetNoteHDto> assetNoteHDtos = new Assets().getAssetNotes(1).getContent();

        FilterCodicilsPage filterCodicilsPage = assetNotesPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class);

        List<String> statusListFE = filterCodicilsPage.selectOpenRadioButton().getStatusList();
        assert_().withFailureMessage("All Statuses should be listed according to statuses of available Asset notes.")
                .that(assetNoteHDtos.stream()
                        .map(e-> e.getStatusH().getName())
                        .distinct()
                        .collect(Collectors.toList()))
                .containsAllIn(statusListFE);

        //STEP 16 - Filter by Category
        List<String> categoryListFE = filterCodicilsPage.getCategoryList();
        assert_().withFailureMessage("All Categories should be listed according to categories of available Asset notes.")
                .that(categoryListFE)
                .containsAllIn(assetNoteHDtos.stream()
                        .filter(e -> e.getCategoryH() != null)
                        .map(e-> e.getCategoryH().getName())
                        .distinct()
                        .collect(Collectors.toList()));

        filterCodicilsPage
                .deselectAllCheckBox()
                .selectClassCheckBox()
                .clickSubmitButton(AssetNotesPage.class)
                .getCodicilElements()
                .forEach(e -> assert_().withFailureMessage(String.format("Category should be %s", assetNoteCategory))
                        .that(e.getCategory())
                        .isEqualTo(assetNoteCategory));

        //STEP 17 - Export
        codicilsAndDefectsPage.clickExportButton();
        ExportPdfDto exportPdfDto = new ExportPdfDto();
        exportPdfDto.setCode(Asset.LAURA.getAssetCode());
        String exportAssetNoteToken = export.getExportAssetNoteToken(exportPdfDto);
        assert_().withFailureMessage("Asset Note list should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(exportAssetNoteToken))
                .isNotEmpty();
    }

    private void assertCodicilElements(CodicilElement codicilElement)
    {
        assert_().withFailureMessage("Title is expected to be displayed.")
                .that(codicilElement.getTitle())
                .isNotEmpty();

        assert_().withFailureMessage("Category is expected to be displayed.")
                .that(codicilElement.getCategory())
                .isNotEmpty();

        assert_().withFailureMessage("Imposed date is expected to be displayed.")
                .that(codicilElement.getImposedDate())
                .isNotEmpty();

        assert_().withFailureMessage("By default, asset notes displayed should have status Open.")
                .that(codicilElement.getStatus())
                .isEqualTo(defaultCodicilStatus);

        assert_().withFailureMessage("Status is expected to be displayed.")
                .that(codicilElement.getStatus())
                .isNotEmpty();
    }
}
