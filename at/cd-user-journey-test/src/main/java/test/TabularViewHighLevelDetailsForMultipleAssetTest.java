package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import main.serviceschedule.tabular.base.AssetTable;
import main.serviceschedule.tabular.base.CodicilTable;
import main.serviceschedule.tabular.base.ServiceTable;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.ConditionOfClassDetailsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.DueStatus;
import static constant.Epic.SERVICE_SCHEDULE;

public class TabularViewHighLevelDetailsForMultipleAssetTest extends BaseTest
{
    @Test(description = "Verify high level details of multiple asset in Service Schedule.")
    @Issue("LRCDT-1326")
    @Features(SERVICE_SCHEDULE)
    public void tabularViewHighLevelDetailsForMultipleAssetTest()
    {
        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class);

        serviceSchedulePage.getTabularView()
                .getAssetTables()
                .forEach(e ->
                {
                    //STEP 3 to 5
                    assert_().withFailureMessage("By default, assets are not expanded.")
                            .that(e.isAssetTableExpanded())
                            .isFalse();
                    assert_().withFailureMessage("Asset name should be displayed")
                            .that(e.getAssetCard().getAssetName())
                            .isNotEmpty();
                    assert_().withFailureMessage("Assset Type should be displayed")
                            .that(e.getAssetCard().getAssetType())
                            .isNotEmpty();
                    assert_().withFailureMessage("Asset class status should be displayed")
                            .that(e.getAssetCard().getClassStatus())
                            .isNotEmpty();
                    assert_().withFailureMessage("Flag should be displayed")
                            .that(e.getAssetCard().getFlag())
                            .isNotEmpty();
                    //STEP 10
                    assert_().withFailureMessage("Asset Emblem should be displayed")
                            .that(e.getAssetCard().isAssetEmblemDisplayed())
                            .isTrue();
                    //STEP 11
                    assert_().withFailureMessage("Due Status should be one of these: Overdue, Imminent, Due Soon, Not Due")
                            .that(Arrays.stream(DueStatus.values()).map(DueStatus::getDueStatus).collect(Collectors.toList()))
                            .contains(e.getAssetCard().getDueStatus());
                });

        //STEP 6 to 8
        serviceSchedulePage.getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Asset table is empty."))
                .getAssetCard()
                .clickRequestSurveyButton()
                .getSelectServicesSubPage()
                .clickCancelButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Asset table is empty."))
                .getAssetCard()
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .clickTabularViewTab();

        //STEP 9
        getDriver().navigate().back();

        //STEP 12
        AssetTable assetTable = LandingPage.getTopBar().clickClientTab()
                .getLandingPage()
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .setSearchTextBox(LAURA.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .getPageReference(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset table not found."));

        assert_().withFailureMessage("Asset Table should be expanded")
                .that(assetTable.expand().isAssetTableExpanded())
                .isTrue();

        //STEP 13
        CodicilTable codicilTable = assetTable.getCodicilTable()
                .expand();
        assert_().withFailureMessage("Codicil Table should be expanded")
                .that(codicilTable.isTableExpanded())
                .isTrue();

        //STEP 14 to 15
        List<ServiceTable> serviceTableList = codicilTable.getCodicils()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Codicil list is empty."))
                .clickArrowButton(ConditionOfClassDetailsPage.class)
                .getAssetHeaderPage()
                .clickBackButton(ServiceSchedulePage.class)
                .getFilterBarPage()
                .setSearchTextBox(LAURA.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .getPageReference(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset table not found."))
                .expand()
                .getServiceTables();

        assert_().withFailureMessage("First Service Table should be expanded")
                .that(serviceTableList.get(0)
                        .expand()
                        .isTableExpanded())
                .isTrue();

        //STEP 16
        assert_().withFailureMessage("Second Service Table should be expanded")
                .that(serviceTableList.get(1)
                        .expand()
                        .isTableExpanded())
                .isTrue();
        assert_().withFailureMessage("The first Service table should remain expanded.")
                .that(serviceTableList.get(0).isTableExpanded())
                .isTrue();

        //STEP 17 - Postponement Date
        String productName = "Asset Miscellaneous";
        long serviceWithPostponementDates = serviceTableList
                .stream()
                .filter(e-> e.getProductName().equals(productName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected product: '%s' not found.", productName)))
                .expand()
                .getServices()
                .stream()
                .filter(e-> !e.getPostponementDate().equals(""))
                .count();

        assert_().withFailureMessage("Number of services with postponement dates are expected to be more than one.")
                .that(serviceWithPostponementDates)
                .isGreaterThan(1L);
    }
}
