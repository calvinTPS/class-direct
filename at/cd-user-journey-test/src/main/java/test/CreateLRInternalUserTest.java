package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.User;
import main.administration.AdministrationPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.administration.adduser.steps.ConfirmAccountCreationPage;
import main.administration.adduser.steps.PermissionsPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.LR_INTERNAL_EMAIL;

public class CreateLRInternalUserTest extends BaseTest
{
    @Test(description = "Login as an LR Administrator and create LR Internal user")
    @Issue("LRCDT-2192")
    @Features(USER_MANAGEMENT)
    public void createLRInternalUserTest()
    {
        AdministrationPage administrationPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();

        assert_().withFailureMessage("Cannot create LR Internal user that is already existing.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(LR_INTERNAL_EMAIL))
                        .count())
                .isEqualTo(0L);

        administrationPage.clickAddUserButton()
                .setEmailAddress(LR_INTERNAL_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User card for %s was not found.", LR_INTERNAL_EMAIL)))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(User.NON_ADMIN_ACCOUNT_TYPE))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Account type %s was not found.", User.NON_ADMIN_ACCOUNT_TYPE)))
                .clickSelectButton(PermissionsPage.class)
                .setAccountExpiryDate(now.format(FRONTEND_TIME_FORMAT_DTS))
                .getFooterPage()
                .clickConfirmButton(ConfirmAccountCreationPage.class)
                .getFooterPage()
                .clickConfirmButton(AdministrationPage.class);

        assert_().withFailureMessage("Newly created LR Internal user should be displayed in the list.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(LR_INTERNAL_EMAIL))
                        .count())
                .isGreaterThan(0L);
    }
}
