package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import constant.ClassDirect;
import helper.DownloadReportHelper;
import main.common.element.CodicilElement;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterCodicilsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.actionableitems.ActionableItemDetailsPage;
import main.viewasset.codicils.codicilsanddefects.sub.actionableitems.ActionableItemsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;

public class ActionableItemsSearchFilterSortReportTest extends BaseTest
{
    @Test(description = "User can view Actionable Items for an asset and perform the following operations - search, filter, and download a Final Survey Report")
    @Issue("LRCDT-1022")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void actionableItemsSearchFilterSortReportTest()
    {
        final String searchKeyword = "4 Actionable";
        final int[] searchByNameReturnedCounts = {1, 3};
        final LocalDate[] imposedDateRange = {LocalDate.of(2015, 6, 18), LocalDate.of(2016, 6, 18)};
        final LocalDate[] dueDateRange = {LocalDate.of(2016, 12, 1), LocalDate.of(2017, 6, 1)};
        final Export export = new Export();

        // STEP 1-3: View actionable items
        CodicilsAndDefectsPage codicilsAndDefectsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s is not found.", Asset.LAURA.getName())))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton();
        ActionableItemsPage actionableItemsPage = codicilsAndDefectsPage.clickActionableItemsTab();

        // STEP 4: Verify that only actionable items with Open status are displayed
        assert_().withFailureMessage("Verify default Open status: Items with different status than Open are displayed.")
                .that(actionableItemsPage.getCodicilElements().stream().allMatch(i -> i.getStatus().equals(defaultCodicilStatus)))
                .isTrue();

        // STEP 5: Actionable item tab is already selected
        assert_().withFailureMessage("Actionable Items tab should be selected.")
                .that(codicilsAndDefectsPage.isActionableItemsTabSelected())
                .isTrue();

        // STEP 6: Verify High-level details on 'Actionable Items' page
        CodicilElement actionableItem = actionableItemsPage.getCodicilElements().get(0);
        assert_().withFailureMessage(String.format("Item '%s' does not have Title visible.", actionableItem.getTitle()))
                .that(actionableItem.getTitle().length() > 0)
                .isTrue();
        assert_().withFailureMessage(String.format("Item '%s' does not have Category displayed.", actionableItem.getTitle()))
                .that(actionableItem.getCategory().length() > 0)
                .isTrue();
        assert_().withFailureMessage(String.format("Item '%s' does not have Imposed date displayed.", actionableItem.getTitle()))
                .that(actionableItem.getImposedDate().length() > 0 && actionableItem.isImposedDateDisplayed())
                .isTrue();
        assert_().withFailureMessage(String.format("Item '%s' does not have Due date displayed.", actionableItem.getTitle()))
                .that(actionableItem.getDueDate().length() > 0 && actionableItem.isDueDateDisplayed())
                .isTrue();
        assert_().withFailureMessage(String.format("Item '%s' does not have Status displayed.", actionableItem.getTitle()))
                .that(actionableItem.getStatus().length() > 0)
                .isTrue();

        // STEP 7: View Detailed Info
        ActionableItemDetailsPage actionableItemDetailsPage = actionableItem.clickArrowButton(ActionableItemDetailsPage.class);
        assert_().withFailureMessage("Actionable Item does not have Title displayed.")
                .that(actionableItemDetailsPage.getTitle())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Actionable Item '%s' does not have Category displayed.", actionableItemDetailsPage.getTitle()))
                .that(actionableItemDetailsPage.getCategory())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Actionable Item '%s' does not have Imposed Date displayed.", actionableItemDetailsPage.getTitle()))
                .that(actionableItemDetailsPage.getImposedDate())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Actionable Item '%s' does not have Due Date displayed.", actionableItemDetailsPage.getTitle()))
                .that(actionableItemDetailsPage.getDueDate())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Actionable Item '%s' does not have Items displayed.", actionableItemDetailsPage.getTitle()))
                .that(actionableItemDetailsPage.getItems())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Actionable Item '%s' does not have Status displayed.", actionableItemDetailsPage.getTitle()))
                .that(actionableItemDetailsPage.getStatus())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Actionable Item '%s' does not have Description displayed.", actionableItemDetailsPage.getTitle()))
                .that(actionableItemDetailsPage.getDescription())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Actionable Item '%s' does not have list of Associated Defects displayed.", actionableItemDetailsPage.getTitle()))
                .that(actionableItemDetailsPage.getAssociatedClassDefects().size())
                .isGreaterThan(0);

        // STEP 8 - Associated Defects
        actionableItemDetailsPage.getAssociatedClassDefects().forEach(e->
        {
            assert_().withFailureMessage("Associated Defect does not have Title displayed.")
                    .that(e.getDefectTitle())
                    .isNotEmpty();
            assert_().withFailureMessage(String.format("Associated Defect '%s' does not have Category displayed.", e.getDefectTitle()))
                    .that(e.getDefectCategory())
                    .isNotEmpty();
            assert_().withFailureMessage(String.format("Associated Defect '%s' does not have Date Occurred displayed.", e.getDefectTitle()))
                    .that(e.getDateOccurred())
                    .isNotEmpty();
            assert_().withFailureMessage(String.format("Associated Defect '%s' does not have Status displayed.", e.getDefectTitle()))
                    .that(e.getStatus())
                    .isNotEmpty();
            assert_().withFailureMessage(String.format("Associated Defect '%s' does not have Job Number displayed.", e.getDefectTitle()))
                    .that(e.getJobNumber())
                    .isNotEmpty();
        });

        // STEP 9 - Redirect to Associated Defect details
        actionableItemDetailsPage.getAssociatedClassDefects().get(0)
                .clickArrowButton();
        codicilsAndDefectsPage.getAssetHeaderPage().clickBackButton(ActionableItemDetailsPage.class);

        // STEP 10: Download Final Survey Report
        actionableItemDetailsPage.clickDownloadFinalServiceReportIcon();
        ReportExportDto reportExportDto = new ReportExportDto();
        reportExportDto.setJobId(ClassDirect.JobReport.FSR.getReportId());
        reportExportDto.setReportId(ClassDirect.JobReport.FSR.getReportId());

        assert_().withFailureMessage("Final Service Report should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(export.getExportReportToken(reportExportDto)))
                .isNotEmpty();

        // STEP 11: Navigate back to 'Actionable Items' page and verify search options
        actionableItemsPage = actionableItemDetailsPage.getAssetHeaderPage().clickBackButton(ActionableItemsPage.class);
        assert_().withFailureMessage("Search options are not complete.")
                .that(actionableItemsPage.getFilterBarPage()
                        .getSearchOptionList()
                        .stream()
                        .allMatch(o -> Arrays.asList(codicilSearchOptions).contains(o)))
                .isTrue();

        // STEP 12-13: Verify searching by Name
        actionableItemsPage.getFilterBarPage()
                .closeSearchOptionList()
                .selectSearchDropDown(codicilSearchOptions[0])
                .setSearchTextBox(searchKeyword);

        assert_().withFailureMessage(String.format("No Actionable Item has been found for '%s' search keyword.", searchKeyword))
                .that(actionableItemsPage.getCodicilElements().size())
                .isEqualTo(searchByNameReturnedCounts[0]);

        // STEP 14: Verify clearing the search text box
        actionableItemsPage.getFilterBarPage()
                .clickClearButtonInSearchTextBox();

        assert_().withFailureMessage("Not all Actionable Items are displayed after clearing Search Text Box.")
                .that(actionableItemsPage.getCodicilElements().size())
                .isEqualTo(searchByNameReturnedCounts[1]);

        // STEP 15: Verify visibility of Status filter criteria
        FilterCodicilsPage filterCodicilsPage = actionableItemsPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class);

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Inactive is not displayed.")
                .that(filterCodicilsPage.isInactiveRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Open is not displayed.")
                .that(filterCodicilsPage.isOpenRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter 'Change Recommended' is not displayed.")
                .that(filterCodicilsPage.isChangeRecommendedRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter 'Continues Satisfactory' is not displayed.")
                .that(filterCodicilsPage.isContinuesSatisfactoryRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Closed is not displayed.")
                .that(filterCodicilsPage.isClosedRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Open is not selected by default.")
                .that(filterCodicilsPage.isOpenRadioButtonSelected())
                .isTrue();

        // STEP 16: Verify filtering by Inactive status
        actionableItemsPage = filterCodicilsPage.selectInactiveRadioButton()
                .clickSubmitButton(ActionableItemsPage.class);

        for (CodicilElement codicil : actionableItemsPage.getCodicilElements())
        {
            assert_().withFailureMessage(String.format("Filter by Inactive status: Item '%s' does not have Inactive status.", codicil.getTitle()))
                    .that(codicil.getStatus().equals("Inactive"))
                    .isTrue();
        }

        // STEP 17: Verify visibility of Category filter criteria
        filterCodicilsPage = actionableItemsPage
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class) ;
        assert_().withFailureMessage("Filter codicils dropdown: Category filter All is not displayed.")
                .that(filterCodicilsPage.isAllCheckBoxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Statutory is not displayed.")
                .that(filterCodicilsPage.isStatutoryCheckBoxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Class is not displayed.")
                .that(filterCodicilsPage.isClassCheckBoxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter External is not displayed.")
                .that(filterCodicilsPage.isExternalCheckBoxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter All is not selected.")
                .that(filterCodicilsPage.isAllCheckBoxSelected())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Statutory is not selected.")
                .that(filterCodicilsPage.isStatutoryCheckBoxSelected())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Class is not selected.")
                .that(filterCodicilsPage.isClassCheckBoxSelected())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter External is not selected.")
                .that(filterCodicilsPage.isExternalCheckBoxSelected())
                .isTrue();

        // STEP 18: Verify filtering by Statutory and External categories
        filterCodicilsPage.deselectClassCheckBox();
        assert_().withFailureMessage("Filter codicils dropdown: Category filter All is selected after deselecting Class category.")
                .that(filterCodicilsPage.isAllCheckBoxSelected())
                .isFalse();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Statutory is not selected after deselecting Class category.")
                .that(filterCodicilsPage.isStatutoryCheckBoxSelected())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter External is not selected after deselecting Class category.")
                .that(filterCodicilsPage.isExternalCheckBoxSelected())
                .isTrue();

        actionableItemsPage = filterCodicilsPage.clickSubmitButton(ActionableItemsPage.class);
        assert_().withFailureMessage("Actionable Items with Class category are displayed even it has been excluded from Category filter.")
                .that(actionableItemsPage.getCodicilElements()
                        .stream()
                        .map(CodicilElement::getCategory)
                        .anyMatch(o -> o.equals(searchKeyword)))
                .isFalse();

        // STEP 19: Verify visibility of Impose Date filter
        filterCodicilsPage = actionableItemsPage
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class);
        assert_().withFailureMessage("Filter codicils dropdown: Filter 'Impose Date From' is not displayed.")
                .that(filterCodicilsPage.isImposeDateFromDatepickerDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Filter 'Impose Date To' is not displayed.")
                .that(filterCodicilsPage.isImposeDateToDatepickerDisplayed())
                .isTrue();

        // STEP 20: Verify filtering by Imposed Date range
        actionableItemsPage = filterCodicilsPage.clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setImposeDateFromDatepicker(imposedDateRange[0].format(ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                .setImposeDateToDatepicker(imposedDateRange[1].format(ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ActionableItemsPage.class);

        actionableItemsPage.getCodicilElements().forEach(e->
                assert_().withFailureMessage(String.format("Imposed date of '%s' should be in between %s and %s.", e.getTitle(), imposedDateRange[0], imposedDateRange[1]))
                        .that(LocalDate.parse(e.getImposedDate(), ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(imposedDateRange[0], imposedDateRange[1])));

        // STEP 21: Verify visibility of Due Date filter
        filterCodicilsPage = actionableItemsPage.getFilterBarPage().clickFilterButton(FilterCodicilsPage.class);
        assert_().withFailureMessage("Filter codicils dropdown: Filter 'Due Date From' is not displayed.")
                .that(filterCodicilsPage.isDueDateFromDatepickerDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Filter 'Due Date To' is not displayed.")
                .that(filterCodicilsPage.isDueDateToDatepickerDisplayed())
                .isTrue();

        // STEP 22: Verify filtering by Due Date range
        actionableItemsPage = filterCodicilsPage.clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setDueDateFromDatepicker(dueDateRange[0].format(ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                .setDueDateToDatepicker(dueDateRange[1].format(ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ActionableItemsPage.class);

        actionableItemsPage.getCodicilElements().forEach(e->
                assert_().withFailureMessage(String.format("Due date of '%s' should be in between %s and %s.", e.getTitle(), dueDateRange[0], dueDateRange[1]))
                        .that(LocalDate.parse(e.getDueDate(), ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(dueDateRange[0], dueDateRange[1])));

        // STEP 23: Export Actionable Items
        codicilsAndDefectsPage.clickExportButton();
        ExportPdfDto exportPdfDto = new ExportPdfDto();
        exportPdfDto.setCode(Asset.LAURA.getAssetCode());
        String exportActionableItemToken = export.getExportActionableItemToken(exportPdfDto);
        assert_().withFailureMessage("Actionable Item list should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(exportActionableItemToken))
                .isNotEmpty();
    }
}
