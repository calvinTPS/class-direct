package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.common.element.MiniAssetCard;
import main.common.export.ExportAssetListingPage;
import main.common.export.element.AssetsTab;
import main.common.export.element.SurveysNotesAndActionsTab;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.GENERATE_AND_PRINT;

public class PrintExportFilterVesselListTest extends BaseTest
{
    private final String searchString = "ura";

    @Test(description = "User can export multiple assets information from the fleet dashboard")
    @Issue("LRCDT-1668")
    @Features(GENERATE_AND_PRINT)
    public void printExportFilterVesselListTest()
    {
        VesselListPage vesselListPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage();

        int assetCount = vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(searchString)
                .getPageReference(VesselListPage.class)
                .getShowingXofYPage()
                .getShown();

        ExportAssetListingPage exportAssetListingPage = vesselListPage.clickExportAssetInformationButton();

        // STEP 6 - Asset count
        AssetsTab assetsTab = exportAssetListingPage.clickAssetsTab();
        assert_().withFailureMessage("Number of assets should matched the count filtered on the vessel fleet.")
                .that(assetsTab.getAssetCards().size())
                .isEqualTo(assetCount);

        // STEP 7 - Deselect an asset
        MiniAssetCard assetCard = assetsTab.getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(LAURA.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."));

        assert_().withFailureMessage(String.format("This asset %s should be deselected.", LAURA.getName()))
                .that(assetCard.deselectCheckBox().isAssetCardSelected())
                .isFalse();

        // STEP 8 - Select an asset
        assert_().withFailureMessage(String.format("This asset %s should be selected.", LAURA.getName()))
                .that(assetCard.selectAssetCard().isAssetCardSelected())
                .isTrue();

        // STEP 9 - Basic Asset Information Radiobutton in Information Tab
        assert_().withFailureMessage("Basic Asset Information Radiobutton should be selected.")
                .that(exportAssetListingPage.clickInformationTab()
                        .selectBasicAssetInfoRadioButton()
                        .isBasicAssetInfoRadioButtonSelected())
                .isTrue();

        // STEP 10 - uncheck all codicils in 'Services and Codicils' tab
        SurveysNotesAndActionsTab surveysNotesAndActionsTab = exportAssetListingPage.surveysNotesAndActionsTab();
        assert_().withFailureMessage("Condition of Class checkbox should be deselected.")
                .that(surveysNotesAndActionsTab.deselectConditionOfClassCheckbox().isConditionOfClassCheckboxSelected())
                .isFalse();
        assert_().withFailureMessage("Actionable Item checkbox should be deselected.")
                .that(surveysNotesAndActionsTab.deselectActionableItemCheckbox().isActionableItemCheckboxSelected())
                .isFalse();
        assert_().withFailureMessage("Asset note checkbox should be deselected.")
                .that(surveysNotesAndActionsTab.deselectAssetNoteCheckbox().isAssetNoteCheckboxSelected())
                .isFalse();

        // STEP 11 - Export assets - There is no fixed checksum since the file name is changing
        exportAssetListingPage.clickExportButton();

        assert_().withFailureMessage("Asset listing should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(getToken()))
                .isNotEmpty();

        // STEP 12-15 - The content of the zip file cannot be verified.
    }

    private String getToken()
    {
        AssetQueryHDto assetQueryHDto = new AssetQueryHDto();
        assetQueryHDto.setSearch(String.format("%s%s%s", "*", searchString, "*"));
        assetQueryHDto.setIsFavourite(false);

        AssetExportDto assetExportDto = new AssetExportDto();
        assetExportDto.setAssetQuery(assetQueryHDto);
        assetExportDto.setInfo(1L);
        assetExportDto.setService(3L);
        assetExportDto.setCodicils(Arrays.asList(4L));
        return new Export().getExportAssetToken(assetExportDto);
    }
}
