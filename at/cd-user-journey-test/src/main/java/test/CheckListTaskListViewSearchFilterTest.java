package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ChecklistExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.TaskListExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import constant.ClassDirect.Asset;
import helper.DownloadReportHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterTasksPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import main.serviceschedule.details.CheckListPage;
import main.serviceschedule.details.TaskListPage;
import main.serviceschedule.details.element.CheckListGroup;
import main.serviceschedule.details.element.TaskElement;
import main.serviceschedule.graphical.ServiceCodicilOverlayPage;
import main.vessellist.VesselListPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.TAKSLISTS_AND_CHECKLISTS;
import static helper.TestDateHelper.toDate;

public class CheckListTaskListViewSearchFilterTest extends BaseTest
{
    private final String productName = "Machinery";
    private final String productNameChecklist = "Bulk Cargoes";
    private final LocalDate fromDueDate = LocalDate.of(2016, 5, 20);
    private final LocalDate toDueDate = fromDueDate.plusMonths(1);
    private final String checkListGroupSearchText = "1";
    private final Export export = new Export();

    @Test(description = "View, Search and Filter Tasklist from SS fleet tabular view and View, Search a Checklist from SS Graphical view")
    @Issue("LRCDT-1539")
    @Features(TAKSLISTS_AND_CHECKLISTS)
    public void checkListTaskListViewSearchFilterInServiceScheduleTest()
    {
        TaskListPage taskListPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset not found."))
                .expand()
                .getServiceTables()
                .stream()
                .filter(e -> e.getProductName().equals(productName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected product: %s not found", productName)))
                .expand()
                .getServices()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("No services found under this product: %s", productName)))
                .clickArrowButton(TaskListPage.class)
                .getFilterBarPage()
                .clickFilterButton(FilterTasksPage.class)
                .clickSubmitButton(TaskListPage.class);

        // STEP 5
        assert_().withFailureMessage("'For information only' text is expected to be displayed")
                .that(taskListPage.getForInformationOnlyText())
                .isNotEmpty();

        // STEP 6 - Verify high level details
        taskListPage.getTaskList()
                .forEach(this::assertTaskList);

        // STEP 7 - Export Task list
        taskListPage.clickExportButton();
        TaskListExportDto taskListExportDto = new TaskListExportDto();
        taskListExportDto.setAssetCode(Asset.WITH_CERTIFICATES.getAssetCode());
        taskListExportDto.setServiceId(528L);

        assert_().withFailureMessage("User listing should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(export.getExportTaskListToken(taskListExportDto)))
                .isNotEmpty();

        // STEP 8 - Cannot automate the verification on PDF content

        // STEP 9 - Search Task list
        assertTaskOrderAndFilterByTaskName(taskListPage);

        // STEP 10 - Filter by due date
        assertDueDateTaskList(taskListPage);

        // STEP 11 - Back
        ServiceSchedulePage serviceSchedulePage = taskListPage.getAssetHeaderPage().clickBackButton(ServiceSchedulePage.class);

        // STEP 12-14
        serviceSchedulePage.clickGraphicalViewTab();
        CheckListPage checkListPage = serviceSchedulePage.getFilterBarPage()
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class)
                .clickGraphicalViewTab()
                .getAssetTables()
                .stream()
                .filter(e -> e.getAssetCard().getAssetName().equals(Asset.WITH_CERTIFICATES.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset not found."))
                .getServices()
                .stream()
                .filter(e ->
                {
                    // find service with Checklist (Bulk Cargoes)
                    ServiceCodicilOverlayPage serviceCodicilOverlayPage = e.clickDataElement();
                    boolean hasChecklist = serviceCodicilOverlayPage.getServiceTables()
                            .stream().filter(m -> m.getProductName().equals(productNameChecklist))
                            .collect(Collectors.toList()).size() > 0;
                    serviceCodicilOverlayPage.clickCloseButton();
                    return hasChecklist;
                })
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No services with Checklist found"))
                .clickDataElement()
                .getServiceTables()
                .stream().filter(m -> m.getProductName().equals(productNameChecklist))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Checklist %s not found", productNameChecklist)))
                .expand()
                .getServices()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Services under %s not found", productNameChecklist)))
                .clickArrowButton(CheckListPage.class);

        // STEP 15 - Verify checklist
        checkListPage.getCheckListGroups().forEach(this::assertCheckList);

        // STEP 16 - Search for checklist group
        checkListPage.setCheckListSearchText(checkListGroupSearchText)
                .getCheckListGroups()
                .forEach(e -> assert_().withFailureMessage(
                        String.format("Check list group name should be filtered by this search text: '%s'.", checkListGroupSearchText))
                        .that(e.getGroupName())
                        .contains(checkListGroupSearchText));
    }

    @Test(description = "View, Search and Filter Tasklist from Asset hub Graphical view and View, Search a Checklist from  Asset hub Tabular view")
    @Issue("LRCDT-1540")
    @Features(TAKSLISTS_AND_CHECKLISTS)
    public void checkListTaskListViewSearchFilterInAssetHubTest()
    {
        final String creditStatus = "Not Credited";
        final String serviceName = "Boiler 1";

        TaskListPage taskListPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .clickGraphicalViewTab()
                .getServiceTables()
                .stream()
                .filter(e -> e.getProductName().equals(productName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected product: %s not found", productName)))
                .getDataElements()
                .stream()
                .filter(e -> e.getServiceName().equals(serviceName))
                .findFirst()
                .orElseThrow(
                        () -> new NoSuchElementException(String.format("Service '%s' not found under this product: %s", serviceName, productName)))
                .clickViewDetailsButton(TaskListPage.class)
                .getFilterBarPage()
                .clickFilterButton(FilterTasksPage.class)
                .clickSubmitButton(TaskListPage.class);

        // STEP 4 - Verify high level details
        int taskCount = taskListPage.getTaskList().size();
        taskListPage.getTaskList().forEach(this::assertTaskList);

        // STEP 5 - Search Tasklist
        assertTaskOrderAndFilterByTaskName(taskListPage);

        // STEP 6 - Clear Search Textbox
        taskListPage.getFilterBarPage().clickClearButtonInSearchTextBox();
        assert_().withFailureMessage("All tasklist items should be displayed.")
                .that(taskListPage.getTaskList().size())
                .isEqualTo(taskCount);

        //STEP 7-9 - Filter by Credit Status
        taskListPage.getFilterBarPage()
                .clickFilterButton(FilterTasksPage.class)
                .deSelectCreditStatusCheckBoxByStatusName("All")
                .selectCreditStatusCheckBoxByStatusName(creditStatus)
                .clickSubmitButton(TaskListPage.class)
                .getTaskList()
                .forEach(e ->
                        assert_().withFailureMessage(String.format("Expected credit status is %s", creditStatus))
                                .that(e.getCreditStatus())
                                .isEqualTo(creditStatus)
                );

        //STEP 8 - Clear filters
        taskListPage.getFilterBarPage()
                .clickFilterButton(FilterTasksPage.class)
                .clickClearFiltersButton();

        // STEP 10 - Filter by Due Date
        assertDueDateTaskList(taskListPage);

        // STEP 11 - Back
        main.viewasset.schedule.ServiceSchedulePage serviceSchedulePage = taskListPage.getAssetHeaderPage()
                .clickBackButton(main.viewasset.schedule.ServiceSchedulePage.class);
        assert_().withFailureMessage("Back button was not successfully clicked.")
                .that(serviceSchedulePage.getScheduleElement().exists())
                .isTrue();

        // STEP 12 - Select service with Checklist
        CheckListPage checkListPage = serviceSchedulePage
                .getTabularView()
                .getServiceTables()
                .stream()
                .filter(e -> e.getProductName().equals(productNameChecklist))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected product %s not found.", productNameChecklist)))
                .getServices()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected service with Checklist not found."))
                .clickArrowButton(CheckListPage.class);

        // STEP 13 - Verify Checklist
        checkListPage.getCheckListGroups().forEach(this::assertCheckList);

        // STEP 14 - Search for checklist group
        checkListPage.setCheckListSearchText(checkListGroupSearchText)
                .getCheckListGroups()
                .forEach(e -> assert_().withFailureMessage(
                        String.format("Check list group name should be filtered by this search text: '%s'.", checkListGroupSearchText))
                        .that(e.getGroupName())
                        .contains(checkListGroupSearchText));

        // STEP 15 - Export Checklist
        checkListPage.clickExportButton();
        ChecklistExportDto checklistExportDto = new ChecklistExportDto();
        checklistExportDto.setAssetCode(Asset.WITH_CERTIFICATES.getAssetCode());
        checklistExportDto.setServiceId(535L);

        assert_().withFailureMessage("User listing should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(export.getExportCheckListToken(checklistExportDto)))
                .isNotEmpty();

        // STEP 16 - Cannot automate the verification on PDF content

        // STEP 17 - Logout
        LandingPage.getTopBar().clickMyAccountButton().clickLogoutButton();
    }

    private void assertTaskList(TaskElement taskElement)
    {
        assert_().withFailureMessage("Task name is expected to be displayed")
                .that(taskElement.getTaskName())
                .isNotEmpty();
        assert_().withFailureMessage("Task number is expected to be displayed")
                .that(taskElement.getTaskNumber())
                .isNotEmpty();
        assert_().withFailureMessage("Due date is expected to be displayed")
                .that(taskElement.getDueDate())
                .isNotNull();
        assert_().withFailureMessage("Assigned date is expected to be displayed")
                .that(taskElement.getAssignedDate())
                .isNotNull();
        assert_().withFailureMessage("Credit status is expected to be displayed")
                .that(taskElement.getCreditStatus())
                .isNotEmpty();
    }

    private void assertTaskOrderAndFilterByTaskName(TaskListPage taskListPage)
    {
        final String taskName = "Task";

        List<String> taskNumberList = taskListPage.getTaskList()
                .stream()
                .map(TaskElement::getTaskNumber)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Tasks should be sorted by Task Number in ascending order")
                .that(Ordering.from(String.CASE_INSENSITIVE_ORDER).isOrdered(taskNumberList))
                .isTrue();

        taskListPage.getFilterBarPage()
                .setSearchTextBox(taskName)
                .getPageReference(TaskListPage.class)
                .getTaskList()
                .forEach(e ->
                        assert_().withFailureMessage(String.format("Tasks with name containing %s are expected to be displayed", taskName))
                                .that(e.getTaskName().toLowerCase())
                                .contains(taskName.toLowerCase())
                );
    }

    private void assertDueDateTaskList(TaskListPage taskListPage)
    {
        taskListPage.getFilterBarPage()
                .clickClearButtonInSearchTextBox()
                .clickFilterButton(FilterTasksPage.class)
                .setDueDateFromDatepicker(fromDueDate.format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDateToDatepicker(toDueDate.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(TaskListPage.class)
                .getTaskList()
                .forEach(e ->
                        assert_().withFailureMessage(String.format("Due date is expected in between %s and %s",
                                fromDueDate.format(FRONTEND_TIME_FORMAT_DTS),
                                toDueDate.format(FRONTEND_TIME_FORMAT_DTS)))
                                .that(e.getDueDate())
                                .isIn(Range.closed(toDate(fromDueDate), toDate(toDueDate)))
                );
    }

    private void assertCheckList(CheckListGroup checkListGroup)
    {
        checkListGroup.expand().getCheckListElements()
                .forEach(checkListElement ->
                {
                    assert_().withFailureMessage("Check list number should be displayed.")
                            .that(checkListElement.getCheckListNumber())
                            .isNotEmpty();
                    assert_().withFailureMessage("Check list description should be displayed.")
                            .that(checkListElement.getDescription())
                            .isNotEmpty();
                    assert_().withFailureMessage("Credit Status should be displayed.")
                            .that(checkListElement.getStatus())
                            .isNotEmpty();
                });
    }
}
