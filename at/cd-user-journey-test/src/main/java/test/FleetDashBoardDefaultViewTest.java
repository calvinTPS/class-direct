package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;
import static constant.Epic.FLEET_DASHBOARD;
import static helper.TestDateHelper.toDate;

public class FleetDashBoardDefaultViewTest extends BaseTest
{
    private LocalDate fromDate = now.of(2014, 5, 1);
    private final String toDate = FRONTEND_TIME_FORMAT_DTS.format(now.minusDays(3));

    @Test(description = "Default view_All_no assets marked as Favourite and filter assets by date of build")
    @Issue("LRCDT-592")
    @Features(FLEET_DASHBOARD)
    public void fleetDashBoardDefaultViewTest()
    {
        //Open the app set "From Date of Build" under Filter Assets side Bar and perform search
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .setFromDateOfBuild(FRONTEND_TIME_FORMAT_DTS.format(fromDate))
                .clickSubmitButton(VesselListPage.class);

        List<Date> buildDates = getFilteredBuildDates(vesselListPage);
        assert_().withFailureMessage("The FROM date search criteria should return results")
                .that(buildDates.size() > 0)
                .isTrue();
        buildDates.forEach(e -> assert_().withFailureMessage("Displayed Assets Build Date are expected to be with specified From date of build ")
                .that(e.equals(toDate(fromDate))
                        || e.after(toDate(fromDate)))
                .isTrue());

        //Set "To Date of Build" under Filter Assets side Bar and perform search
        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .setFromDateOfBuild("")
                .setToDateOfBuild(toDate)
                .clickSubmitButton(VesselListPage.class);

        buildDates = getFilteredBuildDates(vesselListPage);
        assert_().withFailureMessage("The TO date search criteria should return results")
                .that(buildDates.size() > 0)
                .isTrue();
        buildDates.forEach(e -> assert_().withFailureMessage("Displayed Assets Build Date are expected to be with specified To date of build ")
                .that(e.equals(toDate(now.minusDays(3)))
                        || e.before(toDate(now.minusDays(3))))
                .isTrue());

        //Set "From Date of Build" and "To Date of Build" under Filter Assets side Bar and perform search
        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .setFromDateOfBuild(FRONTEND_TIME_FORMAT_DTS.format(fromDate))
                .setToDateOfBuild(toDate)
                .clickSubmitButton(VesselListPage.class);

        buildDates = getFilteredBuildDates(vesselListPage);
        assert_().withFailureMessage("The FROM and TO date search criteria should return results")
                .that(buildDates.size() > 0)
                .isTrue();
        buildDates.forEach(e -> assert_().withFailureMessage("Displayed Assets Build Date are expected to be with specified date of build ")
                .that(e.equals(toDate(fromDate))
                        || e.after(toDate(fromDate))
                        || e.equals(toDate(now.minusDays(3)))
                        || e.before(toDate(now.minusDays(3))))
                .isTrue());
    }

    private List<Date> getFilteredBuildDates(VesselListPage vesselListPage)
    {
        return vesselListPage.getAssetCards()
                .stream()
                .filter(e -> e.getDateOfBuild() != null)
                .map(AssetCard::getDateOfBuild)
                .collect(Collectors.toList());
    }
}
