package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.detail.sub.accessibleassets.AccessibleAssetsPage;
import main.administration.detail.sub.accessibleassets.EditAccessibleAssetsPage;
import main.common.element.MiniAssetCard;
import main.administration.detail.sub.restrictedassets.EditRestrictedAssetsPage;
import main.administration.detail.sub.restrictedassets.RestrictedAssetsPage;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.FLAG_USER;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset.FOR_RESTRICTED_TEST;
import static constant.Epic.USER_MANAGEMENT;

public class ViewEditSelectMoveAccessibleRestrictedAssetTest extends BaseTest {
    @Test(description = "Automation_UJ_UserManagement_AdminUser_ViewEditSelectMoveRestrictedAssets and ViewEditSelectMoveAccessibleAssets")
    @Issues({
            @Issue("LRCDT-1113"),
            @Issue("LRCDT-1114")
    })
    @Features(USER_MANAGEMENT)
    public void viewEditSelectMoveAccessibleRestrictedAssetTest() {
        //Open the app, Click Administration Tab and select user=lr_admin
        RestrictedAssetsPage restrictedAssetsPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(FLAG_USER.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User with email: %s is not present", FLAG_USER.getEmail())))
                .clickFurtherMoreDetailsButton()
                .clickRestrictedAssetsTab();

        boolean isRestrictedAsset = restrictedAssetsPage
                .getRestrictedAssetCards()
                .stream()
                .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()));

        if (isRestrictedAsset)
        {
            moveRestrictedAsset(restrictedAssetsPage);
            moveAccessibleAsset(restrictedAssetsPage);
        }
        else
        {
            moveAccessibleAsset(restrictedAssetsPage);
            moveRestrictedAsset(restrictedAssetsPage);
        }
    }

    private void moveAccessibleAsset(RestrictedAssetsPage restrictedAssetsPage)
    {
        //LRCDT-1113
        AccessibleAssetsPage accessibleAssetsPage = restrictedAssetsPage.getAdministrationDetailPage()
                .clickAccessibleAssetsTab();

        assert_().withFailureMessage("Accessible Asset Cards are expected to be Displayed.")
                .that(accessibleAssetsPage.getAccesibleAssetCards().size())
                .isGreaterThan(0);

        //Click to Edit Accessible Assets
        EditAccessibleAssetsPage editAccessibleAssetsPage = accessibleAssetsPage.clickEditButton();

        assert_().withFailureMessage("Select All Button is expected to be Enabled")
                .that(editAccessibleAssetsPage.isSelectAllButtonEnabled())
                .isTrue();

        editAccessibleAssetsPage
                .getAccessibleAssetCards()
                .stream()
                .filter(e-> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()))
                .findFirst()
                .orElseThrow(()-> new NoSuchElementException(String.format("Expected asset %s is not present", FOR_RESTRICTED_TEST.getName())))
                .selectAssetCard();

        assert_().withFailureMessage("Cancel Button is expected to be Enabled")
                .that(editAccessibleAssetsPage.isCancelButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("Deselect All Button is expected to be Enabled")
                .that(editAccessibleAssetsPage.isDeselectAllButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("Cancel Button is expected to be Enabled")
                .that(editAccessibleAssetsPage.isCancelButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("Move To Restricted Assets Button is expected to be Enabled")
                .that(editAccessibleAssetsPage.isMoveToRestrictedButtonEnabled())
                .isTrue();

        editAccessibleAssetsPage
                .clickMoveToRestrictedAssetButton()
                .clickCancelButton(EditAccessibleAssetsPage.class);

        assert_().withFailureMessage(String.format("Selected Asset %s is expected to be Displayed", FOR_RESTRICTED_TEST.getName()))
                .that(editAccessibleAssetsPage.getAccessibleAssetCards()
                        .stream()
                        .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName())))
                .isTrue();

        editAccessibleAssetsPage.getAccessibleAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset: %s is not present", FOR_RESTRICTED_TEST.getName())))
                .selectAssetCard();

        accessibleAssetsPage = editAccessibleAssetsPage
                .clickMoveToRestrictedAssetButton()
                .clickConfirmButton(AccessibleAssetsPage.class);

        assert_().withFailureMessage(String.format("Asset %s is expected to be moved to Restricted Assets.", FOR_RESTRICTED_TEST.getName()))
                .that(accessibleAssetsPage
                        .getAdministrationDetailPage()
                        .clickRestrictedAssetsTab()
                        .getRestrictedAssetCards()
                        .stream()
                        .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName())))
                .isTrue();
    }

    private void moveRestrictedAsset(RestrictedAssetsPage restrictedAssetsPage)
    {
        //LRCDT-1114
        assert_().withFailureMessage("Restricted Asset Cards are expected to be Displayed.")
                .that(restrictedAssetsPage.getRestrictedAssetCards().size())
                .isGreaterThan(0);

        //Click to Edit Restricted Assets
        EditRestrictedAssetsPage editRestrictedAssetsPage = restrictedAssetsPage.clickEditButton();

        assert_().withFailureMessage("Select All Button is expected to be Enabled")
                .that(editRestrictedAssetsPage.isSelectAllButtonEnabled())
                .isTrue();

        MiniAssetCard restrictedAssetCard = editRestrictedAssetsPage
                .getRestrictedAssetCards()
                .stream()
                .filter(e-> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset: %s is not present", FOR_RESTRICTED_TEST.getName())));

        restrictedAssetCard.selectAssetCard();

        assert_().withFailureMessage("Cancel Button is expected to be Enabled")
                .that(editRestrictedAssetsPage.isCancelButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("Deselect All Button is expected to be Enabled")
                .that(editRestrictedAssetsPage.isDeselectAllButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("Cancel Button is expected to be Enabled")
                .that(editRestrictedAssetsPage.isCancelButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("Move To Accessible Assets Button is expected to be Enabled")
                .that(editRestrictedAssetsPage.isMoveToAccessibleButtonEnabled())
                .isTrue();

        editRestrictedAssetsPage
                .clickMoveToAccessibleAssetButton()
                .clickCancelButton(EditRestrictedAssetsPage.class);

        assert_().withFailureMessage(String.format("Selected Asset %s is expected to be Displayed", FOR_RESTRICTED_TEST.getName()))
                .that(editRestrictedAssetsPage.getRestrictedAssetCards()
                        .stream()
                        .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName())))
                .isTrue();

        editRestrictedAssetsPage.getRestrictedAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()))
                .findFirst()
                .orElseThrow(()->new NoSuchElementException(String.format("Expected asset: %s is not present", FOR_RESTRICTED_TEST.getName())))
                .selectAssetCard();

        restrictedAssetsPage = editRestrictedAssetsPage
                .clickMoveToAccessibleAssetButton()
                .clickConfirmButton(RestrictedAssetsPage.class);

        assert_().withFailureMessage(String.format("Asset %s is expected to be moved to Accessible Assets.", FOR_RESTRICTED_TEST.getName()))
                .that(restrictedAssetsPage
                        .getAdministrationDetailPage()
                        .clickAccessibleAssetsTab()
                        .getAccesibleAssetCards()
                        .stream()
                        .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName())))
                .isTrue();
    }
}
