package test;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import helper.DownloadReportHelper;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.nationaladmin.NationalAdministrationPage;
import main.nationaladmin.element.CountryFileElement;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.CountryFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.COUNTRY_FILES;
import static constant.User.Flag.ANGOLA;

public class ViewNationalAdministrationGuidelinesForACountryTest extends BaseTest
{
    @Test(description = "View the National Administration Guidelines for a country.")
    @Issue("LRCDT-2196")
    @Features(COUNTRY_FILES)
    public void viewNationalAdministrationGuidelinesForACountryTest()
    {
        final String latestCountryFileId = "12";
        final String oldCountryFileId = "11";
        final LocalDate modifiedDateFrom = LocalDate.of(2000, 5, 24);
        final LocalDate modifiedDateTo = LocalDate.of(2001, 6, 24);

        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword());
        NationalAdministrationPage nationalAdministrationPage = LandingPage.getTopBar().clickNationalAdministrationButton();

        // STEP 2-3 - Select flag
        nationalAdministrationPage.selectFlagName(ANGOLA.getName());

        // STEP 4 - Verify Country file (Version in use)
        CountryFileElement countryFileElement = nationalAdministrationPage.getCountryFileVersionInUse();
        assertCountryFile(countryFileElement);

        // Verify older version
        List<CountryFileElement> oldVersionCountryFileElements = nationalAdministrationPage.getCountryFilesOlderVersion();
        oldVersionCountryFileElements.forEach(this::assertCountryFile);

        // Descending order by Modification date
        List<LocalDate> modificationDates = oldVersionCountryFileElements.stream()
                .map(CountryFileElement::getLastModifiedDate)
                .collect(Collectors.toList());

        Collections.reverse(modificationDates);
        assert_().withFailureMessage("Historical country files should be in descending order by modification date.")
                .that(modificationDates)
                .isOrdered();

        // STEP 5 - Download version in use country file
        List<SupplementaryInformationHDto> supplementaryInformationHDto = new CountryFile().getCountryFiles(ANGOLA.getId());
        countryFileElement.clickDownloadIcon();
        String latestCountryFileToken = supplementaryInformationHDto.stream()
                .filter(e-> e.getId().toString().equals(latestCountryFileId))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected country file with ID: %s is not found", latestCountryFileId)))
                .getToken();

        assert_().withFailureMessage("Latest country file should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(latestCountryFileToken))
                .isNotEmpty();

        // STEP 6 - Download an older version country file
        oldVersionCountryFileElements.stream().findFirst().get().clickDownloadIcon();
        String oldCountryFileToken = supplementaryInformationHDto.stream()
                .filter(e-> e.getId().toString().equals(oldCountryFileId))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected country file with ID: %s is not found", oldCountryFileId)))
                .getToken();

        assert_().withFailureMessage("Old country file should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(oldCountryFileToken))
                .isNotEmpty();

        // STEP 7 - Filter by modification range
        nationalAdministrationPage.clickFilterButton().setFromModificationRange(modifiedDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setToModificationRange(modifiedDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton()
                .getCountryFilesOlderVersion()
                .forEach(e ->  assert_().withFailureMessage(
                        String.format("Date modified of country files should be in between %s and %s", modifiedDateFrom, modifiedDateTo))
                        .that(e.getLastModifiedDate())
                        .isIn(Range.closed(modifiedDateFrom, modifiedDateTo)));
    }

    private void assertCountryFile(CountryFileElement countryFileElement)
    {
        assert_().withFailureMessage("Flag name of country file should be displayed.")
                .that(countryFileElement.getFlagName())
                .isNotEmpty();
        assert_().withFailureMessage("Last modified Date of country file should be displayed.")
                .that(countryFileElement.getLastModifiedDate())
                .isNotNull();
        assert_().withFailureMessage("Last modified Time of country file should be displayed.")
                .that(countryFileElement.getLastModifiedTime())
                .isNotEmpty();
    }
}
