package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.serviceschedule.details.TaskListPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.ConditionOfClassDetailsPage;
import main.viewasset.schedule.ServiceSchedulePage;
import main.viewasset.schedule.SurveyPlannerPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import typifiedelement.Timeline;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.SERVICE_SCHEDULE;

public class TabularViewHighLevelDetailsAndTimelineFilterTest extends BaseTest
{
    @Test(description = "Verify that services and codicils for an asset can be viewed and filtered by timescale")
    @Issue("LRCDT-1324")
    @Features(SERVICE_SCHEDULE)
    public void tabularViewHighLevelDetailsAndTimelineFilterTest()
    {
        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        //STEP 3
        assert_().withFailureMessage("By default, tabular view should be selected.")
                .that(serviceSchedulePage.getScheduleElement().isTabularViewSelected())
                .isTrue();

        //STEP 4
        Timeline timeline = serviceSchedulePage.getScheduleElement().getServiceTimeline();
        assert_().withFailureMessage("Today label should be present in the timescale.")
                .that(timeline.isTodayDisplayed())
                .isTrue();

        //STEP 5
        LocalDate earliestDate = timeline.getSelectedStartDate();

        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(e -> e.getServices().forEach(
                        s -> assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of services")
                                .that(LocalDate.parse(s.getDueDate(), FRONTEND_TIME_FORMAT_DTS).isAfter(earliestDate.minusDays(1)))
                                .isTrue())
                );

        serviceSchedulePage.getTabularView()
                .getCodicilTables()
                .getCodicils()
                .forEach(
                        s -> assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of codicils")
                                .that(LocalDate.parse(s.getDueDate(), FRONTEND_TIME_FORMAT_DTS).isAfter(earliestDate.minusDays(1)))
                                .isTrue()
                );

        //STEP 6
        LocalDate latestDate = timeline.getSelectedEndDate();

        assert_().withFailureMessage("Month for latest date should be the same as earliest date.")
                .that(latestDate.getMonth())
                .isEqualTo(earliestDate.getMonth());
        assert_().withFailureMessage("Year for latest date should be 7 years greater.")
                .that(latestDate.getYear())
                .isEqualTo(earliestDate.getYear() + 7);

        //STEP 7 to 8
        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(e ->
                {
                    assert_().withFailureMessage("Services should be listed by product.")
                            .that(e.getProductName().isEmpty())
                            .isFalse();
                    e.getServices().forEach(s ->
                    {
                        assert_().withFailureMessage("Service title should be displayed")
                                .that(s.getServiceName().isEmpty())
                                .isFalse();
                        assert_().withFailureMessage("Due date should be displayed")
                                .that(s.getDueDate().isEmpty())
                                .isFalse();
                        assert_().withFailureMessage("Assigned date should be displayed")
                                .that(s.getAssignedDate().isEmpty())
                                .isFalse();
                        assert_().withFailureMessage("Due Status should be displayed")
                                .that(s.getDueStatus().isEmpty())
                                .isFalse();
                    });
                });

        //STEP 9 to 12
        serviceSchedulePage
                .getTabularView()
                .getServiceTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Service table for this asset not found."))
                .getServices()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Service list is empty."))
                .clickArrowButton(TaskListPage.class)
                .getAssetHeaderPage()
                .clickBackButton(ServiceSchedulePage.class)
                .clickBackButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .getTabularView()
                .getCodicilTables()
                .getCodicils()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Codicil list is empty."))
                .clickArrowButton(ConditionOfClassDetailsPage.class)
                .getAssetHeaderPage()
                .clickBackButton(ServiceSchedulePage.class)
                .clickBackButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        //STEP 14
        LocalDate newEarliestDate = earliestDate.plusMonths(2);
        timeline.selectStartDate(newEarliestDate);

        assert_().withFailureMessage("Earliest date should be changed in the time scale.")
                .that(timeline.getSelectedStartDate())
                .isEqualTo(newEarliestDate);

        //STEP 15
        LocalDate newLatestDate = latestDate.minusMonths(2);
        timeline.selectEndDate(newLatestDate);

        assert_().withFailureMessage("Latest date should be changed in the time scale.")
                .that(timeline.getSelectedEndDate())
                .isEqualTo(newLatestDate);

        //STEP 16
        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(e -> e.getServices().forEach(s ->
                        {
                            LocalDate dueDate = LocalDate.parse(s.getDueDate(), FRONTEND_TIME_FORMAT_DTS);
                            assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of services")
                                    .that(dueDate.isAfter(newEarliestDate))
                                    .isTrue();
                            assert_().withFailureMessage("Latest date in timeline should be the earliest among due dates of services")
                                    .that(dueDate.isBefore(newLatestDate))
                                    .isTrue();
                        })
                );

        serviceSchedulePage.getTabularView()
                .getCodicilTables()
                .getCodicils()
                .forEach(s ->
                        {
                            LocalDate dueDate = LocalDate.parse(s.getDueDate(), FRONTEND_TIME_FORMAT_DTS);
                            assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of codicils")
                                    .that(dueDate.isAfter(newEarliestDate))
                                    .isTrue();
                            assert_().withFailureMessage("Latest date in timeline should be the earliest among due dates of codicils")
                                    .that(dueDate.isBefore(newLatestDate))
                                    .isTrue();
                        }
                );
    }
}
