package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.TopBarPage;
import main.common.element.AssetCard;
import main.common.export.ExportAssetListingPage;
import main.common.export.element.InformationTab;
import main.common.export.element.SurveysNotesAndActionsTab;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.assetdetail.AssetDetailsPage;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import main.viewasset.certificates.certificatesandrecords.CertificatesAndRecordsPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.assetdetail.sistervessels.SisterVesselsPage;
import main.viewasset.surveys.servicehistory.ServiceHistoryPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;

public class ViewAllInformationOfAccessibleLRClassedAssetTest extends BaseTest
{
    @Test(description = "Given the User has an Accessible Asset, the User can view all available information in the System for this Asset")
    @Issue("LRCDT-2124")
    @Features(USER_MANAGEMENT)
    public void viewAllInformationOfAccessibleLRClassedAssetTest()
    {
        AssetCard assetCard = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s not found.", Asset.LAURA.getName())));

        assert_().withFailureMessage("IMO number should be displayed.")
                .that(assetCard.isImoNumberDisplayed())
                .isTrue();
        assert_().withFailureMessage("Asset name should be displayed.")
                .that(assetCard.getAssetName())
                .isNotEmpty();
        assert_().withFailureMessage("Asset type should be displayed.")
                .that(assetCard.isAssetTypeDisplayed())
                .isTrue();
        assert_().withFailureMessage("Date of build should be displayed.")
                .that(assetCard.isBuildDateDisplayed())
                .isTrue();
        assert_().withFailureMessage("Date of build should be displayed.")
                .that(assetCard.isBuildDateDisplayed())
                .isTrue();
        assert_().withFailureMessage("Flag should be displayed.")
                .that(assetCard.getFlagName())
                .isNotEmpty();
        assert_().withFailureMessage("Class should be displayed.")
                .that(assetCard.getClassStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Gross tonnage should be displayed.")
                .that(assetCard.getGrossTonnage())
                .isNotEmpty();
        assert_().withFailureMessage("Overall status should be displayed.")
                .that(assetCard.getDueStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Icon representing the asset type should be displayed.")
                .that(assetCard.isAssetEmblemDisplayed())
                .isTrue();
        assert_().withFailureMessage("LR badge icon should be displayed.")
                .that(assetCard.isLrBadgeIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Request Survey button should be displayed.")
                .that(assetCard.isRequestSurveyButtonDisplayed())
                .isTrue();

        // STEP 3 - Click View Asset button
        AssetHubPage assetHubPage = assetCard.clickViewAssetButton();

        assert_().withFailureMessage("Request Survey button should be displayed in the header.")
                .that(assetHubPage.getAssetHeaderPage().isRequestSurveyButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Asset Details button should be displayed in the header.")
                .that(assetHubPage.getAssetHeaderPage().isAssetDetailsButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Codicils card should be displayed.")
                .that(assetHubPage.getCodicilCard().exists())
                .isTrue();
        assert_().withFailureMessage("Jobs card should be displayed.")
                .that(assetHubPage.getSurveysCard().exists())
                .isTrue();
        assert_().withFailureMessage("Certificates and Records card should be displayed.")
                .that(assetHubPage.getCertificatesAndRecordsCard().exists())
                .isTrue();
        assert_().withFailureMessage("Surveys in Survey planner should be displayed.")
                .that(assetHubPage.getSurveyPlannerPage().getSurveys().size())
                .isGreaterThan(0);
        assert_().withFailureMessage("Notes and Actions in Survey planner should be displayed.")
                .that(assetHubPage.getSurveyPlannerPage().getNotesAndActions().size())
                .isGreaterThan(0);

        // STEP 4-5 - Asset details
        AssetDetailsPage assetDetailsPage = assetHubPage.getAssetHeaderPage()
                .clickRequestSurveyButton()
                .getSelectServicesSubPage()
                .clickCancelButton(AssetHubPage.class)
                .getAssetHeaderPage()
                .clickAssetDetailsButton();

        assert_().withFailureMessage("Export Asset Information card should be displayed.")
                .that(assetDetailsPage.getExportAssetInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Technical Sister Vessels card should be displayed.")
                .that(assetDetailsPage.getTechnicalSisterVesselsCard().exists())
                .isTrue();
        assert_().withFailureMessage("Register Book Sister Vessels card should be displayed.")
                .that(assetDetailsPage.getRegisterBookSisterVesselsCard().exists())
                .isTrue();
        assert_().withFailureMessage("Registry Information card should be displayed.")
                .that(assetDetailsPage.getRegistryInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Principal Dimensions card should be displayed.")
                .that(assetDetailsPage.getPrincipalDimensionsCard().exists())
                .isTrue();
        assert_().withFailureMessage("'Class history, notations and descriptive notes' card should be displayed.")
                .that(assetDetailsPage.getClassHistoryNotationsAndDescriptiveNotesCard().exists())
                .isTrue();
        assert_().withFailureMessage("Equipment Information card should be displayed.")
                .that(assetDetailsPage.getEquipmentInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Registered Owner card should be displayed.")
                .that(assetDetailsPage.getRegisteredOwnerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Group Owner card should be displayed.")
                .that(assetDetailsPage.getGroupOwnerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Operator card should be displayed.")
                .that(assetDetailsPage.getOperatorCard().exists())
                .isTrue();
        assert_().withFailureMessage("Ship Manager card should be displayed.")
                .that(assetDetailsPage.getShipManagerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Technical Manager card should be displayed.")
                .that(assetDetailsPage.getTechnicalManagerCard().exists())
                .isTrue();
        assert_().withFailureMessage("DOC Company card should be displayed.")
                .that(assetDetailsPage.getDocCompanyCard().exists())
                .isTrue();

        // STEP 6 - Codicils and defects
        CodicilsAndDefectsPage codicilsAndDefectsPage = assetDetailsPage.getAssetHeaderPage()
                .clickBackButton(AssetHubPage.class)
                .getCodicilCard()
                .clickViewAllButton();
        assert_().withFailureMessage("Condition of Class should be displayed.")
                .that(codicilsAndDefectsPage.clickConditionsOfClassTab().getCodicilElements().size())
                .isGreaterThan(0);
        assert_().withFailureMessage("Actionable Items should be displayed.")
                .that(codicilsAndDefectsPage.clickActionableItemsTab().getCodicilElements().size())
                .isGreaterThan(0);
        assert_().withFailureMessage("Asset Notes should be displayed.")
                .that(codicilsAndDefectsPage.clickAssetNotesTab().getCodicilElements().size())
                .isGreaterThan(0);
        assert_().withFailureMessage("Defects should be displayed.")
                .that(codicilsAndDefectsPage.clickDefectsTab().getDefectElements().size())
                .isGreaterThan(0);

        //STEP 7 - Jobs
        ServiceHistoryPage serviceHistoryPage = codicilsAndDefectsPage.getAssetHeaderPage()
                .clickBackButton(AssetHubPage.class)
                .getSurveysCard()
                .clickViewAllButton();
        assert_().withFailureMessage("Jobs should be displayed.")
                .that(serviceHistoryPage.getJobElements().size())
                .isGreaterThan(0);

        // STEP 9 - Register Book Sister Assets
        SisterVesselsPage registerBookSisterVesselsPage = serviceHistoryPage
                .getAssetHeaderPage()
                .clickAssetDetailsButton()
                .getRegisterBookSisterVesselsCard()
                .clickViewAllLink();

        assert_().withFailureMessage("Register Book Sister Assets should be displayed.")
                .that(registerBookSisterVesselsPage.getAssetCards().size())
                .isGreaterThan(0);

        // STEP 10 - Technical Sister Assets
        SisterVesselsPage technicalSisterVesselsPage = registerBookSisterVesselsPage.clickBackButton()
                .getTechnicalSisterVesselsCard()
                .clickViewAllLink();

        assert_().withFailureMessage("Technical Sister Assets should be displayed.")
                .that(technicalSisterVesselsPage.getAssetCards().size())
                .isGreaterThan(0);

        // STEP 12 - Export asset information
        ExportAssetListingPage exportAssetListingPage = technicalSisterVesselsPage.clickBackButton()
                .getExportAssetInformationCard()
                .clickExportAssetInformationButton();

        InformationTab informationTab = exportAssetListingPage.clickInformationTab();
        assert_().withFailureMessage("Basic Asset Information radiobutton should be displayed.")
                .that(informationTab.isBasicAssetInfoRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Full Asset Information radiobutton should be displayed.")
                .that(informationTab.isFullAssetInfoRadioButtonDisplayed())
                .isTrue();

        SurveysNotesAndActionsTab surveysNotesAndActionsTab = exportAssetListingPage.surveysNotesAndActionsTab();
        assert_().withFailureMessage("Full Survey Information radiobutton should be displayed.")
                .that(surveysNotesAndActionsTab.isFullSurveyRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("No Survey Information radiobutton should be displayed.")
                .that(surveysNotesAndActionsTab.isNoSurveyRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Basic Survey Information radiobutton should be displayed.")
                .that(surveysNotesAndActionsTab.isBasicSurveyRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Condition of Class checkbox should be displayed.")
                .that(surveysNotesAndActionsTab.isConditionOfClassCheckboxDisplayed())
                .isTrue();
        assert_().withFailureMessage("Asset note checkbox should be displayed.")
                .that(surveysNotesAndActionsTab.isAssetNoteCheckboxDisplayed())
                .isTrue();
        assert_().withFailureMessage("Actionable Item checkbox should be displayed.")
                .that(surveysNotesAndActionsTab.isActionableItemCheckboxDisplayed())
                .isTrue();

        // STEP 8 - Certificates (Test data from Adonia)
        assetHubPage = exportAssetListingPage.clickCloseButton(TopBarPage.class)
                .clickClientTab()
                .getFilterBarPage()
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() ->  new NoSuchElementException(String.format("Expected asset card: %s is not found.", Asset.WITH_CERTIFICATES.getName())))
                .clickViewAssetButton();

        CertificatesAndRecordsPage certificatesAndRecordsPage = assetHubPage.getCertificatesAndRecordsCard().clickViewAllButton();
        assert_().withFailureMessage("Certificates are expected to be displayed.")
                .that(certificatesAndRecordsPage.getCertificateElements().size())
                .isGreaterThan(0);

        // STEP 11 - PMS
        assert_().withFailureMessage("Task Cards are expected to be displayed.")
                .that(certificatesAndRecordsPage.getAssetHeaderPage()
                        .clickBackButton(AssetHubPage.class)
                        .getMpmsCard()
                        .clickCreditingServiceButton()
                        .getTaskCards()
                        .size())
                .isGreaterThan(0);
    }
}
