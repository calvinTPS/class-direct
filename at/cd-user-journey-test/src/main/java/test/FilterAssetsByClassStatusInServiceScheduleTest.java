package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ExportPdfDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import helper.DownloadReportHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterCodicilsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.ConditionsOfClassPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;

public class FilterAssetsByClassStatusInServiceScheduleTest extends BaseTest
{
    @Test(description = "User can filter assets by Class Status in the Service Schedule view of their fleet")
    @Issue("LRCDT-2110")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void filterAssetsByClassStatusInServiceScheduleTest()
    {
        final LocalDate imposedDateFrom = LocalDate.of(2016, 9, 17);
        final LocalDate imposedDateTo = LocalDate.of(2017, 6, 11);

        FilterAssetsPage filterAssetsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .clickFilterButton(FilterAssetsPage.class);

        // STEP 2 - Verify List of Class Status
        assert_().withFailureMessage("All Class Status are expected to be displayed")
                .that(Arrays.stream(ClassStatus.values()).map(ClassStatus::toString).collect(Collectors.toList()))
                .containsAllIn(filterAssetsPage.getClassStatusList());

        // Select a specific class status
        VesselListPage vesselListPage = filterAssetsPage.deSelectClassStatusCheckBoxByStatusName(ClassStatus.ALL.toString())
                .selectClassStatusCheckBoxByStatusName(ClassStatus.CLASSED_PENDING_CLASS_EXEC_APPROVAL.toString())
                .clickSubmitButton(VesselListPage.class);

        vesselListPage.getAssetCards()
                .forEach(e -> assert_().withFailureMessage(
                        String.format("Assets with the selected filter '%s' should be displayed", ClassStatus.CLASSED_PENDING_CLASS_EXEC_APPROVAL.toString()))
                        .that(e.getClassStatus())
                        .isEqualTo(ClassStatus.CLASSED_PENDING_CLASS_EXEC_APPROVAL.toString()));

        // STEP 3-5 - View asset and navigate to Condition of Class
        CodicilsAndDefectsPage codicilsAndDefectsPage = vesselListPage.getFilterBarPage()
                .setSearchTextBox(LAURA.getName())
                .selectSearchDropDown(AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton();
        ConditionsOfClassPage conditionsOfClassPage = codicilsAndDefectsPage.clickConditionsOfClassTab();

        // STEP 6-7 - Filter CoC by Imposed Date
        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setImposeDateFromDatepicker(imposedDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setImposeDateToDatepicker(imposedDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .forEach(e-> assert_().withFailureMessage(String.format("Imposed Date of %s should be in between %s and %s.", e.getTitle(), imposedDateFrom, imposedDateTo))
                        .that(LocalDate.parse(e.getImposedDate(), FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(imposedDateFrom, imposedDateTo)));

        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .clickClearFiltersButton();

        // STEP 8 - Filter CoC by Status
        FilterCodicilsPage filterCodicilsPage = conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class);

        assert_().withFailureMessage("By default, selected status should be Open")
                .that(filterCodicilsPage.isOpenRadioButtonSelected())
                .isTrue();
        assert_().withFailureMessage("Open status should be displayed.")
                .that(filterCodicilsPage.isOpenRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Closed status should be displyed.")
                .that(filterCodicilsPage.isClosedRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Cancelled status should be displayed.")
                .that(filterCodicilsPage.isCancelledRadioButtonDisplayed())
                .isTrue();

        // STEP 9 - Filter by Closed status
        filterCodicilsPage.selectClosedRadioButton()
                .clickSubmitButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .forEach(e-> assert_().withFailureMessage("Codicils should be filtered by Closed status")
                        .that(e.getStatus())
                        .isEqualTo("Closed"));

        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .clickClearFiltersButton();

        // STEP 10-11 - Filter CoC by Category
        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .deselectAllCheckBox()
                .selectStatutoryCheckBox()
                .clickSubmitButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .forEach(e-> assert_().withFailureMessage("Codicils should be filtered by Statutory category")
                        .that(e.getCategory())
                        .isEqualTo("Statutory"));

        // STEP 12 - Export CoC
        codicilsAndDefectsPage.clickExportButton();
        ExportPdfDto exportPdfDto = new ExportPdfDto();
        exportPdfDto.setCode(LAURA.getAssetCode());
        String exportConditionOfClassToken = new Export().getExportConditionOfClassToken(exportPdfDto);
        assert_().withFailureMessage("Condition of class list should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(exportConditionOfClassToken))
                .isNotEmpty();
    }
}
