package test;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import helper.TestDateHelper;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import main.serviceschedule.graphical.GraphicalView;
import main.serviceschedule.graphical.ServiceCodicilOverlayPage;
import main.serviceschedule.graphical.element.DataElement;
import main.serviceschedule.graphical.element.TooltipElement;
import main.serviceschedule.tabular.base.AssetTable;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import typifiedelement.Timeline;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.SERVICE_SCHEDULE;

public class GraphicalViewVerifyDueStatusAndTimelineFilterTest extends BaseTest
{
    private LocalDate newEarliestDate, newLatestDate;

    @Test(description = "Verify due status icons (description, tooltip, detailed view) and View assets in different views - tabular/graphical")
    @Issue("LRCDT-1327")
    @Features(SERVICE_SCHEDULE)
    public void graphicalViewVerifyDueStatusAndTimelineFilterTest()
    {
        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .setSearchTextBox("ura")
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class);

        //STEP 3
        assert_().withFailureMessage("By default, tabular view should be selected.")
                .that(serviceSchedulePage.getScheduleElement().isTabularViewSelected())
                .isTrue();

        //STEP 4
        Timeline timeline = serviceSchedulePage.getScheduleElement().getServiceTimeline();
        assert_().withFailureMessage("Today label should be present in the timescale.")
                .that(timeline.isTodayDisplayed())
                .isTrue();

        //STEP 5
        LocalDate earliestDate = timeline.getSelectedStartDate();

        serviceSchedulePage.getTabularView()
                .getAssetTables()
                .forEach(assetTable ->
                    assertEarliestDueDateInTabularView(assetTable, earliestDate.minusDays(1))
                );

        //STEP 6
        LocalDate latestDate = timeline.getSelectedEndDate();

        assert_().withFailureMessage("Month for latest date should be the same as earliest date.")
                .that(latestDate.getMonth())
                .isEqualTo(earliestDate.getMonth());
        assert_().withFailureMessage("Year for latest date should be 7 years greater.")
                .that(latestDate.getYear())
                .isEqualTo(earliestDate.getYear() + 7);

        //STEP 7
        newEarliestDate = earliestDate.plusMonths(6);
        timeline.selectStartDate(newEarliestDate);

        assert_().withFailureMessage("Earliest date should be changed in the time scale.")
                .that(timeline.getSelectedStartDate())
                .isEqualTo(newEarliestDate);

        //STEP 8
        newLatestDate = latestDate.minusMonths(6);
        timeline.selectEndDate(newLatestDate);

        assert_().withFailureMessage("Latest date should be changed in the time scale.")
                .that(timeline.getSelectedEndDate())
                .isEqualTo(newLatestDate);

        //STEP 9
        serviceSchedulePage.getFilterBarPage()
                .setSearchTextBox(LAURA.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .forEach(this::assertAssetTableDueDatesInTabularView);

        GraphicalView graphicalView = serviceSchedulePage.clickGraphicalViewTab();

        // Stuck here.. added this extra step - work around for the StaleElementReferenceException =c
        serviceSchedulePage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class);

        graphicalView.getAssetTables()
                .forEach(assetTable ->
                {
                    assetTable.getCodicils()
                            .forEach(this::assertCodicilsInGraphicalView);

                    assetTable.getServices()
                            .forEach(this::assertServicesInGraphicalView);
                });
    }


    private void assertEarliestDueDateInTabularView(AssetTable assetTable, LocalDate earliestDate)
    {
        assetTable.expand().getServiceTables()
                .forEach(serviceTable ->
                        serviceTable.expand().getServices().forEach(serviceElement ->
                                assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of services")
                                        .that(LocalDate.parse(serviceElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS).isAfter(earliestDate))
                                        .isTrue())
                );
        if(assetTable.expand().getCodicilTable().exists())
        {
            assetTable.getCodicilTable()
                    .expand()
                    .getCodicils()
                    .forEach(codicilElement ->
                            assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of services")
                                    .that(LocalDate.parse(codicilElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS).isAfter(earliestDate))
                                    .isTrue());
        }

    }

    private void assertAssetTableDueDatesInTabularView(AssetTable assetTable)
    {
        assetTable.expand().getServiceTables()
                .forEach(serviceTable ->
                        serviceTable.expand().getServices().forEach(serviceElement ->
                        {
                            LocalDate dueDate = LocalDate.parse(serviceElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS);
                            assert_().withFailureMessage("Due dates of services should be in between the earliest and latest date.")
                                    .that(TestDateHelper.toDate(dueDate))
                                    .isIn(Range.closed(TestDateHelper.toDate(newEarliestDate), TestDateHelper.toDate(newLatestDate)));
                        })
                );

        if(assetTable.expand().getCodicilTable().exists())
        {
            assetTable.getCodicilTable()
                    .expand()
                    .getCodicils()
                    .forEach(codicilElement ->
                    {
                        LocalDate dueDate = LocalDate.parse(codicilElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS);
                        assert_().withFailureMessage("Due dates of codicils should be in between the earliest and latest date.")
                                .that(TestDateHelper.toDate(dueDate))
                                .isIn(Range.closed(TestDateHelper.toDate(newEarliestDate), TestDateHelper.toDate(newLatestDate)));
                    });
        }
    }

    private void assertCodicilsInGraphicalView(DataElement e)
    {
        int dataCount = e.getDataCount();
        if(dataCount == 1)
        {
            assertTooltipElement(e.getTooltip());
        }

        ServiceCodicilOverlayPage serviceCodicilOverlayPage = e.clickDataElement();
        assert_().withFailureMessage("Codicils count should match the number of displayed codicils in the overlay.")
                .that(serviceCodicilOverlayPage
                        .getCodicilTables()
                        .stream()
                        .mapToInt(c -> c.expand().getCodicils().size())
                        .sum())
                .isEqualTo(dataCount);

        serviceCodicilOverlayPage
                .getCodicilTables()
                .forEach(c ->
                        c.getCodicils().forEach(s ->
                        {
                            assert_().withFailureMessage("Name should be displayed")
                                    .that(s.getCodicilName().isEmpty())
                                    .isFalse();
                            assert_().withFailureMessage("Due date should be displayed")
                                    .that(s.getDueDate().isEmpty())
                                    .isFalse();
                            assert_().withFailureMessage("Imposed date should be displayed")
                                    .that(s.getImposedDate().isEmpty())
                                    .isFalse();
                            assert_().withFailureMessage("Due Status should be displayed")
                                    .that(s.getDueStatus().isEmpty())
                                    .isFalse();
                        })
                );
        serviceCodicilOverlayPage.clickCloseButton();
    }

    private void assertServicesInGraphicalView(DataElement e)
    {
        if(e.getDataCount() == 1)
        {
            assertTooltipElement(e.getTooltip());
        }

        ServiceCodicilOverlayPage serviceCodicilOverlayPage = e.clickDataElement();
        assert_().withFailureMessage("Services count should match the number of displayed services in the overlay.")
                .that(serviceCodicilOverlayPage
                        .getServiceTables()
                        .stream()
                        .mapToInt(c -> c.expand().getServices().size())
                        .sum())
                .isEqualTo(e.getDataCount());

        serviceCodicilOverlayPage
                .getServiceTables()
                .forEach(c ->
                {
                    assert_().withFailureMessage("Services should be listed by product.")
                            .that(c.getProductName().isEmpty())
                            .isFalse();
                    c.getServices().forEach(s ->
                    {
                        assert_().withFailureMessage("Name should be displayed")
                                .that(s.getServiceName().isEmpty())
                                .isFalse();
                        assert_().withFailureMessage("Due date should be displayed")
                                .that(s.getDueDate().isEmpty())
                                .isFalse();
                        assert_().withFailureMessage("Assigned date should be displayed")
                                .that(s.getAssignedDate().isEmpty())
                                .isFalse();
                        assert_().withFailureMessage("Due Status should be displayed")
                                .that(s.getDueStatus().isEmpty())
                                .isFalse();
                    });
                });
        serviceCodicilOverlayPage.clickCloseButton();
    }

    private void assertTooltipElement(TooltipElement tooltipElement)
    {
        assert_().withFailureMessage("Service/Codicil name should be displayed in the tooltip.")
                .that(tooltipElement.getServiceCodicilName())
                .isNotEmpty();
        assert_().withFailureMessage("Due status should be displayed in the tooltip.")
                .that(tooltipElement.getDueStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Due status definition should be displayed in the tooltip.")
                .that(tooltipElement.getDueStatusDefinition())
                .isNotEmpty();
        assert_().withFailureMessage("Due date should be displayed in the tooltip.")
                .that(tooltipElement.getDueDate())
                .isNotEmpty();
    }
}

