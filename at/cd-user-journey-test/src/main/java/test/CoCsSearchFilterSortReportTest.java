package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import constant.ClassDirect;
import helper.DownloadReportHelper;
import main.common.element.CodicilElement;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterCodicilsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.ConditionsOfClassPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.ConditionOfClassDetailsPage;
import main.common.element.DefectElement;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.RepairDetailsOverlayPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.element.RepairElement;
import main.viewasset.codicils.codicilsanddefects.sub.defects.details.DefectDetailsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.codicilSearchOptions;
import static constant.ClassDirect.defaultCodicilStatus;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;

public class CoCsSearchFilterSortReportTest extends BaseTest
{
    @Test(description = "Automation_UJ_SingleAssetCs and DsViewCoCs_FSR_SearchSortFilter_AssociatedDefectRepair")
    @Issue("LRCDT-1023")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void cocsSearchFilterSortReportTest()
    {
        final String searchKeyword = "Missing ship's cat";
        final int[] searchByNameReturnedCounts = {1, 12};
        final String categoryFilter = "Class";
        final LocalDate imposedDateFrom = LocalDate.of(2016, 9, 17);
        final LocalDate imposedDateTo = LocalDate.of(2017, 6, 11);
        final LocalDate dueDateFrom = LocalDate.of(2017, 5, 1);
        final LocalDate dueDateTo = LocalDate.of(2017, 6, 26);

        // STEP 1-3: View conditions of class
        CodicilsAndDefectsPage codicilsAndDefectsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton();
        ConditionsOfClassPage conditionsOfClassPage = codicilsAndDefectsPage.clickConditionsOfClassTab();

        // STEP 4: Verify that only CoCs with Open status are displayed
        assert_().withFailureMessage("Verify default Open status: CoCs with different status than Open are displayed.")
                .that(conditionsOfClassPage.getCodicilElements().stream().allMatch(i -> i.getStatus().equals(defaultCodicilStatus)))
                .isTrue();

        // STEP 5: Verify CoC details on 'Conditions of Class' page
        CodicilElement conditionOfClass = conditionsOfClassPage.getCodicilElements()
                .stream()
                .filter(c -> c.getTitle().equals(searchKeyword))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("No such coc with title %s is found", searchKeyword)));
        assert_().withFailureMessage("CoC does not have Title displayed.")
                .that(conditionOfClass.getTitle().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Category displayed.", conditionOfClass.getCategory()))
                .that(conditionOfClass.getCategory().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Imposed date displayed.", conditionOfClass.getImposedDate()))
                .that(conditionOfClass.getImposedDate().length() > 0 && conditionOfClass.isImposedDateDisplayed())
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Due date displayed.", conditionOfClass.getTitle()))
                .that(conditionOfClass.getDueDate().length() > 0 && conditionOfClass.isDueDateDisplayed())
                .isTrue();

        // STEP 6: Verify CoC details on 'Condition of Class Details' page
        ConditionOfClassDetailsPage conditionOfClassDetailsPage = conditionOfClass.clickArrowButton(ConditionOfClassDetailsPage.class);
        assert_().withFailureMessage("CoC does not have Title displayed.")
                .that(conditionOfClassDetailsPage.getTitle().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Status displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getStatus().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Category displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getCategory().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Imposed Date displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getImposedDate().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Due Date displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getDueDate().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Job Number displayed.", conditionOfClassDetailsPage.getTitle()))
                .that(conditionOfClassDetailsPage.getJobNumber().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("CoC '%s' does not have Description displayed.", conditionOfClassDetailsPage.getTitle()))
              .that(conditionOfClassDetailsPage.getDescription().length() > 0)
              .isTrue();

        assert_().withFailureMessage("No Associated Class Defects are displayed.")
                .that(conditionOfClassDetailsPage.getAssociatedClassDefects().size())
                .isGreaterThan(0);

        // STEP 7: Verify associated Defects
        DefectElement associatedClassDefect = conditionOfClassDetailsPage.getAssociatedClassDefects().get(0);
        assert_().withFailureMessage("Defect with index 0 does not have Defect Title displayed.")
                .that(associatedClassDefect.getDefectTitle().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("Defect '%s' does not have Defect Category displayed.", associatedClassDefect.getDefectTitle()))
                .that(associatedClassDefect.getDefectCategory().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("Defect '%s' does not have Date Occurred displayed.", associatedClassDefect.getDefectTitle()))
                .that(associatedClassDefect.getDateOccurred().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("Defect '%s' does not have Status displayed.", associatedClassDefect.getDefectTitle()))
                .that(associatedClassDefect.getStatus().length() > 0)
                .isTrue();

        // STEP 8: Verify navigation to Defect Details page
        DefectDetailsPage defectDetailsPage = associatedClassDefect.clickArrowButton();

        // STEP 9: Verify navigation back to Condition of Class Details page
        conditionOfClassDetailsPage = codicilsAndDefectsPage.getAssetHeaderPage().clickBackButton(ConditionOfClassDetailsPage.class);

        // STEP 10: Find CoC with associated Repairs
        conditionOfClassDetailsPage = conditionOfClassDetailsPage.getAssetHeaderPage()
                .clickBackButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .stream()
                .filter(c -> c.getTitle().equals(searchKeyword))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("No such coc with title %s is found", searchKeyword)))
                .clickArrowButton(ConditionOfClassDetailsPage.class);

        // STEP 10 Continue: Verify associated Repairs
        RepairElement repairElement = conditionOfClassDetailsPage.getAssociatedRepairs().get(0);
        assert_().withFailureMessage("Repair with index 0 does not have Repair Title displayed.")
                .that(repairElement.getRepairTitle().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("Repair '%s' does not have Repair Type displayed.", repairElement.getRepairTitle()))
                .that(repairElement.getRepairType().length() > 0)
                .isTrue();

        assert_().withFailureMessage(String.format("Repair '%s' does not have Date Added displayed.", repairElement.getRepairTitle()))
              .that(repairElement.getDateAdded().length() > 0)
              .isTrue();

        // STEP 11: Repair details in overlay
        RepairDetailsOverlayPage repairDetailsOverlayPage = repairElement.clickArrowButton();
        assert_().withFailureMessage("Repair Title should be displayed in the overlay page.")
                .that(repairDetailsOverlayPage.getRepairTitle())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Repair '%s' does not have Repair Type displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getRepairType())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Repair '%s' does not have Repair Action displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getRepairAction())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Repair '%s' does not have Items displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getItems())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Repair '%s' does not have Description displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getDescription())
                .isNotEmpty();
        assert_().withFailureMessage(String.format("Repair '%s' does not have Date Added displayed in the overlay page.", repairDetailsOverlayPage.getRepairTitle()))
                .that(repairDetailsOverlayPage.getDateAdded())
                .isNotEmpty();

        // STEP 12: Close overlay page
        repairDetailsOverlayPage.clickCloseButton();

        // STEP 13: Download FSR
        conditionOfClassDetailsPage.clickDownloadFinalServiceReportIcon();
        ReportExportDto reportExportDto = new ReportExportDto();
        reportExportDto.setJobId(ClassDirect.JobReport.FSR.getReportId());
        reportExportDto.setReportId(ClassDirect.JobReport.FSR.getReportId());

        assert_().withFailureMessage("Final Service Report should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(new Export().getExportReportToken(reportExportDto)))
                .isNotEmpty();

        // STEP 14-15: Navigate back to Conditions of Class page and verify search options
        conditionsOfClassPage = conditionOfClassDetailsPage.getAssetHeaderPage().clickBackButton(ConditionsOfClassPage.class);
        assert_().withFailureMessage("Search options are not complete.")
                .that(conditionsOfClassPage.getFilterBarPage()
                        .getSearchOptionList()
                        .stream()
                        .allMatch(o -> Arrays.asList(codicilSearchOptions).contains(o)))
                .isTrue();

        // STEP 16: Verify searching by Name
        conditionsOfClassPage.getFilterBarPage()
                .closeSearchOptionList()
                .selectSearchDropDown(codicilSearchOptions[0])
                .setSearchTextBox(searchKeyword);

        assert_().withFailureMessage(String.format("No CoC has been found for '%s' search keyword.", searchKeyword))
                .that(conditionsOfClassPage.getCodicilElements().size())
                .isEqualTo(searchByNameReturnedCounts[0]);

        // STEP 17: Verify clearing the search text box
        conditionsOfClassPage.getFilterBarPage()
                .clickClearButtonInSearchTextBox();

        assert_().withFailureMessage("Not all CoCs are displayed after clearing Search Text Box.")
                .that(conditionsOfClassPage.getCodicilElements().size())
                .isEqualTo(searchByNameReturnedCounts[1]);

        // STEP 18: Verify visibility of Status filter criteria
        FilterCodicilsPage filterCodicilsPage = conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)              ;

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Open is not displayed.")
                .that(filterCodicilsPage.isOpenRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Closed is not displayed.")
                .that(filterCodicilsPage.isClosedRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Cancelled is not displayed.")
                .that(filterCodicilsPage.isCancelledRadioButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Status filter Open is not selected by default.")
                .that(filterCodicilsPage.isOpenRadioButtonSelected())
                .isTrue();

        // STEP 19: Verify filtering by Cancelled status
        conditionsOfClassPage = filterCodicilsPage.selectCancelledRadioButton()
                .clickSubmitButton(ConditionsOfClassPage.class);

        for (CodicilElement codicil : conditionsOfClassPage.getCodicilElements())
        {
            assert_().withFailureMessage(String.format("Filter by Cancelled status: Item '%s' does not have Cancelled status.", codicil.getTitle()))
                    .that(codicil.getStatus().equals("Cancelled"))
                    .isTrue();
        }

        // STEP 20: Verify visibility of Category filter criteria
        filterCodicilsPage = conditionsOfClassPage.getFilterBarPage().clickFilterButton(FilterCodicilsPage.class);
        assert_().withFailureMessage("Filter codicils dropdown: Category filter All is not displayed.")
                .that(filterCodicilsPage.isAllCheckBoxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Class is not displayed.")
                .that(filterCodicilsPage.isClassCheckBoxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Statutory is not displayed.")
                .that(filterCodicilsPage.isStatutoryCheckBoxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter All is not selected.")
                .that(filterCodicilsPage.isAllCheckBoxSelected())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Class is not selected.")
                .that(filterCodicilsPage.isClassCheckBoxSelected())
                .isTrue();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Statutory is not selected.")
                .that(filterCodicilsPage.isStatutoryCheckBoxSelected())
                .isTrue();

        // STEP 21: Verify filtering by Category
        filterCodicilsPage.clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .deselectClassCheckBox();
        assert_().withFailureMessage("Filter codicils dropdown: Category filter All is selected after deselecting Class category.")
                .that(filterCodicilsPage.isAllCheckBoxSelected())
                .isFalse();

        assert_().withFailureMessage("Filter codicils dropdown: Category filter Statutory is not selected after deselecting Class category.")
                .that(filterCodicilsPage.isStatutoryCheckBoxSelected())
                .isTrue();

        conditionsOfClassPage = filterCodicilsPage.clickSubmitButton(ConditionsOfClassPage.class);
        assert_().withFailureMessage(String.format("CoCs with deselected category '%s' are displayed.", categoryFilter))
                .that(conditionsOfClassPage.getCodicilElements()
                        .stream()
                        .map(CodicilElement::getCategory)
                        .anyMatch(o -> o.equals(categoryFilter)))
                .isFalse();

        // STEP 22-23: Filter by imposed date
        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setImposeDateFromDatepicker(imposedDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setImposeDateToDatepicker(imposedDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .forEach(e-> assert_().withFailureMessage(String.format("Imposed Date of %s should be in between %s and %s.", e.getTitle(), imposedDateFrom, imposedDateTo))
                        .that(LocalDate.parse(e.getImposedDate(), FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(imposedDateFrom, imposedDateTo)));

        // STEP 24-25: Filter by Due date
        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setDueDateFromDatepicker(dueDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDateToDatepicker(dueDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ConditionsOfClassPage.class)
                .getCodicilElements()
                .forEach(e-> assert_().withFailureMessage(String.format("Due Date of %s should be in between %s and %s.", e.getTitle(), dueDateFrom, dueDateTo))
                        .that(LocalDate.parse(e.getDueDate(), FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(dueDateFrom, dueDateTo)));
    }
}
