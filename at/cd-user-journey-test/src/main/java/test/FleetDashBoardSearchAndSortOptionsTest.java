package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect.AssetSearchOptions;
import constant.ClassDirect.AssetSortByOptions;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Assets;
import service.ReferenceData;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FleetDashBoardSearchAndSortOptionsTest extends BaseTest
{
    @Test(description = "Search and Sort options")
    @Issue("LRCDT-595")
    @Features(FLEET_DASHBOARD)
    public void fleetDashBoardSearchAndSortOptionsTest()
    {
        final String defaultHelperText = "Type here to search";
        final String assetNameSearchString = "asset";
        final String imoNumberSearchString = "65398";
        final String clientSearchString = "Cheoy Lee Shipyards Limited";
        final String shipBuilderSearchString = "Levis";
        final String yardNumberSearchString = "200";
        final String IncorrectSortingErrorMessage = "Asset cards should be ordered by %s.";
        final String SearchCategoryErrorMessage = "%s should be displayed as selected category.";

        Assets assets = new Assets();

        FilterBarPage filterBarPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .getPageReference(FilterBarPage.class);

        VesselListPage vesselListPage = filterBarPage
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class);

        //STEP 3
        assert_().withFailureMessage("Default helper text of Search text field should be 'Search here'")
                .that(defaultHelperText)
                .isEqualTo(filterBarPage.getSearchHelperText());

        //STEP 4 - SEARCH options
        List<String> lstCategorySearchOptions = filterBarPage.getSearchOptionList();
        for (AssetSearchOptions strCategory : AssetSearchOptions.values())
        {
            assert_().withFailureMessage(strCategory.toString() + " should be in the category list")
                    .that(lstCategorySearchOptions.stream().anyMatch(e -> e.equals(strCategory.toString())))
                    .isTrue();
        }
        //STEP 5 - default category search
        assert_().withFailureMessage(AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString() + " should be displayed as default selected Category")
                .that(AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString())
                .isEqualTo(filterBarPage.closeSearchOptionList().getSelectedSearchOption());

        //STEP 6 - Search by Asset Name
        filterBarPage.setSearchTextBox(assetNameSearchString);
        List<AssetCard> lstFilteredAssetNames = vesselListPage.getAssetCards();
        assert_().withFailureMessage("Asset Name search criteria should return results")
                .that(lstFilteredAssetNames.size() > 0)
                .isTrue();
        lstFilteredAssetNames.forEach(e -> assert_().withFailureMessage("Asset names containing the search string should be displayed")
                .that(e.getAssetName().toLowerCase().contains(assetNameSearchString))
                .isTrue());

        //STEP 8 - Search by IMO Number
        filterBarPage.setSearchTextBox(imoNumberSearchString).selectSearchDropDown(AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString());
        List<AssetCard> lstFilteredImoNumber = vesselListPage.getAssetCards();
        assert_().withFailureMessage("IMO Number search criteria should return results")
                .that(lstFilteredImoNumber.size() > 0)
                .isTrue();
        lstFilteredImoNumber.forEach(e -> assert_().withFailureMessage("IMO Number containing the search string should be displayed")
                .that(e.getImoNumber().contains(imoNumberSearchString))
                .isTrue());

        //STEP 9 - Search by Client
        filterBarPage.selectSearchDropDown(AssetSearchOptions.CLIENT.toString());
        assert_().withFailureMessage(String.format(SearchCategoryErrorMessage, AssetSearchOptions.CLIENT.toString()))
                .that(AssetSearchOptions.CLIENT.toString())
                .isEqualTo(filterBarPage.getSelectedSearchOption());
        //STEP 10
        filterBarPage.setSearchTextBox(clientSearchString);
        AssetQueryHDto clientAssetQueryHDto = new AssetQueryHDto();
        clientAssetQueryHDto.setClientName(String.format("*%s*", clientSearchString));

        assert_().withFailureMessage("Asset Cards with client name in FE should match with that of BE")
                .that(assets.getAssets(clientAssetQueryHDto).size())
                .isEqualTo(vesselListPage.getShowingXofYPage().getTotal());

        //STEP 11 - Search by Ship builder
        filterBarPage.selectSearchDropDown(AssetSearchOptions.SHIP_BUILDER.toString());
        assert_().withFailureMessage(String.format(SearchCategoryErrorMessage, AssetSearchOptions.SHIP_BUILDER.toString()))
                .that(AssetSearchOptions.SHIP_BUILDER.toString())
                .isEqualTo(filterBarPage.getSelectedSearchOption());
        //STEP 12
        filterBarPage.setSearchTextBox(shipBuilderSearchString);
        AssetQueryHDto builderAssetQueryHDto = new AssetQueryHDto();
        builderAssetQueryHDto.setBuilder(String.format("*%s*", shipBuilderSearchString));

        assert_().withFailureMessage("Asset Cards in FE should match with that of BE")
                .that(assets.getAssets(builderAssetQueryHDto).size())
                .isEqualTo(vesselListPage.getShowingXofYPage().getTotal());

        //STEP 13 - Search by Yard Number
        filterBarPage.selectSearchDropDown(AssetSearchOptions.YARD_NUMBER.toString());
        assert_().withFailureMessage(String.format(SearchCategoryErrorMessage, AssetSearchOptions.YARD_NUMBER.toString()))
                .that(AssetSearchOptions.YARD_NUMBER.toString())
                .isEqualTo(filterBarPage.getSelectedSearchOption());
        //STEP 14
        filterBarPage.setSearchTextBox(yardNumberSearchString);
        AssetQueryHDto yardAssetQueryHDto = new AssetQueryHDto();
        yardAssetQueryHDto.setYardNumber(String.format("*%s*", yardNumberSearchString));
        List<AssetHDto> yardAssets = assets.getAssets(yardAssetQueryHDto);
        assert_().withFailureMessage("Asset Cards with yard number in FE should match with that of BE")
                .that(yardAssets.size())
                .isEqualTo(vesselListPage.getShowingXofYPage().getTotal());

        //STEP 15-16 - SORT options
        List<String> lstSortByOptions = filterBarPage.getSortByOptionList();
        for (AssetSortByOptions strSortBy : AssetSortByOptions.values())
        {
            assert_().withFailureMessage(strSortBy.toString() + " should be in the SortBy list")
                    .that(lstSortByOptions.stream().anyMatch(e -> e.equals(strSortBy.toString())))
                    .isTrue();
        }
        //STEP 17 - Default sorting
        assert_().withFailureMessage(AssetSortByOptions.ASSET_NAME.toString() + " should be displayed as default selected SortBy")
                .that(AssetSortByOptions.ASSET_NAME.toString())
                .isEqualTo(filterBarPage.closeSortByOptionList().getSelectedSortByOption());

        //sort by asset name
        filterBarPage.clickClearButtonInSearchTextBox().selectSortByDropDown(AssetSortByOptions.ASSET_NAME.toString());
        List<String> assetNameList = vesselListPage
                .getAssetCards()
                .stream()
                .map(e-> e.getAssetName().toLowerCase())
                .collect(Collectors.toList());

        assert_().withFailureMessage(String.format(IncorrectSortingErrorMessage, AssetSortByOptions.ASSET_NAME.toString()))
                .that(assetNameList)
                .isOrdered();

        //sort by class status - should be sorted according to reference data
        filterBarPage.selectSortByDropDown(AssetSortByOptions.CLASS_STATUS.toString());
        List<ClassStatusHDto> classStatusHDtoList = new ReferenceData().getClassStatus();
        List<Long> classStatusList = vesselListPage
                .getAssetCards()
                .stream()
                .map(e -> classStatusHDtoList.stream()
                        .filter(s-> s.getName().equals(e.getClassStatus()))
                        .findFirst()
                        .orElseThrow(() -> new NoSuchElementException(String.format("'%s' is not found in the reference data.", e.getClassStatus())))
                        .getId())
                .collect(Collectors.toList());

        assert_().withFailureMessage(String.format(IncorrectSortingErrorMessage, AssetSortByOptions.CLASS_STATUS.toString()))
                .that(classStatusList)
                .isOrdered();

        //sort by asset type
        filterBarPage.selectSortByDropDown(AssetSortByOptions.ASSET_TYPE.toString());
        List<String> assetTypeList = vesselListPage
                .getAssetCards()
                .stream()
                .map(e -> e.getAssetType().toLowerCase())
                .collect(Collectors.toList());

        assert_().withFailureMessage(String.format(IncorrectSortingErrorMessage, AssetSortByOptions.ASSET_TYPE.toString()))
                .that(assetTypeList)
                .isOrdered();

        //sort by flag
        filterBarPage.selectSortByDropDown(AssetSortByOptions.FLAG.toString());
        List<String> flagList = vesselListPage
                .getAssetCards()
                .stream()
                .map(e-> e.getFlagName().toLowerCase())
                .collect(Collectors.toList());

        assert_().withFailureMessage(String.format(IncorrectSortingErrorMessage, AssetSortByOptions.FLAG.toString()))
                .that(flagList)
                .isOrdered();

        //sort by date of build
        filterBarPage.selectSortByDropDown(AssetSortByOptions.DATE_OF_BUILD.toString());
        List<Date> dateOfBuildList = vesselListPage
                .getAssetCards()
                .stream()
                .map(AssetCard::getDateOfBuild)
                .collect(Collectors.toList());

        assert_().withFailureMessage(String.format(IncorrectSortingErrorMessage, AssetSortByOptions.DATE_OF_BUILD.toString()))
                .that(dateOfBuildList)
                .isOrdered();

        //sort by Yard number
        filterBarPage.selectSortByDropDown(AssetSortByOptions.YARD_NUMBER.toString());
        List<String> yardNumberList = vesselListPage
                .getAssetCards()
                .stream()
                .map(e -> yardAssets.stream()
                            .filter(s -> s.getName() != null && s.getName().equals(e.getAssetName()))
                            .findFirst()
                            .orElseThrow(() -> new NoSuchElementException("Expected asset not found in Backend"))
                            .getYardNumber()
                            .toLowerCase())
                .collect(Collectors.toList());

        assert_().withFailureMessage(String.format(IncorrectSortingErrorMessage, AssetSortByOptions.YARD_NUMBER.toString()))
                .that(yardNumberList)
                .isOrdered();
    }
}
