package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.adduser.eor.AddEorPermissionPage;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.eor.EorAssetCard;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Credentials.SHIP_BUILDER_USER;
import static constant.ClassDirect.Asset.LAURA;
import static constant.Epic.USER_MANAGEMENT;

public class AddViewEditEorAccessToAssetTest extends BaseTest
{
    @Test(description = "As an Admin user, the user can add, view and edit EoR access to an asset for a client and ship builder user")
    @Issue("LRCDT-2172")
    @Features(USER_MANAGEMENT)
    public void addViewEditEorAccessToAssetTest()
    {
        final String assetExpiryDate = "31 Dec 2025";
        final String newAssetExpiryDate = "31 Dec 2030";

        AdministrationDetailPage administrationDetailPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(SHIP_BUILDER_USER.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User with email %s is not found", SHIP_BUILDER_USER.getEmail())))
                .clickFurtherMoreDetailsButton();

        // STEP 3-4 - Add EOR access
        AddEorPermissionPage addEorPermissionPage = administrationDetailPage.getExhibitionOfRecordsCard()
                .clickAddEorAccessButton()
                .setAssetExpiryDate(assetExpiryDate)
                .selectThicknessMeasurementReports()
                .setImoNumber(LAURA.getImoNumber());

        assert_().withFailureMessage(String.format("Asset card with %s should be displayed.", LAURA.getImoNumber()))
                .that(addEorPermissionPage.isAssetCardDisplayed())
                .isTrue();
        assert_().withFailureMessage("'Add Exhibition of Records asset' button should be enabled.")
                .that(addEorPermissionPage.isAddExhibitionOfRecordsAssetEnabled())
                .isTrue();

        // STEP 5 - Confirm
        List<EorAssetCard> eorAssetCardList = addEorPermissionPage.clickAddExhibitionRecordsAssetButton()
                .getFooterPage()
                .clickConfirmButton(AdministrationDetailPage.class)
                .getExhibitionOfRecordsCard()
                .getEorAssetCards();

        assert_().withFailureMessage(String.format("EOR Asset with %s is not displayed or not successfully added.", LAURA.getImoNumber()))
                .that(eorAssetCardList.stream().anyMatch(e -> e.getImoNumber().equals(LAURA.getImoNumber())))
                .isTrue();

        // STEP 6 - View EOR access
        EorAssetCard eorAssetCard = eorAssetCardList.stream()
                .filter(e -> e.getImoNumber().equals(LAURA.getImoNumber()))
                .findFirst().get();

        assert_().withFailureMessage(String.format("IMO Number %s is expected to be displayed.", LAURA.getImoNumber()))
                .that(eorAssetCard.getImoNumber())
                .isNotEmpty();
        assert_().withFailureMessage("Asset name is expected to be displayed.")
                .that(eorAssetCard.getAssetName())
                .isNotEmpty();
        assert_().withFailureMessage("Expiration Date is expected to be displayed.")
                .that(eorAssetCard.getExpirationDate())
                .isNotEmpty();
        assert_().withFailureMessage("Client Code is expected to be displayed.")
                .that(eorAssetCard.getClientCode())
                .isNotEmpty();

        // STEP 7-8 - Edit EOR access
        eorAssetCard = eorAssetCard.clickUpdateEorExpirationDateButton()
                .setNewAssetExpiryDate(newAssetExpiryDate)
                .clickConfirmButton()
                .getExhibitionOfRecordsCard()
                .getEorAssetCards()
                .stream()
                .filter(e -> e.getImoNumber().equals(LAURA.getImoNumber()))
                .findFirst().get();

        assert_().withFailureMessage("New expiry date of EOR Asset should be successfully saved.")
                .that(eorAssetCard.getExpirationDate())
                .isEqualTo(newAssetExpiryDate);

        // STEP 9 - Remove EOR access
        administrationDetailPage.getExhibitionOfRecordsCard()
                .getEorAssetCards()
                .stream()
                .filter(e -> e.getImoNumber().equals(LAURA.getImoNumber()))
                .findFirst()
                .get()
                .clickRemoveEorPermissionButton()
                .clickConfirmButton()
                .getAccountSettingsCard()
                .clickAmendAccountExpiryButton()
                .clickCancelButton();

        assert_().withFailureMessage(String.format("EOR access for %s should be successfully removed.", LAURA.getName()))
                .that(administrationDetailPage.getExhibitionOfRecordsCard()
                        .getEorAssetCards()
                        .stream()
                        .anyMatch(e -> e.getImoNumber().equals(LAURA.getImoNumber())))
                .isFalse();
    }
}
