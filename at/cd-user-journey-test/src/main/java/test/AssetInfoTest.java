package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.assetdetail.AssetDetailsPage;
import main.common.AssetHeaderPage;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import main.viewasset.certificates.certificatesandrecords.CertificatesAndRecordsPage;
import main.viewasset.codicils.CodicilsCard;
import main.assetdetail.sistervessels.element.RegisterBookSisterVesselsCard;
import main.assetdetail.sistervessels.SisterVesselsPage;
import main.viewasset.surveys.SurveysCard;
import main.assetdetail.sistervessels.element.TechnicalSisterVesselsCard;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ASSET_HUB;

public class AssetInfoTest extends BaseTest {
    @Test(description = "Automation_UJ_Asset info_Verify asset card, asset header and asset hub functionality (high-level)")
    @Issue("LRCDT-617")
    @Features(ASSET_HUB)

    public void assetInfoTest() {
        //Open the app, Search for Asset "Laura Test Asset" and click view asset
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class);

        AssetCard assetCard = vesselListPage
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."));

        boolean favourited = assetCard.isFavourite();

        // STEP 2-3 - Asset Card Info
        assert_().withFailureMessage("Asset Type is expected to be displayed")
                .that(assetCard.isAssetTypeDisplayed())
                .isTrue();
        assert_().withFailureMessage("Build Date is expected to be displayed")
                .that(assetCard.isBuildDateDisplayed())
                .isTrue();
        assert_().withFailureMessage("IMO number is expected to be displayed")
                .that(assetCard.isImoNumberDisplayed())
                .isTrue();
        assert_().withFailureMessage("Asset Name is expected to be displayed")
                .that(assetCard.getAssetName())
                .isNotEmpty();
        assert_().withFailureMessage("Flag is expected to be displayed")
                .that(assetCard.getFlagName())
                .isNotEmpty();
        assert_().withFailureMessage("Gross Tonnage is expected to be displayed")
                .that(assetCard.getGrossTonnage())
                .isNotEmpty();
        assert_().withFailureMessage("Asset Emblem is expected to be displayed")
                .that(assetCard.isAssetEmblemDisplayed())
                .isTrue();
        assert_().withFailureMessage("LR Indicator is expected to be displayed")
                .that(assetCard.isLrBadgeIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Request Survey is expected to be displayed")
                .that(assetCard.isRequestSurveyButtonDisplayed())
                .isTrue();

        // STEP 4 - Click View Asset button
        AssetHubPage assetHubPage = assetCard.clickViewAssetButton();

        // STEP 5 - Asset hub page
        assert_().withFailureMessage("Codicils card should be displayed.")
                .that(assetHubPage.getCodicilCard().exists())
                .isTrue();
        assert_().withFailureMessage("Jobs card should be displayed.")
                .that(assetHubPage.getSurveysCard().exists())
                .isTrue();
        assert_().withFailureMessage("Certficates and Records card should be displayed.")
                .that(assetHubPage.getCertificatesAndRecordsCard().exists())
                .isTrue();
        assert_().withFailureMessage("View Full Schedule button should be displayed.")
                .that(assetHubPage.getSurveyPlannerPage().isViewFullScheduleButtonDisplayed())
                .isTrue();

        // STEP 6 - Check for asset header
        AssetHeaderPage assetHeaderPage = assetHubPage.getAssetHeaderPage();
        assert_().withFailureMessage("Asset Emblem should be displayed.")
                .that(assetHeaderPage.isAssetEmblemDisplayed())
                .isTrue();
        assert_().withFailureMessage("Asset Name should be displayed.")
                .that(assetHeaderPage.getAssetName())
                .isNotEmpty();
        assert_().withFailureMessage("Asset Type should be displayed.")
                .that(assetHeaderPage.getAssetType())
                .isNotEmpty();
        assert_().withFailureMessage("Overall Status should be displayed.")
                .that(assetHeaderPage.getOverallDueStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Date of Build should be displayed.")
                .that(assetHeaderPage.getDateOfBuild())
                .isNotEmpty();
        assert_().withFailureMessage("Gross Tonnage should be displayed.")
                .that(assetHeaderPage.getGrossTonnage())
                .isNotEmpty();
        assert_().withFailureMessage("Flag should be displayed.")
                .that(assetHeaderPage.getFlag())
                .isNotEmpty();
        assert_().withFailureMessage("Class Status should be displayed.")
                .that(assetHeaderPage.getClassStatus())
                .isNotEmpty();
        assert_().withFailureMessage("IMO Number should be displayed.")
                .that(assetHeaderPage.getImoNumber())
                .isNotEmpty();
        assert_().withFailureMessage("MMS Indicator should be displayed.")
                .that(assetHeaderPage.isMmsIndicatorDisplayed())
                .isTrue();

        // STEP 7-8 - Check for Favourite Icon
        if (!favourited)
            assetHeaderPage.selectFavourite();
        assert_().withFailureMessage("Favourite Icon is expected to be selected")
                .that(assetHeaderPage.isFavouriteIconSelected())
                .isTrue();

        // STEP 9 - Request Survey button
        assert_().withFailureMessage("Request Survey button should be displayed.")
                .that(assetHeaderPage.isRequestSurveyButtonDisplayed())
                .isTrue();

        //Step 10 - View Details
        AssetDetailsPage assetDetailsPage = assetHeaderPage.clickAssetDetailsButton();
        assert_().withFailureMessage("Export Asset Information card should be displayed")
                .that(assetDetailsPage.getExportAssetInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Technical Sister Vessels card should be displayed.")
                .that(assetDetailsPage.getTechnicalSisterVesselsCard().exists())
                .isTrue();
        assert_().withFailureMessage("Register Book Sister Vessels card should be displayed.")
                .that(assetDetailsPage.getRegisterBookSisterVesselsCard().exists())
                .isTrue();
        assert_().withFailureMessage("Registry Information card should be displayed.")
                .that(assetDetailsPage.getRegistryInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Principal Dimensions card should be displayed.")
                .that(assetDetailsPage.getPrincipalDimensionsCard().exists())
                .isTrue();
        assert_().withFailureMessage("'Class history, notations and descriptive notes' card should be displayed.")
                .that(assetDetailsPage.getClassHistoryNotationsAndDescriptiveNotesCard().exists())
                .isTrue();
        assert_().withFailureMessage("Equipment Information card should be displayed.")
                .that(assetDetailsPage.getEquipmentInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Registered Owner card should be displayed.")
                .that(assetDetailsPage.getRegisteredOwnerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Group Owner card should be displayed.")
                .that(assetDetailsPage.getGroupOwnerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Operator card should be displayed.")
                .that(assetDetailsPage.getOperatorCard().exists())
                .isTrue();
        assert_().withFailureMessage("Ship Manager card should be displayed.")
                .that(assetDetailsPage.getShipManagerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Technical Manager card should be displayed.")
                .that(assetDetailsPage.getTechnicalManagerCard().exists())
                .isTrue();
        assert_().withFailureMessage("DOC Company card should be displayed.")
                .that(assetDetailsPage.getDocCompanyCard().exists())
                .isTrue();

        // STEP 11-12 - Check for Codicils
        CodicilsCard codicilsCard = assetDetailsPage.getAssetHeaderPage().clickBackButton(AssetHubPage.class).getCodicilCard();
        assert_().withFailureMessage("Codicils are expected to be displayed")
                .that(codicilsCard.getCodicilsElements().size())
                .isGreaterThan(0);

        // STEP 13-14 - Click View all for Codicils
        codicilsCard.clickViewAllButton().getAssetHeaderPage().clickBackButton(AssetHubPage.class);

        // STEP 15 - Check for Surveys
        SurveysCard surveysCard = assetHubPage.getSurveysCard();
        assert_().withFailureMessage("Survey are expected to be displayed")
                .that(surveysCard.getSurveysElements().size())
                .isGreaterThan(0);

        // STEP 16-17 - Click View all for Surveys
        assetHubPage = surveysCard.clickViewAllButton().getAssetHeaderPage().clickBackButton(AssetHubPage.class);

        // STEP 21 - Check for Technical Sister Vessels
        TechnicalSisterVesselsCard technicalSisterVesselsCard = assetHubPage.getAssetHeaderPage()
                .clickAssetDetailsButton()
                .getTechnicalSisterVesselsCard();
        assert_().withFailureMessage("The number of Technical Sister Vessels associated with the asset should be displayed.")
                .that(technicalSisterVesselsCard.getTotalTechnicalSisterVessels())
                .isGreaterThan(0);

        // STEP 22 - Click View All
        SisterVesselsPage technicalSisterVesselsPage = technicalSisterVesselsCard.clickViewAllLink();
        assert_().withFailureMessage("Title for Technical Sister Vessels is expected to be displayed")
                .that(technicalSisterVesselsPage.getTitle())
                .isNotEmpty();
        assert_().withFailureMessage("Asset Cards of Technical Sister Vessels are expected to be displayed")
                    .that(technicalSisterVesselsPage.getAssetCards().size())
                    .isGreaterThan(0);

        // STEP 23 - Click Back button
        technicalSisterVesselsPage.clickBackButton();

        // STEP 24 - Check for Register Book Sister Vessels
        RegisterBookSisterVesselsCard registerBookSisterVesselsCard = assetDetailsPage.getRegisterBookSisterVesselsCard();
        assert_().withFailureMessage("The number of Register Book Sister Vessels associated with the asset should be displayed.")
                .that(registerBookSisterVesselsCard.getTotalRegisterBookSisterVessels())
                .isGreaterThan(0);

        // STEP 25 - Click View All
        SisterVesselsPage registerBookSisterVesselsPage = registerBookSisterVesselsCard.clickViewAllLink();
        assert_().withFailureMessage("Title for Register Book Sister Vessels is expected to be displayed")
                .that(registerBookSisterVesselsPage.getTitle())
                .isNotEmpty();
        assert_().withFailureMessage("Asset Cards of Register Book Sister Vessels are expected to be displayed")
                .that(registerBookSisterVesselsPage.getAssetCards().size())
                .isGreaterThan(0);

        // STEP 26 - Click back button
        registerBookSisterVesselsPage.clickBackButton().getAssetHeaderPage().clickBackButton(AssetHubPage.class);

        // STEP 27 - Survey Planner
        assert_().withFailureMessage("Surveys in Survey planner should be displayed.")
                .that(assetHubPage.getSurveyPlannerPage().getSurveys().size())
                .isGreaterThan(0);
        assert_().withFailureMessage("'Notes and Actions' in Survey planner should be displayed.")
                .that(assetHubPage.getSurveyPlannerPage().getNotesAndActions().size())
                .isGreaterThan(0);

        //Step 18 - View certificates (Adonia)
        assetHubPage = LandingPage.getTopBar().clickClientTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .selectSearchDropDown(ClassDirect.AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() ->  new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton();

        assetHubPage.getCertificatesAndRecordsCard()
                .getCertificateElements()
                .forEach(e->
                {
                    assert_().withFailureMessage("Certificate number is expected to be displayed.")
                            .that(e.getCertificateNumber())
                            .isNotEmpty();
                    assert_().withFailureMessage("Issued Date is expected to be displayed.")
                            .that(e.getIssuedDate())
                            .isNotEmpty();
                });

        // STEP 19 - View All
        CertificatesAndRecordsPage certificatesAndRecordsPage = assetHubPage.getCertificatesAndRecordsCard().clickViewAllButton();
        assert_().withFailureMessage("Certificates are expected to be displayed.")
                .that(certificatesAndRecordsPage.getCertificateElements().size())
                .isGreaterThan(0);

        // STEP 20 - CLick back button
        certificatesAndRecordsPage.getAssetHeaderPage().clickBackButton(AssetHubPage.class);
    }
}
