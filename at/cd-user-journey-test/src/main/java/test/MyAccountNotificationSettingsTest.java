package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.login.LoginPage;
import main.myaccount.element.NotificationsCard;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.NOTIFICATIONS;

public class MyAccountNotificationSettingsTest extends BaseTest
{
    @Test(description = "As a user user, the user can view his notifications he has opted in / out", enabled = false)
    @Issue("LRCDT-1624")
    @Features(NOTIFICATIONS)
    public void myAccountNotificationSettingsTest()
    {
        NotificationsCard notificationsCard = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getTopBar()
                .clickMyAccountButton()
                .getNotificationsCard();

        assert_().withFailureMessage("Asset Listings Title should be displayed.")
                .that(notificationsCard.getAssetListingsTitle())
                .isNotEmpty();

        assert_().withFailureMessage("Asset Listings Description should be displayed.")
                .that(notificationsCard.getAssetListingsDescription())
                .isNotEmpty();

        assert_().withFailureMessage("Asset Due Status Title should be displayed.")
                .that(notificationsCard.getAssetDueStatusTitle())
                .isNotEmpty();

        assert_().withFailureMessage("Asset Due Status Description should be displayed.")
                .that(notificationsCard.getAssetDueStatusDescription())
                .isNotEmpty();

        assert_().withFailureMessage("Newly Completed Survey Reports Title should be displayed.")
                .that(notificationsCard.getNewlyCompletedSurveyReportsTitle())
                .isNotEmpty();

        assert_().withFailureMessage("Newly Completed Survey Reports Description should be displayed.")
                .that(notificationsCard.getNewlyCompletedSurveyReportsDescription())
                .isNotEmpty();

        // Enable the 3 notification switches
        notificationsCard.selectAssetListingsSwitch()
                .selectAssetDueStatusSwitch()
                .selectNewlyCompletedSurveyReportsSwitch();

        assert_().withFailureMessage("Notification for 'Asset Listings' should be switched ON.")
                .that(notificationsCard.isAssetListingsSwitchedOn())
                .isTrue();

        assert_().withFailureMessage("Notification for 'Asset Due Status' should be switched ON.")
                .that(notificationsCard.isAssetDueStatusSwitchedOn())
                .isTrue();

        assert_().withFailureMessage("Notification for 'Newly Completed Survey Reports' should be switched ON.")
                .that(notificationsCard.isNewlyCompletedSurveyReportsSwitchedOn())
                .isTrue();

        // Disable the 3 notification switches
        notificationsCard.deselectAssetListingsSwitch()
                .deselectAssetDueStatusSwitch()
                .deselectNewlyCompletedSurveyReportsSwitch();

        assert_().withFailureMessage("Notification for 'Asset Listings' should be switched OFF.")
                .that(notificationsCard.isAssetListingsSwitchedOn())
                .isFalse();

        assert_().withFailureMessage("Notification for 'Asset Due Status' should be switched OFF.")
                .that(notificationsCard.isAssetDueStatusSwitchedOn())
                .isFalse();

        assert_().withFailureMessage("Notification for 'Newly Completed Survey Reports' should be switched OFF.")
                .that(notificationsCard.isNewlyCompletedSurveyReportsSwitchedOn())
                .isFalse();
    }
}
