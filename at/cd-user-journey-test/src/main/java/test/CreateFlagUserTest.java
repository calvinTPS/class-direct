package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.administration.adduser.flag.SelectFlagPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.administration.adduser.steps.ConfirmAccountCreationPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.FLAG_EMAIL;
import static constant.User.Flag.BANGLADESH;
import static constant.User.UserAccountTypes;

public class CreateFlagUserTest extends BaseTest
{
    @Test(description = "Login as an LR Administrator and create a Flag user")
    @Issue("LRCDT-2195")
    @Features(USER_MANAGEMENT)
    public void createFlagUserTest()
    {
        AdministrationPage administrationPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();

        assert_().withFailureMessage("Cannot create flag user that is already existing.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(FLAG_EMAIL))
                        .count())
                .isEqualTo(0L);

        administrationPage.clickAddUserButton()
                .setEmailAddress(FLAG_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User card for %s was not found.", FLAG_EMAIL)))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.FLAG.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Account type %s was not found.", UserAccountTypes.FLAG.toString())))
                .clickSelectButton(SelectFlagPage.class)
                .setFlagTextbox(BANGLADESH.getName())
                .getFlagCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Flag %s was not found.", BANGLADESH.getName())))
                .clickSelectButton()
                .setAccountExpiryDate(now.format(FRONTEND_TIME_FORMAT_DTS))
                .getFooterPage()
                .clickConfirmButton(ConfirmAccountCreationPage.class)
                .getFooterPage()
                .clickConfirmButton(AdministrationPage.class);

        assert_().withFailureMessage("Newly created flag user should be displayed in the list.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(FLAG_EMAIL))
                        .count())
                .isGreaterThan(0L);
    }
}
