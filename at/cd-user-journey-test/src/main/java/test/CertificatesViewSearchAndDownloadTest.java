package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.CertificateAttachDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.certificates.certificatesandrecords.CertificatesAndRecordsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Certificates;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.CertificateSearchOptions;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.CERTIFICATES;

public class CertificatesViewSearchAndDownloadTest extends BaseTest
{
    @Test(description = "User is able to view, search and download certificates.")
    @Issue("LRCDT-1887")
    @Features(CERTIFICATES)
    public void certificatesViewSearchAndDownloadTest()
    {
        final String certificateNameSearchString = "General Certificate";
        final String certificateNumberSearchString = "03";
        final String formNumberSearchString = "123";
        final String versionSearchString = "2015";
        final String officeSearchString = "Bordeaux";

        // STEP 1-3 Navigate to Certificates page
        CertificatesAndRecordsPage certificatesAndRecordsPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s not found.", Asset.WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getCertificatesAndRecordsCard()
                .clickViewAllButton();

        // STEP 4 - Verify High Level details
        certificatesAndRecordsPage.getCertificateElements()
                .forEach(e-> {
                    assert_().withFailureMessage("Certificate name should be displayed.")
                            .that(e.getCertificateName())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate number should be displayed.")
                            .that(e.getCertificateNumber())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate version should be displayed.")
                            .that(e.getCertificateVersion())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate status should be displayed.")
                            .that(e.getDueStatus())
                            .isNotEmpty();
                    assert_().withFailureMessage("Issuing surveyor should be displayed.")
                            .that(e.getIssuingSurveyor())
                            .isNotEmpty();
                    assert_().withFailureMessage("Issued Date should be displayed.")
                            .that(e.getIssueDate())
                            .isNotNull();
                    assert_().withFailureMessage("Expiry Date should be displayed.")
                            .that(e.getExpiryDate())
                            .isNotNull();
                    assert_().withFailureMessage("Endorsed Date should be displayed.")
                            .that(e.getEndorsedDate())
                            .isNotNull();
                    assert_().withFailureMessage("Form number should be displayed.")
                            .that(e.getFormNumber())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate Type should be displayed.")
                            .that(e.getCertificateType())
                            .isNotEmpty();
                });

        // STEP 5 - Default search option
        FilterBarPage filterBarPage = certificatesAndRecordsPage.getFilterBarPage();
        assert_().withFailureMessage("Default search option should be Certificte Name.")
                .that(filterBarPage.getSelectedSearchOption())
                .isEqualTo(CertificateSearchOptions.CERTIFICATE_NAME.toString());

        // STEP 6 - Verify Search options
        assert_().withFailureMessage("All search options should be present in the dropdown list.")
                .that(filterBarPage.getSearchOptionList())
                .isEqualTo(Arrays.stream(CertificateSearchOptions.values()).map(CertificateSearchOptions::toString).collect(Collectors.toList()));
        filterBarPage.closeSearchOptionList();

        // Search by Certificate Name
        filterBarPage.selectSearchDropDown(CertificateSearchOptions.CERTIFICATE_NAME.toString())
                .setSearchTextBox(certificateNameSearchString)
                .getPageReference(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e->  assert_().withFailureMessage(String.format("%s should contain the search string '%s'", e.getCertificateName(), certificateNameSearchString))
                        .that(e.getCertificateName())
                        .contains(certificateNameSearchString));

        // STEP 7 - Search by Certificate Number
        filterBarPage.selectSearchDropDown(CertificateSearchOptions.CERTIFICATE_NUMBER.toString())
                .setSearchTextBox(certificateNumberSearchString)
                .getPageReference(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e->  assert_().withFailureMessage(String.format("%s should contain the search string '%s'", e.getCertificateNumber(), certificateNumberSearchString))
                        .that(e.getCertificateNumber())
                        .contains(certificateNumberSearchString));

        // STEP 8 - Search by Form Number
        filterBarPage.selectSearchDropDown(CertificateSearchOptions.FORM_NUMBER.toString())
                .setSearchTextBox(formNumberSearchString)
                .getPageReference(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e->  assert_().withFailureMessage(String.format("%s should contain the search string '%s'", e.getFormNumber(), formNumberSearchString))
                        .that(e.getFormNumber())
                        .contains(formNumberSearchString));

        // STEP 9 - Search by version
        filterBarPage.selectSearchDropDown(CertificateSearchOptions.VERSION.toString())
                .setSearchTextBox(versionSearchString)
                .getPageReference(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e->  assert_().withFailureMessage(String.format("%s should contain the search string '%s'", e.getCertificateVersion(), versionSearchString))
                        .that(e.getCertificateVersion())
                        .contains(versionSearchString));

        // STEP 10 - Search by Office
        filterBarPage.selectSearchDropDown(CertificateSearchOptions.OFFICE.toString())
                .setSearchTextBox(officeSearchString)
                .getPageReference(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e->  assert_().withFailureMessage(String.format("%s should contain the search string '%s'", e.getOffice(), officeSearchString))
                        .that(e.getOffice())
                        .contains(officeSearchString));

        // STEP 11 - Download certificate
        certificatesAndRecordsPage.getCertificateElements()
                .stream()
                .filter(e-> e.getOffice().equals(officeSearchString))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected certificate with office: %s is not found.", officeSearchString)))
                .clickDownloadButton();

        CertificateAttachDto certificateAttachDto = new CertificateAttachDto();
        certificateAttachDto.setCertId(10L);
        certificateAttachDto.setJobId(110023L);
        String certificateToken = new Certificates().getCertificateAttachmentToken(certificateAttachDto);

        assert_().withFailureMessage(String.format("Certificate attachment for ID: %s should be downloaded successfully.", certificateAttachDto.getCertId()))
                .that(DownloadReportHelper.getDownloadFileChecksum(certificateToken))
                .isNotEmpty();
    }
}
