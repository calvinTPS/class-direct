package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.sub.accessibleassets.AccessibleAssetsPage;
import main.administration.detail.sub.accessibleassets.EditAccessibleAssetsPage;
import main.assetdetail.AssetDetailsPage;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.util.Collections;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset.FOR_RESTRICTED_TEST;
import static constant.Epic.USER_MANAGEMENT;
import static java.util.Arrays.asList;

public class ViewPublicInformationForRestrictedAssetTest extends BaseTest
{
    @Test(description = "Given the User has a Restricted Asset, the User can view only publicly available information in the System for this Asset")
    @Issue("LRCDT-2125")
    @Features(USER_MANAGEMENT)
    public void viewPublicInformationForRestrictedAssetTest()
    {
        // STEP 1
        LandingPage landingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName());

        AdministrationDetailPage administrationDetailPage = landingPage.clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(LR_ADMIN.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected user with email %s is not found.", LR_ADMIN.getEmail())))
                .clickFurtherMoreDetailsButton();

        boolean isRestrictedAsset = administrationDetailPage.clickRestrictedAssetsTab()
                .getRestrictedAssetCards()
                .stream()
                .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()));

        if (!isRestrictedAsset) {
            //set as restricted asset
            EditAccessibleAssetsPage editAccessibleAssetsPage = administrationDetailPage.clickAccessibleAssetsTab()
                    .clickEditButton();

            editAccessibleAssetsPage.getAccessibleAssetCards()
                    .stream()
                    .filter(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()))
                    .findFirst()
                    .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset: %s not found.", FOR_RESTRICTED_TEST.getName())))
                    .selectAssetCard();

            editAccessibleAssetsPage.clickMoveToRestrictedAssetButton()
                    .clickConfirmButton(AccessibleAssetsPage.class);

            // Checking if the asset is successfully moved to restricted assets
            assert_().withFailureMessage(String.format("Asset [%s] is not successfully moved to restricted assets.", FOR_RESTRICTED_TEST.getName()))
                    .that(administrationDetailPage.clickRestrictedAssetsTab()
                            .getRestrictedAssetCards()
                            .stream()
                            .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName())))
                    .isTrue();
        }

        AssetCard assetCard = landingPage.clickVesselListTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(FOR_RESTRICTED_TEST.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s not found.", FOR_RESTRICTED_TEST.getName())));

        assert_().withFailureMessage("IMO number should be displayed.")
                .that(assetCard.isImoNumberDisplayed())
                .isTrue();
        assert_().withFailureMessage("Asset name should be displayed.")
                .that(assetCard.getAssetName())
                .isNotEmpty();
        assert_().withFailureMessage("Asset type should be displayed.")
                .that(assetCard.isAssetTypeDisplayed())
                .isTrue();
        assert_().withFailureMessage("Date of build should be displayed.")
                .that(assetCard.isBuildDateDisplayed())
                .isTrue();
        assert_().withFailureMessage("Date of build should be displayed.")
                .that(assetCard.isBuildDateDisplayed())
                .isTrue();
        assert_().withFailureMessage("Flag should be displayed.")
                .that(assetCard.getFlagName())
                .isNotEmpty();
        assert_().withFailureMessage("Class should be displayed.")
                .that(assetCard.getClassStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Gross tonnage should be displayed.")
                .that(assetCard.getGrossTonnage())
                .isNotEmpty();
        assert_().withFailureMessage("Overall status should be displayed.")
                .that(assetCard.getDueStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Icon representing the asset type should be displayed.")
                .that(assetCard.isAssetEmblemDisplayed())
                .isTrue();
        assert_().withFailureMessage("LR badge icon should be displayed.")
                .that(assetCard.isLrBadgeIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Request Survey button should NOT be displayed.")
                .that(assetCard.isRequestSurveyButtonDisplayed())
                .isFalse();

        // STEP 3 - Click View Asset button
        AssetHubPage assetHubPage = assetCard.clickViewAssetButton();

        assert_().withFailureMessage("Request Survey button should NOT be displayed in the header.")
                .that(assetHubPage.getAssetHeaderPage().isRequestSurveyButtonDisplayed())
                .isFalse();
        assert_().withFailureMessage("Codicils card should NOT be displayed.")
                .that(assetHubPage.getCodicilCard().exists())
                .isFalse();
        assert_().withFailureMessage("Jobs card should NOT be displayed.")
                .that(assetHubPage.getSurveysCard().exists())
                .isFalse();
        assert_().withFailureMessage("Certficates and Records card should NOT be displayed.")
                .that(assetHubPage.getCertificatesAndRecordsCard().exists())
                .isFalse();
        assert_().withFailureMessage("Asset Details button should be displayed in the header.")
                .that(assetHubPage.getAssetHeaderPage().isAssetDetailsButtonDisplayed())
                .isTrue();

        // STEP 4 - Asset details
        AssetDetailsPage assetDetailsPage = assetHubPage.getAssetHeaderPage().clickAssetDetailsButton();
        assert_().withFailureMessage("Export Asset Information card should be displayed.")
                .that(assetDetailsPage.getExportAssetInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Technical Sister Vessels card should NOT be displayed.")
                .that(assetDetailsPage.getTechnicalSisterVesselsCard().exists())
                .isFalse();
        assert_().withFailureMessage("Register Book Sister Vessels card should NOT be displayed.")
                .that(assetDetailsPage.getRegisterBookSisterVesselsCard().exists())
                .isFalse();
        assert_().withFailureMessage("Registry Information card should be displayed.")
                .that(assetDetailsPage.getRegistryInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Principal Dimensions card should be displayed.")
                .that(assetDetailsPage.getPrincipalDimensionsCard().exists())
                .isTrue();
        assert_().withFailureMessage("'Class history notations and descriptive notes' card should be displayed.")
                .that(assetDetailsPage.getClassHistoryNotationsAndDescriptiveNotesCard().exists())
                .isTrue();
        assert_().withFailureMessage("Equipment Information card should be displayed.")
                .that(assetDetailsPage.getEquipmentInformationCard().exists())
                .isTrue();
        assert_().withFailureMessage("Registered Owner card should be displayed.")
                .that(assetDetailsPage.getRegisteredOwnerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Group Owner card should be displayed.")
                .that(assetDetailsPage.getGroupOwnerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Operator card should be displayed.")
                .that(assetDetailsPage.getOperatorCard().exists())
                .isTrue();
        assert_().withFailureMessage("Ship Manager card should be displayed.")
                .that(assetDetailsPage.getShipManagerCard().exists())
                .isTrue();
        assert_().withFailureMessage("Technical Manager card should be displayed.")
                .that(assetDetailsPage.getTechnicalManagerCard().exists())
                .isTrue();
        assert_().withFailureMessage("DOC Company card should be displayed.")
                .that(assetDetailsPage.getDocCompanyCard().exists())
                .isTrue();

        // STEP 5 - Export
        assetDetailsPage.getExportAssetInformationCard()
                .clickExportAssetInformationButton()
                .clickExportButton();

        assert_().withFailureMessage(String.format("%s should be exported successfully.", FOR_RESTRICTED_TEST.getName()))
                .that(DownloadReportHelper.getDownloadFileChecksum(getToken()))
                .isNotEmpty();
    }

    private String getToken()
    {
        AssetQueryHDto assetQueryHDto = new AssetQueryHDto(); //empty query

        AssetExportDto assetExportDto = new AssetExportDto();
        assetExportDto.setIncludes(Collections.singletonList(FOR_RESTRICTED_TEST.getAssetCode()));
        assetExportDto.setAssetQuery(assetQueryHDto);
        assetExportDto.setInfo(2L);
        assetExportDto.setService(3L);
        assetExportDto.setCodicils(asList(1L, 4L, 2L));
        return new Export().getExportAssetToken(assetExportDto);
    }
}
