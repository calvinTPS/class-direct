package test;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.certificates.certificatesandrecords.CertificatesAndRecordsPage;
import main.viewasset.certificates.certificatesandrecords.element.CertificateElement;
import main.viewasset.certificates.certificatesandrecords.filter.FilterCertificatePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.CertificateSortOptions;
import static constant.ClassDirect.CertificateStatus;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.CERTIFICATES;

public class CertificatesFilterAndSortTest extends BaseTest
{
    @Test(description = "User is able to view, filter and sort certificates.")
    @Issue("LRCDT-1886")
    @Features(CERTIFICATES)
    public void certificatesFilterAndSortTest()
    {
        final LocalDate issuedDateFrom = LocalDate.of(2010, 9, 29);
        final LocalDate issuedDateTo = LocalDate.of(2011, 10, 20);
        final LocalDate expiryDateFrom = LocalDate.of(2019, 10, 25);
        final LocalDate expiryDateTo = LocalDate.of(2020, 1, 20);

        // STEP 1-3 Navigate to Certificates page
        CertificatesAndRecordsPage certificatesAndRecordsPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.WITH_CERTIFICATES.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s not found.", Asset.WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getCertificatesAndRecordsCard()
                .clickViewAllButton();

        // STEP 4 - Default Sort category is Expiry Date
        FilterBarPage filterBarPage = certificatesAndRecordsPage.getFilterBarPage();
        assert_().withFailureMessage("Expiry date should be the default sort category.")
                .that(filterBarPage.getSelectedSortByOption())
                .isEqualTo(CertificateSortOptions.EXPIRY_DATE.toString());

        // STEP 5 - Sort options
        assert_().withFailureMessage("All sort options should be present in the dropdown list.")
                .that(filterBarPage.getSortByOptionList())
                .isEqualTo(Arrays.stream(CertificateSortOptions.values()).map(CertificateSortOptions::toString).collect(Collectors.toList()));
        filterBarPage.closeSortByOptionList();

        // STEP 6 - Sort by Certificate number
        filterBarPage.selectSortByDropDown(CertificateSortOptions.CERTIFICATE_NUMBER.toString());
        List<String> certificateNumberList = certificatesAndRecordsPage.getCertificateElements()
                .stream()
                .map(CertificateElement::getCertificateNumber)
                .collect(Collectors.toList());

        Collections.reverse(certificateNumberList);
        assert_().withFailureMessage("Certificate numbers should be in descending order")
                .that(certificateNumberList)
                .isOrdered();

        // STEP 7 - Sort by Certificate Name
        filterBarPage.selectSortByDropDown(CertificateSortOptions.CERTIFICATE_NAME.toString());
        assert_().withFailureMessage("Certificate Names should be in ascending order")
                .that(certificatesAndRecordsPage.getCertificateElements()
                        .stream()
                        .map(CertificateElement::getCertificateName)
                        .collect(Collectors.toList()))
                .isOrdered();

        // STEP 8-9 - Filter by Issued Date
        filterBarPage.clickFilterButton(FilterCertificatePage.class)
                .setFromIssuedDate(issuedDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setToIssuedDate(issuedDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e-> assert_().withFailureMessage(String.format("Issued Date should be in between %s and %s.", issuedDateFrom, issuedDateTo))
                        .that(e.getIssueDate())
                        .isIn(Range.closed(issuedDateFrom, issuedDateTo)));

        // STEP 10 - Filter by Expiry Date
        filterBarPage.clickFilterButton(FilterCertificatePage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterCertificatePage.class)
                .setFromExpiryDate(expiryDateFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setToExpiryDate(expiryDateTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e-> assert_().withFailureMessage(String.format("Expiry Date should be in between %s and %s.", expiryDateFrom, expiryDateTo))
                        .that(e.getExpiryDate())
                        .isIn(Range.closed(expiryDateFrom, expiryDateTo)));

        // STEP 11 - Deselect status: All
        FilterCertificatePage filterCertificatePage = filterBarPage.clickFilterButton(FilterCertificatePage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterCertificatePage.class);

        filterCertificatePage
                .deselectCertificateStatusCheckbox(CertificateStatus.ALL)
                .getCertificateStatusCheckboxList()
                .forEach(e -> assert_().withFailureMessage(String.format("This certificate status: %s is expected to be deselected.", e.getCertificateStatusName()))
                        .that(e.isSelected())
                        .isFalse());

        // STEP 12 - Select Status: ISSUED
        filterCertificatePage.selectCertificateStatusCheckbox(CertificateStatus.ISSUED)
                .clickSubmitButton(CertificatesAndRecordsPage.class)
                .getCertificateElements()
                .forEach(e-> assert_().withFailureMessage(String.format("Certificate Status should be filtered by %s.", CertificateStatus.ISSUED.toString()))
                        .that(e.getDueStatus())
                        .isEqualTo(CertificateStatus.ISSUED.toString()));

        // STEP 13 - Clear filters
        filterBarPage.clickFilterButton(FilterCertificatePage.class)
                .clickClearFiltersButton();

        // STEP 14 - Sort by Certificate Status
        filterBarPage.selectSortByDropDown(CertificateSortOptions.CERTIFICATE_STATUS.toString());
        assert_().withFailureMessage("Certificates should be sorted by Status.")
                .that(certificatesAndRecordsPage.getCertificateElements()
                        .stream()
                        .map(e-> CertificateStatus.valueOf(e.getDueStatus().toUpperCase()))
                        .collect(Collectors.toList()))
                .isOrdered();

        // STEP 15 - Sort by Issued date
        filterBarPage.selectSortByDropDown(CertificateSortOptions.ISSUE_DATE.toString());
        List<LocalDate> issuedDateList = certificatesAndRecordsPage.getCertificateElements()
                .stream()
                .map(CertificateElement::getIssueDate)
                .collect(Collectors.toList());

        Collections.reverse(issuedDateList);
        assert_().withFailureMessage("Certificates should be sorted by Issued date.")
                .that(issuedDateList)
                .isOrdered();
    }
}
