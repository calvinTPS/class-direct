package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.assettype.FilterByAssetTypePage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.AssetTableName.NON_SHIP_STRUCTURES;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;
import static constant.User.Flag.JERSEY;

public class FilterAssetsByAssetTypeAndFlagTest extends BaseTest
{
    private final String flagType = JERSEY.getName();

    @Test(description = "Automation_UJ_Fleet dashboard_Filter assets_by_Asset type and Flag")
    @Issue("LRCDT-598")
    @Features(FLEET_DASHBOARD)
    public void filterAssetsByAssetTypeAndFlagTest()
    {
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickFlagButton()
                .setSearchFlagTextBox(flagType)
                .clickSubmitButton()
                .clickSubmitButton(VesselListPage.class);

        vesselListPage.getAssetCards()
                .forEach(assetCard -> assert_().withFailureMessage("Assets with Flag " + flagType + " are expected to displayed")
                        .that(assetCard.getFlagName())
                        .isEqualTo(flagType));

        assert_().withFailureMessage("Asset Cards are expected to be displayed.")
                .that(vesselListPage.getAssetCards().size())
                .isGreaterThan(0);

        FilterByAssetTypePage filterByAssetTypePage = vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .removeSelectedFlags()
                .clickAssetTypeButton();
        filterByAssetTypePage.getAssetCardsByTableName(NON_SHIP_STRUCTURES.getTableName())
                .getAssetType()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset type not found."))
                .selectAssetType();

        filterByAssetTypePage.clickSubmitButton()
                .clickSubmitButton(VesselListPage.class);

        assert_().withFailureMessage("Asset Cards are expected to be displayed.")
                .that(vesselListPage.getAssetCards().size())
                .isGreaterThan(0);
    }
}
