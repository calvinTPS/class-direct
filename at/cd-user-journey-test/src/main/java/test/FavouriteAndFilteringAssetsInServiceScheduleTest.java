package test;

import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDateHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.assettype.FilterByAssetTypePage;
import main.common.filter.assettype.element.AssetType;
import main.common.filter.servicetype.FilterByServiceTypePage;
import main.common.filter.servicetype.base.ServiceTable;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import main.serviceschedule.element.AssetCard;
import main.serviceschedule.tabular.base.AssetTable;
import main.vessellist.VesselListPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.ReferenceData;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.collect.Range.closed;
import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.AssetTableName.NON_MERCHANTS;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SERVICE_SCHEDULE;
import static constant.User.Flag.MEXICO;

public class FavouriteAndFilteringAssetsInServiceScheduleTest extends BaseTest
{
    @Test(description = "Search Sort Filter assets, add/remove an asset from Favourites in Service Schedule")
    @Issue("LRCDT-1328")
    @Features(SERVICE_SCHEDULE)
    public void favouriteAndFilteringAssetsInServiceScheduleTest()
    {
        final String assetNameSearchString = "Asset",
                imoNumberSearchString = "397",
                flagSearchString = MEXICO.getName();
        final int minGrossTonnage = 10,
                maxGrossTonnage = 1000;
        final LocalDate toDateOfBuild = LocalDate.of(2016, 4, 14);
        final LocalDate fromDateOfBuild = toDateOfBuild.minusDays(2);

        FilterBarPage filterBarPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class);

        List<String> lstCategorySearchOptions = filterBarPage.getSearchOptionList();
        for (AssetSearchOptions strCategory : AssetSearchOptions.values())
        {
            assert_().withFailureMessage(strCategory.toString() + " should be in the category list")
                    .that(lstCategorySearchOptions.stream().anyMatch(e -> e.equals(strCategory.toString())))
                    .isTrue();
        }

        assert_().withFailureMessage(
                String.format("%s should be displayed as default selected Category.", AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString()))
                .that(filterBarPage.closeSearchOptionList().getSelectedSearchOption())
                .isEqualTo(AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString());

        //STEP 4
        List<AssetTable> assetTableList = filterBarPage.setSearchTextBox(imoNumberSearchString)
                .getPageReference(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables();

        assetTableList.forEach(e ->
                assert_().withFailureMessage(String.format("No assets containing the search string IMO Number %s was found", imoNumberSearchString))
                        .that(e.getAssetCard().getImoNumber().contains(imoNumberSearchString))
                        .isTrue()
        );

        //STEP 5
        filterBarPage.setSearchTextBox(assetNameSearchString)
                .selectSearchDropDown(AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString());
        assetTableList.forEach(e ->
                assert_().withFailureMessage(String.format("No assets name containing the search string \"%s\" was found.",
                        assetNameSearchString))
                        .that(e.getAssetCard().getAssetName().contains(assetNameSearchString))
                        .isTrue()
        );

        //STEP 6
        assert_().withFailureMessage("Search text box should be cleared when close icon is clicked.")
                .that(filterBarPage.clickClearButtonInSearchTextBox().getSearchText().isEmpty())
                .isTrue();

        //STEP 8
        List<String> lstSortByOptions = filterBarPage.getSortByOptionList();
        for (AssetSortByOptions strSortBy : AssetSortByOptions.values())
        {
            assert_().withFailureMessage(strSortBy.toString() + " should be in the SortBy list")
                    .that(lstSortByOptions.stream().anyMatch(e -> e.equals(strSortBy.toString())))
                    .isTrue();
        }

        //STEP 7
        assert_().withFailureMessage(String.format("%s should be displayed as default selected SortBy.", AssetSortByOptions.ASSET_NAME.toString()))
                .that(filterBarPage.closeSortByOptionList().getSelectedSortByOption())
                .isEqualTo(AssetSortByOptions.ASSET_NAME.toString());

        String unfavouriteAssetName = assetTableList.stream()
                .filter(e -> !e.getAssetCard().isAssetFavourite())
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No unfavourited assets was found"))
                .getAssetCard()
                .selectFavouriteButton()
                .getAssetName();

        //STEP 10
        LandingPage.getTopBar()
                .clickClientTab()
                .getLandingPage()
                .getVesselListPage()
                .getAssetCards()
                .forEach(e ->
                        assert_().withFailureMessage(String.format("%s should be marked as favourite.", e.getAssetName()))
                                .that(e.isFavourite())
                                .isTrue()
                );

        //STEP 11
        filterBarPage.getPageReference(LandingPage.class)
                .clickServiceScheduleTab()
                .getTabularView()
                .getAssetTables()
                .forEach(e ->
                        assert_().withFailureMessage(String.format("%s should be marked as favourite.", e.getAssetCard().getAssetName()))
                                .that(e.getAssetCard().isAssetFavourite())
                                .isTrue()
                );

        //STEP 12
        FilterAssetsPage filterAssetsPage = filterBarPage.clickFilterButton(FilterAssetsPage.class);
        assert_().withFailureMessage("My Favourites radio button should be selected.")
                .that(filterAssetsPage.isMyFavouritesRadioButtonSelected())
                .isTrue();

        //STEP 13
        filterAssetsPage.clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .filter(e -> e.getAssetCard().getAssetName().equals(unfavouriteAssetName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Selected asset marked as favourite not found."))
                .getAssetCard()
                .deselectFavouriteButton();

        //STEP 15 - Filter by Class Status
        filterBarPage.clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deSelectClassStatusCheckBoxByStatusName(ClassStatus.ALL.toString())
                .selectClassStatusCheckBoxByStatusName(ClassStatus.CLASSED.toString())
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .forEach(e ->
                        {
                            AssetCard assetCard = e.getAssetCard();
                            assert_().withFailureMessage(String.format("Class of %s should be %s.", assetCard.getAssetName(), assetCard.getClassStatus()))
                                    .that(assetCard.getClassStatus())
                                    .isEqualTo(ClassStatus.CLASSED.toString());
                        }
                );

        //STEP 16 - Filter by Flag
        filterBarPage.clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickFlagButton()
                .setSearchFlagTextBox(flagSearchString)
                .clickSubmitButton()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .map(AssetTable::getAssetCard)
                .forEach(assetCard ->
                        assert_().withFailureMessage(String.format("Flag of %s should be %s.", assetCard.getAssetName(), assetCard.getFlag()))
                                .that(assetCard.getFlag())
                                .isEqualTo(flagSearchString)
                );

        //STEP 18 - filter by asset type
        FilterByAssetTypePage filterByAssetTypePage = filterBarPage.clickFilterButton(FilterAssetsPage.class)
                .removeSelectedFlags()
                .clickAssetTypeButton();

        AssetType assetType = filterByAssetTypePage.getAssetCardsByTableName(NON_MERCHANTS.getTableName())
                .getAssetType()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("No assets were found of asset type %s", NON_MERCHANTS.getTableName())));
        assetType.selectAssetType();

        ReferenceData referenceData = new ReferenceData();
        Long parentIdOfThirdLevel = referenceData.getAssetType()
                .stream()
                .filter(e -> e.getName().equals(assetType.getAssetTypeName()) && e.getLevelIndication().equals(3))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected Asset type: %s is not found.", assetType.getAssetTypeName())))
                .getId();

        List<Long> parentIdsOfFourthLevel = referenceData.getAssetType()
                .stream()
                .filter(e -> e.getParentId() != null && e.getParentId().equals(parentIdOfThirdLevel))
                .map(AssetTypeHDto::getId)
                .collect(Collectors.toList());

        //The asset type on 5th level is displayed
        List<String> assetTypeNames = referenceData.getAssetType()
                .stream()
                .filter(e -> parentIdsOfFourthLevel.contains(e.getParentId()))
                .map(AssetTypeHDto::getName)
                .collect(Collectors.toList());

        filterByAssetTypePage.clickSubmitButton()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .forEach(e ->
                        assert_().withFailureMessage(String.format("This asset: %s should have asset type: %s", e.getAssetCard().getAssetName(),
                                NON_MERCHANTS.getTableName()))
                                .that(assetTypeNames)
                                .contains(e.getAssetCard().getAssetType())
                );

        //STEP 19 - filter by gross tonnage
        filterBarPage.clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .setMinGrossTonnage(String.valueOf(minGrossTonnage))
                .setMaxGrossTonnage(String.valueOf(maxGrossTonnage))
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .flatMap(l->Stream.of(l.getAssetCard().getAssetName()))
                .collect(Collectors.toList())
                .forEach(assetName ->
                        {
                            int grossTonnage = Integer.parseInt(LandingPage.getTopBar()
                                    .clickClientTab()
                                    .getLandingPage()
                                    .getFilterBarPage()
                                    .clickFilterButton(FilterAssetsPage.class)
                                    .selectAllVesselsRadioButton()
                                    .clickSubmitButton(FilterBarPage.class)
                                    .setSearchTextBox(assetName)
                                    .getPageReference(VesselListPage.class)
                                    .getAssetCards()
                                    .stream()
                                    .findFirst()
                                    .orElseThrow(() -> new NoSuchElementException(String.format("Asset card named %s is not found.", assetName)))
                                    .getGrossTonnage());

                            assert_().withFailureMessage(
                                    String.format("Assets with gross tonnage from %d to %d are expected to be displayed", minGrossTonnage, maxGrossTonnage))
                                    .that(grossTonnage)
                                    .isIn(closed(minGrossTonnage, maxGrossTonnage));
                        }
                );

        //STEP 20 - filter by date of build
        filterBarPage.getPageReference(LandingPage.class)
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .setFromDateOfBuild(fromDateOfBuild.format(FRONTEND_TIME_FORMAT_DTS))
                .setToDateOfBuild(toDateOfBuild.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .flatMap(assetTable->Stream.of(assetTable.getAssetCard().getAssetName()))
                .collect(Collectors.toList())
                .forEach(assetName ->
                        {
                            Date dateOfBuild = LandingPage.getTopBar()
                                    .clickClientTab()
                                    .getLandingPage()
                                    .getFilterBarPage()
                                    .clickFilterButton(FilterAssetsPage.class)
                                    .selectAllVesselsRadioButton()
                                    .clickSubmitButton(FilterBarPage.class)
                                    .setSearchTextBox(assetName)
                                    .getPageReference(VesselListPage.class)
                                    .getAssetCards()
                                    .stream()
                                    .findFirst()
                                    .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                                    .getDateOfBuild();

                            assert_().withFailureMessage(
                                    String.format("Assets with date of build from %s to %s are expected to be displayed", fromDateOfBuild, toDateOfBuild))
                                    .that(dateOfBuild)
                                    .isIn(closed(TestDateHelper.toDate(fromDateOfBuild), TestDateHelper.toDate(toDateOfBuild)));
                        }
                );

        //STEP 21 - filter by codicil type (condition of class)
        filterBarPage.getPageReference(LandingPage.class)
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deselectCodicilTypeAll()
                .selectCodicilTypeConditionOfClass()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .map(map -> map.getAssetCard().getAssetName())
                .collect(Collectors.toList())
                .forEach(assetName ->
                        {
                            int codicilsCount = LandingPage.getTopBar()
                                    .clickClientTab()
                                    .getLandingPage()
                                    .getFilterBarPage()
                                    .clickFilterButton(FilterAssetsPage.class)
                                    .selectAllVesselsRadioButton()
                                    .clickSubmitButton(FilterBarPage.class)
                                    .setSearchTextBox(assetName)
                                    .getPageReference(VesselListPage.class)
                                    .getAssetCards()
                                    .stream()
                                    .findFirst()
                                    .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                                    .clickViewAssetButton()
                                    .getCodicilCard()
                                    .clickViewAllButton()
                                    .clickConditionsOfClassTab()
                                    .getCodicilElements()
                                    .size();

                            assert_().withFailureMessage("This asset is expected to have Condition of Class.")
                                    .that(codicilsCount)
                                    .isGreaterThan(0);
                        }
                );

        //STEP 22 filter by codicil type (Actionable items)
        LandingPage.getTopBar()
                .clickClientTab()
                .getLandingPage()
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deselectCodicilTypeAll()
                .selectCodicilTypeActionableItem()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .flatMap(assetTable-> Stream.of(assetTable.getAssetCard().getAssetName()))
                .collect(Collectors.toList())
                .forEach(assetName ->
                        {
                            int codicilsCount = LandingPage.getTopBar()
                                    .clickClientTab()
                                    .getLandingPage()
                                    .getFilterBarPage()
                                    .clickFilterButton(FilterAssetsPage.class)
                                    .selectAllVesselsRadioButton()
                                    .clickSubmitButton(FilterBarPage.class)
                                    .setSearchTextBox(assetName)
                                    .getPageReference(VesselListPage.class)
                                    .getAssetCards()
                                    .stream()
                                    .findFirst()
                                    .orElseThrow(() -> new NoSuchElementException(String.format("Asset card named %s card not found.", assetName)))
                                    .clickViewAssetButton()
                                    .getCodicilCard()
                                    .clickViewAllButton()
                                    .clickActionableItemsTab()
                                    .getCodicilElements()
                                    .size();

                            assert_().withFailureMessage("This asset is expected to have Actionable Items.")
                                    .that(codicilsCount)
                                    .isGreaterThan(0);
                        }
                );

        //STEP 23-25 filter by Service Type
        FilterByServiceTypePage filterByServiceTypePage = LandingPage.getTopBar()
                .clickClientTab()
                .getLandingPage()
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickServiceTypeButton();

        ServiceTable serviceTable = filterByServiceTypePage.clickClearAllButton()
                .getServiceTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected service table not found."))
                .expandServiceTable();

        String productName = serviceTable.getProductName();
        String serviceName = serviceTable.getServices()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected list of services is empty."))
                .selectCheckbox()
                .getServiceName();

        filterByServiceTypePage.clickSubmitButton(FilterAssetsPage.class)
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .forEach(e ->
                        {
                            assert_().withFailureMessage(
                                    String.format("This asset %s does not have this product: %s.", e.getAssetCard().getAssetName(), productName))
                                    .that(e.expand().getServiceTables()
                                            .stream()
                                            .map(m -> m.getProductName())
                                            .collect(Collectors.toList()))
                                    .contains(productName);

                            boolean isServiceExists = e.getServiceTables()
                                    .stream()
                                    .filter(p -> p.getProductName().equals(productName))
                                    .findFirst()
                                    .orElseThrow(() -> new NoSuchElementException(String.format("No product named %s was found", productName)))
                                    .expand()
                                    .getServices()
                                    .stream()
                                    .anyMatch(s -> s.getServiceName().contains(serviceName));

                            assert_().withFailureMessage(String.format("Assets containing this service: %s are expected to be displayed", serviceName))
                                    .that(isServiceExists)
                                    .isTrue();
                        }
                );
    }
}
