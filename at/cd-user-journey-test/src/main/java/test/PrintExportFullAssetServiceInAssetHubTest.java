package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.assetdetail.AssetDetailsPage;
import main.common.export.ExportAssetListingPage;
import main.common.export.element.SurveysNotesAndActionsTab;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.util.Collections;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.GENERATE_AND_PRINT;
import static java.util.Arrays.asList;

public class PrintExportFullAssetServiceInAssetHubTest extends BaseTest
{
    @Test(description = "User can export asset information from the asset hub for a single asset")
    @Issue("LRCDT-1669")
    @Features(GENERATE_AND_PRINT)
    public void printExportFullAssetServiceInAssetHubTest()
    {
        AssetDetailsPage assetDetailsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getAssetHeaderPage()
                .clickAssetDetailsButton();

        assert_().withFailureMessage("Export Asset Information card should be displayed.")
                .that(assetDetailsPage.getExportAssetInformationCard().exists())
                .isTrue();

        // STEP 4-7
        ExportAssetListingPage exportAssetListingPage = assetDetailsPage.getExportAssetInformationCard()
                .clickExportAssetInformationButton()
                .clickCancelButton(AssetDetailsPage.class)
                .getExportAssetInformationCard()
                .clickExportAssetInformationButton();

        // STEP 8 - Select 'Full Asset Information' radiobutton
        assert_().withFailureMessage("'Full Asset Information' radiobutton should be selected.")
                .that(exportAssetListingPage.clickInformationTab()
                        .selectFullAssetInfoRadioButton()
                        .isFullAssetInfoRadioButtonSelected())
                .isTrue();

        // STEP 9 - Services and Codicils tab
        SurveysNotesAndActionsTab surveysNotesAndActionsTab = exportAssetListingPage.surveysNotesAndActionsTab();
        assert_().withFailureMessage("'Full Survey Information' radiobutton should be selected.")
                .that(surveysNotesAndActionsTab.selectFullSurveyInfoRadioButton().isFullSurveyRadioButtonSelected())
                .isTrue();
        assert_().withFailureMessage("'Asset Note' checkbox should be deselected.")
                .that(surveysNotesAndActionsTab.deselectAssetNoteCheckbox().isAssetNoteCheckboxSelected())
                .isFalse();

        // STEP 10 - Export asset - There is no fixed checksum since the file name is changing
        exportAssetListingPage.clickExportButton();

        assert_().withFailureMessage(String.format("%s should be exported successfully.", LAURA.getName()))
                .that(DownloadReportHelper.getDownloadFileChecksum(getToken()))
                .isNotEmpty();

        // STEP 11-14 - The content of the pdf file cannot be verified.
    }

    private String getToken()
    {
        AssetQueryHDto assetQueryHDto = new AssetQueryHDto(); // empty assetquery

        AssetExportDto assetExportDto = new AssetExportDto();
        assetExportDto.setIncludes(Collections.singletonList(LAURA.getAssetCode()));
        assetExportDto.setAssetQuery(assetQueryHDto);
        assetExportDto.setInfo(2L);
        assetExportDto.setService(3L);
        assetExportDto.setCodicils(asList(1L, 4L, 2L));
        return new Export().getExportAssetToken(assetExportDto);
    }
}
