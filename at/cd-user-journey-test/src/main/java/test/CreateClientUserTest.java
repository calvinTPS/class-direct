package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.administration.adduser.client.SelectClientPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.administration.adduser.steps.ConfirmAccountCreationPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.Client.CLIENT;
import static constant.User.NON_LR_EMAIL;
import static constant.User.UserAccountTypes;

public class CreateClientUserTest extends BaseTest
{
    @Test(description = "Login as an LR Administrator and create a Client user")
    @Issue("LRCDT-2193")
    @Features(USER_MANAGEMENT)
    public void createClientUserTest()
    {
        AdministrationPage administrationPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();

        assert_().withFailureMessage("Cannot create client user that is already existing.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(NON_LR_EMAIL))
                        .count())
                .isEqualTo(0L);

        administrationPage.clickAddUserButton()
                .setEmailAddress(NON_LR_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User card for %s was not found.", NON_LR_EMAIL)))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.CLIENT.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Account type %s was not found.", UserAccountTypes.CLIENT.toString())))
                .clickSelectButton(SelectClientPage.class)
                .setClientCodeTextbox(CLIENT.getCode())
                .getClientTypeAheadOptions()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Client code %s was not found.", CLIENT.getCode())))
                .clickArrowButton()
                .setAccountExpiryDate(now.format(FRONTEND_TIME_FORMAT_DTS))
                .getFooterPage()
                .clickConfirmButton(ConfirmAccountCreationPage.class)
                .getFooterPage()
                .clickConfirmButton(AdministrationPage.class);

        assert_().withFailureMessage("Newly created client user should be displayed in the list.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(NON_LR_EMAIL))
                        .count())
                .isGreaterThan(0L);
    }
}
