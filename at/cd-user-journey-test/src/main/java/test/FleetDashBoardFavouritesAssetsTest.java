package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;
import java.util.stream.IntStream;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FleetDashBoardFavouritesAssetsTest extends BaseTest
{
    @Test(description = "Favourites assets marked as Favourite and filter assets by codicils")
    @Issue("LRCDT-705")
    @Features(FLEET_DASHBOARD)
    public void fleetDashBoardFavouritesAssetsTest()
    {
        // Mark first asset as favourite if none is presented
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .setSearchTextBox("test")
                .selectSearchDropDown(ClassDirect.AssetSearchOptions.ASSET_NAME_IMO_NUMBER.toString())
                .getPageReference(VesselListPage.class);
        List<AssetCard> assetCardList = vesselListPage.getAssetCards();
        if (!assetCardList.get(0).isFavourite())
        {
            IntStream.of(0, 5, 10).forEach(i -> assetCardList.get(i).selectFavourite());
            getDriver().navigate().refresh();
        }

        // STEP 3: Verify that My Favourites filter is selected
        FilterAssetsPage filterAssetsPage = vesselListPage.getFilterBarPage().clickFilterButton(FilterAssetsPage.class);
        assert_().withFailureMessage("My Favourites radio button is not selected by default.")
                .that(filterAssetsPage.isMyFavouritesRadioButtonSelected())
                .isTrue();

        // STEP 4: Verify that 'Showing X from Y' message is displayed
        vesselListPage = filterAssetsPage.clickSubmitButton(VesselListPage.class);
        assert_().withFailureMessage("Pagination total count should be displayed.")
                .that(vesselListPage.getShowingXofYPage().getTotal())
                .isAtLeast(1);
        assert_().withFailureMessage("Pagination shown count should be displayed.")
                .that(vesselListPage.getShowingXofYPage().getShown())
                .isAtLeast(1);

        // STEP 5: Verify that only favourite assets are displayed
        assert_().withFailureMessage("Non-favourite assets are displayed by default.")
                .that(vesselListPage.getAssetCards().stream().allMatch(AssetCard::isFavourite))
                .isTrue();

        // STEP 6: Verify Codicil Type filters are displayed
        vesselListPage.getFilterBarPage().clickFilterButton(FilterAssetsPage.class);
        assert_().withFailureMessage("Codicil type 'All' has not been found in Filter asset.")
                .that(filterAssetsPage.isCodicilTypeAllCheckboxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Codicil type 'Conditions of Class' has not been found in Filter asset.")
                .that(filterAssetsPage.isCodicilTypeConditionsOfClassCheckboxDisplayed())
                .isTrue();

        assert_().withFailureMessage("Codicil type 'Actionable Items' has not been found in Filter asset.")
                .that(filterAssetsPage.isCodicilTypeActionableItemsCheckboxDisplayed())
                .isTrue();

        // STEP 7: Verify Codicil Type filters are selected
        assert_().withFailureMessage("Codicil type 'All' is not selected by default.")
                .that(filterAssetsPage.isCodicilTypeAllCheckboxSelected())
                .isFalse();

        assert_().withFailureMessage("Codicil type 'Conditions of Class' is not selected by default.")
                .that(filterAssetsPage.isCodicilTypeConditionsOfClassCheckboxSelected())
                .isFalse();

        assert_().withFailureMessage("Codicil type 'Actionable Items' is not selected by default.")
                .that(filterAssetsPage.isCodicilTypeActionableItemsCheckboxSelected())
                .isFalse();

        filterAssetsPage.
                selectAllVesselsRadioButton()
                .deselectCodicilTypeAll()
                .selectCodicilTypeConditionOfClass()
                .clickSubmitButton(VesselListPage.class);

        assert_().withFailureMessage("Asset with Codicil Type COC is expected to be displayed")
                .that(vesselListPage.getAssetCards().size())
                .isGreaterThan(0);

        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .deselectConditionOfClass()
                .selectCodicilTypeActionableItem()
                .clickSubmitButton(VesselListPage.class);

        assert_().withFailureMessage("Asset with Codicil Type AI is expected to be displayed")
                .that(vesselListPage.getAssetCards().size())
                .isGreaterThan(0);

    }
}
