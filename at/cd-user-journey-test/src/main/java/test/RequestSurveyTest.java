package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect.RequestSurveyData;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.requestsurvey.RequestSurveyPage;
import main.requestsurvey.services.BaseServiceTable;
import main.requestsurvey.services.RowElement;
import main.requestsurvey.services.ServiceElement;
import main.requestsurvey.sub.ConfirmSubPage;
import main.requestsurvey.sub.RequestSuccessfulSubPage;
import main.requestsurvey.sub.SelectLocationAndDatesSubPage;
import main.requestsurvey.sub.SelectServicesSubPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.RequestSurveyData.*;
import static constant.Epic.REQUEST_SURVEY;

public class RequestSurveyTest extends BaseTest
{
    private String portDetails, serviceProductName, serviceName, serviceDueDate,
            serviceAssignedDate, serviceDueStatus, serviceRangeDateFrom, serviceRangeDateTo;

    @Test(description = "Request Survey")
    @Issue("LRCDT-610")
    @Features(REQUEST_SURVEY)
    public void requestSurveyTest()
    {
        RequestSurveyPage requestSurveyPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getFilterBarPage()
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .get(0)
                .clickRequestSurveyButton();

        //STEP 4
        assert_().withFailureMessage("Select Services should be highlighted in the bar")
                .that(requestSurveyPage.isSelectServicesTabHighlighted())
                .isTrue();

        //STEP 5 to 8
        SelectServicesSubPage selectServicesSubPage = requestSurveyPage.getSelectServicesSubPage();
        BaseServiceTable serviceTable = selectServicesSubPage.getServiceTable();
        ServiceElement serviceElement = serviceTable.getRows().get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows().get(0)
                .selectCheckBox();
        serviceProductName = serviceTable.getRows().get(0).getProductName();
        serviceName = serviceElement.getServiceName();
        serviceDueDate = serviceElement.getDueDate();
        serviceAssignedDate = serviceElement.getAssignedDate();
        serviceRangeDateFrom = serviceElement.getRangeDateFrom();
        serviceRangeDateTo = serviceElement.getRangeDateTo();
        serviceDueStatus = serviceElement.getDueStatus();

        assert_().withFailureMessage("Continue button should be enabled")
                .that(selectServicesSubPage.isContinueButtonDisabled())
                .isFalse();

        //STEP 9
        SelectLocationAndDatesSubPage selectLocationAndDatesSubPage = selectServicesSubPage.clickContinueButton();

        //STEP 11
        selectLocationAndDatesSubPage.selectPortName(RequestSurveyData.portName);
        portDetails = selectLocationAndDatesSubPage.getPortDetails();
        assert_().withFailureMessage("Continue button should be enabled")
                .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                .isFalse();

        //STEP 12 to 24
        selectLocationAndDatesSubPage.setEstimatedDateOfArrival(arrivalDate.format(FRONTEND_TIME_FORMAT_DTS))
                .setSurveyStartDate(surveyStartDate.format(FRONTEND_TIME_FORMAT_DTS))
                .setEstimatedDateOfDeparture(departureDate.format(FRONTEND_TIME_FORMAT_DTS))
                .setArrivalTime(RequestSurveyData.arrivalTime)
                .setDepartureTime(RequestSurveyData.departureTime)
                .setAgentName(RequestSurveyData.agentName)
                .setAgentPhoneNumber(RequestSurveyData.agentPhoneNumber)
                .setAgentEmail(RequestSurveyData.agentEmailAddress)
                .setBerthAnchorage(RequestSurveyData.berthAnchorage)
                .setShipContactDetails(RequestSurveyData.shipContactDetails);
        assertSelectLocationAndDatesSubPage(selectLocationAndDatesSubPage);

        //STEP 26
        selectLocationAndDatesSubPage.getServicesAddedPage()
                .clickEditSurveysButton()
                .clickContinueButton();

        //STEP 27
        assert_().withFailureMessage("Continue button should be enabled")
                .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                .isFalse();
        assert_().withFailureMessage("Correct Port name should be displayed")
                .that(selectLocationAndDatesSubPage.getSelectedPortName().equals(RequestSurveyData.portName))
                .isTrue();
        assertSelectLocationAndDatesSubPage(selectLocationAndDatesSubPage);

        //STEP 28 to 29
        ConfirmSubPage confirmSubPage = selectLocationAndDatesSubPage
                .clickContinueButton();
        assertConfirmSubPage(confirmSubPage);

        //STEP 30 to 32
        confirmSubPage = confirmSubPage.clickEditDetailsButton()
                .clickContinueButton();
        assertConfirmSubPage(confirmSubPage);

        //STEP 35

        confirmSubPage.clickShareToOthersButton();
        assert_().withFailureMessage("Correct helper text for email textbox should be displayed")
                .that(confirmSubPage.getEmailHelperText().equals(RequestSurveyData.emailHelperText))
                .isTrue();

        //STEP 33 to 34
        assert_().withFailureMessage("Correct helper text for comment textarea should be displayed")
                .that(confirmSubPage.getCommentHelperText().equals(RequestSurveyData.commentHelperText))
                .isTrue();
        confirmSubPage.setComments(RequestSurveyData.comments);
        assert_().withFailureMessage("Correct comments should be displayed")
                .that(confirmSubPage.getComments().equals(RequestSurveyData.comments))
                .isTrue();

        //STEP 36
        confirmSubPage.setEmailForShareToOthers(RequestSurveyData.emailForShareToOthers);
        assert_().withFailureMessage("Correct email for share to others should be displayed")
                .that(confirmSubPage.getEmailForShareToOthers(0).contains(RequestSurveyData.emailForShareToOthers))
                .isTrue();

        //STEP 37
        RequestSuccessfulSubPage requestSuccessfulSubPage = confirmSubPage.clickContinueButton();
        assert_().withFailureMessage("Request sent successfully should be displayed")
                .that(requestSuccessfulSubPage.getRequestSentSuccessfulText().equals(RequestSurveyData.requestSuccessfulMessage))
                .isTrue();
    }

    private void assertSelectLocationAndDatesSubPage(SelectLocationAndDatesSubPage selectLocationAndDatesSubPage)
    {
        assert_().withFailureMessage("Port details should be displayed")
                .that(selectLocationAndDatesSubPage.getPortDetails().isEmpty())
                .isFalse();
        assert_().withFailureMessage("Correct arrival date should be displayed")
                .that(LocalDate.parse(selectLocationAndDatesSubPage.getArrivalDate(), FRONTEND_TIME_FORMAT_DTS)
                        .equals(arrivalDate))
                .isTrue();
        assert_().withFailureMessage("Correct survey start date should be displayed")
                .that(LocalDate.parse(selectLocationAndDatesSubPage.getSurveyStartDate(), FRONTEND_TIME_FORMAT_DTS)
                        .equals(surveyStartDate))
                .isTrue();
        assert_().withFailureMessage("Correct departure date should be displayed")
                .that(LocalDate.parse(selectLocationAndDatesSubPage.getDepartureDate(), FRONTEND_TIME_FORMAT_DTS)
                        .equals(departureDate))
                .isTrue();
        assert_().withFailureMessage("Correct arrival time should be displayed")
                .that(selectLocationAndDatesSubPage.getArrivalTime().equals(RequestSurveyData.arrivalTime))
                .isTrue();
        assert_().withFailureMessage("Correct departure time should be displayed")
                .that(selectLocationAndDatesSubPage.getDepartureTime().equals(RequestSurveyData.departureTime))
                .isTrue();
        assert_().withFailureMessage("Correct agent name should be displayed")
                .that(selectLocationAndDatesSubPage.getAgentName().equals(RequestSurveyData.agentName))
                .isTrue();
        assert_().withFailureMessage("Correct agent phone number should be displayed")
                .that(selectLocationAndDatesSubPage.getAgentPhoneNumber().equals(RequestSurveyData.agentPhoneNumber))
                .isTrue();
        assert_().withFailureMessage("Correct agent email address should be displayed")
                .that(selectLocationAndDatesSubPage.getAgentEmailAddress().equals(RequestSurveyData.agentEmailAddress))
                .isTrue();
        assert_().withFailureMessage("Correct berth/anchorage should be displayed")
                .that(selectLocationAndDatesSubPage.getBerthAnchorage().equals(RequestSurveyData.berthAnchorage))
                .isTrue();
        assert_().withFailureMessage("Correct ship contact details should be displayed")
                .that(selectLocationAndDatesSubPage.getShipContactDetails().equals(RequestSurveyData.shipContactDetails))
                .isTrue();

        //STEPS 22-24
        BaseServiceTable servicesAddedTable = selectLocationAndDatesSubPage.getServicesAddedPage().getServicesAddedTable();
        RowElement rowElement = servicesAddedTable.getRows().get(0);
        ServiceElement serviceElement = rowElement.clickPlusOrMinusIcon()
                .getServiceRows().get(0);

        assert_().withFailureMessage("Correct service product name should be displayed")
                .that(servicesAddedTable.getRows().get(0).getProductName().equals(serviceProductName))
                .isTrue();
        assert_().withFailureMessage("Correct service name should be displayed in the table")
                .that(serviceElement.getServiceName().equals(serviceName))
                .isTrue();
        assert_().withFailureMessage("Correct service due date should be displayed in the table")
                .that(serviceElement.getDueDate().equals(serviceDueDate))
                .isTrue();
        assert_().withFailureMessage("Correct service assigned date should be displayed in the table")
                .that(serviceElement.getAssignedDate().equals(serviceAssignedDate))
                .isTrue();
        assert_().withFailureMessage("Correct service range date (from) should be displayed in the table")
                .that(serviceElement.getRangeDateFrom().equals(serviceRangeDateFrom))
                .isTrue();
        assert_().withFailureMessage("Correct service range date (to) should be displayed in the table")
                .that(serviceElement.getRangeDateTo().equals(serviceRangeDateTo))
                .isTrue();
        assert_().withFailureMessage("Correct service due status should be displayed in the table")
                .that(serviceElement.getDueDate().equals(serviceDueStatus))
                .isTrue();

        rowElement.clickPlusOrMinusIcon();
        assert_().withFailureMessage("The service table should be minimized")
                .that(rowElement.isRowExpanded())
                .isFalse();

    }

    private void assertConfirmSubPage(ConfirmSubPage confirmSubPage)
    {
        assert_().withFailureMessage("Correct port details should be displayed")
                .that(confirmSubPage.getPortDetails().equals(portDetails))
                .isTrue();
        assert_().withFailureMessage("Correct arrival date should be displayed")
                .that(LocalDate.parse(confirmSubPage.getArrivalDate(), FRONTEND_TIME_FORMAT_DTS)
                        .equals(arrivalDate))
                .isTrue();
        assert_().withFailureMessage("Correct survey start date should be displayed")
                .that(LocalDate.parse(confirmSubPage.getSurveyStartDate(), FRONTEND_TIME_FORMAT_DTS)
                        .equals(surveyStartDate))
                .isTrue();
        assert_().withFailureMessage("Correct departure date should be displayed")
                .that(LocalDate.parse(confirmSubPage.getDepartureDate(), FRONTEND_TIME_FORMAT_DTS)
                        .equals(departureDate))
                .isTrue();
        assert_().withFailureMessage("Correct arrival time should be displayed")
                .that(confirmSubPage.getArrivalTime().equals(RequestSurveyData.arrivalTime))
                .isTrue();
        assert_().withFailureMessage("Correct departure time should be displayed")
                .that(confirmSubPage.getDepartureTime().equals(RequestSurveyData.departureTime))
                .isTrue();
        assert_().withFailureMessage("Correct agent name should be displayed")
                .that(confirmSubPage.getAgentName().equals(RequestSurveyData.agentName))
                .isTrue();
        assert_().withFailureMessage("Correct agent phone number should be displayed")
                .that(confirmSubPage.getAgentPhoneNumber().equals(RequestSurveyData.agentPhoneNumber))
                .isTrue();
        assert_().withFailureMessage("Correct agent email address should be displayed")
                .that(confirmSubPage.getAgentEmailAddress().equals(RequestSurveyData.agentEmailAddress))
                .isTrue();
        assert_().withFailureMessage("Correct berth/anchorage should be displayed")
                .that(confirmSubPage.getBerthAnchorage().equals(RequestSurveyData.berthAnchorage))
                .isTrue();
        assert_().withFailureMessage("Correct ship contact details should be displayed")
                .that(confirmSubPage.getShipContactDetails().equals(RequestSurveyData.shipContactDetails))
                .isTrue();
    }
}
