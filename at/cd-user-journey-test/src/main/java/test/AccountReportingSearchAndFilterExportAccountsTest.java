package test;

import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.User.UserAccountTypes;
import constant.User.UserSearchOptions;
import helper.DownloadReportHelper;
import main.administration.AdministrationPage;
import main.administration.export.ExportAccountReportPage;
import main.administration.export.element.InformationTab;
import main.administration.export.element.UserCard;
import main.administration.filter.FilterUserPage;
import main.administration.filter.element.AccountTypeCheckbox;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Credentials.SHIP_BUILDER_USER;
import static constant.Epic.ACCOUNT_REPORTING;
import static constant.User.Client;
import static constant.User.Flag.ANGOLA;
import static constant.User.UserSearchOptions.*;

public class AccountReportingSearchAndFilterExportAccountsTest extends BaseTest
{
    @Test(description = "As an LR Admin user, I'm able to search and filter the list of users and export the information of one or more users")
    @Issue("LRCDT-1888")
    @Features(ACCOUNT_REPORTING)
    public void accountReportingSearchAndFilterExportAccountsTest()
    {
        final String userEmailSearchString = "gmail";
        final String userNameSearchString = "demo";

        FilterBarPage filterBarPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickExportAccountReportButton()
                .clickCloseButton()
                .getFilterBarPage();

        assert_().withFailureMessage(String.format("Default search category should be %s.", USER_EMAIL_ADDRESS.toString()))
                .that(filterBarPage.getSelectedSearchOption())
                .isEqualTo(USER_EMAIL_ADDRESS.toString());

        // STEP 6 - Check User Search options if all displayed
        assert_().withFailureMessage("All User search options should be listed.")
                .that(Arrays.stream(UserSearchOptions.values()).map(UserSearchOptions::toString).collect(Collectors.toList()))
                .containsAllIn(filterBarPage.getSearchOptionList());

        filterBarPage.closeSearchOptionList();

        // additional search options
        searchUsers(USER_EMAIL_ADDRESS, filterBarPage, userEmailSearchString);
        searchUsers(USER_NAME, filterBarPage, userNameSearchString);
        searchUsers(COMPANY_NAME, filterBarPage, ANGOLA.getName());

        // STEP 5,7,8 - Search by Client code, Ship builder code and Flag code
        searchUsers(CLIENT_CODE, filterBarPage, Client.CLIENT.getCode());
        searchUsers(SHIP_BUILDER_CODE, filterBarPage, Client.SHIP_BUILDER.getCode());
        searchUsers(FLAG_CODE, filterBarPage, ANGOLA.getCode());

        // STEP 9-10 - clear filters
        FilterUserPage filterUserPage = filterBarPage.selectSearchDropDown(FLAG_CODE.toString())
                .clickClearButtonInSearchTextBox()
                .clickFilterButton(FilterUserPage.class);

        // STEP 11 - Verify Account types if all displayed
        assert_().withFailureMessage("All account types should be listed in Filter User panel.")
                .that(Arrays.stream(UserAccountTypes.values()).map(UserAccountTypes::toString).collect(Collectors.toList()))
                .containsAllIn(filterUserPage.getAccountTypeCheckboxList()
                        .stream()
                        .map(AccountTypeCheckbox::getAccountTypeName)
                        .collect(Collectors.toList()));

        // Verify account types if selected by default
        filterUserPage.getAccountTypeCheckboxList()
                .forEach(e -> assert_().withFailureMessage(String.format("By default, %s should be selected.", e.getAccountTypeName()))
                        .that(e.isCheckboxSelected())
                        .isTrue());

        // STEP 12 - Deselect ALL
        filterUserPage.deselectAccountTypeCheckbox(UserAccountTypes.ALL)
                .getAccountTypeCheckboxList()
                .forEach(e -> assert_().withFailureMessage("All other account types should be deselected when ALL is deselected.")
                        .that(e.isCheckboxSelected())
                        .isFalse());

        // STEP 13 - Select ALL
        filterUserPage.selectAccountTypeCheckbox(UserAccountTypes.ALL)
                .getAccountTypeCheckboxList()
                .forEach(e -> assert_().withFailureMessage("All other account types should be selected when ALL is selected.")
                        .that(e.isCheckboxSelected())
                        .isTrue());

        // STEP 14 - Filter by Account type (Ship builder)
        AdministrationPage administrationPage = filterUserPage.deselectAccountTypeCheckbox(UserAccountTypes.ALL)
                .selectAccountTypeCheckbox(UserAccountTypes.SHIP_BUILDER)
                .clickSubmitButton(AdministrationPage.class)
                .getFilterBarPage()
                .selectSearchDropDown(FLAG_CODE.toString())
                .getPageReference(AdministrationPage.class);

        administrationPage.getAdministrationTable()
                .getDataElements()
                .forEach(e -> assert_().withFailureMessage("Users should be filtered by Ship builder.")
                        .that(e.getRoleName().toLowerCase())
                        //regex to accommodate entries such as " xxxx, ShipBuilder, yyyyy, zzzz "
                        .containsMatch("[\\s,]*" + UserAccountTypes.SHIP_BUILDER.toString().toLowerCase() + "[\\s,]*"));

        // STEP 15 - Filter by Date of Last Login
        ExportAccountReportPage exportAccountReportPage = administrationPage.getFilterBarPage()
                .selectSearchDropDown(FLAG_CODE.toString())
                .clickFilterButton(FilterUserPage.class)
                .clickClearFiltersButton()
                .getPageReference(AdministrationPage.class)
                .clickExportAccountReportButton();

        InformationTab informationTab = exportAccountReportPage.clickInformationTab();
        exportAccountReportPage.clickUsersTab();
        exportAccountReportPage.clickInformationTab();
        String lastLoginDateFrom = informationTab.getLastLoginDateFrom();
        String lastLoginDateTo = informationTab.getLastLoginDateTo();

        exportAccountReportPage.clickCloseButton()
                .getFilterBarPage()
                .clickFilterButton(FilterUserPage.class)
                .setLastLoginDateFrom(lastLoginDateFrom)
                .setLastLoginDateTo(lastLoginDateTo)
                .clickSubmitButton(AdministrationPage.class);

        assert_().withFailureMessage("Filter by Last login date is expected to have result.")
                .that(administrationPage.getAdministrationTable().getDataElements().size())
                .isGreaterThan(0);

        int userCount = filterBarPage.clickFilterButton(FilterUserPage.class)
                .clickClearFiltersButton()
                .getPageReference(AdministrationPage.class)
                .getAdministrationTable()
                .getDataElements()
                .size();

        // STEP 16
        exportAccountReportPage = filterBarPage.clickFilterButton(FilterUserPage.class)
                .clickClearFiltersButton()
                .getPageReference(AdministrationPage.class)
                .clickExportAccountReportButton();

        assert_().withFailureMessage("All users in Administration page should be displayed.")
                .that(exportAccountReportPage.getUsersTab().getUserCards().size())
                .isEqualTo(userCount);

        // STEP 17 - By default, all users should be selected
        List<UserCard> userCards = exportAccountReportPage.getUsersTab().getUserCards();
        userCards.forEach(e -> assert_().withFailureMessage(String.format("This user: %s is expected to be selected, by default.", e.getName()))
                .that(e.isUserCardSelected())
                .isTrue());

        // Deselect a user
        UserCard selectedUserCard = userCards.stream()
                .filter(e -> e.getUserEmail().equals(SHIP_BUILDER_USER.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected user email not found."))
                .deselectUserCard();

        assert_().withFailureMessage(String.format("This user with email %s is expected to be deselected.", SHIP_BUILDER_USER.getEmail()))
                .that(selectedUserCard.isUserCardSelected())
                .isFalse();

        // STEP 18 - Select the user again
        assert_().withFailureMessage(String.format("This user with email %s is expected to be selected.", SHIP_BUILDER_USER.getEmail()))
                .that(selectedUserCard.selectUserCard().isUserCardSelected())
                .isTrue();

        //STEP 19 - Export as CSV
        String exportUserToken = new Export().getExportUserToken(new UserProfileDto());
        assert_().withFailureMessage("User listing should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(exportUserToken))
                .isNotEmpty();

        // STEP 20 - Verify content - will not verify content
    }

    private void searchUsers(UserSearchOptions userSearchOption, FilterBarPage filterBarPage, String searchString)
    {
        filterBarPage.selectSearchDropDown(userSearchOption.toString())
                .setSearchTextBox(searchString)
                .getPageReference(AdministrationPage.class)
                .getAdministrationTable()
                .getDataElements()
                .forEach(e ->
                {
                    String searchFor;
                    switch (userSearchOption)
                    {
                        case USER_EMAIL_ADDRESS:
                            searchFor = e.getUserEmail();
                            break;
                        case USER_NAME:
                            searchFor = e.getUserName();
                            break;
                        case COMPANY_NAME:
                            searchFor = e.getCompanyName();
                            break;
                        default:
                            searchFor = e.getShipBuilderClientFlagCode();
                            break;
                    }

                    assert_().withFailureMessage(String.format("%s should contain %s.", userSearchOption.toString(), searchString))
                            .that(searchFor.toLowerCase())
                            .contains(searchString.toLowerCase());
                });
    }
}
