package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.administration.adduser.shipbuilder.SelectShipBuilderPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.administration.adduser.steps.ConfirmAccountCreationPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.Client.SHIP_BUILDER_DSME;
import static constant.User.SHIP_BUILDER_EMAIL;
import static constant.User.UserAccountTypes;

public class CreateShipBuilderUser extends BaseTest
{
    @Test(description = "Login as an LR Administrator and create a Ship builder user")
    @Issue("LRCDT-2194")
    @Features(USER_MANAGEMENT)
    public void createShipBuilderUser()
    {
        AdministrationPage administrationPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();

        assert_().withFailureMessage("Cannot create ship builder user that is already existing.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(SHIP_BUILDER_EMAIL))
                        .count())
                .isEqualTo(0L);

        administrationPage.clickAddUserButton()
                .setEmailAddress(SHIP_BUILDER_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User card for %s was not found.", SHIP_BUILDER_EMAIL)))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.SHIP_BUILDER.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Account type %s was not found.", UserAccountTypes.SHIP_BUILDER.toString())))
                .clickSelectButton(SelectShipBuilderPage.class)
                .setShipBuilderCodeTextbox(SHIP_BUILDER_DSME.getCode())
                .getShipBuilderTypeAheadOptions()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Client code %s was not found.", SHIP_BUILDER_DSME.getCode())))
                .clickArrowButton()
                .setAccountExpiryDate(now.format(FRONTEND_TIME_FORMAT_DTS))
                .getFooterPage()
                .clickConfirmButton(ConfirmAccountCreationPage.class)
                .getFooterPage()
                .clickConfirmButton(AdministrationPage.class);

        assert_().withFailureMessage("Newly created ship builder user should be displayed in the list.")
                .that(administrationPage.getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getUserEmail().equals(SHIP_BUILDER_EMAIL))
                        .count())
                .isGreaterThan(0L);
    }
}
