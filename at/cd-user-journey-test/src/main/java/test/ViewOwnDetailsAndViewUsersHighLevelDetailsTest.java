package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.administration.element.DataElement;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.myaccount.MyAccountPage;
import main.myaccount.element.PersonalDetailsCard;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class ViewOwnDetailsAndViewUsersHighLevelDetailsTest extends BaseTest
{
    private final List<String> rolesWithCode = Arrays.asList("Ship builder", "Client", "Flag");

    @Test(description = "As an Admin user, the user can view his own details and view high level details of users associated to him")
    @Issue("LRCDT-1084")
    @Features(FLEET_DASHBOARD)
    public void viewOwnDetailsAndViewUsersHighLevelDetailsTest()
    {
        MyAccountPage myAccountPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getTopBar()
                .clickMyAccountButton();

        // STEP 2 - System header - already covered by @Visible annotation

        // STEP 3
        PersonalDetailsCard personalDetailsCard = myAccountPage.getPersonalDetailsCard();

        assert_().withFailureMessage("Full name should be displayed")
                .that(myAccountPage.getFullName())
                .isNotEmpty();
        assert_().withFailureMessage("Account Type should be displayed")
                .that(myAccountPage.getAccountType())
                .isNotEmpty();
        assert_().withFailureMessage("Email Address should be displayed")
                .that(personalDetailsCard.getEmailAddress())
                .isNotEmpty();
        // telephone is optional - no assertion needed
        assert_().withFailureMessage("Company Name should be displayed")
                .that(personalDetailsCard.getCompanyName())
                .isNotEmpty();
        assert_().withFailureMessage("Address line 1 should be displayed")
                .that(personalDetailsCard.getAddressLine1())
                .isNotEmpty();
        // address line 2 and line 3 are optional - no assertion needed
        assert_().withFailureMessage("City should be displayed")
                .that(personalDetailsCard.getCity())
                .isNotEmpty();
        assert_().withFailureMessage("State/Province/Region should be displayed")
                .that(personalDetailsCard.getStateProvinceRegion())
                .isNotEmpty();
        assert_().withFailureMessage("Postal Code should be displayed")
                .that(personalDetailsCard.getPostalCode())
                .isNotEmpty();
        assert_().withFailureMessage("Country should be displayed")
                .that(personalDetailsCard.getCountry())
                .isNotEmpty();

        //STEP 4
        AdministrationPage administrationPage = LandingPage.getTopBar()
                .clickClientTab()
                .getLandingPage()
                .clickAdministrationTab();

        //STEP 5 to 6
        List<DataElement> usersList = administrationPage.getAdministrationTable()
                .getDataElements();

        for (DataElement dataElement: usersList)
        {
            assert_().withFailureMessage("First name/Last name should be displayed")
                    .that(dataElement.getUserName())
                    .isNotEmpty();
            assert_().withFailureMessage("Company name should be displayed")
                    .that(dataElement.getCompanyName())
                    .isNotEmpty();
            assert_().withFailureMessage("Role name should be displayed")
                    .that(dataElement.getRoleName())
                    .isNotEmpty();
            assert_().withFailureMessage("Account status should matched the displayed icon.")
                    .that(dataElement.getAccountStatusByIcon().getAccountStatus())
                    .isEqualTo(dataElement.getAccountStatus());

            // STEP 7
            if(rolesWithCode.contains(dataElement.getRoleName()))
            {
                assert_().withFailureMessage("Ship Builder/ Client/ Flag Code should be displayed")
                        .that(dataElement.getShipBuilderClientFlagCode())
                        .isNotEmpty();
            }
        }

        // STEP 8
        assert_().withFailureMessage("Total number of users shown in pagination should match the number of users displayed")
                .that(usersList.size())
                .isEqualTo(administrationPage.getShowingXofYPage().getShown());
    }
}
