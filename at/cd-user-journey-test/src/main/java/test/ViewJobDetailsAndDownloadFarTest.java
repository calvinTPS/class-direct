package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect.JobReport;
import constant.ClassDirect.Asset;
import helper.DownloadReportHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.surveys.servicehistory.ServiceHistoryPage;
import main.viewasset.surveys.servicehistory.detail.JobDetailsPage;
import main.viewasset.surveys.servicehistory.element.JobElement;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SERVICE_HISTORY;

public class ViewJobDetailsAndDownloadFarTest extends BaseTest
{
    @Test(description = "User can view details of Job, including associated services and download a FAR.")
    @Issue("LRCDT-1248")
    @Features(SERVICE_HISTORY)
    public void viewJobDetailsAndDownloadFarTest()
    {
        ServiceHistoryPage serviceHistoryPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(Asset.LAURA.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s is not found.", Asset.LAURA.getName())))
                .clickViewAssetButton()
                .getSurveysCard()
                .clickViewAllButton();

        //STEP 4 to 5
        List<JobElement> jobElements = serviceHistoryPage.getJobElements();
        assert_().withFailureMessage("This asset is expected to have at least one job")
                .that(jobElements.size())
                .isGreaterThan(0);

        for (JobElement job : jobElements)
        {
            assert_().withFailureMessage("Job Id should be displayed.")
                    .that(job.getJobId().isEmpty())
                    .isFalse();
            assert_().withFailureMessage("Job Location should be displayed.")
                    .that(job.getJobLocation().isEmpty())
                    .isFalse();
            assert_().withFailureMessage("Lead Surveyor should be displayed.")
                    .that(job.getLeadSurveyor().isEmpty())
                    .isFalse();
            job.getServiceElements().forEach(e ->
            {
                assert_().withFailureMessage("Service Name should be displayed.")
                        .that(e.getServiceName().isEmpty())
                        .isFalse();
                assert_().withFailureMessage("Status Icon should be displayed.")
                        .that(e.getServiceStatusByIcon())
                        .isNotNull();
            });
        }

        //STEP 6
        JobDetailsPage jobDetailsPage = jobElements.stream()
                .filter(e -> e.getJobId().equals(JobReport.FAR.getJobId()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected job with id: %s is not found.", JobReport.FAR.getJobId())))
                .clickMoreDetailsButton();

        //STEP 7
        assert_().withFailureMessage("Job Id should be displayed.")
                .that(jobDetailsPage.getJobId().isEmpty())
                .isFalse();
        assert_().withFailureMessage("Job Location should be displayed.")
                .that(jobDetailsPage.getJobLocation().isEmpty())
                .isFalse();
        assert_().withFailureMessage("Lead Surveyor should be displayed.")
                .that(jobDetailsPage.getLeadSurveyor().isEmpty())
                .isFalse();
        assert_().withFailureMessage("Last Visit Date should be displayed.")
                .that(jobDetailsPage.getLastVisitDate().isEmpty())
                .isFalse();

        //STEP 8
        jobDetailsPage.getServiceElements().forEach(e ->
        {
            assert_().withFailureMessage("Service Name should be displayed.")
                    .that(e.getServiceName().isEmpty())
                    .isFalse();
            assert_().withFailureMessage("Status Name should be displayed.")
                    .that(e.getStatusName().isEmpty())
                    .isFalse();
            assert_().withFailureMessage("Status Icon should be displayed.")
                    .that(e.getServiceStatusByIcon())
                    .isNotNull();
        });

        //STEP 9 to 10
        jobDetailsPage.clickDownloadFARButton();
        ReportExportDto reportExportDto = new ReportExportDto();
        reportExportDto.setJobId(Long.parseLong(jobDetailsPage.getJobId()));
        reportExportDto.setReportId(JobReport.FAR.getReportId());

        assert_().withFailureMessage("FAR Attachment should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(new Export().getExportReportToken(reportExportDto)))
                .isNotEmpty();
        //STEP 11
        jobDetailsPage.getAssetHeaderPage().clickBackButton(ServiceHistoryPage.class);
    }
}
