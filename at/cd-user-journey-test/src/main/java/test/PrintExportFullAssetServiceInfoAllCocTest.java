package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.AssetExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.common.export.ExportAssetListingPage;
import main.common.export.element.AssetsTab;
import main.common.export.element.SurveysNotesAndActionsTab;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.util.Arrays;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.GENERATE_AND_PRINT;

public class PrintExportFullAssetServiceInfoAllCocTest extends BaseTest
{
    private final String searchString = "ado";

    @Test(description = "User can export multiple assets information from the Service Schedule fleet")
    @Issue("LRCDT-1667")
    @Features(GENERATE_AND_PRINT)
    public void printExportFullAssetServiceInfoAllCocTest()
    {
        ExportAssetListingPage exportAssetListingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(searchString)
                .getPageReference(ServiceSchedulePage.class)
                .clickExportAssetInformationButton();

        AssetsTab assetsTab = exportAssetListingPage.clickAssetsTab();

        // STEP 6 - By default, All Assets are selected, and its name and IMO number should be displayed.
        assetsTab.getAssetCards()
                .forEach(e ->
                {
                    assert_().withFailureMessage(String.format("This asset: %s is expected to be selected, by default.", e.getAssetName()))
                            .that(e.isAssetCardSelected())
                            .isTrue();

                    assert_().withFailureMessage(String.format("Emblem for this asset: %s should be displayed.", e.getAssetName()))
                            .that(e.isAssetEmblemDisplayed())
                            .isTrue();

                    assert_().withFailureMessage(String.format("Asset name for this asset: %s should be displayed.", e.getAssetName()))
                            .that(e.getAssetName())
                            .isNotEmpty();

                    assert_().withFailureMessage(String.format("IMO number for this asset: %s should be displayed.", e.getAssetName()))
                            .that(e.getImoNumber())
                            .isNotEmpty();
                });

        assert_().withFailureMessage("By default, export button should be enabled.")
                .that(exportAssetListingPage.isExportButtonEnabled())
                .isTrue();

        // STEP 7 - Remove all button
        assert_().withFailureMessage("Remove all button should be enabled.")
                .that(assetsTab.isRemoveAllButtonEnabled())
                .isTrue();

        assetsTab.clickRemoveAllButton()
                .getAssetCards()
                .forEach(e -> assert_().withFailureMessage(String.format("This asset: %s is expected to be deselected.", e.getAssetName()))
                        .that(e.isAssetCardSelected())
                        .isFalse());

        assert_().withFailureMessage("Remove all button should be disabled.")
                .that(assetsTab.isRemoveAllButtonEnabled())
                .isFalse();
        assert_().withFailureMessage("Add all button should be enabled.")
                .that(assetsTab.isAddAllButtonEnabled())
                .isTrue();

        // STEP 8 - Add All button
        assetsTab.clickAddAllButton()
                .getAssetCards()
                .forEach(e -> assert_().withFailureMessage(String.format("This asset: %s is expected to be selected again.", e.getAssetName()))
                        .that(e.isAssetCardSelected())
                        .isTrue());

        // STEP 9 - Full Asset information radiobutton in Information tab
        assert_().withFailureMessage("By default, Full Asset information radiobutton is selected in Information tab.")
                .that(exportAssetListingPage.clickInformationTab().isFullAssetInfoRadioButtonSelected())
                .isTrue();

        // STEP 10 - Services and Codicils Tab
        SurveysNotesAndActionsTab surveysNotesAndActionsTab = exportAssetListingPage.surveysNotesAndActionsTab();
        assert_().withFailureMessage("By default, 'Condition of Class' checkbox should be selected.")
                .that(surveysNotesAndActionsTab.isConditionOfClassCheckboxSelected())
                .isTrue();
        assert_().withFailureMessage("By default, 'Actionable Item' checkbox should be selected.")
                .that(surveysNotesAndActionsTab.isActionableItemCheckboxSelected())
                .isTrue();
        assert_().withFailureMessage("By default, 'Asset Note' checkbox should be selected.")
                .that(surveysNotesAndActionsTab.isAssetNoteCheckboxSelected())
                .isTrue();

        // STEP 11 - Export assets - There is no fixed checksum since the file name is changing
        exportAssetListingPage.clickExportButton();

        assert_().withFailureMessage("Asset listing should be exported successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(getToken()))
                .isNotEmpty();

        // STEP 12-13 - The content of the zip file cannot be verified.
    }

    private String getToken()
    {
        AssetQueryHDto assetQueryHDto = new AssetQueryHDto();
        assetQueryHDto.setSearch(String.format("%s%s%s", "*", searchString, "*"));
        assetQueryHDto.setIsFavourite(false);
        assetQueryHDto.setIsSubfleet(true);

        AssetExportDto assetExportDto = new AssetExportDto();
        assetExportDto.setAssetQuery(assetQueryHDto);
        assetExportDto.setInfo(2L);
        assetExportDto.setService(3L);
        assetExportDto.setCodicils(Arrays.asList(1L, 4L, 2L, 3L));
        return new Export().getExportAssetToken(assetExportDto);
    }
}
