package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ESPReportExportDto;
import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect.JobReport;
import helper.DownloadReportHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.surveys.servicehistory.ServiceHistoryPage;
import main.viewasset.surveys.servicehistory.detail.JobDetailsPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.JobSearchOptions.JOB_NUMBER;
import static constant.Epic.SERVICE_HISTORY;

public class DownloadReportsTest extends BaseTest {
    @Test(description = "Automation_UJ_DO_DownloadReports_FSR_ESP_TM")
    @Issue("LRCDT-1250")
    @Features(SERVICE_HISTORY)
    public void downloadReportsTest() {

        // STEP 1-3: Navigate to Service History Page
        ServiceHistoryPage serviceHistoryPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s is not found.", LAURA.getName())))
                .clickViewAssetButton()
                .getSurveysCard()
                .clickViewAllButton();

        // STEP 4-5: View a job with FSR, ESP, and TM reports available
        JobDetailsPage jobDetailsPage = serviceHistoryPage.getFilterBarPage()
                .selectSearchDropDown(JOB_NUMBER.toString())
                .setSearchTextBox(JobReport.FAR.getJobId())
                .getPageReference(ServiceHistoryPage.class)
                .getJobElements()
                .stream()
                .filter(j -> j.getJobId().equals(JobReport.FAR.getJobId()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Job number %s is not found.", JobReport.FAR.getJobId())))
                .clickMoreDetailsButton();

        // STEP 6: Verify downloading Final Attendance Report (FAR)
        jobDetailsPage.clickDownloadFARButton();
        ReportExportDto reportExportDto = new ReportExportDto();
        reportExportDto.setJobId(Long.parseLong(jobDetailsPage.getJobId()));
        reportExportDto.setReportId(JobReport.FAR.getReportId());

        assert_().withFailureMessage("Final Attendance Report should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(new Export().getExportReportToken(reportExportDto)))
                .isNotEmpty();

        // STEP 7-8: Navigate to another job
        jobDetailsPage = jobDetailsPage.getAssetHeaderPage().clickBackButton(ServiceHistoryPage.class)
                .getFilterBarPage()
                .selectSearchDropDown(JOB_NUMBER.toString())
                .setSearchTextBox(JobReport.ESP.getJobId())
                .getPageReference(ServiceHistoryPage.class)
                .getJobElements()
                .stream()
                .filter(j -> j.getJobId().equals(JobReport.ESP.getJobId()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Job number " + JobReport.ESP.getJobId() + " not found"))
                .clickMoreDetailsButton();

        // STEP 10 - Download ESP report
        jobDetailsPage.clickDownloadESPReportButton();
        // There's no getter/setter in EspReportExportDto, so use this json string for body
        String espReportExportDto = String.format("{\"jobId\":%s}", JobReport.ESP.getReportId());

        assert_().withFailureMessage("Enhanced Survey Programme Report should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(new Export().getExportESPReportToken(espReportExportDto)))
                .isNotEmpty();

        // TODO - STEP 9-16: Verify downloading TM reports is blocked by CR127
    }
}
