package test;

import com.baesystems.ai.lr.cd.be.domain.dto.export.ReportExportDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import helper.DownloadReportHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.ConditionOfClassDetailsPage;
import main.viewasset.codicils.codicilsanddefects.sub.defects.DefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.defects.details.DefectDetailsPage;
import main.viewasset.codicils.codicilsanddefects.sub.defects.filter.FilterDefectsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Export;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.defaultCodicilStatus;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.JobReport;

public class DefectsSearchFilterReportAndAssociatedCocTest extends BaseTest
{
    @Test(description = "User can view Defects for an asset and perform the following operations - search, filter and download a Final Survey Report")
    @Issue("LRCDT-1666")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void defectsSearchFilterReportAndAssociatedCocTest()
    {
        final String searchString = "Window";
        final String closedStatus = "Closed";
        final String hullCategory = "Hull";
        final String allCategory = "All";
        final LocalDate dateOccurredFrom = LocalDate.of(2016, 10, 16);
        final LocalDate dateOccurredTo = LocalDate.of(2016, 10, 29);
        final String defectTitle = "Cracked Window";
        final String defectCategory = "Electro-technical";

        DefectsPage defectsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickDefectsTab();

        // STEP 4 - By default, defects are ordered by date occurred (descending)
        List<LocalDate> dateOccurredList = defectsPage.getDefectElements()
                .stream()
                .map(e-> LocalDate.parse(e.getDateOccurred(), FRONTEND_TIME_FORMAT_DTS))
                .collect(Collectors.toList());

        Collections.reverse(dateOccurredList);
        assert_().withFailureMessage("Date Occurred of defects should be in descending order")
                .that(dateOccurredList)
                .isOrdered();

        // STEP 5 - Verify high level details
        defectsPage.getDefectElements()
                .forEach(e->
                {
                    assert_().withFailureMessage("Defect Title should be displayed.")
                            .that(e.getDefectTitle())
                            .isNotEmpty();
                    assert_().withFailureMessage("Defect Category should be displayed.")
                            .that(e.getDefectCategory())
                            .isNotEmpty();
                    assert_().withFailureMessage("Date occurred should be displayed.")
                            .that(e.getDateOccurred())
                            .isNotEmpty();
                    assert_().withFailureMessage("Defect Status should be Open.")
                            .that(e.getStatus())
                            .isEqualTo(defaultCodicilStatus);
                });

        // STEP 6 - Defect Detailed Info
        DefectDetailsPage defectDetailsPage = defectsPage.getDefectElements()
                .stream()
                .filter(e -> e.getDefectTitle().equals(defectTitle) && e.getDefectCategory().equals(defectCategory))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected defect with title %s is not found.", defectTitle)))
                .clickArrowButton();

        assert_().withFailureMessage("Defect Title should be displayed.")
                .that(defectDetailsPage.getDefectTitle())
                .isNotEmpty();
        assert_().withFailureMessage("Defect Category should be displayed.")
                .that(defectDetailsPage.getDefectCategory())
                .isNotEmpty();
        assert_().withFailureMessage("Date occurred should be displayed.")
                .that(defectDetailsPage.getDateOccurred())
                .isNotEmpty();
        assert_().withFailureMessage("Defect Status should be displayed.")
                .that(defectDetailsPage.getDefectStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Items should be displayed.")
                .that(defectDetailsPage.getItems())
                .isNotEmpty();
        assert_().withFailureMessage("Job number should be displayed.")
                .that(defectDetailsPage.getJobNumber())
                .isNotEmpty();
        assert_().withFailureMessage("Description should be displayed.")
                .that(defectDetailsPage.getDescription())
                .isNotEmpty();

        // STEP 7 - Associated Condition of Class (High level details)
        defectDetailsPage.getAssociatedConditionOfClass()
                .forEach(e ->
                {
                    assert_().withFailureMessage("CoC Title should be displayed.")
                            .that(e.getTitle())
                            .isNotEmpty();
                    assert_().withFailureMessage("CoC Category should be displayed.")
                            .that(e.getCategory())
                            .isNotEmpty();
                    assert_().withFailureMessage("Imposed Date should be displayed.")
                            .that(e.getImposedDate())
                            .isNotEmpty();
                    assert_().withFailureMessage("Due Date should be displayed.")
                            .that(e.getDueDate())
                            .isNotEmpty();
                    assert_().withFailureMessage("CoC Status should be displayed.")
                            .that(e.getStatus())
                            .isNotEmpty();
                });

        // STEP 8-9 - Associated Condition of Class (Detailed Info) and back button
        defectDetailsPage.getAssociatedConditionOfClass()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("There is no 'Associated Condition of Class' in the list."))
                .clickArrowButton(ConditionOfClassDetailsPage.class)
                .getAssetHeaderPage()
                .clickBackButton(DefectDetailsPage.class);

        // STEP 10 - Download Final Survey Report
        defectDetailsPage.clickDownloadFinalServiceReportIcon();
        ReportExportDto reportExportDto = new ReportExportDto();
        reportExportDto.setJobId(JobReport.FSR.getReportId());
        reportExportDto.setReportId(JobReport.FSR.getReportId());

        assert_().withFailureMessage("Final Service Report should be downloaded successfully.")
                .that(DownloadReportHelper.getDownloadFileChecksum(new Export().getExportReportToken(reportExportDto)))
                .isNotEmpty();

        // STEP 11 - Back to Defect list page and get the count
        int countBeforeSearch = defectDetailsPage.getAssetHeaderPage().clickBackButton(DefectsPage.class)
                .getDefectElements()
                .size();

        // STEP 12 - Search
        defectsPage.getFilterBarPage()
                .setSearchTextBox(searchString)
                .getPageReference(DefectsPage.class)
                .getDefectElements()
                .forEach(e -> assert_().withFailureMessage(String.format("Defect title should contain the search string: %s", searchString))
                                .that(e.getDefectTitle())
                                .contains(searchString));

        // STEP 13 - Clear Search
        assert_().withFailureMessage("All defects should be displayed again when Search filter is cleared.")
                .that(defectsPage.getFilterBarPage()
                        .clickClearButtonInSearchTextBox()
                        .getPageReference(DefectsPage.class)
                        .getDefectElements()
                        .size())
                .isEqualTo(countBeforeSearch);

        // STEP 14 - Filter
        FilterDefectsPage filterDefectsPage = defectsPage.getFilterBarPage()
                .clickFilterButton(FilterDefectsPage.class);

        assert_().withFailureMessage("By default, Open status should be selected.")
                .that(filterDefectsPage.isOpenRadioButtonSelected())
                .isTrue();

        // STEP 15 - Filter by 'Closed' status
        filterDefectsPage.selectClosedRadioButton()
                .clickSubmitButton()
                .getDefectElements()
                .forEach(e-> assert_().withFailureMessage("Defect Status should be Closed.")
                                .that(e.getStatus())
                                .isEqualTo(closedStatus));

        // STEP 16 - Filter by Category
        defectsPage.getFilterBarPage()
                .clickFilterButton(FilterDefectsPage.class)
                .getCategoryCheckboxList()
                .forEach(e -> assert_().withFailureMessage("By default, All Categories are selected.")
                                .that(e.isCheckboxSelected())
                                .isTrue());

        // STEP 17 - Uncheck 'Hull' category
        filterDefectsPage.deselectCategoryCheckbox(hullCategory)
                .getCategoryCheckboxList()
                .forEach(e ->
                {
                    if(e.getCategoryName().equals(hullCategory) || e.getCategoryName().equals(allCategory))
                    {
                        assert_().withFailureMessage(String.format("%s category should be deselected.", e.getCategoryName()))
                                .that(e.isCheckboxSelected())
                                .isFalse();
                    }
                    else
                    {
                        assert_().withFailureMessage(String.format("%s category should remain selected.", e.getCategoryName()))
                                .that(e.isCheckboxSelected())
                                .isTrue();
                    }
                });

        filterDefectsPage.clickSubmitButton()
                .getDefectElements()
                .forEach(e -> assert_().withFailureMessage(String.format("Defects with category %s should not be displayed.", hullCategory))
                                .that(e.getDefectCategory())
                                .isNotEqualTo(hullCategory));

        // STEP 18-19 - Filter by Occurred Date
        defectsPage.getFilterBarPage()
                .clickFilterButton(FilterDefectsPage.class)
                .clickClearFiltersButton()
                .getFilterBarPage()
                .clickFilterButton(FilterDefectsPage.class)
                .setDateOccurredFrom(dateOccurredFrom.format(FRONTEND_TIME_FORMAT_DTS))
                .setDateOccurredTo(dateOccurredTo.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton()
                .getDefectElements()
                .forEach(e -> assert_().withFailureMessage(String.format("Date Occurred should be in between %s and %s.", dateOccurredFrom, dateOccurredTo))
                                .that(LocalDate.parse(e.getDateOccurred(), FRONTEND_TIME_FORMAT_DTS))
                                .isIn(Range.closed(dateOccurredFrom, dateOccurredTo)));
    }
}
