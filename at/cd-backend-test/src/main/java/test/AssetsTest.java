package test;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;

import com.frameworkium.core.api.tests.BaseTest;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import service.Assets;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;

public class AssetsTest extends BaseTest
{
    @Test
    public void getAssetsTest()
    {
        new Assets().getAssets(new AssetQueryHDto());
    }

    @Test
    public void getAssetTest()
    {
        AssetHDto asset = new Assets().getAsset(LAURA.getAssetCode());
        assert_().withFailureMessage("Returned asset id doesn't match the expected value.")
                .that(asset.getId()).isEqualTo(1);
    }

    @Test
    public void getActionableItemsTest()
    {
        new Assets().getActionableItems(1);
    }

    @Test
    public void negativeGetAssetTest_AssetAsString_InternalServerError()
    {
        ErrorMessageDto errorMessage = new Assets().getAssetExpectedError(HttpStatus.SC_INTERNAL_SERVER_ERROR, "test");
        assert_().withFailureMessage("Error is expected to be returned.")
                .that(errorMessage.getMessage())
                .isEqualTo("Internal Server Error.");
    }

    @Test
    public void negativeGetAssetTest_NonExistingAssetId_NotFound()
    {
        ErrorMessageDto errorMessage = new Assets().getAssetExpectedError(HttpStatus.SC_NOT_FOUND, 0);
        assert_().withFailureMessage("Error is expected to be returned.")
                .that(errorMessage.getMessage())
                .isEqualTo("Record Not Found.");
    }

    @Test
    public void negativeGetActionableItemsTest_AssetAsString_InternalServerError()
    {
        ErrorMessageDto errorMessage = new Assets().getActionableItemsExpectedError(HttpStatus.SC_INTERNAL_SERVER_ERROR, "test");
        assert_().withFailureMessage("Error is expected to be returned.")
                .that(errorMessage.getMessage())
                .isEqualTo("Internal Server Error.");
    }
}
