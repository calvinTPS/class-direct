#@IgnoreInspection BashAddShebang
HUB_IP="$(cat hubIp.txt)"
GRID_IP="10.210.69.20"

#Wait for Tomcat to start
echo "Waiting for tomcat to start..."
STATUSCODE=`curl -m 180 -s -o /dev/null -w "%{http_code}" http://${HUB_IP}:8080/api/v1/ping`

if [ $STATUSCODE -ne 200 ]; then
  echo "---------- ERROR -----------"
  echo "/api/v1/ping Status Code: ${STATUSCODE}"
  echo "CD failed to boot properly, aborting job..."
  echo "---------- ERROR -----------"
  exit 1;
fi

#Store for later use
PROFILE="user-journey-tests"
TESTSUITE="AllUserJourney.xml"
MODULE="cd-user-journey-test"
echo ${PROFILE} > profile.txt
echo ${TESTSUITE} > testSuite.txt
echo ${MODULE} > module.txt

#Execute test
/opt/apache-maven-3.3.3/bin/mvn \
-Djava.net.preferIPv4Stack=true \
-f at/pom.xml verify \
-P ${PROFILE} \
-pl ${MODULE} \
-Dsurefire.suiteXmlFiles=testsuite/${TESTSUITE} \
-Dbrowser=chrome \
-Dmaximise=true \
-DgridURL=http://${GRID_IP}:4444/wd/hub \
-DvideoCaptureUrl=http://${GRID_IP}:3000/download_video/%s.mp4 \
-Dquick=true \
-DenvironmentKey=${HUB_IP} \
-DapiKey=${HUB_IP}:8080 \
-DdbKey=${HUB_IP}:3306 \
-am
