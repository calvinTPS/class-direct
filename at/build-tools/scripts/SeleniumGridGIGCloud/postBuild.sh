#@IgnoreInspection BashAddShebang
#Server Information
GRID_ARBITRATOR_IP="10.210.69.20"
SERVER_PASS="4utomation!";
HUB_IP="$(cat hubIp.txt)"
MODULE="$(cat module.txt)"
CATALINA_HOME="/c/apache-tomcat-8.5.4"
JAVA_HOME="/c/program\ files/Java/jdk1.8.0_111"

#Mark hub as not in-use
echo "Setting the hub ${HUB_IP} to not in-use..."
mysql -h ${GRID_ARBITRATOR_IP} -utestUser -ps3curePa55word -e "UPDATE selenium_grid.grid_hub SET in_use = 0 WHERE ip = '${HUB_IP}';"

cd ${WORKSPACE}
mkdir at/${MODULE}/catalina_logs
cd at/${MODULE}/catalina_logs

sshpass -p ${SERVER_PASS} scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r cyg_server@${HUB_IP}:${CATALINA_HOME}/logs/catalina.out .



