#@IgnoreInspection BashAddShebang
#Server Information
SERVER_PASS="4utomation!";
HUB_IP="$(cat hubIp.txt)"
PROVISIONING_DIR="/c/Git/Lloyds/class-direct-provisioning/local"


#Copy config ino the hub
sshpass -p ${SERVER_PASS} scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r ${WORKSPACE}/at/build-tools/src/main/resources/configGridHub.properties cyg_server@${HUB_IP}:${PROVISIONING_DIR}

# ------------------- SSH'n INTO HUB -----------------------------
sshpass -p $SERVER_PASS ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no cyg_server@${HUB_IP} << EOF

  #Source the babunrc, env variables aren't being set..
  source ~/.babunrc
  source ${PROVISIONING_DIR}/configGridHub.properties

  #Shutdown tomcat
  echo ""; echo "Shutting down tomcat"
  cd \$CATALINA_HOME/bin
  sh shutdown.sh
  sleep 10s

  #Clean up logs
  echo "Cleaning up tomcat logs ..."
  rm -rf \$CATALINA_HOME/logs/*

  #Clean up old files
  rm -rf ${PROVISIONING_DIR}/backend
  rm -rf ${PROVISIONING_DIR}/frontend
  rm -rf ${PROVISIONING_DIR}/mast

  #Update provisioning script
  cd ${PROVISIONING_DIR}
  git stash
  git pull -p
  git stash pop
  ./apache_bypassAzure.bat

  bash provisioning.sh -d data-load -b ${BE_BUILD} -f ${FE_BUILD} -m ${MAST_BUILD} -j ${BUILD_JOB} -c ${PROVISIONING_DIR}/configGridHub.properties -x true

EOF

exit
