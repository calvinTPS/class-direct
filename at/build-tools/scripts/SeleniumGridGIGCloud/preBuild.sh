#@IgnoreInspection BashAddShebang
#Grid Information
GRID_PASS="4utomation!";
GRID_ARBITRATOR_IP="10.210.69.20"
HUB_IP=`mysql -h ${GRID_ARBITRATOR_IP} -utestUser -ps3curePa55word -N -e "SELECT ip FROM selenium_grid.grid_hub WHERE in_use = 0 LIMIT 1;" 2>&1 | grep -v "Using a password"`


#------------------ FIND AVAILABLE HUB --------------------------
echo ""; echo ""
if [[ -z "$HUB_IP" ]]; then
  echo "No hubs are available, exiting..."
  exit 1
fi

#Store Hub IP for re-use
echo $HUB_IP > hubIp.txt
echo "Found available Grid Hub";
echo "Hub IP: ${HUB_IP}"; echo ""

#Mark hub as in-use
echo "Setting the hub ${HUB_IP} to in-use..."
mysql -h ${GRID_ARBITRATOR_IP} -utestUser -ps3curePa55word -e "UPDATE selenium_grid.grid_hub SET in_use = 1 WHERE ip = '${HUB_IP}';"
echo "DONE"

exit

#TODO - we will only use $HUB_IP to track tomcats, not the grid. change the variable name to reflect so
