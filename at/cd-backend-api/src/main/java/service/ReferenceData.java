package service;

import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.references.asset.ClassStatusHDto;
import org.apache.http.HttpStatus;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Arrays;
import java.util.List;

import static constant.Endpoint.REFERENCE_DATA_ASSET_TYPE;
import static constant.Endpoint.REFERENCE_DATA_CLASS_STATUS;
import static constant.Endpoint.REFERENCE_DATA_SERVICE_CATALOGUES;

public class ReferenceData extends BaseClassDirectService
{
    @Step("Get Class Status")
    public List<ClassStatusHDto> getClassStatus()
    {
        ClassStatusHDto[] classStatusHDtos = getRequestSpec()
                .get(REFERENCE_DATA_CLASS_STATUS.getUrl())
                .as(ClassStatusHDto[].class);
        return Arrays.asList(classStatusHDtos);
    }

    @Step("Get Asset Types")
    public List<AssetTypeHDto> getAssetType()
    {
        AssetTypeHDto[] assetTypeHDtos = getRequestSpec()
                .get(REFERENCE_DATA_ASSET_TYPE.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(AssetTypeHDto[].class);
        return Arrays.asList(assetTypeHDtos);
    }

    @Step("Get Service Catalogues")
    public List<ServiceCatalogueHDto> getServiceCatalogues()
    {
        ServiceCatalogueHDto[] serviceCatalogueHDtos = getRequestSpec().get(REFERENCE_DATA_SERVICE_CATALOGUES.getUrl())
                .as(ServiceCatalogueHDto[].class);
        return Arrays.asList(serviceCatalogueHDtos);
    }
}
