package service;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.*;
import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemPreviewListHDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static constant.Endpoint.*;

public class Assets extends BaseClassDirectService
{

    @Step("Get Assets")
    public List<AssetHDto> getAssets(AssetQueryHDto assetQueryHDto)
    {
        Response response = getRequestSpec()
                .body(assetQueryHDto)
                .contentType(ContentType.JSON)
                .post(ASSETS_POST_ASSETS_QUERY.getUrl());
        return response.as(AssetPageResource.class).getContent();
    }

    @Step("Get Asset By Asset code")
    public AssetHDto getAsset(String assetCode)
    {
        return getRequestSpec()
                .get(ASSETS_GET_ASSET.getUrl(assetCode))
                .as(AssetHDto.class);
    }

    @Step("Get Register Book Sisters")
    public List<AssetHDto> getRegisterBookSisters(String assetCode)
    {
        return getRequestSpec()
                .get(ASSETS_GET_REGISTER_BOOK_SISTERS.getUrl(assetCode))
                .as(AssetPageResource.class)
                .getContent();
    }

    @Step("Get Technical Sisters")
    public List<AssetHDto> getTechnicalSisters(String assetCode)
    {
        return getRequestSpec()
                .get(ASSETS_GET_TECHNICAL_SISTERS.getUrl(assetCode))
                .as(AssetPageResource.class)
                .getContent();
    }

    @Step("Get Actionable Items By Asset Id")
    public List<ActionableItemHDto> getActionableItems(int id)
    {
        return getRequestSpec()
                .get(ASSETS_GET_ACTIONABLE_ITEMS.getUrl(id))
                .as(ActionableItemPageResource.class)
                .getContent();
    }

    @Step("Get Asset Notes By Asset Id")
    public AssetNotePageResource getAssetNotes(int id)
    {
        return getRequestSpec()
                .get(ASSETS_GET_ASSET_NOTES.getUrl(id))
                .as(AssetNotePageResource.class);
    }

    @Step("Get Asset By Invalid/Non-existing Asset Id")
    public ErrorMessageDto getAssetExpectedError(int expectedStatusCode, Object id)
    {
        return getRequestSpec()
                .when()
                .get(ASSETS_GET_ASSET.getUrl(id))
                .then()
                .log().all()
                .assertThat()
                .statusCode(expectedStatusCode)
                .extract()
                .as(ErrorMessageDto.class);
    }

    @Step("Get Actionable Items By Invalid/Non-existing Asset Id")
    public ErrorMessageDto getActionableItemsExpectedError(int expectedStatusCode, Object id)
    {
        return getRequestSpec()
                .when()
                .get(ASSETS_GET_ACTIONABLE_ITEMS.getUrl(id))
                .then()
                .log().all()
                .assertThat()
                .statusCode(expectedStatusCode)
                .extract()
                .as(ErrorMessageDto.class);
    }

    @Step("Post CODICILS Query")
    public CodicilActionableHDto postCodicilsQuery(int id, CodicilDefectQueryHDto codicilDefectQueryHDto)
    {
        Response response = getRequestSpec()
                .contentType(ContentType.JSON)
                .body(codicilDefectQueryHDto)
                .post(ASSETS_POST_CODICILS_QUERY.getUrl(id));
        return response.as(CodicilActionableHDto.class);
    }

    @Step("Get Services By Asset Id")
    public List<ScheduledServiceHDto> getServicesByAssetId(int assetId)
    {
        Response response = getRequestSpec().get(ASSETS_GET_SERVICES.getUrl(assetId));
        return response.asString().equals("")
                ? new ArrayList<>()
                : Arrays.asList(response.as(ScheduledServiceHDto[].class));
    }

    @Step("Get Asset details")
    public AssetDetailsDto getAssetDetails(String assetId)
    {
        return getRequestSpec()
                .get(ASSETS_GET_DETAILS.getUrl(assetId))
                .as(AssetDetailsDto.class);
    }

    @Step("Get Asset Tasks")
    public WorkItemPreviewListHDto getAssetTasks(int assetId, boolean pmsAble)
    {
        return getRequestSpec()
                .param("pmsAble", pmsAble)
                .get(ASSETS_GET_TASKS.getUrl(assetId))
                .as(WorkItemPreviewListHDto.class);
    }
}
