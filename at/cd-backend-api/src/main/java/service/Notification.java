package service;

import ru.yandex.qatools.allure.annotations.Step;

import static constant.Endpoint.NOTIFICATIONS_GET_JOB_NOTIFICATION;

public class Notification extends BaseClassDirectService
{
    @Step("Get Job Notification")
    public int getJobNotification()
    {
        return getRequestSpec()
                .get(NOTIFICATIONS_GET_JOB_NOTIFICATION.getUrl())
                .thenReturn()
                .statusCode();
    }
}
