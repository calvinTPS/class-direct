package service;

import com.baesystems.ai.lr.cd.be.domain.dto.task.WorkItemLightListHDto;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import ru.yandex.qatools.allure.annotations.Step;

import static constant.Endpoint.TASKS_PUT;

public class Tasks extends BaseClassDirectService
{

    @Step("PUT Tasks")
    public WorkItemLightListHDto putTasks(WorkItemLightListHDto workItemLightListHDto)
    {
        Response response = getRequestSpec()
                .contentType(ContentType.JSON)
                .body(workItemLightListHDto)
                .put(TASKS_PUT.getUrl());
        return response.as(WorkItemLightListHDto.class);
    }
}
