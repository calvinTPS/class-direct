package service;

import com.baesystems.ai.lr.cd.be.domain.dto.export.CertificateAttachDto;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import ru.yandex.qatools.allure.annotations.Step;

import static constant.Endpoint.CERTIFICATE_ATTACHMENT;

public class Certificates extends BaseClassDirectService
{
    @Step("Get Certificate Attachment Token")
    public String getCertificateAttachmentToken(CertificateAttachDto certificateAttachDto)
    {
        Response response = getRequestSpec()
                .body(certificateAttachDto)
                .contentType(ContentType.JSON)
                .post(CERTIFICATE_ATTACHMENT.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }
}
