package service;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetPageResource;
import com.baesystems.ai.lr.cd.be.domain.repositories.UserProfiles;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static constant.Endpoint.*;

public class Users extends BaseClassDirectService
{
    @Step("Get Users")
    public UserProfiles getUser(String userId)
    {
        return getRequestSpec().get(USERS_GET_USER.getUrl(userId))
                .as(UserProfiles.class);
    }

    @Step("Get Accessible assets of user")
    public List<AssetHDto> getAccessibleAssets(String userId)
    {
        AssetPageResource assetPageResource = getRequestSpec()
                .get(USERS_GET_SUBFLEET.getUrl(userId))
                .as(AssetPageResource.class);
        return assetPageResource.getContent();
    }

    @Step("Get Restricted assets of user")
    public List<AssetHDto> getRestrictedAssets(String userId)
    {
        AssetPageResource assetPageResource = getRequestSpec()
                .get(USERS_GET_RESTRICTED_ASSETS.getUrl(userId))
                .as(AssetPageResource.class);
        return assetPageResource.getContent();
    }
}
