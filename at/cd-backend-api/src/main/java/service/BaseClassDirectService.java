package service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.frameworkium.core.api.services.BaseService;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.parsing.Parser;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;

import static constant.Constant.HeaderData.HEADER_DATA;
import static constant.Endpoint.BASE_URI;
import static io.restassured.config.RestAssuredConfig.newConfig;

/**
 * Base Service for ClassDirect specific services.
 */
public class BaseClassDirectService extends BaseService
{
    public BaseClassDirectService()
    {
        RestAssured.config = newConfig().objectMapperConfig(
                ObjectMapperConfig.objectMapperConfig().jackson2ObjectMapperFactory((cls, charset) ->
                {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                            .registerModule(new JSR310Module());
                    return objectMapper;
                }));
        RestAssured.defaultParser = Parser.JSON;
    }

    /**
     * @return a Rest Assured {@link RequestSpecification} with the baseUri
     * (and anything else required by most ClassDirect services).
     */
    @Override
    protected RequestSpecification getRequestSpec()
    {
        return RestAssured.given()
                .headers(HEADER_DATA.getHeaderName(), HEADER_DATA.getHeaderValue())
                .baseUri(BASE_URI.getUrl());
    }

    /**
     * @return a Rest Assured {@link ResponseSpecification} with basic checks
     * (and anything else required by most ClassDirect services).
     */
    @Override
    protected ResponseSpecification getResponseSpec()
    {
        return RestAssured.expect().response().statusCode(HttpStatus.SC_OK);
    }
}
