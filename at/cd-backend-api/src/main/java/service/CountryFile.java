package service;

import com.baesystems.ai.lr.cd.be.domain.dto.attachments.SupplementaryInformationHDto;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Arrays;
import java.util.List;

import static constant.Endpoint.COUNTRY_FILE;

public class CountryFile extends BaseClassDirectService
{
    @Step("Get Country files")
    public List<SupplementaryInformationHDto> getCountryFiles(int flagId)
    {
        Response response = getRequestSpec()
                .then()
                .statusCode(HttpStatus.SC_OK)
                .request()
                .get(COUNTRY_FILE.getUrl(flagId));

        return Arrays.asList(response.as(SupplementaryInformationHDto[].class));
    }
}
