package service;

import com.baesystems.ai.lr.cd.be.domain.dto.export.*;
import com.baesystems.ai.lr.cd.be.domain.dto.user.UserProfileDto;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import ru.yandex.qatools.allure.annotations.Step;

import static constant.Endpoint.*;

public class Export extends BaseClassDirectService
{
    @Step("Get Export User Token")
    public String getExportUserToken(UserProfileDto userProfileDto)
    {
        Response response = getRequestSpec()
                .body(userProfileDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_USERS.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export Asset Token")
    public String getExportAssetToken(AssetExportDto assetExportDto)
    {
        Response response = getRequestSpec()
                .body(assetExportDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_ASSETS.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export CoC Token")
    public String getExportConditionOfClassToken(ExportPdfDto exportPdfDto)
    {
        Response response = getRequestSpec()
                .body(exportPdfDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_COC_LIST.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export AI Token")
    public String getExportActionableItemToken(ExportPdfDto exportPdfDto)
    {
        Response response = getRequestSpec()
                .body(exportPdfDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_AI_LIST.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export Asset Note Token")
    public String getExportAssetNoteToken(ExportPdfDto exportPdfDto)
    {
        Response response = getRequestSpec()
                .body(exportPdfDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_AN_LIST.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export MPMS Token")
    public String getExportMpmsToken(PmsTaskListExportDto pmsTaskListExportDto)
    {
        Response response = getRequestSpec()
                .body(pmsTaskListExportDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_MPMS.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export Task List Token")
    public String getExportTaskListToken(TaskListExportDto taskListExportDto)
    {
        Response response = getRequestSpec()
                .body(taskListExportDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_TASKLIST.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export CheckList Token")
    public String getExportCheckListToken(ChecklistExportDto checklistExportDto)
    {
        Response response = getRequestSpec()
                .body(checklistExportDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_CHECKLIST.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export Report Token")
    public String getExportReportToken(ReportExportDto reportExportDto)
    {
        Response response = getRequestSpec()
                .body(reportExportDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_REPORT.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }

    @Step("Get Export ESP Report Token")
    public String getExportESPReportToken(String espReportExportDto)
    {
        Response response = getRequestSpec()
                .body(espReportExportDto)
                .contentType(ContentType.JSON)
                .post(EXPORT_ESP_REPORT.getUrl())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();
        return new JSONObject(response.asString()).getString("response");
    }
}
