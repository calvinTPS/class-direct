package constant;

public class Constant
{
    public enum HeaderData
    {
        HEADER_DATA("userId", "37be2b5b-63ee-4535-bca4-202d556d626e");

        private final String headerName;
        private final String headerValue;

        HeaderData(String headerName, String headerValue)
        {
            this.headerName = headerName;
            this.headerValue = headerValue;
        }

        public String getHeaderName()
        {
            return this.headerName;
        }

        public String getHeaderValue()
        {
            return this.headerValue;
        }
    }
}
