package constant;

import config.Config;

public enum Endpoint
{
    BASE_URI("http://" + Config.getBaseAPI() + "/api/v1"),
    ASSETS_POST_ASSETS_QUERY("/assets/query"),
    ASSETS_GET_ASSET("/assets/%s"),
    ASSETS_GET_ACTIONABLE_ITEMS("/assets/%s/actionable-items"),
    ASSETS_GET_ASSET_NOTES("/assets/%s/asset-notes"),
    ASSETS_GET_SERVICES("/assets/%s/services"),
    ASSETS_GET_REGISTER_BOOK_SISTERS("/assets/%s/register-book-sisters"),
    ASSETS_GET_TECHNICAL_SISTERS("/assets/%s/technical-sisters"),
    ASSETS_GET_DETAILS("/assets/%s/details"),
    ASSETS_GET_TASKS("/assets/%s/tasks"),
    USERS_GET_USER("/users/%s"),
    USERS_GET_SUBFLEET("/users/%s/subfleet"),
    USERS_GET_RESTRICTED_ASSETS("/users/%s/restricted-assets"),
    EXPORT_USERS("/export/users"),
    EXPORT_ASSETS("/export/assets"),
    EXPORT_COC_LIST("/export/coc-list"),
    EXPORT_AI_LIST("/export/ai-list"),
    EXPORT_AN_LIST("/export/an-list"),
    EXPORT_MPMS("/export/mpms"),
    COUNTRY_FILE("/country-file/attachments?flagId=%s"),
    EXPORT_TASKLIST("/export/tasklist"),
    EXPORT_CHECKLIST("/export/checklist"),
    EXPORT_REPORT("/export/reports"),
    EXPORT_ESP_REPORT("/export/esp-report"),
    ASSETS_POST_CODICILS_QUERY("/assets/%s/codicils/query"),
    REFERENCE_DATA_CLASS_STATUS("reference-data/asset/class-statuses"),
    REFERENCE_DATA_ASSET_TYPE("reference-data/asset/asset-types"),
    REFERENCE_DATA_SERVICE_CATALOGUES("/reference-data/service/service-catalogues"),
    CERTIFICATE_ATTACHMENT("/certificates/attachment"),
    NOTIFICATIONS_GET_JOB_NOTIFICATION("/notifications/job-notification"),
    TASKS_PUT("/tasks");

    private String url;

    Endpoint(String url)
    {
        this.url = url;
    }

    public String getUrl(Object... params)
    {
        return String.format(url, params);
    }
}
