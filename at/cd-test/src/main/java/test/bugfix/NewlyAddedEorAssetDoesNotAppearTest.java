package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.eor.AmendAssetExpiryDatePage;
import main.administration.detail.eor.EorAssetCard;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.*;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.CLIENT_USER;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;

public class NewlyAddedEorAssetDoesNotAppearTest extends BaseTest
{
    @Test(description = "The system does not display newly added EOR asset on administration user profile page. - Redis Cache issue")
    @Issues({
        @Issue("LRCD-2124"),
        @Issue("LRCD-1952")
    })
    @Features(USER_MANAGEMENT)
    public void newlyAddedEorAssetDoesNotAppearTest()
    {
        List<EorAssetCard> eorAssetCardList = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(CLIENT_USER.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected user with email %s is not found.", CLIENT_USER.getEmail())))
                .clickFurtherMoreDetailsButton()
                .getExhibitionOfRecordsCard()
                .clickAddEorAccessButton()
                .setAssetExpiryDate(FRONTEND_TIME_FORMAT_DTS.format(now))
                .setImoNumber(LAURA.getImoNumber())
                .clickAddExhibitionRecordsAssetButton()
                .getFooterPage()
                .clickConfirmButton(AdministrationDetailPage.class)
                .getExhibitionOfRecordsCard()
                .getEorAssetCards();

        // for LRCD-2124
        assert_().withFailureMessage("Newly added EOR asset card should be displayed.")
                .that(eorAssetCardList.stream()
                        .map(EorAssetCard::getImoNumber)
                        .collect(Collectors.toList()))
                .contains(LAURA.getImoNumber());

        // for LRCD-1952
        // UM-55:AC13 - Overlay is not displayed when click on Update EOR expiration date
        AmendAssetExpiryDatePage assetExpiryDatePage = eorAssetCardList.stream()
                .filter(e -> e.getImoNumber().equals(LAURA.getImoNumber()))
                .findFirst()
                .orElseThrow(()-> new NoSuchElementException(String.format("No such asset with imo number %s is found", LAURA.getImoNumber())))
                .clickUpdateEorExpirationDateButton();

        assert_().withFailureMessage(String.format("Existing Asset Expiry date should be %s", now))
                .that(assetExpiryDatePage.getExistingAssetExpiryDate())
                .isEqualTo(FRONTEND_TIME_FORMAT_DTS.format(now));
    }
}
