package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect.AssetSortByOptions;
import main.landing.LandingPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class SortingInFleetDashboardUpdatesSortingInServiceScheduleTest extends BaseTest
{
    @Test(description = "Changing a sort category on the fleet dashboard updates sorting on SS fleet")
    @Issue("LRCD-1759")
    @Features(FLEET_DASHBOARD)
    public void sortingInFleetDashboardUpdatesSortingInServiceScheduleTest()
    {
        LandingPage landingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword());

        //Change sorting in Fleet dash board
        landingPage.getFilterBarPage()
                .selectSortByDropDown(AssetSortByOptions.CLASS_STATUS.toString());

        //Switch to Service Schedule tab and get asset names
        List<String> assetNames = landingPage.clickServiceScheduleTab()
                .getTabularView()
                .getAssetTables()
                .stream()
                .map(e -> e.getAssetCard().getAssetName().toLowerCase())
                .collect(Collectors.toList());

        assert_().withFailureMessage("Asset cards should be ordered by " + AssetSortByOptions.ASSET_NAME.toString())
                .that(assetNames)
                .isOrdered();
    }
}
