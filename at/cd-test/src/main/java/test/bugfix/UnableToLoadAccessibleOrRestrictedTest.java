package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.sub.accessibleassets.AccessibleAssetsPage;
import main.administration.detail.sub.restrictedassets.RestrictedAssetsPage;
import main.administration.element.DataElement;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;

public class UnableToLoadAccessibleOrRestrictedTest extends BaseTest
{
    @Test(description = "UM-11/ UM-12: Unable to load Assets from Restricted Assets and Accessible Assets")
    @Issues({
            @Issue("LRCD-1064"),
            @Issue("LRCD-2476")
    })

    @Features(USER_MANAGEMENT)
    public void unableToLoadAccessibleOrRestrictedTest()
    {
        DataElement dataElement = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getRoleName().equals(LR_ADMIN.getRole()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(LR_ADMIN.getRole() + " not present. "));

        AdministrationDetailPage administrationDetailPage = dataElement.clickFurtherMoreDetailsButton();
        AccessibleAssetsPage accessibleAssetsPage = administrationDetailPage
                .clickAccessibleAssetsTab();

        int accessibleAssetCards = accessibleAssetsPage.getAccesibleAssetCards().size();
        if (accessibleAssetCards > 0)
        {
            assert_().withFailureMessage("Accessible Asset Cards are expected to be displayed ")
                    .that(accessibleAssetCards)
                    .isGreaterThan(0);
        }
        AppHelper.scrollToBottom();
        assert_().withFailureMessage("After scrolling to the bottom of the page, more assets are expected to be displayed")
                .withFailureMessage(String.format("Expected more than %d to be displayed", accessibleAssetCards))
                .that(accessibleAssetsPage.getAccesibleAssetCards().size())
                .isAtLeast(accessibleAssetCards);


        RestrictedAssetsPage restrictedAssetsPage = administrationDetailPage.clickRestrictedAssetsTab();

        int restrictedAssetCards = restrictedAssetsPage.getRestrictedAssetCards().size();
        if (restrictedAssetCards > 0)
        {
            assert_().withFailureMessage("Restricted Asset Cards are expected to be displayed ")
                    .that(restrictedAssetCards)
                    .isGreaterThan(0);
        }

        AppHelper.scrollToBottom();
        assert_().withFailureMessage("After loading more assets, Restricted Asset Cards are expected to be displayed ")
                .that(restrictedAssetsPage.getRestrictedAssetCards().size() >= restrictedAssetCards)
                .isTrue();

        assert_().withFailureMessage("Accessible Asset Cards or restricted Asset Cards are expected to be displayed")
                .that(accessibleAssetCards > 0 || restrictedAssetCards > 0)
                .isTrue();
    }

}

