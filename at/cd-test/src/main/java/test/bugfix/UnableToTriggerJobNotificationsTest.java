package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Notification;

import static com.google.common.truth.Truth.assert_;
import static constant.Epic.NOTIFICATIONS;

public class UnableToTriggerJobNotificationsTest extends BaseTest
{
    @Test(description = "When Job Notification is triggered via Swagger, system is showing error code 500.")
    @Issue("LRCD-2218")
    @Features(NOTIFICATIONS)
    public void unableToTriggerJobNotificationsTest()
    {
        int statusCode = new Notification().getJobNotification();

        assert_().withFailureMessage("Job notification should return status code 200.")
                .that(HttpStatus.SC_OK)
                .isEqualTo(statusCode);
    }
}
