package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FilterAssetsByCodicilTypeTest extends BaseTest
{
    @Test(description = "Filter the asset by Codicil type and check the fleet dashboard")
    @Issues({
            @Issue("LRCD-1845"),
            @Issue("LRCD-1704")
    })
    @Features(FLEET_DASHBOARD)
    public void filterAssetsByCodicilTypeTest()
    {
        FilterBarPage filterBarPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage();

        FilterAssetsPage filterAssetsPage = filterBarPage
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deSelectClassStatusCheckBoxByStatusName(ClassDirect.ClassStatus.ALL.toString());

        int totalAssetCount = assertDefaultSelectedCodicilType(filterAssetsPage);

        //clear filter
        filterAssetsPage = filterBarPage.clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deSelectClassStatusCheckBoxByStatusName(ClassDirect.ClassStatus.ALL.toString());

        int totalAssetCountAfterClear = assertDefaultSelectedCodicilType(filterAssetsPage);

        //LRCD-1845
        assert_().withFailureMessage("All assets should be displayed even if no codicil type is selected.")
                .that(totalAssetCount)
                .isEqualTo(totalAssetCountAfterClear);

        //Select Codicil type: Actionable Item
        filterAssetsPage = filterBarPage.clickFilterButton(FilterAssetsPage.class)
                .selectCodicilTypeActionableItem()
                .clickSubmitButton(FilterBarPage.class)
                .clickFilterButton(FilterAssetsPage.class);

        //LRCD-1704
        assert_().withFailureMessage("Codicil type 'Actionable Item' is expected to be selected.")
                .that(filterAssetsPage.isCodicilTypeActionableItemsCheckboxSelected())
                .isTrue();
        assert_().withFailureMessage("Codicil type 'All' should remain not selected.")
                .that(filterAssetsPage.isCodicilTypeAllCheckboxSelected())
                .isFalse();
        assert_().withFailureMessage("Codicil type 'Condition of Class' should remain not selected.")
                .that(filterAssetsPage.isCodicilTypeConditionsOfClassCheckboxSelected())
                .isFalse();
    }

    private int assertDefaultSelectedCodicilType(FilterAssetsPage filterAssetsPage)
    {
        //LRCD-1845
        assert_().withFailureMessage("By default, codicil type 'All' should not be selected.")
                .that(filterAssetsPage.isCodicilTypeAllCheckboxSelected())
                .isFalse();

        assert_().withFailureMessage("By default, codicil type 'Condition of Class' should not be selected.")
                .that(filterAssetsPage.isCodicilTypeConditionsOfClassCheckboxSelected())
                .isFalse();

        assert_().withFailureMessage("By default, codicil type 'Actionable Item' should not be selected.")
                .that(filterAssetsPage.isCodicilTypeActionableItemsCheckboxSelected())
                .isFalse();

        return filterAssetsPage.clickSubmitButton(VesselListPage.class)
                .getShowingXofYPage()
                .getTotal();
    }
}
