package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.login.LoginPage;
import main.myaccount.MyAccountPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.NOTIFICATIONS;

public class LRAdminCanAbleToViewNotificationSectionTest extends BaseTest
{
    @Test(description = "Notification section should not be visible for LR Admin")
    @Issue("LRCD-2359")
    @Features(NOTIFICATIONS)
    public void lRAdminCanAbleToViewNotificationSectionTest()
    {
        MyAccountPage myAccountPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getTopBar()
                .clickMyAccountButton();

        assert_().withFailureMessage("Notification card should not be visible for LR Admin.")
                .that(myAccountPage.getNotificationsCard().exists())
                .isFalse();
    }
}
