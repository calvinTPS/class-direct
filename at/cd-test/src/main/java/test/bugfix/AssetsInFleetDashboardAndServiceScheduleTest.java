package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetQueryHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilActionableHDto;
import com.baesystems.ai.lr.cd.be.domain.dto.asset.CodicilDefectQueryHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;
import service.Assets;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;

public class AssetsInFleetDashboardAndServiceScheduleTest extends BaseTest {
    private final Assets assetService = new Assets();
    private final CodicilDefectQueryHDto codicilDefectQueryHDto = new CodicilDefectQueryHDto();

    //Test has been made invalid due to - LRCD-754 and LRD-10311
    @Test(description = "Assets mismatch between Fleet Dashboard & Service Schedule (multiple Assets) Page.", enabled = false)
    @Issues({
            @Issue("LRCD-1396"),
            @Issue("LRCD-1706")
    })
    @Features(Epic.SERVICE_SCHEDULE)
    public void assetsInFleetDashboardAndServiceScheduleTest() {
        List<AssetHDto> assetHDtoList = assetService.getAssets(new AssetQueryHDto());

        //Assets from back-end with Services and/or Codicils
        Long assetsWithServices = assetHDtoList.stream()
                .filter(this::hasServicesOrCodicils)
                .count();

        //Assets in fleet dashboard
        LandingPage landingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword());
        int fleetDashboardAssetCount = landingPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getShowingXofYPage()
                .getTotal();

        assert_().withFailureMessage("Total asset count in the fleet dashboard should be equal to the total count in backend.")
                .that(fleetDashboardAssetCount)
                .isEqualTo(assetHDtoList.size());

        //Assets in Service Schedule
        int serviceScheduleAssetCount = landingPage
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getShowingXofYPage()
                .getTotal();

        assert_().withFailureMessage("Total asset count in the service schedule should be equal to the total asset count with services/codicils in backend.")
                .that(serviceScheduleAssetCount)
                .isEqualTo(assetsWithServices);
    }

    private boolean hasServicesOrCodicils(AssetHDto assetHDto) {
        //this method returns true if the asset has services or codicils (Condition of Class/ Actionable items)
        CodicilActionableHDto codicilActionableHDto = assetService.postCodicilsQuery(assetHDto.getId().intValue(), codicilDefectQueryHDto);

        boolean hasServices = assetService.getServicesByAssetId(assetHDto.getId().intValue()).size() > 0;
        boolean hasConditionOfClass = codicilActionableHDto.getCocHDtos() != null && codicilActionableHDto.getCocHDtos().size() > 0;
        boolean hasActionableItems = codicilActionableHDto.getActionableItemHDtos() != null && codicilActionableHDto.getActionableItemHDtos().size() > 0;

        return hasServices || hasConditionOfClass || hasActionableItems;
    }
}
