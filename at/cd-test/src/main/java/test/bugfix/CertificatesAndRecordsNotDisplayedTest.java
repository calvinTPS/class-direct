package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.certificates.CertificatesAndRecordsCard;
import main.viewasset.certificates.element.CertificateElement;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset.WITH_CERTIFICATES;

public class CertificatesAndRecordsNotDisplayedTest extends BaseTest
{
    @Test(description = "The system frontend does not display list of certificates and records items on respective tabs.")
    @Issue("LRCD-2103")
    @Stories("LRCD-139")
    @Features(Epic.CERTIFICATES)
    public void certificatesAndRecordsNotDisplayedTest()
    {
        CertificatesAndRecordsCard certificatesAndRecordsCard = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .setSearchTextBox(WITH_CERTIFICATES.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s was not found", WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getCertificatesAndRecordsCard();

        List<CertificateElement> certificateElementList = certificatesAndRecordsCard.getCertificateElements();
        assert_().withFailureMessage("Certificates should be displayed in Certificate and Records card of Asset hub page.")
                .that(certificateElementList.size())
                .isGreaterThan(0);

        certificateElementList.forEach(e->
        {
            assert_().withFailureMessage("Certificate number should be displayed in Certificate and Records card of Asset hub page.")
                    .that(e.getCertificateNumber())
                    .isNotEmpty();
            assert_().withFailureMessage("Issue date should be displayed in Certificate and Records card of Asset hub page.")
                    .that(e.getIssuedDate())
                    .isNotEmpty();
        });

        assert_().withFailureMessage("Certificates should be displayed in certificates page.")
                .that(certificatesAndRecordsCard.clickViewAllButton().getCertificateElements().size())
                .isGreaterThan(0);

    }
}
