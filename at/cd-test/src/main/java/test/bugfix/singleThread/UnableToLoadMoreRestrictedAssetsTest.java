package test.bugfix.singleThread;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.sub.accessibleassets.AccessibleAssetsPage;
import main.administration.detail.sub.restrictedassets.RestrictedAssetsPage;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;

public class UnableToLoadMoreRestrictedAssetsTest extends BaseTest
{
    private final int maxDisplayedAsset = 16;
    private final String username = LR_ADMIN.getUserName();
    @Test(description = "If more than 16 Assets are returned, the System will load the first 16 Assets" +
            " and will load the next 16 as the User scrolls down the screen.")
    @Issue("LRCD-783")
    public void unableToLoadMoreRestrictedAssetsTest()
    {
        RestrictedAssetsPage restrictedAssetsPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserName().equals(username))
                .findFirst()
                .orElseThrow(()-> new NoSuchElementException(String.format("No such card with user %s is found", username)))
                .clickFurtherMoreDetailsButton()
                .clickRestrictedAssetsTab();

        AppHelper.scrollToBottom();
        int restrictedAssetsCount = restrictedAssetsPage
                .getAdministrationDetailPage()
                .clickAccessibleAssetsTab()
                .getAdministrationDetailPage()
                .clickRestrictedAssetsTab()
                .getRestrictedAssetCards()
                .size();

        if (restrictedAssetsCount <= maxDisplayedAsset)
        {
            //ensure there are restricted assets
            restrictedAssetsPage.getAdministrationDetailPage()
                    .clickAccessibleAssetsTab()
                    .clickEditButton()
                    .clickSelectAllButton()
                    .clickMoveToRestrictedAssetButton()
                    .clickConfirmButton(AccessibleAssetsPage.class)
                    .getAdministrationDetailPage()
                    .clickRestrictedAssetsTab();
        }
        else
        {
            restrictedAssetsPage = restrictedAssetsPage
                    .getAdministrationDetailPage()
                    .clickBackButton()
                    .getAdministrationTable()
                    .getDataElements()
                    .stream()
                    .filter(e -> e.getUserName().equals(username))
                    .findFirst()
                    .orElseThrow(()-> new NoSuchElementException(String.format("No such card with user %s is found", username)))
                    .clickFurtherMoreDetailsButton()
                    .clickRestrictedAssetsTab();
        }

        assert_().withFailureMessage("Default assets loaded should be 16")
                .that(restrictedAssetsPage
                        .getRestrictedAssetCards()
                        .size())
                .isEqualTo(maxDisplayedAsset);

        AppHelper.scrollToBottom();
        restrictedAssetsCount = restrictedAssetsPage
                .getAdministrationDetailPage()
                .clickAccessibleAssetsTab()
                .getAdministrationDetailPage()
                .clickRestrictedAssetsTab()
                .getRestrictedAssetCards()
                .size();

        assert_().withFailureMessage("More assets should be displayed")
                .that(restrictedAssetsCount)
                .isGreaterThan(maxDisplayedAsset);
    }

    @AfterTest(alwaysRun = true)
    private void cleanUp(){
        RestrictedAssetsPage restrictedAssetsPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserName().equals(username))
                .findFirst()
                .orElseThrow(()-> new NoSuchElementException(String.format("No such card with user %s is found", username)))
                .clickFurtherMoreDetailsButton()
                .clickRestrictedAssetsTab();

        if (restrictedAssetsPage.getRestrictedAssetCards().size() > 0)
        {
            restrictedAssetsPage
                    .clickEditButton()
                    .clickSelectAllButton()
                    .clickMoveToAccessibleAssetButton()
                    .clickConfirmButton(AdministrationDetailPage.class);

        }

        BaseTest.tearDownBrowser();
    }

}




