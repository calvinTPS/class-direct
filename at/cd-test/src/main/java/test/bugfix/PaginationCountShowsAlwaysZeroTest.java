package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class PaginationCountShowsAlwaysZeroTest extends BaseTest
{
    @Test(description = "When search an asset there is no indication of result of 'Showing 0 of 0 assets'")
    @Issue("LRCD-600")
    @Features(FLEET_DASHBOARD)
    public void paginationCountShowsAlwaysZeroTest()
    {
        VesselListPage vesselListPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class);

        assert_().withFailureMessage("Shown Asset count should be greater than zero.")
                .that(vesselListPage.getShowingXofYPage().getShown())
                .isGreaterThan(0);

        assert_().withFailureMessage("Total Asset count should be greater than zero.")
                .that(vesselListPage.getShowingXofYPage().getTotal())
                .isGreaterThan(0);
    }
}
