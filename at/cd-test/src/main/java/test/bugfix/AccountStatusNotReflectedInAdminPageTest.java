package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.administration.detail.AdministrationDetailPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Credentials.SHIP_BUILDER_USER;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.AccountStatus.Active;
import static constant.User.AccountStatus.Disabled;

public class AccountStatusNotReflectedInAdminPageTest extends BaseTest
{
    @Test(description = "Account status change is not reflected in the Admin User List page when we navigate from the user details page.")
    @Issue("LRCD-839")
    @Features(USER_MANAGEMENT)

    public void accountStatusNotReflectedInAdminPageTest()
    {
        AdministrationPage administrationPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();
        AdministrationDetailPage administrationDetailPage = administrationPage.getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getRoleName().equals(SHIP_BUILDER_USER.getRole()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("Builder user %s is not present", SHIP_BUILDER_USER.getRole())))
                .clickFurtherMoreDetailsButton()
                .getAccessibleAssetsPage()
                .getAdministrationDetailPage();

        String accountStatus = administrationDetailPage.getAccountStatus();
        if (accountStatus.equals(Active.getAccountStatus()))
        {
            administrationDetailPage.setAccountStatus(Disabled.getAccountStatus())
                    .clickConfirmButton();
            accountStatus = Disabled.getAccountStatus();
        }
        else
        {
            administrationDetailPage.setAccountStatus(Active.getAccountStatus())
                    .clickConfirmButton();
            accountStatus = Active.getAccountStatus();
        }

        assert_().withFailureMessage("Account status is expected to be " + accountStatus)
                .that(administrationDetailPage.clickBackButton()
                        .getAdministrationTable()
                        .getDataElements()
                        .stream()
                        .filter(e -> e.getRoleName().equals(SHIP_BUILDER_USER.getRole()))
                        .findFirst()
                        .orElseThrow(() -> new RuntimeException(String.format("Builder user %s is not present", SHIP_BUILDER_USER.getRole())))
                        .getAccountStatus())
                .isEqualTo(accountStatus);

        // Clean up - set account back to active
        // FIXME: There is a danger that disabling account for tests causes race conditions with other tests
        administrationDetailPage = administrationPage.getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getRoleName().equals(SHIP_BUILDER_USER.getRole()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("Builder user %s is not present", SHIP_BUILDER_USER.getRole())))
                .clickFurtherMoreDetailsButton();

        accountStatus = administrationDetailPage.getAccountStatus();
        if (accountStatus.equals(Disabled.getAccountStatus()))
        {
            administrationDetailPage.setAccountStatus(Active.getAccountStatus())
                    .clickConfirmButton();
        }
    }
}
