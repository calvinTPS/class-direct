package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.actionableitems.ActionableItemDetailsPage;
import main.viewasset.codicils.codicilsanddefects.sub.assetnotes.AssetNoteDetailsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;

public class UnableToViewItemsForAssetNoteInDetailsPageTest extends BaseTest
{
    @Test(description = "CO-27: Unable to view the Items for the Actionable items and Asset Notes in the detailed description page")
    @Issue("LRCDT-1321")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void unableToViewItemsForAssetNoteInDetailsPageTest()
    {
        CodicilsAndDefectsPage codicilsAndDefectsPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton();

        ActionableItemDetailsPage actionableItemDetailsPage = codicilsAndDefectsPage.clickActionableItemsTab()
                .getCodicilElements()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Actionable Item not found."))
                .clickArrowButton(ActionableItemDetailsPage.class);

        assert_().withFailureMessage("Title is expected to be displayed.")
                .that(actionableItemDetailsPage.getTitle())
                .isNotEmpty();
        assert_().withFailureMessage("Category is expected to be displayed.")
                .that(actionableItemDetailsPage.getCategory())
                .isNotEmpty();
        assert_().withFailureMessage("Imposed date is expected to be displayed.")
                .that(actionableItemDetailsPage.getImposedDate())
                .isNotEmpty();
        assert_().withFailureMessage("Status is expected to be displayed.")
                .that(actionableItemDetailsPage.getStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Job number is expected to be displayed.")
                .that(actionableItemDetailsPage.getJobNumber())
                .isNotEmpty();
        assert_().withFailureMessage("Items are expected to be displayed.")
                .that(actionableItemDetailsPage.getItems())
                .isNotEmpty();

        AssetNoteDetailsPage assetNoteDetailsPage = actionableItemDetailsPage.getAssetHeaderPage()
                .clickBackButton(CodicilsAndDefectsPage.class)
                .clickAssetNotesTab()
                .getCodicilElements()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Asset note not found."))
                .clickArrowButton(AssetNoteDetailsPage.class);

        assert_().withFailureMessage("Category is expected to be displayed.")
                .that(assetNoteDetailsPage.getCategory())
                .isNotEmpty();
        assert_().withFailureMessage("Imposed date is expected to be displayed.")
                .that(assetNoteDetailsPage.getImposedDate())
                .isNotEmpty();
        assert_().withFailureMessage("Status is expected to be displayed.")
                .that(assetNoteDetailsPage.getStatus())
                .isNotEmpty();
        assert_().withFailureMessage("Job number is expected to be displayed.")
                .that(assetNoteDetailsPage.getJobNumber())
                .isNotEmpty();
        assert_().withFailureMessage("Items are expected to be displayed.")
                .that(assetNoteDetailsPage.getItems())
                .isNotEmpty();
    }
}
