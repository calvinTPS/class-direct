package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;
import static constant.User.Flag.JAPAN;

public class NotMatchedResultForFilterAssetsByFlagTest extends BaseTest
{
    @Test(description = "Filter result is not matching for Filter Assets by Flag")
    @Issue("LRCD-601")
    @Features(FLEET_DASHBOARD)
    public void notMatchedResultForFilterAssetsByFlagTest()
    {
        final String flagType = JAPAN.getName();

        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickFlagButton()
                .setSearchFlagTextBox(flagType)
                .clickSubmitButton()
                .clickSubmitButton(VesselListPage.class);

        List<AssetCard> assetCards = vesselListPage.getAssetCards();
        for (AssetCard assetCard : assetCards)
        {
            assert_().withFailureMessage("Asset with Flag " + flagType + " are expected to displayed")
                    .that(assetCard.getFlagName())
                    .isEqualTo(flagType);
        }
    }
}
