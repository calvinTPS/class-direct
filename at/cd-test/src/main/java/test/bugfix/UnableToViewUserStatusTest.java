package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import main.administration.element.DataElement;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;

public class UnableToViewUserStatusTest extends BaseTest
{
    @Test(description = "Unable to view User Status in user details page - Android phone")
    @Issue("LRCD-1078")
    @Features(USER_MANAGEMENT)
    public void unableToViewUserStatusTest()
    {
        DataElement dataElement = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e-> e.getUserEmail().equals(LR_ADMIN.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected user with email %s not found.", LR_ADMIN.getEmail())));

        AppHelper.shrinkIntoMobileView();

        assert_().withFailureMessage("Account Status should not be empty.")
                .that(dataElement.clickFurtherMoreDetailsButton().getAccountStatus())
                .isNotEmpty();
    }
}
