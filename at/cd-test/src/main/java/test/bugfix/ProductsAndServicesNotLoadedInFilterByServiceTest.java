package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ServiceCatalogueHDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.servicetype.base.ServiceTable;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;
import service.ReferenceData;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SERVICE_SCHEDULE;

public class ProductsAndServicesNotLoadedInFilterByServiceTest extends BaseTest
{
    @Test(description = "SS: System not loading the products and services when we select Filter by Product in AWS for the first time.",
            threadPoolSize = 4)
    @Issues({
            @Issue("LRCD-1499"),
            @Issue("LRCD-1860")
    })
    @Features(SERVICE_SCHEDULE)
    public void productsAndServicesNotLoadedInFilterByServiceTest()
    {
        //Products and Services in Front-end
        List<ServiceTable> serviceTableList = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickServiceTypeButton()
                .getServiceTables();

        //Service Catalogues in Back-end
        List<ServiceCatalogueHDto> serviceCatalogueHDtoList = new ReferenceData().getServiceCatalogues();

        //Products
        List<ProductCatalogueDto> productCatalogueDtoList = serviceCatalogueHDtoList.parallelStream()
                .map(ServiceCatalogueHDto::getProductCatalogue)
                .filter(productCatalogueDto -> productCatalogueDto.getDeleted().equals(false))
                .distinct()
                .collect(Collectors.toList());

        //Product names from FE
        List<String> productNamesFromTable = serviceTableList
                .stream()
                .map(ServiceTable::getProductName)
                .collect(Collectors.toList());

        //Product names from BE
        List<String> productNamesFromBE = productCatalogueDtoList
                .parallelStream()
                .map(ProductCatalogueDto::getName)
                .map(String::trim)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Products on FE are expected to match with BE")
                .that(productNamesFromTable)
                .containsExactlyElementsIn(productNamesFromBE);

        //Count services per product from FE
        Map<String, Long> servicesFECount = serviceTableList
                .stream()
                .collect(Collectors.toMap(ServiceTable::getProductName,
                        ServiceTable::getServiceCount)
                );
        //sort the entries
        Map<String, Long> sortedServicesFECount = new TreeMap<>(servicesFECount);

        //Count services per product from BE
        Map<String, Long> servicesBECount = serviceCatalogueHDtoList
                .parallelStream()
                .filter(c -> c.getDeleted().equals(false))
                .collect(Collectors.groupingBy(c -> c.getProductCatalogue().getName().trim(), Collectors.counting()));
        //sort the entries
        Map<String, Long> sortedServicesBECount = new TreeMap<>(servicesBECount);

        assert_().withFailureMessage("Services count per product is expected to match with BE")
                .that(sortedServicesFECount)
                .containsExactlyEntriesIn(sortedServicesBECount);
    }
}

