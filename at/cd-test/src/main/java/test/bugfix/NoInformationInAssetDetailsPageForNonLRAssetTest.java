package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.RegistryInformationDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect.Asset;
import main.assetdetail.element.RegistryInformationCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;
import service.Assets;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.ASSET_DETAILS_DATE_FORMAT;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ASSET_HUB;

public class NoInformationInAssetDetailsPageForNonLRAssetTest extends BaseTest
{
    @Test(description = "AI-02: IHS data is displayed in the Detailed information page instead of data from MAST.")
    @Issues({
            @Issue("LRCD-1731"),
            @Issue("LRCD-1732"),
            @Issue("LRCD-2107")
    })
    @Features(ASSET_HUB)
    public void noInformationInAssetDetailsPageForNonLRAssetTest()
    {
        final String EMPTY_VALUE = "-";

        RegistryInformationCard registryInformationCard = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.NON_LR_ASSET.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Expected non-LR asset card not found."))
                .clickViewAssetButton()
                .getAssetHeaderPage()
                .clickAssetDetailsButton()
                .getRegistryInformationCard();

        RegistryInformationDto registryInformationDto = new Assets().getAssetDetails(Asset.NON_LR_ASSET.getImoNumber()).getRegistryInformation();

        String expected = registryInformationDto == null ? null : registryInformationDto.getPortOfRegistry();
        assert_().withFailureMessage("Port registry should be displayed correctly.")
                .that(registryInformationCard.getPortOfRegistry())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getCallSign();
        assert_().withFailureMessage("Call Sign should be displayed correctly.")
                .that(registryInformationCard.getCallSign())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getOfficialNumber();
        assert_().withFailureMessage("Official Number should be displayed correctly.")
                .that(registryInformationCard.getOfficialNumber())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getAssetTypeDetails();
        assert_().withFailureMessage("Ship type details should be displayed correctly.")
                .that(registryInformationCard.getShipTypeDetails())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getFormerAssetNames();
        assert_().withFailureMessage("Former asset name should be displayed correctly.")
                .that(registryInformationCard.getFormerAssetName())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getYard();
        assert_().withFailureMessage("Yard should be displayed correctly.")
                .that(registryInformationCard.getYard())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getYardNumber();
        assert_().withFailureMessage("Yard number should be displayed correctly.")
                .that(registryInformationCard.getYardNumber())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getKeelLayingDate();
        assert_().withFailureMessage("Keel Laying date should be displayed correctly.")
                .that(registryInformationCard.getKeelLayingDate())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getDateOfBuild();
        assert_().withFailureMessage("Date of build should be displayed correctly.")
                .that(registryInformationCard.getDateOfBuild() != null ?
                        ASSET_DETAILS_DATE_FORMAT.format(registryInformationCard.getDateOfBuild()) :
                        EMPTY_VALUE)
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getYearOfBuild();
        assert_().withFailureMessage("Year of build should be displayed correctly.")
                .that(registryInformationCard.getYearOfBuild())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getCountryOfBuild();
        assert_().withFailureMessage("Country of build should be displayed correctly.")
                .that(registryInformationCard.getCountryOfBuild())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getFlag();
        assert_().withFailureMessage("Flag should be displayed correctly.")
                .that(registryInformationCard.getFlag())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getBuilder();
        assert_().withFailureMessage("Builder should be displayed correctly.")
                .that(registryInformationCard.getBuilder())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);

        expected = registryInformationDto == null ? null : registryInformationDto.getMmsiNumber();
        assert_().withFailureMessage("MMSI Number should be displayed correctly.")
                .that(registryInformationCard.getMmsiNumber())
                .isEqualTo(expected == null ? EMPTY_VALUE :
                        expected.equals("") ? EMPTY_VALUE : expected);
    }
}
