package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.nationaladmin.NationalAdministrationPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.COUNTRY_FILES;
import static constant.User.Flag.ANGOLA;

public class UnableToViewListOfCountryFilesTest extends BaseTest
{
    @Test(description = "Unable to view the list of country files for a country.")
    @Issue("LRCD-2343")
    @Features(COUNTRY_FILES)
    public void unableToViewListOfCountryFilesTest()
    {
        final String flagName = ANGOLA.getName();

        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword());
        NationalAdministrationPage nationalAdministrationPage = LandingPage.getTopBar().clickNationalAdministrationButton();

        nationalAdministrationPage.selectFlagName(flagName);

        assert_().withFailureMessage(String.format("Country file of version in use should be displayed for %s.", flagName))
                .that(nationalAdministrationPage.getCountryFileVersionInUse().exists())
                .isTrue();
        assert_().withFailureMessage(String.format("Country files of older version should be displayed for %s.", flagName))
                .that(nationalAdministrationPage.getCountryFilesOlderVersion().size())
                .isGreaterThan(0);
    }
}
