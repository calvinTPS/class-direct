package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterJobsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SERVICE_HISTORY;

public class IncorrectLabelForLastVisitDateInFilterPanelTest extends BaseTest
{
    @Test(description = "Issue in Filter by Last Visit date, system displays filter by Due date in the filter panel")
    @Issues({
            @Issue("LRCD-1322"),
            @Issue("LRCD-1104")
    })
    @Features(SERVICE_HISTORY)
    public void incorrectLabelForLastVisitDateInFilterPanelTest()
    {
        final String expectedLastVisitDateHeaderText = "Last visit date";

        String lastVisitDateHeaderText = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(LAURA.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset not found."))
                .clickViewAssetButton()
                .getSurveysCard()
                .clickViewAllButton()
                .getFilterBarPage()
                .clickFilterButton(FilterJobsPage.class)
                .getLastVisitedDateAccordionHeaderText();

        assert_().withFailureMessage(String.format("Expected header text for this accordion is '%s'", expectedLastVisitDateHeaderText))
                .that(lastVisitDateHeaderText)
                .isEqualTo(expectedLastVisitDateHeaderText);
    }
}
