package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class InconsistentSearchResultsInFleetDashboardTest extends BaseTest
{
    private final List<String> searchStrings = Arrays.asList("lau", "opa", "cele", "celes", "dea", "deav");

    @Test(description = "Search results in fleet dashboard is not filtering correctly")
    @Issue("LRCD-1024")
    @Features(FLEET_DASHBOARD)
    public void inconsistentSearchResultsInFleetDashboardTest()
    {

        VesselListPage vesselListPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .selectSearchDropDown("Asset name / IMO number")
                .getPageReference(VesselListPage.class);

        // write once to activate search, force overwrite immediately to re-search
        for (int i = 0; i < searchStrings.size(); i = i + 2)
        {
            vesselListPage.getFilterBarPage()
                    .setSearchTextBox(searchStrings.get(i), 300, TimeUnit.MILLISECONDS) //search and don't wait for script to finish
                    .setSearchTextBox(searchStrings.get(i + 1)); //immediately force re-search

            for (AssetCard assetCard : vesselListPage.getAssetCards())
            {
                assert_().withFailureMessage("Asset name is expected to contain " + searchStrings.get(i + 1))
                        .that(assetCard.getAssetName().toLowerCase())
                        .contains(searchStrings.get(i + 1));
            }
        }

    }
}
