package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.flag.FilterByFlagPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FilterAssetsByFlagBehaviourTest extends BaseTest
{
    @Test(description = "Filter asset by flag behaviour test")
    @Issue("LRCD-592")
    @Features(FLEET_DASHBOARD)
    public void filterAssetsByFlagBehaviourTest()
    {
        FilterByFlagPage filterByFlagPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickFlagButton();
        filterByFlagPage.getSearchFlagTextBox().click();
        assert_().withFailureMessage("Lookahead dropdown is expected not to be displayed")
                .that(filterByFlagPage.getSearchFlagTextBox().isDropdownVisible())
                .isFalse();
    }
}
