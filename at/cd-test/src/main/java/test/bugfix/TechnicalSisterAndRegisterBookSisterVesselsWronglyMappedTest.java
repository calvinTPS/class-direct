package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import main.assetdetail.AssetDetailsPage;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.assetdetail.sistervessels.SisterVesselsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Assets;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ASSET_HUB;

public class TechnicalSisterAndRegisterBookSisterVesselsWronglyMappedTest extends BaseTest
{
    @Test(description = "In Asset Hub, it is showing the same assets for both Technical Sister Vessels & Register book sister Vessels, which is incorrect.")
    @Issue("LRCD-1705")
    @Features(ASSET_HUB)
    public void technicalSisterAndRegisterBookSisterVesselsWronglyMappedTest()
    {
        Assets assets = new Assets();

        //Get Register Book Sisters Asset and Technical Sisters Assets in the back-end
        List<String> registerBookSisterAssetsBE = assets.getRegisterBookSisters(LAURA.getAssetCode())
                .stream()
                .map(AssetHDto::getName)
                .collect(Collectors.toList());
        List<String> technicalSistersAssetsBE = assets.getTechnicalSisters(LAURA.getAssetCode())
                .stream()
                .map(AssetHDto::getName)
                .collect(Collectors.toList());

        AssetDetailsPage assetDetailsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getAssetHeaderPage()
                .clickAssetDetailsButton();

        if(registerBookSisterAssetsBE.size() > 0)
        {
            SisterVesselsPage registerBookSisterVesselsPage = assetDetailsPage
                    .getRegisterBookSisterVesselsCard()
                    .clickViewAllLink();

            List<String> registerBookSisterAssetsFE = registerBookSisterVesselsPage.getAssetCards()
                    .stream()
                    .map(AssetCard::getAssetName)
                    .collect(Collectors.toList());

            assert_().withFailureMessage("Register Book Sisters Asset in the front-end should match the back-end.")
                    .that(registerBookSisterAssetsFE)
                    .isEqualTo(registerBookSisterAssetsBE);

            registerBookSisterVesselsPage.navigateToAssetDetailsPage();
        }
        else
        {
            assert_().withFailureMessage("Register Book Sisters Asset count should be zero")
                    .that(assetDetailsPage.getRegisterBookSisterVesselsCard().getTotalRegisterBookSisterVessels())
                    .isEqualTo(0);
        }

        if(technicalSistersAssetsBE.size() > 0)
        {
            List<String> technicalSistersAssetsFE = assetDetailsPage
                    .getTechnicalSisterVesselsCard()
                    .clickViewAllLink()
                    .getAssetCards()
                    .stream()
                    .map(AssetCard::getAssetName)
                    .collect(Collectors.toList());

            assert_().withFailureMessage("Technical Sisters Asset in the front-end should match the back-end.")
                    .that(technicalSistersAssetsFE)
                    .isEqualTo(technicalSistersAssetsBE);
        }
        else
        {
            assert_().withFailureMessage("Technical Sisters Asset count should be zero")
                    .that(assetDetailsPage.getTechnicalSisterVesselsCard().getTotalTechnicalSisterVessels())
                    .isEqualTo(0);
        }
    }
}
