package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FilterAssetsByClassStatusTest extends BaseTest
{
    private final String classStatusAll = ClassDirect.ClassStatus.ALL.toString();
    private final String classStatusInClassPending = ClassDirect.ClassStatus.CLASSED_PENDING_CLASS_EXEC_APPROVAL.toString();
    private final String classStatusSuspended = ClassDirect.ClassStatus.CLASSED_SUSPENDED.toString();

    @Test(description = "Filter the asset by Class Status and check the fleet dashboard")
    @Issue("LRCD-1096")
    @Features(FLEET_DASHBOARD)
    public void filterAssetsByClassStatusTest()
    {
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deSelectClassStatusCheckBoxByStatusName(classStatusAll)
                .selectClassStatusCheckBoxByStatusName(classStatusSuspended)
                .clickSubmitButton(VesselListPage.class);

        vesselListPage.getAssetCards()
                .forEach(e ->
                        assert_().withFailureMessage(
                                String.format("Class status of this asset: %s should be %s", e.getAssetName(), classStatusSuspended))
                                .that(e.getClassStatus())
                                .isEqualTo(classStatusSuspended)
                );

        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectClassStatusCheckBoxByStatusName(classStatusInClassPending)
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .forEach(e ->
                        assert_().withFailureMessage(
                                String.format("Class status of this asset: %s should be %s", e.getAssetName(), classStatusInClassPending))
                                .that(e.getClassStatus())
                                .isEqualTo(classStatusInClassPending)
                );

        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton();
    }
}
