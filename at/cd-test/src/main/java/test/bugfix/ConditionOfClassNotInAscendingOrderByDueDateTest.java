package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Ordering;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_DATE_FORMAT;

public class ConditionOfClassNotInAscendingOrderByDueDateTest extends BaseTest
{
    @Test(description = "Condition of Class is not Sorted by ascending order of Due date by Default")
    @Issue("LRCD-1094")
    @Features(Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void conditionOfClassNotInAscendingOrderByDueDateTest()
    {
        List<Date> dueDates = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickConditionsOfClassTab()
                .getCodicilElements()
                .stream()
                .map(map ->
                {
                    try
                    {
                        return FRONTEND_DATE_FORMAT.parse(map.getDueDate());
                    }
                    catch (ParseException e)
                    {
                        return null;
                    }
                })
                .collect(Collectors.toList());

        dueDates.forEach(
                e -> assert_().withFailureMessage("Due dates should be in dd MMM yyyy format.")
                        .that(e)
                        .isNotNull()
        );

        assert_().withFailureMessage("Due dates of codicils should be in ascending order by default.")
                .that(Ordering.natural().isOrdered(dueDates))
                .isTrue();
    }
}
