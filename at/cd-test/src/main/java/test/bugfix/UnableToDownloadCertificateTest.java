package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.export.CertificateAttachDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DownloadReportHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Certificates;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.CERTIFICATES;
import static constant.ClassDirect.Asset.WITH_CERTIFICATES;

public class UnableToDownloadCertificateTest extends BaseTest
{
    @Test(description = "Unable to download certificates and records when click on the download button.")
    @Issue("LRCDT-2127")
    @Features(CERTIFICATES)
    public void unableToDownloadCertificateTest()
    {
        final String officeSearchString = "Bordeaux";
        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(WITH_CERTIFICATES.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s not found.", WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getCertificatesAndRecordsCard()
                .clickViewAllButton().getCertificateElements()
                .stream()
                .filter(e-> e.getOffice().equals(officeSearchString))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected certificate with office: %s is not found.", officeSearchString)))
                .clickDownloadButton();

        CertificateAttachDto certificateAttachDto = new CertificateAttachDto();
        certificateAttachDto.setCertId(10L);
        certificateAttachDto.setJobId(110023L);
        String certificateToken = new Certificates().getCertificateAttachmentToken(certificateAttachDto);

        assert_().withFailureMessage(String.format("Certificate attachment for ID: %s should be downloaded successfully.", certificateAttachDto.getCertId()))
                .that(DownloadReportHelper.getDownloadFileChecksum(certificateToken))
                .isNotEmpty();
    }
}
