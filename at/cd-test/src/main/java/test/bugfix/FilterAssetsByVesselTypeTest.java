package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.assettype.FilterByAssetTypePage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.AssetTableName.NON_SHIP_STRUCTURES;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FilterAssetsByVesselTypeTest extends BaseTest
{
    @Test(description = "Filter assets by vessel type - Selected vessel type is not visible after setting up the filter by vessel type")
    @Issue("LRCDT-1034")
    @Features(FLEET_DASHBOARD)
    public void filterAssetsByVesselTypeTest()
    {
        FilterByAssetTypePage filterByAssetTypePage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickAssetTypeButton();
        filterByAssetTypePage.getAssetCardsByTableName(NON_SHIP_STRUCTURES.getTableName())
                .getAssetType()
                .get(0)
                .selectAssetType();

        VesselListPage vesselListPage = filterByAssetTypePage.clickSubmitButton()
                .clickSubmitButton(VesselListPage.class);

        assert_().withFailureMessage("Asset Cards is expected to be displayed")
                .that(vesselListPage.getAssetCards().size())
                .isGreaterThan(0);

    }
}
