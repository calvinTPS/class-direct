package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class UnableToViewMyFavouritesTest extends BaseTest
{
    @Test(description = "FI-04 - Unable to view My Favourites")
    @Issue("LRCD-1102")
    @Features(FLEET_DASHBOARD)
    public void unableToViewMyFavouritesTest()
    {
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class);

        //If a non-favourite asset exists in the initial assets shown, mark it as favourite
        vesselListPage.getAssetCards()
                .stream()
                .filter(e -> !e.isFavourite())
                .findFirst()
                .ifPresent(AssetCard::selectFavourite);

        //Filter by My Favourites
        vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectMyFavouritesRadioButton()
                .clickSubmitButton(VesselListPage.class);

        assert_().withFailureMessage("Assets marked as favourite should be shown.")
                .that(vesselListPage.getAssetCards().size())
                .isGreaterThan(0);

        assert_().withFailureMessage("Number of favourite assets shown should match the Shown Count in pagination text.")
                .that(vesselListPage.getShowingXofYPage().getShown())
                .isEqualTo(vesselListPage.getAssetCards().size());

        //Scroll to bottom until all favourite assets are displayed
        int numberOfScrollToBottom = vesselListPage.getShowingXofYPage().getTotal() / 16;
        for (int i = 0; i < numberOfScrollToBottom; i++)
        {
            AppHelper.scrollToBottom();
        }

        // Removed due to Y not being accurate, refer to LRCD-754 and LRCD-10311
//        assert_().withFailureMessage("Number of favourite assets shown should match the Total Count in pagination text.")
//                .that(vesselListPage.getShowingXofYPage().getTotal())
//                .isEqualTo(vesselListPage.getFilterBarPage()
//                        .clickFilterButton(FilterAssetsPage.class)
//                        .clickSubmitButton(VesselListPage.class)
//                        .getAssetCards()
//                        .size());
    }
}
