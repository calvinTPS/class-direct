package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import constant.User;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.administration.adduser.steps.element.AccountTypeCard;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;

public class IncorrectAccountTypeDisplayedForLRInternalEmailTest extends BaseTest {
    @Test(description = "UM-27_AC3_Add User_Incorrect account type cards displayed for LR internal email id")
    @Issue("LRCD-2030")
    @Features(USER_MANAGEMENT)
    public void incorrectAccountTypeDisplayedForLRInternalEmailTest() {
        final String LR_INTERNAL_EMAIL = User.LR_INTERNAL_EMAIL;
        final String NON_ADMIN_ACCOUNT_TYPE = User.NON_ADMIN_ACCOUNT_TYPE;

        List<AccountTypeCard> accountTypeCardList = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickAddUserButton()
                .setEmailAddress(LR_INTERNAL_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("User card with " + User.LR_INTERNAL_EMAIL + " not found"))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards();

        assert_().withFailureMessage("Account type list should contain only one account type.")
                .that(accountTypeCardList.size())
                .isEqualTo(1);
        assert_().withFailureMessage(String.format("Account type for LR Internal Email should be %s.", NON_ADMIN_ACCOUNT_TYPE))
                .that(accountTypeCardList.get(0).getName())
                .isEqualTo(NON_ADMIN_ACCOUNT_TYPE);
    }
}
