package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.assettype.FilterByAssetTypePage;
import main.common.filter.servicetype.base.ServiceTable;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.AssetTableName.NON_SHIP_STRUCTURES;
import static constant.ClassDirect.ClassStatus;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.User.Flag.JAPAN;

public class ClearFilterDoesNotClearServiceTypeTest extends BaseTest
{
    @Test(description = "When user selects 'clear filters' in filter dropdown of service schedule page, "
            + "all the selected filters are cleared except the filters applied for 'Service type'", threadPoolSize = 4)
    @Issues({
            @Issue("LRCD-1496"),
            @Issue("LRCD-1551")
    })
    @Features(Epic.SERVICE_SCHEDULE)
    public void clearFilterDoesNotClearServiceTypeTest()
    {
        final String minGrossTonnage = "10";
        final String maxGrossTonnage = "10000";
        final String fromDateOfBuild = "10 Oct 2016";
        final String toDateOfBuild = "10 Oct 2018";

        //Filter by flag, gross tonnage, date of build, class status and codicil type
        FilterAssetsPage filterAssetsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickFlagButton()
                .setSearchFlagTextBox(JAPAN.getName())
                .clickSubmitButton()
                .setMinGrossTonnage(minGrossTonnage)
                .setMaxGrossTonnage(maxGrossTonnage)
                .setFromDateOfBuild(fromDateOfBuild)
                .setToDateOfBuild(toDateOfBuild)
                .deSelectClassStatusCheckBoxByStatusName(ClassStatus.ALL.toString())
                .selectClassStatusCheckBoxByStatusName(ClassStatus.CLASSED_LAID_UP.toString())
                .selectCodicilTypeActionableItem();

        //Filter by Asset type
        FilterByAssetTypePage filterByAssetTypePage = filterAssetsPage
                .clickAssetTypeButton();
        filterByAssetTypePage.getAssetCardsByTableName(NON_SHIP_STRUCTURES.getTableName())
                .getAssetType()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("List for %s is empty.", NON_SHIP_STRUCTURES.getTableName())))
                .selectAssetType();

        //Filter by Service Type
        filterByAssetTypePage.clickSubmitButton()
                .clickServiceTypeButton()
                .clickClearAllButton()
                .clickSubmitButton(FilterAssetsPage.class);

        //Submit then Clear filtering
        filterAssetsPage = filterAssetsPage
                .clickSubmitButton(FilterBarPage.class)
                .clickFilterButton(FilterAssetsPage.class)
                .clickClearFiltersButton()
                .clickFilterButton(FilterAssetsPage.class);

        assert_().withFailureMessage("By default, no flag is selected.")
                .that(filterAssetsPage.getSelectedFlags().size())
                .isEqualTo(0);
        assert_().withFailureMessage("Minimum gross tonnage is expected to be empty.")
                .that(filterAssetsPage.getMinGrossTonnage())
                .isEmpty();
        assert_().withFailureMessage("Maximum gross tonnage is expected to be empty.")
                .that(filterAssetsPage.getMaxGrossTonnage())
                .isEmpty();
        assert_().withFailureMessage("From Date of Build is expected to be empty.")
                .that(filterAssetsPage.getFromDateOfBuild())
                .isEmpty();
        assert_().withFailureMessage("To Date of Build is expected to be empty.")
                .that(filterAssetsPage.getToDateOfBuild())
                .isEmpty();

        assert_().withFailureMessage("By default, codicil type 'All' should not be selected.")
                .that(filterAssetsPage.isCodicilTypeAllCheckboxSelected())
                .isFalse();
        assert_().withFailureMessage("By default, codicil type 'Condition of Class' should not be selected.")
                .that(filterAssetsPage.isCodicilTypeConditionsOfClassCheckboxSelected())
                .isFalse();
        assert_().withFailureMessage("By default, codicil type 'Actionable Item' should not be selected.")
                .that(filterAssetsPage.isCodicilTypeActionableItemsCheckboxSelected())
                .isFalse();

        filterAssetsPage.getClassStatusCheckBox()
                .forEach(e -> assert_().withFailureMessage("By default, all class status are selected.")
                        .that(e.isSelected())
                        .isTrue());

        //Clear filtering of Asset Type - LRCD-1551
        assert_().withFailureMessage("No asset type should be selected.")
                .that(filterAssetsPage
                        .clickAssetTypeButton()
                        .getAssetCardsByTableName(NON_SHIP_STRUCTURES.getTableName())
                        .getAssetType()
                        .parallelStream()
                        .findFirst()
                        .orElseThrow(() -> new NoSuchElementException(String.format("List for %s is empty.", NON_SHIP_STRUCTURES.getTableName())))
                        .isSelected())
                .isFalse();

        List<ServiceTable> serviceTableList =
                filterByAssetTypePage
                        .clickCancelButton()
                        .clickServiceTypeButton()
                        .getServiceTables();
        serviceTableList.forEach(ServiceTable::expandServiceTable);

        // this assertion is done in a way where it will only grab the product name and service name when it fails
        // this is to significantly reduce looping and subsequently test time
        serviceTableList.parallelStream()
                .flatMap(st -> st.getServices().parallelStream())
                .filter(serviceElement -> !serviceElement.isCheckboxSelected()) // find if any of the boxes is unchecked i.e. failure
                .findFirst()
                .ifPresent(serviceElement -> assert_()
                        .withFailureMessage(serviceElement.getParentServiceTable().getProductName() + ": " + serviceElement
                                .getServiceName() + " is expected to be checked")
                        .that(serviceElement.isCheckboxSelected())
                        .isTrue()
                );
    }
}
