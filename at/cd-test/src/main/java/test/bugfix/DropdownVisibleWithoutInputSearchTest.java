package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.flag.FilterByFlagPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;
import static constant.User.Flag.JAPAN;

public class DropdownVisibleWithoutInputSearchTest extends BaseTest
{
    @Test(description = "FI-07:AC3:0:Search box displays the drop down options without any search terms")
    @Issue("LRCD-595")
    @Features(FLEET_DASHBOARD)

    public void dropdownVisibleWithoutInputSearchTest()
    {
        FilterByFlagPage filterByFlagPage =
                LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                        .getVesselListPage()
                        .getFilterBarPage()
                        .clickFilterButton(FilterAssetsPage.class)
                        .clickFlagButton();

        filterByFlagPage.setSearchFlagTextBox(JAPAN.getName());
        assert_().withFailureMessage("The type ahead dropdown list should NOT be visible once a flag is selected")
               .that(filterByFlagPage.getSearchFlagTextBox().isDropdownVisible())
               .isFalse();

        filterByFlagPage.getSearchFlagTextBox().sendKeys("a");
        assert_().withFailureMessage("The type ahead dropdown should be visible with the matched options")
                .that(filterByFlagPage.getSearchFlagTextBox().isDropdownVisible())
                .isTrue();

        filterByFlagPage.getSearchFlagTextBox().clear();
        assert_().withFailureMessage("The type ahead dropdown should NOT be visible if the search textbox is empty")
                .that(filterByFlagPage.getSearchFlagTextBox().isDropdownVisible())
                .isFalse();
    }


}
