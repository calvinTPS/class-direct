package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import helper.TestDateHelper;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.FLEET_DASHBOARD;

public class NoResultsReturnedForDateOfBuildFilterTest extends BaseTest
{
    @Test(description = "No result is returned for Date of build filter")
    @Issue("LRCDT-500")
    @Features(FLEET_DASHBOARD)
    public void noResultsReturnedForDateOfBuildFilterTest()
    {
        LocalDate toDate = LocalDate.of(2016, 11, 21);
        LocalDate fromDate = toDate.minusDays(10);

        FilterAssetsPage filterAssetsPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class);

        VesselListPage vesselListPage = filterAssetsPage
                .selectAllVesselsRadioButton()
                .setFromDateOfBuild(fromDate.format(FRONTEND_TIME_FORMAT_DTS))
                .setToDateOfBuild(toDate.format(FRONTEND_TIME_FORMAT_DTS))
                .clickSubmitButton(VesselListPage.class);

        assert_().withFailureMessage("No asset cards returned for the search.")
                .that(vesselListPage.getAssetCards().size())
                .isGreaterThan(0);

        for (AssetCard assetCard : vesselListPage.getAssetCards())
        {
            assert_().withFailureMessage(String.format("Date of build for the asset '%s' is not inside the range.",
                    assetCard.getAssetName()))
                    .that(assetCard.getDateOfBuild())
                    .isIn(Range.closed(TestDateHelper.toDate(fromDate), TestDateHelper.toDate(toDate)));
        }
    }
}
