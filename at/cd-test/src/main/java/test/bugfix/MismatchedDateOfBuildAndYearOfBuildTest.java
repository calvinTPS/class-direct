package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDateHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.assetdetail.element.RegistryInformationCard;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ASSET_HUB;

public class MismatchedDateOfBuildAndYearOfBuildTest extends BaseTest
{
    @Test(description = "AI-02: AC2: View Registry Information: Mismatch between Date of build and Year of build")
    @Issue("LRCD-1730")
    @Features(ASSET_HUB)
    public void mismatchedDateOfBuildAndYearOfBuildTest()
    {
        final String searchTerm = "test";

        RegistryInformationCard registryInformationCard = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(searchTerm)
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No asset card available."))
                .clickViewAssetButton()
                .getAssetHeaderPage()
                .clickAssetDetailsButton()
                .getRegistryInformationCard();

        assert_().withFailureMessage("Year of build should match the year in 'Date of build'")
                .that(TestDateHelper.toLocalDate(registryInformationCard.getDateOfBuild()).getYear())
                .isEqualTo(Integer.parseInt(registryInformationCard.getYearOfBuild()));
    }
}
