package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.serviceschedule.ServiceSchedulePage;
import main.serviceschedule.tabular.base.AssetTable;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.DueStatus;
import static constant.ClassDirect.DueStatus.*;
import static constant.Epic.DUE_STATUS;

public class IncorrectAssetOverallDueStatusTest extends BaseTest
{
    private ServiceSchedulePage serviceSchedulePage;

    @Test(description = "DS-05 : System displays Incorrect Asset Overall due Status")
    @Issues({
            @Issue("LRCD-1912"),
            @Issue("LRCD-2404")
    })

    @Features(DUE_STATUS)
    public void incorrectAssetOverallDueStatusTest()
    {
        final List<String> assets = Arrays.asList(LAURA.getName(), "Vltava", "Carnival Glory", "Costa Classica");

        serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(ServiceSchedulePage.class);

        assets.forEach(this::assertOverallStatus);
    }

    private void assertOverallStatus(String assetName)
    {
        AssetTable assetTable = serviceSchedulePage.getFilterBarPage()
                .setSearchTextBox(assetName)
                .getPageReference(ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .filter(e -> e.getAssetCard().getAssetName().equals(assetName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s is not found.", assetName)))
                .expand();

        // get overall status
        DueStatus overallStatus = DueStatus.valueOf(assetTable.getAssetCard().getDueStatus().replace(" ", "_").toUpperCase());
        List<String> dueStatusList = new ArrayList<>();

        // get individual due status of services
        assetTable.getServiceTables()
                .forEach(e -> e.expand()
                        .getServices()
                        .forEach(s -> dueStatusList.add(s.getDueStatus())));

        // get individual due status of codicils
        if (assetTable.getCodicilTable().exists())
        {
            assetTable.getCodicilTable()
                    .expand()
                    .getCodicils()
                    .forEach(codicilElement -> dueStatusList.add(codicilElement.getDueStatus()));
        }

        switch (overallStatus)
        {
            case NOT_DUE:
                assert_().withFailureMessage("All services and codicils should have status 'Not due'.")
                        .that(dueStatusList)
                        .containsNoneOf(OVERDUE.getDueStatus(), IMMINENT.getDueStatus(), DUE_SOON.getDueStatus());
                break;

            case OVERDUE:
                assert_().withFailureMessage("There should be at least one service or codicil with status OVERDUE.")
                        .that(dueStatusList)
                        .contains(OVERDUE.getDueStatus());
                break;

            case IMMINENT:
                assert_().withFailureMessage("There should be at least one service or codicil with status IMMINENT.")
                        .that(dueStatusList)
                        .contains(IMMINENT.getDueStatus());
                assert_().withFailureMessage("There should be no service or codicil with status OVERDUE.")
                        .that(dueStatusList)
                        .doesNotContain(OVERDUE.getDueStatus());
                break;

            case DUE_SOON:
                assert_().withFailureMessage("There should be at least one service or codicil with status 'Due soon'.")
                        .that(dueStatusList)
                        .contains(DUE_SOON.getDueStatus());
                assert_().withFailureMessage("Services and codicils should only have 'Due soon' or 'Not due' status.")
                        .that(dueStatusList)
                        .containsNoneOf(OVERDUE.getDueStatus(), IMMINENT.getDueStatus());
                break;
        }
    }
}
