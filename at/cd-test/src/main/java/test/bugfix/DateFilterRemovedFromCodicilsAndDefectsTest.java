package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterCodicilsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.defects.filter.FilterDefectsPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS;

public class DateFilterRemovedFromCodicilsAndDefectsTest extends BaseTest
{
    @Test(description = "CO - Date filter has been removed from Codicils and defects")
    @Issue("LRCD-1334")
    @Features(SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void dateFilterRemovedFromCodicilsAndDefectsTest()
    {
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class);
        vesselListPage.getFilterBarPage()
                .setSearchTextBox(Asset.LAURA.getName());

        CodicilsAndDefectsPage codicilsAndDefectsPage = vesselListPage.getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException(Asset.LAURA.getName() + " is not present"))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton();

        FilterCodicilsPage filterCodicilsPage = codicilsAndDefectsPage.clickConditionsOfClassTab()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class);
        dateFiledValidation(filterCodicilsPage);
        filterCodicilsPage.clickCancelButton(CodicilsAndDefectsPage.class);

        filterCodicilsPage = codicilsAndDefectsPage.clickActionableItemsTab()
                .getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class);
        dateFiledValidation(filterCodicilsPage);
        filterCodicilsPage.clickCancelButton(CodicilsAndDefectsPage.class);

        FilterDefectsPage filterDefectsPage = codicilsAndDefectsPage.clickDefectsTab()
                .getFilterBarPage()
                .clickFilterButton(FilterDefectsPage.class);

        assert_().withFailureMessage("From date occurred date picker is expected to be displayed")
                .that(filterDefectsPage.isDateOccuredFromDatePickerDisplayed())
                .isTrue();
        assert_().withFailureMessage("To date occurred date picker is expected to be displayed")
                .that(filterDefectsPage.isDateOccuredToDatePickerDisplayed())
                .isTrue();
        filterCodicilsPage.clickCancelButton(CodicilsAndDefectsPage.class);

    }

    private void dateFiledValidation(FilterCodicilsPage filterCodicilsPage)
    {
        assert_().withFailureMessage("From impose date picker is expected to be displayed")
                .that(filterCodicilsPage.isImposeDateFromDatepickerDisplayed())
                .isTrue();
        assert_().withFailureMessage("To impose date picker is expected to be displayed")
                .that(filterCodicilsPage.isImposeDateToDatepickerDisplayed())
                .isTrue();

        assert_().withFailureMessage("From Due date picker is expected to be displayed")
                .that(filterCodicilsPage.isDueDateFromDatepickerDisplayed())
                .isTrue();
        assert_().withFailureMessage("To Due date picker is expected to be displayed")
                .that(filterCodicilsPage.isDueDateToDatepickerDisplayed())
                .isTrue();
    }
}
