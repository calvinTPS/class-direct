package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.administration.detail.sub.modalwindow.AmendAccountExpiryDatePage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Credentials.SHIP_BUILDER_USER;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;

public class AllowsBackDateInAssetExpiryDateTest extends BaseTest
{
    @Test(description = "UM-51: EoR account type: System allows to select/enter backdated date in Asset Expiry Date")
    @Issue("LRCD-1570")
    @Features(Epic.USER_MANAGEMENT)
    public void allowsBackDateInAssetExpiryDateTest()
    {
        final String EXPECTED_ERROR_MESSAGE = "You did not enter a valid date";
        final String PAST_DATE = FRONTEND_TIME_FORMAT_DTS.format(now.minusDays(7));

        AmendAccountExpiryDatePage amendAccountExpiryDatePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getRoleName().equals(SHIP_BUILDER_USER.getRole()))
                .findFirst()
                .orElseThrow(()-> new NoSuchElementException("Expected role for shipbuilder not found"))
                .clickFurtherMoreDetailsButton()
                .getAccountSettingsCard()
                .clickAmendAccountExpiryButton();

        String actualErrorMessage = amendAccountExpiryDatePage.clickCancelButton()
                .getExhibitionOfRecordsCard()
                .clickAddEorAccessButton()
                .setAssetExpiryDate(PAST_DATE)
                .getAssetExpiryDateErrorMessage();

        assert_().withFailureMessage("Expected error message did not match the actual error message.")
                .that(actualErrorMessage)
                .isEqualTo(EXPECTED_ERROR_MESSAGE);
    }
}
