package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.Credentials.LR_ADMIN;

public class CertificateCardsDueStatusIsNotDisplayedTest extends BaseTest
{
    String asset = Asset.WITH_CERTIFICATES.getName();

    @Test(description = "Due status and other details on certificate card are not displayed")
    @Issue("LRCD-1871")
    @Stories("DO-23")
    @Features(Epic.SERVICE_HISTORY)
    public void certificateCardsDueStatusIsNotDisplayedTest()
    {
        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .setSearchTextBox(asset)
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset named %s was not found", asset)))
                .clickViewAssetButton()
                .getCertificatesAndRecordsCard()
                .clickViewAllButton()
                .getCertificateElements()
                .forEach(certificateElement ->
                {
                    assert_().withFailureMessage("Certificate name is expected to be displayed")
                            .that(certificateElement.getCertificateName())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate number is expected to be displayed")
                            .that(certificateElement.getCertificateNumber())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate version is expected to be displayed")
                            .that(certificateElement.getCertificateVersion())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate issuing surveyor is expected to be displayed")
                            .that(certificateElement.getIssuingSurveyor())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate form number is expected to be displayed")
                            .that(certificateElement.getFormNumber())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate expiry date is expected to be displayed")
                            .that(certificateElement.getExpiryDate())
                            .isNotNull();
                    assert_().withFailureMessage("Certificate endorsed date is expected to be displayed")
                            .that(certificateElement.getEndorsedDate())
                            .isNotNull();
                    assert_().withFailureMessage("Certificate type is expected to be displayed")
                            .that(certificateElement.getCertificateType())
                            .isNotEmpty();
                    assert_().withFailureMessage("Certificate due status is expected to be displayed")
                            .that(certificateElement.getDueStatus())
                            .isNotEmpty();
                    //no assertion needed for dueStatusIcon - set to @Visible
                });
    }
}
