package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.references.ScheduledServiceHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.schedule.tabular.base.ServiceTable;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Assets;

import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_DATE_FORMAT;
import static constant.Epic.SERVICE_SCHEDULE;

public class PostponementDatesAreNotDisplayedForServicesTest extends BaseTest
{
    @Test(description = "Postponement dates are not displayed for services.")
    @Issue("LRCD-1498")
    @Features(SERVICE_SCHEDULE)
    public void postponementDatesAreNotDisplayedForServicesTest()
    {
        final String approvedPostponementType = "Approved";

        List<ServiceTable> serviceTables = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .getTabularView()
                .getServiceTables();

        List<ScheduledServiceHDto> servicesFromBackend = new Assets().getServicesByAssetId(LAURA.getId());

        servicesFromBackend.stream()
                .filter(e -> e.getPostponementTypeH() != null && e.getPostponementTypeH().getName().equals(approvedPostponementType))
                .forEach(e ->
                {
                    boolean isPostponementDateDisplayed = serviceTables.stream()
                            .filter(serviceTable -> serviceTable.getProductName().equals(e.getServiceCatalogueH().getProductCatalogue().getName()))
                            .findFirst()
                            .orElseThrow(() -> new NoSuchElementException("Expected product catalogue not found."))
                            .getServices()
                            .stream()
                            .anyMatch(serviceElement -> FRONTEND_DATE_FORMAT.format(e.getPostponementDate()).equals(serviceElement.getPostponementDate()));

                    assert_().withFailureMessage("Postponement date is expected to be displayed.")
                            .that(isPostponementDateDisplayed)
                            .isTrue();
                });
    }
}
