package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Credentials.SHIP_BUILDER_USER;
import static constant.Epic.USER_MANAGEMENT;

public class UnableToNavigateBackToAdminPageTest extends BaseTest
{
    @Test(description = "Select the icon or the User's Name in user details page has no responding")
    @Issue("LRCD-1065")
    @Features(USER_MANAGEMENT)
    public void unableToNavigateBackToAdminPageTest()
    {
        //No need to assert: This will throw an error if AdministrationPage is not displayed when back button is clicked from user details page.
        AdministrationPage administrationPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getRoleName().equals(LR_ADMIN.getRole()))
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Card with role of " + LR_ADMIN.getRole() + " not found."))
                .clickFurtherMoreDetailsButton()
                .clickBackButton();

        administrationPage.getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getRoleName().equals(SHIP_BUILDER_USER.getRole()))
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Card with role of " + SHIP_BUILDER_USER.getRole() + " not found."))
                .clickFurtherMoreDetailsButton()
                .clickBackButton();
    }
}
