package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class UnableToViewAnyAssetInMyFavouriteFilterTest extends BaseTest
{
    @Test(description = "Unable to view any assets in the Favourite Filter when the user assign the assets as Favourite in the fleet dashboard")
    @Issue("LRCD-1556")
    @Features(FLEET_DASHBOARD)
    public void unableToViewAnyAssetInMyFavouriteFilterTest()
    {
        final String searchTerm = "test";

        FilterBarPage filterBarPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage();

        String assetName = filterBarPage
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(searchTerm)
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(e -> !e.isFavourite())
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Expected asset card not found."))
                .selectFavourite()
                .getAssetName();

        assert_().withFailureMessage("Favourite assets are expected to be displayed.")
                .that(filterBarPage
                        .clickFilterButton(FilterAssetsPage.class)
                        .selectMyFavouritesRadioButton()
                        .clickSubmitButton(VesselListPage.class)
                        .getAssetCards().size())
                .isGreaterThan(0);

        assert_().withFailureMessage("The asset is expected to be marked as favourite.")
                .that(filterBarPage.clickFilterButton(FilterAssetsPage.class)
                        .selectMyFavouritesRadioButton()
                        .clickSubmitButton(FilterBarPage.class)
                        .setSearchTextBox(assetName)
                        .getPageReference(VesselListPage.class)
                        .getAssetCards()
                        .stream()
                        .findFirst()
                        .orElseThrow(() -> new java.util.NoSuchElementException("Expected asset card not found."))
                        .isFavourite())
                .isTrue();
    }
}
