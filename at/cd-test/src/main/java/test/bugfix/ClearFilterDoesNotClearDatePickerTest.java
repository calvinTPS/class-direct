package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.common.filter.FilterCodicilsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.ConditionsOfClassPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;

public class ClearFilterDoesNotClearDatePickerTest extends BaseTest
{
    @Test(description = "Date pickers are not cleared when Clear Filters button is clicked in filter panel of Codicils and Defects page.")
    @Issue("LRCD-2309")
    @Features(Epic.SINGLE_ASSET_CODICILS_AND_DEFECTS)
    public void clearFilterDoesNotClearDatePickerTest()
    {
        String dateToday = ClassDirect.now.format(FRONTEND_TIME_FORMAT_DTS);

        ConditionsOfClassPage conditionsOfClassPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s was not found.", LAURA.getName())))
                .clickViewAssetButton()
                .getCodicilCard()
                .clickViewAllButton()
                .clickConditionsOfClassTab();

        conditionsOfClassPage.getFilterBarPage()
                .clickFilterButton(FilterCodicilsPage.class)
                .setDueDateToDatepicker(dateToday)
                .setDueDateFromDatepicker(dateToday)
                .setImposeDateToDatepicker(dateToday)
                .setImposeDateFromDatepicker(dateToday)
                .clickSubmitButton(FilterBarPage.class)
                .clickFilterButton(FilterCodicilsPage.class)
                .clickClearFiltersButton();

        FilterCodicilsPage filterCodicilsPage = conditionsOfClassPage.getFilterBarPage().clickFilterButton(FilterCodicilsPage.class);

        assert_().withFailureMessage("'Due date From' datepicker should be cleared.")
                .that(filterCodicilsPage.getDueDateFromDatepicker())
                .isEmpty();
        assert_().withFailureMessage("'Due date To' datepicker should be cleared.")
                .that(filterCodicilsPage.getDueDateToDatepicker())
                .isEmpty();
        assert_().withFailureMessage("'Imposed date From' datepicker should be cleared.")
                .that(filterCodicilsPage.getImposedDateFromDatepicker())
                .isEmpty();
        assert_().withFailureMessage("'Imposed date To' datepicker should be cleared.")
                .that(filterCodicilsPage.getImposedDateToDatepicker())
                .isEmpty();
    }
}
