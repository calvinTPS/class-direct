package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.adduser.steps.element.UserCard;
import main.administration.detail.AdministrationDetailPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;

public class SystemAllowsCreationOfExistingUserAccountTest extends BaseTest
{
    @Test(description = "System allow creating an existing user, and this happens only when the user try to search with the email they logged in to CD application")
    @Issue("LRCD-2110")
    @Features(USER_MANAGEMENT)
    public void systemAllowsCreationOfExistingUserAccountTest()
    {
        UserCard userCard = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword()).clickAdministrationTab()
                .clickAddUserButton()
                .setEmailAddress(LR_ADMIN.getEmail())
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected user card for %s was not found.", LR_ADMIN.getEmail())));

        assert_().withFailureMessage("For existing user, button text should be 'Edit User'")
                .that(userCard.getCreateNewOrEditAccountButtonText())
                .contains("Edit");

        // Should redirect to Administration detail page
        userCard.clickCreateNewOrEditAccountButton(AdministrationDetailPage.class);
    }
}
