package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.surveys.element.SurveysElements;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.format.DateTimeParseException;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.ASSET_HUB;

public class JobAndIssueDateNotDisplayedInSurveySectionTest extends BaseTest
{
    @Test(description = "1. The Job number is not displayed for a report in survey section of asset hub page."
            + "2. The issue date format is not matching with the expected format (dd Mmm yyyy)")
    @Issue("LRCD-559")
    @Features(ASSET_HUB)
    public void jobAndIssueDateNotDisplayedInSurveySectionTest()
    {
        VesselListPage vesselListPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class);

        List<SurveysElements> surveysElementsList =
                vesselListPage.getAssetCards()
                        .stream()
                        .filter(e -> e.getAssetName().equals(LAURA.getName()))
                        .findFirst()
                        .orElseThrow(()-> new RuntimeException(String.format("Asset with name %s not found", LAURA.getName())))
                        .clickViewAssetButton()
                        .getSurveysCard()
                        .getSurveysElements();

        for (SurveysElements surveyElement : surveysElementsList)
        {
            assert_().withFailureMessage("Job number should be displayed")
                    .that(surveyElement.getSurveyCount())
                    .isNotEmpty();

            if(!surveyElement.getSurveyDate().isEmpty()) // survey date is not mandatory - LRCD-620
            assert_().withFailureMessage("Issue date should be displayed with (dd Mmm yyyy) format")
                    .that(isSurveyDateValid(surveyElement.getSurveyDate()))
                    .isTrue();
        }
    }

    private boolean isSurveyDateValid(String dateToValidate)
    {
        if (dateToValidate == null)
        {
            return false;
        }
        try
        {
            FRONTEND_TIME_FORMAT_DTS.parse(dateToValidate);
            return true;
        }
        catch (DateTimeParseException e)
        {
            return false;
        }
    }
}
