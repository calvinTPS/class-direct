package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.requestsurvey.sub.SelectLocationAndDatesSubPage;
import main.requestsurvey.sub.SelectServicesSubPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.RequestSurveyData.*;
import static constant.Epic.REQUEST_SURVEY;

public class TimePickerDropdownNotVisibleInRequestSurveyTest extends BaseTest
{
    @Test(description = "There is no drop down display for Estimated time of arrival and Estimated time of departure.")
    @Issues({
            @Issue("LRCD-2280"),
            @Issue("LRCD-535")
    })

    @Features(REQUEST_SURVEY)
    public void timePickerDropdownNotVisibleInRequestSurveyTest()
    {
        SelectServicesSubPage selectServicesSubPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getFilterBarPage()
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Asset card list is empty."))
                .clickRequestSurveyButton()
                .getSelectServicesSubPage();

        selectServicesSubPage.getServiceTable()
                .getRows()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Product list is empty."))
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Service list is empty for the selected product."))
                .selectCheckBox();

        SelectLocationAndDatesSubPage selectLocationAndDatesSubPage = selectServicesSubPage
                .clickContinueButton()
                .selectPortName(portName);

        assert_().withFailureMessage("Dropdown list for estimated time of arrival should be displayed.")
                .that(selectLocationAndDatesSubPage.isEstimatedTimeOfArrivalDropdownVisible())
                .isTrue();

        selectLocationAndDatesSubPage.setArrivalTime(arrivalTime)
                .setSurveyStartDate(surveyStartDate.format(FRONTEND_TIME_FORMAT_DTS))
                .setEstimatedDateOfDeparture(departureDate.format(FRONTEND_TIME_FORMAT_DTS));

        assert_().withFailureMessage("Dropdown list for estimated time of departure should be displayed.")
                .that(selectLocationAndDatesSubPage.isEstimatedTimeOfDepartureDropdownVisible())
                .isTrue();
    }
}
