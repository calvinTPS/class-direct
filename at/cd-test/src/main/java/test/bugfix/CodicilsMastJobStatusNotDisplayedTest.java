package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.*;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.apache.commons.collections.CollectionUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Assets;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset;
import static constant.ClassDirect.BACKEND_TIME_FORMAT;
import static constant.ClassDirect.Credentials.LR_ADMIN;

public class CodicilsMastJobStatusNotDisplayedTest extends BaseTest
{
    private String serviceStatus;
    private String serviceName;

    @Test(description = "Codicils Mast Job status is not displayed correctly in Asset hub page")
    @Issue("LRCD-1282")
    @Features(Epic.SERVICE_SCHEDULE)
    public void codicilsMastJobStatusNotDisplayedTest() throws ParseException
    {
        List<String> jobStatusFromFe = new ArrayList<>();
        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .getTabularView()
                .getCodicilTables()
                .getCodicils()
                .forEach(e ->
                {
                    serviceName = e.getCodicilName().trim().toLowerCase();
                    serviceStatus = e.getJobStatus().trim().toLowerCase();
                    jobStatusFromFe.add(String.format("%s || %s", serviceName, serviceStatus));

                });

        List<String> jobStatusFromBe = getCodicilsJobStatus();
        assert_().withFailureMessage("Service Status and Name in FE is expected to be match with BE ")
                .that(CollectionUtils.isEqualCollection(jobStatusFromFe, jobStatusFromBe))
                .isTrue();
    }

    private List<String> getCodicilsJobStatus() throws ParseException
    {
        Assets assets = new Assets();
        CodicilDefectQueryHDto codicilDefectQueryHDto = new CodicilDefectQueryHDto();
        codicilDefectQueryHDto.setDueDateMax(BACKEND_TIME_FORMAT.parse("2022-08-01"));
        codicilDefectQueryHDto.setDueDateMin(BACKEND_TIME_FORMAT.parse("2015-08-01"));
        CodicilActionableHDto codicilActionableHDto = assets.postCodicilsQuery(1, codicilDefectQueryHDto);

        List<ActionableItemHDto> actionableItemHDtos = codicilActionableHDto.getActionableItemHDtos().stream()
                .filter(e ->
                {
                    Long id = e.getStatus().getId();
                    return id.equals(3L) ||
                            id.equals(4L) ||
                            id.equals(6L);
                })
                .collect(Collectors.toList());

        List<CoCHDto> coCHDtos = codicilActionableHDto.getCocHDtos()
                .stream().filter(e -> e.getStatus().getId().equals(14L))
                .collect(Collectors.toList());

        List<StatutoryFindingHDto> statutoryFindingHDtos = codicilActionableHDto.getStatutoryFindingHDtos().stream()
                .filter(e -> e.getStatus().getId().equals(23L))
                .collect(Collectors.toList());

        List<String> expectedJobStatus = new ArrayList<>();

        for (CoCHDto coCHDto : coCHDtos)
        {
            serviceStatus = (coCHDto.getJobH() != null) ?
                    (coCHDto.getJobH().getJobStatusDto() != null) ?
                            coCHDto.getJobH().getJobStatusDto().getName().toLowerCase() : "" : "";
            serviceName = coCHDto.getTitle() != null ? coCHDto.getTitle().toLowerCase() : "";
            expectedJobStatus.add(String.format("%s || %s", serviceName, serviceStatus));
        }

        for (ActionableItemHDto actionableItemHDto : actionableItemHDtos)
        {
            serviceStatus = (actionableItemHDto.getJobH() != null) ?
                    (actionableItemHDto.getJobH().getJobStatusDto() != null) ?
                            actionableItemHDto.getJobH()
                                    .getJobStatusDto().getName().toLowerCase() : "" : "";
            serviceName = actionableItemHDto.getTitle() != null
                    ? actionableItemHDto.getTitle().toLowerCase() : "";
            expectedJobStatus.add(String.format("%s || %s", serviceName, serviceStatus));
        }

        for (StatutoryFindingHDto statutoryFindingHDto : statutoryFindingHDtos)
        {
            serviceName = statutoryFindingHDto.getTitle() != null ? statutoryFindingHDto.getTitle().toLowerCase() : "";
            expectedJobStatus.add(String.format("%s || %s", serviceName, ""));
        }

        return expectedJobStatus;
    }
}
