package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class UnFavouriteAssetTest extends BaseTest
{
    @Test(description = "FI-001 AC1 - Unable to unfavourite asset")
    @Issue("LRCD-756")
    @Features(FLEET_DASHBOARD)
    public void unFavouriteAssetTest()
    {

        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class);

        AssetCard actualAssetCard = vesselListPage.getAssetCards().get(0);

        String expectedAssetCard = actualAssetCard.getAssetName();

        if (!actualAssetCard.isFavourite())
        {
            actualAssetCard.selectFavourite();
        }

        List<AssetCard> assetCards = vesselListPage
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectMyFavouritesRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards();

        assert_().withFailureMessage(expectedAssetCard + " is expected to be favourite")
                .that(assetCards
                        .stream()
                        .anyMatch(e -> e.getAssetName().equals(expectedAssetCard)))
                .isTrue();

        AssetCard assetCard = assetCards.stream()
                .filter(e -> e.getAssetName().equals(expectedAssetCard))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        expectedAssetCard + "' not present"));

        assetCard.deselectFavourite();
        vesselListPage
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class);

        assetCard = vesselListPage
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(expectedAssetCard))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        expectedAssetCard + "' not present"));

        assert_().withFailureMessage(expectedAssetCard + " favourite icon is expect to be unchecked")
                .that(assetCard.isFavourite())
                .isFalse();

        assetCards = vesselListPage
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectMyFavouritesRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards();

        assert_().withFailureMessage(expectedAssetCard + " is not expected to be favourite")
                .that(assetCards
                        .stream()
                        .anyMatch(e -> e.getAssetName().equals(expectedAssetCard)))
                .isFalse();
    }
}
