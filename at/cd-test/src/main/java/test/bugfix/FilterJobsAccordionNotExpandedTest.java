package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SERVICE_HISTORY;

public class FilterJobsAccordionNotExpandedTest extends BaseTest
{
    @Test(description = "When Filter jobs accordion is clicked, it is not expanded.", enabled = false)
    @Issue("LRCD-1113")
    @Features(SERVICE_HISTORY)
    public void filterJobsAccordionNotExpandedTest()
    {
        assert_().withFailureMessage("Filter jobs accordion is expected to be expanded when clicked.")
                .that(LoginPage
                        .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                        .getFilterBarPage()
                        .clickFilterButton(FilterAssetsPage.class)
                        .selectAllVesselsRadioButton()
                        .clickSubmitButton(FilterBarPage.class)
                        .setSearchTextBox(LAURA.getName())
                        .getPageReference(VesselListPage.class)
                        .getAssetCards()
                        .stream()
                        .filter(e -> e.getAssetName().equals(LAURA.getName()))
                        .findFirst()
                        .orElseThrow(() -> new NoSuchElementException("Expected asset not found."))
                        .clickViewAssetButton()
                        .getSurveysCard()
                        .clickViewAllButton()
                        .getFilterBarPage()
                        .clickFilterButton(FilterBarPage.class)
                        .isFilterIconDisplayed()) // This test is not relevant anymore since there's no more accordion - LRCD-2530
                .isTrue();
    }
}
