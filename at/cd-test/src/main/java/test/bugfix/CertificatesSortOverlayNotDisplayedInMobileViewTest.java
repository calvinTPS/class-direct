package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import helper.AppHelper;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.mobile.MobileFilterBarPage;
import main.mobile.filter.MobileFilterAssetsPage;
import main.mobile.sort.MobileSortPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset.WITH_CERTIFICATES;

public class CertificatesSortOverlayNotDisplayedInMobileViewTest extends BaseTest
{
    @Test(description = "Filter overlay is displayed instead of sort overlay.")
    @Issue("LRCD-1921")
    @Features(Epic.CERTIFICATES)
    public void certificatesSortOverlayNotDisplayedInMobileViewTest()
    {
        LandingPage landingPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword());

        AppHelper.shrinkIntoMobileView();

        MobileSortPage mobileSortPage = landingPage.getVesselListPage().getMobileFilterBarPage()
                .clickFilterButton(MobileFilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .getMobileDialogFooter()
                .clickSubmitButton(MobileFilterBarPage.class)
                .clickSearchButton()
                .setSearchTextBox(WITH_CERTIFICATES.getName())
                .getMobileDialogFooter()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s was not found.", WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getCertificatesAndRecordsCard()
                .clickViewAllButton()
                .getMobileFilterBarPage()
                .clickSortButton();

        assert_().withFailureMessage("Sort category radio group should be displayed if Sort overlay appeared.")
                .that(mobileSortPage.isSortCategoryRadioGroupDisplayed())
                .isTrue();
    }
}
