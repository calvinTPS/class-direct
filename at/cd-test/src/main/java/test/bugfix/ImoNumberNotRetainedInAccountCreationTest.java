package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.User;
import main.administration.adduser.eor.AddEorPermissionPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;
import static constant.ClassDirect.Asset.LAURA;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.UserAccountTypes;

public class ImoNumberNotRetainedInAccountCreationTest extends BaseTest {
    @Test(description = "During account creation, IMO Number is not retained when returning to account type selection")
    @Issue("LRCD-2123")
    @Features(USER_MANAGEMENT)
    public void imoNumberNotRetainedInAccountCreationTest() {
        final String assetExpiryDate = FRONTEND_TIME_FORMAT_DTS.format(now.plusDays(7));

        AddEorPermissionPage addEorPermissionPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickAddUserButton()
                .setEmailAddress(User.NON_LR_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new org.openqa.selenium.NoSuchElementException("User card not found"))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.EOR.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected account type not found."))
                .clickSelectButton(AddEorPermissionPage.class)
                .setAssetExpiryDate(assetExpiryDate)
                .setImoNumber(LAURA.getImoNumber())
                .clickAddExhibitionRecordsAssetButton()
                .getProgressSubPage()
                .clickProgressCardByText(ChooseAccountTypePage.class, UserAccountTypes.EOR.toString())
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.EOR.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected account type not found."))
                .clickSelectButton(AddEorPermissionPage.class);

        assert_().withFailureMessage(String.format("Asset expiry should retain its value as %s", assetExpiryDate))
                .that(addEorPermissionPage.getAssetExpiryDate())
                .isEqualTo(assetExpiryDate);

        assert_().withFailureMessage(String.format("IMO Number should retain its value as %s", LAURA.getImoNumber()))
                .that(addEorPermissionPage.getImoNumber())
                .isEqualTo(LAURA.getImoNumber());

        assert_().withFailureMessage(String.format("Asset card with %s should be displayed.", LAURA.getImoNumber()))
                .that(addEorPermissionPage.isAssetCardDisplayed())
                .isTrue();
    }
}
