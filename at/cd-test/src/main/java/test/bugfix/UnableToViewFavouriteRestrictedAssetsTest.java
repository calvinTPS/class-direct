package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.sub.accessibleassets.AccessibleAssetsPage;
import main.administration.detail.sub.accessibleassets.EditAccessibleAssetsPage;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset;
import static constant.Epic.FLEET_DASHBOARD;

public class UnableToViewFavouriteRestrictedAssetsTest extends BaseTest {
    @Test(description = "FI-01:AC - Unable to filter Favourite with restricted/IHS asset")
    @Issue("LRCD-1653")
    @Features(FLEET_DASHBOARD)
    public void unableToViewFavouriteRestrictedAssetsTest() {
        LandingPage landingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName());
        AdministrationDetailPage administrationDetailPage = landingPage.clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(LR_ADMIN.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected user with email %s is not found.", LR_ADMIN.getEmail())))
                .clickFurtherMoreDetailsButton();

        boolean isRestrictedAsset = administrationDetailPage.clickRestrictedAssetsTab()
                .getRestrictedAssetCards()
                .stream()
                .anyMatch(e -> e.getAssetName().equals(Asset.FOR_RESTRICTED_TEST.getName()));

        if (!isRestrictedAsset) {
            //set as restricted asset
            EditAccessibleAssetsPage editAccessibleAssetsPage = administrationDetailPage.clickAccessibleAssetsTab()
                    .clickEditButton();

            editAccessibleAssetsPage.getAccessibleAssetCards()
                    .stream()
                    .filter(e -> e.getAssetName().equals(Asset.FOR_RESTRICTED_TEST.getName()))
                    .findFirst()
                    .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset: %s not found.", Asset.FOR_RESTRICTED_TEST.getName())))
                    .selectAssetCard();

            editAccessibleAssetsPage.clickMoveToRestrictedAssetButton()
                    .clickConfirmButton(AccessibleAssetsPage.class);
        }

        //set the restricted asset as favourite
        landingPage.clickVesselListTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.FOR_RESTRICTED_TEST.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(Asset.FOR_RESTRICTED_TEST.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset not found."))
                .clickViewAssetButton()
                .getAssetHeaderPage()
                .selectFavourite();

        AssetCard assetCard = LandingPage.getTopBar()
                .clickClientTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(Asset.FOR_RESTRICTED_TEST.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(Asset.FOR_RESTRICTED_TEST.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset not found."));

        assert_().withFailureMessage("The restricted asset is expected to be favourite.")
                .that(assetCard.isFavourite())
                .isTrue();

        assert_().withFailureMessage("The restricted asset is expected to be favourite.")
                .that(assetCard.clickViewAssetButton()
                        .getAssetHeaderPage()
                        .isFavouriteIconSelected())
                .isTrue();
    }
}
