package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.User.UserAccountTypes;
import main.administration.AdministrationPage;
import main.administration.filter.FilterUserPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ACCOUNT_REPORTING;

public class InconsistentPaginationCountInUsersListPageTest extends BaseTest
{
    @Test(description = "AR:03: Inconsistency in the 'Showing X of Y users' values in the User list page")
    @Issue("LRCD-1870")
    @Features(ACCOUNT_REPORTING)
    public void inconsistentPaginationCountInUsersListPageTest()
    {
        AdministrationPage administrationPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();

        int userCountWhenAllAccountTypeSelected = administrationPage.getShowingXofYPage().getShown();

        assert_().withFailureMessage("Total number of users shown should match the count in pagination.")
                .that(userCountWhenAllAccountTypeSelected)
                .isEqualTo(administrationPage.getAdministrationTable().getDataElements().size());

        administrationPage = administrationPage.getFilterBarPage()
                .clickFilterButton(FilterUserPage.class)
                .deselectAccountTypeCheckbox(UserAccountTypes.ALL)
                .clickSubmitButton(AdministrationPage.class);

        assert_().withFailureMessage("Total number of users shown should still be the same when all account types are deselected.")
                .that(administrationPage.getShowingXofYPage().getShown())
                .isEqualTo(userCountWhenAllAccountTypeSelected);

        administrationPage = administrationPage.getFilterBarPage()
                .clickFilterButton(FilterUserPage.class)
                .selectAccountTypeCheckbox(UserAccountTypes.LR_ADMIN)
                .clickSubmitButton(AdministrationPage.class);

        assert_().withFailureMessage("Total number of users shown should match the count in pagination.")
                .that(administrationPage.getShowingXofYPage().getShown())
                .isEqualTo(administrationPage.getAdministrationTable().getDataElements().size());
    }
}
