package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.AssetHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.administration.AdministrationPage;
import main.administration.detail.AdministrationDetailPage;
import main.common.element.MiniAssetCard;
import main.login.LoginPage;
import org.apache.commons.collections.CollectionUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.Users;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.USER_MANAGEMENT;

public class IncorrectDisplayedAssetsForFlagShipBuilderClientTest extends BaseTest
{
    private final String clientUser = ClassDirect.Credentials.CLIENT_USER.getId();
    private final String flagUser = ClassDirect.Credentials.FLAG_USER.getId();
    private final String shipBuilderUser = ClassDirect.Credentials.SHIP_BUILDER_USER.getId();

    @Test(description = "Incorrect assets view for Flag, Shipbuilder & Client users with LR Admin role login")
    @Issue("LRCD-929")
    @Features(USER_MANAGEMENT)
    public void incorrectDisplayedAssetsForFlagShipBuilderClientTest()
    {
        AdministrationPage administrationPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab();

        assertDisplayedAccessibleAssetsOfUser(flagUser, administrationPage)
                .clickBackButton();

        assertDisplayedAccessibleAssetsOfUser(shipBuilderUser, administrationPage)
                .clickBackButton();

        assertDisplayedAccessibleAssetsOfUser(clientUser, administrationPage);
    }

    private AdministrationDetailPage assertDisplayedAccessibleAssetsOfUser(String userId, AdministrationPage administrationPage)
    {
        Users userDTO = new Users();
        String userEmail = userDTO.getUser(userId).getEmail();

        AdministrationDetailPage administrationDetailPage = administrationPage
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(userEmail))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("User email: %s not found.", userEmail)))
                .clickFurtherMoreDetailsButton();

        //Get the accessible assets from back end
        List<String> accessibleAssetCardsBE = userDTO.getAccessibleAssets(userId)
                .stream()
                .map(AssetHDto::getName)
                .collect(Collectors.toList());

        //Get the restricted assets from back end
        List<String> restrictedAssetCardsBE = userDTO.getRestrictedAssets(userId)
                .stream()
                .map(AssetHDto::getName)
                .collect(Collectors.toList());

        //Get the accessible assets from front end
        List<String> accessibleAssetCardsFE = administrationDetailPage
                .getAccessibleAssetsPage()
                .loadMoreAssetCards()
                .getAccesibleAssetCards()
                .stream()
                .map(MiniAssetCard::getAssetName)
                .collect(Collectors.toList());

        //Get the restricted assets from front end
        List<String> restrictedAssetCardsFE = administrationDetailPage
                .clickRestrictedAssetsTab()
                .getRestrictedAssetCards()
                .stream()
                .map(MiniAssetCard::getAssetName)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Accessible assets in front end should match the back end.")
                .that(CollectionUtils.isEqualCollection(accessibleAssetCardsBE, accessibleAssetCardsFE))
                .isTrue();

        assert_().withFailureMessage("Restricted assets in front end should match the back end.")
                .that(CollectionUtils.isEqualCollection(restrictedAssetCardsBE, restrictedAssetCardsFE))
                .isTrue();

        return administrationDetailPage;
    }
}
