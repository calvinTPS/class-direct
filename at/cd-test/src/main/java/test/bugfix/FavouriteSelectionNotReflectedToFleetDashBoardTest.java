package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ASSET_HUB;

public class FavouriteSelectionNotReflectedToFleetDashBoardTest extends BaseTest
{
    @Test(description = "System is not reflecting the single asset favourite selection with user navigates back to fleet dashboard.")
    @Issue("LRCD-552")
    @Features(ASSET_HUB)
    public void favouriteSelectionNotReflectedToFleetDashBoardTest()
    {
        String searchTerm = "test";

        AssetCard assetCard  = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .setSearchTextBox(searchTerm)
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(e -> !e.isFavourite())
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No non-favourite asset can be found"));

        String assetName = assetCard.getAssetName();
        assetCard.clickViewAssetButton()
                .getAssetHeaderPage()
                .selectFavourite();

        assert_().withFailureMessage("The selected asset should still be marked as favourite when back to Vessel list page.")
                .that(LandingPage.getTopBar()
                        .clickClientTab()
                        .getFilterBarPage()
                        .clickFilterButton(FilterAssetsPage.class)
                        .selectMyFavouritesRadioButton()
                        .clickSubmitButton(VesselListPage.class)
                        .getAssetCards()
                        .stream()
                        .filter(e -> e.getAssetName().equals(assetName))
                        .findFirst()
                        .orElseThrow(() -> new RuntimeException(String.format("Asset %s is not displayed", assetName)))
                        .isFavourite())
                .isTrue();
    }
}
