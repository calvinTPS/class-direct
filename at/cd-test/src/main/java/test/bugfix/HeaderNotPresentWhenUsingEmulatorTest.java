package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.element.DataElement;
import main.common.TopBarPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Credentials.SHIP_BUILDER_USER;
import static constant.Epic.USER_MANAGEMENT;

public class HeaderNotPresentWhenUsingEmulatorTest extends BaseTest
{
    @Test(description = "Header is not visible when the user clicks on the Emulator CTA.")
    @Issues({
            @Issue("LRCD-2027"),
            @Issue("LRCD-2245"),
            @Issue("LRCD-2562")
    })
    @Features(USER_MANAGEMENT)

    public void headerNotPresentWhenUsingEmulatorTest()
    {
        DataElement dataElement = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getRoleName().equals(SHIP_BUILDER_USER.getRole()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected role for %s not found", SHIP_BUILDER_USER.getRole())));

        String userName = dataElement.getUserName();
        TopBarPage topBarPage = dataElement.clickFurtherMoreDetailsButton()
                .getEmulateUserCard()
                .clickEmulateUserButton(TopBarPage.class); //This test will get an error here if Topbar is not visible

        assert_().withFailureMessage(String.format("Expected user name is %s", userName))
                .that(topBarPage.getAccountName())
                .isEqualTo(userName);
    }
}
