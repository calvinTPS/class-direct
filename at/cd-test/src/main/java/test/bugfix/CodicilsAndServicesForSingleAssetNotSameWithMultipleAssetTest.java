package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.filter.FilterAssetsPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.serviceschedule.tabular.base.AssetTable;
import main.vessellist.VesselListPage;
import main.viewasset.schedule.tabular.TabularView;
import main.viewasset.schedule.tabular.base.ServiceTable;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset;

public class CodicilsAndServicesForSingleAssetNotSameWithMultipleAssetTest extends BaseTest
{
    @Test(description = "The number of codicils and services displayed for single asset is not the same for multiple assets in service schedule page.")
    @Issue("LRCD-2335")
    @Features(Epic.SERVICE_SCHEDULE)
    public void codicilsAndServicesForSingleAssetNotSameWithMultipleAssetTest()
    {
        TabularView tabularView = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .setSearchTextBox(Asset.LAURA.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s was not found.", Asset.LAURA.getName())))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .getTabularView();

        long codicilsCountInSingleAsset = tabularView.getCodicilTables().exists()
                ? tabularView.getCodicilTables().getCodicils().size() : 0;

        long servicesCountInSingleAsset = tabularView.getServiceTables().stream().map(ServiceTable::getServices).count();

        AssetTable assetTable = LandingPage.getTopBar().clickClientTab()
                .getLandingPage()
                .clickServiceScheduleTab()
                .getFilterBarPage()
                .setSearchTextBox(Asset.LAURA.getName())
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .selectCodicilTypeAll()
                .clickSubmitButton(main.serviceschedule.ServiceSchedulePage.class)
                .getTabularView()
                .getAssetTables()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s was not found.", Asset.LAURA.getName())))
                .expand();

        long codicilsCountInMultipleAsset = assetTable.getCodicilTable().exists()
                ? assetTable.getCodicilTable().expand().getCodicils().size() : 0;

        long servicesCountInMultipleAsset = assetTable.getServiceTables().stream().map(e-> e.expand().getServices()).count();

        assert_().withFailureMessage("Codicils count in Service Schedule for single asset should be the same from Service Schedule for multiple asset.")
                .that(codicilsCountInSingleAsset)
                .isEqualTo(codicilsCountInMultipleAsset);

        assert_().withFailureMessage("Services count in Service Schedule for single asset should be the same from Service Schedule for multiple asset.")
                .that(servicesCountInSingleAsset)
                .isEqualTo(servicesCountInMultipleAsset);
    }
}
