package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class NoResultsReturnedForIMONumberFilterTest extends BaseTest
{
    @Test(description = "No result is returned for IMO Number filter")
    @Issue("LRCD-598")
    @Features(FLEET_DASHBOARD)
    public void noResultsReturnedForIMONumberFilterTest()
    {
        final String imoNumberSearchString = "389";

        List<AssetCard> assetCardList = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(imoNumberSearchString)
                .getPageReference(VesselListPage.class)
                .getAssetCards();

        assert_().withFailureMessage("The filter by IMO number is expected to return results.")
                .that(assetCardList.size())
                .isGreaterThan(0);

        assetCardList.forEach(e->
                assert_().withFailureMessage(String.format("IMO number of displayed assets should contain '%s'.", imoNumberSearchString))
                        .that(e.getImoNumber())
                        .contains(imoNumberSearchString));
    }
}
