package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import main.landing.LandingPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.COUNTRY_FILES;

public class UnableToViewCountryFileIconInMobileViewTest extends BaseTest
{
    @Test(description = "Unable to view the County file icon in mobile device.")
    @Issue("LRCD-2082")
    @Features(COUNTRY_FILES)
    public void unableToViewCountryFileIconInMobileViewTest()
    {
        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName());

        AppHelper.shrinkIntoMobileView();

        assert_().withFailureMessage("National Administration button should be displayed in mobile view.")
                .that(LandingPage.getTopBar().isNationalAdministrationButtonDisplayed())
                .isTrue();
    }
}
