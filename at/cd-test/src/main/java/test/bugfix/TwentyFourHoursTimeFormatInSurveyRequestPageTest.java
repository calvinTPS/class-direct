package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.requestsurvey.sub.SelectLocationAndDatesSubPage;
import main.requestsurvey.sub.SelectServicesSubPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.REQUEST_SURVEY;

public class TwentyFourHoursTimeFormatInSurveyRequestPageTest extends BaseTest
{
    private final List<String> invalidTimeFormats = Arrays.asList("1:00", "3:00", "4.00");  //12 hour formats
    private final List<String> validTimeFormats = Arrays.asList("13:00", "01:00", "23:45"); //24 hour formats
    private final String errMessage = "Please select one of the options";

    @Test(description = "Time format in Survey Request Page is in 24 hours time format")
    @Issue("LRCD-534")
    @Features(REQUEST_SURVEY)
    public void twentyFourHoursTimeFormatInSurveyRequestPageTest()
    {
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class);

        SelectServicesSubPage selectServicesSubPage = vesselListPage.getAssetCards()
                .get(0)
                .clickRequestSurveyButton()
                .getSelectServicesSubPage();
        selectServicesSubPage.getServiceTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        String port = "Abenra";
        SelectLocationAndDatesSubPage selectLocationAndDatesSubPage = selectServicesSubPage
                .clickContinueButton()
                .selectPortName(port);

        // Verify that invalid arrival time format disables continue button
        invalidTimeFormats.forEach(time ->
                {
                    selectLocationAndDatesSubPage.setArrivalTime(time);
                    assert_().withFailureMessage("Arrival time error message is expected to be displayed")
                            .that(selectLocationAndDatesSubPage.getArrivalTimeErrorMessage())
                            .isEqualTo(errMessage);
                    selectLocationAndDatesSubPage.setDepartureTime(validTimeFormats.get(0));
                    assert_().withFailureMessage("Continue button is expected to be disabled")
                            .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                            .isTrue();

                }
        );

        //Verify that invalid departure time format disables continue button
        invalidTimeFormats.forEach(time ->
                {
                    selectLocationAndDatesSubPage.setArrivalTime(validTimeFormats.get(0));
                    selectLocationAndDatesSubPage.setDepartureTime(time);
                    assert_().withFailureMessage("Departure time error message is expected to be displayed")
                            .that(selectLocationAndDatesSubPage.getDepartureTimeErrorMessage())
                            .isEqualTo(errMessage);
                    assert_().withFailureMessage("Continue button is expected to be disabled")
                            .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                            .isTrue();
                }
        );

        //Verify that valid  arrival and departure time formats enables continue button
        validTimeFormats.forEach(time ->
                {
                    selectLocationAndDatesSubPage.setArrivalTime(time);
                    selectLocationAndDatesSubPage.setDepartureTime(time);
                    assert_().withFailureMessage("Continue button is expected to be enabled")
                            .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                            .isFalse();
                    assert_().withFailureMessage("Arrival time error message is expected not to be displayed")
                            .that(selectLocationAndDatesSubPage.getArrivalTimeErrorMessage())
                            .isEqualTo("");
                    assert_().withFailureMessage("Departure time error message is expected not to be displayed")
                            .that(selectLocationAndDatesSubPage.getDepartureTimeErrorMessage())
                            .isEqualTo("");
                }
        );
    }
}
