package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.adduser.steps.SearchUserEmailPage;
import main.administration.adduser.steps.element.UserCard;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.User.CLIENT_EMAIL;
import static constant.Epic.USER_MANAGEMENT;

public class UnableToSearchUserWithValidAzureAccountTest extends BaseTest
{
    @Test(description = "The LR admin user is unable to search a user email with valid azure account while creating/adding account.")
    @Issues({
           @Issue("LRCD-1680"),
           @Issue("LRCD-1892")
    })
    @Features(USER_MANAGEMENT)
    public void unableToSearchUserWithValidAzureAccountTest()
    {
        final String userEmail = CLIENT_EMAIL;
        final String invalidUserEmail = "invalid@test.com";

        SearchUserEmailPage searchUserEmailPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickAddUserButton();

        //LRCD-1680 - Valid user should be displayed
        boolean isUserFound = searchUserEmailPage.setEmailAddress(userEmail)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("User card not found"))
                .isDisplayed();

        assert_().withFailureMessage(String.format("Expected user with email %s is not found.", userEmail))
                .that(isUserFound)
                .isTrue();

        //LRCD-1892 - Error message should only be displayed when user is not found.
        assert_().withFailureMessage("Error message should not be displayed")
                .that(searchUserEmailPage.isUserNotFoundErrorMessageDisplayed())
                .isFalse();

        List<UserCard> cards = searchUserEmailPage.setEmailAddress(invalidUserEmail)
                .clickSearchButton()
                .getUserCards();

        assert_().withFailureMessage(String.format("User with email %s should not be found.", invalidUserEmail))
                .that(cards)
                .isEmpty();

        assert_().withFailureMessage("Error message should be displayed")
                .that(searchUserEmailPage.isUserNotFoundErrorMessageDisplayed())
                .isTrue();
    }
}
