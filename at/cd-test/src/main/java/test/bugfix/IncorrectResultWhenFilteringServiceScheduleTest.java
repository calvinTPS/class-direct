package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.serviceschedule.tabular.element.CodicilElement;
import main.serviceschedule.tabular.element.ServiceElement;
import main.vessellist.VesselListPage;
import main.viewasset.schedule.ServiceSchedulePage;
import org.apache.commons.collections.CollectionUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import typifiedelement.Timeline;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.SERVICE_SCHEDULE;

public class IncorrectResultWhenFilteringServiceScheduleTest extends BaseTest
{
    @Test(description = "SS-20 AC5 - [Mobile] Result not displays correctly based on date filtering in Service Schedule [Mobile]")
    @Issue("LRCD-1057")
    @Features(SERVICE_SCHEDULE)
    public void incorrectResultWhenFilteringServiceScheduleTest()
    {
        final LocalDate startDate = LocalDate.of(2016, 5, 1);
        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        Timeline timeline = serviceSchedulePage.getScheduleElement()
                .getServiceTimeline();
        LocalDate oneDayBeforeStartDate = startDate.minusDays(1);

        //Before changing timeline
        List<String> dueDateCodicils = serviceSchedulePage.getTabularView()
                .getCodicilTables()
                .getCodicils()
                .stream()
                .map(CodicilElement::getDueDate)
                .filter(e -> oneDayBeforeStartDate.isBefore(LocalDate.parse(e, ClassDirect.FRONTEND_TIME_FORMAT_DTS)))
                .collect(Collectors.toList());

        List<String> dueDateServices = new ArrayList<>();
        List<String> filteredDueDateServices = new ArrayList<>();

        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(e -> e.getServices()
                        .stream()
                        .map(ServiceElement::getDueDate)
                        .filter(s -> oneDayBeforeStartDate.isBefore(LocalDate.parse(s, ClassDirect.FRONTEND_TIME_FORMAT_DTS)))
                        .forEach(dueDateServices::add)
                );

        //Changing the start date of timeline
        timeline.selectStartDate(startDate);

        //After changing timeline
        List<String> filteredDueDateCodicils = serviceSchedulePage.getTabularView()
                .getCodicilTables()
                .getCodicils()
                .stream()
                .map(CodicilElement::getDueDate)
                .collect(Collectors.toList());

        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(e -> e.getServices()
                        .stream()
                        .map(ServiceElement::getDueDate)
                        .forEach(filteredDueDateServices::add)
                );

        assert_().withFailureMessage("Codicils with correct due dates should be displayed according to filtered timeline.")
                .that(CollectionUtils.isEqualCollection(dueDateCodicils, filteredDueDateCodicils))
                .isTrue();

        assert_().withFailureMessage("Services with correct due dates should be displayed according to filtered timeline.")
                .that(CollectionUtils.isEqualCollection(dueDateServices, filteredDueDateServices))
                .isTrue();
    }
}
