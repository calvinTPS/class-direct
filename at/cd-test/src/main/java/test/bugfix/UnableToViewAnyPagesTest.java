package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class UnableToViewAnyPagesTest extends BaseTest
{
    @Test(description = "Unable to view any page in the CD application")
    @Issue("LRCD-1069")
    @Features(FLEET_DASHBOARD)
    public void unableToViewAnyPagesTest()
    {
        assert_().withFailureMessage("Asset Cards are expected to be displayed")
                .that(LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                        .getVesselListPage()
                        .getAssetCards()
                        .size()).isGreaterThan(0);
    }

}
