package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.references.AssetTypeHDto;
import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.assettype.FilterByAssetTypePage;
import main.common.filter.assettype.element.AssetType;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import service.ReferenceData;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.AssetTableName.NON_SHIP_STRUCTURES;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class VesselTypeNotVisibleTest extends BaseTest
{

    @Test(description = "Filter assets by vessel type - Selected vessel type is not visible after setting up the filter by vessel type")
    @Issue("LRCD-568")
    @Features(FLEET_DASHBOARD)
    public void vesselTypeNotVisibleTest()
    {
        FilterByAssetTypePage filterByAssetTypePage =
                LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                        .getVesselListPage()
                        .getFilterBarPage()
                        .clickFilterButton(FilterAssetsPage.class)
                        .selectAllVesselsRadioButton()
                        .clickAssetTypeButton();

        List<AssetType> assetTypes = filterByAssetTypePage
                .getAssetCardsByTableName(NON_SHIP_STRUCTURES.getTableName())
                .getAssetType();
        assetTypes.forEach(AssetType::selectAssetType);

        List<String> assetTypeList = assetTypes.stream()
                .map(AssetType::getAssetTypeName)
                .collect(Collectors.toList());

        ReferenceData referenceData = new ReferenceData();
        List<Long> parentIdOfThirdLevel = referenceData.getAssetType()
                .stream()
                .filter(e -> assetTypeList.contains(e.getName()) && e.getLevelIndication().equals(3))
                .map(AssetTypeHDto::getId)
                .collect(Collectors.toList());

        List<Long> parentIdsOfFourthLevel = referenceData.getAssetType()
                .stream()
                .filter(e -> parentIdOfThirdLevel.contains(e.getParentId()))
                .map(AssetTypeHDto::getId)
                .collect(Collectors.toList());

        //The asset type on 5th level is displayed
        List<String> assetTypeNames = referenceData.getAssetType()
                .stream()
                .filter(e -> parentIdsOfFourthLevel.contains(e.getParentId()))
                .map(AssetTypeHDto::getName)
                .collect(Collectors.toList());

        filterByAssetTypePage.clickSubmitButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(c -> !c.getAssetType().equals("-")) //ignore ihs data set
                .forEach(e -> assert_().withFailureMessage("Selected Asset Type is expected to be displayed")
                        .that(assetTypeNames)
                        .contains(e.getAssetType()));

    }
}
