package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.adduser.eor.AddEorPermissionPage;
import main.administration.adduser.eor.ConfirmEorPermissionPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.*;
import static constant.Epic.USER_MANAGEMENT;
import static constant.User.NON_LR_EMAIL;
import static constant.User.UserAccountTypes;

public class TermsAndConditionsVisibleWhenConfirmingAccountCreationTest extends BaseTest
{
    @Test(description = "UM-53: Terms and Conditions section should not exist during confirmation of account creation.")
    @Issue("LRCD-2060")
    @Features(USER_MANAGEMENT)
    public void termsAndConditionsVisibleWhenConfirmingAccountCreationTest()
    {
        ConfirmEorPermissionPage confirmEorPermissionPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickAddUserButton()
                .setEmailAddress(NON_LR_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("User card not found"))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.EOR.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected account type not found."))
                .clickSelectButton(AddEorPermissionPage.class)
                .setAssetExpiryDate(FRONTEND_TIME_FORMAT_DTS.format(now.plusDays(7)))
                .setImoNumber(Asset.LAURA.getImoNumber())
                .clickAddExhibitionRecordsAssetButton();

        assert_().withFailureMessage("Terms and Conditions section should not exist during confirmation of account creation.")
                .that(confirmEorPermissionPage.isAcceptTermsAndConditionsVisible())
                .isFalse();
    }
}
