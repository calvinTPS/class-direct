package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.requestsurvey.sub.SelectLocationAndDatesSubPage;
import main.requestsurvey.sub.SelectServicesSubPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.REQUEST_SURVEY;

public class InvalidDateFormatOnSurveyRequestPageTest extends BaseTest
{
    @Test(description = "There is no drop down display for Estimated time of arrival and Estimated time of departure in Request Survey")
    @Issues({
            @Issue("LRCDT-535"),
            @Issue("LRCDT-2280")
    })
    @Features(REQUEST_SURVEY)

    public void invalidDateFormatOnSurveyRequestPageTest()
    {
        final String portName = "Belfast";
        final LocalDate date = ClassDirect.now;
        final String dateWithoutSpaces = date.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        final String dateWithInvalidCapitalLetter = getDateStringWithInvalidCapitalLetters(date);
        final String expectedErrorMessage = "You did not enter a valid date";

        // Navigate to Select services page
        SelectServicesSubPage selectServicesSubPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .get(0)
                .clickRequestSurveyButton()
                .getSelectServicesSubPage();

        // Select the first available service
        selectServicesSubPage.getServiceTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        // Proceed to the next page and fill in mandatory fields
        SelectLocationAndDatesSubPage selectLocationAndDatesSubPage = selectServicesSubPage
                .clickContinueButton()
                .selectPortName(portName);

        // Verify an arrival date as a string without spaces
        selectLocationAndDatesSubPage.setArrivalDate(dateWithoutSpaces);

        assert_().withFailureMessage("Error message not presented for an arrival date without spaces.")
                .that(selectLocationAndDatesSubPage.getArrivalDateErrorMessage())
                .isEqualTo(expectedErrorMessage);

        assert_().withFailureMessage("Continue button is enabled for an arrival date without spaces.")
                .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                .isTrue();

        // Clear the text box and verify that Continue button is enabled
        selectLocationAndDatesSubPage.clearArrivalDate();
        assert_().withFailureMessage("Continue button is disabled.")
                .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                .isFalse();

        // Verify an arrival date as a string with invalid capital letters
        selectLocationAndDatesSubPage.setArrivalDate(dateWithInvalidCapitalLetter);

        assert_().withFailureMessage("Capitalization for the month should be corrected automatically.")
                .that(selectLocationAndDatesSubPage.getArrivalDateErrorMessage())
                .isEmpty();

        assert_().withFailureMessage("Continue button is enabled for an arrival date with corrected invalid capital letters.")
                .that(selectLocationAndDatesSubPage.isContinueButtonDisabled())
                .isFalse();
    }

    private String getDateStringWithInvalidCapitalLetters(LocalDate date)
    {
        char[] month = date.getMonth().getDisplayName(TextStyle.SHORT, Locale.getDefault()).toLowerCase().toCharArray();
        month[2] = Character.toUpperCase(month[2]);
        return String.format("%s %s %s", date.getDayOfMonth(), String.valueOf(month), date.getYear());
    }
}
