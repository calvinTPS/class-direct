package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import typifiedelement.MultiTagTextBox;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class OneCharacterAsSearchCriteriaDoesNotShowAllFlagsTest extends BaseTest
{
    @Test(description = "Filter by Flag: System not loading all the flags in the fleet if we entered only one character in the search Criteria")
    @Issue("LRCD-1275")
    @Features(FLEET_DASHBOARD)
    public void oneCharacterAsSearchCriteriaDoesNotShowAllFlagsTest()
    {
        MultiTagTextBox multiTagTextBox = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .clickFlagButton()
                .getSearchFlagTextBox();

        multiTagTextBox.sendKeys("a");

        assert_().withFailureMessage("The number of flags is expected to be more than one.")
                .that(multiTagTextBox.getOptions().size())
                .isGreaterThan(1);
    }
}
