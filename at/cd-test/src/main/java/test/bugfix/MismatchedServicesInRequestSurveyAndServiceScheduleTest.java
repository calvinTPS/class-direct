package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.requestsurvey.RequestSurveyPage;
import main.requestsurvey.services.ServiceElement;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import main.viewasset.schedule.ServiceSchedulePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.REQUEST_SURVEY;

public class MismatchedServicesInRequestSurveyAndServiceScheduleTest extends BaseTest
{
    @Test(description = "Mismatch between services displayed in Request Survey page and in Service Schedule page.")
    @Issue("LRCD-1085")
    @Features(REQUEST_SURVEY)
    public void mismatchedServicesInRequestSurveyAndServiceScheduleTest()
    {
        AssetHubPage assetHubPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton();

        //Get list of services in Service Schedule page
        LinkedHashMap<String, List<String>> assetHubServices = new LinkedHashMap<>();
        ServiceSchedulePage serviceSchedulePage = assetHubPage.getSurveyPlannerPage()
                .clickViewFullScheduleButton();
        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(e ->
                        {
                            List<String> serviceList = e.getServices()
                                    .stream()
                                    .map(map -> map.getServiceName())
                                    .collect(Collectors.toList());
                            assetHubServices.put(e.getProductName(), serviceList);
                        }
                );

        RequestSurveyPage requestSurveyPage = serviceSchedulePage
                .clickBackButton()
                .getAssetHeaderPage()
                .clickRequestSurveyButton();

        //Get list of services in Request Survey page
        requestSurveyPage.getSelectServicesSubPage()
                .getServiceTable()
                .getRows()
                .stream()
                .filter(e -> !e.getProductName().isEmpty())
                .forEach(e ->
                        {
                            assert_().withFailureMessage(String.format("Product: %s should also exist in Request Survey page.", e.getProductName()))
                                    .that(assetHubServices.containsKey(e.getProductName()))
                                    .isTrue();

                            List<ServiceElement> requestSurveyServices = e.clickPlusOrMinusIcon().getServiceRows();
                            List<String> serviceScheduleServices = assetHubServices.get(e.getProductName());
                            assert_().withFailureMessage("All services in Request Survey page should match in Service Schedule page.")
                                    .that(requestSurveyServices.size())
                                    .isEqualTo(serviceScheduleServices.size());

                            for (int i = 0; i < requestSurveyServices.size(); i++)
                            {
                                assert_().withFailureMessage(
                                        String.format("Service: %s should also exist in Request Survey page.", requestSurveyServices.get(i).getServiceName()))
                                        .that(requestSurveyServices.get(i).getServiceName())
                                        .isEqualTo(serviceScheduleServices.get(i));
                            }

                            e.clickPlusOrMinusIcon();
                        }
                );
    }
}
