package test.bugfix;

import com.baesystems.ai.lr.cd.be.domain.dto.asset.ActionableItemHDto;
import com.baesystems.ai.lr.dto.codicils.CodicilDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import helper.TestDateHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.serviceschedule.tabular.element.CodicilElement;
import main.vessellist.VesselListPage;
import main.viewasset.schedule.ServiceSchedulePage;
import main.viewasset.schedule.tabular.base.ServiceTable;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;
import service.Assets;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;
import static constant.Epic.DUE_STATUS;

public class DueStatusTooltipNotDisplayedTest extends BaseTest
{
    private final Date currentDate = TestDateHelper.toDate(now);
    private static final String DUE_STATUS_ERROR_MESSAGE = "Due status does not meet the criteria for '%s'.";
    private static final String DUE_STATUS_NOT_VALID = "%s is not a valid status tooltip.";

    Assets assets = new Assets();
    List<ActionableItemHDto> actionableItemHDtos = assets.getActionableItems(LAURA.getId());
    List<String> statutoryActionableItems = actionableItemHDtos.stream()
            .filter(actionableItemHDto -> actionableItemHDto.getCategoryH().getName().equals("Statutory"))
            .map(CodicilDto::getTitle)
            .collect(Collectors.toList());

    @Test(description = "Due status_Text is not displayed when hover on the Due status")
    @Issues({
            @Issue("LRCD-1107"),
            @Issue("LRCD-1513"),
            @Issue("LRCD-1560")
    })
    @Features(DUE_STATUS)
    public void dueStatusTooltipNotDisplayedTest()
    {

        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(this::assertServiceDueStatusTooltip);

        serviceSchedulePage.clickFilterButton()
                .deselectActionableItem()
                .deselectStatutoryFinding()
                .selectConditionOfClass()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getCodicilTables()
                .getCodicils()
                .forEach(this::assertConditionOfClassDueStatusTooltip);

        serviceSchedulePage.clickFilterButton()
                .deselectConditionOfClass()
                .deselectStatutoryFinding()
                .selectActionableItem()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getCodicilTables()
                .getCodicils()
                .forEach(this::assertActionableItemDueStatusTooltip);
    }

    private void assertServiceDueStatusTooltip(ServiceTable serviceTable)
    {
        serviceTable.getServices().forEach(serviceElement ->
        {
            String dueStatusTooltip = serviceElement.getDueStatusTooltip();
            assert_().withFailureMessage("Due status tooltip is expected to be displayed")
                    .that(dueStatusTooltip)
                    .isNotEmpty();

            LocalDate dueDate = LocalDate.parse(serviceElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS);

            if (!serviceElement.getPostponementDate().equals(StringUtils.EMPTY))
            {
                dueDate = LocalDate.parse(serviceElement.getPostponementDate(), FRONTEND_TIME_FORMAT_DTS);
            }

            LocalDate oneMonthBeforeDueDate = dueDate.minusMonths(1);
            LocalDate threeMonthsBeforeDueDate = dueDate.minusMonths(3);

            switch (dueStatusTooltip)
            {
                case "Overdue":
                {
                    assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                            .that(now.isAfter(dueDate))
                            .isTrue();
                    break;
                }
                case "Due within 1 month":
                {
                    assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                            .that(currentDate)
                            .isIn(Range.closed(TestDateHelper.toDate(oneMonthBeforeDueDate), TestDateHelper.toDate(dueDate)));
                    break;
                }
                case "Lower Range Date Passed":
                {
                    assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                            .that(!serviceElement.getRangeDateFrom().equals(StringUtils.EMPTY) && serviceElement.getPostponementDate()
                                    .equals(StringUtils.EMPTY))
                            .isTrue();

                    LocalDate lowerRangeDate = LocalDate.parse(serviceElement.getRangeDateFrom(), FRONTEND_TIME_FORMAT_DTS);
                    assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                            .that(currentDate)
                            .isIn(Range.closed(TestDateHelper.toDate(lowerRangeDate), TestDateHelper.toDate(dueDate)));
                    break;
                }
                case "Not overdue":
                {
                    dueDate = serviceElement.getRangeDateFrom().equals(StringUtils.EMPTY) ? dueDate.minusMonths(3) : oneMonthBeforeDueDate;
                    assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                            .that(now)
                            .isAtMost(dueDate);
                    break;
                }
                case "Due within 3 months":
                {
                    //LRCD-1513 and LRCD-1560
                    assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                            .that(serviceElement.getRangeDateFrom())
                            .isEmpty();
                    assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                            .that(currentDate)
                            .isIn(Range.closed(TestDateHelper.toDate(threeMonthsBeforeDueDate), TestDateHelper.toDate(oneMonthBeforeDueDate)));
                    break;
                }
                // Not yet sure if there's a need to raise a bug since "Due Soon" is not in LRCD-851
                default:
                {
                    assert_().withFailureMessage(String.format(DUE_STATUS_NOT_VALID, dueStatusTooltip))
                            .fail();
                    break;
                }
            }
        });
    }

    private void assertActionableItemDueStatusTooltip(CodicilElement codicilElement)
    {
        String dueStatusTooltip = codicilElement.getDueStatusTooltip();
        assert_().withFailureMessage("Due status tooltip is expected to be displayed")
                .that(dueStatusTooltip)
                .isNotEmpty();

        LocalDate dueDate = LocalDate.parse(codicilElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS);
        LocalDate oneMonthBeforeDueDate = dueDate.minusMonths(1);
        LocalDate threeMonthsBeforeDueDate = dueDate.minusMonths(3);
        LocalDate oneYearBeforeDueDate = dueDate.minusYears(1);

        switch (dueStatusTooltip)
        {
            case "Overdue":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                        .that(now)
                        .isGreaterThan(dueDate);
                break;
            }
            case "Due within 1 month":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                        .that(currentDate)
                        .isIn(Range.closed(TestDateHelper.toDate(oneMonthBeforeDueDate), TestDateHelper.toDate(dueDate)));
                break;
            }
            case "Due within 3 months (non-Statutory)":
            {
                // only if its  non-statutory
                statutoryActionableItems.stream()
                        .filter(c -> !c.equals(codicilElement.getCodicilName()))
                        .findAny()
                        .ifPresent(c ->
                                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                                        .that(currentDate)
                                        .isIn(Range.closed(TestDateHelper.toDate(threeMonthsBeforeDueDate),
                                                TestDateHelper.toDate(oneMonthBeforeDueDate)))
                        );
                break;
            }
            case "Due within 1 year (Statutory)":
            {
                // only if its statutory
                statutoryActionableItems.stream()
                        .filter(c -> c.equals(codicilElement.getCodicilName()))
                        .findAny()
                        .ifPresent(c ->
                                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                                        .that(currentDate)
                                        .isIn(Range
                                                .closed(TestDateHelper.toDate(oneYearBeforeDueDate), TestDateHelper.toDate(threeMonthsBeforeDueDate)))
                        );
                break;
            }
            case "Not overdue":
            {
                statutoryActionableItems.stream()
                        .filter(c -> c.equals(codicilElement.getCodicilName()))
                        .findAny()
                        //check due status for statutory AIs
                        .map(c ->
                        {
                            assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                                    .that(now)
                                    .isAtMost(oneYearBeforeDueDate);
                            return c;
                        })
                        //check due status for non-statutory AIs
                        .orElseGet(() ->
                        {
                            assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                                    .that(now)
                                    .isAtMost(threeMonthsBeforeDueDate);
                            return ""; //dummy return
                        });
                break;
            }
            default:
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_NOT_VALID, dueStatusTooltip))
                        .fail();
                break;
            }
        }

    }

    private void assertConditionOfClassDueStatusTooltip(CodicilElement codicilElement)
    {
        String dueStatusTooltip = codicilElement.getDueStatusTooltip();
        assert_().withFailureMessage("Due status tooltip is expected to be displayed")
                .that(dueStatusTooltip)
                .isNotEmpty();

        LocalDate dueDate = LocalDate.parse(codicilElement.getDueDate(), FRONTEND_TIME_FORMAT_DTS);
        LocalDate oneMonthBeforeDueDate = dueDate.minusMonths(1);
        LocalDate threeMonthsBeforeDueDate = dueDate.minusMonths(3);

        switch (dueStatusTooltip)
        {
            case "Overdue":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                        .that(now.isAfter(dueDate))
                        .isTrue();
                break;
            }
            case "Due within 30 days":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                        .that(currentDate)
                        .isIn(Range.closed(TestDateHelper.toDate(oneMonthBeforeDueDate), TestDateHelper.toDate(dueDate)));
                break;
            }
            case "Due within three months":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                        .that(currentDate)
                        .isIn(Range.closed(TestDateHelper.toDate(threeMonthsBeforeDueDate), TestDateHelper.toDate(oneMonthBeforeDueDate)));
                break;
            }
            case "Not due within three months":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, dueStatusTooltip))
                        .that(now)
                        .isAtMost(threeMonthsBeforeDueDate);
                break;
            }
            default:
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_NOT_VALID, dueStatusTooltip))
                        .fail();
                break;
            }
        }
    }
}
