package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.mpms.crediting.MpmsCreditingPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;
import static constant.Epic.PMS;

public class SubsequentSubmitReportDoesNotWorkTest extends BaseTest
{
    @Test(description = "System should allow the user to do subsequent crediting.")
    @Issue("LRCDT-1747")
    @Features(PMS)
    public void subsequentSubmitReportDoesNotWorkTest()
    {
        final String expectedSuccessfulMessage = "The MPMS report submission was successful";
        MpmsCreditingPage mpmsCreditingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(ClassDirect.Asset.WITH_CERTIFICATES.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset card for %s not found.", ClassDirect.Asset.WITH_CERTIFICATES.getName())))
                .clickViewAssetButton()
                .getMpmsCard()
                .clickCreditingServiceButton();

        for (int i = 0; i < 2; i++)
        {
            // credit first 2 tasks subsequently
            mpmsCreditingPage.getTaskCards().parallelStream()
                    .findFirst()
                    .orElseThrow(() -> new java.util.NoSuchElementException("Expected task card is not found."))
                    .clickCreditingTaskButton()
                    .setDateCredited(now.format(FRONTEND_TIME_FORMAT_DTS));

            assert_().withFailureMessage("Submission successful message is expected to be displayed ")
                    .that(mpmsCreditingPage.getFooterPage()
                            .clickSubmitReportButton(MpmsCreditingPage.class)
                            .getTaskReportSuccessfulSubmissionText())
                    .isEqualTo(expectedSuccessfulMessage);
        }
    }
}
