package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.detail.AdministrationDetailPage;
import main.administration.detail.sub.accessibleassets.AccessibleAssetsPage;
import main.administration.detail.sub.accessibleassets.EditAccessibleAssetsPage;
import main.assetdetail.AssetDetailsPage;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.Asset.FOR_RESTRICTED_TEST;
import static constant.Epic.ASSET_HUB;

public class FullAssetDetailsShownForRestrictedAssetTest extends BaseTest {
    @Test(description = "Restricted asset can only view asset header with asset details CTA and Export asset information CTA at asset hub.")
    @Issue("LRCD-1828")
    @Features(ASSET_HUB)
    public void fullAssetDetailsShownForRestrictedAssetTest() {
        LandingPage landingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName());

        AdministrationDetailPage administrationDetailPage = landingPage.clickAdministrationTab()
                .getAdministrationTable()
                .getDataElements()
                .stream()
                .filter(e -> e.getUserEmail().equals(LR_ADMIN.getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Expected user with email %s is not found.", LR_ADMIN.getEmail())))
                .clickFurtherMoreDetailsButton();

        boolean isRestrictedAsset = administrationDetailPage.clickRestrictedAssetsTab()
                .getRestrictedAssetCards()
                .stream()
                .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()));

        if (!isRestrictedAsset) {
            //set as restricted asset
            EditAccessibleAssetsPage editAccessibleAssetsPage = administrationDetailPage.clickAccessibleAssetsTab()
                    .clickEditButton();

            editAccessibleAssetsPage.getAccessibleAssetCards()
                    .stream()
                    .filter(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()))
                    .findFirst()
                    .orElseThrow(() -> new NoSuchElementException(String.format("Expected asset: %s not found.", FOR_RESTRICTED_TEST.getName())))
                    .selectAssetCard();

            editAccessibleAssetsPage.clickMoveToRestrictedAssetButton()
                    .clickConfirmButton(AccessibleAssetsPage.class);

            // Checking if the asset is successfully moved to restricted assets
            assert_().withFailureMessage(String.format("Asset [%s] is not successfully moved to restricted assets.", FOR_RESTRICTED_TEST.getName()))
                    .that(administrationDetailPage.clickRestrictedAssetsTab()
                            .getRestrictedAssetCards()
                            .stream()
                            .anyMatch(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName())))
                    .isTrue();
        }

        AssetHubPage assetHubPage = landingPage.clickVesselListTab()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(FOR_RESTRICTED_TEST.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(FOR_RESTRICTED_TEST.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset not found."))
                .clickViewAssetButton();

        assert_().withFailureMessage("Request Survey button should not be visible.")
                .that(assetHubPage.getAssetHeaderPage().isRequestSurveyButtonDisplayed())
                .isFalse();

        assert_().withFailureMessage("Codicil card should not be visible.")
                .that(assetHubPage.getCodicilCard().exists())
                .isFalse();

        assert_().withFailureMessage("Surveys/Job card should not be visible.")
                .that(assetHubPage.getSurveysCard().exists())
                .isFalse();

        assert_().withFailureMessage("MPMS card should not be visible.")
                .that(assetHubPage.getMpmsCard().exists())
                .isFalse();

        assert_().withFailureMessage("Certificates and Records card should not be visible.")
                .that(assetHubPage.getCertificatesAndRecordsCard().exists())
                .isFalse();

        AssetDetailsPage assetDetailsPage = assetHubPage.getAssetHeaderPage().clickAssetDetailsButton();
        assert_().withFailureMessage("Export Asset Information card should be visible")
                .that(assetDetailsPage.getExportAssetInformationCard().exists())
                .isTrue();

        assert_().withFailureMessage("Technical Sister Vessels card should not be visible.")
                .that(assetDetailsPage.getTechnicalSisterVesselsCard().exists())
                .isFalse();

        assert_().withFailureMessage("Register Book Sister Vessels card should not be visible.")
                .that(assetDetailsPage.getRegisterBookSisterVesselsCard().exists())
                .isFalse();
    }
}
