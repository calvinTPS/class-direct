package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FilterAssetsDoesNotResetResultWhenInputTwoCharactersTest extends BaseTest
{
    @Test(description = "Filter does not default back to the default message when less than 3 characters is left in the search input.")
    @Issue("LRCD-2315")
    @Features(FLEET_DASHBOARD)
    public void filterAssetsDoesNotResetResultWhenInputTwoCharactersTest()
    {
        final String NOTIFICATION_TOO_MANY_RESULTS = "Too many results, please apply filters or do a search.";
        final String NOTIFICATION_NO_RESULT = "Your search did not return any results. Please modify your filter settings or reset to view all assets.";

        final String searchFilterWithResult = "opa";
        final String searchFilterTooManyResult = "op";
        final String searchFilterNoResult = "opaxxxxx";

        FilterBarPage filterBarPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(searchFilterWithResult);

        VesselListPage vesselListPage = filterBarPage.getPageReference(VesselListPage.class);
        assert_().withFailureMessage(String.format("Assets with name containing %s should be displayed.", searchFilterWithResult))
                .that(vesselListPage.getShowingXofYPage().getShown())
                .isGreaterThan(0);

        filterBarPage.setSearchTextBox(searchFilterTooManyResult);
        assert_().withFailureMessage(String.format("Notification message is expected to be '%s'.", NOTIFICATION_TOO_MANY_RESULTS))
                .that(NOTIFICATION_TOO_MANY_RESULTS)
                .isEqualTo(vesselListPage.getNotificationMessage());

        filterBarPage.setSearchTextBox(searchFilterNoResult);
        assert_().withFailureMessage(String.format("Notification message is expected to be '%s'.", NOTIFICATION_NO_RESULT))
                .that(NOTIFICATION_NO_RESULT)
                .isEqualTo(vesselListPage.getNotificationMessage());
    }
}
