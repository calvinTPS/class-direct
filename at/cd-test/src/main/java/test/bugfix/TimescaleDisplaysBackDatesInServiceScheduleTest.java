package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.schedule.ServiceSchedulePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import typifiedelement.Timeline;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.now;
import static constant.Epic.SERVICE_SCHEDULE;

public class TimescaleDisplaysBackDatesInServiceScheduleTest extends BaseTest
{
    @Test(description = "System displays back dates timescale starting from 2015 whereas the user as codicils due date from june 2017")
    @Issue("LRCD-794")
    @Features(SERVICE_SCHEDULE)
    public void timescaleDisplaysBackDatesInServiceScheduleTest()
    {
        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new java.util.NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        Timeline timeline = serviceSchedulePage.getScheduleElement().getServiceTimeline();
        LocalDate earliestDate = timeline.getSelectedStartDate().minusDays(1);

        assert_().withFailureMessage(
                "Earliest date in timescale should be Today's date or the earliest date among codicils/services if less than Today's date")
                .that(now.isAfter(earliestDate))
                .isTrue();

        serviceSchedulePage.getTabularView()
                .getServiceTables()
                .forEach(e -> e.getServices().forEach(
                        s -> assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of services.")
                                .that(LocalDate.parse(s.getDueDate(), FRONTEND_TIME_FORMAT_DTS).isAfter(earliestDate))
                                .isTrue())
                );

        serviceSchedulePage.getTabularView()
                .getCodicilTables()
                .getCodicils()
                .forEach(
                        s -> assert_().withFailureMessage("Earliest date in timeline should be the earliest among due dates of codicils.")
                                .that(LocalDate.parse(s.getDueDate(), FRONTEND_TIME_FORMAT_DTS).isAfter(earliestDate))
                                .isTrue()
                );
    }
}
