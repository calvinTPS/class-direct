package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import constant.Epic;
import main.administration.adduser.client.SelectClientPage;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.User.NON_LR_EMAIL;
import static constant.User.UserAccountTypes;

public class SearchByClientCodeDoesNotImplementTypeAheadTest extends BaseTest
{
    @Test(description = "System should start displaying results as the user is typing the code.")
    @Issue("LRCD-2212")
    @Stories("UM-28")
    @Features(Epic.USER_MANAGEMENT)
    public void searchByClientCodeDoesNotImplementTypeAheadTest()
    {
        final String clientCode = "100";
        SelectClientPage selectClientPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickAddUserButton()
                .setEmailAddress(NON_LR_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("User card not found"))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards()
                .stream()
                .filter(e -> e.getName().equals(UserAccountTypes.CLIENT.toString()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected account type not found."))
                .clickSelectButton(SelectClientPage.class);

        assert_().withFailureMessage("System should start displaying results as the user is typing the code.")
                .that(selectClientPage.setClientCodeTextbox(clientCode)
                        .getClientTypeAheadOptions().size())
                .isGreaterThan(0);

    }
}
