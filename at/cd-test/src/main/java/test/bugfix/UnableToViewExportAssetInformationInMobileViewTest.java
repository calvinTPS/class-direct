package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import main.common.export.ExportAssetListingPage;
import main.common.export.element.InformationTab;
import main.common.export.element.SurveysNotesAndActionsTab;
import main.landing.LandingPage;
import main.login.LoginPage;
import main.mobile.MobileFilterBarPage;
import main.mobile.filter.MobileFilterAssetsPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.ASSET_HUB;

public class UnableToViewExportAssetInformationInMobileViewTest extends BaseTest
{
    @Test(description = "User unable to view the complete Export Asset information overlay")
    @Issue("LRCD-2514")
    @Features(ASSET_HUB)
    public void unableToViewExportAssetInformationInMobileViewTest()
    {
        LandingPage landingPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getUserName());

        AppHelper.shrinkIntoMobileView();

        ExportAssetListingPage exportAssetListingPage = landingPage.getVesselListPage()
                .getMobileFilterBarPage()
                .clickFilterButton(MobileFilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .getMobileDialogFooter()
                .clickSubmitButton(MobileFilterBarPage.class)
                .clickSearchButton()
                .setSearchTextBox(LAURA.getName())
                .getMobileDialogFooter()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Asset card for %s was not found.", LAURA.getName())))
                .clickViewAssetButton()
                .getAssetHeaderPage()
                .clickAssetDetailsButton()
                .getExportAssetInformationCard()
                .clickExportAssetInformationButton();

        InformationTab informationTab = exportAssetListingPage.clickInformationTab();

        assert_().withFailureMessage("Basic asset information radio button should be displayed.")
                .that(informationTab.isBasicAssetInfoRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Full asset information radio button should be displayed.")
                .that(informationTab.isFullAssetInfoRadioButtonDisplayed())
                .isTrue();

        SurveysNotesAndActionsTab surveysNotesAndActionsTab = exportAssetListingPage.surveysNotesAndActionsTab();

        assert_().withFailureMessage("No Survey information radio button should be displayed.")
                .that(surveysNotesAndActionsTab.isNoSurveyRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Basic Survey information radio button should be displayed.")
                .that(surveysNotesAndActionsTab.isBasicSurveyRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Full Survey information radio button should be displayed.")
                .that(surveysNotesAndActionsTab.isFullSurveyRadioButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Condition of Class checkbox should be displayed.")
                .that(surveysNotesAndActionsTab.isConditionOfClassCheckboxDisplayed())
                .isTrue();
        assert_().withFailureMessage("Actionable Item checkbox should be displayed.")
                .that(surveysNotesAndActionsTab.isActionableItemCheckboxDisplayed())
                .isTrue();
        assert_().withFailureMessage("Asset Note checkbox should be displayed.")
                .that(surveysNotesAndActionsTab.isAssetNoteCheckboxDisplayed())
                .isTrue();
        assert_().withFailureMessage("Statutory finding checkbox should be displayed.")
                .that(surveysNotesAndActionsTab.isStatutoryFindingCheckboxDisplayed())
                .isTrue();
    }
}
