package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.User;
import constant.User.UserAccountTypes;
import constant.Epic;
import main.administration.adduser.steps.ChooseAccountTypePage;
import main.administration.adduser.steps.element.AccountTypeCard;
import main.login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;

public class AccountTypeNameNotDisplayedForNonLREmailTest extends BaseTest {
    @Test(description = "UM-27_Add User_Account Type Name not displayed and Administrator/Non-Administrator Account type card displayed for non LR email.")
    @Issue("LRCD-1894")
    @Features(Epic.USER_MANAGEMENT)
    public void accountTypeNameNotDisplayedForNonLREmailTest() {
        final List<String> EXPECTED_ACCOUNT_TYPES = Arrays.asList(
                UserAccountTypes.CLIENT.toString(),
                UserAccountTypes.FLAG.toString(),
                UserAccountTypes.SHIP_BUILDER.toString(),
                UserAccountTypes.EOR.toString());

        List<AccountTypeCard> accountTypeCardList = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .clickAddUserButton()
                .setEmailAddress(User.NON_LR_EMAIL)
                .clickSearchButton()
                .getUserCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("User card not found"))
                .clickCreateNewOrEditAccountButton(ChooseAccountTypePage.class)
                .getAccountTypeCards();

        accountTypeCardList.forEach(e ->
                assert_().withFailureMessage("Account type name should be displayed.")
                        .that(e.getName())
                        .isNotEmpty());

        assert_().withFailureMessage("Account type list should not include Administrator and Non Administrator.")
                .that(accountTypeCardList.stream()
                        .map(AccountTypeCard::getName)
                        .collect(Collectors.toList()))
                .containsAllIn(EXPECTED_ACCOUNT_TYPES);
    }
}
