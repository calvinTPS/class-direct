package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.ALL_SEARCH_TERM;

public class ClickHereToViewAllAssetDoesNotDisplayAllAssetsTest extends BaseTest
{
    private final String toBeFavouriteSearchTerm = "York";
    private final String expectedNotificationMessage =
            "Your search did not return any results. Please modify your filter settings or reset to view all "
                    + "assets.";

    @Test(description = "Clicking ViewAllAssets returns favourite assets instead of showing all assets")
    @Issues({
            @Issue("LRCD-2489"),
            @Issue("LRCD-2113")
    })

    @Features(Epic.FLEET_DASHBOARD)
    public void clickHereToViewAllAssetDoesNotDisplayAllAssetsTest()
    {

        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage();
        List<AssetCard> favouriteAssetCards = vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectMyFavouritesRadioButton()
                .clickSubmitButton(VesselListPage.class)
                .getAssetCards();
        // No favourites, add some
        if (favouriteAssetCards.size() == 0)
        {
            vesselListPage.getFilterBarPage()
                    .setSearchTextBox(toBeFavouriteSearchTerm)
                    .getPageReference(VesselListPage.class)
                    .getAssetCards()
                    .forEach(AssetCard::selectFavourite);
        }

        // get list of non favourite assets names
        List<String> nonFavouriteAssetCards = vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(ALL_SEARCH_TERM)
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .filter(assetCard -> !assetCard.isFavourite())
                .map(AssetCard::getAssetName)
                .collect(Collectors.toList());

        //Search for non favourite asset with favourite filter selected
        String notificationMessage = vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectMyFavouritesRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(nonFavouriteAssetCards.get(0))
                .getPageReference(VesselListPage.class)
                .getNotificationMessage();
        assert_().withFailureMessage(String.format("Notification message is expected to be : %s", expectedNotificationMessage))
                .that(notificationMessage)
                .isEqualTo(expectedNotificationMessage);
        assert_().withFailureMessage("All Vessel Radio Button is expected to be selected")
                .that(vesselListPage.clickNotificationLink()
                        .getFilterBarPage()
                        .clickFilterButton(FilterAssetsPage.class)
                        .isAllVesselsRadioButtonSelected()
                ).isTrue();

    }
}
