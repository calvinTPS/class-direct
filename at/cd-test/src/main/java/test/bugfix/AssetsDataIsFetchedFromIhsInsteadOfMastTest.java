package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LR_ASSET_WITH_IHS_DATA;
import static constant.ClassDirect.Credentials.LR_ADMIN;

public class AssetsDataIsFetchedFromIhsInsteadOfMastTest extends BaseTest
{
    private final String EMPTY_VALUE = "-";

    @Test(description = "Asset's data is fetched from IHS instead of MAST")
    @Issue("LRCD-2046")
    @Features(Epic.ASSET_HUB)
    public void assetsDataIsFetchedFromIhsInsteadOfMastTest()
    {
        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .setSearchTextBox(LR_ASSET_WITH_IHS_DATA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .forEach(
                        assetCard ->
                        {
                            assert_().withFailureMessage(String.format("Asset name is expected to be %s", LR_ASSET_WITH_IHS_DATA.getName()))
                                    .that(assetCard.getAssetName())
                                    .isEqualTo(LR_ASSET_WITH_IHS_DATA.getName());

                            assert_()
                                    .withFailureMessage(String.format("Asset imo number is expected to be %s", LR_ASSET_WITH_IHS_DATA.getImoNumber()))
                                    .that(assetCard.getImoNumber())
                                    .isEqualTo(LR_ASSET_WITH_IHS_DATA.getImoNumber());

                            assert_().withFailureMessage("Asset date of build is expected to be not empty")
                                    .that(assetCard.getDateOfBuild())
                                    .isNotEqualTo(EMPTY_VALUE);

                            assert_().withFailureMessage("Asset gross tonnage is expected to be not empty")
                                    .that(assetCard.getGrossTonnage())
                                    .isNotEqualTo(EMPTY_VALUE);

                            assert_().withFailureMessage("Asset flag is expected to be not empty")
                                    .that(assetCard.getFlagName())
                                    .isNotEqualTo(EMPTY_VALUE);

                            assert_().withFailureMessage("Asset class status is expected to be not empty")
                                    .that(assetCard.getClassStatus())
                                    .isNotEqualTo(EMPTY_VALUE);

                            assert_().withFailureMessage("Request Survey button is expected to be displayed")
                                    .that(assetCard.isRequestSurveyButtonDisplayed())
                                    .isTrue();
                        });

    }
}
