package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.serviceschedule.tabular.element.CodicilElement;
import main.vessellist.VesselListPage;
import main.viewasset.schedule.ServiceSchedulePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.STATUTORY_FINDINGS;

public class IncorrectDueStatusToolTipForStatutoryFindingsTest extends BaseTest
{
    private static final String DUE_STATUS_ERROR_MESSAGE = "Incorrect due status tool tip for '%s'.";

    @Test(description = "The due status cursor hover text message is not as per the requirement.")
    @Issue("LRCD-2567")
    @Features(STATUTORY_FINDINGS)
    public void incorrectDueStatusToolTipForStatutoryFindingsTest()
    {
        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        serviceSchedulePage.clickFilterButton()
                .deselectActionableItem()
                .deselectConditionOfClass()
                .selectStatutoryFinding()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getCodicilTables()
                .getCodicils()
                .forEach(this::assertStatutoryFindingDueStatusTooltip);
    }

    private void assertStatutoryFindingDueStatusTooltip(CodicilElement codicilElement)
    {
        String dueStatusTooltip = codicilElement.getDueStatusTooltip();
        assert_().withFailureMessage("Due status tooltip is expected to be displayed")
                .that(dueStatusTooltip)
                .isNotEmpty();

        switch (codicilElement.getDueStatus())
        {
            case "Overdue":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, codicilElement.getCodicilName()))
                        .that(dueStatusTooltip)
                        .isEqualTo("Overdue");
                break;
            }
            case "Not due":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, codicilElement.getCodicilName()))
                        .that(dueStatusTooltip)
                        .isEqualTo("Not due within three months");
                break;
            }
            case "Imminent":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, codicilElement.getCodicilName()))
                        .that(dueStatusTooltip)
                        .isEqualTo("Due within 30 days");
                break;
            }
            case "Due soon":
            {
                assert_().withFailureMessage(String.format(DUE_STATUS_ERROR_MESSAGE, codicilElement.getCodicilName()))
                        .that(dueStatusTooltip)
                        .isEqualTo("Due within three months");
                break;
            }
            default:
                assert_().withFailureMessage(
                        String.format("Due Status '%s' for '%s' is not valid.", codicilElement.getDueStatus(), codicilElement.getCodicilName()))
                        .fail();
                break;
        }
    }
}
