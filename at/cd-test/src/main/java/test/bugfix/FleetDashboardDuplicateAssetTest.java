package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.AppHelper;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class FleetDashboardDuplicateAssetTest extends BaseTest
{
    @Test(description = "When user scrolls down to see more than 15 assets, the displayed assets are duplicated.")
    @Issue("LRCD-1326")
    @Features(FLEET_DASHBOARD)
    public void fleetDashboardDuplicateAssetTest()
    {
        VesselListPage vesselListPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .getPageReference(VesselListPage.class);

        AppHelper.scrollToBottom();

        List<String> assetNames = vesselListPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .map(AssetCard::getAssetName)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Asset cards should not be duplicated when scrolled down.")
                .that(assetNames)
                .containsNoDuplicates();
    }
}
