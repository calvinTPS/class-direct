package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.administration.AdministrationPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.User.UserSearchOptions.COMPANY_NAME;
import static constant.Epic.USER_MANAGEMENT;

public class InaccurateResultWhenSearchingCompanyNameTest extends BaseTest
{
    @Test(description = "All users returned should have a company name containing the search term.")
    @Issue("LRCD-2243")
    @Features(USER_MANAGEMENT)
    public void inaccurateResultWhenSearchingCompanyNameTest()
    {
        final String companyName = "Lloyd's";

        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .clickAdministrationTab()
                .getFilterBarPage()
                .setSearchTextBox(companyName)
                .selectSearchDropDown(COMPANY_NAME.toString())
                .getPageReference(AdministrationPage.class)
                .getAdministrationTable()
                .getDataElements()
                .forEach(e-> assert_().withFailureMessage(
                        String.format("Company name of %s should contain the search string '%s'", e.getUserName(), companyName))
                        .that(e.getCompanyName())
                        .contains(companyName)
                );
    }
}
