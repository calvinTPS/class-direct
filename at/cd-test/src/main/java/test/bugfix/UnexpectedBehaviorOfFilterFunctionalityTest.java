package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterAssetsPage;
import main.landing.LandingPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class UnexpectedBehaviorOfFilterFunctionalityTest extends BaseTest
{
    @Test(description = "Filter should not default to codicil types 'All'")
    @Issue("LRCD-1700")
    @Features(FLEET_DASHBOARD)
    public void assetFilterShouldNotDefaultToCodicilTypeAll()
    {
        LandingPage landingPage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword());

        FilterAssetsPage filterAssetsPage = landingPage.getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class);

        assert_().withFailureMessage("Notes and actions type checkbox is expected to NOT be selected by default")
                .that(filterAssetsPage.isCodicilTypeAllCheckboxSelected())
                .isFalse();

    }
}
