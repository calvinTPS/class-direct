package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Range;
import constant.ClassDirect;
import helper.AppHelper;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.schedule.ServiceSchedulePage;
import main.viewasset.schedule.tabular.TabularView;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.time.LocalDate;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.Epic.SERVICE_SCHEDULE;

public class IncorrectResultWhenOnlyOneRangeDateSpecifiedTest extends BaseTest
{
    @Test(description = "If the range date has only one extension (From or To), system does not display correct result as specified.")
    @Issue("LRCD-1270")
    @Features(SERVICE_SCHEDULE)
    public void incorrectResultWhenOnlyOneRangeDateSpecifiedTest()
    {
        LocalDate fromDateRange = LocalDate.of(2016, 11, 16);
        LocalDate toDateRange = LocalDate.of(2016, 12, 16);

        ServiceSchedulePage serviceSchedulePage = LoginPage
                .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox(LAURA.getName())
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset card not found."))
                .clickViewAssetButton()
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton();

        // This Test is for mobile.
        AppHelper.shrinkIntoMobileView();

        // Initial range
        LocalDate minDateRange = serviceSchedulePage.getFromDatePickerForMobile();
        LocalDate maxDateRange = serviceSchedulePage.getToDatePickerForMobile();

        // Set the range from 16-Nov-2016 to 16-Dec-2016
        TabularView tabularView = serviceSchedulePage.setFromDatePickerForMobile(fromDateRange.format(FRONTEND_TIME_FORMAT_DTS))
                .setToDatePickerForMobile(toDateRange.format(FRONTEND_TIME_FORMAT_DTS))
                .getTabularView();

        assert_().withFailureMessage(String.format("There are expected codicils to be displayed for range %s to %s.", fromDateRange, toDateRange))
                .that(tabularView.getCodicilTables().getCodicils().size())
                .isGreaterThan(0);
        assert_().withFailureMessage(String.format("No service is expected to be displayed for range %s to %s.", fromDateRange, toDateRange))
                .that(tabularView.getServiceTables().size())
                .isEqualTo(0);

        // Set 'To' range as Empty
        tabularView = serviceSchedulePage.setToDatePickerForMobile(StringUtils.EMPTY)
                .getTabularView();

        assertDisplayedCodicilsAndServices(tabularView, fromDateRange, maxDateRange);

        // Set 'From' range as Empty
        tabularView = serviceSchedulePage.setToDatePickerForMobile(toDateRange.format(FRONTEND_TIME_FORMAT_DTS))
                .setFromDatePickerForMobile(StringUtils.EMPTY)
                .getTabularView();

        assertDisplayedCodicilsAndServices(tabularView, minDateRange, toDateRange);

        // Set 'From' and 'To' range as Empty
        tabularView = serviceSchedulePage.setFromDatePickerForMobile(StringUtils.EMPTY)
                .setToDatePickerForMobile(StringUtils.EMPTY)
                .getTabularView();

        assertDisplayedCodicilsAndServices(tabularView, minDateRange, maxDateRange);
    }

    private void assertDisplayedCodicilsAndServices(TabularView tabularView, LocalDate minDateRange, LocalDate maxDateRange)
    {
        assert_().withFailureMessage("Codicils should still be displayed if 'To' or/and 'From' range is empty.")
                .that(tabularView.getCodicilTables().getCodicils().size())
                .isGreaterThan(0);
        assert_().withFailureMessage("Services should still be displayed if 'To' or/and 'From' range is empty.")
                .that(tabularView.getServiceTables().size())
                .isGreaterThan(0);

        tabularView.getCodicilTables()
                .getCodicils()
                .forEach(e -> assert_()
                        .withFailureMessage(String.format("Due Date of Codicils should be in between %s and %s.", minDateRange, maxDateRange))
                        .that(LocalDate.parse(e.getDueDate(), ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                        .isIn(Range.closed(minDateRange, maxDateRange)));

        tabularView.getServiceTables()
                .forEach(s -> s.getServices()
                        .forEach(e -> assert_()
                                .withFailureMessage(String.format("Due Date of Services should be in between %s and %s.", minDateRange, maxDateRange))
                                .that(LocalDate.parse(e.getDueDate(), ClassDirect.FRONTEND_TIME_FORMAT_DTS))
                                .isIn(Range.closed(minDateRange, maxDateRange)))
                );
    }
}
