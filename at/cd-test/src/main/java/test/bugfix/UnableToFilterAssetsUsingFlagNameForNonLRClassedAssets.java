package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.element.AssetCard;
import main.common.filter.FilterAssetsPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.ClassStatus.ALL;
import static constant.ClassDirect.ClassStatus.NOT_LR_CLASSED;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.User.Flag.JAPAN;

public class UnableToFilterAssetsUsingFlagNameForNonLRClassedAssets extends BaseTest
{
    @Test(description = "User can filter asset by flag option from the filter panel")
    @Issue("LRCD-1729")
    @Features(Epic.FLEET_DASHBOARD)
    public void unableToFilterAssetsUsingFlagNameForNonLRClassedAssets()
    {
        final String searchFlag = JAPAN.getName();
        VesselListPage vesselListPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .selectAllVesselsRadioButton()
                .deSelectClassStatusCheckBoxByStatusName(ALL.toString())
                .selectClassStatusCheckBoxByStatusName(NOT_LR_CLASSED.toString())
                .clickFlagButton()
                .setSearchFlagTextBox(searchFlag)
                .clickSubmitButton()
                .clickSubmitButton(VesselListPage.class);
        List<AssetCard> cards = vesselListPage.getAssetCards();

        //There should be a ihs test asset called RED ISLAND that has the country flag Japan
        assert_().withFailureMessage("Assets are expected to be found")
                .that(cards)
                .isNotEmpty();

        cards.forEach(assetCard ->
                assert_().withFailureMessage("Expected assets flags to be " + searchFlag)
                        .that(assetCard.getFlagName())
                        .isEqualTo(searchFlag)
        );
    }
}
