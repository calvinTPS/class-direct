package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.ClassDirect;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.DUE_STATUS;

public class DueStatusAndIconNotDisplayedInAssetCardTest extends BaseTest
{
    @Test(description = "DS-05 :Vessel List - Asset Due status & Icon not displayed in the Asset Card")
    @Issue("LRCD-1917")
    @Features(DUE_STATUS)
    public void dueStatusAndIconNotDisplayedInAssetCardTest()
    {
        LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getVesselListPage()
                .getFilterBarPage()
                .clickFilterButton(FilterAssetsPage.class)
                .deSelectClassStatusCheckBoxByStatusName(ClassDirect.ClassStatus.NOT_LR_CLASSED.toString())
                .clickSubmitButton(FilterBarPage.class)
                .setSearchTextBox("***")
                .getPageReference(VesselListPage.class)
                .getAssetCards()
                .forEach(e ->
                {
                    assert_().withFailureMessage(String.format("This asset: %s should display its due status icon.", e.getAssetName()))
                            .that(e.isDueStatusIconDisplayed())
                            .isTrue();

                    assert_().withFailureMessage(String.format("This asset: %s should display its due status.", e.getAssetName()))
                            .that(e.getDueStatus())
                            .isNotEmpty();
                });
    }
}
