package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.Epic;
import main.common.element.CodicilElement;
import main.common.filter.FilterAssetsPage;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import main.vessellist.VesselListPage;
import main.viewasset.AssetHubPage;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.schedule.ServiceSchedulePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Asset.LAURA;
import static constant.ClassDirect.Credentials.LR_ADMIN;

public class ActionableItemsNotDisplayedInSurveyPlannerTest extends BaseTest
{
    @Test(description = "Actionable Item is not displayed if Due Date is Less than the oldest due/postponement date among Codicils and Services).")
    @Issue("LRCD-1913")
    @Features(Epic.DUE_STATUS)
    public void actionableItemsNotDisplayedInSurveyPlannerTest()
    {
         CodicilsAndDefectsPage codicilsAndDefectsPage = LoginPage
                 .login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                 .getFilterBarPage()
                 .clickFilterButton(FilterAssetsPage.class)
                 .selectAllVesselsRadioButton()
                 .clickSubmitButton(FilterBarPage.class)
                 .setSearchTextBox(LAURA.getName())
                 .getPageReference(VesselListPage.class)
                 .getAssetCards()
                 .stream()
                 .findFirst()
                 .orElseThrow(() -> new java.util.NoSuchElementException(String.format("Expected asset card for %s was not found.", LAURA.getName())))
                 .clickViewAssetButton()
                 .getCodicilCard()
                 .clickViewAllButton();

        List<String> actionableItems = codicilsAndDefectsPage
                .clickActionableItemsTab()
                .getCodicilElements()
                .stream()
                .map(CodicilElement::getTitle)
                .collect(Collectors.toList());

        List<String> actionableItemsInSurveyPlanner = codicilsAndDefectsPage.getAssetHeaderPage()
                .clickBackButton(AssetHubPage.class)
                .getSurveyPlannerPage()
                .clickViewFullScheduleButton()
                .clickFilterButton()
                .deselectConditionOfClass()
                .deselectStatutoryFinding()
                .selectActionableItem()
                .clickSubmitButton(ServiceSchedulePage.class)
                .getTabularView()
                .getCodicilTables()
                .getCodicils()
                .stream()
                .map(main.serviceschedule.tabular.element.CodicilElement::getCodicilName)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Actionable items should be displayed in Survey Planner page.")
                .that(actionableItemsInSurveyPlanner)
                .containsAllIn(actionableItems);
    }
}
