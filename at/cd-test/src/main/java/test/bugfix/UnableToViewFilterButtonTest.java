package test.bugfix;

import com.frameworkium.core.ui.tests.BaseTest;
import main.common.filter.FilterBarPage;
import main.login.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.Credentials.LR_ADMIN;
import static constant.Epic.FLEET_DASHBOARD;

public class UnableToViewFilterButtonTest extends BaseTest
{
    @Test(description = "FI-10 AC1 - Unable to view filter button on the left side bar")
    @Issue("LRCD-491")
    @Features(FLEET_DASHBOARD)
    public void unableToViewFilterButtonTest()
    {

        FilterBarPage filterBarPage = LoginPage.login(LR_ADMIN.getUserName(), LR_ADMIN.getPassword())
                .getFilterBarPage();

        assert_().withFailureMessage("Filter Icon is expected to be visible")
                .that(filterBarPage.isFilterIconDisplayed())
                .isTrue();

    }
}
