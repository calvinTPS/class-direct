package main.login;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import config.Config;
import helper.PageFactory;
import main.landing.LandingPage;
import main.toc.TermsAndConditionsPage;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import retry.RetryAnalyser;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import static constant.Config.*;

public class LoginPage extends BasePage<LoginPage>
{

    @Name("User Name Text Box")
    @FindBy(css = "")
    private WebElement userNameTextBox;

    @Name("Password Text Box")
    @FindBy(css = "")
    private WebElement passwordTextBox;

    @Name("Sign in button")
    @FindBy(css = "")
    private WebElement signInButton;

    @Step("Sign in to Application - username: \"{0}\", password: \"{0}\"")
    public static LandingPage login(String userName, String password)
    {
        //TODO: Login work flow to be coded here
        BaseTest.getDriver().navigate().to(HTTP + Config.getBaseURL());
        Wait<WebDriver> wait = BaseTest.newWaitWithTimeout(GLOBAL_PAGE_LOAD_TIMEOUT);
        wait.until(ExpectedConditions.urlMatches(HTTP_OR_HTTPS_REGEX + Config.getBaseURL() + "/")); //wait until its past any weird auto proxy load
        wait.until(ExpectedConditions
                .not(ExpectedConditions.urlMatches(HTTP_OR_HTTPS_REGEX + Config.getBaseURL() + "/$"))); //wait until its not an empty url
        wait.until(ExpectedConditions.not(ExpectedConditions.urlContains(MAIN_URL))); //wait until the main redirection url passes
        //TODO: this results in a hard wait of 1 second as the redirection glitches to /vessel-list page and back to /toc
        //TODO: /vessel-list spins for data when first loaded which will mitigate any of test from idle wait (no increase of test time)
        //TODO: this circumstance will change when the CR to defaults /vessel-list to return empty page (potentially accrue 1 sec idle time per test across all tests)
        //TODO: to refactor this with a more robust wait state while minimizing the potential idle wait time when the CR comes through
        int time;
        if (RetryAnalyser.getCurrentRetry() > 0 && RetryAnalyser.wasCausedByTOC())
        {
            time = 5; // it takes longer time to get to TOC page, increase wait time
        }
        else
        {
            time = 1; // TOC page has been accepted or is quick to be redirected, reduce wait time
        }
        wait = BaseTest.newWaitWithTimeout(time);
        try
        {
            wait.until(ExpectedConditions.urlContains(TOC_URL));
        }
        catch (TimeoutException ignore)
        {
        }

        String url = BaseTest.getDriver().getCurrentUrl();
        if (url.contains(TOC_URL))
        {
            return PageFactory.newInstance(TermsAndConditionsPage.class)
                    .clickAcceptTermsAndConditions();
        }
        else
        {
            return PageFactory.newInstance(LandingPage.class);
        }
    }
}
