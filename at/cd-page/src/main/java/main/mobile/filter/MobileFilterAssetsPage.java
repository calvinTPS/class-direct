package main.mobile.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import main.mobile.MobileDialogFooter;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;
import typifiedelement.RadioButton;
import typifiedelement.WebElement;

import java.util.List;

public class MobileFilterAssetsPage extends BasePage<MobileFilterAssetsPage>
{
    @Visible
    @Name("All Vessels Radio button")
    @FindBy(css = "md-dialog [aria-label='All Assets']")
    private RadioButton allVesselsRadioButton;

    @Visible
    @Name("My Favourites Radio button")
    @FindBy(css = "md-dialog [aria-label='My Favourites']")
    private RadioButton myFavouritesRadioButton;

    @Visible
    @Name("Minimum Gross tonnage Text box")
    @FindBy(css = "md-dialog .input-min")
    private WebElement minGrossTonnageTextBox;

    @Visible
    @Name("Maximum Gross tonnage Text box")
    @FindBy(css = "md-dialog .input-max")
    private WebElement maxGrossTonnageTextBox;

    @Visible
    @Name("Date of Build From")
    @FindBy(css = "md-dialog .date-from")
    private WebElement dateOfBuildFromDatepicker;

    @Visible
    @Name("Date of Build To")
    @FindBy(css = "md-dialog .date-to")
    private WebElement dateOfBuildToDatepicker;

    @Name("Class Status CheckBox")
    @FindBy(css = "md-dialog .class-status-container md-checkbox")
    private List<CheckBox> classStatusCheckBox;

    @Visible
    @Name("Mobile Dialog footer")
    @FindBy(css = "md-dialog[aria-label='Filter assets'] .cd-actions")
    private MobileDialogFooter mobileDialogFooter;

    @Step("Select All Vessels Radio button")
    public MobileFilterAssetsPage selectAllVesselsRadioButton()
    {
        allVesselsRadioButton.select();
        return this;
    }

    @Step("Select My Favourites Radio button")
    public MobileFilterAssetsPage selectMyFavouritesRadioButton()
    {
        myFavouritesRadioButton.select();
        return this;
    }

    @Step("Set minimum Gross Tonnage as \"{0}\"")
    public MobileFilterAssetsPage setMinGrossTonnage(String tons)
    {
        minGrossTonnageTextBox.clear();
        minGrossTonnageTextBox.sendKeys(tons);
        minGrossTonnageTextBox.sendKeys(Keys.ENTER);
        return this;
    }

    @Step("Set maximum Gross Tonnage as \"{0}\"")
    public MobileFilterAssetsPage setMaxGrossTonnage(String tons)
    {
        maxGrossTonnageTextBox.clear();
        maxGrossTonnageTextBox.sendKeys(tons);
        maxGrossTonnageTextBox.sendKeys(Keys.ENTER);
        return this;
    }

    @Step("Set 'Date Of Build From' as \"{0}\"")
    public MobileFilterAssetsPage setDateOfBuildFrom(String date)
    {
        dateOfBuildFromDatepicker.clear();
        dateOfBuildFromDatepicker.sendKeys(date);
        dateOfBuildFromDatepicker.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set 'Date Of Build To' as \"{0}\"")
    public MobileFilterAssetsPage setDateOfBuildTo(String date)
    {
        dateOfBuildToDatepicker.clear();
        dateOfBuildToDatepicker.sendKeys(date);
        dateOfBuildToDatepicker.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Select \"{0}\" Class Status Checkbox")
    public MobileFilterAssetsPage selectClassStatusCheckBoxByStatusName(String statusName)
    {
        classStatusCheckBox.stream()
                .filter(e -> e.getAttribute("aria-label").trim().equals(statusName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("This status: %s is not found", statusName)))
                .select();
        return this;
    }

    @Step("Deselect \"{0}\" Class Status Checkbox")
    public MobileFilterAssetsPage deSelectClassStatusCheckBoxByStatusName(String statusName)
    {
        classStatusCheckBox.stream()
                .filter(e -> e.getAttribute("aria-label").trim().equals(statusName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("This status: %s is not found", statusName)))
                .deselect();
        return this;
    }

    @Step("Get Mobile Dialog Footer")
    public MobileDialogFooter getMobileDialogFooter()
    {
        return mobileDialogFooter;
    }
}
