package main.mobile.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import constant.ClassDirect;
import main.mobile.MobileDialogFooter;
import main.viewasset.certificates.certificatesandrecords.filter.element.CertificateStatusCheckbox;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class MobileFilterCertificatesPage extends BasePage<MobileFilterCertificatesPage>
{
    @Visible
    @Name("Issued Date From")
    @FindBy(css = "md-dialog div.widget-container:nth-child(2) .date-from")
    private WebElement issuedDateFromDatepicker;

    @Visible
    @Name("Issued Date To")
    @FindBy(css = "md-dialog div.widget-container:nth-child(2) .date-to")
    private WebElement issuedDateToDatepicker;

    @Visible
    @Name("Expiry Date From")
    @FindBy(css = "md-dialog div.widget-container:nth-child(3) .date-from")
    private WebElement expiryDateFromDatepicker;

    @Visible
    @Name("Expiry Date To")
    @FindBy(css = "md-dialog div.widget-container:nth-child(3) .date-to")
    private WebElement expiryDateToDatepicker;

    @Name("Certificate Status Checkbox List")
    @FindBy(css = "md-dialog .filter div.checkbox")
    private List<CertificateStatusCheckbox> certificateStatusCheckboxList;

    @Visible
    @Name("Mobile Dialog footer")
    @FindBy(css = "md-dialog[aria-label='Filter certificates'] .cd-actions")
    private MobileDialogFooter mobileDialogFooter;

    @Step("Set 'Issued Build From' as \"{0}\"")
    public MobileFilterCertificatesPage setIssuedDateFrom(String date)
    {
        issuedDateFromDatepicker.clear();
        issuedDateFromDatepicker.sendKeys(date);
        issuedDateFromDatepicker.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set 'Issued Build To' as \"{0}\"")
    public MobileFilterCertificatesPage setIssuedDateTo(String date)
    {
        issuedDateToDatepicker.clear();
        issuedDateToDatepicker.sendKeys(date);
        issuedDateToDatepicker.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set 'Expiry Build From' as \"{0}\"")
    public MobileFilterCertificatesPage setExpiryDateFrom(String date)
    {
        expiryDateFromDatepicker.clear();
        expiryDateFromDatepicker.sendKeys(date);
        expiryDateFromDatepicker.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set 'Expiry Build To' as \"{0}\"")
    public MobileFilterCertificatesPage setExpiryDateTo(String date)
    {
        expiryDateToDatepicker.clear();
        expiryDateToDatepicker.sendKeys(date);
        expiryDateToDatepicker.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Select Certificate Status: \"{0}\" checkbox")
    public MobileFilterCertificatesPage selectCertificateStatusCheckbox(ClassDirect.CertificateStatus certificateStatus)
    {
        certificateStatusCheckboxList.stream()
                .filter(e -> certificateStatus.toString().equals(e.getCertificateStatusName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Certificate Status not found."))
                .selectCheckbox();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Deselect Certificate Status: \"{0}\" checkbox")
    public MobileFilterCertificatesPage deselectCertificateStatusCheckbox(ClassDirect.CertificateStatus certificateStatus)
    {
        certificateStatusCheckboxList.stream()
                .filter(e -> certificateStatus.toString().equals(e.getCertificateStatusName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Certificate Status not found."))
                .deselectCheckbox();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Mobile Dialog Footer")
    public MobileDialogFooter getMobileDialogFooter()
    {
        return mobileDialogFooter;
    }
}
