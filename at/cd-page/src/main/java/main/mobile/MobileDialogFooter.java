package main.mobile;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import helper.PageFactory;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class MobileDialogFooter extends HtmlElement
{
    @Name("Cancel button")
    @FindBy(className = "modal-cancel-button")
    private List<WebElement> cancelButton;

    @Visible
    @Name("Submit button")
    @FindBy(className = "modal-submit-button")
    private WebElement confirmButton;

    @Step("Click 'Cancel' button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> pageObjectClass)
    {
        cancelButton.stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No cancel button was displayed."))
                .click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click 'Submit' button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass)
    {
        confirmButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }
}
