package main.mobile.search;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import main.mobile.MobileDialogFooter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.ClearableTextBox;
import typifiedelement.RadioGroup;

public class MobileSearchPage extends BasePage<MobileSearchPage>
{
    @Visible
    @Name("Search Category Radio group")
    @FindBy(css = "[aria-label='Select Category']")
    private RadioGroup searchCategoryRadioGroup;

    @Visible
    @Name("Search Text box")
    @FindBy(css = "md-dialog .search input")
    private ClearableTextBox searchTextBox;

    @Visible
    @Name("Mobile Dialog footer")
    @FindBy(css = "md-dialog .cd-actions")
    private MobileDialogFooter mobileDialogFooter;

    @Step("Set Search Text Box as \"{0}\"")
    public MobileSearchPage setSearchTextBox(String text)
    {
        searchTextBox.clear();
        searchTextBox.sendKeys(text);
        return this;
    }

    @Step("Select \"{0}\" from Search Category Radio Group")
    public MobileSearchPage selectSearchCategoryByName(String category)
    {
        searchCategoryRadioGroup.selectByName(category);
        return this;
    }

    @Step("Get Mobile Dialog Footer")
    public MobileDialogFooter getMobileDialogFooter()
    {
        return mobileDialogFooter;
    }
}
