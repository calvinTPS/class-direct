package main.mobile.sort;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import main.mobile.MobileDialogFooter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioGroup;

public class MobileSortPage extends BasePage<MobileSortPage>
{
    @Visible
    @Name("Sort Category Radio group")
    @FindBy(css = "md-dialog md-radio-group")
    private RadioGroup sortCategoryRadioGroup;

    @Visible
    @Name("Mobile Dialog footer")
    @FindBy(css = "md-dialog .cd-actions")
    private MobileDialogFooter mobileDialogFooter;

    @Step("Select \"{0}\" from Sort Category Radio Group")
    public MobileSortPage selectSortCategoryByName(String category)
    {
        sortCategoryRadioGroup.selectByName(category);
        return this;
    }

    @Step("Get Mobile Dialog Footer")
    public MobileDialogFooter getMobileDialogFooter()
    {
        return mobileDialogFooter;
    }

    @Step("Is Sort Category Radio group displayed")
    public boolean isSortCategoryRadioGroupDisplayed()
    {
        return sortCategoryRadioGroup.exists();
    }
}
