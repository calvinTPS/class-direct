package main.mobile;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.mobile.search.MobileSearchPage;
import main.mobile.sort.MobileSortPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class MobileFilterBarPage extends BasePage<MobileFilterBarPage>
{
    @Visible
    @Name("Filter button")
    @FindBy(css = ".filter-search-mobile-container [aria-label='Filter']")
    private WebElement filterButton;

    @Visible
    @Name("Search button")
    @FindBy(css = ".filter-search-mobile-container [aria-label='Search']")
    private WebElement searchButton;

    @Visible
    @Name("Sort button")
    @FindBy(css = ".filter-search-mobile-container [aria-label='Sort']")
    private WebElement sortButton;

    @Name("Export button")
    @FindBy(css = ".filter-search-mobile-container [aria-label='Export']")
    private WebElement exportButton;

    @Step("Click Filter button and open \"{0}\" page")
    public <T extends BasePage<T>> T clickFilterButton(Class<T> page)
    {
        filterButton.click();
        return PageFactory.newInstance(page);
    }

    @Step("Click Search button and open Mobile Search page")
    public MobileSearchPage clickSearchButton()
    {
        searchButton.click();
        return PageFactory.newInstance(MobileSearchPage.class);
    }

    @Step("Click Sort button and open Mobile Sort page")
    public MobileSortPage clickSortButton()
    {
        sortButton.click();
        return PageFactory.newInstance(MobileSortPage.class);
    }

    @Step("Click Export button and open \"{0}\" page")
    public <T extends BasePage<T>> T clickExportButton(Class<T> page)
    {
        exportButton.click();
        return PageFactory.newInstance(page);
    }
}
