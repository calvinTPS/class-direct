package main.toc;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.landing.LandingPage;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TermsAndConditionsPage extends BasePage<TermsAndConditionsPage>
{

    @Visible
    @Name("I accept terms and conditions button")
    @FindBy(css = "accept-terms-and-conditions button:nth-child(2)")
    private WebElement acceptTermsAndConditionsButton;

    @Step("Click accept terms and conditions")
    public LandingPage clickAcceptTermsAndConditions()
    {
        //In parallel execution, simultaneous clicks will cause instances where the page will not respond to clicks
        //Random wait times, before clicks will help with parallel execution problems
        ThreadLocal<Integer> s = ThreadLocal.withInitial(() -> new Random().nextInt(6));
        try
        {
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(s.get(), TimeUnit.SECONDS)); //stagger wait
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        acceptTermsAndConditionsButton.click();
        return PageFactory.newInstance(LandingPage.class);
    }
}
