package main.vessellist;

import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.common.ShowingXOfYPage;
import main.common.element.AssetCard;
import main.common.export.ExportAssetListingPage;
import main.common.filter.FilterBarPage;
import main.landing.LandingPage;
import main.mobile.MobileFilterBarPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

public class VesselListPage extends BasePage<VesselListPage>
{
    @Name("Asset Cards")
    private List<AssetCard> assetCards;

    @Name("Shown Asset Count")
    @FindBy(css = "visible-results .visible-count")
    private WebElement shownAssetCount;

    @Name("Total Asset Count")
    @FindBy(css = "visible-results .total-count")
    private WebElement totalAssetCount;

    @Name("Export Asset Information button")
    @FindBy(css = ".filter-search-container export-assets button")
    private WebElement exportAssetInformationButton;

    @Name("Notification Message")
    @FindBy(className = "notification")
    private WebElement notification;

    @Step("Get Asset Cards")
    public List<AssetCard> getAssetCards()
    {
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return assetCards;
    }

    @Step("Get Filter Bar")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Get Mobile Filter Bar")
    public MobileFilterBarPage getMobileFilterBarPage()
    {
        return PageFactory.newInstance(MobileFilterBarPage.class);
    }

    @Step("Get Landing Page")
    public LandingPage getLandingPage()
    {
        return PageFactory.newInstance(LandingPage.class);
    }

    @Step("Get \"Showing X of Y page\"")
    public ShowingXOfYPage getShowingXofYPage(){
        return PageFactory.newInstance(ShowingXOfYPage.class);
    }

    @Step("Click 'Export Asset Information' button")
    public ExportAssetListingPage clickExportAssetInformationButton()
    {
        exportAssetInformationButton.click();
        return PageFactory.newInstance(ExportAssetListingPage.class);
    }

    @Step("Get notification message")
    public String getNotificationMessage()
    {
        return notification.getText();
    }

    @Step("Click on notification link")
    public VesselListPage clickNotificationLink()
    {
        notification.findElement(By.tagName("a")).click();
        return this;
    }
}
