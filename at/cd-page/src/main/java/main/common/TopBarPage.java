package main.common;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.cdlive.CdLivePage;
import main.myaccount.MyAccountPage;
import main.nationaladmin.NationalAdministrationPage;
import main.vessellist.VesselListPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class TopBarPage extends BasePage<TopBarPage>
{
    @Visible
    @Name("Class Direct Logo")
    @FindBy(css = "cd-logo a")
    private WebElement classDirectLogo;

    //@Visible - only visible if data is available in Azure
    @Name("Client Name Tab")
    @FindBy(css = "asset-client-name button")
    private WebElement clientTab;

    @Visible
    @Name("My Account Button")
    @FindBy(css = ".my-account")
    private WebElement myAccountButton;

    @Visible
    @Name("Lloyd's Register Logo")
    @FindBy(css = ".md-toolbar-tools>a:nth-child(1)")
    private WebElement lloydsRegisterLogo;

    @Visible
    @Name("National Administration Button")
    @FindBy(css = "a[href='/national-administration']")
    private WebElement nationalAdministrationButton;

    @Visible
    @Name("External Link Button")
    @FindBy(css = "header-menu button")
    private WebElement externalLinkButton;

    @Step("Click Class Direct Logo")
    public CdLivePage clickClassDirectLogo()
    {
        classDirectLogo.click();
        return PageFactory.newInstance(CdLivePage.class, "https://www.cdlive.lr.org/");
    }

    @Step("Click Client Tab")
    public VesselListPage clickClientTab()
    {
        clientTab.click();
        return PageFactory.newInstance(VesselListPage.class);
    }

    @Step("Click My Account button")
    public MyAccountPage clickMyAccountButton()
    {
        myAccountButton.click();
        return PageFactory.newInstance(MyAccountPage.class);
    }

    @Step("Get my account name")
    public String getAccountName()
    {
        return myAccountButton.getText();
    }

    @Step("Click National Administration button")
    public NationalAdministrationPage clickNationalAdministrationButton()
    {
        nationalAdministrationButton.click();
        return PageFactory.newInstance(NationalAdministrationPage.class);
    }

    @Step("Is National Administration button displayed")
    public boolean isNationalAdministrationButtonDisplayed()
    {
        return nationalAdministrationButton.isDisplayed();
    }
}
