package main.common.export;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.export.element.AssetsTab;
import main.common.export.element.InformationTab;
import main.common.export.element.SurveysNotesAndActionsTab;

import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import static java.util.Arrays.asList;

public class ExportAssetListingPage extends BasePage<ExportAssetListingPage>
{
    @Visible
    @Name("Close button")
    @FindBy(css = "md-toolbar>div.content-main>button")
    private WebElement closeButton;

    @Visible
    @Name("Cancel button")
    @FindBy(css = "md-dialog-actions button:nth-child(1)")
    private WebElement cancelButton;

    @Name("Assets tab")
    @FindBy(css = "div.item span[data-translate='Assets']")
    private WebElement assetsTab;

    @Visible
    @Name("Information tab")
    @FindBy(css = "div.item span[data-translate='cd-information']")
    private WebElement informationTab;

    @Visible
    @Name("Surveys, Notes and Actions tab")
    @FindBy(css = "div.item span[data-translate='cd-surveys-notes-and-actions']")
    private WebElement surveysNotesAndActionsTab;

    @Name("Assets tab content")
    private AssetsTab assetsTabContent;

    @Name("Information tab content")
    private InformationTab informationTabContent;

    @Name("Services and Codicils tab content")
    private SurveysNotesAndActionsTab surveysNotesAndActionsTabContent;

    @Visible
    @Name("Export button")
    @FindBy(css = "md-dialog-actions button:nth-child(2)")
    private WebElement exportButton;

    @Step("Click Close button")
    public <T extends BasePage<T>> T clickCloseButton(Class<T> classObjectPage)
    {
        closeButton.click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(asList(closeButton, cancelButton)));
        return PageFactory.newInstance(classObjectPage);
    }

    @Step("Click Cancel button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> classObjectPage)
    {
        cancelButton.click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(asList(closeButton, cancelButton)));
        return PageFactory.newInstance(classObjectPage);
    }

    @Step("Click Assets tab")
    public AssetsTab clickAssetsTab()
    {
        assetsTab.click();
        return assetsTabContent;
    }

    @Step("Click Information tab")
    public InformationTab clickInformationTab()
    {
        informationTab.click();
        return informationTabContent;
    }

    @Step("Click 'Surveys, Notes and Actions' tab")
    public SurveysNotesAndActionsTab surveysNotesAndActionsTab()
    {
        surveysNotesAndActionsTab.click();
        return surveysNotesAndActionsTabContent;
    }

    @Step("Click Export button")
    public ExportAssetListingPage clickExportButton()
    {
        exportButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Is Export button enabled")
    public boolean isExportButtonEnabled()
    {
        return exportButton.isEnabled();
    }
}
