package main.common.export.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;
import typifiedelement.RadioButton;

@FindBy(css = "[data-ng-show*='servicesCodicils']")
public class SurveysNotesAndActionsTab extends HtmlElement
{
    @Visible
    @Name("No survey information radiobutton")
    @FindBy(css = "md-radio-button[aria-label='No survey information.']")
    private RadioButton noSurveyInfoRadioButton;

    @Visible
    @Name("Basic survey information radiobutton")
    @FindBy(css = "md-radio-button[aria-label='Basic survey information']")
    private RadioButton basicSurveyInfoRadioButton;

    @Visible
    @Name("Full survey information radiobutton")
    @FindBy(css = "md-radio-button[aria-label='Full survey information']")
    private RadioButton fullSurveyInfoRadioButton;

    @Visible
    @Name("Condition of Class checkbox")
    @FindBy(css = "div.codicils md-checkbox[aria-label='Condition of class']")
    private CheckBox conditionOfClassCheckbox;

    @Visible
    @Name("Actionable item checkbox")
    @FindBy(css = "div.codicils md-checkbox[aria-label='Actionable item']")
    private CheckBox actionableItemCheckbox;

    @Visible
    @Name("Asset Note checkbox")
    @FindBy(css = "div.codicils md-checkbox[aria-label='Asset note']")
    private CheckBox assetNoteCheckbox;

    @Name("Statutory Finding checkbox")
    @FindBy(css = "div.codicils md-checkbox[aria-label='Statutory finding']")
    private CheckBox statutoryFindingCheckbox;

    @Step("Select 'No survey information' radiobutton")
    public SurveysNotesAndActionsTab selectNoSurveyInfoRadioButton()
    {
        noSurveyInfoRadioButton.select();
        return this;
    }

    @Step("Select 'Basic survey information' radiobutton")
    public SurveysNotesAndActionsTab selectBasicSurveyInfoRadioButton()
    {
        basicSurveyInfoRadioButton.select();
        return this;
    }

    @Step("Select 'Full survey information' radiobutton")
    public SurveysNotesAndActionsTab selectFullSurveyInfoRadioButton()
    {
        fullSurveyInfoRadioButton.select();
        return this;
    }

    @Step("Select 'Condition of class' checkbox")
    public SurveysNotesAndActionsTab selectConditionOfClassCheckbox()
    {
        conditionOfClassCheckbox.select();
        return this;
    }

    @Step("Select 'Actionable item' checkbox")
    public SurveysNotesAndActionsTab selectActionableItemCheckbox()
    {
        actionableItemCheckbox.select();
        return this;
    }

    @Step("Select 'Asset Note' checkbox")
    public SurveysNotesAndActionsTab selectAssetNoteCheckbox()
    {
        assetNoteCheckbox.select();
        return this;
    }

    @Step("Select 'Statutory Finding' checkbox")
    public SurveysNotesAndActionsTab selectStatutoryFindingCheckbox()
    {
        statutoryFindingCheckbox.select();
        return this;
    }

    @Step("Deselect 'Condition of class' checkbox")
    public SurveysNotesAndActionsTab deselectConditionOfClassCheckbox()
    {
        conditionOfClassCheckbox.deselect();
        return this;
    }

    @Step("Deselect 'Actionable item' checkbox")
    public SurveysNotesAndActionsTab deselectActionableItemCheckbox()
    {
        actionableItemCheckbox.deselect();
        return this;
    }

    @Step("Deselect 'Asset Note' checkbox")
    public SurveysNotesAndActionsTab deselectAssetNoteCheckbox()
    {
        assetNoteCheckbox.deselect();
        return this;
    }

    @Step("Deselect 'Statutory Finding' checkbox")
    public SurveysNotesAndActionsTab deselectStatutoryFindingCheckbox()
    {
        statutoryFindingCheckbox.deselect();
        return this;
    }

    @Step("Is 'Condition of Class' checkbox selected")
    public boolean isConditionOfClassCheckboxSelected()
    {
        return conditionOfClassCheckbox.isSelected();
    }

    @Step("Is 'Actionable Item' checkbox selected")
    public boolean isActionableItemCheckboxSelected()
    {
        return actionableItemCheckbox.isSelected();
    }

    @Step("Is 'Asset note' checkbox selected")
    public boolean isAssetNoteCheckboxSelected()
    {
        return assetNoteCheckbox.isSelected();
    }

    @Step("Is 'Full Survey Information' radiobutton selected")
    public boolean isFullSurveyRadioButtonSelected()
    {
        return fullSurveyInfoRadioButton.isSelected();
    }

    @Step("Is 'Condition of Class' checkbox displayed")
    public boolean isConditionOfClassCheckboxDisplayed()
    {
        return conditionOfClassCheckbox.isDisplayed();
    }

    @Step("Is 'Actionable Item' checkbox displayed")
    public boolean isActionableItemCheckboxDisplayed()
    {
        return actionableItemCheckbox.isDisplayed();
    }

    @Step("Is 'Asset note' checkbox displayed")
    public boolean isAssetNoteCheckboxDisplayed()
    {
        return assetNoteCheckbox.isDisplayed();
    }

    @Step("Is 'Statutory Finding' checkbox displayed")
    public boolean isStatutoryFindingCheckboxDisplayed()
    {
        return statutoryFindingCheckbox.isDisplayed();
    }

    @Step("Is 'Basic Survey Information' radiobutton displayed")
    public boolean isBasicSurveyRadioButtonDisplayed()
    {
        return basicSurveyInfoRadioButton.isDisplayed();
    }

    @Step("Is 'No Survey Information' radiobutton displayed")
    public boolean isNoSurveyRadioButtonDisplayed()
    {
        return noSurveyInfoRadioButton.isDisplayed();
    }

    @Step("Is 'Full Survey Information' radiobutton displayed")
    public boolean isFullSurveyRadioButtonDisplayed()
    {
        return fullSurveyInfoRadioButton.isDisplayed();
    }
}
