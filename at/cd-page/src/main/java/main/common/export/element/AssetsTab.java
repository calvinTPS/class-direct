package main.common.export.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import main.common.element.MiniAssetCard;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

@FindBy(css = "[data-ng-show*='assets']")
public class AssetsTab extends HtmlElement
{
    @Visible
    @Name("Remove All button")
    @FindBy(css = "top-button-controls>button:nth-child(1)")
    private WebElement removeAllButton;

    @Visible
    @Name("Add All button")
    @FindBy(css = "top-button-controls>button:nth-child(2)")
    private WebElement addAllButton;

    @Name("Asset Cards")
    private List<MiniAssetCard> assetCards;

    @Step("Get asset cards")
    public List<MiniAssetCard> getAssetCards()
    {
        return assetCards;
    }

    @Step("Click Remove All button")
    public AssetsTab clickRemoveAllButton()
    {
        removeAllButton.click();
        return this;
    }

    @Step("Click Add All button")
    public AssetsTab clickAddAllButton()
    {
        addAllButton.click();
        return this;
    }

    @Step("Is Add All button enabled")
    public boolean isAddAllButtonEnabled()
    {
        return addAllButton.isEnabled();
    }

    @Step("Is Remove All button enabled")
    public boolean isRemoveAllButtonEnabled()
    {
        return removeAllButton.isEnabled();
    }
}
