package main.common.export.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioButton;

@FindBy(css = "[data-ng-show*='information']")
public class InformationTab extends HtmlElement
{
    @Visible
    @Name("Basic Asset information radiobutton")
    @FindBy(css = "md-radio-button[aria-label='Basic asset information']")
    private RadioButton basicAssetInfoRadioButton;

    @Visible
    @Name("Full Asset information radiobutton")
    @FindBy(css = "md-radio-button[aria-label='Full asset information']")
    private RadioButton fullAssetInfoRadioButton;

    @Step("Select 'Basic Asset information' radiobutton")
    public InformationTab selectBasicAssetInfoRadioButton()
    {
        basicAssetInfoRadioButton.select();
        return this;
    }

    @Step("Select 'Full Asset information' radiobutton")
    public InformationTab selectFullAssetInfoRadioButton()
    {
        fullAssetInfoRadioButton.select();
        return this;
    }

    @Step("Is 'Basic Asset information' Radiobutton selected")
    public boolean isBasicAssetInfoRadioButtonSelected()
    {
        return basicAssetInfoRadioButton.isSelected();
    }

    @Step("Is 'Full Asset information' Radiobutton selected")
    public boolean isFullAssetInfoRadioButtonSelected()
    {
        return fullAssetInfoRadioButton.isSelected();
    }

    @Step("Is 'Basic Asset information' Radiobutton displayed")
    public boolean isBasicAssetInfoRadioButtonDisplayed()
    {
        return basicAssetInfoRadioButton.isDisplayed();
    }

    @Step("Is 'Full Asset information' Radiobutton displyed")
    public boolean isFullAssetInfoRadioButtonDisplayed()
    {
        return fullAssetInfoRadioButton.isDisplayed();
    }
}
