package main.common;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.requestsurvey.RequestSurveyPage;
import main.assetdetail.AssetDetailsPage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import typifiedelement.Star;
import typifiedelement.WebElement;
import java.util.List;

public class AssetHeaderPage extends BasePage<AssetHeaderPage>
{
    @Visible
    @Name("Back Button")
    @FindBy(css = "button.back-button")
    private WebElement backButton;

    @Name("Asset details Button")
    @FindBy(css = "[aria-label='Asset details']")
    private WebElement assetDetailsButton;

    @Name("Request Survey Button")
    @FindBy(css = "[aria-label='Request survey']")
    private WebElement requestSurveyButton;

    @Visible
    @Name("Asset Emblem")
    @FindBy(tagName = "emblem")
    private WebElement assetEmblem;

    @Visible
    @Name("Asset Name")
    @FindBy(className = "asset-name")
    private WebElement assetName;

//    @Visible
    @Name("Favourite Icon")
    @FindBy(css = "star")
    //annoying switch between views leaves us hidden star that we can't distinguish
    //use isDisplayed to determine which star is currently active on screen
    private List<Star> favouriteIcon;

    @Name("Overall Due Status")
    @FindBy(css = "asset-header due-status strong")
    private List<WebElement> overallDueStatus;

    @Name("MMS Indicator icon")
    @FindBy(css = ".asset-info .status+icon")
    private WebElement mmsIndicator;

    @Name("IMO Number")
    @FindBy(css = ".details div:nth-child(1)> .detail:nth-child(1) strong")
    private WebElement imoNumber;

    @Name("Asset Type")
    @FindBy(css = ".details div:nth-child(1)> .detail:nth-child(2) strong")
    private WebElement assetType;

    @Name("Class Status")
    @FindBy(css = ".details div:nth-child(1)> .detail:nth-child(3) strong")
    private WebElement classStatus;

    @Name("Date of build")
    @FindBy(css = ".details div:nth-child(2)> .detail:nth-child(1) strong")
    private WebElement dateOfBuild;

    @Name("Gross Tonnage")
    @FindBy(css = ".details div:nth-child(2)> .detail:nth-child(2) strong")
    private WebElement grossTonnage;

    @Name("Flag")
    @FindBy(css = ".details div:nth-child(2)> .detail:nth-child(3) strong")
    private WebElement flag;

    @Step("Click View Asset Button")
    public AssetDetailsPage clickAssetDetailsButton() {
        assetDetailsButton.click();
        return PageFactory.newInstance(AssetDetailsPage.class);
    }

    @Step("Click Request Survey Button")
    public RequestSurveyPage clickRequestSurveyButton() {
        requestSurveyButton.click();
        return PageFactory.newInstance(RequestSurveyPage.class);
    }

    @Step("Select Favourite")
    public AssetHeaderPage selectFavourite() {
        favouriteIcon.stream()
                .filter(TypifiedElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No favourite icon is displayed"))
                .select();
        wait.until(webDriver -> isFavouriteIconSelected());
        return this;
    }

    @Step("Is Favourite Icon Selected")
    public boolean isFavouriteIconSelected() {
        return favouriteIcon.stream()
                .filter(TypifiedElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No favourite icon is displayed"))
                .isFavourited();
    }

    @Step("Is request survey button displayed")
    public boolean isRequestSurveyButtonDisplayed() {
        return requestSurveyButton.isExists();
    }

    @Step("Is asset details button displayed")
    public boolean isAssetDetailsButtonDisplayed() {
        return assetDetailsButton.isExists();
    }

    @Step("Is asset emblem displayed")
    public boolean isAssetEmblemDisplayed() {
        return assetEmblem.isExists();
    }

    @Step("Is MMS Indicator displayed")
    public boolean isMmsIndicatorDisplayed() {
        return mmsIndicator.isExists();
    }

    @Step("Get Asset Name")
    public String getAssetName() {
        return assetName.getText().trim();
    }

    @Step("Get Overall Due Status")
    public String getOverallDueStatus() {
        return overallDueStatus.stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No Overall Status is displayed"))
                .getText().trim();
    }

    @Step("Get IMO Number")
    public String getImoNumber() {
        return imoNumber.getText().trim();
    }

    @Step("Get Asset Type")
    public String getAssetType() {
        return assetType.getText().trim();
    }

    @Step("Get Class Status")
    public String getClassStatus() {
        return classStatus.getText().trim();
    }

    @Step("Get Date of Build")
    public String getDateOfBuild() {
        return dateOfBuild.getText().trim();
    }

    @Step("Get Gross Tonnge")
    public String getGrossTonnage() {
        return grossTonnage.getText().trim();
    }

    @Step("Get Flag")
    public String getFlag() {
        return flag.getText().trim();
    }

    @Step("Click Back Button")
    public <T extends BasePage<T>> T clickBackButton(Class<T> page)
    {
        backButton.click();
        return PageFactory.newInstance(page);
    }
}
