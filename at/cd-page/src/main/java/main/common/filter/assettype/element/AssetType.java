package main.common.filter.assettype.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.AppHelper;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Asset Type Cards")
@FindBy(css = "div.asset-type-category mini-asset-card .card")
public class AssetType extends HtmlElement
{
    @Visible
    @Name("Asset Type Name")
    @FindBy(css = "div.card-container div.emblem-wrapper>div")
    private WebElement assetTypeName;

    @Visible
    @Name("Asset type emblem")
    @FindBy(css = "emblem md-icon")
    private WebElement assetTypeEmblem;

    @Step("Get Asset Type Name")
    public String getAssetTypeName()
    {
        return assetTypeName.getAttribute("title").trim();
    }

    @Step("Select Asset Type")
    public AssetType selectAssetType()
    {
        if(!isAssetTypeSelected())
        {
            AppHelper.scrollToMiddle(this);
            this.click();
        }
        return this;
    }

    @Step("Deselect Asset Type")
    public AssetType deselectAssetType()
    {
        if(isAssetTypeSelected())
        {
            AppHelper.scrollToMiddle(this);
            this.click();
        }
        return this;
    }

    @Step("Is Asset type selected")
    private boolean isAssetTypeSelected()
    {
        return this.getAttribute("class").contains("highlighted");
    }
}
