package main.common.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import helper.ExtraExpectedConditions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ByChained;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.*;
import typifiedelement.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class FilterCodicilsPage extends BasePage<FilterCodicilsPage>
{
    @Visible
    @Name("Status Header")
    @FindBy(css = "accordion[data-title='cd-status'] .accordion-header")
    private WebElement statusHeader;

    @Visible
    @Name("Category Header")
    @FindBy(css = "accordion[data-title='cd-category'] .accordion-header")
    private WebElement categoryHeader;

    @Visible
    @Name("Impose date Header")
    @FindBy(css = "accordion[data-title='cd-impose-date'] .accordion-header")
    private WebElement imposeDateHeader;

    @Name("Due date Header")
    @FindBy(css = "accordion[data-title='cd-due-date'] .accordion-header")
    private WebElement dueDateHeader;

    @Visible
    @Name("Cancel button")
    @FindBy(className = "modal-cancel-button")
    private WebElement cancelButton;

    @Visible
    @Name("Submit button")
    @FindBy(className = "modal-submit-button")
    private WebElement submitButton;

    @Visible
    @Name("Clear filters button")
    @FindBy(className = "modal-clear-button")
    private WebElement clearFiltersButton;

    @Name("Inactive radio button")
    @FindBy(css = "md-radio-button[aria-label='Inactive']")
    private RadioButton inactiveRadioButton;

    @Name("Open radio button")
    @FindBy(css = "md-radio-button[aria-label='Open']")
    private RadioButton openRadioButton;

    @Name("Change Recommended radio button")
    @FindBy(css = "md-radio-button[aria-label='Change Recommended']")
    private RadioButton changeRecommendedRadioButton;

    @Name("Closed for current job radio button")
    @FindBy(css = "md-radio-button[aria-label='Closed for Current Job']")
    private RadioButton closedForCurrentJobRadioButton;

    @Name("Closed radio button")
    @FindBy(css = "md-radio-button[aria-label='Closed']")
    private RadioButton closedRadioButton;

    @Name("Cancelled radio button")
    @FindBy(css = "md-radio-button[aria-label='Cancelled']")
    private RadioButton cancelledRadioButton;

    @Name("Continues Satisfactory radio button")
    @FindBy(css = "md-radio-button[aria-label='Continues Satisfactory']")
    private RadioButton continuesSatisfactoryRadioButton;

    @Name("All check box")
    @FindBy(xpath = "//div[contains(text(), 'All')]/preceding-sibling::md-checkbox")
    private CheckBox allCheckBox;

    @Name("Statutory check box")
    @FindBy(xpath = "//div[contains(text(), 'Statutory')]/preceding-sibling::md-checkbox")
    private CheckBox statutoryCheckBox;

    @Name("Class check box")
    @FindBy(xpath = "//div[contains(text(), 'Class')]/preceding-sibling::md-checkbox")
    private CheckBox classCheckBox;

    @Name("External check box")
    @FindBy(xpath = "//div[contains(text(), 'External')]/preceding-sibling::md-checkbox")
    private CheckBox externalCheckBox;

    @Name("Impose Date From date picker")
    @FindBy(xpath = "(//accordion[@data-title='cd-impose-date']//md-datepicker)[1]")
    private Datepicker imposeDateFromDatepicker;

    @Name("Impose Date To date picker")
    @FindBy(xpath = "(//accordion[@data-title='cd-impose-date']//md-datepicker)[2]")
    private Datepicker imposeDateToDatepicker;

    @Name("Due Date From date picker")
    @FindBy(xpath = "(//accordion[@data-title='cd-due-date']//md-datepicker)[1]")
    private Datepicker dueDateFromDatepicker;

    @Name("Due Date To date picker")
    @FindBy(xpath = "(//accordion[@data-title='cd-due-date']//md-datepicker)[2]")
    private Datepicker dueDateToDatepicker;

    @Name("Status Radio Group")
    @FindBy(css = "md-radio-group.rider-filter")
    private RadioGroup statusRadioGroup;

    @Name("Category check box list")
    @FindBy(css = "div.checkbox .label")
    private List<WebElement> categoryCheckboxList;

    @Step("Click Cancel button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> pageObjectClass)
    {
        cancelButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return helper.PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Submit button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass)
    {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Clear filters button")
    public FilterCodicilsPage clickClearFiltersButton()
    {
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return this;
    }

    @Step("Select Inactive radio button")
    public FilterCodicilsPage selectInactiveRadioButton()
    {
        inactiveRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Open radio button")
    public FilterCodicilsPage selectOpenRadioButton()
    {
        openRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Change Recommended radio button")
    public FilterCodicilsPage selectChangeRecommendedRadioButton()
    {
        changeRecommendedRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Continues Satisfactory radio button")
    public FilterCodicilsPage selectContinuesSatisfactoryRadioButton()
    {
        continuesSatisfactoryRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Closed for Current Job radio button")
    public FilterCodicilsPage selectClosedForCurrentJobRadioButton()
    {
        closedForCurrentJobRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Closed radio button")
    public FilterCodicilsPage selectClosedRadioButton()
    {
        closedRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Cancelled radio button")
    public FilterCodicilsPage selectCancelledRadioButton()
    {
        cancelledRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select All check box")
    public FilterCodicilsPage selectAllCheckBox()
    {
        allCheckBox.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Deselect All check box")
    public FilterCodicilsPage deselectAllCheckBox()
    {
        allCheckBox.deselect();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Statutory check box")
    public FilterCodicilsPage selectStatutoryCheckBox()
    {
        statutoryCheckBox.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Deselect Statutory check box")
    public FilterCodicilsPage deselectStatutoryCheckBox()
    {
        statutoryCheckBox.deselect();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Class check box")
    public FilterCodicilsPage selectClassCheckBox()
    {
        classCheckBox.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Deselect Class check box")
    public FilterCodicilsPage deselectClassCheckBox()
    {
        classCheckBox.deselect();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select External check box")
    public FilterCodicilsPage selectExternalCheckBox()
    {
        externalCheckBox.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Deselect External check box")
    public FilterCodicilsPage deselectExternalCheckBox()
    {
        externalCheckBox.deselect();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set 'Impose Date From' date picker - \"{0}\"")
    public FilterCodicilsPage setImposeDateFromDatepicker(String date)
    {
        imposeDateFromDatepicker.sendKeys(date);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set 'Impose Date To' date picker - \"{0}\"")
    public FilterCodicilsPage setImposeDateToDatepicker(String date)
    {
        imposeDateToDatepicker.sendKeys(date);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set 'Due Date From' date picker - \"{0}\"")
    public FilterCodicilsPage setDueDateFromDatepicker(String date)
    {
        dueDateFromDatepicker.sendKeys(date);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set 'Due Date To' date picker - \"{0}\"")
    public FilterCodicilsPage setDueDateToDatepicker(String date)
    {
        dueDateToDatepicker.sendKeys(date);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Check if Inactive radio button is displayed")
    public boolean isInactiveRadioButtonDisplayed()
    {
        return inactiveRadioButton.isDisplayed();
    }

    @Step("Check if Open radio button is displayed")
    public boolean isOpenRadioButtonDisplayed()
    {
        return openRadioButton.isDisplayed();
    }

    @Step("Check if 'Change Recommended' radio button is displayed")
    public boolean isChangeRecommendedRadioButtonDisplayed()
    {
        return changeRecommendedRadioButton.isDisplayed();
    }

    @Step("Check if 'Closed for Current Job' radio button is displayed")
    public boolean isClosedForCurrentJobRadioButtonDisplayed()
    {
        return closedForCurrentJobRadioButton.isDisplayed();
    }

    @Step("Check if Closed radio button is displayed")
    public boolean isClosedRadioButtonDisplayed()
    {
        return closedRadioButton.isDisplayed();
    }

    @Step("Check if Cancelled radio button is displayed")
    public boolean isCancelledRadioButtonDisplayed()
    {
        return cancelledRadioButton.isDisplayed();
    }

    @Step("Check if Continues Satisfactory radio button is displayed")
    public boolean isContinuesSatisfactoryRadioButtonDisplayed()
    {
        return continuesSatisfactoryRadioButton.isDisplayed();
    }

    @Step("Check if Open radio button is selected")
    public boolean isOpenRadioButtonSelected()
    {
        return openRadioButton.isSelected();
    }

    @Step("Check if All check box is displayed")
    public boolean isAllCheckBoxDisplayed()
    {
        return allCheckBox.isDisplayed();
    }

    @Step("Check if Statutory check box is displayed")
    public boolean isStatutoryCheckBoxDisplayed()
    {
        return statutoryCheckBox.isDisplayed();
    }

    @Step("Check if Class check box is displayed")
    public boolean isClassCheckBoxDisplayed()
    {
        return classCheckBox.isDisplayed();
    }

    @Step("Check if External check box is displayed")
    public boolean isExternalCheckBoxDisplayed()
    {
        return externalCheckBox.isDisplayed();
    }

    @Step("Check if All check box is selected")
    public boolean isAllCheckBoxSelected()
    {
        return allCheckBox.isSelected();
    }

    @Step("Check if Statutory check box is selected")
    public boolean isStatutoryCheckBoxSelected()
    {
        return statutoryCheckBox.isSelected();
    }

    @Step("Check if Class check box is selected")
    public boolean isClassCheckBoxSelected()
    {
        return classCheckBox.isSelected();
    }

    @Step("Check if External check box is selected")
    public boolean isExternalCheckBoxSelected()
    {
        return externalCheckBox.isSelected();
    }

    @Step("Check if 'Impose Date From' date picker is displayed")
    public boolean isImposeDateFromDatepickerDisplayed()
    {
        return imposeDateFromDatepicker.isDisplayed();
    }

    @Step("Check if 'Impose Date To' date picker is displayed")
    public boolean isImposeDateToDatepickerDisplayed()
    {
        return imposeDateToDatepicker.isDisplayed();
    }

    @Step("Check if 'Due Date From' date picker is displayed")
    public boolean isDueDateFromDatepickerDisplayed()
    {
        return dueDateFromDatepicker.isDisplayed();
    }

    @Step("Check if 'Due Date To' date picker is displayed")
    public boolean isDueDateToDatepickerDisplayed()
    {
        return dueDateToDatepicker.isDisplayed();
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Get Status List")
    public List<String> getStatusList()
    {
        return statusRadioGroup.getOptions()
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    @Step("Get Category List")
    public List<String> getCategoryList()
    {
        return categoryCheckboxList.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    @Step("Get from datepicker - 'Due Date To'")
    public String getDueDateToDatepicker()
    {
        return dueDateToDatepicker.getText();
    }

    @Step("Get from datepicker - 'Due Date From'")
    public String getDueDateFromDatepicker()
    {
        return dueDateFromDatepicker.getText();
    }

    @Step("Get from datepicker - 'Imposed Date To'")
    public String getImposedDateToDatepicker()
    {
        return imposeDateToDatepicker.getText();
    }

    @Step("Get from datepicker - 'Imposed Date From'")
    public String getImposedDateFromDatepicker()
    {
        return imposeDateFromDatepicker.getText();
    }
}
