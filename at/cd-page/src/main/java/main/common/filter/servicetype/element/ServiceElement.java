package main.common.filter.servicetype.element;

import com.frameworkium.core.ui.annotations.Visible;
import main.common.filter.servicetype.base.ServiceTable;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.CheckBox;
import typifiedelement.WebElement;

@Name("Service Rows")
@FindBy(css = "md-list-item")
public class ServiceElement extends HtmlElement
{
    @Visible
    @Name("Service Name")
    @FindBy(css = "div.service-name")
    private WebElement serviceName;

    @Visible
    @Name("Checkbox")
    @FindBy(css = "div.checkbox md-checkbox")
    private CheckBox checkBox;

    @Visible
    @Name("Parent Service Table")
    @FindBy(xpath = "./ancestor::md-list")
    private ServiceTable serviceTable;

    @Step("Get Service Name")
    public String getServiceName()
    {
        return serviceName.getText().trim();
    }

    @Step("Select Checkbox")
    public ServiceElement selectCheckbox()
    {
        checkBox.select();
        return this;
    }

    @Step("Deselect Checkbox")
    public ServiceElement deselectCheckbox()
    {
        checkBox.deselect();
        return this;
    }

    @Step("Is Checkbox selected")
    public boolean isCheckboxSelected()
    {
        return checkBox.isSelected();
    }

    @Step("Get parent service table")
    public ServiceTable getParentServiceTable()
    {
        return serviceTable;
    }
}
