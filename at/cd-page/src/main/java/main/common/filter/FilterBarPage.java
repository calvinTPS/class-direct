package main.common.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.ClearableTextBox;
import typifiedelement.Dropdown;
import typifiedelement.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class FilterBarPage extends BasePage<FilterBarPage> {
    @Visible
    @Name("Filter Button")
    @FindBy(css = ".filter .filter-title")
    private WebElement filterButton;

    @Name("Search DropDown")
    @FindBy(css = ".search-column md-select-value")
    private Dropdown searchDropDown;

    @Name("Search Text Box")
    @FindBy(css = ".search-column .md-input")
    private ClearableTextBox searchTextBox;

    @Name("Sort By DropDown")
    @FindBy(css = ".sorting-column md-select-value")
    private Dropdown sortByDropDown;

    @Name("Filter Icon")
    @FindBy(css = ".filter .filter-header icon[data-name='filters']")
    private List<WebElement> filterIcon;

    @Name("Filter Bar")
    @FindBy(css = "div.filter-search-container")
    private WebElement filterBar;

    @Step("Click on Filter Assets/Codicils button")
    public <T extends BasePage<T>> T clickFilterButton(Class<T> page) {
        waitForFilterBarToBeEnabled();
        filterButton.click();
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        return PageFactory.newInstance(page);
    }

    @Step("Get Search Option List")
    public List<String> getSearchOptionList() {
        return searchDropDown.getOptionList();
    }

    @Step("Close Search Option List")
    public FilterBarPage closeSearchOptionList() {
        searchDropDown.closeOptionList();
        return this;
    }

    @Step("Select Search DropDown - \"{0}\"")
    public FilterBarPage selectSearchDropDown(String option) {
        searchDropDown.selectByText(option);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Selected Search Option")
    public String getSelectedSearchOption() {
        return searchDropDown.getSelectedOption();
    }

    @Step("Get Helper Text of Search Text Box")
    public String getSearchHelperText() {
        return searchTextBox.getAttribute("placeholder");
    }

    @Step("Set Search Text Box - \"{0}\"")
    public FilterBarPage setSearchTextBox(String text) {
        waitForFilterBarToBeEnabled();
        searchTextBox.clear();
        searchTextBox.sendKeys(text);
        waitForFilterBarToBeEnabled();
        return this;
    }

    //This is a special search delay needed to reproduce a bug race condition when search box resets its search text
    @Step("Set Search Text Box - \"{0}\"")
    public FilterBarPage setSearchTextBox(String text, long duration, TimeUnit timeUnit) {
        searchTextBox.clear();
        searchTextBox.sendKeys(text);
        try {
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(duration, timeUnit));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Step("Click Clear button in Search Text Box")
    public FilterBarPage clickClearButtonInSearchTextBox() {
        searchTextBox.clickClear();
        waitForFilterBarToBeEnabled();
        return this;
    }

    @Step("Get Sort By Option List")
    public List<String> getSortByOptionList() {
        return sortByDropDown.getOptionList();
    }

    @Step("Close SortBy Option List")
    public FilterBarPage closeSortByOptionList() {
        sortByDropDown.closeOptionList();
        return this;
    }

    @Step("Select SortBy DropDown - \"{0}\"")
    public FilterBarPage selectSortByDropDown(String option) {
        sortByDropDown.selectByText(option);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Selected SortBy")
    public String getSelectedSortByOption() {
        return sortByDropDown.getSelectedOption();
    }

    @Step("Get Reference To Page Object - \"{0}\"")
    public <T extends BasePage<T>> T getPageReference(Class<T> page) {
        return PageFactory.newInstance(page);
    }

    @Step("Get Text of Search Text Box")
    public String getSearchText() {
        return searchTextBox.getText().trim();
    }

    @Step("Is Filter Icon Displayed")
    public boolean isFilterIconDisplayed() {
        return filterIcon.size() > 0;
    }

    @Step("Wait for filter bar to be enabled")
    private void waitForFilterBarToBeEnabled()
    {
        wait.until(ExpectedConditions.and(ExtraExpectedConditions.invisibilityOfSpinner(), webDriver -> filterBar.isEnabled()));
    }
}
