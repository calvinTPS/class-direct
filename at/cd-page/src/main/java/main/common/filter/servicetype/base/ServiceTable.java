package main.common.filter.servicetype.base;

import com.frameworkium.core.ui.annotations.Visible;
import main.common.filter.servicetype.element.ServiceElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@Name("Service Table per Product")
@FindBy(css = "md-list")
public class ServiceTable extends HtmlElement
{
    @Visible
    @Name("Product Name")
    @FindBy(css = "md-card>div:nth-child(2)")
    private WebElement productName;

    @Name("Service Rows")
    private List<ServiceElement> serviceElements;

    @Step("Get Services")
    public List<ServiceElement> getServices()
    {
        return serviceElements;
    }

    @Step("Get service count")
    public long getServiceCount(){
        return this.expandServiceTable()
                .getServices()
                .size();
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }

    @Step("Expand service table")
    public ServiceTable expandServiceTable()
    {
        if(!isServiceTableExpanded())
        {
            productName.click();
        }
        return this;
    }

    @Step("Is service table expanded")
    private boolean isServiceTableExpanded(){
        return productName.getAttribute("class").contains("active");
    }
}
