package main.common.filter.assettype.base;

import main.common.filter.assettype.element.AssetType;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

public class BaseTable extends HtmlElement
{
    private List<AssetType> assetType;

    @Step("Get AssetType")
    public List<AssetType> getAssetType()
    {
        return assetType;
    }
}
