package main.common.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;

public class FilterJobsPage extends BasePage<FilterJobsPage>
{
    @Visible
    @Name("Submit button")
    @FindBy(className = "modal-submit-button")
    private WebElement submitButton;

    @Visible
    @Name("Clear filters button")
    @FindBy(className = "modal-clear-button")
    private WebElement clearFiltersButton;

    @Name("Last Visit Date From date picker")
    @FindBy(css = "md-datepicker[data-name='dateMin']")
    private Datepicker lastVisitDateFromDatepicker;

    @Name("Last Visit Date To date picker")
    @FindBy(css = "md-datepicker[data-name='dateMax']")
    private Datepicker lastVisitDateToDatepicker;

    @Name("Last Visit Date Header")
    @FindBy(css = "accordion[data-title='cd-last-visit-date'] .accordion-header")
    private WebElement lastVisitDateHeader;

    @Step("Click Submit button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass)
    {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Clear filters button")
    public FilterJobsPage clickClearFiltersButton()
    {
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return this;
    }

    @Step("Set 'Last Visit Date From' date picker - \"{0}\"")
    public FilterJobsPage setLastVisitDateFromDatepicker(String date)
    {
        lastVisitDateFromDatepicker.sendKeys(date);
        return this;
    }

    @Step("Set 'Last Visit Date To' date picker - \"{0}\"")
    public FilterJobsPage setLastVisitDateToDatepicker(String date)
    {
        lastVisitDateToDatepicker.sendKeys(date);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get To Last Visited Date Text")
    public String getToLastVisitedDateText()
    {
        return lastVisitDateToDatepicker.getText().trim();
    }

    @Step("Get From Last Visited Date Text")
    public String getFromLastVisitedDateText()
    {
        return lastVisitDateFromDatepicker.getText().trim();
    }

    @Step("Get Last Visit Date Accordion Header Text")
    public String getLastVisitedDateAccordionHeaderText()
    {
        return lastVisitDateHeader.getText().trim();
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }
}
