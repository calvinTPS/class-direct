package main.common.filter.assettype;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.filter.FilterAssetsPage;
import main.common.filter.assettype.base.BaseTable;
import main.common.filter.assettype.element.AssetType;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class FilterByAssetTypePage extends BasePage<FilterByAssetTypePage>
{
    @Visible
    @Name("Submit Button")
    @FindBy(css = ".asset-type-filter-modal .modal-submit-button")
    private WebElement submitButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".asset-type-filter-modal .modal-cancel-button")
    private WebElement cancelButton;

    @Name("Asset Type Cards")
    private List<AssetType> assetTypeCards;

    @Name("Asset Type Table")
    @FindBy(css = "md-dialog-content div.category")
    private List<AssetTypeTable> assetTypeTable;

    @Step("Get Asset Type Cards")
    public AssetTypeTable getAssetCardsByTableName(String tableName)
    {
        return assetTypeTable.stream()
                .filter(e -> e.findElement(By.cssSelector("div.category-title>div"))
                        .getText()
                        .equals(tableName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected asset type not found."));
    }

    public class AssetTypeTable extends BaseTable
    {
    }

    @Step("Click Submit Button")
    public FilterAssetsPage clickSubmitButton()
    {
        submitButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.name(assetTypeTable.toString())));
        return PageFactory.newInstance(FilterAssetsPage.class);
    }

    @Step("Click Cancel Button")
    public FilterAssetsPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(FilterAssetsPage.class);
    }
}
