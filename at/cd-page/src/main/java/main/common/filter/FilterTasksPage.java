package main.common.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;
import typifiedelement.Datepicker;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class FilterTasksPage extends BasePage<FilterTasksPage>
{
    @Visible
    @Name("Credit status Header")
    @FindBy(css = "accordion[data-title='cd-credit-status'] .accordion-header")
    private WebElement creditStatusHeader;

    @Visible
    @Name("Credit status Header")
    @FindBy(css = "accordion[data-title='cd-due-date'] .accordion-header")
    private WebElement dueDateHeader;

    @Visible
    @Name("Submit button")
    @FindBy(className = "modal-submit-button")
    private WebElement submitButton;

    @Visible
    @Name("Clear filters button")
    @FindBy(className = "modal-clear-button")
    private WebElement clearFiltersButton;

    @Name("Credit Status CheckBox")
    @FindBy(css = "div.accordion-content md-checkbox")
    private List<CheckBox> creditStatusCheckBox;

    @Name("Due Date From date picker")
    @FindBy(css = "div.start-date md-datepicker")
    private Datepicker dueDateFromDatepicker;

    @Name("Due Date To date picker")
    @FindBy(css = "div.end-date md-datepicker")
    private Datepicker dueDateToDatepicker;

    @Step("Click Submit button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass)
    {
        wait.until(webDriver -> submitButton.isEnabled());
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Clear filters button")
    public FilterTasksPage clickClearFiltersButton()
    {
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return this;
    }

    @Step("Set 'Due Date From' date picker")
    public FilterTasksPage setDueDateFromDatepicker(String date)
    {
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        dueDateFromDatepicker.sendKeys(date);
        return this;
    }

    @Step("Set 'Due Date To' date picker - \"{0}\"")
    public FilterTasksPage setDueDateToDatepicker(String date)
    {
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        dueDateToDatepicker.sendKeys(date);
        return this;
    }

    @Step("Select Credit Status By Name - \"{0}\"")
    public FilterTasksPage selectCreditStatusCheckBoxByStatusName(String statusName)
    {
        creditStatusCheckBox.stream()
                .filter(e -> e.findElement(By.xpath("./following-sibling::div"))
                        .getText()
                        .equals(statusName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("This status: %s is not found", statusName)))
                .select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Deselect Credit Status By Name - \"{0}\"")
    public FilterTasksPage deSelectCreditStatusCheckBoxByStatusName(String statusName)
    {
        creditStatusCheckBox.stream()
                .filter(e ->
                        e.findElement(By.xpath("./following-sibling::div"))
                                .getText()
                                .equals(statusName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("This status: %s is not found", statusName)))
                .deselect();
        waitForJavascriptFrameworkToFinish();
        return this;
    }
}
