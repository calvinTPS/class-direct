package main.common.filter.servicetype;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.filter.servicetype.base.ServiceTable;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.Arrays;
import java.util.List;

public class FilterByServiceTypePage extends BasePage<FilterByServiceTypePage>
{
    @Visible
    @Name("Clear All Button")
    @FindBy(css = "md-dialog-content div.title a")
    private WebElement clearAllButton;

    @Visible
    @Name("Close Button")
    @FindBy(css = "[aria-label='Filter survey schedule by'] div.md-toolbar-tools icon")
    private WebElement closeButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[aria-label='Filter survey schedule by'] md-dialog-actions > div button:nth-child(1)")
    private WebElement cancelButton;

    @Visible
    @Name("Submit Button")
    @FindBy(css = "[aria-label='Filter survey schedule by'] md-dialog-actions > div button:nth-child(2)")
    private WebElement submitButton;

    @Name("Service Tables")
    private List<ServiceTable> serviceTables;

    @Step("Click Clear All Button")
    public FilterByServiceTypePage clickClearAllButton()
    {
        clearAllButton.click();
        return this;
    }

    @Step("Click Close Button")
    public <T extends BasePage<T>> T clickCloseButton(Class<T> page)
    {
        closeButton.click();
        return PageFactory.newInstance(page);
    }

    @Step("Click Cancel Button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> page)
    {
        cancelButton.click();
        return PageFactory.newInstance(page);
    }

    @Step("Click Submit Button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> page)
    {
        submitButton.click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(Arrays.asList(cancelButton, submitButton)));
        return PageFactory.newInstance(page);
    }

    @Step("Get Service Tables")
    public List<ServiceTable> getServiceTables()
    {
        return serviceTables;
    }
}
