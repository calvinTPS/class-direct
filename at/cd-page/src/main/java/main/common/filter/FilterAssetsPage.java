package main.common.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.common.filter.assettype.FilterByAssetTypePage;
import main.common.filter.flag.FilterByFlagPage;
import main.common.filter.servicetype.FilterByServiceTypePage;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;
import typifiedelement.Datepicker;
import typifiedelement.RadioButton;
import typifiedelement.WebElement;

import java.util.List;
import java.util.stream.Collectors;

import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;

public class FilterAssetsPage extends BasePage<FilterAssetsPage>
{

    @Name("Filter Asset Side Bar")
    @FindBy(css = ".filter .filter-title")
    private WebElement filterAssetSideBar;

    @Visible
    @Name("All Vessels RadioButton")
    @FindBy(css = "md-radio-button[value='all']")
    private RadioButton allVesselsRadioButton;

    @Visible
    @Name("My Favourites Button")
    @FindBy(css = "md-radio-button[value='favourites']")
    private RadioButton myFavouritesRadioButton;

    @Name("From Date Of Build Text Input")
    @FindBy(css = ".filter date-range .date-from")
    private Datepicker fromDateOfBuildButton;

    @Name("To Date Of Build Text Input")
    @FindBy(css = ".filter date-range .date-to")
    private Datepicker toDateOfBuildButton;

    @Name("Flag Button")
    @FindBy(css = ".flag-filter-container .md-button")
    private WebElement flagButton;

    @Name("Selected Flags")
    @FindBy(css = "div.flag-filter-container div.tag:not(.show-more-less-button)")
    private List<WebElement> selectedFlags;

    @Name("Asset Type Button")
    @FindBy(css = ".asset-type-filter-container .md-button")
    private WebElement assetTypeButton;

    @Name("Codicil type > All Checkbox")
    @FindBy(className = "checkbox-all")
    private CheckBox codicilTypeAllCheckbox;

    @Name("Codicil type > Conditions of Class Checkbox")
    @FindBy(className = "checkbox-coc")
    private CheckBox codicilTypeConditionsOfClassCheckbox;

    @Name("Codicil type > Actionable Items Checkbox")
    @FindBy(className = "checkbox-ai")
    private CheckBox codicilTypeActionableItemsCheckbox;

    @Name("Class Status CheckBox")
    @FindBy(css = ".class-status-container md-checkbox")
    private List<CheckBox> classStatusCheckBox;

    @Name("Minimum Gross Tonnage")
    @FindBy(css = "input[name='grossMin']")
    private WebElement minGrossTonnage;

    @Name("Maximum Gross Tonnage")
    @FindBy(css = "input[name='grossMax']")
    private WebElement maxGrossTonnage;

    @Name("Service type button")
    @FindBy(css = "service-type button")
    private WebElement serviceTypeButton;

    @Name("Submit Button")
    @FindBy(className = "modal-submit-button")
    private WebElement submitButton;

    @Name("Clear Filters Button")
    @FindBy(className = "modal-clear-button")
    private WebElement clearFiltersButton;

    @Name("Asset Information header")
    @FindBy(css = "accordion[data-title='cd-sidebar-asset-info'] .accordion-header")
    private WebElement assetInformationHeader;

    @Name("Class Status Header")
    @FindBy(css = "accordion[data-title='cd-class-status'] .accordion-header")
    private WebElement classStatusHeader;

    @Name("Note and action Type Header")
    @FindBy(css = "accordion[data-title='cd-note-and-action-type'] .accordion-header")
    private WebElement noteAndActionTypeHeader;

    @Name("Surveys Header")
    @FindBy(css = "accordion[data-title='cd-surveys'] .accordion-header")
    private WebElement surveysHeader;

    @Step("Set From Date Of Build")
    public FilterAssetsPage setFromDateOfBuild(String date)
    {
        fromDateOfBuildButton.clear();
        fromDateOfBuildButton.sendKeys(date);
        fromDateOfBuildButton.sendKeys(Keys.TAB);
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Set To date Of Build")
    public FilterAssetsPage setToDateOfBuild(String date)
    {
        toDateOfBuildButton.clear();
        toDateOfBuildButton.sendKeys(date);
        toDateOfBuildButton.sendKeys(Keys.TAB);
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Get From Date Of Build")
    public String getFromDateOfBuild()
    {
        return fromDateOfBuildButton.getSelectedDate();
    }

    @Step("Get To Date Of Build")
    public String getToDateOfBuild()
    {
        return toDateOfBuildButton.getSelectedDate();
    }

    @Step("Click All Vessels")
    public FilterAssetsPage selectAllVesselsRadioButton()
    {
        allVesselsRadioButton.select();
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Click My Favourites")
    public FilterAssetsPage selectMyFavouritesRadioButton()
    {
        myFavouritesRadioButton.select();
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Click Flag Button")
    public FilterByFlagPage clickFlagButton()
    {
        flagButton.click();
        return PageFactory.newInstance(FilterByFlagPage.class);
    }

    @Step("Click Asset Type Button")
    public FilterByAssetTypePage clickAssetTypeButton()
    {
        assetTypeButton.click();
        return PageFactory.newInstance(FilterByAssetTypePage.class);
    }

    @Step("Check if Codicil type > All Checkbox is displayed")
    public boolean isCodicilTypeAllCheckboxDisplayed()
    {
        return codicilTypeAllCheckbox.isDisplayed();
    }

    @Step("Check if Codicil type > Conditions of Class Checkbox is displayed")
    public boolean isCodicilTypeConditionsOfClassCheckboxDisplayed()
    {
        return codicilTypeConditionsOfClassCheckbox.isDisplayed();
    }

    @Step("Check if Codicil type > Actionable Items is displayed")
    public boolean isCodicilTypeActionableItemsCheckboxDisplayed()
    {
        return codicilTypeActionableItemsCheckbox.isDisplayed();
    }

    @Step("Check if Codicil type > All Checkbox is selected")
    public boolean isCodicilTypeAllCheckboxSelected()
    {
        return codicilTypeAllCheckbox.isSelected();
    }

    @Step("Check if Codicil type > Conditions of Class Checkbox is selected")
    public boolean isCodicilTypeConditionsOfClassCheckboxSelected()
    {
        return codicilTypeConditionsOfClassCheckbox.isSelected();
    }

    @Step("Check if Codicil type > Actionable Items is selected")
    public boolean isCodicilTypeActionableItemsCheckboxSelected()
    {
        return codicilTypeActionableItemsCheckbox.isSelected();
    }

    @Step("Select Class Status By Name - \"{0}\"")
    public FilterAssetsPage selectClassStatusCheckBoxByStatusName(String statusName)
    {
        classStatusCheckBox.stream()
                .filter(e -> e.getAttribute("aria-label").trim()
                        .equals(statusName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("This status: %s is not found", statusName)))
                .select();
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("De-Select Class Status By Name - \"{0}\"")
    public FilterAssetsPage deSelectClassStatusCheckBoxByStatusName(String statusName)
    {
        classStatusCheckBox.stream()
                .filter(e -> e.getAttribute("aria-label").trim()
                        .equals(statusName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("This status: %s is not found", statusName)))
                .deselect();
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Get Class Status checkbox - \"{0}\"")
    public List<CheckBox> getClassStatusCheckBox()
    {
        return classStatusCheckBox;
    }

    @Step("Set min Gross Tonnage - \"{0}\"")
    public FilterAssetsPage setMinGrossTonnage(String tons)
    {
        minGrossTonnage.clear();
        minGrossTonnage.sendKeys(tons);
        minGrossTonnage.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set max Gross Tonnage - \"{0}\"")
    public FilterAssetsPage setMaxGrossTonnage(String tons)
    {
        maxGrossTonnage.clear();
        maxGrossTonnage.sendKeys(tons);
        minGrossTonnage.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get min Gross Tonnage")
    public String getMinGrossTonnage()
    {
        return minGrossTonnage.getText().trim();
    }

    @Step("Get max Gross Tonnage")
    public String getMaxGrossTonnage()
    {
        return maxGrossTonnage.getText().trim();
    }

    @Step("Check if gross tonnage is displayed")
    public boolean isGrossTonnageFieldDisplayed()
    {
        return (maxGrossTonnage.isDisplayed()) && (minGrossTonnage.isDisplayed());
    }

    @Step("Check if All Vessels radio button is selected")
    public boolean isAllVesselsRadioButtonSelected()
    {
        return allVesselsRadioButton.isSelected();
    }

    @Step("Check if My Favourites radio button is selected")
    public boolean isMyFavouritesRadioButtonSelected()
    {
        return myFavouritesRadioButton.isSelected();
    }

    @Step("Click Submit Button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass)
    {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        wait.until(webDriver -> filterAssetSideBar.isEnabled());
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Clear Filter Button")
    public FilterBarPage clickClearFiltersButton()
    {
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Remove selected flags")
    public FilterAssetsPage removeSelectedFlags()
    {
        selectedFlags.parallelStream()
                .forEach(e ->
                        {
                            // ignore the responsive doubles that mirrors the destruction of the active element
                            try
                            {
                                e.findElement(By.cssSelector("div.tag-remove-container")).click();
                            }
                            catch (StaleElementReferenceException | ElementNotVisibleException | NoSuchElementException ignore)
                            {
                                //ignore
                            }
                            waitForJavascriptFrameworkToFinish();
                        }
                );
        return this;
    }

    @Step("Get selected flags")
    public List<WebElement> getSelectedFlags()
    {
        return selectedFlags;
    }

    @Step("Select Condition of class")
    public FilterAssetsPage selectCodicilTypeConditionOfClass()
    {
        codicilTypeConditionsOfClassCheckbox.select();
        return this;
    }

    @Step("Select Actionable Item")
    public FilterAssetsPage selectCodicilTypeActionableItem()
    {
        codicilTypeActionableItemsCheckbox.select();
        return this;
    }

    @Step("Select All Codicil Type")
    public FilterAssetsPage selectCodicilTypeAll(){
        codicilTypeAllCheckbox.select();
        return this;
    }

    @Step("Deselect All Codicil type")
    public FilterAssetsPage deselectCodicilTypeAll()
    {
        codicilTypeAllCheckbox.deselect();
        return this;
    }

    @Step("Deselect Condition of class")
    public FilterAssetsPage deselectConditionOfClass()
    {
        codicilTypeConditionsOfClassCheckbox.deselect();
        return this;
    }

    @Step("Deselect Codicil Type ActionableItem")
    public FilterAssetsPage deselectCodicilTypeActionableItem()
    {
        codicilTypeActionableItemsCheckbox.deselect();
        return this;
    }

    @Step("Click service type button")
    public FilterByServiceTypePage clickServiceTypeButton()
    {
        serviceTypeButton.click();
        return PageFactory.newInstance(FilterByServiceTypePage.class);
    }

    @Step("Get class status list")
    public List<String> getClassStatusList()
    {
        return classStatusCheckBox.stream()
                .map(e -> e.getAttribute("aria-label").trim())
                .collect(Collectors.toList());
    }
}
