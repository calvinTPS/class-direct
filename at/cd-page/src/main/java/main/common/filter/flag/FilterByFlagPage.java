package main.common.filter.flag;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.filter.FilterAssetsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.MultiTagTextBox;

public class FilterByFlagPage extends BasePage<FilterByFlagPage> {

    @Visible
    @Name("Search Flag Text Box")
    @FindBy(css = "[aria-label='Select box']")
    private MultiTagTextBox searchFlagTextBox;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[ng-click='vm.cancel()']")
    private WebElement cancelButton;

    @Visible
    @Name("Submit Button")
    @FindBy(css = "[ng-click='vm.submit()']")
    private WebElement submitButton;

    @Step("Set Search Flag Text Box - \"{0}\"")
    public FilterByFlagPage setSearchFlagTextBox(String flag) {
        searchFlagTextBox.sendKeys(flag);
        waitForJavascriptFrameworkToFinish();
        searchFlagTextBox.selectOptionByVisibleText(flag);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Search Flag Text Box")
    public MultiTagTextBox getSearchFlagTextBox() {
        return searchFlagTextBox;
    }

    @Step("Click Submit Button")
    public FilterAssetsPage clickSubmitButton() {
        submitButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.name(searchFlagTextBox.toString())));
        return PageFactory.newInstance(FilterAssetsPage.class);
    }
}
