package main.common;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class ShowingXOfYPage extends BasePage<ShowingXOfYPage>
{
    @Name("Shown Asset Count")
    @FindBy(css = "visible-results strong:nth-child(1)")
    private WebElement shownAssetCount;

    @Name("Total Asset Count")
    @FindBy(css = "visible-results strong:nth-child(2)")
    private WebElement totalAssetCount;

    /**
     * Returns the shown asset (X) count from the "Showing X of Y" element
     * @return the shown asset (X) or 0 when shownAssetCount element is not found
     */
    @Step("Get Shown Asset Count")
    public int getShown()
    {
        try
        {
            return Integer.parseInt(shownAssetCount.getText().trim());
        }
        catch (NoSuchElementException e)
        {
            return 0;
        }
    }

    /**
     * Returns the total assets (Y) count from the "Showing X of Y" element
     * @return the total assets (Y) or 0 when totalAssetCount element is not found
     */
    @Step("Get Total Asset Count")
    public int getTotal()
    {
        try
        {
            return Integer.parseInt(totalAssetCount.getText().trim());
        }
        catch (NoSuchElementException e)
        {
            return 0;
        }
    }
}
