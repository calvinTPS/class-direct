package main.common.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.AppHelper;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Asset Cards")
@FindBy(css = "assets mini-asset-card .card")
public class MiniAssetCard extends HtmlElement
{
    @Visible
    @Name("Asset Emblem")
    @FindBy(css = "div.emblem-wrapper emblem md-icon")
    private WebElement assetEmblem;

    @Visible
    @Name("Asset Name")
    @FindBy(className = "h3")
    private WebElement assetName;

    @Name("Asset IMO number")
    @FindBy(className = "h4")
    private WebElement imoNumber;

    @Step("Get Asset Name")
    public String getAssetName()
    {
        return assetName.getText().trim();
    }

    @Step("Get Asset IMO number")
    public String getImoNumber()
    {
        return imoNumber.getText().trim();
    }

    @Step("Select Asset")
    public MiniAssetCard selectAssetCard()
    {
        if (!isAssetCardSelected())
        {
            AppHelper.scrollToMiddle(this);
            this.click();
        }
        return this;
    }

    @Step("Deselect Asset")
    public MiniAssetCard deselectCheckBox()
    {
        if (isAssetCardSelected())
        {
            AppHelper.scrollToMiddle(this);
            this.click();
        }
        return this;
    }

    @Step("Is asset emblem displayed")
    public Boolean isAssetEmblemDisplayed()
    {
        return assetEmblem.isDisplayed();
    }

    @Step("Is Asset card selected")
    public boolean isAssetCardSelected()
    {
        return this.getAttribute("class").contains("highlighted");
    }
}
