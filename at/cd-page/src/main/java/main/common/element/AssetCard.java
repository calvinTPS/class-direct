package main.common.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.ClassDirect;
import helper.AppHelper;
import helper.HtmlElement;
import helper.PageFactory;
import main.requestsurvey.RequestSurveyPage;
import main.viewasset.AssetHubPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Star;
import typifiedelement.WebElement;

import java.text.ParseException;
import java.util.Date;

@Name("Asset Cards")
@FindBy(css = "asset-card")
public class AssetCard extends HtmlElement
{
    @Visible
    @Name("Asset Emblem")
    @FindBy(css = "emblem")
    private WebElement assetEmblem;

    @Visible
    @Name("Asset Name")
    @FindBy(css = ".card-name")
    private WebElement assetName;

    @Visible
    @Name("Imo Number")
    @FindBy(css = ".info-detail.imo")
    private WebElement imoNumber;

    @Visible
    @Name("Asset Type")
    @FindBy(css = ".info-detail.asset-type")
    private WebElement assetType;

    @Visible
    @Name("Date of build")
    @FindBy(css = ".info-detail.build-date")
    private WebElement dateOfBuild;

    @Name("Gross Tonnage")
    @FindBy(css = ".info-detail.gross-tonnage")
    private WebElement grossTonnage;

    @Visible
    @Name("Request Survey button")
    @FindBy(css = ".button-request-survey")
    private WebElement requestSurveyButton;

    @Name("Class Status")
    @FindBy(css = ".info-detail.class-status")
    private WebElement classStatus;

    @Name("Favourite Icon")
    @FindBy(css = "star")
    private Star favouriteIcon;

    @Name("Badge Icon for MMS")
    @FindBy(css = ".badges [data-name='mms']")
    private WebElement mmsBadgeIcon;

    @Name("Badge Icon for LR")
    @FindBy(css = ".badges [data-name='lr']")
    private WebElement lrBadgeIcon;

    @Name("Due Status Icon")
    @FindBy(css = "due-status icon")
    private WebElement dueStatusIcon;

    @Name("Due Status")
    @FindBy(css = "due-status .label")
    private WebElement dueStatus;

    @Visible
    @Name("View Asset Button")
    @FindBy(css = ".button-view-asset")
    private WebElement viewAssetButton;

    @Name("Flag Name")
    @FindBy(css = ".flag")
    private WebElement flagName;

    @Step("Get Date Of Build")
    public Date getDateOfBuild()
    {
        Date date;
        String dateOfBuild = this.dateOfBuild.getText();
        try
        {
            date = ClassDirect.FRONTEND_TIME_FORMAT.parse(dateOfBuild);
        }
        catch (ParseException e)
        {
            date = null;
        }
        return date;
    }

    @Step("Get Asset Name")
    public String getAssetName()
    {
        return assetName.getText();
    }

    @Step("Get IMO Number")
    public String getImoNumber()
    {
        return imoNumber.getText();
    }

    @Step("Get Asset Type")
    public String getAssetType()
    {
        return assetType.getText().trim();
    }

    @Step("Get Flag Name")
    public String getFlagName()
    {
        return flagName.getText().trim();
    }

    @Step("Click request survey button")
    public RequestSurveyPage clickRequestSurveyButton()
    {
        AppHelper.scrollToMiddle(requestSurveyButton);
        requestSurveyButton.click();
        waitForSpinnerToFinish();
        return PageFactory.newInstance(RequestSurveyPage.class);
    }

    @Step("Is IMO number displayed")
    public boolean isImoNumberDisplayed()
    {
        return imoNumber.isDisplayed();
    }

    @Step("Is Asset Type Displayed")
    public boolean isAssetTypeDisplayed()
    {
        return assetType.isDisplayed();
    }

    @Step("Is Build Date Displayed")
    public boolean isBuildDateDisplayed()
    {
        return dateOfBuild.isDisplayed();
    }

    @Step("Click view asset button")
    public AssetHubPage clickViewAssetButton()
    {
        AppHelper.scrollToMiddle(viewAssetButton);
        viewAssetButton.click();
        waitForSpinnerToFinish();
        return PageFactory.newInstance(AssetHubPage.class);
    }

    @Step("Get Class Status")
    public String getClassStatus()
    {
        return classStatus.getText();
    }

    @Step("Get Gross Tonnage")
    public String getGrossTonnage()
    {
        return grossTonnage.getText();
    }

    @Step("Is Asset Card Marked as Favourite")
    public boolean isFavourite()
    {
        return favouriteIcon.isFavourited();
    }

    @Step("Select Favourite")
    public AssetCard selectFavourite()
    {
        favouriteIcon.select();
        return this;
    }

    @Step("Deselect Favourite")
    public AssetCard deselectFavourite()
    {
        favouriteIcon.deselect();
        return this;
    }

    @Step("Get Due Status")
    public String getDueStatus()
    {
        return dueStatus.getText().trim();
    }

    @Step("Is Due Status Icon Displayed")
    public boolean isDueStatusIconDisplayed()
    {
        return dueStatusIcon.isDisplayed();
    }

    @Step("Is asset emblem displayed")
    public boolean isAssetEmblemDisplayed()
    {
        return assetEmblem.isDisplayed();
    }

    @Step("Is MMS badge icon displayed")
    public boolean isMmsBadgeIconDisplayed()
    {
        return mmsBadgeIcon.isDisplayed();
    }

    @Step("Is LR badge icon displayed")
    public boolean isLrBadgeIconDisplayed()
    {
        return lrBadgeIcon.isDisplayed();
    }

    @Step("Is Request survey button displayed")
    public boolean isRequestSurveyButtonDisplayed()
    {
        return requestSurveyButton.isExists();
    }
}
