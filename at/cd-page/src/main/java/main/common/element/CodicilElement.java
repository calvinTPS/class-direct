package main.common.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.WebElement;

import java.util.List;

@Name("Codicil Element")
@FindBy(css = "md-card a[ui-sref*='codicilId'] md-card-content")
public class CodicilElement extends HtmlElement
{
    @Visible
    @Name("Title")
    @FindBy(css = ".title")
    private WebElement title;

    @Visible
    @Name("Category")
    @FindBy(css = ".category")
    private WebElement category;

    @Name("Imposed date")
    @FindBy(css = ".imposed-date")
    private WebElement imposedDate;

    @Name("Due date")
    @FindBy(css = ".due-date")
    private WebElement dueDate;

    // @Visible - this is visible, but has mobile-view, desktop-view clones
    @Name("Status")
    @FindBy(css = "active-status")
    private List<WebElement> status;

    @Visible
    @Name("Arrow button")
    @FindBy(css = ".action icon")
    private WebElement arrowButton;

    @Step("Get Title")
    public String getTitle()
    {
        return title.getText();
    }

    @Step("Get Category")
    public String getCategory()
    {
        return category.getText();
    }

    @Step("Get Imposed date")
    public String getImposedDate()
    {
        return imposedDate.getText();
    }

    @Step("Get Due date")
    public String getDueDate()
    {
        return dueDate.getText();
    }

    @Step("Get Status")
    public String getStatus()
    {
        return status.stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("No status was found for codicil titled \"%s\"",this.getTitle())))
                .getText();
    }

    @Step("Click Arrow button")
    public <T extends BasePage<T>> T clickArrowButton(Class<T> page)
    {
        arrowButton.click();
        return PageFactory.newInstance(page);
    }

    @Step("Check if Imposed date is displayed")
    public boolean isImposedDateDisplayed()
    {
        return imposedDate.isDisplayed();
    }

    @Step("Check if Due date is displayed")
    public boolean isDueDateDisplayed()
    {
        return dueDate.isDisplayed();
    }
}
