package main.common.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import main.viewasset.codicils.codicilsanddefects.sub.defects.details.DefectDetailsPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(css = ".defect-item")
public class DefectElement extends HtmlElement
{
    @Visible
    @Name("Defect Title")
    @FindBy(css = ".title")
    private WebElement defectTitle;

    @Visible
    @Name("Defect Category")
    @FindBy(css = ".category")
    private WebElement defectCategory;

    @Name("Job number")
    @FindBy(css = ".job-number")
    private WebElement jobNumber;

    @Visible
    @Name("Date Occurred")
    @FindBy(css = ".date-occurred")
    private WebElement dateOccurred;

    //@Visible - Status is missing due to this bug: LRCD-2496
    @Name("Status")
    @FindBy(css = "div[data-show-gt-sm] active-status")
    private WebElement status;

    @Visible
    @Name("Arrow button")
    @FindBy(css = ".action icon")
    private WebElement arrowButton;

    @Step("Get Defect Title")
    public String getDefectTitle()
    {
        return defectTitle.getText();
    }

    @Step("Get Job Number")
    public String getJobNumber()
    {
        return jobNumber.getText();
    }

    @Step("Get Defect Category")
    public String getDefectCategory()
    {
        return defectCategory.getText();
    }

    @Step("Get Date Occurred")
    public String getDateOccurred()
    {
        return dateOccurred.getText();
    }

    @Step("Get Status")
    public String getStatus()
    {
        return status.getText();
    }

    @Step("Click on Arrow button")
    public DefectDetailsPage clickArrowButton()
    {
        arrowButton.click();
        return PageFactory.newInstance(DefectDetailsPage.class);
    }
}
