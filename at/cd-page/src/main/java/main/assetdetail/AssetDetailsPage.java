package main.assetdetail;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.assetdetail.element.*;
import main.assetdetail.sistervessels.element.RegisterBookSisterVesselsCard;
import main.assetdetail.sistervessels.element.TechnicalSisterVesselsCard;
import main.common.AssetHeaderPage;
import main.viewasset.AssetHubPage;
import main.viewasset.export.ExportAssetInformationCard;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class AssetDetailsPage extends BasePage<AssetDetailsPage>
{
    @Visible
    @Name("Export Asset Information card")
    private ExportAssetInformationCard exportAssetInformationCard;

    @Name("Register Book Sister Vessels Card")
    private RegisterBookSisterVesselsCard registerBookSisterVesselsCard;

    @Name("Technical Sister Vessels Card")
    private TechnicalSisterVesselsCard technicalSisterVesselsCard;

    @Name("Registry Information Card")
    private RegistryInformationCard registryInformationCard;

    @Name("Principal Dimensions Card")
    private PrincipalDimensionsCard principalDimensionsCard;

    @Name("Class history, notations and descriptive notes Card")
    private ClassHistoryNotationsAndDescriptiveNotesCard classHistoryNotationsAndDescriptiveNotesCard;

    @Name("Equipment Information Card")
    private EquipmentInformationCard equipmentInformationCard;

    @Name("Registered Owner Card")
    @FindBy(className = "section-registered-owner")
    private OwnershipAndManagementDetailsCard registeredOwnerCard;

    @Name("Group Owner Card")
    @FindBy(className = "section-group-owner")
    private OwnershipAndManagementDetailsCard groupOwnerCard;

    @Name("Operator Card")
    @FindBy(className = "section-operator")
    private OwnershipAndManagementDetailsCard operatorCard;

    @Name("Ship Manager Card")
    @FindBy(className = "section-ship-manager")
    private OwnershipAndManagementDetailsCard shipManagerCard;

    @Name("Technical Manager Card")
    @FindBy(className = "section-technical-manager")
    private OwnershipAndManagementDetailsCard technicalManagerCard;

    @Name("DOC Company Card")
    @FindBy(className = "section-doc-company")
    private OwnershipAndManagementDetailsCard docCompanyCard;

    @Step("Navigate To Asset Hub Page")
    public AssetHubPage navigateToAssetHubPage()
    {
        driver.navigate().back();
        return PageFactory.newInstance(AssetHubPage.class);
    }

    @Step("Get Asset Header Page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }

    @Step("Get Export Asset Information card")
    public ExportAssetInformationCard getExportAssetInformationCard()
    {
        return exportAssetInformationCard;
    }

    @Step("Get Register Book Sister Vessels Card")
    public RegisterBookSisterVesselsCard getRegisterBookSisterVesselsCard()
    {
        return registerBookSisterVesselsCard;
    }

    @Step("Get Technical Sister Vessels Card")
    public TechnicalSisterVesselsCard getTechnicalSisterVesselsCard()
    {
        return technicalSisterVesselsCard;
    }

    @Step("Get Registry Information Card")
    public RegistryInformationCard getRegistryInformationCard()
    {
        return registryInformationCard;
    }

    @Step("Get Principal Dimensions Card")
    public PrincipalDimensionsCard getPrincipalDimensionsCard()
    {
        return principalDimensionsCard;
    }

    @Step("Get 'Class history, notations and descriptive notes' Card")
    public ClassHistoryNotationsAndDescriptiveNotesCard getClassHistoryNotationsAndDescriptiveNotesCard()
    {
        return classHistoryNotationsAndDescriptiveNotesCard;
    }

    @Step("Get Equipment Information Card")
    public EquipmentInformationCard getEquipmentInformationCard()
    {
        return equipmentInformationCard;
    }

    @Step("Get Registered Owner Card")
    public OwnershipAndManagementDetailsCard getRegisteredOwnerCard()
    {
        return registeredOwnerCard;
    }

    @Step("Get Group Owner Card")
    public OwnershipAndManagementDetailsCard getGroupOwnerCard()
    {
        return groupOwnerCard;
    }

    @Step("Get Operator Card")
    public OwnershipAndManagementDetailsCard getOperatorCard()
    {
        return operatorCard;
    }

    @Step("Get Ship Manager Card")
    public OwnershipAndManagementDetailsCard getShipManagerCard()
    {
        return shipManagerCard;
    }

    @Step("Get Technical Manager Card")
    public OwnershipAndManagementDetailsCard getTechnicalManagerCard()
    {
        return technicalManagerCard;
    }

    @Step("Get DOC Company Card")
    public OwnershipAndManagementDetailsCard getDocCompanyCard()
    {
        return docCompanyCard;
    }
}
