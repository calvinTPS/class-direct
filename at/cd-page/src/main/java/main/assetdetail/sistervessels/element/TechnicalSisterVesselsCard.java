package main.assetdetail.sistervessels.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import main.assetdetail.sistervessels.SisterVesselsPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(css = ".page-details div[aria-hidden='false'] div.technical-vessel")
public class TechnicalSisterVesselsCard extends HtmlElement
{
    @Visible
    @Name("Technical Sister Vessels Label")
    @FindBy(css = ".box .label")
    private WebElement technicalSisterVesselsLabel;

    @Name("View All Link")
    @FindBy(css = ".box .view-all")
    private WebElement viewAllLink;

    @Visible
    @Name("Total Technical Sister Vessels ")
    @FindBy(css = ".box .count")
    private WebElement totalTechnicalSisterVessels;

    @Step("Click View All Link")
    public SisterVesselsPage clickViewAllLink()
    {
        viewAllLink.click();
        return PageFactory.newInstance(SisterVesselsPage.class);
    }

    @Step("Get Total Technical Sister Vessels")
    public int getTotalTechnicalSisterVessels()
    {
        return Integer.parseInt(totalTechnicalSisterVessels.getText().trim());
    }
}

