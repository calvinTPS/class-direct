package main.assetdetail.sistervessels;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.assetdetail.AssetDetailsPage;
import main.common.element.AssetCard;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

public class SisterVesselsPage extends BasePage<SisterVesselsPage>
{
    @Visible
    @Name("Back button")
    @FindBy(className = "back-button")
    private WebElement backButton;

    @Visible
    @Name("Title")
    @FindBy(css = ".title .text")
    private WebElement title;

    @Name("Asset Cards")
    private List<AssetCard> assetCards;

    @Step("Get Asset Cards")
    public List<AssetCard> getAssetCards()
    {
        return assetCards;
    }

    @Step("Click Navigate Back Button")
    public AssetDetailsPage navigateToAssetDetailsPage()
    {
        driver.navigate().back();
        return PageFactory.newInstance(AssetDetailsPage.class);
    }

    @Step("Click Back Button")
    public AssetDetailsPage clickBackButton()
    {
        backButton.click();
        return PageFactory.newInstance(AssetDetailsPage.class);
    }

    @Step("Get Title")
    public String getTitle()
    {
        return title.getText().trim();
    }
}
