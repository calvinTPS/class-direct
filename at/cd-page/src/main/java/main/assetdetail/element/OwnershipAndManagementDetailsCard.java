package main.assetdetail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class OwnershipAndManagementDetailsCard extends HtmlElement
{
    @Visible
    @Name("Email Address")
    @FindBy(className = "email-address")
    private WebElement emailAddress;

    @Visible
    @Name("Phone number")
    @FindBy(className = "phone-number")
    private WebElement phoneNumber;

    @Visible
    @Name("Address")
    @FindBy(className = "address")
    private WebElement address;
}
