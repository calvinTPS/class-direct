package main.assetdetail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

@Name("Principal Dimensions")
@FindBy(css = "[class*='section-principal-dimensions']")
public class PrincipalDimensionsCard extends HtmlElement
{
    @Visible
    @Name("Length Overall")
    @FindBy(className = "length-overall")
    private WebElement lengthOverall;

    @Visible
    @Name("Breadth Moulded")
    @FindBy(className = "breadth-moulded")
    private WebElement breadthMoulded;

    @Visible
    @Name("Drought Max")
    @FindBy(className = "drought-max")
    private WebElement droughtMax;

    @Visible
    @Name("Gross Tonnage")
    @FindBy(className = "gross-tonnage")
    private WebElement grossTonnage;

    @Visible
    @Name("Deadweight")
    @FindBy(className = "deadweight")
    private WebElement deadweight;

    @Visible
    @Name("Propulsion")
    @FindBy(className = "propulsion")
    private WebElement propulsion;

    @Visible
    @Name("Length Between Perpendiculars")
    @FindBy(className = "length-between-perpendiculars")
    private WebElement lengthBetweenPerpendiculars;

    @Visible
    @Name("Breadth Extreme")
    @FindBy(className = "breadth-extreme")
    private WebElement breadthExtreme;

    @Visible
    @Name("Depth Moulded")
    @FindBy(className = "depth-moulded")
    private WebElement depthMoulded;

    @Visible
    @Name("Net Tonnage")
    @FindBy(className = "net-tonnage")
    private WebElement netTonnage;

    @Visible
    @Name("Decks")
    @FindBy(className = "decks")
    private WebElement decks;
}
