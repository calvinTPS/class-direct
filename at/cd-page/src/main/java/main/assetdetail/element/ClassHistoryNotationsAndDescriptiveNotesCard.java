package main.assetdetail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

@Name("Class history, notations and descriptive notes")
@FindBy(className = "section-class-history-notations-and-descriptive-notes")
public class ClassHistoryNotationsAndDescriptiveNotesCard extends HtmlElement
{
    @Visible
    @Name("Machinery Notation")
    @FindBy(className = "machinery-notation")
    private WebElement machineryNotation;

    @Visible
    @Name("Hull Notation")
    @FindBy(className = "hull-notation")
    private WebElement hullNotation;

    @Visible
    @Name("Descriptive Notes")
    @FindBy(className = "descriptive-notes")
    private WebElement descriptiveNotes;
}
