package main.assetdetail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

@Name("Equipment Information")
@FindBy(css = "[class*='section-equipment-information']")
public class EquipmentInformationCard extends HtmlElement
{
    @Visible
    @Name("Cable length")
    @FindBy(className = "cable-length")
    private WebElement cableLength;

    @Visible
    @Name("Cable Diameter")
    @FindBy(className = "cable-diameter")
    private WebElement cableDiameter;

    @Visible
    @Name("Cable Grade")
    @FindBy(className = "cable-grade")
    private WebElement cableGrade;

    @Visible
    @Name("Equipment Letter")
    @FindBy(className = "equipment-letter")
    private WebElement equipmentLetter;
}
