package main.assetdetail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.text.ParseException;
import java.util.Date;

import static constant.ClassDirect.ASSET_DETAILS_DATE_FORMAT;

@Name("Registry Information")
@FindBy(className = "section-registry-information")
public class RegistryInformationCard extends HtmlElement
{
    @Visible
    @Name("Port of Registry")
    @FindBy(className = "port-of-registry")
    private WebElement portOfRegistry;

    @Visible
    @Name("Call Sign")
    @FindBy(className = "call-sign")
    private WebElement callSign;

    @Visible
    @Name("Official Number")
    @FindBy(className = "official-number")
    private WebElement officialNumber;

    @Visible
    @Name("Ship Type Details")
    @FindBy(className = "asset-type-details")
    private WebElement shipTypeDetails;

    @Visible
    @Name("Former Asset Name")
    @FindBy(className = "former-asset-names")
    private WebElement formerAssetName;

    @Visible
    @Name("Yard")
    @FindBy(className = "yard")
    private WebElement yard;

    @Visible
    @Name("Yard Number")
    @FindBy(className = "yard-number")
    private WebElement yardNumber;

    @Visible
    @Name("Keel laying date")
    @FindBy(className = "keel-laying-date")
    private WebElement keelLayingDate;

    @Visible
    @Name("Date of build")
    @FindBy(className = "date-of-build")
    private WebElement dateOfBuild;

    @Visible
    @Name("Year of build")
    @FindBy(className = "year-of-build")
    private WebElement yearOfBuild;

    @Visible
    @Name("Country of build")
    @FindBy(className = "country-of-build")
    private WebElement countryOfBuild;

    @Visible
    @Name("Flag")
    @FindBy(className = "flag")
    private WebElement flag;

    @Visible
    @Name("Builder")
    @FindBy(className = "builder")
    private WebElement builder;

    @Visible
    @Name("MMSI Number")
    @FindBy(className = "mmsi-number")
    private WebElement mmsiNumber;

    @Step("Get Port of Registry")
    public String getPortOfRegistry()
    {
        return portOfRegistry.getText().trim();
    }

    @Step("Get Call Sign")
    public String getCallSign()
    {
        return callSign.getText().trim();
    }

    @Step("Get Official Number")
    public String getOfficialNumber()
    {
        return officialNumber.getText().trim();
    }

    @Step("Get Ship type details")
    public String getShipTypeDetails()
    {
        return shipTypeDetails.getText().trim();
    }

    @Step("Get Former Asset Name")
    public String getFormerAssetName()
    {
        return formerAssetName.getText().trim();
    }

    @Step("Get Yard")
    public String getYard()
    {
        return yard.getText().trim();
    }

    @Step("Get Yard Number")
    public String getYardNumber()
    {
        return yardNumber.getText().trim();
    }

    @Step("Get Keel laying date")
    public String getKeelLayingDate()
    {
        return keelLayingDate.getText().trim();
    }

    @Step("Get Date of build")
    public Date getDateOfBuild()
    {
        try
        {
            return ASSET_DETAILS_DATE_FORMAT.parse(dateOfBuild.getText().trim());
        }
        catch(ParseException ex)
        {
            return null;
        }
    }

    @Step("Get Year of build")
    public String getYearOfBuild()
    {
        return yearOfBuild.getText().trim();
    }

    @Step("Get Country of build")
    public String getCountryOfBuild()
    {
        return countryOfBuild.getText().trim();
    }

    @Step("Get Flag")
    public String getFlag()
    {
        return flag.getText().trim();
    }

    @Step("Get Builder")
    public String getBuilder()
    {
        return builder.getText().trim();
    }

    @Step("Get MMSI Number")
    public String getMmsiNumber()
    {
        return mmsiNumber.getText().trim();
    }
}
