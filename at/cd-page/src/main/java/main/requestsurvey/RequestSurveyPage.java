package main.requestsurvey;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.requestsurvey.sub.SelectServicesSubPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class RequestSurveyPage extends BasePage<RequestSurveyPage>
{
    @Visible
    @Name("Step 1 - Select services")
    @FindBy(css = "ul[class='steps-indicator steps-3'] li:nth-child(1)")
    private WebElement selectServicesTab;

    @Visible
    @Name("Step 2 - Select location and dates")
    @FindBy(css = "ul[class='steps-indicator steps-3'] li:nth-child(2)")
    private WebElement selectLocationAndDatesTab;

    @Visible
    @Name("Step 3 - Confirm")
    @FindBy(css = "ul[class='steps-indicator steps-3'] li:nth-child(3)")
    private WebElement confirmTab;

    @Step("Get Select Services page")
    public SelectServicesSubPage getSelectServicesSubPage()
    {
        return PageFactory.newInstance(SelectServicesSubPage.class);
    }

    @Step("Check if Select Services tab is highlighted")
    public Boolean isSelectServicesTabHighlighted()
    {
        return selectServicesTab.getAttribute("class").contains("current");
    }
}
