package main.requestsurvey.services;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

public class BaseServiceTable extends HtmlElement
{
    @FindBy(xpath = "./descendant::md-card[@role='button']")
    private List<RowElement> rowElement;

    @Step("Get rows")
    public List<RowElement> getRows()
    {
        return rowElement;
    }
}
