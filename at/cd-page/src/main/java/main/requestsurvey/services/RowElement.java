package main.requestsurvey.services;

import helper.AppHelper;
import helper.HtmlElement;
import org.openqa.selenium.By;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;


import java.util.List;

public class RowElement extends HtmlElement
{
    @Name("Service Elements")
    @FindBy(xpath = "./following-sibling::md-card/md-list-item")
    private List<ServiceElement> serviceElements;

    @Name("+/- Icon")
    @FindBy(css = "[class*='icon-toggle']")
    private WebElement plusOrMinusIcon;

    @Name("Product Name")
    @FindBy(css = "div.icon-toggle~div")
    private WebElement productName;

    @Step("Click on expand icon")
    public RowElement clickPlusOrMinusIcon()
    {
        AppHelper.scrollToMiddle(plusOrMinusIcon);
        plusOrMinusIcon.click();
        return this;
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }

    @Step("Check if row element is expanded")
    public boolean isRowExpanded()
    {
        return this.findElement(By.cssSelector("div:nth-child(2)")).getAttribute
                ("class").contains("active");
    }

    @Step("Get Services Rows")
    public List<ServiceElement> getServiceRows()
    {
        waitForSpinnerToFinish();
        return serviceElements;
    }

}
