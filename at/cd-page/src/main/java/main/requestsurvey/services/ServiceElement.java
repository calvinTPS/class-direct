package main.requestsurvey.services;

import com.frameworkium.core.ui.annotations.Visible;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.CheckBox;

public class ServiceElement extends HtmlElement {

    @Visible
    @Name("Service Name")
    @FindBy(css = ".service-name")
    private WebElement serviceName;

    @Visible
    @Name("Due Date")
    @FindBy(css = ".due-date")
    private WebElement dueDate;

    @Visible
    @Name("Assigned Date")
    @FindBy(css = ".assigned-date")
    private WebElement assignedDate;

    @Visible
    @Name("Range Date From")
    @FindBy(css = ".dateFrom")
    private WebElement rangeDateFrom;

    @Visible
    @Name("Range Date To")
    @FindBy(css = ".dateTo")
    private WebElement rangeDateTo;

    @Visible
    @Name("Due Status")
    @FindBy(css = "due-status")
    private WebElement dueStatus;

    @Name("Service Checkbox")
    @FindBy(css = "md-checkbox")
    private CheckBox serviceCheckbox;

    @Step("Get service name")
    public String getServiceName() {
        return serviceName.getText();

    }

    @Step("Get due date")
    public String getDueDate() {
        return dueDate.getText();

    }

    @Step("Get assigned date")
    public String getAssignedDate() {
        return assignedDate.getText();

    }

    @Step("Get range date from")
    public String getRangeDateFrom() {
        return rangeDateFrom.getText().substring(0, 11);

    }

    @Step("Get range date to")
    public String getRangeDateTo() {
        return rangeDateTo.getText();

    }

    @Step("Get due status")
    public String getDueStatus() {
        return dueDate.getText().trim();

    }

    @Step("Select check box")
    public ServiceElement selectCheckBox() {
        serviceCheckbox.select();
        return this;
    }

    @Step("De-select check box")
    public ServiceElement deselectCheckBox() {
        serviceCheckbox.deselect();
        return this;
    }
}
