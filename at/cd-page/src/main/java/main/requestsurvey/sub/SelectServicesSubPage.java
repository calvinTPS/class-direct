package main.requestsurvey.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.requestsurvey.services.BaseServiceTable;
import main.vessellist.VesselListPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class SelectServicesSubPage extends BasePage<SelectServicesSubPage>
{
    @Visible
    @Name("Cancel Button")
    @FindBy(css = "section:nth-child(1) [aria-label='Cancel']")
    private WebElement cancelButton;

    @Visible
    @Name("Continue Button")
    @FindBy(css = "section:nth-child(1) [aria-label='Continue']")
    private WebElement continueButton;

    @Visible
    @Name("Service Table")
    @FindBy(css = "survey-request-form section:nth-child(1) bucket")
    private BaseServiceTable serviceTable;

    @Step("Click Cancel Button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> classObjectPage)
    {
        cancelButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(classObjectPage);
    }

    @Step("Click Continue Button")
    public SelectLocationAndDatesSubPage clickContinueButton()
    {
        continueButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(SelectLocationAndDatesSubPage.class);
    }

    @Step("Get Service Table")
    public BaseServiceTable getServiceTable()
    {
        return serviceTable;
    }

    @Step("Check if Continue button is disabled")
    public Boolean isContinueButtonDisabled()
    {
        String isDisabled = continueButton.getAttribute("disabled");
        if (isDisabled == null)
            return false;

        return continueButton.getAttribute("disabled").contains("disabled");
    }

}
