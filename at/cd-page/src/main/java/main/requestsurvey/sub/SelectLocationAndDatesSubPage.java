package main.requestsurvey.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.*;

import java.time.LocalDate;

public class SelectLocationAndDatesSubPage extends BasePage<SelectLocationAndDatesSubPage>
{
    @Visible
    @Name("Port Text Box")
    @FindBy(name = "selectedPort")
    private TypeAheadSelect portTextBox;

    @Name("Port Details")
    @FindBy(css = "[data-ng-if='vm.portExists']")
    private WebElement portDescriptionText;

    @Visible
    @Name("Estimated date of arrival Text Box")
    @FindBy(id = "arrivalDate")
    private Datepicker estimatedDateOfArrivalTextBox;

    @Visible
    @Name("Survey Start Date Text Box")
    @FindBy(id = "surveyStartDate")
    private Datepicker surveyStartDateTextBox;

    @Visible
    @Name("Estimated date of departure Text Box")
    @FindBy(id = "departureDate")
    private Datepicker estimatedDateOfDepartureTextBox;

    @Visible
    @Name("Estimated local time of arrival Text Box")
    @FindBy(css = "[data-md-input-name='arrivalTime'] input")
    private TimePicker estimatedLocalTimeOfArrivalTextBox;

    @Visible
    @Name("Estimated time of departure Text Box")
    @FindBy(css = "[data-md-input-name='departureTime'] input")
    private TimePicker estimatedTimeOfDepartureTextBox;

    @Visible
    @Name("Agent Name Text Box")
    @FindBy(id = "agentName")
    private WebElement agentNameTextBox;

    @Visible
    @Name("Agent Phone Number Text Box")
    @FindBy(id = "agentPhoneNumber")
    private WebElement agentPhoneNumberTextBox;

    @Visible
    @Name("Agent Email Text Box")
    @FindBy(id = "agentEmail")
    private WebElement agentEmailTextBox;

    @Visible
    @Name("Berth / Anchorage Text Box")
    @FindBy(id = "berthAnchorage")
    private WebElement berthAnchorageTextBox;

    @Visible
    @Name("Ship Contact Details Text Box")
    @FindBy(id = "shipContactDetails")
    private TextArea shipContactDetailsTextBox;

    @Visible
    @Name("Back Button")
    @FindBy(css = "[ng-click='vm.backStep()']")
    private WebElement backButton;

    @Visible
    @Name("Continue Button")
    @FindBy(css = "[ng-click='vm.toConfirmation()']")
    private WebElement continueButton;

    @Step("Get services added sub page")
    public ServicesAddedSubPage getServicesAddedPage()
    {
        return PageFactory.newInstance(ServicesAddedSubPage.class);
    }

    @Step("Click Back Button")
    public SelectServicesSubPage clickBackButton()
    {
        backButton.click();
        return PageFactory.newInstance(SelectServicesSubPage.class);
    }

    @Step("Click Continue Button")
    public ConfirmSubPage clickContinueButton()
    {
        continueButton.click();
        return PageFactory.newInstance(ConfirmSubPage.class);
    }

    @Step("Select Port Name - \"{0}\"")
    public SelectLocationAndDatesSubPage selectPortName(String strPortName)
    {
        portTextBox.clear();
        portTextBox.sendKeys(strPortName.substring(0, strPortName.length()-1));
        portTextBox.selectByVisibleText(strPortName);
        return this;
    }

    @Step("Set date of arrival by entering a text box - \"{0}\"")
    public SelectLocationAndDatesSubPage setArrivalDate(String arrivalDate)
    {
        estimatedDateOfArrivalTextBox.sendKeys(arrivalDate);
        estimatedDateOfArrivalTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Clear date of arrival text box")
    public SelectLocationAndDatesSubPage clearArrivalDate()
    {
        estimatedDateOfArrivalTextBox.clear();
        estimatedDateOfArrivalTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Get error message for date of arrival text box")
    public String getArrivalDateErrorMessage()
    {
        return estimatedDateOfArrivalTextBox.getErrorMessage();
    }

    @Step("Select date of arrival by clicking calendar button")
    public SelectLocationAndDatesSubPage selectArrivalDateByCalendarPane(LocalDate localDate)
    {
        estimatedDateOfArrivalTextBox.selectByCalendarPane(localDate);
        return this;
    }

    @Step("Set Estimated date of arrival - \"{0}\"")
    public SelectLocationAndDatesSubPage setEstimatedDateOfArrival(String dateOfArrival)
    {
        estimatedDateOfArrivalTextBox.clear();
        estimatedDateOfArrivalTextBox.sendKeys(dateOfArrival);
        return this;
    }

    @Step("Select survey start date by clicking calendar button - \"{0}\"")
    public SelectLocationAndDatesSubPage selectSurveyStartDateByCalendarPane(LocalDate localDate)
    {
        surveyStartDateTextBox.selectByCalendarPane(localDate);
        return this;
    }

    @Step("Set survey start date with \"{0}\"")
    public SelectLocationAndDatesSubPage setSurveyStartDate(String surveyStartDate)
    {
        surveyStartDateTextBox.clear();
        surveyStartDateTextBox.sendKeys(surveyStartDate);
        surveyStartDateTextBox.sendKeys(Keys.ENTER);
        return this;
    }

    @Step("Select departure date by clicking calendar button - \"{0}\"")
    public SelectLocationAndDatesSubPage selectDepartureDateByCalendarPane(LocalDate localDate)
    {
        estimatedDateOfDepartureTextBox.selectByCalendarPane(localDate);
        return this;
    }

    @Step("Select departure date - \"{0}\"")
    public SelectLocationAndDatesSubPage setEstimatedDateOfDeparture(String estimatedDateOfDeparture)
    {
        estimatedDateOfDepartureTextBox.clear();
        estimatedDateOfDepartureTextBox.sendKeys(estimatedDateOfDeparture);
        estimatedDateOfDepartureTextBox.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set time of arrival - \"{0}\"")
    public SelectLocationAndDatesSubPage setArrivalTime(String strArrivalTime)
    {
        AppHelper.scrollIntoView(estimatedDateOfArrivalTextBox);
        estimatedLocalTimeOfArrivalTextBox.clear();
        estimatedLocalTimeOfArrivalTextBox.sendKeys(strArrivalTime);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set time of departure - \"{0}\"")
    public SelectLocationAndDatesSubPage setDepartureTime(String strDepartureTime)
    {
        AppHelper.scrollIntoView(estimatedDateOfDepartureTextBox);
        estimatedTimeOfDepartureTextBox.clear();
        estimatedTimeOfDepartureTextBox.sendKeys(strDepartureTime);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set agent name \"{0}\"")
    public SelectLocationAndDatesSubPage setAgentName(String strPortAgentName)
    {
        agentNameTextBox.clear();
        agentNameTextBox.sendKeys(strPortAgentName);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set agent phone number - \"{0}\"")
    public SelectLocationAndDatesSubPage setAgentPhoneNumber(String strAgentPhoneNumber)
    {
        agentPhoneNumberTextBox.clear();
        agentPhoneNumberTextBox.sendKeys(strAgentPhoneNumber);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set agent email address - \"{0}\"")
    public SelectLocationAndDatesSubPage setAgentEmail(String strAgentEmail)
    {
        agentEmailTextBox.clear();
        agentEmailTextBox.sendKeys(strAgentEmail);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set berth / Anchorage - \"{0}\"")
    public SelectLocationAndDatesSubPage setBerthAnchorage(String strBerthAnchorage)
    {
        berthAnchorageTextBox.clear();
        berthAnchorageTextBox.sendKeys(strBerthAnchorage);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set ship contact details - \"{0}\"")
    public SelectLocationAndDatesSubPage setShipContactDetails(String strShipContactDetails)
    {
        shipContactDetailsTextBox.clear();
        shipContactDetailsTextBox.sendKeys(strShipContactDetails);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Port Description")
    public String getPortDetails()
    {
        return portDescriptionText.getText();
    }

    @Step("Check if continue button is disabled")
    public boolean isContinueButtonDisabled()
    {
        // Fix the return value - http://stackoverflow.com/questions/25995656/why-does-getattributedisabled-return-true-not-disabled
        return Boolean.parseBoolean(continueButton.getAttribute("disabled"));
    }

    @Step("Get arrival date")
    public String getArrivalDate()
    {
        return estimatedDateOfArrivalTextBox.getSelectedDate();
    }

    @Step("Get survey start date")
    public String getSurveyStartDate()
    {
        return surveyStartDateTextBox.getSelectedDate();
    }

    @Step("Get departure date")
    public String getDepartureDate()
    {
        return estimatedDateOfDepartureTextBox.getSelectedDate();
    }

    @Step("Get arrival time")
    public String getArrivalTime()
    {
        return estimatedLocalTimeOfArrivalTextBox.getAttribute("value");
    }

    @Step("Get arrival time error message")
    public String getArrivalTimeErrorMessage()
    {
        return estimatedLocalTimeOfArrivalTextBox.getErrorMessage();
    }

    @Step("Get departure time")
    public String getDepartureTime()
    {
        return estimatedTimeOfDepartureTextBox.getAttribute("value");
    }

    @Step("Get departure time error message")
    public String getDepartureTimeErrorMessage()
    {
        return estimatedTimeOfDepartureTextBox.getErrorMessage();
    }

    @Step("Get agent name")
    public String getAgentName()
    {
        return agentNameTextBox.getAttribute("value");
    }

    @Step("Get agent phone number")
    public String getAgentPhoneNumber()
    {
        return agentPhoneNumberTextBox.getAttribute("value");
    }

    @Step("Get agent email address")
    public String getAgentEmailAddress()
    {
        return agentEmailTextBox.getAttribute("value");
    }

    @Step("Get berth / anchorage")
    public String getBerthAnchorage()
    {
        return berthAnchorageTextBox.getAttribute("value");
    }

    @Step("Get ship contact details")
    public String getShipContactDetails()
    {
        return shipContactDetailsTextBox.getText();
    }

    @Step("Get portName")
    public String getSelectedPortName()
    {
        return portTextBox.getAttribute("value");
    }

    @Step("Is Estimated time of arrival dropdown visible")
    public boolean isEstimatedTimeOfArrivalDropdownVisible()
    {
        estimatedLocalTimeOfArrivalTextBox.click();
        return estimatedLocalTimeOfArrivalTextBox.isDropdownVisible();
    }

    @Step("Is Estimated time of departure dropdown visible")
    public boolean isEstimatedTimeOfDepartureDropdownVisible()
    {
        estimatedTimeOfDepartureTextBox.click();
        return estimatedTimeOfDepartureTextBox.isDropdownVisible();
    }
}
