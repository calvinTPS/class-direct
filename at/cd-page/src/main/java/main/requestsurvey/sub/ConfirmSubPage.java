package main.requestsurvey.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.MultiTagTextBox;
import typifiedelement.TextArea;

public class ConfirmSubPage extends BasePage<ConfirmSubPage>
{
    @Step("Get services added sub page")
    public ServicesAddedSubPage getServicesAddedPage()
    {
        return PageFactory.newInstance(ServicesAddedSubPage.class);
    }

    @Visible
    @Name("Edit Details Button")
    @FindBy(css = "[aria-label='Edit details']")
    private WebElement editDetailsButton;

    @Visible
    @Name("Comments Text Area")
    @FindBy(id = "survey-request-comments")
    private TextArea commentsTextArea;

    @Name("Share to Others Button")
    @FindBy(css = "[aria-label='Share to others']")
    private WebElement shareToOthersButton;

    @Name("Email For Share To Others Textbox")
    @FindBy(className = "ui-select-search")
    private MultiTagTextBox emailTextbox;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".step-3 [aria-label='Cancel']")
    private WebElement cancelButton;

    @Visible
    @Name("Continue Button")
    @FindBy(css = ".step-3 [aria-label='Continue']")
    private WebElement continueButton;

    @Visible
    @Name("Survey Start Date")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.surveyStartDate']")
    private WebElement surveyStartDateText;

    @Visible
    @Name("Arrival Date")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.estimatedDateOfArrival']")
    private WebElement arrivalDateText;

    @Visible
    @Name("Departure Date")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.estimatedDateOfDeparture']")
    private WebElement departureDateText;

    @Visible
    @Name("Arrival Time")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.estimatedTimeOfArrival']")
    private WebElement arrivalTimeText;

    @Visible
    @Name("Departure Time")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.estimatedTimeOfDeparture']")
    private WebElement departureTimeText;

    @Visible
    @Name("Agent Name")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.portAgent.name']")
    private WebElement agentNameText;

    @Visible
    @Name("Agent Phone Number")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.portAgent.phoneNumber']")
    private WebElement agentPhoneNumberText;

    @Visible
    @Name("Agent Email Address")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.portAgent.email']")
    private WebElement agentEmailText;

    @Visible
    @Name("Berth / Anchorage")
    @FindBy(css = "[data-ng-bind*='vm.surveyDetailModel.berthAnchorage']")
    private WebElement berthAnchorageText;

    @Visible
    @Name("Ship contact details")
    @FindBy(css = "[data-ng-if='vm.surveyDetailModel.shipContactDetails']")
    private WebElement shipContactDetailsText;

    @Visible
    @Name("Port details")
    @FindBy(css = ".main-title~h3")
    private WebElement portDetailsText;

    @Step("Get survey start date")
    public String getSurveyStartDate()
    {
        return surveyStartDateText.getText();
    }

    @Step("Get arrival date")
    public String getArrivalDate()
    {
        return arrivalDateText.getText();
    }

    @Step("Get departure date")
    public String getDepartureDate()
    {
        return departureDateText.getText();
    }

    @Step("Get arrival time")
    public String getArrivalTime()
    {
        return arrivalTimeText.getText();
    }

    @Step("Get departure time")
    public String getDepartureTime()
    {
        return departureTimeText.getText();
    }

    @Step("Get agent name")
    public String getAgentName()
    {
        return agentNameText.getText();
    }

    @Step("Get agent phone number")
    public String getAgentPhoneNumber()
    {
        return agentPhoneNumberText.getText();
    }

    @Step("Get agent email address")
    public String getAgentEmailAddress()
    {
        return agentEmailText.getText();
    }

    @Step("Get berth / anchorage")
    public String getBerthAnchorage()
    {
        return berthAnchorageText.getText();
    }

    @Step("Get ship contact details")
    public String getShipContactDetails()
    {
        return shipContactDetailsText.getText();
    }

    @Step("Get port details")
    public String getPortDetails()
    {
        return portDetailsText.getText();
    }

    @Step("Get comment helper text")
    public String getCommentHelperText()
    {
        return commentsTextArea.getAttribute("placeholder");
    }

    @Step("Get email helper text")
    public String getEmailHelperText()
    {
        return emailTextbox.getAttribute("placeholder");
    }

    @Step("Set comment")
    public ConfirmSubPage setComments(String strComments)
    {
        commentsTextArea.clear();
        commentsTextArea.sendKeys(strComments);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Share To Others")
    public ConfirmSubPage clickShareToOthersButton()
    {
        AppHelper.scrollToMiddle(shareToOthersButton);
        shareToOthersButton.click();
        return this;
    }

    @Step("Set Email for share to others")
    public ConfirmSubPage setEmailForShareToOthers(String strEmail)
    {
        emailTextbox.sendKeys(strEmail);
        emailTextbox.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get comment")
    public String getComments()
    {
        return commentsTextArea.getText();
    }

    @Step("Get email for Share To Others")

    public String getEmailForShareToOthers(int index)
    {
        return emailTextbox.getTags().get(index).getText();
    }

    @Step("Click Edit Details Button")
    public SelectLocationAndDatesSubPage clickEditDetailsButton()
    {
        editDetailsButton.click();
        return PageFactory.newInstance(SelectLocationAndDatesSubPage.class);
    }

    @Step("Click Continue button")
    public RequestSuccessfulSubPage clickContinueButton()
    {
        continueButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(RequestSuccessfulSubPage.class);
    }
}
