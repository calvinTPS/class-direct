package main.requestsurvey.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.requestsurvey.services.BaseServiceTable;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import static helper.AppHelper.scrollToMiddle;

public class ServicesAddedSubPage extends BasePage<ServicesAddedSubPage> {
    @Visible
    @Name("Edit Surveys Button")
    @FindBy(css = "section:nth-child(2) [aria-label='Edit surveys']")
    private WebElement editSurveysButton;

    @Visible
    @Name("Services Added Table")
    @FindBy(css = "[data-is-read-only='true']")
    private BaseServiceTable servicesAddedTable;


    @Step("Click Edit Surveys Button")
    public SelectServicesSubPage clickEditSurveysButton() {
        scrollToMiddle(editSurveysButton);
        editSurveysButton.click();
        return PageFactory.newInstance(SelectServicesSubPage.class);
    }

    @Step("Get Services Added Table")
    public BaseServiceTable getServicesAddedTable() {
        return servicesAddedTable;
    }
}
