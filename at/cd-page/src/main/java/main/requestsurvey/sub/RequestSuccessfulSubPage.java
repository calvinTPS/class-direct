package main.requestsurvey.sub;

import com.frameworkium.core.ui.pages.BasePage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class RequestSuccessfulSubPage extends BasePage<RequestSuccessfulSubPage>
{
    @Name("Request Sent Successfult Text")
    @FindBy(css = "md-card-content .mt0")
    private WebElement requestSentSuccessfulText;

    @Step("Get request sent successful text")
    public String getRequestSentSuccessfulText()
    {
        return requestSentSuccessfulText.getText();
    }
}
