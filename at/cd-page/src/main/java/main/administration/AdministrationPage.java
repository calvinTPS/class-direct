package main.administration;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.adduser.steps.SearchUserEmailPage;
import main.administration.base.BaseTable;
import main.administration.export.ExportAccountReportPage;
import main.common.ShowingXOfYPage;
import main.common.filter.FilterBarPage;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class AdministrationPage extends BasePage<AdministrationPage>
{
    @Name("Administration Table")
    @FindBy(css = ".rider-items-container")
    private BaseTable administrationTable;

    @Visible
    @Name("Add user button")
    @FindBy(css = "[aria-label='Add user']")
    private WebElement addUserButton;

    @Visible
    @Name("Export Account Report button")
    @FindBy(css = "[aria-label='Export account report']")
    private WebElement exportAccountReportButton;

    @Step("Get Administration Table")
    public BaseTable getAdministrationTable()
    {
        return administrationTable;
    }

    @Step("Get \"Showing X of Y page\"")
    public ShowingXOfYPage getShowingXofYPage()
    {
        return PageFactory.newInstance(ShowingXOfYPage.class);
    }

    @Step("Click 'Add user' button")
    public SearchUserEmailPage clickAddUserButton()
    {
        addUserButton.click();
        return PageFactory.newInstance(SearchUserEmailPage.class);
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Click 'Export Account Report' button")
    public ExportAccountReportPage clickExportAccountReportButton()
    {
        wait.until(ExpectedConditions.elementToBeClickable(exportAccountReportButton));
        exportAccountReportButton.click();
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        return PageFactory.newInstance(ExportAccountReportPage.class);
    }
}
