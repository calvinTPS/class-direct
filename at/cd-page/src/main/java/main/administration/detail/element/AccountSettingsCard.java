package main.administration.detail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.detail.sub.modalwindow.AmendAccountExpiryDatePage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class AccountSettingsCard extends HtmlElement
{
    @Visible
    @Name("Account Expiry Date")
    @FindBy(css = "md-card-content div.item-value")
    private WebElement accountExpiryDate;

    @Visible
    @Name("Edit Account Expiry Date Button")
    @FindBy(css = "md-card-content div.item-action button")
    private WebElement editAccountExpiryDateButton;

    @Step("Click 'Amend Account Expiry Date' button")
    public AmendAccountExpiryDatePage clickAmendAccountExpiryButton()
    {
        editAccountExpiryDateButton.click();
        return PageFactory.newInstance(AmendAccountExpiryDatePage.class);
    }

    @Step("Get Asset Expiry Date")
    public String getAssetExpiryDate()
    {
        return accountExpiryDate.getText().trim();
    }
}
