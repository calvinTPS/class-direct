package main.administration.detail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class CompanyDetailsCard extends HtmlElement
{
    @Visible
    @Name("Company Details title")
    @FindBy(css = "md-card-title-text")
    private WebElement companyDetailsTitle;

    @Name("Company Name")
    @FindBy(className = "company")
    private WebElement companyName;

    @Name("Company Address Line 1")
    @FindBy(className = "address-line-1")
    private WebElement addressLine1;

    @Name("Company City")
    @FindBy(className = "city")
    private WebElement city;

    @Name("Company State/Province/Region")
    @FindBy(className = "state-province-region")
    private WebElement stateProvinceRegion;

    @Name("Company Postal code")
    @FindBy(className = "zip-postal-code")
    private WebElement postalCode;

    @Name("Company Country")
    @FindBy(className = "country")
    private WebElement country;

    @Step("Get Company Details Title")
    public String getCompanyDetailsTitle()
    {
        return companyDetailsTitle.getText().trim();
    }

    @Step("Get Company Name")
    public String getCompanyName()
    {
        return companyName.getText().trim();
    }

    @Step("Get Address Line 1")
    public String getAddressLine1()
    {
        return addressLine1.getText().trim();
    }

    @Step("Get City")
    public String getCity()
    {
        return city.getText().trim();
    }

    @Step("Get State/ Province/ Region")
    public String getStateProvinceRegion()
    {
        return stateProvinceRegion.getText().trim();
    }

    @Step("Get Postal Code")
    public String getPostalCode()
    {
        return postalCode.getText().trim();
    }

    @Step("Get Country")
    public String getCountry()
    {
        return country.getText().trim();
    }
}
