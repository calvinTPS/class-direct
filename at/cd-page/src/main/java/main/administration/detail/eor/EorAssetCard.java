package main.administration.detail.eor;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.detail.sub.modalwindow.ConfirmationSubPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

@FindBy(css = "md-card-content eor-asset")
public class EorAssetCard extends HtmlElement
{
    @Visible
    @Name("Asset Name")
    @FindBy(css = "mini-asset-card .h3")
    private WebElement assetName;

    @Visible
    @Name("IMO Number")
    @FindBy(css = "mini-asset-card .h4")
    private WebElement imoNumber;

    @Visible
    @Name("Expiration Date")
    @FindBy(css = "div.expiry-date")
    private WebElement expirationDate;

    @Visible
    @Name("Update EOR Expiration Date")
    @FindBy(css = "button[aria-label='Update EOR expiration date']")
    private WebElement updateEorExpirationDateButton;

    @Visible
    @Name("Remove EOR Permission")
    @FindBy(css = "button[aria-label='Remove EOR Permission']")
    private WebElement removeEorPermissionButton;

    @Visible
    @Name("Client Code")
    @FindBy(css = ".card h3")
    private WebElement clientCode;

    @Step("Get asset name")
    public String getAssetName()
    {
        return assetName.getText().trim();
    }

    @Step("Get IMO Number")
    public String getImoNumber()
    {
        return imoNumber.getText().trim();
    }

    @Step("Get Expiration Date")
    public String getExpirationDate()
    {
        return expirationDate.getText().trim();
    }

    @Step("Click 'Update EOR Expiration Date' button")
    public AmendAssetExpiryDatePage clickUpdateEorExpirationDateButton()
    {
        updateEorExpirationDateButton.click();
        return PageFactory.newInstance(AmendAssetExpiryDatePage.class);
    }

    @Step("Get Client Code")
    public String getClientCode()
    {
        return clientCode.getText().trim();
    }

    @Step("Click 'Remove EOR Permission' button")
    public ConfirmationSubPage clickRemoveEorPermissionButton()
    {
        removeEorPermissionButton.click();
        return PageFactory.newInstance(ConfirmationSubPage.class);
    }
}
