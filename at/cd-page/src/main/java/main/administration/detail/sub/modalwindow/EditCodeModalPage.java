package main.administration.detail.sub.modalwindow;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.detail.AdministrationDetailPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class EditCodeModalPage extends BasePage<EditCodeModalPage>
{
    @Visible
    @Name("Title")
    @FindBy(className = "modal-title")
    private WebElement modalTitle;

    @Visible
    @Name("Code Text box")
    @FindBy(css = "md-input-container input[id*='Code']")
    private WebElement codeTextBox;

    @Visible
    @Name("Cancel button")
    @FindBy(className = "modal-cancel-button")
    private WebElement cancelButton;

    @Visible
    @Name("Confirm button")
    @FindBy(className = "modal-confirm-button")
    private WebElement confirmButton;

    @Step("Click Cancel button")
    public AdministrationDetailPage clickCancelButton()
    {
        cancelButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Click Confirm button")
    public AdministrationDetailPage clickConfirmButton()
    {
        confirmButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Set Code Textbox")
    public EditCodeModalPage setCodeTextBox(String code)
    {
        codeTextBox.sendKeys(code);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Modal title")
    public String getModalTitle()
    {
        return modalTitle.getText().trim();
    }
}
