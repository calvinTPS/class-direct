package main.administration.detail.sub.restrictedassets;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.element.MiniAssetCard;
import main.administration.detail.sub.modalwindow.ConfirmAccessibleRestrictedAssetPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class EditRestrictedAssetsPage extends BasePage<EditRestrictedAssetsPage>
{
    @Visible
    @Name("Deselect all button")
    @FindBy(css = "button[aria-label='Deselect all']")
    private WebElement deselectAllButton;

    @Visible
    @Name("Select all button")
    @FindBy(css = "button[aria-label='Select all']")
    private WebElement selectAllButton;

    @Visible
    @Name("Cancel button")
    @FindBy(css = "[aria-label='Cancel']")
    private WebElement cancelButton;

    @Visible
    @Name("Move to accessible assets button")
    @FindBy(css = "[aria-label='Move to accessible assets']")
    private WebElement moveToAccessibleAssetButton;

    @Name("Restricted Assets Elements")
    private List<MiniAssetCard> restrictedAssetCards;

    @Step("Get Restricted Assets")
    public List<MiniAssetCard> getRestrictedAssetCards()
    {
        return restrictedAssetCards;
    }

    @Step("Click cancel button")
    public RestrictedAssetsPage clickCancelButton()
    {
        cancelButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(RestrictedAssetsPage.class);
    }

    @Step("Is DeSelect All Button Enabled")
    public boolean isDeselectAllButtonEnabled()
    {
        return deselectAllButton.isEnabled();
    }

    @Step("Is Select All Button Enabled")
    public boolean isSelectAllButtonEnabled()
    {
        return selectAllButton.isEnabled();
    }

    @Step("Move To Accessible Button Enabled")
    public boolean isMoveToAccessibleButtonEnabled()
    {
        return moveToAccessibleAssetButton.isEnabled();
    }

    @Step("Is Cancel Button Enabled")
    public boolean isCancelButtonEnabled()
    {
        return cancelButton.isEnabled();
    }

    @Step("Click Move to Restricted asset button")
    public ConfirmAccessibleRestrictedAssetPage clickMoveToAccessibleAssetButton()
    {
        moveToAccessibleAssetButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ConfirmAccessibleRestrictedAssetPage.class);
    }

    @Step("Click Select All button")
    public EditRestrictedAssetsPage clickSelectAllButton()
    {
        selectAllButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }
}
