package main.administration.detail.sub.modalwindow;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.detail.AdministrationDetailPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ConfirmationSubPage extends BasePage<ConfirmationSubPage>
{
    @Visible
    @Name("Confirmation Dialog Title")
    @FindBy(className = "modal-title")
    private WebElement confirmationDialogTitle;

    @Visible
    @Name("Confirmation message body")
    @FindBy(css = "md-dialog-content div")
    private WebElement confirmationMessageBody;

    @Visible
    @Name("Close Button")
    @FindBy(className = "close-modal-button")
    private WebElement closeButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".modal-close-button, .modal-cancel-button")
    private WebElement cancelButton;

    @Visible
    @Name("Confirm Button")
    @FindBy(className = "modal-confirm-button")
    private WebElement confirmButton;

    @Step("Click confirm button")
    public AdministrationDetailPage clickConfirmButton()
    {
        AppHelper.scrollToMiddle(confirmButton);
        confirmButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Click cancel button")
    public AdministrationDetailPage clickCancelButton()
    {
        AppHelper.scrollToMiddle(cancelButton);
        cancelButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Click close button")
    public AdministrationDetailPage clickCloseButton()
    {
        AppHelper.scrollToMiddle(closeButton);
        closeButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }
}
