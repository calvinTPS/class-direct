package main.administration.detail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.adduser.eor.AddEorPermissionPage;
import main.administration.detail.eor.EorAssetCard;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class ExhibitionOfRecordsCard extends HtmlElement
{
    @Visible
    @Name("Title")
    @FindBy(css = "md-card-title-text>div.h2-primary")
    private WebElement exhibitionOfRecordsTitle;

    @Visible
    @Name("'Add EOR Access' button")
    @FindBy(css = "md-card-title-text button")
    private WebElement addEorAccessButton;

    @Name("EOR Asset cards")
    private List<EorAssetCard> eorAssetCards;

    @Step("Click 'Add EOR Access' button")
    public AddEorPermissionPage clickAddEorAccessButton()
    {
        addEorAccessButton.click();
        return PageFactory.newInstance(AddEorPermissionPage.class);
    }

    @Step("Get EOR Asset cards")
    public List<EorAssetCard> getEorAssetCards()
    {
        return eorAssetCards;
    }

    @Step("Is 'Add EOR Access' button displayed")
    public boolean isAddEorAccessButtonDisplayed()
    {
        return addEorAccessButton.isExists();
    }
}
