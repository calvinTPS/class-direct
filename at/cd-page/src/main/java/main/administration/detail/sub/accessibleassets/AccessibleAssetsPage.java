package main.administration.detail.sub.accessibleassets;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.PageFactory;
import main.administration.detail.AdministrationDetailPage;
import main.common.ShowingXOfYPage;
import main.common.element.MiniAssetCard;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

public class AccessibleAssetsPage extends BasePage<AccessibleAssetsPage>
{
    @Visible
    @Name("Edit button")
    @FindBy(css = "div.wizard-wrapper>div:nth-child(3) button")
    private WebElement editAccessibleAssetsButton;

    @Name("Accessible Assets Elements")
    private List<MiniAssetCard> assetCards;

    @Step("Click edit button")
    public EditAccessibleAssetsPage clickEditButton()
    {
        AppHelper.scrollIntoView(editAccessibleAssetsButton);
        editAccessibleAssetsButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(EditAccessibleAssetsPage.class);
    }

    @Step("Get Administration Details Page")
    public AdministrationDetailPage getAdministrationDetailPage()
    {
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Get accessible assets table")
    public List<MiniAssetCard> getAccesibleAssetCards()
    {
        return assetCards;
    }

    /**
     * Loads more assets if the initial asset is not already 0
     *
     * @return this {@code AccessibleAssetsPage}
     */
    @Step("Load more asset cards")
    public AccessibleAssetsPage loadMoreAssetCards()
    {
        ShowingXOfYPage xOfYPage = get(ShowingXOfYPage.class);
        int shownAssets = xOfYPage.getShown();
        if (shownAssets != 0)
        {
            AppHelper.scrollToBottom();
            wait.until(driver -> xOfYPage.getShown() > shownAssets ||
                    xOfYPage.getShown() == xOfYPage.getTotal());
        }
        return this;
    }

    private <T extends BasePage<T>> T get(Class<T> page)
    {
        return PageFactory.newInstance(page);
    }
}

