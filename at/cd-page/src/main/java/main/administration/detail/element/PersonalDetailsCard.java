package main.administration.detail.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.detail.sub.modalwindow.EditCodeModalPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class PersonalDetailsCard extends HtmlElement
{
    @Visible
    @Name("Personal Details title")
    @FindBy(css = "md-card-title-text")
    private WebElement personalDetailsTitle;

    @Visible
    @Name("Account type text")
    @FindBy(css = ".item-row .account-type")
    private WebElement accountType;

    @Visible
    @Name("Email text")
    @FindBy(css = ".item-row .email")
    private WebElement emailAddress;

    @Name("Telephone text")
    @FindBy(css = ".item-row .telephone")
    private WebElement telephoneNumber;

    @Name("Client code")
    @FindBy(css = ".item-row .client-code")
    private WebElement clientCode;

    @Name("Ship builder code")
    @FindBy(css = ".item-row .ship-builder-code")
    private WebElement shipBuilderCode;

    @Name("Flag code")
    @FindBy(css = ".item-row .flag-code")
    private WebElement flagCode;

    @Name("Client code button")
    @FindBy(css = "div:nth-child(1)>.item-row button")
    private WebElement clientCodeButton;

    @Name("Ship builder code button")
    @FindBy(css = "div:nth-child(2)>.item-row button")
    private WebElement shipBuilderCodeButton;

    @Name("Flag code button")
    @FindBy(css = "div:nth-child(3)>.item-row button")
    private WebElement flagCodeButton;

    @Step("Get Personal Details Title")
    public String getPersonalDetailsTitle()
    {
        return personalDetailsTitle.getText().trim();
    }

    @Step("Get Account type")
    public String getAccountType()
    {
        return accountType.getText().trim();
    }

    @Step("Get Email Address")
    public String getEmailAddress()
    {
        return emailAddress.getText().trim();
    }

    @Step("Get Telephone Number")
    public String getTelephoneNumber()
    {
        return telephoneNumber.getText().trim();
    }

    @Step("Get Client code")
    public String getClientCode()
    {
        return clientCode.getText().trim();
    }

    @Step("Get Ship Builder code")
    public String getShipBuilderCode()
    {
        return shipBuilderCode.getText().trim();
    }

    @Step("Get Flag code")
    public String getFlagCode()
    {
        return flagCode.getText().trim();
    }

    @Step("Click Client code button")
    public EditCodeModalPage clickClientCodeButton()
    {
        clientCodeButton.click();
        return PageFactory.newInstance(EditCodeModalPage.class);
    }

    @Step("Click Ship builder code button")
    public EditCodeModalPage clickShipBuilderCodeButton()
    {
        shipBuilderCode.click();
        return PageFactory.newInstance(EditCodeModalPage.class);
    }

    @Step("Click Flag code button")
    public EditCodeModalPage clickFlagCodeButton()
    {
        flagCodeButton.click();
        return PageFactory.newInstance(EditCodeModalPage.class);
    }
}
