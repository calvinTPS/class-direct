package main.administration.detail.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.detail.MasqueradeBar;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;
import typifiedelement.WebElement;

public class EmulateUserCard extends HtmlElement
{
    @Visible
    @Name("Title")
    @FindBy(css = "md-card-title-text")
    private WebElement emulateUserTitle;

    @Visible
    @Name("Emulate User Button")
    @FindBy(css = "md-card-content button")
    private WebElement emulateUserButton;

    @Visible
    @Name("Help text")
    @FindBy(css = "md-card-content p")
    private WebElement helpText;

    @Step("Click 'Emulate User' button")
    public <T extends BasePage<T>> T clickEmulateUserButton(Class<T> pageObjectClass)
    {
        emulateUserButton.click();
        wait.until(ExpectedConditions.visibilityOf(HtmlElementLoader.create(MasqueradeBar.class, driver)));
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Get help text")
    public String getHelpText()
    {
        return helpText.getText().trim();
    }

    @Step("Is 'Emulate Class Direct as this user' button displayed")
    public boolean isEmulateUserButtonDisplayed()
    {
        return emulateUserButton.isExists();
    }
}
