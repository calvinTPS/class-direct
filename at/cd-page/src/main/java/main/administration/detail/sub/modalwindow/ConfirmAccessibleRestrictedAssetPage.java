package main.administration.detail.sub.modalwindow;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ConfirmAccessibleRestrictedAssetPage extends BasePage<ConfirmAccessibleRestrictedAssetPage>
{
    @Visible
    @Name("Confirmation Dialog Title")
    @FindBy(css = ".modal-title")
    private WebElement confirmationDialogTitle;

    @Visible
    @Name("Confirmation message body")
    @FindBy(css = "md-dialog-content[id^='dialogContent']")
    private WebElement confirmationMessageBody;

    @Visible
    @Name("Close Button")
    @FindBy(css = ".close-modal-button")
    private WebElement closeButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".modal-close-button")
    private WebElement cancelButton;

    @Visible
    @Name("Confirm Button")
    @FindBy(css = ".modal-confirm-button")
    private WebElement confirmButton;

    @Step("Click confirm button")
    public <T extends BasePage<T>> T clickConfirmButton(Class<T> pageObjectClass)
    {
        confirmButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click cancel button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> pageObjectClass)
    {
        cancelButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click close button")
    public <T extends BasePage<T>> T clickCloseButton(Class<T> pageObjectClass)
    {
        closeButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(pageObjectClass);
    }
}
