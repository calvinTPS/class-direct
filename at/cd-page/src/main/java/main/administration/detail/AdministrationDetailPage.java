package main.administration.detail;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.PageFactory;
import main.administration.AdministrationPage;
import main.administration.detail.element.*;
import main.administration.detail.sub.accessibleassets.AccessibleAssetsPage;
import main.administration.detail.sub.modalwindow.ConfirmationSubPage;
import main.administration.detail.sub.restrictedassets.RestrictedAssetsPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Dropdown;

public class AdministrationDetailPage extends BasePage<AdministrationDetailPage>
{
    @Visible
    @Name("Back Button")
    @FindBy(css = "page-header md-icon")
    private WebElement backButton;

    @Visible
    @Name("User Name")
    @FindBy(css = ".title .text")
    private WebElement userName;

    @Visible
    @Name("Accessible Assets Tab")
    @FindBy(css = "[class*='flex-auto']>li:nth-child(1)")
    private WebElement accessibleAssetsTab;

    @Visible
    @Name("Restricted Assets Tab")
    @FindBy(css = "[class*='flex-auto']>li:nth-child(2)")
    private WebElement restrictedAssetsTab;

    @Visible
    @Name("Personal Details Card")
    @FindBy(css = "admin-user-details md-card")
    private PersonalDetailsCard personalDetailsCard;

    @Visible
    @Name("Company Details Card")
    @FindBy(css = "main company-details")
    private CompanyDetailsCard companyDetailsCard;

    @Name("Exhibition of Records Card")
    @FindBy(css = "eor-assets md-card")
    private ExhibitionOfRecordsCard exhibitionOfRecordsCard;

    @Name("Account Settings Card")
    @FindBy(css = "admin-account-details .cd-card-context md-card")
    private AccountSettingsCard accountSettingsCard;

    @Name("Emulate User Card")
    @FindBy(css = "masquerade-user md-card")
    private EmulateUserCard emulateUserCard;

    @Visible
    @Name("Account Status Dropdown")
    @FindBy(css = "md-select md-select-value[class='md-select-value']")
    private Dropdown accountStatusDropdown;

    @Step("Get Personal Details card")
    public PersonalDetailsCard getPersonalDetailsCard()
    {
        return personalDetailsCard;
    }

    @Step("Get Company Details card")
    public CompanyDetailsCard getCompanyDetailsCard()
    {
        return companyDetailsCard;
    }

    @Step("Get Exhibition of Records card")
    public ExhibitionOfRecordsCard getExhibitionOfRecordsCard()
    {
        return exhibitionOfRecordsCard;
    }

    @Step("Get Account Settings card")
    public AccountSettingsCard getAccountSettingsCard()
    {
        return accountSettingsCard;
    }

    @Step("Get Emulate User card")
    public EmulateUserCard getEmulateUserCard()
    {
        return emulateUserCard;
    }

    @Step("Click Accessible Assets Tab")
    public AccessibleAssetsPage clickAccessibleAssetsTab()
    {
        AppHelper.scrollIntoView(accessibleAssetsTab);
        if (!accessibleAssetsTab.getAttribute("class").contains("current"))
        {
            accessibleAssetsTab.click();
        }
        return PageFactory.newInstance(AccessibleAssetsPage.class);
    }

    @Step("Click Restricted Assets Tab")
    public RestrictedAssetsPage clickRestrictedAssetsTab()
    {
        AppHelper.scrollIntoView(restrictedAssetsTab);
        restrictedAssetsTab.click();
        return PageFactory.newInstance(RestrictedAssetsPage.class);
    }

    @Step("Click back button")
    public AdministrationPage clickBackButton()
    {
        AppHelper.scrollToMiddle(backButton);
        backButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AdministrationPage.class);
    }

    @Step("Check if accessible assets is the current selected")
    public boolean isAccessibleAssetsSelected()
    {
        return accessibleAssetsTab.getAttribute("class").contains("current");
    }

    @Step("Get Accessible Assets Page")
    public AccessibleAssetsPage getAccessibleAssetsPage()
    {
        return PageFactory.newInstance(AccessibleAssetsPage.class);
    }

    @Step("Get Account Status")
    public String getAccountStatus()
    {
        return accountStatusDropdown.getSelectedOption();
    }

    @Step("Set Account Status")
    public ConfirmationSubPage setAccountStatus(String accountStatus)
    {
        accountStatusDropdown.selectByText(accountStatus);
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ConfirmationSubPage.class);
    }

    @Step("Get User Name")
    public String getUserName()
    {
        return userName.getText().trim();
    }
}
