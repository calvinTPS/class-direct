package main.administration.detail.sub.restrictedassets;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.PageFactory;
import main.administration.detail.AdministrationDetailPage;
import main.common.element.MiniAssetCard;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class RestrictedAssetsPage extends BasePage<RestrictedAssetsPage>
{
    @Visible
    @Name("Edit button")
    @FindBy(css = "div.wizard-wrapper>div:nth-child(4) button")
    private WebElement editRestrictedAssetsButton;

    @Name("Restricted Assets Elements")
    private List<MiniAssetCard> restrictedAssetCards;

    @Step("Click edit button")
    public EditRestrictedAssetsPage clickEditButton()
    {
        AppHelper.scrollToBottom();
        AppHelper.scrollIntoView(editRestrictedAssetsButton);
        editRestrictedAssetsButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(EditRestrictedAssetsPage.class);
    }

    @Step("Get restricted assets table")
    public List<MiniAssetCard> getRestrictedAssetCards()
    {
        return restrictedAssetCards;
    }

    @Step("Get Administration Details Page")
    public AdministrationDetailPage getAdministrationDetailPage()
    {
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }
}
