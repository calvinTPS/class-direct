package main.administration.detail.eor;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import main.administration.detail.AdministrationDetailPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;

import java.util.concurrent.TimeUnit;

public class AmendAssetExpiryDatePage extends BasePage<AmendAssetExpiryDatePage>
{
    @Visible
    @Name("Close Button")
    @FindBy(css = ".close-modal-button")
    private WebElement closeButton;

    @Visible
    @Name("Title")
    @FindBy(css = ".modal-title")
    private WebElement title;

    @Visible
    @Name("Existing Asset Expiry Date")
    @FindBy(css = "md-dialog-content div.item-value")
    private WebElement existingAssetExpiryDate;

    @Visible
    @Name("New Asset expiry Date Datepicker")
    @FindBy(css = "div.date-picker md-datepicker")
    private Datepicker newAssetExpiryDatepicker;

    @Visible
    @Name("Cancel button")
    @FindBy(css = "button.modal-cancel-button")
    private WebElement cancelButton;

    @Visible
    @Name("Confirm button")
    @FindBy(css = "button.modal-confirm-button")
    private WebElement confirmButton;

    @Step("Click close button")
    public AdministrationDetailPage clickCloseButton()
    {
        closeButton.click();
        return helper.PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Click cancel button")
    public AdministrationDetailPage clickCancelButton()
    {
        cancelButton.click();
        return helper.PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Click confirm button")
    public AdministrationDetailPage clickConfirmButton()
    {
        confirmButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        try
        {   // It takes 3.42 seconds
            // Developer said it's Azure issue
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(4, TimeUnit.SECONDS));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        return helper.PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Get existing asset expiry date")
    public String getExistingAssetExpiryDate()
    {
        return existingAssetExpiryDate.getText().trim();
    }

    @Step("Set new asset expiry date")
    public AmendAssetExpiryDatePage setNewAssetExpiryDate(String newAssetExpiryDate)
    {
        newAssetExpiryDatepicker.clear();
        newAssetExpiryDatepicker.sendKeys(newAssetExpiryDate);
        newAssetExpiryDatepicker.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }
}
