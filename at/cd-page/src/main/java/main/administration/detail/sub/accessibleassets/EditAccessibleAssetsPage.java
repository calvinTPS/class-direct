package main.administration.detail.sub.accessibleassets;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.element.MiniAssetCard;
import main.administration.detail.sub.modalwindow.ConfirmAccessibleRestrictedAssetPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class EditAccessibleAssetsPage extends BasePage<EditAccessibleAssetsPage>
{

    @Visible
    @Name("Deselect all button")
    @FindBy(css = "button[aria-label='Deselect all']")
    private WebElement deselectAllButton;

    @Visible
    @Name("Select all button")
    @FindBy(css = "button[aria-label='Select all']")
    private WebElement selectAllButton;

    @Visible
    @Name("Cancel button")
    @FindBy(css = "button[aria-label='Cancel']")
    private WebElement cancelButton;

    @Visible
    @Name("Move to restricted assets button")
    @FindBy(css = "[aria-label='Move to restricted assets']")
    private WebElement moveToRestrictedAssetButton;

    @Name("Accessible Assets Table")
    private List<MiniAssetCard> assetCard;

    @Step("Check if cancel button is enabled")
    public boolean isCancelButtonEnabled()
    {
        return cancelButton.isEnabled();
    }

    @Step("Check if move to restricted button is enabled")
    public boolean isMoveToRestrictedButtonEnabled()
    {
        return moveToRestrictedAssetButton.isEnabled();
    }

    @Step("Check if deselect all button is enabled")
    public boolean isDeselectAllButtonEnabled()
    {
        return deselectAllButton.isEnabled();
    }

    @Step("Check if select all button is enabled")
    public boolean isSelectAllButtonEnabled()
    {
        return selectAllButton.isEnabled();
    }

    @Step("Get Accessible Asset Table")
    public List<MiniAssetCard> getAccessibleAssetCards()
    {
        return assetCard;
    }

    @Step("Click cancel button")
    public AccessibleAssetsPage clickCancelButton()
    {
        cancelButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AccessibleAssetsPage.class);
    }

    @Step("Click Move to Restricted asset button")
    public ConfirmAccessibleRestrictedAssetPage clickMoveToRestrictedAssetButton()
    {
        moveToRestrictedAssetButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ConfirmAccessibleRestrictedAssetPage.class);
    }

    @Step("Click Select All button")
    public EditAccessibleAssetsPage clickSelectAllButton()
    {
        selectAllButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }
}
