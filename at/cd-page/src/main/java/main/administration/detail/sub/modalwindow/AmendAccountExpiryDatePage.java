package main.administration.detail.sub.modalwindow;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.detail.AdministrationDetailPage;
import org.apache.tools.ant.taskdefs.Sleep;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;

import java.util.concurrent.TimeUnit;

public class AmendAccountExpiryDatePage extends BasePage<AmendAccountExpiryDatePage>
{
    @Visible
    @Name("Amend Account Expiry date title")
    @FindBy(className = "modal-title")
    private WebElement amendAccountExpiryDateTitle;

    @Visible
    @Name("Account Expiry Datepicker")
    @FindBy(css = "md-dialog md-datepicker")
    private Datepicker accountExpiryDatepicker;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "md-dialog-actions button:nth-child(1)")
    private WebElement cancelButton;

    @Visible
    @Name("Confirm Button")
    @FindBy(css = "md-dialog-actions button:nth-child(2)")
    private WebElement confirmButton;

    @Step("Click Cancel button")
    public AdministrationDetailPage clickCancelButton()
    {
        cancelButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Click Confirm button")
    public AdministrationDetailPage clickConfirmButton()
    {
        confirmButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        try
        {   // It takes 3.42 seconds
            // This was raised but developer said it's Azure issue
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(4, TimeUnit.SECONDS));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    @Step("Click Confirm button")
    public AmendAccountExpiryDatePage setAccountExpiryDate(String accountExpiryDate)
    {
        accountExpiryDatepicker.sendKeys(accountExpiryDate);
        accountExpiryDatepicker.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get 'Amend account expiry date' title")
    public String getAmendAccountExpiryDateTitle()
    {
        return amendAccountExpiryDateTitle.getText().trim();
    }
}
