package main.administration.detail;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import typifiedelement.WebElement;

@FindBy(css = "masquerade-bar")
public class MasqueradeBar extends HtmlElement
{
    @Visible
    @FindBy(css = "button[aria-label='Stop emulation']")
    private WebElement stopEmulationButton;
}
