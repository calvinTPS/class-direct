package main.administration.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.User.AccountStatus;
import helper.AppHelper;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.detail.AdministrationDetailPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import static constant.User.AccountStatus.*;

@Name("Data Elements")
@FindBy(css = "cd-card-tabular")
public class DataElement extends HtmlElement
{
    @Visible
    @Name("User Name")
    @FindBy(css = ".user-name")
    private WebElement userName;

    @Visible
    @Name("User Email")
    @FindBy(css = ".email")
    private WebElement userEmail;

    @Visible
    @Name("Company Name")
    @FindBy(css = ".company")
    private WebElement companyName;

    @Visible
    @Name("Role Name")
    @FindBy(css = ".role")
    private WebElement roleName;

    @Name("Ship Builder, Client or Flag Code")
    @FindBy(css = ".client-code, .flag-code, .ship-builder-code")
    private WebElement shipBuilderClientFlagCode;

    @Visible
    @Name("Account status")
    @FindBy(css = ".show-gt-sm active-status .label b")
    private WebElement accountStatus;

    @Visible
    @Name("Account Status Icon")
    @FindBy(css = ".show-gt-sm active-status .circle")
    private WebElement accountStatusIcon;

    @Visible
    @Name("Further Details Icon")
    @FindBy(css = ".action icon")
    private WebElement furtherDetailsIcon;

    @Step("Get User Role")
    public String getUserName()
    {
        return userName.getText().trim();
    }

    @Step("Get User Email")
    public String getUserEmail()
    {
        return userEmail.getText().trim();
    }

    @Step("Get Company Name")
    public String getCompanyName()
    {
        return companyName.getText().trim();
    }

    @Step("Get Role Name")
    public String getRoleName()
    {
        return roleName.getText().trim();
    }

    @Step("Get Ship Builder, Client or Flag Code")
    public String getShipBuilderClientFlagCode()
    {
        return shipBuilderClientFlagCode.getText().trim();
    }

    @Step("Get Account Status")
    public String getAccountStatus()
    {
        return accountStatus.getText().trim();
    }

    @Step("Click Further More Details Button")
    public AdministrationDetailPage clickFurtherMoreDetailsButton()
    {
        furtherDetailsIcon.click();
        waitForSpinnerToFinish();
        return PageFactory.newInstance(AdministrationDetailPage.class);
    }

    public AccountStatus getAccountStatusByIcon() {
        String accountStatus = accountStatusIcon.getAttribute("class");

        if (accountStatus.contains(Active.getAccountStatus().toLowerCase())) {
            return Active;
        } else if (accountStatus.contains(Disabled.getAccountStatus().toLowerCase())) {
            return Disabled;
        } else if (accountStatus.contains(Archived.getAccountStatus().toLowerCase())) {
            return Archived;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }
}
