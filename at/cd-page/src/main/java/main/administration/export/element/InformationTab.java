package main.administration.export.element;

import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

@FindBy(css = "div.tab-content>div:nth-child(2)")
public class InformationTab extends HtmlElement
{
    @Name("Account types")
    @FindBy(css = "div.account-type-box")
    private List<WebElement> accountTypes;

    @Name("Last login date From")
    @FindBy(css = "md-input-container:nth-child(1) div.from-to-box")
    private WebElement lastLoginDateFrom;

    @Name("Last login date To")
    @FindBy(css = "md-input-container:nth-child(2) div.from-to-box")
    private WebElement lastLoginDateTo;

    @Step("Get Account Types")
    public List<WebElement> getAccountTypes()
    {
        return accountTypes;
    }

    @Step("Get Last Login Date From")
    public String getLastLoginDateFrom()
    {
        return lastLoginDateFrom.getText().trim();
    }

    @Step("Get Last Login Date To")
    public String getLastLoginDateTo()
    {
        return lastLoginDateTo.getText().trim();
    }
}
