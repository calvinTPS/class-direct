package main.administration.export.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

@FindBy(css = "div.tab-content>div:nth-child(1)")
public class UsersTab extends HtmlElement
{
    @Visible
    @Name("Remove All button")
    @FindBy(css = "top-button-controls>button:nth-child(1)")
    private WebElement removeAllButton;

    @Visible
    @Name("Select All button")
    @FindBy(css = "top-button-controls>button:nth-child(2)")
    private WebElement selectAllButton;

    private List<UserCard> userCards;

    @Step("Get user cards")
    public List<UserCard> getUserCards()
    {
        return userCards;
    }

    @Step("Click Remove All button")
    public UsersTab clickRemoveAllButton()
    {
        removeAllButton.click();
        return this;
    }

    @Step("Click Select All button")
    public UsersTab clickSelectAllButton()
    {
        selectAllButton.click();
        return this;
    }

    @Step("Is Select All button enabled")
    public boolean isSelectAllButtonEnabled()
    {
        return selectAllButton.isEnabled();
    }

    @Step("Is Remove All button enabled")
    public boolean isRemoveAllButtonEnabled()
    {
        return removeAllButton.isEnabled();
    }
}
