package main.administration.export.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

@FindBy(css = "div.user-card>div")
public class UserCard extends HtmlElement
{
    @Visible
    @Name("Full name")
    @FindBy(css = "div.emblem-wrapper>div:nth-child(1)")
    private WebElement fullName;

    @Visible
    @Name("User Email")
    @FindBy(css = "div.emblem-wrapper>div:nth-child(2)")
    private WebElement userEmail;

    @Visible
    @Name("User Id")
    @FindBy(css = "div.emblem-wrapper>div:nth-child(3)")
    private WebElement userId;

    @Visible
    @Name("Account Type")
    @FindBy(css = "div.emblem-wrapper>div:nth-child(5)")
    private WebElement accountType;

    @Step("Get Full Name")
    public String getFullName()
    {
        return fullName.getText().trim();
    }

    @Step("Get User Email")
    public String getUserEmail()
    {
        return userEmail.getText().trim();
    }

    @Step("Get User ID")
    public String getUserId()
    {
        return userId.getText().trim();
    }

    @Step("Get Account Type")
    public String getAccountType()
    {
        return accountType.getText().trim();
    }

    @Step("Select User Card")
    public UserCard selectUserCard()
    {
        if(!isUserCardSelected())
        {
            this.click();
        }
        return this;
    }

    @Step("Deselect User Card")
    public UserCard deselectUserCard()
    {
        if(isUserCardSelected())
        {
            this.click();
        }
        return this;
    }

    @Step("Is User card selected")
    public boolean isUserCardSelected()
    {
        return this.getAttribute("class").contains("highlighted");
    }
}
