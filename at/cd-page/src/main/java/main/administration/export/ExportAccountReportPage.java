package main.administration.export;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.AdministrationPage;
import main.administration.export.element.InformationTab;
import main.administration.export.element.UsersTab;
import org.openqa.selenium.support.ui.ExpectedConditions;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import static java.util.Arrays.asList;

public class ExportAccountReportPage extends BasePage<ExportAccountReportPage>
{
    @Visible
    @Name("Close button")
    @FindBy(css = "md-toolbar>div.content-main>button")
    private WebElement closeButton;

    @Visible
    @Name("Users Tab")
    @FindBy(css = "div.tab-items>div:nth-child(1)")
    private WebElement usersTab;

    @Visible
    @Name("Information Tab")
    @FindBy(css = "div.tab-items>div:nth-child(2)")
    private WebElement informationTab;

    @Name("Users Tab Content")
    private UsersTab usersTabContent;

    @Name("Information Tab Content")
    private InformationTab informationTabContent;

    @Visible
    @Name("Cancel button")
    @FindBy(css = "md-dialog-actions button:nth-child(1)")
    private WebElement cancelButton;

    @Visible
    @Name("Export button")
    @FindBy(css = "md-dialog-actions button:nth-child(2)")
    private WebElement exportButton;

    @Step("Click Close button")
    public AdministrationPage clickCloseButton()
    {
        closeButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(AdministrationPage.class);
    }

    @Step("Click Cancel button")
    public AdministrationPage clickCancelButton()
    {
        cancelButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(AdministrationPage.class);
    }

    @Step("Get Users tab")
    public UsersTab getUsersTab()
    {
        return usersTabContent;
    }

    @Step("Click Users tab")
    public UsersTab clickUsersTab()
    {
        usersTab.click();
        return usersTabContent;
    }

    @Step("Click Information tab")
    public InformationTab clickInformationTab()
    {
        informationTab.click();
        return informationTabContent;
    }

    @Step("Is Export button enabled")
    public boolean isExportButtonEnabled()
    {
        return exportButton.isEnabled();
    }

    @Step("Click Export button")
    public ExportAccountReportPage clickExportButton()
    {
        exportButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }
}
