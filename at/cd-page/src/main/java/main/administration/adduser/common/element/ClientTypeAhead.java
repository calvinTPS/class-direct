package main.administration.adduser.common.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.adduser.steps.PermissionsPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

@FindBy(css = ".cd-card-context cd-card-tabular")
public class ClientTypeAhead extends HtmlElement
{
    @Visible
    @Name("Code")
    @FindBy(className = "h2")
    private WebElement code;

    @Visible
    @Name("Name")
    @FindBy(css = ".h4:nth-child(2)")
    private WebElement name;

    @Step("Get Code")
    public String getCode()
    {
        return code.getText().trim();
    }

    @Step("Get Name")
    public String getName()
    {
        return name.getText().trim();
    }

    @Step("Click arrow button")
    public PermissionsPage clickArrowButton()
    {
        this.click();
        return PageFactory.newInstance(PermissionsPage.class);
    }
}
