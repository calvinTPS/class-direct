package main.administration.adduser.client;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.administration.adduser.common.element.ClientTypeAhead;
import main.administration.adduser.common.FooterPage;
import main.administration.adduser.common.ProgressSubPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.ClearableTextBox;

import java.util.List;

public class SelectClientPage extends BasePage<SelectClientPage>
{
    @Visible
    @FindBy(css = "search-client input")
    @Name("Client code Textbox")
    private ClearableTextBox clientCodeTextbox;

    @Name("Client typeahead options")
    private List<ClientTypeAhead> clientTypeAheadOptions;

    @Step("Get client type ahead options")
    public List<ClientTypeAhead> getClientTypeAheadOptions()
    {
        return clientTypeAheadOptions;
    }

    @Step("Set Client code type ahead - \"{0}\"")
    public SelectClientPage setClientCodeTextbox(String clientCode)
    {
        clientCodeTextbox.clear();
        clientCodeTextbox.sendKeys(clientCode);
        return this;
    }

    @Step("Get Progress common page")
    public ProgressSubPage getProgressSubPage()
    {
        return PageFactory.newInstance(ProgressSubPage.class);
    }

    @Step("Get Footer Page")
    public FooterPage getFooterPage()
    {
        return PageFactory.newInstance(FooterPage.class);
    }
}
