package main.administration.adduser.steps;

import com.frameworkium.core.ui.pages.BasePage;
import main.administration.adduser.steps.element.AccountTypeCard;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class ChooseAccountTypePage extends BasePage<ChooseAccountTypePage>
{
    @Name("Account Type cards")
    @FindBy(css = "account-type cd-card")
    private List<AccountTypeCard> accountTypeCards;

    @Step("Get Description")
    public List<AccountTypeCard> getAccountTypeCards()
    {
        return accountTypeCards;
    }

}
