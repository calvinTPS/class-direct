package main.administration.adduser.steps;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.AdministrationPage;
import main.administration.adduser.steps.element.UserCard;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

import static constant.Config.GLOBAL_SCRIPT_TIMEOUT;
import static constant.Config.GLOBAL_SPINNER_SPAWN_TIME;

public class SearchUserEmailPage extends BasePage<SearchUserEmailPage> {
    @Visible
    @Name("Email text box")
    @FindBy(css = "div.steps md-input-container input")
    private WebElement emailTextbox;

    @Visible
    @Name("Search button")
    @FindBy(css = "div.steps button[aria-label='Search']")
    private WebElement searchButton;

    @Name("User Card")
    @FindBy(css = "user-card:not(.ng-hide) div.cd-card md-card")
    private List<UserCard> userCards;

    @Name("User not found Error Message")
    @FindBy(css = "section search-user>div")
    private WebElement userNotFoundErrorMessage;

    @Step("Set search email address - \"{0}\" ")
    public SearchUserEmailPage setEmailAddress(String emailAddress) {
        emailTextbox.clear();
        emailTextbox.sendKeys(emailAddress);
        return this;
    }

    @Step("Click Search button")
    public SearchUserEmailPage clickSearchButton() {
        searchButton.click();
        try {
            BaseTest.newWaitWithTimeout(GLOBAL_SPINNER_SPAWN_TIME).until(ExtraExpectedConditions.visibilityOfSpinner());
        } catch (TimeoutException ignore) {

        }
        BaseTest.newWaitWithTimeout(GLOBAL_SCRIPT_TIMEOUT).until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Get User Card")
    public List<UserCard> getUserCards() {
        return userCards;
    }

    @Step("Get User Not Found Error Message")
    public boolean isUserNotFoundErrorMessageDisplayed() {
        return userNotFoundErrorMessage.isExists() && userNotFoundErrorMessage.isDisplayed();
    }
}
