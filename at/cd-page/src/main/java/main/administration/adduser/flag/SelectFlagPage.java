package main.administration.adduser.flag;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.administration.adduser.common.FooterPage;
import main.administration.adduser.common.ProgressSubPage;
import main.administration.adduser.flag.element.FlagCard;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.ClearableTextBox;

import java.util.List;

public class SelectFlagPage extends BasePage<SelectFlagPage>
{
    @Visible
    @FindBy(css = "search-flag input")
    @Name("Client flag Textbox")
    private ClearableTextBox flagTextbox;

    @Name("Flag cards")
    private List<FlagCard> flagCards;

    @Step("Set Flag text box as \"{0}\"")
    public SelectFlagPage setFlagTextbox(String flag)
    {
        flagTextbox.clear();
        flagTextbox.sendKeys(flag);
        flagTextbox.sendKeys(Keys.BACK_SPACE); //not sure if this is bug. it takes time to show the country list
        return this;
    }

    @Step("Get Flag cards")
    public List<FlagCard> getFlagCards()
    {
        return flagCards;
    }

    @Step("Get Progress common page")
    public ProgressSubPage getProgressSubPage()
    {
        return PageFactory.newInstance(ProgressSubPage.class);
    }

    @Step("Get Footer Page")
    public FooterPage getFooterPage()
    {
        return PageFactory.newInstance(FooterPage.class);
    }
}
