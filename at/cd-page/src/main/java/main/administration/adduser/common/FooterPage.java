package main.administration.adduser.common;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.administration.AdministrationPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class FooterPage extends BasePage<FooterPage>
{
    @Visible
    @Name("Cancel button")
    @FindBy(css = "[aria-label='Cancel']")
    private WebElement cancelButton;

    @Name("Confirm button")
    @FindBy(css = "div.cd-actions button")
    private WebElement confirmButton;

    @Step("Click cancel button")
    public AdministrationPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(AdministrationPage.class);
    }

    @Step("Click 'Confirm' button")
    public <T extends BasePage<T>> T clickConfirmButton(Class<T> pageObjectClass)
    {
        confirmButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }
}
