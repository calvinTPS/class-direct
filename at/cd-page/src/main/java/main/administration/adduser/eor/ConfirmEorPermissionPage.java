package main.administration.adduser.eor;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.administration.adduser.common.FooterPage;
import main.administration.adduser.common.ProgressSubPage;
import main.common.element.MiniAssetCard;
import org.openqa.selenium.By;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ConfirmEorPermissionPage extends BasePage<ConfirmEorPermissionPage>
{
    @Visible
    @Name("Asset expiry")
    @FindBy(css = "confirmation account-expiry>div:nth-child(2)")
    private WebElement assetExpiry;

    @Visible
    @Name("Asset card")
    @FindBy(css = "confirmation mini-asset-card")
    private MiniAssetCard assetCard;

    @Step("Is 'Accept terms and conditions checkbox' exist")
    public boolean isAcceptTermsAndConditionsVisible()
    {
        return driver.findElements(By.cssSelector(".accept-checkbox input")).size() > 0;
    }

    @Step("Get Progress common page")
    public ProgressSubPage getProgressSubPage()
    {
        return PageFactory.newInstance(ProgressSubPage.class);
    }

    @Step("Get Footer Page")
    public FooterPage getFooterPage()
    {
        return PageFactory.newInstance(FooterPage.class);
    }

    @Step("Get asset expiry")
    public String getAssetExpiry()
    {
        return assetExpiry.getText().trim();
    }

    @Step("Get asset card")
    public MiniAssetCard getAssetCard()
    {
        return assetCard;
    }
}
