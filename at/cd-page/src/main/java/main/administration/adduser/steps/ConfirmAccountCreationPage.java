package main.administration.adduser.steps;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.administration.adduser.common.FooterPage;
import main.administration.adduser.common.ProgressSubPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class ConfirmAccountCreationPage extends BasePage<ConfirmAccountCreationPage>
{
    @Visible
    @Name("Asset expiry")
    @FindBy(css = "confirmation account-expiry>div:nth-child(2)")
    private WebElement assetExpiry;

    @Step("Get asset expiry")
    public String getAssetExpiry()
    {
        return assetExpiry.getText().trim();
    }

    @Step("Get Progress common page")
    public ProgressSubPage getProgressSubPage()
    {
        return PageFactory.newInstance(ProgressSubPage.class);
    }

    @Step("Get Footer Page")
    public FooterPage getFooterPage()
    {
        return PageFactory.newInstance(FooterPage.class);
    }
}
