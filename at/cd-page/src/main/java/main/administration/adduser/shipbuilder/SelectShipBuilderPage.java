package main.administration.adduser.shipbuilder;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.administration.adduser.common.element.ClientTypeAhead;
import main.administration.adduser.common.FooterPage;
import main.administration.adduser.common.ProgressSubPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.ClearableTextBox;

import java.util.List;

public class SelectShipBuilderPage extends BasePage<SelectShipBuilderPage>
{
    @Visible
    @FindBy(css = "search-ship-builder input")
    @Name("Ship builder code Textbox")
    private ClearableTextBox shipBuilderCodeTextbox;

    @Name("Ship builder typeahead options")
    private List<ClientTypeAhead> shipBuilderTypeAheadOptions;

    @Step("Set Ship builder code type ahead - \"{0}\"")
    public SelectShipBuilderPage setShipBuilderCodeTextbox(String shipBuilderCode)
    {
        shipBuilderCodeTextbox.clear();
        shipBuilderCodeTextbox.sendKeys(shipBuilderCode);
        return this;
    }

    @Step("Get Ship builder type ahead options")
    public List<ClientTypeAhead> getShipBuilderTypeAheadOptions()
    {
        return shipBuilderTypeAheadOptions;
    }

    @Step("Get Progress common page")
    public ProgressSubPage getProgressSubPage()
    {
        return PageFactory.newInstance(ProgressSubPage.class);
    }

    @Step("Get Footer Page")
    public FooterPage getFooterPage()
    {
        return PageFactory.newInstance(FooterPage.class);
    }
}
