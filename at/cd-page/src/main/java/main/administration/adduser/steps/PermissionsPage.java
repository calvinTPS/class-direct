package main.administration.adduser.steps;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.administration.adduser.common.FooterPage;
import main.administration.adduser.common.ProgressSubPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;

public class PermissionsPage extends BasePage<PermissionsPage>
{
    @Visible
    @Name("Account expiry datepicker")
    @FindBy(css = "permissions md-datepicker")
    private Datepicker accountExpiryDatepicker;

    @Step("Set account expiry date - \"{0}\"")
    public PermissionsPage setAccountExpiryDate(String accountExpiryDate)
    {
        accountExpiryDatepicker.clear();
        accountExpiryDatepicker.sendKeys(accountExpiryDate);
        accountExpiryDatepicker.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Get Progress common page")
    public ProgressSubPage getProgressSubPage()
    {
        return PageFactory.newInstance(ProgressSubPage.class);
    }

    @Step("Get Footer Page")
    public FooterPage getFooterPage()
    {
        return PageFactory.newInstance(FooterPage.class);
    }
}
