package main.administration.adduser.common;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class ProgressSubPage extends BasePage<ProgressSubPage>
{
    @Visible
    @Name("Progress cards")
    @FindBy(css = "div.progress-card")
    private List<WebElement> progressCards;

    @Step("Click Progress bar by text")
    public <T extends BasePage<T>> T clickProgressCardByText(Class<T> pageObjectClass, String progressCardText)
    {
        progressCards.stream()
                .filter(e -> e.getText().contains(progressCardText))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected progress card not found."))
                .click();
        return PageFactory.newInstance(pageObjectClass);
    }
}
