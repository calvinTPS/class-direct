package main.administration.adduser.flag.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.adduser.steps.PermissionsPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

@FindBy(css = ".rider-items-container md-card")
public class FlagCard extends HtmlElement
{
    @Visible
    @FindBy(css = "div> div> .flex-auto")
    @Name("Flag name")
    private WebElement flagName;

    @Visible
    @FindBy(className = "md-button")
    @Name("Select button")
    private WebElement selectButton;

    @Step("Get Flag Name")
    public String getFlagName()
    {
        return flagName.getText().trim();
    }

    @Step("Click Select button")
    public PermissionsPage clickSelectButton()
    {
        selectButton.click();
        return PageFactory.newInstance(PermissionsPage.class);
    }
}
