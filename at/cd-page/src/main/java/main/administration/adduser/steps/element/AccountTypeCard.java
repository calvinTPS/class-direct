package main.administration.adduser.steps.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import helper.PageFactory;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class AccountTypeCard extends HtmlElement
{
    @Visible
    @Name("User Name")
    @FindBy(css = "md-card-title-text h2")
    private WebElement name;

    @Visible
    @Name("Description")
    @FindBy(css = "md-card-content md-card-title-text div")
    private WebElement description;

    @Visible
    @Name("Select button")
    @FindBy(css = "button[aria-label='Select']")
    private WebElement selectButton;

    @Step("Get Name")
    public String getName()
    {
        return name.getText().trim();
    }

    @Step("Get Description")
    public String getDescription()
    {
        return description.getText().trim();
    }

    @Step("Click Select button")
    public <T extends BasePage<T>> T clickSelectButton(Class<T> page)
    {
        selectButton.click();
        return PageFactory.newInstance(page);
    }
}
