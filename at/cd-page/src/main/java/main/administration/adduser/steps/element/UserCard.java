package main.administration.adduser.steps.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import helper.PageFactory;
import main.administration.adduser.steps.ChooseAccountTypePage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class UserCard extends HtmlElement
{
    @Visible
    @Name("User Name")
    @FindBy(css = "md-card-title-text>div")
    private WebElement userName;

    @Visible
    @Name("User Email")
    @FindBy(css = "md-card-content strong:nth-child(1)")
    private WebElement userEmail;

    @Visible
    @Name("User Role")
    @FindBy(css = "md-card-content strong:nth-child(3)")
    private WebElement userRole;

    @Visible
    @Name("Create new account/ Edit account button")
    @FindBy(css = "md-card-content button")
    private WebElement createNewOrEditAccountButton;

    @Step("Get User Name")
    public String getUserName()
    {
        return userName.getText().trim();
    }

    @Step("Get User Email")
    public String getUserEmail()
    {
        return userEmail.getText().trim();
    }

    @Step("Get User Role")
    public String getUserRole()
    {
        return userRole.getText().trim();
    }

    @Step("Get Button Text - createNewOrEditAccountButton")
    public String getCreateNewOrEditAccountButtonText()
    {
        return createNewOrEditAccountButton.getText().trim();
    }

    @Step("Click create new or edit account button")
    public <T extends BasePage<T>> T clickCreateNewOrEditAccountButton(Class<T> page)
    {
        //ChooseAccountTypePage for Create and AdministrationDetailPage for Edit
        createNewOrEditAccountButton.click();
        return PageFactory.newInstance(page);
    }
}
