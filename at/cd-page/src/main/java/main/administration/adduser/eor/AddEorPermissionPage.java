package main.administration.adduser.eor;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.element.MiniAssetCard;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.ClearableTextBox;
import typifiedelement.Datepicker;
import typifiedelement.Switch;
import typifiedelement.WebElement;

public class AddEorPermissionPage extends BasePage<AddEorPermissionPage>
{
    @Visible
    @Name("Asset Expiry Datepicker")
    @FindBy(css = "account-expiry md-datepicker")
    private Datepicker assetExpiryDatepicker;

    @Visible
    @Name("IMO Number text box")
    @FindBy(css = "search-user-assets input")
    private ClearableTextBox imoNumberTextbox;

    @Visible
    @Name("Thickness Measurement Reports")
    @FindBy(css = ".header-switch md-switch")
    private Switch thicknessMeasurementReportsSwitch;

    @Visible
    @Name("Add exhibition of records asset button")
    @FindBy(css = "div.cd-actions button")
    private WebElement addExhibitionOfRecordsAssetButton;

    @Name("Asset card")
    @FindBy(tagName = "mini-asset-card")
    private MiniAssetCard assetCard;

    @Step("Set asset expiry date")
    public AddEorPermissionPage setAssetExpiryDate(String assetExpiryDate)
    {
        assetExpiryDatepicker.sendKeys(assetExpiryDate);
        assetExpiryDatepicker.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set IMO number")
    public AddEorPermissionPage setImoNumber(String imoNumber)
    {
        imoNumberTextbox.sendKeys(imoNumber);
        imoNumberTextbox.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click 'Add Exhibition of Records Asset' button")
    public ConfirmEorPermissionPage clickAddExhibitionRecordsAssetButton()
    {
        wait.until(ExpectedConditions.elementToBeClickable(addExhibitionOfRecordsAssetButton));
        addExhibitionOfRecordsAssetButton.click();
        return PageFactory.newInstance(ConfirmEorPermissionPage.class);
    }

    @Step("Get Asset Expiry Date Error Message")
    public String getAssetExpiryDateErrorMessage()
    {
        return assetExpiryDatepicker.getErrorMessage();
    }

    @Step("Get Asset Expiry Date")
    public String getAssetExpiryDate()
    {
        return assetExpiryDatepicker.getSelectedDate();
    }

    @Step("Get IMO Number")
    public String getImoNumber()
    {
        return imoNumberTextbox.getText().trim();
    }

    @Step("Is Asset card displayed")
    public boolean isAssetCardDisplayed()
    {
        return assetCard.isDisplayed();
    }

    @Step("Select 'Thickness measurement Reports'")
    public AddEorPermissionPage selectThicknessMeasurementReports()
    {
        thicknessMeasurementReportsSwitch.select();
        return this;
    }

    @Step("Is 'Add Exhibition of Records Asset' button enabled")
    public boolean isAddExhibitionOfRecordsAssetEnabled()
    {
        return addExhibitionOfRecordsAssetButton.isEnabled();
    }
}
