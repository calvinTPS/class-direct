package main.administration.base;

import main.administration.element.DataElement;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Data Elements")
    private List<DataElement> dataElements;

    @Step("Get Data Elements")
    public List<DataElement> getDataElements()
    {
        return dataElements;
    }

}
