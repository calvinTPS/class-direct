package main.administration.filter.element;

import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;

@FindBy(css = "accordion[data-title='cd-account-type'] div.checkbox")
public class AccountTypeCheckbox extends HtmlElement
{
    @Name("Account Type name")
    @FindBy(css = "div.label")
    private WebElement accountTypeName;

    @Name("Account Type Checkbox")
    @FindBy(tagName = "md-checkbox")
    private CheckBox checkbox;

    @Step("Get Account Type Name")
    public String getAccountTypeName()
    {
        return accountTypeName.getText().trim();
    }

    @Step("Select checkbox")
    public void selectCheckbox()
    {
        checkbox.select();
    }

    @Step("Deselect checkbox")
    public void deselectCheckbox()
    {
        checkbox.deselect();
    }

    @Step("Is checkbox selected")
    public boolean isCheckboxSelected()
    {
        return checkbox.isSelected();
    }

}
