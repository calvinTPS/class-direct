package main.administration.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import constant.User.UserAccountTypes;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.filter.element.AccountTypeCheckbox;
import main.common.filter.FilterBarPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class FilterUserPage extends BasePage<FilterUserPage>
{
    @Visible
    @Name("Account Type Header")
    @FindBy(css = "accordion[data-title='cd-account-type'] .accordion-header")
    private WebElement accountTypeHeader;

    @Visible
    @Name("Date of Last Log in Header")
    @FindBy(css = "accordion[data-title='cd-last-login-date'] .accordion-header")
    private WebElement dateOfLastLoginHeader;

    @Visible
    @Name("Submit Button")
    @FindBy(css = "button[aria-label='Submit']")
    private WebElement submitButton;

    @Visible
    @Name("Clear Filters Button")
    @FindBy(css = "button[aria-label='Clear filters']")
    private WebElement clearFiltersButton;

    @Name("Last login Date from Datepicker")
    @FindBy(css = "md-datepicker.date-from")
    private Datepicker lastLoginDateFromDatePicker;

    @Name("Last login Date To Datepicker")
    @FindBy(css = "md-datepicker.date-to")
    private Datepicker lastLoginDateToDatePicker;

    @Name("Account Type Checkbox")
    private List<AccountTypeCheckbox> accountTypeCheckboxList;

    @Step("Click Submit Button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass)
    {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Clear Filter Button")
    public FilterBarPage clickClearFiltersButton()
    {
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Get Account type checkbox list")
    public List<AccountTypeCheckbox> getAccountTypeCheckboxList()
    {
        return accountTypeCheckboxList;
    }

    @Step("Select Account type checkbox")
    public FilterUserPage selectAccountTypeCheckbox(UserAccountTypes accountTypeName)
    {
        accountTypeCheckboxList.stream()
                .filter(e -> accountTypeName.toString().equals(e.getAccountTypeName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Account type not found."))
                .selectCheckbox();
        return this;
    }

    @Step("Deselect Account type checkbox")
    public FilterUserPage deselectAccountTypeCheckbox(UserAccountTypes accountTypeName)
    {
        accountTypeCheckboxList.stream()
                .filter(e -> accountTypeName.toString().equals(e.getAccountTypeName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Account type not found."))
                .deselectCheckbox();
        return this;
    }

    @Step("Set Last login date from - \"{0}\"")
    public FilterUserPage setLastLoginDateFrom(String lastLoginDateFrom)
    {
        lastLoginDateFromDatePicker.sendKeys(lastLoginDateFrom);
        return this;
    }

    @Step("Set Last login date to - \"{0}\"")
    public FilterUserPage setLastLoginDateTo(String lastLoginDateTo)
    {
        lastLoginDateToDatePicker.sendKeys(lastLoginDateTo);
        return this;
    }

}
