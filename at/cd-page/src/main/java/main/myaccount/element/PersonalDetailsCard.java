package main.myaccount.element;

import com.frameworkium.core.ui.annotations.Visible;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

public class PersonalDetailsCard extends HtmlElement
{
    @Visible
    @Name("Personal Details Title")
    @FindBy(css = ".h2-primary")
    private WebElement personalDetailsTitle;

    @Visible
    @Name("Email Address")
    @FindBy(css = ".email")
    private WebElement emailAddress;

    @Name("Telephone Number")
    @FindBy(css = ".telephone")
    private WebElement telephoneNumber;

    @Visible
    @Name("Company Name")
    @FindBy(css = ".company")
    private WebElement companyName;

    @Name("Address Line 1")
    @FindBy(css = ".address-line-1")
    private WebElement addressLine1;

    @Name("Address Line 2")
    @FindBy(css = ".address-line-2")
    private WebElement addressLine2;

    @Name("Address Line 3")
    @FindBy(css = ".address-line-3")
    private WebElement addressLine3;

    @Name("City")
    @FindBy(css = ".city")
    private WebElement city;

    @Name("State/ Province/ Region")
    @FindBy(css = ".state-province-region")
    private WebElement stateProvinceRegion;

    @Name("Postal Code")
    @FindBy(css = ".zip-postal-code")
    private WebElement postalCode;

    @Name("Country")
    @FindBy(css = ".country")
    private WebElement country;

    @Step("Get Personal Details Title")
    public String getPersonalDetailsTitle()
    {
        return personalDetailsTitle.getText().trim();
    }

    @Step("Get Email Address")
    public String getEmailAddress()
    {
        return emailAddress.getText().trim();
    }

    @Step("Get Telephone")
    public String getTelephoneNumber()
    {
        return telephoneNumber.getText().trim();
    }

    @Step("Get Company name")
    public String getCompanyName()
    {
        return companyName.getText().trim();
    }

    @Step("Get Address line 1")
    public String getAddressLine1()
    {
        return addressLine1.getText().trim();
    }

    @Step("Get Address line 2")
    public String getAddressLine2()
    {
        return addressLine2.getText().trim();
    }

    @Step("Get Address line 3")
    public String getAddressLine3()
    {
        return addressLine3.getText().trim();
    }

    @Step("Get City")
    public String getCity()
    {
        return city.getText().trim();
    }

    @Step("Get State/Province/Region")
    public String getStateProvinceRegion()
    {
        return stateProvinceRegion.getText().trim();
    }

    @Step("Get Postal Code")
    public String getPostalCode()
    {
        return postalCode.getText().trim();
    }

    @Step("Get Country")
    public String getCountry()
    {
        return country.getText().trim();
    }
}
