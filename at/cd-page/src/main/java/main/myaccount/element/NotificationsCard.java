package main.myaccount.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Switch;

public class NotificationsCard extends HtmlElement
{
    @Visible
    @Name("Notifications Title")
    @FindBy(css = "md-card-title-text>div")
    private WebElement notificationsTitle;

    @Visible
    @Name("Asset Listings Title")
    @FindBy(css = "md-card-content>div:nth-child(1) div>p:nth-child(1)")
    private WebElement assetListingsTitle;

    @Visible
    @Name("Asset Listings Description")
    @FindBy(css = "md-card-content>div:nth-child(1)>p")
    private WebElement assetListingsDescription;

    @Visible
    @Name("Asset Listings Switch")
    @FindBy(css = "md-card-content>div:nth-child(1) md-switch")
    private Switch assetListingsSwitch;

    @Visible
    @Name("Asset Due Status Title")
    @FindBy(css = "md-card-content>div:nth-child(2) div>p:nth-child(1)")
    private WebElement assetDueStatusTitle;

    @Visible
    @Name("Asset Due Status Description")
    @FindBy(css = "md-card-content>div:nth-child(2)>p")
    private WebElement assetDueStatusDescription;

    @Visible
    @Name("Asset Due Status Switch")
    @FindBy(css = "md-card-content>div:nth-child(2) md-switch")
    private Switch assetDueStatusSwitch;

    @Visible
    @Name("Newly Completed Survey Reports Title")
    @FindBy(css = "md-card-content>div:nth-child(3) div>p:nth-child(1)")
    private WebElement newlyCompletedSurveyReportsTitle;

    @Visible
    @Name("Newly Completed Survey Reports Description")
    @FindBy(css = "md-card-content>div:nth-child(3)>p")
    private WebElement newlyCompletedSurveyReportsDescription;

    @Visible
    @Name("Newly Completed Survey Reports Switch")
    @FindBy(css = "md-card-content>div:nth-child(3) md-switch")
    private Switch newlyCompletedSurveyReportsSwitch;

    @Step("Get Notifications Title")
    public String getNotificationsTitle()
    {
        return notificationsTitle.getText().trim();
    }

    @Step("Get 'Asset Listings' Title")
    public String getAssetListingsTitle()
    {
        return assetListingsTitle.getText().trim();
    }

    @Step("Get 'Asset Due Status' Title")
    public String getAssetDueStatusTitle()
    {
        return assetDueStatusTitle.getText().trim();
    }

    @Step("Get 'Newly Completed Survey Reports' Title")
    public String getNewlyCompletedSurveyReportsTitle()
    {
        return newlyCompletedSurveyReportsTitle.getText().trim();
    }

    @Step("Get 'Asset Listings' Description")
    public String getAssetListingsDescription()
    {
        return assetListingsDescription.getText().trim();
    }

    @Step("Get 'Asset Due Status' Description")
    public String getAssetDueStatusDescription()
    {
        return assetDueStatusDescription.getText().trim();
    }

    @Step("Get 'Newly Completed Survey Reports' Description")
    public String getNewlyCompletedSurveyReportsDescription()
    {
        return newlyCompletedSurveyReportsDescription.getText().trim();
    }

    @Step("Select 'Asset Listing' Switch")
    public NotificationsCard selectAssetListingsSwitch()
    {
        assetListingsSwitch.select();
        return this;
    }

    @Step("Deselect 'Asset Listing' Switch")
    public NotificationsCard deselectAssetListingsSwitch()
    {
        assetListingsSwitch.deselect();
        return this;
    }

    @Step("Is 'Asset Listing' Switch On")
    public boolean isAssetListingsSwitchedOn()
    {
        return assetListingsSwitch.isSelected();
    }

    @Step("Select 'Asset Due Status' Switch")
    public NotificationsCard selectAssetDueStatusSwitch()
    {
        assetDueStatusSwitch.select();
        return this;
    }

    @Step("Deselect 'Asset Due Status' Switch")
    public NotificationsCard deselectAssetDueStatusSwitch()
    {
        assetDueStatusSwitch.deselect();
        return this;
    }

    @Step("Is 'Asset Due Status' Switch On")
    public boolean isAssetDueStatusSwitchedOn()
    {
        return assetDueStatusSwitch.isSelected();
    }

    @Step("Select 'Newly Completed Survey Reports' Switch")
    public NotificationsCard selectNewlyCompletedSurveyReportsSwitch()
    {
        newlyCompletedSurveyReportsSwitch.select();
        return this;
    }

    @Step("Deselect 'Newly Completed Survey Reports' Switch")
    public NotificationsCard deselectNewlyCompletedSurveyReportsSwitch()
    {
        newlyCompletedSurveyReportsSwitch.deselect();
        return this;
    }

    @Step("Is 'Newly Completed Survey Reports' Switched On")
    public boolean isNewlyCompletedSurveyReportsSwitchedOn()
    {
        return newlyCompletedSurveyReportsSwitch.isSelected();
    }
}
