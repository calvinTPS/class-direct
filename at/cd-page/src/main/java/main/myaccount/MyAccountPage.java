package main.myaccount;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import main.myaccount.element.NotificationsCard;
import main.myaccount.element.PersonalDetailsCard;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class MyAccountPage extends BasePage<MyAccountPage>
{
    @Visible
    @Name("Full name")
    @FindBy(css = ".title h1")
    private WebElement fullName;

    @Visible
    @Name("Account Type")
    @FindBy(css = ".title .h4")
    private WebElement accountType;

    @Visible
    @Name("Personal Details card")
    @FindBy(css = "user-details md-card")
    private PersonalDetailsCard personalDetailsCard;

    @Name("Notifications card")
    @FindBy(css = "div.right>div>div:nth-child(1) md-card")
    private NotificationsCard notificationsCard;

    @Name("Logout button")
    @FindBy(css = "[aria-label='Log out']")
    private WebElement logoutButton;

    @Step("Get Full Name")
    public String getFullName()
    {
        return fullName.getText().trim();
    }

    @Step("Get Username")
    public String getAccountType()
    {
        return accountType.getText().trim();
    }

    @Step("Get Personal Details Card")
    public PersonalDetailsCard getPersonalDetailsCard()
    {
        return personalDetailsCard;
    }

    @Step("Get Notifications Card")
    public NotificationsCard getNotificationsCard()
    {
        return notificationsCard;
    }

    @Step("Click Logout button")
    public MyAccountPage clickLogoutButton()
    {
        logoutButton.click();
        return this;
    }
}
