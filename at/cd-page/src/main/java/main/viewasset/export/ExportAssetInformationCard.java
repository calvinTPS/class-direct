package main.viewasset.export;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.common.export.ExportAssetListingPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

@FindBy(css = "div[aria-hidden='false'] export-assets")
public class ExportAssetInformationCard extends HtmlElement
{
    @Visible
    @Name("Export Asset Information button")
    @FindBy(css = "button.flex")
    private WebElement exportAssetInformationButton;

    @Step("Click 'Export Asset Information' button")
    public ExportAssetListingPage clickExportAssetInformationButton()
    {
        exportAssetInformationButton.click();
        return PageFactory.newInstance(ExportAssetListingPage.class);
    }
}
