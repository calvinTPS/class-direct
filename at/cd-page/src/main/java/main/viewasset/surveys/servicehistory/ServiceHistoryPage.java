package main.viewasset.surveys.servicehistory;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.common.filter.FilterBarPage;
import main.viewasset.AssetHubPage;
import main.viewasset.surveys.servicehistory.element.JobElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

public class ServiceHistoryPage extends BasePage<ServiceHistoryPage>
{
    @Visible
    @Name("Job Elements")
    private List<JobElement> jobElements;

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }

    @Step("Click Navigate Back Button")
    public AssetHubPage navigateToAssetHubPage()
    {
        driver.navigate().back();
        return PageFactory.newInstance(AssetHubPage.class);
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Get Job elements")
    public List<JobElement> getJobElements()
    {
        return jobElements;
    }
}
