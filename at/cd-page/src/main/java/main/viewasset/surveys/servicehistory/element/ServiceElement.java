package main.viewasset.surveys.servicehistory.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.ClassDirect.ServiceStatus;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import static constant.ClassDirect.ServiceStatus.*;

@Name("Service Element")
@FindBy(css = "top-services[data-services='vm.item.services'] div[data-ng-if='vm.statusName'")
public class ServiceElement extends HtmlElement
{
    @Visible
    @Name("Service Name")
    @FindBy(css = ".flex-order-2 b")
    private WebElement serviceName;

    @Visible
    @Name("Service Status Icon")
    @FindBy(css = ".flex-order-3 span")
    private WebElement serviceStatusIcon;

    @Step("Get Service Name")
    public String getServiceName()
    {
        return serviceName.getText().trim();
    }

    @Step("Get Service Status by Icon")
    public ServiceStatus getServiceStatusByIcon()
    {
        String serviceStatus = serviceStatusIcon.getAttribute("class");
        if (serviceStatus.toLowerCase().contains(Complete.getServiceStatus().toLowerCase()))
        {
            return Complete;
        }

        if (serviceStatus.toLowerCase().contains("part-held"))
        {
            return PartHeld;
        }
        if (serviceStatus.toLowerCase().contains(Finished.getServiceStatus().toLowerCase()))
        {
            return Finished;
        }
        if (serviceStatus.toLowerCase().contains("not-started"))
        {
            return Not_Started;
        }
        if (serviceStatus.toLowerCase().contains(Unknown.getServiceStatus().toLowerCase()))
        {
            return Unknown;
        }
        if (serviceStatus.toLowerCase().contains(Postponed.getServiceStatus().toLowerCase()))
        {
            return Postponed;
        }
        else
        {
            throw new java.util.NoSuchElementException();
        }
    }
}
