package main.viewasset.surveys.servicehistory.detail;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.viewasset.surveys.servicehistory.detail.element.ServiceElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class JobDetailsPage extends BasePage<JobDetailsPage>
{
    @Visible
    @Name("Job Id")
    @FindBy(css = "page-header div.text h1")
    private WebElement jobId;

    @Visible
    @Name("Job Location")
    @FindBy(css = ".details-content>div.info strong:nth-child(1)")
    private WebElement jobLocation;

    @Visible
    @Name("Last visit date")
    @FindBy(css = ".details-content>div.info strong:nth-child(2)")
    private WebElement lastVisitDate;

    @Visible
    @Name("Lead Surveyor")
    @FindBy(css = "page-header div.text div:nth-child(2)")
    private WebElement leadSurveyor;

    @Name("Service Elements")
    private List<ServiceElement> serviceElements;

    @Name("Download Final Attendance Report Button")
    @FindBy(css = "cd-card-tabular:nth-child(1) div.action")
    private WebElement downloadFARButton;

    @Name("Download Enhanced Survey Programme Report Button")
    @FindBy(css = "cd-card-tabular:nth-child(2) div.action")
    private WebElement downloadESPReportButton;

    @Step("Get Job Id")
    public String getJobId()
    {
        return jobId.getText().trim();
    }

    @Step("Get Job Location")
    public String getJobLocation()
    {
        return jobLocation.getText().trim();
    }

    @Step("Get Lead Surveyor")
    public String getLeadSurveyor()
    {
        return leadSurveyor.getText().trim();
    }

    @Step("Get Last Visit Date")
    public String getLastVisitDate()
    {
        return lastVisitDate.getText().trim();
    }

    @Step("Get Service Elements")
    public List<ServiceElement> getServiceElements()
    {
        return serviceElements;
    }

    @Step("Click Download Final Attendance Report Button")
    public JobDetailsPage clickDownloadFARButton()
    {
        downloadFARButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Download Enhanced Survey Programme Report Button")
    public JobDetailsPage clickDownloadESPReportButton()
    {
        downloadESPReportButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }
}
