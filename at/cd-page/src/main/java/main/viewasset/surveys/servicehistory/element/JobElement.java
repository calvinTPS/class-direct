package main.viewasset.surveys.servicehistory.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.ClassDirect;
import helper.PageFactory;
import main.viewasset.surveys.servicehistory.detail.JobDetailsPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Name("Job Element")
@FindBy(css = "cd-card-tabular md-card-content")
public class JobElement extends HtmlElement
{
    @Visible
    @Name("Job Id")
    @FindBy(css = "div:nth-child(1) div[class*='h3']")
    private WebElement jobId;

    @Visible
    @Name("Lead Surveyor")
    @FindBy(css = "div:nth-child(2) div[class*='field-text']:nth-child(1) div[class='value']")
    private WebElement leadSurveyor;

    @Visible
    @Name("Location")
    @FindBy(css = "div:nth-child(2) div[class*='field-text']:nth-child(2) div[class='value']")
    private WebElement jobLocation;

    @Visible
    @Name("Last Visit Date")
    @FindBy(css = "div:nth-child(2) div[class*='field-text']:nth-child(3) div[class='value']")
    private WebElement lastVisitDate;

    @Name("Services")
    private List<ServiceElement> serviceElements;

    @Name("More Services Count")
    @FindBy(css = "[data-translate='cd-more-services']")
    private WebElement moreServicesCount;

    @Visible
    @Name("Arrow Icon - More Details Button")
    @FindBy(css = "div.action")
    private WebElement moreDetailsButton;

    @Step("Get Job Id")
    public String getJobId()
    {
        return jobId.getText().trim();
    }

    @Step("Get Lead Surveyor")
    public String getLeadSurveyor()
    {
        return leadSurveyor.getText().trim();
    }

    @Step("Get Job Location")
    public String getJobLocation()
    {
        return jobLocation.getText().trim();
    }

    @Step("Get Last Visit Date")
    public Date getLastVisitDate()
    {
        Date date;
        String visitDate = lastVisitDate.getText().trim();
        try
        {
            date = ClassDirect.FRONTEND_DATE_FORMAT.parse(visitDate);
        }
        catch (ParseException e)
        {
            date = null;
        }
        return date;
    }

    @Step("Get Service Elements")
    public List<ServiceElement> getServiceElements()
    {
        return serviceElements;
    }

    @Step("Get more services count")
    public int getMoreServicesCount()
    {
        if (moreServicesCount.getText().trim().isEmpty())
        {
            return 0;
        }
        return Integer.parseInt(moreServicesCount.getText().trim());
    }

    @Step("Click arrow icon - more details button")
    public JobDetailsPage clickMoreDetailsButton()
    {
        moreDetailsButton.click();
        return PageFactory.newInstance(JobDetailsPage.class);
    }
}
