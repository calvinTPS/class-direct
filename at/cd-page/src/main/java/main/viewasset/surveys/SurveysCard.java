package main.viewasset.surveys;

import com.frameworkium.core.ui.annotations.Visible;
import helper.AppHelper;
import helper.PageFactory;
import main.viewasset.surveys.element.SurveysElements;
import main.viewasset.surveys.servicehistory.ServiceHistoryPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = ".asset-hub-container div:nth-child(2) md-card")
public class SurveysCard extends HtmlElement
{

    @Visible
    @Name("View All Button")
    @FindBy(css = ".view-all")
    private WebElement viewAllButton;

    @Name("CodicilsElements")
    private List<SurveysElements> surveysElements;

    @Step("Get Codicils Elements")
    public List<SurveysElements> getSurveysElements()
    {
        return surveysElements;
    }

    @Step("Click View All Button")
    public ServiceHistoryPage clickViewAllButton()
    {
        AppHelper.scrollToMiddle(viewAllButton);
        viewAllButton.click();
        return PageFactory.newInstance(ServiceHistoryPage.class);
    }

}
