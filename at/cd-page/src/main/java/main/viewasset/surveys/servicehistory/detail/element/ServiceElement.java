package main.viewasset.surveys.servicehistory.detail.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.ClassDirect;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.WebElement;

import static constant.ClassDirect.ServiceStatus.*;

@Name("Service Element")
@FindBy(css = "cd-card-tabular[data-ng-repeat='item in vm.job.services'] md-card[class='_md']")
public class ServiceElement extends HtmlElement
{
    @Visible
    @Name("Service Name")
    @FindBy(css = "div .h4-regular")
    private WebElement serviceName;

    @Visible
    @Name("Service Status Name")
    @FindBy(css = ".field-status .label strong")
    private WebElement statusName;

    @Visible
    @Name("Service Status Icon")
    @FindBy(css = ".field-status icon")
    private WebElement statusIcon;

    @Step("Get Service Name")
    public String getServiceName()
    {
        return serviceName.getText().trim();
    }

    @Step("Get Status Name")
    public String getStatusName()
    {
        return statusName.getText().trim();
    }

    @Step("Get Service Status by Icon")
    public ClassDirect.ServiceStatus getServiceStatusByIcon()
    {
        String serviceStatus = statusIcon.getAttribute("class");
        if (serviceStatus.toLowerCase().contains(Complete.getServiceStatus().toLowerCase()))
        {
            return Complete;
        }
        if (serviceStatus.toLowerCase().contains("part-held"))
        {
            return PartHeld;
        }
        if (serviceStatus.toLowerCase().contains("not-started"))
        {
            return Not_Started;
        }
        if (serviceStatus.toLowerCase().contains(Finished.getServiceStatus().toLowerCase()))
        {
            return Finished;
        }
        if (serviceStatus.toLowerCase().contains(Unknown.getServiceStatus().toLowerCase()))
        {
            return Unknown;
        }
        if (serviceStatus.toLowerCase().contains(Postponed.getServiceStatus().toLowerCase()))
        {
            return Postponed;
        }
        else
        {
            throw new java.util.NoSuchElementException();
        }
    }
}
