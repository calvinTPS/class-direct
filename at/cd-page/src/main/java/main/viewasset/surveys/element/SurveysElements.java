package main.viewasset.surveys.element;

import com.frameworkium.core.ui.annotations.Visible;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(css = ".item .layout-row")
public class SurveysElements extends HtmlElement
{
    @Visible
    @Name("Survey Count")
    @FindBy(css = ".details")
    private WebElement surveyCount;

    @Visible
    @Name("Survey Name")
    @FindBy(css = ".name")
    private WebElement surveyName;

    @Visible
    @Name("Survey Date")
    @FindBy(css = ".date")
    private WebElement surveyDate;

    @Step("Get Survey Count")
    public String getSurveyCount()
    {
        return surveyCount.getText().trim();
    }

    @Step("Get Survey Date")
    public String getSurveyDate()
    {
        return surveyDate.getText().trim();
    }
}
