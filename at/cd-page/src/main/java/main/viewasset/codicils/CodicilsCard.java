package main.viewasset.codicils;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import helper.PageFactory;
import main.viewasset.codicils.codicilsanddefects.CodicilsAndDefectsPage;
import main.viewasset.codicils.element.CodicilsElements;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

@FindBy(css = "asset-hub .block-codicils")
public class CodicilsCard extends HtmlElement {

    @Visible
    @Name("View All Button")
    @FindBy(css = ".view-all")
    private WebElement viewAllButton;

    @Name("CodicilsElements")
    private List<CodicilsElements> codicilsElements;

    @Step("Get Codicils Elements")
    public List<CodicilsElements> getCodicilsElements() {
        return codicilsElements;
    }

    @Step("Click View All Button")
    public CodicilsAndDefectsPage clickViewAllButton() {
        viewAllButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

}
