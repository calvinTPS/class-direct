package main.viewasset.codicils.element;

/**
 * Use as parameter for {@link main.viewasset.codicils.element.ScrollableTabContainer#clickTab(Tab)}
 */
public enum Tab
{
    CONDITIONS_OF_CLASS,
    DEFECTS,
    STATUTORY_FINDINGS,
    STATUTORY_DEFICIENCIES,
    ACTIONABLE_ITEMS,
    ASSET_NOTES
}
