package main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.RepairDetailsOverlayPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(css = "[aria-label='cd-associated-repairs'] md-card-content")
public class RepairElement extends HtmlElement
{
    @Visible
    @Name("Repair Title")
    @FindBy(css = "riders>div>div>div>div:nth-child(1)")
    private WebElement repairTitle;

    @Visible
    @Name("Repair Type")
    @FindBy(css = "riders>div>div>div>div:nth-child(3)")
    private WebElement repairType;

    @Visible
    @Name("Date Added")
    @FindBy(css = "riders>div>div>div>div:nth-child(5) strong")
    private WebElement dateAdded;

    @Visible
    @Name("Arrow button")
    @FindBy(css = "div.action icon")
    private WebElement arrowButton;

    @Step("Get Repair Title")
    public String getRepairTitle()
    {
        return repairTitle.getText();
    }

    @Step("Get Repair Type")
    public String getRepairType()
    {
        return repairType.getText();
    }

    @Step("Get Date Added")
    public String getDateAdded()
    {
        return dateAdded.getText();
    }

    @Step("Click Arrow button")
    public RepairDetailsOverlayPage clickArrowButton()
    {
        arrowButton.click();
        return PageFactory.newInstance(RepairDetailsOverlayPage.class);
    }
}
