package main.viewasset.codicils.codicilsanddefects.sub.defects.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.viewasset.codicils.codicilsanddefects.sub.defects.DefectsPage;
import main.viewasset.codicils.codicilsanddefects.sub.defects.filter.element.CategoryCheckbox;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;
import typifiedelement.RadioButton;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class FilterDefectsPage extends BasePage<FilterDefectsPage>
{
    @Visible
    @Name("Status Header")
    @FindBy(css = "accordion[data-title='cd-status'] .accordion-header")
    private WebElement statusHeader;

    @Visible
    @Name("Category Header")
    @FindBy(css = "accordion[data-title='cd-category'] .accordion-header")
    private WebElement categoryHeader;

    @Visible
    @Name("Date occurred Header")
    @FindBy(css = "accordion[data-title='cd-date-occurred'] .accordion-header")
    private WebElement dateOccuredHeader;

    @Visible
    @Name("Submit button")
    @FindBy(className = "modal-submit-button")
    private WebElement submitButton;

    @Visible
    @Name("Clear filters button")
    @FindBy(className = "modal-clear-button")
    private WebElement clearFiltersButton;

    @Name("Open radio button")
    @FindBy(css = "md-radio-button[aria-label='Open']")
    private RadioButton openRadioButton;

    @Name("Closed radio button")
    @FindBy(css = "md-radio-button[aria-label='Closed']")
    private RadioButton closedRadioButton;

    @Name("Cancelled radio button")
    @FindBy(css = "md-radio-button[aria-label='Cancelled']")
    private RadioButton cancelledRadioButton;

    @Name("Category checkboxes")
    private List<CategoryCheckbox> categoryCheckboxList;

    @Name("Date Occurred 'from' Datepicker")
    @FindBy(css = "md-datepicker.date-from")
    private Datepicker dateOccurredFromDatePicker;

    @Name("Date Occurred 'To' Datepicker")
    @FindBy(css = "md-datepicker.date-to")
    private Datepicker dateOccurredToDatePicker;

    @Step("Click Submit button")
    public DefectsPage clickSubmitButton()
    {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(DefectsPage.class);
    }

    @Step("Click Clear filters button")
    public DefectsPage clickClearFiltersButton()
    {
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(DefectsPage.class);
    }

    @Step("Select Open radio button")
    public FilterDefectsPage selectOpenRadioButton()
    {
        openRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Closed radio button")
    public FilterDefectsPage selectClosedRadioButton()
    {
        closedRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Cancelled radio button")
    public FilterDefectsPage selectCancelledRadioButton()
    {
        cancelledRadioButton.select();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Category checkbox list")
    public List<CategoryCheckbox> getCategoryCheckboxList()
    {
        return categoryCheckboxList;
    }

    @Step("Select Account type checkbox")
    public FilterDefectsPage selectCategoryCheckbox(String categoryName)
    {
        categoryCheckboxList.stream()
                .filter(e -> categoryName.equals(e.getCategoryName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("%s category not found.", categoryName)))
                .selectCheckbox();
        return this;
    }

    @Step("Deselect Category checkbox")
    public FilterDefectsPage deselectCategoryCheckbox(String categoryName)
    {
        categoryCheckboxList.stream()
                .filter(e -> categoryName.equals(e.getCategoryName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("%s category not found.", categoryName)))
                .deselectCheckbox();
        return this;
    }

    @Step("Set Date Occurred from")
    public FilterDefectsPage setDateOccurredFrom(String dateOccurredFrom)
    {
        dateOccurredFromDatePicker.sendKeys(dateOccurredFrom);
        return this;
    }

    @Step("Set Date Occurred to")
    public FilterDefectsPage setDateOccurredTo(String dateOccurredTo)
    {
        dateOccurredToDatePicker.sendKeys(dateOccurredTo);
        return this;
    }

    @Step("Is Open radio button selected")
    public boolean isOpenRadioButtonSelected()
    {
        return openRadioButton.isSelected();
    }

    @Step("Is From Date Occcured date picker is displayed")
    public boolean isDateOccuredFromDatePickerDisplayed()
    {
        return dateOccurredFromDatePicker.isDisplayed();
    }

    @Step("Is To Date Occcured date picker is displayed")
    public boolean isDateOccuredToDatePickerDisplayed()
    {
        return dateOccurredToDatePicker.isDisplayed();
    }
}
