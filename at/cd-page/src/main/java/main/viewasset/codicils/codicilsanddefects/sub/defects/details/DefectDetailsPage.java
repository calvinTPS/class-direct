package main.viewasset.codicils.codicilsanddefects.sub.defects.details;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.common.element.CodicilElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class DefectDetailsPage extends BasePage<DefectDetailsPage>
{
    @Visible
    @Name("Defect Title")
    @FindBy(css = ".defect")
    private WebElement defectTitle;

    @Visible
    @Name("Defect Category")
    @FindBy(css = ".category")
    private WebElement defectCategory;

    @Visible
    @Name("Date Occurred")
    @FindBy(css = ".date-occurred")
    private WebElement dateOccurred;

    @Visible
    @Name("Items")
    @FindBy(css = ".items")
    private WebElement items;

    @Visible
    @Name("Defect Status")
    @FindBy(css = "active-status b")
    private WebElement defectStatus;

    @Visible
    @Name("Job number")
    @FindBy(css = ".job-number")
    private WebElement jobNumber;

    @Visible
    @Name("Description")
    @FindBy(css = ".description")
    private WebElement description;

    @Name("Associated Condition of Class")
    @FindBy(css = "[aria-label='cd-associated-conditions-of-class']")
    private List<CodicilElement> associatedConditionOfClass;

    @Name("Download Final Service Report icon")
    @FindBy(css = ".action icon")
    private WebElement downloadFinalServiceReportIcon;

    @Step("Click Download Final Service Report icon")
    public DefectDetailsPage clickDownloadFinalServiceReportIcon()
    {
        downloadFinalServiceReportIcon.click();
        return this;
    }

    @Step("Get Defect Title")
    public String getDefectTitle()
    {
        return defectTitle.getText().trim();
    }

    @Step("Get Defect Category")
    public String getDefectCategory()
    {
        return defectCategory.getText().trim();
    }

    @Step("Get Date Occurred")
    public String getDateOccurred()
    {
        return dateOccurred.getText().trim();
    }

    @Step("Get Items")
    public String getItems()
    {
        return items.getText().trim();
    }

    @Step("Get Defect Status")
    public String getDefectStatus()
    {
        return defectStatus.getText().trim();
    }

    @Step("Get Job number")
    public String getJobNumber()
    {
        return jobNumber.getText().trim();
    }

    @Step("Get Description")
    public String getDescription()
    {
        return description.getText().trim();
    }

    @Step("Get Associated Condition of Class")
    public List<CodicilElement> getAssociatedConditionOfClass()
    {
        return associatedConditionOfClass;
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }
}
