package main.viewasset.codicils.codicilsanddefects.sub.defects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import main.common.filter.FilterBarPage;
import main.common.element.DefectElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class DefectsPage extends BasePage<DefectsPage>
{
    @Visible
    @Name("Defect Elements")
    @FindBy(css = "div.rider-items-container md-card")
    private List<DefectElement> defectElements;

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage(){
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Get Defect Elements")
    public List<DefectElement> getDefectElements()
    {
        return defectElements;
    }
}
