package main.viewasset.codicils.codicilsanddefects.sub.assetnotes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import main.common.element.CodicilElement;
import main.common.filter.FilterBarPage;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class AssetNotesPage extends BasePage<AssetNotesPage>
{
    @Visible
    @Name("Codicil Elements")
    private List<CodicilElement> codicilElements;

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage(){
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Get Codicil Elements")
    public List<CodicilElement> getCodicilElements()
    {
        return codicilElements;
    }
}
