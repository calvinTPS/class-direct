package main.viewasset.codicils.element;

import com.frameworkium.core.ui.annotations.Visible;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(css = ".item .layout-row")
public class CodicilsElements extends HtmlElement
{
    @Visible
    @Name("Codicils Description")
    @FindBy(css = ".details.flex div:nth-child(1)")
    private WebElement codicilsDescriptionText;

    @Visible
    @Name("Codicils Date")
    @FindBy(css = ".details.flex div:nth-child(2)")
    private WebElement codicilsDate;
}
