package main.viewasset.codicils.codicilsanddefects.sub.assetnotes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import main.common.AssetHeaderPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class AssetNoteDetailsPage extends BasePage<AssetNoteDetailsPage>
{
    @Visible
    @Name("Title")
    @FindBy(css = "page-header .title")
    private WebElement title;

    @Visible
    @Name("Note and Action")
    @FindBy(className = "note-and-action")
    private WebElement noteAndAction;

    @Visible
    @Name("Category")
    @FindBy(css = ".category")
    private WebElement category;

    @Visible
    @Name("Imposed date")
    @FindBy(css = ".imposed-date")
    private WebElement imposedDate;

    @Visible
    @Name("Items")
    @FindBy(css = ".items")
    private WebElement items;

    @Visible
    @Name("Status")
    @FindBy(css = "active-status b")
    private WebElement status;

    @Visible
    @Name("Job number")
    @FindBy(css = ".job-number")
    private WebElement jobNumber;

    @Visible
    @Name("Description")
    @FindBy(css = ".description")
    private WebElement description;

    @Name("Download Final Service Report icon")
    @FindBy(css = ".action icon")
    private WebElement downloadFinalServiceReportIcon;

    @Step("Click Download Final Service Report icon")
    public AssetNoteDetailsPage clickDownloadFinalServiceReportIcon()
    {
        downloadFinalServiceReportIcon.click();
        return this;
    }

    @Step("Get Title")
    public String getTitle()
    {
        return title.getText();
    }

    @Step("Get Note and Action")
    public String getNoteAndAction()
    {
        return noteAndAction.getText();
    }

    @Step("Get Category")
    public String getCategory()
    {
        return category.getText();
    }

    @Step("Get Imposed date")
    public String getImposedDate()
    {
        return imposedDate.getText();
    }

    @Step("Get Items")
    public String getItems()
    {
        return items.getText();
    }

    @Step("Get Status")
    public String getStatus()
    {
        return status.getText();
    }

    @Step("Get Job number")
    public String getJobNumber()
    {
        return jobNumber.getText();
    }

    @Step("Get Description")
    public String getDescription()
    {
        return description.getText();
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }
}
