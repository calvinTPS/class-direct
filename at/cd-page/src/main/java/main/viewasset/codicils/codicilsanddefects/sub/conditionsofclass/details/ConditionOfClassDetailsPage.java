package main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.common.element.DefectElement;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details.element.RepairElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class ConditionOfClassDetailsPage extends BasePage<ConditionOfClassDetailsPage>
{
    @Visible
    @Name("Title")
    @FindBy(css = "page-header .title h1")
    private WebElement title;

    @Visible
    @Name("Note and Action")
    @FindBy(className = "note-and-action")
    private WebElement noteAndAction;

    @Visible
    @Name("Category")
    @FindBy(css = ".category")
    private WebElement category;

    @Visible
    @Name("Imposed Date")
    @FindBy(css = ".imposed-date")
    private WebElement imposedDate;

    @Visible
    @Name("Due Date")
    @FindBy(css = ".due-date")
    private WebElement dueDate;

    @Visible
    @Name("Status")
    @FindBy(css = "active-status b")
    private WebElement status;

    @Visible
    @Name("Job Number")
    @FindBy(css = ".job-number")
    private WebElement jobNumber;

    @Name("Description")
    @FindBy(css = ".description")
    private WebElement description;

    @Name("Associated Class Defects")
    private List<DefectElement> associatedClassDefects;

    @Name("Associated Repairs")
    private List<RepairElement> associatedRepairs;

    @Name("Download Final Service Report icon")
    @FindBy(css = ".action icon")
    private WebElement downloadFinalServiceReportIcon;

    @Step("Click Download Final Service Report icon")
    public ConditionOfClassDetailsPage clickDownloadFinalServiceReportIcon()
    {
        downloadFinalServiceReportIcon.click();
        return this;
    }

    @Step("Get Title")
    public String getTitle()
    {
        return title.getText();
    }

    @Step("Get Note and Action")
    public String getNoteAndAction()
    {
        return noteAndAction.getText();
    }

    @Step("Get Category")
    public String getCategory()
    {
        return category.getText();
    }

    @Step("Get Imposed Date")
    public String getImposedDate()
    {
        return imposedDate.getText();
    }

    @Step("Get Due Date")
    public String getDueDate()
    {
        return dueDate.getText();
    }

    @Step("Get Status")
    public String getStatus()
    {
        return status.getText();
    }

    @Step("Get Job Number")
    public String getJobNumber()
    {
        return jobNumber.getText();
    }

    @Step("Get Description")
    public String getDescription()
    {
        return description.getText();
    }

    @Step("Get Associated Class Defects")
    public List<DefectElement> getAssociatedClassDefects()
    {
        return associatedClassDefects;
    }

    @Step("Get Associated Repairs")
    public List<RepairElement> getAssociatedRepairs()
    {
        return associatedRepairs;
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }
}
