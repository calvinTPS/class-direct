package main.viewasset.codicils.codicilsanddefects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.viewasset.AssetHubPage;
import main.viewasset.codicils.codicilsanddefects.sub.actionableitems.ActionableItemsPage;
import main.viewasset.codicils.codicilsanddefects.sub.assetnotes.AssetNotesPage;
import main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.ConditionsOfClassPage;
import main.viewasset.codicils.codicilsanddefects.sub.defects.DefectsPage;
import main.viewasset.codicils.element.ScrollableTabContainer;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import static main.viewasset.codicils.element.Tab.*;

public class CodicilsAndDefectsPage extends BasePage<CodicilsAndDefectsPage>
{
    @Visible
    @Name("Tabs scrolling container")
    @FindBy(css = "md-tabs.scrollable-tabs")
    private ScrollableTabContainer scrollableTabContainer;

    @Visible
    @Name("Export button")
    @FindBy(css = "export small")
    private WebElement exportButton;

    @Step("Click Conditions of Class Tab")
    public ConditionsOfClassPage clickConditionsOfClassTab()
    {
        scrollableTabContainer.clickTab(CONDITIONS_OF_CLASS);
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }

    @Step("Click Actionable Items Tab")
    public ActionableItemsPage clickActionableItemsTab()
    {
        scrollableTabContainer.clickTab(ACTIONABLE_ITEMS);
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

    @Step("Click Asset Notes Tab")
    public AssetNotesPage clickAssetNotesTab()
    {
        scrollableTabContainer.clickTab(ASSET_NOTES);
        return PageFactory.newInstance(AssetNotesPage.class);
    }

    @Step("Click Defects Tab")
    public DefectsPage clickDefectsTab()
    {
        scrollableTabContainer.clickTab(DEFECTS);
        return PageFactory.newInstance(DefectsPage.class);
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }

    @Step("Click Export Button")
    public CodicilsAndDefectsPage clickExportButton()
    {
        exportButton.click();
        return this;
    }

    @Step("Is Condition of Class tab selected")
    public boolean isConditionOfClassTabSelected()
    {
        return scrollableTabContainer.isTabSelected(CONDITIONS_OF_CLASS);
    }

    @Step("Is Actionable Items tab selected")
    public boolean isActionableItemsTabSelected()
    {
        return scrollableTabContainer.isTabSelected(ACTIONABLE_ITEMS);
    }

    @Step("Is Asset Note tab selected")
    public boolean isAssetNotesTabSelected()
    {
        return scrollableTabContainer.isTabSelected(ASSET_NOTES);
    }

    @Step("Is Defects tab selected")
    public boolean isDefectsTabSelected()
    {
        return scrollableTabContainer.isTabSelected(DEFECTS);
    }
}
