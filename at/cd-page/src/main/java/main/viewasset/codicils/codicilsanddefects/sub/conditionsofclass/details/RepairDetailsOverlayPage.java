package main.viewasset.codicils.codicilsanddefects.sub.conditionsofclass.details;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class RepairDetailsOverlayPage extends BasePage<RepairDetailsOverlayPage>
{
    @Visible
    @Name("Repair Title")
    @FindBy(css = "div.modal-title")
    private WebElement repairTitle;

    @Visible
    @Name("Close button")
    @FindBy(css = "md-toolbar .close-modal-button")
    private WebElement closeButton;

    @Visible
    @Name("Repair Type")
    @FindBy(css = "div.repair-type")
    private WebElement repairType;

    @Visible
    @Name("Repair Action")
    @FindBy(css = "div.repair-action")
    private WebElement repairAction;

    @Visible
    @Name("Date Added")
    @FindBy(css = "div.date-added")
    private WebElement dateAdded;

    @Visible
    @Name("Items")
    @FindBy(css = "md-dialog-content div.items")
    private WebElement items;

    @Visible
    @Name("Description")
    @FindBy(css = "md-dialog-content div.description")
    private WebElement description;

    @Step("Get Repair Title")
    public String getRepairTitle()
    {
        return repairTitle.getText();
    }

    @Step("Get Repair Type")
    public String getRepairType()
    {
        return repairType.getText();
    }

    @Step("Get Repair Action")
    public String getRepairAction()
    {
        return repairAction.getText();
    }

    @Step("Get Date Added")
    public String getDateAdded()
    {
        return dateAdded.getText();
    }

    @Step("Get Items")
    public String getItems()
    {
        return items.getText();
    }

    @Step("Get Description")
    public String getDescription()
    {
        return description.getText();
    }

    @Step("Click Close button")
    public ConditionOfClassDetailsPage clickCloseButton()
    {
        closeButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(ConditionOfClassDetailsPage.class);
    }
}
