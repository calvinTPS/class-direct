package main.viewasset.codicils.element;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.ExtraExpectedConditions;
import helper.HtmlElement;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.concurrent.TimeUnit;

import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;
import static constant.Config.GLOBAL_SPINNER_SPAWN_TIME;

//ScrollableTabContainer was written in a way to preserve test functions due to a major refactor triggered by a UI change
public class ScrollableTabContainer extends HtmlElement
{
    @Name("Previous button")
    @FindBy(css = "md-prev-button")
    private WebElement previousButton;

    @Name("Next button")
    @FindBy(css = "md-next-button")
    private WebElement nextButton;

    @Name("Conditions of Class tab")
    @FindBy(css = "md-tab-item .tab-coc")
    private WebElement conditionsOfClassTab;

    @Name("Defects tab")
    @FindBy(css = "md-tab-item .tab-defects")
    private WebElement defectsTab;

    @Name("Statutory Findings tab")
    @FindBy(css = "md-tab-item .tab-statutory-findings")
    private WebElement statutoryTab;

    @Name("Statutory Deficiencies tab")
    @FindBy(css = "md-tab-item .tab-statutory-deficiencies")
    private WebElement statutoryDeficiencyTab;

    @Name("Actionable Items tab")
    @FindBy(css = "md-tab-item .tab-actionable-items")
    private WebElement actionableItemsTab;

    @Name("Asset Notes")
    @FindBy(css = "md-tab-item .tab-asset-notes")
    private WebElement assetNotesTab;

    @Step("Click Conditions of Class Tab - \"{0}\"")
    public ScrollableTabContainer clickTab(Tab tab)
    {
        WebElement we = toWebElement(tab);
        find(we);
        we.click();
        waitForDelayedSpinnerSpawn();
        return this;
    }

    @Step("Is tab selected - \"{0}\"")
    public boolean isTabSelected(Tab tab)
    {
        return Boolean.parseBoolean(toWebElement(tab).findElement(By.xpath("./ancestor-or-self::md-tab-item")).getAttribute("aria-selected"));
    }

    /**
     * Convert the tab from {@link Tab} to appropriate WebElements
     *
     * @param tab the tab we want to convert
     * @return the tab as a WebElement
     */
    private WebElement toWebElement(Tab tab)
    {
        WebElement we = null;
        switch (tab)
        {
            case DEFECTS:
                we = defectsTab;
                break;
            case ACTIONABLE_ITEMS:
                we = actionableItemsTab;
                break;
            case ASSET_NOTES:
                we = assetNotesTab;
                break;
            case STATUTORY_FINDINGS:
                we = statutoryTab;
                break;
            case CONDITIONS_OF_CLASS:
                we = conditionsOfClassTab;
                break;
            case STATUTORY_DEFICIENCIES:
                we = statutoryDeficiencyTab;
                break;
        }
        return we;
    }

    /**
     * Finds the tab which may be hidden in the scrolling mechanism especially on lower screen resolutions
     *
     * @param tab the tab that we want to find
     * @return whether the tab was found
     */
    @Step("Finding tab - \"{0}\"")
    private boolean find(WebElement tab)
    {
        boolean found = tab.isDisplayed();
        //check left until end
        while (!found && isEnabled(previousButton))
        {
            previousButton.click();
            found = tab.isDisplayed();
        }
        //check right until end
        while (!found && isEnabled(nextButton))
        {
            nextButton.click();
            found = tab.isDisplayed();
        }
        return found;
    }

    private void waitForDelayedSpinnerSpawn()
    {
        try
        {
            BaseTest.newWaitWithTimeout(GLOBAL_SPINNER_SPAWN_TIME).until(ExtraExpectedConditions.visibilityOfSpinner());
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
        }
        catch (TimeoutException ignore)
        {

        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
    }

    /**
     * Returns state of a web element; used particularly in {@link ScrollableTabContainer}  due to Angular Material
     * @param webElement the web element to check enable status for
     * @return <code>true</code> if webelement is enabled; <code>false</code> otherwise
     */
    private boolean isEnabled(WebElement webElement)
    {
        return !webElement.getAttribute("class").contains("md-disabled");
    }

}
