package main.viewasset.codicils.codicilsanddefects.sub.defects.filter.element;

import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;

@FindBy(css = "accordion div.checkbox")
public class CategoryCheckbox extends HtmlElement
{
    @Name("Category name")
    @FindBy(css = "div.label")
    private WebElement categoryName;

    @Name("Category Checkbox")
    @FindBy(tagName = "md-checkbox")
    private CheckBox checkbox;

    @Step("Get Category Name")
    public String getCategoryName()
    {
        return categoryName.getText().trim();
    }

    @Step("Select checkbox")
    public void selectCheckbox()
    {
        checkbox.select();
    }

    @Step("Deselect checkbox")
    public void deselectCheckbox()
    {
        checkbox.deselect();
    }

    @Step("Is checkbox selected")
    public boolean isCheckboxSelected()
    {
        return checkbox.isSelected();
    }
}
