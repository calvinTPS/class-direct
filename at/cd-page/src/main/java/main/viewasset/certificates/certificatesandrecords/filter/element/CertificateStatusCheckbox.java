package main.viewasset.certificates.certificatesandrecords.filter.element;

import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;
import typifiedelement.WebElement;

@FindBy(css = ".filter div.checkbox")
public class CertificateStatusCheckbox extends HtmlElement
{
    @Name("Certificate Status name")
    @FindBy(css = "div.label")
    private WebElement certificateStatusName;

    @Name("Certificate Status Checkbox")
    @FindBy(tagName = "md-checkbox")
    private CheckBox checkbox;

    @Step("Get Certificate Status Name")
    public String getCertificateStatusName()
    {
        return certificateStatusName.getText().trim();
    }

    @Step("Select checkbox")
    public void selectCheckbox()
    {
        checkbox.select();
    }

    @Step("Deselect checkbox")
    public void deselectCheckbox()
    {
        checkbox.deselect();
    }

    @Step("Is checkbox selected")
    public boolean isCheckboxSelected()
    {
        return checkbox.isSelected();
    }
}
