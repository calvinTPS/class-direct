package main.viewasset.certificates.certificatesandrecords.filter;

import com.frameworkium.core.ui.pages.BasePage;
import constant.ClassDirect;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.common.filter.FilterBarPage;
import main.viewasset.certificates.certificatesandrecords.filter.element.CertificateStatusCheckbox;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ByChained;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

public class FilterCertificatePage extends BasePage<FilterCertificatePage>
{
    @Name("Submit Button")
    @FindBy(css = "button[aria-label='Submit']")
    private WebElement submitButton;

    @Name("Clear Filters Button")
    @FindBy(css = "button[aria-label='Clear filters']")
    private WebElement clearFiltersButton;

    @Name("'Issued On' Header")
    @FindBy(css = "accordion[data-title='cd-issued-on'] .accordion-header")
    private WebElement issuedOnHeader;

    @Name("'Expires On' Header")
    @FindBy(css = "accordion[data-title='cd-expires-on'] .accordion-header")
    private WebElement expiresOnHeader;

    @Name("'Certificate Status' Header")
    @FindBy(css = "accordion[data-title='cd-certificate-status'] .accordion-header")
    private WebElement certificateStatusHeader;

    @Name("From Issued Date Text Input")
    @FindBy(css = "div.widget-container:nth-child(2) date-range .date-from")
    private Datepicker fromIssuedDate;

    @Name("To Issued Date Text Input")
    @FindBy(css = "div.widget-container:nth-child(2) date-range .date-to")
    private Datepicker toIssuedDate;

    @Name("From Expiry Date Text Input")
    @FindBy(css = "div.widget-container:nth-child(3) date-range .date-from")
    private Datepicker fromExpiryDate;

    @Name("To Expiry Date Text Input")
    @FindBy(css = "div.widget-container:nth-child(3) date-range .date-to")
    private Datepicker toExpiryDate;

    @Name("Certificate Status Checkbox")
    private List<CertificateStatusCheckbox> certificateStatusCheckboxList;

    @Step("Set From 'Issued Date' \"{0}\"")
    public FilterCertificatePage setFromIssuedDate(String date) {
        fromIssuedDate.clear();
        fromIssuedDate.sendKeys(date);
        fromIssuedDate.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set To 'Issued Date' \"{0}\"")
    public FilterCertificatePage setToIssuedDate(String date) {
        toIssuedDate.clear();
        toIssuedDate.sendKeys(date);
        toIssuedDate.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set From 'Expiry Date' \"{0}\"")
    public FilterCertificatePage setFromExpiryDate(String date) {
        fromExpiryDate.clear();
        fromExpiryDate.sendKeys(date);
        fromExpiryDate.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set To 'Expiry Date' \"{0}\"")
    public FilterCertificatePage setToExpiryDate(String date) {
        toExpiryDate.clear();
        toExpiryDate.sendKeys(date);
        toExpiryDate.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Certificate Status: \"{0}\" checkbox")
    public FilterCertificatePage selectCertificateStatusCheckbox(ClassDirect.CertificateStatus certificateStatus)
    {
        certificateStatusCheckboxList.stream()
                .filter(e -> certificateStatus.toString().equals(e.getCertificateStatusName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Certificate Status not found."))
                .selectCheckbox();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Deselect Certificate Status: \"{0}\" checkbox")
    public FilterCertificatePage deselectCertificateStatusCheckbox(ClassDirect.CertificateStatus certificateStatus)
    {
        certificateStatusCheckboxList.stream()
                .filter(e -> certificateStatus.toString().equals(e.getCertificateStatusName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Expected Certificate Status not found."))
                .deselectCheckbox();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Certificate Status list")
    public List<CertificateStatusCheckbox> getCertificateStatusCheckboxList()
    {
        return certificateStatusCheckboxList;
    }

    @Step("Click Submit Button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass) {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Clear Filter Button")
    public FilterBarPage clickClearFiltersButton() {
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(FilterBarPage.class);
    }
}
