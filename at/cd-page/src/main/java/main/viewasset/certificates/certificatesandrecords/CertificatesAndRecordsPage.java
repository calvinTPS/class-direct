package main.viewasset.certificates.certificatesandrecords;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import main.common.AssetHeaderPage;
import main.common.filter.FilterBarPage;
import main.mobile.MobileFilterBarPage;
import main.viewasset.AssetHubPage;
import main.viewasset.certificates.certificatesandrecords.element.CertificateElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

public class CertificatesAndRecordsPage extends BasePage<CertificatesAndRecordsPage>
{
    @Visible
    @Name("Title")
    @FindBy(css = "div.title")
    private WebElement title;

    @Name("Certificate Elements")
    private List<CertificateElement> certificateElements;

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage(){
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Get Mobile Filter Bar Page")
    public MobileFilterBarPage getMobileFilterBarPage(){
        return PageFactory.newInstance(MobileFilterBarPage.class);
    }

    @Step("Get Certificate Elements")
    public List<CertificateElement> getCertificateElements()
    {
        return certificateElements;
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }
}
