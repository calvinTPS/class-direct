package main.viewasset.certificates.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

@FindBy(css = "div.block-certificates .item")
public class CertificateElement extends HtmlElement
{
    @Visible
    @Name("Certificate Number")
    @FindBy(css = "div.name strong")
    private WebElement certificateNumber;

    @Visible
    @Name("Certificate Issued Date")
    @FindBy(css = "div.date")
    private WebElement issuedDate;

    @Visible
    @Name("Arrow button")
    @FindBy(css = "div.arrow")
    private WebElement arrowButton;

    @Step("Get Certificate number")
    public String getCertificateNumber()
    {
        return certificateNumber.getText().trim();
    }

    @Step("Get Issued Date")
    public String getIssuedDate()
    {
        return issuedDate.getText().trim();
    }

    @Step("Click Arrow Button")
    public CertificateElement clickArrowButton()
    {
        arrowButton.click();
        return this;
    }
}
