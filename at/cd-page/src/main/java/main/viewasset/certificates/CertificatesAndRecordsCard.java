package main.viewasset.certificates;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.ExtraExpectedConditions;
import helper.HtmlElement;
import helper.PageFactory;
import main.viewasset.certificates.certificatesandrecords.CertificatesAndRecordsPage;
import main.viewasset.certificates.element.CertificateElement;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;
import static constant.Config.GLOBAL_SPINNER_SPAWN_TIME;

@FindBy(css = "asset-hub .block-certificates")
public class CertificatesAndRecordsCard extends HtmlElement
{
    @Name("View All Button")
    @FindBy(className = "view-all")
    private WebElement viewAllButton;

    @Name("Certificate Elements")
    private List<CertificateElement> certificateElements;

    @Step("Click View All Button")
    public CertificatesAndRecordsPage clickViewAllButton() {
        viewAllButton.click();
        try
        {
            BaseTest.newWaitWithTimeout(GLOBAL_SPINNER_SPAWN_TIME).until(ExtraExpectedConditions.visibilityOfSpinner());
            Sleeper.SYSTEM_SLEEPER.sleep(new Duration(GLOBAL_ANIMATION_TRANSITION_TIME, TimeUnit.MILLISECONDS));
        }
        catch (TimeoutException ignore)
        {

        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());

        return PageFactory.newInstance(CertificatesAndRecordsPage.class);
    }

    @Step("Get Certificate Elements")
    public List<CertificateElement> getCertificateElements()
    {
        return certificateElements;
    }
}
