package main.viewasset.certificates.certificatesandrecords.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.ClassDirect;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import static constant.Config.GLOBAL_ANIMATION_TRANSITION_TIME;

@FindBy(css = "div.cd-card-context md-card-content")
public class CertificateElement extends HtmlElement
{
    @Visible
    @Name("Issuing Surveyor")
    @FindBy(css = "riders>div>div>div:nth-child(3)>div:nth-child(1) strong")
    private WebElement issuingSurveyor;

    @Name("Certificate Name")
    @FindBy(css = "div.dark:nth-child(1)")
    private List<WebElement> certificateName;

    @Name("Certificate Number")
    @FindBy(css = "div.dark:nth-child(2)")
    private List<WebElement> certificateNumber;

    @Name("Certificate Version")
    @FindBy(css = "div.dark:nth-child(3)")
    private List<WebElement> certificateVersion;

    @Name("Office")
    @FindBy(css = "div[data-ng-if*='office'] strong")
    private WebElement office;

    @Visible
    @Name("Issue Date")
    @FindBy(css = "div[data-ng-if*='issueDate'] strong")
    private WebElement issueDate;

    @Visible
    @Name("Form Number")
    @FindBy(css = "riders>div>div>div:nth-child(4)>div:nth-child(1) strong")
    private WebElement formNumber;

    @Visible
    @Name("Expiry Date")
    @FindBy(css = "riders>div>div>div:nth-child(4)>div:nth-child(2) strong")
    private WebElement expiryDate;

    @Visible
    @Name("Endorsed Date")
    @FindBy(css = "riders>div>div>div:nth-child(4)>div:nth-child(3) strong")
    private WebElement endorsedDate;

    @Visible
    @Name("Certificate Type")
    @FindBy(css = "riders>div>div>div:nth-child(4)>div:nth-child(4) strong")
    private WebElement certificateType;

    @Name("Due Status")
    @FindBy(css = "due-status strong")
    private List<WebElement> dueStatus;

    @Name("Due Status Icon")
    @FindBy(css = "due-status md-icon")
    private List<WebElement> dueStatusIcon;

    @Visible
    @Name("Download button")
    @FindBy(css = "div.action icon")
    private WebElement downloadButton;

    @Step("Get Certificate Name")
    public String getCertificateName()
    {
        return certificateName.stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No certificate name was displayed."))
                .getText().trim();
    }

    @Step("Get Certificate Number")
    public String getCertificateNumber()
    {
        return certificateNumber.stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No certificate number was displayed."))
                .getText().trim();
    }

    @Step("Get Certificate Version")
    public String getCertificateVersion()
    {
        return certificateVersion
                .stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No certificate version was displayed."))
                .getText().trim();
    }

    @Step("Get Issuing Surveyor")
    public String getIssuingSurveyor()
    {
        return issuingSurveyor.getText().trim();
    }

    @Step("Get Office")
    public String getOffice()
    {
        return office.getText().trim();
    }

    @Step("Get Issue Date")
    public LocalDate getIssueDate()
    {
        return LocalDate.parse(issueDate.getText().trim(), ClassDirect.FRONTEND_TIME_FORMAT_DTS);
    }

    @Step("Get Form Number")
    public String getFormNumber()
    {
        return formNumber.getText().trim();
    }

    @Step("Get Expiry Date")
    public LocalDate getExpiryDate()
    {
        return LocalDate.parse(expiryDate.getText().trim(), ClassDirect.FRONTEND_TIME_FORMAT_DTS);
    }

    @Step("Get Endorsed Date")
    public LocalDate getEndorsedDate()
    {
        return LocalDate.parse(endorsedDate.getText().trim(), ClassDirect.FRONTEND_TIME_FORMAT_DTS);
    }

    @Step("Get Certificate Type")
    public String getCertificateType()
    {
        return certificateType.getText().trim();
    }

    @Step("Get Due Status")
    public String getDueStatus()
    {
        return dueStatus.stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No Due Status was displayed."))
                .getText().trim();
    }

    @Step("Click download button")
    public CertificateElement clickDownloadButton()
    {
        downloadButton.click();
        return this;
    }
}
