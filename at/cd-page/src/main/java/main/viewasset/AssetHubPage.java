package main.viewasset;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.viewasset.certificates.CertificatesAndRecordsCard;
import main.viewasset.codicils.CodicilsCard;
import main.viewasset.mpms.MpmsCard;
import main.viewasset.schedule.SurveyPlannerPage;
import main.viewasset.surveys.SurveysCard;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

public class AssetHubPage extends BasePage<AssetHubPage>
{
    /**
     * The visibility matrix:
     * | Accessible | Restricted | EOR | Equasis/Thetis | Flag user
     * Codicils     | /          |  X         | /   |       /        |  /
     * Jobs         | /          | X          | /   |       X        |  x
     * Certificates | /          | X          | /   |       /        |  /
     * MPMS         | /          | X          | X   | Role-dependent.|  x
     **/
    @Visible //this was added to anchor the page with @Visible
    @Name("Asset Hub container")
    @FindBy(css = "asset-home asset-hub")
    private WebElement assetHubContainer;

    @Name("Codicil Card")
    private CodicilsCard codicilsCard;

    @Name("Survey Card")
    private SurveysCard surveysCard;

    @Name("MPMS card")
    private MpmsCard mpmsCard;

    @Name("Certificates and Records card")
    private CertificatesAndRecordsCard certificatesAndRecordsCard;

    @Step("Get Asset Header Page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return PageFactory.newInstance(AssetHeaderPage.class);
    }

    @Step("Get Codicil Card")
    public CodicilsCard getCodicilCard()
    {
        return codicilsCard;
    }

    @Step("Get Survey Card")
    public SurveysCard getSurveysCard()
    {
        return surveysCard;
    }

    @Step("Get Survey Planner page")
    public SurveyPlannerPage getSurveyPlannerPage()
    {
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return PageFactory.newInstance(SurveyPlannerPage.class);
    }

    @Step("Get MPMS card")
    public MpmsCard getMpmsCard()
    {
        return mpmsCard;
    }

    @Step("Get Certificates and Records card")
    public CertificatesAndRecordsCard getCertificatesAndRecordsCard()
    {
        return certificatesAndRecordsCard;
    }
}
