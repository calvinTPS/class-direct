package main.viewasset.schedule.graphical.base;

import com.frameworkium.core.ui.annotations.Visible;
import main.viewasset.schedule.graphical.element.DataElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

public class BaseTable extends HtmlElement
{
    @Visible
    @Name("Service Name")
    @FindBy(css = ".cd-card-title")
    private WebElement productName;

    @Name("Data Elements")
    private List<DataElement> dataElements;

    @Step("Get Data Elements")
    public List<DataElement> getDataElements()
    {
        return dataElements;
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }
}
