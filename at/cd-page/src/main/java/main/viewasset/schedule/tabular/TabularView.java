package main.viewasset.schedule.tabular;

import main.viewasset.schedule.tabular.base.CodicilTable;
import main.viewasset.schedule.tabular.base.ServiceTable;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = "asset-service-schedule div.service-schedule-detailed div[data-ng-if*='tabular']")
public class TabularView extends HtmlElement
{
    @Name("Codicil Table")
    private CodicilTable codicilTables;

    @Name("Service Table")
    private List<ServiceTable> serviceTables;

    @Step("Get Codicil Table")
    public CodicilTable getCodicilTables()
    {
        return codicilTables;
    }

    @Step("Get Service Table")
    public List<ServiceTable> getServiceTables()
    {
        return serviceTables;
    }
}
