package main.viewasset.schedule.tabular.base;

import com.frameworkium.core.ui.annotations.Visible;
import main.serviceschedule.tabular.element.CodicilElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = "cd-card[data-title='cd-notes-and-actions']")
public class CodicilTable extends HtmlElement
{
    @Visible
    @Name("Codicil Name")
    @FindBy(css = "div .cd-card-title")
    private WebElement productName;

    @Name("Codicils")
    @FindBy(css = "md-card-content")
    private List<CodicilElement> codicils;

    @Step("Get Codicils")
    public List<CodicilElement> getCodicils()
    {
        return codicils;
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }
}
