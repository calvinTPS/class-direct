package main.viewasset.schedule.graphical.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

@FindBy(css = "md-card-content")
public class DataElement extends HtmlElement
{
    @Visible
    @Name("Service name")
    @FindBy(css = "div.details h3")
    private WebElement serviceName;

    @Visible
    @Name("Due Date")
    @FindBy(css = "div>small")
    private List<WebElement> nextDueDate;

    @Visible
    @Name("Due Status")
    @FindBy(css = "div.details due-status strong")
    private WebElement dueStatus;

    @Visible
    @Name("View Details button")
    @FindBy(css = "div.details a")
    private WebElement viewDetailsButton;

    @Step("Get Service Name")
    public String getServiceName()
    {
        return serviceName.getText().trim();
    }

    @Step("Get Due Status")
    public String getDueStatus()
    {
        return dueStatus.getText().trim();
    }

    @Step("Get Next Due Date")
    public String getDueDate()
    {
        // There are 2 due dates but it will show only one depending on the resolution
        return (nextDueDate.get(0).getText().isEmpty() ? nextDueDate.get(1) : nextDueDate.get(0)).getText().split(":")[1].trim();
    }

    @Step("Click View Details Button")
    public <T extends BasePage<T>> T clickViewDetailsButton(Class<T> page)
    {
        viewDetailsButton.click();
        waitForSpinnerToFinish();
        return helper.PageFactory.newInstance(page);
    }
}
