package main.viewasset.schedule;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.serviceschedule.graphical.element.DataElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

public class SurveyPlannerPage extends BasePage<SurveyPlannerPage>
{

    @Name("View full schedule button")
    @FindBy(css = "button[aria-label='View full schedule']")
    private WebElement viewFullScheduleButton;

    @Name("Notes and Actions")
    @FindBy(css = "cd-card-gantt[data-title='cd-notes-and-actions'] div.timeline-bar div.item strong")
    private List<DataElement> notesAndActions;

    @Name("Surveys")
    @FindBy(css = "cd-card-gantt[data-title='cd-surveys'] div.timeline-bar div.item")
    private List<DataElement> surveys;

    @Step("Click 'View full schedule' button")
    public ServiceSchedulePage clickViewFullScheduleButton()
    {
        viewFullScheduleButton.click();
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Get 'Notes and Actions' Table")
    public List<DataElement> getNotesAndActions()
    {
        return notesAndActions;
    }

    @Step("Get Surveys Table")
    public List<DataElement> getSurveys()
    {
        return surveys;
    }

    @Step("Is 'View full schedule' button displayed")
    public boolean isViewFullScheduleButtonDisplayed()
    {
        return viewFullScheduleButton.isExists();
    }
}
