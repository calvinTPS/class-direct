package main.viewasset.schedule.graphical;

import main.viewasset.schedule.graphical.base.BaseTable;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = "asset-service-schedule div.service-schedule-detailed div[data-ng-if*='gantt']")
public class GraphicalView extends HtmlElement
{
    @Name("Codicil Table")
    @FindBy(css = "cd-card[data-title='cd-notes-and-actions']")
    private List<BaseTable> codicilTables;

    @Name("Service Table")
    @FindBy(css = "cd-card[data-item='product']")
    private List<BaseTable> serviceTables;

    @Step("Get Codicil Table")
    public List<BaseTable> getCodicilTables()
    {
        return codicilTables;
    }

    @Step("Get Service Table")
    public List<BaseTable> getServiceTables()
    {
        return serviceTables;
    }
}
