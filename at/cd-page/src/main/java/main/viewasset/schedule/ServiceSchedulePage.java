package main.viewasset.schedule;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import helper.AppHelper;
import main.common.filter.FilterBarPage;
import main.serviceschedule.element.ScheduleElement;
import main.viewasset.AssetHubPage;
import main.viewasset.schedule.filter.FilterServicePage;
import main.viewasset.schedule.graphical.GraphicalView;
import main.viewasset.schedule.tabular.TabularView;
import org.openqa.selenium.Keys;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;

import java.text.ParseException;
import java.time.LocalDate;

import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;

public class ServiceSchedulePage extends BasePage<ServiceSchedulePage>
{
    @Visible
    @Name("back button")
    @FindBy(css = "asset-header button")
    private WebElement backButton;

    @Visible
    @Name("Filter Button")
    @FindBy(css = ".filter-title.layout-row")
    private WebElement filterButton;

    @Name("Schedule element")
    private ScheduleElement scheduleElement;

    @Name("Tabular View")
    private TabularView tabularView;

    @Name("Graphical View")
    private GraphicalView graphicalView;

    @Name("Filter body")
    @FindBy(css = "div.filter-body")
    private WebElement filterBody;

    @Name("'From' Date picker for Mobile")
    @FindBy(css = "md-datepicker.date-from")
    private Datepicker fromDatepickerForMobile;

    @Name("'To' Date picker for Mobile")
    @FindBy(css = "md-datepicker.date-to")
    private Datepicker toDatepickerForMobile;

    @Step("Get Tabular View")
    public TabularView getTabularView()
    {
        return tabularView;
    }

    @Step("Get Graphical View")
    public GraphicalView getGraphicalView()
    {
        return graphicalView;
    }

    @Step("Get Time Scale element")
    public ScheduleElement getScheduleElement()
    {
        return scheduleElement;
    }

    @Step("Click Tabular View Tab")
    public TabularView clickTabularViewTab()
    {
        AppHelper.scrollToMiddle(scheduleElement.getTabularViewtab());
        scheduleElement.getTabularViewtab().click();
        waitForJavascriptFrameworkToFinish();
        return tabularView;
    }

    @Step("Click Graphical View Tab")
    public GraphicalView clickGraphicalViewTab()
    {
        AppHelper.scrollToMiddle(scheduleElement.getGraphicalViewtab());
        scheduleElement.getGraphicalViewtab().click();
        waitForJavascriptFrameworkToFinish();
        return graphicalView;
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Click on Filter button")
    public FilterServicePage clickFilterButton()
    {
        filterButton.click();
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        return PageFactory.newInstance(FilterServicePage.class);
    }

    @Step("Set 'From' Datepicker for Mobile")
    public ServiceSchedulePage setFromDatePickerForMobile(String fromDatePicker)
    {
        fromDatepickerForMobile.clear();
        fromDatepickerForMobile.sendKeys(fromDatePicker);
        fromDatepickerForMobile.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set 'To' Datepicker for Mobile")
    public ServiceSchedulePage setToDatePickerForMobile(String toDatePicker)
    {
        toDatepickerForMobile.clear();
        toDatepickerForMobile.sendKeys(toDatePicker);
        toDatepickerForMobile.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get 'From' Datepicker for Mobile")
    public LocalDate getFromDatePickerForMobile()
    {
        return LocalDate.parse(fromDatepickerForMobile.getSelectedDate(), FRONTEND_TIME_FORMAT_DTS);
    }

    @Step("Get 'To' Datepicker for Mobile")
    public LocalDate getToDatePickerForMobile()
    {
        return LocalDate.parse(toDatepickerForMobile.getSelectedDate(), FRONTEND_TIME_FORMAT_DTS);
    }

    @Step("Click back button")
    public AssetHubPage clickBackButton()
    {
        backButton.click();
        return PageFactory.newInstance(AssetHubPage.class);
    }
}
