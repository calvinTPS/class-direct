package main.viewasset.schedule.filter;

import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.common.filter.FilterBarPage;
import main.common.filter.servicetype.FilterByServiceTypePage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.CheckBox;

public class FilterServicePage extends BasePage<FilterServicePage>
{
    @Name("Service type button")
    @FindBy(css = "md-dialog service-type button")
    private WebElement serviceTypeButton;

    @Name("Submit Button")
    @FindBy(css = "button[aria-label='Submit']")
    private WebElement submitButton;

    @Name("Clear Filters Button")
    @FindBy(css = "button[aria-label='Clear filters']")
    private WebElement clearFiltersButton;

    @Name("Conditions of Class Checkbox")
    @FindBy(css = "md-dialog md-checkbox.checkbox-coc")
    private CheckBox conditionsOfClassCheckbox;

    @Name("Actionable Items Checkbox")
    @FindBy(css = "md-dialog md-checkbox.checkbox-ai")
    private CheckBox actionableItemsCheckbox;

    @Name("Statutory Finding Checkbox")
    @FindBy(css = "md-dialog md-checkbox[aria-label='Statutory finding']")
    private CheckBox statutoryFindingCheckbox;

    @Step("Click Submit Button")
    public <T extends BasePage<T>> T clickSubmitButton(Class<T> pageObjectClass)
    {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Clear Filter Button")
    public FilterBarPage clickClearFiltersButton()
    {
        clearFiltersButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Click service type button")
    public FilterByServiceTypePage clickServiceTypeButton()
    {
        serviceTypeButton.click();
        return  PageFactory.newInstance(FilterByServiceTypePage.class);
    }

    @Step("Select Condition of class")
    public FilterServicePage selectConditionOfClass()
    {
        conditionsOfClassCheckbox.select();
        return this;
    }

    @Step("Deselect Condition of class")
    public FilterServicePage deselectConditionOfClass()
    {
        conditionsOfClassCheckbox.deselect();
        return this;
    }

    @Step("Select Actionable Item")
    public FilterServicePage selectActionableItem()
    {
        actionableItemsCheckbox.select();
        return this;
    }

    @Step("Deselect Actionable Item")
    public FilterServicePage deselectActionableItem()
    {
        actionableItemsCheckbox.deselect();
        return this;
    }

    @Step("Select Statutory Finding")
    public FilterServicePage selectStatutoryFinding()
    {
        statutoryFindingCheckbox.select();
        return this;
    }

    @Step("Deselect Statutory Finding")
    public FilterServicePage deselectStatutoryFinding()
    {
        statutoryFindingCheckbox.deselect();
        return this;
    }
}
