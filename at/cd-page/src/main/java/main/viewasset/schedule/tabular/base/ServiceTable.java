package main.viewasset.schedule.tabular.base;

import com.frameworkium.core.ui.annotations.Visible;
import main.serviceschedule.tabular.element.ServiceElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = "cd-card[data-item='product']")
public class ServiceTable extends HtmlElement
{
    @Visible
    @Name("Service Name")
    @FindBy(css = ".cd-card-title")
    private WebElement productName;

    @Name("Services")
    @FindBy(css = "md-card-content")
    private List<ServiceElement> services;

    @Step("Get Services")
    public List<ServiceElement> getServices()
    {
        return services;
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }
}
