package main.viewasset.mpms.crediting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.viewasset.mpms.crediting.element.TaskCard;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class MpmsCreditingPage extends BasePage<MpmsCreditingPage>
{
    @Visible
    @Name("Export button")
    @FindBy(css = "export strong")
    private WebElement exportButton;

    @Name("Task report submission Text")
    @FindBy(css = "div.mpms-message-bar")
    private WebElement taskReportSuccessfulSubmissionText;

    @Name("Task Cards")
    private List<TaskCard> taskCards;

    @Step("Get Task Cards")
    public List<TaskCard> getTaskCards()
    {
        return taskCards;
    }

    @Step("Get Task report submission Text")
    public String getTaskReportSuccessfulSubmissionText()
    {
        return taskReportSuccessfulSubmissionText.getText().trim();
    }

    @Step("Click Export Button")
    public MpmsCreditingPage clickExportButton()
    {
        exportButton.click();
        return this;
    }

    @Step("Get FooterPage")
    public FooterPage getFooterPage()
    {
        return PageFactory.newInstance(FooterPage.class);
    }
}
