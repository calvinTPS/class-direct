package main.viewasset.mpms;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import main.viewasset.mpms.crediting.MpmsCreditingPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(css = "div.block-mpms")
public class MpmsCard extends HtmlElement
{
    @Visible
    @Name("Crediting Service Button")
    @FindBy(css = "a.display-block")
    private WebElement creditingServiceButton;

    @Step("Click Crediting Service Button")
    public MpmsCreditingPage clickCreditingServiceButton()
    {
        creditingServiceButton.click();
        return PageFactory.newInstance(MpmsCreditingPage.class);
    }
}
