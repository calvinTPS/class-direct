package main.viewasset.mpms.crediting.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.js.JavascriptWait;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.Keys;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.Datepicker;

@FindBy(css = "mpms-task>cd-card")
public class TaskCard extends HtmlElement
{
    @Visible
    @Name("Task Name")
    @FindBy(className = "task-name")
    private WebElement taskName;

    @Visible
    @Name("Task ID")
    @FindBy(className = "task-number")
    private WebElement taskNumber;

    @Visible
    @Name("Assigned Date")
    @FindBy(className = "task-assigned-date")
    private WebElement assignedDate;

    @Visible
    @Name("Due/Postponed date")
    @FindBy(css = ".task-postponement-date, .task-due-date")
    private WebElement duePostponedDate;

    @Visible
    @Name("Credit Task Button")
    @FindBy(className = "button-credit-task")
    private WebElement creditTaskButton;

    @Name("Date Credited Date Picker")
    @FindBy(css = "div.date-picker md-datepicker")
    private Datepicker dateCreditedDatepicker;

    @Name("Date credited Error Text")
    @FindBy(css = "div.md-input-message-animation")
    private WebElement dateCreditedErrorText;

    @Step("Click Crediting Task Button")
    public TaskCard clickCreditingTaskButton()
    {
        creditTaskButton.click();
        return this;
    }

    @Step("Get Task Name")
    public String getTaskName()
    {
        return taskName.getText().trim();
    }

    @Step("Get Task Number")
    public String getTaskNumber()
    {
        return taskNumber.getText().trim();
    }

    @Step("Get Assigned Date")
    public String getAssignedDate()
    {
        return assignedDate.getText().trim();
    }

    @Step("Get Due/Postponed Date")
    public String getDuePostponedDate()
    {
        return duePostponedDate.getText().trim();
    }

    @Step("Is Date credited Error Text Displayed")
    public boolean isDateCreditedErrorTextDisplayed()
    {
        return dateCreditedErrorText.exists();
    }

    @Step("Is Date Credited Date Picker")
    public boolean isDateCreditedDatePickerDisplayed()
    {
        return dateCreditedDatepicker.isDisplayed();
    }

    @Step("Set Date Credited - \"{0}\"")
    public TaskCard setDateCredited(String dateCredited)
    {
        dateCreditedDatepicker.clear();
        dateCreditedDatepicker.sendKeys(dateCredited);
        dateCreditedDatepicker.sendKeys(Keys.TAB);
        new JavascriptWait(BaseTest.getDriver(), BaseTest.getWait()).waitForJavascriptFramework();
        return this;
    }

    @Step("Get Date Credited")
    public String getDateCredited()
    {
        return dateCreditedDatepicker.getSelectedDate();
    }
}
