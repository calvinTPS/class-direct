package main.viewasset.mpms.crediting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class FooterPage extends BasePage<FooterPage>
{
    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[aria-label='Cancel']")
    private WebElement cancelButton;

    @Visible
    @Name("Submit Report Button")
    @FindBy(css = "[aria-label='Submit report']")
    private WebElement submitReportButton;

    @Name("Success Message")
    @FindBy(css = "div.mpms-message-bar")
    private WebElement successMessage;

    @Step("Click Submit Report Button")
    public <T extends BasePage<T>> T clickSubmitReportButton(Class<T> pageObjectClass)
    {
        submitReportButton.click();
        wait.until(ExpectedConditions.visibilityOf(successMessage));
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Cancel Button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> pageObjectClass)
    {
        cancelButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Is Submit Report Button enabled")
    public boolean isSubmitReportButtonEnabled()
    {
        return submitReportButton.isEnabled();
    }
}
