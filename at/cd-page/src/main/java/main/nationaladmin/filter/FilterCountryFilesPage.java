package main.nationaladmin.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.nationaladmin.NationalAdministrationPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;
import typifiedelement.WebElement;

public class FilterCountryFilesPage extends BasePage<FilterCountryFilesPage>
{
    @Visible
    @Name("Submit Button")
    @FindBy(css = "[aria-label='Submit']")
    private WebElement submitButton;

    @Name("Clear Filters Button")
    @FindBy(css = "[aria-label='Clear filters']")
    private WebElement clearFiltersButton;

    @Name("Modification Range Header")
    @FindBy(css = "accordion[data-title='cd-modification-range'] .accordion-header")
    private WebElement modificationRangeHeader;

    @Name("Modification Range From Date ")
    @FindBy(css = ".filter date-range .date-from")
    private Datepicker modificationRangeFromDatepicker;

    @Name("Modification Range To Date")
    @FindBy(css = ".filter date-range .date-to")
    private Datepicker modificationRangeToDatepicker;

    @Step("Set From Modification Range - \"{0}\"")
    public FilterCountryFilesPage setFromModificationRange(String date)
    {
        modificationRangeFromDatepicker.clear();
        modificationRangeFromDatepicker.sendKeys(date);
        modificationRangeFromDatepicker.sendKeys(Keys.TAB);
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Set To Modification Range - \"{0}\"")
    public FilterCountryFilesPage setToModificationRange(String date)
    {
        modificationRangeToDatepicker.clear();
        modificationRangeToDatepicker.sendKeys(date);
        modificationRangeToDatepicker.sendKeys(Keys.TAB);
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return this;
    }

    @Step("Click Submit Button")
    public NationalAdministrationPage clickSubmitButton()
    {
        submitButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(NationalAdministrationPage.class);
    }

    @Step("Click Clear Filter Button")
    public NationalAdministrationPage clickClearFiltersButton()
    {
        clearFiltersButton.click();
        return PageFactory.newInstance(NationalAdministrationPage.class);
    }
}
