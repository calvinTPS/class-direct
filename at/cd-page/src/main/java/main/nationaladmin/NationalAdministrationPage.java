package main.nationaladmin;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.nationaladmin.element.CountryFileElement;
import main.nationaladmin.filter.FilterCountryFilesPage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.TypeAheadSelect;
import typifiedelement.WebElement;

import java.util.List;

public class NationalAdministrationPage extends BasePage<NationalAdministrationPage>
{
    @Visible
    @Name("Flag Text Box")
    @FindBy(name = "flag")
    private TypeAheadSelect flagTextBox;

    @Name("Filter Button")
    @FindBy(css = ".filter .filter-title")
    private WebElement filterButton;

    @Name("Filter body")
    @FindBy(css = ".filter .filter-body")
    private WebElement filterBody;

    @Name("Country File version in Use")
    @FindBy(css = ".national-administration-wrapper form+div cd-card-tabular")
    private CountryFileElement countryFileVersionInUse;

    @Name("Country Files older version")
    @FindBy(css = ".rider-items-container cd-card-tabular")
    private List<CountryFileElement> countryFilesOlderVersion;

    @Step("Select Flag Name - \"{0}\"")
    public NationalAdministrationPage selectFlagName(String flagName)
    {
        flagTextBox.clear();
        flagTextBox.sendKeys(flagName.substring(0, flagName.length()-1));
        flagTextBox.selectByVisibleText(flagName);
        return this;
    }

    @Step("Click on Filter button")
    public FilterCountryFilesPage clickFilterButton()
    {
        filterButton.click();
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        return PageFactory.newInstance(FilterCountryFilesPage.class);
    }

    @Step("Get country file Version in use")
    public CountryFileElement getCountryFileVersionInUse()
    {
        return countryFileVersionInUse;
    }

    @Step("Get country files of older version")
    public List<CountryFileElement> getCountryFilesOlderVersion()
    {
        return countryFilesOlderVersion;
    }
}
