package main.nationaladmin.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.ClassDirect;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.time.LocalDate;

public class CountryFileElement extends HtmlElement
{
    @Visible
    @Name("Flag Name")
    @FindBy(className = "flag-name")
    private WebElement flagName;

    @Name("Last modified date")
    @FindBy(className = "last-modified-date")
    private WebElement lastModifiedDate;

    @Name("Last modified time")
    @FindBy(className = "last-modified-time")
    private WebElement lastModifiedTime;

    @Name("Download icon")
    @FindBy(css = ".action icon")
    private WebElement downloadIcon;

    @Step("Click download Icon")
    public CountryFileElement clickDownloadIcon()
    {
        downloadIcon.click();
        return this;
    }

    @Step("Get Flag Name")
    public String getFlagName()
    {
        return flagName.getText().trim();
    }

    @Step("Get Last modified date")
    public LocalDate getLastModifiedDate()
    {
        return LocalDate.parse(lastModifiedDate.getText().trim(), ClassDirect.FRONTEND_TIME_FORMAT_DTS);
    }

    @Step("Get Last modified time")
    public String getLastModifiedTime()
    {
        return lastModifiedTime.getText().trim();
    }
}
