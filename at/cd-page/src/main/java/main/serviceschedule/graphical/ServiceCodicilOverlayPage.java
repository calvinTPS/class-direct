package main.serviceschedule.graphical;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.serviceschedule.ServiceSchedulePage;
import main.serviceschedule.tabular.base.CodicilTable;
import main.serviceschedule.tabular.base.ServiceTable;
import org.openqa.selenium.By;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class ServiceCodicilOverlayPage extends BasePage<ServiceCodicilOverlayPage>
{
    @Visible
    @Name("Asset name")
    @FindBy(css = "div.content-main>div.h1")
    private WebElement assetName;

    @Visible
    @Name("Close button")
    @FindBy(css = "button[aria-label='Close']")
    private WebElement closeButton;

    @Name("Codicils")
    private List<CodicilTable> codicilTables;

    @Name("Services")
    private List<ServiceTable> serviceTables;

    @Step("Get asset name")
    public String getAssetName()
    {
        return assetName.getText().trim();
    }

    @Step("Click close button")
    public ServiceSchedulePage clickCloseButton()
    {
        closeButton.click();
        wait.until(ExtraExpectedConditions.invisibilityOfDialog());
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Get codicil tables")
    public List<CodicilTable> getCodicilTables()
    {
        return codicilTables;
    }

    @Step("Get service tables")
    public List<ServiceTable> getServiceTables()
    {
        return serviceTables;
    }
}
