package main.serviceschedule.graphical;

import main.serviceschedule.graphical.base.AssetTable;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = "service-schedule pagination>div:nth-child(2)")
public class GraphicalView extends HtmlElement
{
    @Name("Asset Table")
    private List<AssetTable> assetTables;

    @Step("Get Asset Table")
    public List<AssetTable> getAssetTables()
    {
        return assetTables;
    }
}
