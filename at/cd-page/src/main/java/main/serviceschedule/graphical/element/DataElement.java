package main.serviceschedule.graphical.element;

import com.frameworkium.core.ui.pages.PageFactory;
import helper.ExtraExpectedConditions;
import helper.HtmlElement;
import main.serviceschedule.graphical.ServiceCodicilOverlayPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class DataElement extends HtmlElement
{
    @Name("Data count")
    @FindBy(css = ".dot-wrapper>strong")
    private WebElement dataCount;

    @Name("Data tooltip")
    @FindBy(css = "div.item>cd-tooltip")
    private TooltipElement dataTooltip;

    @Step("Get data count")
    public int getDataCount()
    {
        try
        {
            return Integer.parseInt(dataCount.getText().trim());
        }
        catch(NumberFormatException ex)
        {
            return 1;
        }
    }

    @Step("Get Tooltip")
    public TooltipElement getTooltip()
    {
        dataCount.moveToElement();
        return dataTooltip;
    }

    @Step("Click data element")
    public ServiceCodicilOverlayPage clickDataElement()
    {
        dataCount.click();
        wait.until(ExtraExpectedConditions.visibilityOfDialog());
        return PageFactory.newInstance(ServiceCodicilOverlayPage.class);
    }
}
