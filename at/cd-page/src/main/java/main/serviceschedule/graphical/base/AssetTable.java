package main.serviceschedule.graphical.base;

import main.serviceschedule.element.AssetCard;
import main.serviceschedule.graphical.element.DataElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = "pagination assets-service-high-level")
public class AssetTable extends HtmlElement
{
    @Name("Asset Card")
    private AssetCard assetCard;

    @Name("Codicils")
    @FindBy(css = "cd-card-gantt[data-title='cd-notes-and-actions'] div.timeline-bar div.item")
    private List<DataElement> codicils;

    @Name("Services")
    @FindBy(css = "cd-card-gantt[data-title='cd-surveys'] div.timeline-bar div.item")
    private List<DataElement> services;

    @Step("Get Codicil Table")
    public List<DataElement> getCodicils()
    {
        return codicils;
    }

    @Step("Get Service Table")
    public List<DataElement> getServices()
    {
        return services;
    }

    @Step("Get Asset Card")
    public AssetCard getAssetCard()
    {
        return assetCard;
    }
}
