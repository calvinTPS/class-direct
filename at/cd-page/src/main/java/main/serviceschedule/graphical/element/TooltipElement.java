package main.serviceschedule.graphical.element;

import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

public class TooltipElement extends HtmlElement
{
    @Name("Name")
    @FindBy(css = "ng-transclude div:nth-child(1) strong")
    private WebElement serviceCodicilName;

    @Name("Due Status")
    @FindBy(css = "ng-transclude div:nth-child(2)")
    private WebElement dueStatus;

    @Name("Due Status definition")
    @FindBy(css = "ng-transclude div:nth-child(3)")
    private WebElement dueStatusDefinition;

    @Name("Due Date")
    @FindBy(css = "ng-transclude div:nth-child(4)")
    private WebElement dueDate;

    @Step("Get Service/Codicil Name")
    public String getServiceCodicilName()
    {
        return serviceCodicilName.getText().trim();
    }

    @Step("Get Due Status")
    public String getDueStatus()
    {
        return dueStatus.getText().split(":")[1].trim();
    }

    @Step("Get Due Status Definition")
    public String getDueStatusDefinition()
    {
        return dueStatusDefinition.getText().split(":")[1].trim();
    }

    @Step("Get Due Date")
    public String getDueDate()
    {
        return dueDate.getText().split(":")[1].trim();
    }
}
