package main.serviceschedule.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.AppHelper;
import helper.HtmlElement;
import main.requestsurvey.RequestSurveyPage;
import main.viewasset.AssetHubPage;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import typifiedelement.Star;

@FindBy(css = "asset-card")
public class AssetCard extends HtmlElement
{
    @Visible
    @FindBy(css = "emblem")
    @Name("Asset Emblem")
    private WebElement assetEmblem;

    @Visible
    @Name("Asset name")
    @FindBy(css = ".card-name")
    private WebElement assetName;

    @Visible
    @Name("IMO Number")
    @FindBy(css = ".imo")
    private WebElement imoNumber;

    @Visible
    @Name("Asset Type")
    @FindBy(css = ".asset-type")
    private WebElement assetType;

    @Visible
    @Name("Class Status")
    @FindBy(css = ".class-status")
    private WebElement classStatus;

    @Visible
    @Name("Flag")
    @FindBy(css = ".flag")
    private WebElement flag;

    @Visible
    @Name("Due Status")
    @FindBy(css = "div.status due-status strong")
    private WebElement dueStatus;

    @Visible
    @Name("Request Survey button")
    @FindBy(css = ".button-request-survey")
    private WebElement requestSurveyButton;

    @Visible
    @Name("View Asset button")
    @FindBy(css = ".button-view-asset")
    private WebElement viewAssetButton;

    @Visible
    @Name("Favourite button")
    @FindBy(css = ".card-footer star .star-container")
    private Star favouriteButton;

    @Step("Get Asset Name")
    public String getAssetName()
    {
        return assetName.getText().trim();
    }

    @Step("Get IMO Number")
    public String getImoNumber()
    {
        return imoNumber.getText().trim();
    }

    @Step("Get Asset Type")
    public String getAssetType()
    {
        return assetType.getText().trim();
    }

    @Step("Get Class Status")
    public String getClassStatus()
    {
        return classStatus.getText().trim();
    }

    @Step("Get Flag")
    public String getFlag()
    {
        return flag.getText().trim();
    }

    @Step("Get Due Status")
    public String getDueStatus()
    {
        return dueStatus.getText().trim();
    }

    @Step("Click Request Survey button")
    public RequestSurveyPage clickRequestSurveyButton()
    {
        AppHelper.scrollIntoView(requestSurveyButton);
        requestSurveyButton.click();
        return helper.PageFactory.newInstance(RequestSurveyPage.class);
    }

    @Step("Click View Asset button")
    public AssetHubPage clickViewAssetButton()
    {
        AppHelper.scrollIntoView(viewAssetButton);
        viewAssetButton.click();
        return helper.PageFactory.newInstance(AssetHubPage.class);
    }

    @Step("Select Favourite button")
    public AssetCard selectFavouriteButton(){
        favouriteButton.select();
        return this;
    }

    @Step("Deselect Favourite button")
    public AssetCard deselectFavouriteButton(){
        favouriteButton.deselect();
        return this;
    }

    @Step("Is Asset Favourite")
    public boolean isAssetFavourite()
    {
        return favouriteButton.isFavourited();
    }

    @Step("Is asset emblem displayed")
    public boolean isAssetEmblemDisplayed()
    {
        return assetEmblem.isDisplayed();
    }
}
