package main.serviceschedule.element;

import com.frameworkium.core.ui.annotations.Visible;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.Timeline;

@FindBy(css = "div.main-content, asset-service-schedule div.content-main")
public class ScheduleElement extends HtmlElement
{
    @Visible
    @Name("Tabular View Tab")
    @FindBy(css = "md-tab-item.md-tab:nth-child(1)")
    private WebElement tabularViewTab;

    @Visible
    @Name("Graphical View Tab")
    @FindBy(css = "md-tab-item.md-tab:nth-child(2)")
    private WebElement graphicalViewTab;

    @Visible
    @Name("Service Timeline")
    @FindBy(css = "div.date-range-sticky")
    private Timeline serviceTimeline;

    @Step("Get Service Timeline")
    public Timeline getServiceTimeline()
    {
        return serviceTimeline;
    }

    @Step("Is Tabular View selected")
    public boolean isTabularViewSelected()
    {
        return tabularViewTab.getAttribute("class").contains("md-active");
    }

    @Step("Get Tabular View tab")
    public WebElement getTabularViewtab()
    {
        return tabularViewTab;
    }

    @Step("Get Graphical View tab")
    public WebElement getGraphicalViewtab()
    {
        return graphicalViewTab;
    }
}
