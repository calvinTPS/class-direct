package main.serviceschedule;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.common.ShowingXOfYPage;
import main.common.export.ExportAssetListingPage;
import main.common.filter.FilterBarPage;
import main.serviceschedule.element.ScheduleElement;
import main.serviceschedule.graphical.GraphicalView;
import main.serviceschedule.tabular.TabularView;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ServiceSchedulePage extends BasePage<ServiceSchedulePage>
{
    @Visible
    @Name("Export Asset information button")
    @FindBy(css = "div.filter-search-container export-assets button")
    private WebElement exportAssetInformationButton;

    @Name("Shown Asset Count")
    @FindBy(css = "visible-results strong:nth-child(1)")
    private WebElement shownAssetCount;

    @Name("Total Asset Count")
    @FindBy(css = "visible-results strong:nth-child(2)")
    private WebElement totalAssetCount;

    @Name("Timescale element")
    private ScheduleElement scheduleElement;

    @Name("Tabular View")
    private TabularView tabularView;

    @Name("Graphical View")
    private GraphicalView graphicalView;

    @Step("Get Time Scale element")
    public ScheduleElement getScheduleElement()
    {
        return scheduleElement;
    }

    @Step("Get Tabular View")
    public TabularView getTabularView()
    {
        wait.until(ExtraExpectedConditions.invisibilityOfSpinner());
        return tabularView;
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Click Tabular View Tab")
    public TabularView clickTabularViewTab()
    {
        AppHelper.scrollToMiddle(scheduleElement.getTabularViewtab());
        scheduleElement.getTabularViewtab().click();
        waitForJavascriptFrameworkToFinish();
        return tabularView;
    }

    @Step("Click Graphical View Tab")
    public GraphicalView clickGraphicalViewTab()
    {
        AppHelper.scrollToMiddle(scheduleElement.getGraphicalViewtab());
        scheduleElement.getGraphicalViewtab().click();
        waitForJavascriptFrameworkToFinish();
        return graphicalView;
    }

    @Step("Get \"Showing X of Y\"")
    public ShowingXOfYPage getShowingXofYPage(){
        return PageFactory.newInstance(ShowingXOfYPage.class);
    }

    @Step("Click Export Asset Information button")
    public ExportAssetListingPage clickExportAssetInformationButton()
    {
        exportAssetInformationButton.click();
        return PageFactory.newInstance(ExportAssetListingPage.class);
    }
}
