package main.serviceschedule.details;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.AppHelper;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.common.filter.FilterBarPage;
import main.serviceschedule.details.element.TaskElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class TaskListPage extends BasePage<TaskListPage>
{
    @Visible
    @Name("Service name")
    @FindBy(css = ".title h1")
    private WebElement serviceName;

    @Name("Export button")
    @FindBy(css = "export small")
    private WebElement exportButton;

    @Name("For Information only text")
    @FindBy(css = "page-header~div strong")
    private WebElement forInformationOnlyText;

    @Name("Task list")
    private List<TaskElement> taskList;

    @Step("Get service name")
    public String getServiceName()
    {
        return serviceName.getText().trim();
    }

    @Step("Get For Information Only text")
    public String getForInformationOnlyText()
    {
        return forInformationOnlyText.getText().trim();
    }

    @Step("Get Task list")
    public List<TaskElement> getTaskList()
    {
        return taskList;
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage()
    {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Click Export Button")
    public TaskListPage clickExportButton()
    {
        exportButton.click();
        return this;
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }
}
