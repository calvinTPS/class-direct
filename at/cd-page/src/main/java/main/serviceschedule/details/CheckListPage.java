package main.serviceschedule.details;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import helper.PageFactory;
import main.common.AssetHeaderPage;
import main.serviceschedule.details.element.CheckListGroup;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.ClearableTextBox;
import typifiedelement.WebElement;

import java.util.List;

public class CheckListPage extends BasePage<CheckListPage>
{
    @Visible
    @Name("Service name")
    @FindBy(css = "checklist h1")
    private WebElement serviceName;

    @Name("Export button")
    @FindBy(css = "export small")
    private WebElement exportButton;

    @Name("For Information only text")
    @FindBy(css = "page-header~div strong")
    private WebElement forInformationOnlyText;

    @Name("Search by Checklist group text box")
    @FindBy(css = ".filter-search-container input")
    private ClearableTextBox searchByChecklistTextbox;

    @Name("Check list group")
    private List<CheckListGroup> checkListGroups;

    @Step("Get For Information Only text")
    public String getForInformationOnlyText()
    {
        return forInformationOnlyText.getText().trim();
    }

    @Step("Get Asset header page")
    public AssetHeaderPage getAssetHeaderPage()
    {
        return PageFactory.newInstance(AssetHeaderPage.class);
    }

    @Step("Get service name")
    public String getServiceName()
    {
        return serviceName.getText().trim();
    }

    @Step("Get Check list groups")
    public List<CheckListGroup> getCheckListGroups()
    {
        checkListGroups
                .parallelStream()
                .forEach(HtmlElement::waitForStaleness);
        return checkListGroups;
    }

    @Step("Click Export Button")
    public CheckListPage clickExportButton()
    {
        exportButton.click();
        return this;
    }

    @Step("Enter search text - \"{0}\"")
    public CheckListPage setCheckListSearchText(String searchString)
    {
        searchByChecklistTextbox.sendKeys(searchString);
        return PageFactory.newInstance(CheckListPage.class);
    }
}
