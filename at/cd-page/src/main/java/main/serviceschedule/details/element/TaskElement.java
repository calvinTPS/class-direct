package main.serviceschedule.details.element;

import constant.ClassDirect;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.text.ParseException;
import java.util.Date;

@FindBy(css = "cd-card-tabular md-card-content")
public class TaskElement extends HtmlElement
{
    @Name("Task name")
    @FindBy(css = ".task-name")
    private WebElement taskName;

    @Name("Task number")
    @FindBy(css = ".task-number")
    private WebElement taskNumber;

    @Name("Due date")
    @FindBy(css = ".due-date")
    private WebElement dueDate;

    @Name("Assigned date")
    @FindBy(css = ".assigned-date")
    private WebElement assignedDate;

    @Name("Credit Status")
    @FindBy(css = ".credit-status")
    private WebElement creditStatus;

    @Step("Get task name")
    public String getTaskName()
    {
        return taskName.getText().trim();
    }

    @Step("Get task number")
    public String getTaskNumber()
    {
        return taskNumber.getText().trim();
    }

    @Step("Get due date")
    public Date getDueDate()
    {
        Date date;
        try
        {
            date = ClassDirect.FRONTEND_DATE_FORMAT.parse(dueDate.getText().trim());
        }
        catch (ParseException e)
        {
            date = null;
        }
        return date;
    }

    @Step("Get assigned date")
    public Date getAssignedDate()
    {
        Date date;
        try
        {
            date = ClassDirect.FRONTEND_DATE_FORMAT.parse(assignedDate.getText().trim());
        }
        catch (ParseException e)
        {
            date = null;
        }
        return date;
    }

    @Step("Get credit status")
    public String getCreditStatus()
    {
        return creditStatus.getText().trim();
    }
}
