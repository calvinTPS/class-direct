package main.serviceschedule.details.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

@FindBy(css = "cd-card-tabular md-card-content")
public class CheckListElement extends HtmlElement
{
    @Visible
    @Name("Check list description")
    @FindBy(css = ".description")
    private WebElement description;

    @Visible
    @Name("Check list number")
    @FindBy(css = " .description~strong")
    private WebElement checkListNumber;

    @Visible
    @Name("Status")
    @FindBy(css = ".status strong:not([style*='display: none'])")
    private WebElement status;

    @Step("Get description")
    public String getDescription()
    {
        return description.getText().trim();
    }

    @Step("Get Check list number")
    public String getCheckListNumber()
    {
        return checkListNumber.getText().trim();
    }

    @Step("Get Status")
    public String getStatus()
    {
        return status.getText();
    }
}
