package main.serviceschedule.details.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

@FindBy(css = "div.header-item")
public class CheckListGroup extends HtmlElement
{
    @Visible
    @Name("Expand icon")
    @FindBy(css = "div icon[aria-hidden='false']")
    private WebElement expandIcon;

    @Visible
    @Name("Group name")
    @FindBy(css = "div.h3-primary")
    private WebElement groupName;

    @Name("Check list")
    private List<CheckListElement> checkListElements;

    @Step("Get Status")
    public CheckListGroup expand()
    {
        expandIcon.click();
        return this;
    }

    @Step("Get Group name")
    public String getGroupName()
    {
        return groupName.getText().trim();
    }

    @Step("Get Check list elements")
    public List<CheckListElement> getCheckListElements()
    {
        return checkListElements;
    }
}
