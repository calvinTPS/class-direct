package main.serviceschedule.tabular.base;

import com.frameworkium.core.ui.annotations.Visible;
import helper.HtmlElement;
import main.serviceschedule.tabular.element.ServiceElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;


@FindBy(css = "cd-card-accordion div.header-item:not([aria-label='Notes and actions'])")
public class ServiceTable extends HtmlElement {
    @Visible
    @Name("Service Name")
    @FindBy(css = "div.header-item>div:nth-child(2)")
    private WebElement productName;

    @Visible
    @Name("Expand button")
    @FindBy(css = "div.header-item>div:nth-child(1)")
    private WebElement expandButton;

    @Name("Services")
    @FindBy(xpath = "./following-sibling::cd-card-tabular/md-card/descendant-or-self::md-card-content")
    private List<ServiceElement> services;

    @Step("Get Services")
    public List<ServiceElement> getServices() {
        return services;
    }

    @Step("Get Product Name")
    public String getProductName() {
        return productName.getText().trim();
    }

    @Step("Is table expanded")
    public boolean isTableExpanded() {
        return productName.getAttribute("class").contains("active");
    }

    @Step("Expand Codicils")
    public ServiceTable expand() {
        expandButton.click();
        return this;
    }
}
