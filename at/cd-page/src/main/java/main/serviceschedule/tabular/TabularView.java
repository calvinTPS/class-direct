package main.serviceschedule.tabular;

import helper.HtmlElement;
import main.serviceschedule.tabular.base.AssetTable;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

@FindBy(css = "service-schedule pagination>div:nth-child(2)")
public class TabularView extends HtmlElement {
    @Name("Asset Table")
    private List<AssetTable> assetTables;

    @Step("Get Asset Table")
    public List<AssetTable> getAssetTables() {
        waitForSpinnerToFinish();
        return assetTables;
    }
}
