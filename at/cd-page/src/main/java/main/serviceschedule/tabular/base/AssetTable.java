package main.serviceschedule.tabular.base;

import helper.HtmlElement;
import main.serviceschedule.element.AssetCard;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

@FindBy(css = "pagination assets-service-high-level")
public class AssetTable extends HtmlElement {
    @Name("Asset Card")
    private AssetCard assetCard;

    @FindBy(css = "div.asset-service-high-level-details")
    @Name("Codicils and Services")
    private WebElement assetServiceTable;

    @FindBy(css = "div.asset-service-high-level-container>div:nth-child(1)")
    @Name("Expand Button")
    private WebElement expandButton;

    @Name("Codicil Table")
    private CodicilTable codicilTable;

    @Name("Service Table")
    private List<ServiceTable> serviceTables;

    @Step("Get Asset Card")
    public AssetCard getAssetCard() {
        waitForSpinnerToFinish();
        return assetCard;
    }

    @Step("Get Codicil Table")
    public CodicilTable getCodicilTable() {
        return codicilTable;
    }

    @Step("Get Service Table")
    public List<ServiceTable> getServiceTables() {
        return serviceTables;
    }

    @Step("Is asset table expanded")
    public boolean isAssetTableExpanded() {
        return !Boolean.parseBoolean(assetServiceTable.getAttribute("aria-hidden"));
    }

    @Step("Expand Asset table")
    public AssetTable expand() {
        if (!isAssetTableExpanded()) {
            expandButton.click();
            waitForSpinnerToFinish();
        }
        return this;
    }
}
