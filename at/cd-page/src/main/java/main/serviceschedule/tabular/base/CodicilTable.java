package main.serviceschedule.tabular.base;

import com.frameworkium.core.ui.annotations.Visible;
import main.serviceschedule.tabular.element.CodicilElement;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(css = "cd-card-accordion div.header-item[aria-label='Notes and actions']")
public class CodicilTable extends HtmlElement
{
    @Visible
    @Name("Codicil Name")
    @FindBy(css = "div.header-item>div:nth-child(2)")
    private WebElement productName;

    @Visible
    @Name("Expand button")
    @FindBy(css = "div.header-item>div:nth-child(1)")
    private WebElement expandButton;

    @Name("Codicils")
    @FindBy(xpath = "./following-sibling::cd-card-tabular/md-card/descendant-or-self::md-card-content")
    private List<CodicilElement> codicils;

    @Step("Get Codicils")
    public List<CodicilElement> getCodicils()
    {
        return codicils;
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }

    @Step("Is table expanded")
    public boolean isTableExpanded()
    {
        return productName.getAttribute("class").contains("active");
    }

    @Step("Expand Codicils")
    public CodicilTable expand()
    {
        if(!isTableExpanded())
        {
            expandButton.click();
        }
        return this;
    }
}
