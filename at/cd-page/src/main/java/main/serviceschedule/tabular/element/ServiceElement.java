package main.serviceschedule.tabular.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import helper.PageFactory;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

import java.util.List;

@FindBy(css = "md-card-content")
public class ServiceElement extends HtmlElement
{
    @Visible
    @Name("Service name")
    @FindBy(css = ".title, .service-name")
    private WebElement serviceName;

    @Visible
    @Name("Due Date")
    @FindBy(css = ".due-date .detail")
    private WebElement dueDate;

    @Visible
    @Name("Assigned date")
    @FindBy(css = ".assigned-date .detail")
    private WebElement assignedDate;

    @Visible
    @Name("Range date")
    @FindBy(css = "riders>div>div>div:nth-child(2)>div:nth-child(3)")
    private WebElement rangeDate;

    @Name("Postponement Date")
    @FindBy(css = "riders>div>div>div:nth-child(2)")
    private WebElement postponementDate;

    private final String DUE_STATUS_CSS = "riders div[show-data]>div>div:nth-child(3) due-status ";
    @Visible
    @Name("Due Status")
    @FindBy(css = DUE_STATUS_CSS + " .label")
    private WebElement dueStatus;

    @Name("Due Status tooltip")
    @FindBy(css = DUE_STATUS_CSS + " cd-tooltip")
    private WebElement tooltip;

    @Visible
    @Name("Arrow button")
    @FindBy(css = ".action icon")
    private WebElement arrowButton;

    @Step("Get Service Name")
    public String getServiceName()
    {
        return serviceName.getText().trim();
    }

    @Step("Get Due Date")
    public String getDueDate()
    {
        return dueDate.getText().trim();
    }

    @Step("Get Assigned Date")
    public String getAssignedDate()
    {
        return assignedDate.getText().trim();
    }

    @Step("Get Range Date From")
    public String getRangeDateFrom()
    {
        List<org.openqa.selenium.WebElement> rangeDateFrom = rangeDate.findElements(By.cssSelector(".dateFrom strong"));
        return rangeDateFrom.size() > 0 ? rangeDateFrom.get(0).getText().replace("-", StringUtils.EMPTY).trim() : StringUtils.EMPTY;
    }

    @Step("Get Range Date To")
    public String getRangeDateTo()
    {
        List<org.openqa.selenium.WebElement> rangeDateTo = rangeDate.findElements(By.cssSelector(".dateTo strong"));
        return rangeDateTo.size() > 0 ? rangeDateTo.get(0).getText().trim() : StringUtils.EMPTY;
    }

    @Step("Get PostponementDate")
    public String getPostponementDate()
    {
        List<org.openqa.selenium.WebElement> postponementDates = postponementDate.findElements(By.cssSelector(".postponed-date .detail"));
        return postponementDates.size() > 0 ? postponementDates.get(0).getText().trim() : StringUtils.EMPTY;
    }

    @Step("Get Due Status")
    public String getDueStatus()
    {
        return dueStatus.getText().trim();
    }

    @Step("Get Due Status Tooltip")
    public String getDueStatusTooltip()
    {
        dueStatus.moveToElement();
        wait.until(ExpectedConditions.visibilityOf(tooltip));
        return tooltip.lockScroll().getText().trim();
    }

    @Step("Click Arrow Button")
    public <T extends BasePage<T>> T clickArrowButton(Class<T> classObjectPage)
    {
        arrowButton.click();
        waitForSpinnerToFinish();
        return PageFactory.newInstance(classObjectPage);
    }
}
