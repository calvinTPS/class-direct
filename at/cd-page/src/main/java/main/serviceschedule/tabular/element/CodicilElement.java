package main.serviceschedule.tabular.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.HtmlElement;
import helper.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.WebElement;

@FindBy(css = "md-card-content")
public class CodicilElement extends HtmlElement
{
    @Visible
    @Name("Codicil name")
    @FindBy(css = ".title, .service-name")
    private WebElement codicilName;

    @Visible
    @Name("Due Date")
    @FindBy(css = ".due-date .detail")
    private WebElement dueDate;

    @Visible
    @Name("Job Status")
    @FindBy(css = "riders>div>div>div:nth-child(2)>div:nth-child(2)")
    private WebElement jobStatus;

    @Visible
    @Name("Imposed date")
    @FindBy(css = ".imposed-date .detail")
    private WebElement imposedDate;

    @Visible
    @Name("Due Status")
    @FindBy(css = "riders>div>div>div:nth-child(3) .label")
    private WebElement dueStatus;

    @Name("Tooltip")
    @FindBy(css = "cd-tooltip")
    private WebElement tooltip;

    @Visible
    @Name("Arrow button")
    @FindBy(css = ".action icon")
    private WebElement arrowButton;

    @Step("Get Service Name")
    public String getCodicilName()
    {
        return codicilName.getText().trim();
    }

    @Step("Get Due Date")
    public String getDueDate()
    {
        return dueDate.getText().trim();
    }

    @Step("Get Job Status")
    public String getJobStatus()
    {
        return jobStatus.getText().trim();
    }

    @Step("Get Imposed Date")
    public String getImposedDate()
    {
        return imposedDate.getText().trim();
    }

    @Step("Get Due Status")
    public String getDueStatus()
    {
        return dueStatus.getText().trim();
    }

    @Step("Get Due Status Tooltip")
    public String getDueStatusTooltip()
    {
        dueStatus.moveToElement();
        wait.until(ExpectedConditions.visibilityOf(tooltip));
        return tooltip.lockScroll().getText().trim();
    }

    @Step("Click Arrow Button")
    public <T extends BasePage<T>> T clickArrowButton(Class<T> classObjectPage)
    {
        arrowButton.click();
        waitForSpinnerToFinish();
        return PageFactory.newInstance(classObjectPage);
    }
}
