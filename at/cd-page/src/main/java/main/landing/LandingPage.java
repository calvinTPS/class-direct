package main.landing;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.ExtraExpectedConditions;
import helper.PageFactory;
import main.administration.AdministrationPage;
import main.common.TopBarPage;
import main.common.filter.FilterBarPage;
import main.serviceschedule.ServiceSchedulePage;
import main.vessellist.VesselListPage;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import typifiedelement.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import static constant.Config.GLOBAL_SPINNER_SPAWN_TIME;

public class LandingPage extends BasePage<LandingPage> {
    public static TopBarPage getTopBar() {
        return PageFactory.newInstance(TopBarPage.class);
    }

    @Visible
    @Name("Vessel List Tab")
    @FindBy(css = "tabs-master .tab-asset-list")
    private WebElement vesselListTab;

    @Visible
    @Name("Service Schedule Tab")
    @FindBy(css = "tabs-master .tab-survey-schedule")
    private WebElement serviceScheduleTab;

    @Visible
    @Name("Administration Tab")
    @FindBy(css = "tabs-master .tab-administration")
    private WebElement administrationTab;

    @Step("Click vessel list tab")
    public VesselListPage clickVesselListTab() {
        if(!isTabSelected(vesselListTab))
        {
            vesselListTab.click();
        }
        return PageFactory.newInstance(VesselListPage.class);
    }

    /**
     * This method is used to get VesselListPage after LandingPage loads with VesselListPage as
     * the default page displayed
     * <p>
     * {@link #clickVesselListTab()} is used when {@link VesselListPage} is a result of the tab
     * click
     *
     * @return {@code VesselListPage}
     */
    @Step("Get vessel list page")
    public VesselListPage getVesselListPage() {
        return PageFactory.newInstance(VesselListPage.class);
    }

    @Step("Get Filter Bar Page")
    public FilterBarPage getFilterBarPage() {
        return PageFactory.newInstance(FilterBarPage.class);
    }

    @Step("Click Administration Tab")
    public AdministrationPage clickAdministrationTab() {
        if(!isTabSelected(administrationTab))
        {
            administrationTab.click();
            try
            {
                BaseTest.newWaitWithTimeout(GLOBAL_SPINNER_SPAWN_TIME).until(ExtraExpectedConditions.visibilityOfSpinner());
            }
            catch (TimeoutException ignore)
            {

            }
        }
        return PageFactory.newInstance(AdministrationPage.class);
    }

    @Step("Click Service Schedule Tab")
    public ServiceSchedulePage clickServiceScheduleTab() {
        if(!isTabSelected(serviceScheduleTab))
        {
            serviceScheduleTab.click();
            try
            {
                BaseTest.newWaitWithTimeout(GLOBAL_SPINNER_SPAWN_TIME).until(ExtraExpectedConditions.visibilityOfSpinner());
            }
            catch (TimeoutException ignore)
            {

            }
        }
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Is Tab Selected")
    private boolean isTabSelected(WebElement tab) {
        return Boolean.parseBoolean(tab.findElement(By.xpath("./ancestor-or-self::md-tab-item")).getAttribute("aria-selected"));
    }
}

