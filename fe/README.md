<p align="center">
  <img src="./dev-assets/images/LloydsReg_ClassDirect_MobileMockup2.jpg" alt="class-direct" width="640px;" >
</p>

# Table of Contents
* [Walkthrough](#walkthrough)
    * [Build System](#build-system)
    * [File Structure](#file-structure)
    * [Testing Setup](#testing-setup)
* [Getting Started](#getting-started)
    * [Dependencies](#dependencies)
    * [Installing](#installing)
    * [Running the App](#running-the-app)
        * [Gulp Tasks](#gulp-tasks)
        * [Testing](#testing)
    * [Generating Components](#generating-components)
* [Deployment](#deployment)

# Walkthrough
## Build System
CD uses Gulp and Webpack together for its build system. Yes, you don't need Gulp if you're using Webpack. This is true if your build system is only responsible for file manipulation. However, ours is not.

`Webpack` handles all file-related concerns:

* Transpiling from ES6 to ES5 with `Babel`
* Loading HTML files as modules
* Transpiling stylesheets and appending them to the DOM
* Refreshing the browser and rebuilding on file changes
* Hot module replacement for transpiled stylesheets
* Bundling the app
* Loading all modules
* Doing all of the above for `*.spec.js` files as well

`Gulp` is the orchestrator:

* Starting and calling Webpack
* Starting a development server (yes, Webpack can do this too)
* Generating boilerplate for the Angular app

## File Structure
We use a componentized approach for this project. This will be the eventual standard (and particularly helpful, if using Angular's new router) as well as a great way to ensure a tasteful transition to Angular 2, when the time is ripe. Everything--or mostly everything, as we'll explore (below)--is a component. A component is a self-contained concern--may it be a feature or strictly-defined, ever-present element of the UI (such as a header, sidebar, or footer). Also characteristic of a component is that it harnesses its own stylesheets, templates, controllers, routes, services, and specs. This encapsulation allows us the comfort of isolation and structural locality. Here's how it looks:

    src
      app/
        app.js * app entry file
        app.html * app template
      common/ * functionality pertinent to several components propagate into this directory
      components/ * where components live
        components.js * components entry file
        home/ * home component
          home.js * home entry file (routes, configurations, and declarations occur here)
          home.component.js * home "directive"
          home.controller.js * home controller
          home.styl * home styles
          home.html * home template
          home.spec.js * home specs (for entry, component, and controller)


## Testing Setup
All tests are also written in ES6. We use Webpack to take care of the logistics of getting those files to run in the various browsers, just like with our client files. This is our testing stack:

* Karma (test runner)
* Webpack + Babel (preprocess, transpile)
* Jasmine (specs)

To run tests, type `npm test` or `karma start` in the terminal. Read more about testing [below](#testing).

# Getting Started
## Dependencies
Tools needed to run this project:
* `node` and `npm`. `npm` comes installed with Node. Recommended: Node version 4.4.7 LTS

## Installing
* `npm install -g yarn gulp karma karma-cli webpack json-server cordova protractor` to install global cli dependencies
* `yarn install` to install dependencies
* `yarn add PACKAGE_NAME` to later install further dependencies
* `yarn remove PACKAGE_NAME` to remove dependencies

## Running the App
We use Gulp to build and launch the development environment. After you have installed all dependencies, you may run the app. Running `gulp` will bundle the app with `webpack`, launch a development server, and watch all files. The port will be displayed in the terminal.

### Gulp Tasks
Here's a list of available tasks:

* `webpack` (builds production version)
    * runs Webpack, which will transpile, concatenate, and compress (collectively, "bundle") all assets and modules into `dist/bundle.js`.
    It also prepares `index.html` to be used as application entry point, links assets and created **dist** version of our application.
    * See [https://github.com/webpack/docs/wiki/Configuration]() for how to configure Webpack.
* `serve`
    * starts a dev server via `browser-sync`, serving the `src` folder.
* `watch`
    * alias of `serve`.
* `default` (which is the default task that runs when typing `gulp` without providing an argument)
  * runs `serve`.
* `component`
    * scaffolds a new Angular component. [Read below](#generating-components) for usage details.

**WARNING**: If using VIM or IntelliJ/WebStorm, please read [this](https://webpack.github.io/docs/webpack-dev-server.html#working-with-editors-ides-supporting-safe-write) (disable "safe write" mode in IntelliJ, `:set backupcopy=yes` in VIM, otherwise webpack may often fail to pick up the file changes).

### Testing

#### Unit Tests
To run the tests, run `npm test` or `karma start`. Depending on the config, this may watch your existing files and re-run tests as your develop.

Karma combined with Webpack runs all files matching `*.spec.js` inside the `app` folder. This allows us to keep test files local to the component--which keeps us in good faith with continuing to build our app modularly. The file `spec.bundle.js` is the bundle file for **all** our spec files that Karma will run.

Be sure to define your `*.spec.js` files within their corresponding component directory. You must name the spec file like so, `[name].spec.js`.

To exclude some tests, use either `xdescribe` or `xit`. To only run certain tests, use `fdescribe` or `fit`.

#### End-to-End Tests

We use `Protractor` for running some end-to-end tests.

Run `npm run protractor` to set up the test drivers. Then run `npm run test:e2e` to run the end-to-end tests.

### Generating Components
Following a consistent directory structure between components offers us the certainty of predictability. We can take advantage of this certainty by creating a gulp task to automate the "instantiation" of our components. The component boilerplate task generates this:

    componentName/
      componentName.js // entry file where all its dependencies load
      componentName.component.js
      componentName.controller.js
      componentName.pug // a templating library, formerly known as "jade"
      componentName.scss // scoped to affect only its own template
      componentName.spec.js // contains passing demonstration tests

1. You may, of course, create these files manually, every time a new module is needed, but that gets quickly tedious.
To generate a component, run `gulp component --name componentName`.

1. The parameter following the `--name` flag is the name of the component to be created. Ensure that it is unique or it will overwrite the preexisting identically-named component.

1. The component will be created, by default, inside `src/app/components`. To change this, apply the `--parent` flag, followed by a path relative to `src/app/components/`.

1. For example, running `gulp component --name signup --parent auth` will create a `signup` component at `src/app/components/auth/signup`.  

1. Running `
gulp component --name mncns --parent assets/asset/codicils-listing/
` creates a `footer` component at `src/app/components/codicil-listing` folder.  

1. Because the argument to `--name` applies to the folder name **and** the actual component name, make sure to camelCase the component names.

### Generating Services
Works similar to generating components.

Use `gulp service --name asset` and an assetService.js file will be auto-generated and imported to the `services.js` file. Please re-visit the services.js file and re-order your variables so that it lints successfully.

# Deployment

## Deploying to Android

Please see [https://engineering/confluence/display/LREG/Deploying+to+Android]()
