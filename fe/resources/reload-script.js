const l = console.log;

/*
  * I am a simple timmer which is injected into the login page.
  * I automatically refresh the page if it is inactive for a period
  * of time */
((URL) => {
  l(`Login IDLE Timer started. URL is set to ${URL}`);

  let t;

  // I'm just a utility function to bind multiple events to
  // and Element
  const bindEvent = (target, func, events) => {
    events.split(' ').forEach(event => target[event]=func);
  }

  // I refresh the page, in this case, reload URL
  const refresh = () => {
    window.location.href = URL;  //Adapt to actual logout script
    l('Page is being refreshed');
  }

  // I reset the timer should there be any activity
  const resetTimer = () => {
    l('Timer is reset')
    clearTimeout(t);
    t = setTimeout(refresh, LOGIN_RELOAD_TIME);  // time is in milliseconds (1000 is 1 second)
  }

  // I start the process with a clear of all timeouts
  resetTimer()

  // I bind all events to the window
  bindEvent (window, resetTimer, 'onload onmousemove onmousedown onclick onkeypress');
})(LOGIN_URL)
