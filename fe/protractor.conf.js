/* eslint-disable */
const seleniumJar = require('selenium-server-standalone-jar');

exports.config = {
  seleniumPort: 4444,
  seleniumServerJar: seleniumJar.path,
  specs: [
    'e2e/*.spec.js',
  ]
};
