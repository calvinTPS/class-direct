/* eslint-disable */
require('babel-register');
const HappyPack = require('happypack');
const path = require('path');
const webpackConfig = require('./webpack.config');
const _ = require('lodash');

if (!process.env.NODE_ENV) process.env.NODE_ENV = 'test';

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      { pattern: 'spec.bundle.js', watched: false }
    ],
    exclude: [],
    plugins: [
      require("karma-junit-reporter"),
      require("karma-coverage"),
      require("karma-phantomjs-launcher"),
      require("karma-jasmine"),
      require("karma-sourcemap-loader"),
      require("karma-webpack"),
    ],
    preprocessors: {
      'spec.bundle.js': ['webpack', 'sourcemap']
    },
    webpack: {
      devtool: 'inline-source-map',
      module: {
        rules: webpackConfig.module.rules,
      },
      resolve: webpackConfig.resolve,
      plugins: _.filter(webpackConfig.plugins, (plugin) => plugin instanceof HappyPack),
    },
    webpackServer: {
      noInfo: true, // prevent console spamming when running in Karma!
    },
    reporters: ['dots', 'junit', 'coverage'],
    coverageReporter: {
      dir: 'test-coverage',
      reporters: [
        { type: 'html', subdir: 'html' },
        { type: 'cobertura', subdir: 'cobertura' }
      ]
    },
    junitReporter: {
      outputDir: 'test-results',
      outputFile: 'unit.xml'
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true,
  });
};
