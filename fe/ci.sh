runTests=1
mobileBuild=0

while getopts ":sm" opt; do
  case $opt in
    s)
      echo "-s option: Skipping tests"
      runTests=0
      ;;
    m)
      echo "-m option: Mobile build mode"
      mobileBuild=1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

function program_is_installed {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  type $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  echo "$return_"
}

if [ ! $(program_is_installed yarn) ]; then npm install -g yarn; fi
if [ ! $(program_is_installed gulp) ]; then npm install -g gulp; fi

# Install and clean
yarn install
npm run clean

# Check that it compiles fine
if [[ $mobileBuild -eq 0 ]]; then
  npm run production
else
  npm run mobile-dist
fi

# Get version number from package.json
PKG_VER=$(
    node<<EOF
    var obj = $(<package.json);
    console.log(obj.version);
EOF
)

# Set Version number in the mobile.html
echo "Building artifacts for $PKG_VER"
# @TODO make optional with flag
MOBILE_BUILD_VERSION="v$PKG_VER | FE $TAG_NUMBER"
sed "s/^.*BUILDNUMBER.*$/\<buildnumber\>$MOBILE_BUILD_VERSION\<\/buildnumber\>/g" www/mobile.html > tmp
mv tmp www/mobile.html

# Generate APP_VERSION file
echo "$PKG_VER" > "APP_VERSION"

# Unit tests
if [[ $runTests -eq 1 ]]; then
  echo "Running tests..."
  node_modules/karma/bin/karma start karma.ci.conf.js
  echo "Running tests...DONE."
fi

# End-to-end tests
# gulp e2e --ci

# Build images in be folder, convert to PNG and copy them to /www/assets
pushd ../be/static-file-generator
rm -rf ./dist
yarn install
echo "Building static files..."
yarn run prod
mkdir ../../fe/www/assets
cp -r dist/assets/img/ ../../fe/www/assets/img/
mkdir ../../fe/www/error
cp dist/error.html ../../fe/www/error/index.html
cp dist/maintenance.html ../../fe/www/error/maintenance.html
cp dist/download-failed.html ../../fe/www/error/download_failed.html
cp dist/download-timed-out.html ../../fe/www/error/download_timed_out.html
cp -r dist/assets/ ../../fe/www/error/assets/
echo "Building static files...DONE"
popd
