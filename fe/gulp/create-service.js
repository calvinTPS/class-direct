/* eslint-disable */
import gulp from 'gulp';
import { argv } from 'yargs';
import path from 'path';
import pathResolver from './path-resolver';
import paths from './paths';
import rename from 'gulp-rename';
import template from 'gulp-template';
import util from './util';

gulp.task('createService', () => {
  const name = argv.name;
  const destPath = path.join(pathResolver.resolveToServices());

  return gulp.src(paths.blankServiceTemplates)
    .pipe(template({
      name,
      upCaseName: util.capCamelCase(name),
      hyphenName: util.hyphenate(name),
    }))
    .pipe(rename((path) => { // eslint disable no-shadow
      path.basename = path.basename.replace(// eslint-disable-line no-param-reassign
        'temp',
        util.hyphenate(name),
      );
    }))
    .pipe(gulp.dest(destPath));
});
