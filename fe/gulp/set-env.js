import env from 'gulp-env';
import gulp from 'gulp';

gulp.task('set-production-env', () => {
  env({
    vars: {
      RUN_MODE: 'es',
      NODE_ENV: 'production',
    },
  });
});

gulp.task('set-development-env', () => {
  env({
    vars: {
      RUN_MODE: 'es',
    },
  });
});
