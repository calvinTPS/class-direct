import * as _ from 'lodash';

exports.hyphenate = (val) => _.kebabCase(val);

exports.capCamelCase = (val) => {
  const str = _.camelCase(val);
  return str.charAt(0).toUpperCase() + str.slice(1);
};

