import del from 'del';
import gulp from 'gulp';
import gutil from 'gulp-util';
import paths from './paths';

gulp.task('clean', (cb) => {
  del([paths.dest]).then((paths) => { // eslint-disable-line no-shadow
    gutil.log('[clean]', paths);
    cb();
  });
});

