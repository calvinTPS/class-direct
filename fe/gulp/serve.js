/* eslint-disable */
import _ from 'lodash';
import browserSync from 'browser-sync';
import cacheMiddleware from 'node-cache-middleware';
import chalk from 'chalk';
import colorsSupported from 'supports-color';
import devConfig from '../webpack.dev.config';
import gulp from 'gulp';
import historyApiFallback from 'connect-history-api-fallback';
import http from 'http';
import paths from './paths';
import prettyjson from 'prettyjson';
import projectVariablesRO from '../src/app/config/project-variables.js';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

let projectVariables = _.cloneDeep(projectVariablesRO);

const root = 'src';

const error = chalk.bold.red;
const success = chalk.green;

gulp.task('serve', () => {
  devConfig.entry.app = [
    // module required to make HRM working, responsible for all this webpack magic
    'webpack-hot-middleware/client?reload=true',
  ].concat(paths.entry); // application entry point

  let resolver;
  const donePromise = new Promise((resolve) => { resolver = resolve; });
  const compiler = webpack(devConfig);
  compiler.plugin('done', resolver);

  const forwardClassDirectGetRequest = (requestOption, req, res) => {
    console.log('CD GET Request', prettyjson.render(requestOption));
    http.get(requestOption, (apiRes) => {
      apiRes.on('data', (rbody) => {
        if (apiRes.statusCode !== 200) {
          res.writeHead(apiRes.statusCode, {
            'Content-Length': rbody.length,
            'Content-Type': 'text/plain'
          });
        }
        res.write(rbody);
      });

      apiRes.on('end', () => {
        res.end();
      });
    }).on('error', (e) => {
      console.log(`Error: ${e.message}. Perhaps Class Direct is not deployed locally?`);
    });
  };

  const parseRequestBody = (req, callback) => {
    let body = '';
    req.on('data', (chunk) => {
      body += chunk;
    });
    req.on('error', () => {
      res.end();
    });
    req.on('end', () => {
      callback(body);
    });
  };

  const forwardClassDirectRequest = (requestOption, req, res) => {
    console.log(`CD ${req.method} Request`, prettyjson.render(requestOption));
    parseRequestBody(req, (requestBody) => {
      requestOption.headers['Content-Length'] = requestBody.length;
      const request = http.request(requestOption, (apiRes) => {
        if (apiRes.statusCode !== 200 && apiRes.statusCode !== 401) {
          console.log(`Class Direct API response for ${requestOption.method} ${requestOption.path}:`, apiRes.statusCode);
        } else if (apiRes.statusCode === 401) {
          apiRes.on('data', (rbody) => {
            res.writeHead(401, {
              'Content-Length': rbody.length,
              'Content-Type': 'text/plain'
            });
            res.write(rbody);
          });

          apiRes.on('end', () => {
            res.end();
          });
        } else {
          apiRes.on('data', (chunk) => {
            res.write(chunk);
          });

          apiRes.on('end', () => {
            res.end();
          });
        }
      });
      request.write(requestBody);
      request.on('error', (e) => {
        console.log(`Error: ${e.message}. Perhaps Class Direct is not deployed locally?`);
      });
    })
  };

  const devServerConfig = devConfig.devServer;

  let cdHandler;
  let springHandler;

  const handler = (route) => {
    const customHandler = {
      route,
      handle: (req, res) => {
        console.log(res);
        const requestOption = _.cloneDeep(projectVariables.CD_API); // Clone because it's mutable across requests
        if (process.env.USERID) {
          /*
           * This is going to help you debug mobile emulators
           * and browsers like safari and IE
           * You can set the userID using an env variable.
           * run like:
           * $ USERID='101' gulp
           * Go fuck yer fiddlers
           */
          req.headers.userId = process.env.USERID;
        }
        _.assign(requestOption, {
          path: `${route}${req.url}`,
          method: req.method,
          headers: req.headers,
        });
        console.log('Dev server received call:', req.method, req.url);
        if (req.method == 'GET') {
          forwardClassDirectGetRequest(requestOption, req, res);
        } else {
          forwardClassDirectRequest(requestOption, req, res);
        }
      },
    }
    return customHandler;
  };

  // I reinitialize the middlewares with new projectVariables
  const initCDHandler = () => {
    cdHandler = handler(projectVariables.API_URL);
    springHandler = handler(projectVariables.SPRING_API_URL);
  }

  initCDHandler();

  const bs = browserSync.create();
  bs.init({
    browser: ["google chrome", "firefox", "iexplore"], //Yes!! without "R" in the end
    port: devServerConfig.port,
    open: false,
    server: { baseDir: root },
    middleware: [
      // {
      //   // OPT-IN MIDDLEWARE.
      //   // This middleware is WIP, improves all-round
      //   // work/life balance. It will also be removing
      //   // unwanted data from the payload till
      //   // backend implements payload reduction.
      //   // Also helps when working directly with AWS backend
      //   route: '/api/v1',
      //   handle: (req, res, next) => {
      //     req.header = (s) => req.headers[s];
      //     res.set = (head) => res.writeHead(
      //       res.statusCode, head
      //       );
      //     cacheMiddleware({
      //       durationMilliseconds: 30 * 1000, // cashed API calls for 30 seconds
      //     })(req, res, next);
      //   },
      // },
      // ,
      cdHandler,
      springHandler,
      {
        // Access http://localhost:3000/refresh-variables to reload project-variables.js
        route: '/refresh-variables',
        handle: (req, res, next) => {
          delete require.cache[require.resolve('../src/app/config/project-variables.js')]
          projectVariables = require('../src/app/config/project-variables.js');
          initCDHandler();
          const chunk = JSON.stringify(projectVariables);
          res.writeHead(200, {
            'Content-Length': chunk.length,
            'Content-Type': 'text/plain'
          });
          res.write(chunk);

          console.log(`Project Variables Reloaded :) API URL is ${projectVariables.API_URL}`);
          res.end();
        },
      },
      historyApiFallback(),
      webpackDevMiddleware(compiler, {
        stats: {
          colors: colorsSupported,
          chunks: false,
          modules: false,
          progress: true,
        },
        publicPath: devConfig.output.publicPath,
      }),
      webpackHotMiddleware(compiler),
    ],
  });

  return donePromise;
});
