import _ from 'lodash';
import { argv } from 'yargs';
import gulp from 'gulp';
import path from 'path';
import pathResolver from './path-resolver';
import paths from './paths';
import rename from 'gulp-rename';
import template from 'gulp-template';
import util from './util';

gulp.task('component', () => {
  const name = argv.name;
  const parentPath = argv.parent || '';
  const destPath = path.join(pathResolver.resolveToComponents(), parentPath, util.hyphenate(name));

  return gulp.src(paths.blankTemplates)
    .pipe(template({
      name,
      upCaseName: util.capCamelCase(name),
      camelCaseName: _.camelCase(name),
      hyphenName: util.hyphenate(name),
    }))
    .pipe(rename((path) => { // eslint-disable-line no-shadow
      path.basename = path.basename.replace(// eslint-disable-line no-param-reassign
        'temp',
        util.hyphenate(name),
      );
    }))
    .pipe(gulp.dest(destPath));
});

