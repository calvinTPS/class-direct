/* eslint-disable */
import { argv } from 'yargs';
import { protractor } from 'gulp-protractor';
import gulp from 'gulp';

gulp.task('e2e', ['serve'], () => {
  gulp.src(['./e2e/**/*.spec.js'])
    .pipe(protractor({
      configFile: argv.ci ? './protractor.ci.conf.js' : './protractor.conf.js',
      args: ['--baseUrl', 'http://127.0.0.1:3000'],
    }))
    .on('error', (e) => { throw e; })
    .once('end', () => process.exit());
});
