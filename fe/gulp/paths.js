import path from 'path';

module.exports = {
  dest: path.join(__dirname, '..', 'www'),
  entry: [
    path.join(__dirname, '..', 'src', 'app/app.js'),
  ],
  blankTemplates: path.join(__dirname, '..', 'generator', 'component/**/*.**'),
  blankServiceTemplates: path.join(__dirname, '..', 'generator', '*.service.js'),
};

