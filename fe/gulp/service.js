/* eslint-disable */
import { argv } from 'yargs';
import chalk from 'chalk';
import gulp from 'gulp';
import insertLines from 'gulp-insert-lines';
import util from 'util';

const success = chalk.green;

gulp.task('service', ['createService'], () => {
  const name = argv.name;
  gulp.src('./src/app/services/services.js')
    .pipe(insertLines({
      before: /\/\/\s@insertImport/i,
      lineBefore: `import ${util.capCamelCase(name)}Service from './${util.hyphenate(name)}.service';`,
    }))
    .pipe(insertLines({
      before: /\/\/\s@insertInstance/i,
      lineBefore: `    ${util.capCamelCase(name)}Service,`,
    }))
    .pipe(gulp.dest('./src/app/services/'));

  console.log(
    `${success('Service Created.')}`,
    'Imports added to services.js file. Please re-order your variables in said file.');
});
