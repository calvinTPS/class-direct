/* eslint-disable */
import * as _ from 'lodash';
import colorsSupported from 'supports-color';
import distConfig from '../webpack.dist.config';
import gulp from 'gulp';
import gutil from 'gulp-util';
import paths from './paths';
import webpack from 'webpack';

gulp.task('webpack', ['clean', 'set-production-env'], (cb) => {
  distConfig.entry.app = paths.entry;

  webpack(distConfig, (err, stats) => {
    // Workaround because Webpack fails to exit with non-zero code on lint error
    // See https://github.com/webpack/webpack/issues/708
    const e = err || stats.compilation.errors[0];
    if (e) {
      // Print out compilation errors nicely
      if (stats != null) {
        _.each(stats.compilation.errors, (error) => { console.error(error.message) });
      }
      throw new gutil.PluginError('webpack', e);
    }

    gutil.log('[webpack]', stats.toString({
      colors: colorsSupported,
      chunks: false,
      errorDetails: true,
    }));

    cb();
  });
});
