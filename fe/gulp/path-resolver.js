import path from 'path';

const root = 'src';

// helper methods for resolving paths
exports.resolveToApp = (glob = '') =>
  path.join(root, 'app', glob); // app/{glob}

exports.resolveToComponents = (glob = '') =>
  path.join(root, 'app/components', glob); // app/components/{glob}

exports.resolveToServices = (glob = '') =>
  path.join(root, 'app/services'); // app/services/{glob}
