import angular from 'angular';

export default (element, selector) => angular.element(element[0].querySelector(selector));
