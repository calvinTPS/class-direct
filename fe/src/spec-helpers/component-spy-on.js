/**
 * From https://velesin.io/2016/08/23/unit-testing-angular-1-5-components/
 *
 * Shallow renders the child component and somehow assert what attributes
 * were passed to the child component without having to parse and know its template.
 */

export default (moduleName) => {
  /* @ngInject */
  function componentSpy($provide) {
    componentSpy.bindings = [];

    $provide.decorator(`${moduleName}Directive`, ($delegate) => {
      const component = $delegate[0];

      component.template = '';
      component.controller = class {
        constructor() {
          componentSpy.bindings.push(this);
        }
      };

      return $delegate;
    });
  }

  return componentSpy;
};
