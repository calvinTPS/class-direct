/* global inject */
/* eslint-disable no-underscore-dangle, angular/window-service*/
import AppSettings from 'app/config/project-variables.js';
import ViewService from 'app/services/view.service';

describe('View Service', () => {
  let makeService;
  let $compile;

  beforeEach(window.module(($provide) => {
    const mockTranslateFilter = value => value;
    $provide.value('AppSettings', AppSettings);
    $provide.value('translateFilter', mockTranslateFilter);
    $provide.value('safeTranslateFilter', mockTranslateFilter);
  }));

  beforeEach(inject((_$compile_) => {
    $compile = _$compile_;
    makeService = () => new ViewService($compile, AppSettings);
  }));

  it('isServiceScheduleOnDetailMode should return correct value', () => {
    const service = makeService();
    expect(service.isServiceScheduleOnDetailMode).toBe(false);
    service.isServiceScheduleOnDetailMode = true;
    expect(service.isServiceScheduleOnDetailMode).toBe(true);
  });

  it('get focusedTableHeaderId() should return correct value', () => {
    const service = makeService();
    expect(service.focusedTableHeaderId).toEqual(AppSettings.TABLE_HEADER_SORT_AND_IDS.ASSET_NAME);
    service.focusedTableHeaderId = 'Foo';
    expect(service.focusedTableHeaderId).toEqual('Foo');
  });

  it('get isSurveyScheduleViewToggled() should return correct value', () => {
    const service = makeService();
    expect(service.isSurveyScheduleViewToggled).toBe(false);
    service.isSurveyScheduleViewToggled = true;
    expect(service.isSurveyScheduleViewToggled).toBe(true);
  });

  it('get isExpandableCardAccordion() should return correct value', () => {
    const service = makeService();
    expect(service.isExpandableCardAccordion).toBe(false);
    service.isExpandableCardAccordion = true;
    expect(service.isExpandableCardAccordion).toBe(true);
  });

  it('get isAssetHeaderVisible() should return correct value', () => {
    const service = makeService();
    service.isAssetHeaderVisible = true;
    expect(service.isAssetHeaderVisible).toBe(true);
  });

  it('get isCollapsedAssetHeaderExpandable() should return correct value', () => {
    const service = makeService();
    service.isCollapsedAssetHeaderExpandable = true;
    expect(service.isCollapsedAssetHeaderExpandable).toBe(true);
  });

  it('getVesselListViewType() should return correct value', () => {
    const service = makeService();
    service.setVesselListViewType('fleet');
    expect(service._vesselListViewType).toBe('fleet');
  });

  describe('dismissServiceScheduleDetailedMode', () => {
    it('sets the right bools', () => {
      const service = makeService();
      service.dismissServiceScheduleDetailedMode();

      expect(service.isServiceScheduleOnDetailMode).toEqual(false);
      expect(service.isAssetHeaderVisible).toEqual(true);
      expect(service.isCollapsedAssetHeaderExpandable).toEqual(true);
    });
  });

  describe('getServiceScheduleViewType', () => {
    it('gets the view type correctly', () => {
      const service = makeService();

      service.setServiceScheduleViewType({
        location: 'locationA',
        type: 'tabular',
      });
      let type = service.getServiceScheduleViewType('locationA');
      expect(type).toEqual('tabular');

      service.setServiceScheduleViewType({
        location: 'locationA',
        type: 'gantt',
      });
      type = service.getServiceScheduleViewType('locationA');
      expect(type).toEqual('gantt');
    });
  });

  describe('scrollPos', () => {
    it('getter and setter works', () => {
      const service = makeService();
      expect(service.scrollPos).toEqual({});
      service.scrollPos.asset = 50;
      expect(service.scrollPos.asset).toEqual(50);
    });
  });

  describe('setServiceScheduleViewType', () => {
    it('sets the view type correctly', () => {
      const service = makeService();

      service.setServiceScheduleViewType({
        location: 'locationA',
        type: 'tabular',
      });
      expect(service._serviceScheduleViewType).toEqual({
        locationA: 'tabular',
      });

      service.setServiceScheduleViewType({
        location: 'locationB',
        type: 'tabular',
      });
      expect(service._serviceScheduleViewType).toEqual({
        locationA: 'tabular',
        locationB: 'tabular',
      });

      service.setServiceScheduleViewType({
        location: 'locationA',
        type: 'gantt',
      });
      expect(service._serviceScheduleViewType).toEqual({
        locationA: 'gantt',
        locationB: 'tabular',
      });
    });
  });
});
