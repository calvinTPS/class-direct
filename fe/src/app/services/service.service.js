import * as _ from 'lodash';
import BaseService from './base.service';
import ChecklistItemModel from 'app/common/models/checklist-item';
import ServiceModel from 'app/common/models/service';
import TaskModel from 'app/common/models/task';

export default class ServiceService extends BaseService {
  /* @ngInject */
  constructor(
    AppSettings,
    Restangular,
  ) {
    super(arguments);
    this._assetApiPath = this._AppSettings.API_ENDPOINTS.Assets;
    this._serviceApiPath = this._AppSettings.API_ENDPOINTS.ASSET.Services;
    this._serviceApiPathDetailed = this._AppSettings.API_ENDPOINTS.ASSET.ServicesDetailed;
  }

  async getAll(assetId) {
    const services = await this._Restangular
      .one(this._assetApiPath, assetId)
      .customGET(this._serviceApiPath);

    _.forEach(services, (object, index) => {
      services[index] = Reflect.construct(ServiceModel, [object]);
    });

    return services;
  }

  /** get one service of an asset
   */
  async getOne(assetId, serviceId) {
    const services = await this._Restangular
      .one(this._assetApiPath, assetId)
      .customGET(this._serviceApiPath);

    const service = _.find(services, serv => serv.id === serviceId);
    return Reflect.construct(ServiceModel, [service]);
  }

  async getChecklist(assetId, serviceId) {
    const assetServiceUrl = `${this._assetApiPath}/${assetId}/${this._serviceApiPath}/${serviceId}`;
    const checklist = await this._Restangular
      .one(assetServiceUrl)
      .customPOST({}, this._AppSettings.API_ENDPOINTS.SERVICE.Checklist,
      {}, { Fields: ChecklistItemModel.PAYLOAD_FIELDS.CHECK_LIST.join(',') });

    if (!checklist.checklistGroups) return null;
    return this._assignModelToHierarchyChecklist(checklist.checklistGroups);
  }

  async getTasklist(assetId, serviceId,
          { page, size } = this._AppSettings.DEFAULT_PARAMS.PAGINATION) {
    const assetServiceUrl = `${this._assetApiPath}/${assetId}/${this._serviceApiPath}/${serviceId}`;
    const tasks = await this._Restangular
      .one(assetServiceUrl)
      .customPOST({}, this._AppSettings.API_ENDPOINTS.SERVICE.Tasklist,
      {}, { Fields: TaskModel.PAYLOAD_FIELDS.TASK_LIST.join(',') });

    const tasklistItems = _.get(tasks, 'servicesH[0].itemsH');
    return _.isEmpty(tasklistItems) ? null : this._assignModelToHierarchyTasks(tasklistItems);
  }

  async getPMSableTasks(assetId) {
    const tasks = await this._Restangular
      .one(`${this._assetApiPath}/${assetId}/${this._AppSettings.API_ENDPOINTS.ASSET.Tasks}?pmsAble=true`)
      .get({}, { Fields: TaskModel.PAYLOAD_FIELDS.TASK_LIST.join(',') });

    return _.isEmpty(tasks.servicesH) ? null : this._assignModelToHierarchyTasks(tasks.servicesH);
  }

  /**
   *
   *
   * @param {any} services - new services payload
   * {
   *   services: [...],
   *   repeatedServices: [...],
   * }
   * @returns an array of services
   *
   * @memberOf ServiceService
   */
  restructureServiceArray(services) {
    _.forEach(services.repeatedServices, (service) => {
      /* eslint-disable no-param-reassign */
      if (service.dueDate) {
        service.dueDateEpoch = Date.parse(service.dueDate);
      }
      if (service.lowerRangeDate) {
        service.lowerRangeDateEpoch = Date.parse(service.lowerRangeDate);
      }
      if (service.upperRangeDate) {
        service.upperRangeDateEpoch = Date.parse(service.upperRangeDate);
      }
      if (service.postponementDate) {
        service.postponementDateEpoch = Date.parse(service.postponementDate);
      }
      /* eslint-ensable no-param-reassign */
    });

    return _.concat(services.services || [], services.repeatedServices || []);
  }

  /**
   * @param  {} riders - the riders we need to analyze and hydrate repeated services
   * @returns an array of services that look like the /services/query API
   */
  hydrateRepeatedServices(servicesToHydrate) {
    const services = _.cloneDeep(
      this.restructureServiceArray(_.cloneDeep(servicesToHydrate))
    );

    // I iterate through each service and...
    _.forEach(services, (obj, index) => {
      // I check if its repeatOf value is a proper integer
      if (obj.repeatOf != null && obj.repeatOf !== 0) {
        // If repeatOf is valid, I merge this service's info with
        // that of the service that it is a repeat of,
        // except the dueDate, lowerRangeDate, upperRangeDate, and repeatOf values
        const sourceServiceWithOmitedInfo =
          _.omit( // omit the keys that should not be copied
            _.find(services, { id: _.toSafeInteger(obj.repeatOf) }),
            this._AppSettings.SERVICES.DATA_NOT_TO_HYDRATE);

        // I merge the service information from the source service
        // ES2017 :) http://stackoverflow.com/a/32372430/430773
        services[index] = {
          ...services[index],
          ...sourceServiceWithOmitedInfo,
        };
      }
    });

    return _.cloneDeep(services);
  }

   // See the spec file for the hierarchy
  _assignModelToHierarchyTasks(items) {
    items.forEach((item) => {
      if (!_.isEmpty(item.tasksH)) {
        _.forEach(item.tasksH, (object, index) => {
          // eslint-disable-next-line no-param-reassign
          item.tasksH[index] = Reflect.construct(TaskModel, [object, true]);
        });
      }

      if (!_.isEmpty(item.itemsH)) {
        this._assignModelToHierarchyTasks(item.itemsH);
      }
    });
    return items;
  }

   // See the spec file for the hierarchy
  _assignModelToHierarchyChecklist(groups) {
    _.forEach(groups, (group) => {
      if (!_.isEmpty(group.checklistItems)) {
        _.forEach(group.checklistItems, (object, index) => {
          // eslint-disable-next-line no-param-reassign
          group.checklistItems[index] = Reflect.construct(ChecklistItemModel, [object]);
        });
      }

      if (!_.isEmpty(group.checklistSubgrouplist)) {
        this._assignModelToHierarchyChecklist(group.checklistSubgrouplist);
      }
    });

    return groups;
  }
}
