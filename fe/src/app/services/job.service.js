import * as _ from 'lodash';
import AttachmentModel from 'app/common/models/attachment/index';
import BaseService from './base.service';
import { cacheable } from 'app/common/decorators';
import JobModel from 'app/common/models/job';
import ReportModel from 'app/common/models/report/';

export default class JobService extends BaseService {
  /* @ngInject */
  constructor(
    AppSettings,
    Restangular,
  ) {
    super(arguments);
  }

  @cacheable
  async getJobs(assetId) {
    let jobs = [];

    jobs = await this._Restangular.one(this._AppSettings.API_ENDPOINTS.Assets, assetId)
      .customGET(this._AppSettings.API_ENDPOINTS.JOBS.ROOT);

    _.forEach(jobs, (object, index) => {
      jobs[index] = Reflect.construct(JobModel, [object]);
    });
    return jobs;
  }

  @cacheable
  async getJob(jobId) {
    const jobObj = await this._Restangular
      .one(this._AppSettings.API_ENDPOINTS.JOBS.ROOT, jobId)
      .get();

    return Reflect.construct(JobModel, [jobObj]);
  }

  @cacheable
  async getJobReports(jobId) {
    const jobReports = await this._Restangular
      .one(`${this._AppSettings.API_ENDPOINTS.JOBS.ROOT}/${jobId}/${this._AppSettings.API_ENDPOINTS.JOBS.REPORTS}`)
      .get();

    _.forEach(jobReports, (jobReport, index) => {
      jobReports[index] = Reflect.construct(ReportModel, [jobReport]);
    });
    return jobReports;
  }

  @cacheable
  async getJobAttachments(jobId, category) {
    const jobAttachmentsResponse = await this._Restangular
      .one(`${this._AppSettings.API_ENDPOINTS.JOBS.ROOT}/${jobId}/${this._AppSettings.API_ENDPOINTS.JOBS.ATTACHMENTS}`)
      .get();

    let jobAttachments = [];
    _.forEach(jobAttachmentsResponse.attachments, (attachment, index) => {
      jobAttachments.push(Reflect.construct(AttachmentModel, [attachment]));
    });

    if (category) {
      jobAttachments = _.filter(jobAttachments, attachment =>
        attachment.category && attachment.category.indexOf(category) >= 0);
    }

    return jobAttachments;
  }
}
