import _ from 'lodash';
import Base from 'app/base';

/**
 * PermissionsService provides a centralized place to
 * lookup permissions for frequently used view categories.
 */
export default class PermissionsService extends Base {

  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
  }

  /**
   * Determines if user has permission to view the RBAC section.
   *
   * @param user The user model.
   * @param rbacObject The RBAC object from the app config.
   * @param rbacSetting The RBAC key string that matchers the app config.
   */
  _userHasPermission(user, rbacKey) {
    const rbacObj = this._AppSettings.RBAC_CONFIG[rbacKey];
    if (!rbacObj) {
      throw new Error(`Could not find matching RBAC config object for key ${rbacKey}`);
    }

    if (rbacObj.type === 'BLACKLIST') {
      if (rbacObj.roles.length) {
        return _.indexOf(rbacObj.roles, user.role) < 0;
      }
      return true;
    } else if (rbacObj.type === 'WHITELIST') {
      if (rbacObj.roles.length) {
        return _.indexOf(rbacObj.roles, user.role) >= 0;
      }
      return false;
    }

    throw new Error('RBAC config did not contain whitelist or blacklist directive');
  }

  /**
   * @returns {boolean} True if user is allowed to edit profile or password.
   */
  canEditProfileOrPassword(user) {
    return this._userHasPermission(user, PermissionsService.RBAC_KEYS.EDIT_PROFILE_PASSWORD);
  }

  /**
   * @returns {boolean} True if user can access survey request feature for this particular asset.
   */
  canAccessSurveyRequest(asset, user) {
    if (!asset || !user) {
      return false;
    }

    /**
     *                          | LR Admin | LR Internal | Ship Builder | Client
     * LR classed               |    o     |      o      |      o       |   o
     * Non-LR classed with MMS  |    o     |      o      |      o       |   o
     * Non-LR classed w/t MMS   |    x     |      x      |      x       |   x
     */

    return this._userHasPermission(user, PermissionsService.RBAC_KEYS.SURVEY_REQUEST) &&
      (!asset.restricted && asset.hasActiveServices && !asset.isEORAsset);
  }

  /**
   * @returns {boolean} True if user can access MPMS feature for this particular asset.
   */
  canAccessMPMS(asset, user) {
    if (!asset || !user) {
      return false;
    }

    if (!asset.pmsApplicable) {
      return false;
    } else if (this._userHasPermission(user, PermissionsService.RBAC_KEYS.MPMS)) {
      return asset.isAccessibleLRAsset && !asset.isEORAsset;
    }
    return false;
  }

  /**
   * @returns {boolean} True if user can access Codicil and Certificates and records
   * for this particular asset.
   */
  canAccessCertsAndCodicils(asset) {
    if (!asset) return false;

    return asset.isAccessibleLRAsset || !asset.isLRAsset;
  }

  /**
   * @returns {boolean} True if user can access
   * all sisters, register book sisters, technical sisters
   * for this particular asset.
   *                   | Accessible | Restricted | EOR | Equasis/Thetis   | Flag user
   * Technical Sisters |      /     |      X     |  X  |       X          |     /
   * Reg Book Sisters  |      /     |      X     |  X  |       X          |     /
   *
   * accessibleAsset = !asset.restricted && (asset.isMMSService || asset.isLRAsset);
   */
  canAccessSistersSection(asset, user) {
    if (!asset || !user) return false;

    return this._userHasPermission(user, PermissionsService.RBAC_KEYS.SISTERS) &&
      (!asset.restricted && (asset.isMMSService || asset.isLRAsset));
  }

  /**
   * @returns {boolean} Whether or not the user can view the asset detail
   * based on the user role and asset class.
   */
  canViewAssetDetailInfo(section, asset, user) {
    if (!asset || !user) return false;

    switch (section) {
      case PermissionsService.ASSET_DETAIL_INFO.DOC_COMPANY:
      case PermissionsService.ASSET_DETAIL_INFO.GROUP_OWNER:
      case PermissionsService.ASSET_DETAIL_INFO.OPERATOR:
      case PermissionsService.ASSET_DETAIL_INFO.SHIP_MANAGER:
      case PermissionsService.ASSET_DETAIL_INFO.TECHNICAL_MANAGER:
      case PermissionsService.ASSET_DETAIL_INFO.LEAD_TECHNICAL_SISTER:
        return !user.isEquasisThetis;
      case PermissionsService.ASSET_DETAIL_INFO.EQUIPMENT_DETAILS:
        return asset.isLRAsset && !user.isEquasisThetis;
      case PermissionsService.ASSET_DETAIL_INFO.RULE_SET:
        return asset.isLRAsset && !asset.restricted;
      default:
        return true;
    }
  }

  /**
   * @returns {boolean} True if user can access jobs feature for this particular asset.
   */
  canAccessJobs(asset, user) {
    if (!asset || !user) {
      return false;
    }

    return asset.isFromMAST && !user.isEquasisThetis && !asset.restricted;
  }

  /**
   * @returns {boolean} True if user is not EOR user, False if user is equasis thetis user
   */
  canShowNationalAdminButton(user) {
    if (!user) return false;

    return this._userHasPermission(user, PermissionsService.RBAC_KEYS.NATIONAL_ADMINISTRATION);
  }

  /**
   * @returns {boolean} True if user is not equasis nor thetis user
   */
  canPrintExport(user) {
    if (!user) return false;

    return !user.isEquasisThetis;
  }

  /**
   * @returns {boolean} True if user has permission to view the notification block on user detail
   */
  canViewNotifications(user) {
    if (!user) return false;

    return this._userHasPermission(user, PermissionsService.RBAC_KEYS.NOTIFICATIONS);
  }

  /**
   * @returns {boolean} True if user is allowed to favourite/unfavourite the asset.
   */
  canFavouriteAsset(user) {
    if (!user) return false;

    return this._userHasPermission(user, PermissionsService.RBAC_KEYS.FAVOURITE);
  }

  /**
   * @returns {boolean} True if user is allowed to view Tasklist and Checklist.
   */
  canViewChecklistTasklist(user) {
    if (_.isEmpty(user)) return false;

    return !user.isEquasisThetis;
  }

   /**
   * @returns {boolean} True if user is allowed to view MNCNs.
   */
  canAccessMNCN(user, asset) {
    if (_.isEmpty(user) || _.isEmpty(asset)) return false;
    if (this._userHasPermission(user, PermissionsService.RBAC_KEYS.MNCN)) {
      if (user.isLRAdmin || user.isLRInternal || user.isLRSupport || user.isClient) {
        return true;
      } else if (user.isFlagUser && (user.flagCode === _.get(asset, 'flagStateDto.flagCode', null))) {
        return true;
      }
      return false;
    }

    return false;
  }


  /**
   * Constants to represents fields/sections in asset details.
   *
   * The values should be the keys used in the asset detail's fields-schema.json
   */
  static ASSET_DETAIL_INFO = {
    LEAD_TECHNICAL_SISTER: 'leadTechnicalSister',
    RULE_SET: 'rulesetDetailsDto',
    EQUIPMENT_DETAILS: 'equipmentDetails',
    GROUP_OWNER: 'groupOwner',
    OPERATOR: 'operator',
    SHIP_MANAGER: 'shipManager',
    TECHNICAL_MANAGER: 'technicalManager',
    DOC_COMPANY: 'doc',
  }

  /**
   * Constants to represent RBAC config keys as in app settings.
   */
  static RBAC_KEYS = {
    EDIT_PROFILE_PASSWORD: 'EDIT_PROFILE_PASSWORD',
    FAVOURITE: 'FAVOURITE',
    MPMS: 'MPMS',
    NATIONAL_ADMINISTRATION: 'NATIONAL_ADMINISTRATION',
    NOTIFICATIONS: 'NOTIFICATIONS',
    SISTERS: 'SISTERS',
    SURVEY_REQUEST: 'SURVEY_REQUEST',
    MNCN: 'MNCN',
  }
}
