/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable angular/document-service, one-var, no-unused-vars, no-underscore-dangle,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import DownloadService from 'app/services/download.service';
import UserService from 'app/services/user.service.js';

describe('DownloadService', () => {
  let $rootScope; // eslint-disable-line
  let mockDocument,
    mockPromise,
    mockMdDialog,
    mockRestangular,
    mockUserService,
    mockUtils;
  let downloadService;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;

    mockDocument = [
      {
        body: {},
      },
    ];

    // Mock Service requests
    mockPromise = {
      customGET: () => { },
      get: () => { },
      getList: () => { },
      post: () => { },
      withHttpConfig: () => ({ post: () => { } }),
    };

    mockMdDialog = {
      show: () => { },
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one: () => mockPromise,
      all: () => mockPromise,
      withConfig: () => ({
        one: () => mockPromise,
        all: () => mockPromise,
      }),
    };

    mockUtils = {
      downloadBlobWithFilename: () => { },
      downloadFile: () => { },
      getFileExtension: () => 'xxx',
    };

    mockUserService = {
      masqueradeId: null,
    };

    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    downloadService = new DownloadService(
      mockDocument,
      mockMdDialog,
      $rootScope,
      AppSettings,
      mockRestangular,
      mockUserService,
      mockUtils,
    );

    spyOn(downloadService, 'downloadFile').and.callThrough();
  }));

  describe('downloadCertificate()', () => {
    it.async('downloads the certificate', async () => {
      spyOn(mockPromise, 'post').and.returnValue(Promise.resolve({ response: 'TOKEN', fileName: 'test.pdf' }));
      await downloadService.downloadCertificate({
        certId: 1,
        jobId: 1,
      });
      const mobileDownload = {
        httpMethod: 'GET',
        fileName: 'Certificate',
        fileExtension: 'pdf',
      };
      expect(mockRestangular.all).toHaveBeenCalledWith(
        AppSettings.API_ENDPOINTS.CertificatesAttachment);
      expect(downloadService.downloadFile).toHaveBeenCalledWith({
        token: 'TOKEN',
        mobileDownload,
        downloadFileName: 'test.pdf',
      });
    });

    it.async('should not download the certificate when incomplete certificate info provided', async () => {
      spyOn(mockPromise, 'post').and.returnValue(Promise.resolve({ response: 'TOKEN' }));

      try {
        await downloadService.downloadCertificate({
          jobId: 1,
        });
      } catch (e) {
        expect(e).toEqual(new Error('Missing job ID or cert ID'));
        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(downloadService.downloadFile).not.toHaveBeenCalled();
      }

      try {
        await downloadService.downloadCertificate({
          certId: 1,
        });
      } catch (e) {
        expect(e).toEqual(new Error('Missing job ID or cert ID'));
        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(downloadService.downloadFile).not.toHaveBeenCalled();
      }

      try {
        await downloadService.downloadCertificate({});
      } catch (e) {
        expect(e).toEqual(new Error('Missing job ID or cert ID'));
        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(downloadService.downloadFile).not.toHaveBeenCalled();
      }

      try {
        await downloadService.downloadCertificate();
      } catch (e) {
        expect(e).toEqual(new Error('Missing job ID or cert ID'));
        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(downloadService.downloadFile).not.toHaveBeenCalled();
      }
    });
  });

  describe('downloadReport()', () => {
    it.async('downloads the report', async () => {
      spyOn(mockPromise, 'post').and.returnValue(Promise.resolve({ response: 'TOKEN', fileName: 'testFSR.pdf' }));
      const mockReport = {
        id: 2,
        jobId: 5,
        type: 'FSR',
      };
      const mobileDownload = {
        httpMethod: 'GET',
        fileName: mockReport.name,
        fileExtension: 'xxx',
      };
      await downloadService.downloadReport(mockReport);
      expect(mockRestangular.all).toHaveBeenCalledWith(
        `${AppSettings.API_ENDPOINTS.EXPORT.ROOT}/` +
        `${AppSettings.API_ENDPOINTS.EXPORT.REPORTS}`);
      expect(mockPromise.post).toHaveBeenCalledWith({
        reportId: 2,
        jobId: 5,
      });
      expect(downloadService.downloadFile).toHaveBeenCalledWith({
        token: 'TOKEN',
        mobileDownload,
        downloadFileName: 'testFSR.pdf',
      });

      const mockESPReport = {
        isESPReport: true,
        jobId: 5,
      };
      await downloadService.downloadReport(mockESPReport);
      expect(mockRestangular.all).toHaveBeenCalledWith(
        `${AppSettings.API_ENDPOINTS.EXPORT.ROOT}/` +
        `${AppSettings.API_ENDPOINTS.EXPORT.ESP_REPORT}`);
      expect(mockPromise.post).toHaveBeenCalledWith({
        jobId: 5,
      });
    });

    it.async('should not download the report when incomplete report info provided', async () => {
      spyOn(mockPromise, 'post').and.returnValue(Promise.resolve({ response: 'TOKEN' }));

      try {
        await downloadService.downloadReport({
          reportId: 1,
        });
      } catch (e) {
        expect(e).toEqual(new Error('Missing job ID'));
        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(downloadService.downloadFile).not.toHaveBeenCalled();
      }

      try {
        await downloadService.downloadReport({});
      } catch (e) {
        expect(e).toEqual(new Error('Missing job ID'));
        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(downloadService.downloadFile).not.toHaveBeenCalled();
      }

      try {
        await downloadService.downloadReport();
      } catch (e) {
        expect(e).toEqual(new Error('Missing report object'));
        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(downloadService.downloadFile).not.toHaveBeenCalled();
      }
    });

    it.async('should display modal is it is TM report', async () => {
      const mockReport = {
        jobId: 9,
        isTMReport: true,
      };
      spyOn(downloadService, 'showReportModal');
      await downloadService.downloadReport(mockReport);
      expect(downloadService.showReportModal).toHaveBeenCalledTimes(1);
      expect(mockRestangular.all).not.toHaveBeenCalled();
      expect(downloadService.downloadFile).not.toHaveBeenCalled();
    });
  });

  describe('showReportModal', () => {
    it.async('shows a modal when called', async () => {
      const mockTMReport = {
        shortName: 'TM',
        attachments: [
          {
            title: 'TM Report FileName',
            format: 'PDF',
            token: 'TOKEN2',
            reportType: 3,
            name: 'TM Report',
          },
          {
            title: 'TM Report FileName2',
            isArgonautFile: true,
            token: 'TOKEN1',
            reportType: 2,
            name: 'TM Report',
          },
        ],
      };
      const mobileDownloadPDF = {
        httpMethod: 'GET',
        fileName: 'TM Report',
        fileExtension: 'pdf',
      };

      const mobileDownloadZip = {
        httpMethod: 'GET',
        fileName: 'TM Report',
        fileExtension: 'zip',
      };
      const promise = { token: 'TOKEN2', mobileDownload: { httpMethod: 'GET', fileName: 'TM Report' } };
      const showPromise = Promise.resolve(promise);
      spyOn(mockMdDialog, 'show').and.returnValue(showPromise);

      downloadService.showReportModal(mockTMReport);

      const showArgs = mockMdDialog.show.calls.argsFor(0)[0];
      expect(showArgs.locals.isArgonautFilePresent).toBe(true);
      expect(showArgs.locals.title).toBe('cd-tm-report');
      expect(showArgs.locals.body).toBe('cd-tm-summary-report');
      const buttons = [
        {
          class: 'white',
          label: 'PDF',
          onClick: 'vm._$mdDialog.hide({ token: "TOKEN2", downloadFileName: "TM Report FileName", mobileDownload: { httpMethod: "GET", fileName: "TM Report FileName", fileExtension: "xxx" }})',
          theme: 'white',
          description: 'TM Report FileName',
        },
        {
          class: 'white',
          label: 'ARG',
          onClick: 'vm._$mdDialog.hide({ token: "TOKEN1", downloadFileName: "TM Report FileName2", mobileDownload: { httpMethod: "GET", fileName: "TM Report FileName2", fileExtension: "xxx" }})',
          theme: 'white',
          description: 'TM Report FileName2',
        },
      ];
      const isEqual = _.isEqual(showArgs.locals.buttons, buttons);
      expect(isEqual).toBeTruthy();

      await showPromise;
      expect(downloadService.downloadFile).toHaveBeenCalledWith(promise);
    });
  });

  it('downloadFile() should download the file using the passed in token', () => {
    spyOn(mockUtils, 'downloadFile');
    spyOn($rootScope, '$broadcast');

    const options = {
      token: 'ASDEFGHI123456',
      broadcastEvent: undefined,
      downloadFileName: 'test.pdf',
    };
    const downLoadFileName = 'test.pdf';
    downloadService.downloadFile(options);
    expect(mockUtils.downloadFile).toHaveBeenCalledWith(
      `${AppSettings.SPRING_API_URL}/${AppSettings.API_ENDPOINTS.DownloadFile}?token=${options.token}`, downLoadFileName);
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

    options.broadcastEvent = AppSettings.EVENTS.ASSETS_EXPORT_DONE;
    downloadService.downloadFile(options);
    expect(mockUtils.downloadFile).toHaveBeenCalledWith(
      `${AppSettings.SPRING_API_URL}/${AppSettings.API_ENDPOINTS.DownloadFile}?token=${options.token}`, downLoadFileName);
    expect($rootScope.$broadcast).toHaveBeenCalledWith(AppSettings.EVENTS.ASSETS_EXPORT_DONE);
  });

  it('downloadFile() should download the file using the passed in token and masquerade id is provided', () => {
    spyOn(mockUtils, 'downloadFile');
    spyOn($rootScope, '$broadcast');

    const options = {
      token: 'ASDEFGHI123456',
      broadcastEvent: undefined,
      downloadFileName: 'test.pdf',
    };
    const donwloadFileName = 'test.pdf';
    downloadService.downloadFile(options);
    expect(mockUtils.downloadFile).toHaveBeenCalledWith(
      `${AppSettings.SPRING_API_URL}/${AppSettings.API_ENDPOINTS.DownloadFile}?token=${options.token}`, donwloadFileName);

    mockUserService.masqueradeId = 'LONLBZ';

    downloadService.downloadFile(options);
    expect(mockUtils.downloadFile).toHaveBeenCalledWith(
      `${AppSettings.SPRING_API_URL}/${AppSettings.API_ENDPOINTS.DownloadFile}?token=${options.token}` +
      `&${AppSettings.MASQUERADE_HEADER_NAME}=${mockUserService.masqueradeId}`, donwloadFileName);
  });
});
