/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import AttachmentModel from 'app/common/models/attachment/';
import JobModel from 'app/common/models/job';
import JobService from 'app/services/job.service';
import mockJobAttachments from 'app/common/models/attachment/attachment.mocks.json';

describe('JobService', () => {
  let jobService;
  let mockPromise;
  let mockRestangular;

  const mockJobs = [{
    id: 1,
    leadSurveyor: {
      id: 1,
      name: 'Surveyor 1',
    },
    jobNumber: '4454',
    services: [
      { id: 1, name: 'Service Name 1', creditStatus: 'complete' },
      { id: 2, name: 'Service Name 2', creditStatus: 'part-held' },
    ],
  }, {
    id: 2,
    leadSurveyor: {
      id: 2,
      name: 'Surveyor 2',
    },
    jobNumber: '9961',
    services: [
      { id: 3, name: 'Service Name 3', creditStatus: 'part-held' },
      { id: 4, name: 'Service Name 4', creditStatus: 'complete' },
    ],
  }];

  const mockJobReports = [{
    id: 1,
    reportType: 'Type 1',
    reportVersion: 1,
  }, {
    id: 2,
    reportType: 'Type 2',
    reportVersion: 2,
  }];

  beforeEach(inject(() => {
    // Mock Service requests
    mockPromise = {
      customGET: () => {},
      get: () => {},
      getList: () => {},
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };

    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    jobService = new JobService(AppSettings, mockRestangular);
  }));

  describe('JobService.getJobs', () => {
    it.async('should call Restangular.one to get Asset jobs', async () => {
      spyOn(mockPromise, 'customGET').and.returnValue(mockJobs);
      const jobs = await jobService.getJobs(123);
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 123);
      expect(mockPromise.customGET)
        .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.JOBS.ROOT);
      expect(jobs).toEqual(mockJobs);
    });
  });

  describe('JobService.getJob', () => {
    it.async('should call Restangular.one to get a specific job for an asset', async () => {
      spyOn(mockPromise, 'get').and.returnValue(mockJobs[0]);
      const job = await jobService.getJob(1);
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.JOBS.ROOT, 1);
      expect(mockPromise.get).toHaveBeenCalled();
      expect(job.jobNumber).toEqual('4454');
      expect(job.services[0].name).toEqual(mockJobs[0].services[0].name);
    });
  });

  describe('JobService.getJobReports', () => {
    it.async('should call Restangular.one to get Asset jobs', async () => {
      spyOn(mockRestangular.one(), 'get').and.returnValue(mockJobReports);
      const jobReports = await jobService.getJobReports(123);

      expect(mockRestangular.one).toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.JOBS.ROOT}/123/${AppSettings.API_ENDPOINTS.JOBS.REPORTS}`);
      expect(mockRestangular.one().get).toHaveBeenCalled();
      expect(jobReports).toEqual(mockJobReports);
    });
  });

  describe('JobService.getJobAttachments', () => {
    it.async('should call Restangular.one to get Asset job attachments', async () => {
      spyOn(mockRestangular.one(), 'get').and.returnValue(mockJobAttachments);
      const jobAttachments = await jobService.getJobAttachments(123);
      const reflectedAttachments = [];
      _.forEach(mockJobAttachments.attachments, (attachment) => {
        reflectedAttachments.push(Reflect.construct(AttachmentModel, [attachment]));
      });

      expect(mockRestangular.one).toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.JOBS.ROOT}/123/${AppSettings.API_ENDPOINTS.JOBS.ATTACHMENTS}`);
      expect(mockRestangular.one().get).toHaveBeenCalled();
      expect(jobAttachments).toEqual(reflectedAttachments);
    });

    it.async('filters out attachments by category', async () => {
      spyOn(mockRestangular.one(), 'get').and.returnValue(Promise.resolve(mockJobAttachments));
      const jobAttachments =
        await jobService.getJobAttachments(123, AppSettings.ATTACHMENTS.EHS.Name);
      const reflectedAttachments = [];
      _.forEach(mockJobAttachments.attachments, (attachment) => {
        reflectedAttachments.push(Reflect.construct(AttachmentModel, [attachment]));
      });

      expect(mockRestangular.one).toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.JOBS.ROOT}/123/${AppSettings.API_ENDPOINTS.JOBS.ATTACHMENTS}`);
      expect(mockRestangular.one().get).toHaveBeenCalled();
      expect(jobAttachments).toEqual(reflectedAttachments.filter(
        a => a.category && a.category.indexOf('Executive Hull Summary') >= 0));
    });
  });
});
