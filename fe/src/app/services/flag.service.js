import * as _ from 'lodash';
import BaseService from './base.service';
import CountryFileModel from 'app/common/models/country-file/country-file';
import FlagModel from 'app/common/models/flag/flag';

export default class FlagService extends BaseService {
  /* @ngInject */
  constructor(
    Restangular,
    AppSettings,
  ) {
    super(arguments);
    this._userFlags = {};
    this._allFlags = {};
  }

  /**
   * This function is used to clear this service variables
   */
  clearCaches() {
    this._userFlags = {};
  }

  async getUserFlags() {
    if (_.isEmpty(this._userFlags)) {
      this._userFlags = await this._Restangular
        .all(this._AppSettings.API_ENDPOINTS.CurrentUserFlags)
        .getList();
      this._userFlags = _.map(_.sortBy(this._userFlags, 'name'),
        object => Reflect.construct(FlagModel, [object]));
    }

    return this._userFlags;
  }

  async getAll() {
    if (_.isEmpty(this._allFlags)) {
      this._allFlags = await this._Restangular
        .all(this._AppSettings.API_ENDPOINTS.Flags)
        .getList();

      this._allFlags = _.map(
        _.sortBy(this._allFlags, 'name'),
        object => Reflect.construct(FlagModel, [object])
      );
    }

    return this._allFlags;
  }

  async getFlagsMapByCode() {
    if (_.isEmpty(this._allFlagsMapByCode)) {
      this._allFlagsMapByCode = _.keyBy(await this.getAll(), object => object.flagCode);
    }

    return this._allFlagsMapByCode;
  }

  /**
   * I get the country file data for a particular flag.
   * @param  {flag} the selectedFlag returned by the typeahead
   */
  async getCountryFilesByFlag(flag) {
    if (!flag) return null; // Fallback if no flag being passed

    const apiEndpoint = this._AppSettings.API_ENDPOINTS.CountryFile;

    const countryFiles = await this._Restangular
      .all(apiEndpoint)
      .customGET('', {
        flagId: flag.id,
      }, {
        Fields: CountryFileModel.PAYLOAD_REDUCTION_FIELDS_LIST.join(','),
      });
    const versionInUse = (countryFiles.length) ?
      Reflect.construct(CountryFileModel, [countryFiles.shift()]) :
      null;

    if (countryFiles.length) {
      _.forEach(countryFiles, (obj, index) => {
        countryFiles[index] = Reflect.construct(CountryFileModel, [obj]);
      });
    }

    return { versionInUse, countryFiles };
  }

  async getFlagByFlagCode(flagCode) {
    const allFlags = await this.getAll();
    const resultFlags = [];

    _.forEach(allFlags, (flag) => {
      if (flag.flagCode === flagCode) {
        resultFlags.push(flag);
      }
    });

    return resultFlags;
  }
}
