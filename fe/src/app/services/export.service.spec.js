/* global afterAll afterEach beforeAll beforeEach describe expect inject it window */
/* eslint-disable angular/window-service, max-len, no-underscore-dangle */

import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import ExportService from 'app/services/export.service';
import UsersExportSummaryModel from 'app/common/models/user/users-export-summary';

describe('ExportService', () => {
  let exportService;
  let mockPromise;
  let mockRestangular;
  let mockDownloadService;
  let mockUtils;

  beforeEach(inject(() => {
    // Mock Service requests
    mockPromise = {
      customPOST: () => {},
      post: () => {},
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one: () => mockPromise,
      all: () => mockPromise,
    };

    mockDownloadService = {
      downloadFile: () => {},
    };

    mockUtils = {
      getFileExtension: () => 'xxx',
    };

    exportService = new ExportService(AppSettings, mockDownloadService, mockRestangular, mockUtils);
  }));

  describe('getUsersExportSummary', () => {
    let allReturn;
    beforeEach(() => {
      allReturn = {
        post: () => {},
      };

      spyOn(mockRestangular, 'all').and.returnValue(allReturn);
      spyOn(allReturn, 'post').and.returnValue(Promise.resolve({
        accountTypes: ['LR_ADMIN', 'EOR', 'SHIP_BUILDER'],
        lastLoginDate: { from: '2016-01-02T12:00:00+00:00', to: '2016-04-05T12:00:00+00:00' },
      }));
    });

    it.async('should call users/export-summary query with selectedEmails and no filters', async () => {
      const selectedEmails = { includedUsers: ['foo@bar.com'] };
      await exportService.getUsersExportSummary(selectedEmails);

      expect(mockRestangular.all).toHaveBeenCalledWith('users/export-summary');
      expect(allReturn.post).toHaveBeenCalledWith(selectedEmails);
    });

    it.async('should call users/export-summary query with selectedEmails and with filters', async () => {
      const selectedEmails = { includedUsers: ['foo@bar.com'] };
      const fakeFilters = { some: 'filters', filter1: ['test'] };
      const expectedPayload = { includedUsers: ['foo@bar.com'], some: 'filters', filter1: ['test'] };

      await exportService.getUsersExportSummary(selectedEmails, fakeFilters);
      selectedEmails.filters = fakeFilters;

      expect(mockRestangular.all).toHaveBeenCalledWith('users/export-summary');
      expect(allReturn.post).toHaveBeenCalledWith(expectedPayload);
    });

    it.async('should return instance of UsersExportSummary model', async () => {
      const selectedEmails = { includedUsers: ['foo@bar.com'] };
      const usersExportInfo = await exportService.getUsersExportSummary(selectedEmails);

      expect(usersExportInfo instanceof UsersExportSummaryModel).toBe(true);
      expect(usersExportInfo.accountTypes).toEqual(['LR_ADMIN', 'EOR', 'SHIP_BUILDER']);
      expect(usersExportInfo.lastLoginDate).toEqual({
        from: '2016-01-02T12:00:00+00:00',
        to: '2016-04-05T12:00:00+00:00',
      });
    });
  });

  describe('exportUsersCSV()', () => {
    it.async('should Restangular.all with export-users/exportCSV with no filters', async () => {
      const mobileDownload = {
        httpMethod: 'GET',
        fileName: 'Users-export',
        fileExtension: 'xxx',
      };
      const downloadFileName = 'test.csv';
      const selectedEmails = { includedUsers: ['foo@bar.com'] };
      spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
      spyOn(mockPromise, 'post').and.returnValue({
        response: 'ABCDEFG123456',
        fileName: 'test.csv',
      });
      spyOn(mockDownloadService, 'downloadFile');
      await exportService.exportUsersCSV(selectedEmails);
      expect(mockRestangular.all).toHaveBeenCalledWith('export/users');
      expect(mockPromise.post).toHaveBeenCalledWith(selectedEmails);
      expect(mockDownloadService.downloadFile).toHaveBeenCalledWith({ token: 'ABCDEFG123456', mobileDownload, downloadFileName });
    });

    it.async('should Restangular.all with export-users/exportCSV with filters', async () => {
      const mobileDownload = {
        httpMethod: 'GET',
        fileName: 'Users-export',
        fileExtension: 'xxx',
      };
      const selectedEmails = { includedUsers: ['foo@bar.com'] };
      const fakeFilters = { some: 'filters' };
      const expectedPayload = { includedUsers: ['foo@bar.com'], some: 'filters' };
      const downloadFileName = 'test.csv';
      spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
      spyOn(mockPromise, 'post').and.returnValue({
        response: 'ABCDEFG123456',
        fileName: 'test.csv',
      });
      spyOn(mockDownloadService, 'downloadFile');
      await exportService.exportUsersCSV(selectedEmails, fakeFilters);
      expect(mockRestangular.all).toHaveBeenCalledWith('export/users');
      expect(mockPromise.post).toHaveBeenCalledWith(expectedPayload);
      expect(mockDownloadService.downloadFile).toHaveBeenCalledWith({ token: 'ABCDEFG123456', mobileDownload, downloadFileName });
    });
  });

  describe('exportAssetsPDF()', () => {
    it.async('should Restangular.all with export/assets-async', async () => {
      const payload = { some: 'payload', fileType: 1 };
      const mobileDownload = {
        httpMethod: 'GET',
        fileName: 'Assets-export',
        fileExtension: 'xxx',
      };

      const successData = {
        statusCode: 200,
        downloadToken: 'ABCDEFG123456',
        fileName: 'test.pdf',
      };
      const downloadFileName = 'test.pdf';
      spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
      spyOn(mockPromise, 'post').and.returnValue({
        jobId: 'ABCDEFG123456',
      });
      spyOn(mockRestangular, 'one').and.returnValue({ get: () => {} });
      spyOn(mockRestangular.one(), 'get').and.returnValue(Promise.resolve(successData));

      spyOn(mockDownloadService, 'downloadFile');
      await exportService.exportAssetsPDF(payload);
      expect(mockRestangular.all).toHaveBeenCalledWith('export/assets-async');
      expect(mockPromise.post).toHaveBeenCalledWith(payload);
      expect(mockDownloadService.downloadFile).toHaveBeenCalledWith({
        broadcastEvent: AppSettings.EVENTS.ASSETS_EXPORT_DONE,
        token: 'ABCDEFG123456',
        mobileDownload,
        downloadFileName,
      });
    });
  });

  describe('export()', () => {
    it.async('should call the right endpoint based on the item to print, and get the export url',
      async () => {
        const payload = {
          itemType: 'MPMS',
          assetCode: 'LRV1',
        };
        const mobileDownload = {
          httpMethod: 'GET',
          fileName: 'MPMS-export',
          fileExtension: 'xxx',
        };
        const endpoints = _.clone(AppSettings.API_ENDPOINTS.EXPORT);
        const endpointToUse = `${endpoints.ROOT}/${endpoints[payload.itemType]}`;
        const downloadFileName = 'test.pdf';
        spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
        spyOn(mockPromise, 'post').and.returnValue({
          response: 'ABCDEFG123456',
          fileName: 'test.pdf',
        });
        spyOn(mockDownloadService, 'downloadFile');
        await exportService.export(payload);
        expect(mockRestangular.all).toHaveBeenCalledWith(endpointToUse);
        expect(mockPromise.post).toHaveBeenCalledWith({ assetCode: 'LRV1' });
        expect(mockDownloadService.downloadFile).toHaveBeenCalledWith({ token: 'ABCDEFG123456', mobileDownload, downloadFileName });
      });
  });
});
