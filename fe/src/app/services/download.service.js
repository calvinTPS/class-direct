/* eslint-disable no-param-reassign, angular/window-service*/

import * as _ from 'lodash';
import BaseService from './base.service';
import downloadTMDialogTemplate from './_partials/download-tm-report-modal.pug';
import GeneralModalController from 'app/common/dialogs/general-modal.controller';

export default class DownloadService extends BaseService {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $rootScope,
    AppSettings,
    Restangular,
    UserService,
    utils,
  ) {
    super(arguments);
  }

  /**
   * Download the certificate.
   *
   * @param options.certId The cert ID.
   * @param options.jobId The job ID.
   */
  async downloadCertificate(options) {
    if (!options || (options && (!options.certId || !options.jobId))) {
      throw new Error('Missing job ID or cert ID');
    }

    const mobileDownload = {
      httpMethod: 'GET',
      fileName: 'Certificate',
      fileExtension: 'pdf',
    };

    const response = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.CertificatesAttachment)
      .post(options);

    const token = _.get(response, 'response');
    const downloadFileName = _.get(response, 'fileName');
    if (token) {
      this.downloadFile({ token, mobileDownload, downloadFileName });
    }
  }

  /**
   * Download the report. Report could be an ESP report without report ID.
   *
   * @param options.reportID The report ID.
   * @param options.jobId The job ID.
   */
  async downloadReport(report) {
    if (!report) {
      throw new Error('Missing report object');
    } else if (!report.jobId) {
      throw new Error('Missing job ID');
    } else if ((report.isFARReport || report.isFSRReport) && !report.id) {
      throw new Error('Missing report ID');
    }

    // for the native app download
    const mobileDownload = {
      httpMethod: 'GET',
      fileName: report.name,
    };

    if (report.isTMReport || report.isMMSReport) {
      this.showReportModal(report);
      return;
    }

    const path = report.isESPReport ?
      this._AppSettings.API_ENDPOINTS.EXPORT.ESP_REPORT :
      this._AppSettings.API_ENDPOINTS.EXPORT.REPORTS;

    const payload = { jobId: report.jobId };
    if (report.id) {
      _.assign(payload, {
        reportId: report.id,
      });
    }

    const response = await this._Restangular
      .all(
        `${this._AppSettings.API_ENDPOINTS.EXPORT.ROOT}/${path}`,
    ).post(payload);

    const token = _.get(response, 'response');
    const downloadFileName = _.get(response, 'fileName');
    mobileDownload.fileExtension = this._utils.getFileExtension(downloadFileName);
    if (token) {
      this.downloadFile({ token, mobileDownload, downloadFileName });
    }
  }

  /**
   * Download file based on the options provided
   *
   * @param {Object} options
   * @param {string} options.token The token file to be downloaded
   * @param {string=} options.broadcastEvent The event to be broadcasted upon completion
   */
  downloadFile(options) {
    const downloadAPI = this._AppSettings.API_ENDPOINTS.DownloadFile;
    const xMasqId = this._AppSettings.MASQUERADE_HEADER_NAME;
    let downloadAPIURL = `${this._AppSettings.SPRING_API_URL}/${downloadAPI}?token=${options.token}`;
    const downloadFileName = options.downloadFileName;

    if (this._UserService.masqueradeId) {
      downloadAPIURL += `&${xMasqId}=${this._UserService.masqueradeId}`;
    }
    // native app
    if (window.classDirect) {
      options.mobileDownload.fileUrl = `${window.location.origin}${downloadAPIURL}`;
      // IOS
      if (_.get(window, 'webkit.messageHandlers.download', null)) {
        window.webkit.messageHandlers.download.postMessage(options.mobileDownload);
        // Android
      } else if (_.get(window, 'classDirectAndroid.downloadFile', null)) {
        const base64StringData = window.btoa(angular.toJson(options.mobileDownload));
        window.classDirectAndroid.downloadFile(base64StringData);
      }
    } else {
      this._utils.downloadFile(downloadAPIURL, downloadFileName);
    }

    if (options.broadcastEvent) {
      this._$rootScope.$broadcast(options.broadcastEvent);
    }
  }

  /**
   * Show TM Report Modal.
   * @param {report} item A TM Report object, with attachments.
   */
  showReportModal(item) {
    const downloadButtons = [];
    let isArgonautFilePresent = false;
    _.forEach(item.attachments, (attachment) => {
      if (attachment.isArgonautFile) {
        isArgonautFilePresent = true;
      }
      const attachmentVar = _.cloneDeep(this._AppSettings.ATTACHMENTS);
      const icon = attachment.reportType === attachmentVar.TM.ArgonautCategory ?
        DownloadService.ICON_TYPE.ARG : DownloadService.ICON_TYPE.PDF;
      const fileExtension = this._utils.getFileExtension(attachment.title);
      downloadButtons.push({
        class: 'white',
        label: icon,
        onClick: `vm._$mdDialog.hide({ token: "${attachment.token}", downloadFileName: "${attachment.title}", mobileDownload: { httpMethod: "GET", fileName: "${attachment.title}", fileExtension: "${fileExtension}" }})`,
        theme: 'white',
        description: attachment.title,
      });
    });
    const dialogLocals = {
      isArgonautFilePresent,
      title: `cd-${item.shortName.toLowerCase()}-report`,
      body: item.isMMSReport ? '' : `cd-${item.shortName.toLowerCase()}-summary-report`,
      buttons: _.orderBy(downloadButtons, button => button.label, ['desc']),
    };

    this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: true,
      controller: GeneralModalController,
      controllerAs: 'vm',
      fullscreen: true,
      locals: dialogLocals,
      parent: angular.element(this._$document[0].body),
      template: downloadTMDialogTemplate,
    }).then((promiseObj) => {
      this.downloadFile(promiseObj);
    }, () => {
      // When click close or cancel modal box, a promise rejection will
      // be used. It should be handled here. But since there is nothing
      // to do here, we will leave it as empty function just to  remove
      // error in console: 'Possible unhandled rejection'
    });
  }

  static ICON_TYPE = {
    ARG: 'ARG',
    PDF: 'PDF',
  };
}
