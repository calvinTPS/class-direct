import BaseService from './base.service';

export default class SurveyService extends BaseService {
  /* @ngInject */
  constructor(
    Restangular,
    AppSettings,
  ) {
    super(arguments);
    this._apiPath = AppSettings.API_ENDPOINTS.SurveyRequests;
  }

  createSurvey(assetId, payLoad) {
    return this._Restangular.all(this._apiPath).customPOST(payLoad);
  }
}
