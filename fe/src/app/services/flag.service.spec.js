/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import CountryFileModel from 'app/common/models/country-file/country-file';
import FlagModel from 'app/common/models/flag/flag';
import FlagService from 'src/app/services/flag.service';

describe('FlagService', () => {
  let mockCountryFile,
    mockFlags,
    mockPromise,
    mockRestangular,
    flagService;

  beforeEach(inject((_$rootScope_) => {
    mockFlags = [
      {
        id: 1,
        name: 'Timbuktu',
        flagCode: 'TIM',
      },
      {
        id: 2,
        name: 'Malaysia',
        flagCode: 'MYS',
      },
    ];


    mockCountryFile = [
      {
        id: 1,
        flag: 'Malaysia',
        token: 'eyJ0eXBlIjoiUzMiLCJzM1Rva2VuIjp7InJlZ2lvbiI6IkVVX1dFU1RfMSIsImJ1Y2tldE5hbWUiOiJsci1jbGFzc2RpcmVjdCIsImZpbGVLZXkiOiJjb3VudHJ5LWZpbGVzL01ZX3ZlcnNpb25fMS4wLnBkZiJ9fQ==',
        lastModifiedDate: '2006-05-23T23:00:00Z',
        creationDateWithTime: '2006-05-23T23:00:00Z',
        updatedDateWithTime: '2006-05-23T23:00:00Z',
      },
      {
        id: 2,
        flag: 'London',
        token: 'eyJ0eXBlIjoiUzMiLCJzM1Rva2VuIjp7InJlZ2lvbiI6IkVVX1dFU1RfMSIsImJ1Y2tldE5hbWUiOiJsci1jbGFzc2RpcmVjdCIsImZpbGVLZXkiOiJjb3VudHJ5LWZpbGVzL01ZX3ZlcnNpb25fMS4wLnBkZiJ9fQ==',
        lastModifiedDate: '2006-05-23T23:00:00Z',
        creationDateWithTime: '2006-05-23T23:00:00Z',
        updatedDateWithTime: '2006-05-23T23:00:00Z',
      },
    ];

    mockPromise = {
      get() {
        return this;
      },
      getList() {
        return this;
      },
      customPOST() {
        return this;
      },
      customGET() {
        return this;
      },
      customGETLIST() {
        return this;
      },
      one() {
        return this;
      },
    };
    spyOn(mockPromise, 'get').and.callFake(() => mockFlags[0]);
    spyOn(mockPromise, 'getList').and.returnValue(mockFlags);
    spyOn(mockPromise, 'customPOST').and.returnValue(mockFlags[0]);
    spyOn(mockPromise, 'customGET').and.returnValue(mockCountryFile);
    spyOn(mockPromise, 'customGETLIST').and.returnValue(mockFlags[0]);

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };
    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    flagService = new FlagService(mockRestangular, AppSettings);
  }));

  describe('Flag service', () => {
    describe('getAll', () => {
      it.async('gets a list of all flags using Restangular', async() => {
        const flags = await flagService.getUserFlags();
        expect(mockRestangular.one).not.toHaveBeenCalled();
        expect(AppSettings.API_ENDPOINTS.CurrentUserFlags).not.toBeUndefined();
        expect(mockRestangular.all).toHaveBeenCalledWith(
          AppSettings.API_ENDPOINTS.CurrentUserFlags
        );
        expect(mockPromise.getList).toHaveBeenCalledWith();
        expect(flags instanceof Array).toBe(true);
      });

      xit.async('caches the flag data from restangular, returns a copy of it', async() => {
        const flags = await flagService.getUserFlags();

        // Write on it
        flags[0] = new FlagModel({ id: 999, name: 'GOTCHA ' });

        mockRestangular.all.calls.reset();
        mockPromise.getList.calls.reset();

        const cachedFlags = await flagService.getUserFlags();

        // Fresh copy should be returned
        expect(cachedFlags[0].model).not.toEqual(flags[0].model);

        expect(mockRestangular.all).not.toHaveBeenCalled();
        expect(mockPromise.getList).not.toHaveBeenCalledWith();

        expect(cachedFlags instanceof Array).toBe(true);
      });

      it.async('sorts the flags by ASC name', async () => {
        const flags = await flagService.getUserFlags();

        expect(flags[0].name).toBe('Malaysia');
        expect(flags[1].name).toBe('Timbuktu');
      });
    });

    it.async('getFlagsMapByCode() should return a flag map by flag code', async () => {
      const flagMapByCode = await flagService.getFlagsMapByCode();

      expect(flagMapByCode.MYS.name).toBe('Malaysia');
      expect(flagMapByCode.TIM.name).toBe('Timbuktu');
    });

    it.async('getCountryFilesByFlag() should return the country file data for a particular flag', async () => {
      const countryFile = await flagService.getCountryFilesByFlag({
        id: 1,
      });
      expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.CountryFile);
      expect(mockPromise.customGET).toHaveBeenCalledWith('', {
        flagId: 1,
      }, {
        Fields: CountryFileModel.PAYLOAD_REDUCTION_FIELDS_LIST.join(','),
      });
    });

    it.async('clearCaches() should clear all the cached variables', async () => {
      await flagService.getUserFlags();

      expect(_.isEmpty(flagService._userFlags)).toBe(false);
      flagService.clearCaches();
      expect(_.isEmpty(flagService._userFlags)).toBe(true);
    });

    it.async('getFlagByFlagCode() should get flag object which has the right flag code', async () => {
      const mockFlagsObject = [{
        flagCode: 'ABC',
      }, {
        flagCode: 'DEF',
      }, {
        flagCode: 'GHI',
      }];
      spyOn(flagService, 'getAll').and.returnValue(mockFlagsObject);

      const response = await flagService.getFlagByFlagCode('ABC');
      expect(flagService.getAll).toHaveBeenCalled();
      expect(response).toEqual([{
        flagCode: 'ABC',
      }]);
    });
  });
});
