/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import AssetService from 'app/services/asset.service.js';
import PermissionsService from 'app/services/permissions.service.js';

describe('Asset Service', () => {
  let makeService;
  let mockRestangular;
  let mockPromise;
  let mockPermissionsService;
  let mockAssetObject;
  let defaultParams;

  beforeEach(inject((_$rootScope_) => {
    defaultParams = _.assign(
      {},
      AppSettings.DEFAULT_PARAMS.PAGINATION,
      AppSettings.DEFAULT_PARAMS.FLEET_SORT
    );
    mockAssetObject = {
      $object: {},
    };
    mockPromise = {
      get() {
        return this;
      },
      post() {
        return this;
      },
      getList() {
        return this;
      },
      customGET() {
        return this;
      },
      customPUT() {
        return this;
      },
      customPOST() {
        return this;
      },
      customDELETE() {
        return this;
      },
      customGETLIST() {
        return this;
      },
      one() {
        return this;
      },
      all() {
        return this;
      },
    };
    spyOn(mockPromise, 'all').and.returnValue(mockPromise);
    spyOn(mockPromise, 'get').and.callFake(() => mockAssetObject);
    spyOn(mockPromise, 'getList').and.returnValue(mockAssetObject);
    spyOn(mockPromise, 'customGET').and.returnValue(mockAssetObject);
    spyOn(mockPromise, 'customPOST').and.returnValue(mockAssetObject);
    spyOn(mockPromise, 'customDELETE').and.returnValue(mockAssetObject);
    spyOn(mockPromise, 'customGETLIST').and.returnValue(mockAssetObject);
    spyOn(mockPromise, 'customPUT').and.returnValue(mockAssetObject);
    spyOn(mockPromise, 'post').and.returnValue(mockAssetObject);

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };

    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    makeService = () => new AssetService(AppSettings, mockRestangular);
  }));

  describe('AssetService', () => {
    it('getAssetCurrentUser() should retrun the correct value', () => {
      const assetService = makeService();
      expect(assetService.getAssetCurrentUser()).toEqual(null);
      expect(assetService.getAssetCurrentUser('asset-card')).toEqual(undefined);
      expect(assetService.getAssetCurrentUser('xxxx')).toEqual(null);

      const userObj = { foo: 'Foo', id: 'BAR' };
      assetService.setAssetCurrentUser(userObj);
      expect(assetService.getAssetCurrentUser('asset-card')).toEqual(userObj);
    });

    it('calls Restangular.one to get single asset', () => {
      const assetService = makeService();

      assetService.getOne('LRV123');
      expect(mockRestangular.all).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.Assets).not.toBeUndefined();
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 'LRV123');
      expect(mockPromise.get).toHaveBeenCalledWith();

      mockRestangular.all.calls.reset();
      mockRestangular.one.calls.reset();
      mockPromise.get.calls.reset();

      assetService.getOne('LRV456');
      expect(mockRestangular.all).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.Assets).not.toBeUndefined();
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 'LRV456');
      expect(mockPromise.get).toHaveBeenCalledWith();
    });

    describe('getDetails', () => {
      it('calls the getDetails method with the right api endpoint', () => {
        const assetService = makeService();
        assetService.getDetails(1);
        expect(mockRestangular.one).toHaveBeenCalled();
        expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 1);
      });
    });

    describe('getTasks', () => {
      it('getTasks(), calls the getTasks method with the right api endpoint', () => {
        const assetService = makeService();
        assetService.getTasks(1);
        const path = `${AppSettings.API_ENDPOINTS.Assets}/1/${AppSettings.API_ENDPOINTS.ASSET.Tasks}`;
        expect(mockRestangular.one).toHaveBeenCalledWith(path);
        expect(mockPromise.get).toHaveBeenCalled();
      });
    });

    describe('updateTasks', () => {
      it('updateTasks(), update the method with right api endpoint', () => {
        const assetService = makeService();
        assetService.updateTasks([]); // Putting empty array as a payload
        const path = `${AppSettings.API_ENDPOINTS.ASSET.Tasks}`;
        expect(mockRestangular.one).toHaveBeenCalledWith(path);
        expect(mockPromise.customPUT).toHaveBeenCalledWith([]);
      });
    });

    describe('getSurveyReports', () => {
      it.async('getSurveyReports(), calls the getSurveyReports method with the right api endpoint', async () => {
        const assetService = makeService();

        await assetService.getSurveyReports('LRV1');
        const path = `${AppSettings.API_ENDPOINTS.Assets}/LRV1/${AppSettings.API_ENDPOINTS.SurveyReports}`;
        expect(mockRestangular.one).toHaveBeenCalledWith(path);
        expect(mockPromise.get).toHaveBeenCalled();
      });
    });

    describe('getSisterAssetsCount', () => {
      it.async(`getSisterAssetsCount(), calls the getSisterAssetsCount method with
        the right api endpoint and param values`, async () => {
        const assetService = makeService();

        const options = {
          assetId: 'LRV1',
          leadImo: '12345678',
          type: 'technical-sisters',
        };

        await assetService.getSisterAssetsCount(options);
        const path = `${AppSettings.API_ENDPOINTS.Assets}/LRV1/technical-sisters/count`;
        expect(mockRestangular.one).toHaveBeenCalledWith(path);
        expect(mockPromise.customGET).toHaveBeenCalledWith('', {
          leadImo: options.leadImo,
        });

        const options1 = {
          assetId: 'LRV1',
          leadShip: '12345678',
          type: 'register-book-sisters',
        };

        await assetService.getSisterAssetsCount(options1);
        const path1 = `${AppSettings.API_ENDPOINTS.Assets}/LRV1/register-book-sisters/count`;
        expect(mockRestangular.one).toHaveBeenCalledWith(path1);
        expect(mockPromise.customGET).toHaveBeenCalledWith('', {
          leadShip: options1.leadShip,
        });
      });
    });

    describe('getAll', () => {
      it('calls Restangular.all to get list of assets', () => {
        const assetService = makeService();

        assetService.getAll();
        expect(mockRestangular.one).not.toHaveBeenCalled();
        expect(AppSettings.API_ENDPOINTS.Assets).not.toBeUndefined();
        expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets);
        expect(mockPromise.customGET).toHaveBeenCalledWith('', AppSettings.DEFAULT_PARAMS.PAGINATION);
      });
    });

    describe('query()', () => {
      it('calls Restangular.all with Promise.customPOST, queries and default params', () => {
        const assetService = makeService();
        const fields = assetService.getAssetListPayloadFields();
        const query = { search: 'ABC', hasActiveServices: true };
        assetService.query(query);

        expect(mockRestangular.one).not.toHaveBeenCalled();
        expect(AppSettings.API_ENDPOINTS.Assets).not.toBeUndefined();
        expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets);
        expect(mockPromise.customPOST).toHaveBeenCalledWith(query, 'query', defaultParams, fields);
      });

      it('calls Restangular.all with Promise.customPOST, queries and params is passed in', () => {
        const assetService = makeService();
        const fields = assetService.getAssetListPayloadFields();
        const query = { search: 'ABC', hasActiveServices: true };
        const params = { sort: 'name', order: 'DESC' };
        assetService.query(query, params);

        expect(mockRestangular.one).not.toHaveBeenCalled();
        expect(AppSettings.API_ENDPOINTS.Assets).not.toBeUndefined();
        expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets);
        expect(mockPromise.customPOST).toHaveBeenCalledWith(query, 'query',
          { page: 1, size: 16, sort: 'name', order: 'DESC' }, fields);
      });

      it('modifies buildDateMin and buildDateMax params before sending out to server', () => {
        const assetService = makeService();
        const fields = assetService.getAssetListPayloadFields();
        const query = {
          buildDateMin: new Date('2012-09-09'),
          buildDateMax: new Date('2013-01-01'),
        };
        assetService.query(query);

        expect(mockPromise.customPOST).toHaveBeenCalledWith(
          {
            buildDateMin: '2012-09-09',
            buildDateMax: '2013-01-01',
          },
          'query',
          defaultParams, fields);
      });

      it.async('causes isQuerying to be set to true when querying', async () => {
        const assetService = makeService();
        expect(assetService.isQuerying).toBe(false);

        const query = { search: 'ABC' };
        const queryPromise = assetService.query(query);
        expect(assetService.isQuerying).toBe(true);

        await queryPromise;
        expect(assetService.isQuerying).toBe(false);
      });
    });

    it('calls AssetService.get to get list of codicils for the asset', () => {
      const assetService = makeService();
      assetService.getOne(1, 'codicils', { size: 4 });
      expect(mockPromise.get).toHaveBeenCalledWith();
    });

    it('calls AssetService.setFavourite to update asset as favourite', () => {
      const assetService = makeService();
      assetService.setFavourite('LRV123');
      expect(mockPromise.customPOST).toHaveBeenCalledWith('', 'favourite');
    });

    it('calls AssetService.removeFavourite to update asset as not favourite', () => {
      const assetService = makeService();
      assetService.removeFavourite('LRV123');
      expect(mockPromise.customDELETE).toHaveBeenCalledWith('favourite');
    });

    it('calls AssetService.updateFavourites to batch update assets as favourite/not favourite', () => {
      const assetService = makeService();
      const payload = {
        add: 'LRV1, LRV2 LRV3',
        remove: 'LRV10, LRV20 LRV30',
      };
      assetService.updateFavourites(payload);
      expect(mockPromise.post).toHaveBeenCalledWith(payload);
    });

    describe('getByDueDate', () => {
      it('calls AssetService.getByDueDate to get filtered items', () => {
        const assetService = makeService();
        const query = {
          dueDateMin: new Date('2012-09-09'),
          dueDateMax: new Date('2013-01-01'),
        };
        assetService.getByDueDate('LRV123', assetService.constructor.ASSET_ITEMS.Codicils, ['dueDateMin', 'dueDateMax'], query);
        assetService.getByDueDate('LRV123', assetService.constructor.ASSET_ITEMS.Services, ['dueDateMin', 'dueDateMax'], query);

        expect(mockPromise.all).toHaveBeenCalledWith('codicils/query');
        expect(mockPromise.all).toHaveBeenCalledWith('services/query-detailed');
        expect(mockPromise.customPOST).toHaveBeenCalledWith(
          {
            dueDateMin: '2012-09-09',
            dueDateMax: '2013-01-01',
          });
      });

      it.async('non-LR asset gives null', async () => {
        const assetService = makeService();
        const result = await assetService.getByDueDate('IHS123465');
        expect(result).toEqual(null);
      });

      it.async('filters items based on certain configurable criteria', async () => {
        const assetService = makeService();
        const query = {
          dueDateMin: new Date('2012-09-09'),
          dueDateMax: new Date('2013-01-01'),
        };

        // clear spy
        mockPromise.customPOST = () => { };
        spyOn(mockPromise, 'customPOST').and.returnValue(Promise.resolve({
          cocHDtos: [
            { status: { id: 14 } },
          ],
          actionableItemHDtos: [
            { status: { id: 4 } },
          ],
          statutoryFindingHDtos: [],
        }));

        const result = await assetService.getByDueDate('LRV123465', assetService.constructor.ASSET_ITEMS.Codicils, ['dueDateMin', 'dueDateMax'], query);
        expect(result.map(r => r.model)).toEqual([{ status: { id: 14 } }, { status: { id: 4 } }]);
      });
    });

    describe('getServiceScheduleRange', () => {
      it('calls AssetService.getServiceScheduleRange to get min max date', () => {
        const assetService = makeService();
        assetService.getServiceScheduleRange('LRV123');
        expect(mockPromise.customGET).toHaveBeenCalledWith('schedule-range');
      });

      it.async('non-LR asset gives null', async () => {
        const assetService = makeService();
        const result = await assetService.getServiceScheduleRange('IHS123465');

        expect(result).toEqual(null);
      });
    });

    describe('getEORAssetByAssetId', () => {
      it('calls AssetService.getEORAssetByAssetId to get eor asset', () => {
        const assetService = makeService();
        assetService.getEORAssetByAssetId('LRV123');
        expect(mockPromise.customGET).toHaveBeenCalledWith('eor');
      });
    });

    describe('getEORDetails', () => {
      it('calls AssetService.getEORDetails to get eor asset details', () => {
        const assetService = makeService();
        assetService.getEORDetails('LRV123');
        expect(mockPromise.customGET).toHaveBeenCalledWith('eor');
      });
    });

    describe('Favourites management', () => {
      it.async('keeps track of number of favourite assets that user has', async () => {
        const assetService = makeService();
        expect(assetService.userHasFavourites).toBe(false);
        assetService.setFavourite('LRV123');
        expect(assetService.userHasFavourites).toBe(true);
        assetService.removeFavourite('LRV123');
        expect(assetService.userHasFavourites).toBe(false);

        // clear spy
        mockPromise.customPOST = () => { };

        spyOn(mockPromise, 'customPOST').and.returnValue([{ isFavourite: true }]); // eslint-disable-line jasmine/no-unsafe-spy
        await assetService.query({ isFavourite: true });
        expect(assetService.userHasFavourites).toBe(true);

        // clear spy
        mockPromise.customPOST = () => { };

        spyOn(mockPromise, 'customPOST').and.returnValue([{ isFavourite: false }]); // eslint-disable-line jasmine/no-unsafe-spy
        await assetService.query({ isFavourite: true });
        expect(assetService.userHasFavourites).toBe(false);
      });
    });
  });
});
