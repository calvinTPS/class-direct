/* global afterAll afterEach beforeAll beforeEach describe expect inject it */

import AppSettings from 'app/config/project-variables.js';
import AssetType from 'app/common/models/asset/asset-type/asset-type';
import ReferenceDataService from 'app/services/reference-data.service.js';

describe('Reference Data Service', () => {
  let mockPromise;
  let mockRestangular;
  let referenceDataService;

  beforeEach(inject(() => {
    // Mock Service requests
    mockPromise = {
      getList: () => {},
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };

    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    referenceDataService = new ReferenceDataService(mockRestangular, AppSettings);
  }));

  describe('ReferenceDataService', () => {
    it('calls Restangular.all to get Asset Class Statuses', () => {
      referenceDataService.getAssetClassStatuses();

      expect(mockRestangular.one).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.AssetClassStatuses).not.toBeUndefined();
      expect(mockRestangular.all)
        .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.AssetClassStatuses);
    });

    it('calls Restangular.all to get Asset Types', () => {
      referenceDataService.getAssetTypes();

      expect(mockRestangular.one).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.AssetTypes).not.toBeUndefined();
      expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.AssetTypes);
    });

    describe('getDisplayedAssetTypes', () => {
      it.async('returns level 3 asset types', async () => {
        const assetTypes = [
          new AssetType({
            id: 1,
            name: 'Parent',
            parentId: null,
          }),
          new AssetType({
            id: 2,
            name: 'Child',
            levelIndication: 3,
            parentId: 1,
          }),
        ];
        spyOn(referenceDataService, 'getAssetTypes').and.returnValue(assetTypes);

        const displayedAssetTypes = await referenceDataService.getAssetTypesAtLevel(3);
        expect(displayedAssetTypes.length).toEqual(1);
        expect(displayedAssetTypes[0].name).toEqual('Child');
      });
    });
  });
});
