/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
 one-var-declaration-per-line, sort-vars, angular/window-service */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import ChecklistItemModel from 'app/common/models/checklist-item';
import MockChecklist from 'app/services/service.service.checklist.mock.json';
import ServiceService from 'app/services/service.service';
import TaskModel from 'app/common/models/task';

describe('Service service', () => {
  let serviceService;
  let mockPromise;
  let mockRestangular;

  const servicesPayload = {
    services: [{
      id: 1,
      updatedBy: null,
      updatedDate: null,
      dueDate: '2016-11-05T00:00:00Z',
      dueDateEpoch: 1478304000000,
      dueDateManual: null,
      dueStatus: 1,
      dueStatusH: {
        id: 1,
      },
      model: {},
      postponementDate: '2016-12-05T00:00:00Z',
      postponementDateEpoch: 1480896000000,
      lowerRangeDate: '2016-09-02T00:00:00Z',
      lowerRangeDateEpoch: 1472774400000,
      upperRangeDate: '2016-11-08T00:00:00Z',
      upperRangeDateEpoch: 1478563200000,
      code: 'LRV1',
      repeatOf: 1,
      serviceCatalogueH: {
        name: 'Service 1',
        productCatalogue: {
          name: 'Product 1',
        },
      },
      serviceCatalogueName: 'Service 1',
      serviceCreditStatusH: {
        name: 'credit status 1',
      },
      serviceStatusH: {
        name: 'service status 1',
      },
    },
    {
      id: 2,
      updatedBy: null,
      updatedDate: null,
      dueDate: '2016-11-05T00:00:00Z',
      dueDateEpoch: 1478304000000,
      dueDateManual: null,
      dueStatus: 2,
      dueStatusH: {
        id: 2,
      },
      model: {},
      postponementDate: '2016-12-05T00:00:00Z',
      postponementDateEpoch: 1480896000000,
      lowerRangeDate: '2016-11-02T00:00:00Z',
      lowerRangeDateEpoch: 1478044800000,
      upperRangeDate: '2017-06-08T00:00:00Z',
      upperRangeDateEpoch: 1496880000000,
      code: 'LRV2',
      repeatOf: 0,
      serviceCatalogueH: {
        name: 'Service 2',
        productCatalogue: {
          name: 'Product 2',
        },
      },
      serviceCatalogueName: 'Service 1',
      serviceCreditStatusH: {
        name: 'credit status 2',
      },
      serviceStatusH: {
        name: 'service status 2',
      },
    }],
    repeatedServices: [{
      repeatOf: 1,
      dueDate: '2017-04-01',
      dueStatus: 99,
      dueStatusH: {
        id: 99,
      },
      lowerRangeDate: '2017-01-01',
      upperRangeDate: '2017-10-01',
      postponementDate: '2017-06-30',
    },
    {
      repeatOf: 2,
      dueDate: '2018-04-01',
      lowerRangeDate: '2018-01-01',
      dueStatus: 99,
      dueStatusH: {
        id: 99,
      },
      model: {},
      upperRangeDate: '2018-10-01',
      postponementDate: '2018-06-30',
    }],
  };

  const mockServices =
    Object.assign({}, servicesPayload);
  const mockServicesRefactored = Object.assign({},
    _.cloneDeep(mockServices)
      .services
      .concat(_.cloneDeep(mockServices)
      .repeatedServices));

  const repeatedItems = [2, 3];

  const mockServiceLowerRangeDates = [
    '2016-09-02T00:00:00Z',
    '2016-11-02T00:00:00Z',
    '2017-01-01',
    '2018-01-01',
  ];

  const mockServiceUpperRangeDates = [
    '2016-11-08T00:00:00Z',
    '2017-06-08T00:00:00Z',
    '2017-10-01',
    '2018-10-01',
  ];

  const mockTasklist = [{
    name: 'task 1',
    taskNumber: 'x0001',
    dueDate: '2017-01-01',
    asssignedDate: '2017-01-01',
    creditStatus: 'Complete',
  }, {
    name: 'task 2',
    taskNumber: 'x0002',
    dueDate: '2017-01-02',
    asssignedDate: '2017-01-02',
    creditStatus: 'Extended',
  }];

  // eslint-disable-next-line no-undef
  beforeEach(inject(() => {
    // Mock Service requests
    mockPromise = {
      get: () => {},
      customGET: () => {},
      customPOST: () => {},
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };
    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    serviceService = new ServiceService(AppSettings, mockRestangular);
  }));

  describe('ServiceService.getAll()', () => {
    it.async('should call the correct API to retrieve a list of services', async () => {
      spyOn(mockPromise, 'customGET').and.returnValue(mockServicesRefactored);
      const services = await serviceService.getAll(123);
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 123);
      expect(services).toEqual(mockServicesRefactored);
    });
  });

  describe('ServiceService.getOne()', () => {
    it.async('should call the correct API to retrieve a single of service', async () => {
      spyOn(mockPromise, 'customGET').and.returnValue(mockServicesRefactored);
      const service = await serviceService.getOne(123, 1);
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 123);
      expect(service.id).toEqual(1);
      expect(service.catalogueName).toEqual('Service 1');
      expect(service.creditStatus).toEqual('credit status 1');
      expect(service.productCatalogueName).toEqual('Product 1');
      expect(service.statusName).toEqual('service status 1');
    });
  });

  describe('ServiceService.getChecklist()', () => {
    it.async('should call the correct API to get the checklist for a service', async () => {
      spyOn(mockPromise, 'customPOST').and.returnValue(MockChecklist);
      spyOn(serviceService, '_assignModelToHierarchyChecklist');

      const checklist = await serviceService.getChecklist(1, 101);
      const assetServiceUrl = `${AppSettings.API_ENDPOINTS.Assets}/1/${AppSettings.API_ENDPOINTS.ASSET.Services}/101`;
      expect(mockRestangular.one).toHaveBeenCalledWith(assetServiceUrl);
      expect(serviceService._assignModelToHierarchyChecklist).toHaveBeenCalled();
    });
  });

  describe('ServiceService.getTasklist()', () => {
    it.async('should call the correct API to get the tasklist for a service', async () => {
      const mockTasklistItem = {
        servicesH: [
          { itemsH: [{ id: 1 }, { id: 2 }] },
        ],
      };
      spyOn(mockPromise, 'customPOST').and.returnValue(mockTasklistItem);
      spyOn(serviceService, '_assignModelToHierarchyTasks');
      const tasklist = await serviceService.getTasklist(1, 101);
      const assetServiceUrl = `${AppSettings.API_ENDPOINTS.Assets}/1/${AppSettings.API_ENDPOINTS.ASSET.Services}/101`;
      expect(mockRestangular.one).toHaveBeenCalledWith(assetServiceUrl);
      expect(serviceService._assignModelToHierarchyTasks).toHaveBeenCalled();
    });
  });

  describe('getPMSableTasks', () => {
    it.async('getPMSableTasks(), calls the getPMSableTasks method with the right api endpoint', async () => {
      const mockTasks = {
        servicesH: [
          { itemsH: [{ id: 1 }, { id: 2 }] },
          { itemsH: [{ id: 3 }, { id: 4 }] },
          { itemsH: [{ id: 6 }, { id: 6 }] },
        ],
      };

      spyOn(mockPromise, 'get').and.returnValue(mockTasks);
      spyOn(serviceService, '_assignModelToHierarchyTasks');

      await serviceService.getPMSableTasks(1);
      const path = `${AppSettings.API_ENDPOINTS.Assets}/1/${AppSettings.API_ENDPOINTS.ASSET.Tasks}?pmsAble=true`;
      expect(mockRestangular.one).toHaveBeenCalledWith(path);
      expect(mockPromise.get).toHaveBeenCalled();
      expect(serviceService._assignModelToHierarchyTasks).toHaveBeenCalled();
    });
  });

  describe('ServiceService._assignModelToHierarchyTasks()', () => {
    it('recursively modelizes the tasks', () => {
      const tasks = [
        {
          itemsH: [
            {
              name: 'Vessel',
              tasksH: [{ id: 10 }, { id: 20 }, { id: 30 }],
              itemsH: [
                {
                  name: 'Vessel 2',
                  tasksH: [{ id: 40 }, { id: 50 }],
                  itemsH: [
                    {
                      name: 'Vessel 3',
                      tasksH: [{ id: 60 }, { id: 70 }, { id: 80 }],
                    },
                    {
                      name: 'Vessel 3',
                      tasksH: [{ id: 90 }, { id: 100 }],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ];

      const modelizedTasks = serviceService._assignModelToHierarchyTasks(tasks);
      expect(modelizedTasks[0].itemsH[0].tasksH[0])
        .toEqual(Reflect.construct(TaskModel, [{ id: 10 }]));
    });
  });

  describe('ServiceService._assignModelToHierarchyChecklist()', () => {
    it('recursively modelizes the checklist', () => {
      const checklistGroups = _.cloneDeep(MockChecklist).checklistGroups;
      const items = _.cloneDeep(checklistGroups[0].checklistSubgrouplist[0].checklistItems);
      const modelizedChecklist = serviceService._assignModelToHierarchyChecklist(checklistGroups);

      expect(modelizedChecklist[0].checklistSubgrouplist[0].checklistItems.length).toEqual(3);
      expect(modelizedChecklist[0].checklistSubgrouplist[0].checklistItems[0])
        .toEqual(Reflect.construct(ChecklistItemModel, [items[0]]));
      expect(modelizedChecklist[0].checklistSubgrouplist[0].checklistItems[1])
        .toEqual(Reflect.construct(ChecklistItemModel, [items[1]]));
    });
  });

  describe('ServiceService.restructureServiceArray', () => {
    it('should refactor the payload to an array of services', () => {
      const servicesRefactored = serviceService.restructureServiceArray(mockServices);
      expect(servicesRefactored.length).toEqual(
        mockServices.services.length + mockServices.repeatedServices.length
      );
      expect(_.first(servicesRefactored)).toEqual(_.first(mockServices.services));
      expect(_.last(servicesRefactored)).toEqual(_.last(mockServices.repeatedServices));

      expect(servicesRefactored[2].dueDateEpoch).toEqual(1491004800000); // "2017-04-01"
      expect(servicesRefactored[2].lowerRangeDateEpoch).toEqual(1483228800000); // "2017-01-01"
      expect(servicesRefactored[2].upperRangeDateEpoch).toEqual(1506816000000); // "2017-10-01"
      expect(servicesRefactored[2].postponementDateEpoch).toEqual(1498780800000); // "2017-10-01"
    });
  });

  describe('ServiceService.hydrateRepeatedServices()', () => {
    it('should copy over ID based on repeatOf', () => {
      const servicesHydrated =
        serviceService.hydrateRepeatedServices(mockServices);
      _.forEach(repeatedItems, (serviceID, index) => {
        const originalService = _.find(mockServicesRefactored, { id: _.toSafeInteger(serviceID) });
        const hydratedService = _.find(servicesHydrated, { repeatOf: _.toSafeInteger(serviceID) });
        if (hydratedService) {
          expect(hydratedService.repeatOf).toEqual(originalService.id);
        }
      });
    });

    it('should copy over all info from original service', () => {
      const servicesHydrated = serviceService.hydrateRepeatedServices(mockServices);

      _.forEach(repeatedItems, (serviceID, index) => {
        const originalService =
          _.omit(
            _.find(mockServicesRefactored, { id: _.toSafeInteger(serviceID) }),
            AppSettings.SERVICES.DATA_NOT_TO_HYDRATE,
          );
        const hydratedService =
          _.omit(
            _.find(servicesHydrated, { repeatOf: _.toSafeInteger(serviceID) }),
            AppSettings.SERVICES.DATA_NOT_TO_HYDRATE,
          );

        const keysToCheck =
          Object.keys(originalService);

        const keysToCheckHydrated =
          _.difference(
            _.sortBy(Object.keys(hydratedService)),
            ['id', '_id', 'model'] // because the repeating services have no ID
          );

        // I check if the same fields exist in the source service
        // and the dynamically hydrated service
        expect(
          _.sortBy(
            keysToCheck,
          ).filter(value => !value.endsWith('Formatted'))
        ).toEqual(keysToCheckHydrated);

        // I then check if each key/value pair is the same between
        //  the source and dynamically hydrated service (`model` key is ignored)
        if (hydratedService && originalService) {
          _.forEach(keysToCheckHydrated, (theKey) => {
            expect(hydratedService[theKey]).toEqual(originalService[theKey]);
          });
        }
      });
    });

    it('should not copy date information from original service', () => {
      const servicesHydrated = serviceService.hydrateRepeatedServices(mockServices);
      _.forEach(repeatedItems, (serviceID, index) => {
        const originalService = _.find(mockServicesRefactored, { id: _.toSafeInteger(serviceID) });
        const hydratedService = _.find(servicesHydrated, { repeatOf: _.toSafeInteger(serviceID) });
        if (hydratedService) {
          // I now check if the dates are NOT copied over from the original service
          expect(hydratedService.dueDate).not.toEqual(originalService.dueDate);
          expect(hydratedService.dueStatus).not.toEqual(originalService.dueStatus);
          expect(hydratedService.dueStatusH).not.toEqual(originalService.dueStatusH);
          expect(hydratedService.upperRangeDate).not.toEqual(originalService.upperRangeDate);
          expect(hydratedService.lowerRangeDate).not.toEqual(originalService.lowerRangeDate);
        }
      });
    });

    _.forEach(AppSettings.SERVICES.DATA_NOT_TO_HYDRATE, (keyName) => {
      it(`should not copy ${keyName} value from original service`, () => {
        const servicesHydrated = serviceService.hydrateRepeatedServices(mockServices);

        _.forEach(repeatedItems, (serviceID, index) => {
          const originalService =
            _.find(mockServicesRefactored, { id: _.toSafeInteger(serviceID) });
          const hydratedService =
            _.find(servicesHydrated, { repeatOf: _.toSafeInteger(serviceID) });
          if (hydratedService) {
            // I now check if all data that needs to be copied
            // from source to destination is the same
            expect(hydratedService[keyName]).not.toEqual(originalService[keyName]);
          }
        });
      });
    });

    _.forEach(mockServiceLowerRangeDates, (date, index) => {
      it('should retain the original lower dates of the service', () => {
        const servicesHydrated =
          serviceService.hydrateRepeatedServices(mockServices);
        expect(servicesHydrated[index].lowerRangeDate).toEqual(date);
      });
    });

    _.forEach(mockServiceUpperRangeDates, (date, index) => {
      it('should retain the original upper dates of the service', () => {
        const servicesHydrated =
          serviceService.hydrateRepeatedServices(mockServices);
        expect(servicesHydrated[index].upperRangeDate).toEqual(date);
      });
    });
  });
});
