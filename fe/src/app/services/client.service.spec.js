/* global describe beforeEach beforeAll afterEach afterAll it expect inject */
import AppSettings from 'app/config/project-variables.js';
import ClientService from 'app/services/client.service';

describe('Client Service', () => {
  let makeService;
  let mockRestangular;
  let mockPromise;

  beforeEach(inject((_$rootScope_) => {
    mockPromise = {
      get() {
        return this;
      },
      getList() {
        return this;
      },
      customGET() {
        return this;
      },
      post() {
        return this;
      },
      customPOST() {
        return this;
      },
      customDELETE() {
        return this;
      },
      customGETLIST() {
        return this;
      },
      one() {
        return this;
      },
      all() {
        return this;
      },
    };

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
      setFavourite() {
        return mockPromise;
      },
      removeFavourite() {
        return mockPromise;
      },
      getByDueDate() {
        return mockPromise;
      },
      getServiceScheduleRange() {
        return mockPromise;
      },
      getDetails() {
        return mockPromise;
      },
    };

    makeService = () => new ClientService(mockRestangular, AppSettings);
  }));

  it.async('getAssetClientsByIMONumber() should have called the right functions', async () => {
    const clientService = makeService();
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular.all(), 'customGET').and.returnValue({
      name: 'Asset Name',
      assetType: 'Asset Type',
      imoNumber: 'Asset IMO Number',
    });

    await clientService.getAssetClientsByIMONumber('123456');

    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets);
    expect(mockRestangular.all().customGET).toHaveBeenCalledWith(`123456/${AppSettings.API_ENDPOINTS.Customers}`);
  });

  it.async('getClientsByName() should have called the right functions with wildcard', async () => {
    const clientService = makeService();
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular.all(), 'post');

    await clientService.getClientsByName('ClientName');

    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Clients);
    expect(mockRestangular.all().post).toHaveBeenCalledWith({
      name: '*ClientName*',
      shipBuilder: false,
    });
  });

  it.async('getClientsByIMONumber() should have called the right functions with wildcard', async () => {
    const clientService = makeService();
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular.all(), 'post');

    await clientService.getClientsByIMONumber(1234567);

    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Clients);
    expect(mockRestangular.all().post).toHaveBeenCalledWith({
      imoNumber: '*1234567*',
      shipBuilder: false,
    });
  });

  it.async('getClientsByIMONumber() should have called the right functions without wildcard', async () => {
    const clientService = makeService();
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular.all(), 'post');

    await clientService.getClientsByIMONumber(1234567, false);

    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Clients);
    expect(mockRestangular.all().post).toHaveBeenCalledWith({
      imoNumber: 1234567,
      shipBuilder: false,
    });
  });

  it.async('getShipBuilderByIMONumber() should have called the right functions with wildcard', async () => {
    const clientService = makeService();
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular.all(), 'post');

    await clientService.getShipBuilderByIMONumber(1234567);

    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Clients);
    expect(mockRestangular.all().post).toHaveBeenCalledWith({
      imoNumber: '*1234567*',
      shipBuilder: true,
    });
  });

  it.async('getShipBuilderByIMONumber() should have called the right functions without wildcard', async () => {
    const clientService = makeService();
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular.all(), 'post');

    await clientService.getShipBuilderByIMONumber(1234567, false);

    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Clients);
    expect(mockRestangular.all().post).toHaveBeenCalledWith({
      imoNumber: 1234567,
      shipBuilder: true,
    });
  });

  it.async('getShipBuilderByName() should have called the right functions with wildcard', async () => {
    const clientService = makeService();
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular.all(), 'post');

    await clientService.getShipBuilderByName('ClientName');

    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Clients);
    expect(mockRestangular.all().post).toHaveBeenCalledWith({
      name: '*ClientName*',
      shipBuilder: true,
    });
  });
});
