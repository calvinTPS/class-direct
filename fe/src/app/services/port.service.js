import * as _ from 'lodash';
import BaseService from './base.service';
import PortModel from '../common/models/port';

export default class PortService extends BaseService {
  /* @ngInject */
  constructor(
    Restangular,
    AppSettings,
  ) {
    super(arguments);
    this._apiPath = AppSettings.API_ENDPOINTS.Ports;
    this._modelClass = PortModel;
  }

  get modelClass() {
    return this._modelClass;
  }

  set modelClass(modelClass) {
    this._modelClass = modelClass;
  }

  get apiPath() {
    return this._apiPath;
  }

  set apiPath(apiPath) {
    this._apiPath = apiPath;
  }

  getAll() {
    return this._Restangular.all(this.apiPath).getList();
  }

  async search(searchKey) {
    const ports = await this._Restangular.all(this.apiPath).customPOST({
      search: searchKey,
    }, 'query');
    // we only want to search active ports where deleted = false
    return _.filter(ports, ['deleted', false]);
  }

  getPortSDO(portId) {
    return this._Restangular.one(this.apiPath, portId).customGET('sdo');
  }

  searchWithJsonDB(searchKey) {
    return this._Restangular.all(`port?name_like=${searchKey}&_limit=5`).getList();
  }

}
