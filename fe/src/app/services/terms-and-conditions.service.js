// import * as _ from 'lodash';
import BaseService from './base.service';

const CLASS_DIRECT_TNC_ID_KEY = 'classDirectTnCIDKey'; // Can be any string

export default class TermsAndConditionsService extends BaseService {
  /* @ngInject */
  constructor(
    $cookies,
    AppSettings,
    Restangular,
  ) {
    super(arguments);

    this._apiEndPoints = this._AppSettings.API_ENDPOINTS.AcceptTermsAndConditions;
    this._apiEndPointAccept = this._AppSettings.API_ENDPOINTS.ACCEPT_TERMS_AND_CONDITIONS.Accept;
    this._apiEndPointLatest = this._AppSettings.API_ENDPOINTS.ACCEPT_TERMS_AND_CONDITIONS.Latest;
  }

  async getLatestTermsAndConditions() {
    if (this._latestTermsAndConditions == null) {
      this._latestTermsAndConditions = await this._Restangular
          .one(this._apiEndPoints)
          .customGET(this._apiEndPointLatest);
    }

    return this._latestTermsAndConditions;
  }

  acceptTermsAndConditions(tcId) {
    return this._Restangular
      .one(this._apiEndPoints, tcId)
      .customPUT({}, this._apiEndPointAccept);
  }

  hasEquasisThetisAcceptedTC() {
    return !!this._$cookies.get(CLASS_DIRECT_TNC_ID_KEY);
  }

  setEquasisThetisAcceptedTC(tcId) {
    const expires = new Date(Date.now() + (60 * 60 * 1000)); // One hour
    this._$cookies.put(CLASS_DIRECT_TNC_ID_KEY, tcId, { expires });
  }
}
