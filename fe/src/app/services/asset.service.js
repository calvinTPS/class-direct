import * as _ from 'lodash';
import { cacheable, pageable } from 'app/common/decorators';
import AssetModel from 'app/common/models/asset/asset';
import BaseService from './base.service';
import CodicilModel from 'app/common/models/codicil/codicil';
import dateHelper from 'app/common/helpers/date-helper';
import ReportModel from 'app/common/models/report/';
import ServiceModel from 'app/common/models/service';
import TaskModel from 'app/common/models/task';

export default class AssetService extends BaseService {
  /* @ngInject */
  constructor(
    AppSettings,
    Restangular,
    ServiceService,
  ) {
    super(arguments);
    this._apiEndPoints = AppSettings.API_ENDPOINTS;
    this._apiPath = this._apiEndPoints.Assets;

    this._modelClass = AssetModel;
    this._isQuerying = false;
    this._favouriteAssetIds = [];
  }

  /**
   * Whether or not the user has favourite assets.
   *
   * @returns {boolean}
   */
  get userHasFavourites() {
    return this._favouriteAssetIds.length > 0;
  }

  /**
   * Whether or not a query is underway.
   *
   * @returns {boolean}
   */
  get isQuerying() {
    return this._isQuerying;
  }

  /**
   * Setter for isQuerying
   *
   * @param {boolean} boolean
   */
  set isQuerying(boolean) {
    this._isQuerying = boolean;
  }

  /**
   * Get method for asset or asset items.
   */
  @pageable
  async get(assetId, item, params = {}) {
    let assets = null;

    if (item) {
      const { SISTER_VESSEL_PAGINATION } = this._AppSettings.DEFAULT_PARAMS;
      _.forEach(SISTER_VESSEL_PAGINATION, (value, key) => {
        // Overwrite with the default if param doesn't exist
        _.set(params, key, _.get(params, key, value));
      });
      if (!AssetModel.isLRAsset(assetId)) {
        return null;
      }

      assets = await this._Restangular.one(this._apiPath, assetId)
        .customGET(item, params);

      _.forEach(assets, (object, index) => {
        assets[index] = Reflect.construct(AssetModel, [object]);
      });
    } else {
      assets = super.getOne(assetId);
    }

    return assets;
  }

  /**
   * Get the survey reports associated to an asset
   */
  @cacheable
  async getSurveyReports(assetId) {
    if (!AssetModel.isLRAsset(assetId)) {
      return null;
    }

    let reports = null;

    try {
      reports = await this._Restangular
        .one(`${this._apiPath}/${assetId}/${this._apiEndPoints.SurveyReports}`).get();

      _.forEach(reports, (object, index) => {
        reports[index] = Reflect.construct(ReportModel, [object]);
      });
    } catch (e) {
      // Will be handled globally
    }

    return reports;
  }

  /**
   * Get the sister assets count
   */
  @cacheable
  async getSisterAssetsCount(options) {
    if (!AssetModel.isLRAsset(options.assetId)) {
      return null;
    }
    let param;
    if (options.type === this._AppSettings.API_ENDPOINTS.RegisterBookSisters) {
      param = { leadShip: options.leadShip };
    } else if (options.type === this._AppSettings.API_ENDPOINTS.TechnicalSisters) {
      param = { leadImo: options.leadImo };
    }

    return await this._Restangular
      .one(`${this._apiPath}/${options.assetId}/${options.type}/count`)
      .customGET('', param);
  }

  @cacheable
  getDetails(assetId) {
    return this._Restangular.one(this._apiPath, assetId)
      .customGET(this._apiEndPoints.ASSET.Details);
  }

  @cacheable
  async getTasks(assetId) {
    const tasks = await this._Restangular
      .one(`${this._apiPath}/${assetId}/${this._apiEndPoints.ASSET.Tasks}`).get();

    _.forEach(tasks, (task, index) => {
      tasks[index] = Reflect.construct(TaskModel, [task]);
    });
    return tasks;
  }

  /**
   * PUT call to update tasks list
   *
   * @param payload {array} contains :
   *  {
   *    typeOfTask: String,
   *    creditedStatus: String,
   *    notes: String,
   *    dateCredited: String (format: YYYY-MM-DD)
   *  }
   */

  updateTasks(payload) {
    return this._Restangular
      .one(this._AppSettings.API_ENDPOINTS.ASSET.Tasks)
      .customPUT(payload);
  }

  /**
    * Get Assets based on provided query and params
    * @param {Object} query - The query parameters
    * @param {Object} params -  It should be in this format { page, size, sort, order }
    */
  @pageable
  async query(query, params = {}) {
    this._isQuerying = true;
    // Duplicate query because we are going to modify it a bit...
    const serverQuery = _.clone(query);

    // Format buildDateMin and buildDateMax to only be a date string
    ['buildDateMin', 'buildDateMax'].forEach((param) => {
      if (serverQuery[param]) {
        serverQuery[param] = dateHelper.formatDateServer(query[param]);
      }
    });

    // True if we are only querying for favourite
    const isFavouriteQueryOnly = _.isMatch(serverQuery, { isFavourite: true });
    if (isFavouriteQueryOnly) {
      // Reset favourite asset IDs cache
      this._favouriteAssetIds = [];
    }

    // By default use the pagination params from AppSettings
    const defaultParams = _.assign({}, this._AppSettings.DEFAULT_PARAMS.PAGINATION,
      this._AppSettings.DEFAULT_PARAMS.FLEET_SORT);
    _.forEach(defaultParams, (value, key) => {
      // Overwrite the default if param doesn't exist
      _.set(params, key, _.get(params, key, value));
    });

    const assets = await this._Restangular.all(this._apiPath)
      .customPOST(serverQuery, 'query', params, this.getAssetListPayloadFields());

    _.forEach(assets, (object, index) => {
      _.set(object, 'decorate', true);
      assets[index] =
        Reflect.construct(this._modelClass, [object, true]);

      if (isFavouriteQueryOnly && object.isFavourite) {
        this._favouriteAssetIds.push(object.id);
      }
    });

    this._isQuerying = false;

    return assets;
  }

  setFavourite(assetId) {
    // - This is used at the asset hub, e.g single asset page
    this._favouriteAssetIds.push(assetId);

    return this._Restangular
      .one(this._apiPath, assetId)
      .customPOST('', this._apiEndPoints.ASSET.Favourite);
  }

  removeFavourite(assetId) {
    // - This is used at the asset hub, e.g single asset page
    _.pull(this._favouriteAssetIds, assetId);

    return this._Restangular
      .one(this._apiPath, assetId)
      .customDELETE(this._apiEndPoints.ASSET.Favourite);
  }

  /**
   * @param  { add: ['string'], remove: ['string'] } payload
   */
  async updateFavourites(payload) {
    return this._Restangular.all(this._apiEndPoints.FAVOURITES).post(payload);
  }

  @cacheable
  async getByDueDate(assetId, item, dueDateParams, query) {
    if (!AssetModel.isLRAsset(assetId)) {
      return null;
    }

    const serverQuery = _.clone(query);

    _.forEach(dueDateParams, (param) => {
      if (serverQuery[param]) {
        serverQuery[param] = dateHelper.formatDateServer(query[param]);
      }
    });

    // Switch to query endpoints.
    const endpoint = (item === AssetService.ASSET_ITEMS.Services) ?
      this._AppSettings.API_ENDPOINTS.ASSET.ServicesDetailedQuery :
      this._AppSettings.API_ENDPOINTS.ASSET.CodicilsQuery;


    // Interceptor for codicils
    // We only want to show the codicils with certain unresolved statuses
    // Configure these statuses in app settings.
    // The filtering of the codicils is done by backend

    if (serverQuery && item === AssetService.ASSET_ITEMS.Codicils) {
      serverQuery.statusList = this._AppSettings.ASSET_DUE_STATUS_OPEN_IDS.OPEN_CODICILS;
    }

    let filteredItems = await this._Restangular
      .one(this._apiPath, assetId)
      .all(endpoint)
      .customPOST(serverQuery);

    // Interceptor for services
    // We only want to show the services with certain unresolved statuses
    // Configure these statuses in app settings.
    // As for codicils
    // Concate cocHDtos, actionableItemHDtos & statutoryFindingHDtos arrays into single array
    if (item === AssetService.ASSET_ITEMS.Services) {
      // eslint-disable-no-param-reassign
      filteredItems = this._ServiceService.hydrateRepeatedServices(filteredItems);
      filteredItems =
        _.filter(filteredItems,
          o => _.indexOf(this._AppSettings.ASSET_DUE_STATUS_OPEN_IDS.OPEN_SERVICE_IDS, _.get(o, 'serviceStatus.id')) !== -1);
    } else if (item === AssetService.ASSET_ITEMS.Codicils) {
      filteredItems = filteredItems.cocHDtos.concat(
        filteredItems.actionableItemHDtos, filteredItems.statutoryFindingHDtos);
    }

    // Convert to models if applicable
    _.forEach(filteredItems, (object, index) => {
      filteredItems[index] =
        Reflect.construct(
          item === AssetService.ASSET_ITEMS.Codicils ? CodicilModel : ServiceModel, [object]
        );
    });

    return filteredItems;
  }

  async getAssetByImoNumber(imoNumber) {
    let asset = await this._Restangular
      .one(this._apiEndPoints.Assets, imoNumber)
      .customGET(this._apiEndPoints.Asset);

    asset = Reflect.construct(this._modelClass, [asset]);

    return asset;
  }

  @cacheable
  async getServiceScheduleRange(assetId) {
    if (!AssetModel.isLRAsset(assetId)) {
      return null;
    }

    const scheduleRange = await this._Restangular
      .one(this._apiPath, assetId)
      .all(this._apiEndPoints.ASSET.Services)
      .customGET(this._apiEndPoints.ASSET.ScheduleRange);

    _.forEach(['lowRange', 'highRange'], (param) => {
      if (scheduleRange[param]) {
        scheduleRange[param] = new Date(scheduleRange[param]);
      }
    });
    return scheduleRange;
  }

  async getEORAssetByAssetId(assetId) {
    let asset = await this._Restangular
      .one(this._apiEndPoints.Assets, assetId)
      .customGET(this._apiEndPoints.Eor);

    asset = Reflect.construct(this._modelClass, [asset]);

    return asset;
  }

  getEORDetails(assetId) {
    return this._Restangular.one(this._apiPath, assetId)
      .all(this._apiEndPoints.ASSET.Details)
      .customGET(this._apiEndPoints.Eor);
  }

  // to be used only in asset-card
  getAssetCurrentUser(controllerName) {
    if (controllerName === 'asset-card') {
      return this._assetCurrentUser;
    }
    return null;
  }

  // to be used only in asset-card
  setAssetCurrentUser(userObj) {
    this._assetCurrentUser = userObj;
  }

  // Constants to represent asset item types
  static ASSET_ITEMS = {
    Codicils: 'Codicils',
    Services: 'Services',
  }

  getAssetListPayloadFields() {
    const fields = { Fields: AssetModel.PAYLOAD_FIELDS.FLEET_LIST.join(',') };
    return fields;
  }

}
