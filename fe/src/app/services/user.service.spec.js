/* global afterAll afterEach beforeAll beforeEach describe expect inject it window */
/* eslint-disable angular/window-service, no-underscore-dangle */

import AppSettings from 'app/config/project-variables.js';
import dateHelper from 'app/common/helpers/date-helper';
import ServicesModule from 'app/services/services';
import UserModel from 'app/common/models/user/user';
import UserService from 'app/services/user.service';

describe('User Service', () => {
  let makeUserService;
  let mockCurrentUser;
  let mockDocument;
  let mockFlagService;
  let mockForm;
  let mockPaginatedAssets;
  let mockPromise;
  let mockRestangular;
  let mockViewableUsers;
  let mockTermsAndConditions;
  let mockUtils;

  const ACCOUNT_STATUSES = [
    { id: 1, name: 'Active' },
    { id: 2, name: 'Archived' },
    { id: 3, name: 'Disabled' },
  ];

  const MOCK_LATEST_TC_PROMISE = {
    tcId: 1,
    tcDesc: 'test',
    tcDate: '2017-12-29',
    tcAuthor: 'tester',
    tcVersion: 1,
  };

  beforeEach(window.module(ServicesModule));

  beforeEach(() => {
    mockViewableUsers = [{
      id: 1,
      name: 'Jon',
      status: 1,
    }, {
      id: 2,
      name: 'Shah',
      status: 2,
    }, {
      id: 3,
      name: 'Francis',
      status: 3,
    }];

    mockCurrentUser = {
      userId: '101',
      flagCode: 100,
      shipBuilderCode: 1,
      clientCode: 1,
      name: 'John Doe',
      firstName: 'John',
      lastName: 'Doe',
      email: null,
      userAccExpiryDate: null,
      status: {
        id: 1,
        name: 'Active',
      },
      restrictAll: false,
      roles: [{
        roleId: 101,
        roleName: 'EQUASIS',
        admin: null,
        lrAdmin: null,
      }],
      company: {
        name: 'BAE SYSTEMS APPLIED INTELLIGENCE',
        addressLine1: 'LEVEL 28',
        addressLine2: 'MENARA BINJAI',
        addressLine3: '2 JALAN BINJAI',
        city: 'Kuala Lumpur',
        state: 'WP',
        postCode: '50450',
        country: 'Malaysia',
        telephone: '321913000',
      },
    };

    mockPaginatedAssets = {
      pagination: {
        internalId: '53c7c3b5-7d0a-4155-9bf9-1f6dcd8dd662',
        id: null,
        size: 3,
        number: 1,
        first: true,
        last: false,
        totalPages: 2,
        totalElements: 5,
        numberOfElements: 3,
        sort: null,
        link: null,
      },
      content: [{}],
    };

    // Mock Service requests
    mockPromise = {
      getList: () => mockViewableUsers,
      get: () => Promise.resolve(mockCurrentUser),
      customGET: () => this,
      customPOST: () => this,
      customPUT: () => this,
      post: () => this,
      withHttpConfig: () => mockPromise,
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one: () => mockPromise,
      all: (path) => {
        if (path === 'users/account-statuses') {
          return {
            getList: () => ACCOUNT_STATUSES,
          };
        } else if (path === 'security/create-masquerade-token') {
          return {
            post: () => ({ response: 'dummyToken' }),
          };
        }
        return mockPromise;
      },
      post: () => mockPromise,
    };

    mockTermsAndConditions = {
      getLatestTermsAndConditions: () =>
        new Promise((resolve, reject) => resolve(MOCK_LATEST_TC_PROMISE)),
    };

    mockForm = {
      setAttribute: () => { },
      submit: () => { },
    };

    mockDocument = {
      createElement: () => mockForm,
      body: {
        appendChild: () => { },
      },
    };

    mockUtils = {
      getFileExtension: () => 'xxx',
    };

    mockFlagService = {
      clearCaches: () => { },
    };

    const mockViewService = { registerTemplate: (x, y) => { } };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('Restangular', mockRestangular);
      $provide.value('TermsAndConditionsService', mockTermsAndConditions);
      $provide.value('FlagService', mockFlagService);
      $provide.value('$document', [mockDocument]);
      $provide.value('$window', {
        location: {
          origin: 'foo',
        },
      });
      $provide.value('ViewService', mockViewService);
      $provide.value('utils', mockUtils);
    });
  });

  beforeEach(inject(($injector) => {
    makeUserService = async () => {
      const userService = $injector.get('UserService');
      await userService.initializeUser();
      return userService;
    };
  }));

  describe('UserService', () => {
    describe('logout', () => {
      it.async('does a POST logout using a form', async () => {
        const userService = await makeUserService();

        spyOn(mockDocument, 'createElement').and.callThrough();
        spyOn(mockDocument.body, 'appendChild').and.callThrough();
        spyOn(mockForm, 'setAttribute').and.callThrough();
        spyOn(mockForm, 'submit').and.callThrough();

        userService.logout();

        expect(mockDocument.createElement).toHaveBeenCalledTimes(1);
        expect(mockDocument.body.appendChild).toHaveBeenCalledTimes(1);
        expect(mockDocument.body.appendChild).toHaveBeenCalledWith(mockForm);
        expect(mockForm.setAttribute).toHaveBeenCalledTimes(2);
        expect(mockForm.setAttribute).toHaveBeenCalledWith('method', 'POST');
        expect(mockForm.setAttribute)
          .toHaveBeenCalledWith('action', '/logout?logout=foo');
        expect(mockForm.submit).toHaveBeenCalledTimes(1);
      });
    });

    describe('getUsers', () => {
      beforeEach(() => {
        spyOn(mockRestangular, 'one').and.callThrough();
        spyOn(mockRestangular, 'all').and.callThrough();
        spyOn(mockRestangular.all(), 'customPOST').and.returnValue(mockViewableUsers);
      });

      it.async('calls Restangular.all to get list of users', async () => {
        const userService = await makeUserService();
        await userService.getUsers({ some: 'payload' });

        expect(AppSettings.API_ENDPOINTS.USERS.Query).not.toBeUndefined();
        expect(mockRestangular.all)
          .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.USERS.Query);
        expect(mockRestangular.all().customPOST)
          .toHaveBeenCalledWith(
            { some: 'payload' },
            null,
          {
            page: AppSettings.DEFAULT_PARAMS.PAGINATION.page,
            size: AppSettings.DEFAULT_PARAMS.PAGINATION.size,
          });
      });
    });

    describe('getUser', () => {
      beforeEach(() => {
        spyOn(mockRestangular, 'one').and.returnValue({
          get: () => Promise.resolve({ email: 'foo@test.io' }),
        });
        spyOn(mockRestangular, 'all').and.callThrough();
      });

      it.async('calls Restangular.one to get user', async () => {
        const userService = await makeUserService();
        const user = await userService.getUser(101);

        expect(mockRestangular.one).toHaveBeenCalledWith('users', 101);
        expect(AppSettings.API_ENDPOINTS.Users).not.toBeUndefined();
        expect(mockRestangular.all).not
          .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Users);
        expect(user instanceof UserModel).toBe(true);
        expect(user.email).toBe('foo@test.io');
      });
    });

    describe('getUsersByEmail', () => {
      const customPOSTStub = jasmine.createSpy('customPOST')
        .and.returnValue(Promise.resolve([{ email: 'foo@test.io' }]));
      beforeEach(() => {
        spyOn(mockRestangular, 'all').and.returnValue({
          getList: () => [],
          customPOST: customPOSTStub,
        });
      });

      it.async('calls Restangular.all.customPOST to get users by email', async () => {
        const userService = await makeUserService();
        const users = await userService.getUsersByEmail('foo@test.io');

        // Needs to use searchSSOIfNotExist
        expect(customPOSTStub).toHaveBeenCalledWith({
          email: 'foo@test.io',
          searchSSOIfNotExist: true,
          status: ['Active', 'Disabled'],
        });
        expect(users instanceof Array).toBe(true);
        expect(users[0] instanceof UserModel).toBe(true);
        expect(users[0].email).toBe('foo@test.io');
      });
    });

    describe('getCurrentUser', () => {
      it.async('calls Restangular.one to get the current user', async () => {
        spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
        spyOn(mockRestangular.one(), 'withHttpConfig').and.returnValue(mockPromise);
        spyOn(mockPromise, 'get').and.returnValue(Promise.resolve(mockCurrentUser));

        const userService = await makeUserService();
        const currentUser = await userService.getCurrentUser();

        expect(AppSettings.API_ENDPOINTS.CurrentUser).toEqual('current-user');
        expect(mockRestangular.one).toHaveBeenCalledWith('current-user');
        expect(currentUser.model.userId).toEqual(mockCurrentUser.userId);
        expect(currentUser.model.email).toEqual(mockCurrentUser.email);
      });
    });
  });

  describe('UserService.getAccessibleAssets', () => {
    beforeEach(() => {
      spyOn(mockRestangular, 'all').and.callThrough();
      spyOn(mockRestangular.all(), 'customGET').and.returnValue(mockPaginatedAssets);
    });

    it.async('calls Restangular.all.customGET to get accessible assets', async () => {
      const userService = await makeUserService();
      const userId = '1';
      const assets = await userService.getAccessibleAssets(userId);

      expect(AppSettings.API_ENDPOINTS.Users).not.toBeUndefined();
      expect(mockRestangular.all)
        .toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.Users}/${userId}/`);
      expect(mockRestangular.all().customGET)
        .toHaveBeenCalledWith(
          AppSettings.API_ENDPOINTS.SUBFLEET.AccessibleAssets,
          AppSettings.DEFAULT_PARAMS.PAGINATION
        );

      // Make sure there is pagination in the return object
      expect(assets.pagination).not.toBeUndefined();
    });
  });

  describe('UserService.getRestrictedAssets', () => {
    beforeEach(() => {
      spyOn(mockRestangular, 'all').and.callThrough();
      spyOn(mockRestangular.all(), 'customGET').and.returnValue(mockPaginatedAssets);
    });

    it.async('calls Restangular.all.customGET to get accessible assets', async () => {
      const userService = await makeUserService();
      const userId = '1';
      const assets = await userService.getRestrictedAssets(userId);

      expect(AppSettings.API_ENDPOINTS.Users).not.toBeUndefined();
      expect(mockRestangular.all)
        .toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.Users}/${userId}/`);
      expect(mockRestangular.all().customGET)
        .toHaveBeenCalledWith(
          AppSettings.API_ENDPOINTS.SUBFLEET.RestrictedAssets,
          AppSettings.DEFAULT_PARAMS.PAGINATION
        );

      // Make sure there is pagination in the return object
      expect(assets.pagination).not.toBeUndefined();
    });
  });

  describe('deleteUserEor', () => {
    it.async('calls restangular correctly', async () => {
      const userService = await makeUserService();
      const mockCustom = {
        customDELETE: () => { },
      };
      spyOn(mockCustom, 'customDELETE');
      spyOn(mockRestangular, 'one').and.returnValue(mockCustom);

      userService.deleteUserEor('1', 123123);
      expect(mockRestangular.one).toHaveBeenCalledWith('users', '1');
      expect(mockCustom.customDELETE).toHaveBeenCalledWith(
        'eor/123123',
      );
    });
  });

  describe('UserService.updateEorExpiryDate', () => {
    beforeEach(() => {
      spyOn(mockRestangular, 'one').and.callThrough();
      spyOn(mockRestangular.one(), 'customPUT').and
        .returnValue({ assetExpiry: '2016-12-19', imoNumber: 1 });
    });

    it.async('calls Restangular.one.customPUT', async () => {
      const userService = await makeUserService();
      const newExpiryDate = new Date(2016, 12, 19);
      const result = await userService.updateEorExpiryDate('101', 1, newExpiryDate);

      expect(AppSettings.API_ENDPOINTS.Users).not.toBeUndefined();
      expect(AppSettings.API_ENDPOINTS.Eor).not.toBeUndefined();
      expect(mockRestangular.one().customPUT)
        .toHaveBeenCalledWith({
          eor: [{
            assetExpiry: dateHelper.formatDateServer(newExpiryDate),
            imoNumber: 1,
          }],
        });
      expect(result).toEqual(true);
    });
  });

  // TODO: Update this when backend returns an array list of id's
  describe('UserService.getAccessibleAssetIds', () => {
    beforeEach(() => {
      spyOn(mockRestangular, 'all').and.callThrough();
      spyOn(mockRestangular.all(), 'customGET').and.returnValue([1, 2, 3, 4, 5]);
    });

    it.async('calls Restangular.all.customGET to get accessible asset ids', async () => {
      const userService = await makeUserService();
      const userId = '1';
      await userService.getAccessibleAssetIds(userId);

      expect(AppSettings.API_ENDPOINTS.Users).not.toBeUndefined();
      expect(mockRestangular.all)
        .toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.Users}/${userId}/`);
      expect(mockRestangular.all().customGET)
        .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.SUBFLEET.AccessibleAssetIds);
    });
  });

  // TODO: Update this when backend returns an array list of id's
  describe('UserService.getRestrictedAssetIds', () => {
    beforeEach(() => {
      spyOn(mockRestangular, 'all').and.callThrough();
      spyOn(mockRestangular.all(), 'customGET').and.returnValue([1, 2, 3, 4, 5]);
    });

    it.async('calls Restangular.all.customGET to get restricted asset ids', async () => {
      const userService = await makeUserService();
      const userId = '1';
      await userService.getRestrictedAssetIds(userId);

      expect(AppSettings.API_ENDPOINTS.Users).not.toBeUndefined();
      expect(mockRestangular.all)
        .toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.Users}/${userId}/`);
      expect(mockRestangular.all().customGET)
        .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.SUBFLEET.RestrictedAssetIds);
    });
  });

  describe('UserService.updateSubFleet()', () => {
    beforeEach(() => {
      spyOn(mockRestangular, 'all').and.callThrough();
      spyOn(mockRestangular.all(), 'post').and.returnValue({ message: 'subfleet updated.' });
    });

    it.async('calls Restangular.all.post with the right params', async () => {
      const userService = await makeUserService();
      const userId = '1';
      const includedAssetIds = [1, 2, 3];
      await userService.updateSubFleet(
        userId,
        UserService.MOVE_ASSETS_TO.ACCESSIBLE,
        includedAssetIds,
        null
      );

      expect(AppSettings.API_ENDPOINTS.Users).not.toBeUndefined();
      expect(mockRestangular.all)
        .toHaveBeenCalledWith(`${AppSettings.API_ENDPOINTS.Users}/${userId}/${AppSettings.API_ENDPOINTS.SUBFLEET.SaveSubfleet}`);
      expect(mockRestangular.all().post)
        .toHaveBeenCalledWith({
          moveTo: UserService.MOVE_ASSETS_TO.ACCESSIBLE,
          includedAssetIds,
        });

      const excludedAssetIds = [4, 5, 6];
      await userService.updateSubFleet(
        userId,
        UserService.MOVE_ASSETS_TO.RESTRICTED,
        null,
        excludedAssetIds,
      );
      expect(mockRestangular.all().post)
        .toHaveBeenCalledWith({
          moveTo: UserService.MOVE_ASSETS_TO.RESTRICTED,
          excludedAssetIds,
        });
    });

    it.async('returns true or false', async () => {
      const userService = await makeUserService();
      const userId = '1';
      const includedAssetIds = [1, 2, 3];
      const response =
        await userService.updateSubFleet(
          userId,
          UserService.MOVE_ASSETS_TO.RESTRICTED,
          includedAssetIds
        );

      expect(response).toEqual(true);
    });
  });

  describe('UserService.getCurrentUser', () => {
    it.async('calls Restangular.one to get the currrent user', async () => {
      spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
      spyOn(mockRestangular.one(), 'withHttpConfig').and.returnValue(mockPromise);
      spyOn(mockPromise, 'get').and.returnValue(Promise.resolve(mockCurrentUser));

      const userService = await makeUserService();
      const currentUser = await userService.getCurrentUser();

      expect(AppSettings.API_ENDPOINTS.CurrentUser).toEqual('current-user');
      expect(mockRestangular.one).toHaveBeenCalledWith('current-user');
      expect(currentUser.userId).toEqual(mockCurrentUser.userId);
      expect(currentUser.model.userId).toEqual(mockCurrentUser.userId);
      expect(currentUser.model.email).toEqual(mockCurrentUser.email);
    });
  });

  describe('UserService.unsetCurrentUser', () => {
    it('unsets the current user', async () => {
      const userService = await makeUserService();
      spyOn(mockFlagService, 'clearCaches');

      userService._currentUserPromise = Promise.resolve({});
      userService.unsetCurrentUser();

      expect(userService._currentUserPromise).toEqual(null);
      expect(mockFlagService.clearCaches).toHaveBeenCalled();
    });
  });

  describe('getAccountStatuses', () => {
    beforeEach(() => {
      spyOn(mockRestangular, 'all').and.callThrough();
      spyOn(mockTermsAndConditions, 'getLatestTermsAndConditions').and.callThrough();
    });

    it.async('calls Restangular.all to get the list of account statuses', async () => {
      const userService = await makeUserService();
      const accountStatuses = await userService.getAccountStatuses();

      expect(AppSettings.API_ENDPOINTS.USERS.AccountStatuses).toEqual('users/account-statuses');
      expect(mockRestangular.all).toHaveBeenCalledWith('users/account-statuses');
      expect(accountStatuses).toEqual(ACCOUNT_STATUSES);
    });
  });

  describe('UserService.editUser', () => {
    it.async('should pass only editable fields and make backend call to edit user', async () => {
      const userService = await makeUserService();
      const mockOne = {
        customPUT: () => { },
      };
      spyOn(mockRestangular, 'one').and.returnValue(mockOne);
      spyOn(mockOne, 'customPUT');
      await userService.editUser(101, { status: 3, name: 'John Doe' });

      expect(mockRestangular.one).toHaveBeenCalledWith('users', 101);
      expect(mockOne.customPUT).toHaveBeenCalledWith({ status: 3 }, null, {
        deleteFields: '',
      });
    });
  });

  describe('UserService.isMasquerading', () => {
    it.async('should defined the variable correctly', async () => {
      const userService = await makeUserService();

      expect(userService.isMasquerading).toBe(false);
      userService._isMasquerading = true;
      expect(userService.isMasquerading).toBe(true);
    });
  });

  describe('UserService.masqueradeId', () => {
    it.async('should defined the variable correctly', async () => {
      const userService = await makeUserService();

      expect(userService.masqueradeId).not.toBeUndefined();
      userService.masqueradeId = 'test';
      expect(userService.masqueradeId).toBe('test');
    });
  });

  describe('UserService.getMasqueradeToken()', () => {
    it.async('calls Restangular.post to get a token from the server', async () => {
      const userId = 123;
      const userService = await makeUserService();
      spyOn(mockRestangular, 'all').and.callThrough();

      expect(AppSettings.API_ENDPOINTS.SECURITY.ROOT).toEqual('security');
      expect(AppSettings.API_ENDPOINTS.SECURITY.CREATE_MASQUERADE_TOKEN).toEqual('create-masquerade-token');

      const response = await userService.getMasqueradeToken({ userId });
      expect(mockRestangular.all).toHaveBeenCalledWith('security/create-masquerade-token');
      expect(response).toEqual({ response: 'dummyToken' });
    });
  });
});
