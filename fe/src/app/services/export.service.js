import * as _ from 'lodash';
import BaseService from './base.service';
import UsersExportSummaryModel from 'app/common/models/user/users-export-summary';

export default class ExportService extends BaseService {
  /* @ngInject */
  constructor(
    AppSettings,
    DownloadService,
    Restangular,
    utils,
    $timeout,
  ) {
    super(arguments);
  }

  /**
   * Retrieve the metadata for the users information to be exported
   * given the selected emails object.
   *
   * Only includedUser, OR excludedUsers with optional filter needs to be present in the object.
   *
   * @param {Object} selectedEmails
   * @param {string[]} selectedEmails.includedUsers The list of emails to be included.
   * @param {string[]} selectedEmails.excludedUsers The list of emails to be excluded.
   * @param {string[]} selectedEmails.filter The filter currently applied, may be empty.
   *
   * @returns {Promise}
   */
  async getUsersExportSummary(selectedEmails, filter) {
    let payload = _.clone(selectedEmails);
    if (filter) {
      payload = _.merge(payload, filter);
    }
    const usersExportInfo = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.USERS.ExportSummary)
      .post(payload);

    return Reflect.construct(UsersExportSummaryModel, [usersExportInfo]);
  }

  /**
   * Initialize the export and download of the users export CSV.
   *
   * @see getUsersExportSummary
   * @returns {Promise}
   */
  async exportUsersCSV(selectedEmails, filter) {
    let payload = _.clone(selectedEmails);
    if (filter) {
      payload = _.merge(payload, filter);
    }

    const endpoints = _.clone(this._AppSettings.API_ENDPOINTS.EXPORT);
    const endpointToUse = `${endpoints.ROOT}/${endpoints.USERS}`;
    const mobileDownload = {
      httpMethod: 'GET',
      fileName: 'Users-export',
    };

    const responseObject = await this._Restangular
      .all(endpointToUse)
      .post(payload);

    const token = _.get(responseObject, 'response');
    const downloadFileName = _.get(responseObject, 'fileName');
    mobileDownload.fileExtension = this._utils.getFileExtension(downloadFileName);
    if (token) {
      this._DownloadService.downloadFile({
        token,
        mobileDownload,
        downloadFileName,
      });
    }
  }

  /**
   * Initialize the export and download of the assets export.
   *
   * @param {Object} payload
   * @param {string[]} payload.include The list of asset_id to be included.
   * @param {string[]} payload.exclude The list of asset_id to be excluded.
   */
  async exportAssetsPDF(payload, multipleAssets) {
    const endpoints = _.clone(this._AppSettings.API_ENDPOINTS.EXPORT);
    const endpointToUse = `${endpoints.ROOT}/${endpoints.ASSETS_ASYNC}`;
    const responseObject = await this._Restangular
      .all(endpointToUse)
      .post(payload);

    const jobId = _.get(responseObject, 'jobId');
    this.exportAssetToken(jobId, endpointToUse, -1);
  }

   /**
   * Initialize the export and download of the assets export.
   *
   * @param {string} jobId the key id to check the download status from backend
   * @param {string} endpointToUse the api url to be called.
   * @param {number} executionCount the api call count.
   */
  exportAssetToken(jobId, endpointToUse, executionCount) {
    let status;
    let token;
    let count;
    const timeInterval = [1000, 2000, 3000, 5000, 8000, 15000];
    const maxCount = timeInterval.length - 1;
    this._Restangular.one(endpointToUse, jobId).get().then((sucessData) => {
      status = _.get(sucessData, 'statusCode');
      if (status === this._AppSettings.HTTP_STATUS.ACCEPTED) {
        count = executionCount < maxCount ? executionCount + 1 : executionCount;
        this._$timeout(() => { this.exportAssetToken(jobId, endpointToUse, count); },
        timeInterval[count]);
      } else if (status === this._AppSettings.HTTP_STATUS.OK) {
        token = _.get(sucessData, 'downloadToken');
        const downloadFileName = _.get(sucessData, 'fileName');
        this.downloadFile(token, downloadFileName);
      }
    });
  }

   /**
   * Initialize the download of the assets export.
   *
   * @param {String} token the token to download the file from aws s3 bucket.
   * @param {String} downloadFileName the name of the file to be downloaded from aws s3 bucket.
   */
  downloadFile(token, downloadFileName) {
    const mobileDownload = {
      httpMethod: 'GET',
      fileName: 'Assets-export',
    };

    mobileDownload.fileExtension = this._utils.getFileExtension(downloadFileName);

    if (token) {
      this._DownloadService.downloadFile({
        broadcastEvent: this._AppSettings.EVENTS.ASSETS_EXPORT_DONE,
        token,
        mobileDownload,
        downloadFileName,
      });
    }
  }

  /**
   * General method for exporting most items
   *
   * @param {Object} payload
   * @param {string} payload.itemType Type of item to export. This will determine the api to use
   * @param {any} payload.xxxx (xxxx) Any required payload
   */
  async export(payload) {
    const endpoints = _.clone(this._AppSettings.API_ENDPOINTS.EXPORT);
    const endpointToUse = `${endpoints.ROOT}/${endpoints[payload.itemType]}`;

    const mobileDownload = {
      httpMethod: 'GET',
      fileName: `${payload.itemType}-export`,
    };
    // itemType not required by backend
    delete payload.itemType; // eslint-disable-line no-param-reassign

    const responseObject = await this._Restangular
      .all(endpointToUse)
      .post(payload);
    const token = _.get(responseObject, 'response');
    const downloadFileName = _.get(responseObject, 'fileName');
    mobileDownload.fileExtension = this._utils.getFileExtension(downloadFileName);
    if (token) {
      this._DownloadService.downloadFile({
        token,
        mobileDownload,
        downloadFileName,
      });
    }
  }
}
