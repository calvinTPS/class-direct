import angular from 'angular';
import AssetService from './asset.service';
import CertificateService from './certificate.service';
import ClientService from './client.service';
import CMROService from './cmro.service';
import CodicilService from './codicil.service';
import DefectService from './defect.service';
import DownloadService from './download.service';
import ErrorService from './error.service';
import ExportService from './export.service';
import FlagService from './flag.service';
import JobService from './job.service';
import NotificationService from './notification.service';
import PermissionsService from './permissions.service';
import PortService from './port.service';
import ReferenceDataService from './reference-data.service';
import ServiceService from './service.service';
import SurveyService from './survey.service';
import TermsAndConditionsService from './terms-and-conditions.service';
import UserService from './user.service';
import ViewService from './view.service';
// @insertImport

export default angular
  .module('app.services', [])
  .service({
    AssetService,
    CertificateService,
    ClientService,
    CMROService,
    CodicilService,
    DefectService,
    DownloadService,
    ErrorService,
    ExportService,
    FlagService,
    JobService,
    NotificationService,
    PermissionsService,
    PortService,
    ReferenceDataService,
    ServiceService,
    SurveyService,
    TermsAndConditionsService,
    UserService,
    ViewService,
    // @insertInstance
  }).name;
