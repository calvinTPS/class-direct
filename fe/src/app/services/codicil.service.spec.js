/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import AttachmentModel from 'app/common/models/attachment';
import CodicilModel from 'app/common/models/codicil/codicil';
import CodicilService from 'app/services/codicil.service';

describe('Codicil Service', () => {
  let makeService;
  const mockRestangular = {
    one: () => {},
  };

  beforeEach(inject((_$rootScope_) => {
    makeService = () => new CodicilService(AppSettings, mockRestangular);
  }));

  describe('CodicilService', () => {
    describe('get', () => {
      describe('without assetID', () => {
        it.async('gets the codicil items belonging to an asset', async() => {
          const service = makeService();
          const result = await service.get();

          spyOn(mockRestangular, 'one').and.stub();
          expect(mockRestangular.one).toHaveBeenCalledTimes(0);
          expect(result).toEqual(null);
        });
      });

      describe('with assetID and codicilType', () => {
        it.async('gets the codicil items belonging to an asset', async () => {
          const service = makeService();

          spyOn(mockRestangular, 'one').and.returnValue({
            customGET: () => Promise.resolve([{ codicil: true }]),
          });
          const result = await service.get('LRV123', 'codicils');

          expect(mockRestangular.one).toHaveBeenCalledTimes(1);
          expect(result[0] instanceof CodicilModel).toBe(true);
        });

        it.async('non-LR asset ID returns null', async () => {
          const service = makeService();
          const result = await service.get('IHS1235456', 'codicils');

          expect(result).toBe(null);
        });
      });
    });

    describe('getOne', () => {
      it.async('with empty params', async () => {
        const service = makeService();
        const result = await service.getOne();

        spyOn(mockRestangular, 'one').and.stub();
        expect(mockRestangular.one).toHaveBeenCalledTimes(0);
        expect(result).toEqual(null);
      });
    });

    describe('query', () => {
      it.async('return null with empty params', async () => {
        const service = makeService();

        spyOn(mockRestangular, 'one').and.returnValue({
          customPOST: () => Promise.resolve([{ codicil: true }]),
        });
        const result = await service.query();

        expect(mockRestangular.one).toHaveBeenCalledTimes(0);
        expect(result).toEqual(null);
      });

      it.async('return not null with existing params', async () => {
        const service = makeService();

        spyOn(mockRestangular, 'one').and.returnValue({
          customPOST: () => Promise.resolve([{ codicil: true }]),
        });
        const result = await service.query('LRV1', 'codicils');

        expect(result).not.toEqual(null);
        expect(result[0] instanceof CodicilModel).toBe(true);
      });
    });

    describe('getStatutoryFindings', () => {
      it.async('return null with empty params', async () => {
        const service = makeService();

        spyOn(mockRestangular, 'one').and.stub();
        const result = await service.getStatutoryFindings();

        expect(mockRestangular.one).toHaveBeenCalledTimes(0);
        expect(result).toEqual(null);
      });

      it.async('returns correct instance', async () => {
        const service = makeService();
        spyOn(mockRestangular, 'one').and.returnValue({ one: () => {} });
        spyOn(mockRestangular.one(), 'one').and.returnValue({ one: () => {} });
        spyOn(mockRestangular.one().one(), 'one').and.returnValue({ get: () => {} });
        spyOn(mockRestangular.one().one().one(), 'get').and.returnValue(Promise.resolve({ codicil: true }));

        const result = await service.getStatutoryFindings(
          'LRV1',
          '1',
          '63',
        );

        expect(result).not.toEqual(null);
        expect(result instanceof CodicilModel).toBe(true);
      });
    });

    it.async('getMNCNAttachments() returns correct object instance', async () => {
      const service = makeService();
      spyOn(mockRestangular, 'one').and.returnValue({ get: () => {} });
      spyOn(mockRestangular.one(), 'get').and.returnValue(Promise.resolve([{ codicil: true }]));

      const result = await service.getMNCNAttachments(
        'LRV1',
        '1',
      );

      expect(result).not.toEqual(null);
      expect(result[0] instanceof AttachmentModel).toBe(true);
    });
  });
});
