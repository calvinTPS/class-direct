/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */

import AppSettings from 'app/config/project-variables.js';
import DefectModel from 'app/common/models/defect/';
import DefectService from 'app/services/defect.service.js';

describe('Defect Service', () => {
  let makeService;
  let mockRestangular;
  let mockPromise;

  const initialSampleData = {
    id: 1,
    incidentDate: '2015-04-18T00:00:00Z',
    incidentDescription: 'Small crack found on window, internal damage',
    title: 'Cracked Window',
    defectCategory: {
      id: 1,
      deleted: false,
      name: 'Electro-technical',
      description: null,
      categoryLetter: 'E',
      parent: null,
      faultGroup: {
        id: 1,
        _id: 1,
      },
      productCatalogue: null,
      codicilCategory: {
        id: 11,
        _id: 11,
      },
      _id: 1,
    },
    defectStatusH: {
      id: 1,
      deleted: false,
      name: 'Open',
      description: null,
      _id: 1,
    },
    jobsH: [],
    itemsH: null,
    cocH: null,
  };

  const modifiedSampleData = Reflect.construct(DefectModel, [initialSampleData]);

  beforeEach(inject((_$rootScope_) => {
    // Mock Service requests
    mockPromise = {
      customGET: () => {},
      customPOST: () => {},
      one: () => ({ get: () => {} }),
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one: () => mockPromise,
    };

    makeService = () => new DefectService(AppSettings, mockRestangular);
  }));

  describe('DefectService', () => {
    it.async('getOne()', async () => {
      const service = makeService();
      spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
      spyOn(mockRestangular.one(), 'one').and.returnValue({ get: () => {} });
      spyOn(mockRestangular.one().one(), 'get').and.returnValue(initialSampleData);

      const returnedData = await service.getOne(1, 'defects', 10);

      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 1);
      expect(mockRestangular.one().one)
        .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.ASSET.Defect, 10);
      expect(mockRestangular.one().one().get).toHaveBeenCalled();
      expect(returnedData).toEqual(modifiedSampleData);
    });

    it.async('query()', async () => {
      const service = makeService();
      spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
      spyOn(mockRestangular.one(), 'customPOST').and.returnValue([initialSampleData]);

      const returnedData = await service.query(1, 'defects/query');
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Assets, 1);
      expect(mockRestangular.one().customPOST)
        .toHaveBeenCalledWith({}, AppSettings.API_ENDPOINTS.ASSET.Defects, {}, { Fields: DefectModel.PAYLOAD_FIELDS.DEFECT_LIST.join(',') });
      expect(returnedData).toEqual([modifiedSampleData]);
    });

    it('_getDefectListPayloadFields() should return correct value', () => {
      let defectType;
      let fields;
      const service = makeService();

      fields = service._getDefectListPayloadFields(defectType);
      expect(fields).toEqual({});

      defectType = AppSettings.API_ENDPOINTS.ASSET.StatutoryDeficiencies;
      fields = service._getDefectListPayloadFields(defectType);
      expect(fields).toEqual({ Fields: DefectModel.PAYLOAD_FIELDS.SD_LIST.join(',') });

      defectType = AppSettings.API_ENDPOINTS.ASSET.Defects;
      fields = service._getDefectListPayloadFields(defectType);
      expect(fields).toEqual({ Fields: DefectModel.PAYLOAD_FIELDS.DEFECT_LIST.join(',') });
    });

    it('_getDefectIDetailPayloadFields() should return correct value', () => {
      let defectType;
      let fields;
      const service = makeService();

      fields = service._getDefectIDetailPayloadFields(defectType);
      expect(fields).toEqual({});

      defectType = AppSettings.API_ENDPOINTS.ASSET.Deficiencies;
      fields = service._getDefectIDetailPayloadFields(defectType);
      expect(fields).toEqual({ Fields: DefectModel.PAYLOAD_FIELDS.SD_DETAIL.join(',') });

      defectType = AppSettings.API_ENDPOINTS.ASSET.Defects;
      fields = service._getDefectIDetailPayloadFields(defectType);
      expect(fields).toEqual({});
    });
  });
});
