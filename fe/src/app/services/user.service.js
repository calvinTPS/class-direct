/* eslint-disable no-param-reassign, angular/window-service*/

import * as _ from 'lodash';
import { pageable, sortable } from 'app/common/decorators';
import AssetModel from 'app/common/models/asset/asset';
import BaseService from './base.service';
import dateHelper from 'app/common/helpers/date-helper';
import UserModel from 'app/common/models/user/user';

// Should not include Archived users when creating new users
const ALLOWED_USER_STATUSES_FOR_CREATING_NEW_USERS = ['Active', 'Disabled'];

export default class UserService extends BaseService {
  /* @ngInject */
  constructor(
    $cacheFactory,
    $document,
    $timeout,
    $window,
    AppSettings,
    FlagService,
    Restangular,
    TermsAndConditionsService,
    utils,
    ViewService,
  ) {
    super(arguments);
    this._isMasquerading = false;
    this._masqueradeId = null;
  }

  /**
   * I am the first API call to the server
   * and I prevent the app from running
   * if I return an error.
  **/
  async initializeSession() {
    if (this.initializePromise == null) {
      this.initializePromise =
        this._Restangular.one(this._AppSettings.API_ENDPOINTS.InitCSRF).get();
    }
    return this.initializePromise;
  }

  initializeUser() {
    return this.getAccountStatuses().then((accountStatuses) => {
      // Provide UserModel with Account Statuses so that it can hydrate on the fly
      UserModel.ACCOUNT_STATUSES = accountStatuses;
    });
  }

  get isMasquerading() {
    return this._isMasquerading;
  }

  set isMasquerading(bool) {
    this._isMasquerading = Boolean(bool);
  }

  get masqueradeId() {
    return this._masqueradeId;
  }

  set masqueradeId(id) {
    this._masqueradeId = id;
  }

  logout() {
    if (this._$window.cordova) {
      // Clear all cookies because managed by Cordova cookie plugin
      this._$window.cookieEmperor.clearAll();

      this._$window.fetch(
        `${this._AppSettings.API_URL}/logout?logout=${this._$window.location.origin}`,
        {
          method: 'POST',
          credentials: 'include',
        },
      ).then(() => {
        this._$window.onunload = () => { };
        this._$timeout(() => {
          this._$window.location.reload(true);
        });
      });
    } else {
      // For technical reasons, we have to use form to POST to prevent CORS redirect error
      const form = this._$document[0].createElement('form');
      form.setAttribute('method', 'POST');
      form.setAttribute('action', `/logout?logout=${this._$window.location.origin}`);
      this._$document[0].body.appendChild(form);
      form.submit();
    }
  }

  /**
    @param {object} payload - query paramaters to pass to backend
    {
      "role": ["string"]
      "codeSearch": {
        "type": "string", //"CLIENT", "FLAG", "SHIP_BUILDER"
        "code": "string"
      },
      "dateOfLastLoginMin": "string",
      "dateOfLastLoginMax": "string",
    }
  */
  @pageable
  @sortable('name', 'status')
  async getUsers(
    payload,
    params = {},
  ) {
    // By default use the pagination params from AppSettings
    const defaultParams = _.pick(this._AppSettings.DEFAULT_PARAMS.PAGINATION, ['page', 'size']);
    _.forEach(defaultParams, (value, key) => {
      // Overwrite with the default if param doesn't exist
      _.set(params, key, _.get(params, key, value));
    });

    const users = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.USERS.Query)
      .customPOST(payload, null, params);

    // Using _.map(viewableUsers, userObj => Reflect.construct(UserModel, [userObj]));
    // would be better but the original viewableUsers object has pagination properties.
    // the _.map() will return a new array
    _.forEach(users, (userObj, index) => {
      users[index] = Reflect.construct(UserModel, [userObj]);
    });

    return users;
  }

  async getUser(userId) {
    const userObj = await this._Restangular
      .one(this._AppSettings.API_ENDPOINTS.Users, userId)
      .get();

    return Reflect.construct(UserModel, [userObj]);
  }

  async getUsersByEmail(email) {
    const users = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.USERS.Query)
      .customPOST({
        email,
        // needed for SSO only search, to retrieve users not yet registered
        // in Class Direct but registered in SSO
        status: ALLOWED_USER_STATUSES_FOR_CREATING_NEW_USERS,
        searchSSOIfNotExist: true,
      });

    _.forEach(users, (userObj, index) => {
      users[index] = Reflect.construct(UserModel, [userObj]);
    });

    return users;
  }

  @pageable
  @sortable('name', 'imo')
  async getAccessibleAssets(
    userId,
    { page, size, sort, order } = this._AppSettings.DEFAULT_PARAMS.PAGINATION
  ) {
    const assets = await this._Restangular
      .all(`${this._AppSettings.API_ENDPOINTS.Users}/${userId}/`)
      .customGET(
        this._AppSettings.API_ENDPOINTS.SUBFLEET.AccessibleAssets,
        { page, size, sort, order });

    _.forEach(assets, (assetObj, index) => {
      assets[index] = Reflect.construct(AssetModel, [assetObj]);
    });

    return assets;
  }

  @pageable
  @sortable('name', 'imo')
  async getRestrictedAssets(
    userId,
    { page, size, sort, order } = this._AppSettings.DEFAULT_PARAMS.PAGINATION
  ) {
    const assets = await this._Restangular
      .all(`${this._AppSettings.API_ENDPOINTS.Users}/${userId}/`)
      .customGET(
        this._AppSettings.API_ENDPOINTS.SUBFLEET.RestrictedAssets,
        { page, size, sort, order });

    _.forEach(assets, (assetObj, index) => {
      assets[index] = Reflect.construct(AssetModel, [assetObj]);
    });

    return assets;
  }

  async getAccessibleAssetIds(userId) {
    return await this._Restangular
      .all(`${this._AppSettings.API_ENDPOINTS.Users}/${userId}/`)
      .customGET(this._AppSettings.API_ENDPOINTS.SUBFLEET.AccessibleAssetIds);
  }

  async getRestrictedAssetIds(userId) {
    return await this._Restangular
      .all(`${this._AppSettings.API_ENDPOINTS.Users}/${userId}/`)
      .customGET(this._AppSettings.API_ENDPOINTS.SUBFLEET.RestrictedAssetIds);
  }

  /**
    * POST request to update new set of accessible assets and restricted assets
    * @param {Number} userId - The id of user currently in view
    * @param {String} action - "ACCESSIBLE" or "RESTRICTED"
    * @param {Array<Number>} includedAssetIds - List of included asset IDs
    * @param {Array<Number>} excludedAssetIds - List of excluded asset IDs
    *
    * @returns {Boolean}
    */
  async updateSubFleet(userId, action, includedAssetIds, excludedAssetIds) {
    if (UserService.MOVE_ASSETS_TO[action] == null) {
      throw new Error('Unsupported action');
    }
    if (includedAssetIds && excludedAssetIds) {
      throw new Error('Only includedAssetIds or excludedAssetIds may be given, not both');
    }

    const payload = { moveTo: action };
    if (includedAssetIds) {
      _.assign(payload, { includedAssetIds });
    }
    if (excludedAssetIds) {
      _.assign(payload, { excludedAssetIds });
    }

    try {
      await this._Restangular
        .all(
          `${this._AppSettings.API_ENDPOINTS.Users}/${userId}/${this._AppSettings.API_ENDPOINTS.SUBFLEET.SaveSubfleet}`,
      )
        .post(payload);

      return true;
    } catch (error) {
      return false;
    }
  }

  unsetCurrentUser() {
    this._$cacheFactory.get('$http').removeAll();
    this._currentUserPromise = null;
    this._FlagService.clearCaches();
  }

  async getCurrentUser() {
    if (this._currentUserPromise == null) {
      // Wait on for csrf initialization to finish
      this._currentUserPromise = this.initializeSession()
        .then(() =>
          this._Restangular
            .one(this._AppSettings.API_ENDPOINTS.CurrentUser)
            .get()
            .then((currentUser) => {
              if (_.has(currentUser, 'eors')) {
                // Provide AssetModel with EOR assets so that it can check on the fly
                AssetModel.EOR_ASSETS = _.cloneDeep(currentUser.eors);
              }

              const user = Reflect.construct(UserModel, [currentUser]);
              if (window.trackJs) {
                window.trackJs.configure({ userId: user.userId });
              }
              if (window.classDirect) {
                const userObj = {
                  userId: user.userId,
                  firstName: user.firstName,
                  lastName: user.lastName,
                };
                // IOS
                if (_.get(window, 'webkit.messageHandlers.userLogin', null)) {
                  window.webkit.messageHandlers.userLogin.postMessage(userObj);
                  // Android
                } else if (_.get(window, 'classDirectAndroid.onUserLogin', null)) {
                  const base64StringData = window.btoa(angular.toJson(userObj));
                  window.classDirectAndroid.onUserLogin(base64StringData);
                }
              }
              return user;
            }),
      );
    }

    return this._currentUserPromise;
  }

  /**
 * Retrieves the list of possible account statuses and their
 * corresponding IDs.
 *
 * @returns {Array}
 */
  async getAccountStatuses() {
    if (this._accountStatuses == null) {
      this._accountStatuses = await this._Restangular
        .all(this._AppSettings.API_ENDPOINTS.USERS.AccountStatuses)
        .getList();
    }

    return _.cloneDeep(this._accountStatuses);
  }


  /**
   * Create user based on account type
   */
  createUser(url, payLoad) {
    return this._Restangular.all(url).customPOST(payLoad);
  }

  /**
   * Update single user EOR
   * @param {number|string} userId The ID of the user to edit.
   * @param {object} payload
   */
  async updateUserEor(userId, payload) {
    await this._Restangular
      .all(
        `${this._AppSettings.API_ENDPOINTS.Users}/${userId}/${this._AppSettings.API_ENDPOINTS.Eor}`
      )
      .customPUT(payload);
  }

  /**
   * Delete EOR from user.
   *
   * @param {string} userId The user id.
   * @param {number} imoNumber The asset IMO number.
   */
  async deleteUserEor(userId, imoNumber) {
    await this._Restangular
      .one(this._AppSettings.API_ENDPOINTS.Users, userId)
      .customDELETE(
        `${this._AppSettings.API_ENDPOINTS.Eor}/${imoNumber}`,
    );
  }

  /**
   * Edits the user details.
   *
   * @param {number|string} userId The ID of the user to edit.
   * @param {object} payload
   * @param {Array<string>} fieldsToDelete List of fields to delete.
   */
  async editUser(userId, payload, fieldsToDelete = []) {
    const sanitizedPayload = _.pick(payload, this._AppSettings.EDITABLE_USER_PROPERTIES);
    return await this._Restangular
      .one(this._AppSettings.API_ENDPOINTS.Users, userId)
      .customPUT(sanitizedPayload, null, {
        deleteFields: fieldsToDelete.join(','),
      });
  }

  /**
   * PUT request to update a single user's EOR expiry date.
   *
   * @param (String|number) userId The user ID to update.
   * @param (Number) imoNumber The asset IMO ID to update.
   * @param (Date) expiryDate The new Expiry date.
   */
  async updateEorExpiryDate(userId, imoNumber, expiryDate) {
    const payload = {
      eor: [{
        imoNumber,
        assetExpiry: dateHelper.formatDateServer(expiryDate),
      }],
    };
    try {
      await this._Restangular
        .all(
          `${this._AppSettings.API_ENDPOINTS.Users}/${userId}/${this._AppSettings.API_ENDPOINTS.Eor}`
        )
        .customPUT(payload);
      return true;
    } catch (e) {
      return false;
    }
  }

  /**
   * POST request to get a token for masquerading
   *
   * @param (String|number) userId. The user ID of the user to masquerade.
   * Expected reponse (Object: { response: 'theToken' })
   */
  async getMasqueradeToken(userId) {
    const endPoint = this._AppSettings.API_ENDPOINTS.SECURITY;

    return await this._Restangular
      .all(`${endPoint.ROOT}/${endPoint.CREATE_MASQUERADE_TOKEN}`)
      .post({ userId });
  }

  /**
   * Subfleet actions.
   */
  static MOVE_ASSETS_TO = {
    ACCESSIBLE: 'ACCESSIBLE',
    RESTRICTED: 'RESTRICTED',
  }
}
