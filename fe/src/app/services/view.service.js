import * as _ from 'lodash';
import BaseService from './base.service';

// Load icons from filesystem and put into icon map
const templateMap = {};
const req = require.context('src/app/common/riders/templates', false, /^(.*\.(pug$))[^.]*$/i);
req.keys().forEach((key) => {
  // get the template filename
  const templateName = _.camelCase(key.match(/\.\/(\w+)/)[1]);
  templateMap[templateName] = req(key);
});

export default class ViewService extends BaseService {
  /* @ngInject */
  constructor(
    $compile,
    AppSettings,
  ) {
    super(arguments);
    this._isServiceScheduleOnDetailMode = false;
    this._isAssetHeaderVisible = true;
    this._isCollapsedAssetHeaderExpandable = true;
    this._serviceScheduleViewType = {};
    this._isExpandableCardAccordion = false;
    this._scrollPos = {};
    this._vesselListViewType = this._AppSettings.VIEW_TYPES.TABLE;
    this._isSurveyScheduleViewToggled = false;

    this.templateCache = new Map();
    _.forEach(templateMap,
      (templateContent, templateLocation) => {
        this.templateCache.set(templateLocation, this._$compile(templateContent));
      }
    );
  }

    /**
   * The scrolling position for each states. Return object of
   * { state.name: pageYoffset }
   *
   * @returns {object}
   */
  get scrollPos() {
    return this._scrollPos;
  }

      /**
   * Setter for scrollPos. Each page scrolling position will be saved
   * and use by keepScrollPos directive
   *
   * @param {object}
   */
  set scrollPos(value) {
    this._scrollPos = value;
  }

  /**
   * Whether or not the app showing ss detail
   *
   * @returns {boolean}
   */
  get isServiceScheduleOnDetailMode() {
    return this._isServiceScheduleOnDetailMode;
  }

  /**
   * setter for isServiceScheduleOnDetailMode
   *
   * @param {boolean} boolean
   */
  set isServiceScheduleOnDetailMode(boolean) {
    this._isServiceScheduleOnDetailMode = boolean;
  }

  get isAssetHeaderVisible() {
    return this._isAssetHeaderVisible;
  }

  set isAssetHeaderVisible(boolean) {
    this._isAssetHeaderVisible = boolean;
  }

  get isCollapsedAssetHeaderExpandable() {
    return this._isCollapsedAssetHeaderExpandable;
  }

  set isCollapsedAssetHeaderExpandable(boolean) {
    this._isCollapsedAssetHeaderExpandable = boolean;
  }

  getServiceScheduleViewType(location) {
    return this._serviceScheduleViewType[location];
  }

  setServiceScheduleViewType(obj) {
    if (obj.location && obj.type) {
      this._serviceScheduleViewType[obj.location] = obj.type;
    }
  }

  getVesselListViewType() {
    return this._vesselListViewType;
  }

  setVesselListViewType(type) {
    this._vesselListViewType = type;
  }

  dismissServiceScheduleDetailedMode() {
    this.isServiceScheduleOnDetailMode = false;
    this.isAssetHeaderVisible = true;
    this.isCollapsedAssetHeaderExpandable = true;
  }

  showServiceScheduleDetailedMode() {
    this.isServiceScheduleOnDetailMode = true;
    this.isAssetHeaderVisible = false;
    this.isCollapsedAssetHeaderExpandable = false;
  }

  registerTemplate(location, templateContent) {
  }

  getRegisteredTemplate(location) {
    return this.templateCache.get(_.camelCase(location));
  }

  get isExpandableCardAccordion() {
    return this._isExpandableCardAccordion;
  }

  set isExpandableCardAccordion(bool) {
    this._isExpandableCardAccordion = !!bool;
  }

  get asset() {
    return this._asset;
  }

  set asset(asset) {
    this._asset = asset;
  }

  get focusedTableHeaderId() {
    if (!this._focusedTableHeaderId) {
      this._focusedTableHeaderId = this._AppSettings.TABLE_HEADER_SORT_AND_IDS.ASSET_NAME;
    }
    return this._focusedTableHeaderId;
  }

  set focusedTableHeaderId(param) {
    this._focusedTableHeaderId = param;
  }

  get isSurveyScheduleViewToggled() {
    return this._isSurveyScheduleViewToggled;
  }

  set isSurveyScheduleViewToggled(val) {
    this._isSurveyScheduleViewToggled = val;
  }
}
