import * as _ from 'lodash';
import Asset from 'app/common/models/asset/asset';
import BaseService from './base.service';
import { cacheable } from 'app/common/decorators';
import CertificateModel from 'app/common/models/certificate/';

export default class CertificateService extends BaseService {
  /* @ngInject */
  constructor(
    Restangular,
    AppSettings,
  ) {
    super(arguments);
  }

  @cacheable
  async getCertificates(assetId, options = {}) {
    if (!Asset.isLRAsset(assetId)) {
      return null;
    }

    if (_.isString(assetId)) {
      assetId = Number(assetId.replace(this._AppSettings.ASSET_CODE_LR, '')); // eslint-disable-line no-param-reassign
    }

    const certificates = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.Certificates)
      .post({ assetId }, options);

    _.forEach(certificates, (certificateObject, index) => {
      certificates[index] = Reflect.construct(CertificateModel, [certificateObject]);
    });

    return certificates;
  }
}
