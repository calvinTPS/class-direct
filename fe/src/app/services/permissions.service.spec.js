/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import PermissionsService from 'src/app/services/permissions.service';

beforeEach(() => {
  window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  });
});

describe('PermissionsService', () => {
  describe('canEditProfileOrPassword', () => {
    let mockUser, permissionsService;

    beforeEach(() => {
      permissionsService = new PermissionsService(AppSettings);
    });

    it('allows user to access based on config', () => {
      mockUser = { role: 'LR_INTERNAL' };
      expect(permissionsService.canEditProfileOrPassword(mockUser)).toBe(false);
      mockUser = { role: 'LR_ADMIN' };
      expect(permissionsService.canEditProfileOrPassword(mockUser)).toBe(false);
      mockUser = { role: 'LR_SUPPORT' };
      expect(permissionsService.canEditProfileOrPassword(mockUser)).toBe(true);
    });
  });

  describe('canAccessSurveyRequest', () => {
    let mockAsset, mockUser, permissionsService;

    beforeEach(() => {
      permissionsService = new PermissionsService(AppSettings);
    });

    it('allows user to access with correct conditions', () => {
      const accessibleAsset = {
        restricted: false,
        isLRAsset: true,
        isMMSService: false,
        isEORAsset: false,
        hasActiveServices: true,
      };
      const nonLRAsset = {
        restricted: false,
        isLRAsset: false,
        isMMSService: false,
        isEORAsset: false,
        hasActiveServices: false,
      };
      const nonLRAssetWithMMS = {
        restricted: false,
        isLRAsset: false,
        isMMSService: true,
        isEORAsset: false,
        hasActiveServices: true,
      };
      const lrAdmin = { role: 'LR_ADMIN' };
      const lrSupport = { role: 'LR_SUPPORT' };
      expect(permissionsService.canAccessSurveyRequest(
        accessibleAsset, lrAdmin)).toBe(true);
      expect(permissionsService.canAccessSurveyRequest(
        accessibleAsset, lrSupport)).toBe(true);
      expect(permissionsService.canAccessSurveyRequest(
        nonLRAsset, lrSupport)).toBe(false);
      expect(permissionsService.canAccessSurveyRequest(
        nonLRAssetWithMMS, lrSupport)).toBe(true);
    });
  });

  describe('canAccessMPMS', () => {
    let mockAsset, mockUser, permissionsService;

    beforeEach(() => {
      mockUser = {
        role: 'LR_ADMIN',
      };
      mockAsset = {
        pmsApplicable: true,
        isAccessibleLRAsset: true,
        isEORAsset: false,
      };

      permissionsService = new PermissionsService(AppSettings);
    });

    it('disallows MPMS access if asset not pmsApplicable', () => {
      expect(permissionsService.canAccessMPMS(mockAsset, mockUser)).toBe(true);
      mockAsset.pmsApplicable = false;
      expect(permissionsService.canAccessMPMS(mockAsset, mockUser)).toBe(false);
    });

    describe('asset is PMS applicable', () => {
      it('allows user with right roles to access', () => {
        ['LR_ADMIN', 'LR_INTERNAL', 'LR_SUPPORT', 'CLIENT'].forEach((role) => {
          mockUser.role = role;
          expect(permissionsService.canAccessMPMS(mockAsset, mockUser)).toBe(true);
        });

        ['SHIP_BUILDER'].forEach((role) => {
          mockUser.role = role;
          expect(permissionsService.canAccessMPMS(mockAsset, mockUser)).toBe(false);
        });
      });

      describe('with correct role', () => {
        it('disallows MPMS access if EOR asset', () => {
          mockAsset.isEORAsset = true;
          expect(permissionsService.canAccessMPMS(mockAsset, mockUser)).toBe(false);
        });

        it('disallows MPMS access if asset is restricted', () => {
          mockAsset.isAccessibleLRAsset = false;
          expect(permissionsService.canAccessMPMS(mockAsset, mockUser)).toBe(false);
        });
      });
    });
  });

  describe('canViewNotifications', () => {
    let mockUser, permissionsService;

    beforeEach(() => {
      mockUser = {};

      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper value if user can view notifications', () => {
      expect(permissionsService.canViewNotifications()).toBe(false);
      mockUser.role = 'EOR';
      expect(permissionsService.canViewNotifications(mockUser)).toBe(false);

      ['FLAG', 'SHIP_BUILDER', 'LR_SUPPORT', 'CLIENT'].forEach((role) => {
        mockUser.role = role;
        expect(permissionsService.canViewNotifications(mockUser)).toBe(true);
      });
    });
  });

  describe('canAccessCertsAndCodicils', () => {
    let mockAsset, permissionsService;

    beforeEach(() => {
      mockAsset = {
        isAccessibleLRAsset: false,
        isLRAsset: false,
      };

      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper value if user can access certs and codicils feature', () => {
      expect(permissionsService.canAccessCertsAndCodicils(mockAsset)).toBe(true);
      mockAsset.isLRAsset = true;
      mockAsset.isAccessibleLRAsset = true;
      expect(permissionsService.canAccessCertsAndCodicils(mockAsset)).toBe(true);
      mockAsset.isAccessibleLRAsset = false;
      expect(permissionsService.canAccessCertsAndCodicils(mockAsset)).toBe(false);
    });
  });

  describe('canAccessSistersSection', () => {
    let mockUser, permissionsService;
    beforeEach(() => {
      mockUser = {};
      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper value if user can access all sisters feature', () => {
      const accessibleAsset = { restricted: false, isLRAsset: true, isMMSService: false };

      expect(permissionsService.canAccessSistersSection()).toBe(false);
      mockUser.role = 'CLIENT';
      expect(permissionsService.canAccessSistersSection(accessibleAsset, mockUser)).toBe(true);

      ['EOR', 'EQUASIS', 'THETIS'].forEach((role) => {
        mockUser.role = role;
        expect(permissionsService.canAccessSistersSection(accessibleAsset, mockUser))
          .toBe(false);
      });
    });
  });

  describe('canViewAssetDetailInfo', () => {
    let mockUser, mockAsset, permissionsService;
    beforeEach(() => {
      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return the correct value', () => {
      // !user.isEquasisThetis
      [
        PermissionsService.ASSET_DETAIL_INFO.DOC_COMPANY,
        PermissionsService.ASSET_DETAIL_INFO.GROUP_OWNER,
        PermissionsService.ASSET_DETAIL_INFO.OPERATOR,
        PermissionsService.ASSET_DETAIL_INFO.SHIP_MANAGER,
        PermissionsService.ASSET_DETAIL_INFO.TECHNICAL_MANAGER,
        PermissionsService.ASSET_DETAIL_INFO.LEAD_TECHNICAL_SISTER,
      ].forEach((key) => {
        mockUser = { isEquasisThetis: true };
        mockAsset = { isLRAsset: true };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(false);
        mockUser = { isEquasisThetis: false };
        mockAsset = { isLRAsset: true };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(true);
        mockUser = { isEquasisThetis: true };
        mockAsset = { isLRAsset: false };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(false);
        mockUser = { isEquasisThetis: false };
        mockAsset = { isLRAsset: false };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(true);
      });

      // asset.isLRAsset && !user.isEquasisThetis
      [
        PermissionsService.ASSET_DETAIL_INFO.EQUIPMENT_DETAILS,
      ].forEach((key) => {
        mockUser = { isEquasisThetis: true };
        mockAsset = { isLRAsset: true };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(false);
        mockUser = { isEquasisThetis: false };
        mockAsset = { isLRAsset: true };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(true);
        mockUser = { isEquasisThetis: true };
        mockAsset = { isLRAsset: false };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(false);
        mockUser = { isEquasisThetis: false };
        mockAsset = { isLRAsset: false };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(false);
      });

      // asset.isLRAsset && !asset.restricted
      [
        PermissionsService.ASSET_DETAIL_INFO.RULE_SET,
      ].forEach((key) => {
        mockUser = { isEquasisThetis: true };
        mockAsset = { isLRAsset: true, restricted: false };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(true);
        mockUser = { isEquasisThetis: false };
        mockAsset = { isLRAsset: true, restricted: false };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(true);
        mockUser = { isEquasisThetis: true };
        mockAsset = { isLRAsset: false, restricted: true };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(false);
        mockUser = { isEquasisThetis: false };
        mockAsset = { isLRAsset: false, restricted: true };
        expect(permissionsService.canViewAssetDetailInfo(key, mockAsset, mockUser)).toEqual(false);
      });
    });
  });

  describe('canAccessJobs', () => {
    let mockUser, mockAsset, permissionsService;
    beforeEach(() => {
      mockUser = {
        isEquasisThetis: true,
      };
      mockAsset = {
        isAccessibleLRAsset: true,
        isFromMAST: true,
      };

      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper value if user can access jobs feature', () => {
      expect(permissionsService.canAccessJobs(mockAsset, mockUser)).toBe(false);
      mockUser.isEquasisThetis = false;
      expect(permissionsService.canAccessJobs(mockAsset, mockUser)).toBe(true);
    });
  });

  describe('canShowNationalAdminButton', () => {
    let mockUser, permissionsService;

    beforeEach(() => {
      mockUser = {};

      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper value for showing national admin btn', () => {
      expect(permissionsService.canShowNationalAdminButton()).toBe(false);
      mockUser.role = 'CLIENT';
      expect(permissionsService.canShowNationalAdminButton(mockUser)).toBe(true);

      ['EOR', 'EQUASIS', 'THETIS'].forEach((role) => {
        mockUser.role = role;
        expect(permissionsService.canShowNationalAdminButton(mockUser)).toBe(false);
      });
    });
  });

  describe('canFavouriteAsset', () => {
    let mockUser, permissionsService;

    beforeEach(() => {
      mockUser = {};

      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper value for showing favourite button', () => {
      mockUser.role = 'CLIENT';
      expect(permissionsService.canFavouriteAsset(mockUser)).toBe(true);

      ['EOR', 'EQUASIS', 'THETIS'].forEach((role) => {
        mockUser.role = role;
        expect(permissionsService.canFavouriteAsset(mockUser)).toBe(false);
      });
    });
  });

  describe('canPrintExport', () => {
    let mockUser, permissionsService;
    beforeEach(() => {
      mockUser = {
        isEquasisThetis: false,
      };

      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper value if user can export asset', () => {
      expect(permissionsService.canPrintExport()).toBe(false);
      expect(permissionsService.canPrintExport(mockUser)).toBe(true);
      mockUser.isEquasisThetis = true;
      expect(permissionsService.canPrintExport(mockUser)).toBe(false);
    });
  });

  describe('canViewChecklistTasklist', () => {
    it('should return proper value if user can view checklist or tasklist', () => {
      const permissionsService = new PermissionsService(AppSettings);

      // User is undefined
      expect(permissionsService.canViewChecklistTasklist()).toBe(false);

      const mockUser = {};
      expect(permissionsService.canViewChecklistTasklist(mockUser)).toBe(false);

      mockUser.isEquasisThetis = false;
      expect(permissionsService.canViewChecklistTasklist(mockUser)).toBe(true);

      mockUser.isEquasisThetis = true;
      expect(permissionsService.canViewChecklistTasklist(mockUser)).toBe(false);
    });
  });

  describe('canAccessMNCN', () => {
    let mockUser, mockAsset, permissionsService;
    beforeEach(() => {
      mockUser = { isLRInternal: true, role: 'LR_INTERNAL' };
      mockAsset = { id: 1 };

      permissionsService = new PermissionsService(AppSettings);
    });

    it('should return proper boolean if user can view MNCN', () => {
      expect(permissionsService.canAccessMNCN()).toBe(false);
      expect(permissionsService.canAccessMNCN(mockUser, mockAsset)).toBe(true);

      mockUser = { isFlagUser: true };
      expect(permissionsService.canAccessMNCN(mockUser, mockAsset)).toBe(false);

      mockUser = { isFlagUser: true, flagCode: 'foo', role: 'FLAG' };
      mockAsset = { flag: 'xxx' };
      expect(permissionsService.canAccessMNCN(mockUser, mockAsset)).toBe(false);

      mockAsset = { flagStateDto: {
        flagCode: 'foo' },
      };
      expect(permissionsService.canAccessMNCN(mockUser, mockAsset)).toBe(true);
    });
  });
});
