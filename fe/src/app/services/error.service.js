import Base from 'app/base';
import errorDialogTemplate from 'app/_partials/error-dialog.pug';
import GeneralModalController from 'app/common/dialogs/general-modal.controller';

export default class ErrorService extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $state,
    $window,
    Restangular,
    UserService,
    ) {
    super(arguments);
  }

  showErrorDialog(options = {}) {
    const isAnyDialogShowing = angular.element(this._$document.body).hasClass('md-dialog-is-showing');

    if (isAnyDialogShowing) {
      this._$mdDialog.hide();
    }

    const opts = {
      bindToController: true,
      controller: GeneralModalController,
      controllerAs: 'vm',
      locals: {},
      parent: angular.element(this._$document[0].body),
      template: errorDialogTemplate,
    };

    opts.locals.title = options.title || 'cd-error-dialog-title';
    opts.locals.message = options.message || 'cd-error-dialog-message';
    opts.locals.ariaLabel = options.ariaLabel || options.title || '';
    opts.locals.confirmLabel = options.confirmLabel || 'cd-accept';
    opts.locals.dismissLabel = options.dismissLabel || 'cd-dismiss';

    if (options.description) {
      opts.locals.description = options.description;
    }

    if (options.confirmCallback) {
      opts.locals.confirmCallback = options.confirmCallback;
    }

    if (options.redirectState) {
      opts.locals.confirmCallback = () => {
        this._$mdDialog.hide();
        this._$state.go(options.redirectState);
      };
    }

    if (!options.disableDismiss) {
      opts.locals.dismissCallback = () => {
        this._$mdDialog.hide();
      };
    }

    this._$mdDialog.show(opts);
  }
}
