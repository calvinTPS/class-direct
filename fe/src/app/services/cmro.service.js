import * as _ from 'lodash';
import Base from 'app/base';
import { cacheable } from 'app/common/decorators';

export default class CMROService extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    $base64,
  ) {
    super(arguments);
    this._links = {};
  }

  /**
   * @param  {integer} asset imoNumber
   * Converts the imoNumber to base64 hash and appends to the url
   */
  _encryptURL(links, imoNumber) {
    const encrypted = this._$base64.encode(imoNumber);
    _.forEach(links, (link, key) => {
      link.URL += `/${encrypted}`; // eslint-disable-line
    });
    return links;
  }

  /**
   * @param  {integer} asset imoNumber
   */
  @cacheable
  getLinks(imoNumber) {
    if (imoNumber && _.isEmpty(this._links[imoNumber])) {
      const links = _.cloneDeep(this._AppSettings.CMRO.LINKS);
      this._links[imoNumber] = this._encryptURL(links, imoNumber);
    }
    return _.cloneDeep(this._links[imoNumber]);
  }
}
