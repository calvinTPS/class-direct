import * as _ from 'lodash';
import AssetClassStatusModel from 'app/common/models/asset/asset-class-status/asset-class-status';
import AssetTypeModel from 'app/common/models/asset/asset-type/asset-type';
import BaseService from './base.service';
import CodicilTypeMockData from 'app/common/models/codicil/codicil-type/codicil-type.mocks.json';
import ServiceTypeModel from 'app/common/models/service-type/service-type';
// import CodicilTypeModel from 'app/common/models/codicil/codicil-type/codicil-type';

export default class ReferenceDataService extends BaseService {
  /* @ngInject */
  constructor(
    Restangular,
    AppSettings,
  ) {
    super(arguments);
  }

  async getAssetClassStatuses() {
    if (this._classStatuses == null) {
      const classStatuses = await this._Restangular
        .all(this._AppSettings.API_ENDPOINTS.AssetClassStatuses)
        .getList();

      _.forEach(classStatuses, (statusObj, index) => {
        classStatuses[index] = Reflect.construct(AssetClassStatusModel, [statusObj]);
      });

      this._classStatuses = classStatuses;
    }

    return this._classStatuses;
  }

  async getAssetTypes() {
    if (this._assetTypes == null) {
      const assetTypes = await this._Restangular
        .all(this._AppSettings.API_ENDPOINTS.AssetTypes)
        .getList();

      // Inject parent and children attributes in asset type data object first.
      ReferenceDataService.buildAssetTypeTree(assetTypes);

      _.forEach(assetTypes, (typesObj, index) => {
        assetTypes[index] = Reflect.construct(AssetTypeModel, [typesObj]);
      });

      this._assetTypes = assetTypes;
    }

    return this._assetTypes;
  }

  /**
   * Retrieves the list of asset types at the configured hierarchy level.
   */
  async getAssetTypesAtLevel(level) {
    const allAssetTypes = await this.getAssetTypes();

    return _.filter(allAssetTypes, assetType => assetType.levelIndication === level);
  }

  /**
   * TODO
   *   - Modify with real API later on
   */
  async getCodicilTypes() {
    const codicilTypes = await Promise.resolve(CodicilTypeMockData);

    // Reconstruct model wasn't working on just fetching mock data
    // There is an issue when going into mobile view
    // Will uncomment after real API is there
    // _.forEach(codicilTypes, (statusObj, index) => {
    //   codicilTypes[index] = Reflect.construct(CodicilTypeModel, [statusObj]);
    // });

    return codicilTypes;
  }

  /**
   * Returning list of services
   */
  async getServiceTypes() {
    if (this._serviceTypes == null) {
      const fields = ['id', 'name', 'productCatalogue.name'];
      const serviceTypes = await this._Restangular
        .all(this._AppSettings.API_ENDPOINTS.Services)
        .customGET('', { Fields: fields.join(',') });

      _.forEach(serviceTypes, (obj, index) => {
        serviceTypes[index] = Reflect.construct(ServiceTypeModel, [obj]);
      });

      this._serviceTypes = serviceTypes;
    }

    return this._serviceTypes;
  }

  /**
   * Adds parent and children attributes in each asset type object.
   *
   * @private
   */
  static buildAssetTypeTree(assetTypes) {
    // Generate an index by id
    const assetTypeMapById = _.keyBy(assetTypes, 'id');

    _.forEach(assetTypes, (assetType) => {
      /* eslint-disable no-param-reassign */
      const parent = assetTypeMapById[assetType.parentId];
      if (parent) {
        assetType.parent = parent;
        if (assetType.parent.children == null) assetType.parent.children = [];
        assetType.parent.children.push(assetType);
      }
      /* eslint-enable no-param-reassign */
    });
  }
}
