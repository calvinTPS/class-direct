/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import AssetModel from 'app/common/models/asset/asset';
import AssetService from 'app/services/asset.service.js';
import BaseService from 'app/services/base.service.js';

describe('Base Service', () => {
  let makeService;
  let mockRestangular;
  let mockPromise;
  let mockPermissionsService;
  let mockAssetObject;

  beforeEach(inject((_$rootScope_) => {
    mockAssetObject = {
      $object: {},
    };
    mockPromise = {
      get() {
        return this;
      },
      customGET() {
        return this;
      },
    };
    spyOn(mockPromise, 'get').and.callFake(() => mockAssetObject);
    spyOn(mockPromise, 'customGET').and.returnValue(mockAssetObject);

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };

    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    makeService = () => {
      const service = new BaseService();
      service._AppSettings = AppSettings;
      service._Restangular = mockRestangular;
      return service;
    };
  }));

  describe('BaseService', () => {
    it('getOne(), correctly invokes the correct Restangular methods with proper arguments', () => {
      const baseService = makeService();
      baseService._apiPath = '/API_PATH';
      baseService._modelClass = AssetModel;

      baseService.getOne('LRV123');
      expect(mockRestangular.one).toHaveBeenCalledWith('/API_PATH', 'LRV123');
      expect(mockPromise.get).toHaveBeenCalledWith();
    });

    it('getAll(), correctly invokes the correct Restangular methods with proper arguments', () => {
      const baseService = makeService();
      const options = { page: 1, size: 10, sort: 'sort', order: 'order' };
      baseService._apiPath = '/API_PATH';
      baseService._modelClass = AssetModel;

      baseService.getAll(options);
      expect(mockRestangular.all).toHaveBeenCalledWith('/API_PATH');
      expect(mockPromise.customGET).toHaveBeenCalledWith('', options);
    });
  });
});
