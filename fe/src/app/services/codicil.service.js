import * as _ from 'lodash';
import Asset from 'app/common/models/asset/asset';
import AttachmentModel from 'app/common/models/attachment';
import Base from 'app/base';
import { cacheable } from 'app/common/decorators';
import CodicilModel from 'app/common/models/codicil/codicil';

export default class CodicilService extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    Restangular,
  ) {
    super(arguments);
    this._assetApiPath = AppSettings.API_ENDPOINTS.Assets;
    this.getOne = this.getOne.bind(this);
  }

  /** get a list codicils of an asset
   */
  @cacheable
  async get(assetId, codicilType, params = {}) {
    if (codicilType) {
      if (!Asset.isLRAsset(assetId)) {
        return null;
      }

      const codicils = await this._Restangular.one(this._assetApiPath, assetId)
        .customGET(codicilType, params, this._getCodidicilListPayloadFields(codicilType));

      _.forEach(codicils, (codicil, index) => {
        codicils[index] = Reflect.construct(CodicilModel, [codicil]);
      });
      return codicils;
    }
    return null;
  }

  /** get one codicil of an asset
   */
  @cacheable
  async getOne(assetId, codicilType, codicilId, additionalItems) {
    if (codicilType) {
      if (!Asset.isLRAsset(assetId)) {
        return null;
      }

      const codicil = await this._Restangular.one(this._assetApiPath, assetId)
        .one(codicilType, codicilId).get({}, this._getCodidicilIDetailPayloadFields(codicilType));
      const constructedCodicil = Reflect.construct(CodicilModel, [codicil]);

      _.forEach(additionalItems, (async (functionName, key) => {
        _.set(constructedCodicil, key, await this[functionName](assetId, constructedCodicil));
      }));

      return constructedCodicil;
    }
    return null;
  }

  @cacheable
  async query(assetId, codicilType, params = {}) {
    if (codicilType) {
      if (!Asset.isLRAsset(assetId)) {
        return null;
      }

      const codicils = await this._Restangular.one(this._assetApiPath, assetId)
        .customPOST({}, codicilType, params, { Fields: CodicilModel.PAYLOAD_FIELDS.SF_LIST.join(',') });

      _.forEach(codicils, (codicil, index) => {
        codicils[index] = Reflect.construct(CodicilModel, [codicil]);
      });

      return codicils;
    }
    return null;
  }

  /**
   * Get associated attachements for a MNCN
   */
  @cacheable
  async getMNCNAttachments(assetId, { id }) {
    const attachments = await this._Restangular
      .one(`${this._AppSettings.API_ENDPOINTS.Assets}/${assetId}/${this._AppSettings.API_ENDPOINTS.ASSET.MNCNs}/${id}/${this._AppSettings.API_ENDPOINTS.ASSET.MNCNAttachment}`)
      .get({}, { Fields: CodicilModel.PAYLOAD_FIELDS.MNCN_ATTACHMENT.join(',') });

    _.forEach(attachments, (attachment, index) => {
      _.set(attachments[index], 'attachmentCategoryH.name', this._AppSettings.ATTACHMENTS.MNCN.ShortName);
      attachments[index] = Reflect.construct(AttachmentModel, [attachment]);
    });

    return attachments;
  }

  /**
   * Get associated statutory findings from statutory deficiencies
   */
  @cacheable
  async getStatutoryFindings(assetId, deficiencyId, statFindingsId) {
    if (assetId) {
      const codicil = await this._Restangular
        .one(this._assetApiPath, assetId)
        .one(this._AppSettings.API_ENDPOINTS.ASSET.Deficiencies, deficiencyId)
        .one(this._AppSettings.API_ENDPOINTS.ASSET.Findings, statFindingsId)
        .get({}, { Fields: CodicilModel.PAYLOAD_FIELDS.SF_LIST.concat(CodicilModel.PAYLOAD_FIELDS.SF_DETAIL).join(',') });

      return Reflect.construct(CodicilModel, [codicil]);
    }
    return null;
  }

  _getCodidicilListPayloadFields(codicilType) {
    let fields = {};

    if (codicilType) {
      switch (codicilType) {
        case this._AppSettings.API_ENDPOINTS.ASSET.MNCNs:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.MNCN_LIST.join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.Cocs:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.COC_LIST.join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.ActionableItems:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.AI_LIST.join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.AssetNotes:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.AN_LIST.join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.StatutoryFindings:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.AN_LIST.join(',') };
          break;
        default:
          break;
      }
    }
    return fields;
  }

  _getCodidicilIDetailPayloadFields(codicilType) {
    let fields = {};

    if (codicilType) {
      switch (codicilType) {
        case this._AppSettings.API_ENDPOINTS.ASSET.MNCNs:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.MNCN_LIST.concat(CodicilModel.PAYLOAD_FIELDS.MNCN_DETAIL).join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.Cocs:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.COC_LIST.concat(CodicilModel.PAYLOAD_FIELDS.COC_DETAIL).join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.ActionableItems:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.AI_LIST.concat(CodicilModel.PAYLOAD_FIELDS.AI_DETAIL).join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.AssetNotes:
          fields = { Fields: CodicilModel.PAYLOAD_FIELDS.AN_LIST.concat(CodicilModel.PAYLOAD_FIELDS.AN_DETAIL).join(',') };
          break;
        default:
          break;
      }
    }
    return fields;
  }
}
