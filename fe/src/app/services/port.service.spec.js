/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import Port from 'app/common/models/port';
import PortService from 'src/app/services/port.service';

describe('PortService', () => {
  let mockPortObject,
    mockPromise,
    mockRestangular,
    portService;

  beforeEach(inject((_$rootScope_) => {
    mockPortObject = {
      $object: {},
    };
    mockPromise = {
      get() {
        return this;
      },
      getList() {
        return this;
      },
      customPOST() {
        return this;
      },
      customGET() {
        return this;
      },
      customGETLIST() {
        return this;
      },
      one() {
        return this;
      },
    };
    spyOn(mockPromise, 'get').and.callFake(() => mockPortObject);
    spyOn(mockPromise, 'getList').and.returnValue([mockPortObject]);
    spyOn(mockPromise, 'customPOST').and.returnValue(mockPortObject);
    spyOn(mockPromise, 'customGET').and.returnValue(mockPortObject);
    spyOn(mockPromise, 'customGETLIST').and.returnValue(mockPortObject);
    spyOn(mockPromise, 'one').and.returnValue(mockPortObject);

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };
    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    portService = new PortService(mockRestangular, AppSettings);
  }));

  describe('Port service', () => {
    it.async('calls Restangular.one to get single port', async() => {
      const port = await portService.getOne(123);
      expect(mockRestangular.all).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.Ports).not.toBeUndefined();
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Ports, 123);
      expect(mockPromise.get).toHaveBeenCalledWith();
      expect(port instanceof Port).toBe(true);
    });

    it.async('calls Restangular.all to get list of ports', async() => {
      const ports = await portService.getAll();
      expect(mockRestangular.one).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.Ports).not.toBeUndefined();
      expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Ports);
      expect(mockPromise.getList).toHaveBeenCalledWith();
      expect(ports instanceof Array).toBe(true);
    });

    it('searches port by calling search API with search key', () => {
      mockRestangular.all.calls.reset();
      mockRestangular.one.calls.reset();
      mockPromise.customGETLIST.calls.reset();
      mockPromise.customPOST.calls.reset();
      portService.search('ABC');
      expect(mockRestangular.one).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.Ports).not.toBeUndefined();
      expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Ports);
      expect(mockPromise.customGETLIST).not.toHaveBeenCalled();
      expect(mockPromise.customPOST).toHaveBeenCalledWith(jasmine.objectContaining({
        search: 'ABC',
      }), 'query');

      mockRestangular.all.calls.reset();
      mockRestangular.one.calls.reset();
      mockPromise.customGETLIST.calls.reset();
      mockPromise.customPOST.calls.reset();
      portService.search('I am fool');
      expect(mockRestangular.one).not.toHaveBeenCalled();
      expect(AppSettings.API_ENDPOINTS.Ports).not.toBeUndefined();
      expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Ports);
      expect(mockPromise.customGETLIST).not.toHaveBeenCalled();
      expect(mockPromise.customPOST).toHaveBeenCalledWith(jasmine.objectContaining({
        search: 'I am fool',
      }), 'query');
    });

    it('retrieve port SDO', () => {
      mockRestangular.one.calls.reset();
      mockPromise.customGET.calls.reset();
      portService.getPortSDO(123);
      expect(AppSettings.API_ENDPOINTS.Ports).not.toBeUndefined();
      expect(mockRestangular.one).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Ports, 123);
      expect(mockPromise.customGET).toHaveBeenCalledWith('sdo');
    });

    it('can call searchWithJsonDB', () => {
      mockRestangular.all.calls.reset();
      mockPromise.getList.calls.reset();
      portService.searchWithJsonDB('I am fool');
      expect(mockRestangular.all)
        .toHaveBeenCalledWith('port?name_like=I am fool&_limit=5');
    });
  });
});
