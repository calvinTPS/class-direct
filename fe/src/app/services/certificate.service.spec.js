/* global inject */
import AppSettings from 'app/config/project-variables.js';
import Asset from 'app/common/models/asset/asset';
import CertificateMocks from 'app/common/models/certificate/certificate.mocks.json';
import CertificateService from 'app/services/certificate.service';

describe('Certificate Service', () => {
  let makeService;
  let mockRestangular;
  let mockPromise;

  beforeEach(inject(() => {
    mockPromise = {
      post() {
        return this;
      },
    };
    spyOn(mockPromise, 'post').and.returnValue(CertificateMocks);

    mockRestangular = {
      name: 'mockRestangular',
      all() {
        return mockPromise;
      },
    };

    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    makeService = () => new CertificateService(mockRestangular, AppSettings);
  }));

  it.async('getCertificates() should get list of certificates based on assetId', async () => {
    const service = makeService();
    service.getCertificates('LRV101');
    expect(mockRestangular.all).toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Certificates);
    expect(mockRestangular.all().post).toHaveBeenCalledWith({ assetId: 101 }, {});
  });

  it.async('getCertificates() calls the api with the correct arguments', async () => {
    const service = makeService();
    service.getCertificates('LRV101', { size: 4, sort: 'issueDate' });
    expect(mockRestangular.all().post).toHaveBeenCalledWith({ assetId: 101 }, { size: 4, sort: 'issueDate' });
  });

  it.async('getCertificates() should not get list of certificates if it\'s IHS asset', async () => {
    const service = makeService();
    const originalFn = Asset.isLRAsset;
    Asset.isLRAsset = true;

    service.getCertificates('LRV101');
    Asset.isLRAsset = originalFn; // assign back the original value for other unit tests to use

    expect(mockRestangular.all).not.toHaveBeenCalled();
    expect(mockRestangular.all().post).not.toHaveBeenCalled();
  });
});
