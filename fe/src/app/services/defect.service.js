import * as _ from 'lodash';
import Base from 'app/base';
import { cacheable } from 'app/common/decorators';
import DefectModel from 'app/common/models/defect/';

export default class DefectService extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    Restangular,
  ) {
    super(arguments);
    this._assetApiPath = this._AppSettings.API_ENDPOINTS.Assets;
  }

  /** get one defect of an asset
   */
  @cacheable
  async getOne(assetId, defectType, defectId) {
    if (defectType) {
      const defect = await this._Restangular
        .one(this._assetApiPath, assetId)
        .one(defectType, defectId)
        .get({}, this._getDefectIDetailPayloadFields(defectType));

      return Reflect.construct(DefectModel, [defect]);
    }
    return null;
  }

  @cacheable
  async query(assetId, defectType) {
    if (defectType) {
      const defects = await this._Restangular
        .one(this._assetApiPath, assetId)
        .customPOST({}, defectType, {}, this._getDefectListPayloadFields(defectType));

      _.forEach(defects, (defect, index) => {
        defects[index] = Reflect.construct(DefectModel, [defect]);
      });

      return defects;
    }
    return null;
  }

  _getDefectListPayloadFields(defectType) {
    let fields = {};

    if (defectType) {
      switch (defectType) {
        case this._AppSettings.API_ENDPOINTS.ASSET.StatutoryDeficiencies:
          fields = { Fields: DefectModel.PAYLOAD_FIELDS.SD_LIST.join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.Defects:
          fields = { Fields: DefectModel.PAYLOAD_FIELDS.DEFECT_LIST.join(',') };
          break;
        default:
          break;
      }
    }
    return fields;
  }

  _getDefectIDetailPayloadFields(defectType) {
    let fields = {};

    if (defectType) {
      switch (defectType) {
        case this._AppSettings.API_ENDPOINTS.ASSET.Deficiencies:
          fields = { Fields: DefectModel.PAYLOAD_FIELDS.SD_DETAIL.join(',') };
          break;
        case this._AppSettings.API_ENDPOINTS.ASSET.Defect:
          // TODO - Need to spend time to investigate on the root cause.
          // if it is set to the required fiels, it will break and show the revisions in a loop
          // leave it to be {} for now
          fields = {}; // Fields: DefectModel.PAYLOAD_FIELDS.DEFECT_DETAIL.join(',') };
          break;
        default:
          break;
      }
    }
    return fields;
  }
}
