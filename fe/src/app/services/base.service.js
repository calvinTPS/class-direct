import * as _ from 'lodash';
import assert from 'assert';
import Base from 'app/base';
import { pageable } from 'app/common/decorators';

const ABSTRACT_FUNCTIONS = [
  // none for now as service migration is too hard with these
];

// Really an interface
export default class BaseService extends Base {
  constructor(args) {
    super(args);
    _.forEach(ABSTRACT_FUNCTIONS, (func) => {
      if (!_.isFunction(this[func])) {
        throw new TypeError(
        `Must implement function '${func}' in class ${this.constructor.name}`);
      }
    });

    this._apiPath = '';
    this._modelClass = undefined;
  }

  @pageable
  async getAll(
    { page, size, sort, order } = this._AppSettings.DEFAULT_PARAMS.PAGINATION,
  ) {
    assert(this._modelClass != null);

    this._data = await this._Restangular
      .all(this._apiPath)
      .customGET('', { page, size, sort, order });

    // Convert vanilla JS object to model object.
    // Pagination data is written on the Array object, hence map is not used here
    _.forEach(this._data, (object, index) => {
      this._data[index] = Reflect.construct(this._modelClass, [object]);
    });

    return this._data;
  }

  async getOne(id) {
    assert(this._modelClass != null);

    return Reflect.construct(this._modelClass, [
      await this._Restangular.one(this._apiPath, id).get(),
    ]);
  }
}
