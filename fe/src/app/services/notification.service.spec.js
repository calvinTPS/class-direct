/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import NotificationService from 'src/app/services/notification.service';

describe('NotificationService', () => {
  let mockNotificationTypes,
    mockPromise,
    mockRestangular,
    mockUserService,
    notificationService;

  beforeEach(inject((_$rootScope_) => {
    mockNotificationTypes = [
      {
        id: 1,
        name: 'Asset Listing',
        label: 'asset-listing',
        isEnabled: true,
        isfavouritesOnly: true,
      },
      {
        id: 2,
        name: 'Due Status',
        label: 'due-status',
        isEnabled: false,
        isfavouritesOnly: true,
      },
      {
        id: 3,
        name: 'Job Reports',
        label: 'job-reports',
        isEnabled: true,
        isfavouritesOnly: true,
      },
    ];
    mockPromise = {
      get() {
        return this;
      },
      getList() {
        return this;
      },
      customPOST() {
        return this;
      },
      customGET() {
        return this;
      },
      customGETLIST() {
        return this;
      },
      one() {
        return this;
      },
      customDELETE() {
        return this;
      },
      post() {
        return this;
      },
    };
    spyOn(mockPromise, 'get').and.callFake(() => mockNotificationTypes[0]);
    spyOn(mockPromise, 'getList').and.returnValue(mockNotificationTypes);
    spyOn(mockPromise, 'customPOST').and.returnValue(mockNotificationTypes[0]);
    spyOn(mockPromise, 'customGET').and.returnValue(mockNotificationTypes[0]);
    spyOn(mockPromise, 'customGETLIST').and.returnValue(mockNotificationTypes[0]);
    spyOn(mockPromise, 'customDELETE').and.returnValue(mockNotificationTypes[0]);
    spyOn(mockPromise, 'post').and.returnValue(mockNotificationTypes[0]);

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
      customDELETE() {
        return mockPromise;
      },
      post() {
        return mockPromise;
      },
    };
    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'customDELETE').and.returnValue(mockPromise);

    mockUserService = {
      getCurrentUser: () => Promise.resolve({ id: 1, name: 'testUser' }),
      logout: () => {},
      unsetCurrentUser: () => {},
    };

    notificationService = new NotificationService(AppSettings, mockUserService, mockRestangular);
  }));

  describe('Notification service', () => {
    describe('getAllTypes', () => {
      it.async('gets a list of all notification types using Restangular', async() => {
        const types = await notificationService.getAllTypes();
        expect(mockRestangular.one).not.toHaveBeenCalled();
        expect(AppSettings.API_ENDPOINTS.NotificationTypes).not.toBeUndefined();
        expect(mockRestangular.all)
          .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.NotificationTypes);
        expect(types instanceof Array).toBe(true);
      });
    });

    describe('setNotification', () => {
      it.async('sets notification based on id using Restangular', async() => {
        spyOn(mockUserService, 'unsetCurrentUser');
        const mockNotification = {
          id: 1,
          userOptionChanges: 1,
        };
        const notification = await notificationService.setNotification(mockNotification);
        expect(mockRestangular.one)
          .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Notifications, 1);
        expect(AppSettings.API_ENDPOINTS.Notifications).not.toBeUndefined();
        expect(mockUserService.unsetCurrentUser).toHaveBeenCalledTimes(1);
      });
    });

    describe('removeNotification', () => {
      it.async('deletes notification based on id using Restangular', async() => {
        spyOn(mockUserService, 'unsetCurrentUser');
        const notification = await notificationService.removeNotification(1);
        expect(mockRestangular.one)
          .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.Notifications);
        expect(AppSettings.API_ENDPOINTS.Notifications).not.toBeUndefined();
        expect(mockUserService.unsetCurrentUser).toHaveBeenCalledTimes(1);
      });
    });
  });
});
