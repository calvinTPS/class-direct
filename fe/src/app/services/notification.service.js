import Base from 'app/base';

export default class NotificationService extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    UserService,
    Restangular,
  ) {
    super(arguments);
    this._notificationsApiPath = this._AppSettings.API_ENDPOINTS.Notifications;
    this._typesApiPath = this._AppSettings.API_ENDPOINTS.NotificationTypes;
  }

  /** get a list of notification types
   */
  getAllTypes() {
    if (this._allTypes == null) {
      this._allTypes = this._Restangular
        .all(this._typesApiPath).getList();
    }

    return this._allTypes;
  }

  setNotification(notification) {
    // Notifications sit within current user object. Need updating.
    const id = notification.id;
    const isfavouritesOnly = !!notification.userOptionChanges;
    this._UserService.unsetCurrentUser();

    return this._Restangular
      .one(this._notificationsApiPath, id)
      .customPOST({}, null, { favouritesOnly: isfavouritesOnly }, {});
  }

  removeNotification(notification) {
    // Notifications sit within current user object. Need updating.
    this._UserService.unsetCurrentUser();
    const id = notification.id;

    return this._Restangular
      .one(this._notificationsApiPath)
      .customDELETE(id);
  }
}
