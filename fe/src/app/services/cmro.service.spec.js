/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import CMROService from 'app/services/cmro.service.js';

describe('CMRO Service', () => {
  let makeService;

  const $base64 = {
    encode: (imoNumber) => {
      let encrypted;
      if (imoNumber === 1000019) {
        encrypted = 'ecrypted1000019';
      } else if (imoNumber === 1000021) {
        encrypted = 'ecrypted1000021';
      }
      return encrypted;
    },
  };

  beforeEach(inject((_$rootScope_) => {
    makeService = () => new CMROService(AppSettings, $base64);
  }));

  describe('Methods', () => {
    it('_encryptURL(), corrects ecypts and appends IMO Number ', () => {
      const service = makeService();
      const imoNumber = 1000019;

      const links = {
        link1: {
          NAME: 'One one one',
          URL: 'first-url',
        },
        link2: {
          NAME: 'Two two two',
          URL: 'second-url',
        },
        link3: {
          NAME: 'Three three three',
          URL: 'third-url',
        },
      };

      const encryptedLinks = service._encryptURL(links, imoNumber);
      expect(encryptedLinks.link1.URL).toEqual('first-url/ecrypted1000019');
      expect(encryptedLinks.link2.URL).toEqual('second-url/ecrypted1000019');
      expect(encryptedLinks.link1.NAME).toEqual('One one one');
      expect(encryptedLinks.link2.NAME).toEqual('Two two two');
    });

    it('getLinks(), returns the CMRO links', () => {
      const imoNumber = 1000019;
      const service = makeService();
      const encryptedLinks = service.getLinks(imoNumber);
      expect(_.keys(encryptedLinks).length).toEqual(_.keys(AppSettings.CMRO.LINKS).length);
      expect(encryptedLinks.INCIDENT_SUMMARY.URL).toEqual('/cmro/IncidentSummary/ecrypted1000019');

      const imoNumber2 = 1000021;
      const service2 = makeService();
      const encryptedLinks2 = service.getLinks(imoNumber2);
      expect(encryptedLinks2.INCIDENT_SUMMARY.URL).toEqual('/cmro/IncidentSummary/ecrypted1000021');

      const service3 = makeService();
      const encryptedLinks3 = service3.getLinks();
      expect(encryptedLinks3).toBeUndefined();
    });
  });
});
