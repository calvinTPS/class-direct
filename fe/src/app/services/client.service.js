import * as _ from 'lodash';
import AssetModel from 'app/common/models/asset/asset';
import BaseService from './base.service';
import ClientModel from 'app/common/models/client';

export default class ClientService extends BaseService {
  /* @ngInject */
  constructor(
    Restangular,
    AppSettings,
  ) {
    super(arguments);
  }

  async getAssetClientsByIMONumber(imoNumber) {
    const assetClients = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.Assets)
      .customGET(`${imoNumber}/${this._AppSettings.API_ENDPOINTS.Customers}`);

    _.forEach(assetClients.customers, (obj, index) => {
      assetClients.customers[index] = Reflect.construct(ClientModel, [obj]);
    });

    return Reflect.construct(AssetModel, [assetClients]);
  }

  async getClientsByName(name) {
    const clients = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.Clients)
      .post({ name: `*${name}*`, shipBuilder: false });

    _.forEach(clients, (obj, index) => {
      clients[index] = Reflect.construct(ClientModel, [obj]);
    });

    return clients;
  }

  async getClientsByIMONumber(imoNumber, wildcard = true) {
    imoNumber = wildcard ? `*${imoNumber}*` : imoNumber; // eslint-disable-line no-param-reassign
    const clients = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.Clients)
      .post({ imoNumber, shipBuilder: false });

    _.forEach(clients, (obj, index) => {
      clients[index] = Reflect.construct(ClientModel, [obj]);
    });

    return clients;
  }

  async getShipBuilderByName(name) {
    const shipBuilder = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.Clients)
      .post({ name: `*${name}*`, shipBuilder: true });

    _.forEach(shipBuilder, (obj, index) => {
      shipBuilder[index] = Reflect.construct(ClientModel, [obj]);
    });

    return shipBuilder;
  }

  async getShipBuilderByIMONumber(imoNumber, wildcard = true) {
    imoNumber = wildcard ? `*${imoNumber}*` : imoNumber; // eslint-disable-line no-param-reassign
    const shipBuilder = await this._Restangular
      .all(this._AppSettings.API_ENDPOINTS.Clients)
      .post({ imoNumber, shipBuilder: true });

    _.forEach(shipBuilder, (obj, index) => {
      shipBuilder[index] = Reflect.construct(ClientModel, [obj]);
    });

    return shipBuilder;
  }
}
