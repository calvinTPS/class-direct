/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import TermsAndConditionsService from 'app/services/terms-and-conditions.service';

describe('TermsAndConditionsService', () => {
  let termsAndConditionsService;
  let mockPromise;
  let mockRestangular;
  let mockCookiesProvider;

  const mockTc = {
    tcId: 3,
    tcDesc: 'Title of section',
    tcDate: '2016-12-29',
    tcVersion: 1,
    tcAuthor: 'Lloyds Register',
  };

  beforeEach(inject(() => {
    // Mock Service requests
    mockPromise = {
      customGET: () => {},
      get: () => {},
      getList: () => {},
      customPOST: () => {},
      customPUT: () => {},
      post: () => {},
    };

    // Mock Restangular requests
    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };

    mockCookiesProvider = {
      cookie: {},
      get: key => mockCookiesProvider.cookie[key],
      put: (key, val) => {
        mockCookiesProvider.cookie[key] = val;
      },
    };

    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    termsAndConditionsService =
      new TermsAndConditionsService(mockCookiesProvider, AppSettings, mockRestangular);
  }));

  describe('Constructor default state', () => {
    it('should defined default correct value', () => {
      expect(termsAndConditionsService._apiEndPoints)
        .toEqual(AppSettings.API_ENDPOINTS.AcceptTermsAndConditions);
    });
  });

  describe('TermsAndConditionsService.getLatestTermsAndConditions', () => {
    it.async('should call Restangular.one to get latest TC', async () => {
      spyOn(mockPromise, 'customGET').and.returnValue(mockTc);
      const latestTC = await termsAndConditionsService.getLatestTermsAndConditions();
      expect(mockRestangular.one)
        .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.AcceptTermsAndConditions);
      expect(mockPromise.customGET)
        .toHaveBeenCalled();
      expect(latestTC).toEqual(mockTc);
    });
  });

  describe('TermsAndConditionsService.acceptTermsAndConditions', () => {
    it.async('should call Restangular customPost to update the approved TC', async () => {
      spyOn(mockPromise, 'customPUT').and.returnValue(mockTc);
      const mocktcID = 3;
      const updateTC = await termsAndConditionsService.acceptTermsAndConditions(mocktcID);
      expect(mockRestangular.one)
        .toHaveBeenCalledWith(AppSettings.API_ENDPOINTS.AcceptTermsAndConditions,
          mocktcID);
      expect(mockPromise.customPUT)
        .toHaveBeenCalledWith({}, AppSettings.API_ENDPOINTS.ACCEPT_TERMS_AND_CONDITIONS.Accept);
      expect(updateTC).toEqual(mockTc);
    });
  });

  describe('TnC acceptance for Equasis/Thetis users', () => {
    it('should be able to return true if TnC has been accepted or else return false', () => {
      termsAndConditionsService.setEquasisThetisAcceptedTC(123);
      expect(termsAndConditionsService.hasEquasisThetisAcceptedTC()).toEqual(true);
    });
  });
});
