import applyFilter from './apply-filter';
import convertToFrutiger from './convert-glyphs-to-frutiger.js';
import roleTranslate from './role-translate';
import safeHtml from './safe-html';
import safeTranslate from './safe-translate';
import titleize from './titleize';
import trusted from './trusted';

export default angular
  .module('app.filters', [])
  .filter({
    applyFilter,
    convertToFrutiger,
    roleTranslate,
    safeTranslate,
    titleize,
    trusted,
    safeHtml,
  }).name;
