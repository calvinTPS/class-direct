// Read https://docs.angularjs.org/api/ng/service/$sce
/* @ngInject */
export default $sce =>
  url => $sce.trustAsResourceUrl(url);
