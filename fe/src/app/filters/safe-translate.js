/* @ngInject */
export default $filter => (
   inputText,
   inputOptions
  ) => decodeURIComponent($filter('translate')(
    encodeURIComponent(inputText),
    inputOptions
    )
  );
