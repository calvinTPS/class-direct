import _ from 'lodash';

/**
  * Translate the input string into either long or short version
  * example usage: {{ 'EOR' | roleTranslate }}
  *                &filter('roleTranslate')('EOR');
  * will return the long name based on en-GB.json specified in 'cd-role-eor'
  * which should return 'Exhibition of Records'
  *
  * OR:
  *                {{ 'EOR' | roleTranslate : 'short' }}
  *                $filter('roleTranslate')('EOR', 'short');
  * will return the short name version based on en-GB.jsron specified in 'cd-eor'
  * which should return 'EOR'. BUT if not specified, this filter will return
  * the long version.
  * @param {String} roles - Role name need to be translated
  * @param {Array} roles - Role names need to be translated
  * @param {String} version - 'long' => long version by default if not specified
  *                           'short' => short version
  * @return {String} - The translated role name
  */
/* @ngInject */
export default $filter =>
  (roles, version) => {
    const translate = (input) => {
      const shortName = $filter('translate')(`cd-${_.kebabCase(input)}`);
      const longName = $filter('translate')(`cd-role-${_.kebabCase(input)}`);
      return (version === 'short' && shortName) ? shortName : longName;
    };

    const name = [];
    if (_.isArray(roles)) {
      _.forEach(roles, (role) => {
        name.push(translate(role));
      });
    } else {
      name.push(translate(roles));
    }

    return name.join(', ');
  };
