import _ from 'lodash';

/* @ngInject */
export default AppSettings =>
  (input) => {
    let mutableInput = input;
    if (_.isString(mutableInput)) {
      AppSettings.CONVERT_TO_FRUTIGER
        .forEach((charToReplace) => {
          mutableInput = mutableInput.split(charToReplace)
            .join(`<span class="frutiger">${charToReplace}</span>`);
        });
    }

    return mutableInput;
  };
