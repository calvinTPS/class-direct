/* eslint-disable one-var, no-undef, angular/window-service */
import FiltersModule from './';

describe('The apply filter', () => {
  let $filter,
    capitalizeFilter;

  beforeEach(() => {
    window.module(FiltersModule);
    capitalizeFilter = jasmine.createSpy('capitalize').and.returnValue('SOME STRING');

    window.module(($provide) => {
      $provide.value('capitalizeFilter', capitalizeFilter);
    });

    inject((_$filter_) => {
      $filter = _$filter_;
    });
  });

  it('should call the provided filter and pass the argument to it', () => {
    const result = $filter('applyFilter')('some string', 'capitalize');
    expect(capitalizeFilter).toHaveBeenCalledTimes(1);
    expect(capitalizeFilter).toHaveBeenCalledWith('some string');
    expect(result).toEqual('SOME STRING');
  });
});
