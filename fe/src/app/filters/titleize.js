/* @ngInject */
export default $filter =>
  (input) => {
    if (input) {
      let stringArray = input.split(' ');
      stringArray = stringArray.map(str => str.charAt(0).toUpperCase() + str.slice(1));
      return stringArray.join(' ');
    }
    return input;
  };
