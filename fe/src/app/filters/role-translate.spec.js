/* eslint-disable one-var, no-undef, angular/window-service */
import FiltersModule from './';

describe('The apply filter', () => {
  let $filter,
    translateFilter;

  beforeEach(() => {
    window.module(FiltersModule);
    translateFilter = jasmine.createSpy('translate').and.callFake((roleName) => {
      switch (roleName) {
        case ('cd-role-lr-admin'):
          return 'LR Admin';
        case ('cd-role-eor'):
          return 'Exhibition of Records';
        case ('cd-eor'):
          return 'EOR';
        default:
          return null;
      }
    });

    window.module(($provide) => {
      $provide.value('translateFilter', translateFilter);
    });

    inject((_$filter_) => {
      $filter = _$filter_;
    });
  });

  it('provides translated role, long name version', () => {
    const resultAdmin = $filter('roleTranslate')('LR_ADMIN');

    expect(translateFilter).toHaveBeenCalledWith('cd-role-lr-admin');
    expect(resultAdmin).toEqual('LR Admin');

    const resultEOR = $filter('roleTranslate')('EOR');

    expect(translateFilter).toHaveBeenCalledWith('cd-role-eor');
    expect(resultEOR).toEqual('Exhibition of Records');
  });

  it('provides translated role, short name version', () => {
    const resultAdmin = $filter('roleTranslate')('LR_ADMIN', 'short');

    expect(translateFilter).toHaveBeenCalledWith('cd-role-lr-admin');
    expect(resultAdmin).toEqual('LR Admin');

    const resultEOR = $filter('roleTranslate')('EOR', 'short');

    expect(translateFilter).toHaveBeenCalledWith('cd-eor');
    expect(resultEOR).toEqual('EOR');
  });

  it('provides translated roles, comma separated', () => {
    const longResult = $filter('roleTranslate')(['LR_ADMIN', 'EOR']);
    const shortResult = $filter('roleTranslate')(['LR_ADMIN', 'EOR'], 'short');

    expect(longResult).toEqual('LR Admin, Exhibition of Records');
    expect(shortResult).toEqual('LR Admin, EOR');
  });
});
