/* eslint-disable one-var, no-undef, angular/window-service */
import FiltersModule from './';

describe('The apply filter', () => {
  let $filter;

  beforeEach(() => {
    window.module(FiltersModule);

    inject((_$filter_) => {
      $filter = _$filter_;
    });
  });


  it('provides translated role', () => {
    let result;

    result = $filter('titleize')('i LOVE angularJS');
    expect(result).toEqual('I LOVE AngularJS');

    result = $filter('titleize')('i LOVE-angularJS');
    expect(result).toEqual('I LOVE-angularJS');

    result = $filter('titleize')('i-LOVE-angularJS');
    expect(result).toEqual('I-LOVE-angularJS');

    result = $filter('titleize')('i love 123');
    expect(result).toEqual('I Love 123');

    result = $filter('titleize')(undefined);
    expect(result).toEqual(undefined);
  });
});
