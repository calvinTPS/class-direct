/* eslint-disable one-var, no-undef, angular/window-service */
import AppSettings from 'app/config/project-variables.js';
import FiltersModule from './';

describe('The apply filter', () => {
  let $filter;

  beforeEach(() => {
    window.module(FiltersModule);

    inject((_$filter_) => {
      $filter = _$filter_;
    });
  });


  it('returns the character in wrapped in a span with class "furtiger"', () => {
    AppSettings.CONVERT_TO_FRUTIGER
      .forEach((charToReplace) => {
        const result = $filter('convertToFrutiger')(charToReplace);
        expect(result).toEqual(`<span class="frutiger">${charToReplace}</span>`);
      });
  });
});
