/* @ngInject */
export default $sce =>
  html => $sce.trustAsHtml(html || '-');

