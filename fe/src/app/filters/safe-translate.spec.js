/* eslint-disable one-var, no-undef, angular/window-service */
import * as _ from 'lodash';
import FiltersModule from './';

describe('The apply filter', () => {
  let $filter,
    translateFilter;

  beforeEach(() => {
    window.module(FiltersModule);
    translateFilter = jasmine.createSpy('translate').and.callFake((input, inputValue) => {
      if (!_.isEmpty(inputValue)) {
        switch (input) {
          case ('cd-lr-client'):
            return 'LR Client';
          case ('cd-lr-fleet'):
            return 'LR Fleet';
          default:
            return input;
        }
      }

      switch (input) {
        case ('cd-role-lr-admin'):
          return 'LR Admin';
        default:
          return input;
      }
    });

    window.module(($provide) => {
      $provide.value('translateFilter', translateFilter);
    });

    inject((_$filter_) => {
      $filter = _$filter_;
    });
  });

  it('returns the input as string without evaluating', () => {
    let result = $filter('safeTranslate')('<div>alert(1)</div>', {});
    expect(result).toEqual('<div>alert(1)</div>');

    result = $filter('safeTranslate')('{{2+1}}', {});
    expect(result).toEqual('{{2+1}}');
  });

  it('translates the input according to input value if given', () => {
    const inputValues = {
      'cd-lr-client': 'LR Client',
    };

    const result = $filter('safeTranslate')('cd-lr-client', { inputValues });
    expect(translateFilter).toHaveBeenCalledWith('cd-lr-client', { inputValues });
    expect(result).toEqual('LR Client');

    const result2 = $filter('safeTranslate')('non-existant-value', { inputValues });
    expect(result2).toEqual('non-existant-value');
  });

  it('translates the input according to variable/property value if given', () => {
    const inputValues = {
      'cd-lr-client': 'LR Client',
      'cd-lr-fleet': 'LR Fleet',
    };

    const val1 = 'cd-lr-client';
    const result = $filter('safeTranslate')(val1, { inputValues });
    expect(result).toEqual('LR Client');

    const val2 = 'Some custom value';
    const result2 = $filter('safeTranslate')(val2, { inputValues });
    expect(result2).toEqual('Some custom value');
  });
});
