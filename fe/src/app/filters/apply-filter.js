/* @ngInject */
export default $filter =>
  (input, filterName) => filterName ? $filter(filterName)(input) : input;
