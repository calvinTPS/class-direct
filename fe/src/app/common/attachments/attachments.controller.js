import Base from 'app/base';

export default class AttachmentsController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    AppSettings,
    DownloadService,
    ErrorService,
    ViewService,
  ) {
    super(arguments);
    this.isRetrievingAttachements = false;
  }

  $onInit() {
    this.attachmentItemTpl = this._AppSettings.RIDER_TEMPLATES.ATTACHMENT;
    this.action = {
      icon: 'download',
      clickArea: 'icon',
      ngClick: (attachment) => {
        this.downloadFile(attachment);
      },
    };
  }

  /**
   * Triggered when download icon is clicked.
   *
   * @param {Report|Object} item  A Report object or report-like object which would have attachments
   *                              attribute populated with array of Attachment objects.
   */
  downloadFile(item) {
    // DownloadService handles everything, including showing modal for TM report.
    if (this.isRetrievingAttachements) return;

    this.isRetrievingAttachements = true;

    if (item.isMNCNAttachment) {
      if (item.token) {
        this._DownloadService.downloadFile({
          token: item.token,
          mobileDownload: {
            httpMethod: 'GET',
            fileName: item.name,
          },
        });
        this.isRetrievingAttachements = false;
      } else {
        this.isRetrievingAttachements = false;
        this._ErrorService.showErrorDialog({ message: 'cd-failed-to-retrieve-attachment' });
      }
    } else {
      this._DownloadService.downloadReport(item)
        .then(() => {
          this.isRetrievingAttachements = false;
        }).catch((response) => {
          this.isRetrievingAttachements = false;
          this._ErrorService.showErrorDialog({ message: 'cd-failed-to-retrieve-attachment' });
        });
    }
  }
}
