/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable angular/window-service, no-undef, no-underscore-dangle,
no-unused-vars, one-var-declaration-per-line, one-var, sort-vars */
import angularMaterial from 'angular-material';
import AttachmentsComponent from './attachments.component';
import AttachmentsController from './attachments.controller';
import AttachmentsModule from './';
import AttachmentsTemplate from './attachments.pug';
import mockAppSettings from 'app/config/project-variables.js';
import ReportModel from 'app/common/models/report/';

describe('Attachments', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    makeController;

  beforeEach(window.module(
    AttachmentsModule,
    angularMaterial,
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockDownloadService = {
      downloadFile: () => {},
      downloadReport: () => {},
    };

    const mockErrorService = {
      showErrorDialog: () => {},
    };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('DownloadService', mockDownloadService);
      $provide.value('ErrorService', mockErrorService);
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('attachments', { $dep: dep }, bindings);
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        foo: 'bar',
      });
      expect(controller.foo).toEqual('bar');
    });

    it('attachments getter/setter works as expected', () => {
      const controller = makeController();
      expect(controller._attachments).toBeUndefined();

      controller.attachments = [{
        name: 'Attachment type',
        type: 'Attachment type',
        version: 1,
      }, {
        name: 'Another attachment',
        type: 'Another attachment',
        version: 2,
      }];

      expect(controller.attachments.length).toEqual(2);
    });

    it.async('downloadFile() should call DownloadService.downloadFile report type object', async () => {
      const controller = makeController();
      spyOn(controller._DownloadService, 'downloadReport').and.returnValue({
        then: () => ({
          catch: () => true,
        }),
      });

      const sampleObject = { id: 1, name: 'EG', type: 'EG', version: 2 };

      controller.isRetrievingAttachements = true;
      await controller.downloadFile(sampleObject);
      expect(controller._DownloadService.downloadReport).not.toHaveBeenCalled();

      controller.isRetrievingAttachements = false;
      await controller.downloadFile(sampleObject);
      expect(controller._DownloadService.downloadReport).toHaveBeenCalledWith(sampleObject);
    });

    it.async('downloadFile() should call DownloadService.downloadFile for ESP report', async () => {
      const controller = makeController();
      spyOn(controller._DownloadService, 'downloadReport').and.returnValue({
        then: () => ({
          catch: () => true,
        }),
      });

      const espReport = { isESPReport: true };
      controller.isRetrievingAttachements = true;
      await controller.downloadFile(espReport);
      expect(controller._DownloadService.downloadReport).not.toHaveBeenCalled();

      controller.isRetrievingAttachements = false;
      await controller.downloadFile(espReport);
      expect(controller._DownloadService.downloadReport).toHaveBeenCalledWith(espReport);
    });

    it.async('downloadFile() should call DownloadService.downloadFile for TM report', async () => {
      const controller = makeController();
      const mockReport = { isTMReport: true, type: 'TM', shortName: 'TM', token: '123' };
      spyOn(controller._DownloadService, 'downloadReport').and.returnValue({
        then: () => ({
          catch: () => true,
        }),
      });

      controller.isRetrievingAttachements = true;
      await controller.downloadFile(mockReport);
      expect(controller._DownloadService.downloadReport).not.toHaveBeenCalled();

      controller.isRetrievingAttachements = false;
      await controller.downloadFile(mockReport);
      expect(controller._DownloadService.downloadReport).toHaveBeenCalledWith(mockReport);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AttachmentsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AttachmentsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AttachmentsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<attachments foo="bar" attachments=[]><attachments/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Attachments');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
