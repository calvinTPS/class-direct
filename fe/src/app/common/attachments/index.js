import angular from 'angular';
import AttachmentsComponent from './attachments.component';
import uiRouter from 'angular-ui-router';

export default angular.module('attachments', [
  uiRouter,
])
.component('attachments', AttachmentsComponent)
.name;
