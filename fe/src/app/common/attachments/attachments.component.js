import './attachments.scss';
import controller from './attachments.controller';
import template from './attachments.pug';

export default {
  bindings: {
    attachments: '<',
    // loadAttachments: '&',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
