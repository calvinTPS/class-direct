import Base from 'app/base';

export default class CdAccordionTogglerController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    AppSettings,
    ViewService,
  ) {
    super(arguments);
  }

  onClick() {
    if (this.toggleFunc) {
      this.toggleFunc();
    } else {
      this._ViewService.isExpandableCardAccordion = !!this.shouldExpand;
      this._$rootScope.$broadcast(this._AppSettings.EVENTS.EXPAND_OR_COLLAPSE_ALL_ACCORDIONS,
        !!this.shouldExpand);
    }
  }

  $onDestroy() {
    this._ViewService.isExpandableCardAccordion = false;
  }

  get title() {
    if (this.shouldExpand) {
      return 'cd-expand-all';
    }
    return 'cd-collapse-all';
  }

  get iconName() {
    if (this.shouldExpand) {
      return 'expand';
    }
    return 'collapse';
  }

  get layout() {
    if (this.isLayoutRow) {
      return 'row';
    }
    return 'column';
  }
}
