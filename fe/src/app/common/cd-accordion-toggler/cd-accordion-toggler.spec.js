/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import CdAccordionTogglerComponent from './cd-accordion-toggler.component';
import CdAccordionTogglerController from './cd-accordion-toggler.controller';
import CdAccordionTogglerModule from './';
import CdAccordionTogglerTemplate from './cd-accordion-toggler.pug';

describe('CdAccordionToggler', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(CdAccordionTogglerModule));

  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
    $provide.value('ViewService', { isExpandableCardAccordion: 'foo' });
  }));

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('cdAccordionToggler', { $dep: dep }, bindings);
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('shoulExpand is undefined by default', () => {
      const controller = makeController();
      expect(controller.shouldExpand).toEqual(undefined);

      const controller2 = makeController({ shouldExpand: true });
      expect(controller2.shouldExpand).toEqual(true);
    });

    it('isLayoutRow is undefined by default', () => {
      const controller = makeController();
      expect(controller.isLayoutRow).toEqual(undefined);

      const controller2 = makeController({ isLayoutRow: true });
      expect(controller2.isLayoutRow).toEqual(true);
    });

    it('onClick() should fire the correct event', () => {
      const controller = makeController();
      spyOn(controller._$rootScope, '$broadcast');
      controller.onClick();
      expect(controller._$rootScope.$broadcast).toHaveBeenCalledWith(
        AppSettings.EVENTS.EXPAND_OR_COLLAPSE_ALL_ACCORDIONS,
        false,
      );

      const controller1 = makeController({ toggleFunc: () => {} });
      spyOn(controller1, 'toggleFunc');
      controller1.onClick();
      expect(controller1.toggleFunc).toHaveBeenCalled();
    });

    it('get title() should retrun the correct value', () => {
      const controller = makeController();
      expect(controller.title).toEqual('cd-collapse-all');

      const controller2 = makeController({ shouldExpand: true });
      expect(controller2.title).toEqual('cd-expand-all');
    });

    it('get iconName() should retrun the correct value', () => {
      const controller = makeController();
      expect(controller.iconName).toEqual('collapse');

      const controller2 = makeController({ shouldExpand: true });
      expect(controller2.iconName).toEqual('expand');
    });

    it('get layout() should retrun the correct value', () => {
      const controller = makeController();
      expect(controller.layout).toEqual('column');

      const controller2 = makeController({ isLayoutRow: true });
      expect(controller2.layout).toEqual('row');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdAccordionTogglerComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CdAccordionTogglerTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdAccordionTogglerController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<cd-accordion-toggler foo="bar"><cd-accordion-toggler/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('CdAccordionToggler');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
