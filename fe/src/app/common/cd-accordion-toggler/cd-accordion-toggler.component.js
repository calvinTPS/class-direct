import './cd-accordion-toggler.scss';
import controller from './cd-accordion-toggler.controller';
import template from './cd-accordion-toggler.pug';

export default {
  bindings: {
    shouldExpand: '<?',
    isLayoutRow: '<?',
    toggleFunc: '&?',
  },
  controller,
  controllerAs: 'vm',
  template,
};
