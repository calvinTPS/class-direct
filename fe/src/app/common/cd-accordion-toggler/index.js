import angular from 'angular';
import CdAccordionTogglerComponent from './cd-accordion-toggler.component';

export default angular.module('cdAccordionToggler', [])
.component('cdAccordionToggler', CdAccordionTogglerComponent)
.name;
