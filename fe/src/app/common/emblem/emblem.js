import angular from 'angular';
import iconComponent from './emblem.component';
import uiRouter from 'angular-ui-router';

export default angular.module('emblem', [
  uiRouter,
])
.component('emblem', iconComponent)
.name;
