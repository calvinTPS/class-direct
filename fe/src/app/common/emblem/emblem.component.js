import './emblem.scss';
import controller from './emblem.controller';
import template from './emblem.pug';

export default {
  restrict: 'AE',
  bindings: {
    type: '@',
  },
  template,
  controllerAs: 'vm',
  controller,
};
