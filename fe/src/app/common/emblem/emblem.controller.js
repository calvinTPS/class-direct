/* eslint-disable global-require, import/no-webpack-loader-syntax */
import * as _ from 'lodash';
import Base from 'app/base';

const emblemMap = {};
const req = require.context('../../../../../be/static-file-generator/app/assets/img/emblems', true, /^(.*\.(svg$))[^.]*$/i);
req.keys().forEach((filename) => {
  // get the emblem filename
  // Filenames are then split with a ',''
  // to get an array of stat codes as
  // multiple types have the same emblems
  const emblemData =
    filename.replace('.svg', '')
      .replace('./', '')
      .split('_');
  const emblemStatCodes = emblemData[0].split(',');
  const emblemName = _.capitalize(emblemData[1].replace(/-/g, ' ').trim());

  _.forEach(emblemStatCodes, (code) => {
    emblemMap[code] = {
      url: req(filename),
      name: emblemName,
    };
  });
});

export default class EmblemContoller extends Base {
  /* @ngInject */
  constructor(
    $sce,
  ) {
    super(arguments);
  }

  get emblemUrl() {
    const emblemObj = emblemMap[this._emblemCode()];
    return this._$sce.trustAsHtml(emblemObj ? emblemObj.url : '');
  }

  get emblemName() {
    const emblemObj = emblemMap[this._emblemCode()];
    return emblemObj ? emblemObj.name : '';
  }

  _emblemCode() {
    return _.padEnd(this.type.match(/^.{0,3}/g)[0], 3, '1');
  }
}
