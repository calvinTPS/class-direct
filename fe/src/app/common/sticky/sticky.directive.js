/**
  * Taken from https://github.com/jonathantneal/angular-sticky/blob/master/angular-sticky.js
  * by Jonathan Neal on Github with minor changes like:
  * converting it to ES6 compatible, using export module, added controllerAs vm for
  * scope isolation, used $document and $window, get scroll element as scrollEl,
  * set wrapper class name to 'is-sticky', added condition when sticky shrink for responsive design.
  */

  import * as _ from 'lodash';

  /* @ngInject */
  export default (
    AppSettings,
    $document,
    $rootScope,
    $timeout,
    $window,
  ) => ({
    restrict: 'A',
    controllerAs: 'vm',
    link: (
      $scope,
      element,
      attributes,
      controller,
    ) => {
      // get element
      const stickyEl = element[0];

      // get $document
      $document = stickyEl.ownerDocument; // eslint-disable-line no-param-reassign

      // get $window
      $window = $document.defaultView; // eslint-disable-line no-param-reassign

      // get scroll element
      const scrollEl = angular.element($window);

      // get wrapper
      const wrapper = $document.createElement('div');

      // cache style
      const style = stickyEl.getAttribute('style');

      // get options
      const bottom = parseFloat(attributes.stickyBottom);
      const media = $window.matchMedia(attributes.stickyMedia || 'all');
      const top = parseFloat(attributes.stickyTop);
      // const hasStickyShrink = {}.hasOwnProperty.call(attributes, 'stickyShrink');

      // initialize states
      let activeBottom = false;
      let activeTop = false;
      let offset = {};

      // configure wrapper
      wrapper.className = 'is-sticky';

      // activate sticky
      const activate = () => {
      // get element computed style
        const computedStyle = getComputedStyle(stickyEl);
        const position = activeTop ? `top:${top}` : `bottom:${bottom}`;
        const parentNode = stickyEl.parentNode;
        const nextSibling = stickyEl.nextSibling;

        // replace element with wrapper containing element
        wrapper.appendChild(stickyEl);

        if (parentNode) {
          parentNode.insertBefore(wrapper, nextSibling);
        }
        // style wrapper
        wrapper.setAttribute('style', `
          display:${computedStyle.display};
          height:${stickyEl.offsetHeight}px;
          margin:${computedStyle.margin};
          width:${stickyEl.offsetWidth}px
        `);
        // style element
        // sticky bar disapear on mobile saire and ios app
        // Read https://stackoverflow.com/questions/19325052/disappearing-position-fixed-header-in-ios7-mobile-safari
        // Read https://stackoverflow.com/questions/11100747/css-translation-vs-changing-absolute-positioning-values/18516653#18516653
        stickyEl.setAttribute('style', `
          left:${offset.left}px;
          margin:0;
          position:fixed;
          transition:none;
          transform: translateZ(0);
          -moz-transform: translatez(0);
          -ms-transform: translatez(0);
          -o-transform: translatez(0);
          -webkit-transform: translateZ(0);
          -webkit-font-smoothing: antialiased;
          ${position}px;
          width:${computedStyle.width}
        `);

        // SAFARI MAGIC - easiest hack to enable
        // scrolling when cursor on top of fixed
        // element AND be able to manipulate timescale
        const $stickyEl = angular.element(stickyEl);
        angular.element($stickyEl).bind(
          'mousewheel',
          () => {
            $stickyEl.addClass('allow-events');
            stickyEl.setAttribute('style', `
              pointer-events: none !important;
              ${stickyEl.getAttribute('style')}
            `);
            $timeout(() => {
              $stickyEl.removeClass('allow-events');
              stickyEl.setAttribute('style', _.replace(stickyEl.getAttribute('style'), 'pointer-events: none !important', ''));
            }, 100);
          }
        );

        if (parentNode) {
          angular.element(parentNode).addClass('sticky');
        }
      };

      // deactivate sticky
      const deactivate = () => {
        const parentNode = wrapper.parentNode;
        const nextSibling = wrapper.nextSibling;

        // replace wrapper with element
        parentNode.removeChild(wrapper);
        angular.element(parentNode).removeClass('sticky');
        parentNode.insertBefore(stickyEl, nextSibling);

        // unstyle element
        if (style === null) {
          stickyEl.removeAttribute('style');
        } else {
          stickyEl.setAttribute('style', style);
        }

        // unstyle wrapper
        wrapper.removeAttribute('style');

        activeTop = activeBottom = false;
      };

      // $window scroll listener
      const onscroll = () => {
      // if activated
        if (activeTop || activeBottom) {
          // get wrapper offset
          offset = wrapper.getBoundingClientRect();
          activeBottom =
            !isNaN(bottom) && offset.top > $window.innerHeight - bottom - wrapper.offsetHeight;
          activeTop = !isNaN(top) && offset.top < top;

          // deactivate if wrapper is inside range
          if (!activeTop && !activeBottom) {
            deactivate();
          }
        } else if (media.matches) { // if not activated
          // get element offset
          offset = stickyEl.getBoundingClientRect();
          activeBottom =
            !isNaN(bottom) && offset.top > $window.innerHeight - bottom - stickyEl.offsetHeight;
          activeTop = !isNaN(top) && offset.top < top;
          // activate if element is outside range
          if (activeTop || activeBottom) {
            activate();
          }
        }
      };

      // $window resize listener
      const onresize = () => {
        // conditionally deactivate sticky
        if (activeTop || activeBottom) {
          deactivate();
        }
        // re-initialize sticky
        onscroll();
      };

      // destroy listener
      const ondestroy = () => {
        onresize();

        angular.element(scrollEl).unbind('scroll', onscroll);
        $window.removeEventListener('resize', onresize);
      };

      // bind listeners
      angular.element(scrollEl).bind('scroll', onscroll);
      angular.element(scrollEl).bind('resize', onresize);
      $window.addEventListener('resize', onresize);
      $scope.$on(AppSettings.EVENTS.ACTIVATE_STICKY, onresize);
      $scope.$on('$destroy', ondestroy);

      // eslint-disable-next-line
      $rootScope.$on('wizard:stepChanged', () => {
        $timeout(() => onscroll());
      });
      // initialize sticky
      $timeout(() => onscroll()); // all it needed was this bloody line to reposition cd-actions
    },
  });
