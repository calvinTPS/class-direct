import './sticky.scss';
import angular from 'angular';
import sticky from './sticky.directive';

export default angular.module('sticky', [
])
.directive('sticky', sticky)
.name;
