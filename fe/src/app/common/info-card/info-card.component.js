import './info-card.scss';
import controller from './info-card.controller';
import template from './info-card.pug';

export default {
  bindings: {
    button: '<?',
    description: '@?',
    sections: '<?',
    icon: '<?',
    title: '@',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
