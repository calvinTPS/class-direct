/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import InfoCardComponent from './info-card.component';
import InfoCardController from './info-card.controller';
import InfoCardModule from './';
import InfoCardTemplate from './info-card.pug';

describe('InfoCard', () => {
  let $rootScope,
    $compile,
    makeController;

  const mockTranslateFilter = value => value;
  beforeEach(window.module(
    InfoCardModule,
    { translateFilter: mockTranslateFilter },
    { safeTranslateFilter: mockTranslateFilter },
  ));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('infoCard', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = InfoCardComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(InfoCardTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(InfoCardController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<info-card foo="bar"><info-card/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('InfoCard');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
