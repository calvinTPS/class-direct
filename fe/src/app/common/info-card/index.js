import angular from 'angular';
import infoCardComponent from './info-card.component';
import uiRouter from 'angular-ui-router';

export default angular.module('info-card', [
  uiRouter,
])
.component('infoCard', infoCardComponent)
.name;
