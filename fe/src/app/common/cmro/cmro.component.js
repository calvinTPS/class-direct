import './cmro.scss';
import controller from './cmro.controller';
import template from './cmro.pug';

export default {
  bindings: {
    imoNumber: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
