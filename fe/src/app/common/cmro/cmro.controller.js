import * as _ from 'lodash';
import Base from 'app/base';
import linksModalTemplate from './_partials/cmro-links-modal.pug';

export default class CmroController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $stateParams,
    $window,
    AppSettings,
    CMROService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.links = _.filter(this._CMROService.getLinks(this.imoNumber),
      (link, key) => this.constructor.LINKS_TO_SHOW.indexOf(key) > -1);
  }

  showModal(evt) {
    this._$mdDialog.show({
      bindToController: true,
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true,
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: linksModalTemplate,
    });
  }

  displayCMROInModal(cmroUrl, cmroName) {
    if (!this._$window.cordova) {
      throw new Error('Cannot display in mobile browser in non-mobile platform');
    }

    this._$window.cordova.InAppBrowser.open(this._AppSettings.BASE_URL + cmroUrl, '_blank');
  }

  cancel() {
    this._$mdDialog.cancel();
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }

  static LINKS_TO_SHOW = [
    'INCIDENT_SUMMARY',
    'HULLD_EFECTS',
    'MACHINERY_DEFECTS',
    'FIRES_AND_EXPLOSIONS',
    'FIRES_AND_EXPLOSIONS_SUMMARY',
  ]
}
