import angular from 'angular';
import CMROComponent from './cmro.component';
import uiRouter from 'angular-ui-router';

export default angular.module('cmro', [
  uiRouter,
])
.component('cmro', CMROComponent)
.name;
