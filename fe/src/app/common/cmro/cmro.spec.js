/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import CmroComponent from './cmro.component';
import CmroController from './cmro.controller';
import CmroModule from './';
import CmroTemplate from './cmro.pug';

describe('Cmro', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(CmroModule));

  beforeEach(() => {
    const mockCMROlinks = {
      INCIDENT_SUMMARY: { URL: 'blah' },
      HULLD_EFECTS: { URL: 'blah' },
      MACHINERY_DEFECTS: { URL: 'blah' },
      FIRES_AND_EXPLOSIONS: { URL: 'blah' },
      FIRES_AND_EXPLOSIONS_SUMMARY: { URL: 'blah' },
    };

    const mockCMROService = {
      getLinks: () => mockCMROlinks,
    };

    window.module(($provide) => {
      $provide.value('$mdDialog', {});
      $provide.value('$document', {
        on: () => true,
        off: () => true,
      });
      $provide.value('$mdMedia', {});
      $provide.value('$state', {});
      $provide.value('AppSettings', AppSettings);
      $provide.value('CMROService', mockCMROService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('cmro', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
      expect(controller.links).toBeDefined();
      expect(controller.links.length).toEqual(5);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CmroComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CmroTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CmroController);
    });
  });
});
