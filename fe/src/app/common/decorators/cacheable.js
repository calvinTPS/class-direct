/* eslint-disable no-param-reassign, angular/window-service, angular/document-service */
import TimedCache from 'timed-cache';

const DefaultCacheDuration = 600 * 1000; // 10 mins
const PermanentCacheDuration = 120 * 60 * 1000; // 2 hours

class LRCDTimedCacheManager {
  resetCache() {
    if (this.cache) {
      this.cache.clear();
    }
  }

  getCache() {
    if (this.cache == null) {
      this.cache = new TimedCache({ defaultTtl: DefaultCacheDuration });
    }
    return this.cache;
  }
}

// Global instance of the timed cache manager.
window.LRCDGlobalTimedCacheManager =
  window.LRCDGlobalTimedCacheManager || new LRCDTimedCacheManager();

/**
 * NOTE: All arguments to cacheable function must be JSON serializable.
 *
 * @param {number|boolean} ttlInSeconds Cache TTL in seconds.
 * If true, TTL is semi-permanent (2 hrs).
 */
export default (target, key, descriptor, ...args) => {
  const ttlInSeconds = args[0];
  const fn = descriptor.value;
  const newFn = function newFn() {
    // Cache key of arguments
    const argCacheKey = Array.from(arguments).map(arg => angular.toJson(arg)).join(':');
    // Add class name and method name to cache key
    const cacheKey = `${target.constructor.name}:${key}:${argCacheKey}`;

    const cachedVal = window.LRCDGlobalTimedCacheManager.getCache().get(cacheKey);
    if (cachedVal != null) {
      // Force digest because API isn't actually calling and Angular relies on that
      document.documentElement.click();

      return cachedVal;
    }

    const newVal = fn.apply(this, arguments);

    let ttlInMillis = null;
    if (ttlInSeconds === true) {
      ttlInMillis = PermanentCacheDuration;
    } else if (ttlInSeconds) {
      ttlInMillis = ttlInSeconds * 1000;
    }

    window.LRCDGlobalTimedCacheManager.getCache().put(
      cacheKey,
      newVal,
      ttlInMillis ? { ttl: ttlInMillis } : null
    );
    return newVal;
  };

  descriptor.value = newFn;
  return descriptor;
};
