/* eslint-disable no-param-reassign */

/*
  Provides a nextPage method to collections returned by decorated
  service methods.

  NOTE: Only use on service methods which have query params
  with { page: ? } as the last argument.
*/

import * as _ from 'lodash';

export default (target, key, descriptor) => {
  const method = descriptor.value;

  // NOTE: use function() here instead of lambda as `this` isn't mangled
  descriptor.value = function paged(...args) {
    return Reflect.apply(method, this, args).then((data) => {
      data.nextPage = () => {
        removePageableOrUndefinedFromArgs();

        data.reqParams.page += 1;
        args.push(data.reqParams);

        return Reflect.apply(method, this, args);
      };

      data.refresh = () => {
        removePageableOrUndefinedFromArgs();
        args.push({

          // Don't want to modify the reqParams, just create a similar
          // object with different size and page.
          ...data.reqParams,

          // Total number of elements requested so far.
          size: data.reqParams.size * (data.reqParams.page + 1),
          page: 1,
        });

        return Reflect.apply(method, this, args).then((resp) => {
          data.pagination = resp.pagination;

          // We want to keep the methods on data, so we just replace
          // all of the array elements with the ones from the new response.
          return data.splice(0, data.length, ...resp);
        });
      };

      data.pageable = true;
      return data;

      function removePageableOrUndefinedFromArgs() {
        const pageableObj = _.find(args, arg => arg && arg.page != null);

        // Pop args if last arg is undefined, or if is { page: ? }
        if (_.isUndefined(args[args.length - 1]) || pageableObj) {
          args.pop();
        }
      }
    });
  };

  descriptor.value.pageable = true;

  return descriptor;
};
