import angular from 'angular';
import preventDefault from './prevent-default.directive';

export default angular.module('preventDefault', [
])
.directive('preventDefault', preventDefault)
.name;
