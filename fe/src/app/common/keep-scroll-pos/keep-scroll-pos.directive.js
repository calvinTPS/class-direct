/* eslint-disable no-param-reassign */

/* @ngInject */
export default (
  AppSettings,
  $document,
  $rootScope,
  $timeout,
  $window,
  $location,
  $state,
  $transitions,
  ViewService
) => ({
  restrict: 'A',
  controllerAs: 'vm',
  link: (
    $scope,
    element,
    attributes,
    controller,
  ) => {
    $transitions.onStart({}, (transition) => {
      if ($state.current) {
        ViewService.scrollPos[$state.current.name] = $window.pageYOffset;
      }
    });

    $transitions.onSuccess({}, (transition) => {
      const prevScrollPos = ViewService.scrollPos[$state.current.name] || 0;
      $timeout(() => $window.scrollTo(0, prevScrollPos));
    });
  },
});
