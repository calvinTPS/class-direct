import angular from 'angular';
import keepScrollPos from './keep-scroll-pos.directive';

export default angular.module('keepScrollPos', [
])
.directive('keepScrollPos', keepScrollPos)
.name;
