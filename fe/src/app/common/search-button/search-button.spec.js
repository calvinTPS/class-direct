/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import SearchButtonComponent from './search-button.component';
import SearchButtonController from './search-button.controller';
import SearchButtonModule from './search-button';
import SearchButtonTemplate from './search-button.pug';

describe('SearchButton', () => {
  let makeController;
  let $rootScope;
  let $compile;
  let $controller;

  beforeEach(window.module(SearchButtonModule));


  beforeEach(inject((_$rootScope_, _$compile_, _$controller_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    const $scope = $rootScope.$new();

    makeController = () =>
      $controller(SearchButtonController, {
        $scope,
      });
  }));

  describe('Component', () => {
    const component = SearchButtonComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SearchButtonTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SearchButtonController);
    });
  });

  describe('Rendering', () => {
    let controller;
    let element;
    let scope;

    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element(
        `<search-button data-is-disable='false' data-on-click='foo()'>
        </search-button>`
      );
      element = $compile(element)(scope);
      scope.$apply();

      controller = element.controller('SearchButton');
    });

    it('should render the element correctly', () => {
      expect(element.length).toEqual(1);
    });
  });
});
