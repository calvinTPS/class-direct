import './search-button.scss';
import controller from './search-button.controller';
import template from './search-button.pug';

export default {
  bindings: {
    isDisable: '<',
    onClick: '&?',
  },
  controller,
  controllerAs: 'vm',
  template,
};
