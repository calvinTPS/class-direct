import * as _ from 'lodash';
import Base from 'app/base';

export default class SearchButtonController extends Base {
  /* @ngInject */
  constructor(
  ) {
    super(arguments);
  }

  $onInit() {
    if (!_.isFunction(this.onClick)) {
      throw new Error('Missing search-button click event handler function');
    }
  }
}
