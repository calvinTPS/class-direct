import angular from 'angular';
import searchButtonComponent from './search-button.component';

export default angular.module('searchButtonComponent', [])
.component('searchButton', searchButtonComponent)
.name;
