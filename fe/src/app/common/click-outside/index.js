import angular from 'angular';
import clickOutside from './click-outside.directive';

export default angular.module('clickOutside', [
])
.directive('clickOutside', clickOutside)
.name;
