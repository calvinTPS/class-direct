import * as _ from 'lodash';

/* Instructions

You can also add exceptions via the outside-if-not tag,
which executes the callback function, but only if the id's
or classes listed aren't clicked.

In this case closeThis() will be called only if clicked outside and
#my-button wasn't clicked as well (note .my-button also would be an exception).
This can be great for things like slide in menus that might have a button
outside of the menu scope that triggers it:

  <button id="my-button">Menu Trigger Button</button>
  <div class="menu" id="main-menu" click-outside="closeThis()" outside-if-not="my-button">
      ...
  </div>

*/


/* @ngInject */
export default ($document, $parse, $timeout) => ({
  restrict: 'A',
  link: (
    $scope,
    elem,
    attr,
    controller,
  ) => {
    // postpone linking to next digest to allow for unique id generation
    $timeout(() => {
      const classList = (!_.isUndefined(attr.outsideIfNot)) ? attr.outsideIfNot.split(/[ ,]+/) : [];
      let fn;

      const eventHandler = (e) => {
        let i;
        let element;
        let r;
        let id;
        let classNames;
        let l;

        // check if our element already hidden and abort if so
        if (angular.element(elem).hasClass('ng-hide')) {
          return;
        }

        // if there is no click target, no point going on
        if (!e || !e.target) {
          return;
        }

        // loop through the available elements, looking
        // for classes in the class list that might match and so will eat

        for (element = e.target; element; element = element.parentNode) {
          // check if the element is the same element
          // the directive is attached to and exit if so (props @CosticaPuntaru)
          if (element === elem[0]) {
            return;
          }

          // now we have done the initial checks, start gathering id's and classes
          id = element.id;
          classNames = element.className;
          l = classList.length;

          // Unwrap SVGAnimatedString classes
          if (classNames && !_.isUndefined(classNames.baseVal)) {
            classNames = classNames.baseVal;
          }

          // if there are no class names on the element clicked, skip the check
          if (classNames || id) {
          // loop through the elements id's and classnames looking for exceptions
            for (i = 0; i < l; i++) {
              // prepare regex for class word matching
              r = new RegExp(`\\b${classList[i]}\\b`);

              // check for exact matches on id's or classes,
              // but only if they exist in the first place
              if (
                (!_.isUndefined(id) && id === classList[i]) ||
                (classNames && r.test(classNames))
              ) {
                // now let's exit out as it is an element
                // that has been defined as being ignored for clicking outside
                return;
              }
            }
          }
        }

        // if we have got this far, then we are good
        // to go with processing the command passed in via the click-outside attribute
        $timeout(() => {
          fn = $parse(attr.clickOutside);
          fn($scope);
        });
      };

      /**
       * @description Private function to attempt to figure out if we are on a touch device
       * @private
       **/

      // if the devices has a touchscreen, listen for this event
      const hasTouch = () => 'ontouchstart' in window || navigator.maxTouchPoints;
      if (hasTouch()) {
        $document.on('touchstart', eventHandler);
      }

      // still listen for the click event even if there is touch to cater for touchscreen laptops
      $document.on('click', eventHandler);

      // when the scope is destroyed, clean up the documents event handlers
      // as we don't want it hanging around
      $scope.$on('$destroy', () => {
        if (hasTouch()) {
          $document.off('touchstart', eventHandler);
        }

        $document.off('click', eventHandler);
      });
    });
  },
});
