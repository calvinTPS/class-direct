/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AccordionComponent from './accordion.component';
import AccordionController from './accordion.controller';
import AccordionModule from './accordion';
import AccordionTemplate from './accordion.pug';

describe('Accordion', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(AccordionModule));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('accordion', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('isExpanded is true by default, else follows binding', () => {
      const controller = makeController();
      expect(controller.isExpanded).toEqual(false);

      const controller2 = makeController({ isExpanded: true });
      expect(controller2.isExpanded).toEqual(true);
    });

    describe('accordion is expandable', () => {
      it('toggleExpand toggles isExpanded', () => {
        const controller = makeController({
          isExpanded: false,
        });

        expect(controller.expandable).toBe(true);

        controller.toggleExpand();
        expect(controller.isExpanded).toBe(true);

        controller.toggleExpand();
        expect(controller.isExpanded).toBe(false);
      });

      describe('id is present', () => {
        it('toggleExpand() sets selectedId if it is being expanded', () => {
          const controller = makeController({
            id: 1,
            isExpanded: false,
          });

          controller.toggleExpand();
          expect(controller.selectedId).toEqual(1);
        });

        it('set selectedId will unexpand accordion if id does not match', () => {
          const controller = makeController({
            id: 1,
            isExpanded: false,
          });

          controller.isExpanded = true;
          controller.selectedId = 2;
          expect(controller.isExpanded).toEqual(false);
          expect(controller.selectedId).toEqual(2);
        });
      });
    });

    describe('accordion is not expandable', () => {
      it('toggleExpand has no effect', () => {
        const controller = makeController({
          isExpanded: false,
          expandable: false,
        });

        controller.toggleExpand();
        expect(controller.isExpanded).toBe(false);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AccordionComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AccordionTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AccordionController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<accordion foo="bar"><accordion/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Accordion');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
