import Base from 'app/base';

export default class AccordionController extends Base {
  /* @ngInject */
  constructor(
  ) {
    super(arguments);
  }

  $onInit() {
    // Default to expandable
    this.expandable = this.expandable == null ? true : !!this.expandable;
    // Default to collapsed state.
    this.isExpanded = this.isExpanded == null ? false : !!this.isExpanded;
  }

  /**
   * Get and Set selectedId is meant for collapsing other
   * accordions when one accordion is open.
   *
   * If this behavior is not desired, then don't assign id and selectedId
   * to the accordion.
   */
  get selectedId() {
    return this._selectedId;
  }

  set selectedId(selectedId) {
    if (this.id == null || !this.expandable) {
      // No ID, or not expandable, so don't care.
      return;
    }

    // The current selected ID does not match ID, so close.
    if (this.id !== selectedId) {
      this.isExpanded = false;
    }
    this._selectedId = selectedId;
  }

  toggleExpand() {
    if (!this.expandable) {
      return;
    }

    this.isExpanded = !this.isExpanded;

    // Current accordion is the selected one. Set the selected ID.
    if (this.isExpanded && this.id != null) {
      this.selectedId = this.id;
    }
  }
}
