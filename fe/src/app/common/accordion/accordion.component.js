import './accordion.scss';
import controller from './accordion.controller';
import template from './accordion.pug';

export default {
  bindings: {
    expandable: '<?',
    isExpanded: '<',
    title: '@',
    id: '<?',
    selectedId: '=?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
