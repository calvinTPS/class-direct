import accordionComponent from './accordion.component';
import angular from 'angular';
import uiRouter from 'angular-ui-router';

export default angular.module('accordion', [
  uiRouter,
])
.component('accordion', accordionComponent)
.name;
