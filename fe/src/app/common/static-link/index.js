import angular from 'angular';
import StaticLinkComponent from './static-link.component';
import uiRouter from 'angular-ui-router';

export default angular.module('staticLink', [
  uiRouter,
])
.component('staticLink', StaticLinkComponent)
.name;
