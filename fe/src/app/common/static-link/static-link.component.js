import './static-link.scss';
import controller from './static-link.controller';
import template from './static-link.pug';

export default {
  bindings: {
    altText: '@',
    name: '@',
    text: '@',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
