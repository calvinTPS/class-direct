/* eslint-disable global-require, angular/window-service */
import _ from 'lodash';
import Base from 'app/base';

// Load docs from filesystem and put into doc map
const docMap = {};
const req = require.context('resources/documents', true, /^(.*\.(pdf$))[^.]*$/i);
req.keys().forEach((key) => {
  // get the doc filename
  const docName = _.camelCase(key.match(/\.\/(\w+)/)[1]);
  docMap[docName] = req(key);
});

export default class StaticLinkController extends Base {
  /* @ngInject */
  constructor(
    $filter,
    utils,
  ) {
    super(arguments);
  }

  onClick() {
    if (window.classDirect) {
      const mobileDownload = {
        httpMethod: 'GET',
        fileName: this._$filter('translate')('cd-national-administration-distribution-notes-download'),
        fileUrl: `${window.location.origin}${this.docUrl}`,
        fileExtension: 'pdf',
      };
      // IOS
      if (_.get(window, 'webkit.messageHandlers.download', null)) {
        window.webkit.messageHandlers.download.postMessage(mobileDownload);
        // Android
      } else if (_.get(window, 'classDirectAndroid.downloadFile', null)) {
        const base64StringData = window.btoa(angular.toJson(mobileDownload));
        window.classDirectAndroid.downloadFile(base64StringData);
      }
    } else {
      this._utils.downloadFile(this.docUrl, this.docName, '_blank');
    }
  }

  $onInit() {
  }

  /**
   *
   *
   * @readonly I return the relative URL of this document
   *
   * @memberOf StaticLinkController
   */
  get docUrl() {
    return docMap[this.name];
  }

  get docName() {
    return this._utils.getFileName(this.name, this.docUrl);
  }
}
