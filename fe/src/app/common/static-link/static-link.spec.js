/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import StaticLinkComponent from './static-link.component';
import StaticLinkController from './static-link.controller';
import StaticLinkModule from './';
import StaticLinkTemplate from './static-link.pug';
import utils from 'app/common/utils/utils.factory';

describe('StaticLink', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(StaticLinkModule));
  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('utils', utils());
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('staticLink', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = StaticLinkComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(StaticLinkTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(StaticLinkController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<static-link foo="bar"><static-link/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('StaticLink');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
