import addClearBtn from './add-clear-btn.directive';
import angular from 'angular';

export default angular.module('addClearBtn', [])
.directive('addClearBtn', addClearBtn)
.name;
