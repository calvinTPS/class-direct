import './add-clear-btn.scss';
import template from './add-clear-btn.pug';

/* @ngInject */

export default $compile => ({
  restrict: 'A',
  scope: {
    ngModel: '=',
  },
  require: '?ngModel',
  link: (
    scope,
    element,
    attributes,
    ngModelCtrl,
  ) => {
    const dirTemplate = angular.element(template);
    const compiledTemplate = $compile(dirTemplate)(scope);

    compiledTemplate.bind('click', () => {
      /*
         Set the model into null and render the model view
      */

      ngModelCtrl.$setViewValue(null);
      ngModelCtrl.$render();
    });

    element.parent().append(angular.element(compiledTemplate));
  },
});
