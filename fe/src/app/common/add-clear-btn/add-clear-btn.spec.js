/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AddClearBtnDirective from './add-clear-btn.directive';
import AddClearBtnModule from './add-clear-btn';
import AddClearBtnTemplate from './add-clear-btn.pug';

describe('Add clear button', () => {
  let $log, makeController, element, scope, controller;

  beforeEach(window.module(AddClearBtnModule));
  beforeEach(inject(($rootScope, $compile) => {
    scope = $rootScope.$new();
    element = angular.element(
      '<div class="input"><input type="text" ng-model="model" add-clear-btn="" /></div>'
    );

    element = $compile(element)(scope);

    scope.model = 'This is a test model';
    scope.$apply();
  }));

  it('should clear the model', () => {
    const btn = element[0].querySelectorAll('.clear-btn');

    expect(scope.model).toBeDefined();
    expect(angular.element(btn).hasClass('show')).toBeTruthy();

    angular.element(btn).triggerHandler('click'); // clear the model

    expect(angular.element(btn).hasClass('show')).toBeFalsy();
    expect(scope.model).toBe(null);
  });
});
