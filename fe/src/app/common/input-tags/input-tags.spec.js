/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import 'ui-select';
import AppSettings from 'app/config/project-variables.js';
import InputTagsComponent from './input-tags.component';
import InputTagsController from './input-tags.controller';
import InputTagsModule from './input-tags';
import InputTagsTemplate from './input-tags.pug';
import Key from 'app/constants/key';

describe('Input Tags', () => {
  let $compile, $rootScope, makeController;

  beforeEach(window.module(InputTagsModule, 'ui.select'));
  beforeEach(window.module(($provide) => {
    $provide.value('translateFilter', value => value);
    $provide.value('AppSettings', AppSettings);
    $provide.value('Key', Key);
  }));
  beforeEach(inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    makeController = () => {
      const controller = new InputTagsController(AppSettings);
      controller.$onInit();
      return controller;
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    describe('searchFromBeginningFilter', () => {
      it('searches from the beginning, case insensitively', () => {
        const controller = makeController();
        const fooFilter = controller.searchFromBeginningFilter('foo');
        const ooFilter = controller.searchFromBeginningFilter('oo');

        expect(_.filter([{ name: 'foo' }], fooFilter)).toEqual([{ name: 'foo' }]);
        expect(_.filter([{ name: 'foo' }], ooFilter)).toEqual([]);
        expect(_.filter([{ name: 'Foo' }], fooFilter))
          .toEqual([{ name: 'Foo' }]); // case insensitive
      });
    });

    describe('emailTransform', () => {
      it('detects emails', () => {
        const controller = makeController();
        [
          { email: 'foo@bar', expected: null },
          { email: 'foo@bar.com', expected: 'foo@bar.com' },
        ]
          .forEach((testItem) => {
            expect(controller.emailTransform(testItem.email)).toBe(testItem.expected);
          });
      });
    });
  });

  describe('Rendering', () => {
    let controller,
      scope,
      element;

    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element(
        `<input-tags
           data-is-email="true"
           data-model="model">
        </input-tags>
        `
      );
      element = $compile(element)(scope);
      scope.model = [];
      scope.$apply();

      controller = element.controller(InputTagsModule);
    });

    // Still trying to figure out how to test directives.
    it('renders', () => {
      expect(element.length).toBe(1);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = InputTagsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(InputTagsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(InputTagsController);
    });
  });
});
