/* eslint-disable no-param-reassign */

/**
 * Prevents backspace on empty input box from clearing the tags.
 */
/* @ngInject */
export default Key => ({
  require: 'uiSelect',
  link(
    scope,
    element,
    attributes,
    $select,
  ) {
    const inputEl = element[0].querySelector('.ui-select-search');

    inputEl.addEventListener('keydown', (e) => {
      // If we are pressing backspace when input is empty,
      // prevent choices from being deleted.
      const lockChoices = e.which === Key.BACKSPACE && !inputEl.value;
      $select.isLocked = () => lockChoices;
    }, true);

    element[0].addEventListener('click', () => {
      // Trying to delete by clicking on tag 'x' button should delete it
      $select.isLocked = () => false;
    }, true);
  },
});
