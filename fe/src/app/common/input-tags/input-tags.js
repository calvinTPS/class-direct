import angular from 'angular';
import inputTagsBackspaceNoClearDirective from './input-tags-backspace-no-clear.directive';
import inputTagsComponent from './input-tags.component';
import inputTagsValidationDirective from './input-tags-validations.directive';
import uiRouter from 'angular-ui-router';

export default angular.module('inputTags', [
  uiRouter,
])
.component('inputTags', inputTagsComponent)
.directive('validations', inputTagsValidationDirective)
.directive('uiSelectBackspaceNoClear', inputTagsBackspaceNoClearDirective)
.name;
