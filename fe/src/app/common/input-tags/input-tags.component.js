import './input-tags.scss';
import controller from './input-tags.controller';
import template from './input-tags.pug';

export default {
  restrict: 'AE',
  bindings: {
    autoFocus: '@?',
    backspaceNoClear: '<?',
    closeOnSelect: '<?',
    form: '<',
    isEmail: '@?',
    isRequired: '@?',
    label: '@',
    model: '=',
    name: '@',
    onRemove: '&?',
    options: '<',
    placeholder: '@',
    searchFromBeginningExp: '@?',
    validation: '@?',
  },
  template,
  controllerAs: 'vm',
  controller,
};
