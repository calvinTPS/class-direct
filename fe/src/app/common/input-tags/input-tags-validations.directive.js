/* eslint-disable no-param-reassign, max-len */
import * as _ from 'lodash';

export default () => ({
  require: 'ngModel',
  link(
    scope,
    element,
    attributes,
    ngModel
  ) {
    if (attributes.isRequired) {
      ngModel.$validators.isRequired = (modelValue, viewValue) => {
        let determineVal;
        if (_.isArray(modelValue)) {
          determineVal = modelValue;
        } else if (_.isArray(viewValue)) {
          determineVal = viewValue;
        } else {
          return false;
        }
        return determineVal.length > 0;
      };
    }

    if (attributes.isEmail) {
      const inputEl = angular.element(element[0].querySelector('.ui-select-search'));
      ngModel.$validators.isEmail = () => {
        if (inputEl.val().length) {
          // eslint-disable-next-line no-useless-escape
          const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(inputEl.val());
        }
        return true;
      };

      inputEl.on('keyup', (event) => {
        ngModel.$pristine = false;
        if (event.which === 13) {
          ngModel.$validate();
        } else if (!inputEl.val().length) {
          ngModel.$setValidity('isEmail', true);
        }
      });

      inputEl.on('blur', (event) => {
        ngModel.$setValidity('isEmail', true);
      });
    }
  },
});
