/* eslint-disable global-require, max-len, prefer-template, no-useless-escape */
import Base from 'app/base';

export default class InputTagsController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
  }

  $onInit() {
    this.validation = this.validation || 'disableTransform';
    this.minKeyLength = this._AppSettings.AUTO_COMPLETE.minimumCharacter;
  }

  disableTransform() {
    return false;
  }

  emailTransform(tag) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(tag)) {
      return tag;
    }
    return null;
  }

  searchFromBeginningFilter(s) {
    if (this._searchFromBeginningFilter == null) {
      this._searchFromBeginningFilter = search =>
        val => val.name.toLowerCase().indexOf(search) === 0;
    }
    const searchTerm = s.length ? s.trim().toLowerCase() : '';
    return this._searchFromBeginningFilter(searchTerm);
  }
}
