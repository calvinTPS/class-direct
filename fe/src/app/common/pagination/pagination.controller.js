import * as _ from 'lodash';
import Base from 'app/base';

export default class PaginationController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $rootScope,
    $scope,
    AppSettings,
    DashboardService,
  ) {
    super(arguments);

    if (!_.get(this, 'pageable.pageable')) {
      this._$log.warn('Collection is not pageable');
    }

    this._$scope.$on(this._AppSettings.EVENTS.SCROLL_TO_BOTTOM_PAGE, () => {
      this.loadMore()
        .then(() => {
          // Re-activate/re-evaluate sticky because the position of sticky bottom
          // is missing after new assets loaded.
          this._$rootScope.$broadcast(this._AppSettings.EVENTS.ACTIVATE_STICKY);
        });
    });
  }

  get isFilterApplied() {
    return !this._DashboardService.isUnfilteredQueryExcludingSearch;
  }

  get pagination() {
    return this.pageable.pagination || {};
  }

  get totalLoadedElements() {
    const pagination = this.pagination;
    return pagination.numberOfElements + ((pagination.number - 1) * pagination.size);
  }

  get totalElements() {
    return this.pagination.totalElements;
  }

  get isFirstPage() {
    return this.pagination.first;
  }

  get isLastPage() {
    return this.pagination.last;
  }

  get hasMorePages() {
    return !this.pagination.last;
  }

  async loadMore() {
    if (this.loading || this.disabled) {
      return;
    }

    if (this.pagination.number < this.pagination.totalPages) {
      this.loading = true;

      const resp = await this.pageable.nextPage();

      this.pageable.pagination = resp.pagination;
      this.pageable.splice(this.pageable.length, 0, ...resp);
      this.pageable.updated = true;

      this.loading = false;

      this._$scope.$apply();
    }
  }

  async loadAll() {
    if (!this.pagination.last) {
      this.loading = true;

      const resp = await this.pageable.getAll();

      this.pageable.pagination = resp.pagination;
      this.pageable.splice(this.pageable.length, 0, ...resp);
      this.pageable.updated = true;
      this.loading = false;

      this._$scope.$apply();
    }
  }
}
