/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import PaginationController from './pagination.controller';
import paginationDirective from './pagination.directive';
import PaginationModule from './pagination';
import PaginationTemplate from './pagination.pug';

describe('Pagination', () => {
  let $rootScope;
  let controller;
  let element;
  let scope;
  let mockDashboardService;

  beforeEach(window.module(PaginationModule));

  beforeEach(window.module(($provide) => {
    mockDashboardService = {
      isUnfilteredQueryExcludingSearch: () => false,
    };

    $provide.value('AppSettings', AppSettings);
    $provide.value('DashboardService', mockDashboardService);
  }));

  beforeEach(inject((_$rootScope_, $compile) => {
    // Initialize directive
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
    element = angular.element(
      `<pagination pageable="sample" type="sample" hide-count="false">
        <label ng-repeat="asset in sample.content">{{ asset.name }}</label>
      </pagination>`
    );
    element = $compile(element)(scope);

    // Pass sample data to directive
    scope.sample = {
      pagination: {
        nextPage: () => {},
        id: null,
        size: 15,
        number: 2,
        first: true,
        last: false,
        totalPages: 3,
        totalElements: 15,
        numberOfElements: 5,
        sort: null,
      },
      content: [
        { id: 1, name: 'Ship 1' },
        { id: 2, name: 'Ship 2' },
        { id: 3, name: 'Ship 3' },
        { id: 4, name: 'Ship 4' },
        { id: 5, name: 'Ship 5' },
      ],
    };

    // Instantiate directive
    scope.$apply();

    // Get the controller form the instanciated directive above
    controller = element.controller('pagination');
  }));

  it.async('should listen to event scroll to bottom page and loadMore data', async () => {
    spyOn(controller, 'loadMore').and.returnValue(Promise.resolve());

    $rootScope.$broadcast(AppSettings.EVENTS.SCROLL_TO_BOTTOM_PAGE);
    expect(controller.loadMore).toHaveBeenCalled();

    spyOn($rootScope, '$broadcast');

    await new Promise((resolve) => {
      setTimeout(resolve, 500); // eslint-disable-line angular/timeout-service
    });

    expect($rootScope.$broadcast).toHaveBeenCalledWith(AppSettings.EVENTS.ACTIVATE_STICKY);
  });

  it('should render the right data sample', () => {
    const label = element.find('label');
    for (let i = 0; i < scope.sample.content.length; i++) {
      expect(label[i].innerHTML).toBe(scope.sample.content[i].name);
    }
  });

  describe('Directive controller', () => {
    it('pagination() should', () => {
      expect(controller.pagination).toEqual(scope.sample.pagination);
    });

    it('totalLoadedElements() should', () => {
      expect(controller.totalLoadedElements).toEqual(20);
    });

    it('totalElements() should', () => {
      expect(controller.totalElements).toEqual(scope.sample.pagination.totalElements);
    });

    it('isFirstPage() should', () => {
      expect(controller.isFirstPage).toEqual(scope.sample.pagination.first);
    });

    it('isLastPage() should', () => {
      expect(controller.isLastPage).toEqual(scope.sample.pagination.last);
    });

    it('hasMorePages() should', () => {
      expect(controller.hasMorePages).toEqual(!scope.sample.pagination.last);
    });

    const firstPageable = [
      { id: 1, name: 'Ship 1' },
      { id: 2, name: 'Ship 2' },
      { id: 3, name: 'Ship 3' },
      { id: 4, name: 'Ship 4' },
      { id: 5, name: 'Ship 5' },
    ];
    firstPageable.nextPage = () => angular.noop;
    firstPageable.pagination = {
      id: null,
      size: 5,
      number: 0,
      first: false,
      last: false,
      totalPages: 3,
      totalElements: 15,
      numberOfElements: 5,
      sort: null,
    };

    const secondPageable = [
      { id: 6, name: 'Ship 6' },
      { id: 7, name: 'Ship 7' },
      { id: 8, name: 'Ship 8' },
      { id: 9, name: 'Ship 9' },
      { id: 10, name: 'Ship 10' },
    ];
    secondPageable.nextPage = firstPageable.nextPage;
    secondPageable.pagination = {
      id: null,
      size: 5,
      number: 1,
      first: false,
      last: false,
      totalPages: 3,
      totalElements: 15,
      numberOfElements: 5,
      sort: null,
    };

    it.async('loadMore() should increase the total amount of sample data', async () => {
      // Bind sample to directive's attribute
      controller.pageable = firstPageable;

      spyOn(controller.pageable, 'nextPage').and.returnValue(secondPageable);

      const expectedResult = [
        { id: 1, name: 'Ship 1' },
        { id: 2, name: 'Ship 2' },
        { id: 3, name: 'Ship 3' },
        { id: 4, name: 'Ship 4' },
        { id: 5, name: 'Ship 5' },
        { id: 6, name: 'Ship 6' },
        { id: 7, name: 'Ship 7' },
        { id: 8, name: 'Ship 8' },
        { id: 9, name: 'Ship 9' },
        { id: 10, name: 'Ship 10' },
      ];
      expectedResult.nextPage = controller.pageable.nextPage;
      expectedResult.pagination = {
        id: null,
        size: 5,
        number: 1,
        first: false,
        last: false,
        totalPages: 3,
        totalElements: 15,
        numberOfElements: 5,
        sort: null,
      };
      expectedResult.updated = true;

      // Execute loadMore function
      await controller.loadMore();

      expect(controller.pageable.nextPage).toHaveBeenCalled();
      expect(controller.pageable).toEqual(expectedResult);
    });

    it.async('calling loadMore() multiple times should only load once', async () => {
      // Bind sample to directive's attribute
      controller.pageable = firstPageable;

      spyOn(controller.pageable, 'nextPage').and.returnValue(secondPageable);

      // Execute loadMore function more than once
      controller.loadMore();
      await controller.loadMore();

      expect(controller.pageable.nextPage).toHaveBeenCalledTimes(1);
    });

    it.async('loadAll() should increase the total amount of sample data', async () => {
      // Bind sample to directive's attibute
      controller.pageable = [
        { id: 1, name: 'Ship 1' },
        { id: 2, name: 'Ship 2' },
        { id: 3, name: 'Ship 3' },
        { id: 4, name: 'Ship 4' },
        { id: 5, name: 'Ship 5' },
      ];
      controller.pageable.getAll = () => angular.noop;
      controller.pageable.pagination = {
        id: null,
        size: 5,
        number: 0,
        first: false,
        last: false,
        totalPages: 3,
        totalElements: 15,
        numberOfElements: 5,
        sort: null,
      };

      spyOn(controller.pageable, 'getAll').and.callFake(() => {
        const result = [
          { id: 6, name: 'Ship 6' },
          { id: 7, name: 'Ship 7' },
          { id: 8, name: 'Ship 8' },
          { id: 9, name: 'Ship 9' },
          { id: 10, name: 'Ship 10' },
        ];
        result.getAll = controller.pageable.getAll;
        result.pagination = {
          id: null,
          size: 5,
          number: 1,
          first: false,
          last: false,
          totalPages: 3,
          totalElements: 15,
          numberOfElements: 5,
          sort: null,
        };

        return result;
      });

      const expectedResult = [
        { id: 1, name: 'Ship 1' },
        { id: 2, name: 'Ship 2' },
        { id: 3, name: 'Ship 3' },
        { id: 4, name: 'Ship 4' },
        { id: 5, name: 'Ship 5' },
        { id: 6, name: 'Ship 6' },
        { id: 7, name: 'Ship 7' },
        { id: 8, name: 'Ship 8' },
        { id: 9, name: 'Ship 9' },
        { id: 10, name: 'Ship 10' },
      ];
      expectedResult.getAll = controller.pageable.getAll;
      expectedResult.pagination = {
        id: null,
        size: 5,
        number: 1,
        first: false,
        last: false,
        totalPages: 3,
        totalElements: 15,
        numberOfElements: 5,
        sort: null,
      };
      expectedResult.updated = true;

      // Execute loadAll function
      await controller.loadAll();

      expect(controller.pageable.getAll).toHaveBeenCalled();
      expect(controller.pageable).toEqual(expectedResult);
    });
  });

  describe('Directive', () => {
    // component/directive specs
    const directive = paginationDirective();

    it('includes the intended template', () => {
      expect(directive.template).toEqual(PaginationTemplate);
    });

    it('invokes the right controller', () => {
      expect(directive.controller).toEqual(PaginationController);
    });
  });
});
