import './pagination.scss';
import controller from './pagination.controller';
import template from './pagination.pug';

export default () => ({
  bindToController: {
    hideCount: '=',
    label: '@',
    pageable: '=',
    showSpinner: '<?',
    disabled: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  scope: true,
  template,
  transclude: {
    topButtonControls: '?topButtonControls',
  },
});
