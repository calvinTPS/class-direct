import angular from 'angular';
import paginationDirective from './pagination.directive';

export default angular.module('pagination', [])
.directive('pagination', paginationDirective)
.name;
