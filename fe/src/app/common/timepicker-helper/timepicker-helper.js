import angular from 'angular';
import timepickerHelper from './timepicker-helper.directive';

export default angular.module('timepickerHelper', [])
.directive('timepickerHelper', timepickerHelper)
.name;
