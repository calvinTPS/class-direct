/* @ngInject */

export default $timeout => ({
  restrict: 'A',
  require: 'mdAutocomplete',
  link: (
    scope,
    element,
    attributes,
    mdAutocompleteCtrl,
  ) => {
    $timeout(() => {
      const input = element.find('md-autocomplete-wrap')[0].querySelector('input');
      input.placeholder = attributes.timepickerHelper;

      /**
       * Revalidate input model fix based on the official commit
       *
       * https://github.com/angular/material/commit/399016dc76ea347cf36a9d063bcc5b5894b80f55
       */

      const revalidateInputModel = () => {
        const ngModel = angular.element(input).controller('ngModel');
        const autoCompleteScope = mdAutocompleteCtrl.scope;

        ngModel.$setValidity('md-require-match', !!autoCompleteScope.selectedItem ||
          !autoCompleteScope.searchText
        );
      };

      input.addEventListener('focus', () => {
        revalidateInputModel();
      });

      input.addEventListener('input', () => {
        revalidateInputModel();
      });
    });
  },
});
