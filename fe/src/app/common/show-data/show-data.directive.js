/* @ngInject */

export default (
  $compile,
  ViewService
) => ({
  scope: true,
  link: (
    scope,
    element,
    attributes,
  ) => {
    const templateLocation = attributes.template;

    if (angular.isDefined(templateLocation)) {
      // compile the provided template against the current scope
      const tpl = ViewService.getRegisteredTemplate(templateLocation);
      tpl(scope, (clonedElement) => {
        // stupid way of emptying the element
        element.empty();
        // add the template content
        element.prepend(clonedElement);
      });
    }
  },
});
