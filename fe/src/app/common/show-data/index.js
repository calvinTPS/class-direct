import angular from 'angular';
import showData from './show-data.directive';

export default angular.module('showData', [])
.directive('showData', showData)
.name;
