/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import SpinnerComponent from './spinner.component';
import SpinnerController from './spinner.controller';
import SpinnerModule from './spinner';
import SpinnerTemplate from './spinner.pug';

describe('Spinner', () => {
  let $rootScope, makeController;

  beforeEach(window.module(SpinnerModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => new SpinnerController();
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller.name).toBeDefined();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SpinnerComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SpinnerTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SpinnerController);
    });
  });
});
