import './spinner.scss';
import controller from './spinner.controller';
import template from './spinner.pug';

export default {
  restrict: 'E',
  bindings: {
    message: '@',
    light: '=',
  },
  template,
  controllerAs: 'vm',
  controller,
};
