import angular from 'angular';
import spinnerComponent from './spinner.component';
import uiRouter from 'angular-ui-router';

export default angular.module('spinner', [
  uiRouter,
])
.component('spinner', spinnerComponent)
.name;
