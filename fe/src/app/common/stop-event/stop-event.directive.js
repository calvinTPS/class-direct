/* @ngInject */
// - I am a directive that is used to stop my parent from propogating events,
// - Used in case of actions in nested elements
export default () => ({
  restrict: 'A',
  link(scope, elem, attrs) {
    if (attrs && attrs.stopEvent) {
      elem.on(attrs.stopEvent, (e) => {
        e.stopPropagation();
        e.preventDefault();
      });
    }
  },
});
