import angular from 'angular';
import stopEvent from './stop-event.directive';

export default angular.module('stopEvent', [
])
.directive('stopEvent', stopEvent)
.name;
