import angular from 'angular';
import starComponent from './star.component';

export default angular.module('star', [
])
.component('star', starComponent)
.name;
