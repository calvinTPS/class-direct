/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import StarComponent from './star.component';
import StarController from './star.controller';
import StarModule from './star';
import StarTemplate from './star.pug';

describe('Star', () => {
  let makeController;
  let $rootScope;
  let $compile;
  let $controller;

  const mockAssetService = {
    removeFavourite: () => new Promise((resolve, reject) => resolve()),
    setFavourite: () => new Promise((resolve, reject) => resolve()),
  };

  beforeEach(window.module(StarModule));

  beforeEach(() => {
    window.module(($provide) => {
      const mockTranslateFilter = value => value;

      $provide.value('AssetService', mockAssetService);
      $provide.value('AppSettings', AppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$controller_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    const $scope = $rootScope.$new();

    makeController = () =>
      $controller(StarController, {
        $scope,
      });
  }));

  describe('Component', () => {
    const component = StarComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(StarTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(StarController);
    });
  });

  describe('Toggle Favourite (Private methods)', () => {
    let controller;

    beforeEach(() => {
      controller = makeController();
      controller.isFavourite = undefined;
      controller.onToggle = () => {};
    });

    it('should toggle the isFavourite property', () => {
      controller._toggleFavourite();
      expect(controller.isFavourite).toBeTruthy();

      controller.isFavourite = true;
      controller._toggleFavourite();
      expect(controller.isFavourite).toBeFalsy();

      controller.isFavourite = false;
      controller._toggleFavourite();
      expect(controller.isFavourite).toBeTruthy();
    });
  });

  describe('Toggle Favourite - Server update SUCCESS', () => {
    let controller;

    beforeEach(() => {
      controller = makeController();
      controller.isFavourite = undefined;
      controller.updateServer = true;
      controller.onToggle = () => {};
    });

    it.async('should toggle the isFavourite property from UNDEFINED/FALSE to TRUE',
      async (done) => {
        await controller.toggleFavourite();
        expect(controller.isFavourite).toBeTruthy();
      });

    it.async('should toggle the isFavourite property from TRUE to FALSE', async (done) => {
      controller.isFavourite = true;
      await controller.toggleFavourite();
      expect(controller.isFavourite).toBeFalsy();
    });
  });

  describe('Toggle Favourite - Server update FAIL', () => {
    let controller;

    beforeEach(() => {
      controller = makeController();
      controller.isFavourite = undefined;
      controller.updateServer = true;
      controller.onToggle = () => {};
      controller._AssetService.removeFavourite = () => new Promise((resolve, reject) => reject());
      controller._AssetService.setFavourite = () => new Promise((resolve, reject) => reject());
    });

    it.async('fails to toggle the isFavourite property from UNDEFINED/FALSE to TRUE',
      async (done) => {
        await controller.toggleFavourite();
        expect(controller.isFavourite).toBeFalsy();
      });

    it.async('fails to toggle the isFavourite property from TRUE to FALSE', async (done) => {
      controller.isFavourite = true;
      await controller.toggleFavourite();
      expect(controller.isFavourite).toBeTruthy();
    });
  });

  describe('UI interaction', () => {
    let controller;
    let element;
    let scope;

    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element(
        `<star
          data-is-Favourite="false"
          data-assetId="1">
        </star>`);
      element = $compile(element)(scope);
      scope.$apply();

      controller = element.controller('star');
    });

    it('should render the element correctly', () => {
      expect(element.length).toEqual(1);
    });

    it('calls controller\'s toggleFavourite() when star is clicked', () => {
      const toggleSpy = spyOn(controller, 'toggleFavourite');
      const elm = element[0].querySelectorAll('.star-container');
      expect(elm.length).toEqual(1);

      angular.element(elm).triggerHandler('click');
      expect(toggleSpy).toHaveBeenCalled();
    });
  });
});
