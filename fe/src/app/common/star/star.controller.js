import Base from 'app/base';

export default class StarController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $scope,
    $rootScope,
    AppSettings,
    AssetService,
  ) {
    super(arguments);
  }

  $onInit() {
    // - LRCD-3645 - CR198.C40
    // - To send imo number (IHSXXXXXX) if it exists, else send the standard assetId
    this._favouriteId = (this.assetImo) ? `IHS${this.assetImo}` : this.assetId;

    this._$scope.$on(this._AppSettings.EVENTS.UPDATE_FAVOURITE, (event, payload) => {
      if (payload.assetId === this._favouriteId) {
        this.isFavourite = payload.isFavourite;
      }
    });
  }

  async toggleFavourite(event) {
    if (event) {
      event.stopPropagation();
    }
    const { SET, REMOVE } = StarController.FAV_UPDATE_TYPE;
    const action = this.isFavourite ? REMOVE : SET;

    // Toggle before updating server
    this._toggleFavourite();

    if (this.updateServer) {
      try {
        await this._AssetService[`${action}Favourite`](this._favouriteId);
      } catch (err) {
        // Revert toggle
        this._$log.error(err);
        this._toggleFavourite();
        this._$scope.$apply();
      }
    } else {
      this._$rootScope.$broadcast(this._AppSettings.EVENTS.ON_TOGGLE_FAVOURITE, {
        type: action,
        assetId: this._favouriteId,
      });
    }
  }

  _toggleFavourite() {
    this.isFavourite = !this.isFavourite;
    this.onToggle({ isFavourite: this.isFavourite });
  }

  static FAV_UPDATE_TYPE = {
    SET: 'set',
    REMOVE: 'remove',
  }
}
