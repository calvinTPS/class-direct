import './star.scss';
import controller from './star.controller';
import template from './star.pug';

export default {
  bindings: {
    isFavourite: '<',
    assetId: '<',
    assetImo: '<',
    onToggle: '&',
    updateServer: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
