import angular from 'angular';
import recordSummary from './visible-results.component';

export default angular.module('visibleResults', [
])
.component('visibleResults', recordSummary)
.name;
