/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import VisibleResultsComponent from './visible-results.component';
import VisibleResultsController from './visible-results.controller';
import VisibleResultsModule from './visible-results';

describe('Visible results', () => {
  let $compile, $rootScope, makeController, mockTranslateFilter;

  beforeEach(window.module(VisibleResultsModule));
  beforeEach(() => {
    mockTranslateFilter = value => value;
    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    makeController = () => {
      const controller = new VisibleResultsController();
      controller.$onInit();
      return controller;
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // Make sure the var isn't get defined on the first installation
    it('returned the visible sum of result', () => {
      const controller = makeController();
      expect(controller.visibleCount).not.toBeDefined();
    });

    // Make sure the var isn't get defined on the first installation
    it('returned the total sum of result', () => {
      const controller = makeController();
      expect(controller.visibleCount).not.toBeDefined();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = VisibleResultsComponent;

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(VisibleResultsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element(
        '<visible-results visible-count="vc" total-count="tc" label-key="lk"><visible-results/>');
      element = $compile(element)(scope);

      scope.vc = 15;
      scope.tc = 150;
      scope.lk = 'cd-assets';
      scope.$apply();

      controller = element.controller('VisibleResults');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
