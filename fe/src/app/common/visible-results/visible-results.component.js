import './visible-results.scss';
import controller from './visible-results.controller';
import template from './visible-results.pug';

export default {
  restrict: 'E',
  template,
  bindings: {
    visibleCount: '<',
    totalCount: '<',
    labelKey: '@',
    isFilteredResult: '<?',
  },
  controllerAs: 'vm',
  controller,
};
