import Base from 'app/base';

export default class VisibleResultsController extends Base {
  /* @ngInject */
  constructor(
    $filter,
  ) {
    super(arguments);
  }

  $onInit() {
    // used to translate key value from en-GB.json
    this.label = this.labelKey ? this._$filter('translate')(this.labelKey) : '';
  }
}
