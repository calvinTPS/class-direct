/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var */
import dateHelper from './date-helper';

describe('Date Helper', () => {
  describe('isValid', () => {
    it('treats dates with year < 1000 as invalid', () => {
      expect(dateHelper.isValid('10 Oct 999')).toBe(false);
      expect(dateHelper.isValid('10 Oct 1000')).toBe(true);
    });
  });

  describe('inMonths', () => {
    it('calculates the number of months between two date objects', () => {
      expect(dateHelper.inMonths(new Date('2014'), new Date('2018'))).toEqual(48);
      expect(dateHelper.inMonths(new Date('2017'), new Date('2018'))).toEqual(12);
    });
  });

  describe('getDatePercent', () => {
    it('returns the correct percentage a date is between two other dates', () => {
      expect(dateHelper.getDatePercent(0, 100, 50)).toEqual(50);
      expect(dateHelper.getDatePercent(0, 50, 50)).toEqual(100);
      expect(dateHelper.getDatePercent(new Date('1 Jan 2016'), new Date('31 Dec 2016'), new Date('1 jan 2016'))).toEqual(0);
      expect(dateHelper.getDatePercent(new Date('1 Jan 2016'), new Date('31 Dec 2016'), new Date('31 Dec 2016'))).toEqual(100);
    });
  });

  describe('generatePips', () => {
    it('returns the correct collection', () => {
      const pips = dateHelper.generatePips(new Date('2017'), new Date('2020'));
      expect(pips[0].pipType).toEqual('large');
      expect(pips[0].date.getFullYear()).toEqual(2017);
      expect(pips[0].date.getDate()).toEqual(1);
      expect(pips[0].date.getMonth()).toEqual(0);
    });
  });

  describe('inRange', () => {
    it('includes to and from dates inclusively', () => {
      expect(dateHelper.inRange('2012-01-01', '2012-01-01', '2012-01-03')).toBe(true);
      expect(dateHelper.inRange('2012-01-03', '2012-01-01', '2012-01-03')).toBe(true);
    });

    it('handles string date comparisons, inclusively', () => {
      const testStringDate = '2016-11-13';

      expect(dateHelper.inRange(testStringDate, testStringDate, null)).toBe(true);
      expect(dateHelper.inRange(testStringDate, null, testStringDate)).toBe(true);
      expect(dateHelper.inRange(testStringDate, testStringDate, testStringDate)).toBe(true);

      expect(dateHelper.inRange(testStringDate, '2016-11-12', '2016-11-14')).toBe(true);
      expect(dateHelper.inRange(testStringDate, '2016-11-13', '2016-11-14')).toBe(true);
      expect(dateHelper.inRange(testStringDate, '2016-11-12', '2016-11-13')).toBe(true);
    });

    it('can handle a mixture of UTC midnight string + date objects in midnight local time', () => {
      const startDateWithUtcMidnight = new Date('2016-11-13T00:00+00:00');
      const startDateInMillisWithLocalMidnight =
        startDateWithUtcMidnight.getTime() +
        (startDateWithUtcMidnight.getTimezoneOffset() * 60 * 1000);
      const startDateWithLocalMidnight = new Date(startDateInMillisWithLocalMidnight);
      const testStringDate = '2016-11-13';

      expect(dateHelper.inRange(testStringDate, startDateWithLocalMidnight, null)).toBe(true);
      expect(dateHelper.inRange(testStringDate, null, startDateWithLocalMidnight)).toBe(true);
      expect(dateHelper.inRange(
        testStringDate,
        startDateWithLocalMidnight,
        startDateWithLocalMidnight
      )).toBe(true);
    });
  });

  describe('setTimelineDateRange', () => {
    it('should behaves correctly', () => {
      const comparatorDates = {
        minDate: new Date('2017-01-01'),
        maxDate: new Date('2021-01-15'),
      };

      // [2017-01-01, 2021-01-15]
      let dates = [1483228800000, 1610668800000];
      dateHelper.setTimelineDateRange(dates, comparatorDates);

      // Not manipulating something so returning initial value which is 2017-01-01
      expect(new Date(dates[0]).getDate()).toEqual(1);
      expect(new Date(dates[0]).getMonth()).toEqual(0); // it's an array so start from 0
      expect(new Date(dates[0]).getFullYear()).toEqual(2017);

      // Not manipulating something so returning initial value which is 2021-01-15
      expect(new Date(dates[1]).getDate()).toEqual(15);
      expect(new Date(dates[1]).getMonth()).toEqual(0); // it's an array so start from 0
      expect(new Date(dates[1]).getFullYear()).toEqual(2021);

      // [2017-02-20, 2019-01-25]
      dates = [1487548800000, 1548374400000];
      dateHelper.setTimelineDateRange(dates, comparatorDates);

      // Manipulating something so returning value which is 2017-02-01
      expect(new Date(dates[0]).getDate()).toEqual(1);
      expect(new Date(dates[0]).getMonth()).toEqual(1); // it's an array so start from 0
      expect(new Date(dates[0]).getFullYear()).toEqual(2017);

      // Manipulating something so returning value which is 2019-01-31
      expect(new Date(dates[1]).getDate()).toEqual(31);
      expect(new Date(dates[1]).getMonth()).toEqual(0); // it's an array so start from 0
      expect(new Date(dates[1]).getFullYear()).toEqual(2019);
    });
  });
});
