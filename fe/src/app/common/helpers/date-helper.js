import _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import fecha from 'fecha';

const PIPSIZE = {
  LARGE: 'large',
  MEDIUM: 'medium',
  SMALL: 'small',
};
/**
 * I take a date range (min to max) and return to what percentage
 * the first date is between them
 * @param  {date} min
 * @param  {date} max
 * @param  {date} date
 */
const getDatePercent = (min, max, date) => ((date - min) / (max - min)) * 100;

/**
 * I take in two date objects and tell you how many months apart they are
 * @param  {timestamp or date object} start
 * @param  {timestamp or date object} end
 */
const inMonths = (start, end) => {
  if (_.isNumber(start)) {
    start = new Date(start); // eslint-disable-line no-param-reassign
  }
  if (_.isNumber(end)) {
    end = new Date(end); // eslint-disable-line no-param-reassign
  }
  const d1Y = start.getFullYear();
  const d2Y = end.getFullYear();
  const d1M = start.getMonth();
  const d2M = end.getMonth();
  return ((d2Y * 12) + d2M) - ((d1Y * 12) + d1M);
};

export default {
  inMonths,
  getDatePercent,

  /**
   * Formats the date string for server consumption.
   *
   * @param {date} date The date instance to get the date from.
   */
  formatDateServer: date =>
    _.isDate(date) ?
    fecha.format(date, AppSettings.FORMATTER.LONG_DATE_SERVER) : undefined,

  /**
   * Formats the time string for server consumption.
   *
   * @param {date} time The date instance to get the time from.
   */
  formatTimeServer: time =>
    _.isDate(time) ?
    fecha.format(time, AppSettings.FORMATTER.LONG_TIME_SERVER) : undefined,

  /**
   * Check if it's a valid date
   *
   * @param {string} date
   */
  isValid: (date) => {
    if (!date) return false;

    const dateParts = _.isDate(date) ?
      fecha.format(date, AppSettings.FORMATTER.LONG_DATE_MOMENT).split(' ') :
      date.split(' ');

    if (dateParts.length !== 3) return false;

    const day = _.toNumber(dateParts[0]);
    const month = _.toLower(dateParts[1]);
    const year = _.toNumber(dateParts[2]);

    const months = fecha.i18n.monthNamesShort;
    const parsedDate = new Date(date);

    if (day !== parsedDate.getDate()) return false;

    if (month !== _.lowerCase(months[parsedDate.getMonth()])) return false;

    // BE throws 500 error for dates with year < 1000
    if (year !== parsedDate.getFullYear() || year < 1000) return false;

    return true;
  },

  /**
   * I take a date range (min to max) and return to what percentage
   * the first date is between them
   * @param  {timestamp or date object} startDate
   * @param  {timestamp or date object} endDate
   */
  generatePips: (startDate, endDate) => {
    if (_.isNumber(startDate)) {
      startDate = new Date(startDate); // eslint-disable-line no-param-reassign
    }
    if (_.isNumber(endDate)) {
      endDate = new Date(endDate); // eslint-disable-line no-param-reassign
    }
    // +1 to include the next month (see endDate below)
    const numberOfMonths = inMonths(startDate, endDate) + 1;
    const pips = [];
    const isMoreThanOneYear = numberOfMonths > 12;
    /* eslint-disable no-param-reassign */
    // Put variable manipulation because earlier calculation was returning negative values
    // and also to make the pips regeneration more accurate
    startDate = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
    endDate = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 1);
    /* eslint-enable no-param-reassign */

    /**
     * This quarterMonths array is filled with months quarter per year.
     * There are two array data being used based on whether the number of the months
     * Which basically filled with :
     *
     * - January
     * - April
     * - July
     * - October
     * - December
     *
     * as the quarter months of the year
     * TODO : refactor if have time
     */
    const quarterMonths = isMoreThanOneYear ? [3, 6, 9] : [0, 3, 6, 9, 11];
    // Used to get large, medium, small pip type
    const getPipType = (theDate) => {
      if (
        /**
         * The condition will be met for certain criteria :
         *
         * - isMoreThanOneYear && Month is January on every year
         * - isNotMoreThanOneYear && numberOfMonths > 3 && the month is a quarter month of the year
         * - numberOfMonths <= 3 && the month is a quarter month of the year
         */
        (isMoreThanOneYear && theDate.getMonth() === 0) ||
        (!isMoreThanOneYear && numberOfMonths > 3 &&
          quarterMonths.indexOf(theDate.getMonth()) !== -1) ||
        (numberOfMonths <= 3 && quarterMonths.indexOf(theDate.getMonth()) !== -1)
      ) {
        return PIPSIZE.LARGE;
      } else if (
        /**
         * The condition will be met for certain criteria :
         *
         * - isMoreThanOneYear && The month is a quarter month of the year
         * - isNotMoreThanOneYear && numberOfMonths > 3 &&
         *   The month is not a quarter month of the year
         * - numberOfMonths <= 3 && the month is not a quarter month of the year
         */
        (isMoreThanOneYear && quarterMonths.indexOf(theDate.getMonth()) !== -1) ||
        (!isMoreThanOneYear && numberOfMonths > 3 &&
          quarterMonths.indexOf(theDate.getMonth()) === -1) ||
        (numberOfMonths <= 3 && quarterMonths.indexOf(theDate.getMonth()) === -1)
      ) {
        return PIPSIZE.MEDIUM;
      }

      return PIPSIZE.SMALL;
    };

    _.each(_.range(0, numberOfMonths), (month) => {
      // I calculate the dates of the 1st of the month for all months
      // between the range dates
      const theDate = new Date(startDate.getFullYear(), startDate.getMonth() + month, 1);

      pips.push(
        { // I push every month between the two range dates into
          // an array, and assume every month starts on the 1st
          date: theDate,
          dateFormatDefault: "MMM ''yy",
          dateFormatMobile: "MMM ''yy",
          position: getDatePercent(startDate, endDate, theDate),
          isShowLabel:
            (numberOfMonths > 12 && getPipType(theDate) === PIPSIZE.LARGE) ||
            (numberOfMonths <= 12 &&
            (getPipType(theDate) === PIPSIZE.LARGE || getPipType(theDate) === PIPSIZE.MEDIUM)),
          pipType: getPipType(theDate),
        }
      );

      // Push additional pips if it's less or equal than one year
      if (!isMoreThanOneYear) {
        let incrementFlag = 0;
        // Adding 3 inside range because we want to add more 3 pip as 'weeks'
        // to the collection if the accumulated month is less from or equal one year
        _.each(_.range(0, 3), (week) => {
          const theWeekDate = theDate.setDate(incrementFlag + (31 / 4));
          pips.push(
            {
              date: theWeekDate,
              dateFormatDefault: 'MMM yyyy',
              dateFormatMobile: "MMM ''yy", // Not sure why must be 2 single quotes
              // Push every week between the two range dates into the array
              position: getDatePercent(startDate, endDate, theWeekDate),
              pipType: PIPSIZE.SMALL,
            }
          );

          // Add one week flag
          // for the next iteration
          incrementFlag += (31 / 4);
        });
      }
    });

    return pips;
  },

  /**
   * Checks if date in d is between dates in start and end.
   *
   * If string date is provided, it is converted to a date object,
   * such that the date has a time of midnight local time (as opposed to default of
   * midnight UTC.
   *
   * @param date {date|string}
   * @param start {date|string}
   * @param end {date|string}
   *
   * @return {boolean} Returns a boolean:
   * true  : if d is between start and end (inclusive) / d is before end / start is before d
   * false : if none of the conditions is fulfilled
   */
  inRange: (date, start, end) => { // eslint-disable-line arrow-body-style
    const [dateInMillis, startInMillis, endInMillis] = _.map([date, start, end], (d) => {
      let dateObj = d;
      if (_.isString(d) || _.isNumber(d)) {
        // Date object from string treated as UTC. Convert back to local time zone.
        const utcMidnightDate = new Date(d);
        const localMidnightInMillis =
          utcMidnightDate.getTime() + (utcMidnightDate.getTimezoneOffset() * 60 * 1000);
        dateObj = new Date(localMidnightInMillis);
      }

      if (dateObj instanceof Date && isFinite(dateObj)) {
        return dateObj.getTime();
      }
      return null;
    });

    if (dateInMillis != null && startInMillis != null && endInMillis != null) {
      return startInMillis <= dateInMillis && dateInMillis <= endInMillis;
    } else if (dateInMillis != null && startInMillis != null) {
      return startInMillis <= dateInMillis;
    } else if (dateInMillis != null && endInMillis != null) {
      return endInMillis >= dateInMillis;
    }

    return false;
  },

  /**
   * Remanipulate the date values that will be passed to server/locally
   *
   * @param dates {array} dates timestamp
   * @param initialDates {object} initialDates values for comparisson
   */

  setTimelineDateRange: (dates, initialDates) => {
    /**
     * Returns if the dates are sharing the same info / same dates
     *
     * @param date {object} the date object
     * @param comparatorDate {object} the comparator date object
     */

    const isSameDateAs = (date, comparatorDate) => (
        date.getFullYear() === comparatorDate.getFullYear() &&
        date.getMonth() === comparatorDate.getMonth() &&
        date.getDate() === comparatorDate.getDate()
      );

    // Respecify the first date
    const firstDate = new Date(+dates[0]);
    const firstDayOfMonth = new Date(
      firstDate.getFullYear(), firstDate.getMonth(), 1, firstDate.getHours()
    ).getTime();

    // Respecify the second date
    const lastDate = new Date(+dates[1]);
    const lastDayOfMonth = new Date(lastDate.getFullYear(), lastDate.getMonth() + 1, 0).getTime();

    dates[0] = isSameDateAs(firstDate, initialDates.minDate) ? firstDate : firstDayOfMonth; //eslint-disable-line
    dates[1] = isSameDateAs(lastDate, initialDates.maxDate) ? lastDate : lastDayOfMonth; //eslint-disable-line
  },
};
