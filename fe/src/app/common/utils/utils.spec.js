/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service,
angular/document-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import utilsFactory from './utils.factory';

describe('Utils', () => {
  let $state;
  beforeEach(() => {
    window.module(($provide) => {
      const mockState = {
        reload: () => {},
        current: {},
      };

      $provide.value('AppSettings', AppSettings);
      $provide.value('$state', mockState);
    });
  });
  const ie10UserAgent = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)';
  const ie11UserAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2; ' +
    '.NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; ' +
    '.NET4.0E; InfoPath.3; rv:11.0) like Gecko';

  const generateTimeDataMethod = utilsFactory().generateTimeData();

  let fakeNavigator;

  let fakeWindow;

  it('should detect IE browser when IE 10', () => {
    fakeNavigator = { userAgent: ie10UserAgent };
    expect(utilsFactory().isIE(fakeNavigator)).toBeTruthy();
  });

  it('should detect IE browser  when IE 11', () => {
    fakeNavigator = { userAgent: ie11UserAgent };
    expect(utilsFactory().isIE(fakeNavigator)).toBeTruthy();
  });

  it('should define the first order of the generateTimeData() array', () => {
    expect(generateTimeDataMethod[0]).toMatch('00:00');
  });

  it('should define the last order of the generateTimeData() method array', () => {
    expect(generateTimeDataMethod[generateTimeDataMethod.length - 1]).toMatch('23:59');
  });

  it('should test isMobileDevice() method', () => {
    expect(utilsFactory().isMobileDevice()).toBe(false);
  });

  it('should return the file name from the alt text', () => {
    const altText = 'national administration';
    const fullURL = 'http://localhost:9876/12345678.pdf';
    const fileName = 'national-administration.pdf';

    expect(
      utilsFactory().getFileName(altText, fullURL)
      ).toBe(fileName);
  });

  it('should return an object of icon name and path', () => {
    expect(utilsFactory().getIconMap()).toBeDefined();
  });

  describe('ConfigForFileDownload', () => {
    it('sets up the configurer correctly', () => {
      const configFunction = utilsFactory().ConfigForFileDownload;
      const configurer = {
        setBaseUrl: () => {},
        setFullResponse: () => {},
      };
      spyOn(configurer, 'setBaseUrl');
      spyOn(configurer, 'setFullResponse');

      configFunction(configurer);
      expect(configurer.setBaseUrl).toHaveBeenCalledWith('/api/v1-spring');
      expect(configurer.setFullResponse).toHaveBeenCalledWith(true);
    });
  });

  describe('getDueStatusDefinition()', () => {
    const utils = utilsFactory();
    const { IMMINENT, OVERDUE, DUE_SOON, NOT_DUE } = _.cloneDeep(AppSettings.OVERALL_STATUS_NAMES);
    const { COC, AI, SF } = AppSettings.CODICILS_META;

    it('should return the correct text for status OVERDUE', () => {
      const item = { category: 'Statutory' };
      expect(utils.getDueStatusDefinition(null, OVERDUE, item)).toEqual('cd-asset-overall-status-overdue');
      expect(utils.getDueStatusDefinition(COC.ID, OVERDUE, item)).toEqual('cd-overdue');
      expect(utils.getDueStatusDefinition(AI.ID, OVERDUE, item)).toEqual('cd-overdue');
      expect(utils.getDueStatusDefinition(SF.ID, OVERDUE, item)).toEqual('cd-overdue');
    });

    it('should return the correct text for status IMMINENT', () => {
      const item = { category: 'Statutory' };
      expect(utils.getDueStatusDefinition(COC.ID, IMMINENT, item)).toEqual('cd-due-within-30-days');
      expect(utils.getDueStatusDefinition(SF.ID, IMMINENT, item)).toEqual('cd-due-within-30-days');
      expect(utils.getDueStatusDefinition(AI.ID, IMMINENT, item)).toEqual('cd-due-within-1-month');
      expect(utils.getDueStatusDefinition('service', IMMINENT, item)).toEqual('cd-due-within-1-month');
      expect(utils.getDueStatusDefinition('task', IMMINENT, item)).toEqual('cd-due-within-one-month');
      expect(utils.getDueStatusDefinition(null, IMMINENT, item)).toEqual('cd-asset-overall-status-imminent');
    });

    it('should return the correct text for status DUE_SOON', () => {
      let item = { category: 'Statutory' };
      expect(utils.getDueStatusDefinition(AI.ID, DUE_SOON, item)).toEqual('cd-due-within-1-year-statutory');
      item = { category: null };
      expect(utils.getDueStatusDefinition(AI.ID, DUE_SOON, item)).toEqual('cd-due-within-3-months-non-statutory');
      expect(utils.getDueStatusDefinition(SF.ID, DUE_SOON, item)).toEqual('cd-due-within-three-months');
      expect(utils.getDueStatusDefinition('service', DUE_SOON, item)).toEqual('cd-due-within-3-months');
      expect(utils.getDueStatusDefinition(COC.ID, DUE_SOON, item)).toEqual('cd-due-within-three-months');
      expect(utils.getDueStatusDefinition('task', DUE_SOON, item)).toEqual('cd-due-within-three-months');
      expect(utils.getDueStatusDefinition(null, DUE_SOON, item)).toEqual('cd-asset-overall-status-due-soon');
    });

    it('should return the correct text for status NOT_DUE', () => {
      expect(utils.getDueStatusDefinition(COC.ID, NOT_DUE)).toEqual('cd-not-due-within-three-months');
      expect(utils.getDueStatusDefinition(SF.ID, NOT_DUE)).toEqual('cd-not-due-within-three-months');
      expect(utils.getDueStatusDefinition(AI.ID, NOT_DUE)).toEqual('cd-not-overdue');
      expect(utils.getDueStatusDefinition(null, NOT_DUE)).toEqual('cd-asset-overall-status-not-due');
    });
  });

  describe('downloadBlobWithFilename', () => {
    it('(WITHOUT file name), uses anchor and clicks on it', () => {
      const fakeElement = document.createElement('a');
      spyOn(document, 'createElement').and.returnValue(fakeElement);
      spyOn(fakeElement, 'click').and.callFake(() => {});

      utilsFactory().downloadFile('http://test.org/test-file.pdf');

      expect(document.createElement).toHaveBeenCalledWith('a');
      expect(fakeElement.onclick).not.toBeUndefined();
      expect(fakeElement.href).toEqual('http://test.org/test-file.pdf');
      expect(fakeElement.download).toEqual('undefined'); // Default value is empty string not undefined
      expect(fakeElement.target).toEqual('_self');
      expect(fakeElement.click).toHaveBeenCalled();
    });

    it('(WITH file name), uses anchor and clicks on it', () => {
      const fakeElement = document.createElement('a');
      spyOn(document, 'createElement').and.returnValue(fakeElement);
      spyOn(fakeElement, 'click').and.callFake(() => {});

      utilsFactory().downloadFile('http://test.org/test-file.pdf', 'custom-file-name');

      expect(document.createElement).toHaveBeenCalledWith('a');
      expect(fakeElement.onclick).not.toBeUndefined();
      expect(fakeElement.href).toEqual('http://test.org/test-file.pdf');
      expect(fakeElement.download).toEqual('custom-file-name');
      expect(fakeElement.target).toEqual('_self');
      expect(fakeElement.click).toHaveBeenCalled();
    });
  });

  describe('hasUnfilteredQuery', () => {
    it('will returns proper boolean value', () => {
      const unfilteredQuery = _.clone(AppSettings.DEFAULT_PARAMS.FLEET.UNFILTERED_QUERY);

      expect(utilsFactory().hasUnfilteredQuery(unfilteredQuery)).toBe(true);

      const anotherQuery = {};
      _.assign(anotherQuery, unfilteredQuery, { buildMinDate: new Date() });

      expect(utilsFactory().hasUnfilteredQuery(anotherQuery)).toBe(false);

      const anotherQuery2 = {};
      _.assign(anotherQuery2, unfilteredQuery, { assetTypeCodes: [] });

      expect(utilsFactory().hasUnfilteredQuery(anotherQuery2)).toBe(true);

      const anotherQuery3 = {};
      _.assign(anotherQuery3, unfilteredQuery, { flagStateCodes: [] });
      expect(utilsFactory().hasUnfilteredQuery(anotherQuery3)).toBe(true);
    });
  });

  describe('hasUnfilteredFavouritesQuery', () => {
    it('returns the proper boolean value', () => {
      const unfilteredFavouritesQuery = _.clone(AppSettings.DEFAULT_PARAMS.FLEET.QUERY);

      expect(utilsFactory().hasUnfilteredFavouritesQuery(unfilteredFavouritesQuery)).toBe(true);

      const anotherQuery = {};
      _.assign(anotherQuery, unfilteredFavouritesQuery, { buildMinDate: new Date() });

      expect(utilsFactory().hasUnfilteredFavouritesQuery(anotherQuery)).toBe(false);

      const anotherQuery2 = {};
      _.assign(anotherQuery2, unfilteredFavouritesQuery, { assetTypeCodes: [] });

      expect(utilsFactory().hasUnfilteredFavouritesQuery(anotherQuery2)).toBe(true);

      const anotherQuery3 = {};
      _.assign(anotherQuery3, unfilteredFavouritesQuery, { flagStateCodes: [] });
      expect(utilsFactory().hasUnfilteredFavouritesQuery(anotherQuery3)).toBe(true);
    });
  });

  describe('getTabularAction() generates an ui-sref string or custom click event for a tabular item', () => {
    let fakeActionObj,
      fakeItems;

    it('should return false if action is not defined', () => {
      expect(utilsFactory().getTabularAction()).toEqual(false);
    });

    it('should return ngClick function and set click boolean to true if ngClick is provided', () => {
      fakeActionObj = {
        ngClick: () => true,
      };

      const result = utilsFactory().getTabularAction(fakeActionObj);
      expect(result()).toBe(true);
      expect(result.click).toBe(true);
      expect(result.customIcon).not.toBeDefined();
    });

    it('should call ngClick with the item if the item is provided', () => {
      const result = [];
      fakeActionObj = {
        ngClick: (item) => {
          result.push(item);
          return true;
        },
      };

      const tabularAction = utilsFactory().getTabularAction(fakeActionObj, { foo: 'bar' });
      expect(tabularAction()).toBe(true);
      expect(result).toEqual([{ foo: 'bar' }]);
      expect(tabularAction.click).toBe(true);
      expect(tabularAction.customIcon).not.toBeDefined();
    });

    it('should return ngClick function, set clickArea and set customIcon into true if action.icon is provided', () => {
      fakeActionObj = {
        ngClick: () => true,
      };

      let result = utilsFactory().getTabularAction(fakeActionObj);
      expect(result()).toBe(true);
      expect(result.click).toBe(true);
      expect(result.clickArea).not.toBeDefined();
      expect(result.customIcon).not.toBeDefined();

      fakeActionObj.clickArea = 'icon';
      fakeActionObj.icon = 'reopen';
      result = utilsFactory().getTabularAction(fakeActionObj);
      expect(result.clickArea).toBe('icon');
      expect(result.customIcon).toEqual('reopen');
    });

    it('should return correct sref when evalParams and params are not provided', () => {
      fakeActionObj = {
        stateName: 'hello.world',
      };
      expect(utilsFactory().getTabularAction(fakeActionObj)).toEqual('hello.world');
    });

    it('should return correct sref when evalParams is not provided', () => {
      fakeActionObj = {
        stateName: 'hello.world',
        params: {
          assetId: 1,
        },
      };
      expect(utilsFactory().getTabularAction(fakeActionObj)).toEqual('hello.world({"assetId":1})');
    });

    it('should return correct sref when params is not provided', () => {
      fakeActionObj = {
        stateName: 'hello.world',
        evalParams: {
          param1: 'id',
          param2: 'id2',
        },
      };
      fakeItems = [{
        id: 1,
        id2: 2,
        some: 'thing',
      }, {
        id: 2,
        id2: 3,
        some: 'thing2',
      }];
      expect(utilsFactory().getTabularAction(fakeActionObj, fakeItems[1])).toEqual('hello.world({"param1":2,"param2":3})');
    });

    it('should return correct sref when evalParams and params are provided', () => {
      fakeActionObj = {
        stateName: 'hello.world',
        evalParams: {
          param1: 'id',
          param2: 'id2',
        },
        params: {
          assetId: 1,
        },
      };
      expect(utilsFactory().getTabularAction(fakeActionObj, fakeItems[0])).toEqual('hello.world({"assetId":1,"param1":1,"param2":2})');
    });
  });

  describe('getServiceCodicilsSrefObject', () => {
    it('should return the correct ui-sref', () => {
      const item = {
        id: 1,
        code: 'LRV2', // should change to assetCode in the future
        codicilType: 'COC',
        asset: {
          id: 2,
        },
      };
      expect(utilsFactory().getServiceCodicilsSrefObject(item))
        .toEqual({ stateName: 'asset.conditionsOfClassDetail', params: { assetId: 'LRV2', codicilId: 1, deficiencyId: undefined } });

      item.codicilType = 'AI';
      expect(utilsFactory().getServiceCodicilsSrefObject(item))
        .toEqual({ stateName: 'asset.actionableItemsDetail', params: { assetId: 'LRV2', codicilId: 1, deficiencyId: undefined } });

      item.codicilType = null;
      expect(utilsFactory().getServiceCodicilsSrefObject(item))
        .toEqual({ stateName: 'asset.serviceDetails', params: { assetId: 'LRV2', serviceId: 1 } });

      item.workItemType = 'TASKLIST';
      expect(utilsFactory().getServiceCodicilsSrefObject(item))
        .toEqual({ stateName: 'asset.serviceDetails.tasklist', params: { assetId: 'LRV2', serviceId: 1 } });

      item.workItemType = 'CHECKLIST';
      expect(utilsFactory().getServiceCodicilsSrefObject(item))
        .toEqual({ stateName: 'asset.serviceDetails.checklist', params: { assetId: 'LRV2', serviceId: 1 } });
    });
  });
});
