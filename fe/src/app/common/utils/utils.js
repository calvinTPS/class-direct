import angular from 'angular';
import utilsFactory from './utils.factory';

export default angular.module('utils', [])
.factory('utils', utilsFactory)
.name;
