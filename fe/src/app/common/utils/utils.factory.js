/* eslint-disable prefer-template, no-param-reassign, max-len, angular/window-service, wrap-iife, no-useless-escape, no-else-return */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import mimes from 'mime.json';

/* @ngInject */
const UtilsFactory = (
  $rootScope,
  $window,
  $timeout,
  $filter,
) => {
  const isIE = nav => (nav.userAgent.indexOf('MSIE') >= 0 || nav.userAgent.indexOf('Trident') >= 0 || nav.userAgent.indexOf('Edge') >= 0);

  /**
   * Pass this into Restangular's withConfig method to configure
   * downloads.
   */
  const ConfigForFileDownload = (RestangularConfigurer) => {
    RestangularConfigurer.setBaseUrl(AppSettings.SPRING_API_URL);
    RestangularConfigurer.setFullResponse(true);
  };

  /**
   * I download the given file from a url.
   *
   * @param {string} downloadUrl
   * @param {string} downloadName - The filename to be given to the saved file,
   */
  const downloadFile = (downloadUrl, downloadName, target) => {
    if (window.cordova) {
      mobileDownload(downloadUrl, downloadName);
      return false;
    }
    // We are not using $document due to some unsupported functionality needed
    /* eslint-disable angular/document-service */
    const a = document.createElement('a');

    // MS Edge fix
    // Only remove the anchor DOM after a timeout to ensure download event is fired
    a.onclick = event => $timeout(document.body.removeChild(event.target), 500);
    a.href = downloadUrl;
    a.download = downloadName;
    a.target = '_self';

    // Target is required
    document.body.appendChild(a);
    a.click();
    return false; // age old hack to allow for '#' hrefs
  };
  /* eslint-enable angular/document-service */

  /**
   * I show the spinner dialog with a message
   * I will inform the user that Cordova is doing something
   * on the background
   */
  const showNativeSpinner = (message) => {
    window.plugins.spinnerDialog.hide(); // needed as cordova is stupid enough to cause a stack overflow on Android
    if (message != null) { // Simple shorthand to close the spinner if null
      window.plugins.spinnerDialog.show(
        null, // title: unsupported in iOS
        message,
        true,
      );
    }
  };

  /**
   * I do some extra Pizazz to get the Pizass file from the https server
   * @param  {string} URL - Absolute URL
   * @param  {string} fileName - optional file name if needed
   */
  const mobileDownload = (URL, fileName) => {
    $rootScope.$broadcast(AppSettings.EVENTS.ASSETS_EXPORT_DONE);
    // TODO - need to revisit with API_URL once CI is setup
    //
    // I check if the URL starts with 'http' and prepend the BASE_URL
    // if it is a relative path
    URL = URL.indexOf('http') === 0 ?
      URL : `${AppSettings.BASE_URL}${URL.replace('./', '/')}`;

    // Parameters mismatch check
    if (URL !== null && fileName !== null) {
      prepareDownload(URL, fileName); // If available download function call
    }
  };

  const prepareDownload = (URL, fileName) => {
    // I get the local directory
    // i.e. They differ in iOS and Android
    // We want to use the Downloads folder on Android
    // and the Documents folder in iOS
    const localDirectory = window.cordova.file.externalRootDirectory ?
      (window.cordova.file.externalRootDirectory + 'Download') :
      window.cordova.file.documentsDirectory;

    showNativeSpinner('Praparing download...');

    // I am called when the blob contents is received and ensure
    // writing to that location is possible
    const saveFile = (dirEntry, fileData, localFileName) => {
      dirEntry.getFile(localFileName, { create: true, exclusive: false }, (fileEntry) => {
        // I am directly writing to the file if not iOS, as Cordova 7 has introduced a bug
        if (window.device.platform !== 'iOS') {
          writeFile(fileEntry, fileData);
        } else {
          // Set the metadata, prevent iOS files downloaded from being backed up
          fileEntry.setMetadata(() => writeFile(
            fileEntry, fileData),
            (error) => {
              throw new Error('I failed to save the file');
            },
            { 'com.apple.MobileBackup': 1 },
          );
        }
      });
    };


    // I ask Cordova to initialize all
    // that is necessary to write to the file system
    window.resolveLocalFileSystemURL(
      localDirectory,
      (fileEntry) => {
        showNativeSpinner('Downloading file...');
        // I use fetch to get the blob
        fetch(URL, {
          // I need to force fetch to use credentials for XSRF
          credentials: 'include',
        }).then(
          async (r) => {
            const blob = await r.blob();

            // I check if we have a filename provided
            // and use `content-disposition` if we don't have
            // it.
            let localFileName = (r.headers instanceof Headers && r.headers.get('content-disposition')) ?
            r.headers.get('content-disposition') : fileName;
            const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            const matches = filenameRegex.exec(localFileName);

            if (matches != null && matches[1]) {
              localFileName = matches[1].replace(/['"]/g, '');
            }

            return [blob, localFileName];
          }).then((res) => {
            showNativeSpinner('Download completed.');
            saveFile(fileEntry, res[0], res[1]);
          });
      });

    // I write to the file by getting the name from the fileEntry
    // so that I can depend on content-disposition dynamically if it exists
    const writeFile = (fileEntry, dataObj, isAppend) => {
      fileEntry.createWriter((fileWriter) => {
        fileWriter.onprogress = (entry) => {};

        fileWriter.onwriteend = (entry) => {
          // I open the file in the native file viewer or
          // open the relavent links in the Play / App store to
          // download the necessary apps
          const generatedFileName =
            decodeURIComponent(localDirectory + '/' + entry.target.localURL.split('/').pop());

          // I am the file extension
          const extension = generatedFileName.split('.').pop();

          showNativeSpinner(`Opening ${extension.toUpperCase()} file.`);
          // I then open the file, using fileOpener2
          window.cordova.plugins.fileOpener2.open(
            generatedFileName,
            mimes[
              extension === 'csv' ? 'txt' : extension // I treat CSV as TXT files
            ],
            {
              error: (error) => {
                throw new Error(
                  'Cordova: ' + error.message
                );
              },
              success: () => {
                /*  I say fuck all and silently be proud I finally worked  */
                showNativeSpinner(null);
              },
            },
          );
        };

        fileWriter.write(dataObj);
      });
    };
  };

  /**
   *
   * I return the document name which is constructed from
   * the alt text attribute. i.e. 'national administrator' will be
   * 'national-administration.pdf'
   * @param {string} altText - the alt text of the <a> tag
   * @param {string} docURL - the url of the download
   * @returns {string} filename in kebab-case
   */
  const getFileName = (altText, docURL) =>
    `${_.kebabCase(altText)}.${docURL.split('.').pop()}`;

  const generateTimeData = () => {
    const times = [];

    for (let i = 0; i < 24; i++) {
      for (let j = 0; j < 60; j++) {
        const hour = i.toString().length === 1 ? '0' + i : i;
        const minute = j.toString().length === 1 ? '0' + j : j;

        times.push(hour + ':' + minute);
      }
    }

    return times;
  };

  const isMobileDevice = () => {
    let check = false;
    ((a) => {
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_______)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
        check = true;
      }
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
  };

  // Load icons from filesystem and put into icon map
  const getIconMap = () => {
    const iconMap = {};
    const req = require.context('resources/images/icons', true, /^(.*\.(svg$))[^.]*$/i);
    req.keys().forEach((key) => {
      // get the icon filename
      const iconName = _.camelCase(key.match(/\.\/(\w+)/)[1]);
      iconMap[iconName] = req(key);
    });

    return iconMap;
  };

  const getTabularAction = (action, item) => {
    if (!action) {
      return false;
    } else if (_.get(action, 'ngClick')) {
      let ngClickFn;
      if (item) {
        ngClickFn = () => action.ngClick(item);
      } else {
        ngClickFn = _.get(action, 'ngClick');
      }
      _.set(ngClickFn, 'customIcon', action.icon);
      _.set(ngClickFn, 'clickArea', action.clickArea);
      _.set(ngClickFn, 'click', true);
      _.set(ngClickFn, 'hasGap', action.hasGap);
      return ngClickFn;
    }

    if (!action.evalParams && !action.params) {
      return action.stateName;
    }

    const stateName = action.stateName;
    const stateParams = action.params || {};
    _.forEach(action.evalParams, (value, key) => {
      stateParams[key] = item[value];
    });

    return `${stateName}(${angular.toJson(stateParams)})`;
  };

  const DUE_STATUS_TYPES = {
    SERVICE: 'service',
    TASK: 'task',
  };

  const getDueStatusDefinition = (type, dueStatus, item) => {
    const status = _.cloneDeep(AppSettings.OVERALL_STATUS_NAMES);
    const {
      COC,
      AI,
      SF,
    } = AppSettings.CODICILS_META;
    let text = '';

    if (item && item.isPostponed && type === 'service') {
      const postponementDate = $filter('date')(item.postponementDate, _.cloneDeep(AppSettings.FORMATTER.LONG_DATE_ANGULAR));
      let subText = '';
      if (Date.parse(new Date()) <= item.postponementDateEpoch) {
        subText = $filter('translate')('cd-agreed-postponement-date');
      } else {
        subText = $filter('translate')('cd-overdue-postponement-date');
      }
      text = `${subText} ${postponementDate}`;
    } else {
      switch (_.startCase(dueStatus)) {
        case status.OVERDUE:
          if (!type) {
            text = 'cd-asset-overall-status-overdue';
          } else {
            text = 'cd-overdue';
          }
          break;
        case status.IMMINENT:
          if (type === COC.ID || type === SF.ID) {
            text = 'cd-due-within-30-days';
          } else if (type === DUE_STATUS_TYPES.TASK) {
            text = 'cd-due-within-one-month';
          } else if (type === DUE_STATUS_TYPES.SERVICE || type === AI.ID) {
            text = 'cd-due-within-1-month';
          } else {
            text = 'cd-asset-overall-status-imminent';
          }
          break;
        case status.DUE_SOON:
          if (type === AI.ID && item.category === 'Statutory') {
            text = 'cd-due-within-1-year-statutory';
          } else if (type === AI.ID) {
            text = 'cd-due-within-3-months-non-statutory';
          } else if (type === DUE_STATUS_TYPES.SERVICE) {
            text = 'cd-due-within-3-months';
          } else if (type === COC.ID || type === DUE_STATUS_TYPES.TASK || type === SF.ID) {
            text = 'cd-due-within-three-months';
          } else {
            text = 'cd-asset-overall-status-due-soon';
          }
          break;
        case status.NOT_DUE:
          if (type === COC.ID || type === SF.ID) {
            text = 'cd-not-due-within-three-months';
          } else if (type) {
            text = 'cd-not-overdue';
          } else {
            text = 'cd-asset-overall-status-not-due';
          }
          break;
        default:
          break;
      }
    }
    return text;
  };

  const scrollToTop = () => {
    $window.scroll(0, 0);
  };

  const serviceCodicilsSrefCache = new WeakMap();
  const getServiceCodicilsSrefObject = (item) => {
    const cachedResult = serviceCodicilsSrefCache.get(item);
    if (cachedResult != null) {
      return cachedResult;
    }

    let obj;
    let stateName = null;

    if (item.codicilType && !_.isUndefined(item.code) && !_.isUndefined(item.id)) {
      switch (item.codicilType) {
        case AppSettings.CODICILS_META.COC.ID:
          stateName = AppSettings.STATES.CONDITIONS_OF_CLASS_DETAILS;
          break;
        case AppSettings.CODICILS_META.MNCN.ID:
          stateName = AppSettings.STATES.MNCNS_DETAILS;
          break;
        case AppSettings.CODICILS_META.AI.ID:
          stateName = AppSettings.STATES.ACTIONABLE_ITEMS_DETAILS;
          break;
        case AppSettings.CODICILS_META.SF.ID:
          stateName = AppSettings.STATES.STATUTORY_FINDINGS_DETAILS;
          break;
        default:
      }

      obj = {
        stateName,
        params: {
          assetId: item.code,
          codicilId: item.id,
          deficiencyId: item.deficiencyId,
        },
      };
    } else if (!item.codicilType && !_.isUndefined(item.code) && !_.isUndefined(item.id)) {
      if (item.workItemType == null) {
        stateName = AppSettings.STATES.ASSET_SERVICE_DETAILS;
      } else {
        stateName = AppSettings.STATES[`ASSET_SERVICE_${item.workItemType}`];
      }
      obj = {
        stateName,
        params: {
          assetId: item.code,
          serviceId: item.id,
        },
      };
    }

    serviceCodicilsSrefCache.set(item, obj);
    return obj;
  };

  // Omit some parameters if they're carrying empty array, or is param value is empty
  const omitKeyMap = _.keyBy(['assetTypeCodes', 'flagStateCodes']);
  const omitKeyPredicate = (v, k) =>
    ((omitKeyMap[k] != null && _.isEmpty(v) && _.isArray(v)) || v == null);

  // Survey schedule options have extra parameters needed to be omitted before checking for unfilteredQuery
  const omitKeySurveySchedule = ['codicilDueDateMax', 'codicilDueDateMin', 'isSubfleet'];

  const hasUnfilteredQuery = (options, excludeSearch) => {
    if (excludeSearch) {
      options = _.omitBy(options, 'search');
    }
    let queryToSubmit = _.omitBy(options, omitKeyPredicate);
    if (queryToSubmit.isSubfleet) {
      queryToSubmit = _.omit(queryToSubmit, omitKeySurveySchedule);
    }
    const unfilteredQuery = _.clone(AppSettings.DEFAULT_PARAMS.FLEET.UNFILTERED_QUERY);
    if (_.isEqual(queryToSubmit, unfilteredQuery)) {
      return true;
    }
    return false;
  };

  const hasUnfilteredFavouritesQuery = (options) => {
    let queryToSubmit = _.omitBy(options, omitKeyPredicate);
    if (queryToSubmit.isSubfleet) {
      queryToSubmit = _.omit(queryToSubmit, omitKeySurveySchedule);
    }
    const unfilteredFavouritesQuery = _.clone(AppSettings.DEFAULT_PARAMS.FLEET.QUERY);

    if (_.isEqual(queryToSubmit, unfilteredFavouritesQuery)) {
      return true;
    }

    return false;
  };

  const getFileExtension = fileName => fileName.toLowerCase().split('.').pop();

  return {
    ConfigForFileDownload,
    downloadFile,
    generateTimeData,
    getFileName,
    getIconMap,
    getTabularAction,
    getServiceCodicilsSrefObject,
    hasUnfilteredQuery,
    hasUnfilteredFavouritesQuery,
    isIE,
    isMobileDevice,
    getDueStatusDefinition,
    mobileDownload,
    scrollToTop,
    getFileExtension,
  };
};
export default UtilsFactory;
