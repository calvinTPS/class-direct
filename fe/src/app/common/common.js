import A from './a';
import Accordion from './accordion/accordion';
import ActiveStatus from './active-status/active-status';
import AddClearBtn from './add-clear-btn/add-clear-btn';
import angular from 'angular';
import Attachments from './attachments';
import BindHeight from './bind-height';
import CdAccordionToggler from './cd-accordion-toggler';
import CdActions from './cd-actions';
import CdCard from './cd-card';
import CdTooltip from './cd-tooltip';
import ClickOutside from './click-outside';
import CMRO from './cmro';
import DatePicker from './date-picker/date-picker';
import DateRange from './date-range';
import DisableAnimate from './disable-animate';
import DueStatus from './due-status/due-status';
import Emblem from './emblem/emblem';
import Export from './export';
import Footer from './footer';
import Icon from './icon/icon';
import InfiniteScroll from './infinite-scroll/infinite-scroll';
import InputTags from './input-tags/input-tags';
import KeepScrollPos from './keep-scroll-pos';
import MiniAssetCard from './mini-asset-card';
import PageHeader from './page-header';
import Pagination from './pagination/pagination';
import ParseTime from './parse-time';
import PreventDefault from './prevent-default';
import Riders from './riders';
import SearcButton from './search-button/search-button';
import ServiceModal from './service-modal/service-modal';
import ShowData from './show-data';
import Spinner from './spinner/spinner';
import Star from './star/star';
import StaticLink from './static-link';
import Sticky from './sticky';
import StopEvent from './stop-event';
import TabularGanttSwitch from './tabular-gantt-switch';
import TimepickerHelper from './timepicker-helper/timepicker-helper';
import Utils from './utils/utils';
import ViewSwitch from './view-switch';
import VisibleResults from './visible-results/visible-results';

const commonModule = angular.module('app.common', [
  A,
  Accordion,
  ActiveStatus,
  AddClearBtn,
  Attachments,
  BindHeight,
  CdAccordionToggler,
  CdActions,
  CdCard,
  CdTooltip,
  ClickOutside,
  CMRO,
  DatePicker,
  DateRange,
  DisableAnimate,
  DueStatus,
  Emblem,
  Export,
  Footer,
  Icon,
  InfiniteScroll,
  InputTags,
  KeepScrollPos,
  MiniAssetCard,
  PageHeader,
  Pagination,
  ParseTime,
  PreventDefault,
  Riders,
  SearcButton,
  ServiceModal,
  ShowData,
  Spinner,
  Star,
  StaticLink,
  Sticky,
  StopEvent,
  TabularGanttSwitch,
  TimepickerHelper,
  Utils,
  ViewSwitch,
  VisibleResults,
])
.name;

export default commonModule;
