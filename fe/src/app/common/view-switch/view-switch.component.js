import './view-switch.scss';
import controller from './view-switch.controller';
import template from './view-switch.pug';

export default {
  bindings: {
    type: '=',
    location: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
