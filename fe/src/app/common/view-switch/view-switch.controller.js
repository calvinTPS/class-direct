import Base from 'app/base';

export default class TabularGanttSwitchController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    ViewService,
    DashboardService,
  ) {
    super(arguments);
  }

  $onInit() {
    const { TABLE, CARD } = this._AppSettings.VIEW_TYPES;
    this._tabs = [
      {
        icon: 'dummyTable',
        type: TABLE,
      },
      {
        icon: 'administration',
        type: CARD,
      }];

    this.selectedTabIndex = this.type === CARD ? 1 : 0;
  }

  get tabs() {
    return this._tabs;
  }

  setType(tab) {
    this.type = tab.type;
    this._ViewService.setVesselListViewType(this.type);
    this._DashboardService.resetFavourites();
  }
}
