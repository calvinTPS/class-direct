import angular from 'angular';
import viewSwitchComponent from './view-switch.component';

export default angular.module('viewSwitch', [])
.component('viewSwitch', viewSwitchComponent)
.name;
