/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import viewSwitchComponent from './view-switch.component';
import viewSwitchController from './view-switch.controller';
import viewSwitchModule from './';
import viewSwitchTemplate from './view-switch.pug';

describe('viewSwitch', () => {
  let $rootScope, $compile, makeController;

  beforeEach(window.module(viewSwitchModule));

  beforeEach(() => {
    const mockViewService = {
      setVesselListViewType: () => true,
    };

    const mockDashBoardService = {
      resetFavourites: () => true,
    };

    window.module(($provide) => {
      $provide.value('ViewService', mockViewService);
      $provide.value('DashboardService', mockDashBoardService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('viewSwitch', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({
        foo: 'bar',
        type: 'table',
      });
    });

    it('initializes just fine', () => {
      expect(controller.foo).toEqual('bar');
    });

    it('tabs is defined', () => {
      expect(controller._tabs).toBeDefined();
      expect(controller.tabs).toEqual(controller._tabs);
    });

    it('returns the correct value of tabs', () => {
      expect(controller.tabs[0].type).toEqual('table');
      expect(controller.tabs[1].type).toEqual('card');
    });

    it('updates the correct value of type', () => {
      const tabs = [
        { type: 'table' },
        { type: 'card' },
      ];
      controller.setType(tabs[0]);
      expect(controller.type).toEqual('table');

      controller.setType(tabs[1]);
      expect(controller.type).toEqual('card');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = viewSwitchComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(viewSwitchTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(viewSwitchController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element =
        angular.element('<view-switch><view-switch/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('viewSwitch');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
