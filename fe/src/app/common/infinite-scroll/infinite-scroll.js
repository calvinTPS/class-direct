import angular from 'angular';
import infiniteScroll from './infinite-scroll.directive';

export default angular.module('infiniteScroll', [
])
.directive('infiniteScroll', infiniteScroll)
.name;
