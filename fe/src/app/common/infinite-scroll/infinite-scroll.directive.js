import AppSettings from 'app/config/project-variables.js';

/* @ngInject */
export default ($rootScope, $window) => ({
  restrict: 'A',
  link: (
    scope,
    element,
    attributes,
    controller,
  ) => {
    const isBody = element[0].localName === 'body';

    if (isBody) {
      element = angular.element($window); // eslint-disable-line no-param-reassign
    }

    element.bind('scroll', () => {
      const e = element[0];
      let lastScrollPos = 0;
      let currentScrollPos = 0;
      let scrollHeight = 0;
      let innerHeight = 0;
      let pageYOffset = 0;
      let bottomSpace = 0;

      if (isBody) {
        // e.scrollY doesn't support IE so had to put e.pageYOffset
        currentScrollPos = e.scrollY || e.pageYOffset;
        scrollHeight = document.documentElement.scrollHeight; // eslint-disable-line
        innerHeight = e.innerHeight;
        pageYOffset = e.pageYOffset;
        bottomSpace = scrollHeight - pageYOffset - innerHeight;
      } else {
        currentScrollPos = e.scrollTop;
        scrollHeight = e.scrollHeight;
        pageYOffset = e.offsetHeight;
        bottomSpace = scrollHeight - pageYOffset - currentScrollPos;
      }

      if (currentScrollPos > 5) {
        angular.element(element).addClass('scrolled');
      } else {
        angular.element(element).removeClass('scrolled');
      }

      const isScrollingDown = currentScrollPos > lastScrollPos;
      if (isScrollingDown && bottomSpace < AppSettings.DEFAULT_PARAMS.INFINITE_SCROLL_OFFSET) {
        scope.$broadcast(AppSettings.EVENTS.SCROLL_TO_BOTTOM_PAGE);
      }

      lastScrollPos = currentScrollPos;
    });
  },
});
