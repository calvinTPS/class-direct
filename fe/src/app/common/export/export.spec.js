/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import ExportComponent from './export.component';
import ExportController from './export.controller';
import ExportModule from './';
import ExportTemplate from './export.pug';

describe('Export', () => {
  let $rootScope,
    $compile,
    makeController,
    mockExportService;

  beforeEach(window.module(ExportModule));

  beforeEach(() => {
    mockExportService = {
      export: () => {},
    };

    window.module(($provide) => {
      const mockTranslateFilter = value => value;
      $provide.value('AppSettings', AppSettings);
      $provide.value('ExportService', mockExportService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('export', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
      expect(controller.exportOptions).toEqual({});
      expect(controller.exportOptions.itemType).toBeUndefined();
    });

    it('export() calls the export service with correct options', () => {
      const controller = makeController();
      spyOn(mockExportService, 'export');

      controller.export();
      expect(controller._ExportService.export).toHaveBeenCalledWith({ itemsToExport: [] });
    });

    it('isTaskMpmsOrChecklist return correct boolean value', () => {
      const controller = makeController();
      // return true if checklist (will always contain serviceId)
      controller.exportOptions = {
        assetCode: '1',
        serviceId: '2',
        itemType: 'CHECKLIST',
      };
      expect(controller.isTaskMpmsOrChecklist).toBeTruthy();

      // return true if tasklist (will always contain serviceId)
      controller.exportOptions = {
        assetCode: '1',
        serviceId: '2',
        itemType: 'TASKLIST',
      };
      expect(controller.isTaskMpmsOrChecklist).toBeTruthy();

      // return true if MPMS (will not contain serviceId)
      controller.exportOptions = {
        assetCode: '1',
        itemType: 'MPMS',
      };
      expect(controller.isTaskMpmsOrChecklist).toBeTruthy();

      // return false otherwise
      controller.exportOptions = {
        assetCode: '1',
        itemType: 'CONDITIONS_OF_CLASS',
      };
      expect(controller.isTaskMpmsOrChecklist).toBeFalsy();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ExportComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ExportTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ExportController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<export foo="bar"><export/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Export');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
