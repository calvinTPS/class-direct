import * as _ from 'lodash';
import Base from 'app/base';

export default class ExportController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    AppSettings,
    ExportService,
  ) {
    super(arguments);
  }

  $onInit() {
    // Set options to blank object if no default options passed in
    // when component is initialized
    this.exportOptions = this.exportOptions || {};
  }

  get isTaskMpmsOrChecklist() {
    return this.exportOptions.serviceId ||
      this.exportOptions.itemType === this._AppSettings.EXPORTABLE_ITEMS.MPMS;
  }

  // I check if there are any results to export
  get exportable() {
    return (this.exportOptions.service &&
        this.exportOptions.service.itemsToExport &&
        this.exportOptions.service.itemsToExport.length) || // For Codicils
        this.isTaskMpmsOrChecklist; // For checklist, Mpms and tasklists
  }

  export() {
    if (!this.isTaskMpmsOrChecklist) {
      // I set the itemsToExport to the ids or an empty array
      this.exportOptions.itemsToExport =
        this.exportable ?
          _.map(this.exportOptions.service.itemsToExport, 'id') :
          [];
    }

    // clone the object to not mutate the service
    // in case I need it again
    const exportOptionsToServer = { ...this.exportOptions };

    // I clean the payload to send to the server
    delete exportOptionsToServer.service;

    this._ExportService.export(exportOptionsToServer);
  }
}
