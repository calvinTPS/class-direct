import angular from 'angular';
import ExportComponent from './export.component';
import uiRouter from 'angular-ui-router';

export default angular.module('export', [
  uiRouter,
])
.component('export', ExportComponent)
.name;
