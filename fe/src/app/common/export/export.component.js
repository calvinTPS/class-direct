import './export.scss';
import controller from './export.controller';
import template from './export.pug';

export default {
  bindings: {
    exportOptions: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
