import './icon.scss';
import controller from './icon.controller';
import template from './icon.pug';

export default {
  restrict: 'AE',
  bindings: {
    name: '@',
  },
  template,
  controllerAs: 'vm',
  controller,
};
