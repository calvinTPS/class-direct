import angular from 'angular';
import iconComponent from './icon.component';
import uiRouter from 'angular-ui-router';

export default angular.module('icon', [
  uiRouter,
])
.component('icon', iconComponent)
.name;
