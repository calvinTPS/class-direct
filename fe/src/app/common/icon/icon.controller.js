/* eslint-disable global-require */
import _ from 'lodash';
import Base from 'app/base';

// Load icons from filesystem and put into icon map
const iconMap = {};
const req = require.context('resources/images/icons', true, /^(.*\.(svg$))[^.]*$/i);
req.keys().forEach((key) => {
  // get the icon filename
  const iconName = _.camelCase(key.match(/\.\/(\w+)/)[1]);
  iconMap[iconName] = req(key);
});

export default class IconController extends Base {
  /* @ngInject */
  constructor(
    $sce,
  ) {
    super(arguments);
  }

  get iconUrl() {
    return this._$sce.trustAsHtml(iconMap[this.name]);
  }

}
