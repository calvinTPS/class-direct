/**
 * This directive works on ALL <a> tags. It checks if it's an external link
 * and opens the link in a new tab. For Cordova, it uses '_system'
 * which triggers the inAppBrowser to open this tab in the phone's browser
 */
/* @ngInject */
export default $window => ({
  restrict: 'E',
  link: (
    scope, elem, attrs
  ) => {
    attrs.$observe('href', () => {
      const a = elem[0];
      const target = $window.cordova ? '_system' : '_blank';
      if (location.host.indexOf(a.hostname) !== 0) {
        const href = a.href;
        a.href = '#';
        angular.element(a).on('click', (e) => {
          $window.open(href, target);
          e.preventDefault();
          return false;
        });
      }
    });
  },
});
