import a from './a.directive';
import angular from 'angular';

export default angular.module('a', [])
  .directive('a', a)
  .name;
