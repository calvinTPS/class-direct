import _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import dateHelper from 'app/common/helpers/date-helper';
import fecha from 'fecha';

/* @ngInject */
export default $timeout => ({
  require: ['^form', 'ngModel'],
  link: (
    scope,
    element,
    attributes,
    controller, // eslint-disable-line no-shadow
  ) => { // eslint-disable-line no-param-reassign
    /**
     * Reformats the date picker input value according to the correct format.
     *
     * @param {object} form The form object.
     * @param {string} val min or max.
     */
    const inputEl = angular.element(element[0].querySelector('.md-datepicker-input'));
    inputEl.on('blur', (event) => {
      $timeout(() => {
        if (controller[0] && controller[1].$valid) {
          const date = controller[1].$modelValue;
          // only re-format if form is valid
          if (_.isDate(date) && dateHelper.isValid(date)) {
            // Force refresh to update date to correct format
            inputEl.val(fecha.format(date, AppSettings.FORMATTER.LONG_DATE_MOMENT));
            controller[1].$setValidity('valid', true);
          } else if (inputEl.val().length || (attributes.required && !inputEl.val().length)) {
            controller[1].$setValidity('valid', false);
          }
        }
      }, AppSettings.DEFAULT_PARAMS.DEBOUNCE);
    });
  },
});
