import angular from 'angular';
import datePicker from './date-picker.directive';

export default angular.module('datePicker', [
])
.directive('datePicker', datePicker)
.name;
