import './cd-card.scss';
import controller from './cd-card.controller';
import template from './cd-card.pug';

export default {
  bindings: {
    collapsible: '<?',
    item: '<?',
    title: '@?',
    titleSize: '@?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
