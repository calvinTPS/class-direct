import Base from 'app/base';

export default class CdCardController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
  ) {
    super(arguments);
  }

  get titleClass() {
    return {
      'h3-primary': this.titleSize == null || this.titleSize === 'small',
      h2: this.titleSize === 'big',
    };
  }
}
