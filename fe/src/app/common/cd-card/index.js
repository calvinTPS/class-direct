import angular from 'angular';
import cdCardAccordionComponent from './cd-card-accordion';
import cdCardComponent from './cd-card.component';
import cdCardGanttComponent from './cd-card-gantt';
import cdCardTabularComponent from './cd-card-tabular';
import uiRouter from 'angular-ui-router';

export default angular.module('cdCard', [
  cdCardAccordionComponent,
  cdCardGanttComponent,
  cdCardTabularComponent,
  uiRouter,
])
.component('cdCard', cdCardComponent)
.name;
