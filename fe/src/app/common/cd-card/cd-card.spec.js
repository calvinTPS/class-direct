/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import angularMaterial from 'angular-material';
import CdCardComponent from './cd-card.component';
import CdCardController from './cd-card.controller';
import CdCardModule from './';
import CdCardTemplate from './cd-card.pug';

describe('CdCard', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    makeController;

  beforeEach(window.module(
    angularMaterial,
    CdCardModule,
  ));

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('cdCard', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('should return the correct title class', () => {
      const controller = makeController();
      expect(controller.titleClass).toEqual({ 'h3-primary': true, h2: false });

      controller.titleSize = 'small';
      expect(controller.titleClass).toEqual({ 'h3-primary': true, h2: false });

      controller.titleSize = 'big';
      expect(controller.titleClass).toEqual({ 'h3-primary': false, h2: true });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdCardComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CdCardTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdCardController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<cd-card item="item"><cd-card/>');
      element = $compile(element)(scope);
      scope.$apply();

      controller = element.controller('CdCard');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
