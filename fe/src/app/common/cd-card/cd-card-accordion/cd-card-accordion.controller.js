import * as _ from 'lodash';
import Base from 'app/base';

export default class CdCardAccordionController extends Base {
  /* @ngInject */
  constructor(
    $compile,
    $element,
    $log,
    $mdMedia,
    $rootScope,
    $scope,
    AppSettings,
    UserService,
    utils,
    ViewService,
  ) {
    super(arguments);
  }

  onClick() {
    // Tell all child accordions to expand if this is accordion LEVEL 1(top most)
    // AND should expand the children as well

    if (!this.headerItem.isExpanded && this._shouldChildrenExpand()) {
      this.headerItem.isExpanded = true;
      this._$scope.$broadcast(this._AppSettings.EVENTS.EXPAND_ACCORDION, { group: this.group });
    } else {
      this.headerItem.isExpanded = !this.headerItem.isExpanded;
    }
  }

  $onInit() {
    if (this.headerItem) {
      this.headerItem.isExpanded = !!this.isExpanded;
    }

    if (this._ViewService.isExpandableCardAccordion && this.headerItem) {
      this.headerItem.isExpanded = true;
    }

    this._$rootScope.$on(this._AppSettings.EVENTS.EXPAND_OR_COLLAPSE_ALL_ACCORDIONS,
      (events, param) => {
        this.headerItem.isExpanded = !!param;
      });
    this._$scope.$on(this._AppSettings.EVENTS.EXPAND_ACCORDION, (events, data) => {
      if (data.group === this.group) {
        this.headerItem.isExpanded = true;
      }
      if (data && this.group && data.groupKey === this.group) {
        this.headerItem.isExpanded = !!data.isExpanded;
      }
    });

    if (this.mode === this.constructor.MODE.SERVICE_SCHEDULE) {
      // this.currentUser Need to be created as an object so the child component will be updated
      // when the object is populated with the user details
      this.currentUser = {};
      this._getCurrentUser();
    }

    // - This will get the default accordion_XXX templates for the child
    if (!this.templateLocation) {
      this._$log.warn('Using default template as no template provided');
      this.templateLocation = this.mode && this.mode !== this.constructor.MODE.SERVICE_SCHEDULE ?
        `accordion_${this.mode}` : 'accordion_schedule';
    }

    if (angular.isDefined(this.templateLocation)) {
      // compile the provided template against the current scope
      const tpl = this._ViewService.getRegisteredTemplate(this.templateLocation);
      tpl(this._$scope, (clonedElement) => {
        // add the template content
        this._$element.prepend(clonedElement);
      });
    }
  }

  get toggleIconName() {
    return this.headerItem.isExpanded ? 'collapse' : 'expand';
  }

  async _getCurrentUser() {
    this.currentUser = await this._UserService.getCurrentUser();
    this._$scope.$apply();
  }

  get name() {
    return _.get(this.headerItem, 'serviceCatalogueH.name') || this.headerItem.name;
  }

  _shouldChildrenExpand() {
    return (this.mode === this.constructor.MODE.TASKLIST && this.level === 1) ||
      (this.mode === this.constructor.MODE.MPMS && this.level === 2);
  }

  dueCount(items) {
    let count = 0;
    const counts = _.countBy(items, item => _.get(item, 'dueStatusH.name'));
    const STATUSES = this._AppSettings.OVERALL_STATUS_NAMES;
    [
      _.lowerCase(STATUSES.OVERDUE),
      _.lowerCase(STATUSES.IMMINENT),
      _.lowerCase(STATUSES.DUE_SOON),
    ].forEach((status) => {
      const statusCount = counts[status];
      if (statusCount != null) {
        count += statusCount;
      }
    });
    return count;
  }

  static MODE = {
    TASKLIST: 'tasklist',
    MPMS: 'mpms',
    SERVICE_SCHEDULE: 'serviceSchedule',
  }
}
