import angular from 'angular';
import cdCardAccordionComponent from './cd-card-accordion.component';
import uiRouter from 'angular-ui-router';

export default angular.module('cdCardAccordion', [
  uiRouter,
])
.component('cdCardAccordion', cdCardAccordionComponent)
.name;
