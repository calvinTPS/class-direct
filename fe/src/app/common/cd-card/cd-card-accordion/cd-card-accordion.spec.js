/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import CdCardAccordionComponent from './cd-card-accordion.component';
import CdCardAccordionController from './cd-card-accordion.controller';
import CdCardAccordionModule from './';

describe('CdCardAccordion', () => {
  let $rootScope,
    $compile,
    $element,
    makeController;

  beforeEach(window.module(
    CdCardAccordionModule,
    angularMaterial,
  ));

  beforeEach(window.module(($provide) => {
    const mockUtilsFactory = {
      getTabularAction: () => {},
    };
    const mockTranslateFilter = value => value;
    const mock$Scope = {
      $broadcast: (event, data) => true,
      $apply: () => null,
    };

    const mockUserService = {
      getCurrentUser: () => Promise.resolve(),
    };

    $provide.value('AppSettings', AppSettings);
    $provide.value('UserService', mockUserService);
    $provide.value('utils', mockUtilsFactory);
    $provide.value('translateFilter', mockTranslateFilter);
    $provide.value('safeTranslateFilter', mockTranslateFilter);
    $provide.value('$scope', mock$Scope);
    $provide.value('$element', angular.element('<div></div>'));
    $provide.value('ViewService', { getRegisteredTemplate: (x, y) => function a() {} });
  }));

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        bindings.headerItem = {}; // eslint-disable-line
        const controller = $componentController('cdCardAccordion', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
      expect(controller.currentUser).toBeUndefined();

      const controller2 = makeController({ mode: 'serviceSchedule' });
      expect(controller2.currentUser).toEqual({});
    });

    it.async('_getCurrentUser, gets the current user from the User Service', async () => {
      const controller = makeController({ mode: 'serviceSchedule' });
      spyOn(controller._UserService, 'getCurrentUser').and.returnValue({ id: 1 });
      spyOn(controller._$scope, '$apply');

      await controller._getCurrentUser();
      expect(controller._UserService.getCurrentUser).toHaveBeenCalled();
      expect(controller.currentUser).toEqual({ id: 1 });
      expect(controller._$scope.$apply).toHaveBeenCalled();
    });

    it('sets and returns isExpanded value correctly', () => {
      const controller = makeController();
      controller.isExpanded = true;
      expect(controller.isExpanded).toEqual(true);
      controller.isExpanded = false;
      expect(controller.isExpanded).toEqual(false);
    });

    it('returns correct overdue status count', () => {
      const controller = makeController();
      const data = [
        { dueStatusH: { name: 'due' } },
        { dueStatusH: { name: 'overdue' } },
        { dueStatusH: { name: 'overdue' } },
        { dueStatusH: { name: 'due-soon' } },
      ];
      expect(controller.dueCount(data)).toEqual(2);
    });

    it('onClick(), correctly changes the isExpanded value and broadcasts to child accordions', () => {
      const controller = makeController({
        isExpanded: true,
        level: 1,
      });
      spyOn(controller._$scope, '$broadcast');
      spyOn(controller, '_shouldChildrenExpand').and.returnValue(true);

      expect(controller.headerItem.isExpanded).toEqual(true);
      controller.onClick();
      expect(controller.headerItem.isExpanded).toEqual(false);
      expect(controller._$scope.$broadcast).not.toHaveBeenCalled();

      controller.onClick();
      expect(controller.headerItem.isExpanded).toEqual(true);
      expect(controller._$scope.$broadcast).toHaveBeenCalledWith(
        AppSettings.EVENTS.EXPAND_ACCORDION,
        { group: controller.group },
      );
    });

    it('_shouldChildrenExpand(), return true or false on whether all the children should be expanded', () => {
      const controller = makeController({
        isExpanded: true,
      });

      // Should be true for mode = tasklist and level = 1 OR
      // mode = mpms and level = 2

      controller.mode = 'someMode';
      controller.level = 999;
      expect(controller._shouldChildrenExpand()).toEqual(false);

      controller.mode = 'tasklist';
      controller.level = 999;
      expect(controller._shouldChildrenExpand()).toEqual(false);

      controller.mode = 'tasklist';
      controller.level = 2;
      expect(controller._shouldChildrenExpand()).toEqual(false);

      controller.mode = 'tasklist';
      controller.level = 1;
      expect(controller._shouldChildrenExpand()).toEqual(true);

      controller.mode = 'mpms';
      controller.level = 999;
      expect(controller._shouldChildrenExpand()).toEqual(false);

      controller.mode = 'mpms';
      controller.level = 1;
      expect(controller._shouldChildrenExpand()).toEqual(false);

      controller.mode = 'mpms';
      controller.level = 2;
      expect(controller._shouldChildrenExpand()).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdCardAccordionComponent;

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdCardAccordionController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element(
        '<cd-card-accordion data-header-item="{}" foo="bar"><cd-card-accordion/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('CdCardAccordion');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
