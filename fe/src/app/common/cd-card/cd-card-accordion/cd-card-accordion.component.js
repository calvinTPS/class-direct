import './cd-card-accordion.scss';
import controller from './cd-card-accordion.controller';

export default {
  bindings: {
    group: '<', // hierarchy group
    headerItem: '<',
    isExpanded: '<',
    level: '<', // level in hierarchy/multiple level
    mode: '@?', // - Sometimes used to control the template
    shouldExpandChildren: '<', // children in hierarchy
    templateLocation: '<?',
    theme: '@?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
};
