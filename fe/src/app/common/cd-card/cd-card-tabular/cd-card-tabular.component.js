import './cd-card-tabular.scss';
import controller from './cd-card-tabular.controller';
import template from './cd-card-tabular.pug';

export default {
  bindings: {
    action: '<?',
    currentUser: '<?',
    item: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
