import angular from 'angular';
import cdCardTabularComponent from './cd-card-tabular.component';
import uiRouter from 'angular-ui-router';

export default angular.module('cdCardTabular', [
  uiRouter,
])
.component('cdCardTabular', cdCardTabularComponent)
.name;
