import * as _ from 'lodash';
import Base from 'app/base';

export default class CdCardTabularController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    PermissionsService,
    utils,
  ) {
    super(arguments);
  }

  onClickContainer($event) {
    if (!this.userHasPermissionToClick || this.clickArea === 'icon') {
      $event.preventDefault();
    } else if (this.handleClick) {
      this.handleClick('parent');
    }
  }

  onClickIcon() {
    if (this.clickArea === 'icon') {
      this.handleClick('icon');
    }
  }

  $onInit() {
    this.goTo = '-';
    this.hasGap = false;

    if (this.action) {
      const tabularAction = this._utils.getTabularAction(this.action, this.item);
      this.mode = _.get(tabularAction, 'click') ? 'click' : 'sref';
      this.icon = _.get(tabularAction, 'customIcon');
      this.clickArea = _.get(tabularAction, 'clickArea');
      this.hasGap = _.get(tabularAction, 'hasGap');

      if (this.mode === 'click') {
        this.handleClick = tabularAction;
      } else {
        this.goTo = tabularAction;
      }
    }
  }

  get actionIcon() {
    if (this.mode === 'sref') {
      return 'right';
    } else if (this.mode === 'click') {
      return this.icon || 'right';
    }
    return null;
  }

  get actionClass() {
    return {
      'gap-left': this.hasGap && this.clickArea === 'icon' && this._$mdMedia('gt-sm'),
      'gap-top': this.hasGap && this.clickArea === 'icon' && !this._$mdMedia('gt-sm'),
    };
  }

  get userHasPermissionToClick() {
    // If currentUser in not passed in, assume we do not care who the user is
    if (_.isUndefined(this.currentUser)) return true;

    return this._PermissionsService.canViewChecklistTasklist(this.currentUser);
  }

  get isContainerClickable() {
    return !!this.action && this.clickArea !== 'icon';
  }

  get canViewCTA() {
    return !!this.action && this.userHasPermissionToClick;
  }
}
