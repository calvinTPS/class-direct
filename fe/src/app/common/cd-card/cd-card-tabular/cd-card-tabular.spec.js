/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import angularMaterial from 'angular-material';
import CdCardTabularComponent from './cd-card-tabular.component';
import CdCardTabularController from './cd-card-tabular.controller';
import CdCardTabularModule from './';
import CdCardTabularTemplate from './cd-card-tabular.pug';
import PermissionsService from 'app/services/permissions.service';

describe('CdCardTabular', () => {
  let $rootScope,
    $compile,
    mockUtils,
    makeController;

  beforeEach(window.module(
    angularMaterial,
    CdCardTabularModule,
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    mockUtils = {
      getTabularAction: () => {},
    };

    window.module(($provide) => {
      $provide.value('$mdMedia', () => false);
      $provide.value('utils', mockUtils);
      $provide.value('PermissionsService', new PermissionsService());
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('cdCardTabular', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      spyOn(mockUtils, 'getTabularAction').and.callFake(() => {});
      const action = { ngClick: () => {} };
      const item = { foo: 'bar' };
      const controller = makeController({ action, item });

      expect(mockUtils.getTabularAction).toHaveBeenCalledWith(action, item);
      expect(controller.icon).not.toBeDefined();
    });

    it('onClickContainer(), should correctly handle how the click based on conditions', () => {
      const $event = { preventDefault: () => true };
      spyOn($event, 'preventDefault');

      const controller = makeController();
      controller.currentUser = {}; // makes controller.userHasPermissionToClick -> false
      controller.clickArea = 'icon';
      controller.handleClick = () => true;
      spyOn(controller, 'handleClick');

      controller.onClickContainer($event);
      expect($event.preventDefault).toHaveBeenCalled();
      expect(controller.handleClick).not.toHaveBeenCalled();

      controller.currentUser = { id: 1 }; // makes controller.userHasPermissionToClick -> false
      controller.clickArea = 'something not icon';
      controller.onClickContainer($event);
      expect($event.preventDefault).toHaveBeenCalledTimes(1);
      expect(controller.handleClick).toHaveBeenCalledTimes(1);
    });

    it('onClickIcon(), should correctly handle clicking of CTA icon', () => {
      const controller = makeController();
      controller.clickArea = 'xxxxx';
      controller.handleClick = () => true;
      spyOn(controller, 'handleClick');

      controller.onClickIcon();
      expect(controller.handleClick).not.toHaveBeenCalled();

      controller.clickArea = 'icon';
      controller.onClickIcon();
      expect(controller.handleClick).toHaveBeenCalled();
    });

    it('actionIcon() should return the correct action icon', () => {
      const controller = makeController();
      expect(controller.actionIcon).toEqual(null);

      controller.mode = 'sref';
      expect(controller.actionIcon).toEqual('right');

      controller.mode = 'click';
      expect(controller.actionIcon).toEqual('right');

      controller.icon = 'download';
      expect(controller.actionIcon).toEqual('download');
    });

    it('actionClass() should return the correct action class', () => {
      const controller = makeController();
      expect(controller.actionClass).toEqual({ 'gap-left': false, 'gap-top': false });

      controller.hasGap = true;
      controller.clickArea = 'icon';
      expect(controller.actionClass).toEqual({ 'gap-left': false, 'gap-top': true });

      controller._$mdMedia = () => true;
      expect(controller.actionClass).toEqual({ 'gap-left': true, 'gap-top': false });
    });

    it('isContainerClickable() should return the correct value', () => {
      const controller = makeController();
      expect(controller.isContainerClickable).toBe(false);

      controller.action = {};
      controller.clickArea = 'icon';
      expect(controller.isContainerClickable).toBe(false);

      controller.action = null;
      controller.clickArea = 'xxxx';
      expect(controller.isContainerClickable).toBe(false);

      controller.action = {};
      controller.clickArea = 'xxxx';
      expect(controller.isContainerClickable).toBe(true);
    });

    it('userHasPermissionToClick() should correctly say whether the element is clickable', () => {
      const controller = makeController();

      // controller.currentUser is not defined
      expect(controller.userHasPermissionToClick).toEqual(true);

      controller.currentUser = { isEquasisThetis: false };
      expect(controller.userHasPermissionToClick).toEqual(true);

      controller.currentUser = { isEquasisThetis: true };
      expect(controller.userHasPermissionToClick).toEqual(false);
    });

    it('canViewCTA(), correctly tells whether the CTA can be seee', () => {
      const controller = makeController();

      // 1) controller.userHasPermissionToClick = false because controller.currentUser is undefined
      // 2) controller.action is undefined
      expect(controller.canViewCTA).toEqual(false);

      controller.action = {};
      expect(controller.canViewCTA).toEqual(true);

      controller.currentUser = { isEquasisThetis: true };
      expect(controller.canViewCTA).toEqual(false);

      controller.currentUser = { isEquasisThetis: false };
      expect(controller.canViewCTA).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdCardTabularComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CdCardTabularTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdCardTabularController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<cd-card-tabular item="item"><cd-card-tabular/>');
      element = $compile(element)(scope);
      scope.$apply();

      controller = element.controller('CdCardTabular');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
