import angular from 'angular';
import cdCardGanttComponent from './cd-card-gantt.component';
import modalCloser from './modal-closer.directive';
import uiRouter from 'angular-ui-router';

export default angular.module('cdCardGantt', [
  uiRouter,
])
.component('cdCardGantt', cdCardGanttComponent)
.directive('modalCloser', modalCloser)
.name;
