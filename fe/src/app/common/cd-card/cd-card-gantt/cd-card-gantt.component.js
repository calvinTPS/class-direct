import './cd-card-gantt.scss';
import controller from './cd-card-gantt.controller';
import template from './cd-card-gantt.pug';

export default {
  bindings: {
    asset: '<',
    currentUser: '<?',
    ganttPadding: '<?', // gives the huge padding that aligns with the timeline
    iconName: '@?',
    isShowingTitle: '<?',
    isShowingTodayPip: '<?',
    isShowingTooltip: '<?',
    items: '<',
    maxDate: '<',
    minDate: '<',
    showDetails: '<?',
    showRangeDates: '<?',
    showPipDates: '<?',
    title: '@?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
