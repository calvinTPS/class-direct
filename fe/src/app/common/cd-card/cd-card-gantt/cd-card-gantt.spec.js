/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import * as _ from 'lodash';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import CdCardGanttComponent from './cd-card-gantt.component';
import CdCardGanttController from './cd-card-gantt.controller';
import CdCardGanttModule from './';
import CdCardGanttTemplate from './cd-card-gantt.pug';
import PermissionsService from 'app/services/permissions.service';
import utils from 'app/common/utils/utils.factory';

describe('CdCardGantt', () => {
  let $rootScope, $compile, $mdDialog, makeController, mockData, ServiceService, mockViewService;

  const services = [
    {
      acomment: 'I am a mock Classification service',
      productFamilyName: 'Classification',
      serviceCatalogue: { id: 1 },
      serviceCatalogueH: {
        id: 1,
        name: 'S1',
        productCatalogue: {
          name: 'P1',
          productType: {
            id: 1,
          },
        },
      },
      dueDateEpoch: Date.parse('2018-06-11T00:00:00Z'),
      displayOrder: 1,
      isSelected: true,
      isPostponed: true,
    },
    {
      acomment: 'I am a mock Statutory service',
      productFamilyName: 'Statutory',
      serviceCatalogue: { id: 2 },
      serviceCatalogueH: {
        id: 2,
        name: 'S1',
        productCatalogue: {
          name: 'P2',
          productType: {
            id: 3,
          },
        },
      },
      dueDateEpoch: Date.parse('2018-06-11T00:00:00Z'),
      displayOrder: 2,
      isSelected: true,
      isPostponed: true,
    }, // same ID
    {
      acomment: 'I am a mock Statutory service',
      productFamilyName: 'Statutory',
      serviceCatalogue: { id: 3 },
      serviceCatalogueH: {
        id: 3,
        name: 'S2',
        productCatalogue: {
          name: 'P3',
          productType: {
            id: 3,
          },
        },
      },
      dueDateEpoch: Date.parse('2020-02-11T00:00:00Z'),
      isSelected: true,
      isPostponed: true,
    },
    {
      acomment: 'I am a mock MMS service',
      productFamilyName: 'MMS',
      serviceCatalogue: { id: 4 },
      serviceCatalogueH: {
        id: 4,
        name: 'S3',
        productCatalogue: {
          name: 'P4',
          productType: {
            id: 2,
          },
        },
      },
      dueDateEpoch: Date.parse('2021-11-11T00:00:00Z'),
      isSelected: true,
      isPostponed: true,
    },
    {
      acomment: 'I am a mock MMS service',
      productFamilyName: 'MMS',
      serviceCatalogue: { id: 6 },
      serviceCatalogueH: { id: 6,
        name: 'S4',
        productCatalogue: {
          name: 'P5',
          productType: {
            id: 2,
          },
        },
      },
      dueDateEpoch: Date.parse('2022-03-10T00:00:00Z'),
      isSelected: true,
      isPostponed: true,
    },
  ];

  const codicils = [
    {
      id: 25,
      title: 'GMDSS for Australian Ships',
      codicilType: 'COC',
      description: 'AMSA ORDER 27 - ALL APPLICABLE VESSELS UNDER 300 GRT TO BE FULLY GMDSS COMPLIANT FROM 01/07/02',
      imposedDateEpoch: 1441756800000,
      dueDateEpoch: 1481500800000,
    },
    {
      id: 26,
      title: 'No Description Available',
      codicilType: 'COC',
      description: 'No Narrative Available',
      imposedDateEpoch: 1441756800000,
      dueDateEpoch: 1481500800000,
    },
    {
      id: 24,
      title: 'CoC for Defect Title',
      codicilType: 'COC',
      description: 'Codicil CoC for Defect',
      imposedDateEpoch: 1479772800000,
      dueDateEpoch: 1479772800000,
    },
  ];

  beforeEach(window.module(
    CdCardGanttModule,
    angularMaterial,
  ));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockServiceService = () => {};
    mockViewService = () => {};

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('PermissionsService', new PermissionsService());
      $provide.value('ServiceService', mockServiceService);
      $provide.value('ViewService', mockViewService);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('utils', utils());
    });

    mockData = [{
      label: '',
      status: {
        label: '',
        type: '',
      },
      dueStatusH: {
        name: 'Overdue',
      },
      postponementDate: null,
      postponementDateEpoch: Date.parse('2016-05-30'),
      dueDate: '2016-01-01',
      dueDateEpoch: Date.parse('2016-01-01'),
      lowerRangeDate: '2015-06-01',
      lowerRangeDateEpoch: Date.parse('2015-06-01'),
      upperRangeDate: '2016-10-30',
      upperRangeDateEpoch: Date.parse('2016-10-30'),
    }];
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('cdCardGantt', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    const minDateStr = '2015-01-01';
    const maxDateStr = '2017-01-01';
    const nowDateStr = '2016-01-01';
    const minDateObj = new Date(minDateStr);
    const maxDateObj = new Date(maxDateStr);
    const nowTimestamp = Date.now();
    const totalMonths = 25;

    beforeEach(() => {
      controller = makeController({
        items: mockData,
        minDate: minDateObj,
        maxDate: maxDateObj,
      });

      // Override now for reliable test.
      controller.nowTimestamp = nowTimestamp;
    });

    it('initializes just fine', () => {
      expect(controller.item).not.toBeNull();
      expect(controller.pips.length).toEqual(totalMonths);

      controller.minDate = new Date('2016-01-01');
      $rootScope.$broadcast(AppSettings.EVENTS.UPDATE_DATE_PICKER);

      const newTotalMonths = 13;
      expect(controller.pips.length).toEqual(newTotalMonths);
    });

    it('Formats the date to correct format', () => {
      expect(controller._formatDate(minDateObj)).toEqual('January 2015');
      expect(controller._formatDate(maxDateObj)).toEqual('January 2017');
    });

    it('returns the timeline date with correct format', () => {
      expect(controller.timelineStartDate).toEqual('January 2015');
      expect(controller.timelineEndDate).toEqual('January 2017');
    });

    it('calculates the correct % value', () => {
      const theDate = Date.parse('2016-01-01');
      const percentage = controller._getDatePercent(theDate);
      // 50 is calculated using formula
      // Math.round(((theDate - minDate) / (maxDate - minDate)) * 100)
      expect(percentage).toEqual(49.93160054719562);
    });

    it('returns the correct % css value position for dot', () => {
      // const theDate = Date.parse('2016-01-01');
      const theDate = Date.parse('2016-05-30');
      const percentage = controller._getDatePercent(theDate);
      expect(controller.positionForDot(mockData[0])).toEqual(percentage);
    });

    it('(when postponement date is null) returns the correct % css value position for dynamic dot position', () => {
      const data = _.cloneDeep(mockData);
      const targetDate = '2016-05-30';
      data.postponementDateEpoch = null;
      data.dueDateEpoch = targetDate;

      const ourController = makeController({
        items: data,
        minDate: minDateObj,
        maxDate: maxDateObj,
      });

      const percentage = ourController._getDatePercent(Date.parse(targetDate));
      expect(ourController.positionForDot(data[0])).toEqual(percentage);
    });

    it('(when postponement date is NOT null) returns the correct % css value position for dynamic dot position', () => {
      const data = _.cloneDeep(mockData);
      const targetDate = '2016-05-30';
      data.postponementDateEpoch = targetDate;
      data.dueDateEpoch = null;

      const ourController = makeController({
        items: data,
        minDate: minDateObj,
        maxDate: maxDateObj,
      });

      const percentage = ourController._getDatePercent(Date.parse(targetDate));
      expect(ourController.positionForDot(data[0])).toEqual(percentage);
    });

    it('returns the correct % css value position for the today', () => {
      const percentage = controller._getDatePercent(controller.nowTimestamp);
      expect(controller.positionForToday).toEqual(percentage);
    });

    it('returns the correct % css value position for dot range from', () => {
      let percentage;
      if (controller.items[0].lowerRangeDate) {
        percentage = controller._getDatePercent(controller.items[0].lowerRangeDateEpoch);
      } else {
        percentage = controller.positionForDot(controller.items[0]);
      }
      if (percentage < 0) {
        percentage = 0;
      }
      const func = controller._positionForDotRangeFrom(controller.items[0]);
      expect(func).toEqual(percentage);
    });

    it('returns the correct % css value position for dot range to', () => {
      let percentage;
      if (controller.items[0].upperRangeDate) {
        percentage = controller._getDatePercent(controller.items[0].upperRangeDateEpoch);
      } else {
        percentage = controller.positionForDot(controller.items[0]);
      }
      if (percentage < 0) {
        percentage = 0;
      }
      const func = controller._positionForDotRangeTo(controller.items[0]);
      expect(func).toEqual(percentage);
    });

    it('returns the correct % css value position for grouped dot', () => {
      controller.nowTimestamp = Date.parse('2016-01-01'); // positionOfToday => 50
      expect(controller._getGroupedDatePercent(49)).toEqual(40);
      expect(controller._getGroupedDatePercent(100)).toEqual(99);
      expect(controller._getGroupedDatePercent(0)).toEqual(1);
      expect(controller._getGroupedDatePercent(50.5)).toEqual(50);
      expect(controller._getGroupedDatePercent(55)).toEqual(50);
      expect(controller._getGroupedDatePercent(60.5)).toEqual(60);
      expect(controller._getGroupedDatePercent(65)).toEqual(60);
    });

    it('openModal() should open up modal', () => {
      spyOn($mdDialog, 'show');

      controller.showDetails = true;
      controller.openModal();
      expect($mdDialog.show).not.toHaveBeenCalled();

      controller.showDetails = false;
      controller.openModal();
      expect($mdDialog.show).toHaveBeenCalled();
    });

    it('_groupedRiders() should group the items', () => {
      const mockServiceStructure = { riders: services };
      expect(controller._groupedRiders(mockServiceStructure).length).toEqual(3);

      const groupedCodicils = controller._groupedRiders({ riders: codicils });
      expect(groupedCodicils[0].productName).toEqual('cd-notes-and-actions');
      expect(groupedCodicils[0].details).toEqual(codicils);
    });

    it('dueStatusClass should return the correct class', () => {
      const mockSingleItemStatus = { dueStatus: 'overdue' };
      expect(controller.dueStatusClass(mockSingleItemStatus)).toEqual('overdue');
      const mockMultipleItemStatuses1 = {
        id: 1,
        riders: [
          { dueStatus: 'imminent' },
          { dueStatus: 'due soon' },
          { dueStatus: 'not due' },
        ],
      };
      expect(controller.dueStatusClass(mockMultipleItemStatuses1)).toEqual('imminent');
      const mockMultipleItemStatuses2 = {
        id: 2,
        riders: [
          { dueStatus: 'not due' },
          { dueStatus: 'overdue' },
        ],
      };
      expect(controller.dueStatusClass(mockMultipleItemStatuses2)).toEqual('overdue');
    });

    it('_setPostponementProperty() should return the correct value', () => {
      const postponement = controller._setPostponementProperty(services);
      expect(postponement.hasPostponedService).toBeTruthy();
      expect(postponement.postponedServiceCount).toEqual(5);
      const services1 = _.cloneDeep(services);
      services1[0].isPostponed = false;
      services1[2].isPostponed = false;
      const postponement1 = controller._setPostponementProperty(services1);
      expect(postponement1.hasPostponedService).toBeTruthy();
      expect(postponement1.postponedServiceCount).toEqual(3);
    });

    it('hasDotInteraction() should return the correct value', () => {
      expect(controller.hasDotInteraction()).toEqual(true);

      controller.isShowingTooltip = true;
      controller.showDetails = true;
      expect(controller.hasDotInteraction()).toEqual(true);

      controller.isShowingTooltip = false;
      controller.showDetails = true;
      expect(controller.hasDotInteraction()).toEqual(false);

      controller.isShowingTooltip = false;
      controller.showDetails = false;
      expect(controller.hasDotInteraction()).toEqual(true);
    });

    describe('tooltip', () => {
      const mockSingleItem = {
        id: 1,
        riders: [
          { dueDate: '2017-05-08T00:00:00Z' },
        ],
      };

      const mockGroupedItems = {
        id: 2,
        riders: [
          { dueDate: '2017-05-08T00:00:00Z' },
          { dueDate: '2015-01-08T00:00:00Z' },
          { dueDate: '2016-02-10T10:00:00Z' },
          { dueDate: '2015-01-08T10:00:00Z' },
        ],
      };

      it('toggleDotTooltip() should show or hide tooltip correctly', () => {
        // single item
        let item = _.clone(mockSingleItem);

        controller.toggleDotTooltip(item, true);
        expect(item.showDotTooltip).toEqual(true);
        expect(item.showGroupedDotTooltip).not.toBeDefined();

        controller.toggleDotTooltip(item, false);
        expect(item.showDotTooltip).toEqual(false);
        expect(item.showGroupedDotTooltip).not.toBeDefined();

        // grouped items
        item = _.clone(mockGroupedItems);

        controller.toggleDotTooltip(item, true);
        expect(item.showGroupedDotTooltip).toEqual(true);
        expect(item.showDotTooltip).not.toBeDefined();

        controller.toggleDotTooltip(item, false);
        expect(item.showGroupedDotTooltip).toEqual(false);
        expect(item.showDotTooltip).not.toBeDefined();
      });

      it('groupedDotMinMaxDate() should return the min or max date in grouped items correctly', () => {
        // single item
        let item = _.clone(mockSingleItem);

        expect(controller.groupedDotMinMaxDate(item, 'min')).toEqual('');

        // grouped items
        item = _.clone(mockGroupedItems);
        expect(controller.groupedDotMinMaxDate(item, 'min')).toEqual('2015-01-08T00:00:00Z');
        expect(controller.groupedDotMinMaxDate(item, 'max')).toEqual('2017-05-08T00:00:00Z');
      });

      it('canViewCTA(), correctly tells whether the CTA can be seee', () => {
        // controller.currentUser = undefined
        expect(controller.canViewCTA).toEqual(true);

        controller.currentUser = { isEquasisThetis: true };
        expect(controller.canViewCTA).toEqual(false);

        controller.currentUser = { isEquasisThetis: false };
        expect(controller.canViewCTA).toEqual(true);
      });

      it('setGroupedItem should create grouped items based on services', () => {
        spyOn(controller, 'createGroup');
        controller._setGroupedItems();
        expect(controller.createGroup).toHaveBeenCalled();

        const mockData1 = {
          label: '',
          status: {
            label: '',
            type: '',
          },
          dueStatusH: {
            name: 'Overdue',
          },
          postponementDate: null,
          postponementDateEpoch: Date.parse('2016-05-30'),
          dueDate: '2016-01-01',
          dueDateEpoch: Date.parse('2016-01-01'),
          lowerRangeDate: '2015-06-01',
          lowerRangeDateEpoch: Date.parse('2015-06-01'),
          upperRangeDate: '2016-10-30',
          upperRangeDateEpoch: Date.parse('2016-10-30'),
          isPostponed: 'true',
        };

        const groupedItems = [{
          label: '',
          status: {
            label: '',
            type: '',
          },
          dueStatusH: {
            name: 'Overdue',
          },
          postponementDate: null,
          postponementDateEpoch: Date.parse('2016-05-30'),
          dueDate: '2016-01-01',
          dueDateEpoch: Date.parse('2016-01-01'),
          lowerRangeDate: '2015-06-01',
          lowerRangeDateEpoch: Date.parse('2015-06-01'),
          upperRangeDate: '2016-10-30',
          upperRangeDateEpoch: Date.parse('2016-10-30'),
          isPostponed: 'true',
          postponement: Object({ hasPostponedService: 'true' }),
        }];
        const controller1 = makeController({
          items: mockData1,
          minDate: minDateObj,
          maxDate: maxDateObj,
        });
        spyOn(controller1, 'createGroup');
        controller1._setGroupedItems();
        expect(controller1.createGroup).not.toHaveBeenCalled();
        expect(controller1._groupedItems).toEqual(groupedItems);
      });

      it('createGroup should populate item position', () => {
        spyOn(controller, '_getGroupedDatePercent');
        controller.createGroup();
        expect(controller._getGroupedDatePercent).toHaveBeenCalled();
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdCardGanttComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CdCardGanttTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdCardGanttController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<cd-card-gantt items="items" min-date="minDate" max-date="maxDate"><cd-card-gantt/>');
      scope.items = mockData;
      scope.minDate = new Date('2017');
      scope.maxDate = new Date('2020');
      scope.$apply();
      element = $compile(element)(scope);

      controller = element.controller('CdCardGantt');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
