import * as _ from 'lodash';
import Base from 'app/base';
import dateHelper from 'app/common/helpers/date-helper';
import fecha from 'fecha';
import GeneralModalController from 'app/common/dialogs/general-modal.controller';
import modalTemplate from './cd-card-gantt-modal.pug';

export default class CdCardGanttController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $rootScope,
    AppSettings,
    PermissionsService,
    ServiceService,
    utils,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    // Assign it to this.nowTimestamp so that test can override it
    this.nowTimestamp = Date.now(); // timestamp
    // Show today pip as true for default value
    // - A pip is the | line
    this.isShowingTodayPip = _.isUndefined(this.isShowingTodayPip) ? true : this.isShowingTodayPip;
    this._months = fecha.i18n.monthNames;
    this.groups = {};
    // set the services and codicils for gantt chart
    this._setGroupedItems();
    this._pips = dateHelper.generatePips(this.minDate, this.maxDate);
    this._$rootScope.$on(this._AppSettings.EVENTS.UPDATE_DATE_PICKER, () => {
      this._pips = dateHelper.generatePips(this.minDate, this.maxDate);
    });
  }

  get minDateTimestamp() {
    return Date.parse(this.minDate);
  }

  get maxDateTimestamp() {
    return Date.parse(this.maxDate);
  }

  /**
   * Calculate % to range position of a EPOCH date
   * @param  {timestampEpoch} min
   * @param  {timestampEpoch} max
   * @param  {timestampEpoch} date
   */
  _getDatePercent = date =>
    !dateHelper.inRange(date, this.minDateTimestamp, this.maxDateTimestamp) ? -100 :
    ((date - this.minDateTimestamp) / (this.maxDateTimestamp - this.minDateTimestamp)) * 100;

  dynamicDotPosition(item) {
    return _.isNumber(item.position) ? item.position :
      this._getDatePercent(item.postponementDateEpoch || item.dueDateEpoch);
  }

  // floor the item dot position to group the dot in interval position of 10
  _getGroupedDatePercent = (oldDatePercent) => {
    let groupedDatePercent = _.floor(oldDatePercent, -1);

    // Prevent not overdue items to be displayed before today's bar position and vice versa
    // example today's position is 32%. oldDatePercent is 36%. The item is not due yet.
    // flooring oldDatePercent will get groupedDatePercent of 30%.
    // thus add 10% to group the item with the next grouped dots
    if (groupedDatePercent < this.positionForToday && oldDatePercent >= this.positionForToday) {
      groupedDatePercent += 10;
    }

    //  to make sure the dot is display within dot pane
    if (groupedDatePercent === 100) {
      groupedDatePercent -= 1;
    } else if (groupedDatePercent === 0) {
      groupedDatePercent += 1;
    }

    return groupedDatePercent;
  }

  dueStatusClass = (item) => {
    // if single item
    if (item.dueStatus) {
      return _.kebabCase(item.dueStatus);
    }

    const status = this._AppSettings.OVERALL_STATUS_NAMES;
    const itemStatusList = _.map(item.riders, rider => _.lowerCase(rider.dueStatus));
    // if multiple items, prioritise overdue, imminent, due soon, else not due
    if (_.includes(itemStatusList, status.OVERDUE.toLowerCase())) {
      return _.kebabCase(status.OVERDUE);
    }
    if (_.includes(itemStatusList, status.IMMINENT.toLowerCase())) {
      return _.kebabCase(status.IMMINENT);
    }
    if (_.includes(itemStatusList, status.DUE_SOON.toLowerCase())) {
      return _.kebabCase(status.DUE_SOON);
    }
    return _.kebabCase(status.NOT_DUE);
  }

  // gets us a % value to give as the element's left CSS property
  positionForDot = (item) => {
    const verifiedDate = item.postponementDateEpoch == null ? item.dueDateEpoch :
      item.postponementDateEpoch;
    return this._getDatePercent(verifiedDate);
  }

  _positionForDotRangeFrom = (item) => {
    if (!item.lowerRangeDate) {
      return this.positionForDot;
    }

    // _getDatePercent returns -100 if upperRangeDate is not in between display date range
    const leftRangePosition = this._getDatePercent(item.lowerRangeDateEpoch);

    //  if lowerRangeDate is out of display range, clip it's position to 0
    return (leftRangePosition < 0) ? 0 : leftRangePosition;
  }

  _positionForDotRangeTo = (item) => {
    if (!item.upperRangeDate) {
      return this.positionForDot;
    }
    // _getDatePercent returns -100 if upperRangeDate is not in between display date range
    const rightRangePosition = this._getDatePercent(item.upperRangeDateEpoch);

    //  if upperRangeDate is out of display range, clip it's position to 100
    return (rightRangePosition < 0) ? 0 : rightRangePosition;
  }

  /*
  * I return the object necessary to position the rectangular bar that signifies range date
  */
  getRangeDateStyle(item) {
    return {
      left: `${this._positionForDotRangeFrom(item)}%`,
      right: `${100 - this._positionForDotRangeTo(item)}%`,
    };
  }

  getRangeDateStyleLeft(item) {
    return this._positionForDotRangeFrom(item);
  }

  getRangeDateStyleRight(item) {
    return 100 - this._positionForDotRangeTo(item);
  }

  // gets us a % value to give as the element's left CSS property
  get positionForToday() {
    return this._getDatePercent(this.nowTimestamp);
  }

  // Create a string representation of the date.
  _formatDate(date) {
    return `${this._months[new Date(date).getMonth()]} ${new Date(date).getFullYear()}`;
  }

  // Open up modal box when clicking the dots
  openModal(item, ev) {
    if (!this.showDetails) {
      this._$mdDialog.show({
        bindToController: true,
        clickOutsideToClose: true,
        controller: GeneralModalController,
        controllerAs: 'vm',
        locals: {
          title: this.asset ? this.asset.name : '',
          items: item ? this._groupedRiders(item) : [],
        },
        parent: angular.element(this._$document[0].body),
        targetEvent: ev,
        template: modalTemplate,
      });
    }
  }

  // Used to group the riders item being passed
  _groupedRiders(item) {
    let result = [];
    const isCodicilCollection = items => _.uniq(_.map(items, v => v.codicilType != null))[0];
    const isCodicil = item.riders ? isCodicilCollection(item.riders) : item.codicilType != null;

    if (isCodicil) {
      result.push({
        productName: 'cd-notes-and-actions',
        details: item.riders ? item.riders : [item],
      });
    } else {
      const collection = item.riders ? item.riders : [item];
      // - An array of services is passed in

      const groupIfSameCatalogue = services =>
        _.forEach(services, (service) => {
          // - Only show the primary service
          // - Especially when there are two or more services, eg:
          // - 5 annual services for the next 5 years
          // - These additional service gets
          // - assigned to the parent service attribute
          if (!service.repeatOf) {
            _.set(service, 'myRepeatServices', _.filter(services, service2 =>
              service.serviceCatalogue.id === service2.serviceCatalogue.id
            ));
          }
        });

      // Do not remove repeated services here
      // const removeIfNotPrimary = services =>
      //   // - Remove services that has this attribute
      //   // - Merged to primary service
      //   _.reject(services, service => service.repeatOf);

      // - Helper function to sort Classification
      const sortClassificatonServices = c => _.sortBy(c, [
        'serviceCatalogueH.productGroupH.displayOrder',
        'dueDateEpoch',
        'serviceCatalogueH.displayOrder',
        'occurrenceNumber']
      );

      // - Helper function to sort MMS and Statutory Services
      // -'serviceCatalogueH.productGroupH.displayOrder',
      // - 'serviceCatalogueH.productGroupH.id',
      const sortOtherServices = c => _.sortBy(c, [
        'dueDateEpoch',
        'serviceCatalogueH.displayOrder',
        'occurrenceNumber']
      );

      const advanceSort = (services) => {
        const classificationServices = sortClassificatonServices(_.filter(services, [
          'serviceCatalogueH.productCatalogue.productType.id',
          this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.ID]));

        const statutoryServices = sortOtherServices(_.filter(services, [
          'serviceCatalogueH.productCatalogue.productType.id',
          this._AppSettings.PRODUCT_FAMILY_TYPES.STATUTORY.ID]));

        const mmsServices = sortOtherServices(_.filter(services, [
          'serviceCatalogueH.productCatalogue.productType.id',
          this._AppSettings.PRODUCT_FAMILY_TYPES.MMS.ID]));

        return _.concat(classificationServices, mmsServices, statutoryServices);
      };
      // I will group the services into 3 buckets :
      // Classification, Statutory, MMS
      // Not required to display the classification label
      //
      // Product Groups(Classification label is not needed)
      //    Service Groups
      // MMS
      //    Service Groups
      // Statutory
      //    Service Groups
      const pairsCollection = c => _.toPairs(_.groupBy(c, (service) => {
        if (service.productFamilyName ===
          this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.NAME) {
          return service.productGroupName;
        }
        return service.productFamilyName;
      }));
      const mapCollection = c => _.map(c, r => _.zipObject(['productName', 'details'], r));
      result = _.flow(
        groupIfSameCatalogue,
        advanceSort,
        pairsCollection,
        mapCollection,
      )(collection);
    }
    return result;
  }

  get timelineStartDate() {
    return this._formatDate(this.minDateTimestamp);
  }

  get timelineEndDate() {
    return this._formatDate(this.maxDateTimestamp);
  }

  get pips() {
    return this._pips;
  }

  /**
   * A gantt dot has two types of interactions:
   * 1) Hover triggers a tooltip, if `this.isShowingTooltip` is true.
   * 2) Click triggers a modal, if `this.showDetails` is false.
   *
   * @returns {boolean}
   */
  hasDotInteraction() {
    return this.isShowingTooltip || !this.showDetails;
  }

  /**
   * To show or hide tooltip.
   *
   * @param {object} item     The grouped codicils or services object.
   * @param {boolean} bool    The display status of tooltip.
   */
  toggleDotTooltip(item, bool) {
    /* eslint-disable no-param-reassign */
    if (item.riders.length > 1) {
      item.showGroupedDotTooltip = bool;
    } else {
      item.showDotTooltip = bool;
    }
    /* eslint-enable no-param-reassign */
  }

  /**
   * To find min or max date in grouped codicils or services.
   *
   * @param {object} item     The grouped codicils or services object.
   * @param {string} dateType The type of date. [min | max]
   *
   * @returns {string} date   The date based on dateType.
   */
  groupedDotMinMaxDate(item, dateType) {
    let date = '';

    if (item.riders.length > 1) {
      const datesArray = _.map(item.riders, 'dueDate');

      if (dateType === 'min') {
        date = _.min(datesArray);
      } else if (dateType === 'max') {
        date = _.max(datesArray);
      }
    }
    return date;
  }

  hasPostponedService(item) {
    if (item.riders) {
      return item.postponement.hasPostponedService && item.riders.length;
    }
    return item.postponement.hasPostponedService;
  }

  get tabularAction() {
    return this._utils.getTabularAction(
      this._utils.getServiceCodicilsSrefObject(this.items),
      this.items
    );
  }

  get canViewCTA() {
    // If currentUser in not passed in, assume we do not care who the user is
    if (_.isUndefined(this.currentUser)) return true;

    return this._PermissionsService.canViewChecklistTasklist(this.currentUser);
  }

  _setPostponementProperty(services) {
    const postponedServices = _.filter(services, service => service.isPostponed);
    return {
      postponedServiceCount: postponedServices.length,
      hasPostponedService: !!postponedServices.length,
    };
  }

  get groupedItems() {
    // set will only get called if service schedule gantt chart view is selected
    // this is the solution for changing the service schedule gantt chart based on changed date
    // from the date filter.
    if (this._ViewService.isSurveyScheduleViewToggled) {
      this._setGroupedItems();
    }
    return this._groupedItems;
  }

  _setGroupedItems(value) {
    if (_.isArray(this.items)) {
      this.multiMode = true;
      this.createGroup();
      // Make into array and calculate the position of the group
      this._groupedItems = _.map(this.groups, (group, key) => {
        const averageDate = _.sum(_.map(group, groupObject =>
          groupObject.postponementDateEpoch || groupObject.dueDateEpoch)) / group.length;

        return {
          position: this._getDatePercent(averageDate),
          riders: group,
          postponement: this._setPostponementProperty(group),
        };
      });
    } else {
      // if this.items is NOT an array
      this.multiMode = false;
      if (this.items.myRepeatServices) {
        _.foreach(this.items.myRepeatServices, (service) => {
          _.set(service, 'postponement.hasPostponedService', service.isPostponed);
        });
        this._groupedItems = this.items.myRepeatServices;
      } else {
        _.set(this.items, 'postponement.hasPostponedService', this.items.isPostponed);
        this._groupedItems = [this.items];
      }
      // - Add a postponement attribute to the item for...
    }
  //  this._groupedItems = value
  }

  createGroup() {
    _.each(this.items, (item) => {
      // Position of the item from the beginning of the timeline in percentage
      const finalDate = item.isPostponed ? item.postponementDateEpoch : item.dueDateEpoch;
      const itemPosition = this._getDatePercent(
        finalDate
      );
      _.set(item, 'itemPosition', itemPosition);
      // Group the item belongs to, based on its position
      const group = this._getGroupedDatePercent(itemPosition);
      // Add the group if there is none
      // AND push the item to the approprite group
      if (!this.groups[group]) {
        this.groups[group] = [item];
      } else {
        this.groups[group].push(item);
      }
    });
  }

}
