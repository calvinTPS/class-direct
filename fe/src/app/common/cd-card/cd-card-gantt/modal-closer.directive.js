/* @ngInject */

export default ($timeout, $parse) => ({
  restrict: 'A',
  link: (
    scope,
    element,
    attributes,
  ) => {
    $timeout(() => {
      let fn;
      element.on('click', (e) => {
        if (
          angular.element(e.target).closest('cd-card-tabular')
        ) {
          $timeout(() => {
            fn = $parse(attributes.modalCloser);
            fn(scope);
          });
        }
      });
    });
  },
});
