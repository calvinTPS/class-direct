/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import ServiceModalComponent from './service-modal.component';
import ServiceModalController from './service-modal.controller';
import ServiceModalModule from './service-modal';
import ServiceModalTemplate from './service-modal.pug';

describe('ServiceModal', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    makeController;

  const mockTranslateFilter = value => value;

  beforeEach(window.module(
    ServiceModalModule,
    angularMaterial,
    { translateFilter: mockTranslateFilter },
  ));

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('serviceModal', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('should test cancel() behaviour', () => {
      const controller = makeController();
      spyOn($mdDialog, 'cancel');
      controller.cancel();
      expect($mdDialog.cancel).toHaveBeenCalled();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ServiceModalComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ServiceModalTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ServiceModalController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<service-modal foo="bar"><service-modal/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ServiceModal');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
