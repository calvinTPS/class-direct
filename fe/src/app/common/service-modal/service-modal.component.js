import './service-modal.scss';
import controller from './service-modal.controller';
import template from './service-modal.pug';

export default {
  bindings: {
    actionButton: '@',
    close: '&',
    title: '@',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
