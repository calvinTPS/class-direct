import Base from 'app/base';

export default class ServiceModalController extends Base {
  /* @ngInject */
  constructor(
    $mdDialog,
  ) {
    super(arguments);
  }

  cancel() {
    this._$mdDialog.cancel();
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
