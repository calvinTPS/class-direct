import angular from 'angular';
import serviceModalComponent from './service-modal.component';
import uiRouter from 'angular-ui-router';

export default angular.module('serviceModal', [
  uiRouter,
])
.component('serviceModal', serviceModalComponent)
.name;
