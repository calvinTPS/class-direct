/*
 * Used by DEFECT and STATUTORY DEFICIENCY
 */

import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import CodicilModel from '../codicil/codicil';
import ItemModel from '../item/';
import JobModel from '../job';
import Model from '../model';
import translation from '../../../../../resources/locales/en-GB.json';

export default class DefectModel extends Model {
  get title() {
    if (this._title == null) {
      this._title = this.model.title || this.model.defectTitle;
    }
    return this._title;
  }

  get typeName() {
    return translation[_.get(AppSettings.DEFECTS_META[this.defectType], 'NAME')];
  }

  get defectType() {
    if (this._defectType == null) {
      if (this.model.deficiencyChecklists) {
        this._defectType = AppSettings.DEFECTS_META.SD.ID;
      } else if (this.model.defectCategory || this.model.defectStatusH || this.model.defectStatus) {
        this._defectType = AppSettings.DEFECTS_META.DEFECT.ID;
      }
    }
    return this._defectType;
  }

  get type() {
    return this.defectType;
  }

  get category() {
    if (this._category == null) {
      // use defectCategory.name if available else use defectCategory if its a string
      this._category = _.get(this.model, 'defectCategory.name') ||
        _.get(this.model, 'category.name') ||
        (_.isString(this.model.defectCategory) ? this.model.defectCategory : null);
    }
    return this._category;
  }

  get status() {
    if (this._status == null) {
      // use defectStatusH if available else use status if its a string
      this._status = _.get(this.model, 'defectStatusH.name') ||
        _.get(this.model, 'statusH.name') ||
        (_.isString(this.model.status) ? this.model.status : null);
    }
    return this._status;
  }

  get cocs() {
    const cocs = this.model.cocH;
    if (_.size(cocs) > 0) {
      cocs.forEach((coc, index) => {
        cocs[index] = Reflect.construct(CodicilModel, [coc]);
      });
      return cocs;
    }
    return null;
  }

  get description() {
    if (this._description == null) {
      this._description = this.model.incidentDescription || this.model.defectNarrative || '';
    }
    return this._description;
  }

  get truncatedDescription() {
    if (this._truncatedDescription == null) {
      this._truncatedDescription =
        this.description.substr(0, AppSettings.DEFAULT_PARAMS.TRUNCATED_DESCRIPTION_CHARACTER);
    }

    return this._truncatedDescription;
  }

  get dateOccurred() {
    if (this._dateOccurred == null) {
      this._dateOccurred = this.model.incidentDate || this.model.dateOccured;
    }
    return this._dateOccurred;
  }

  get dateOccurredFormatted() {
    if (this._dateOccurredFormatted == null) {
      this._dateOccurredFormatted = this.incidentDateFormatted ||
        this.dateOccuredFormatted;
    }
    return this._dateOccurredFormatted;
  }


  get dateOccurredEpoch() {
    if (this._dateOccurredEpoch == null && this.dateOccurred) {
      this._dateOccurredEpoch = Date.parse(this.dateOccurred);
    }
    return this._dateOccurredEpoch;
  }

  get items() {
    if (this._items == null) {
      const items = this.model.deficiencyChecklists || this.model.itemsH || this.model.items;
      this._items = _.map(items, item => Reflect.construct(ItemModel, [item]));
    }
    return this._items;
  }

  get jobs() {
    const jobs = this.model.jobsH || this.model.jobs;
    if (this._jobs == null && _.size(jobs) > 0) {
      this._jobs = _.map(jobs, job => Reflect.construct(JobModel, [job]));
    }
    return this._jobs;
  }

  get jobNumbers() {
    return _.map(this.jobs, job => job.jobNumber).join(', ');
  }

  get flagStateConsulted() {
    if (this._flagStateConsulted == null) {
      this._flagStateConsulted = this.model.flagStateConsulted ? 'cd-yes' : 'cd-no';
    }
    return this._flagStateConsulted;
  }

  get imposedDateEpoch() {
    if (this._imposedDateEpoch == null && this.model.imposedDate) {
      this._imposedDateEpoch = this.model.imposedDateEpoch || Date.parse(this.model.imposedDate);
    }
    return this._imposedDateEpoch;
  }

  get dueDateEpoch() {
    if (this._dueDateEpoch == null && this.model.dueDate) {
      this._dueDateEpoch = this.model.dueDateEpoch || Date.parse(this.model.dueDate);
    }
    return this._dueDateEpoch;
  }

  static PAYLOAD_FIELDS = {
    SD_LIST: [
      'id',
      'incidentDate',
      'incidentDescription',
      'title',
      'jobs.id',
      'job.id',
      'deficiencyChecklists.id',
      'deficiencyChecklists.workItem.name',
      'flagStateConsulted',
      'statusH.id',
      'statusH.name',
      'category.id',
      'category.name',
      'cocH.id',
    ],
    SD_DETAIL: [
      'id',
      'incidentDate',
      'incidentDescription',
      'title',
      'jobs.*',
      'job.*',
      'narrative',
      'flagStateConsulted',
      'statusH.id',
      'statusH.name',
      'category.id',
      'category.name',
      'itemsH.*',
      'items.*',
      'deficiencyChecklists.*',
      'cocH.*',
      'statutoryFindings.*',
      'rectificationsH.*',
    ],
    DEFECT_LIST: [
      'id',
      'incidentDate',
      'incidentDescription',
      'title',
      'jobs.id',
      'jobsH.id',
      'job.id',
      'flagStateConsulted',
      'defectStatusH.id',
      'defectStatusH.name',
      'statusH.id',
      'statusH.name',
      'defectStatus.id',
      'defectStatus.name',
      'defectCategory.id',
      'defectCategory.name',
      'cocH.id',
      'itemsH.id',
      'items.id',
    ],
  }

}
