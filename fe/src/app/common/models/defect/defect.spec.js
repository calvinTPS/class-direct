/* global inject */

import * as _ from 'lodash';
import DefectModel from './';

describe('DefectModel', () => {
  let makeModel;

  beforeEach(inject(() => {
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        // eslint-disable-next-line no-param-reassign
        data[index] = Reflect.construct(DefectModel, [rawObject]);
      });
      return data;
    };
  }));

  it('CoC Defects, getters should return correct value', () => {
    const CocDefectMocks = [{
      dateOccured: '10/10/10',
      defectTitle: 'Title of this defect',
      defectCategory: { name: 'Category name' },
      defectNarrative: 'Some incident description',
      items: [{ name: 'Item 1' }, { name: 'Item 2' }, { name: 'Item 3' }],
      status: 'Status name',
      imposedDate: '2014-12-18T00:00:00Z',
      dueDate: '2015-08-01T00:00:00Z',
    }];

    const model = makeModel(_.clone(CocDefectMocks));

    expect(model[0].title).toEqual('Title of this defect');
    expect(model[0].typeName).toEqual('Class defect');
    expect(model[0].type).toEqual('DEFECT');
    expect(model[0].category).toEqual('Category name');
    expect(model[0].status).toEqual('Status name');
    expect(model[0].description).toEqual('Some incident description');
    expect(model[0].truncatedDescription).toEqual('Some incident description');
    expect(model[0].dateOccurred).toEqual('10/10/10');
    expect(model[0].items[0].name).toEqual('Item 1');
    expect(model[0].imposedDateEpoch).toEqual(Date.parse(model[0].imposedDate));
    expect(model[0].dueDateEpoch).toEqual(Date.parse(model[0].dueDate));
  });

  it('Fault Defects, getters should return correct value', () => {
    const FaultDefectMocks = [{
      title: 'Title of this defect 2',
      defectCategory: { name: 'Category name 2' },
      defectStatusH: { name: 'Status name 2' },
      incidentDescription: 'Some incident description 2',
      incidentDate: '12/12/12',
      items: [{ itemName: 'Item 4' }, { itemName: 'Item 5' }, { itemName: 'Item 6' }],
      jobs: [
        {
          id: 1,
          job: { id: 111 },
          jobNumber: '4454',
        },
        {
          id: 2,
          job: { id: 222 },
          jobNumber: '9961',
        },
      ],
    }];

    const model = makeModel(_.clone(FaultDefectMocks));

    expect(model[0].title).toEqual('Title of this defect 2');
    expect(model[0].typeName).toEqual('Class defect');
    expect(model[0].type).toEqual('DEFECT');
    expect(model[0].category).toEqual('Category name 2');
    expect(model[0].status).toEqual('Status name 2');
    expect(model[0].description).toEqual('Some incident description 2');
    expect(model[0].truncatedDescription).toEqual('Some incident description 2');
    expect(model[0].dateOccurred).toEqual('12/12/12');
    expect(model[0].dateOccurredEpoch).toEqual(Date.parse(model[0].dateOccurred));
    expect(model[0].items[0].name).toEqual('Item 4');
    expect(model[0].jobs[0].jobNumber).toEqual('4454');
    expect(model[0].jobs[1].jobNumber).toEqual('9961');
    expect(model[0].jobNumbers).toEqual('4454, 9961');
  });

  it('Deficiencies, getters should return correct value', () => {
    const DeficiencyMocks = [{
      title: 'Deficiency 11',
      incidentDate: '01/10/16',
      incidentDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas mollis odio quis elit commodo, vel dapibus nisl hendrerit. Nullam consectetur eu massa interdum ultricies. Curabitur eu velit eget quam maximus egestas luctus sit amet metus.',
      flagStateConsulted: null,
      narrative: 'Some narrative',
      statusH: { name: 'Deficiency status' },
      deficiencyChecklists: [
        { id: 1, name: 'Checklist 1' },
        { id: 2, name: 'Checklist 2' },
      ],
    }];

    const model = makeModel(_.clone(DeficiencyMocks));

    expect(model[0].title).toEqual('Deficiency 11');
    expect(model[0].typeName).toEqual('Statutory deficiency');
    expect(model[0].type).toEqual('SD');
    expect(model[0].status).toEqual('Deficiency status');
    expect(model[0].description).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas mollis odio quis elit commodo, vel dapibus nisl hendrerit. Nullam consectetur eu massa interdum ultricies. Curabitur eu velit eget quam maximus egestas luctus sit amet metus.');
    expect(model[0].truncatedDescription).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas mollis odio quis elit commodo, vel dapibus nisl hendrerit. Nullam consectetur eu massa interdum ultricies. Curabitur eu velit eget qua');
    expect(model[0].dateOccurred).toEqual('01/10/16');
    expect(model[0].flagStateConsulted).toEqual('cd-no');
    expect(model[0].items.length).toEqual(2);
    expect(model[0].items[0].name).toEqual('Checklist 1');
    expect(model[0].items[1].name).toEqual('Checklist 2');
  });
});
