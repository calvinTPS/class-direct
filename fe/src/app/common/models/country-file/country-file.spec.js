/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-param-reassign */
import * as _ from 'lodash';
import countryFileMockData from './country-file.mocks.json';
import countryFileModel from './country-file';

describe('countryFileModel', () => {
  let $rootScope,
    makeModel;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        data[index] = Reflect.construct(countryFileModel, [rawObject]);
      });
      return data;
    };
  }));

  describe('get flag', () => {
    it('should return the flag name', () => {
      const model = makeModel(_.clone(countryFileMockData));
      expect(model[0].flag).toEqual('flag test');
    });
  });

  describe('get lastModified', () => {
    it('should return the value from creationDateWithTime field', () => {
      const model = makeModel(_.clone(countryFileMockData));
      expect(model[0].lastModified).toEqual('2009-08-16T02:02:00Z');
    });
  });

  describe('Download button', () => {
    it('should have a token attribute', () => {
      const model = makeModel(_.clone(countryFileMockData));
      expect(model[0].token).toEqual('eyJ0eXBlIjoiQ1MxMCIsImNzMTBUb2tlbiI6eyJub2RlSWQiOjQyNTY1MzYzfX0=');
    });
  });
});
