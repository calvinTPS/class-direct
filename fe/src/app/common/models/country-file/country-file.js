import * as _ from 'lodash';
import Model from '../model';

export default class CountryFile extends Model {
  constructor(model) {
    super(model);
    const getTime = dateString =>
      // - A time is passed in '2006-05-23T23:00:00Z'
      // - Remove the Z at the end of the string
      // - And return everything after T
      _.replace(dateString, 'Z', '').split('T')[1];

    if (this._lastModified == null) {
      this._lastModified = _.get(this, 'model.creationDateWithTime', '');
      this._lastModifiedDate = this.creationDateWithTimeFormatted;
      this._lastModifiedTime = getTime(this._lastModified);
    }
  }

  set dateFilter(filter) {
    this._dateFilter = filter;
  }

  get flag() {
    if (this._flag == null) {
      this._flag = _.get(this, 'model.title', '');
    }
    return this._flag;
  }

  get lastModified() {
    return this._lastModified;
  }

  get lastModifiedDate() {
    return this._lastModifiedDate;
  }

  get lastModifiedTime() {
    return this._lastModifiedTime;
  }

  // - Token is used for the download link
  static PAYLOAD_REDUCTION_FIELDS_LIST = [
    'creationDateWithTime',
    'id',
    'title',
    'token',
  ];
}
