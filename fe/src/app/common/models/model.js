export default class Model {
  constructor(model, decorate) {
    this.model = model;

    const DATES_TO_FORMAT = [
      'assignedDate',
      'buildDate',
      'closureDate',
      'creationDateWithTime',
      'dateOccurred',
      'dueDate',
      'imposedDate',
      'lastVisitDate',
      'incidentDate',
      'lowerRangeDate',
      'postponementDate',
      'upperRangeDate',
    ];

    // I convert the server's time format to 01 Apr 2017
    // I am done in such a way as this is the model and I do not
    // wan't to import any project-variables or formatting libraries.
    // If you need the naval date, which has the numerical month
    // user navel = true
    const convertDate = (date, naval) => {
      const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

      if (date) {
        const tempDate = date.split('T')[0].split('-');

        return !naval ? `${tempDate[2]} ${months[tempDate[1] - 1]} ${tempDate[0]}` :
        tempDate.reverse().join('.') || '-';
      }
      return '-';
    };

    Object.defineProperty(
      this,
      'convertDate',
      {
        enumerable: false,
        value: convertDate,
      },
    );

    Object.defineProperty(
      this,
      'DATES_TO_FORMAT',
      {
        enumerable: false,
        value: DATES_TO_FORMAT,
      },
    );

    if (model) {
      Object.entries(model).forEach(([key, value]) => {
        if (!Reflect.has(this, key)) {
          let valueToSet = model[key];
          if (decorate) {
            valueToSet = model[key] == null ? '-' : model[key];
          }

          Reflect.defineProperty(this,
            key,
            {
              get: () => valueToSet,
              enumerable: true,
            });

          // - Important section here that converts the date to the time
          // - Currently checking if it is listed as part of the array on top
          // - Suggestion to standardise the names to be formatted
          if (DATES_TO_FORMAT.indexOf(key) !== -1) {
            this[`${key}Formatted`] = model[key] != null ?
              this.convertDate(model[key]) :
              '-';
          }
        }
      });
    }
  }
  /* eslint no-invalid-this: 0 */
  static from(list) {
    const newList = [];
    list.forEach((item, index) => {
      if (item.constructor !== this) {
        newList[index] = new this(item);
      }
    });
    return newList;
  }
}
