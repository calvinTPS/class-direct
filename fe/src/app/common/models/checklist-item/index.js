/* eslint-disable no-useless-constructor */
import * as _ from 'lodash';
import Model from 'app/common/models/model';
import translation from '../../../../../resources/locales/en-GB.json';

export default class ChecklistItem extends Model {
  get id() {
    if (this._id == null) {
      this._id = _.get(this, 'model.id');
    }
    return this._id;
  }


  get parentId() {
    if (this._parentId == null) {
      this._parentId = _.get(this, 'model.conditionalParent.id');
    }
    return this._parentId;
  }

  get creditStatus() {
    if (this._creditStatus == null) {
      this._creditStatus = _.get(this, 'model.resolutionStatusH.name', translation['cd-not-credited']);
    }
    return this._creditStatus;
  }

  static PAYLOAD_FIELDS = {
    CHECK_LIST: [
      'checklistGroups.name',
      'checklistGroups.checklistSubgrouplist.name',
      'checklistGroups.checklistSubgrouplist.checklistItems.longDescription',
      'checklistGroups.checklistSubgrouplist.checklistItems.referenceCode',
      'checklistGroups.checklistSubgrouplist.checklistItems.resolutionStatusH.name',
    ],
  }
}
