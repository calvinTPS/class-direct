/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
 import ChecklistItemModel from './';

 describe('Checklist item', () => {
   let $rootScope, makeChecklistItemModel;

   const mockChecklistItem = {
     id: 1,
     name: 'Checklist 1',
     conditionalParent: { id: 2 },
     resolutionStatusH: { name: 'checklist 1 status' },
   };

   beforeEach(inject((_$rootScope_) => {
     $rootScope = _$rootScope_;
     makeChecklistItemModel = data => new ChecklistItemModel(data);
   }));

   describe('ChecklistItemModel', () => {
     it('should return correct data model for ChecklistItem', () => {
       const checklistItemModel = makeChecklistItemModel(mockChecklistItem);

       expect(checklistItemModel.id).toEqual(1);
       expect(checklistItemModel.parentId).toEqual(2);
       expect(checklistItemModel.creditStatus).toEqual('checklist 1 status');
     });
   });
 });
