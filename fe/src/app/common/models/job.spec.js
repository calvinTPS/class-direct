/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import JobModel from './job';

describe('Job', () => {
  let $rootScope, makeJobModel;

  const rawJob = {
    id: 1,
    location: 'loc',
    lastVisitDate: '2016-11-02',
    leadSurveyor: {
      id: 1,
      name: 'Lead Surveyor',
    },
    jobNumber: '4454',
    jobStatusDto: {
      name: 'Job Status name',
    },
  };

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeJobModel = data => new JobModel(data);
  }));

  describe('JobModel', () => {
    it('should return correct data model for Job', () => {
      const jobModel = makeJobModel(rawJob);

      expect(jobModel.id).toEqual(1);
      expect(jobModel.jobNumber).toEqual('4454');
      expect(jobModel.location).toEqual('loc');
      expect(jobModel.lastVisitDate).toEqual('2016-11-02');
      expect(jobModel.leadSurveyorName).toEqual('Lead Surveyor');
      expect(jobModel.status).toEqual('Job Status name');
    });
  });
});
