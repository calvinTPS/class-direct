import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import AttachmentModel from 'app/common/models/attachment';
import JobModel from 'app/common/models/job';
import Model from 'app/common/models/model';

export default class Report extends Model {
  get name() {
    return this.type;
  }

  get shortName() {
    return this.name;
  }

  get type() {
    if (this._type == null) {
      // use reportTypeDto if available else use reportType if its a string
      this._type = _.get(this.model, 'reportTypeDto.name') ||
        (_.isString(this.model.reportType) ? this.model.reportType : '');
    }
    return this._type;
  }

  get version() {
    this._version = !this._version ? _.get(this, 'model.reportVersion', 0) : this._version;
    return this._version;
  }

  get jobId() {
    if (this._jobId == null) {
      this._jobId = this.model.jobId || this.job.id;
    }

    return this._jobId;
  }

  get job() {
    const job = this.model.jobH || this.model.job;

    if (this._job == null && job) {
      this._job = Reflect.construct(JobModel, [job]);
    }
    return this._job;
  }

  get attachments() {
    if (this._attachments == null) {
      this._attachments = this.model.attachments.map(
        attachment => Reflect.construct(AttachmentModel, [attachment]),
      );
    }

    return this._attachments;
  }

  set attachments(attachments) {
    this._attachments = null;
    this.model.attachments = attachments;
  }

  get isFARReport() {
    return this.type === AppSettings.REPORT_TYPES.FAR;
  }

  get isFSRReport() {
    return this.type === AppSettings.REPORT_TYPES.FSR;
  }

  get isESPReport() {
    return this.type === AppSettings.REPORT_TYPES.ESP;
  }

  get isTMReport() {
    return this.type === AppSettings.REPORT_TYPES.TM;
  }

  get isMMSReport() {
    return this.type === AppSettings.REPORT_TYPES.MMS;
  }
}
