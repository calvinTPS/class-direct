/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import JobModel from 'app/common/models/job';
import mockReport from './report.mocks.json';
import ReportModel from './';

describe('Report', () => {
  let $rootScope, makeModel;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = data => Reflect.construct(ReportModel, [data]);
  }));

  it('Test all expected data from report', () => {
    const model1 = makeModel(_.clone(mockReport[0]));
    expect(model1.id).toEqual(1);
    expect(model1.name).toEqual('ABC');
    expect(model1.shortName).toEqual('ABC');
    expect(model1.type).toEqual('ABC');
    expect(model1.version).toEqual(1);

    const model2 = makeModel(_.clone(mockReport[1]));
    expect(model2.id).toEqual(2);
    expect(model2.name).toEqual('XYZ');
    expect(model2.shortName).toEqual('XYZ');
    expect(model2.type).toEqual('XYZ');
    expect(model2.version).toEqual(12);
    expect(model2.job.lastVisitDate).toEqual('01-01-2018');
  });

  it('Test all expected data from report when theres errors from original object', () => {
    const model = makeModel({ id: 2 });
    expect(model.id).toEqual(2);
    expect(model.name).toEqual('');
    expect(model.type).toEqual('');
    expect(model.version).toEqual(0);
    expect(model.job).toBeUndefined();
  });

  it('has custom report type getters', () => {
    expect(makeModel({ reportType: 'ESP' }).isESPReport).toBe(true);
    expect(makeModel({ reportType: 'TM' }).isTMReport).toBe(true);
    expect(makeModel({ reportType: 'FAR' }).isFARReport).toBe(true);
    expect(makeModel({ reportType: 'FSR' }).isFSRReport).toBe(true);

    expect(makeModel({ reportType: 'FSR' }).isTMReport).toBe(false);
  });
});
