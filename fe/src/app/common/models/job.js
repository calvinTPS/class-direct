import * as _ from 'lodash';
import Model from 'app/common/models/model';
import ServiceModel from 'app/common/models/service';

export default class Job extends Model {
  get jobNumber() {
    if (this._jobNumber == null) {
      this._jobNumber = _.get(this, 'model.jobNumber');
    }
    return this._jobNumber;
  }

  get leadSurveyorName() {
    return _.get(this, 'model.leadSurveyor.name');
  }

  get services() {
    if (this._services == null) {
      this._services = _.map(_.get(this, 'model.services'), service => new ServiceModel(service));
    }
    return this._services;
  }

  get status() {
    if (this._status == null) {
      this._status = _.get(this.model, 'jobStatusDto.name');
    }
    return this._status;
  }
}
