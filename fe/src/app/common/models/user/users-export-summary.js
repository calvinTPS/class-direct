import _ from 'lodash';
import Model from 'app/common/models/model';

export default class UsersExportSummary extends Model {
  get accountTypes() {
    if (this._accountTypes == null) {
      this._accountTypes = _.get(this, 'model.accountTypes', []);
    }
    return this._accountTypes;
  }

  get lastLoginDate() {
    if (this._lastLoginDate == null) {
      this._lastLoginDate = _.get(this, 'model.lastLoginDate');
    }
    return this._lastLoginDate;
  }
}
