import _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import Model from 'app/common/models/model';

let statusIdToStatusMap = {};

export default class User extends Model {
  get userId() {
    if (this._userId == null) {
      this._userId = _.get(this, 'model.userId', '');
    }
    return this._userId;
  }

  get fullName() {
    if (this._fullName == null) {
      const fullName = _.get(this, 'model.name') || '';
      if (fullName) {
        this._fullName = fullName;
      } else {
        // Fallback
        const firstName = _.get(this, 'model.firstName') || '';
        const lastName = _.get(this, 'model.lastName') || '';
        this._fullName = `${firstName} ${lastName}`.trim();
      }
    }
    return this._fullName;
  }

  get email() {
    if (this._email == null) {
      this._email = _.get(this, 'model.email', '');
    }
    return this._email;
  }

  get role() {
    if (this._role == null) {
      this._role = this.roles[0];
    }
    return this._role;
  }

  /**
   * Used to set user between EOR role and normal role (e.g. client)
   *
   * Application needs to know what role user currently has to behave correctly.
   */
  set role(role) {
    if (this.roles.length > 1 && this.roles.indexOf(role) >= 0) {
      this._role = role;

      // Reset restricted map
      this._userRestrictedStateMap = null;
    }
  }

  get roles() {
    if (this._roles == null) {
      // Get the roles, then sort the roles by returning EOR to the last index
      this._roles = _.sortBy(_.map(this.model.roles, 'roleName'),
        role => role === AppSettings.ROLES.EOR ? 1 : -1);
    }
    return this._roles;
  }

  get company() {
    if (this._company == null) {
      // this.model.company can also be a null. Null values are valid values
      // and will be returned back as null, not the default value.
      this._company = _.get(this, 'model.company') || {};
    }
    return this._company;
  }

  get companyPosition() {
    if (this._companyPosition == null) {
      this._companyPosition = _.get(this, 'model.companyPosition', '');
    }
    return this._companyPosition;
  }

  get isClient() {
    if (this._isClient == null) {
      this._isClient = this.hasRole(AppSettings.ROLES.CLIENT);
    }
    return this._isClient;
  }

  get isShipBuilder() {
    if (this._isShipBuilder == null) {
      this._isShipBuilder = this.hasRole(AppSettings.ROLES.SHIP_BUILDER);
    }
    return this._isShipBuilder;
  }

  /**
   * Returns true if user has LR_ADMIN, LR_SUPPORT, or LR_INTERNAL permissions.
   */
  get isLRUser() {
    return this.isLRAdmin || this.isLRSupport || this.isLRInternal;
  }

  get isLRAdmin() {
    if (this._isLRAdmin == null) {
      this._isLRAdmin = this.hasRole(AppSettings.ROLES.LR_ADMIN);
    }
    return this._isLRAdmin;
  }

  get isLRSupport() {
    if (this._isLRSupport == null) {
      this._isLRSupport = this.hasRole(AppSettings.ROLES.LR_SUPPORT);
    }
    return this._isLRSupport;
  }

  get isLRInternal() {
    if (this._isLRInternal == null) {
      this._isLRInternal = this.hasRole(AppSettings.ROLES.LR_INTERNAL);
    }
    return this._isLRInternal;
  }

  get isEquasisThetis() {
    if (this._isEquasisThetis == null) {
      this._isEquasisThetis = !!_.find(this.model.roles, role =>
        role.roleName === AppSettings.ROLES.EQUASIS || role.roleName === AppSettings.ROLES.THETIS);
    }
    return this._isEquasisThetis;
  }

  /**
   * Returns true if the user has the EOR role (in addition to other possible roles).
   */
  get isEORUser() {
    return this.hasRole(AppSettings.ROLES.EOR);
  }

  get isFlagUser() {
    if (this._isFlagUser == null) {
      this._isFlagUser = this.hasRole(AppSettings.ROLES.FLAG);
    }

    return this._isFlagUser;
  }

  get shouldTruncateSearchResults() {
    return this.isLRAdmin || this.isLRSupport || this.isLRInternal || this.isFlagUser;
  }

  /**
   * Returns the status ID for the user.
   *
   * @returns {number}
   */
  get statusId() {
    // Not using a private here because this is editable.
    return _.get(this, 'model.status.id', 0);
  }

  /**
   * Set the new account status ID.
   *
   * @param {number} statusId
   */
  set statusId(statusId) {
    _.set(this, 'model.status.id', Number(statusId));
    _.set(this, 'model.status.name', User.getStatus(statusId).name);
    this._statusName = User.getStatus(statusId).name;
  }

  /**
   * Returns "Active", "Disabled", or "Archived".
   *
   * @returns {string}
   */
  get statusName() {
    if (this._statusName == null) {
      this._statusName = _.get(this, 'model.status.name', '');
    }

    return this._statusName;
  }

  get isArchived() {
    return this.statusName === AppSettings.ACCOUNT_STATUS.ARCHIVED;
  }

  get clientCode() {
    if (this._clientCode == null) {
      this._clientCode = _.get(this, 'model.clientCode', '');
    }
    return this._clientCode;
  }

  set clientCode(code) {
    this.model.clientCode = code;
    this._clientCode = null;
  }

  get shipBuilderCode() {
    if (this._shipBuilderCode == null) {
      this._shipBuilderCode = _.get(this, 'model.shipBuilderCode', '');
    }
    return this._shipBuilderCode;
  }

  set shipBuilderCode(code) {
    this.model.shipBuilderCode = code;
    this._shipBuilderCode = null;
  }

  get flagCode() {
    if (this._flagCode == null) {
      this._flagCode = _.get(this, 'model.flagCode', '');
    }
    return this._flagCode;
  }

  set flagCode(code) {
    this.model.flagCode = code;
    this._flagCode = null;
  }

  get isAdmin() {
    if (this._isAdmin == null) {
      this._adminRolesMap = _.keyBy(AppSettings.ADMIN_ROLES);
      this._isAdmin = Boolean(
        _.find(this.model.roles, userRole => this._adminRolesMap[userRole.roleName]));
    }
    return this._isAdmin;
  }

  /**
   * Populated dynamically by the UserService.
   *
   * @type {Array}
   */
  static ACCOUNT_STATUSES = [];

  static getStatus(statusId) {
    if (_.isEmpty(User.ACCOUNT_STATUSES)) {
      throw new Error('Account statuses not populated');
    }

    if (_.isEmpty(statusIdToStatusMap)) {
      statusIdToStatusMap = _.keyBy(User.ACCOUNT_STATUSES, 'id');
    }

    return statusIdToStatusMap[statusId];
  }

  get notifications() {
    if (this._notifications == null) {
      this._notifications = _.get(this, 'model.notifications', '');
    }

    return this._notifications;
  }

  get expiryDate() {
    if (this._expiryDate == null) {
      this._expiryDate = _.get(this, 'model.userAccExpiryDate', '');
    }
    return this._expiryDate;
  }

  set expiryDate(expiryDate) {
    this.model.userAccExpiryDate = expiryDate;
    this._expiryDate = null;
  }

  hasRole(roleName) {
    return _.includes(_.map(this.model.roles, 'roleName'), roleName);
  }

  /**
   * This getter is used to populate all the accessible states for roles.
   * For example if current user with LR_INTERNAL roles CANNOT ACCESS these states:
   *
   * - main
   * - asset.mpms
   *
   * Then on project-variables.js please provide this option
   * "LR_INTERNAL": ["main", "asset.mpms"]
   *
   * Then this getter will manipulate the state permission so that the result will be like this
   * {
   *   main: true,
   *   asset.mpms: true
   * }
   *
   * Above object will be used to validate the states permission on app.controller
   */
  get userRestrictedStateMap() {
    if (this._userRestrictedStateMap == null) {
      const map = c => _.map(c, v => AppSettings.ROLE_STATE_RESTRICTIONS[v]);
      const flatten = c => _.flatten(c);
      const uniq = c => _.uniq(_.compact(c));

      // Zip an array to object and assign value to always true
      const zip = c => _.zipObject(c, _.map(c, () => true));

      this._userRestrictedStateMap = _.flow(
        map,
        flatten,
        uniq,
        zip,
      )([this.role]);
    }

    return _.isEmpty(this._userRestrictedStateMap) ? undefined : this._userRestrictedStateMap;
  }

  /**
   * @param {string} state The name of the state.
   */
  canAccessState(state) {
    if (!_.isString(state)) {
      throw new Error('State should be a string');
    }
    return _.isEmpty(this.userRestrictedStateMap) ? true : !this.userRestrictedStateMap[state];
  }

  /**
   * Returns true if the user has the EOR role ONLY.
   */
  get isEORUserOnly() {
    if (this.model.roles.length === 1) {
      return this.hasRole(AppSettings.ROLES.EOR);
    }
    return false;
  }

  get latestUserTC() {
    if (this._latestTC == null) {
      this._latestTC = (!_.isEmpty(this.model.userTCs)) ? _.maxBy(this.model.userTCs, 'tcId') : null;
    }
    return this._latestTC;
  }

  hasAcceptedLatestTC(latestTC) {
    if (!this.latestUserTC) {
      return false;
    }
    return this.latestUserTC.tcId === latestTC.tcId;
  }
}
