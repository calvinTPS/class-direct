/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable no-param-reassign, no-unused-vars */

import * as _ from 'lodash';
import UsersExportSummaryModel from './users-export-summary';

describe('UserModel', () => {
  let $rootScope;
  let makeModel;

  const mockUsersSummary = {
    accountTypes: ['Client', 'Exhibition of Records', 'Flag'],
    lastLoginDate: { from: '2016-01-02', to: '2016-04-05' },
  };

  beforeEach(inject(() => {
    makeModel = data => Reflect.construct(UsersExportSummaryModel, [data]);
  }));

  describe('get accountTypes', () => {
    it('should return the accountTypes', () => {
      const model = makeModel(_.clone(mockUsersSummary));
      expect(model.accountTypes).toEqual(mockUsersSummary.accountTypes);
    });
  });

  describe('get lastLoginDate', () => {
    it('should return the lastLoginDate', () => {
      const model = makeModel(_.clone(mockUsersSummary));
      expect(model.lastLoginDate).toEqual(mockUsersSummary.lastLoginDate);
    });
  });
});
