/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable no-param-reassign, no-unused-vars */

import * as _ from 'lodash';
import AccountStatuses from './account-statuses.mocks.json';
import UserMocks from './user.mocks.json';
import UserModel from './user';

describe('UserModel', () => {
  let $rootScope;
  let makeModel;
  let userData;

  // Populate account statuses. In production, this is
  // populated by UserService
  UserModel.ACCOUNT_STATUSES = AccountStatuses;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    userData = _.cloneDeep(UserMocks);
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        data[index] = Reflect.construct(UserModel, [rawObject]);
      });
      return data;
    };
  }));

  describe('get userId', () => {
    it('should return the userId', () => {
      const model = makeModel(userData);
      expect(model[0].userId).toEqual('101');
    });
  });

  describe('get fullName', () => {
    it('should return the full name', () => {
      const model = makeModel(userData);
      expect(model[0].fullName).toEqual('Jane Doe');
      expect(model[1].fullName).toEqual('James');
    });
  });

  describe('get statusId', () => {
    it('should return the status ID', () => {
      const model = makeModel(userData);
      expect(model[0].statusId).toEqual(1);
      expect(model[1].statusId).toEqual(2);
      expect(model[2].statusId).toEqual(0);
    });
  });

  describe('get statusName', () => {
    it('should return the status name', () => {
      const model = makeModel(userData);

      expect(model[0].statusName).toEqual('Active');
      expect(model[1].statusName).toEqual('Disabled');
      expect(model[2].statusName).toEqual('');
    });
  });

  describe('set statusId', () => {
    it('should set the new status ID', () => {
      const model = makeModel(userData);

      model[0].statusId = 2;
      expect(model[0].statusId).toEqual(2);

      // Should update the underlying object model
      expect(model[0].model.status.id).toEqual(2);
    });

    it('should get the new status name', () => {
      const model = makeModel(userData);
      model[0].statusId = 1;
      expect(model[0].statusName).toEqual('Active');

      // Should update the underlying object model
      expect(model[0].model.status.name).toEqual('Active');

      model[0].statusId = 2;
      expect(model[0].statusName).toEqual('Disabled');

      // Should update the underlying object model
      expect(model[0].model.status.name).toEqual('Disabled');

      model[0].statusId = 3;
      expect(model[0].statusName).toEqual('Archived');

      // Should update the underlying object model
      expect(model[0].model.status.name).toEqual('Archived');
    });
  });

  describe('get email', () => {
    it('should return the email', () => {
      const model = makeModel(userData);
      expect(model[0].email).toEqual('admin@lr.org');
    });
  });

  describe('get clientCode', () => {
    it('should return the clientCode', () => {
      const model = makeModel(userData);
      expect(model[0].clientCode).toEqual(1);
    });
  });

  describe('set clientCode', () => {
    it('should allow setting a new client code', () => {
      const model = makeModel(userData);
      expect(model[0].clientCode).toEqual(1);
      model[0].clientCode = 123123;
      expect(model[0].clientCode).toEqual(123123);
    });
  });

  describe('get/set shipBuilderCode', () => {
    it('should return the shipBuilderCode', () => {
      const model = makeModel(userData);
      expect(model[0].shipBuilderCode).toEqual(1);

      model[0].shipBuilderCode = 100101;
      expect(model[0].shipBuilderCode).toEqual(100101);
    });
  });

  describe('get/set flagCode', () => {
    it('should return the flagCode', () => {
      const model = makeModel(userData);
      expect(model[0].flagCode).toEqual(100);

      model[0].flagCode = 'ABC';
      expect(model[0].flagCode).toEqual('ABC');
    });
  });

  describe('get role', () => {
    it('should return the role', () => {
      const model = makeModel(userData);
      expect(model[0].role).toEqual('LR_ADMIN');
      expect(model[1].role).toEqual('EOR');
      expect(model[2].role).toEqual(undefined);
    });
  });

  describe('get roles', () => {
    it('should return the roles', () => {
      const model = makeModel(userData);
      expect(model[0].roles).toEqual(['LR_ADMIN', 'SHIP_BUILDER']);
      expect(model[1].roles).toEqual(['EOR']);
      expect(model[2].roles).toEqual([]);
      expect(model[4].roles).toEqual(['CLIENT', 'EOR']);
    });
  });

  describe('get shouldTruncateSearchResults', () => {
    it('should return the shouldTruncateSearchResults', () => {
      const model = makeModel(_.clone(userData));
      expect(model[0].shouldTruncateSearchResults).toBe(true);
      expect(model[1].shouldTruncateSearchResults).toBe(false);
      expect(model[2].shouldTruncateSearchResults).toBe(false);
      expect(model[4].shouldTruncateSearchResults).toBe(false);
    });
  });

  describe('get userRestrictedStateMap', () => {
    it('should return userRestrictedStateMap', () => {
      const model = makeModel(userData);
      const mockUserRestrictedMapForEOR = {
        'asset.registerBookSisters': true,
        'asset.surveyRequest': true,
        'asset.technicalSisters': true,
        'fleet.vessel': true,
        nationalAdministration: true,
      };

      const mockUserRestrictedMapForFlag = {
        'administration.addUser': true,
        'administration.editUser': true,
        'administration.userDetails': true,
        'administration.userList': true,
        'asset.surveyRequest': true,
      };
      expect(model[0].userRestrictedStateMap).toEqual(undefined); // Admin primary role
      expect(model[1].userRestrictedStateMap).toEqual(mockUserRestrictedMapForEOR);
      expect(model[6].userRestrictedStateMap).toEqual(mockUserRestrictedMapForFlag); // Equasis
    });
  });

  describe('canAccessState', () => {
    it('should decide whether user can access the page or not', () => {
      const model = makeModel(userData);
      expect(model[0].canAccessState('main')).toBe(true);
      expect(model[1].canAccessState('main')).toBe(true);
      expect(model[4].canAccessState('main')).toBe(true);
    });
  });

  describe('hasRole', () => {
    it('should return true if the user has the role', () => {
      const model = makeModel(userData);
      expect(model[0].hasRole('LR_ADMIN')).toEqual(true);
      expect(model[0].hasRole('EOR')).toEqual(false);
    });
  });

  describe('get isAdmin', () => {
    it('should return true if user is an admin', () => {
      const model = makeModel(userData);
      expect(model[0].isAdmin).toEqual(true);
      expect(model[1].isAdmin).toEqual(false);
      expect(model[2].isAdmin).toEqual(false);
    });
  });

  describe('isLRUser', () => {
    it('should return true if user has any kind of LR role', () => {
      const model = makeModel(_.clone([
        { roles: [{ roleName: 'LR_ADMIN' }] },
        { roles: [{ roleName: 'LR_SUPPORT' }] },
        { roles: [{ roleName: 'LR_INTERNAL' }] },
        { roles: [{ roleName: 'FOOBAR' }] },
      ]));
      expect(model[0].isLRUser).toEqual(true);
      expect(model[1].isLRUser).toEqual(true);
      expect(model[2].isLRUser).toEqual(true);
      expect(model[3].isLRUser).toEqual(false);
    });
  });

  describe('isLRAdmin', () => {
    it('should return true if user have LR_ADMIN role', () => {
      const model = makeModel(userData);
      expect(model[0].isLRAdmin).toEqual(true);
      expect(model[5].isLRAdmin).toEqual(false);
    });
  });

  describe('isLRSupport', () => {
    it('should return true if user have LR_SUPPORT role', () => {
      const model = makeModel(userData);
      expect(model[0].isLRSupport).toEqual(false);
      expect(model[5].isLRSupport).toEqual(true);
    });
  });

  describe('isLRInternal', () => {
    it('should return true if user has LR_INTERNAL role', () => {
      const model = makeModel(_.clone([
        { roles: [{ roleName: 'LR_INTERNAL' }] },
        { roles: [{ roleName: 'LR_SUPPORT' }] },
      ]));
      expect(model[0].isLRInternal).toEqual(true);
      expect(model[1].isLRInternal).toEqual(false);
    });
  });

  describe('isClient', () => {
    it('should return true if user have CLIENT role', () => {
      const model = makeModel(userData);
      expect(model[4].isClient).toEqual(true);
      expect(model[0].isClient).toEqual(false);
    });
  });

  describe('isShipBuilder', () => {
    it('should return true if user have SHIP_BUILDER role', () => {
      const model = makeModel(userData);
      expect(model[0].isShipBuilder).toEqual(true);
      expect(model[1].isShipBuilder).toEqual(false);
    });
  });

  describe('company', () => {
    it('returns company data', () => {
      const model = makeModel(userData);
      expect(model[0].company.name).toEqual('BAE SYSTEMS APPLIED INTELLIGENCE');
      expect(model[0].company.addressLine1).toEqual('LEVEL 28');
      expect(model[0].company.addressLine2).toEqual('MENARA BINJAI');
      expect(model[0].company.addressLine3).toEqual('2 JALAN BINJAI');
      expect(model[0].company.city).toEqual('Kuala Lumpur');
      expect(model[0].company.state).toEqual('WP');
      expect(model[0].company.postCode).toEqual('50450');
      expect(model[0].company.country).toEqual('Malaysia');
      expect(model[0].company.telephone).toEqual('0321913000');
    });

    it('returns blank string on empty data', () => {
      const model = makeModel(userData);
      expect(model[1].company.name).toEqual('EOR Shipping Company');
      expect(model[1].company.addressLine1).toEqual('EOR Company Address line 1');
      expect(model[1].company.addressLine2).toEqual('Address line2');
      expect(model[1].company.addressLine3).toEqual('Address line3');
      expect(model[1].company.city).toEqual('California');
      expect(model[1].company.state).toEqual('San Francisco');
      expect(model[1].company.postCode).toEqual('123456');
      expect(model[1].company.country).toEqual('USA');
      expect(model[1].company.telephone).toEqual('222222');
    });

    it('returns {} when recieved as null', () => {
      const model = makeModel([{
        company: null,
      }]);

      expect(model[0].company).toEqual({});
    });
  });

  describe('User.companyPosition', () => {
    it('should return the correct companyPosition', () => {
      const model = makeModel(userData);
      expect(model[0].companyPosition).toEqual('Department Manager');
      expect(model[1].companyPosition).toEqual('Shipping Clerk');
      expect(model[2].companyPosition).toEqual('Shipping Clerk 2');
    });
  });

  describe('User.isEquasisThetis', () => {
    it('should return true if user is an Equasis Thetsis user', () => {
      const model = makeModel(userData);

      expect(model[0].isEquasisThetis).toEqual(false);
      expect(model[3].isEquasisThetis).toEqual(true);
    });
  });

  describe('User.isEORUser', () => {
    it('should return the correct isEORUser value', () => {
      const model = makeModel(userData);
      expect(model[0].isEORUser).toEqual(false);
      expect(model[1].isEORUser).toEqual(true);
      expect(model[2].isEORUser).toEqual(false);
    });
  });

  describe('User.isFlagUser', () => {
    it('should return the correct isFlagUser value', () => {
      const model = makeModel(userData);
      expect(model[6].isFlagUser).toEqual(true);
      expect(model[1].isFlagUser).toEqual(false);
    });
  });

  describe('User.statusId', () => {
    it('should return the correct status id', () => {
      const model = makeModel(userData);
      expect(model[0].statusId).toEqual(1);
      expect(model[3].statusId).toEqual(2);
      model[3].statusId = 1;
      expect(model[3].statusId).toEqual(1);
    });
  });

  describe('User.statusName', () => {
    it('should return the correct status name', () => {
      const model = makeModel(userData);
      expect(model[0].statusName).toEqual('Active');
      expect(model[1].statusName).toEqual('Disabled');
      model[3].statusId = 2;
      expect(model[3].statusName).toEqual('Disabled');
    });
  });

  describe('User.isArchived', () => {
    it('should return the correct boolean value based on status name', () => {
      const model = makeModel(userData);
      expect(model[0].isArchived).toEqual(false);
      expect(model[1].isArchived).toEqual(false);
      model[0].statusId = 3;
      expect(model[0].isArchived).toEqual(true);
    });
  });

  describe('User.clientCode', () => {
    it('should return correct client code', () => {
      const model = makeModel(userData);
      expect(model[0].clientCode).toEqual(1);
      expect(model[3].clientCode).toEqual(3);
    });
  });

  describe('User.shipBuilderCode', () => {
    it('should return the correct shipBuilderCode', () => {
      const model = makeModel(userData);
      expect(model[0].shipBuilderCode).toEqual(1);
      expect(model[3].shipBuilderCode).toEqual(3);
    });
  });

  describe('User.flagCode', () => {
    it('should return the correct flagCode', () => {
      const model = makeModel(userData);
      expect(model[0].flagCode).toEqual(100);
      expect(model[3].flagCode).toEqual(101);
    });
  });

  describe('User.isAdmin', () => {
    it('should return if user is admin or not', () => {
      const model = makeModel(userData);
      expect(model[0].isAdmin).toEqual(true);
      expect(model[3].isAdmin).toEqual(false);
    });
  });

  describe('User.getStatus', () => {
    it('should return the status name for a given statusId', () => {
      expect(UserModel.getStatus(1).name).toEqual('Active');
      expect(UserModel.getStatus(2).name).toEqual('Disabled');
      expect(UserModel.getStatus(3).name).toEqual('Archived');

      const temp = UserModel.ACCOUNT_STATUSES;
      UserModel.ACCOUNT_STATUSES = {};
      try {
        UserModel.getStatus(1);
      } catch (e) {
        expect(e).toEqual(new Error('Account statuses not populated'));
      }
      UserModel.ACCOUNT_STATUSES = temp;
    });
  });

  describe('User.notifications', () => {
    it('should return user notifications', () => {
      const model = makeModel(userData);
      expect(model[0].notifications.length).toEqual(3);
      expect(model[0].notifications[0].id).toEqual(6);
      expect(model[0].notifications[0].notificationTypes.name).toEqual('Asset Listing');
      expect(model[0].notifications[0].favouritesOnly).toEqual(true);
      expect(model[3].notifications.length).toEqual(2);
    });
  });

  describe('User.expiryDate', () => {
    it('should return user expiry date', () => {
      const model = makeModel(userData);
      expect(model[0].expiryDate).toEqual(null);
      expect(model[3].expiryDate).toEqual('2018-03-03');

      model[0].expiryDate = '2020-01-01';
      expect(model[0].expiryDate).toEqual('2020-01-01');
    });
  });

  describe('User.hasRole', () => {
    it('should return true if role matches', () => {
      const model = makeModel(userData);
      expect(model[0].hasRole('EQUASIS')).toEqual(false);
      expect(model[0].hasRole('LR_ADMIN')).toEqual(true);
    });
  });

  describe('User.isEORUserOnly', () => {
    it('should return true if eor role matches', () => {
      const model = makeModel(userData);
      expect(model[1].isEORUserOnly).toEqual(true);
      expect(model[3].isEORUserOnly).toEqual(false);
    });
  });

  describe('User.latestUserTC', () => {
    it('should return latest user tc', () => {
      const model = makeModel(userData);
      expect(model[0].latestUserTC).toEqual(null);
      expect(model[1].latestUserTC).toEqual({ tcId: 1 });
    });
  });

  describe('User.hasAcceptedLatestTC', () => {
    it.async('should return true if user tc matched latest tc', async () => {
      const model = makeModel(userData);
      expect(model[0].hasAcceptedLatestTC(null)).toEqual(false);
      expect(model[1].hasAcceptedLatestTC({ tcId: 1 })).toEqual(true);
      expect(model[1].hasAcceptedLatestTC({ tcId: 2 })).toEqual(false);
    });
  });
});
