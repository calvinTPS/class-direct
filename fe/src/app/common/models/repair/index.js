/*
 * Used by REPAIR and RECTIFICATION
 */

import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import ItemModel from '../item/';
import Model from '../model';

export default class Repair extends Model {
  get type() {
    if (this._type == null) {
      if (this.model.repairsH) {
        this._type = AppSettings.DEFECTS_META.REPAIR.ID;
      } else if (this.model.rectificationChecklists) {
        this._type = AppSettings.DEFECTS_META.RECTIFICATION.ID;
      }
    }
    return this._type;
  }

  get items() {
    if (this._items == null) {
      let items = [];
      // repairsH contain items for repair
      // rectificationChecklists contain items for rectification
      if (this.type === AppSettings.DEFECTS_META.REPAIR.ID) {
        items = this.model.repairsH;
        this._items = _.map(items, item =>
          Reflect.construct(ItemModel, [item]));
      } else if (this.type === AppSettings.DEFECTS_META.RECTIFICATION.ID) {
        items = this.model.rectificationChecklists;
        this._items = _.map(items, item =>
          Reflect.construct(ItemModel, [item.deficiencyChecklist]));
      }
    }
    return this._items;
  }

  get repairAction() {
    if (this._repairAction == null) {
      this._repairAction = _.get(this.model, 'repairActionH.description');
    }
    return this._repairAction;
  }

  get actionTaken() {
    if (this._actionTaken == null) {
      this._actionTaken = _.get(this.model, 'repairActionH.name');
    }
    return this._actionTaken;
  }

  get title() {
    if (this._title == null) {
      this._title = this.model.title;
    }
    return this._title;
  }

  set title(title) {
    this._title = title;
  }
}
