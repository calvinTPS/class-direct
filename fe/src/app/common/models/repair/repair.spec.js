/* global inject */

import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import RepairModel from './';

describe('RepairModel', () => {
  let RepairMockData;
  let makeModel;
  let RectificationMockData;

  beforeEach(inject(() => {
    RepairMockData = [{
      repairsH: [
        { name: 'Repair item 1' },
        { itemName: 'Repair item 2' },
      ],
      title: 'Defect title',
      repairActionH: {
        name: 'This is the action taken',
        description: 'This is the repair action',
      },
    }];

    RectificationMockData = [{
      rectificationChecklists: [
        {
          deficiencyChecklist: {
            workItem: {
              name: 'Rectification item 1',
            },
          },
        },
        {
          deficiencyChecklist: {
            workItem: {
              name: 'Rectification item 2',
            },
          },
        },
      ],
      repairActionH: {
        name: 'Repair action title',
      },
    }];

    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        // eslint-disable-next-line no-param-reassign
        data[index] = Reflect.construct(RepairModel, [rawObject]);
      });
      return data;
    };
  }));

  it('items getter returns correct data', () => {
    const model = makeModel(_.clone(RepairMockData));
    expect(model[0].items.length).toEqual(2);
    expect(model[0].items[0].name).toEqual('Repair item 1');
    expect(model[0].items[1].name).toEqual('Repair item 2');

    const model2 = makeModel(_.clone(RectificationMockData));
    expect(model2[0].items[0].name).toEqual('Rectification item 1');
    expect(model2[0].items[1].name).toEqual('Rectification item 2');
  });

  it('title getter/setter returns correct data', () => {
    let model = makeModel(_.clone(RepairMockData));
    expect(model[0].title).toEqual('Defect title');

    model[0].title = 'Repair custom title';
    expect(model[0].title).toEqual('Repair custom title');

    model = makeModel(_.clone(RectificationMockData));
    expect(model[0].title).toBeUndefined();
  });

  it('repairAction getter returns correct value', () => {
    const model = makeModel(_.clone(RepairMockData));
    expect(model[0].repairAction).toEqual('This is the repair action');
  });

  it('actionTaken getter returns correct value', () => {
    const model = makeModel(_.clone(RepairMockData));
    expect(model[0].actionTaken).toEqual('This is the action taken');
  });

  it('type getter returns correct value', () => {
    const model = makeModel(_.clone(RepairMockData));
    expect(model[0].type).toEqual(AppSettings.DEFECTS_META.REPAIR.ID);

    const model2 = makeModel(_.clone(RectificationMockData));
    expect(model2[0].type).toEqual(AppSettings.DEFECTS_META.RECTIFICATION.ID);
  });
});
