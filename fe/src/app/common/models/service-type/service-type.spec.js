/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable no-param-reassign, no-unused-vars */

import * as _ from 'lodash';
import ServiceTypeMocks from './service-type.mocks.json';
import ServiceTypeModel from './service-type';

describe('ServiceTypeModel', () => {
  let $rootScope;
  let makeModel;

  const serviceTypeData = ServiceTypeMocks;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        data[index] = Reflect.construct(ServiceTypeModel, [rawObject]);
      });
      return data;
    };
  }));

  describe('get id', () => {
    it('should return the id', () => {
      const model = makeModel(_.clone(serviceTypeData));
      expect(model[0].model.id).toEqual(1);
    });
  });

  describe('get service catalogue', () => {
    it('should return the service catalogue name', () => {
      const model = makeModel(_.clone(serviceTypeData));
      expect(model[0].serviceCatalogue).toMatch('Anti-fouling system');
    });
  });

  describe('get product catalogue', () => {
    it('should return the product catalogue name', () => {
      const model = makeModel(_.clone(serviceTypeData));
      expect(model[0].productCatalogue).toMatch('Anti-Fouling System');
    });
  });

  describe('get isSelected', () => {
    it('should return true by default', () => {
      const model = makeModel(_.clone(serviceTypeData));
      expect(model[0].isSelected).toEqual(true);
    });
  });

  describe('set isSelected', () => {
    it('should return same boolean value', () => {
      const model = makeModel(_.clone(serviceTypeData));
      model[0].isSelected = false;
      expect(model[0].isSelected).toEqual(false);

      model[0].isSelected = true;
      expect(model[0].isSelected).toEqual(true);
    });
  });

  describe('get serviceCatalogueH', () => {
    it('should return serviceCatalogueH', () => {
      const model = makeModel(_.clone(serviceTypeData));
      expect(model[0].serviceCatalogueH).toEqual(model[0].model);
    });
  });

  describe('get id()', () => {
    it('should return id', () => {
      const model = makeModel(_.clone(serviceTypeData));
      expect(model[0].id).toEqual('1');
    });
  });
});
