import * as _ from 'lodash';
import Model from 'app/common/models/model';

export default class ServiceType extends Model {
  get id() {
    if (!this._id && this.model.id) {
      this._id = _.get(this, 'model.id', 0);
    }
    return this._id;
  }

  get serviceCatalogue() {
    if (!this._serviceCatalogue && this.model.name) {
      this._serviceCatalogue = _.get(this, 'model.name', '');
    }
    return this._serviceCatalogue;
  }

  get productCatalogue() {
    if (!this._productCatalogue && this.model.productCatalogue.name) {
      this._productCatalogue = _.get(this, 'model.productCatalogue.name', '');
    }
    return this._productCatalogue;
  }

  get isSelected() {
    if (_.isNull(this._isSelected) || _.isUndefined(this._isSelected)) {
      this._isSelected = true; // - Selected by default
    }
    return this._isSelected;
  }

  set isSelected(bool) {
    this._isSelected = bool;
  }

  get serviceCatalogueH() {
    if (!this._serviceCatalogueH && this.model) {
      this._serviceCatalogueH = _.get(this, 'model', '');
    }
    return this._serviceCatalogueH;
  }
}
