/* global inject */

import * as _ from 'lodash';
import ItemModel from './';

describe('ItemModel', () => {
  let MockData;
  let makeModel;

  beforeEach(inject(() => {
    MockData = [{
      name: 'Key for name is name',
    },
    {
      itemName: 'Key for name is itemName',
    },
    {
      workItem: {
        name: 'Key for name is workitem.name',
      },
    }];

    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        // eslint-disable-next-line no-param-reassign
        data[index] = Reflect.construct(ItemModel, [rawObject]);
      });
      return data;
    };
  }));

  it('all getters returns correct data', () => {
    const model = makeModel(_.clone(MockData));
    expect(model[0].name).toEqual('Key for name is name');
    expect(model[1].name).toEqual('Key for name is itemName');
    expect(model[2].name).toEqual('Key for name is workitem.name');
  });
});
