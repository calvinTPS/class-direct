import * as _ from 'lodash';
import Model from '../model';

export default class Item extends Model {
  get name() {
    if (this._name == null) {
      this._name = this.model.name || this.model.itemName || _.get(this.model, 'workItem.name');
    }
    return this._name;
  }
}
