import Model from './model';

export default class Attribute extends Model {

  set value(val) {
    this.model.value = val;
  }

  get value() {
    return this.model.value;
  }

}
