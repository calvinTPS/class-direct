import * as _ from 'lodash';
import Model from 'app/common/models/model';

export default class Flag extends Model {
  get id() {
    if (this._id == null) {
      this._id = _.get(this, 'model.id');
    }
    return this._id;
  }

  get label() {
    if (this._label == null) {
      this._label = _.get(this, 'model.name', '');
    }
    return this._label;
  }

  // nameCode returns the flag name and code in single text for searching
  get nameCode() {
    if (this._nameCode == null) {
      this._nameCode = `${_.get(this, 'model.name', '')} ${_.get(this, 'model.flagCode', '')}`;
    }
    return this._nameCode;
  }
}
