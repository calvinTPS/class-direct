/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-param-reassign */
import * as _ from 'lodash';
import FlagMocks from './flag.mocks.json';
import FlagModel from './flag';

describe('Flags', () => {
  let $rootScope,
    makeModel;

  const flagsData = FlagMocks;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        data[index] = Reflect.construct(FlagModel, [rawObject]);
      });
      return data;
    };
  }));

  describe('get id', () => {
    it('should return the id', () => {
      const model = makeModel(_.clone(flagsData));
      expect(model[0].id).toEqual(1);
    });
  });

  describe('get label', () => {
    it('should return the label', () => {
      const model = makeModel(_.clone(flagsData));
      expect(model[0].label).toMatch('United Kingdom');
    });
  });

  describe('flag nameCode', () => {
    it('should return the name and flagCode', () => {
      const model = makeModel(_.clone(flagsData));
      expect(model[0].nameCode).toMatch('United Kingdom (GBI)');
    });
  });
});
