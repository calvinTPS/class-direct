/* global inject */

import * as _ from 'lodash';
import AttachmentModel from './';
import mockAttachment from './attachment.mocks.json';

describe('AttachmentModel', () => {
  let makeModel;

  beforeEach(inject(() => {
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        // eslint-disable-next-line no-param-reassign
        data[index] = Reflect.construct(AttachmentModel, [rawObject]);
      });
      return data;
    };
  }));

  it('type getter returns correct data', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].type).toEqual('ZIP');
    expect(model[1].type).toEqual('PDF');
    expect(model[2].type).toEqual('PDF');
    expect(model[3].type).toEqual('PDF');
    expect(model[4].type).toEqual('PDF');
    expect(model[5].type).toBeUndefined();
  });

  it('name getter returns correct data', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].category).toEqual('Thickness Measurement Report - Argonaut');
    expect(model[1].category).toEqual('Thickness Measurement Report - Long');
    expect(model[2].category).toEqual('Thickness Measurement Report - Short');
    expect(model[3].category).toEqual('Statutory Record');
    expect(model[4].category).toEqual('Executive Hull Summary');
  });

  it('shortName getter returns correct data', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].shortName).toEqual('TM');
    expect(model[1].shortName).toEqual('TM');
    expect(model[2].shortName).toEqual('TM');
    expect(model[3].shortName).toEqual('Statutory Record');
    expect(model[4].shortName).toEqual('EHS');
    expect(model[7].shortName).toEqual('MMS Report');
  });

  it('category getter returns correct data', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].category).toEqual('Thickness Measurement Report - Argonaut');
    expect(model[1].category).toEqual('Thickness Measurement Report - Long');
    expect(model[2].category).toEqual('Thickness Measurement Report - Short');
    expect(model[3].category).toEqual('Statutory Record');
    expect(model[4].category).toEqual('Executive Hull Summary');
    expect(model[5].category).toEqual('MMS Report');
  });

  it('format getter returns correct data', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].format).toEqual('ZIP');
    expect(model[1].format).toEqual('PDF');
    expect(model[2].format).toEqual('PDF');
    expect(model[3].format).toEqual('PDF');
    expect(model[4].format).toEqual('PDF');
    expect(model[5].format).toBeUndefined();
  });

  it('isArgonautFile returns correctly if attachment if Argonaut type', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].isArgonautFile).toEqual(true);
    expect(model[1].isArgonautFile).toEqual(false);
    expect(model[2].isArgonautFile).toEqual(false);
  });

  it('get reportType() returns correctly the TM report file type', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].reportType).toEqual(2);
    expect(model[1].reportType).toEqual(3);
    expect(model[2].reportType).toEqual(4);
    expect(model[6].reportType).toEqual(15);
    expect(model[7].reportType).toEqual(14);
  });

  it('isMNCNAttachment returns correct boolean value', () => {
    const model = makeModel(_.clone(mockAttachment.attachments));
    expect(model[0].isMNCNAttachment).toEqual(false);
    expect(model[4].isMNCNAttachment).toEqual(false);
    expect(model[5].isMNCNAttachment).toEqual(false);
  });
});
