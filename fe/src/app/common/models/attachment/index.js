import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import Model from 'app/common/models/model';

export default class Attachment extends Model {
  constructor(model) {
    super(model);
    this.model = _.omitBy(this.model, _.isNull);
  }

  /**
   * Alias for category.
   */
  get name() {
    return this.category;
  }

  get shortName() {
    if (this.name) {
      if (this.name.indexOf(AppSettings.ATTACHMENTS.TM.Name) === 0) {
        return AppSettings.ATTACHMENTS.TM.ShortName;
      } else if (this.name.indexOf(AppSettings.ATTACHMENTS.EHS.Name) === 0) {
        return AppSettings.ATTACHMENTS.EHS.ShortName;
      }
    }

    // No short name
    return this.name;
  }

  get category() {
    if (this._category == null) {
      this._category = _.get(this, 'model.attachmentCategoryH.name');
    }
    return this._category;
  }

  get type() {
    if (this._type == null) {
      this._type = _.get(this, 'model.attachmentTypeH.fileExtension');
    }
    return this._type;
  }

  get date() {
    if (this._date == null) {
      this._date = _.get(this, 'model.creationDate');
    }
    return this._date;
  }

  /**
   * Alias for type.
   */
  get format() {
    return this.type;
  }

  get isArgonautFile() {
    return this.name === AppSettings.ATTACHMENTS.TM.ArgonautName;
  }

  get reportType() {
    if (this._reportType == null) {
      this._reportType = _.get(this.model, 'attachmentCategoryH.id');
    }
    return this._reportType;
  }

  get isMNCNAttachment() {
    return this.category === AppSettings.ATTACHMENTS.MNCN.ShortName;
  }
}
