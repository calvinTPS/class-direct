import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import Model from 'app/common/models/model';

export default class Asset extends Model {
  /**
   * Class Direct will treat the asset code (e.g. LRV123, IHS12341234)
   * as the Asset ID. CD Backend will convert this code as necessary for BE use via
   * a Camel interceptor.
   *
   * @returns {*}
   */
  get id() {
    // _.id is defined in the model, hence use _cdId
    if (this._cdId == null) {
      this._cdId = _.get(this, 'model.code', null);
    }

    return this._cdId;
  }

  /**
   * Whether or not this asset is an LR-classed asset.
   *
   * @returns {boolean}
   */
  get isLRAsset() {
    if (this._isLRAsset == null) {
      this._isLRAsset =
        this.isFromMAST && this.classStatus !== AppSettings.CLASS_STATUSES.NOT_LR_CLASSED;
    }

    return this._isLRAsset;
  }

  get isIHSAsset() {
    if (this._isIHSAsset == null) {
      this._isIHSAsset = this.id.indexOf(AppSettings.ASSET_CODE_NON_LR) === 0;
    }

    return this._isIHSAsset;
  }

  /**
   * Whether or not this asset is from MAST db.
   *
   * @returns {boolean}
   */
  get isFromMAST() {
    if (this._isFromMAST == null) {
      this._isFromMAST = this.id.indexOf(AppSettings.ASSET_CODE_LR) === 0;
    }

    return this._isFromMAST;
  }

  /**
   * @returns {boolean} True if is LR asset and not restricted.
   */
  get isAccessibleLRAsset() {
    return this.isLRAsset && !this.restricted;
  }

  get isAccessibleMASTAsset() {
    return this.isFromMAST && !this.restricted;
  }

  get classStatus() {
    if (this._classStatus == null) {
      // If asset is in MAST then class status is guaranteed to be non-null,
      // even if not LR classed.
      this._classStatus = this.isFromMAST ?
        _.get(this, 'model.classStatusDto.name', null) : AppSettings.CLASS_STATUSES.NOT_LR_CLASSED;
    }
    return this._classStatus;
  }

  get buildDate() {
    if (!this._buildDate) {
      this._buildDate = _.get(this, 'model.buildDate') ||
          _.get(this, 'model.ihsAssetDto.ihsAsset.dateOfBuild');
    }
    return this._buildDate;
  }

  get buildDateFormatted() {
    return this.convertDate(this.buildDate, true);
  }

  get isFavourite() {
    if (this._isFavourite == null) {
      this._isFavourite = _.get(this, 'model.isFavourite', false);
    }
    return this._isFavourite;
  }

  set isFavourite(isFavourite) {
    this._isFavourite = !!isFavourite;
  }

  get imo() {
    if (!this._imo) {
      this._imo = _.get(this, 'model.imoNumber') ||
        _.get(this, 'model.ihsAsset.id') ||
        _.get(this, 'model.ihsAssetDto.id', null);
    }
    return this._imo;
  }

  get ihsImo() {
    // For Asset in ihs, return the id
    if (!this._ihsImo) {
      this._ihsImo = _.get(this, 'model.ihsAssetDto.id', null);
    }
    return this._ihsImo;
  }

  get flag() {
    if (!this._flag) {
      this._flag = _.get(this, 'model.flagStateDto.name') ||
          _.get(this, 'model.ihsAssetDto.ihsAsset.flagName');
    }
    return this._flag;
  }

  get flagCode() {
    if (!this._flag) {
      this._flagCode = _.get(this, 'model.flagStateDto.flagCode') ||
          _.get(this, 'model.ihsAssetDto.ihsAsset.flag');
    }
    return this._flagCode;
  }

  /**
   * @returns {string} The name of the asset type.
   */
  get type() {
    if (!this._type) {
      this._type = _.get(this, 'model.assetTypeHDto.name') ||
          _.get(this, 'model.assetType.name') ||
          _.get(this, 'model.ihsAssetDto.ihsAsset.ihsAssetType.statDeCode');
    }
    return this._type;
  }

  /**
   * @returns {string} The asset type code.
   */
  get typeCode() {
    if (this._typeCode == null) {
      this._typeCode = _.get(this, 'model.assetTypeHDto.code') ||
          _.get(this, 'model.assetType.code') ||
          _.get(this, 'model.ihsAssetDto.ihsAsset.ihsAssetType.stat5Code');
    }

    return this._typeCode;
  }

  get name() {
    if (!this._name) {
      this._name = _.get(this, 'model.name', '');
    }
    return this._name;
  }

  get clientName() {
    return _.get(this, 'model.customers[0].customer.name');
  }

  get grossTonnage() {
    if (!this._grossTonnage) {
      this._grossTonnage = _.get(this, 'model.grossTonnage', null);
    }
    return this._grossTonnage;
  }

  get overallStatus() {
    return _.kebabCase(_.get(this, 'model.dueStatusH.name'));
  }

  get selected() {
    if (this._selected == null) {
      this._selected = false;
    }
    return this._selected;
  }

  set selected(bool) {
    this._selected = bool;
  }

  get ownerName() {
    if (!this._ownerName) {
      this._ownerName = _.get(this, 'model.ihsAssetDto.ihsAsset.owner', null);
    }
    return this._ownerName;
  }

  get ownerCode() {
    if (!this._ownerCode) {
      this._ownerCode = _.get(this, 'model.ihsAssetDto.ihsAsset.ownerCode', null);
    }
    return this._ownerCode;
  }

  get isEORAsset() {
    if (_.isEmpty(Asset.EOR_ASSETS)) {
      return false;
    }
    const eorAsset = _.find(Asset.EOR_ASSETS, ['imoNumber', this.imo]);
    if (eorAsset) {
      this._assetExpiryDate = _.get(eorAsset, 'assetExpiryDate');
      this._assetExpiryDateFormatted = this.convertDate(this._assetExpiryDate);
      return true;
    }
    return false;
  }

  get assetExpiryDateFormatted() {
    return this._assetExpiryDateFormatted;
  }

  get assetExpiryDate() {
    return this._assetExpiryDate;
  }

  set assetExpiryDate(date) {
    this._assetExpiryDate = date;
  }

  get cfoOffice() {
    if (this._cfoOffice == null && _.isObject(this.model.cfoOfficeH)) {
      // If the associated Office value in the MAST Office table has Active = No (deleted=true),
      // the System will not display the Office details
      if (this.model.cfoOfficeH.deleted) {
        return null;
      }
      const office = this.model.cfoOfficeH;
      this._cfoOffice = Object.assign({}, office);

      let address = office.addressLine1 || '';
      address += office.addressLine2 ? `, ${office.addressLine2}` : '';
      address += office.addressLine3 ? `, ${office.addressLine3}` : '';
      this._cfoOffice.address = address;
    }
    return this._cfoOffice;
  }

  get class() {
    if (this._class == null) {
      this._class = _.get(this, 'model.ihsAssetDto.ihsAsset.classList', '-');
    }
    return this._class;
  }

  // return '-' if the property is null
  get deathWeight() {
    if (this._deathWeight == null) {
      this._deathWeight = _.get(this, 'model.ihsAssetDto.ihsAsset.dwt', '-');
    }
    return this._deathWeight;
  }

  /**
   * Whether or not the given asset id (code) is an LR asset.
   *
   * @param assetId {string}
   * @returns {boolean}
   */
  static isLRAsset(assetId) {
    return assetId.indexOf(AppSettings.ASSET_CODE_LR) === 0;
  }

  static EOR_ASSETS = [];

  static PAYLOAD_FIELDS = {
    FLEET_LIST: [
      'assetType.*',
      'assetTypeHDto.*',
      'buildDate',
      'builder',
      'cfoOfficeH.*',
      'classStatusDto.*',
      'code',
      'customers',
      'customersH.*',
      'deadWeight',
      'dueStatusH.*',
      'flagStateDto.*',
      'grossTonnage',
      'hasActiveServices',
      'id',
      'ihsAsset.id',
      'ihsAssetDto.id',
      'ihsAssetDto.ihsAsset.classList',
      'ihsAssetDto.ihsAsset.dateOfBuild',
      'ihsAssetDto.ihsAsset.dwt',
      'ihsAssetDto.ihsAsset.flag',
      'ihsAssetDto.ihsAsset.grossTonnage',
      'ihsAssetDto.ihsAsset.ihsAssetType.*',
      'ihsAssetDto.ihsAsset.name',
      'ihsAssetDto.ihsAsset.owner',
      'ihsAssetDto.ihsAsset.ownerCode',
      'ihsAssetDto.ihsAsset.yardNumber',
      'ihsAssetDto.ihsAsset.flagName',
      'imoNumber',
      'isEOR',
      'isFavourite',
      'isLead',
      'isMMSService',
      'name',
      'offices.*',
      'restricted',
      'yardNumber',
    ],
  }
}
