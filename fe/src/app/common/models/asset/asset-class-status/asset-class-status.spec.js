/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-param-reassign */
import * as _ from 'lodash';
import AssetClassStatusMocks from './asset-class-status.mocks.json';
import AssetClassStatusModel from './asset-class-status';

describe('AssetClassModel', () => {
  let $rootScope,
    makeModel;

  const assetClassData = AssetClassStatusMocks;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        data[index] = Reflect.construct(AssetClassStatusModel, [rawObject]);
      });
      return data;
    };
  }));

  describe('get id', () => {
    it('should return the id', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].id).toEqual(1);
    });
  });

  describe('get name', () => {
    it('should return the name', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].name).toMatch('This Is An Asset Class Sample Name');
    });
  });

  describe('get description', () => {
    it('should return the description', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].description).toMatch('This is an asset class sample description');
    });
  });

  describe('get selected', () => {
    it('should return false', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].selected).toEqual(false);
    });
  });

  describe('set selected', () => {
    it('should return same boolean value', () => {
      const model = makeModel(_.clone(assetClassData));
      model[0].selected = true;
      expect(model[0].selected).toEqual(true);

      model[0].selected = false;
      expect(model[0].selected).toEqual(false);
    });
  });
});
