import * as _ from 'lodash';
import Model from 'app/common/models/model';

export default class AssetType extends Model {
  get id() {
    // _id exists in object
    if (this.__id == null) {
      this.__id = _.get(this, 'model.id', 0);
    }
    return this.__id;
  }

  get name() {
    if (this._name == null) {
      this._name = _.get(this, 'model.name', '');
    }
    return this._name;
  }

  get category() {
    if (this._category == null) {
      this._category = _.get(this, 'model.categoryDto.name', '');
    }
    return this._category;
  }

  get parent() {
    return _.get(this, 'model.parent');
  }

  set parent(parentModel) {
    this.model.parent = parentModel;
  }

  get selected() {
    if (this._selected == null) {
      this._selected = false;
    }
    return this._selected;
  }

  set selected(bool) {
    this._selected = bool;
  }
}
