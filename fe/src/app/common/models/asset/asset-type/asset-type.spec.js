/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable no-param-reassign, no-unused-vars */

import * as _ from 'lodash';
import AssetTypeMocks from './asset-type.mocks.json';
import AssetTypeModel from './asset-type';

describe('AssetTypeModel', () => {
  let $rootScope;
  let makeModel;

  const assetClassData = AssetTypeMocks;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        data[index] = Reflect.construct(AssetTypeModel, [rawObject]);
      });
      return data;
    };
  }));

  describe('get id', () => {
    it('should return the id', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].id).toEqual(7);
    });
  });

  describe('get name', () => {
    it('should return the name', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].name).toMatch('Cargo Carrying');
    });
  });

  describe('get category', () => {
    it('should return the category', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].category).toMatch('Vessel');
    });
  });

  describe('get selected', () => {
    it('should return false', () => {
      const model = makeModel(_.clone(assetClassData));
      expect(model[0].selected).toEqual(false);
    });
  });

  describe('set selected', () => {
    it('should return same boolean value', () => {
      const model = makeModel(_.clone(assetClassData));
      model[0].selected = true;
      expect(model[0].selected).toEqual(true);

      model[0].selected = false;
      expect(model[0].selected).toEqual(false);
    });
  });
});
