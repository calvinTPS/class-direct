/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AssetMocks from './asset.mocks.json';
import AssetModel from './asset';

describe('Asset', () => {
  let $rootScope, makeModel;
  const assetData = _.cloneDeep(AssetMocks);

  AssetModel.EOR_ASSETS = [
    { imoNumber: 1000019, assetExpiryDate: '2017-01-01' },
  ];

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = data => Reflect.construct(AssetModel, [data]);
  }));

  describe('get buildDate', () => {
    it('should return the buildDate', () => {
      const model = makeModel(_.cloneDeep(assetData));
      expect(model.buildDate).toMatch('2015-08-19T00:00:00Z');

      model.classStatusDto.name = 'Not LR Classed';
      expect(model.buildDate).toMatch('2015-08-19T00:00:00Z');

      // Test fallback to IHS Data
      const ihsAssetData = _.cloneDeep(assetData);
      ihsAssetData.buildDate = null;
      const nonLRModel = makeModel(ihsAssetData);
      expect(nonLRModel.buildDate).toMatch('1111-11-01T00:00:00Z');
    });
  });

  describe('get id', () => {
    it('should return the ASSET CODE', () => {
      const model = makeModel(assetData);
      expect(model.id).toEqual('LRV1');
    });
  });

  describe('get isLRAsset', () => {
    it('should indicate whether or not it is an LR-classed Asset', () => {
      // LR-classed asset
      const model = makeModel(assetData);
      expect(model.isLRAsset).toEqual(true);

      // Non LR Classed class status, but present in MAST
      const nonLRMastAssetData = _.cloneDeep(assetData);
      nonLRMastAssetData.classStatusDto.name = 'Not LR Classed';
      const nonLRMastAsset = makeModel(nonLRMastAssetData);
      expect(nonLRMastAsset.isLRAsset).toEqual(false);

      // IHS asset
      const cloneAssetData = _.cloneDeep(assetData);
      cloneAssetData.code = 'IHS123';
      const nonLRModel = makeModel(cloneAssetData);
      expect(nonLRModel.isLRAsset).toEqual(false);
    });
  });

  describe('get isFromMAST', () => {
    it('should indicate whether or not the asset is from MAST DB', () => {
      // LR-classed asset
      const model = makeModel(assetData);
      expect(model.isFromMAST).toEqual(true);

      // Non LR Classed class status, but present in MAST
      const nonLRMastAssetData = _.cloneDeep(assetData);
      nonLRMastAssetData.classStatusDto.name = 'Not LR Classed';
      const nonLRMastAsset = makeModel(nonLRMastAssetData);
      expect(nonLRMastAsset.isFromMAST).toEqual(true);

      // IHS asset
      const cloneAssetData = _.cloneDeep(assetData);
      cloneAssetData.code = 'IHS123';
      const nonLRModel = makeModel(cloneAssetData);
      expect(nonLRModel.isFromMAST).toEqual(false);
    });
  });

  describe('get isAccessibleLRAsset', () => {
    it('should return true if asset is LR asset, and is not restricted', () => {
      expect(makeModel({ code: 'LRV123', restricted: true }).isAccessibleLRAsset).toEqual(false);
      expect(makeModel({ code: 'LRV456', restricted: false }).isAccessibleLRAsset).toEqual(true);
      expect(makeModel({ code: 'IHS123456', restricted: true }).isAccessibleLRAsset).toEqual(false);
      expect(makeModel({ code: 'IHS000000', restricted: false }).isAccessibleLRAsset).toEqual(false);
    });
  });

  describe('Asset.isLRAsset', () => {
    it('indicates whether given asset ID is LR or non-LR', () => {
      expect(AssetModel.isLRAsset('IHS123123')).toEqual(false);
      expect(AssetModel.isLRAsset('LRV123')).toEqual(true);
    });
  });

  describe('get imo', () => {
    it('should return the imoNumber', () => {
      const model = makeModel(assetData);
      expect(model.imo).toMatch('123123');
    });

    it('falls back to ihsAsset.id', () => {
      assetData.imoNumber = null;
      const model = makeModel(assetData);
      expect(model.imo).toMatch('1000019');
    });
  });

  describe('get ihsImo', () => {
    it('should return the ihsImo Number', () => {
      const model = makeModel(assetData);
      expect(model.ihsImo).toMatch('1000021');
    });

    it('falls back to null', () => {
      assetData.ihsAssetDto.id = null;
      const model = makeModel(assetData);
      expect(model.ihsImo).toBeNull();
    });
  });

  describe('get flag', () => {
    it('should return the flag', () => {
      const model = makeModel(_.cloneDeep(assetData));
      expect(model.flag).toMatch('Jersey');

      // Test fallback to IHS data
      const ihsAssetData = _.cloneDeep(assetData);
      ihsAssetData.flagStateDto.name = null;
      const nonLRModel = makeModel(ihsAssetData);
      expect(nonLRModel.flag).toMatch('flag 1');
    });
  });

  describe('get type', () => {
    it('should return the type based on the assetType for certain assetType', () => {
      const model = makeModel(_.cloneDeep(assetData));
      expect(model.type).toMatch('General Cargo Barge, non propelled');

      // Test IHS data fallback
      const ihsAssetData = _.cloneDeep(assetData);
      ihsAssetData.assetTypeHDto.name = null;
      const nonLRModel = makeModel(ihsAssetData);
      expect(nonLRModel.type).toMatch('LPG Tanker');
    });
  });

  describe('get typeCode', () => {
    it('should return the asset type code', () => {
      const model = makeModel(_.cloneDeep(assetData));
      expect(model.typeCode).toMatch('Y11A5GC');

      model.classStatusDto.name = 'Not LR Classed';
      expect(model.typeCode).toMatch('Y11A5GC');

      // Test IHS data fallback
      const ihsAssetData = _.cloneDeep(assetData);
      ihsAssetData.assetTypeHDto.code = null;
      const nonLRModel = makeModel(ihsAssetData);
      expect(nonLRModel.typeCode).toMatch('A11B2TG');
    });
  });

  describe('get name', () => {
    it('should return the asset name', () => {
      const model = makeModel(assetData);
      expect(model.name).toMatch('LADY K II');
    });
  });

  describe('get grossTonnage', () => {
    it('should return the grossTonnage', () => {
      const model = makeModel(assetData);
      expect(model.grossTonnage).toMatch('100');
    });
  });

  describe('get overallStatus', () => {
    it('should return the overallStatus', () => {
      const model = makeModel(assetData);
      expect(model.overallStatus).toMatch('imminent');
    });
  });

  describe('get is favourite flag', () => {
    it('should return the favourite flag state', () => {
      const model = makeModel(assetData);
      expect(model.isFavourite).toBe(true);
    });
  });

  describe('get class status', () => {
    it('should return the correct class status', () => {
      const model = makeModel(assetData);
      expect(model.classStatus).toMatch('Class Contemplated');

      const ihsAssetData = _.cloneDeep(assetData);
      ihsAssetData.code = 'IHS123123';
      ihsAssetData.classStatusDto = null;
      const ihsModel = makeModel(ihsAssetData);
      expect(ihsModel.classStatus).toEqual('Not LR Classed');
    });
  });

  describe('get selected', () => {
    it('should return false', () => {
      const model = makeModel(assetData);
      expect(model.selected).toEqual(false);
    });
  });

  describe('set selected', () => {
    it('should return same boolean value', () => {
      const model = makeModel(assetData);
      model.selected = true;
      expect(model.selected).toEqual(true);

      model.selected = false;
      expect(model.selected).toEqual(false);
    });
  });

  describe('get ownerName', () => {
    it('should return the owner name', () => {
      const model = makeModel(assetData);
      expect(model.ownerName).toMatch('The Owner');
    });
  });

  describe('get ownerCode', () => {
    it('should return the owner code', () => {
      const model = makeModel(assetData);
      expect(model.ownerCode).toMatch('5000006');
    });
  });

  describe('get isEORAsset', () => {
    it('should return true if asset is EOR else false', () => {
      const model = makeModel(_.cloneDeep(assetData));
      expect(model.isEORAsset).toEqual(true);
    });
  });

  describe('get/set assetExpiryDate', () => {
    it('should return assetExpiryDate', () => {
      const model = makeModel(_.cloneDeep(assetData));
      expect(model.isEORAsset).toEqual(true);
      expect(model.assetExpiryDate).toEqual('2017-01-01');
      model.assetExpiryDate = '2018-01-10';
      expect(model.assetExpiryDate).toEqual('2018-01-10');
    });
  });

  describe('get cfoOffice', () => {
    it('should return the correct value for cfoOffice', () => {
      const model = makeModel(_.cloneDeep(assetData));

      expect(model.cfoOffice.name).toEqual('Mr CFO');
      expect(model.cfoOffice.emailAddress).toEqual('email@lr.com');
      expect(model.cfoOffice.phoneNumber).toEqual('556677889900');
      expect(model.cfoOffice.address).toEqual('Address Line 1, Address Line 2, Address Line 3');

      const mockAsset2 = {
        cfoOfficeH: {
          _id: '1652466a-5f60-4def-b071-55fa46430bbf',
          addressLine1: 'Address Line 1',
          deleted: 1,
          emailAddress: 'email@lr.com',
          phoneNumber: '556677889900',
        },
      };
      const model2 = makeModel(mockAsset2);
      expect(model2.cfoOffice).toBeNull();
    });
  });

  describe('get isIHSAsset', () => {
    it('should correctly say whether it is an IHS asset', () => {
      const data = _.cloneDeep(assetData);
      data.code = 'IHS999';
      const model = makeModel(data);
      expect(model.isIHSAsset).toEqual(true);

      const data2 = _.cloneDeep(assetData);
      data2.code = 'LRV999';
      const model2 = makeModel(data2);
      expect(model2.isIHSAsset).toEqual(false);
    });
  });
});
