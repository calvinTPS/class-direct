import * as _ from 'lodash';
import Model from './model';
import Port from './port';

export default class SurveyRequestModel extends Model {
  /* @ngInject */
  constructor(
    model
  ) {
    super(model);

    this._otherEmails = [];
    this._serviceIds = [];
    this._port = new Port();
    this.model = this.model || {};
  }

  get groupedServices() {
    return this._groupedServices;
  }

  set groupedServices(groupedServices) {
    this._groupedServices = groupedServices;
  }

  get serviceIds() {
    return this._serviceIds;
  }

  set serviceIds(ids) {
    this._serviceIds = ids;
  }

  get otherEmails() {
    return this._otherEmails;
  }

  set otherEmails(otherEmails) {
    this._otherEmails = otherEmails;
  }

  get port() {
    return this._port;
  }

  set port(port) {
    this._port = port;
  }

  get portAddress() {
    if (this._port) {
      return _.get(this._port.sdo, 'addressLine1', '') + _.get(this._port.sdo, 'addressLine2', '') +
        _.get(this._port.sdo, 'addressLine3', '');
    }
    return null;
  }
}
