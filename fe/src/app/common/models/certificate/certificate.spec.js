/* global inject */

import * as _ from 'lodash';
import CertificateMocks from './certificate.mocks.json';
import CertificateModel from './';

describe('CertificateModel', () => {
  let makeModel;

  beforeEach(inject(() => {
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        // eslint-disable-next-line no-param-reassign
        data[index] = Reflect.construct(CertificateModel, [rawObject]);
      });
      return data;
    };
  }));

  it('certificateName getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].certificateName).toEqual('ABC DEF');
    expect(model[1].certificateName).toEqual('GHI JKL');
    expect(model[2].certificateName).toEqual('MNO PQR');
  });

  it('certificateNumber getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].certificateNumber).toEqual('TEST CERT 3');
    expect(model[1].certificateNumber).toEqual('TEST CERT 4');
    expect(model[2].certificateNumber).toEqual('TEST CERT 5');
  });

  it('issuingSurveyor getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].issuingSurveyor).toEqual('John Doe 1');
    expect(model[1].issuingSurveyor).toEqual('Jane Doe 2');
    expect(model[2].issuingSurveyor).toEqual('Jack Doe 3');
  });

  it('office getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].office).toEqual('Office 1');
    expect(model[1].office).toEqual('Office 2');
    expect(model[2].office).toEqual('Office 3');
  });

  it('issueDate getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].issueDate).toEqual('2015-01-02T00:00:00Z');
    expect(model[1].issueDate).toEqual('2015-01-02T00:00:00Z');
    expect(model[2].issueDate).toEqual('2015-01-02T00:00:00Z');
  });

  it('formNumber getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].formNumber).toEqual('1123');
    expect(model[1].formNumber).toEqual('1124');
    expect(model[2].formNumber).toEqual('1125');
  });

  it('version getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].version).toEqual(2015.06);
    expect(model[1].version).toEqual(2015.07);
    expect(model[2].version).toEqual(2015.08);
  });

  it('endorsedDate getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].endorsedDate).toEqual('2015-01-02T00:00:00Z');
    expect(model[1].endorsedDate).toEqual('2015-01-02T00:00:00Z');
    expect(model[2].endorsedDate).toEqual('2015-01-02T00:00:00Z');
  });

  it('expiryDate getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].expiryDate).toEqual('2020-01-01T00:00:00Z');
    expect(model[1].expiryDate).toEqual('2020-01-01T00:00:00Z');
    expect(model[2].expiryDate).toEqual('2017-01-25T00:00:00Z');
  });

  it('extendedDate getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].extendedDate).toEqual('2020-01-02T00:00:00Z');
    expect(model[1].extendedDate).toEqual('2020-01-02T00:00:00Z');
    expect(model[2].extendedDate).toEqual('2020-01-02T00:00:00Z');
  });

  it('status getter returns correct data', () => {
    const model = makeModel(_.clone(CertificateMocks));
    expect(model[0].status).toEqual('Issued');
    expect(model[1].status).toEqual('Withdrawn');
    expect(model[2].status).toEqual('Issued');
    expect(model[3].status).toEqual(null);
  });
});
