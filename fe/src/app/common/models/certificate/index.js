import * as _ from 'lodash';
import Model from 'app/common/models/model';

export default class Certificate extends Model {
  get certificateName() {
    if (this._certificateName == null) {
      this._certificateName = _.get(this, 'model.name', null);
    }
    return this._certificateName;
  }

  get issuingSurveyor() {
    if (this._issuingSurveyor == null) {
      this._issuingSurveyor = _.get(this, 'model.surveyor.name', null);
    }
    return this._issuingSurveyor;
  }

  get office() {
    if (this._office == null) {
      this._office = _.get(this, 'model.issuedAt', null);
    }
    return this._office;
  }

  get formNumber() {
    if (this._formNumber == null) {
      this._formNumber = _.get(this, 'model.certificateTemplateHDto.formNumber', null);
    }
    return this._formNumber;
  }

  get version() {
    if (this._version == null) {
      this._version = _.get(this, 'model.certificateTemplateHDto.version', null);
    }
    return this._version;
  }

  get certificateType() {
    if (this._certificateType == null) {
      this._certificateType = _.get(this, 'model.certificateTypeDto.name', null);
    }
    return this._certificateType;
  }

  /**
   * Get Certificate status based on statusDto provided by backend OR if today's
   * date has pass the expiryDate
   * @return {String} : 'Issued', 'Withdrawn', 'Expired'
  */
  get status() {
    if (this._status == null) {
      this._status = _.get(this, 'model.certificateStatusDto.name', null);
    }
    return this._status;
  }
}
