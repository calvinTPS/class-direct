import * as _ from 'lodash';

export default class FormModel {
  constructor(data, service) {
    this._data = data;
    _.assignIn(this, service);
  }

  //  ---------------------------------------------------------
  $get(name, param) {
    if (!this.elements) {
      this.elements = {};
      this.list.forEach((val) => {
        if (val.name) {
          this.elements[val.name] = val;
        }
      });
    }
    const res = this.elements[name];
    return param ? _.get(res, param) : res;
  }

  //  ---------------------------------------------------------
  getModel() {
    return this;
  }

  //  ---------------------------------------------------------
  isEmpty(value) {
    return _.isNil(value) ||
      typeof value === 'object' && _.isEmpty(value);
  }

  //  ---------------------------------------------------------
  //  Set values to From elements
  setValues(values, not) {
    values = values || this._data;

    this.list = this.list.filter((item) => {
      item.$getModel = this.getModel.bind(this);
      item.$get = this.$get.bind(this);

      if (item.name && !_.includes(not, item.name)) {
        item.value = _.get(values, item.name);
      }

      return item.autoHide ? !this.isEmpty(item.value) : true;
    });
  }

  //  ---------------------------------------------------------
  //  Set disabled to From elements
  setDisabled(disable, not) {
    this.list.forEach((item) => {
      if (!_.includes(not, item.name)) {
        item.disabled = disable;
      }
    });
  }

  //  ---------------------------------------------------------
  //  Get values from Form and set to model data
  get values() {
    this._data = this._data || {};

    this.list.forEach((item) => {
      if (item.name) {
        const oldValue = _.get(this._data, item.name);
        if (oldValue !== item.value) {
          _.set(this._data, item.name, item.value);
        }
      }
    });
    return this._data;
  }
}
