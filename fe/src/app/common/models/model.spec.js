/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import Model from './model';

describe('Assets', () => {
  let $rootScope, makeSubModel;

  const mockData = {
    foo: {
      baz: true,
    },
    bar: {
      baz: true,
    },
    testNullField: null,
    dueDate: '2009-08-16T02:02:00Z',
    closureDate: null,
  };

  class SubModel extends Model {
  }

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeSubModel = (data, decorate) => new SubModel(data, decorate);
  }));

  describe('reflection', () => {
    it('properties in data object will be assigned to the model directly', () => {
      const subModel = makeSubModel(mockData, true);
      expect(subModel.foo).toEqual({ baz: true });
      expect(subModel.bar).toEqual({ baz: true });
    });
    it('formats the dates', () => {
      const subModel = makeSubModel(mockData, true);
      expect(subModel.dueDateFormatted).toEqual('16 Aug 2009');
    });
    it('decorates null fields with a `-`', () => {
      const subModel = makeSubModel(mockData, true);
      expect(subModel.testNullField).toEqual('-');
    });
  });
});
