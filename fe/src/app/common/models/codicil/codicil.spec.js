/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-param-reassign */
import * as _ from 'lodash';
import codicilMockData from './codicil.mocks.json';
import codicilModel from './codicil';

describe('CodicilModel', () => {
  let $rootScope,
    makeModel;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = data => Reflect.construct(codicilModel, [data]);
  }));

  describe('get id', () => {
    it('should return the id', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.id).toEqual(1);
    });

    it('should return the correct typeName', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.typeName).toEqual('Condition of class');
    });

    it('should return the correct repairs', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.repairs[0].name).toEqual('Conditions of Class 1');
      expect(model.repairs[0].items[0].name).toEqual('Item 1');
      expect(model.repairs[0].items[1].name).toEqual('Item 2');
    });

    it('should return the correct status', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.status).toEqual('Cancelled');
    });

    it('should return the correct due status', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.dueStatus).toEqual('Not Due');
    });

    it('should return the correct category', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.category).toEqual('Hull');
    });

    it('should return the correct ', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.defect.id).toEqual(1);
    });

    it('should return the correct job', () => {
      const data = _.clone(codicilMockData);
      const model = makeModel(data); // Should use data.job
      expect(model.job.id).toEqual(1);

      const data2 = _.clone(codicilMockData);
      data2.jobH = { id: 2 };
      const model2 = makeModel(data2); // Should use data2.jobH
      expect(model2.job.id).toEqual(2);
    });

    it('should return the correct items', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.items[0].name).toEqual('Item name 1');
    });

    it('should return the correct type', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.type).toEqual('COC');
    });

    it('should return the correct codicil type', () => {
      let model = makeModel(_.clone(codicilMockData));
      expect(model.codicilType).toEqual('COC');

      codicilMockData.deficiencyH = [
        { id: 1, title: 'Deficiency 1' },
        { id: 2, title: 'Deficiency 2' },
      ];
      model = makeModel(_.clone(codicilMockData));
      expect(model.codicilType).toEqual('SF');
    });

    it('should return correct deficiency id', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.deficiencyId).toEqual(1);
    });

    it('should return correct revisions', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.revisions.length).toEqual(2);
      expect(model.revisions[0].jobId).toEqual(1);
      expect(model.revisions[0].action).toEqual('Raised');
      expect(model.revisions[0].dueDate).toEqual('2017-12-12');
      expect(model.revisions[0].logEntry).toEqual(123);
      expect(model.revisions[0].jobNumber).toEqual('4454');
      expect(model.revisions[1].jobNumber).toEqual('9961');
    });

    it('should return correct imposed date epoch', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.imposedDateEpoch).toEqual(Date.parse(model.imposedDate));
    });

    it('should return correct due date epoch', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.dueDateEpoch).toEqual(Date.parse(model.dueDate));
    });

    it('should return truncated description', () => {
      const model = makeModel(_.clone(codicilMockData));
      expect(model.truncatedDescription).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas mollis odio quis elit commodo, vel dapibus nisl hendrerit. Nullam consectetur eu massa interdum ultricies. Curabitur eu velit eget qua...');
      const mockCodicil = _.clone(codicilMockData);
      // trunctedDescription will not at ... for description with characters equal or less than 200
      mockCodicil.description = 'Lorem ipsum dolor sit amet';
      const model2 = makeModel(mockCodicil);
      expect(model2.truncatedDescription).toEqual('Lorem ipsum dolor sit amet');
    });
  });
});
