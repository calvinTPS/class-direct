/*
 * Used by COC, AI, AN and STATUTORY FINDINGS
 */

import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import DefectModel from '../defect/';
import ItemModel from '../item/';
import JobModel from '../job';
import Model from '../model';
import RepairModel from '../repair/';
import translation from '../../../../../resources/locales/en-GB.json';

export default class Codicil extends Model {
  get dueStatus() {
    if (this._dueStatus == null) {
      this._dueStatus = _.get(this.dueStatusH, 'name');
    }
    return this._dueStatus;
  }

  get typeName() {
    return translation[_.get(AppSettings.CODICILS_META[this.codicilType], 'NAME')];
  }

  get repairs() {
    if (this._repairs == null && this.model.repairH) {
      this._repairs = this.model.repairH.map(repair => Reflect.construct(RepairModel, [repair]));
    }
    return this._repairs;
  }

  get status() {
    return _.get(this.statusH, 'name');
  }

  get category() {
    const category = this.model.categoryH || this.model.category;
    if (this._category == null && category) {
      this._category = _.get(category, 'name');
    }
    return this._category;
  }

  get defect() {
    if (this._defect == null && this.model.defectH) {
      this._defect = this.model.defectH ?
        Reflect.construct(DefectModel, [this.model.defectH]) : null;
    }
    return this._defect;
  }

  get job() {
    if (this._job == null) {
      const job = this.model.jobH || this.model.job;
      this._job = job ? Reflect.construct(JobModel, [job]) : null;
    }
    return this._job;
  }

  get items() {
    if (this._items == null && (this.model.itemsH || this.model.assetItemH)) {
      const items = this.model.itemsH || (this.model.assetItemH ? [this.model.assetItemH] : null);
      this._items = items.map(item => Reflect.construct(ItemModel, [item]));
    }
    return this._items;
  }

  get codicilType() {
    if (this._codicilType == null) {
      if (_.has(this.model, 'deficiencyH')) {
        this._codicilType = AppSettings.CODICILS_META.SF.ID;
      } else {
        this._codicilType = this.model.codicilType;
      }
    }
    return this._codicilType;
  }

  get type() {
    return this.codicilType;
  }

  get deficiencyId() {
    if (this._deficiencyId == null) {
      this._deficiencyId = _.get(this, 'model.defect.id');
    }
    return this._deficiencyId;
  }

  get revisions() {
    if (this._revisions == null && this.model.history) {
      // It is return direct from MAST without any changes from CDF BE
      //   So need to convert back to a json object
      // It comes from mast_db.mast_asset_codicil
      //   where mast_asset_codicil.history_content is not null
      // It is shown on the AI/COC lists and not details page
      this._revisions = angular.fromJson(_.get(this, 'model.history'));
    }

    return this._revisions;
  }

  get truncatedDescription() {
    if (this._truncatedDescription == null) {
      const description = _.get(this.model, 'description', '');
      if (description.length > 200) {
        this._truncatedDescription = `${_.get(this.model, 'description', '')
          .substr(0, AppSettings.DEFAULT_PARAMS.TRUNCATED_DESCRIPTION_CHARACTER)
          .trim()}...`;
      } else {
        this._truncatedDescription = description;
      }
    }
    return this._truncatedDescription;
  }

  get imposedDateEpoch() {
    if (this._imposedDateEpoch == null && this.model.imposedDate) {
      this._imposedDateEpoch = this.model.imposedDateEpoch || Date.parse(this.model.imposedDate);
    }
    return this._imposedDateEpoch;
  }

  get dueDateEpoch() {
    if (this._dueDateEpoch == null && this.model.dueDate) {
      this._dueDateEpoch = this.model.dueDateEpoch || Date.parse(this.model.dueDate);
    }
    return this._dueDateEpoch;
  }

  get title() {
    if (this._title == null || this.isMNCN) {
      this._title = _.get(this.model, 'title', this.isMNCN ? this.mncnNumber : null);
    }
    return this._title;
  }

  get isMNCN() {
    return AppSettings.CODICILS_META.MNCN.ID === this.codicilType;
  }

  static PAYLOAD_FIELDS = {
    AI_LIST: [
      'assetItem.id',
      'assetItem.name',
      'assetItemH.id',
      'assetItemH.name',
      'categoryH.id',
      'categoryH.name',
      'codicilType',
      'description',
      'dueDate',
      'dueStatusH.id',
      'dueStatusH.name',
      'history',
      'id',
      'imposedDate',
      'statusH.id',
      'statusH.name',
      'title',
    ],
    AI_DETAIL: [
      'defectH.*',
      'deficiencyH.*',
      'dueDate',
      'itemsH.*',
      'job.id',
      'jobH.id',
      'jobH.jobNumber',
      'repairH.*',
    ],
    AN_LIST: [
      'id',
      'title',
      'description',
      'imposedDate',
      'codicilType',
      'categoryH.id',
      'categoryH.name',
      'dueStatusH.id',
      'dueStatusH.name',
      'statusH.id',
      'statusH.name',
      'assetItemH.id',
      'assetItemH.name',
      'assetItem.id',
      'assetItem.name',
    ],
    AN_DETAIL: [
      'dueDate',
      'job.id',
      'jobH.id',
      'jobH.jobNumber',
      'deficiencyH.*',
      'repairH.*',
      'defectH.*',
      'itemsH.*',
    ],
    COC_LIST: [
      'assetItem.id',
      'assetItem.name',
      'assetItemH.id',
      'assetItemH.name',
      'category.id',
      'categoryH.id',
      'categoryH.name',
      'codicilType',
      'description',
      'dueDate',
      'dueStatus.id',
      'dueStatusH.id',
      'dueStatusH.name',
      'history',
      'id',
      'imposedDate',
      'statusH.id',
      'statusH.name',
      'title',
    ],
    COC_DETAIL: [
      'categoryH.description',
      'defectH.*',
      'deficiencyH.*',
      'dueStatusH.icon',
      'itemsH.*',
      'job.id',
      'jobH.id',
      'jobH.jobNumber',
      'repairH.*',
    ],
    MNCN_LIST: [
      'id',
      'mncnNumber',
      'job.id',
      'asset.id',
      'status.id',
      'areaUnderReview',
      'ismClause',
      'dueDate',
      'codicilType',
      'statusH.id',
      'statusH.name',
    ],
    MNCN_DETAIL: [
      'closureDate',
      'closureReason',
      'description',
      'docCompanyName',
      'docCompanyImo',
      'docCompanyCountry',
      'correctiveAction',
      'objectiveEvidence',
    ],
    MNCN_ATTACHMENT: [
      'id',
      'token',
      'attachmentCategoryH.name',
      'attachmentCategoryH.id',
      'attachmentTypeH.fileExtension',
      'creationDate',
      'entityLink.type',
    ],
    SF_LIST: [
      'id',
      'title',
      'description',
      'imposedDate',
      'dueDate',
      'codicilType',
      'categoryH.id',
      'categoryH.name',
      'dueStatusH.id',
      'dueStatusH.name',
      'statusH.id',
      'statusH.name',
      'defect.id',
    ],
    SF_DETAIL: [
      'job.id',
      'jobH.id',
      'jobH.jobNumber',
      'narrative',
      'referenceCode',
      'flagStateConsulted',
      'deficiencyH.*',
      'repairH.*',
      'defectH.*',
      'itemsH.*',
    ],
  };
}
