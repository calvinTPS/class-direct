import * as _ from 'lodash';
import Model from 'app/common/models/model';

export default class CodicilType extends Model {
  get id() {
    if (!this._id && this.model.id) {
      this._id = _.get(this, 'model.id', 0);
    }
    return this._id;
  }

  get name() {
    if (!this._name && this.model.name) {
      this._name = _.get(this, 'model.name', '');
    }
    return this._name;
  }

  get selected() {
    if (_.isNull(this._selected) || _.isUndefined(this._selected)) {
      this._selected = false;
    }
    return this._selected;
  }

  set selected(bool) {
    this._selected = bool;
  }
}
