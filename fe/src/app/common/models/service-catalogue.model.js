import * as _ from 'lodash';
import Model from './model';

export default class ServiceCatalogueModel extends Model {
  constructor(model) {
    super(model);
  }

  /**
   * @param {string} type relationship type
   * @returns {array} list of relationships
   */
  getRelationshipByType(type) {
    return _.filter(this.relationships, {
      relationshipType: {
        id: _.get(this.relationshipType, type)
      }
    });
  }

  /**
   * @return {boolean} does this catalogue have counterparts?
   */
  get hasCounterparts() {
    return !_.isEmpty(
      this.getRelationshipByType('counterpart')
    );
  }

  /**
   * @return {array} the IDs of counterpart catalogues
   */
  get counterparts() {
    return _.map(this.getRelationshipByType('counterpart'), (r) =>
      r.fromServiceCatalogue.id === this.id ? r.toServiceCatalogue.id :
        r.fromServiceCatalogue.id);
  }

  /**
   * @return {String} Scheduling Regime name
   */
  get schedulingRegimeName() {
    return _.get(this, 'schedulingRegime.name') || '';
  }

  /**
   * @returns {number} scheduling regime id
   */
  get schedulingRegimeId() {
    return _.get(this, 'schedulingRegime.id');
  }

  /**
   * @param {number} regimeId the id to compare
   * @returns {boolean} result of comparing scheduling regime ids
   */
  isSchedulingRegime(regimeId) {
    return this.schedulingRegimeId === regimeId;
  }

  /**
   * @returns {boolean} is this an harmonised service?
   */
  get isHarmonised() {
    return !_.isNull(this.harmonisationType);
  }

  /**
   * @returns {number} service type id
   */
  get serviceTypeId() {
    return _.get(this, 'serviceType.id');
  }

  /**
   * @param {number} typeId the id to compare
   * @returns {boolean} result of comparing service types
   */
  isType(typeId) {
    return _.isEqual(this.serviceTypeId, typeId);
  }

  /**
   * @returns {array} list of service catalogues this catalogue depends on
   */
  get dependsOn() {
    return _.map(_.filter(this.relationships, {
      fromServiceCatalogue: { id: this.id },
      relationshipType: {
        id: this.relationshipType.dependent
      }
    }), 'toServiceCatalogue.id');
  }

  /**
   * @returns {array} list of service catalogues that depend on this one
   */
  get dependents() {
    return _.map(_.filter(this.relationships, {
      toServiceCatalogue: { id: this.id },
      relationshipType: {
        id: this.relationshipType.dependent
      }
    }), 'fromServiceCatalogue.id');
  }
}
