import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import Model from './model';

export default class Service extends Model {

  get catalogueName() {
    // - This comes from the backend and has the service occurrence name\
    // - Screwshaft - 1
    if (this._catalogueName == null) {
      this._catalogueName = _.get(this, 'model.serviceCatalogueName');
    }
    return this._catalogueName;
  }

  get creditStatus() {
    if (this._creditStatus == null) {
      this._creditStatus = _.get(this, 'model.serviceCreditStatusH.name');
    }
    return this._creditStatus;
  }

  get dueStatus() {
    if (this._dueStatus == null) {
      this._dueStatus = _.get(this.dueStatusH, 'name');
    }
    return this._dueStatus;
  }

  get id() {
    if (this._id == null) {
      this._id = _.get(this, 'model.id');
    }
    return this._id;
  }

  get isPostponed() {
    return !!this.model.postponementDate;
  }

  // same as serviceCatalogueH.name in asset service
  get name() {
    if (this._name == null) {
      this._name = _.get(this, 'model.serviceCatalogueH.name', null);
    }

    // - Jobs will return services that uses this model
    // - If still null try the model.name
    if (this._name == null) {
      this._name = _.get(this, 'model.name');
    }

    return this._name;
  }

  get occurrenceNumber() {
    if (this._occurrenceNumber == null) {
      this._occurrenceNumber = _.get(this, 'model.occurrenceNumber');
    }
    return this._occurrenceNumber;
  }

  get postponementDateEpoch() {
    if (this._postponementDateEpoch == null && this.model.postponementDate) {
      this._postponementDateEpoch = Date.parse(this.model.postponementDate);
    }
    return this._postponementDateEpoch;
  }

  get productCatalogueName() {
    if (this._productCatalogueName == null) {
      this._productCatalogueName = _.get(this, 'model.serviceCatalogueH.productCatalogue.name');
    }
    return this._productCatalogueName;
  }
  get productGroupName() {
    if (this._productGroupName == null) {
      this._productGroupName = _.get(this, 'model.serviceCatalogueH.productGroupH.name');
    }
    return this._productGroupName;
  }

  // - Used to split the services into 3 buckets
  // - based on the product family, Classfication, Statutory, MMS
  get productFamilyName() {
    if (this._productFamilyName == null) {
      switch (_.get(this, 'model.serviceCatalogueH.productCatalogue.productType.id')) {
        case AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.ID:
          this._productFamilyName = AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.NAME;
          break;
        case AppSettings.PRODUCT_FAMILY_TYPES.STATUTORY.ID:
          this._productFamilyName = AppSettings.PRODUCT_FAMILY_TYPES.STATUTORY.NAME;
          break;
        case AppSettings.PRODUCT_FAMILY_TYPES.MMS.ID:
          this._productFamilyName = AppSettings.PRODUCT_FAMILY_TYPES.MMS.NAME;
          break;
        default:
          break;
      }
    }
    return this._productFamilyName;
  }

  get statusName() {
    if (this._statusName == null) {
      this._statusName = _.get(this, 'model.serviceStatusH.name');
    }
    return this._statusName;
  }

  get serviceCode() {
    if (this._serviceCode == null) {
      this._serviceCode = _.get(this, 'model.serviceCatalogueH.code');
    }
    return this._serviceCode;
  }

  // same as serviceStatusH.name in asset service
  get status() {
    if (this._status == null) {
      const status = _.get(this, 'model.status');
      this._status = _.indexOf(AppSettings.UNKNOWN_CREDIT_STATUSES, status) === -1 ? status : 'Unknown';
    }
    return this._status;
  }

  get title() {
    // - Want to show the following string everywhere LRCD-3756
    // - <Service code><Service Occurrence number> - <Service Name>
    return this.occurrenceNumber ?
      `${this.serviceCode}${this.occurrenceNumber} - ${this.name}` :
      `${this.serviceCode} - ${this.name}`;
  }

 /**
 * @return {string} Returns "TASKLIST" or "CHECKLIST" or undefined
 */
  get workItemType() {
    if (this._workItemType == null) {
      const workItemTypeId = _.get(this, 'model.serviceCatalogueH.workItemType.id');
      this._workItemType = workItemTypeId ?
                            _.get(AppSettings.WORK_ITEM_TYPES, workItemTypeId.toString()) :
                            workItemTypeId;
    }
    return this._workItemType;
  }

}
