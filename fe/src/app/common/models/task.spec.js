/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import TaskModel from './task';

describe('TaskModel', () => {
  let $rootScope, makeTaskModel;

  const rawTasks = [{
    id: 3,
    name: 'A very long task name 2',
    number: 3456712,
    dueDate: '2016-11-12',
    assignedDate: '2016-11-11',
    resolutionStatusH: { name: 'Complete' },
    postponementDate: '2016-01-01',
    postponementTypeH: { name: 'xxx' },
  }, {
    id: 4,
    name: 'A very long task name 3',
    number: null,
    dueDate: '2016-11-12',
    assignedDate: '2016-11-11',
    resolutionStatusH: null,
    postponementDate: null,
    postponementTypeH: { name: null },
    notes: 'Best note ever',
  }];

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeTaskModel = data => Reflect.construct(TaskModel, [data]);
  }));

  describe('Model', () => {
    it('should return correct data model for Job', () => {
      const taskModel = makeTaskModel(rawTasks[0]);

      expect(taskModel.creditStatus).toEqual('Complete');
      expect(taskModel.postponementDate).toEqual('2016-01-01');
      expect(taskModel.postponementStatus).toEqual('xxx');
    });

    it('should return default status string for creditStatus when resolutionStatusH is null', () => {
      const taskModel = makeTaskModel(rawTasks[1]);
      expect(taskModel.creditStatus).toEqual(AppSettings.TASK.CREDIT_STATUSES.NOT_CREDITED);
    });
  });
});
