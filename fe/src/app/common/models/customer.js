import * as _ from 'lodash';
import Model from './model';

export default class Customer extends Model {
  get functionNameList() {
    return _.map(this.functions, (func) =>
        _.get(func, 'customerFunction.name'));
  }
}
