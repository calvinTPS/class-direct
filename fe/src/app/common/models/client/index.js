import * as _ from 'lodash';
import Model from 'app/common/models/model';

export default class Client extends Model {
  get code() {
    if (this._code == null) {
      this._code = _.get(this, 'model.imoNumber', '');
    }
    return this._code;
  }

  get company() {
    if (this._company == null) {
      this._company = _.get(this, 'model.name', '');
    }
    return this._company;
  }

  get relationship() {
    if (this._relationship == null) {
      this._relationship = _.get(this, 'model.clientType');
    }
    return this._relationship;
  }
}
