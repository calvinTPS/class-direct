/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-param-reassign */
import * as _ from 'lodash';
import ClientMocks from './client.mocks.json';
import ClientModel from './';

describe('Client Model', () => {
  let $rootScope;
  let makeModel;

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeModel = (data) => {
      _.forEach(data, (rawObject, index) => {
        data[index] = Reflect.construct(ClientModel, [rawObject]);
      });
      return data;
    };
  }));

  describe('get data', () => {
    it('should return the right data', () => {
      const model = makeModel(_.clone(ClientMocks));
      expect(model[0].code).toEqual(6000001);
      expect(model[0].company).toEqual('GBOCompanyName1');
      expect(model[0].relationship).toEqual('Client');

      expect(model[1].code).toEqual(3000109);
      expect(model[1].company).toEqual('TechnicalMagCompanyName1');
      expect(model[1].relationship).toEqual('Ship');
    });
  });
});
