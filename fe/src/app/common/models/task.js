import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import Model from './model';

export default class Task extends Model {
  /**
   * The Credit Status for a task for Class Direct is actually
   * obtained from the Resolution Status, and defaults to a default status if empty.
   */
  get creditStatus() {
    if (this._creditStatus == null) {
      this._creditStatus = _.get(this, 'model.resolutionStatusH.name', AppSettings.TASK.CREDIT_STATUSES.NOT_CREDITED);
    }
    return this._creditStatus;
  }

  get postponementStatus() {
    return _.get(this, 'model.postponementTypeH.name');
  }

  static PAYLOAD_FIELDS = {
    TASK_LIST: [
      'servicesH.id',
      'servicesH.itemsH.*',
      'servicesH.serviceCatalogueH.code',
      'servicesH.serviceCatalogueH.name',
      'servicesH.serviceCatalogueH.workItemType',
      'servicesH.serviceCatalogueH.productCatalogue.productType.id',
      'servicesH.serviceCatalogueH.productGroupH.*',
      'servicesH.serviceCatalogueH.displayOrder',
    ],
  }
}
