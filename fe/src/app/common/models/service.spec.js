/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
 one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import ServiceModel from './service';

describe('Service', () => {
  let $rootScope, makeServiceModel;

  const mockServices = [{
    id: 1,
    dueStatusH: {
      name: 'Not Due',
    },
    serviceCatalogueH: {
      name: 'Service 1',
      productCatalogue: { name: 'Product 1' },
      workItemType: { id: 1 },
    },
    serviceCatalogueName: 'Service 1',
    serviceCreditStatusH: { name: 'Completed' },
    serviceStatusH: { name: 'service status 1' },
    name: 'Service 1',
    status: 'Completed',
  }, {
    id: 2,
    dueStatusH: {
      name: 'Due',
    },
    serviceCatalogueH: {
      name: 'Service 2',
      productCatalogue: { name: 'Product 2' },
      workItemType: null,
      multipleIndicator: true,
    },
    occurrenceNumber: 5,
    serviceCreditStatusH: { name: 'UNKNOWN_SERVICE_CREDIT_STATUS' },
    serviceStatusH: { name: 'service status 2' },
    name: 'Service 1',
    status: 'UNKNOWN_SERVICE_CREDIT_STATUS',
  }];

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeServiceModel = data => new ServiceModel(data);
  }));

  describe('ServiceModel', () => {
    it('should return correct data model for Service', () => {
      const serviceModel = makeServiceModel(mockServices[0]);

      expect(serviceModel.id).toEqual(1);
      expect(serviceModel.catalogueName).toEqual('Service 1');
      expect(serviceModel.creditStatus).toEqual('Completed');
      expect(serviceModel.productCatalogueName).toEqual('Product 1');
      expect(serviceModel.statusName).toEqual('service status 1');
      expect(serviceModel.status).toEqual('Completed');
      expect(serviceModel.name).toEqual('Service 1');
    });

    it('should return the correct due status', () => {
      const serviceModel = makeServiceModel(mockServices[0]);
      expect(serviceModel.dueStatus).toEqual('Not Due');
    });

    it('should return correct credit status when it is unknown', () => {
      const serviceModel = makeServiceModel(mockServices[1]);

      expect(serviceModel.id).toEqual(2);
      expect(serviceModel.status).toEqual('Unknown');
    });

    it('should return correct workItemType when TASKLIST', () => {
      const serviceModel = makeServiceModel(mockServices[0]);
      expect(serviceModel.workItemType).toEqual('TASKLIST');
    });

    it('should return correct workItemType when UNDEFINED', () => {
      const serviceModel = makeServiceModel(mockServices[1]);
      expect(serviceModel.workItemType).toEqual(undefined);
    });
  });
});
