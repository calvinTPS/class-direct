import './footer.scss';
import controller from './footer.controller';
import template from './footer.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
