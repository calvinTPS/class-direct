import angular from 'angular';
import FooterComponent from './footer.component';
import uiRouter from 'angular-ui-router';

export default angular.module('footer', [
  uiRouter,
])
.component('footer', FooterComponent)
.name;
