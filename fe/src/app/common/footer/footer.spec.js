/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import FooterComponent from './footer.component';
import FooterController from './footer.controller';
import FooterModule from './';
import FooterTemplate from './footer.pug';

describe('Footer', () => {
  let $rootScope,
    $compile,
    makeController;

  const mockTranslateFilter = value => value;
  beforeEach(window.module(
    FooterModule,
    {
      translateFilter: mockTranslateFilter,
      AppSettings,
    },
  ));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('footer', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('getCurrentYear() should return current year correctly', () => {
      const controller = makeController();
      expect(controller.getCurrentYear).toEqual(new Date().getFullYear().toString());
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = FooterComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(FooterTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(FooterController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<footer foo="bar"><footer/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Footer');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
