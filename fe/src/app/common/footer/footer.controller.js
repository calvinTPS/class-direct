import Base from 'app/base';
import fecha from 'fecha';

export default class FooterController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
  }

  get getCurrentYear() {
    return fecha.format(new Date(), 'YYYY');
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
