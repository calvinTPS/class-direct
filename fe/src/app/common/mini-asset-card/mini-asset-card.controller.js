/* eslint-disable global-require */

import Base from 'app/base';

export default class MiniAssetCardController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);
  }

  $onInit() {
    // Default Theme if theme is not provided
    this.theme = this.theme ? this.theme : 'blue';
  }

  toggleSelectedAsset() {
    if (this.editable) {
      this.selected = !this.selected;
    }
  }
}
