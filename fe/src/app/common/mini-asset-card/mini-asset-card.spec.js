/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, no-underscore-dangle, no-unused-vars */
import MiniAssetCardComponent from './mini-asset-card.component';
import MiniAssetCardController from './mini-asset-card.controller';
import MiniAssetCardModule from './';
import MiniAssetCardTabularTemplate from './mini-asset-card-tabular.pug';
import MiniAssetCardTemplate from './mini-asset-card.pug';

describe('MiniAssetCard', () => {
  let $rootScope;
  let $compile;
  let makeController;

  beforeEach(window.module(MiniAssetCardModule));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('miniAssetCard', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('toggleSelectedAsset() should toggle when it\'s editable', () => {
      const controller = makeController({
        editable: false,
        model: { id: 1 },
        selected: false,
      });
      controller.toggleSelectedAsset();
      expect(controller.selected).toEqual(false);
      // One more time to toggle to true, but souldn't toggle due to editable=false
      controller.toggleSelectedAsset();
      expect(controller.selected).toEqual(false);

      // Now, change editable to true
      controller.editable = true;
      controller.toggleSelectedAsset();
      expect(controller.selected).toEqual(true);

      // One more time to toggle to true
      controller.toggleSelectedAsset();
      expect(controller.selected).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = MiniAssetCardComponent;

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(MiniAssetCardController);
    });
  });

  describe('Rendering', () => {
    let controller;
    let element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.model = {};
      scope.$apply();
      element = angular.element('<mini-asset-card model="model"><mini-asset-card/>');
      element = $compile(element)(scope);

      controller = element.controller('MiniAssetCard');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
