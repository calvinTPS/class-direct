import angular from 'angular';
import miniAssetCardComponent from './mini-asset-card.component';

export default angular.module('miniAssetCard', [])
.component('miniAssetCard', miniAssetCardComponent)
.name;
