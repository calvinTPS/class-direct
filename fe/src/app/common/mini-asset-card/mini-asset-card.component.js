import './mini-asset-card.scss';
import './mini-asset-card-tabular.scss';
import controller from './mini-asset-card.controller';
import tabularTemplate from './mini-asset-card-tabular.pug';
import template from './mini-asset-card.pug';

export default {
  bindings: {
    cardDetail: '@?',
    cardTitle: '@?',
    editable: '<?',
    emblemType: '@?',
    mode: '@?',
    selected: '=',
    theme: '@',
    hasLargeTitle: '<?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  /* @ngInject */
  template: ($element, $attrs) => $attrs.mode === 'tabular' ?
    tabularTemplate : template,
};
