import angular from 'angular';
import RidersComponent from './riders.component';
import uiRouter from 'angular-ui-router';

export default angular.module('riders', [
  uiRouter,
])
.component('riders', RidersComponent)
.name;
