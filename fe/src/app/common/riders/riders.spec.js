/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import RidersComponent from './riders.component';
import RidersController from './riders.controller';
import RidersModule from './';

describe('Riders', () => {
  let $rootScope,
    $compile,
    $element,
    $mdMedia,
    makeController;

  beforeEach(window.module(
    RidersModule,
    angularMaterial,
  ));
  beforeEach(() => {
    const mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('ViewService', { getRegisteredTemplate: (x, y) => function a() {} });
      $provide.value('$element', angular.element('<div></div>'));
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('riders', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = RidersComponent;
    it('invokes the right controller', () => {
      expect(component.controller).toEqual(RidersController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<riders foo="bar"><riders/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Riders');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
