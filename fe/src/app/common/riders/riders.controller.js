import Base from 'app/base';
// import defaultAssetTemplate from './rider-default-item-template.pug';

export default class RidersController extends Base {
  /* @ngInject */
  constructor(
    $compile,
    $element,
    $log,
    $mdMedia,
    $scope,
    AppSettings,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    if (!this.templateLocation) {
      this._$log.warn('Using default template as no template provided');
      this.templateLocation = this._AppSettings.RIDER_TEMPLATES.DEFAULT;
    }

    if (angular.isDefined(this.templateLocation)) {
      // compile the provided template against the current scope
      const tpl = this._ViewService.getRegisteredTemplate(this.templateLocation);
      tpl(this._$scope, (clonedElement) => {
        // add the template content
        this._$element.prepend(clonedElement);
      });
    }
  }
}
