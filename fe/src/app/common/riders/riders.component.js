import './riders.scss';
import controller from './riders.controller';
import template from './rider-template.pug';

export default {
  bindings: {
    asset: '<',
    customData: '<?',
    item: '<',
    templateLocation: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
