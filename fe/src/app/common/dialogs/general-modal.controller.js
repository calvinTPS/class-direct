import Base from 'app/base';

/**
 * A general modal/dialog controller that has mdDialog injected.
 *
 * Users of this controller should chain the promise returned by
 * $mdDialog.show() in order to determine the affirmative or the
 * rejection of the modal action.
 */
export default class GeneralModalController extends Base {
  /* @ngInject */
  constructor($mdDialog) {
    super(arguments);
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
