/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import GeneralModalController from './general-modal.controller';

describe('General Modal', () => {
  let makeController;
  beforeEach(() => {
    makeController = () => new GeneralModalController();
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
    });
  });
});
