import Base from 'app/base';

export default class CdActionsController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    AppSettings,
    DashboardService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this._$rootScope.isActionBarEnabled = true;
    this._$rootScope.$broadcast(this._AppSettings.EVENTS.CD_ACTIONS_TOGGLED, true);
  }

  $onDestroy() {
    this._$rootScope.isActionBarEnabled = false;
    this._$rootScope.$broadcast(this._AppSettings.EVENTS.CD_ACTIONS_TOGGLED, false);
    this._DashboardService.resetFavourites();
  }
}
