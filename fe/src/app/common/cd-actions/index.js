import angular from 'angular';
import CdActionsComponent from './cd-actions.component';
import uiRouter from 'angular-ui-router';

export default angular.module('cdActions', [
  uiRouter,
])
.component('cdActions', CdActionsComponent)
.name;
