import './cd-actions.scss';
import controller from './cd-actions.controller';
import template from './cd-actions.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
