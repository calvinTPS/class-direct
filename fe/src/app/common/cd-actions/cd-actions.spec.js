/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import CdActionsComponent from './cd-actions.component';
import CdActionsController from './cd-actions.controller';
import CdActionsModule from './';
import CdActionsTemplate from './cd-actions.pug';

describe('CdActions', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(CdActionsModule));
  beforeEach(window.module(($provide) => {
    const mockDashBoardService = {
      resetFavourites: () => true,
    };
    $provide.value('DashboardService', mockDashBoardService);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    const mockDashBoardService = {};

    makeController =
      (bindings = {}) => {
        const controller = $componentController('cdActions', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdActionsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CdActionsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdActionsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<cd-actions foo="bar"><cd-actions/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('CdActions');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
