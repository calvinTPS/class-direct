/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import * as _ from 'lodash';
import angularMaterial from 'angular-material';
import DueStatusComponent from './due-status.component';
import DueStatusController from './due-status.controller';
import DueStatusModule from './due-status.js';
import DueStatusTemplate from './due-status.pug';
import mockAppSettings from 'app/config/project-variables.js';
import translationStrings from 'resources/locales/en-GB.json';
import utils from 'app/common/utils/utils.factory';


describe('DueStatus', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    item,
    makeController;

  beforeEach(window.module(
    angularMaterial,
    DueStatusModule,
  ));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockDateFilter = (value) => {
      if (value === 'translate') {
        return val => translationStrings[val];
      }
      return val => val;
    };

    window.module(($provide) => {
      $provide.value('AppSettings', mockAppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('utils', utils({}, {}, {}, mockDateFilter));
    });

    item = {
      codicilType: 'COC',
      categoryH: 'statutory',
      isPostponed: false,
      isOverallStatus: false,
      postponementDate: '2020-10-13',
      serviceCatalogueH: null,
    };
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('dueStatus', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    beforeEach(() => {
      controller = makeController({
        item,
        statusName: 'Overdue',
      });
    });
    it('initializes just fine', () => {
      controller.statusName = 'overdue';
      expect(controller.item).not.toBeNull();
    });

    it('returns the correct copytext for COC tooltip hover text', () => {
      controller.statusName = 'overdue';
      controller.dueStatus = _.startCase(controller.statusName);
      let copytext = controller._hoverText(controller.item.codicilType);

      expect(controller.item.codicilType).toEqual('COC');

      expect(copytext).toEqual('cd-overdue');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'imminent';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText(controller.item.codicilType);
      expect(copytext).toEqual('cd-due-within-30-days');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'Due soon';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText(controller.item.codicilType);
      expect(copytext).toEqual('cd-due-within-three-months');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'not due';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText(controller.item.codicilType);
      expect(copytext).toEqual('cd-not-due-within-three-months');
      expect(controller.toolTipHoverText).toEqual(copytext);
    });

    it('returns the correct copytext for AI tooltip hover text', () => {
      controller.statusName = 'overdue';
      controller.dueStatus = _.startCase(controller.statusName);
      controller.item.codicilType = 'AI';
      let copytext = controller._hoverText(controller.item.codicilType);

      expect(controller.item.codicilType).toEqual('AI');

      expect(copytext).toEqual('cd-overdue');
      controller.dueStatus = _.startCase(controller.statusName);
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'imminent';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText(controller.item.codicilType);
      expect(copytext).toEqual('cd-due-within-1-month');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'Due soon';
      controller.dueStatus = _.startCase(controller.statusName);
      controller.item.category = 'Statutory';
      copytext = controller._hoverText(controller.item.codicilType);
      expect(copytext).toEqual('cd-due-within-1-year-statutory');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.item.category = 'non statutory';
      copytext = controller._hoverText(controller.item.codicilType);
      expect(copytext).toEqual('cd-due-within-3-months-non-statutory');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'not due';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText(controller.item.codicilType);
      expect(copytext).toEqual('cd-not-overdue');
      expect(controller.toolTipHoverText).toEqual(copytext);
    });

    it('returns the correct copytext for service tooltip hover text', () => {
      controller.statusName = 'overdue';
      controller.dueStatus = _.startCase(controller.statusName);
      controller.item.codicilType = null;
      controller.item.serviceCatalogueH = { id: 1 };
      let copytext = controller._hoverText('service');

      expect(controller.item.serviceCatalogueH).not.toBeNull();

      expect(copytext).toEqual('cd-overdue');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'imminent';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText('service');
      expect(copytext).toEqual('cd-due-within-1-month');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'Due soon';
      controller.dueStatus = _.startCase(controller.statusName);

      copytext = controller._hoverText('service');
      expect(copytext).toEqual('cd-due-within-3-months');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'not due';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText('service');
      expect(copytext).toEqual('cd-not-overdue');
      expect(controller.toolTipHoverText).toEqual(copytext);
    });

    it('should returns the correct tooltip text when the service has a postponement date', () => {
      controller.dueStatus = _.startCase(controller.statusName);
      controller.item.codicilType = null;
      controller.item.serviceCatalogueH = { id: 1 };
      controller.item.isPostponed = true;
      controller.item.postponementDateEpoch = Date.parse(controller.item.postponementDate);
      let copytext = controller._hoverText('service');
      let subHoverText = translationStrings['cd-agreed-postponement-date'];
      expect(copytext).toEqual(`${subHoverText} ${controller.item.postponementDate}`);
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText('service');
      subHoverText = translationStrings['cd-agreed-postponement-date'];
      expect(copytext).toEqual(`${subHoverText} ${controller.item.postponementDate}`);
      expect(controller.toolTipHoverText).toEqual(copytext);
    });

    it('should returns the correct label text and icon name', () => {
      controller = makeController({
        item,
        statusName: 'Due Soon',
      });

      controller.dueStatus = _.startCase(controller.statusName);
      expect(controller.label).toEqual(_.capitalize(controller.dueStatus.split('-').join(' ')));
      expect(controller.iconName)
          .toEqual(mockAppSettings.DUE_STATUS_ICON_MAP[(controller.isOverallStatus ? 'overall-' : '') + controller.cssClass]);
      controller = makeController({
        item: { isPostponed: true },
        statusName: 'Imminant',
      });
      controller.dueStatus = _.startCase(controller.statusName);
      expect(controller.label).toEqual(`Postponed - <br/> ${_.capitalize(controller.dueStatus.split('-').join(' '))}`);
      expect(controller.iconName).toEqual(mockAppSettings.DUE_STATUS_ICON_MAP.postponed);
    });

    it('should returns the correct label text and icon name when the item is postponed', () => {
      controller = makeController({
        item: { isPostponed: true },
        statusName: 'Imminant',
      });
      controller.dueStatus = _.startCase(controller.statusName);
      expect(controller.label).toEqual(`Postponed - <br/> ${_.capitalize(controller.dueStatus.split('-').join(' '))}`);
      expect(controller.iconName).toEqual(mockAppSettings.DUE_STATUS_ICON_MAP.postponed);
    });

    it('should returns the correct label text and icon name when the item  is postponed and has overall status', () => {
      controller = makeController({
        item,
        statusName: 'Overdue',
        isOverallStatus: true,
        isPostponed: true,
      });
      controller.dueStatus = _.startCase(controller.statusName);
      expect(controller.label).toEqual(`Postponed - <br/> ${_.capitalize(controller.dueStatus.split('-').join(' '))}`);
      expect(controller.iconName).toEqual(mockAppSettings.DUE_STATUS_ICON_MAP[`overall-${_.kebabCase(controller.statusName)}`]);
    });

    it('returns the correct copytext for task tooltip hover text', () => {
      controller.statusName = 'overdue';
      controller.dueStatus = _.startCase(controller.statusName);
      controller.item.codicilType = null;
      controller.item.serviceCatalogueH = null;
      let copytext = controller._hoverText('task');

      expect(copytext).toEqual('cd-overdue');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'imminent';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText('task');
      expect(copytext).toEqual('cd-due-within-one-month');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'Due soon';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText('task');
      expect(copytext).toEqual('cd-due-within-three-months');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'not due';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText('task');
      expect(copytext).toEqual('cd-not-overdue');
      expect(controller.toolTipHoverText).toEqual(copytext);
    });

    it('returns the default copytext for tooltip hover text when item is not existed', () => {
      controller.statusName = 'overdueH';
      controller.dueStatus = _.startCase(controller.statusName);
      let copytext = controller._hoverText();

      controller.item.codicilType = null;
      controller.item.serviceCatalogueH = null;
      expect(copytext).toEqual('');
      expect(controller.toolTipHoverText).toEqual(copytext);

      // When item not exist
      controller.item = null;
      controller.statusName = 'overdue';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText();
      expect(copytext).toEqual('cd-asset-overall-status-overdue');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'imminent';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText();
      expect(copytext).toEqual('cd-asset-overall-status-imminent');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'Due soon';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText();
      expect(copytext).toEqual('cd-asset-overall-status-due-soon');
      expect(controller.toolTipHoverText).toEqual(copytext);

      controller.statusName = 'not due';
      controller.dueStatus = _.startCase(controller.statusName);
      copytext = controller._hoverText();
      expect(copytext).toEqual('cd-asset-overall-status-not-due');
      expect(controller.toolTipHoverText).toEqual(copytext);
    });

    it('should return correct label', () => {
      controller._label = 'Not-due';
      expect(controller.label).toEqual('Not-due');
    });

    it('should return correct icon', () => {
      controller._iconName = 'Not-due';
      expect(controller._iconName).toEqual('Not-due');
    });

    it('should return correct cssClass', () => {
      controller._cssClass = 'Not-due';
      expect(controller._cssClass).toEqual('Not-due');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = DueStatusComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(DueStatusTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(DueStatusController);
    });
  });
});
