/* eslint-disable max-len */
import './due-status.scss';
import controller from './due-status.controller';
import template from './due-status.pug';

export default {
  bindings: {
    colorize: '@?',
    disableTooltip: '<?',
    hideLabel: '<?',
    iconSize: '@?',
    isPostponed: '<?',
    isOverallStatus: '<?',
    item: '<?',
    label: '@?', // If defined, `label` will be used as text instead of status. Useful for custom text such as `overdueCount()` in `cd-accordion`
    statusName: '@',
    theme: '@?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
