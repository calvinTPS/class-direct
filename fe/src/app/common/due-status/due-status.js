import angular from 'angular';
import dueStatusComponent from './due-status.component';
import uiRouter from 'angular-ui-router';

export default angular.module('dueStatus', [
  uiRouter,
])
.component('dueStatus', dueStatusComponent)
.name;
