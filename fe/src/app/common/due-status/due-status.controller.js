import * as _ from 'lodash';
import Base from 'app/base';

export default class DueStatusController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    utils,
    $mdMedia,
  ) {
    super(arguments);
  }

  $onInit() {
    this.showTooltip = false;
    this.dueStatus = this.statusName;
    this._cssClass = _.kebabCase(this.statusName);
    let subLabel;
    if (!this._label && this.statusName) {
      subLabel = _.capitalize(this.dueStatus.split('-').join(' '));
    } else {
      subLabel = this._label;
    }
    // label can be too long for riders
    const postponedLabel = `Postponed - <br/> ${subLabel}`;
    this._iconName = this._AppSettings.DUE_STATUS_ICON_MAP[(this.isOverallStatus ? 'overall-' : '') + this._cssClass];

    // for postponed asset
    if (this.isOverallStatus && this.isPostponed) {
      this._label = postponedLabel;
      // for postponed service
    } else if (this.item && this.item.isPostponed) {
      this._iconName = this._AppSettings.DUE_STATUS_ICON_MAP.postponed;
      this._label = postponedLabel;
    } else if (!this._label) {
      // if `label` is not defined, status name will be returned instead
      this._label = subLabel;
    }
  }

  get label() {
    return this._label;
  }

  set label(value) {
    this._label = value;
  }

  get iconName() {
    return this._iconName;
  }

  set iconName(value) {
    this._iconName = value;
  }

  get cssClass() {
    return this._cssClass;
  }

  set cssClass(value) {
    this._cssClass = value;
  }


  $onChanges(changesObj) {
    // this will be called on date filter change on service schedule Gantt chart
    //  this.createlabelAndIconName();
    this._cssClass = _.kebabCase(this.statusName);
    this.dueStatus = this.statusName;
    this._label = _.capitalize(this.dueStatus.split('-').join(' '));
    // const postponedLabel = `Postponed - <br/> ${subLabel}`;
    this._iconName =
    this._AppSettings.DUE_STATUS_ICON_MAP[(this.isOverallStatus ? 'overall-' : '') + this._cssClass];
    const postponedLabel = `Postponed - <br/> ${this._label}`;
    // for postponed asset
    if (this.isOverallStatus && this.isPostponed) {
      this._label = postponedLabel;
      // for postponed service
    } else if (this.item && this.item.isPostponed) {
      this._iconName = this._AppSettings.DUE_STATUS_ICON_MAP.postponed;
      this._label = postponedLabel;
    }
  }

  get toolTipHoverText() {
    let text;
    if (this.item) {
      if (this.item.codicilType) {
        text = this._hoverText(this.item.codicilType);
      } else if (this.item.serviceCatalogueH) {
        text = this._hoverText('service');
      } else {
        text = this._hoverText('task');
      }
    } else {
      text = this._hoverText();
    }
    return text;
  }

  _hoverText = type => this._utils.getDueStatusDefinition(type, this.dueStatus, this.item);
}
