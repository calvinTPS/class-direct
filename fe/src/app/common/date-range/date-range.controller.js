import * as _ from 'lodash';
import Base from 'app/base';

export default class DateRangeController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdMedia,
    $rootScope,
    $timeout,
    AppSettings,
  ) {
    super(arguments);
  }

  $onInit() {
    this.maxDateValid = this.max;
    this.minDateValid = this.min;
    this._currentDate = new Date(new Date().toDateString());  // strip time
    this.disallowFutureDates = _.get(this.filterOptions, 'disallowFutureDates', false);

    if (this.disallowFutureDates) {
      // on init, prevent future dates
      this._maxDateValid = this._currentDate;
      this.maxLimit = this._currentDate;
    }
  }

  get maxDateValid() {
    return this._maxDateValid;
  }

  // custom setter for maxDateValid: must be a month or less than second date picker
  set maxDateValid(max) {
    this._maxDateValid = _.clone(this.max);
    if (this.disallowFutureDates && _.isDate(this.max) && this.max > this._currentDate) {
      // force date to current date when user enters a future date
      this.max = this._currentDate;
    }

    if (this.rangeValidator && _.isDate(this.max)) {
      this._maxDateValid.setMonth(this.max.getMonth() - 1);
    }
  }

  get minDateValid() {
    return this._minDateValid;
  }

  // custom setter for minDateValid: must be a month or more than first date picker
  set minDateValid(min) {
    this._minDateValid = _.clone(this.min);
    if (this.disallowFutureDates && _.isDate(this.min) && this.min > this._currentDate) {
      // force date to current date when user enters a future date
      this.min = this._currentDate;
    }
    if (this.rangeValidator && _.isDate(this.min)) {
      this._minDateValid.setMonth(this.min.getMonth() + 1);
    }
  }

  setMinMaxDate(form) {
    this.maxDateValid = this.max;
    this.minDateValid = this.min;

    if (this.disallowFutureDates) {
      this._maxDateValid = _.isDate(this.max) ? this.max : this._currentDate;

      if (_.isDate(this.min) && this.min > this._currentDate) {
        this.minDateValid = this._currentDate;
      }

      if (_.isDate(this.max) && this.max > this._currentDate) {
        this.maxDateValid = this._currentDate;
      }
    }

    // need a short $timeout here because it takes time for form.$valid to update
    this._$timeout(() => {
      // Pass the form status back to parent component so that we can enable/disable submit button
      this.isValid = form.$valid && (!this.max || !this.min || (this.max >= this.min));
      if (this.isValid) {
        // broadcast event for manipulate month pips on cd-card-gantt
        // not using broadcast will breaking the current implementation of
        // regenerating the month pips
        this._$rootScope.$broadcast(this._AppSettings.EVENTS.UPDATE_DATE_PICKER);
        this.onChange({ minDate: this.min, maxDate: this.max }, this.filterOptions);
      }
    });
  }

  resetModel(model) {
    if (!this.rangeValidator && model === 'min' && _.isNull(this[model])) {
      this._minDateValid = undefined;
    } else if (!this.rangeValidator && model === 'max' && _.isNull(this[model])) {
      this._maxDateValid = undefined;
    }
  }

  get getTooltipPosition() {
    return angular.element(this._$document[0].querySelector('.sticky')).length ? 'bottom' : 'top';
  }
}
