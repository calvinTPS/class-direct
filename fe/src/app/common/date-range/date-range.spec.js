/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-underscore-dangle */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import DateRangeComponent from './date-range.component';
import DateRangeController from './date-range.controller';
import DateRangeModule from './';
import DateRangeTemplate from './date-range.pug';
import translationStrings from 'resources/locales/en-GB.json';

describe('DateRange', () => {
  let $rootScope,
    $compile,
    $document,
    $interpolate,
    $timeout,
    makeController,
    scope;

  beforeEach(window.module(DateRangeModule,
    angularMaterial,
  ));
  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));
  beforeEach(() => {
    const mockTranslateFilter = value => $interpolate(translationStrings[value])(scope);
    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((
    _$rootScope_,
    _$compile_,
    _$document_,
    _$interpolate_,
    _$timeout_,
    $componentController
  ) => {
    $interpolate = _$interpolate_;
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $timeout = _$timeout_;
    $document = _$document_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('dateRange', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({});
    });

    it('should set the maxLimit and _maxDateValid to current date, when disallowFutureDates is true', () => {
      let expectedDate = new Date();
      expectedDate = expectedDate.getDate();
      controller = makeController({ filterOptions: { disallowFutureDates: true } });
      expect(controller._maxDateValid.getDate()).toEqual(expectedDate);
      expect(controller.maxLimit.getDate()).toEqual(expectedDate);
    });

    it('getTooltipPosition should return the correct position', () => {
      controller = makeController();
      spyOn(angular, 'element').and.returnValue(['sticky-child-element']);
      expect(controller.getTooltipPosition).toEqual('bottom');
      expect(angular.element).toHaveBeenCalledWith($document[0].querySelector('.sticky'));
    });
  });

  describe('Controller maxDateValid setter', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({});
    });

    it('should set max date to current date when disallowFutureDates=true and user enters a future date', () => {
      const currentDate = new Date();
      const futureDate = new Date();
      futureDate.setDate(futureDate.getDate() + 1);
      controller = makeController({ filterOptions: { disallowFutureDates: true } });
      controller._currentDate = currentDate;
      controller.max = futureDate;
      controller.maxDateValid = futureDate;
      expect(controller.max).toEqual(currentDate);
    });

    it('check if set maxDateValid function sets date 1 month before max', () => {
      const max = new Date();
      const mockMaxDateValid = new Date();
      mockMaxDateValid.setMonth(max.getMonth() - 1);

      controller = makeController({
        onChange: () => {},
        min: new Date(),
        max,
        _maxDateValid: '',
        rangeValidator: true,
      });

      expect(controller.maxDateValid).toEqual(mockMaxDateValid);

      controller.rangeValidator = false;
      controller.maxDateValid = max;
      expect(controller.maxDateValid).not.toEqual(mockMaxDateValid);
      expect(controller.maxDateValid).toEqual(controller.max);
    });
  });


  describe('Controller minDateValid setter', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({});
    });

    it('should set min date to current date when disallowFutureDates=true and user enters a future date', () => {
      const currentDate = new Date();
      const futureDate = new Date();
      futureDate.setDate(futureDate.getDate() + 1);
      controller = makeController({ filterOptions: { disallowFutureDates: true } });
      controller._currentDate = currentDate;
      controller.min = futureDate;
      controller.minDateValid = futureDate;
      expect(controller.min).toEqual(currentDate);
    });

    it('check if set minDateValid function sets date 1 month after min', () => {
      const min = new Date();
      const mockMinDateValid = new Date();
      mockMinDateValid.setMonth(min.getMonth() + 1);

      controller = makeController({
        onChange: () => {},
        min,
        max: new Date(),
        _minDateValid: '',
        rangeValidator: true,
      });

      expect(controller.minDateValid).toEqual(mockMinDateValid);

      controller.rangeValidator = false;
      controller.minDateValid = min;
      expect(controller.minDateValid).not.toEqual(mockMinDateValid);
      expect(controller.minDateValid).toEqual(controller.min);
    });
  });

  describe('Controller setMinMaxDate()', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({});
    });

    it('onChange in setMinMaxDate function', () => {
      const today = new Date().getDate();
      const tomorrow = new Date().setDate(today + 1);

      controller = makeController({
        onChange: () => {},
        min: today,
        max: tomorrow,
      });

      spyOn(controller, 'onChange').and.callThrough();
      spyOn($rootScope, '$broadcast');

      controller.setMinMaxDate({ $valid: true });
      $timeout.flush();
      expect(controller.isValid).toEqual(true);
      expect(controller.onChange).toHaveBeenCalledWith({
        minDate: today,
        maxDate: tomorrow,
      }, undefined);

      // Test invalid input
      controller.onChange.calls.reset();
      controller.setMinMaxDate({ $valid: false });
      $timeout.flush();
      expect(controller.onChange).not.toHaveBeenCalled();
      expect(controller.isValid).toEqual(false);

      // Test max less than min
      controller.onChange.calls.reset();
      controller.min = tomorrow;
      controller.max = today;
      controller.setMinMaxDate({ $valid: true });
      $timeout.flush();
      expect(controller.onChange).not.toHaveBeenCalled();
      expect(controller.isValid).toEqual(false);

      // Test max is empty
      controller.onChange.calls.reset();
      controller.min = today;
      controller.max = '';
      controller.setMinMaxDate({ $valid: true });
      $timeout.flush();
      expect(controller.onChange).toHaveBeenCalledWith({
        minDate: today,
        maxDate: '',
      }, undefined);
      expect(controller.isValid).toEqual(true);

      // Test min is empty
      controller.onChange.calls.reset();
      controller.min = '';
      controller.max = tomorrow;
      controller.setMinMaxDate({ $valid: true });
      $timeout.flush();
      expect(controller.onChange).toHaveBeenCalledWith({
        minDate: '',
        maxDate: tomorrow,
      }, undefined);
      expect(controller.isValid).toEqual(true);

      // Test both max and min is empty
      controller.onChange.calls.reset();
      controller.min = '';
      controller.max = '';
      controller.setMinMaxDate({ $valid: true });
      $timeout.flush();
      expect(controller.onChange).toHaveBeenCalledWith({
        minDate: '',
        maxDate: '',
      }, undefined);
      expect(controller.isValid).toEqual(true);
    });

    it('check if min variable is a date in setMinMaxDate function', () => {
      const min = new Date();

      controller = makeController({
        onChange: () => {},
        min,
        _minDateValid: '',
      });

      controller.min = new Date(min.setDate(min.getDate() + 1));
      controller.setMinMaxDate({ $valid: true });

      expect(controller._minDateValid).toEqual(controller.min);

      controller.min = null;
      controller.setMinMaxDate({ $valid: true });
      expect(controller._minDateValid).toEqual(controller.min);
    });

    it('check if max variable is a date in setMinMaxDate function', () => {
      const max = new Date();

      controller = makeController({
        onChange: () => {},
        max,
        _maxDateValid: '',
      });

      controller.max = new Date(max.setDate(max.getDate() + 1));
      controller.setMinMaxDate({ $valid: true });

      expect(controller._maxDateValid).toEqual(controller.max);

      controller.max = null;
      controller.setMinMaxDate({ $valid: true });
      expect(controller._maxDateValid).toEqual(controller.max);
    });

    it('should force min date to current date when user enters future date and disallowFutureDates=true', () => {
      const currentDate = new Date();
      const futureDate = new Date();
      futureDate.setDate(futureDate.getDate() + 1);

      controller = makeController({
        filterOptions: { disallowFutureDates: true },
        min: futureDate,
        max: currentDate,
        onChange: () => {},
      });
      controller._currentDate = currentDate;
      controller.setMinMaxDate({ $valid: true });
      expect(controller.min).toEqual(currentDate);
    });

    it('should force max date to current date when user enters future date and disallowFutureDates=true', () => {
      const currentDate = new Date();
      const futureDate = new Date();
      futureDate.setDate(futureDate.getDate() + 1);

      controller = makeController({
        filterOptions: { disallowFutureDates: true },
        min: currentDate,
        max: futureDate,
        onChange: () => {},
      });
      controller._currentDate = currentDate;
      controller.setMinMaxDate({ $valid: true });
      expect(controller.max).toEqual(currentDate);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = DateRangeComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(DateRangeTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(DateRangeController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element('<date-range min="min" max="max"><date-range/>');
      element = $compile(element)(scope);
      scope.min = undefined;
      scope.max = undefined;
      scope.$apply();

      controller = element.controller('DateRange');
    });

    it('set initial scope', () => {
      expect(scope.min).not.toBeDefined();
      expect(scope.max).not.toBeDefined();
      expect(scope.title).not.toBeDefined();
    });

    it('should have a valid form state when both value is not empty', () => {
      const today = new Date();
      const tomorrow = new Date();

      tomorrow.setDate(today.getDate() + 1);

      scope.min = today;
      scope.max = tomorrow;
      scope.$digest();

      expect(element.find('form').hasClass('ng-valid')).toBeTruthy();
      expect(element.find('form').hasClass('ng-invalid')).toBeFalsy();
    });
  });
});
