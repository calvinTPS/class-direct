import angular from 'angular';
import dateRangeComponent from './date-range.component';
import forceModelUpdate from './force-model-update.directive';

export default angular.module('dateRange', [])
.component('dateRange', dateRangeComponent)
.directive('forceModelUpdate', forceModelUpdate)
.name;
