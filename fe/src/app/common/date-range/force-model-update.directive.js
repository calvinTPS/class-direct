/* eslint-disable no-mixed-operators */
import * as _ from 'lodash';
import dateHelper from 'app/common/helpers/date-helper';

/**
 * Force model updates on changes
 */
/* @ngInject */
export default (AppSettings, $parse) => ({
  require: ['mdDatepicker', 'ngModel'],
  restrict: 'A',
  link(
    scope,
    element,
    attributes,
    ctrls,
  ) {
    const inputEl = element.find('input');
    const mdDatepickerCtrl = ctrls[0];
    const ngModelCtrl = ctrls[1];
    const forceUpdateInvalidModel = $parse(attributes.forceModelUpdate)(scope);
    inputEl.on('input', () => {
      if (
        !forceUpdateInvalidModel && (dateHelper.isValid(inputEl.val()) || !inputEl.val())
      ) {
        /**
         * Set datepicker model and ngModel value to prevent not calling backend
         * on changes.
         *
         * This checkpoint being made so that it won't affecting fleet dashboard date-range
         */
        const value = inputEl.val() ? new Date(inputEl.val()) : '';

        mdDatepickerCtrl.setModelValue(value);

        ngModelCtrl.$setViewValue(value);
        ngModelCtrl.$render();
        scope.$broadcast(AppSettings.EVENTS.FORCE_MODEL_UPDATE, attributes.name);
      } else if (forceUpdateInvalidModel) {
        /**
        * This checkpoint being made so that it target specifically only on rider-filter date-range
        *
        * This conditional changes being made because previously the directive doesn't update
        * the model when the input data was invalid.
        *
        *
        * Had to force the value using 'invalid' or string value since
        * if the number value being passed
        * it will treated as an epoch number and being converted to '01 Jan 1970'
        * luckily it won't affect date picker render view since the model being validated again
        * inside the md-datepicker controller
        */

        const value = dateHelper.isValid(inputEl.val()) ? new Date(inputEl.val()) : 'invalid';
        mdDatepickerCtrl.setModelValue(value);

        ngModelCtrl.$setViewValue(value);
        ngModelCtrl.$render();
        scope.$broadcast(AppSettings.EVENTS.FORCE_MODEL_UPDATE, attributes.name);
      }
    });

    scope.$on(AppSettings.EVENTS.FORCE_MODEL_UPDATE, (obj, n) => {
      const isDateMinComponent = !forceUpdateInvalidModel &&
        n !== attributes.name && attributes.name === 'dateMin';
      const isDateMaxComponent = !forceUpdateInvalidModel &&
        n !== attributes.name && attributes.name === 'dateMax';

      const isLessThanMinDate = !_.isUndefined(mdDatepickerCtrl.minDate) &&
        new Date(inputEl.val()) <= mdDatepickerCtrl.minDate;
      const isMoreThanMaxDate = !_.isUndefined(mdDatepickerCtrl.maxDate) &&
        new Date(inputEl.val()) >= mdDatepickerCtrl.maxDate;

      if (
        (isDateMinComponent && !isLessThanMinDate) ||
        (isDateMaxComponent && !isMoreThanMaxDate)
      ) {
        mdDatepickerCtrl.setModelValue(inputEl.val() ? new Date(inputEl.val()) : '');
        mdDatepickerCtrl.updateErrorState.call(mdDatepickerCtrl);
      } else if (
        forceUpdateInvalidModel &&
        n !== attributes.name &&
        (dateHelper.isValid(inputEl.val()) || !inputEl.val())
      ) {
        // Validator is used to update datepicker if force update invalid model
        // is turned on
        mdDatepickerCtrl.setModelValue(inputEl.val() ? new Date(inputEl.val()) : '');
        mdDatepickerCtrl.updateErrorState.call(mdDatepickerCtrl);
      }
    });
  },
});
