import './date-range.scss';
import controller from './date-range.controller';
import template from './date-range.pug';

export default {
  bindings: {
    filterOptions: '<?',
    forceUpdateInvalidModel: '<?',
    hasDatepickerLabel: '<?',
    hasTimeline: '<?',
    hideTitle: '<?',
    isValid: '=?',
    max: '=',
    maxLimit: '<',
    min: '=',
    minLimit: '<',
    onChange: '&',
    rangeLayout: '@',
    rangeValidator: '<',
    title: '@',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
