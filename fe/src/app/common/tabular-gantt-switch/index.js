import angular from 'angular';
import tabularGanttSwitchComponent from './tabular-gantt-switch.component';
import uiRouter from 'angular-ui-router';

export default angular.module('tabularGanttSwitch', [
  uiRouter,
])
.component('tabularGanttSwitch', tabularGanttSwitchComponent)
.name;
