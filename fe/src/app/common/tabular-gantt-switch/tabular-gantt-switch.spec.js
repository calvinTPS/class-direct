/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import TabularGanttSwitchComponent from './tabular-gantt-switch.component';
import TabularGanttSwitchController from './tabular-gantt-switch.controller';
import TabularGanttSwitchModule from './';
import TabularGanttSwitchTemplate from './tabular-gantt-switch.pug';

describe('TabularGanttSwitch', () => {
  let $rootScope, $compile, makeController;

  beforeEach(window.module(TabularGanttSwitchModule));

  beforeEach(() => {
    const mockViewService = {
      setServiceScheduleViewType: () => true,
    };

    const mockDashBoardService = {
      resetFavourites: () => true,
    };

    window.module(($provide) => {
      $provide.value('ViewService', mockViewService);
      $provide.value('DashboardService', mockDashBoardService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('tabularGanttSwitch', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({
        foo: 'bar',
        type: 'tabular',
      });
    });

    it('initializes just fine', () => {
      expect(controller.foo).toEqual('bar');
    });

    it('tabs is defined', () => {
      expect(controller._tabs).toBeDefined();
      expect(controller.tabs).toEqual(controller._tabs);
    });

    it('returns the correct value of tabs', () => {
      expect(controller.tabs[0].type).toEqual('tabular');
      expect(controller.tabs[1].type).toEqual('gantt');
    });

    it('updates the correct value of type', () => {
      const tabs = [
        { type: 'tabular' },
        { type: 'gantt' },
      ];
      controller.setType(tabs[0]);
      expect(controller.type).toEqual('tabular');

      controller.setType(tabs[1]);
      expect(controller.type).toEqual('gantt');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = TabularGanttSwitchComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(TabularGanttSwitchTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(TabularGanttSwitchController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element =
        angular.element('<tabular-gantt-switch is-tabular="isTabular"><tabular-gantt-switch/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('TabularGanttSwitch');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
