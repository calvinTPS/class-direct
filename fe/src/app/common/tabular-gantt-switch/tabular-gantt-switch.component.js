import './tabular-gantt-switch.scss';
import controller from './tabular-gantt-switch.controller';
import template from './tabular-gantt-switch.pug';

export default {
  bindings: {
    type: '=',
    location: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
