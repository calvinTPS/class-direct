import Base from 'app/base';

export default class TabularGanttSwitchController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    ViewService,
    DashboardService,
  ) {
    super(arguments);
  }

  $onInit() {
    const { TABULAR, GANTT } = this._AppSettings.SERVICE_SCHEDULE.VIEW_TYPES;
    this._tabs = [
      {
        icon: 'list',
        type: TABULAR,
      },
      {
        icon: 'gantt',
        type: GANTT,
      }];

    this.selectedTabIndex = this.type === 'gantt' ? 1 : 0;
  }

  get tabs() {
    return this._tabs;
  }

  setType(tab) {
    this.type = tab.type;
    this._ViewService.setServiceScheduleViewType({
      location: this.location,
      type: this.type,
    });
    this._DashboardService.resetFavourites();
  }
}
