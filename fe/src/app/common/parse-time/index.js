import angular from 'angular';
import parseTime from './parse-time.directive';

export default angular.module('parseTime', [
])
.directive('parseTime', parseTime)
.name;
