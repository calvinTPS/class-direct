import * as _ from 'lodash';

/* @ngInject */
export default () => ({
  restrict: 'A',
  scope: true,
  controllerAs: 'ctrl',
  require: 'ngModel',
  link: (
    $scope,
    element,
    attributes,
    controller,
  ) => {
    // const TIME_REGEX = AppSettings.FORMATTER.TIME_TWENTYFOUR_HOURS
    // Above line doesn't work, but we should find a better way to
    // have Regexp constants. Can anyone help?
    const TIME_REGEX = /([01]+?[0-9]|2[0-3]):[0-5][0-9]$/;
    element.bind('blur', () => {
      // this will run all formatters onBlur. Eliminates the need for $timeout
      // I'd like to ideally make this a directive if anyone else needs to re-use it
      const viewValue = controller.$modelValue;
      _.each(controller.$formatters, (formatter) => {
        formatter(viewValue);
      });
      controller.$render();
    });

    controller.$parsers.unshift((value) => {
      const viewValue = _.padStart(value.replace(/[^0-9:]/g, ''), 5, '0');
      // ^ removes unwanted characters and parses & pads it with a leading '0'
      // if leading zero is missing. I.e. 4:00 -> 04:00
      if (TIME_REGEX.test(viewValue)) {
        // it is valid
        controller.$setValidity('time', true);
        return viewValue;
      } else if (value === '') {
        controller.$setValidity('time', true);
        return 0;
      }
      // it is invalid, return undefined (no model update)
      controller.$setValidity('time', false);
      return value;
    });

    controller.$formatters.push((modelValue) => {
      if (modelValue === 0) { // we're using integer pence, so this is safe
        return '';
      }
      return modelValue;
    });
  },
});
