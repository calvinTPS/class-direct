/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import CdTooltipComponent from './cd-tooltip.component';
import CdTooltipController from './cd-tooltip.controller';
import CdTooltipModule from './';
import CdTooltipTemplate from './cd-tooltip.pug';

describe('CdTooltip', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(CdTooltipModule));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('cdTooltip', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdTooltipComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CdTooltipTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdTooltipController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<cd-tooltip foo="bar"><cd-tooltip/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('CdTooltip');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
