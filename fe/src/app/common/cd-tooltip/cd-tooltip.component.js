import './cd-tooltip.scss';
import controller from './cd-tooltip.controller';
import template from './cd-tooltip.pug';

export default {
  bindings: {
    position: '@',  // default `top` - `top|bottom`
    theme: '@?',    // default `dark-blue` - `dark-blue|dark-grey|warn`
    template: '<?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
  transclude: true,
};
