import angular from 'angular';
import cdTooltipComponent from './cd-tooltip.component';
import uiRouter from 'angular-ui-router';

export default angular.module('cdTooltip', [
  uiRouter,
])
.component('cdTooltip', cdTooltipComponent)
.name;
