import angular from 'angular';
import disableAnimate from './disable-animate.directive';

export default angular.module('disableAnimate', [
])
  .directive('disableAnimate', disableAnimate)
  .name;
