/* @ngInject */
export default $animate => ({
  restrict: 'A',
  link: (
    scope,
    element,
  ) => {
    $animate.enabled(element, false);
  },
});
