/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import PageHeaderComponent from './page-header.component';
import PageHeaderController from './page-header.controller';
import PageHeaderModule from './';
import PageHeaderTemplate from './page-header.pug';

describe('PageHeader', () => {
  let $rootScope,
    $compile,
    $state,
    $window,
    makeController;

  beforeEach(window.module(PageHeaderModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;

    const mockUserService = {
      getCurrentUser: () => Promise.resolve({ userId: 1, name: 'mockUser', isEquasisThetis: false }),
    };

    const mockPermissionsService = {
      canAccessMPMS: () => true,
      canAccessCertsAndCodicils: () => {},
      canAccessSistersSection: () => {},
      canAccessJobs: () => {},
      canPrintExport: user => user && !user.isEquasisThetis,
    };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
      $provide.value('AppSettings', AppSettings);
      $provide.value('UserService', mockUserService);
      $provide.value('PermissionsService', mockPermissionsService);
      $provide.value('$state', {});
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$window_, _$state_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $window = _$window_;
    $state = {
      previous: {
        name: null,
      },
    };

    const dep = {};
    makeController =
      async (bindings = {}) => {
        const controller = $componentController('pageHeader', { $dep: dep, $state }, bindings);
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('useHistoryFirst should return the correct value', async () => {
      const controller = await makeController();
      expect(controller.useHistoryFirst).toBeFalsy();

      controller.useHistoryFirst = true;
      expect(controller.useHistoryFirst).toEqual(true);
    });

    it.async('goTo should set and return the correct value', async () => {
      const controller = await makeController();
      expect(controller.goTo).toEqual(undefined);

      controller.useHistoryFirst = true;
      controller.goTo = null;
      expect(controller.goTo).toEqual(false);
      controller.goTo = 'newState';
      expect(controller.goTo).toEqual('newState');

      controller._$state.previous.name = 'previousStateName';
      controller.goTo = null;
      expect(controller.goTo).toEqual(false);
      controller.goTo = 'newState';
      expect(controller.goTo).toEqual(false);
    });

    describe('goBack() should call the expected function', () => {
      it.async('goBack() should call $window.history.go() when there is previous history', async () => {
        const controller = await makeController();
        const spyWindow = spyOn(controller._$window.history, 'go');
        controller.goBack();
        expect(spyWindow).not.toHaveBeenCalled();

        controller._$state.previous.name = 'previousStateName';
        controller.goBack();
        expect(spyWindow).not.toHaveBeenCalled();

        controller.useHistoryFirst = true;
        controller.goBack();
        expect(spyWindow).toHaveBeenCalledWith(-1);
      });

      it.async('goBack() should call onClick() when there is no previous history', async () => {
        const controller = await makeController();
        const spyWindow = spyOn(controller._$window.history, 'go');

        controller.goBack();
        expect(spyWindow).not.toHaveBeenCalled();
        expect(typeof controller.onClick).not.toEqual('function');

        controller.onClick = () => null;
        const spyClick = spyOn(controller, 'onClick');
        controller.goBack();
        expect(spyWindow).not.toHaveBeenCalled();
        expect(spyClick).toHaveBeenCalled();
      });
    });

    it.async('showIcon should display back button icon correctly', async () => {
      const controller = await makeController();
      expect(controller.showIcon).toBeFalsy();

      controller.goTo = 'newState';
      expect(controller.showIcon).toEqual(true);

      controller.goTo = false;
      controller.onClick = () => null;
      expect(controller.showIcon).toEqual(true);

      controller.onClick = {};
      controller.useHistoryFirst = true;
      expect(controller.showIcon).toEqual(true);
    });

    it.async('showPrintExport() should display the export button correctly', async () => {
      const controller = await makeController();
      controller.exportOptions = { itemType: 'Foo' };
      expect(controller.showPrintExport(controller.currentUser)).toEqual(true);

      controller.exportOptions = { itemType: AppSettings.EXPORTABLE_ITEMS.DEFECTS };
      expect(controller.showPrintExport(controller.currentUser)).toEqual(false);

      controller.currentUser.isEquasisThetis = true;
      expect(controller.showPrintExport(controller.currentUser)).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = PageHeaderComponent;

    it.async('includes the intended template', async () => {
      expect(component.template).toEqual(PageHeaderTemplate);
    });

    it.async('invokes the right controller', async () => {
      expect(component.controller).toEqual(PageHeaderController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<page-header foo="bar"><page-header/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('PageHeader');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
