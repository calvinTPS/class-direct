import './page-header.scss';
import controller from './page-header.controller';
import template from './page-header.pug';

export default {
  bindings: {
    exportOptions: '<',
    goTo: '@',
    headingSub: '@?',
    label: '@',
    labelSub: '@?',
    onClick: '&?',
    useHistoryFirst: '<?',
    disableBackgroundImage: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
