import * as _ from 'lodash';
import Base from 'app/base';

export default class PageHeaderController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $window,
    PermissionsService,
    UserService,
    AppSettings,
  ) {
    super(arguments);
  }

  async $onInit() {
    this.currentUser = await this._UserService.getCurrentUser();
  }

  get goTo() {
    return this._goTo;
  }

  set goTo(newState) {
    this._goTo = (
      (this.useHistoryFirst && this._$state.previous.name) ||
      !newState
    ) ?
      false : newState;
  }

  goBack() {
    if (this.useHistoryFirst && this._$state.previous.name) {
      this._$window.history.go(-1);
    } else if (_.isFunction(this.onClick)) {
      this.onClick();
    }
  }

  get showIcon() {
    return !_.isEmpty(this.goTo) || _.isFunction(this.onClick) || this.useHistoryFirst;
  }

  showPrintExport() {
    return this._PermissionsService.canPrintExport(this.currentUser)
      && this.exportOptions.itemType !== this._AppSettings.EXPORTABLE_ITEMS.DEFECTS;
  }
}
