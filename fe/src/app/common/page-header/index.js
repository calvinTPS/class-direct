import angular from 'angular';
import pageHeaderComponent from './page-header.component';
import uiRouter from 'angular-ui-router';

export default angular.module('pageHeader', [
  uiRouter,
])
.component('pageHeader', pageHeaderComponent)
.name;
