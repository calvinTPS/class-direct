/* @ngInject */
export default ($document, $parse, $timeout) => ({
  restrict: 'A',

  link(scope, elem, attrs) {
    const targetElem = $document[0].querySelector('#main-wrapper');
            // Watch for changes
    scope.$watch(() => targetElem.offsetHeight,
      (newValue, oldValue) => {
        if (newValue !== oldValue) {
          elem.css('height', newValue);
        }
      });
  },
});
