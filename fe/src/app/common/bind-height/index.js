import angular from 'angular';
import bindHeight from '../bind-height/bind-height.directive';

export default angular.module('bindHeight', [
])
.directive('bindHeight', bindHeight)
.name;
