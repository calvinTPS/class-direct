/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable angular/window-service, no-undef, no-underscore-dangle,
no-unused-vars, one-var-declaration-per-line, one-var, sort-vars */
import ActiveStatusComponent from './active-status.component';
import ActiveStatusController from './active-status.controller';
import ActiveStatusModule from './active-status';
import ActiveStatusTemplate from './active-status.pug';
import angularMaterial from 'angular-material';
import mockAppSettings from 'app/config/project-variables.js';
import utils from 'app/common/utils/utils.factory';

describe('ActiveStatus', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    makeController;

  beforeEach(window.module(
    ActiveStatusModule,
    angularMaterial,
  ));

  beforeEach(() => {
    const mockFilter = x => y => y;

    window.module(($provide) => {
      $provide.value('$filter', mockFilter);
      $provide.value('AppSettings', mockAppSettings);
      $provide.value('utils', utils());
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('activeStatus', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        foo: 'bar',
      });
      expect(controller.foo).toEqual('bar');
    });

    it('set statusName() should set the satatusName correctly', () => {
      const controller = makeController({});

      expect(controller.statusName).toBeUndefined();

      controller.statusName = 'Hello World';
      expect(controller.statusName).toEqual('hello-world');

      controller.statusName = 'Hello - World';
      expect(controller.statusName).toEqual('hello-world');
    });

    it('get toolTipHoverText() should retrun the correct value', () => {
      const controller = makeController({});
      expect(controller.toolTipHoverText).toBeNull();

      const status = mockAppSettings.OVERALL_STATUS_NAMES;

      controller.statusName = status.OVERDUE;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-overdue');

      controller.statusName = status.IMMINENT;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-imminent');

      controller.statusName = status.DUE_SOON;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-due-soon');

      controller.statusName = status.NOT_DUE;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-not-due');

      controller.hasPostponedItems = true;

      controller.statusName = status.OVERDUE;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-postponed-overdue');

      controller.statusName = status.IMMINENT;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-postponed-imminent');

      controller.statusName = status.DUE_SOON;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-postponed-due-soon');

      controller.statusName = status.NOT_DUE;
      expect(controller.toolTipHoverText).toEqual('cd-asset-overall-status-postponed-not-due');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ActiveStatusComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ActiveStatusTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ActiveStatusController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<active-status foo="bar"><active-status/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ActiveStatus');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
