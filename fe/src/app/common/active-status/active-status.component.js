import './active-status.scss';
import controller from './active-status.controller';
import template from './active-status.pug';

export default {
  bindings: {
    customLabel: '<',
    enableTooltip: '<?',
    hasPostponedItems: '<?',
    iconPosition: '@?',
    mode: '@?',
    showLabel: '<',
    statusName: '<',
    useEllipsis: '<?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
