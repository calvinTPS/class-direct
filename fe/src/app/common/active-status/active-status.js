import activeStatusComponent from './active-status.component';
import angular from 'angular';
import uiRouter from 'angular-ui-router';

export default angular.module('activeStatus', [
  uiRouter,
])
.component('activeStatus', activeStatusComponent)
.name;
