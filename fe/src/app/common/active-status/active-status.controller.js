import * as _ from 'lodash';
import Base from 'app/base';

export default class ActiveStatusController extends Base {
  /* @ngInject */
  constructor(
    $filter,
    $mdMedia,
    AppSettings,
    utils,
  ) {
    super(arguments);
  }

  $onInit() {
    this.iconPosition = this.iconPosition || 'left';
  }

  get statusName() {
    return this._statusName;
  }

  set statusName(statusName) {
    this._statusName = statusName ?
      statusName.replace('- ', '').split(' ').join('-').toLowerCase() : '';
  }

  get statusLabel() {
    return this.customLabel || this._$filter('safeTranslate')(`cd-${this._statusName}`);
  }

  get isIconCircle() {
    if (this.hasPostponedItems ||
      this._statusName === this._AppSettings.ACCOUNT_STATUS.ARCHIVED.toLowerCase() ||
      this._statusName === this._AppSettings.CODICILS_STATUSES.CANCELLED.toLowerCase()) {
      return false;
    }
    return true;
  }

  get iconName() {
    let iconName;
    if (this.hasPostponedItems) {
      iconName = 'triangle';
    } else {
      iconName = 'cancel';
    }
    return iconName;
  }

  get toolTipHoverText() {
    let toolTipHoverText = null;

    if (this.hasPostponedItems) {
      const status = _.cloneDeep(this._AppSettings.OVERALL_STATUS_NAMES);
      switch (_.startCase(this._statusName)) {
        case status.OVERDUE:
          toolTipHoverText = 'cd-asset-overall-status-postponed-overdue';
          break;
        case status.IMMINENT:
          toolTipHoverText = 'cd-asset-overall-status-postponed-imminent';
          break;
        case status.DUE_SOON:
          toolTipHoverText = 'cd-asset-overall-status-postponed-due-soon';
          break;
        case status.NOT_DUE:
          toolTipHoverText = 'cd-asset-overall-status-postponed-not-due';
          break;
        default:
          break;
      }
    } else {
      toolTipHoverText = this._utils.getDueStatusDefinition(
        '',
        this._statusName,
      );
    }

    toolTipHoverText = toolTipHoverText ? this._$filter('safeTranslate')(toolTipHoverText) : null;

    return toolTipHoverText;
  }

}
