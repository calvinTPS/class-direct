/* eslint-disable angular/timeout-service, angular/document-service,
angular/window-service, no-underscore-dangle, import/no-webpack-loader-syntax */
import 'whatwg-fetch';
import * as _ from 'lodash';
import Base from 'app/base';
import reloadScript from 'raw-loader!reload-script.js';
// import errorDialogTemplate from 'app/_partials/error-dialog.pug';
// import GeneralModalController from 'app/common/dialogs/general-modal.controller';

export default class AppController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $location,
    $mdDialog,
    $scope,
    $state,
    $rootScope,
    $transitions,
    $timeout,
    $translate,
    $window,
    Analytics,
    AppSettings,
    cfpLoadingBar,
    fetch,
    Restangular,
    TermsAndConditionsService,
    UserService,
    utils,
    ViewService,
    ErrorService,
  ) {
    super(arguments);
  }

  /**
    I use a slightly different approach is necessary for logging into mobile devices
    We use the inAppBrowser to initialize the cookie and CSRF token.
    I run before the actual this.run() method.
    */
  async _mobileRun() {
    // @FIXME - Remove when going live
    function l() { // I use a function because scope is needsd
      const args = Array.prototype.slice.call(arguments);
      args.unshift('(_mobileRun)');
      // eslint-disable-next-line
      console.log.apply(console, args);
    }

    l('`mobileRun()` initialized');
    // I use fetch as it returns the 302 redirect URL much more easily than
    // Restangular, which needs an unreliable hack
    const url = `${this._AppSettings.API_URL}/${this._AppSettings.API_ENDPOINTS.InitCSRF}`;
    const loginURL = (await fetch(url)).url;

    // If yes, I return this.initilizeUser.
    l('Fetch has returned the following URL:', loginURL);
    // I check if the returned response is a 302 redirect to Azure
    // If no, I end the login process - I've already logged in.
    // If yes, I use that URL & check if URL is Azure.
    if (loginURL.indexOf('microsoft') >= 0) {
      const inAppBrowser = this._$window.cordova.InAppBrowser.open(url, '_blank',
        'location=no,toolbar=no,clearcache=yes,clearsessioncache=yes');

      const code = reloadScript
        .replace('LOGIN_URL', `"${loginURL}"`)
        .replace('LOGIN_RELOAD_TIME', `${this._AppSettings.LOGIN_RELOAD_TIME}`);

      l(`Code injected to inAppBrowser${code}`);

      // I should only run once, and show the login page in the inAppBrowser
      inAppBrowser.addEventListener('loadstop', (opt) => {
        inAppBrowser.executeScript({
          code,
        }, params => l('Script executed in inAppBrowser', params));
        if (
          opt.url.indexOf(this._AppSettings.API_URL) >= 0
        ) {
          l('`loadStop` event received from ', opt.url);
          inAppBrowser.close();

          // Cordova's WebViews cannot access cookie via document.cookie, hence Angular
          // cannot do the magic for us. Retrieve the CSRF token manually and
          // set it on window for retrieval later.
          window.cookieEmperor.getCookie(
            this._AppSettings.BASE_URL,
            this._AppSettings.COOKIE_XSRF_TOKEN,
            (data) => {
              l('Retrieved xsrf token from plugin and setting on global...', data.cookieValue);
              window[this._AppSettings.GLOBAL_XSRF_TOKEN] = data.cookieValue;
            },
            (error) => {
              if (error) {
                l(`Error retrieving XSRF token: ${error}`);
              }
            }
          );

          const exitAppPopup = () => {
            let result = true;
            if (navigator.notification) {
              navigator.notification.confirm(
                this._$translate('cd-logout-confirmation'),
                (stat) => {
                  if (stat === '1') {
                    navigator.app.exitApp();
                  } else {
                    result = false;
                  }
                },
                '',
                this._$translate('cd-logout-confirmation-options'),
              );
            }
            return result;
          };

          this._$window.onUnload = exitAppPopup();

          this.initializeSession();
        }
        return false;
      });
    } else {
      l('Initializing session without login');
      return this.initializeSession();
    }
    return true;
  }

  // I initialize the app by running the UserService's initializeSession()
  // which in turn initializes the CSRF token before getting the user details.
  async $onInit() {
    // Add `.is-touch-device` class on body element if it's a touch device
    if ('ontouchstart' in window || navigator.maxTouchPoints) {
      angular.element(this._$document[0].body).addClass('is-touch-device');
    } else {
      angular.element(this._$document[0].body).addClass('not-touch-device');
    }

    // I check if the app is in the background and hide sensitive contents to prevent iOS snaphots
    this.showBody = true;
    const paused = () => { this.showBody = false; };
    const resumed = () => { this.showBody = true; };

    this._$document[0].addEventListener('pause', paused, false);
    this._$document[0].addEventListener('resume', resumed, false);

    function l() {
      const args = Array.prototype.slice.call(arguments);
      args.unshift('($onInit)');
      // eslint-disable-next-line
      console.log.apply(console, args);
    }

    // I determine if I am still initializing and will be false
    // when login and init-csrf are complete
    this.isInitializing = true;

    // I check if we're a cordova app and run something extra
    // so that we can perfor and Azure login.
    if (this._$window.cordova) {
      l('this is a mobile app');
      this._$document[0].addEventListener(
        'deviceReady', () => {
          // Clear all cookies because managed by Cordova cookie plugin
          this._$window.cookieEmperor.clearAll();
          this._mobileRun();
        }
      );
    } else {
      l('I am about to initialize the app');
      // I finally initialize the session, which in turn,
      // is followed by this._initializeApp();
      this.initializeSession();
    }
  }

  async initializeSession() {
    function l() { // I use a function to have a scope
      const args = Array.prototype.slice.call(arguments);
      args.unshift('(initializeSession)');
      // eslint-disable-next-line
      console.log.apply(console, args);
    }

    l('Initializing app...');
    this._initializeApp();

    await this._UserService.initializeSession();
    l('Initialize session done. Initializing user...');

    await this._UserService.initializeUser();
    this.isInitializing = false;

    this._$scope.$apply();

    l('Broadcasting APP_INITIALIZED...');
    this._$rootScope.$broadcast(
      this._AppSettings.EVENTS.APP_INITIALIZED,
    );

    if (this._$window.cordova) {
      l('Going to vessel list...');
      this._$state.go(this._AppSettings.STATES.VESSEL_LIST);
    }
  }

  // I encapsulate all the initializations for the app, and am in a separate
  // method so that I can be run without blocking the page load
  _initializeApp() {
    function l() {
      const args = Array.prototype.slice.call(arguments);
      args.unshift('(_initializeApp)');
      // eslint-disable-next-line
      console.log.apply(console, args);
    }

    l(`_initializeApp started. API_URL is ${this._AppSettings.API_URL}`);
    this.name = 'app controller';
    this._isEORAsset = false;
    this.isTransitioning = false;

    this._statesAccessibleToNonLR = [
      this._AppSettings.STATES.ASSET_DETAILS,
      this._AppSettings.STATES.ASSET_HOME,
    ];

    this._$scope.$on(this._AppSettings.EVENTS.START_MASQUERADE, (event) => {
      this.masqueradeBarVisible = true;
      this._$scope.$apply();
    });

    this._$scope.$on(this._AppSettings.EVENTS.IDLE_TIMEOUT, () => {
      l('IDLE_TIMEOUT received, logging out');
      this._UserService.logout();
    });

    this._$rootScope.$on(this._AppSettings.EVENTS.LOADING_BAR_COMPLETED, () => {
      // Nothing to do for now.
    });

    // We need to keep the backend session alive because:
    // 1. If user is interacting with page without clicking on anything that calls BE,
    //    and then proceeds to call BE, we get a 302 response to login page via XHR that
    //    cannot be intercepted by FE (browser handles it first).
    // 2. If we call /logout when session has timed out in BE, we get a 405 error.
    // NOTE: Not using KeepAlive because: simplicity.
    this._$window.setInterval(() => {
      this._Restangular.one(this._AppSettings.API_ENDPOINTS.Ping).get();
    }, this._AppSettings.KEEPALIVE_INTERVAL_IN_SECONDS * 1000);

    if (this._utils.isIE(navigator)) {
      this._$document[0].documentElement.setAttribute('data-useragent', 'MSIE');
    }

    //
    // Error interceptor setup
    //
    this._Restangular.setErrorInterceptor((response, deferred, responseHandler) => {
      if (this._$window.location.hostname.indexOf('classdirect') >= 0 && this._$window.trackJs) {
        this._$window.trackJs.track(`Http Error ${response.status} received, the response is`, response);
      }

      if (
        _.indexOf(this._AppSettings.ERROR_CODES.UNAUTHORIZED, response.status) !== -1
      ) {
        l(`Error ${response.status} received:`, response);
        l(`Redirecting to ${this._AppSettings.STATES.ERROR} because ${response.data.errorCode} at`, response.data.message);
        this._$state.go(this._AppSettings.STATES.ERROR, {
          statusCode: response.data.errorCode,
          errorMessage: response.data.message,
        });
        l('Error Handled');
        // Return response so the calling component can catch the error
        // for additional error handling
        return response;
      } else if (
        _.indexOf(this._AppSettings.ERROR_CODES.UNDER_MAINTENANCE, response.status) !== -1
      ) {
        l('Error: UNDER MAINTENANCE');
        this._$state.go(this._AppSettings.STATES.ERROR, {
          statusCode: response.status,
          errorMessage: 'cd-under-maintenance',
        });
        // Return response so the calling component can catch the error
        // for additional error handling
        return response;
      } else if (
        _.indexOf(this._AppSettings.ERROR_CODES.SERVER, response.status) !== -1
      ) {
        l(`Error: Server error ${response.status}`);
        l(response); // For more information when we have 500 errors

        const errorDescription =
          `${response.config.method} ${response.config.url} is returning ${response.status} ${response.statusText}.
            The browser I am using is ... version ... and my device is ...`;

        this._ErrorService.showErrorDialog({
          description: errorDescription,
          redirectState: this._AppSettings.STATES.VESSEL_LIST,
          confirmLabel: 'cd-return-to-asset-list',
        });

        // Return response so the calling component can catch the error
        // for additional error handling
        return response;
      }
      l('Error not handled');
      return true; // error not handled
    });

    // onSuccess is the only transition callback that captures initial page load.
    this._$transitions.onSuccess({}, async (transition) => {
      // Complete the progress bar on top
      this._cfpLoadingBar.complete();

      l('Setting isTransitioning to false');
      this.isTransitioning = false;
      l('Scrolling to top');
      this._utils.scrollToTop();
      l('Setting updateMode to false');
      this._updateMode();

      this._Analytics.trackPage(this._$location.path());

      const state = transition.$to();

      l('`getCurrentUser` called');
      const currentUser = await this._UserService.getCurrentUser();
      if (!currentUser.canAccessState(state.name)) {
        l('User can\'t access state, redirecting to error page with 401');
        this._$state.go(this._AppSettings.STATES.ERROR, { statusCode: 401 });
        return;
      }

      const latestTC = await this._TermsAndConditionsService.getLatestTermsAndConditions();
      const hasAcceptedLatestTC = currentUser.hasAcceptedLatestTC(latestTC);
      l('Latest T&C', latestTC);
      l('User accepted T&C: ', hasAcceptedLatestTC);
      if (
        !currentUser.isEquasisThetis &&
        !hasAcceptedLatestTC &&
        !this._UserService.isMasquerading
      ) {
        l('Redirecting to T&C page');
        this._$state.go(this._AppSettings.STATES.ACCEPT_TERMS_AND_CONDITIONS);
        return;
      }
    });

    this._$transitions.onStart({}, (transition) => {
      // Start the progress bar on top
      this._cfpLoadingBar.start();

      // Hide any open modal on transition.
      this._$mdDialog.hide();

      // This is used to display the global transitioning spinner.
      this.isTransitioning = true;
      l('Transition is starting', this.isTransitioning);
    });

    // Turn off transitioning between codicils listing tabs.
    this._$transitions.onStart({
      to: 'asset.codicilsListing.**',
      from: 'asset.codicilsListing.**',
    }, (transition) => {
      this.isTransitioning = false;
    });

    this._$transitions.onError({}, (transition) => {
      // Complete the progress bar on top
      this._cfpLoadingBar.complete();

      const errorMessage = _.split(transition.error(), ',');
      l('There\'s been an error!', errorMessage);
      // Caught an error and user has been redirected to error page. Page has initialised.
      this.isInitializing = false;
      return true;
    });

    // We need this to prevent Safari and Firefox from storing the DOM in the "bfcache"
    // "bfcache" will cause app to show on back button click after logout.
    // http://madhatted.com/2013/6/16/you-do-not-understand-browser-history
    this._$window.onunload = () => { };

    // If the above "bfcache" handler fails, we have the following
    this._$window.onpageshow = (evt) => {
      if (evt.persisted) {
        // evt.persisted is false on initial load. If true, it means
        // we are loading from back button press using FF/Safari Back/Forward cache "bfcache"
        // Force the user to relogin if necessary.
        this._$window.location.reload(true);
      }
    };
  }

  // This method is to update the role mode which is use to
  // let the system know if it is in EOR view or normal view.
  _updateMode() {
    switch (this._$state.current.name) {
      case this._AppSettings.STATES.EOR_VESSEL_LIST:
        this._$rootScope.role_mode = this._AppSettings.ROLES.EOR;
        this._isEORAsset = true;
        break;
      case this._AppSettings.STATES.VESSEL_LIST:
        this._$rootScope.role_mode = this._AppSettings.ROLES.CLIENT;
        this._isEORAsset = false;
        break;
      default:
        if (this._isEORAsset
          || this._$state.previous.name === this._AppSettings.STATES.EOR_VESSEL_LIST
        ) {
          this._$rootScope.role_mode = this._AppSettings.ROLES.EOR;
        } else {
          this._$rootScope.role_mode = this._AppSettings.ROLES.CLIENT;
          this._isEORAsset = false;
        }
    }
  }

  get isTnCState() {
    return this._$state.current.name === this._AppSettings.STATES.ACCEPT_TERMS_AND_CONDITIONS;
  }

  get loadingMessage() {
    const message = window.classDirect ? 'cd-general-page-loading-message-mobile-app' : 'cd-general-page-loading-message';
    return message;
  }
}
