/* eslint-disable */
// Restangular expects an old version of lodash to be available globally
import * as _ from 'lodash';

import 'normalize.css';
import angular from 'angular';
import 'angular-translate'; // Needs Angular to be loaded first.
import 'angular-translate-once';
import angularMaterial from 'angular-material';
import ngAnimate from 'angular-animate';
import uiRouter from 'angular-ui-router';
import uiSelect from 'ui-select';
import 'restangular';
import 'angular-base64';
import 'angular-bind-notifier';
import 'angular-cookies';
import 'angular-inview';
import 'angular-messages';
import 'angular-loading-bar';
import 'angular-google-analytics';
import 'angular-wizard';
import 'ng-idle';
import 'nouislider-angular-es6';
import 'inmoment.angular-ellipsis';

import AppComponent from './app.component';
import AppSettings from './config/project-variables.js'
import Common from './common/common';
import Components from './components/components';
import Config from './config/config';
import Colors from './config/config.colors';
import Security from './config/config.security.js';
import Key from 'app/constants/key';
import Routes from './config/config.routes';
import Run from './config/run';
import Services from './services/services';
import Filters from './filters';


import '../robots.txt';

export default angular.module('app', [
  angularMaterial,
  Common,
  Components,
  Filters,
  ngAnimate,
  'angular.bind.notifier',
  'angular-inview',
  'angular-google-analytics',
  'cfp.loadingBar',
  'base64',
  'inmoment.angular-ellipsis',
  'mgo-angular-wizard',
  'ngCookies',
  'ngIdle',
  'ngMessages',
  'pascalprecht.translate',
  'restangular',
  Services,
  'ui.select',
  uiRouter,
  'ya.nouislider',
])
.constant('AppSettings', AppSettings)
.constant('Key', Key)
.config(Colors)
.config(Config)
.config(Routes)
.config(Security)
.factory('fetch', ['Restangular', function (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setFullResponse(true);
    });
  }])
.component('app', AppComponent)
.run(Run)
.name;
