import './app.scss';
import controller from './app.controller';
import template from './app.pug';

/* @ngInject */
export default {
  template,
  restrict: 'E',
  controller,
  controllerAs: 'vm',
};
