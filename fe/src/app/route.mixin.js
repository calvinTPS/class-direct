import * as _ from 'lodash';

export default Base => class extends Base {
  constructor(args) {
    super(args);
    this.isResolved = false;
  }

  $onInit() {
    this.resolveAll();
  }

  resolveAll() {
    // Filters out only resolve methods that end with 'Defer'
    const toResolve = _.pickBy(this, (value, key) => _.isFunction(value) && _.endsWith(key, 'Defer'));

    const promisedFunctions = [];
    const promisedKeys = [];

    _.forEach(toResolve, (value, key) => {
      promisedKeys.push(key.replace('Defer', ''));
      promisedFunctions.push(value());
    });

    Promise.all(promisedFunctions)
      .then((resolvedValues) => {
        _.forEach(resolvedValues, (value, index) => {
          this[promisedKeys[index]] = value;
          this.onResolved = true;
        });
        this.isResolved = true;

        // Need this to force digest.
        this._$scope.$apply();
      });
  }
};
