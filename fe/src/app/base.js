/* eslint-disable no-return-assign */
/* eslint-disable import/no-dynamic-require, global-require */
export default class Base {
  constructor(args) {
    const inject = this.constructor.$inject || [];
    inject.forEach((key, index) => this[`_${key}`] = args[index]);
  }
}
