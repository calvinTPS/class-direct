import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import dateHelper from 'app/common/helpers/date-helper';
import enGB from 'resources/locales/en-GB.json';
import fecha from 'fecha';
import trackJs from 'trackjs';

/* @ngInject */
export default function config(
  $compileProvider,
  $httpProvider,
  $locationProvider,
  $logProvider,
  $mdDateLocaleProvider,
  $mdGestureProvider,
  $provide,
  $translateProvider,
  $urlMatcherFactoryProvider,
  $urlRouterProvider,
  $windowProvider,
  AnalyticsProvider,
  cfpLoadingBarProvider,
  IdleProvider,
  RestangularProvider,
) {
  const appWindow = $windowProvider.$get();
  const isProduction = appWindow.location.hostname.indexOf('classdirect') >= 0;
  if (trackJs && appWindow.trackJs) {
    // http://docs.trackjs.com/tracker/configuration
    appWindow.trackJs.configure({
      // TrackJS allows you to track errors for multiple applications from the same account.
      application: 'LR Class Direct',
      // Application version
      version: '1.0.0',
      // Whether the tracker should prevent duplicate error messages from being tracked
      // subsequently within a page view.
      dedupe: true,
    });
  }
  // Refactor returned payload
  // (obsolete, should be removed - was used when talking
  // to json-server & MAST)
  const listResponseInterceptor = (data) => {
    let extractedData = data;

    if (data.content && data.pagination) {
      extractedData = data.content || {};
      extractedData.pagination = data.pagination;
    }
    return extractedData;
  };

  // For mobile devices without jQuery loaded, do not
  // intercept click events during the capture phase.
  $mdGestureProvider.skipClickHijack();

  // If hijcacking clicks, change default 6px click distance
  $mdGestureProvider.setMaxClickDistance(12);

  // eslint-disable-next-line no-param-reassign
  cfpLoadingBarProvider.includeSpinner = false;

  // Angular Material fix for not supporting $onInit()
  $compileProvider.preAssignBindingsEnabled(true);

  // The following line enables / disables Angular scope debugging
  // $compileProvider.debugInfoEnabled(false);

  // extracting booleans from url parameters
  $urlMatcherFactoryProvider.type('boolean', {
    name: 'boolean',
    decode: val => val === true ? true : val === 'true',
    encode: val => val ? 1 : 0,
    equals: (a, b) => a === b,
    is: val => _.includes([true, false, 0, 1], val),
    pattern: new RegExp(/bool|true|0|1/),
  });

  // I force all Restangular calls to apply asynchronously
  $httpProvider.useApplyAsync(true);

  if ($windowProvider.$get().cordova) { // Otherwise icons and emblems won't load on mobile app
    RestangularProvider.addFullRequestInterceptor((element, op, what, url, headers, params) => {
      // In Cordova, Angular can't get XSRF-TOKEN from Cookie directly because document.cookie is
      // blank and unavailable. Instead, we retrieve it from the cookie plugin and set it here
      // for every request.
      if (
        headers[AppSettings.HEADER_XSRF_TOKEN.toLowerCase()] == null ||
        headers[AppSettings.HEADER_XSRF_TOKEN.toUpperCase()] == null
      ) {
        /* eslint-disable */
        if (window[AppSettings.GLOBAL_XSRF_TOKEN]) {
          headers[AppSettings.HEADER_XSRF_TOKEN.toLowerCase()] = window[AppSettings.GLOBAL_XSRF_TOKEN];
        }
        /* eslint-enable */
      }
      return { headers };
    });
  } else {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false,
    });
  }

  RestangularProvider.setBaseUrl(AppSettings.API_URL);

  // DO NOT UNCOMMENT IN COMMIT. For the occasional quick debugging in other browsers.
  // RestangularProvider.setDefaultHeaders({ userId: AppSettings.USER_ID });

  RestangularProvider.addResponseInterceptor(listResponseInterceptor);

  $translateProvider.translations('en_GB', enGB);
  $translateProvider.preferredLanguage('en_GB');
  $translateProvider.useSanitizeValueStrategy();

  $mdDateLocaleProvider.parseDate = dateStr => // eslint-disable-line no-param-reassign
    dateHelper.isValid(dateStr) ? new Date(dateStr) : new Date(NaN);

  $mdDateLocaleProvider.formatDate = date => // eslint-disable-line no-param-reassign
    _.isDate(date) ? fecha.format(date, AppSettings.FORMATTER.LONG_DATE_MOMENT) : '';

  const debug = Boolean(AppSettings.DEBUG);
  $logProvider.debugEnabled(debug);

  // Set idle mode duration in seconds
  IdleProvider.idle(AppSettings.IDLE_IN_SECONDS);
  // Immediately timeout after idle duration reached.
  // This cannot be 0, otherwise IdleTimeout event not triggered!
  IdleProvider.timeout(1);

  /* @ngInject */
  $provide.decorator('$exceptionHandler', ($delegate, $log) =>
    (exception, cause) => {
      // log errors with trackJs if production
      if (isProduction && appWindow.trackJs) {
        appWindow.trackJs.track(exception);
        // log error on the console as warning
        $log.warn(exception, cause);
      } else {
        // log error on the console for any others environment
        $delegate(exception, cause);
      }
    }
  );

  /* @ngInject */
  /* To store the previous state details from angular-ui-router $transitions to angular $state
   */
  $provide.decorator('$state', ($delegate, $log, $rootScope, $transitions) => {
    const $state = $delegate;
    $state.previous = undefined;

    $transitions.onStart({}, (trans) => {
      $state.previous = {
        name: trans.$from().name,
        params: _.clone($state.params),
      };
    });

    $state.defaultErrorHandler((err) => {
      if (err) {
        if (isProduction && appWindow.trackJs) {
          appWindow.trackJs.track(err);
        }
        $log.warn('State Error', err);
      }
    });

    return $state;
  });

  // I setup google analytics
  AnalyticsProvider.setAccount({
    tracker: AppSettings.GOOGLE_ANALYTICS_CODE,
    set: {
      forceSSL: false,
      // This is any set of `set` commands to make for the
      // account immediately after the `create` command for the account.
      // The property key is the command and the property value
      // is passed in as the argument, _e.g._, `ga('set', 'forceSSL', true)`.
      // Order of commands is not guaranteed as it is dependent on the
      // implementation of the `for (property in object)` iterator.
    },
  })
    .setHybridMobileSupport(true) // necessary to track cordova app routing
    .setPageEvent('$stateChangeSuccess'); // necessary for use with ui-router
}
