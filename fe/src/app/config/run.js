/* eslint-disable angular/document-service, angular/window-service*/

import * as _ from 'lodash';

/* @ngInject */
export default function run(
  $state,
  $window,
  AppSettings,
  UserService,
  Idle,
  Restangular,
) {
  // Watch the idle service
  Idle.watch();

  /**
   * Add another listener on page visibility. The ng-idle stops counting/timer
   * when user minimize application mobile browser.
   * Resource:  https://developer.mozilla.org/en-US/docs/Web/API/Page_Visibility_API
   *            https://www.html5rocks.com/en/tutorials/pagevisibility/intro/
   */
  (() => {
    const getHiddenPropertyName = () => {
      const prefixes = ['webkit', 'moz', 'ms', 'o'];

      // if 'hidden' is natively supported just return it
      if ('hidden' in document) return 'hidden';

      // otherwise loop over all the known prefixes until we find one
      let property = null;
      _.forEach(prefixes, (prefix) => {
        if (`${prefix}Hidden` in document) {
          property = `${prefix}Hidden`;
          return;
        }
      });
      return property;
    };

    let startTime = 0;
    const hiddenPropertyName = getHiddenPropertyName();
    const eventName = `${hiddenPropertyName.replace(new RegExp('[H|h]idden'), '')}visibilitychange`;
    document.addEventListener(eventName, () => {
      if (document[hiddenPropertyName]) {
        // Reset timestamp
        startTime = new Date();
      // Calculate saved timestamp with current timestamp
      } else if (!window.classDirect &&
        (startTime > 0 && (new Date() - startTime) / 1000 >= AppSettings.IDLE_IN_SECONDS)) {
        startTime = 0;
        this._UserService.logout();
      }
    });
  })();
}
