/* @ngInject */
export default function route(
  $urlRouterProvider,
  $urlMatcherFactoryProvider,
  $stateProvider,
) {
  'ngInject';

  $urlRouterProvider.when('/', '/main');

  // Map routes with trailing slash to routes without trailing slash
  $urlMatcherFactoryProvider.strictMode(false);

  $stateProvider
    .state('main', {
      url: '/main',
      resolve: {
        /* @ngInject */
        currentUser: ($state, AppSettings, UserService) =>
          UserService.getCurrentUser()
            .then((user) => {
              if (user.isEORUserOnly) {
                $state.go(AppSettings.STATES.EOR_VESSEL_LIST);
              } else {
                $state.go(AppSettings.STATES.VESSEL_LIST);
              }
            }),
      },
    });
}
