/* eslint-disable quote-props */

/* @ngInject */
export default ($mdThemingProvider) => {
  const customPrimary = {
    '50': '#5291cf',
    '100': '#3e84c9',
    '200': '#3477ba',
    '300': '#2e6ba6',
    '400': '#295e92',
    '500': '#23517e',
    '600': '#1d446a',
    '700': '#183756',
    '800': '#122a42',
    '900': '#0d1e2e',
    'A100': '#669ed4',
    'A200': '#7aaada',
    'A400': '#8db7df',
    'A700': '#07111a',
    'contrastDefaultColor': 'light',
  };
  $mdThemingProvider
        .definePalette('customPrimary',
                        customPrimary);
  const customAccent = {
    '50': '#020406',
    '100': '#07111a',
    '200': '#0d1e2e',
    '300': '#122a42',
    '400': '#183756',
    '500': '#1d446a',
    '600': '#295e92',
    '700': '#2e6ba6',
    '800': '#3477ba',
    '900': '#3e84c9',
    'A100': '#295e92',
    'A200': '#23517e',
    'A400': '#1d446a',
    'A700': '#5291cf',
  };
  $mdThemingProvider
        .definePalette('customAccent',
                        customAccent);
  const customWarn = {
    '50': '#f98f66',
    '100': '#f87e4d',
    '200': '#f66c35',
    '300': '#f55a1d',
    '400': '#ee4b0a',
    '500': '#d64309',
    '600': '#bd3b08',
    '700': '#a53407',
    '800': '#8d2c06',
    '900': '#742405',
    'A100': '#faa17e',
    'A200': '#fbb397',
    'A400': '#fcc5af',
    'A700': '#5c1d04',
    'contrastDefaultColor': 'light',
  };
  $mdThemingProvider
        .definePalette('customWarn',
                        customWarn);
  const customBackground = {
    '50': '#ebebeb',
    '100': '#ffffff',
    '200': '#ffffff',
    '300': '#ffffff', // It has been used for calendar hilight
    '400': '#f6f7f9',
    '500': '#ebebeb',
    '600': '#d8dfe3',
    '700': '#c9d2d9',
    '800': '#bac6ce',
    '900': '#abb9c4',
    'A100': '#ffffff', // It has been used as calendar background color
    'A200': '#ffffff', // It has been used as calendar font color. It is #ffffff
    'A400': '#ffffff',
    'A700': '#9cadb9',
  };

  $mdThemingProvider
        .definePalette('customBackground',
                        customBackground);
  $mdThemingProvider.theme('default')
       .primaryPalette('customPrimary', {
         'hue-2': '500',
       })
       .accentPalette('customAccent')
       .warnPalette('customWarn')
       .backgroundPalette('customBackground');
};
