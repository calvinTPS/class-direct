/* eslint-disable */
const PROD_URL = 'https://classdirect.lr.org';
const TEST_URL = 'https://classdirect-test.marine.mast.ids';
const API_PATH = '/api/v1';

const resolveAPIUrl = () => {
  // Provide defaults if not set.
  if (typeof API_URL_PRESET === 'undefined') {
    if (typeof IS_MOBILE !== 'undefined' && IS_MOBILE) {
      // Please keep the following console log - we need to be informed of the endpoint
      const k = console; k.log(`${PROD_URL}${API_PATH}`);
      // Default to DEMO for convenience
      return PROD_URL + API_PATH;
    } else {
      return API_PATH;
    }
  }

  if (API_URL_PRESET.indexOf(API_PATH) >= 0) {
    return API_URL_PRESET;
  } else {
    return `${API_URL_PRESET}${API_PATH}`;
  }
}

const CONFIG = {
  API_URL: resolveAPIUrl(),
  SPRING_API_URL: '/api/v1-spring',
  DEBUG: false,
  USER_ID: '1b1e62ba-2923-46e0-90c2-23524904ce70',
  IDLE_IN_SECONDS: 3600,
  KEEPALIVE_INTERVAL_IN_SECONDS: 300,
  //- Update here to a developers IP if you want to use a specific backend
  CD_API: {
    hostname: 'localhost',
    port: 8080,
  },
  LR_LANDING_PAGE_URL: 'http://www.lr.org',
  CD_LANDING_PAGE_URL: 'https://www.classdirect.lr.org/',
  CD_LIVE_LANDING_PAGE_URL: 'https://www.cdlive.lr.org',
  MAST_API: {
    hostname: 'localhost',
    port: 8080,
    path: '/mast/api/v2',
    headers: {
      groups: 'admin',
      user: 'user',
      userId: 104,
    },
  },
  COOKIE_XSRF_TOKEN: 'XSRF-TOKEN',
  HEADER_XSRF_TOKEN: 'x-xsrf-token',
  GLOBAL_XSRF_TOKEN: '__ClassDirectXSRFToken',
  LOGIN_RELOAD_TIME: 1000 * 60 *
    10, // 10 minutes
  FALLBACK_API: {
    hostname: 'localhost',
    port: 8888,
  },
  GOOGLE_ANALYTICS_CODE: 'UA-100789132-1',
  AUTO_COMPLETE: {
    minimumCharacter: 1,
    delay: 500,
  },
  FILTER_OPTIONS: {
    OUTSIDE_IF_NOT_CLASSES: [
      'asset-type-card-container',
      'cd-dialog',
      'dark-background',
      'md-button',
      'md-calendar-date-selection-indicator',
      'md-datepicker-calendar',
      'md-dialog-container',
      'md-scroll-mask',
      'star-container',
    ],
  },
  API_ENDPOINTS: {
    AcceptTermsAndConditions: 'system/terms-conditions',
    ACCEPT_TERMS_AND_CONDITIONS: {
      Accept: 'accept',
      Latest: 'latest',
    },
    ACCOUNT: {
      ResetPassword: 'reset-password',
      EditProfile: 'edit-profile',
    },
    AdministrationUsers: 'administration/users',
    ASSET: {
      ActionableItems: 'actionable-items',
      AssetNotes: 'asset-notes',
      Cocs: 'cocs',
      Codicils: 'codicils',
      CodicilsQuery: 'codicils/query',
      Defect: 'defects',
      Defects: 'defects/query',
      Deficiencies: 'deficiencies',
      Details: 'details',
      Favourite: 'favourite',
      Findings: 'statutory-findings',
      MNCNs: 'mncns',
      MNCNAttachment: 'attachments',
      ScheduleRange: 'schedule-range',
      Services: 'services',
      ServicesDetailedQuery: 'services/query-detailed',
      StatutoryDeficiencies: 'deficiencies/query',
      StatutoryFindings: 'statutory-findings/query',
      SurveyReports: 'survey-reports',
      Tasks: 'tasks',
    },
    Asset: 'asset',
    Assets: 'assets',
    AssetClassStatuses: 'reference-data/asset/class-statuses',
    AssetTypes: 'reference-data/asset/asset-types',
    Certificates: 'certificates/query',
    CertificatesAttachment: 'certificates/attachment',
    Clients: 'clients/query',
    CountryFile: 'country-file/attachments',
    CurrentUser: 'current-user',
    CurrentUserFlags: 'current-user/flags',
    Customers: 'customers',
    DownloadFile: 'download-file',
    DownloadReport: 'cs10/download-file',
    Eor: 'eor',
    EXPORT: {
      ROOT: 'export',
      ACTIONABLE_ITEMS: 'ai-list',
      ASSET_NOTES: 'an-list',
      ASSETS: 'assets',
      ASSETS_ASYNC: 'assets-async',
      CHECKLIST: 'checklist',
      CONDITIONS_OF_CLASS: 'coc-list',
      DEFECTS: 'defect-list',
      ESP_REPORT: 'esp-report',
      MPMS: 'mpms',
      REPORTS: 'reports',
      STATUTORY_DEFICIENCIES: 'statutoryDeficiencies-list',
      STATUTORY_FINDINGS: 'statutoryFindings-list',
      TASKLIST: 'tasklist',
      USERS: 'users',
      MNCN: 'mncn-list',
    },
    FAVOURITES: 'assets/favourites',
    Flags: 'reference-data/flags',
    InitCSRF: 'init-csrf',
    JOBS: {
      ROOT: 'jobs',
      ATTACHMENT: 'attachment',
      ATTACHMENTS: 'attachments',
      EHS: 'executive-hull-summary/attachments',
      REPORTS: 'reports',
    },
    Notifications: 'notifications',
    NotificationTypes: 'notifications/types',
    Ping: 'ping',
    SERVICE: {
      Checklist: 'checklist/query',
      Tasklist: 'tasklist/query',
    },
    SECURITY: {
      ROOT: 'security',
      CREATE_MASQUERADE_TOKEN: 'create-masquerade-token',
    },
    Services: 'reference-data/service/service-catalogues',
    HasRole: 'has-role',
    Ports: 'ports-of-registry',
    RegisterBookSisters: 'register-book-sisters',
    SUBFLEET: {
      AccessibleAssetIds: 'subfleet/ids',
      AccessibleAssets: 'subfleet',
      RestrictedAssetIds: 'restricted-assets/ids',
      RestrictedAssets: 'restricted-assets',
      SaveSubfleet: 'subfleet-optimized',
    },
    SurveyReports: 'survey-reports',
    SurveyRequests: 'survey-requests',
    TechnicalSisters: 'technical-sisters',
    Users: 'users',
    USERS: {
      AccountStatuses: 'users/account-statuses',
      ExportSummary: 'users/export-summary',
      Query: 'users/query',
    },
  },
  FORMATTER: {
    FECHA_DATE_TIME: 'DD MMM YYYY, hh:mm:ss',
    LONG_DATE_ANGULAR: 'dd MMM yyyy',
    LONG_DATE_MOMENT: 'DD MMM YYYY',
    LONG_DATE_SERVER: 'YYYY-MM-DD',
    LONG_TIME_SERVER: 'HH:mm:ss',
    SHORT_DATE_ANGULAR: 'dd.MM.yyyy',
    TIME_PICKER_DEFAULT_RANGE: ':00|:10|:20|:30|:40|:50',
    TIME_TWENTYFOUR_HOURS: '/([01]+?[0-9]|2[0-3]):[0-5][0-9]$/',
  },
  DATES_TO_FORMAT: [
    'dueDate',
    'imposedDate',
    'assignedDate',
    'postponementDate',
    'lowerRangeDate',
    'upperRangeDate',
  ],
  DEFAULT_PARAMS: {
    DEBOUNCE: 500,
    INFINITE_SCROLL_OFFSET: 50,
    MAXIMUM_ASSETS_EXPORT: 256,
    MINIMUM_INPUT_CHARACTER: 3,
    TRUNCATED_DESCRIPTION_CHARACTER: 200,
    PAGINATION: {
      page: 1,
      size: 16,
      sort: 'id',
      order: 'ASC',
    },
    SISTER_VESSEL_PAGINATION: {
      page: 1,
      size: 16,
    },
    FLEET_SORT: {
      sort: 'AssetName',
      order: 'ASC',
    },
    FLEET: {
      UNFILTERED_QUERY: {
        isFavourite: false,
        codicilTypeId: [],
      },
      QUERY: {
        isFavourite: true,
        codicilTypeId: [],
      },
    },
  },
  CERTIFICATE_STATUS: {
    STATUSES: {
      ISSUED: 'Issued',
      WITHDRAWN: 'Withdrawn',
      EXPIRED: 'Expired',
      FINAL: 'Final',
      DRAFT: 'Draft',
    },
    ORDER: {
      Issued: 1,
      Withdrawn: 2,
      Expired: 3,
      Final: 4,
      Draft: 5,
    },
  },
  CLASS_STATUSES: {
    NOT_LR_CLASSED: 'Not LR Classed',
  },
  EVENTS: {
    ACTIVATE_STICKY: 'activateSticky',
    ADD_EXPORT_OPTIONS: 'addExportOptions',
    APP_INITIALIZED: 'appInitialized',
    ASSETS_EXPORT_DONE: 'assetsExportDone',
    ASSETS_EXPORT_INITIALIZE: 'assetsExportInitialize',
    ASSET_HEADER_DESTROYED: 'assetHeaderDestroyed',
    ASSET_HEADER_RENDERED: 'assetHeaderRendered',
    ASSET_HOME_ITEM_LOADED: 'assetHomeItemLoaded',
    BACK_TO_ASSET_HOME: 'backToAssetHomeClicked',
    DETAILED_SS_SHOWN: 'detailedServiceScheduleShown',
    DISABLE_ASSETS_SEARCH: 'disableAssetsSearch',
    EXPAND_ACCORDION: 'expandAccordion',
    EXPAND_OR_COLLAPSE_ALL_ACCORDIONS: 'expandOrCollapseAllAccordions',
    FLEET_ON_TOGGLE_VIEW: 'FleetonToggleView',
    FORCE_MODEL_UPDATE: 'forceUpdateValidity',
    IDLE_TIMEOUT: 'IdleTimeout',
    MPMS_TASK_STATE_CHANGE: 'mpmsTaskStateChange',
    ON_TOGGLE_FAVOURITE: 'onToggleFavourite',
    SCROLL_TO_BOTTOM_PAGE: 'scrollToBottomPage',
    SERVICE_SCHEDULE_DATES_UPDATED: 'serviceScheduleDatesUpdated',
    START_MASQUERADE: 'startMasquerade',
    SURVEY_SCHEDULE_ON_TOGGLE_VIEW: 'SurveyScheduleOnToggleView',
    TOGGLE_FAVOURITE: 'toggleFavourite',
    TOO_MANY_ASSETS_TO_EXPORT: 'tooManyAssetsToExport',
    UPDATE_DATE_PICKER: 'updateDatePicker',
    UPDATE_FAVOURITE: 'updateFavourite',
    VALIDATE_TASK_CREDITING: 'validateTaskCrediting',
  },
  ASSET_HOME_ITEMS: {
    CODICILS: 'codicils',
    SURVEY_REPORTS: 'surveyReports',
    CERTIFICATES: 'certificates',
    SERVICE_SCHEDULE: 'serviceSchedule',
  },
  OVERALL_STATUS_NAMES: {
    OVERDUE: 'Overdue',
    IMMINENT: 'Imminent',
    DUE_SOON: 'Due Soon',
    NOT_DUE: 'Not Due',
  },
  DUE_STATUS_ICON_MAP: {
    'due-soon': 'due',
    imminent: 'imminent',
    'not-due': 'tick',
    overdue: 'overdue',
    'overall-due-soon': 'overallDueSoon',
    'overall-imminent': 'overallImminent',
    'overall-not-due': 'overallNotDue',
    'overall-overdue': 'overallOverdue',
    complete: 'tick',
    finished: 'tick',
    'part-held': 'partHeld',
    postponed: 'overdue',
    'not-started': 'alert',
    issued: 'tick',
    expired: 'overdue',
    unknown: 'alert',
    withdrawn: 'alert',
    final: 'tick',
    draft: 'alert',
  },
  STATES: {
    ACCEPT_TERMS_AND_CONDITIONS: 'acceptTermsAndConditions',
    ACTIONABLE_ITEMS_LIST: 'asset.codicilsListing.actionableItems',
    ACTIONABLE_ITEMS_DETAILS: 'asset.actionableItemsDetail',
    ADD_USER: 'administration.addUser',
    ADMINISTRATION: 'administration.userList',
    ACCESSIBLE_AND_RESTRICTED_ASSETS: 'administration.accessibleAndRestrictedAssets',
    ASSET: 'asset',
    ASSET_HOME: 'asset.home',
    ASSET_DETAILS: 'asset.details',
    ASSET_NOTES_LIST: 'asset.codicilsListing.assetNotes',
    ASSET_NOTES_DETAILS: 'asset.assetNotesDetail',
    ASSET_SERVICE_DETAILS: 'asset.serviceDetails',
    ASSET_SERVICE_CHECKLIST: 'asset.serviceDetails.checklist',
    ASSET_SERVICE_TASKLIST: 'asset.serviceDetails.tasklist',
    ASSET_SURVEY_PLANNER: 'asset.surveyPlanner',
    ASSET_REGISTER_VESSELS: 'asset.registerBookSisters',
    ASSET_TECHNICAL_VESSELS: 'asset.technicalSisters',
    CERTIFICATES_LIST: 'asset.certificatesAndRecords',
    CODICILS_LISTING: 'asset.codicilsListing',
    CONDITIONS_OF_CLASS_LIST: 'asset.codicilsListing.conditionsOfClass',
    CONDITIONS_OF_CLASS_DETAILS: 'asset.conditionsOfClassDetail',
    DEFECTS_LIST: 'asset.codicilsListing.defects',
    DEFECTS_DETAILS: 'asset.defectsDetail',
    EDIT_USER: 'administration.editUser',
    EOR: 'exhibitionOfRecords',
    EOR_VESSEL_LIST: 'exhibitionOfRecords.vessel',
    ERROR: 'pageError',
    FLEET: 'fleet',
    MAIN: 'main',
    MNCNS_LIST: 'asset.codicilsListing.mncns',
    MNCNS_DETAILS: 'asset.mncnsDetail',
    MPMS: 'mpms',
    REQUEST_SUBMISSION_CONFIRMATION: 'asset.requestSubmissionConfirmation',
    SERVICE_HISTORY_JOB_DETAILS: 'asset.jobDetails',
    SERVICE_HISTORY_JOBS_LIST: 'asset.jobsList',
    SERVICE_SCHEDULE: 'fleet.schedule',
    STATUTORY_DEFICIENCIES_DETAILS: 'asset.statutoryDeficienciesDetail',
    STATUTORY_DEFICIENCIES_LIST: 'asset.codicilsListing.statutoryDeficiencies',
    STATUTORY_FINDINGS_DETAILS: 'asset.statutoryFindingsDetail',
    STATUTORY_FINDINGS_LIST: 'asset.codicilsListing.statutoryFindings',
    SURVEY_REQUEST: 'asset.surveyRequest',
    USER: 'user',
    USER_DETAILS: 'administration.userDetails',
    VESSEL_LIST: 'fleet.vessel',
  },
  PREVIOUS_STATES: {
    'asset.home': {
      allowedStates: ['fleet.vessel', 'fleet.schedule', 'asset.registerBookSisters', 'asset.technicalSisters', 'exhibitionOfRecords.vessel'],
      useHistoryFirst: true,
      defaultPreviousState: 'fleet.vessel',
    },
    'asset.details': {
      disallowedStates: ['asset.registerBookSisters', 'asset.technicalSisters'],
      useHistoryFirst: true,
      defaultPreviousState: 'asset.home',
    },
    'fleet.schedule': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.home',
    },
    'asset.surveyRequest': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.home',
    },
    'asset.serviceDetails': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.surveyPlanner',
    },
    'asset.serviceDetails.checklist': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.surveyPlanner',
    },
    'asset.serviceDetails.tasklist': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.surveyPlanner',
    },
    'asset.jobDetails': {
      defaultPreviousState: 'asset.jobsList',
    },
    'asset.registerBookSisters': {
      defaultPreviousState: 'asset.details',
    },
    'asset.technicalSisters': {
      defaultPreviousState: 'asset.details',
    },
    'asset.actionableItemsDetail': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.codicilsListing.actionableItems',
    },
    'asset.assetNotesDetail': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.codicilsListing.assetNotes',
    },
    'asset.conditionsOfClassDetail': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.codicilsListing.conditionsOfClass',
    },
    'asset.defectsDetail': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.codicilsListing.defects',
    },
    'asset.statutoryDeficienciesDetail': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.codicilsListing.statutoryDeficiencies',
    },
    'asset.statutoryFindingsDetail': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.codicilsListing.statutoryFindings',
    },
    'asset.mncnsDetail': {
      useHistoryFirst: true,
      defaultPreviousState: 'asset.codicilsListing.mncns',
    },
  },
  ROLES: {
    LR_ADMIN: 'LR_ADMIN',
    LR_INTERNAL: 'LR_INTERNAL',
    LR_SUPPORT: 'LR_SUPPORT',
    CLIENT: 'CLIENT',
    EOR: 'EOR',
    EQUASIS: 'EQUASIS',
    EXTERNAL_NON_CLIENT: 'EXTERNAL_NON_CLIENT',
    SHIP_BUILDER: 'SHIP_BUILDER',
    THETIS: 'THETIS',
    FLAG: 'FLAG',
  },
  ASSET_CODE_LR: 'LRV',
  ASSET_CODE_NON_LR: 'IHS',
  ASSET_TYPES_DISPLAYED_LEVEL_INDICATION: 3,
  ASSET_TYPES_LEAF_LEVEL_INDICATION: 5,
  ACCOUNT_STATUS: {
    ACTIVE: 'Active',
    ARCHIVED: 'Archived',
    DISABLED: 'Disabled',
  },
  ASSET_TYPES: {
    CATEGORY_LEVEL_INDICATION: 1,
    DISPLAYED_LEVEL_INDICATION: 3,
    LEAF_LEVEL_INDICATION: 5,
  },
  ACCOUNT_STATUS_ORDER: { Active: 1, Disabled: 2, Archived: 3 },
  ADMIN_ROLES: ['LR_ADMIN', 'LR_SUPPORT'],
  ASSETS_SEARCH_CATEGORIES: [
    { name: 'cd-asset-name-or-imo-number', query: 'search', type: 'string', wildcard: true },
    { name: 'cd-former-asset-name', query: 'formerAssetName', type: 'string', wildcard: true },
    { name: 'cd-client', query: 'clientName', type: 'string', wildcard: true },
    { name: 'cd-ship-builder', query: 'builder', type: 'string', wildcard: true },
    { name: 'cd-yard-number', query: 'yardNumber', type: 'string', wildcard: true },
  ],
  ASSETS_SORT_BY_CATEGORIES: [
    { name: 'cd-asset-name', sort: 'AssetName', order: 'ASC' },
    { name: 'cd-asset-class-status', sort: 'ClassStatusId', order: 'ASC' },
    { name: 'cd-asset-type', sort: 'AssetTypeName', order: 'ASC' },
    { name: 'cd-date-of-build', sort: 'BuildDate', order: 'ASC' },
    { name: 'cd-yard-number', sort: 'YardNumber', order: 'ASC' },
    { name: 'cd-flag', sort: 'FlagName', order: 'ASC' },
  ],
  CODICILS_FILTER_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-impose-date',
      key: 'imposedDateEpoch',
    },
    {
      type: 'daterange',
      title: 'cd-due-date',
      key: 'dueDateEpoch',
    },
  ],
  CERTIFICATES_FILTER_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-issued-on',
      key: 'issueDate',
      disallowFutureDates: true,
    },
    {
      type: 'daterange',
      title: 'cd-expires-on',
      key: 'expiryDate',
    },
    {
      type: 'checkboxes',
      title: 'cd-certificate-status',
      key: 'status',
      isAll: true,
    },
  ],
  CERTIFICATES_SEARCH_BY_LIST: [
    { name: 'cd-certificate-name', value: 'certificateName', order: 'ASC', selected: true },
    { name: 'cd-certificate-number', value: 'certificateNumber', order: 'DESC', selected: false },
    { name: 'cd-form-number', value: 'formNumber', order: 'ASC', selected: false },
    { name: 'cd-version', value: 'version', order: 'ASC', selected: false },
    { name: 'cd-office', value: 'office', order: 'ASC', selected: false },
  ],
  CERTIFICATES_SORT_BY_LIST: [
    { name: 'cd-certificate-name', value: 'certificateName', order: 'ASC', selected: false },
    { name: 'cd-certificate-number', value: 'certificateNumber', order: 'DESC', selected: false },
    { name: 'cd-form-number', value: 'formNumber', order: 'ASC', selected: false },
    { name: 'cd-version', value: 'version', order: 'ASC', selected: false },
    { name: 'cd-office', value: 'office', order: 'ASC', selected: false },
    { name: 'cd-certificate-status', customSort: 'sortState', selected: false },
    { name: 'cd-issued-date', value: 'issueDate', order: 'DESC', selected: false },
    { name: 'cd-extended-date', value: 'extendedDate', order: 'DESC', selected: false },
    { name: 'cd-expiry-date', value: 'expiryDate', order: 'ASC', selected: true },
    { name: 'cd-issuing-surveyor', value: 'issuingSurveyor', order: 'ASC', selected: false },
  ],
  CERTIFICATES_ASSET_HUB_QUERY_OPTIONS: {
    size: 4,
    sort: 'issueDate',
    order: 'DESC',
  },
  DEFECTS_FILTER_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-date-occurred',
      key: 'dateOccurredEpoch',
      disallowFutureDates: true,
    },
  ],
  CHECKLIST_SEARCH_BY_LIST: [
    { name: 'cd-name', value: 'name', selected: true },
  ],
  EHS_SEARCH_BY_LIST: [
    { name: 'cd-name', value: 'name', selected: true },
  ],
  EHS_FILTER_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-date-added',
      key: 'date',
    },
  ],
  HISTORICAL_DOCUMENTATION_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-modification-range',
      key: 'lastModified',
    },
  ],
  JOBS_SEARCH_BY_LIST: [
    { name: 'cd-surveys', value: 'services.name', selected: true },
    { name: 'cd-job-number', value: 'jobNumber', selected: false },
    { name: 'cd-lead-surveyor', value: 'leadSurveyorName', selected: false },
    { name: 'cd-location', value: 'location', selected: false },
  ],
  JOBS_SORT_BY_LIST: [
    { name: 'cd-job-number', value: 'jobNumber', selected: false, order: 'DESC' },
    { name: 'cd-last-visit-date', value: 'lastVisitDate', selected: true, order: 'DESC' },
  ],
  JOBS_FILTER_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-last-visit-date',
      key: 'lastVisitDate',
      disallowFutureDates: true,
    },
  ],
  MNCNS_FILTER_OPTIONS: [
    {
      type: 'checkboxes',
      title: 'cd-status',
      key: 'status',
      options: [
        { name: "Open", value: "Open", selected: true },
        { name: "Open - Downgraded", value: "Open - Downgraded", selected: true },
        { name: "Closed", value: "Closed", selected: false },
      ],
    },
    {
      type: 'daterange',
      title: 'cd-due-date',
      key: 'dueDateEpoch',
    },
  ],
  NOTES_AND_ACTIONS_SEARCH_PARAM: {
    ASSET_NOTES_SEARCH_BY_LIST: [
      { name: 'cd-asset-note-title', value: 'title', selected: true },
      { name: 'cd-item', value: 'items.name', selected: false },
    ],
    ACTIONABLE_ITEM_SEARCH_BY_LIST: [
      { name: 'cd-ai-title', value: 'title', selected: true },
      { name: 'cd-item', value: 'items.name', selected: false },
    ],
    COC_SEARCH_BY_LIST: [
      { name: 'cd-coc-title', value: 'title', selected: true },
      { name: 'cd-item', value: 'items.name', selected: false },
    ],
    DEFECTS_SEARCH_BY_LIST: [
      { name: 'cd-defect-title', value: 'title', selected: true },
      { name: 'cd-item', value: 'items.name', selected: false },
    ],
    MNCNS_SEARCH_BY_LIST: [
      { name: 'cd-ism-clause', value: 'ismClause', selected: true },
      { name: 'cd-area-under-review', value: 'areaUnderReview', selected: false },
    ],
    STATUTORY_DEFICIENCIES_SEARCH_BY_LIST: [
      { name: 'cd-sd-title', value: 'title', selected: true },
      { name: 'cd-item', value: 'items.name', selected: false },
    ],
    STATUTORY_FINDINGS_SEARCH: [
      { name: 'cd-name', value: 'title', selected: true, placeholder: 'cd-type-to-search-by-name' },
    ],
  },
  STATUTORY_DEFICIENCIES_FILTER_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-date-occurred',
      key: 'incidentDate',
    },
  ],
  STATUTORY_DEFICIENCIES_SEARCH: [
    { name: 'cd-name', value: 'title', selected: true },
    { name: 'cd-item', value: 'items.name', selected: false },
  ],
  STATUTORY_DEFICIENCIES_SORT: [
    { name: 'cd-date-occurred', value: 'incidentDate', selected: true, order: 'DESC' },
    { name: 'cd-title', value: 'title', selected: false, order: 'ASC' },
  ],
  STATUTORY_FINDINGS_FILTER_OPTIONS: [
    {
      type: 'daterange',
      title: 'cd-impose-date',
      key: 'imposedDate',
    },
    {
      type: 'daterange',
      title: 'cd-due-date',
      key: 'dueDate',
    },
  ],
  STATUTORY_FINDINGS_SORT: [
    { name: 'cd-due-date', value: 'dueDate', selected: true, order: 'ASC' },
    { name: 'cd-title', value: 'title', selected: false, order: 'ASC' },
  ],
  USERS_SEARCH_BY_LIST: [
    { name: 'cd-user-email-address', value: 'email', selected: true },
    { name: 'cd-user-name', value: 'name', selected: false },
    { name: 'cd-company-name', value: 'companyName', selected: false },
    { name: 'cd-client-code', value: 'clientCode', tag: 'CLIENT', selected: false },
    { name: 'cd-ship-builder-code', value: 'shipBuilderCode', tag: 'SHIP_BUILDER', selected: false },
    { name: 'cd-flag-code', value: 'flagCode', tag: 'FLAG', selected: false },
  ],
  USERS_SORT_BY_LIST: [
    { name: 'cd-user-email-ascending', value: 'Email', selected: true, order: 'ASC' },
    { name: 'cd-user-email-descending', value: 'Email', selected: false, order: 'DESC' },
  ],
  USERS_FILTER_OPTIONS: [
    {
      type: 'checkboxes',
      title: 'cd-account-type',
      key: 'roles',
      isAll: true,
    }, {
      type: 'daterange',
      title: 'cd-last-login-date',
      key: 'lastLoginDate',
      disallowFutureDates: true,
    }, {
      type: 'checkboxes',
      title: 'cd-account-status',
      key: 'status',
      options: [{
        name: 'cd-active',
        value: 'Active',
        selected: true,
      }, {
        name: 'cd-disabled',
        value: 'Disabled',
        selected: true,
      }, {
        name: 'cd-archived',
        value: 'Archived',
        selected: false,
      }],
      isAll: false,
    },
  ],
  TASK: {
    CREDIT_STATUSES: {
      NOT_CREDITED: 'Not Credited',
      SATISFACTORY: 'Satisfactory',
      REQ_REPAIR: 'Req. Repair',
      REQ_RENEW: 'Req. Renew',
    },
    SEARCH_BY_LIST: [
      { name: 'cd-task-name', value: 'name', placeholder: 'Type here to search by Task Name', selected: true },
    ],
    FILTER_OPTIONS: [
      {
        type: 'checkboxes',
        title: 'cd-credit-status',
        key: 'creditStatus',
        isAll: true,
      }, {
        type: 'daterange',
        title: 'cd-due-date',
        key: 'dueDate',
      },
    ],
  },
  FLAGS_SEARCH_BY_LIST: [
    { name: 'cd-flag-name-or-code', value: 'nameCode', selected: true },
  ],
  CODICILS_META: {
    COC: {
      ID: 'COC',
      NAME: 'cd-condition-of-class',
    },
    AI: {
      ID: 'AI',
      NAME: 'cd-actionable-item',
    },
    AN: {
      ID: 'AN',
      NAME: 'cd-asset-note',
    },
    SF: {
      ID: 'SF',
      NAME: 'cd-statutory-finding',
    },
    MNCN: {
      ID: 'MNCN',
      NAME: 'cd-mncns',
    },
  },
  DEFECTS_META: {
    DEFECT: {
      ID: 'DEFECT',
      NAME: 'cd-class-defect',
    },
    SD: {
      ID: 'SD',
      NAME: 'cd-statutory-deficiency',
    },
    REPAIR: {
      ID: 'REPAIR',
      NAME: 'cd-repair',
    },
    RECTIFICATION: {
      ID: 'RECTIFICATION',
      NAME: 'cd-rectification',
    },
  },
  JOB: {
    JOB_NUMBER_LENGTH: 9,
    TOP_SERVICES_TO_SHOW: 3,
  },
  REPORT_TYPES: {
    ESP: 'ESP',
    FAR: 'FAR',
    FSR: 'FSR',
    MNCN: 'MNCN',
    TM: 'TM',
    MMS: 'MMS'
  },
  ATTACHMENTS: {
    EHS: {
      Name: 'Executive Hull Summary',
      ShortName: 'EHS',
    },
    TM: {
      ArgonautCategory: 2,
      ArgonautName: 'Thickness Measurement Report - Argonaut',
      LongPDFName: 'Thickness Measurement Report - Long',
      Name: 'Thickness Measurement Report',
      PDFReportCategory: 3,
      ShortName: 'TM',
      ShortPDFName: 'Thickness Measurement Report - Short',
      VerificationReportCategory: 4,
    },
    MNCN: {
      Name: 'Major Non Conformity Note Report',
      ShortName: 'MNCN',
    },
    MMS: {
      JobNoteCategory: 15,
      JobNoteName: 'MMS Job Note',
      Name: 'Marine Management Systems',
      ReportCategory: 14,
      ReportName: 'MMS Report',
      ShortName: 'MMS',
    },
  },
  CODICILS_STATUSES: {
    CANCELLED: 'Cancelled',
    CHANGE_RECOMMENDED: 'Change Recommended',
    CLOSED: 'Closed',
    DELETED: 'Deleted',
    DRAFT: 'Draft',
    OPEN: 'Open',
    OPEN_DOWNGRADED: 'Open - Downgraded',
  },
  ASSET_DUE_STATUS_OPEN_IDS: {
    OPEN_SERVICE_IDS: [1, 2],
    OPEN_AI_IDS: [3, 4, 6],
    OPEN_COC_IDS: [14],
    OPEN_TASK_IDS: [4, 6],
    OPEN_STATUTORY_FINDING_IDS: [23],
    OPEN_CODICILS:[3, 4, 14, 23],
  },
  UNKNOWN_CREDIT_STATUSES: ['UNKNOWN_SERVICE_CREDIT_STATUS', 'UNKNOWN_MIGRATED_SERVICE_CREDIT_STATUS'],
  PERMISSION_TYPES: {
    CLIENT: 'CLIENT',
    EOR: 'EOR',
    SHIP_BUILDER: 'SHIP_BUILDER',
    FLAG: 'FLAG',
    EXTERNAL_NON_CLIENT: 'EXTERNAL_NON_CLIENT',
    LR_ADMIN: 'LR_ADMIN',
    LR_INTERNAL: 'LR_INTERNAL',
  },
  USER_ACCOUNT_TYPES: {
    CLIENT: {
      label: 'cd-role-client',
      description: 'cd-account-type-client-description',
    },
    EOR: {
      label: 'cd-role-eor',
      description: 'cd-account-type-eor-description',
    },
    SHIP_BUILDER: {
      label: 'cd-role-ship-builder',
      description: 'cd-account-type-ship-builder-description',
    },
    FLAG: {
      label: 'cd-role-flag',
      description: 'cd-account-type-flag-description',
    },
  },
  LR_ACCOUNT_TYPES: {
    LR_ADMIN: {
      label: 'cd-role-lr-admin',
      description: 'cd-account-type-administrator',
      'user-creation-label': 'cd-administrator',
    },
    LR_INTERNAL: {
      label: 'cd-role-lr-internal',
      description: 'cd-account-type-non-administrator',
      'user-creation-label': 'cd-non-administrator',
    },
    LR_SUPPORT: {
      label: 'cd-role-lr-support',
      description: 'cd-account-type-support-description',
    },
  },
  LR_EMAIL_DOMAIN: 'lr.org',
  EDITABLE_USER_PROPERTIES: ['status', 'userAccExpiryDate', 'clientCode', 'shipBuilderCode', 'flagCode'],
  DELETABLE_USER_FIELDS: {
    CLIENT: 'clientCode',
    FLAG: 'flagCode',
    SHIP_BUILDER: 'shipBuilderCode',
  },
  MASQUERADE_HEADER_NAME: 'x-masq-id',
  USER_CREATION_JOURNEY: {
    CLIENT: [
      { key: 'SEARCH_CLIENT', title: 'cd-select-a-client', component: 'search-client' },
      { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
      { key: 'CONFIRMATION', component: 'confirmation' },
    ],
    EOR: [
      { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
      { key: 'CONFIRMATION', component: 'confirmation' },
    ],
    SHIP_BUILDER: [
      { key: 'SEARCH_SHIP_BUILDER', title: 'cd-select-a-ship-builder', component: 'search-ship-builder' },
      { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
      { key: 'CONFIRMATION', component: 'confirmation' },
    ],
    FLAG: [
      { key: 'SEARCH_FLAG', title: 'cd-select-a-flag', component: 'search-flag' },
      { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
      { key: 'CONFIRMATION', component: 'confirmation' },
    ],
    EXTERNAL_NON_CLIENT: [{}],
    LR_ADMIN: [
      { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
      { key: 'CONFIRMATION', component: 'confirmation' },
    ],
    LR_INTERNAL: [
      { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
      { key: 'CONFIRMATION', component: 'confirmation' },
    ],
  },
  SHOW_EOR_SECTION_ROLES: ['CLIENT', 'EOR', 'SHIP_BUILDER'],
  WORK_ITEM_TYPES: { 1: 'TASKLIST', 2: 'CHECKLIST' },
  ROLE_STATE_RESTRICTIONS: {
    EOR: [
      'asset.registerBookSisters',
      'asset.surveyRequest',
      'asset.technicalSisters',
      'fleet.vessel',
      'nationalAdministration',
    ],
    EQUASIS: [
      'administration.addUser',
      'administration.editUser',
      'administration.userDetails',
      'administration.userList',
      'asset.codicilsListing.defects',
      'asset.executiveHullSummaries',
      'asset.jobDetails',
      'asset.jobsList',
      'asset.registerBookSisters',
      'asset.surveyRequest',
      'asset.technicalSisters',
      'fleet.schedule',
      'fleet.vessel',
      'nationalAdministration',
    ],
    FLAG: [
      'administration.addUser',
      'administration.editUser',
      'administration.userDetails',
      'administration.userList',
      'asset.surveyRequest',
    ],
    LR_ADMIN: [],
    LR_INTERNAL: [
      'administration.addUser',
      'administration.editUser',
      'administration.userDetails',
      'administration.userList',
    ],
    LR_SUPPORT: [],
    SHIP_BUILDER: [
      'administration.addUser',
      'administration.editUser',
      'administration.userDetails',
      'administration.userList',
    ],
    THETIS: [
      'administration.addUser',
      'administration.editUser',
      'administration.userDetails',
      'administration.userList',
      'asset.codicilsListing.defects',
      'asset.executiveHullSummaries',
      'asset.jobDetails',
      'asset.jobsList',
      'asset.registerBookSisters',
      'asset.surveyRequest',
      'asset.technicalSisters',
      'fleet.schedule',
      'fleet.vessel',
      'nationalAdministration',
    ],
  },
  RBAC_CONFIG: {
    EDIT_PROFILE_PASSWORD: {
      type: 'BLACKLIST',
      roles: ['LR_ADMIN', 'LR_INTERNAL'],
    },
    FAVOURITE: {
      type: 'BLACKLIST',
      roles: ['EOR', 'EQUASIS', 'THETIS'],
    },
    MPMS: {
      type: 'WHITELIST',
      roles: ['CLIENT', 'LR_ADMIN', 'LR_INTERNAL', 'LR_SUPPORT'],
    },
    MNCN: {
      type: 'WHITELIST',
      roles: ['FLAG', 'CLIENT', 'LR_ADMIN', 'LR_INTERNAL', 'LR_SUPPORT'],
    },
    NATIONAL_ADMINISTRATION: {
      type: 'BLACKLIST',
      roles: ['EOR', 'EQUASIS', 'THETIS'],
    },
    NOTIFICATIONS: {
      type: 'WHITELIST',
      roles: ['FLAG', 'SHIP_BUILDER', 'LR_ADMIN', 'LR_SUPPORT', 'CLIENT'],
    },
    SISTERS: {
      type: 'BLACKLIST',
      roles: ['EOR', 'EQUASIS', 'THETIS'],
    },
    SURVEY_REQUEST: {
      type: 'WHITELIST',
      roles: ['LR_ADMIN', 'LR_INTERNAL', 'LR_SUPPORT', 'SHIP_BUILDER', 'CLIENT'],
    },
  },
  EXPORT_TYPES: {
    ASSETS: 'assets',
  },
  SERVICES: {
    DATA_NOT_TO_HYDRATE: [
      '_id',
      'dueDate',
      'dueDateEpoch',
      'dueStatus',
      'dueStatusH',
      'lowerRangeDate',
      'lowerRangeDateEpoch',
      'model',
      'postponementDate',
      'postponementDateEpoch',
      'repeatOf',
      'upperRangeDate',
      'upperRangeDateEpoch',
      'creditStatus',
    ],
  },
  SYSTEM_HEADER_LINKS: {
    DEFAULT: [
      {
        name: 'cd-lr-ships-in-class-portal',
        url: 'http://www.lrshipsinclass.lrfairplay.com/default.aspx?reason=denied_empty&script_name=/default.aspx&path_info=/default.aspx',
        icon: 'link',
      },
      {
        name: 'cd-ruleoutlook-live',
        url: 'https://www.cdlive.lr.org/homepages/Ruleoutlook.html',
        icon: 'link',
      },
      {
        name: 'cd-rulefinder',
        url: 'http://www.lr.org/en/services/software/rulefinder.aspx',
        icon: 'link',
      },
      {
        name: 'Q88',
        url: 'https://www.q88.com',
        icon: 'link',
      },
      {
        name: 'Equasis',
        url: 'http://www.equasis.org',
        icon: 'link',
      },
      {
        name: 'Thetis',
        url: 'https://portal.emsa.europa.eu/web/thetis',
        icon: 'link',
      },
      {
        name: 'IHS',
        url: 'http://maritime.ihs.com',
        icon: 'link',
      },
      {
        name: 'Support',
        url: 'https://lrorg-stg.lr-cloud.info/en/marine/class-direct/support.aspx',
        icon: 'link',
      },
    ],
    EQUASIS: [
      {
        name: 'cd-lr-homepage',
        url: 'https://www.cdlive.lr.org',
        icon: 'link',
      },
    ],
    THETIS: [
      {
        name: 'cd-lr-homepage',
        url: 'https://www.cdlive.lr.org',
        icon: 'link',
      },
    ],
  },
  EQUASIS_HOME_URL: 'http://www.equasis.org',
  THETHIS_HOME_URL: 'https://portal.emsa.europa.eu/web/thetis',
  ERROR_CODES: {
    UNAUTHORIZED: [401, 403],
    UNDER_MAINTENANCE: [503],
    SERVER: [500, 502, 504],
  },
  HTTP_STATUS: {
    ACCEPTED: 202,
    OK: 200,
  },
  EXPORTABLE_ITEMS: {
    ACTIONABLE_ITEMS: 'ACTIONABLE_ITEMS',
    ASSET_NOTES: 'ASSET_NOTES',
    CHECKLIST: 'CHECKLIST',
    CONDITIONS_OF_CLASS: 'CONDITIONS_OF_CLASS',
    DEFECTS: 'DEFECTS',
    MNCN: 'MNCN',
    MPMS: 'MPMS',
    STATUTORY_DEFICIENCIES: 'STATUTORY_DEFICIENCIES',
    STATUTORY_FINDINGS: 'STATUTORY_FINDINGS',
    TASKLIST: 'TASKLIST',
  },
  CMRO: {
    LINKS: {
      INCIDENT_SUMMARY: {
        NAME: 'cd-incident-summary',
        URL: '/cmro/IncidentSummary',
      },
      HULLD_EFECTS: {
        NAME: 'cd-hull-defects',
        URL: '/cmro/HullDefects',
      },
      MACHINERY_DEFECTS: {
        NAME: 'cd-machinery-defects',
        URL: '/cmro/MachineryDefects',
      },
      FIRES_AND_EXPLOSIONS: {
        NAME: 'cd-fire-and-explosions',
        URL: '/cmro/FiresAndExplosions',
      },
      FIRES_AND_EXPLOSIONS_SUMMARY: {
        NAME: 'cd-fire-and-explosions-summary',
        URL: '/cmro/FiresAndExplosionsSummary',
      },
      BASIC_DETAILS: {
        NAME: 'cd-more-details',
        URL: '/cmro/BasicDetails',
      },
      CLASS_HISTORY: {
        NAME: 'cd-class-history',
        URL: '/cmro/ClassHistory',
      },
      VESSEL_SUMMARY_CLASS_NOTATION: {
        NAME: 'cd-asset-notation-summary',
        URL: '/cmro/VesselSummaryClassNotation',
      },
    },
  },
  SERVICE_SCHEDULE: {
    VIEW_TYPES: {
      TABULAR: 'tabular',
      GANTT: 'gantt',
    },
    LOCATIONS: {
      ASSET_SERVICE_SCHEDULE: 'assetServiceSchedule',
      SERVICE_SCHEDULE_LISTING: 'serviceScheduleListing'
    }
  },
  CONVERT_TO_FRUTIGER: ["|", "¦", "¬"],
  FIELDS_TO_CONVERT_TO_FRUTIGER: [
    'rulesetDetailsDto.machineryNotation',
    'rulesetDetailsDto.hullNotation',
  ],
  // - Each key value here must be unique
  // - Tempalates are stored at class-direct\fe\src\app\common\riders\templates
  RIDER_TEMPLATES: {
    ACTIONABLE_ITEM: 'item_actionable_item',
    ASSET_NOTE: 'item_asset_note',
    ASSET_SERVICE_SCHEDULE: 'item_asset_service_schedule',
    ATTACHMENT: 'item_attachment',
    CERTIFICATE_AND_RECORD: 'item_certificate_and_record',
    CHECKLIST: 'accordion_checklist',
    COUNTRY_FILE: 'item_country_file',
    CONDITION_OF_CLASS: 'item_conditions_of_class',
    DEFAULT: 'item_default_rider',
    DEFECT: 'item_defect',
    EXECUTIVE_HULL_SUMMARY: 'item_execuitve_hull_summary',
    FLAG: 'item_flag',
    JOB: 'item_job',
    JOB_DETAILS: 'item_job_service',
    MNCN: 'item_mncn',
    MPMS: 'accordion_mpms',
    NATIONAL_ADMINISTRATION: 'item_country_file_one_time_binding',
    RECTIFICATION: 'item_rectification',
    REPAIR: 'item_repair',
    SCHEDULE: 'accordion_schedule',
    CLIENT: 'item_client',
    SHIP_BUILDER: 'item_ship_builder',
    STATUTORY_DEFICIENCY: 'item_statutory_deficiency',
    STATUTORY_FINDING: 'item_statutory_finding',
    TASKLIST: 'accordion_tasklist',
    USER: 'item_viewable_user',
  },
  VIEW_TYPES: {
    TABLE: 'table',
    CARD: 'card',
  },
  PRODUCT_FAMILY_TYPES: {
    CLASSIFICATION : {
      ID : 1,
      NAME:'Classification'
    },
    MMS : {
      ID : 2,
      NAME:'MMS'
    },
    STATUTORY : {
      ID : 3,
      NAME:'Statutory'
    },
  },
  // - Each key value here must be unique,
  TABLE_HEADER_SORT_AND_IDS: {
    ASSET_NAME: 'AssetName',
    ASSET_TYPE_CODE: 'AssetTypeName',
    CLASS_STATUS: 'ClassStatusId',
    CLASS_SOCIETY: 'ClassSociety',
    DEAD_WEIGHT: 'DeadWeight',
    FLAG_CODE: 'FlagCode',
    GROSS_TONNAGE: 'GrossTonnage',
    IMO_NUMBER: 'IMONumber',
    YEAR_OF_BUILD: 'BuildDate',
  },
  NOTIFICATION_OPTIONS: [
    {
      ID : 1,
      NAME : 'FAVOURITES',
      LABEL : 'Favourites',
      VALUE : 1,
    },
    {
      ID : 2,
      NAME : 'SUBFLEET',
      LABEL : 'Subfleet',
      VALUE : 0,
    }
  ],
};

// Add other properties dynamically
Object.assign(CONFIG, {
  BASE_URL: CONFIG.API_URL.replace(API_PATH, ''),
});

module.exports = CONFIG;
