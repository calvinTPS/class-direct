import './page-error.scss';
import controller from './page-error.controller';
import template from './page-error.pug';

export default {
  bindings: {
    params: '<?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
