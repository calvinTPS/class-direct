import angular from 'angular';
import PageErrorComponent from './page-error.component';
import routes from './page-error.routes';
import uiRouter from 'angular-ui-router';

export default angular.module('pageError', [
  uiRouter,
])
.config(routes)
.component('pageError', PageErrorComponent)
.name;
