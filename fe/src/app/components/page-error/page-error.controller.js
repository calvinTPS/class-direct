import * as _ from 'lodash';
import Base from 'app/base';

export default class PageErrorController extends Base {
  /* @ngInject */
  constructor(
    $window,
  ) {
    super(arguments);
  }

  $onInit() {
    this.statusCode = _.get(this.params, 'statusCode');
    this.errorMessage = _.get(this.params, 'errorMessage');
  }

  get cardTitle() {
    if (this._cardTitle == null) {
      this._cardTitle = `cd-error-page-card-title-${this.statusCode}`;
    }
    return this._cardTitle;
  }

  get cardDescription() {
    if (this._cardDescription == null) {
      if (_.isEmpty(this.errorMessage)) {
        this._cardDescription = `cd-error-page-card-description-${this.statusCode}`;
      } else {
        this._cardDescription = this.errorMessage;
      }
    }

    return this._cardDescription;
  }

  goToRoot() {
    this._$window.location.href = '/';
  }

  goBack() {
    this._$window.history.back();
  }

  reload() {
    this._$window.location.reload(true);
  }
}
