/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import PageErrorComponent from './page-error.component';
import PageErrorController from './page-error.controller';
import PageErrorModule from './';
import PageErrorTemplate from './page-error.pug';

describe('PageError', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(PageErrorModule));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('pageError', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('should check if error message before displaying description', () => {
      const controller = makeController({ params: {
        statusCode: 401,
        errorMessage: 'test',
      } });
      expect(controller.cardDescription).toEqual('test');

      // with no description
      const controller2 = makeController({ params: {
        statusCode: 401,
      } });
      expect(controller2.cardDescription).toEqual('cd-error-page-card-description-401');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = PageErrorComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(PageErrorTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(PageErrorController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<page-error foo="bar"><page-error/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('PageError');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
