import AppSettings from 'app/config/project-variables.js';

/* @ngInject */
export default function config($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('pageError', {
      component: 'pageError',
      params: {
        statusCode: 404,
        errorMessage: '',
      }, // Had to pass default param
      resolve: {
        /* @ngInject */
        params: $state => $state.params, // Async process, so had to pass object
      },
    });

  $urlRouterProvider.otherwise(($injector) => {
    // Only redirect wrong URL to 404 on browsers, not mobile app.
    // URL not editable in mobile app, so this is acceptable.
    // Fixes 404 page showing upfront when app loads.
    // eslint-disable-next-line
    if (!window.cordova) {
      const state = $injector.get('$state');
      state.go(AppSettings.STATES.ERROR, { statusCode: 404 }, {
        location: false,
      });
    }
  });
}
