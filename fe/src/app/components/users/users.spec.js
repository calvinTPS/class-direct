/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import UsersComponent from './users.component';
import UsersController from './users.controller';
import UsersModule from './';
import UsersTemplate from './users.pug';

describe('Users', () => {
  let $compile;
  let $rootScope;
  let makeController;
  let mockUsers;

  beforeEach(window.module(UsersModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    mockUsers = [
      { userId: '0', email: 'user0@test.io', selected: true },
      { userId: '1', email: 'user1@test.io', selected: false },
      { userId: '2', email: 'user2@test.io', selected: true },
      { userId: '3', email: 'user3@test.io', selected: false },
    ];
    mockUsers.pagination = { totalElements: 10 };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('users', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('includeAll getter/setter should return with the right values and set all user.selected = true', () => {
      const controller = makeController({ users: mockUsers });
      controller.includeAll = true;
      _.forEach(controller.users, (user) => {
        expect(user.selected).toEqual(true);
      });

      controller.includeAll = false;
      _.forEach(controller.users, (user) => {
        expect(user.selected).toEqual(false);
      });

      expect(controller.selectedEmails).toEqual({
        includedUsers: [],
        excludedUsers: [],
      });
    });

    it('toggleSelection() should toggle the user object', () => {
      const controller = makeController({ users: [
        { userId: '5' }, // selected is undefiend
      ] });

      controller.toggleSelection(controller.users[0]);
      expect(controller.users[0].selected).toEqual(true);

      controller.toggleSelection(controller.users[0]);
      expect(controller.users[0].selected).toEqual(false);

      controller.toggleSelection(controller.users[0]);
      expect(controller.users[0].selected).toEqual(true);
    });

    it('includedCount should return the right number', () => {
      const controller = makeController({ users: mockUsers });
      controller.includedCount = 0;

      controller.includeAll = true;
      expect(controller.includedCount).toEqual(10);

      controller.users[0].selected = false;
      expect(controller.includedCount).toEqual(9);

      controller.users[1].selected = false;
      expect(controller.includedCount).toEqual(8);

      controller.includeAll = false;
      expect(controller.includedCount).toEqual(0);

      controller.users[0].selected = true;
      expect(controller.includedCount).toEqual(1);

      controller.users[1].selected = true;
      expect(controller.includedCount).toEqual(2);
    });

    it('_getIncludedEmails() should return an array with the right emails', () => {
      const controller = makeController({ users: mockUsers });
      expect(controller._getIncludedEmails()).toEqual(['user0@test.io', 'user2@test.io']);
    });

    it('_getExcludedEmails() should return an array with the right emails', () => {
      const controller = makeController({ users: mockUsers });
      expect(controller._getExcludedEmails()).toEqual(['user1@test.io', 'user3@test.io']);
    });

    it('selectedEmails should return with the right object', () => {
      const controller = makeController({ users: mockUsers });
      controller.selectedEmails = undefined;

      controller.includeAll = true;
      controller.users[1].selected = false;
      controller.users[3].selected = false;
      expect(controller.selectedEmails).toEqual({
        includedUsers: [],
        excludedUsers: ['user1@test.io', 'user3@test.io'],
      });

      controller.includeAll = false;
      controller.users[0].selected = true;
      controller.users[2].selected = true;
      expect(controller.selectedEmails).toEqual({
        includedUsers: ['user0@test.io', 'user2@test.io'],
        excludedUsers: [],
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = UsersComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(UsersTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(UsersController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();
      element = angular.element('<users foo="bar"><users/>');
      element = $compile(element)(scope);

      controller = element.controller('Users');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
