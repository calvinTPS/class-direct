import * as _ from 'lodash';
import Base from 'app/base';

export default class UsersController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);
  }

  get includeAll() {
    return this._includeAll;
  }

  set includeAll(bool) {
    this._includeAll = bool;

    // Selects or deselects all user object
    _.forEach(this.users, (user) => {
      user.selected = bool; // eslint-disable-line no-param-reassign
    });

    // Reset selected emails
    this.selectedEmails = {
      includedUsers: [],
      excludedUsers: [],
    };

    // Disable Select/Deselect All button
    this.isPristine = true;
  }

  toggleSelection(userObj) {
    userObj.selected = !userObj.selected; // eslint-disable-line no-param-reassign

    // Enable Select/Deselect All button
    this.isPristine = false;
  }


  /**
   * The included count is to return how many users has been selected not just based
   * on what is selected on screen. the formula is:
   * if includeAll == true (select all) which is usually set from parent controller or
   * by select all button, includedCount = pagination.totalElements - unselected users
   * BUT if includeAll == false (deselect all), includedCount = all selected users
   *
   * @return {Integer} - The total number of selected users
  */
  get includedCount() {
    return this.includeAll ?
      this.users.pagination.totalElements -
        (_.countBy(this.users, user => user.selected).false || 0) :
      _.countBy(this.users, user => user.selected).true || 0;
  }

  set includedCount(integer) {
    this._includedCount = integer;
  }

  get addButtonLabelCount() {
    return this.users.pagination.totalElements - this.includedCount;
  }

  get removeButtonLabelCount() {
    return this.includedCount;
  }

  _getIncludedEmails() {
    return _.reduce(this.users, (result, user) => {
      if (user.selected) {
        result.push(user.email);
      }
      return result;
    }, []);
  }

  _getExcludedEmails() {
    return _.reduce(this.users, (result, user) => {
      if (!user.selected) {
        result.push(user.email);
      }
      return result;
    }, []);
  }

  /**
   * selectedEmail getter will only be called from parent controller via
   * two way data binding. So the parent controller can get included/excluded
   * emails automatically.
   *
   * @return {object} : This getter will return an object containing
   *                    {
   *                      includedUsers: ['email_strings'],
   *                      excludedUsers: ['email_strings'],
   *                    }
  */
  get selectedEmails() {
    if (this.includeAll) { // When we select all, we will pass excluded emails
      const newExcludedEmails = this._getExcludedEmails();
      this._selectedEmails = !_.isEqual(newExcludedEmails, this._selectedEmails.excludedUsers) ? {
        includedUsers: [],
        excludedUsers: newExcludedEmails,
      } : this._selectedEmails;
    } else {
      // When we remove all, we will pass included emails
      const newIncludedEmails = this._getIncludedEmails();
      this._selectedEmails = !_.isEqual(newIncludedEmails, this._selectedEmails.includedUsers) ? {
        includedUsers: newIncludedEmails,
        excludedUsers: [],
      } : this._selectedEmails;
    }

    return this._selectedEmails;
  }

  set selectedEmails(emailsObject) {
    this._selectedEmails = emailsObject || {};
  }
}
