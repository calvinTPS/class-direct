import './users.scss';
import controller from './users.controller';
import template from './users.pug';

export default {
  bindings: {
    includeAll: '=?',
    includedCount: '=?',
    isEditable: '<?',
    mode: '@?', // export, tabular
    notification: '@?',
    selectedEmails: '=?',
    showEditButton: '<?',
    showSpinner: '<?',
    theme: '@?',
    users: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
