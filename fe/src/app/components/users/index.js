import angular from 'angular';
import UsersComponent from './users.component';

export default angular.module('users', [])
.component('users', UsersComponent)
.name;
