/* eslint-disable no-param-reassign */
/* @ngInject */
export default function config(
  $stateProvider,
  $urlRouterProvider,
) {
  // Redirect /exhibition-of-records to /exhibition-of-records/fleet-list
  $urlRouterProvider.when('/exhibition-of-records', '/exhibition-of-records/fleet-list');

  $stateProvider
    .state('exhibitionOfRecords', {
      url: '/exhibition-of-records',
      abstract: true,
      component: 'eorDashboard',
      resolve: {
        /* @ngInject */
        currentUser: UserService => UserService.getCurrentUser(),
      },
    })
    .state('exhibitionOfRecords.vessel', {
      url: '/fleet-list',
      resolve: {
        /* @ngInject */
        assets: (AppSettings, AssetService, DashboardService) => {
          // Get eor payload for assets query
          DashboardService.queryOptions = {
            isEOR: true,
          };
          return DashboardService.submitQuery();
        },
      },
      views: {
        'eor-dashboard-content': {
          component: 'eorVesselList',
        },
      },
    });
}
