import './eor-dashboard.scss';
import controller from './eor-dashboard.controller';
import template from './eor-dashboard.pug';

export default {
  bindings: {
    assets: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
