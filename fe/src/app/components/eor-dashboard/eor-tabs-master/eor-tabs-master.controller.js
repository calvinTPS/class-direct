import * as _ from 'lodash';
import Base from 'app/base';

export default class EorTabsMasterController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $transitions,
    AppSettings,
  ) {
    super(arguments);
  }

  $onInit() {
    // Initial tabs
    this._tabs = [{
      label: 'cd-tabs-fleet-list',
      icon: 'vessel',
      uiSref: this._AppSettings.STATES.EOR_VESSEL_LIST,
    }];

    // Example usage: _.zipObject(['a', 'b'], [1, 2]) => { 'a': 1, 'b': 2 }
    this._stateToTabPositionIndex =
      _.zipObject(this._tabs.map(tab => tab.uiSref), this._tabs.map((tab, i) => i));

    /**
     * Revalidate tabs selection on refresh
     */
    this.revalidateTabSelection();
  }

  revalidateTabSelection() {
    this._tabSelected = this._stateToTabPositionIndex[this._$state.current.name];
  }

  get tabs() {
    return this._tabs;
  }

  get tabSelected() {
    return this._tabSelected;
  }

  set tabSelected(tabIndex) {
    this._tabSelected = tabIndex;
  }
}
