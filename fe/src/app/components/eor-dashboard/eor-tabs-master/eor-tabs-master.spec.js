/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import EorTabsMasterComponent from './eor-tabs-master.component';
import EorTabsMasterController from './eor-tabs-master.controller';
import EorTabsMasterModule from './';
import EorTabsMasterTemplate from './eor-tabs-master.pug';
import translationStrings from 'resources/locales/en-GB.json';

describe('EorTabsMaster', () => {
  let $rootScope,
    $compile,
    $interpolate,
    $state,
    makeController,
    scope;

  beforeEach(window.module(EorTabsMasterModule));
  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));
  beforeEach(() => {
    const mockTranslateFilter = value => $interpolate(translationStrings[value])(scope);
    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((
    _$rootScope_,
    _$compile_,
    _$interpolate_,
    _$state_,
    $componentController
  ) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $interpolate = _$interpolate_;
    $state = _$state_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('eorTabsMaster', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController();
      $state.current.name = AppSettings.STATES.EOR_VESSEL_LIST;

      expect(controller.tabs.length).toBe(1);
      expect(controller._stateToTabPositionIndex).toEqual({ 'exhibitionOfRecords.vessel': 0 });
    });

    it('tabSelected should set/get the right _tabSelected', () => {
      const controller = makeController();
      controller.tabSelected = 1;
      expect(controller._tabSelected).toEqual(1);

      controller._tabSelected = 2;
      expect(controller.tabSelected).toEqual(2);
    });

    it('revalidateTabSelection() should set _tabSelected with the right selection', () => {
      const controller = makeController();
      // TODO: expand unit test when we have eor service schedule
      $state.current.name = AppSettings.STATES.EOR_VESSEL_LIST;
      controller.revalidateTabSelection();
      expect(controller._tabSelected).toEqual(0);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = EorTabsMasterComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(EorTabsMasterTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(EorTabsMasterController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element('<eor-tabs-master foo="bar"><eor-tabs-master/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('EorTabsMaster');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
