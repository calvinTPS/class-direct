import './eor-tabs-master.scss';
import controller from './eor-tabs-master.controller';
import template from './eor-tabs-master.pug';

export default {
  bindings: {
    tabSelected: '@?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
