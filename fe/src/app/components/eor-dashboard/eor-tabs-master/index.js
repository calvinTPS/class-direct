import angular from 'angular';
import EorTabsMasterComponent from './eor-tabs-master.component';
import uiRouter from 'angular-ui-router';

export default angular.module('eorTabsMaster', [
  uiRouter,
])
.component('eorTabsMaster', EorTabsMasterComponent)
.name;
