/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-underscore-dangle, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import EorVesselListComponent from './eor-vessel-list.component';
import EorVesselListController from './eor-vessel-list.controller';
import EorVesselListModule from './';
import EorVesselListTemplate from './eor-vessel-list.pug';

describe('EorVesselList', () => {
  let $rootScope,
    $compile,
    makeController,
    mockDashboardService,
    mockAssetService;

  beforeEach(window.module(EorVesselListModule));

  beforeEach(() => {
    mockAssetService = {
      removeFavourite: () => {},
      setFavourite: () => {},
    };
    mockDashboardService = {
      assets: () => [],
      queryOptions: {
        isFavourite: true,
      },
    };
    const mockState = {
      reload: () => {},
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('AssetService', mockAssetService);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('$state', mockState);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('eorVesselList', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('get assets() should return assets from dashboard service', () => {
      const controller = makeController();
      controller.currentUser = {
        eors: [
          { assetId: 1 },
          { assetId: 2 },
        ],
      };
      mockDashboardService.assets = [
        { id: 1, imo: 1 },
        { id: 2, imo: 2 },
      ];
      expect(controller.assets.length).toEqual(2);
    });

    it('resetQuery() should reload the fleet page with fresh query parameters', () => {
      const controller = makeController();
      spyOn(controller._$state, 'reload');

      controller.resetQuery();
      expect(controller._$state.reload).toHaveBeenCalled();
    });

    it('showNoResultsMessage should only show no results message when certain conditions are met', () => {
      const controller = makeController();
      controller.currentUser = {
        eors: [],
      };
      mockAssetService.isQuerying = false;
      mockDashboardService.assets = [];

      expect(controller.showNoResultsMessage).toBe(true);

      // Any changes to condition results in false

      mockAssetService.isQuerying = !mockAssetService.isQuerying;
      expect(controller.showNoResultsMessage).toBe(false);
      mockAssetService.isQuerying = !mockAssetService.isQuerying;

      mockDashboardService.assets = [{}];
      expect(controller.showNoResultsMessage).toBe(false);
      mockDashboardService.assets = [];
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = EorVesselListComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(EorVesselListTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(EorVesselListController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<eor-vessel-list current-user="currentUser"><eor-vessel-list/>');
      element = $compile(element)(scope);
      scope.currentUsercurrentUser = { eors: [] }; // CHANGE ME
      scope.$apply();

      controller = element.controller('EorVesselList');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
