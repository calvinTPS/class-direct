import angular from 'angular';
import EorVesselListComponent from './eor-vessel-list.component';
import uiRouter from 'angular-ui-router';

export default angular.module('eorVesselList', [
  uiRouter,
])
.component('eorVesselList', EorVesselListComponent)
.name;
