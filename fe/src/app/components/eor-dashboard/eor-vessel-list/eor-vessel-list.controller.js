import _ from 'lodash';
import Base from 'app/base';

export default class EorVesselListController extends Base {
  /* @ngInject */
  constructor(
    $state,
    AssetService,
    DashboardService,
  ) {
    super(arguments);
  }


  get assets() {
    if (_.has(this.currentUser, 'eors')) {
      return _.forEach(this._DashboardService.assets, asset =>
        _.merge(asset, _.find(this.currentUser.eors, ['assetId', asset.imo])));
    }
    return this._DashboardService.assets;
  }

  /**
   * Show No Results message when:
   * 1. No results, AND
   * 2. Query is not pending, AND
   * 3. Conditions to show No Favourites message is not met.
   *
   * @returns {boolean}
   */
  get showNoResultsMessage() {
    const emptyResults = this.assets.length === 0;
    const isQuerying = this._AssetService.isQuerying;
    return emptyResults && !isQuerying && !this.showNoFavouritesMessage;
  }

  resetQuery() {
    this._$state.reload();
  }
}
