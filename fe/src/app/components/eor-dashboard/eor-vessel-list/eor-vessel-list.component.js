import './eor-vessel-list.scss';
import controller from './eor-vessel-list.controller';
import template from './eor-vessel-list.pug';

export default {
  bindings: {
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
