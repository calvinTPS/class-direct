/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import EorDashboardComponent from './eor-dashboard.component';
import EorDashboardController from './eor-dashboard.controller';
import EorDashboardModule from './';
import EorDashboardTemplate from './eor-dashboard.pug';

describe('EorDashboard', () => {
  let $rootScope,
    $compile,
    $injector,
    $state,
    makeController;

  const mockTranslateFilter = value => value;

  const mockAssetService = {
    query: () => Promise.resolve([]),
  };

  const mockCurrentUser = {
    eors: [
      { assetId: 1 },
    ],
  };

  const mockDashboardService = {
    queryOptions: {},
    submitQuery: () => {},
    filterOptionsChanged: (filterOptions) => {
      _.assign(mockDashboardService.queryOptions, filterOptions);
    },
  };

  beforeEach(window.module(EorDashboardModule,
    { translateFilter: mockTranslateFilter }
  ));

  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
    $provide.value('AssetService', mockAssetService);
    $provide.value('currentUser', mockCurrentUser);
    $provide.value('DashboardService', mockDashboardService);
  }));

  beforeEach(inject((
    _$rootScope_,
    _$compile_,
    _$injector_,
    _$state_,
    $componentController
  ) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $injector = _$injector_;
    $state = _$state_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('eorDashboard', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Routes', () => {
    it.async('should resolve eor assets via asset query', async () => {
      const result = [{ id: 1, name: 'Ship' }];
      spyOn(mockDashboardService, 'submitQuery').and.callFake(() => {
        mockDashboardService.assets = result;
        return Promise.resolve();
      });
      await $injector.invoke($state.get(AppSettings.STATES.EOR_VESSEL_LIST).resolve.assets);

      expect(mockDashboardService.submitQuery).toHaveBeenCalledTimes(1);
      expect(mockDashboardService.assets).toEqual(result);
    });
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = EorDashboardComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(EorDashboardTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(EorDashboardController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<eor-dashboard foo="bar"><eor-dashboard/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('EorDashboard');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
