import angular from 'angular';
import EorDashboardComponent from './eor-dashboard.component';
import EorTabsMaster from './eor-tabs-master';
import EorVesselList from './eor-vessel-list';
import routes from './eor-dashboard.routes';
import uiRouter from 'angular-ui-router';

export default angular.module('eorDashboard', [
  EorTabsMaster,
  EorVesselList,
  uiRouter,
])
.config(routes)
.component('eorDashboard', EorDashboardComponent)
.name;
