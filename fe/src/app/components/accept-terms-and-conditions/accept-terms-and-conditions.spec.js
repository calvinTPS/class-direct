/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AcceptTermsAndConditionsComponent from './accept-terms-and-conditions.component';
import AcceptTermsAndConditionsController from './accept-terms-and-conditions.controller';
import AcceptTermsAndConditionsModule from './';
import AcceptTermsAndConditionsTemplate from './accept-terms-and-conditions.pug';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';

describe('AcceptTermsAndConditions', () => {
  let $rootScope,
    $compile,
    $state,
    makeController;

  const windowObj = {
    location: { href: '', protocol: 'http:', hostname: 'localhost', port: 8080 },
  };
  const TermsAndConditionsService = {
    getLatestTermsAndConditions: () => new Promise((resolve, reject) => resolve()),
    acceptTermsAndConditions: () => new Promise((resolve, reject) => resolve()),
    setEquasisThetisAcceptedTC: () => {},
  };
  const mockUserService = {
    getCurrentUser: () => Promise.resolve({ id: 1, name: 'testUser' }),
    logout: () => {},
    unsetCurrentUser: () => {},
  };

  beforeEach(window.module(
    AcceptTermsAndConditionsModule,
    angularMaterial,
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockSafeHtmlFilter = value => value;

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('safeHtmlFilter', mockSafeHtmlFilter);
      $provide.value('TermsAndConditionsService', TermsAndConditionsService);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('$window', windowObj);
      $provide.value('$mdMedia', () => {});
      $provide.value('UserService', mockUserService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$state_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $state = _$state_;

    const dep = {};
    makeController =
      async (bindings = {}) => {
        const controller = $componentController('acceptTermsAndConditions', { $dep: dep }, bindings);
        // controller.onViewRendered = () => null;
        // To bypass unit testing this method which is very tricky
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', async () => {
      const controller = await makeController();
      expect(controller.isDisabled).toEqual(false);
      expect(controller.user).toEqual({ id: 1, name: 'testUser' });
    });

    it.async('acceptTermsAndConditions should update the terms and conditions', async () => {
      const controller = await makeController();
      const mockUser = {
        userId: 1,
        name: 'mockUser',
        isEquasisThetis: false,
      };
      const mockLatestTermsAndConditions = {
        tcId: 1,
      };
      controller.user = mockUser;
      controller.latestTermsAndConditions = mockLatestTermsAndConditions;

      spyOn(controller._TermsAndConditionsService, 'acceptTermsAndConditions');
      spyOn($state, 'go');

      await controller.acceptTermsAndConditions();
      expect(
        TermsAndConditionsService.acceptTermsAndConditions
      ).toHaveBeenCalled();
      expect($state.go).toHaveBeenCalledWith('main');
    });

    it.async('acceptTermsAndConditions should not be called if user is Equasis / Thetis', async () => {
      const controller = await makeController();
      const mockUser = {
        userId: 1,
        name: 'mockUser',
        isEquasisThetis: true,
      };
      const mockAssetId = '1';
      const mockLatestTermsAndConditions = {
        tcId: 1,
      };
      controller.user = mockUser;
      controller._$stateParams = { assetId: mockAssetId };
      controller.latestTermsAndConditions = mockLatestTermsAndConditions;

      spyOn(controller._TermsAndConditionsService, 'acceptTermsAndConditions');
      spyOn(controller._TermsAndConditionsService, 'setEquasisThetisAcceptedTC');
      spyOn($state, 'go');

      await controller.acceptTermsAndConditions();
      expect(TermsAndConditionsService.acceptTermsAndConditions)
        .not.toHaveBeenCalled();
      expect(
        TermsAndConditionsService.setEquasisThetisAcceptedTC
      ).toHaveBeenCalled();
      expect($state.go).toHaveBeenCalledWith(AppSettings.STATES.ASSET_HOME, { assetId: '1' });
    });

    it('logout should set the location to correct url for a normal user', async () => {
      spyOn(mockUserService, 'logout').and.callThrough();

      const controller = await makeController();
      const mockNormalUser = { isEquasisThetis: false };

      controller.user = mockNormalUser;
      controller.logout();
      expect(mockUserService.logout).toHaveBeenCalledTimes(1);
    });

    it('logout should set the location to correct url for Equasis/Thetis user', async () => {
      const controller = await makeController();
      const mockEquasisThetisUser = { isEquasisThetis: true };

      controller.user = mockEquasisThetisUser;
      controller.logout();
      expect(windowObj.location.href).toEqual(AppSettings.CD_LIVE_LANDING_PAGE_URL);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AcceptTermsAndConditionsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AcceptTermsAndConditionsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AcceptTermsAndConditionsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<accept-terms-and-conditions foo="bar"><accept-terms-and-conditions/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('AcceptTermsAndConditions');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
