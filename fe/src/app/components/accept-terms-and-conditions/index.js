import AcceptTermsAndConditionsComponent from './accept-terms-and-conditions.component';
import angular from 'angular';
import routes from './accept-terms-and-conditions.routes';
import uiRouter from 'angular-ui-router';

export default angular.module('acceptTermsAndConditions', [
  uiRouter,
])
.component('acceptTermsAndConditions', AcceptTermsAndConditionsComponent)
.config(routes)
.name;
