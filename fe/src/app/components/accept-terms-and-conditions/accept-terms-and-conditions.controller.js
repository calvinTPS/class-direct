import * as _ from 'lodash';
import Base from 'app/base';

export default class AcceptTermsAndConditionsController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $state,
    $stateParams,
    $window,
    AppSettings,
    TermsAndConditionsService,
    UserService,
  ) {
    super(arguments);

    this.isDisabled = false;
  }

  // https://stackoverflow.com/questions/21715256/angularjs-event-to-call-after-content-is-loaded
  onViewRendered() {
    angular.element(() => {
      const doc = document;
      const elements = doc.getElementsByClassName('cd-anchor-tag');

      if (_.size(elements) > 0) {
        for (let i = 0; i < elements.length; i++) {
          const id = elements[i].getAttribute('data-scroll-to');

          if (!_.isUndefined(id)) {
            elements[i].addEventListener('click', (ev) => {
              ev.preventDefault();
              doc.getElementById(id).scrollIntoView();
            });
          }
        }
      }
    });
  }

  async $onInit() {
    this.user = await this._UserService.getCurrentUser();
    this.onViewRendered();
  }

  logout() {
    if (this.user.isEquasisThetis) {
      this._$window.location.href =
        this._AppSettings.CD_LIVE_LANDING_PAGE_URL;
    } else {
      this._UserService.logout();
    }
  }

  async acceptTermsAndConditions() {
    this.isDisabled = true;
    if (this.user.isEquasisThetis) {
      const assetId = _.get(this._$stateParams, 'assetId');
      if (!assetId) {
        throw new Error('Missing asset ID');
      }

      this._TermsAndConditionsService
        .setEquasisThetisAcceptedTC(this.latestTermsAndConditions.tcId);

      this.reloadUser();
      this._$state.go(this._AppSettings.STATES.ASSET_HOME, { assetId });
    } else {
      try {
        await this._TermsAndConditionsService
          .acceptTermsAndConditions(this.latestTermsAndConditions.tcId);

        this.reloadUser();
        this._$state.go(this._AppSettings.STATES.MAIN);
      } catch (e) {
        this._$state.go(this._AppSettings.STATES.ERROR, {
          statusCode: e.status,
          errorMessage: e.data,
        });
      }
    }
  }

  reloadUser() {
    this._UserService.unsetCurrentUser();
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
