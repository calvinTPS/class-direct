/* @ngInject */
export default function config($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('acceptTermsAndConditions', {
      url: '/accept-terms-and-conditions',
      component: 'acceptTermsAndConditions',
      params: {
        assetId: '',
      }, // Had to pass default param
      resolve: {
        /* @ngInject */
        latestTermsAndConditions: TermsAndConditionsService =>
          TermsAndConditionsService.getLatestTermsAndConditions(),
      },
    });
}
