import './accept-terms-and-conditions.scss';
import controller from './accept-terms-and-conditions.controller';
import template from './accept-terms-and-conditions.pug';

export default {
  bindings: {
    latestTermsAndConditions: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
