import Base from 'app/base';

export default class SystemHeaderController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $rootScope,
    $scope,
    $state,
    $window,
    AppSettings,
    FlagService,
    PermissionsService,
    UserService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this.updateClientName = this.updateClientName.bind(this);

    this._$scope.$on(this._AppSettings.EVENTS.APP_INITIALIZED, () => {
      this._$scope.$on(this._AppSettings.EVENTS.START_MASQUERADE, () => {
        this.updateClientName();
      });

      this.updateClientName().then(() => {
        this.initialized = true;
      });
    });
  }

  async updateClientName(currentUser) {
    this._currentUser = currentUser || await this._UserService.getCurrentUser();

    if (
      this._currentUser.isEORUserOnly || // User has only EOR role
      (this._currentUser.isEORUser && this._$rootScope.role_mode === this._AppSettings.ROLES.EOR)
    ) {
      this.clientName = 'cd-eor';
    } else if (this._currentUser.isLRUser) {
      this.clientName = 'cd-lloyds-register';
    } else if (this._currentUser.isFlagUser) {
      const flagMap = await this._FlagService.getFlagsMapByCode();
      const flag = flagMap[this._currentUser.flagCode];
      this.clientName = flag.name;
    } else if (this._currentUser.isEquasisThetis) {
      this.clientName = 'cd-limited-access';
    } else if (this._currentUser.isClient || this._currentUser.isShipBuilder) {
      this.clientName = this._currentUser.company.name;
    } else {
      this.clientName = null;
    }

    this._$scope.$apply();
  }

  get currentUser() {
    return this._currentUser;
  }

  get canShowProfileButton() {
    return this.currentUser && !this.currentUser.isEquasisThetis;
  }

  get canShowLoginButton() {
    return this.currentUser && this.currentUser.isEquasisThetis;
  }

  get canShowNationalAdministrationButton() {
    return this._PermissionsService.canShowNationalAdminButton(this.currentUser);
  }

  goToCDLogin() {
    this._UserService.logout();
  }
}
