import './system-header.scss';
import controller from './system-header.controller';
import template from './system-header.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
