/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import SystemHeaderComponent from './system-header.component';
import SystemHeaderController from './system-header.controller';
import SystemHeaderModule from './';
import SystemHeaderTemplate from './system-header.pug';
import UserModel from 'app/common/models/user/user';

describe('SystemHeader', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    mockUserService,
    mockPermissionsService,
    makeController;

  const mockTranslateFilter = value => value;
  const mockFlagMapByCode = {
    ALA: {
      name: 'Angola',
    },
  };

  const mockFlagService = {
    getFlagsMapByCode: () => mockFlagMapByCode,
  };

  beforeEach(window.module(
    SystemHeaderModule,
    { translateFilter: mockTranslateFilter },
  ));

  beforeEach(() => {
    mockUserService = {
      getCurrentUser: () => Promise.resolve(),
      logout: () => {},
    };

    mockPermissionsService = {
      canShowNationalAdminButton: (user) => {
        if (!user) return false;

        return !user.isEORUser;
      },
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('UserService', mockUserService);
      $provide.value('FlagService', mockFlagService);
      $provide.value('PermissionsService', mockPermissionsService);
      $provide.value('$mdMedia', {});
    });
  });

  beforeEach(inject((_$rootScope_, _$mdMedia_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('systemHeader', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    describe('goToCDLogin', () => {
      it('should call logout on user service', () => {
        const controller = makeController();
        spyOn(mockUserService, 'logout');

        controller.goToCDLogin();

        expect(mockUserService.logout).toHaveBeenCalledTimes(1);
      });
    });

    describe('updateClientName()', () => {
      it.async('Should get current user from service and currentUser getter should return the ' +
        'correct user data.', async () => {
        const controller = makeController();
        const mockUserModel = {
          isLRUser: true,
        };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
        expect(controller.currentUser).toEqual(mockUserModel);
      });

      it('Should use the passed in currentUser obj', () => {
        const controller = makeController();
        controller.updateClientName({ custom: true });
        expect(controller.currentUser).toEqual({ custom: true });
      });

      it.async('Should update the LR user header correctly', async () => {
        const controller = makeController();
        const mockUserModel = {
          isLRUser: true,
        };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).toBe('cd-lloyds-register');
      });

      it.async('Should not update to EOR user header when not in EOR mode', async () => {
        const controller = makeController();
        const mockUserModel = {
          isEORUser: true,
        };
        // CLIENT mode
        $rootScope.role_mode = AppSettings.ROLES.CLIENT;
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).not.toBe('cd-eor');
      });

      it.async('Should update the EOR user header correctly', async () => {
        const controller = makeController();
        const mockUserModel = {
          isEORUser: true,
        };
        // EOR mode
        $rootScope.role_mode = AppSettings.ROLES.EOR;
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).toBe('cd-eor');
      });

      it.async('Should update the EOR user header with user is ONLY EOR user', async () => {
        const controller = makeController();
        const mockUserModel = {
          isEORUser: true,
          isEORUserOnly: true,
        };

        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).toBe('cd-eor');
      });

      it.async('Should update the Flag user header correctly', async () => {
        const controller = makeController();
        const mockUserModel = {
          isFlagUser: true,
          flagCode: 'ALA',
        };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).toBe('Angola');
      });

      it.async('should update the ship builder header correctly', async () => {
        const controller = makeController();
        const mockUserModel = {
          isShipBuilder: true,
          company: {
            name: 'SHIP BUILDER NAME',
          },
        };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).toBe('SHIP BUILDER NAME');
      });

      it.async('should update the client header correctly', async () => {
        const controller = makeController();
        const mockUserModel = {
          isClient: true,
          company: {
            name: 'CLIENT NAME',
          },
        };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).toBe('CLIENT NAME');
      });

      it.async('should update the Equasis/Thetis header correctly', async () => {
        const controller = makeController();
        const mockUserModel = {
          isEquasisThetis: true,
        };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        await controller.updateClientName();
        expect(controller.clientName).toBe('cd-limited-access');
      });
    });

    describe('Getter canShowProfileButton should return the correct value', () => {
      it.async('Should return true if not an EquasisThetis user', async () => {
        const mockUserModel = { isEquasisThetis: false };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        const controller = await makeController();
        await $rootScope.$broadcast(AppSettings.EVENTS.APP_INITIALIZED);
        expect(mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
        expect(controller.canShowProfileButton).toBe(true);
      });

      it.async('Should return false if an EquasisThetis user', async () => {
        const mockUserModel = { isEquasisThetis: true };
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
        const controller = await makeController();
        await $rootScope.$broadcast(AppSettings.EVENTS.APP_INITIALIZED);
        expect(mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
        expect(controller.canShowProfileButton).toBe(false);
      });
    });
  });

  describe('Getter canShowLoginButton should return the correct value', () => {
    it.async('Should return false if not an EquasisThetis user', async () => {
      const mockUserModel = { isEquasisThetis: false };
      spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
      const controller = await makeController();
      await $rootScope.$broadcast(AppSettings.EVENTS.APP_INITIALIZED);
      expect(mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
      expect(controller.canShowLoginButton).toBe(false);
    });

    it.async('Should return true if an EquasisThetis user', async () => {
      const mockUserModel = { isEquasisThetis: true };
      spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
      const controller = await makeController();
      await $rootScope.$broadcast(AppSettings.EVENTS.APP_INITIALIZED);
      expect(mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
      expect(controller.canShowLoginButton).toBe(true);
    });
  });

  describe('Getter canShowNationalAdministrationButton should return the correct value', () => {
    it.async('Should returns true if the currentUser is present', async () => {
      const mockUserModel = { id: 1, name: 'testUser' };
      spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
      const controller = await makeController();
      await $rootScope.$broadcast(AppSettings.EVENTS.APP_INITIALIZED);
      expect(mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
      expect(controller.canShowNationalAdministrationButton).toBe(true);
    });

    it.async('Should returns false if the currentUser is not present', async () => {
      const mockUserModel = undefined;
      spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(mockUserModel));
      const controller = await makeController();
      await $rootScope.$broadcast(AppSettings.EVENTS.APP_INITIALIZED);
      expect(mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
      expect(controller.canShowNationalAdministrationButton).toBe(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SystemHeaderComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SystemHeaderTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SystemHeaderController);
    });
  });
});
