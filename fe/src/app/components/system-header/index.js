import angular from 'angular';
import SystemHeaderComponent from './system-header.component';
import uiRouter from 'angular-ui-router';

export default angular.module('systemHeader', [
  uiRouter,
])
.component('systemHeader', SystemHeaderComponent)
.name;
