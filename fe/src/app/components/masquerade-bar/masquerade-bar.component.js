import './masquerade-bar.scss';
import controller from './masquerade-bar.controller';
import template from './masquerade-bar.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
