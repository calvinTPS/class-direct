/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import MasqueradeBarComponent from './masquerade-bar.component';
import MasqueradeBarController from './masquerade-bar.controller';
import MasqueradeBarModule from './';
import MasqueradeBarTemplate from './masquerade-bar.pug';

describe('MasqueradeBar', () => {
  let $rootScope,
    $compile,
    makeController,
    $scope,
    $window;

  beforeEach(window.module(MasqueradeBarModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const windowObj = {
      location: { href: '', protocol: 'http:', hostname: 'localhost', port: 8080 },
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('$window', windowObj);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$window_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $window = _$window_;
    $scope = $rootScope.$new();

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('masqueradeBar', { $dep: dep, $scope }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('$scope.$on, get called in the constructor', () => {
      const controller = makeController();
      $rootScope.$broadcast(AppSettings.EVENTS.START_MASQUERADE, { masqueradedUserId: 123 });
      expect(controller.isVisible).toBe(true);
      expect(controller.masqueradedUserId).toBe(123);
    });

    it('stopMasquerading() redirects user back to user administration page', () => {
      const controller = makeController();
      $rootScope.$broadcast(AppSettings.EVENTS.START_MASQUERADE, { masqueradedUserId: 123 });
      controller.stopMasquerading();
      expect($window.location.href).toEqual('/administration/users/123');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = MasqueradeBarComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(MasqueradeBarTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(MasqueradeBarController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<masquerade-bar foo="bar"><masquerade-bar/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('MasqueradeBar');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
