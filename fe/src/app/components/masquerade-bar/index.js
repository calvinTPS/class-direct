import angular from 'angular';
import MasqueradeBarComponent from './masquerade-bar.component';

export default angular.module('masqueradeBar', [])
.component('masqueradeBar', MasqueradeBarComponent)
.name;
