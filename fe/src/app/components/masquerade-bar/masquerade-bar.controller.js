import Base from 'app/base';

export default class MasqueradeBarController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $window,
    AppSettings,
  ) {
    super(arguments);
  }

  $onInit() {
    this._$scope.$on(this._AppSettings.EVENTS.START_MASQUERADE, (event, options) => {
      this.isVisible = true;
      this.masqueradedUserId = options.masqueradedUserId;
      this._$scope.$apply();
    });
  }

  stopMasquerading() {
    this._$window.location.href =
      `/${this._AppSettings.API_ENDPOINTS.AdministrationUsers}` +
      `/${this.masqueradedUserId}`;
  }
}
