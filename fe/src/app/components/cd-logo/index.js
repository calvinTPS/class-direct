import angular from 'angular';
import CdLogoComponent from './cd-logo.component';
import uiRouter from 'angular-ui-router';

export default angular.module('cdLogo', [
  uiRouter,
])
.component('cdLogo', CdLogoComponent)
.name;
