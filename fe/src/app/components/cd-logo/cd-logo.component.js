import './cd-logo.scss';
import controller from './cd-logo.controller';
import template from './cd-logo.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
