/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import CdLogoComponent from './cd-logo.component';
import CdLogoController from './cd-logo.controller';
import CdLogoModule from './';
import CdLogoTemplate from './cd-logo.pug';

describe('CdLogo', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(CdLogoModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('AppSettings', AppSettings);
      $provide.value('$mdMedia', {});
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('cdLogo', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CdLogoComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CdLogoTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CdLogoController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<cd-logo foo="bar"><cd-logo/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('CdLogo');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
