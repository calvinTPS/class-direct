/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AdminUserDetailsComponent from './admin-user-details.component';
import AdminUserDetailsController from './admin-user-details.controller';
import AdminUserDetailsModule from './';
import AdminUserDetailsTemplate from './admin-user-details.pug';
import AppSettings from 'app/config/project-variables.js';

describe('AdminUserDetails', () => {
  let $rootScope,
    $compile,
    makeController,
    mockUserService,
    mockClientService,
    mockFlagService,
    mockState,
    mockMdDialog,
    mockTranslateFilter;

  const mockUser = {
    id: 101,
    userId: 101,
    equasis: true,
    thetis: false,
    flagCode: 100,
    shipBuilderCode: 1,
    clientCode: 4,
    name: 'John Doe',
    email: 'abc@bae.com',
    telephone: '07865432',
    city: 'WP',
    postCode: '50470',
    status: 1,
    company: 'Lloyds Register',
    address1: 'Address Line 1',
    address2: 'Address Line 2',
    address3: 'Address Line 3',
    country: 'UK',
  };

  beforeEach(window.module(
    AdminUserDetailsModule,
  ));

  beforeEach(() => {
    mockUserService = {
      editUser: () => Promise.resolve(),
    };

    mockClientService = {
      getClientsByIMONumber: () => Promise.resolve([]),
      getShipBuilderByIMONumber: () => Promise.resolve([]),
    };

    mockFlagService = {
      getFlagByFlagCode: () => Promise.resolve([]),
    };

    mockState = {
      reload: () => { },
    };

    mockMdDialog = {
      cancel: () => { },
      hide: () => { },
    };

    mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('UserService', mockUserService);
      $provide.value('ClientService', mockClientService);
      $provide.value('FlagService', mockFlagService);
      $provide.value('$state', mockState);
      $provide.value('$mdDialog', mockMdDialog);
      $provide.value('$translateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('adminUserDetails', { $dep: dep }, bindings);
        if (controller.$onInit) {
          controller.$onInit();
        }
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ user: mockUser });
      expect(controller.user).toEqual(mockUser);
    });

    it('cancelCodeChange() should clear all queries', () => {
      const controller = makeController();
      spyOn(controller._$mdDialog, 'cancel');
      controller.newCode = 'TEST';
      controller.errorObject['client-code'] = true;
      controller.errorObject['ship-builder-code'] = true;
      controller.errorObject['flag-code'] = true;

      controller.cancelCodeChange();

      expect(controller.newCode).toEqual(null);
      expect(controller.errorObject).toEqual({});
      expect(controller._$mdDialog.cancel).toHaveBeenCalled();
    });

    describe('_validateCode() should call the appropriate services and return true if there is matching value', () => {
      it.async('return false when newCode is not provided', async () => {
        const controller = makeController();
        expect(await controller._validateCode()).toEqual(false);
        expect(await controller._validateCode(undefined)).toEqual(false);
        expect(await controller._validateCode(null)).toEqual(false);
      });

      it.async('should call ClientService to get Clients IMONumber and return true if there is response', async () => {
        const controller = makeController();
        const mockResponseArray = [1, 2, 3];
        spyOn(mockClientService, 'getClientsByIMONumber').and.returnValue(Promise.resolve(mockResponseArray));

        const response = await controller._validateCode(100100, AppSettings.ROLES.CLIENT);

        expect(mockClientService.getClientsByIMONumber).toHaveBeenCalledWith(100100, false);
        expect(response).toEqual(true);
      });

      it.async('should call ClientService to get Clients IMONumber and return false if there are no response', async () => {
        const controller = makeController();
        const mockResponseArray = [];
        spyOn(mockClientService, 'getClientsByIMONumber').and.returnValue(Promise.resolve(mockResponseArray));

        const response = await controller._validateCode(100100, AppSettings.ROLES.CLIENT);

        expect(mockClientService.getClientsByIMONumber).toHaveBeenCalledWith(100100, false);
        expect(response).toEqual(false);
      });

      it.async('should call ClientService to get Ship Builder IMONumber and return true if there is response', async () => {
        const controller = makeController();
        const mockResponseArray = [1, 2, 3];
        spyOn(mockClientService, 'getShipBuilderByIMONumber').and.returnValue(Promise.resolve(mockResponseArray));

        const response = await controller._validateCode(100100, AppSettings.ROLES.SHIP_BUILDER);

        expect(mockClientService.getShipBuilderByIMONumber).toHaveBeenCalledWith(100100, false);
        expect(response).toEqual(true);
      });

      it.async('should call ClientService to get Ship Builder IMONumber and return false if there are no response', async () => {
        const controller = makeController();
        const mockResponseArray = [];
        spyOn(mockClientService, 'getShipBuilderByIMONumber').and.returnValue(Promise.resolve(mockResponseArray));

        const response = await controller._validateCode(100100, AppSettings.ROLES.SHIP_BUILDER);

        expect(mockClientService.getShipBuilderByIMONumber).toHaveBeenCalledWith(100100, false);
        expect(response).toEqual(false);
      });

      it.async('should call FlagService to get flag code and return true if there is response', async () => {
        const controller = makeController();
        const mockResponseArray = [1, 2, 3];
        controller.newCode = 'abc';
        spyOn(mockFlagService, 'getFlagByFlagCode').and.returnValue(Promise.resolve(mockResponseArray));

        const response = await controller._validateCode(controller.newCode, AppSettings.ROLES.FLAG);

        expect(controller.newCode).toEqual('ABC');
        expect(mockFlagService.getFlagByFlagCode).toHaveBeenCalledWith('ABC');
        expect(response).toEqual(true);
      });

      it.async('should call FlagService to get flag code and return false if there are no response', async () => {
        const controller = makeController();
        const mockResponseArray = [];
        controller.newCode = 'abc';
        spyOn(mockFlagService, 'getFlagByFlagCode').and.returnValue(Promise.resolve(mockResponseArray));

        const response = await controller._validateCode(controller.newCode, AppSettings.ROLES.FLAG);

        expect(controller.newCode).toEqual('ABC');
        expect(mockFlagService.getFlagByFlagCode).toHaveBeenCalledWith('ABC');
        expect(response).toEqual(false);
      });
    });

    describe('sections', () => {
      it('should return [] when user is not set', () => {
        const controller = makeController();
        expect(controller.sections).toEqual([]);
      });

      it('client code should be editable only for LR Internal user', () => {
        const controller = makeController({ user: mockUser });
        mockUser.isLRInternal = true;
        expect(controller.sections[1].fields[0].editable).toEqual(true);
        expect(controller.sections[1].fields[1].editable).toEqual(true);
        expect(controller.sections[1].fields[2].editable).toEqual(true);

        controller.sections = null;
        mockUser.isLRInternal = false;
        expect(controller.sections[1].fields[0].editable).toEqual(false);
        expect(controller.sections[1].fields[1].editable).toEqual(false);
        expect(controller.sections[1].fields[2].editable).toEqual(false);
      });
    });

    describe('confirmCodeChange', () => {
      it('hides dialog and does a state reload', (done) => {
        const controller = makeController({ user: mockUser });
        const promise = Promise.resolve();

        spyOn(controller, 'updateCode').and.returnValue(promise);
        spyOn(mockMdDialog, 'hide');
        spyOn(mockState, 'reload');

        controller.confirmCodeChange({ $valid: true }, 'SOME_CODE');

        promise
          .then(() => {
            expect(controller.updateCode).toHaveBeenCalledWith('SOME_CODE');
            expect(mockMdDialog.hide).toHaveBeenCalledTimes(1);
            expect(mockState.reload).toHaveBeenCalledTimes(1);
          })
          .then(done);
      });
    });

    describe('updateCode', () => {
      describe('blank client code', () => {
        it.async('should call UserService.editUser updating client code', async (done) => {
          const controller = makeController({ user: mockUser });
          controller.newCode = '';
          spyOn(mockUserService, 'editUser').and.returnValue(Promise.resolve({
            clientCode: null,
            shipBuilderCode: null,
            flagCode: null,
          }));

          await controller.updateCode(AppSettings.ROLES.CLIENT);
          expect(mockUserService.editUser).toHaveBeenCalledWith(101, {}, ['clientCode', 'flagCode', 'shipBuilderCode']);
          expect(controller.user.clientCode).toEqual(null);
          expect(controller.user.shipBuilderCode).toEqual(null);
          expect(controller.user.flagCode).toEqual(null);
          expect(controller.newCode).toBe(null);
          expect(controller.sections[1].fields[0].value).toEqual(null);
          expect(controller.sections[1].fields[1].value).toEqual(null);
          expect(controller.sections[1].fields[2].value).toEqual(null);
        });

        it.async('should call UserService.editUser updating ship builder code', async (done) => {
          const controller = makeController({ user: mockUser });
          controller.newCode = '';
          spyOn(mockUserService, 'editUser').and.returnValue(Promise.resolve({
            clientCode: null,
            shipBuilderCode: null,
            flagCode: null,
          }));

          await controller.updateCode(AppSettings.ROLES.SHIP_BUILDER);
          expect(mockUserService.editUser).toHaveBeenCalledWith(101, {}, ['clientCode', 'flagCode', 'shipBuilderCode']);
          expect(controller.user.clientCode).toEqual(null);
          expect(controller.user.shipBuilderCode).toEqual(null);
          expect(controller.user.flagCode).toEqual(null);
          expect(controller.newCode).toBe(null);
          expect(controller.sections[1].fields[0].value).toEqual(null);
          expect(controller.sections[1].fields[1].value).toEqual(null);
          expect(controller.sections[1].fields[2].value).toEqual(null);
        });

        it.async('should call UserService.editUser updating flag code', async (done) => {
          const controller = makeController({ user: mockUser });
          controller.newCode = '';
          spyOn(mockUserService, 'editUser').and.returnValue(Promise.resolve({
            clientCode: null,
            shipBuilderCode: null,
            flagCode: null,
          }));

          await controller.updateCode(AppSettings.ROLES.FLAG);
          expect(mockUserService.editUser).toHaveBeenCalledWith(101, {}, ['clientCode', 'flagCode', 'shipBuilderCode']);
          expect(controller.user.clientCode).toEqual(null);
          expect(controller.user.shipBuilderCode).toEqual(null);
          expect(controller.user.flagCode).toEqual(null);
          expect(controller.newCode).toBe(null);
          expect(controller.sections[1].fields[0].value).toEqual(null);
          expect(controller.sections[1].fields[1].value).toEqual(null);
          expect(controller.sections[1].fields[2].value).toEqual(null);
        });
      });

      describe('non-blank client code', () => {
        it.async('should check again existing clients before saving client code', async (done) => {
          const controller = makeController({ user: mockUser });
          spyOn(controller, '_validateCode').and.returnValue(true);
          spyOn(mockUserService, 'editUser').and.returnValue(Promise.resolve({
            clientCode: '123123',
            flagCode: null,
            shipBuilderCode: null,
          }));
          controller.newCode = '123123';

          await controller.updateCode(AppSettings.ROLES.CLIENT);
          expect(controller._validateCode).toHaveBeenCalledWith('123123', AppSettings.ROLES.CLIENT);
          expect(mockUserService.editUser).toHaveBeenCalledWith(101, {
            clientCode: '123123',
            flagCode: null,
            shipBuilderCode: null,
          }, ['flagCode', 'shipBuilderCode']);
          expect(controller.user.clientCode).toEqual('123123');
          expect(controller.user.shipBuilderCode).toEqual(null);
          expect(controller.user.flagCode).toEqual(null);
          expect(controller.newCode).toBe(null);
          expect(controller.sections[1].fields[0].value).toEqual('123123');
          expect(controller.sections[1].fields[1].value).toEqual(null);
          expect(controller.sections[1].fields[2].value).toEqual(null);
        });

        it.async('should check again existing ship builder before saving ship builder code', async (done) => {
          const controller = makeController({ user: mockUser });
          spyOn(controller, '_validateCode').and.returnValue(true);
          spyOn(mockUserService, 'editUser').and.returnValue(Promise.resolve({
            clientCode: null,
            flagCode: null,
            shipBuilderCode: '123123',
          }));
          controller.newCode = '123123';

          await controller.updateCode(AppSettings.ROLES.SHIP_BUILDER);
          expect(controller._validateCode).toHaveBeenCalledWith('123123', AppSettings.ROLES.SHIP_BUILDER);
          expect(mockUserService.editUser).toHaveBeenCalledWith(101, {
            clientCode: null,
            flagCode: null,
            shipBuilderCode: '123123',
          }, ['clientCode', 'flagCode']);
          expect(controller.user.clientCode).toEqual(null);
          expect(controller.user.shipBuilderCode).toEqual('123123');
          expect(controller.user.flagCode).toEqual(null);
          expect(controller.newCode).toBe(null);
          expect(controller.sections[1].fields[0].value).toEqual(null);
          expect(controller.sections[1].fields[1].value).toEqual('123123');
          expect(controller.sections[1].fields[2].value).toEqual(null);
        });

        it.async('should check again existing flag before saving flag code', async (done) => {
          const controller = makeController({ user: mockUser });
          spyOn(controller, '_validateCode').and.returnValue(true);
          spyOn(mockUserService, 'editUser').and.returnValue(Promise.resolve({
            clientCode: null,
            flagCode: '123123',
            shipBuilderCode: null,
          }));
          controller.newCode = '123123';

          await controller.updateCode(AppSettings.ROLES.FLAG);
          expect(controller._validateCode).toHaveBeenCalledWith('123123', AppSettings.ROLES.FLAG);
          expect(mockUserService.editUser).toHaveBeenCalledWith(101, {
            clientCode: null,
            flagCode: '123123',
            shipBuilderCode: null,
          }, ['clientCode', 'shipBuilderCode']);
          expect(controller.user.clientCode).toEqual(null);
          expect(controller.user.shipBuilderCode).toEqual(null);
          expect(controller.user.flagCode).toEqual('123123');
          expect(controller.newCode).toBe(null);
          expect(controller.sections[1].fields[0].value).toEqual(null);
          expect(controller.sections[1].fields[1].value).toEqual(null);
          expect(controller.sections[1].fields[2].value).toEqual('123123');
        });

        it.async('returns rejected promise if no clients match', async (done) => {
          const controller = makeController({ user: mockUser });
          controller.newCode = '123123';
          spyOn(mockClientService, 'getClientsByIMONumber').and.returnValue(Promise.resolve([]));
          controller.updateCode(AppSettings.ROLES.CLIENT)
            .catch(() => {
              expect(controller.newCode).toBe('123123');
              done();
            });
        });
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AdminUserDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AdminUserDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AdminUserDetailsController);
    });
  });
});
