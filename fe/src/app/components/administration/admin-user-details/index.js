import adminUserDetailsComponent from './admin-user-details.component';
import angular from 'angular';
import infoCardModule from 'app/common/info-card';
import uiRouter from 'angular-ui-router';

export default angular.module('adminUserDetailsComponent', [
  uiRouter,
  infoCardModule,
])
.component('adminUserDetails', adminUserDetailsComponent)
.name;
