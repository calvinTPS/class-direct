import './admin-user-details.scss';
import controller from './admin-user-details.controller';
import template from './admin-user-details.pug';

export default {
  bindings: {
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
