import * as _ from 'lodash';
import Base from 'app/base';
import editClientCodeModal from './_partials/edit-client-code-modal.pug';
import editFlagCodeModal from './_partials/edit-flag-code-modal.pug';
import editShipBuilderCodeModal from './_partials/edit-ship-builder-code-modal.pug';

export default class AdminUserDetailsController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $scope,
    $state,
    AppSettings,
    ClientService,
    FlagService,
    UserService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.errorObject = {};
  }

  get sections() {
    if (!this.user) {
      return [];
    }

    if (this._sections == null) {
      this._sections = [
        {
          fields: [
            { label: 'cd-name', value: this.user.fullName },
            { label: 'cd-account-type', value: this.user.roles, filter: 'roleTranslate', isArray: true },
            { label: 'cd-email', value: this.user.email },
            { label: 'cd-telephone', value: this.user.company.telephone },
          ],
        }, {
          fields: [{
            label: 'cd-edit-client-code',
            value: this.user.clientCode,
            editable: this.user.isLRInternal,
            onClick: () => {
              this.showUpdateCodeModal(this._AppSettings.ROLES.CLIENT);
            },
          }, {
            label: 'cd-edit-ship-builder-code',
            value: this.user.shipBuilderCode,
            editable: this.user.isLRInternal,
            onClick: () => {
              this.showUpdateCodeModal(this._AppSettings.ROLES.SHIP_BUILDER);
            },
          }, {
            label: 'cd-edit-flag-code',
            value: this.user.flagCode,
            editable: this.user.isLRInternal,
            onClick: () => {
              this.showUpdateCodeModal(this._AppSettings.ROLES.FLAG);
            },
          }],
        },
      ];
    }

    return this._sections;
  }

  set sections(sections) {
    this._sections = sections;
  }

  showUpdateCodeModal(selectedCodeType) {
    this.selectedCodeType = selectedCodeType;
    let editCodeModal;
    if (selectedCodeType === this._AppSettings.ROLES.CLIENT) {
      editCodeModal = editClientCodeModal;
    } else if (selectedCodeType === this._AppSettings.ROLES.SHIP_BUILDER) {
      editCodeModal = editShipBuilderCodeModal;
    } else if (selectedCodeType === this._AppSettings.ROLES.FLAG) {
      editCodeModal = editFlagCodeModal;
    }

    return this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: false,
      controller: () => this,
      controllerAs: 'vm',
      parent: angular.element(this._$document[0].body),
      template: editCodeModal,
    });
  }

  confirmCodeChange(form, selectedCodeType) {
    if (form.$valid) {
      this.updateCode(selectedCodeType)
        .then(() => {
          // All good, hide modal and reload state to
          // reflect subfleet changes.
          this._$mdDialog.hide();
          this._$state.reload();
        })
        .catch(() => {
          // Show error
          this.errorObject['client-code'] = true;
          this.errorObject['ship-builder-code'] = true;
          this.errorObject['flag-code'] = true;
        });
    }
  }

  cancelCodeChange() {
    this.newCode = null;
    this.errorObject = {};
    this._$mdDialog.cancel();
  }

  async _validateCode(newCode, selectedCodeType) {
    let response = [];
    if (newCode == null) {
      return false;
    }

    if (selectedCodeType === this._AppSettings.ROLES.CLIENT) {
      response = await this._ClientService.getClientsByIMONumber(newCode, false);
    } else if (selectedCodeType === this._AppSettings.ROLES.SHIP_BUILDER) {
      response = await this._ClientService.getShipBuilderByIMONumber(newCode, false);
    } else if (selectedCodeType === this._AppSettings.ROLES.FLAG) {
      this.newCode = this.newCode.toUpperCase();
      response = await this._FlagService.getFlagByFlagCode(newCode.toUpperCase());
    }

    if (response.length === 0) {
      return false;
    }

    return true;
  }

  async updateCode(selectedCodeType) {
    if (this.newCode && !await this._validateCode(this.newCode, selectedCodeType)) {
      return Promise.reject();
    }

    const editUserPayload = {};
    const selectedCodeField = this._AppSettings.DELETABLE_USER_FIELDS[selectedCodeType];
    let deleteFields = [];

    if (_.get(this, `user.${selectedCodeField}`) !== this.newCode) {
      if (this.newCode) {
        editUserPayload[selectedCodeField] = this.newCode;
        deleteFields = _.values(_.omit(this._AppSettings.DELETABLE_USER_FIELDS, selectedCodeType));
        _.forEach(deleteFields, (field) => {
          editUserPayload[field] = null;
        });
      } else {
        deleteFields = _.values(this._AppSettings.DELETABLE_USER_FIELDS);
      }

      const userObj = await this._UserService.editUser(
        this.user.userId,
        editUserPayload,
        deleteFields
      );

      this.newCode = null;
      _.forEach(this._AppSettings.DELETABLE_USER_FIELDS, (field) => {
        _.set(this, `user.${field}`, _.get(userObj, field, null));
      });
    }

    // Force view update
    this._sections = null;
    this.errorObject = {};
    return Promise.resolve();
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
