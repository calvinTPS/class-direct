/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, one-var-declaration-per-line,
  sort-vars, angular/window-service, no-underscore-dangle,  no-undef */
import * as _ from 'lodash';
import AccessibleAndRestrictedAssetsComponent from './accessible-and-restricted-assets.component';
import AccessibleAndRestrictedAssetsController from './accessible-and-restricted-assets.controller';
import AccessibleAndRestrictedAssetsModule from './';
import AccessibleAndRestrictedAssetsTemplate from './accessible-and-restricted-assets.pug';
import mockAppSettings from 'app/config/project-variables.js';
import UserService from 'app/services/user.service';

describe('AccessibleAndRestrictedAssets', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    $timeout,
    makeController,
    mockUserService;

  const mockUser = {
    id: 1,
    userId: 101,
    equasis: true,
    thetis: false,
    flagCode: 100,
    shipBuilderCode: 1,
    clientCode: 4,
    name: 'John Doe',
    email: 'abc@bae.com',
    telephone: '07865432',
    city: 'WP',
    postCode: '50470',
    status: 1,
    company: 'Lloyds Register',
    address1: 'Address Line 1',
    address2: 'Address Line 2',
    address3: 'Address Line 3',
    country: 'UK',
  };

  beforeEach(window.module(AccessibleAndRestrictedAssetsModule));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    mockUserService = {
      updateSubFleet: () => new Promise((resolve, reject) => resolve()),
      getAccessibleAssets: () => [{ id: 1 }, { id: 2 }, { id: 3 }],
      getRestrictedAssets: () => [{ id: 4 }, { id: 5 }, { id: 6 }],
      getCurrentUser: () => Promise.resolve({
        userId: 999,
      }),
    };
    const mockDashboardService = {
      hasAssetsInitialised: value => value,
      assetsSearchTerm: value => value,
    };
    const mockMdDialog = function mdDialog() {
      return {
        alert: (arg) => {},
        build: (arg) => {},
        cancel: function cancel(reason, options) {},
        confirm: (arg) => {},
        destroy: function destroyInterimElement(opts) {},
        hide: function hide(reason, options) {},
        prompt: (arg) => {},
        show: function showInterimElement(opts) {
          return new Promise((resolve, reject) => resolve());
        },
      };
    };

    window.module(($provide) => {
      $provide.service('$mdDialog', mockMdDialog); // eslint-disable-line angular/no-service-method
      $provide.value('$mdMedia', {});
      $provide.value('AppSettings', mockAppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('UserService', mockUserService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$timeout_, $componentController, $document) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $timeout = _$timeout_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('accessibleAndRestrictedAssets', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        accessibleAssets: [{ id: 1 }, { id: 2 }],
        restrictedAssets: [{ id: 4 }, { id: 5 }],
        user: mockUser,
      });
      expect(controller.user).toEqual(mockUser);
      expect(controller.tabSelected).toEqual(0);

      expect(controller.showEditAccessible).toEqual(true);
      expect(controller.showEditRestricted).toEqual(true);

      expect(controller.accessibleIsPristine).toEqual(true);
      expect(controller.restrictedIsPristine).toEqual(true);

      expect(controller.accessibleAssetIds).toEqual(undefined);
      expect(controller.restrictedAssetIds).toEqual(undefined);

      expect(controller.restrictedAssetsLoaded).toEqual(false);
    });


    it('tabSelected getter/setter should get and set correct value for the variable', () => {
      // controller.tabSelected = 0 OnInit
      // controller.restrictedAssetsLoaded = false  OnInit
      const controller = makeController({
        user: mockUser,
      });
      controller.restrictedAssetsLoaded = true;
      spyOn(controller, '_getRestrictedAssets');
      expect(controller.tabSelected).toEqual(0);
      expect(controller._getRestrictedAssets).not.toHaveBeenCalled();

      controller.tabSelected = 1;
      expect(controller.tabSelected).toEqual(1);
      expect(controller._getRestrictedAssets).not.toHaveBeenCalled();

      controller.restrictedAssetsLoaded = false;
      controller.tabSelected = 1;
      expect(controller._getRestrictedAssets).toHaveBeenCalled();
    });

    it('_getRestrictedAssets() should get the restricted asset', (done) => {
      const controller = makeController({
        user: { userId: 101 },
      });
      spyOn(controller._UserService, 'getRestrictedAssets').and.returnValue(
        {
          then: () => Promise.resolve([{ id: 4 }, { id: 5 }, { id: 6 }]),
        }
      );

      expect(controller.restrictedAssets).toEqual(undefined);
      expect(controller.restrictedAssetsLoaded).toEqual(false);

      controller._getRestrictedAssets();
      done();
      expect(controller._UserService.getRestrictedAssets).toHaveBeenCalledWith(101);
      expect(controller.restrictedAssets).toEqual([{ id: 4 }, { id: 5 }, { id: 6 }]);
      expect(controller.restrictedAssetsLoaded).toEqual(true);
    });

    it.async('toggleAllAccessible() should toggle includeAllAccessible to new boolean value', async() => {
      const controller = await makeController({
        $timeout,
        user: { userId: 101 },
      });
      controller.includeAllAccessible = false;

      controller.toggleAllAccessible(true);
      controller._$timeout.flush();
      expect(controller.includeAllAccessible).toEqual(true);

      controller.toggleAllAccessible(false);
      controller._$timeout.flush();
      expect(controller.includeAllAccessible).toEqual(false);
    });

    it.async('toggleAllRestricted() should toggle includeAllRestricted to new boolean value', async() => {
      const controller = await makeController({
        $timeout,
        user: { userId: 101 },
      });
      controller.includeAllRestricted = false;

      controller.toggleAllRestricted(true);
      controller._$timeout.flush();
      expect(controller.includeAllRestricted).toEqual(true);

      controller.toggleAllRestricted(false);
      controller._$timeout.flush();
      expect(controller.includeAllRestricted).toEqual(false);
    });

    it.async('closeEditable() should close/disable editables and clear all selections', async () => {
      const controller = await makeController({
        user: { userId: 101 },
        accessibleAssets: [{ id: 1 }, { id: 2 }],
        accessibleAssetIds: [1, 2, 3],
        restrictedAssets: [{ id: 4 }, { id: 5 }],
        restrictedAssetIds: [4, 5, 6],
      });

      controller.editAccessible = true;
      controller.selectedAccessibleIds = [1, 2, 3];
      controller.editRestricted = true;
      controller.selectedRestrictedIds = [4, 5, 6];
      controller.closeEditable();
      expect(controller.showEditAccessible).toEqual(true);
      expect(controller.showEditRestricted).toEqual(true);
      _.forEach(controller.accessibleAssets, assets =>
        expect(assets.selected).toEqual(false)
      );
      _.forEach(controller.restrictedAssets, assets =>
        expect(assets.selected).toEqual(false)
      );
      expect(controller.includeAllRestricted).toEqual(false);
      expect(controller.includeAllAccessible).toEqual(false);
    });

    it.async('_updateSubFleet() should call UserService.updateSubFleet() with the right paramaters and set editable=false', async () => {
      const controller = await makeController({
        user: { userId: 101 },
        currentUser: { userId: 102 },
      });
      spyOn(controller._UserService, 'updateSubFleet').and.returnValue(true);

      const includedAccessibleAssetIds = [1, 2];
      await controller._updateSubFleet({ includedAccessibleAssetIds });
      expect(controller._UserService.updateSubFleet)
        .toHaveBeenCalledWith(101, UserService.MOVE_ASSETS_TO.ACCESSIBLE,
          includedAccessibleAssetIds, null);

      const excludedAccessibleAssetIds = [1, 2];
      await controller._updateSubFleet({ excludedAccessibleAssetIds });
      expect(controller._UserService.updateSubFleet)
        .toHaveBeenCalledWith(101, UserService.MOVE_ASSETS_TO.ACCESSIBLE,
          null, excludedAccessibleAssetIds);

      const includedRestrictedAssetIds = [1, 2];
      await controller._updateSubFleet({ includedRestrictedAssetIds });
      expect(controller._UserService.updateSubFleet)
        .toHaveBeenCalledWith(101, UserService.MOVE_ASSETS_TO.ACCESSIBLE,
          includedRestrictedAssetIds, null);

      const excludedRestrictedAssetIds = [1, 2];
      await controller._updateSubFleet({ excludedRestrictedAssetIds });
      expect(controller._UserService.updateSubFleet)
        .toHaveBeenCalledWith(101, UserService.MOVE_ASSETS_TO.ACCESSIBLE,
          null, excludedRestrictedAssetIds);

      expect(controller.showEditAccessible).toEqual(true);
      expect(controller.showEditRestricted).toEqual(true);
    });

    it.async('_convertToInt() should convert array of LRV id strings to array of integer', async () => {
      const controller = await makeController({
        user: { userId: 101 },
      });
      expect(controller._convertToInt(['LRV1', 'LRV2', 'LRV3'])).toEqual([1, 2, 3]);
    });

    it.async('_moveToRestricted() should call _updateSubFleet() with the right parameters', async () => {
      const controller = await makeController({
        user: { userId: 101 },
        accessibleAssets: [{ id: 1 }, { id: 2 }],
        restrictedAssets: [{ id: 4 }, { id: 5 }],
      });
      spyOn(controller, '_updateSubFleet');

      await controller._moveToRestricted({
        includedAssets: ['LRV1', 'LRV2'],
        excludedAssets: [],
      });
      expect(controller._updateSubFleet).toHaveBeenCalledWith({
        includedRestrictedAssetIds: [1, 2],
      });

      await controller._moveToRestricted({
        includedAssets: [],
        excludedAssets: ['LRV1', 'LRV2'],
      });
      expect(controller._updateSubFleet).toHaveBeenCalledWith({
        excludedRestrictedAssetIds: [1, 2],
      });

      // If both lists empty, send empty excluded list.
      await controller._moveToRestricted({
        includedAssets: [],
        excludedAssets: [],
      });
      expect(controller._updateSubFleet).toHaveBeenCalledWith({
        excludedRestrictedAssetIds: [],
      });
    });

    it.async('_moveToAccessible() should call _updateSubFleet() with the right parameters', async () => {
      const controller = await makeController({
        user: { userId: 101 },
        accessibleAssets: [{ id: 1 }, { id: 2 }],
        restrictedAssets: [{ id: 4 }, { id: 5 }],
      });
      spyOn(controller, '_updateSubFleet');

      await controller._moveToAccessible({
        includedAssets: ['LRV4', 'LRV5'],
        excludedAssets: [],
      });
      expect(controller._updateSubFleet).toHaveBeenCalledWith({
        includedAccessibleAssetIds: [4, 5],
      });

      await controller._moveToAccessible({
        includedAssets: [],
        excludedAssets: ['LRV4', 'LRV5'],
      });
      expect(controller._updateSubFleet).toHaveBeenCalledWith({
        excludedAccessibleAssetIds: [4, 5],
      });

      // If both lists empty, send empty excluded list.
      await controller._moveToAccessible({
        includedAssets: [],
        excludedAssets: [],
      });
      expect(controller._updateSubFleet).toHaveBeenCalledWith({
        excludedAccessibleAssetIds: [],
      });
    });

    it.async('showModal() should call $mdDialog, and submit button executes _moveToRestricted', async() => {
      const controller = await makeController({
        user: { userId: 101 },
        accessibleAssets: [{ id: 1 }, { id: 2 }],
        accessibleAssetIds: [1, 2, 3],
        restrictedAssets: [{ id: 4 }, { id: 5 }],
        restrictedAssetIds: [4, 5, 6],
      });
      spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve());
      spyOn(controller, '_moveToRestricted');
      spyOn(controller, '_moveToAccessible');

      controller.selectedAccessibleIds = {
        includedAssets: [1, 2],
        excludedAssets: [],
      };
      await controller.showModal(Object, true);
      expect(controller._$mdDialog.show).toHaveBeenCalled();
      expect(controller._moveToRestricted).toHaveBeenCalledWith({
        includedAssets: [1, 2],
        excludedAssets: [],
      });
      expect(controller._moveToAccessible).not.toHaveBeenCalled();
    });

    it.async('showModal() should call $mdDialog, and submit button executes _moveToAccessible()', async() => {
      const controller = await makeController({
        user: { userId: 101 },
        accessibleAssets: [{ id: 1 }, { id: 2 }],
        accessibleAssetIds: [1, 2, 3],
        restrictedAssets: [{ id: 4 }, { id: 5 }],
        restrictedAssetIds: [4, 5, 6],
      });
      spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve());
      spyOn(controller, '_moveToRestricted');
      spyOn(controller, '_moveToAccessible');

      controller.selectedRestrictedIds = {
        includedAssets: [4, 5],
        excludedAssets: [],
      };
      await controller.showModal(Object, false);
      expect(controller._$mdDialog.show).toHaveBeenCalled();
      expect(controller._moveToRestricted).not.toHaveBeenCalled();
      expect(controller._moveToAccessible).toHaveBeenCalledWith({
        includedAssets: [4, 5],
        excludedAssets: [],
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AccessibleAndRestrictedAssetsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AccessibleAndRestrictedAssetsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AccessibleAndRestrictedAssetsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<accessible-and-restricted-assets foo="bar"><accessible-and-restricted-assets/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('AccessibleAndRestrictedAssets');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
