import './accessible-and-restricted-assets.scss';
import controller from './accessible-and-restricted-assets.controller';
import template from './accessible-and-restricted-assets.pug';

export default {
  bindings: {
    accessibleAssets: '<',
    currentUser: '<',
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
