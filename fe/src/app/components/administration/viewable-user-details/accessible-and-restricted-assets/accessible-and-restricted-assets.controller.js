import * as _ from 'lodash';
import Base from 'app/base';
import generalDialogTemplate from 'app/common/dialogs/general.pug';
import GeneralModalController from 'app/common/dialogs/general-modal.controller';
import UserServiceClass from 'app/services/user.service';

export default class AccessibleAndRestrictedAssetsController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $scope,
    $state,
    $stateParams,
    $timeout,
    AppSettings,
    DashboardService,
    UserService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this.tabSelected = 0; // Initial tab selection

    this.showEditAccessible = true;
    this.showEditRestricted = true;

    this.selectedAccessibleIds = [];
    this.selectedRestrictedIds = [];

    this.accessibleIsPristine = true;
    this.restrictedIsPristine = true;

    this.restrictedAssetsLoaded = false;
  }

  get tabSelected() {
    return this._tabSelected;
  }

  set tabSelected(i) {
    if (i === 1 && !this.restrictedAssetsLoaded) {
      this._getRestrictedAssets();
    }
    this._tabSelected = i;
  }

  _getRestrictedAssets() {
    this._UserService.getRestrictedAssets(this.user.userId).then((restrictedAssets) => {
      this.restrictedAssets = restrictedAssets;
      this.restrictedAssetsLoaded = true;
      this._$scope.$apply();
    });
  }

  toggleAllAccessible(bool) {
    const tmp = !!this.includeAllAccessible;
    // we set to null and then toggle after short delay to activate the setter function
    this.includeAllAccessible = null;
    this._$timeout(() => {
      this.includeAllAccessible = tmp !== bool ? !tmp : bool;
    });
  }

  toggleAllRestricted(bool) {
    const tmp = !!this.includeAllRestricted;
    // we set to null and then toggle after short delay to activate the setter function
    this.includeAllRestricted = null;
    this._$timeout(() => {
      this.includeAllRestricted = tmp !== bool ? !tmp : bool;
    });
  }

  /**
    * This should close the editable and clear all selections
    */
  closeEditable() {
    this.showEditAccessible = true;
    this.showEditRestricted = true;

    _.forEach(this.accessibleAssets, (asset) => {
      asset.selected = false; // eslint-disable-line no-param-reassign
    });

    _.forEach(this.restrictedAssets, (asset) => {
      asset.selected = false; // eslint-disable-line no-param-reassign
    });

    this.includeAllRestricted = false;
    this.includeAllAccessible = false;
  }

  /**
    * Update Subfleet.
    *
    * @param {Object} idsObj - Object with restricted/accessible ids to include/exclude.
    * @param {Array<Number>} idsObj.includedAccessibleAssetIds
    * @param {Array<Number>} idsObj.excludedAccessibleAssetIds
    * @param {Array<Number>} idsObj.includedRestrictedAssetIds
    * @param {Array<Number>} idsObj.excludedRestrictedAssetIds
    */
  async _updateSubFleet(idsObj) {
    if (_.size(idsObj) !== 1) {
      throw new Error('Expected exactly one property in idsObj');
    }

    let action;
    if (idsObj.includedAccessibleAssetIds || idsObj.excludedAccessibleAssetIds) {
      action = UserServiceClass.MOVE_ASSETS_TO.ACCESSIBLE;
    } else if (idsObj.includedRestrictedAssetIds || idsObj.excludedRestrictedAssetIds) {
      action = UserServiceClass.MOVE_ASSETS_TO.RESTRICTED;
    } else {
      throw new Error('Unexpected property in idsObj');
    }

    // Update backend
    this.isLoading = true;
    const response =
      await this._UserService.updateSubFleet(
        this.user.userId,
        action,
        idsObj.includedAccessibleAssetIds || idsObj.includedRestrictedAssetIds || null,
        idsObj.excludedAccessibleAssetIds || idsObj.excludedRestrictedAssetIds || null,
      );

    if (response) {
      [this.accessibleAssets, this.restrictedAssets] = await Promise.all([
        this._UserService.getAccessibleAssets(this.user.userId),
        this._UserService.getRestrictedAssets(this.user.userId),
      ]);

      this.isLoading = false;
      this.closeEditable();

      // If user is updating their own fleet, we will force it to query when going to
      // fleet list/Service Schedule so table view will display correctly
      if (this.currentUser.userId === this.user.userId) {
        this._DashboardService.forceQuery = true;
      }

      this._$scope.$apply();
    }
  }

  /**
   * Backend requires list of integers insted of strings. Since
   * we append LRV for all LR assets, we just remove them and
   * convert to number
   * @param {Array} idsArray - Sample: ['LRV1', 'LRV2', ..]
   *
   * @return {Array} - Sample: [1, 2, ..]
  */
  _convertToInt(idsArray) {
    return idsArray.map(idStr => Number(idStr.replace('LRV', '')));
  }

  /**
    * This will update subfleet by restricting assets based on included/excluded items.

    * @param {Array} selectedIds - An object {
    *                                 includedAssets: ['LRV1', 'LRV2', ..],
    *                                 excludedAssets: []
    *                               }
    */
  async _moveToRestricted(selectedIds) {
    const includedIds = this._convertToInt(selectedIds.includedAssets);
    const excludedIds = this._convertToInt(selectedIds.excludedAssets);

    if (includedIds.length && excludedIds.length) {
      throw new Error('Only included or excluded IDs should contain elements, not both');
    }

    // If included ids is present, send only that.
    if (includedIds.length) {
      await this._updateSubFleet({
        includedRestrictedAssetIds: includedIds,
      });

      this._$scope.$apply();
    } else {
      await this._updateSubFleet({
        excludedRestrictedAssetIds: excludedIds, // may be empty
      });

      this._$scope.$apply();
    }
  }

  /**
    * This will update subfleet by making assets accessible
    * based on included/excluded items.
    *
    * @param {Array} selectedIds - An object {
    *                                 includedAssets: ['LRV1', 'LRV2', ..],
    *                                 excludedAssets: []
    *                               }
    */
  async _moveToAccessible(selectedIds) {
    const includedIds = this._convertToInt(selectedIds.includedAssets);
    const excludedIds = this._convertToInt(selectedIds.excludedAssets);

    if (includedIds.length && excludedIds.length) {
      throw new Error('Only included or excluded IDs should contain elements, not both');
    }

    // If included ids is present, send only that.
    if (includedIds.length) {
      await this._updateSubFleet({
        includedAccessibleAssetIds: includedIds,
      });

      this._$scope.$apply();
    } else {
      await this._updateSubFleet({
        excludedAccessibleAssetIds: excludedIds, // may be empty
      });

      this._$scope.$apply();
    }
  }

  /**
    * This will pop up a modal box by using angular material $mdDialog and mainly
    * modified by the UI team. It returns promise when user click confirm and
    * executes $mdDialog.hide(). Then it will be handled by .then() to move it to
    * restricted or accessible.
    * @param {Event} ev - $event from pug
    * @param {Boolean} isMoveToRestricted - pass true to move to restricted, else
    *                                       pass false to move to accessible.
    */
  showModal(ev, isMoveToRestricted) {
    const selectedIds = isMoveToRestricted ?
      this.selectedAccessibleIds : this.selectedRestrictedIds;

    const includeAll = isMoveToRestricted ?
      this.includeAllAccessible : this.includeAllRestricted;

    const includedIds = selectedIds.includedAssets;
    const excludedIds = selectedIds.excludedAssets;

    // If statement should pass when includeAll is true but included/exclued Ids is empty
    if (includedIds.length > 0 || excludedIds.length > 0 || includeAll) {
      this._$mdDialog.show({
        bindToController: true,
        clickOutsideToClose: true,
        controller: GeneralModalController,
        controllerAs: 'vm',
        locals: {
          title: 'cd-dialog-confirm-changes-title',
          body: 'cd-dialog-confirm-changes-body',
          buttons: [
            {
              class: 'modal-close-button',
              label: 'cd-cancel',
              onClick: 'vm._$mdDialog.cancel()',
              theme: 'light',
            },
            {
              class: 'modal-confirm-button',
              label: 'cd-confirm',
              onClick: 'vm._$mdDialog.hide()',
              theme: 'warn',
            },
          ],
        },
        parent: angular.element(this._$document[0].body),
        targetEvent: ev,
        template: generalDialogTemplate,
      }).then(() => {
        isMoveToRestricted ? // eslint-disable-line no-unused-expressions
          this._moveToRestricted(selectedIds) :
          this._moveToAccessible(selectedIds);
      });
    }
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
