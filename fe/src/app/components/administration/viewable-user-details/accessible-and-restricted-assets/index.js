import AccessibleAndRestrictedAssetsComponent from './accessible-and-restricted-assets.component';
import angular from 'angular';
import uiRouter from 'angular-ui-router';

export default angular.module('accessibleAndRestrictedAssets', [
  uiRouter,
])
.component('accessibleAndRestrictedAssets', AccessibleAndRestrictedAssetsComponent)
.name;
