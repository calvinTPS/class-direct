import './viewable-user-details.scss';
import controller from './viewable-user-details.controller';
import template from './viewable-user-details.pug';

export default {
  bindings: {
    currentUser: '<',
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
