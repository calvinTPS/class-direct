import './masquerade-user.scss';
import controller from './masquerade-user.controller';
import template from './masquerade-user.pug';

export default {
  bindings: {
    userId: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
