/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import MasqueradeUserComponent from './masquerade-user.component';
import MasqueradeUserController from './masquerade-user.controller';
import MasqueradeUserModule from './';
import MasqueradeUserTemplate from './masquerade-user.pug';

describe('MasqueradeUser', () => {
  let $rootScope,
    $compile,
    mockWindow,
    makeController;

  const mockRestangular = {
    addFullRequestInterceptor: () => null,
  };

  const mockUserService = {
    getCurrentUser: () => Promise.resolve({ id: 1, name: 'testUser' }),
    logout: () => {},
    unsetCurrentUser: () => {},
    getMasqueradeToken: () => ({ response: 'dummyToken' }),
  };

  const mockDashboardService = {
    hasAssetsInitialised: value => value,
    assetsSearchTerm: value => value,
    reset: value => value,
  };

  beforeEach(window.module(MasqueradeUserModule));

  beforeEach(() => {
    mockWindow = {
      LRCDGlobalTimedCacheManager: {
        resetCache: () => {},
      },
    };
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('Restangular', mockRestangular);
      $provide.value('$window', mockWindow);
      $provide.value('$state', {
        go: () => { },
      });
      $provide.value('UserService', mockUserService);
      $provide.value('DashboardService', mockDashboardService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('masqueradeUser', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({ foo: 'bar', userId: 123 });
    });

    it('initializes just fine', () => {
      expect(controller.foo).toEqual('bar');
    });

    it.async('startMasquerading() calls the expected methods', async () => {
      spyOn(controller._UserService, 'getMasqueradeToken')
        .and.returnValue({ response: 'dummyToken' });
      spyOn(controller._Restangular, 'addFullRequestInterceptor');
      spyOn(controller._$rootScope, '$broadcast');
      spyOn(controller._UserService, 'unsetCurrentUser');
      spyOn(controller._$state, 'go');
      spyOn(mockWindow.LRCDGlobalTimedCacheManager, 'resetCache');

      await controller.startMasquerading();

      expect(controller._UserService.getMasqueradeToken).toHaveBeenCalledWith(123);
      expect(controller._Restangular.addFullRequestInterceptor).toHaveBeenCalled();
      expect(controller._$rootScope.$broadcast)
        .toHaveBeenCalledWith(AppSettings.EVENTS.START_MASQUERADE, { masqueradedUserId: 123 });
      expect(controller._UserService.unsetCurrentUser).toHaveBeenCalled();
      expect(controller._$state.go).toHaveBeenCalledWith(AppSettings.STATES.VESSEL_LIST);
      expect(controller._UserService.isMasquerading).toBe(true);
      expect(mockWindow.LRCDGlobalTimedCacheManager.resetCache).toHaveBeenCalledTimes(1);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = MasqueradeUserComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(MasqueradeUserTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(MasqueradeUserController);
    });
  });
});
