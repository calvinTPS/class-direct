/* eslint-disable angular/window-service */
import Base from 'app/base';

export default class MasqueradeUserController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $window,
    AppSettings,
    DashboardService,
    Restangular,
    UserService,
  ) {
    super(arguments);
  }

  async startMasquerading() {
    // Uncache everything.
    this._UserService.unsetCurrentUser();
    this._DashboardService.hasAssetsInitialised = false;
    this._DashboardService.assetsSearchTerm = '';
    if (this._$window.LRCDGlobalTimedCacheManager) {
      this._$window.LRCDGlobalTimedCacheManager.resetCache();
    }

    // Get token for masquerading
    const { response } = await this._UserService.getMasqueradeToken(this.userId);

    // Set the properties inn User Service for masquerading
    this._UserService.isMasquerading = true;
    this._UserService.masqueradeId = response;

    this._Restangular.addFullRequestInterceptor(() =>
      ({
        headers: {
          [this._AppSettings.MASQUERADE_HEADER_NAME]: response,
        },
      })
    );

    // Get details(mainly to see whether the user is EOR only, see further below)
    // of the user being masqueraded
    const masqueradedUser = await this._UserService.getCurrentUser();

    this._$rootScope.$broadcast(
      this._AppSettings.EVENTS.START_MASQUERADE,
      { masqueradedUserId: this.userId }
    );
    this._DashboardService.reset();
    if (masqueradedUser.isEORUserOnly) {
      this._$state.go(this._AppSettings.STATES.EOR_VESSEL_LIST);
    } else {
      this._$state.go(this._AppSettings.STATES.VESSEL_LIST);
    }
  }
}
