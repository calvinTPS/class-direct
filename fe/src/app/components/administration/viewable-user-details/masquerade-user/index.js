import angular from 'angular';
import MasqueradeUserComponent from './masquerade-user.component';

export default angular.module('masqueradeUser', [])
.component('masqueradeUser', MasqueradeUserComponent)
.name;
