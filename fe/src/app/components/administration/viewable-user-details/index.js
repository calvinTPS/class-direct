import accessibleAndRestrictedAssets from './accessible-and-restricted-assets';
import adminAccountDetailsModule from '../admin-account-details';
import adminUserDetailsModule from '../admin-user-details';
import angular from 'angular';
import companyDetailsModule from '../company-details';
import masqueradeUserModule from './masquerade-user';
import viewableUserDetailsComponent from './viewable-user-details.component';

export default angular.module('viewableUserDetails', [
  accessibleAndRestrictedAssets,
  adminAccountDetailsModule,
  adminUserDetailsModule,
  companyDetailsModule,
  masqueradeUserModule,
])
.component('viewableUserDetails', viewableUserDetailsComponent)
.name;
