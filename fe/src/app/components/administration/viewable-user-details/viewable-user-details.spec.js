/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, no-underscore-dangle, no-unused-vars, sort-vars */

import * as _ from 'lodash';
import mockAppSettings from 'app/config/project-variables.js';
import UserService from 'app/services/user.service';
import ViewableUserDetailsComponent from './viewable-user-details.component';
import ViewableUserDetailsController from './viewable-user-details.controller';
import ViewableUserDetailsModule from './';
import ViewableUserDetailsTemplate from './viewable-user-details.pug';

describe('ViewableUserDetails', () => {
  let $compile;
  let $mdDialog;
  let $rootScope;
  let $timeout;
  let makeController;
  let mockState;
  let mockUserService;

  const mockUser = {
    id: 1,
    userId: 101,
    equasis: true,
    thetis: false,
    flagCode: 100,
    shipBuilderCode: 1,
    clientCode: 4,
    name: 'John Doe',
    email: 'abc@bae.com',
    telephone: '07865432',
    city: 'WP',
    postCode: '50470',
    status: 1,
    company: 'Lloyds Register',
    address1: 'Address Line 1',
    address2: 'Address Line 2',
    address3: 'Address Line 3',
    country: 'UK',
  };

  beforeEach(window.module(ViewableUserDetailsModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    mockUserService = {
      updateSubFleet: () => new Promise((resolve, reject) => resolve()),
      getAccessibleAssets: () => [{ id: 1 }, { id: 2 }, { id: 3 }],
      getRestrictedAssets: () => [{ id: 4 }, { id: 5 }, { id: 6 }],
      getCurrentUser: () => Promise.resolve({
        userId: 999,
      }),
    };
    mockState = {
      go: () => { },
    };
    const mockMdDialog = function mdDialog() {
      return {
        alert: (arg) => { },
        build: (arg) => { },
        cancel: function cancel(reason, options) { },
        confirm: (arg) => { },
        destroy: function destroyInterimElement(opts) { },
        hide: function hide(reason, options) { },
        prompt: (arg) => { },
        show: function showInterimElement(opts) {
          return new Promise((resolve, reject) => resolve());
        },
      };
    };

    window.module(($provide) => {
      $provide.service('$mdDialog', mockMdDialog); // eslint-disable-line angular/no-service-method
      $provide.value('$mdMedia', {});
      $provide.value('AppSettings', mockAppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('UserService', mockUserService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$timeout_, $componentController, $document) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $timeout = _$timeout_;

    const dep = {};
    makeController =
      async (bindings = {}) => {
        const controller = $componentController('viewableUserDetails', { $document }, _.assign(bindings, {
          restrictedAssetsFunc: () => Promise.resolve(),
        }));
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('initializes just fine', async () => {
      const controller = await makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it.async('correctly figures out whether can masquerade, get canMasquerade()', async () => {
      const controller = await makeController({
        user: {
          userId: 102,
          statusName: 'Active',
        },
        accessibleAssets: [{ id: 1 }, { id: 2 }],
        accessibleAssetIds: [1, 2, 3],
        restrictedAssets: [{ id: 4 }, { id: 5 }],
        restrictedAssetIds: [4, 5, 6],
        currentUser: {
          userId: 101,
          isAdmin: true,
        },
      });
      expect(controller.canMasquerade).toEqual(true);

      controller.currentUser.isAdmin = false;
      expect(controller.canMasquerade).toEqual(false);
      controller.currentUser.isAdmin = true;

      controller.user.statusName = 'Disabled';
      expect(controller.canMasquerade).toEqual(false);

      controller.user.statusName = 'Archived';
      expect(controller.canMasquerade).toEqual(false);

      controller.user.statusName = 'Active';
      expect(controller.canMasquerade).toEqual(true);

      controller.user.userId = 102;
      expect(controller.canMasquerade).toEqual(true);

      controller.user.userId = 101;
      expect(controller.canMasquerade).toEqual(false);
    });

    it.async('shouldShowManageAccessibleAndRestrictedAssetsButton getter should return the right boolean value', async () => {
      const controller = await makeController({
        user: {},
        currentUser: {},
      });
      expect(controller.shouldShowManageAccessibleAndRestrictedAssetsButton).toEqual(false);
      controller.user.isEORUser = false;
      controller.user.isLRUser = false;
      controller.currentUser.isEORUser = false;
      expect(controller.shouldShowManageAccessibleAndRestrictedAssetsButton).toEqual(true);
    });

    it.async('canViewEor getter should return the right boolean value', async () => {
      const controller = await makeController({
        user: {
          userId: 102,
          statusName: 'Active',
          roles: ['LR_ADMIN'],
        },
      });
      expect(controller.canViewEor).toEqual(false);
      controller.user.roles = ['SHIP_BUILDER'];
      expect(controller.canViewEor).toEqual(true);
      controller.user.roles = ['CLIENT'];
      expect(controller.canViewEor).toEqual(true);
      controller.user.roles = ['EOR'];
      expect(controller.canViewEor).toEqual(true);
      controller.user.roles = ['LR_ADMIN', 'SHIP_BUILDER'];
      expect(controller.canViewEor).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ViewableUserDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ViewableUserDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ViewableUserDetailsController);
    });
  });
});
