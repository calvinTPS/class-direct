import * as _ from 'lodash';
import Base from 'app/base';

export default class ViewableUserDetailsController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $scope,
    $state,
    AppSettings,
    UserService,
  ) {
    super(arguments);
  }

  get canMasquerade() {
    const hasMasqueradePermission = this.currentUser.isAdmin;
    const isUserActive = this.user.statusName === this._AppSettings.ACCOUNT_STATUS.ACTIVE;
    const isUserSame = this.user.userId === this.currentUser.userId;
    const isMasquerading = this._UserService.isMasquerading;

    return hasMasqueradePermission && isUserActive && !isUserSame && !isMasquerading;
  }

  get canViewEor() {
    return _.some(this.user.roles,
      role => _.includes(this._AppSettings.SHOW_EOR_SECTION_ROLES, role));
  }

  get shouldShowManageAccessibleAndRestrictedAssetsButton() {
    return !this.currentUser.isEORUser && (this.user.isClient || (!this.user.isLRUser && !_.get(this.user, 'isEORUser', true)));
  }
}
