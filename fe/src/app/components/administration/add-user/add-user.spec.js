/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AddUserComponent from './add-user.component';
import AddUserController from './add-user.controller';
import AddUserModule from './';
import AddUserTemplate from './add-user.pug';
import AppSettings from 'app/config/project-variables.js';
import dateHelper from 'app/common/helpers/date-helper';

describe('AddUser', () => {
  let $rootScope,
    $compile,
    $state,
    $stateParams,
    $timeout,
    makeController;
  const mockWizardHandler = {
    wizard: () => ({
      currentStepNumber: () => {},
      currentStepTitle: () => {},
      next: () => {},
      previous: () => {},
      goTo: ({
        bind: () => {},
      }),
    }),
  };

  const mockUserService = {
    createUser: () => {},
  };

  const mockFlags = [{
    id: 1,
    name: 'Albania',
    flagCode: 'ALB',
  }, {
    id: 2,
    name: 'Algeria',
    flagCode: 'ALG',
  }];

  const mockFlagService = {
    getAll: () => Promise.resolve(mockFlags),
  };

  beforeEach(window.module(AddUserModule));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('roleTranslateFilter', mockTranslateFilter);
      $provide.value('WizardHandler', mockWizardHandler);
      $provide.value('translateFilter', key => key);
      $provide.value('UserService', mockUserService);
    });
  });
  beforeEach(inject((
    _$rootScope_,
    _$compile_,
    _$state_,
    _$stateParams_,
    _$timeout_,
    $componentController
  ) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $state = _$state_;
    $stateParams = _$stateParams_;
    $timeout = _$timeout_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('addUser', {
          FlagService: mockFlagService,
          $timeout,
        }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController();
      expect(controller.isValid).toEqual(false);
      expect(controller.userModel).toEqual({});
      expect(controller.selectedUser).toEqual(undefined);
      expect(controller._payload).toEqual({});
      expect(controller.currentJourney).toEqual([{}]);
      expect(controller.progressData).toEqual([]);
      expect(controller.isEditingEORPermissions).toEqual(false);
    });

    it('initializes differently in edit mode', () => {
      const controller = makeController();
      const mockSelectedUser = {
        userId: 101,
        email: 'john.doe@client.org',
      };
      const mockSelectedAccountType = 'FLAG';

      spyOn(controller._WizardHandler, 'wizard').and.returnValue({ goTo: () => {}, currentStepNumber: () => {} });
      spyOn(controller._WizardHandler.wizard(), 'goTo');
      spyOn(controller._WizardHandler.wizard(), 'currentStepNumber').and.returnValue(1);

      $stateParams.selectedUser = mockSelectedUser;
      $stateParams.selectedAccountType = mockSelectedAccountType;
      $timeout.flush();

      expect(controller.selectedUser).toEqual(mockSelectedUser);
      expect(controller.selectedAccountType).toEqual(mockSelectedAccountType);
      expect(controller.progressData.length).toEqual(2);
      expect(controller._WizardHandler.wizard().goTo).toHaveBeenCalled();
    });

    describe('isEditingEORPermissions', () => {
      it('getter and setter works', () => {
        const controller = makeController();
        expect(controller.isEditingEORPermissions).toEqual(false);

        controller.isEditingEORPermissions = true;
        expect(controller.isEditingEORPermissions).toEqual(true);
      });
    });

    describe('isNextButtonDisabled getter should return with the right boolean', () => {
      it('return true, when currentStepTitle === SEARCH_FLAG && selectedFlag isEmpty', () => {
        const controller = makeController();
        spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('SEARCH_FLAG');

        const result = controller.isNextButtonDisabled;

        expect(controller._WizardHandler.wizard).toHaveBeenCalled();
        expect(controller._WizardHandler.wizard().currentStepTitle).toHaveBeenCalled();
        expect(result).toEqual(true);
      });

      it('return false, when currentStepTitle === SEARCH_FLAG && selectedFlag !isEmpty', () => {
        const controller = makeController();
        spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('SEARCH_FLAG');

        controller.selectedFlag = [{ test: 'sample object' }];
        const result = controller.isNextButtonDisabled;

        expect(controller._WizardHandler.wizard).toHaveBeenCalled();
        expect(controller._WizardHandler.wizard().currentStepTitle).toHaveBeenCalled();
        expect(result).toEqual(false);
      });

      it('return true, when currentStepTitle is other than SEARCH_FLAG && isValid is true', () => {
        const controller = makeController();
        spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('');

        controller.isValid = true;
        const result = controller.isNextButtonDisabled;

        expect(controller._WizardHandler.wizard).toHaveBeenCalled();
        expect(controller._WizardHandler.wizard().currentStepTitle).toHaveBeenCalled();
        expect(result).toEqual(false);
      });

      it('return false, when currentStepTitle is other than SEARCH_FLAG && isValid is false', () => {
        const controller = makeController();
        spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('');

        controller.isValid = false;
        const result = controller.isNextButtonDisabled;

        expect(controller._WizardHandler.wizard).toHaveBeenCalled();
        expect(controller._WizardHandler.wizard().currentStepTitle).toHaveBeenCalled();
        expect(result).toEqual(true);
      });
    });

    it('next() will call the right function', () => {
      const controller = makeController();
      spyOn(controller._WizardHandler, 'wizard').and.returnValue({ next: () => {}, currentStepTitle: () => 'xxx' });
      spyOn(controller._WizardHandler.wizard(), 'next');
      controller.isValid = true;
      controller.next();

      expect(controller._WizardHandler.wizard).toHaveBeenCalled();
      expect(controller._WizardHandler.wizard().next).toHaveBeenCalled();
      expect(controller.isNextButtonDisabled).toEqual(false);
    });

    it('currentStep() will call the right function and return the right value', () => {
      const controller = makeController();
      const exampleWizardCurrentStep = 10;
      const expectedStepZeroIndexed = 9;
      spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepNumber: () => {} });
      spyOn(controller._WizardHandler.wizard(), 'currentStepNumber').and.returnValue(exampleWizardCurrentStep);

      const step = controller.currentStep();

      expect(controller._WizardHandler.wizard).toHaveBeenCalled();
      expect(controller._WizardHandler.wizard().currentStepNumber).toHaveBeenCalled();
      expect(step).toEqual(expectedStepZeroIndexed);
    });

    it('should show next button when in Find Flag page', () => {
      const controller = makeController();
      spyOn(controller._WizardHandler, 'wizard').and.returnValue({ next: () => {}, currentStepTitle: () => 'SEARCH_FLAG' });
      spyOn(controller._WizardHandler.wizard(), 'next');
      controller.selectedFlag = { some: 'flag' };
      controller.next();
      expect(controller._WizardHandler.wizard).toHaveBeenCalled();
      expect(controller._WizardHandler.wizard().next).toHaveBeenCalled();
      expect(controller.isNextButtonDisabled).toBeFalsy();
    });

    it('insertProgressData(obj) should insert obj into progress data array based on current step', () => {
      const controller = makeController();
      const sampleObject = { test: 'sample object' };
      const stepNumber = 10;
      spyOn(controller, 'currentStep').and.returnValue(stepNumber);

      controller.insertProgressData(sampleObject);

      expect(controller.currentStep).toHaveBeenCalled();
      expect(controller.progressData[stepNumber]).toEqual(sampleObject);
    });

    it('deleteProgressData(obj) should delete objects from progress data array based on current step', () => {
      const controller = makeController();
      const sampleObject = { test: 'sample object' };
      const stepNumber = 3; // Zero indexed
      spyOn(controller, 'currentStep').and.returnValue(stepNumber);

      controller.progressData = [
        { test0: 'sample data 0' },
        { test1: 'sample data 1' },
        { test2: 'sample data 2' },
        { test3: 'sample data 3' },
        { test4: 'sample data 4' },
        { test5: 'sample data 5' },
      ];
      controller.deleteProgressData();

      expect(controller.currentStep).toHaveBeenCalled();
      expect(controller.progressData).toEqual([
        { test0: 'sample data 0' },
        { test1: 'sample data 1' },
        { test2: 'sample data 2' },
        { test3: 'sample data 3' },
      ]);
    });

    describe('isConfirmation getter should return the right boolean value', () => {
      let controller;
      beforeEach(() => {
        controller = makeController();
        spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
      });

      it('returns true, when currentStepTitle === CONFIRMATION', () => {
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('CONFIRMATION');
        const result = controller.isConfirmation;
        expect(result).toEqual(true);
      });

      it('returns false, when currentStepTitle !== CONFIRMATION', () => {
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('');
        const result = controller.isConfirmation;
        expect(result).toEqual(false);
      });
    });

    describe('isPermissions getter should return the right boolean value', () => {
      let controller;
      beforeEach(() => {
        controller = makeController();
        spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
      });

      it('returns true, when currentStepTitle === PERMISSIONS', () => {
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('PERMISSIONS');
        const result = controller.isPermissions;
        expect(result).toEqual(true);
      });

      it('returns false, when currentStepTitle !== PERMISSIONS', () => {
        spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('');
        const result = controller.isPermissions;
        expect(result).toEqual(false);
      });
    });

    it('selectedUser setter/getter should update properly and update progressData', () => {
      const controller = makeController();
      const sampleUser = { fullname: 'Full Name', email: 'email@email.io' };
      const stepNumber = 0;
      spyOn(controller, 'currentStep').and.returnValue(stepNumber);
      spyOn(controller, 'insertProgressData');
      spyOn(controller, 'deleteProgressData');

      // Test passing an object to selectedUser
      controller.selectedUser = sampleUser;

      expect(controller.selectedUser).toEqual(sampleUser);
      expect(controller.currentStep).toHaveBeenCalled();
      expect(controller.insertProgressData).toHaveBeenCalledWith({
        lines: [sampleUser.fullName, sampleUser.email],
        onClick: controller._WizardHandler.wizard().goTo.bind(controller, stepNumber),
      });
      expect(controller.deleteProgressData).toHaveBeenCalled();

      // Test when delete/clear selectedUser
      controller.selectedUser = undefined;
      expect(controller.progressData).toEqual([]);
    });

    it('userToCreate(obj) should update selectedUser and called next()', () => {
      const controller = makeController();
      const sampleUser = { fullname: 'Full Name', email: 'email@email.io' };
      spyOn(controller, 'next');
      spyOn(controller, 'currentStep').and.returnValue(0);

      controller.userToCreate(sampleUser);

      expect(controller.selectedUser).toEqual(sampleUser);
      expect(controller.next).toHaveBeenCalled();
    });

    it('userToEdit(obj) should redirect to another state', () => {
      const controller = makeController();
      const sampleUser = { userId: 1, fullname: 'Full Name', email: 'email@email.io' };
      spyOn(controller._$state, 'go');

      controller.userToEdit(sampleUser);
      expect(controller._$state.go).toHaveBeenCalledWith('administration.userDetails', { id: 1 });
    });

    it('setAccountType() set selectedAccountType and update progressData', () => {
      const controller = makeController();
      const sampleAccTypeString = 'CLIENT';
      const stepNumber = 0;
      spyOn(controller, 'currentStep').and.returnValue(stepNumber);
      spyOn(controller, 'insertProgressData');
      spyOn(controller, 'deleteProgressData');
      spyOn(controller, 'next');

      // Test passing a string to setAccountType()
      controller.setAccountType(sampleAccTypeString);

      expect(controller.selectedAccountType).toEqual(sampleAccTypeString);
      expect(controller.currentJourney).toEqual([
        { key: 'SEARCH_CLIENT', title: 'cd-select-a-client', component: 'search-client' },
        { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
        { key: 'CONFIRMATION', component: 'confirmation' },
      ]);

      expect(controller.currentStep).toHaveBeenCalled();
      expect(controller.insertProgressData).toHaveBeenCalledWith({
        lines: ['CLIENT'],
        onClick: controller._WizardHandler.wizard().goTo.bind(controller, stepNumber),
      });
      expect(controller.deleteProgressData).toHaveBeenCalled();
      expect(controller.next).toHaveBeenCalled();

      const flagAccTypeString = 'FLAG';
      spyOn(controller._FlagService, 'getAll').and.returnValue(Promise.resolve({}));

      controller.setAccountType(flagAccTypeString);

      expect(controller.selectedAccountType).toEqual(flagAccTypeString);
      expect(controller.currentJourney).toEqual([
        { key: 'SEARCH_FLAG', title: 'cd-select-a-flag', component: 'search-flag' },
        { key: 'PERMISSIONS', title: 'cd-permissions', component: 'permissions' },
        { key: 'CONFIRMATION', component: 'confirmation' },
      ]);
      expect(controller._FlagService.getAll).toHaveBeenCalled();
      expect(controller.flags).toEqual(undefined);
      expect(controller.next).toHaveBeenCalled();
    });

    it('setSelectedFlag should contain proper object', () => {
      const controller = makeController();
      const mockFlagObj = {
        id: 1,
        flagType: 'string',
      };

      controller.setSelectedFlag(mockFlagObj);
      expect(controller.selectedFlag).toEqual(mockFlagObj);
    });

    describe('populatePayload()', () => {
      it('should populate proper payload when accountType is EOR', () => {
        const controller = makeController();
        controller.selectedAccountType = 'EOR';
        controller.userModel = {
          registeredPermissionAsset: {
            imo: 1002345,
            id: 1,
          },
          assetExpiryDate: '2016-09-20',
          tmReport: false,
        };
        controller.selectedUser = {
          userId: '1111-2222-3333-444',
          email: 'john.doe@client.org',
          role: [],
          flagCode: 10,
        };

        spyOn(dateHelper, 'formatDateServer').and.returnValue('2016-09-20');

        controller.populatePayload();
        expect(dateHelper.formatDateServer).toHaveBeenCalledWith('2016-09-20');
        expect(controller._payload).toEqual({
          userId: '1111-2222-3333-444',
          email: controller.selectedUser.email,
          role: 'EOR',
          eor: [{
            imoNumber: 1002345,
            assetExpiry: '2016-09-20',
            tmReport: false,
          }],
        });
      });

      it('should populate proper payload when accountType is CLIENT', () => {
        const controller = makeController();
        controller.selectedAccountType = 'CLIENT';
        controller.selectedClient = {
          code: 'ClientCode',
          relationship: 'Relationship',
        };
        controller.userModel = {
          accountExpiry: '2016-09-20',
        };
        controller.selectedUser = {
          userId: '1111-2222-3333-444',
          email: 'john.doe@client.org',
        };

        spyOn(dateHelper, 'formatDateServer').and.returnValue('2016-09-20');

        controller.populatePayload();
        expect(dateHelper.formatDateServer).toHaveBeenCalledWith('2016-09-20');
        expect(controller._payload).toEqual({
          userId: '1111-2222-3333-444',
          email: controller.selectedUser.email,
          role: 'CLIENT',
          clientCode: 'ClientCode',
          clientType: 'Relationship',
          accountExpiry: '2016-09-20',
        });
      });

      it('should populate proper payload when accountType is SHIP_BUILDER', () => {
        const controller = makeController();
        controller.selectedAccountType = 'SHIP_BUILDER';
        controller.selectedUser = {
          userId: '1111-2222-3333-444',
          email: 'john.doe@client.org',
        };
        controller.selectedShipBuilder = {
          code: '12345678',
        };
        controller.populatePayload();
        expect(controller._payload).toEqual({
          userId: '1111-2222-3333-444',
          email: controller.selectedUser.email,
          role: 'SHIP_BUILDER',
          shipBuilderCode: '12345678',
        });
      });
    });

    it.async('createUser() should called on submit', async () => {
      const controller = makeController();
      spyOn(controller, 'populatePayload');
      spyOn(controller._UserService, 'createUser');
      controller.userModel = {
        registeredPermissionAsset: {
          id: 1,
        },
        assetExpiryDate: '2016-09-20',
        tmReport: false,
      };
      controller.selectedUser = {
        userId: '1111-2222-3333-444',
        email: 'john.doe@client.org',
        role: [],
        flagCode: 10,
      };

      await controller.createUser();
      expect(controller.populatePayload).toHaveBeenCalled();
      expect(controller._UserService.createUser).toHaveBeenCalledWith(
        AppSettings.API_ENDPOINTS.Users,
        controller._payload,
      );
    });

    it('setSelectedClient should update with the right variables and functions', () => {
      const controller = makeController();
      const sampleClient = {
        code: '123456',
        company: 'Company Name',
      };
      const stepNumber = 0;
      spyOn(controller, 'currentStep').and.returnValue(stepNumber);
      spyOn(controller, 'insertProgressData');
      spyOn(controller, 'next');

      controller.setSelectedClient(sampleClient);

      expect(controller.selectedClient).toEqual(sampleClient);
      expect(controller.currentStep).toHaveBeenCalled();
      expect(controller.insertProgressData).toHaveBeenCalledWith({
        lines: [sampleClient.code, sampleClient.company],
        onClick: controller._WizardHandler.wizard().goTo.bind(controller, stepNumber),
      });
      expect(controller.next).toHaveBeenCalled();
    });

    it('headerTitle should update based on selected account type and wizard PERMISSIONS step', () => {
      const controller = makeController();
      spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
      spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('PERMISSIONS');

      expect(controller.headerTitle).toEqual('cd-create-new-user-account');

      controller.isEditingEORPermissions = true;

      controller.selectedAccountType = 'EOR';
      expect(controller.headerTitle).toEqual('cd-add-eor-permissions');

      controller.selectedAccountType = 'SHIP_BUILDER';
      expect(controller.headerTitle).toEqual('cd-add-ship-builder-permissions');

      controller.selectedAccountType = 'CLIENT';
      expect(controller.headerTitle).toEqual('cd-add-client-permissions');

      controller.selectedAccountType = 'FLAG';
      expect(controller.headerTitle).toEqual('cd-add-flag-permissions');

      controller.selectedAccountType = 'EXTERNAL_NON_CLIENT';
      expect(controller.headerTitle).toEqual('cd-add-external-non-client-permissions');
    });

    it('headerTitle should update based on selected account type and wizard CONFIRMATION step', () => {
      const controller = makeController();
      spyOn(controller._WizardHandler, 'wizard').and.returnValue({ currentStepTitle: () => {} });
      spyOn(controller._WizardHandler.wizard(), 'currentStepTitle').and.returnValue('CONFIRMATION');

      expect(controller.headerTitle).toEqual('cd-confirm-account-creation');

      controller.isEditingEORPermissions = true;
      controller.selectedAccountType = 'EOR';
      expect(controller.headerTitle).toEqual('cd-confirm-eor-permission');
      // There should not be editing of other account types.
    });

    it('permissionBtnLabel should give correct value based on selected account type', () => {
      const controller = makeController();
      expect(controller.permissionBtnLabel).toEqual('cd-create-user-account');

      controller.isEditingEORPermissions = true;
      controller.selectedAccountType = 'EOR';
      expect(controller.permissionBtnLabel).toEqual('cd-eor-permission-btn-label');
      // There should not be editing of other account types.
    });

    it('confirmationBtnLabel should give correct value based on selected account type', () => {
      const controller = makeController();
      expect(controller.confirmationBtnLabel).toEqual('cd-confirm-account-creation');

      controller.isEditingEORPermissions = true;
      controller.selectedAccountType = 'EOR';
      expect(controller.confirmationBtnLabel).toEqual('cd-eor-confirmation-btn-label');
      // There should not be editing of other account types.
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AddUserComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AddUserTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AddUserController);
    });
  });
});
