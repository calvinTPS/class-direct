/* global describe beforeEach beforeAll afterEach afterAll it expect inject */
/* eslint-disable angular/window-service, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import mockClients from 'app/common/models/client/client.mocks.json';
import SearchClientComponent from './search-client.component';
import SearchClientController from './search-client.controller';
import SearchClientModule from './';
import SearchClientTemplate from './search-client.pug';

describe('SearchClient', () => {
  let $rootScope;
  let $compile;
  let makeController;

  const mockClientService = {
    getAssetClientsByIMONumber: () => new Promise((resolve, reject) => resolve(mockClients)),
    getClientsByIMONumber: () => new Promise((resolve, reject) => resolve(mockClients)),
    getClientsByName: () => new Promise((resolve, reject) => resolve(mockClients)),
  };

  beforeEach(window.module(SearchClientModule));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('ClientService', mockClientService);
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('searchClient', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
      expect(controller.selectedRadio).toEqual('cd-map-from-an-asset');
      expect(controller.assetClients).toEqual([]);
      expect(controller.clientArray).toEqual([]);
    });

    it('inputClientString setter/getter should save the right values and execute search client by imo number and name', (done) => {
      const controller = makeController();
      const resultByIMO = spyOn(mockClientService, 'getClientsByIMONumber')
        .and.returnValue(Promise.resolve(mockClients));
      const resultByName = spyOn(mockClientService, 'getClientsByName')
        .and.returnValue(Promise.resolve(mockClients));

      controller.inputClientString = '1234567';
      expect(controller.inputClientString).toEqual('1234567'); // Just to test getter
      expect(controller.clientArray).toEqual([]);

      Promise.all([
        resultByIMO(),
        resultByName(),
      ]).then(() => {
        expect(mockClientService.getClientsByIMONumber).toHaveBeenCalledWith('1234567');
        expect(mockClientService.getClientsByName).toHaveBeenCalledWith('1234567');
      }).then(done);
    });

    it('inputIMONumber setter/getter should save the right values and not execute search client by imo number', (done) => {
      const controller = makeController();
      const result = spyOn(mockClientService, 'getAssetClientsByIMONumber')
        .and.returnValue(Promise.resolve(mockClients));

      // Reset inputIMONumber
      controller.inputIMONumber = '1234567';
      expect(controller.inputIMONumber).toEqual('1234567');
      result().then(() => {
        expect(mockClientService.getAssetClientsByIMONumber).toHaveBeenCalledWith('1234567');
        expect(controller.assetClients).toEqual(mockClients);
      }).then(done);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SearchClientComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SearchClientTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SearchClientController);
    });
  });

  describe('Rendering', () => {
    let controller; // eslint-disable-line no-unused-vars
    let element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();
      element = angular.element('<search-client foo="bar"><search-client/>');
      element = $compile(element)(scope);

      controller = element.controller('SearchClient');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
