import angular from 'angular';
import SearchClientComponent from './search-client.component';
import uiRouter from 'angular-ui-router';

export default angular.module('searchClient', [
  uiRouter,
])
.component('searchClient', SearchClientComponent)
.name;
