import './search-client.scss';
import controller from './search-client.controller';
import template from './search-client.pug';

export default {
  bindings: {
    onSelect: '&',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
