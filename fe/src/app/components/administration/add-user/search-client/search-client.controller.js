import * as _ from 'lodash';
import Base from 'app/base';

export default class SearchClientController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    AppSettings, // pug needs it
    ClientService,
  ) {
    super(arguments);
    this.template = this._AppSettings.RIDER_TEMPLATES.CLIENT;

    this.selectedRadio = 'cd-map-from-an-asset';
    this.assetClients = [];
    this.clientArray = [];
    this._submissionId = 0;

    // Cap the result so we don't render thousands of result
    // and _.unionWith below has less work to do
    this._maxResultToUse = 25;
  }

  get inputClientString() {
    return this._inputClientString;
  }

  set inputClientString(str) {
    this._inputClientString = str;
    this.clientArray = []; // Always clear the result first

    if (str.length < 3) return; // Require minimum 3 characters to search

    this._submissionId += 1;
    const currentSubmissionId = this._submissionId;
    this.isQuerying = true;

    Promise.all([
      this._ClientService.getClientsByIMONumber(str),
      this._ClientService.getClientsByName(str),
    ]).then(([result1, result2]) => {
      // Only set clientArray if results are for the latest submission.
      if (currentSubmissionId === this._submissionId) {
        const totalResult = _.concat(
          _.slice(result1, 0, this._maxResultToUse),
          _.slice(result2, 0, this._maxResultToUse),
        );

        // We pick only the properties we need so _.unionWith below has comparing less work to do
        const newClients = totalResult.map(builder => _.pick(builder, ['code', 'company']));
        this.clientArray = _.unionWith(this.clientArray, newClients, _.isEqual);
        this.isQuerying = false;
        this._$scope.$apply();
      }
    });
  }

  get inputIMONumber() {
    return this._inputIMONumber;
  }

  set inputIMONumber(str) {
    this._inputIMONumber = str;
    this.assetClients = [];
    if (str) {
      this._ClientService.getAssetClientsByIMONumber(str).then((assetClients) => {
        this.assetClients = assetClients;
        this._$scope.$apply();
      });
    }
  }
}
