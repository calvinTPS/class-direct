/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import SearchUserAssetsComponent from './search-user-assets.component';
import SearchUserAssetsController from './search-user-assets.controller';
import SearchUserAssetsModule from './';
import SearchUserAssetsTemplate from './search-user-assets.pug';

describe('SearchUserAssets', () => {
  let $rootScope,
    $compile,
    makeController,
    mockAssetService;

  beforeEach(window.module(SearchUserAssetsModule));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    mockAssetService = {
      getAssetByImoNumber: () => { },
    };

    window.module(($provide) => {
      $provide.value('AssetService', mockAssetService);
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('searchUserAssets', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    describe('submit', () => {
      describe('in LR asset only mode', () => {
        it.async('should return LR asset', async () => {
          const controller = makeController({
            form: {
              searchUserAssets: {
                $setValidity: () => { },
              },
            },
            isLrAssetOnly: true,
          });

          const mockLRAsset = {
            isLRAsset: true,
          };
          spyOn(mockAssetService, 'getAssetByImoNumber').and.returnValue(mockLRAsset);
          controller.searchText = undefined;

          await controller.submit();
          expect(mockAssetService.getAssetByImoNumber).not.toHaveBeenCalled();

          controller.searchText = '1234567';

          await controller.submit();
          expect(mockAssetService.getAssetByImoNumber).toHaveBeenCalled();
          expect(mockAssetService.getAssetByImoNumber).toHaveBeenCalledWith('1234567');
          expect(controller.registeredAsset).toEqual(mockLRAsset);
          expect(controller.isAssetNotExist).toBe(false);
        });
      });

      it.async('should not return non-LR asset', async () => {
        const controller = makeController({
          form: {
            searchUserAssets: {
              $setValidity: () => { },
            },
          },
          isLrAssetOnly: true,
        });

        spyOn(mockAssetService, 'getAssetByImoNumber').and.returnValue({
          isLRAsset: false,
        });
        controller.searchText = undefined;

        await controller.submit();
        expect(mockAssetService.getAssetByImoNumber).not.toHaveBeenCalled();

        controller.searchText = '1234567';

        await controller.submit();
        expect(mockAssetService.getAssetByImoNumber).toHaveBeenCalled();
        expect(mockAssetService.getAssetByImoNumber).toHaveBeenCalledWith('1234567');
        expect(controller.registeredAsset).toBe(undefined);
        expect(controller.isAssetNotExist).toBe(true);
      });

      it.async('should not call the api', async () => {
        const controller = makeController({
          form: {
            searchUserAssets: {
              $setValidity: () => { },
            },
          },
          isLrAssetOnly: true,
        });

        spyOn(mockAssetService, 'getAssetByImoNumber').and.returnValue({
          isLRAsset: false,
        });
        controller.searchText = '1234';

        await controller.submit();
        expect(mockAssetService.getAssetByImoNumber).not.toHaveBeenCalled();
        expect(controller.isAssetNotExist).toBe(true);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SearchUserAssetsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SearchUserAssetsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SearchUserAssetsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<search-user-assets foo="bar"><search-user-assets/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('SearchUserAssets');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
