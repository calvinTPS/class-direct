import angular from 'angular';
import SearchUserAssetsComponent from './search-user-assets.component';
import uiRouter from 'angular-ui-router';

export default angular.module('searchUserAssets', [
  uiRouter,
])
.component('searchUserAssets', SearchUserAssetsComponent)
.name;
