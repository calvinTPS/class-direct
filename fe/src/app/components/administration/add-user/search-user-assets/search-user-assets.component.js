import './search-user-assets.scss';
import controller from './search-user-assets.controller';
import template from './search-user-assets.pug';

export default {
  bindings: {
    form: '=',
    isReadOnly: '<',
    isRequired: '<?',
    isLrAssetOnly: '<?',
    registeredAsset: '=',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
