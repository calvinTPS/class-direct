import Base from 'app/base';

export default class SearchUserAssetsController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    AssetService,
  ) {
    super(arguments);

    // TODO : Move this component to user-creation
    this.searchText = '';
    this.isAssetNotExist = false;
    this._submissionId = 0;
  }

  async submit() {
    this.registeredAsset = undefined;
    this.isAssetNotExist = false;
    this.form.searchUserAssets.$setValidity('assetRegistered', false);

    if (this.searchText) {
      this._submissionId += 1;
      const currentSubmissionId = this._submissionId;

      if (this.searchText.length < 7 && this._submissionId === currentSubmissionId) {
        this.isAssetNotExist = true;
        return;
      }

      try {
        const asset = await this._AssetService.getAssetByImoNumber(this.searchText);

        if (this._submissionId === currentSubmissionId) {
          const isAssetFound = this.isLrAssetOnly ? asset.isLRAsset : true;

          if (isAssetFound) {
            this.registeredAsset = asset;
            this.form.searchUserAssets.$setValidity('assetRegistered', true);
          } else {
            this.isAssetNotExist = true;
          }

          this._$scope.$apply();
        }
      } catch (e) {
        if (this._submissionId === currentSubmissionId) {
          this.isAssetNotExist = true;
          this._$scope.$apply();
        }
      }
    }
  }
}
