import Base from 'app/base';

export default class ConfirmationController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
  }

  get isEORMode() {
    return this.mode === this._AppSettings.PERMISSION_TYPES.EOR;
  }
}
