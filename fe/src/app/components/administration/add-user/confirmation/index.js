import angular from 'angular';
import ConfirmationComponent from './confirmation.component';
import uiRouter from 'angular-ui-router';

export default angular.module('confirmation', [
  uiRouter,
])
.component('confirmation', ConfirmationComponent)
.name;
