/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import ConfirmationComponent from './confirmation.component';
import ConfirmationController from './confirmation.controller';
import ConfirmationModule from './';
import ConfirmationTemplate from './confirmation.pug';

describe('Confirmation', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(ConfirmationModule));

  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('confirmation', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ConfirmationComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ConfirmationTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ConfirmationController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<confirmation foo="bar"><confirmation/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Confirmation');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
