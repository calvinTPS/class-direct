import './confirmation.scss';
import controller from './confirmation.controller';
import template from './confirmation.pug';

export default {
  bindings: {
    mode: '<',
    model: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
