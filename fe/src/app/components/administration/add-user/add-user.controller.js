import * as _ from 'lodash';
import Base from 'app/base';
import dateHelper from 'app/common/helpers/date-helper';

export default class CreateUserController extends Base {
  /* @ngInject */
  constructor(
    $filter,
    $log,
    $state,
    $stateParams,
    $timeout,
    AppSettings,
    FlagService,
    UserService,
    WizardHandler,
  ) {
    super(arguments);
  }

  $onInit() {
    // The new user. Put all new user data inside.
    this.isValid = false;
    this.userModel = {};
    this.selectedUser = undefined;
    this._payload = {};

    // [{}] below is just an array with an empty object.
    // Wizard component needs initialized data even if it's empty.
    // The wizard view need to be referenced to a memory location.
    // If not initialized, wizard view is not referenced to a memory location, then
    // updating currentJourney = [new set of object] will not reflect on the view
    this.currentJourney = [{}];
    this.progressData = [];

    // This piece of code applies when clicking on ADD EOR PERMISSIONS
    // from user page in Administration.
    //
    // Had to put $timeout because when assigning a value that contains Wizard.goTo
    // function, it returns undefined without wrapping it inside $timeout.
    this._$timeout(() => {
      if (
        !_.isUndefined(_.get(this._$stateParams, 'selectedUser')) &&
        !_.isUndefined(_.get(this._$stateParams, 'selectedAccountType'))
      ) {
        this.selectedUser = _.get(this._$stateParams, 'selectedUser');
        this.setAccountType(_.get(this._$stateParams, 'selectedAccountType'));
        // Remove all progressData array since all of the setter remove one step of wizard
        this.progressData = [];
        // Reassign steps for progressData
        [
          [this.selectedUser.fullName, this.selectedUser.email],
          [this._$filter('translate')(`cd-role-${_.kebabCase(this.selectedAccountType)}`)],
        ].forEach((v, i) => {
          this.progressData.push({
            lines: v,
            onClick: this._WizardHandler.wizard().goTo.bind(this, i),
          });
        });

        // Trigger wizard handler to go to permissions page
        this._$timeout(() => {
          this.isEditingEORPermissions = true;
          this._WizardHandler.wizard().goTo('PERMISSIONS');
        });
      }
    });
  }

  get isEditingEORPermissions() {
    return !!this._isEditingEORPermissions;
  }

  set isEditingEORPermissions(flag) {
    this._isEditingEORPermissions = flag;
  }

  // Getter to toggle the continue button
  get isNextButtonDisabled() {
    let disableFlag = !this.isValid;

    if (this._WizardHandler.wizard().currentStepTitle() === 'SEARCH_FLAG') {
      disableFlag = _.isEmpty(this.selectedFlag);
    }
    return disableFlag;
  }

  get headerTitle() {
    const accType =
      _.toLower(_.kebabCase(this._AppSettings.PERMISSION_TYPES[this.selectedAccountType]));
    // Default title.
    let title = 'cd-create-new-user-account';

    if (this._WizardHandler.wizard()) {
      switch (this._WizardHandler.wizard().currentStepTitle()) {
        case 'PERMISSIONS':
          if (this.isEditingEORPermissions) {
            // Only change the copy text when in editing mode.
            title = `cd-add-${accType}-permissions`;
          }
          break;
        case 'CONFIRMATION':
          if (this.isEditingEORPermissions) {
            // Only change the copy text when in editing mode.
            title = `cd-confirm-${accType}-permission`;
          } else {
            title = 'cd-confirm-account-creation';
          }
          break;
        default:
      }
    }

    return title;
  }

  get permissionBtnLabel() {
    if (this.isEditingEORPermissions) {
      const accType =
        _.toLower(_.kebabCase(this._AppSettings.PERMISSION_TYPES[this.selectedAccountType]));
      return `cd-${accType}-permission-btn-label`;
    }
    return 'cd-create-user-account';
  }

  get confirmationBtnLabel() {
    if (this.isEditingEORPermissions) {
      const accType =
        _.toLower(_.kebabCase(this._AppSettings.PERMISSION_TYPES[this.selectedAccountType]));
      return `cd-${accType}-confirmation-btn-label`;
    }
    return 'cd-confirm-account-creation';
  }

  // Go to next wizard step
  next() {
    this._WizardHandler.wizard().next();
  }

  // Returning current step
  currentStep() {
    return this._WizardHandler.wizard().currentStepNumber() - 1;
  }

  // Insert progress data into progress-card component
  insertProgressData(obj) {
    this.progressData[this.currentStep()] = obj;
  }

  // Removing one of progress data object
  deleteProgressData() {
    this.progressData.splice(this.currentStep() + 1, this.progressData.length);
  }

  get isConfirmation() {
    return this._WizardHandler.wizard().currentStepTitle() === 'CONFIRMATION';
  }

  get isPermissions() {
    return this._WizardHandler.wizard().currentStepTitle() === 'PERMISSIONS';
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }

  get selectedUser() {
    return this._selectedUser;
  }

  // Selected user handler on the first step of the wizard
  set selectedUser(user) {
    this._selectedUser = user;
    if (!_.isUndefined(user)) {
      this.insertProgressData({
        lines: [user.fullName, user.email],
        onClick: this._WizardHandler.wizard().goTo.bind(this, this.currentStep()),
      });
      this.deleteProgressData();
    } else {
      this.progressData = [];
    }
  }

  userToCreate(user) {
    this.selectedUser = user;
    this.next();
  }

  userToEdit(user) {
    this._$state.go('administration.userDetails', { id: user.userId });
  }

  // Selected account type handler on the second step of the wizard
  setAccountType(accountType) {
    if (this.selectedAccountType !== accountType) {
      this.selectedAccountType = accountType;
      this.userModel = {}; // Clear saved data when we select different accountType
    }

    if (!_.isUndefined(accountType)) {
      this.currentJourney = _.clone(this._AppSettings.USER_CREATION_JOURNEY[accountType]);
      this.insertProgressData({
        lines: [this._$filter('roleTranslate')(accountType)],
        onClick: this._WizardHandler.wizard().goTo.bind(this, this.currentStep()),
      });

      this.deleteProgressData();

      if (accountType === this._AppSettings.PERMISSION_TYPES.FLAG) {
        this._FlagService.getAll().then((data) => {
          this.flags = data;
          this.next();
        });
      } else {
        this.next();
      }
    }
  }

  setSelectedFlag(flag) {
    this.selectedFlag = flag;
    this.insertProgressData({
      lines: [`${flag.name} (${flag.flagCode})`],
      onClick: this._WizardHandler.wizard().goTo.bind(this, this.currentStep()),
    });
    this.next();
  }

  /**
   * Set payload to be sent to backend.
   */
  populatePayload() {
    this._payload = {};
    _.set(this._payload, 'userId', _.get(this._selectedUser, 'userId'));
    _.set(this._payload, 'email', _.get(this._selectedUser, 'email'));

    switch (this.selectedAccountType) {
      case this._AppSettings.PERMISSION_TYPES.CLIENT: {
        _.set(this._payload, 'role', this.selectedAccountType);
        _.set(this._payload, 'clientCode', _.get(this.selectedClient, 'code'));
        _.set(this._payload, 'clientType', _.get(this.selectedClient, 'relationship'));
        _.set(this._payload, 'accountExpiry', dateHelper.formatDateServer(_.get(this.userModel, 'accountExpiry')));
        break;
      }

      case this._AppSettings.PERMISSION_TYPES.EOR: {
        _.set(this._payload, 'role', this.selectedAccountType);
        const eorPayload = [{
          imoNumber: _.get(this.userModel, 'registeredPermissionAsset.imo'),
          assetExpiry: dateHelper.formatDateServer(_.get(this.userModel, 'assetExpiryDate')),
          tmReport: _.get(this.userModel, 'tmReport'),
        }];
        _.set(this._payload, 'eor', eorPayload);
        break;
      }

      case this._AppSettings.PERMISSION_TYPES.FLAG: {
        _.set(this._payload, 'role', this.selectedAccountType);
        _.set(this._payload, 'flagCode', _.get(this.selectedFlag, 'flagCode'));
        _.set(this._payload, 'accountExpiry', dateHelper.formatDateServer(_.get(this.userModel, 'accountExpiry')));
        break;
      }

      case this._AppSettings.PERMISSION_TYPES.SHIP_BUILDER: {
        this._payload.role = this.selectedAccountType;
        this._payload.shipBuilderCode = _.get(this.selectedShipBuilder, 'code');
        break;
      }

      case this._AppSettings.PERMISSION_TYPES.LR_ADMIN:
      case this._AppSettings.PERMISSION_TYPES.LR_INTERNAL:
      case this._AppSettings.PERMISSION_TYPES.EXTERNAL_NON_CLIENT: {
        _.set(this._payload, 'role', this.selectedAccountType);
        break;
      }

      default:
    }
  }

  async createUser() {
    this.populatePayload();
    const executeMethod = async () => {
      if (
        !_.isUndefined(_.get(this._$stateParams, 'selectedUser')) &&
        !_.isUndefined(_.get(this._$stateParams, 'selectedAccountType'))
      ) {
        // Update user
        return await this._UserService.updateUserEor(
          this._$stateParams.selectedUser.userId,
          { eor: this._payload.eor },
        );
      }
      // Create user
      return await this._UserService.createUser(
        this._AppSettings.API_ENDPOINTS.Users,
        this._payload,
      );
    };

    try {
      await executeMethod();
      // back to previous state
      const name = _.get(this._$state, 'previous.name') ||
        this._AppSettings.STATES.ADMINISTRATION;
      this._$state.go(name, _.get(this._$state, 'previous.params'));
    } catch (e) {
      this._$log.error(e);
    }
  }

  setSelectedClient(obj) {
    this.selectedClient = obj;
    this.insertProgressData({
      lines: [obj.code, obj.company],
      onClick: this._WizardHandler.wizard().goTo.bind(this, this.currentStep()),
    });
    this.next();
  }

  setSelectedShipBuilder(obj) {
    this.selectedShipBuilder = obj;
    this.insertProgressData({
      lines: [obj.code, obj.company],
      onClick: this._WizardHandler.wizard().goTo.bind(this, this.currentStep()),
    });
    this.next();
  }
}
