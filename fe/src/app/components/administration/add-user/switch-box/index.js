import angular from 'angular';
import SwitchBoxComponent from './switch-box.component';
import uiRouter from 'angular-ui-router';

export default angular.module('switchBox', [
  uiRouter,
])
.component('switchBox', SwitchBoxComponent)
.name;
