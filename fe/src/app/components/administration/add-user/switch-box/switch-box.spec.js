/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import SwitchBoxComponent from './switch-box.component';
import SwitchBoxController from './switch-box.controller';
import SwitchBoxModule from './';
import SwitchBoxTemplate from './switch-box.pug';

describe('SwitchBox', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(SwitchBoxModule));
  beforeEach(() => {
    const mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('switchBox', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController();
      expect(controller.model).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SwitchBoxComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SwitchBoxTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SwitchBoxController);
    });
  });
});
