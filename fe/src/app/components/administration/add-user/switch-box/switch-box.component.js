import './switch-box.scss';

import controller from './switch-box.controller';
import template from './switch-box.pug';

export default {
  bindings: {
    description: '@',
    form: '=',
    header: '@',
    isReadOnly: '<',
    model: '=',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
