import Base from 'app/base';

export default class SwitchBoxController extends Base {
  /* @ngInject */
  constructor(
  ) {
    super(arguments);
  }

  $onInit() {
    this.model = this.model == null ? false : Boolean(this.model);
  }
}
