/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AccountExpiryComponent from './account-expiry.component';
import AccountExpiryController from './account-expiry.controller';
import AccountExpiryModule from './';
import AccountExpiryTemplate from './account-expiry.pug';

describe('AccountExpiry', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(AccountExpiryModule));
  beforeEach(() => {
    const mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('accountExpiry', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController();
      expect((controller.now).getDate()).toEqual((new Date()).getDate());
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AccountExpiryComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AccountExpiryTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AccountExpiryController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<account-expiry foo="bar"><account-expiry/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('AccountExpiry');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
