import './account-expiry.scss';
import controller from './account-expiry.controller';
import template from './account-expiry.pug';

export default {
  bindings: {
    form: '=',
    header: '@',
    label: '@',
    model: '=',
    isReadOnly: '<',
    isRequired: '<?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
