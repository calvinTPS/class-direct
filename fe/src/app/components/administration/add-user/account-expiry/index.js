import AccountExpiryComponent from './account-expiry.component';
import angular from 'angular';
import uiRouter from 'angular-ui-router';

export default angular.module('accountExpiry', [
  uiRouter,
])
.component('accountExpiry', AccountExpiryComponent)
.name;
