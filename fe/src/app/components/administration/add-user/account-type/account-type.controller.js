import * as _ from 'lodash';
import Base from 'app/base';

export default class AccountTypeController extends Base {
  /* @ngInject */
  constructor(AppSettings) {
    super(arguments);
  }

  get user() {
    return this._user;
  }

  set user(user) {
    this._user = user;
    this.accountTypes = null;
  }

  get accountTypes() {
    if (this._accountTypes == null && this.user) {
      const isInternalUser = this.user.internalUser;
      if (isInternalUser) {
        const accountTypes = _.cloneDeep(this._AppSettings.LR_ACCOUNT_TYPES);

        if (this.currentUser.isLRSupport) {
          // manual pick because we don't want LR_SUPPORT
          this._accountTypes = _.pick(accountTypes,
            this._AppSettings.ROLES.LR_INTERNAL,
            this._AppSettings.ROLES.LR_ADMIN,
          );
        } else if (this.currentUser.isLRAdmin) {
          // manual pick because we don't want LR_SUPPORT
          this._accountTypes = _.pick(accountTypes,
            this._AppSettings.ROLES.LR_INTERNAL,
          );
        }
      } else {
        this._accountTypes = _.cloneDeep(this._AppSettings.USER_ACCOUNT_TYPES);
      }

      _.forEach(this._accountTypes, (obj, key) => {
        obj.button = { // eslint-disable-line no-param-reassign
          label: 'cd-select',
          onClick: () => {
            this.onSelect({ accountType: key });
          },
          theme: 'primary',
        };
      });
    }

    return this._accountTypes;
  }

  set accountTypes(obj) {
    this._accountTypes = obj;
  }
}
