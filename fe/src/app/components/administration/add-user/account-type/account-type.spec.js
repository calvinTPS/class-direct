/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AccountTypeComponent from './account-type.component';
import AccountTypeController from './account-type.controller';
import AccountTypeModule from './';
import AccountTypeTemplate from './account-type.pug';
import AppSettings from 'app/config/project-variables.js';

describe('AccountType', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(AccountTypeModule));
  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('translateFilter', trans => trans);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('accountType', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ onSelect: () => {} });
      spyOn(controller, 'onSelect');

      // Test initial data added to accountTypes
      _.forEach(controller.accountTypes, (obj, key) => {
        expect(obj.button.label).toEqual('cd-select');
        expect(typeof obj.button.onClick).toEqual('function');
        expect(obj.button.theme).toEqual('primary');

        obj.button.onClick();

        expect(controller.onSelect).toHaveBeenCalledWith({ accountType: key });
      });
    });

    it('user setter/getter should update with the right values', () => {
      const controller = makeController({ onSelect: () => {} });
      const sampleObj = { test: 'test object' };
      controller.user = sampleObj;
      expect(controller.user).toEqual(sampleObj);
    });

    it('accountTypes setter/getter should return the right values', () => {
      const controller = makeController({ onSelect: () => {} });

      // Only internal users will be shown LR account types
      controller.user = { internalUser: true };
      controller.currentUser = { isLRSupport: true };
      expect(Object.keys(controller.accountTypes)).toEqual(
        ['LR_INTERNAL', 'LR_ADMIN']
      );

      // Reset
      controller.accountTypes = null;

      // Only internal users will be shown LR account types
      controller.user = { internalUser: true };
      controller.currentUser = { isLRAdmin: true };
      expect(Object.keys(controller.accountTypes)).toEqual(
        ['LR_INTERNAL']
      );

      // Reset
      controller.accountTypes = null;

      controller.user = { email: 'something@non-lr.org', internalUser: false };
      expect(Object.keys(controller.accountTypes)).toEqual(
        ['CLIENT', 'EOR', 'SHIP_BUILDER', 'FLAG'],
      );
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AccountTypeComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AccountTypeTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AccountTypeController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<account-type foo="bar"><account-type/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('AccountType');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
