import './account-type.scss';
import controller from './account-type.controller';
import template from './account-type.pug';

export default {
  bindings: {
    currentUser: '<',
    onSelect: '&',
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
