import AccountTypeComponent from './account-type.component';
import angular from 'angular';
import uiRouter from 'angular-ui-router';

export default angular.module('accountType', [
  uiRouter,
])
.component('accountType', AccountTypeComponent)
.name;
