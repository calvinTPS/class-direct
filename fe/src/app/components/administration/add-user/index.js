import accountExpiry from './account-expiry';
import accountType from './account-type';
import AddUser from './add-user.component';
import angular from 'angular';
import confirmation from './confirmation';
import permissions from './permissions';
import progressCards from './progress-cards';
import searchClient from './search-client';
import searchFlag from './search-flag';
import searchShipBuilder from './search-ship-builder';
import searchUser from './search-user';
import searchUserAsset from './search-user-assets';
import switchBox from './switch-box';
import uiRouter from 'angular-ui-router';

export default angular.module('addUser', [
  accountExpiry,
  accountType,
  confirmation,
  permissions,
  progressCards,
  searchClient,
  searchFlag,
  searchShipBuilder,
  searchUser,
  searchUserAsset,
  switchBox,
  uiRouter,
])
.component('addUser', AddUser)
.name;
