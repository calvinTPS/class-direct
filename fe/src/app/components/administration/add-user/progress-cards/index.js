import angular from 'angular';
import ProgressCardsComponent from './progress-cards.component';
import uiRouter from 'angular-ui-router';

export default angular.module('progressCards', [
  uiRouter,
])
.component('progressCards', ProgressCardsComponent)
.name;
