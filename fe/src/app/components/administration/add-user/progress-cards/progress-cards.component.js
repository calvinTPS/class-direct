import './progress-cards.scss';
import controller from './progress-cards.controller';
import template from './progress-cards.pug';

/**
 * @param cards {Array<{line1: string, line2: string}>}
 * An array of objects with line descriptions.
 */
export default {
  bindings: {
    cards: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
