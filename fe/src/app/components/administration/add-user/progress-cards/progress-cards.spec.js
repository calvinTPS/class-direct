/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import ProgressCardsComponent from './progress-cards.component';
import ProgressCardsController from './progress-cards.controller';
import ProgressCardsModule from './';
import ProgressCardsTemplate from './progress-cards.pug';

describe('ProgressCards', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(ProgressCardsModule));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('progressCards', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ProgressCardsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ProgressCardsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ProgressCardsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<progress-cards foo="bar"><progress-cards/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ProgressCards');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
