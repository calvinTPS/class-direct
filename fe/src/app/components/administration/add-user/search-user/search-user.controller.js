import _ from 'lodash';
import Base from 'app/base';

export default class SearchUserController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    UserService,
  ) {
    super(arguments);
    this.noUserAccount = false;
  }

  async queryUserEmail(email) {
    if (this.userSearchForm.$valid && !_.isEmpty(email)) {
      // Note that Archived users can be recreated.
      this.isQuerying = true;

      const users = await this._UserService.getUsersByEmail(email);

      if (_.isArray(users)) {
        this.selectedUser = users[0];

        if (this.selectedUser) {
          this.noUserAccount = !this.selectedUser.cdUser && !this.selectedUser.ssoUser;
        } else {
          this.noUserAccount = true;
        }
      }

      this.isQuerying = false;
      this._$scope.$apply();
    }
  }
}
