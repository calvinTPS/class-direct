import angular from 'angular';
import SearchUserComponent from './search-user.component';
import uiRouter from 'angular-ui-router';
import UserCardComponent from './user-card';

export default angular.module('searchUser', [
  uiRouter,
  UserCardComponent,
])
.component('searchUser', SearchUserComponent)
.name;
