import './search-user.scss';
import controller from './search-user.controller';
import template from './search-user.pug';

export default {
  bindings: {
    editUserFn: '&',
    createUserFn: '&',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
