/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import SearchUserComponent from './search-user.component';
import SearchUserController from './search-user.controller';
import SearchUserModule from './';
import SearchUserTemplate from './search-user.pug';

describe('SearchUser', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    makeController;

  const mockTranslateFilter = value => value;

  const mockUsers = [{
    userId: '101',
    flagCode: null,
    shipBuilderCode: null,
    clientCode: null,
    firstName: null,
    lastName: null,
    name: null,
    expiredDate: null,
    email: 'admin@lr.org',
    status: {
      id: 1,
      name: 'Active',
    },
    restrictAll: false,
    roles: [
      {
        roleId: 107,
        roleName: 'LR_ADMIN',
        isClientAmin: null,
        islrAdmin: null,
        isAdmin: null,
      },
    ],
    notifications: [],
    company: null,
  }];

  const mockUserService = {
    getUsersByEmail: () => Promise.resolve(mockUsers),
  };

  beforeEach(window.module(
    angularMaterial,
    SearchUserModule,
    { translateFilter: mockTranslateFilter },
  ));

  beforeEach(window.module(($provide) => {
    $provide.value('UserService', mockUserService);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('searchUser', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('()', () => {
    it.async('should call UserService.getUsersByEmail() when form is valid', async () => {
      const controller = makeController({ UserService: mockUserService });
      spyOn(mockUserService, 'getUsersByEmail').and.returnValue(Promise.resolve(mockUsers));
      controller.userSearchForm = { $valid: true };
      await controller.queryUserEmail('a@b.com');
      expect(mockUserService.getUsersByEmail).toHaveBeenCalledWith('a@b.com');
      expect(controller.selectedUser).toEqual(mockUsers[0]);
    });

    it.async('should NOT call UserService.getUsersByEmail() when form is NOT valid', async () => {
      const controller = makeController();
      spyOn(mockUserService, 'getUsersByEmail');
      controller.userSearchForm = { $valid: false };
      await controller.queryUserEmail('xxx');
      expect(mockUserService.getUsersByEmail).not.toHaveBeenCalled();

      controller.userSearchForm = { $valid: true };
      await controller.queryUserEmail('');
      expect(mockUserService.getUsersByEmail).not.toHaveBeenCalled();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SearchUserComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SearchUserTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SearchUserController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<add-user foo="bar"><add-user/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('SearchUser');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
