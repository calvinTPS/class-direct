import angular from 'angular';
import uiRouter from 'angular-ui-router';
import UserCardComponent from './user-card.component';

export default angular.module('userCard', [
  uiRouter,
])
.component('userCard', UserCardComponent)
.name;
