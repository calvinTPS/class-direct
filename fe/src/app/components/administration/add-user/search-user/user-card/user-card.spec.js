/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import UserCardComponent from './user-card.component';
import UserCardController from './user-card.controller';
import UserCardModule from './';
import UserCardTemplate from './user-card.pug';

describe('UserCard', () => {
  let $rootScope,
    $compile,
    makeController;

  const mockTranslateFilter = value => value;

  beforeEach(window.module(
    UserCardModule,
    { translateFilter: mockTranslateFilter,
      safeTranslateFilter: mockTranslateFilter,
    },
  ));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('userCard', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    describe('isUserEditable getter should return the right boolean value', () => {
      it('return false, user not exist', () => {
        const controller = makeController();
        expect(controller.isUserEditable).toEqual(false);
      });

      it('return false, cdUser = false and isArchived = false', () => {
        const controller = makeController({
          user: {
            cdUser: false,
            isArchived: false,
          },
        });
        expect(controller.isUserEditable).toEqual(false);
      });

      it('return false, cdUser = false and isArchived = true', () => {
        const controller = makeController({
          user: {
            cdUser: false,
            isArchived: true,
          },
        });
        expect(controller.isUserEditable).toEqual(false);
      });

      it('return true, cdUser = true and isArchived = false', () => {
        const controller = makeController({
          user: {
            cdUser: true,
            isArchived: false,
          },
        });
        expect(controller.isUserEditable).toEqual(true);
      });

      it('return false, cdUser = true and isArchived = true', () => {
        const controller = makeController({
          user: {
            cdUser: true,
            isArchived: true,
          },
        });
        expect(controller.isUserEditable).toEqual(false);
      });
    });

    describe('buttonAction()', () => {
      it('should call createUserFn when user NOT cdUser && NOT isArchived', () => {
        const fakeUser = {
          userId: '101',
          cdUser: false,
          isArchived: false,
        };
        const controller = makeController({
          user: fakeUser,
          editUserFn: () => {},
          createUserFn: () => {},
        });
        spyOn(controller, 'editUserFn');
        spyOn(controller, 'createUserFn');
        controller.buttonAction();
        expect(controller.createUserFn).toHaveBeenCalledWith(fakeUser);
        expect(controller.editUserFn).not.toHaveBeenCalled();
      });

      it('should call editUserFn when user cdUser and NOT isArchived', () => {
        const fakeUser = {
          userId: '101',
          cdUser: true,
          isArchived: false,
        };
        const controller = makeController({
          user: fakeUser,
          editUserFn: () => {},
          createUserFn: () => {},
        });
        spyOn(controller, 'editUserFn');
        spyOn(controller, 'createUserFn');
        controller.buttonAction();
        expect(controller.editUserFn).toHaveBeenCalledWith(fakeUser);
        expect(controller.createUserFn).not.toHaveBeenCalled();
      });

      it('should call createUserFn when user NOT cdUser and isArchived', () => {
        const fakeUser = {
          userId: '101',
          cdUser: false,
          isArchived: true,
        };
        const controller = makeController({
          user: fakeUser,
          editUserFn: () => {},
          createUserFn: () => {},
        });
        spyOn(controller, 'editUserFn');
        spyOn(controller, 'createUserFn');
        controller.buttonAction();
        expect(controller.editUserFn).not.toHaveBeenCalledWith(fakeUser);
        expect(controller.createUserFn).toHaveBeenCalled();
      });

      it('should call createUserFn when user cdUser and isArchived', () => {
        const fakeUser = {
          userId: '101',
          cdUser: true,
          isArchived: true,
        };
        const controller = makeController({
          user: fakeUser,
          editUserFn: () => {},
          createUserFn: () => {},
        });
        spyOn(controller, 'editUserFn');
        spyOn(controller, 'createUserFn');
        controller.buttonAction();
        expect(controller.editUserFn).not.toHaveBeenCalledWith(fakeUser);
        expect(controller.createUserFn).toHaveBeenCalled();
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = UserCardComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(UserCardTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(UserCardController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();
      element = angular.element('<user-card foo="bar" user="{cdUser: true, isArchived: true}"><user-card/>');
      element = $compile(element)(scope);

      controller = element.controller('UserCard');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
