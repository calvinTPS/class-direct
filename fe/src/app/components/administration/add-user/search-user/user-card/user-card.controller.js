import Base from 'app/base';

export default class UserCardController extends Base {
  /* @ngInject */
  constructor(
  ) {
    super(arguments);
  }

  get isUserEditable() {
    return this.user != null && this.user.cdUser && !this.user.isArchived;
  }

  buttonAction() {
    if (this.isUserEditable) {
      this.editUserFn(this.user);
    } else {
      this.createUserFn(this.user);
    }
  }
}
