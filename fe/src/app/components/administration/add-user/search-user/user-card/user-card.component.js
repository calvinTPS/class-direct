import './user-card.scss';
import controller from './user-card.controller';
import template from './user-card.pug';

export default {
  bindings: {
    user: '<',
    editUserFn: '&',
    createUserFn: '&',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
