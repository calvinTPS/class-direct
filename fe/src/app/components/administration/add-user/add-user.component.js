import './add-user.scss';
import controller from './add-user.controller';
import template from './add-user.pug';

export default {
  bindings: {
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
