// import _ from 'lodash';
import Base from 'app/base';

export default class SearchFlagController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    ViewService,
  ) {
    super(arguments);

    this.flagItemTemplate = this._AppSettings.RIDER_TEMPLATES.FLAG;

    this.searchByList = this._AppSettings.FLAGS_SEARCH_BY_LIST;
  }
}
