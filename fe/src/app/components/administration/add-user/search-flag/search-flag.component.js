import './search-flag.scss';
import controller from './search-flag.controller';
import template from './search-flag.pug';

export default {
  bindings: {
    flags: '<',
    setSelectedFlag: '&',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
