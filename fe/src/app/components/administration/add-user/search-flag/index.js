import angular from 'angular';
import SearchFlagComponent from './search-flag.component';
import uiRouter from 'angular-ui-router';

export default angular.module('searchFlag', [
  uiRouter,
])
.component('searchFlag', SearchFlagComponent)
.name;
