/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import SearchFlagComponent from './search-flag.component';
import SearchFlagController from './search-flag.controller';
import SearchFlagModule from './';
import SearchFlagTemplate from './search-flag.pug';

describe('SearchFlag', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(SearchFlagModule));
  const mockViewService = { registerTemplate: (x, y) => {} };

  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
    $provide.value('ViewService', mockViewService);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('searchFlag', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SearchFlagComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SearchFlagTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SearchFlagController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<search-flag foo="bar"><search-flag/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('SearchFlag');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
