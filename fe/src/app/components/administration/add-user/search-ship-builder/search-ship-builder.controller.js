import * as _ from 'lodash';
import Base from 'app/base';

export default class SearchShipBuilderController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    AppSettings, // pug needs it
    ClientService,
  ) {
    super(arguments);
    this.template = this._AppSettings.RIDER_TEMPLATES.SHIP_BUILDER;
    this.shipBuilderArray = [];
    this._submissionId = 0;

    // Cap the result so we don't render thousands of result
    // and _.unionWith below has less work to do
    this._maxResultToUse = 25;
  }

  get inputShipBuilderString() {
    return this._inputShipBuilderString;
  }

  set inputShipBuilderString(str) {
    this._inputShipBuilderString = str;
    this.shipBuilderArray = [];
    this._submissionId += 1;
    const currentSubmissionId = this._submissionId;


    if (str) {
      this.isQuerying = true;
      Promise.all([
        this._ClientService.getShipBuilderByIMONumber(str),
        this._ClientService.getShipBuilderByName(str),
      ]).then(([result1, result2]) => {
        // Only set clientArray if results are for the latest submission.
        if (currentSubmissionId === this._submissionId) {
          const totalResult = _.concat(
            _.slice(result1, 0, this._maxResultToUse),
            _.slice(result2, 0, this._maxResultToUse),
          );

          // We pick only the properties we need so _.unionWith below has comparing less work to do
          const newBuilders = totalResult.map(builder => _.pick(builder, ['code', 'company']));
          this.shipBuilderArray = _.unionWith(this.shipBuilderArray, newBuilders, _.isEqual);
          this.isQuerying = false;
          this._$scope.$apply();
        }
      });
    }
  }
}
