/* global describe beforeEach beforeAll afterEach afterAll it expect inject */
/* eslint-disable angular/window-service, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import mockClients from 'app/common/models/client/client.mocks.json';
import SearchShipBuilderComponent from './search-ship-builder.component';
import SearchShipBuilderController from './search-ship-builder.controller';
import SearchShipBuilderModule from './';
import SearchShipBuilderTemplate from './search-ship-builder.pug';

describe('SearchShipBuilder', () => {
  let $rootScope;
  let $compile;
  let makeController;

  const mockClientService = {
    getShipBuilderByIMONumber: () => new Promise((resolve, reject) => resolve(mockClients)),
    getShipBuilderByName: () => new Promise((resolve, reject) => resolve(mockClients)),
  };

  beforeEach(window.module(SearchShipBuilderModule));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('ClientService', mockClientService);
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('searchShipBuilder', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
      expect(controller.shipBuilderArray).toEqual([]);
    });

    it('inputShipBuilderString setter/getter should save the right values and execute search client by imo number and name', (done) => {
      const controller = makeController();
      const resultByIMO = spyOn(mockClientService, 'getShipBuilderByIMONumber')
        .and.returnValue(Promise.resolve(mockClients));
      const resultByName = spyOn(mockClientService, 'getShipBuilderByName')
        .and.returnValue(Promise.resolve(mockClients));

      controller.inputShipBuilderString = '1234567';
      expect(controller.inputShipBuilderString).toEqual('1234567'); // Just to test getter
      expect(controller.shipBuilderArray).toEqual([]);

      Promise.all([
        resultByIMO(),
        resultByName(),
      ]).then(() => {
        expect(mockClientService.getShipBuilderByIMONumber).toHaveBeenCalledWith('1234567');
        expect(mockClientService.getShipBuilderByName).toHaveBeenCalledWith('1234567');
      }).then(done);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SearchShipBuilderComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SearchShipBuilderTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SearchShipBuilderController);
    });
  });

  describe('Rendering', () => {
    let controller; // eslint-disable-line no-unused-vars
    let element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();
      element = angular.element('<search-ship-builder foo="bar"><search-ship-builder/>');
      element = $compile(element)(scope);

      controller = element.controller('SearchShipBuilder');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
