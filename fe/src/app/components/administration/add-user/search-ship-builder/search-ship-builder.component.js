import './search-ship-builder.scss';
import controller from './search-ship-builder.controller';
import template from './search-ship-builder.pug';

export default {
  bindings: {
    onSelect: '&',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
