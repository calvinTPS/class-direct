import angular from 'angular';
import SearchShipBuilderComponent from './search-ship-builder.component';
import uiRouter from 'angular-ui-router';

export default angular.module('searchShipBuilder', [
  uiRouter,
])
.component('searchShipBuilder', SearchShipBuilderComponent)
.name;
