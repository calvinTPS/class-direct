/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import PermissionsComponent from './permissions.component';
import PermissionsController from './permissions.controller';
import PermissionsModule from './';
import PermissionsTemplate from './permissions.pug';

describe('Permissions', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(PermissionsModule));

  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('permissions', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('get isEORMode', () => {
      const controller = makeController();
      controller.mode = AppSettings.PERMISSION_TYPES.EOR;
      expect(controller.isEORMode).toEqual(true);
    });

    it('get / set form', () => {
      const controller = makeController();
      const mockForm1 = {
        $valid: true,
      };
      const mockForm2 = {
        $valid: false,
      };

      controller._form = mockForm1;
      expect(controller.form).toEqual(mockForm1);
      expect(controller.isValid).toEqual(true);

      controller.form = mockForm2;
      expect(controller.form).toEqual(mockForm2);
      expect(controller.isValid).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = PermissionsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(PermissionsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(PermissionsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<permissions foo="bar"><permissions/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Permissions');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
