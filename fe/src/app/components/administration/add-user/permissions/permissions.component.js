import './permissions.scss';
import controller from './permissions.controller';
import template from './permissions.pug';

export default {
  bindings: {
    isValid: '=',
    mode: '<',
    model: '=',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
