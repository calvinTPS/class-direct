import angular from 'angular';
import PermissionsComponent from './permissions.component';
import uiRouter from 'angular-ui-router';

export default angular.module('permissions', [
  uiRouter,
])
.component('permissions', PermissionsComponent)
.name;
