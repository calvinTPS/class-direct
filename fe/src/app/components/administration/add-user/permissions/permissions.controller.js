import Base from 'app/base';

export default class PermissionsController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
    this._form = {};
  }

  get isEORMode() {
    return this.mode === this._AppSettings.PERMISSION_TYPES.EOR;
  }

  get form() {
    this.isValid = this._form.$valid;
    return this._form;
  }

  set form(arg) {
    this._form = arg;
  }
}
