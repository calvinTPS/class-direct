import AddUser from './add-user';
import angular from 'angular';
import eorAssets from './eor-assets';
import exportUsers from './export-users';
import routes from './administration.routes';
import uiRouter from 'angular-ui-router';
import ViewableUserDetails from './viewable-user-details';
import ViewableUsers from './viewable-users';

export default angular.module('administration', [
  AddUser,
  eorAssets,
  exportUsers,
  uiRouter,
  ViewableUserDetails,
  ViewableUsers,
])
.config(routes)
.name;
