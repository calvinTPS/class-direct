/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import ViewableUsersComponent from './viewable-users.component';
import ViewableUsersController from './viewable-users.controller';
import ViewableUsersModule from './';
import ViewableUsersTemplate from './viewable-users.pug';

describe('ViewableUsers', () => {
  let $rootScope;
  let $compile;
  let makeController;

  const mockUsers = [{
    userId: '101',
    flagCode: null,
    shipBuilderCode: null,
    clientCode: null,
    name: 'John Doe',
    firstName: null,
    lastName: null,
    expiredDate: null,
    email: 'admin@lr.org',
    status: {
      id: 1,
      name: 'Active',
    },
    restrictAll: false,
    roles: [
      {
        roleId: 107,
        roleName: 'LR_ADMIN',
        isClientAmin: null,
        islrAdmin: null,
        isAdmin: null,
      },
    ],
    notifications: [],
    company: null,
  }];

  const mockUserService = {
    getUsers: () => Promise.resolve(mockUsers),
  };

  beforeEach(window.module(ViewableUsersModule));

  beforeEach(() => {
    const mockState = {
      go: () => {},
    };
    const mockUtilsFactory = {
      getTabularAction: () => {},
    };

    window.module(($provide) => {
      $provide.value('$state', mockState);
      $provide.value('AppSettings', AppSettings);
      $provide.value('UserService', mockUserService);
      $provide.value('utils', mockUtilsFactory);
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('viewableUsers', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    const expectedFilterOptions = [{
      type: 'checkboxes',
      title: 'cd-account-type',
      key: 'roles',
      isAll: true,
      options: [
        { name: 'cd-role-client', value: 'CLIENT', selected: false },
        { name: 'cd-role-eor', value: 'EOR', selected: false },
        { name: 'cd-role-ship-builder', value: 'SHIP_BUILDER', selected: false },
        { name: 'cd-role-flag', value: 'FLAG', selected: false },
        { name: 'cd-role-lr-admin', value: 'LR_ADMIN', selected: false },
        { name: 'cd-role-lr-internal', value: 'LR_INTERNAL', selected: false },
        { name: 'cd-role-lr-support', value: 'LR_SUPPORT', selected: false },
      ],
    }, {
      type: 'daterange',
      title: 'cd-last-login-date',
      key: 'lastLoginDate',
      disallowFutureDates: true,
    }, {
      type: 'checkboxes',
      title: 'cd-account-status',
      key: 'status',
      options: [{
        name: 'cd-active',
        value: 'Active',
        selected: true,
      }, {
        name: 'cd-disabled',
        value: 'Disabled',
        selected: true,
      }, {
        name: 'cd-archived',
        value: 'Archived',
        selected: false, // Archived is by default not selected
      }],
      isAll: false,
    }];

    it('initializes just fine', () => {
      const controller = makeController();
      expect(controller.searchByList).toEqual(AppSettings.USERS_SEARCH_BY_LIST);
      expect(controller.filterOptions).toEqual(expectedFilterOptions);
      expect(controller.action).toEqual({
        stateName: 'administration.userDetails',
        evalParams: { id: 'userId' },
      });

      // Ensure settings remains unclobbered
      expect(Object.keys(AppSettings.USER_ACCOUNT_TYPES).length).toEqual(4);
      expect(Object.keys(AppSettings.LR_ACCOUNT_TYPES).length).toEqual(3);
    });
  });

  describe('Controller updateQuery()', () => {
    it('should NOT call UserService.getUsers when queryParams is empty', () => {
      const controller = makeController();
      spyOn(mockUserService, 'getUsers');
      controller.updateQuery();
      expect(mockUserService.getUsers).not.toHaveBeenCalled();
    });

    it.async('should call UserService.getUsers with correct payload and return the filtered items and the filter object', async() => {
      const fakeFilteredItems = [{ some: 'data' }];
      const fakeParams = {
        filterQuery: {
          roles: ['CLIENT', 'EOR', 'SHIP_BUILDER', 'FLAG'],
        },
        searchBy: {
          tag: 'FLAG',
          value: 'flagCode',
        },
        searchText: 'xxx',
      };
      const expectedPayload = {
        codeSearch: { type: 'FLAG', code: 'xxx' },
        roles: ['CLIENT', 'EOR', 'SHIP_BUILDER', 'FLAG'],
      };
      const controller = makeController();
      spyOn(mockUserService, 'getUsers').and.returnValue(Promise.resolve(fakeFilteredItems));
      const { items, filters } = await controller.updateQuery(fakeParams);
      expect(mockUserService.getUsers).toHaveBeenCalledWith(expectedPayload, {});
      expect(items).toEqual(fakeFilteredItems);
      expect(filters).toEqual(expectedPayload);
    });

    it('should call UserService.getUsers with correct payload from partial queryParams', () => {
      const fakeParams = {
        filterQuery: {
          roles: ['CLIENT', 'EOR', 'SHIP_BUILDER', 'FLAG'],
        },
      };
      const expectedPayload = {
        roles: ['CLIENT', 'EOR', 'SHIP_BUILDER', 'FLAG'],
      };
      const controller = makeController();
      spyOn(mockUserService, 'getUsers');
      controller.updateQuery(fakeParams);
      expect(mockUserService.getUsers).toHaveBeenCalledWith(expectedPayload, {});
    });

    it('should call UserService.getUsers with correct payload from partial queryParams 2', () => {
      const fakeParams = {
        filterQuery: {
          status: ['Active', 'Disabled', 'Archived'],
        },
      };
      const expectedPayload = {
        roles: [],
        status: ['Active', 'Disabled', 'Archived'],
      };
      const controller = makeController();
      spyOn(mockUserService, 'getUsers');
      controller.updateQuery(fakeParams);
      expect(mockUserService.getUsers).toHaveBeenCalledWith(expectedPayload, {});
    });

    it('should call UserService.getUsers with correct payload from partial queryParams 3', () => {
      const fakeParams = {
        searchBy: {
          tag: 'FLAG',
          value: 'flagCode',
        },
        searchText: 'xxx',
      };
      const expectedPayload = {
        roles: [],
        codeSearch: { type: 'FLAG', code: 'xxx' },
      };
      const controller = makeController();
      spyOn(mockUserService, 'getUsers');
      controller.updateQuery(fakeParams);
      expect(mockUserService.getUsers).toHaveBeenCalledWith(expectedPayload, {});
    });

    it('should call UserService.getUsers with correct payload from partial queryParams 4', () => {
      const fakeParams = {
        searchBy: {
          // tag: 'EMAIL', // When no tag is provided
          value: 'email',
        },
        searchText: 'xxx',
      };
      const expectedPayload = {
        roles: [],
        email: 'xxx',
      };
      const controller = makeController();
      spyOn(mockUserService, 'getUsers');
      controller.updateQuery(fakeParams);
      expect(mockUserService.getUsers).toHaveBeenCalledWith(expectedPayload, {});
    });

    it('should call UserService.getUsers with query params if sortBy is present', () => {
      const fakeParams = {
        sortBy: {
          value: 'SortField',
          order: 'ASC',
        },
      };
      const controller = makeController();
      spyOn(mockUserService, 'getUsers');
      controller.updateQuery(fakeParams);
      expect(mockUserService.getUsers).toHaveBeenCalledWith({
        roles: [],
      }, {
        sort: 'SortField',
        order: 'ASC',
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ViewableUsersComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ViewableUsersTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ViewableUsersController);
    });
  });

  describe('Rendering', () => {
    let controller;
    let element;

    beforeEach(() => {
      spyOn(document, 'querySelector').and.returnValue({ scrollTop: 0 });
      const scope = $rootScope.$new();
      element = angular.element('<viewable-users foo="bar"><viewable-users/>');

      // TODO: It's taking too much time to figure out the errors. Will need help.
      // element = $compile(element)(scope);
      // scope.bar = 'baz'; // CHANGE ME
      // scope.$apply();
      // controller = element.controller('ViewableUsers');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
