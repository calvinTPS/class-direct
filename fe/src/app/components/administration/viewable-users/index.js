import angular from 'angular';
import uiRouter from 'angular-ui-router';
import viewableUsersComponent from './viewable-users.component';

export default angular.module('viewableUsers', [
  uiRouter,
])
.component('viewableUsers', viewableUsersComponent)
.name;
