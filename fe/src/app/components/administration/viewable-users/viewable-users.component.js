import './viewable-users.scss';
import controller from './viewable-users.controller';
import template from './viewable-users.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
