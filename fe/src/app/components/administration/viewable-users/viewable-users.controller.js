import _ from 'lodash';
import Base from 'app/base';
import dateHelper from 'app/common/helpers/date-helper';
import RouteMixins from 'app/route.mixin';

export default class ViewableUsersController extends RouteMixins(Base) {
  /* @ngInject */
  constructor(
    $scope, // used by RouteMixin
    AppSettings,
    UserService,
    utils,
    ViewService,
  ) {
    super(arguments);

    this.searchByList = AppSettings.USERS_SEARCH_BY_LIST;
    this.sortByList = AppSettings.USERS_SORT_BY_LIST;
    this.filterOptions = _.cloneDeep(AppSettings.USERS_FILTER_OPTIONS);
    const roles = _.find(this.filterOptions, opt => opt.key === 'roles');
    // Copy to new object
    const allAccountTypes =
      _.merge({}, AppSettings.USER_ACCOUNT_TYPES, AppSettings.LR_ACCOUNT_TYPES);
    roles.options = _.keys(allAccountTypes)
                        .map(key => ({
                          name: allAccountTypes[key].label,
                          value: key,
                          selected: false,
                        }));

    this.action = {
      stateName: 'administration.userDetails',
      evalParams: { id: 'userId' },
    };

    this.userItemTemplate = this._AppSettings.RIDER_TEMPLATES.USER;
  }

  $onInit() {
    super.$onInit();
  }

  /**
  * @param {object} queryParams - query parameters set by rider-filters
  * queryParams = {
  *    filterQuery: {},
  *    searchBy: {},
  *    sortBy: {}
  *  }
  */

  async updateQuery(queryParams) {
    const filters = {};
    let items = [];

    if (queryParams) {
      filters.roles = _.get(queryParams, 'filterQuery.roles', []);

      if (_.get(queryParams, 'filterQuery.lastLoginDate.filterDateMin')) {
        filters.dateOfLastLoginMin = dateHelper.formatDateServer(
          _.get(queryParams, 'filterQuery.lastLoginDate.filterDateMin'));
      }

      if (_.get(queryParams, 'filterQuery.lastLoginDate.filterDateMax')) {
        filters.dateOfLastLoginMax = dateHelper.formatDateServer(
          _.get(queryParams, 'filterQuery.lastLoginDate.filterDateMax'));
      }

      if (_.get(queryParams, 'searchText')) {
        if (_.get(queryParams, 'searchBy.tag')) {
          filters.codeSearch = {};
          filters.codeSearch.type = _.get(queryParams, 'searchBy.tag');
          filters.codeSearch.code = _.get(queryParams, 'searchText');
        } else {
          filters[_.get(queryParams, 'searchBy.value')] = _.get(queryParams, 'searchText');
        }
      }

      if (_.get(queryParams, 'filterQuery.status')) {
        filters.status = _.get(queryParams, 'filterQuery.status');
      }

      const params = {}; // url params
      const sortBy = _.get(queryParams, 'sortBy');
      if (sortBy) {
        params.sort = sortBy.value;
        params.order = sortBy.order;
      }

      items = await this._UserService.getUsers(filters, params);
    }

    return { items, filters };
  }
}
