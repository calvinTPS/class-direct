/* global inject */
/* eslint-disable angular/window-service */

import AdministrationModule from './';
import AppSettings from 'app/config/project-variables.js';

describe('Administration', () => {
  let $injector;
  let $state;
  let mockCurrentUser;
  let mockUserService;

  beforeEach(window.module(AdministrationModule));

  beforeEach(window.module(($provide) => {
    mockUserService = {
      getCurrentUser: () => {},
      getAccessibleAssets: () => {},
      getRestrictedAssets: () => {},
      getUser: () => {},
      getUsers: () => {},
    };

    mockCurrentUser = {
      isAdmin: true,
    };

    $provide.value('AppSettings', AppSettings);
    $provide.value('UserService', mockUserService);
    $provide.value('currentUser', mockCurrentUser);
  }));

  beforeEach(inject((
    _$injector_,
    _$state_,
    _$stateParams_,
  ) => {
    $injector = _$injector_;
    $state = _$state_;
  }));

  describe('Routes', () => {
    describe('administration state', () => {
      it('transition to administration should respond to the right URLs', () => {
        expect($state.href('administration')).toEqual('#!/administration');
      });

      it.async('should resolve currentUser', async () => {
        spyOn(mockUserService, 'getCurrentUser');
        await $injector.invoke($state.get('administration').resolve.currentUser);
        expect(mockUserService.getCurrentUser).toHaveBeenCalled();
      });
    });

    describe('administration.userList state', () => {
      it('transition to administration.userList should respond to the right URLs', () => {
        expect($state.href(AppSettings.STATES.ADMINISTRATION)).toEqual('#!/administration/users');
      });

      it.async('should redirect if currentUser is not admin', async() => {
        spyOn($state, 'go');
        spyOn(mockUserService, 'getUsers');
        spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve({ isAdmin: false }));

        $state.go(AppSettings.STATES.ADMINISTRATION);
        expect(mockUserService.getUsers).not.toHaveBeenCalled();

        // FIXME - following line should work
        // expect($state.go.calls.mostRecent().args[0]).toEqual(AppSettings.STATES.VESSEL_LIST);
      });
    });

    describe('administration.userDetails state', () => {
      it('transition to administration.userDetails should respond to the right URLs', () => {
        expect($state.href(AppSettings.STATES.USER_DETAILS, { id: 100 })).toEqual('#!/administration/users/100');
      });

      it.async('should resolve user', async () => {
        spyOn(mockUserService, 'getUser');
        await $injector.invoke(
          $state.get(AppSettings.STATES.USER_DETAILS, { id: 100 }).resolve.user);
        expect(mockUserService.getUser).toHaveBeenCalled();
      });
    });

    describe('administration.addUser state', () => {
      it('transition to administration.addUser should respond to the right URLs', () => {
        expect($state.href(AppSettings.STATES.ADD_USER)).toEqual('#!/administration/add-user');
      });
    });

    describe('administration.editUser state', () => {
      it('transition to administration.editUser should respond to the right URLs', () => {
        expect($state.href(AppSettings.STATES.EDIT_USER, { id: 100 })).toEqual('#!/administration/edit-user/100');
      });
    });

    describe('administration.accessibleAndRestrictedAssets state', () => {
      it('transition to administration.accessibleAndRestrictedAssets should respond to the right URLs', () => {
        expect($state.href(AppSettings.STATES.ACCESSIBLE_AND_RESTRICTED_ASSETS, { id: 100 }))
          .toEqual('#!/administration/users/100/accessible-and-restricted-assets');
      });

      it.async('should resolve accessible assets', async () => {
        spyOn(mockUserService, 'getAccessibleAssets');
        await $injector.invoke($state.get(AppSettings.STATES.ACCESSIBLE_AND_RESTRICTED_ASSETS,
          { id: 100 }).resolve.accessibleAssets);
        expect(mockUserService.getAccessibleAssets).toHaveBeenCalled();
      });

      it.async('should resolve user', async () => {
        spyOn(mockUserService, 'getUser');
        await $injector.invoke($state.get('administration.accessibleAndRestrictedAssets', { id: 100 }).resolve.user);
        expect(mockUserService.getUser).toHaveBeenCalled();
      });
    });
  });
});
