import './export-users.scss';
import controller from './export-users.controller';
import template from './export-users.pug';

export default {
  bindings: {
    filters: '<?',
    hasMobileView: '<?',
    users: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
