import Base from 'app/base';
import dialogTemplate from './export-users-modal.pug';

export default class ExportUsersController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $filter,
    $mdDialog,
    $mdMedia,
    $scope,
    AppSettings,
    ExportService,
  ) {
    super(arguments);

    this.selectedTab = ExportUsersController.TABS.USERS;

    this.tabs = {};
    this.tabs[ExportUsersController.TABS.USERS] = {
      label: 'cd-users',
    };
    this.tabs[ExportUsersController.TABS.INFORMATION] = {
      label: 'cd-information',
      selected: 'full',
    };
  }

  async updateUsersExportSummary() {
    this.usersExportInfo =
      await this._ExportService.getUsersExportSummary(this.selectedEmails, this.filters);

    this._$scope.$apply();
  }

  showModal(ev) {
    this._$mdDialog.show({
      bindToController: true,
      controller: ExportUsersController,
      controllerAs: 'vm',
      fullscreen: true,
      locals: {
        filters: this.filters,
        users: this.users,
      },
      parent: this._$document.body,
      template: dialogTemplate,
    });
  }

  closeModal() {
    this._$mdDialog.cancel();
  }

  get selectedTab() {
    return this._selectedTab;
  }

  set selectedTab(key) {
    if (key === ExportUsersController.TABS.INFORMATION) {
      this.updateUsersExportSummary();
    }
    this._selectedTab = key;
  }

  /**
   * Trigger the call to export users CSV file.
   */
  exportCSV() {
    this._ExportService.exportUsersCSV(this.selectedEmails, this.filters);
  }

  /**
   * Constant that holds the tab names.
   *
   * @type {{INFORMATION: string, USERS: string}}
   */
  static TABS = {
    INFORMATION: 'information',
    USERS: 'users',
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
