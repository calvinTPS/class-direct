/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import ExportUsersComponent from './export-users.component';
import ExportUsersController from './export-users.controller';
import ExportUsersDialogTemplate from './export-users-modal.pug';
import ExportUsersModule from './';
import ExportUsersTemplate from './export-users.pug';

describe('ExportUsers', () => {
  let $compile;
  let $mdDialog;
  let $rootScope;
  let makeController;

  const mockUsersSummary = {
    accountTypes: ['Client', 'Exhibition of Records', 'Flag'],
    lastLoginDate: { from: '2016-01-02', to: '2016-04-05' },
  };

  const mockExportService = {
    getUsersExportSummary: () => Promise.resolve(mockUsersSummary),
    exportUsersCSV: () => Promise.resolve(),
  };

  beforeEach(window.module(ExportUsersModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockMdDialog = function mdDialog() {
      return {
        alert: (arg) => {},
        build: (arg) => {},
        cancel: function cancel(reason, options) {},
        confirm: (arg) => {},
        destroy: function destroyInterimElement(opts) {},
        hide: function hide(reason, options) {},
        prompt: (arg) => {},
        show: function showInterimElement(opts) {
          return new Promise((resolve, reject) => resolve());
        },
      };
    };

    window.module(($provide) => {
      $provide.service('$mdDialog', mockMdDialog); // eslint-disable-line angular/no-service-method
      $provide.value('AppSettings', AppSettings);
      $provide.value('$mdMedia', {});
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
      $provide.value('ExportService', mockExportService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('exportUsers', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController();

      // selectedTab should return the correct selectedTab by default
      expect(controller.selectedTab).toEqual('users');

      // should have the right tabs object
      expect(controller.tabs).toEqual({
        users: {
          label: 'cd-users',
        },
        information: {
          label: 'cd-information',
          selected: 'full',
        },
      });
    });

    it('showModal() should pop the $mdDialog box', () => {
      const fakeUsers = [{ some: 'users' }];
      const fakeFilters = { some: 'filters' };
      const controller = makeController({ users: fakeUsers, filters: fakeFilters });

      spyOn($mdDialog, 'show');
      controller.showModal();
      expect($mdDialog.show).toHaveBeenCalledWith({
        bindToController: true,
        controller: ExportUsersController,
        controllerAs: 'vm',
        fullscreen: true,
        locals: {
          filters: fakeFilters,
          users: fakeUsers,
        },
        parent: controller._$document.body,
        template: ExportUsersDialogTemplate,
      });
    });

    it('closeModal() should close $mdDialog modal box', () => {
      const controller = makeController();
      spyOn($mdDialog, 'cancel');
      controller.closeModal();
      expect($mdDialog.cancel).toHaveBeenCalled();
    });
  });

  describe('Controller exportCSV()', () => {
    it('should call ExportService.exportUsersCSV with correct params', () => {
      const fakeUsers = [{ some: 'users' }];
      const fakeFilters = { some: 'filters' };
      const fakeSelectedEmails = { some: 'data' };
      const controller = makeController({ users: fakeUsers, filters: fakeFilters });

      controller.selectedEmails = fakeSelectedEmails;
      spyOn(mockExportService, 'exportUsersCSV');
      controller.exportCSV();
      expect(mockExportService.exportUsersCSV)
        .toHaveBeenCalledWith(fakeSelectedEmails, fakeFilters);
    });
  });

  describe('Controller updateUsersExportSummary()', () => {
    it.async('should call ExportService.getUsersExportSummary with the selectedEmails', async() => {
      const fakeFilters = { some: 'filters' };
      const fakeSelectedEmails = { some: 'data' };
      const controller = makeController({ filters: fakeFilters });
      controller.selectedEmails = fakeSelectedEmails;
      spyOn(mockExportService, 'getUsersExportSummary');
      await controller.updateUsersExportSummary();
      expect(mockExportService.getUsersExportSummary)
        .toHaveBeenCalledWith(fakeSelectedEmails, fakeFilters);
    });
  });

  describe('Controller set selectedTab', () => {
    it('should call updateUsersExportSummary when selectedTab is "information"', () => {
      const controller = makeController();
      spyOn(controller, 'updateUsersExportSummary').and.returnValue();
      controller.selectedTab = 'information';
      expect(controller.updateUsersExportSummary).toHaveBeenCalled();
    });

    it('should NOT call updateUsersExportSummary when selectedTab is "user"', () => {
      const controller = makeController();
      spyOn(controller, 'updateUsersExportSummary').and.returnValue();
      controller.selectedTab = 'user';
      expect(controller.updateUsersExportSummary).not.toHaveBeenCalled();
    });
  });

  describe('Controller get accountTypes', () => {
    it.async('should return uniques accountTypes of selected users', async() => {
      const controller = makeController();
      controller.selectedTab = 'information';
      await controller.updateUsersExportSummary();
      expect(controller.usersExportInfo.accountTypes).toEqual(['Client', 'Exhibition of Records', 'Flag']);
    });
  });

  describe('Controller get lastLoginDates', () => {
    it.async('should return the min and max dates of last login dates of selected users', async() => {
      const controller = makeController();
      controller.selectedTab = 'information';
      await controller.updateUsersExportSummary();
      expect(controller.usersExportInfo.lastLoginDate).toEqual({ from: '2016-01-02', to: '2016-04-05' });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ExportUsersComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ExportUsersTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ExportUsersController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<export-users foo="bar"><export-users/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ExportAssets');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
