import angular from 'angular';
import ExportUsersComponent from './export-users.component';
import uiRouter from 'angular-ui-router';

export default angular.module('exportUsers', [
  uiRouter,
])
.component('exportUsers', ExportUsersComponent)
.name;
