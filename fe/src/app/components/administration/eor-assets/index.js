import angular from 'angular';
import eorAsset from './eor-asset';
import EorAssetsComponent from './eor-assets.component';
import uiRouter from 'angular-ui-router';

export default angular.module('eorAssets', [
  eorAsset,
  uiRouter,
])
.component('eorAssets', EorAssetsComponent)
.name;
