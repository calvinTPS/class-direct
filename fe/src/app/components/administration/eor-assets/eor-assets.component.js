import './eor-assets.scss';
import controller from './eor-assets.controller';
import template from './eor-assets.pug';

export default {
  bindings: {
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
