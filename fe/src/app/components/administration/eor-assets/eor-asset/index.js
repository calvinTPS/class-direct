import angular from 'angular';
import EorAssetComponent from './eor-asset.component';
import uiRouter from 'angular-ui-router';

export default angular.module('eorAsset', [
  uiRouter,
])
.component('eorAsset', EorAssetComponent)
.name;
