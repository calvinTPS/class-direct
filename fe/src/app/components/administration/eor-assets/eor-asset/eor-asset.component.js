import './eor-asset.scss';
import controller from './eor-asset.controller';
import template from './eor-asset.pug';

export default {
  bindings: {
    asset: '<',
    eor: '=',
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
