import Base from 'app/base';
import dateHelper from 'app/common/helpers/date-helper';
import removeEorModalTemplate from './_partials/remove-eor.pug';
import updateEorAssetExpiryDateModalTemplate from './_partials/edit-eor-expiration-date.pug';

export default class EorAssetController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $scope,
    $state,
    AssetService,
    UserService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.now = new Date();
    this.form = {};
    this.asset = {};

    return this._getAssetData();
  }

  async _getAssetData() {
    this.asset = await this._AssetService.getAssetByImoNumber(this.eor.imoNumber);
    this._$scope.$apply();
  }

  showUpdateExpiryDateDialog(evt) {
    return this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: false,
      controller: () => this,
      controllerAs: 'vm',
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: updateEorAssetExpiryDateModalTemplate,
    })
      .then((form) => {
        this.updateExpirationDate(form);
      });
  }

  showRemoveEORModal() {
    return this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: false,
      controller: () => this,
      controllerAs: 'vm',
      parent: angular.element(this._$document[0].body),
      template: removeEorModalTemplate,
    })
      .then(() =>
        this._UserService.deleteUserEor(this.user.userId, this.eor.imoNumber)
      )
      .then(() => {
        this._$state.reload(true);
      });
  }

  async updateExpirationDate(form) {
    if (form.$valid) {
      const status = await this._UserService.updateEorExpiryDate(
        this.user.userId,
        this.eor.imoNumber,
        this.newEorExpirationDate
      );

      // Update user's eor data.
      if (status) {
        this.eor.assetExpiryDate = dateHelper.formatDateServer(this.newEorExpirationDate);
        // Reload page to update Account expiry date in the view
        this._$state.reload();
      }
      // Clean up the model.
      this.newEorExpirationDate = null;
    }
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
