/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-underscore-dangle, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import EorAssetComponent from './eor-asset.component';
import EorAssetController from './eor-asset.controller';
import EorAssetModule from './';
import EorAssetTemplate from './eor-asset.pug';
import utils from 'app/common/utils/utils.factory';

describe('EorAsset', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    mockAssetService,
    mockEORRecord,
    mockState,
    mockUser,
    mockUserService,
    mockTranslateFilter,
    makeController;

  beforeEach(window.module(
    EorAssetModule,
    angularMaterial,
    { translateFilter: mockTranslateFilter },
  ));

  beforeEach(() => {
    mockUser = {
      id: 1,
      userId: '376ab4e0-7530-410e-aa5c-d7cedaa520de',
      creationDate: '2016-01-01',
      userAccExpiryDate: '2016-12-19',
    };

    mockEORRecord = {
      accessTmReport: false,
      assetExpiryDate: '2017-05-27',
      eorId: 2,
      imoNumber: 7653895,
      userId: '376ab4e0-7530-410e-aa5c-d7cedaa520de',
    };

    mockUserService = {
      updateEorExpiryDate: () => new Promise((resolve, reject) => resolve(true)),
      deleteUserEor: () => Promise.resolve(),
    };

    mockAssetService = {
      getAssetByImoNumber: () => new Promise((resolve, reject) => resolve({ mockData: true })),
    };

    mockState = {
      reload: () => { },
    };

    mockTranslateFilter = value => value;
    window.module(($provide) => {
      $provide.value('UserService', mockUserService);
      $provide.value('AppSettings', AppSettings);
      $provide.value('utils', utils());
      $provide.value('$state', mockState);
      $provide.value('AssetService', mockAssetService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      (bindings = { user: mockUser, eor: mockEORRecord }) => {
        const controller = $componentController('eorAsset', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ user: mockUser, eor: mockEORRecord });
      expect(controller.asset).not.toBeNull();
    });

    it.async('Should call updateEorExpiry service', async (done) => {
      const controller = makeController({ user: mockUser, eor: mockEORRecord });
      controller.newEorExpirationDate = 'mockValue';
      spyOn(mockUserService, 'updateEorExpiryDate').and.callThrough();
      spyOn(mockState, 'reload');

      let mockForm = {
        $valid: false,
      };
      await controller.updateExpirationDate(mockForm);
      expect(controller.newEorExpirationDate).not.toBe(null);

      mockForm = {
        $valid: true,
      };
      await controller.updateExpirationDate(mockForm);
      expect(mockUserService.updateEorExpiryDate).toHaveBeenCalled();
      expect(mockState.reload).toHaveBeenCalled();
    });

    it.async('Should get the asset by calling getAssetByImoNumber', async (done) => {
      spyOn(mockAssetService, 'getAssetByImoNumber').and.callThrough();
      const controller = makeController({ user: mockUser, eor: mockEORRecord });
      expect(mockAssetService.getAssetByImoNumber).toHaveBeenCalled();
      expect(controller.asset).not.toBe({});
    });

    it.async('showUpdateExpiryDateDialog() should open up $mdDialog', async () => {
      const controller = makeController({ user: mockUser, eor: mockEORRecord });
      spyOn($mdDialog, 'show').and.callThrough();

      controller.showUpdateExpiryDateDialog();
      expect($mdDialog.show).toHaveBeenCalled();
    });

    it('showRemoveEORModal() should open up $mdDialog', (done) => {
      const controller = makeController({ user: mockUser, eor: mockEORRecord });
      const showPromise = Promise.resolve();
      spyOn($mdDialog, 'show').and.returnValue(showPromise);
      spyOn(mockUserService, 'deleteUserEor');
      spyOn(mockState, 'reload');

      controller.showRemoveEORModal()
        .then(() => {
          expect($mdDialog.show).toHaveBeenCalled();
          expect(mockUserService.deleteUserEor)
            .toHaveBeenCalledWith(mockUser.userId, mockEORRecord.imoNumber);
          expect(mockState.reload).toHaveBeenCalledWith(true);
        })
        .then(done);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = EorAssetComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(EorAssetTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(EorAssetController);
    });
  });
});
