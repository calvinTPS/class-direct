import _ from 'lodash';
import Base from 'app/base';

export default class EorAssetsController extends Base {
  /* @ngInject */
  constructor(
    $state,
    AppSettings,
  ) {
    super(arguments);
  }

  $onInit() {
    this.eorRecords = _.get(this.user, ['eors']);
  }

  goToAddEorAccess() {
    this._$state.go(
      this._AppSettings.STATES.EDIT_USER,
      {
        id: this.user.userId,
        selectedUser: this.user,
        selectedAccountType: this._AppSettings.PERMISSION_TYPES.EOR,
      }
    );
  }
}
