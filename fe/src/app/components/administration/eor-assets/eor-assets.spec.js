/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import EorAssetsComponent from './eor-assets.component';
import EorAssetsController from './eor-assets.controller';
import EorAssetsModule from './';
import EorAssetsTemplate from './eor-assets.pug';
import utils from 'app/common/utils/utils.factory';

describe('EorAssets', () => {
  let $rootScope,
    $compile,
    $state,
    makeController;

  beforeEach(window.module(EorAssetsModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockUserSevice = {
      updateEorExpiryDate: () => new Promise((resolve, reject) => resolve(true)),
    };
    const mockAssetService = {
      getAssetByImoNumber: () => new Promise((resolve, reject) => resolve({})),
    };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('AppSettings', AppSettings);
      $provide.value('UserService', mockUserSevice);
      $provide.value('utils', utils());
      $provide.value('AssetService', mockAssetService);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$state_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $state = _$state_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('eorAssets', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('goToAddEorAccess will go to add user state', () => {
      const controller = makeController();
      const userObj = {
        userId: 101,
        email: 'john.doe@client.org',
      };
      const eorPermission = AppSettings.PERMISSION_TYPES.EOR;
      spyOn($state, 'go');

      controller.user = userObj;
      controller.goToAddEorAccess();
      expect($state.go).toHaveBeenCalledWith(
        AppSettings.STATES.EDIT_USER,
        {
          id: 101,
          selectedUser: userObj,
          selectedAccountType: AppSettings.PERMISSION_TYPES.EOR,
        }
      );
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = EorAssetsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(EorAssetsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(EorAssetsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<eor-assets foo="bar"><eor-assets/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('EorAssets');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
