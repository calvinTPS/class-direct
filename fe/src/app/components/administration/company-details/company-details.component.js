import './company-details.scss';
import controller from './company-details.controller';
import template from './company-details.pug';

export default {
  bindings: {
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
