/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import CompanyDetailsComponent from './company-details.component';
import CompanyDetailsController from './company-details.controller';
import CompanyDetailsModule from './';
import CompanyDetailsTemplate from './company-details.pug';

describe('CompanyDetails', () => {
  let $rootScope,
    $compile,
    makeController;

  const mockUser = {
    id: 1,
    userId: 101,
    equasis: true,
    thetis: false,
    flagCode: 100,
    shipBuilderCode: 1,
    clientCode: 4,
    name: 'John Doe',
    email: 'abc@bae.com',
    telephone: '07865432',
    city: 'WP',
    postCode: '50470',
    status: 1,
    company: 'Lloyds Register',
    address1: 'Address Line 1',
    address2: 'Address Line 2',
    address3: 'Address Line 3',
    country: 'UK',
  };

  const mockTranslateFilter = value => value;
  beforeEach(window.module(
    CompanyDetailsModule,
    { translateFilter: mockTranslateFilter },
  ));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = { user: mockUser }) => {
        const controller = $componentController('companyDetails',
          { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ user: mockUser });
      expect(controller.user).toEqual(mockUser);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CompanyDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CompanyDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CompanyDetailsController);
    });
  });
});
