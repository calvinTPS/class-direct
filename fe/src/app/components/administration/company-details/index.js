import angular from 'angular';
import companyDetailsComponent from './company-details.component';
import infoCardModule from 'app/common/info-card';
import uiRouter from 'angular-ui-router';

export default angular.module('company-details', [
  uiRouter,
  infoCardModule,
])
.component('companyDetails', companyDetailsComponent)
.name;
