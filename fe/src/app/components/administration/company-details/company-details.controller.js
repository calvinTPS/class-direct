import Base from 'app/base';

export default class CompanyDetailsController extends Base {
  /* @ngInject */
  constructor(
  ) {
    super(arguments);
  }

  $onInit() {
    this.sections = [
      {
        fields: [
          { label: 'cd-company', value: this.user.company.name },
          { label: 'cd-address-line-1', value: this.user.company.addressLine1 },
          { label: 'cd-address-line-2', value: this.user.company.addressLine2 },
          { label: 'cd-address-line-3', value: this.user.company.addressLine3 },
          { label: 'cd-city', value: this.user.company.city },
          { label: 'cd-state-province-region', value: this.user.company.state },
          { label: 'cd-zip-postal-code', value: this.user.company.postCode },
          { label: 'cd-country', value: this.user.company.country },
        ],
      },
    ];
  }
}
