/* @ngInject */
export default function config(
  $stateProvider,
  $urlRouterProvider,
) {
  // Redirect /administration to /administration/users
  $urlRouterProvider.when('/administration', '/administration/users');

  $stateProvider
    .state('administration', {
      url: '/administration',
      abstract: true,
      component: 'dashboard',
      resolve: {
        /* @ngInject */
        currentUser: UserService => UserService.getCurrentUser(),
      },
      onEnter: ($state, AppSettings, currentUser) => {
        if (!currentUser.isAdmin) {
          $state.go(AppSettings.STATES.VESSEL_LIST);
        }
      },
    })
    .state('administration.userList', {
      url: '/users',
      views: {
        'dashboard-content': {
          component: 'viewableUsers',
        },
      },
    })
    .state('administration.userDetails', {
      url: '/users/:id',
      views: {
        'dashboard-content': {
          component: 'viewableUserDetails',
        },
      },
      resolve: {
        /* @ngInject */
        user: ($stateParams, UserService) =>
          UserService.getUser($stateParams.id),
      },
    })
    .state('administration.addUser', {
      url: '/add-user',
      params: {
        selectedUser: undefined,
        selectedAccountType: undefined,
      },
      views: {
        'dashboard-content': {
          component: 'addUser',
        },
      },
    })
    .state('administration.editUser', {
      url: '/edit-user/:id',
      params: {
        selectedUser: undefined,
        selectedAccountType: undefined,
      },
      views: {
        'dashboard-content': {
          component: 'addUser',
        },
      },
    })
    .state('administration.accessibleAndRestrictedAssets', {
      url: '/users/:id/accessible-and-restricted-assets',
      views: {
        'dashboard-content': {
          component: 'accessibleAndRestrictedAssets',
        },
      },
      resolve: {
        /* @ngInject */
        user: ($stateParams, UserService) =>
          UserService.getUser($stateParams.id),

        /* @ngInject */
        accessibleAssets: ($stateParams, UserService) =>
          UserService.getAccessibleAssets($stateParams.id),
      },
    });
}
