/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AdminAccountDetailsComponent from './admin-account-details.component';
import AdminAccountDetailsController from './admin-account-details.controller';
import AdminAccountDetailsModule from './';
import AdminAccountDetailsTemplate from './admin-account-details.pug';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import User from 'app/common/models/user/user';
import utils from 'app/common/utils/utils.factory';

describe('AdminAccountDetails', () => {
  let $rootScope;
  let $compile;
  let $mdDialog;
  let makeController;

  const mockUser = {
    userId: 1,
    creationDate: '2016-01-01',
    expiryDate: '2016-12-19',
  };
  const mockUserService = {
    editUser: () => Promise.resolve({ userAccExpiryDate: '2017-01-01' }),
  };
  const mockTranslateFilter = value => value;

  beforeEach(window.module(
    AdminAccountDetailsModule,
    angularMaterial,
    { translateFilter: mockTranslateFilter },
  ));

  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('UserService', mockUserService);
      $provide.value('AppSettings', AppSettings);
      $provide.value('utils', utils());
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      (bindings = { user: mockUser }) => {
        const controller = $componentController('adminAccountDetails', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    /* top-level specs: i.e., routes, injection, naming*/
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ user: mockUser });
      expect(controller.user).toEqual(mockUser);
      expect((controller.now).getDate()).toEqual((new Date()).getDate());
    });

    it.async('updateAccountExpiryDate should call UserService.editUser', async (done) => {
      const controller = makeController({ user: mockUser, updateState: () => {} });
      let mockForm = {
        $valid: false,
      };
      controller.newAccountExpiryDate = new Date('2017-01-01T00:00Z');
      spyOn(mockUserService, 'editUser').and.callThrough();

      await controller.updateAccountExpiryDate(mockForm);
      expect(controller.newAccountExpiryDate).not.toBe(null);
      expect(mockUserService.editUser).not.toHaveBeenCalled();

      mockForm = {
        $valid: true,
      };
      await controller.updateAccountExpiryDate(mockForm);

      expect(mockUserService.editUser).toHaveBeenCalledTimes(1);
      expect(mockUserService.editUser).toHaveBeenCalledWith(1, {
        userAccExpiryDate: '2017-01-01',
      });
      expect(controller.newAccountExpiryDate).toBe(null);
    });

    it.async('showUpdateAccountExpiryDateModal() should open up $mdDialog', async () => {
      const controller = makeController({ user: mockUser });
      spyOn($mdDialog, 'show').and.callThrough();
      controller.showUpdateAccountExpiryDateModal();
      expect($mdDialog.show).toHaveBeenCalled();
    });

    it('get proper value for latestEORAssetExpiryDate', () => {
      const controller = makeController();
      controller.user.eors = [];
      expect(controller.latestEORAssetExpiryDate).toBe(null);
      controller.user.eors.push({ assetExpiryDate: '2018-05-09' });
      controller.user.eors.push({ assetExpiryDate: '2017-01-01' });
      expect(controller.latestEORAssetExpiryDate).toBe('2018-05-09');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AdminAccountDetailsComponent;
    it('includes the intended template', () => {
      expect(component.template).toEqual(AdminAccountDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AdminAccountDetailsController);
    });
  });

  describe('Rendering', () => {
    let scope, element;
    const userObj = {
      userAccExpiryDate: 'USER_ACCOUNT_EXPIRY_DATE',
      roles: [],
    };

    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element('<admin-account-details user="user"><admin-account-details/>');
      scope.user = new User(userObj);
      element = $compile(element)(scope);
      scope.$apply();
    });

    it('renders correctly', () => {
      expect(element.text().indexOf('USER_ACCOUNT_EXPIRY_DATE')).not.toEqual(-1);
    });
  });
});
