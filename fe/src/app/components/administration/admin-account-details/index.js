import adminAccountDetailsComponent from './admin-account-details.component';
import angular from 'angular';
import uiRouter from 'angular-ui-router';

export default angular.module('admin-account-details', [
  uiRouter,
])
.component('adminAccountDetails', adminAccountDetailsComponent)
.name;
