import * as _ from 'lodash';
import Base from 'app/base';
import dateHelper from 'app/common/helpers/date-helper';
import editExpiryDateModalTemplate from './_partials/edit-expiry-date-modal.pug';

export default class AdminAccountDetailsController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $scope,
    UserService,
    utils,
  ) {
    super(arguments);
  }

  $onInit() {
    this.now = new Date();
    if (this.user.isEORUserOnly) {
      this.user.userAccExpiryDate = this.latestEORAssetExpiryDate;
    }
  }

  showUpdateAccountExpiryDateModal(evt) {
    return this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: false,
      controller: () => this,
      controllerAs: 'vm',
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: editExpiryDateModalTemplate,
    })
      .then((form) => {
        this.updateAccountExpiryDate(form);
      });
  }

  async updateAccountExpiryDate(form) {
    if (form.$valid) {
      const userObj = await this._UserService.editUser(this.user.userId,
        {
          userAccExpiryDate: dateHelper.formatDateServer(this.newAccountExpiryDate),
        });
      this.newAccountExpiryDate = null;
      this.user.expiryDate = userObj.userAccExpiryDate;
      this._$scope.$apply();
    }
  }

  get latestEORAssetExpiryDate() {
    if (this.user.eors.length) {
      return _.maxBy(this.user.eors, 'assetExpiryDate').assetExpiryDate;
    }
    return null;
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
