import './admin-account-details.scss';
import controller from './admin-account-details.controller';
import template from './admin-account-details.pug';

export default {
  bindings: {
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
