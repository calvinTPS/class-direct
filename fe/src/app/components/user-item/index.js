import angular from 'angular';
import uiRouter from 'angular-ui-router';
import userItemComponent from './user-item.component';

export default angular.module('userItem', [
  uiRouter,
])
.component('userItem', userItemComponent)
.name;
