import './user-item.scss';
import controller from './user-item.controller';
import template from './user-item.pug';

export default {
  bindings: {
    item: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
