/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import UserItemComponent from './user-item.component';
import UserItemController from './user-item.controller';
import UserItemModule from './';
import UserItemTemplate from './user-item.pug';

describe('UserItem', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(UserItemModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
      $provide.value('roleTranslateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('userItem', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = UserItemComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(UserItemTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(UserItemController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    const itemObj = {
      roles: [],
    };
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<user-item item="item"><user-item/>');
      scope.item = itemObj;
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME

      scope.$apply();

      controller = element.controller('UserItem');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
