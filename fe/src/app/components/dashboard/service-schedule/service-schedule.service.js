import * as _ from 'lodash';
import Base from 'app/base';

export default class ServiceScheduleService extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    ViewService,
  ) {
    super(arguments);

    this.viewTypeOptions = _.cloneDeep(this._AppSettings.SERVICE_SCHEDULE.VIEW_TYPES);
    this.viewLocation = this._AppSettings.SERVICE_SCHEDULE.LOCATIONS.SERVICE_SCHEDULE_LISTING;

    this.viewType = this._ViewService.getServiceScheduleViewType(this.viewLocation) ||
      this.viewTypeOptions.TABULAR;
  }

  get viewType() {
    return this._viewType;
  }

  set viewType(arg) {
    this._viewType = arg;
  }
}
