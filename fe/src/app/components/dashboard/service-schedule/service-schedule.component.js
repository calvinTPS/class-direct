import './service-schedule.scss';
import controller from './service-schedule.controller';
import template from './service-schedule.pug';

export default {
  restrict: 'E',
  bindings: {
    assetsServiceToDefer: '<',
    currentUser: '<',
    dateRange: '<',
  },
  template,
  controllerAs: 'vm',
  controller,
  transclude: true,
};
