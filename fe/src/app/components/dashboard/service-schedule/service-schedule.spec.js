/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import mockAppSettings from 'app/config/project-variables.js';
import ServiceScheduleComponent from './service-schedule.component';
import ServiceScheduleController from './service-schedule.controller';
import ServiceScheduleModule from './';
import ServiceScheduleTemplate from './service-schedule.pug';

describe('ServiceSchedule', () => {
  let $rootScope,
    $compile,
    mockDashboardService,
    makeController,
    mockAssets,
    mockServiceScheduleService,
    mockAssetService;

  beforeEach(window.module(ServiceScheduleModule));

  beforeEach(() => {
    mockAssets = [
      { id: '0', selected: true },
      { id: '1', selected: false },
      { id: '2', selected: true },
      { id: '3', selected: false },
    ];
    mockAssets.pagination = { totalElements: 10 };
    mockAssetService = {
      removeFavourite: () => {},
      setFavourite: () => {},
      updateFavourites: () => Promise.resolve({}),
    };
    mockDashboardService = {
      assets: () => mockAssets,
      submitQuery: () => {},
      queryOptions: {
        isFavourite: true,
      },
    };
    const mockUser = {
      shouldTruncateSearchResults: false,
    };
    mockServiceScheduleService = {
      viewType: () => 'tabular',
    };
    const mockState = {
      go: () => {},
      reload: () => {},
    };

    const mockViewService = {
      setServiceScheduleViewType: () => true,
      getServiceScheduleViewType: () => true,
    };

    const mockUserService = {
      getCurrentUser: () => Promise.resolve(mockUser),
    };

    window.module(($provide) => {
      $provide.value('$mdMedia', {});
      $provide.value('AppSettings', mockAppSettings);
      $provide.value('AssetService', mockAssetService);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('UserService', mockUserService);
      $provide.value('$state', mockState);
      $provide.value('ViewService', mockViewService);
      $provide.value('ServiceScheduleService', mockServiceScheduleService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('serviceSchedule', { $dep: dep }, bindings);
        controller._$rootScope = $rootScope;
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('has a name property', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
      expect(controller._storedDateRange).toBeDefined();
    });

    it('resetQuery() should reload the fleet page with fresh query parameters', () => {
      const controller = makeController();
      spyOn(controller._$state, 'go');

      controller.resetQuery();
      expect(controller._$state.go).toHaveBeenCalled();
    });

    describe('updateFilterOptions', () => {
      it('updates the filter options of the sidebar', () => {
        const controller = makeController();

        controller._updateFilterOptions({ isFavourite: false });
        expect(mockDashboardService.queryOptions).toEqual({ isFavourite: false });

        controller._updateFilterOptions({ isFavourite: true });
        expect(mockDashboardService.queryOptions).toEqual({ isFavourite: true });
      });

      it('calls submitQuery of DashboardService', () => {
        const controller = makeController();

        spyOn(mockDashboardService, 'submitQuery');

        controller._updateFilterOptions({ isFavourite: false });
        expect(mockDashboardService.submitQuery).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('showNoFavouritesMessage', () => {
    it('should only show no favourites message when certain conditions are met', () => {
      const controller = makeController();

      mockDashboardService.isUnfilteredQuery = false;
      mockDashboardService.assets = [];
      mockAssetService.isQuerying = false;
      mockDashboardService.isUnfilteredFavouritesQuery = true;

      expect(controller.showNoFavouritesMessage).toBe(true);

      // Any changes to condition results in false

      mockAssetService.isQuerying = !mockAssetService.isQuerying; // flip
      expect(controller.showNoFavouritesMessage).toBe(false);
      mockAssetService.isQuerying = !mockAssetService.isQuerying; // restore

      mockDashboardService.isUnfilteredFavouritesQuery =
        !mockDashboardService.isUnfilteredFavouritesQuery;
      expect(controller.showNoFavouritesMessage).toBe(false);
      mockDashboardService.isUnfilteredFavouritesQuery =
        !mockDashboardService.isUnfilteredFavouritesQuery;

      mockDashboardService.assets = [{}];
      expect(controller.showNoFavouritesMessage).toBe(false);
      mockDashboardService.assets = [];
    });

    describe('showNoResultsMessage', () => {
      it('should only show no results message when certain conditions are met', () => {
        const controller = makeController();

        // showNoFavouritesMessage is a getter, we want to override it.
        Object.defineProperty(controller, 'showNoFavouritesMessage', {
          value: false,
          writable: true,
        });
        mockAssetService.isQuerying = false;
        mockDashboardService.assets = [];
        mockAssetService.isUnfilteredQuery = false;

        expect(controller.showNoResultsMessage).toBe(true);

        // Any changes to condition results in false

        mockAssetService.isQuerying = !mockAssetService.isQuerying;
        expect(controller.showNoResultsMessage).toBe(false);
        mockAssetService.isQuerying = !mockAssetService.isQuerying;

        mockDashboardService.assets = [{}];
        expect(controller.showNoResultsMessage).toBe(false);
        mockDashboardService.assets = [];

        controller.showNoFavouritesMessage = true;
        expect(controller.showNoResultsMessage).toBe(false);
      });
    });

    describe('showTooManyResultsMessage', () => {
      it('should only show first query state message when certain conditions are met', () => {
        const controller = makeController();

        mockAssetService.isQuerying = false;
        mockDashboardService.isUnfilteredQuery = true;
        mockDashboardService.forcedQuerySubmission = false;
        controller.user = {
          shouldTruncateSearchResults: true,
        };

        expect(controller.showTooManyResultsMessage).toBe(true);

        mockAssetService.isQuerying = !mockAssetService.isQuerying;
        expect(controller.showTooManyResultsMessage).toBe(false);
        mockAssetService.isQuerying = !mockAssetService.isQuerying;

        mockDashboardService.isUnfilteredQuery = !mockDashboardService.isUnfilteredQuery;
        expect(controller.showTooManyResultsMessage).toBe(false);
        mockDashboardService.isUnfilteredQuery = !mockDashboardService.isUnfilteredQuery;

        mockDashboardService.forcedQuerySubmission = !mockDashboardService.forcedQuerySubmission;
        expect(controller.showTooManyResultsMessage).toBe(false);
        mockDashboardService.forcedQuerySubmission = !mockDashboardService.forcedQuerySubmission;
      });
    });
    it('isShowActionBar(), correctly returns true/false', () => {
      const controller = makeController({});

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isShowActionBar).toEqual(false);

      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isShowActionBar).toEqual(true);

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = { LRV1: 'LRV1' };
      expect(controller.isShowActionBar).toEqual(true);
    });

    it('isFavouriteAsset(), correctly returns whether an asset is in the favourites list', () => {
      const controller = makeController({});

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isFavouriteAsset('LRV1')).toBeUndefined();

      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isFavouriteAsset('LRV1')).toEqual(true);

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = { LRV1: 'LRV1' };
      expect(controller.isFavouriteAsset('LRV1')).toEqual(false);
    });

    it('saveFavourites(), correctly saves the selected assets as favourites', () => {
      const controller = makeController({});
      spyOn(controller._$rootScope, '$broadcast');
      spyOn(controller._AssetService, 'updateFavourites');
      spyOn(controller._DashboardService, 'submitQuery');

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = {};

      controller.saveFavourites();
      expect(controller._$rootScope.$broadcast).not.toHaveBeenCalled();
      expect(controller._AssetService.updateFavourites).not.toHaveBeenCalled();
      expect(controller._DashboardService.submitQuery).not.toHaveBeenCalled();

      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = { LRV2: 'LRV2' };

      controller.saveFavourites();
      expect(controller.isSaving).toEqual(true);
      expect(controller._$rootScope.$broadcast).toHaveBeenCalled();
      expect(controller._AssetService.updateFavourites).toHaveBeenCalled();
    });

    it('cancelSaveFavourites(), correctly reverts the favourites state to blank/empty', () => {
      const controller = makeController({});

      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = { LRV1: 'LRV1' };

      controller.cancelSaveFavourites();
      expect(controller._DashboardService.favourites.toAdd).toEqual({});
      expect(controller._DashboardService.favourites.toRemove).toEqual({});
    });

    it('renderTabularView(), tabular view returns the correct result', () => {
      mockServiceScheduleService.viewType = 'tabular';
      const controller = makeController({});
      expect(controller.renderTabularView).toEqual(true);
    });

    it('renderGanttView(), tabular view returns the correct result', () => {
      mockServiceScheduleService.viewType = 'gantt';
      const controller = makeController({});
      expect(controller.renderGanttView).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ServiceScheduleComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ServiceScheduleTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ServiceScheduleController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<service-schedule foo="bar"><service-schedule/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ServiceSchedule');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
