import * as _ from 'lodash';
import Base from 'app/base';
import RouteMixins from 'app/route.mixin';

export default class ServiceScheduleController extends RouteMixins(Base) {
  /* @ngInject */
  constructor(
    $mdMedia,
    $scope, // used by RouteMixin
    $state,
    $rootScope,
    $log,
    AppSettings,
    ViewService,
    AssetService,
    DashboardService,
    ServiceScheduleService,
    UserService,
  ) {
    super(arguments);
  }

  onDatePickerChange(options) {
    this._updateFilterOptions({
      codicilDueDateMax: new Date(options.maxDate),
      codicilDueDateMin: new Date(options.minDate),
    });
  }

  $onInit() {
    super.$onInit();
    this._DashboardService.activeServicesFlag = true;
    this._UserService.getCurrentUser().then((user) => {
      this.user = user;
    });

    this.dateMin = _.get(this.dateRange, 'lowRange');
    this.dateMax = _.get(this.dateRange, 'highRange');
    // Save the default date range
    this._storedDateRange = _.cloneDeep({
      minDate: this.dateMin,
      maxDate: this.dateMax,
    });

    this.viewTypeOptions = _.cloneDeep(this._AppSettings.SERVICE_SCHEDULE.VIEW_TYPES);

    this._DashboardService.favourites = {
      // Using object insted of array since its easier to add/remove entry
      // e.g { LRV10: 'LRV10', LRV99: 'LRV99'}
      toAdd: {},
      toRemove: {},
    };

    const { SET, REMOVE } = ServiceScheduleController.FAV_UPDATE_TYPE;
    this._ViewService.isSurveyScheduleViewToggled = false;
    this._$scope.$on(this._AppSettings.EVENTS.ON_TOGGLE_FAVOURITE, (event, payload) => {
      const { type, assetId } = payload;

      // SET means to add an asset to the favourites list
      if (type === SET) {
        // If it was added to the toRemove list earlier,
        // meaning it was a favourite asset that the user had just clicked to unfavourite/remove,
        // just remove it from the toRemove list
        if (this._DashboardService.favourites.toRemove[assetId]) {
          delete this._DashboardService.favourites.toRemove[assetId];
        // Else add it to the toAdd list
        } else {
          this._DashboardService.favourites.toAdd[assetId] = assetId;
        }
      }

      // REMOVE means to remove an asset from the favourites list
      if (type === REMOVE) {
        // If it was added to the toAdd list earlier,
        // meaning it was NOT a favourite asset that the user had just clicked to favourite/add,
        // just remove it from the toAdd list
        if (this._DashboardService.favourites.toAdd[assetId]) {
          delete this._DashboardService.favourites.toAdd[assetId];
        // Else add it to the toRemove list
        } else {
          this._DashboardService.favourites.toRemove[assetId] = assetId;
        }
      }
    });
  }

  $onDestroy() {
    this._DashboardService.activeServicesFlag = false;
  }

  get viewType() {
    return this._ServiceScheduleService.viewType;
  }

  get assets() {
    return this._DashboardService.assets;
  }

  /**
   * Show No Favourites message when:
   * 1. No results, AND
   * 2. Query is not pending, AND
   * 3. isFavourite: true is in query, AND
   * 4. User does NOT currently have any favourite assets
   *
   * @returns {boolean}
   */
  get showNoFavouritesMessage() {
    const emptyResults = this.assets && this.assets.length === 0;
    const isQuerying = this._AssetService.isQuerying;
    const isUnfilteredFavouritesQuery = this._DashboardService.isUnfilteredFavouritesQuery;
    return emptyResults
      && !isQuerying
      && isUnfilteredFavouritesQuery;
  }

  /**
   * Show No Results message when:
   * 1. No results, AND
   * 2. Query is not pending, AND
   * 3. Conditions to show No Favourites message is not met.
   *
   * @returns {boolean}
   */
  get showNoResultsMessage() {
    const emptyResults = this.assets && this.assets.length === 0;
    const isQuerying = this._AssetService.isQuerying;
    return emptyResults
      && !isQuerying
      && !this.showNoFavouritesMessage
      && !this.showTooManyResultsMessage;
  }

  /**
   * Show first query state when:
   * 1. No results, AND
   * 2. Query is not pending, AND
   * 3. isUnfilteredQuery: true
   */
  get showTooManyResultsMessage() {
    const isQuerying = this._AssetService.isQuerying;
    const isUnfilteredQuery = this._DashboardService.isUnfilteredQuery;
    const forcedQuerySubmission = this._DashboardService.forcedQuerySubmission;
    const userShouldTruncateSearchResults = this.user && this.user.shouldTruncateSearchResults;
    return !isQuerying
      && isUnfilteredQuery
      && userShouldTruncateSearchResults
      && !forcedQuerySubmission;
  }
  resetQuery(boolean) {
    this._$state.go(this._$state.current, { skipFavourites: boolean }, { reload: true });
  }

  get isQuerying() {
    return this._AssetService.isQuerying;
  }

  async _updateFilterOptions(changedOption) {
    _.assign(this._DashboardService.queryOptions, changedOption);
    await this._DashboardService.submitQuery();
    this._$scope.$apply();
  }

  get isShowActionBar() {
    return !!(_.keys(this._DashboardService.favourites.toAdd).length ||
     _.keys(this._DashboardService.favourites.toRemove).length);
  }


  /**
   * @param string assetId
   * This function will be used when rendering the asset card to figure out whether to show in
   * favourite or not favourite state.
   */
  isFavouriteAsset(assetId) {
    let val; // undefined (if undefined Star Controller will use the value from asset.isFavourite)

    // If the asset had been clicked TO ADD to the favourites list(not saved yet)
    // - actually not a favourite asset yet but temporarily in the favourite state
    if (this._DashboardService.favourites.toAdd[assetId]) {
      val = true;

    // If the asset had been clicked TO REMOVE from the favourites list(not saved yet)
    // - actually a favourite asset but temporarily NOT in the favourite state
    } else if (this._DashboardService.favourites.toRemove[assetId]) {
      val = false;
    }

    return val; // return true || false || undefined
  }

  async saveFavourites() {
    const add = _.keys(this._DashboardService.favourites.toAdd);
    const remove = _.keys(this._DashboardService.favourites.toRemove);

    if (add.length || remove.length) {
      this.isSaving = true;
      this._$rootScope.$broadcast(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, true);

      const payload = { add, remove };

      try {
        await this._AssetService.updateFavourites(payload);
        // Reload the assets so pagination continues to work correctly
        // In favourites view, once you add/remove assets, the assets list in database
        // and data in pagination becomes out of sync
        await this._DashboardService.submitQuery();
        this.isSaving = false;
        this._$rootScope.$broadcast(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, false);
        this._DashboardService.favourites.toAdd = {};
        this._DashboardService.favourites.toRemove = {};
        this._$scope.$apply();
      } catch (err) {
        this.isSaving = false;
        this._$rootScope.$broadcast(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, false);
      }
    }
  }

  cancelSaveFavourites() {
    _.forEach(this._DashboardService.favourites, (assetIds, actionType) => {
      _.forEach(assetIds, (assetId) => {
        this._$rootScope.$broadcast(this._AppSettings.EVENTS.UPDATE_FAVOURITE, {
          assetId,
          // actionType === 'toRemove || 'toAdd', see this._favourites
          // entries in toRemove were initially a favourite,
          // and entries in toAdd were initially NOT a favourite
          isFavourite: actionType === 'toRemove',
        });
      });
    });

    this._DashboardService.favourites.toAdd = {};
    this._DashboardService.favourites.toRemove = {};
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }

  get renderTabularView() {
    return this.viewType === this.viewTypeOptions.TABULAR;
  }

  get renderGanttView() {
    return this.viewType === this.viewTypeOptions.GANTT;
  }

  static FAV_UPDATE_TYPE = {
    SET: 'set',
    REMOVE: 'remove',
  }
}
