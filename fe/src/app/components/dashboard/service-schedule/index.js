import angular from 'angular';
import serviceScheduleComponent from './service-schedule.component';
import ServiceScheduleService from './service-schedule.service';
import uiRouter from 'angular-ui-router';

export default angular.module('serviceSchedule', [
  uiRouter,
])
.service({
  ServiceScheduleService,
  // @insertInstance
})
.component('serviceSchedule', serviceScheduleComponent)
.name;
