/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, no-underscore-dangle, no-unused-vars */

import mockAppSettings from 'app/config/project-variables.js';
import VesselListComponent from './vessel-list.component';
import VesselListController from './vessel-list.controller';
import VesselListModule from './';
import VesselListTemplate from './vessel-list.pug';

describe('VesselList', () => {
  let $rootScope;
  let $compile;
  let makeController;
  let mockAssetService;
  let mockDashboardService;
  let mockUserService;
  let mockUser;

  beforeEach(window.module(VesselListModule));

  beforeEach(() => {
    mockUser = {
      shouldTruncateSearchResults: false,
    };
    mockAssetService = {
      removeFavourite: () => {},
      setFavourite: () => {},
    };
    mockDashboardService = {
      assets: () => [],
      queryOptions: {
        isFavourite: true,
      },
      reset: () => {},
    };
    mockUserService = {
      getCurrentUser: () => Promise.resolve(mockUser),
    };

    const mockState = {
      go: () => {},
      reload: () => {},
    };

    window.module(($provide) => {
      $provide.value('AppSettings', mockAppSettings);
      $provide.value('AssetService', mockAssetService);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('UserService', mockUserService);
      $provide.value('$state', mockState);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('vesselList', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('resetQuery() should reload the fleet page with fresh query parameters', () => {
      const controller = makeController();
      spyOn(controller._$state, 'go');
      spyOn(mockDashboardService, 'reset');

      controller.resetQuery();
      expect(mockDashboardService.reset).toHaveBeenCalled();
      expect(controller._$state.go).toHaveBeenCalled();
    });

    describe('showNoFavouritesMessage', () => {
      it('should only show no favourites message when certain conditions are met', () => {
        const controller = makeController();

        mockDashboardService.isUnfilteredQuery = false;
        mockDashboardService.assets = [];
        mockAssetService.isQuerying = false;
        mockDashboardService.isUnfilteredFavouritesQuery = true;

        expect(controller.showNoFavouritesMessage).toBe(true);

        // Any changes to condition results in false

        mockAssetService.isQuerying = !mockAssetService.isQuerying; // flip
        expect(controller.showNoFavouritesMessage).toBe(false);
        mockAssetService.isQuerying = !mockAssetService.isQuerying; // restore

        mockDashboardService.isUnfilteredFavouritesQuery =
          !mockDashboardService.isUnfilteredFavouritesQuery;
        expect(controller.showNoFavouritesMessage).toBe(false);
        mockDashboardService.isUnfilteredFavouritesQuery =
          !mockDashboardService.isUnfilteredFavouritesQuery;

        mockDashboardService.assets = [{}];
        expect(controller.showNoFavouritesMessage).toBe(false);
        mockDashboardService.assets = [];
      });
    });

    describe('showNoResultsMessage', () => {
      it('should only show no results message when certain conditions are met', () => {
        const controller = makeController();

        // showNoFavouritesMessage is a getter, we want to override it.
        Object.defineProperty(controller, 'showNoFavouritesMessage', {
          value: false,
          writable: true,
        });
        mockAssetService.isQuerying = false;
        mockDashboardService.assets = [];
        mockAssetService.isUnfilteredQuery = false;

        expect(controller.showNoResultsMessage).toBe(true);

        // Any changes to condition results in false

        mockAssetService.isQuerying = !mockAssetService.isQuerying;
        expect(controller.showNoResultsMessage).toBe(false);
        mockAssetService.isQuerying = !mockAssetService.isQuerying;

        mockDashboardService.assets = [{}];
        expect(controller.showNoResultsMessage).toBe(false);
        mockDashboardService.assets = [];

        controller.showNoFavouritesMessage = true;
        expect(controller.showNoResultsMessage).toBe(false);
      });
    });

    describe('showTooManyResultsMessage', () => {
      it('should only show first query state message when certain conditions are met', () => {
        const controller = makeController();

        mockAssetService.isQuerying = false;
        mockDashboardService.isUnfilteredQuery = true;
        mockDashboardService.forcedQuerySubmission = false;
        controller.user = {
          shouldTruncateSearchResults: true,
        };

        expect(controller.showTooManyResultsMessage).toBe(true);

        mockAssetService.isQuerying = !mockAssetService.isQuerying;
        expect(controller.showTooManyResultsMessage).toBe(false);
        mockAssetService.isQuerying = !mockAssetService.isQuerying;

        mockDashboardService.isUnfilteredQuery = !mockDashboardService.isUnfilteredQuery;
        expect(controller.showTooManyResultsMessage).toBe(false);
        mockDashboardService.isUnfilteredQuery = !mockDashboardService.isUnfilteredQuery;

        mockDashboardService.forcedQuerySubmission = !mockDashboardService.forcedQuerySubmission;
        expect(controller.showTooManyResultsMessage).toBe(false);
        mockDashboardService.forcedQuerySubmission = !mockDashboardService.forcedQuerySubmission;
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = VesselListComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(VesselListTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(VesselListController);
    });
  });

  describe('Rendering', () => {
    let controller;
    let element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<vessel-list foo="bar"><vessel-list/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('VesselList');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
