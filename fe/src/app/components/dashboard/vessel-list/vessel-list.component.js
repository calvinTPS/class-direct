import './vessel-list.scss';
import controller from './vessel-list.controller';
import template from './vessel-list.pug';

export default {
  bindings: {
    assetsToDefer: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
