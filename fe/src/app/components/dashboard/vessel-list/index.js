import angular from 'angular';
import vesselListComponent from './vessel-list.component';

export default angular.module('vesselList', [])
.component('vesselList', vesselListComponent)
.name;
