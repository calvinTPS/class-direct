import Base from 'app/base';
import RouteMixins from 'app/route.mixin';

export default class VesselListController extends RouteMixins(Base) {
  /* @ngInject */
  constructor(
    $scope, // used by RouteMixin
    $state,
    AppSettings,
    AssetService,
    DashboardService,
    UserService,
  ) {
    super(arguments);
  }

  $onInit() {
    super.$onInit();
    this._UserService.getCurrentUser().then((user) => {
      this.user = user;
    });
  }

  get assets() {
    return this._DashboardService.assets;
  }

  /**
   * Show No Favourites message when:
   * 1. No results, AND
   * 2. Query is not pending, AND
   * 3. isFavourite: true is in query and query should return all favourites, AND
   *
   * @returns {boolean}
   */
  get showNoFavouritesMessage() {
    const emptyResults = this.assets && this.assets.length === 0;
    const isQuerying = this._AssetService.isQuerying;
    const isUnfilteredFavouritesQuery = this._DashboardService.isUnfilteredFavouritesQuery;
    return emptyResults
      && !isQuerying
      && isUnfilteredFavouritesQuery;
  }

  /**
   * Show No Results message when:
   * 1. No results, AND
   * 2. Query is not pending, AND
   * 3. Conditions to show Too Many Results message is not met.
   * 4. Conditions to show No Favourites message is not met.
   *
   * @returns {boolean}
   */
  get showNoResultsMessage() {
    const emptyResults = this.assets && this.assets.length === 0;
    const isQuerying = this._AssetService.isQuerying;
    return emptyResults
      && !isQuerying
      && !this.showNoFavouritesMessage
      && !this.showTooManyResultsMessage;
  }

  /**
   * Show first query state when:
   * 1. No results, AND
   * 2. Query is not pending, AND
   * 3. isUnfilteredQuery: true
   */
  get showTooManyResultsMessage() {
    const isQuerying = this._AssetService.isQuerying;
    const isUnfilteredQuery = this._DashboardService.isUnfilteredQuery;
    const forcedQuerySubmission = this._DashboardService.forcedQuerySubmission;
    const userShouldTruncateSearchResults = this.user && this.user.shouldTruncateSearchResults;
    return !isQuerying
      && isUnfilteredQuery
      && userShouldTruncateSearchResults
      && !forcedQuerySubmission;
  }

  resetQuery(boolean) {
    this._DashboardService.reset();
    this._$state.go(this._$state.current, { skipFavourites: boolean }, { reload: true });
  }

  get isQuerying() {
    return this._AssetService.isQuerying;
  }
}
