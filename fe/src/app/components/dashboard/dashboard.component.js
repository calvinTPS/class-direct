import './dashboard.scss';
import controller from './dashboard.controller';
import template from './dashboard.pug';

export default {
  bindings: {
    assets: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
