/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, angular/timeout-service,
no-underscore-dangle, no-unused-vars */
import _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import DashboardService from './dashboard.service';

describe('Dashboard Service', () => {
  let dashboardService;
  let mockAssetService;
  let mockUserService;
  let mockUtils;

  beforeEach(inject(() => {
    mockAssetService = {
      query: () => { },
    };
    mockUserService = {
      getCurrentUser: () => { },
    };
    mockUtils = {
      hasUnfilteredQuery: () => true,
    };
    dashboardService = new DashboardService(
      {},
      AppSettings,
      mockAssetService,
      mockUserService,
      mockUtils
    );
  }));

  describe('dashboardFactory', () => {
    it('should get/set assets', () => {
      expect(dashboardService.assets).toEqual([]);
      dashboardService.assets = [{ id: 1 }, { id: 2 }, { id: 3 }];
      expect(dashboardService.assets).toEqual([{ id: 1 }, { id: 2 }, { id: 3 }]);

      dashboardService.assets = [];
      expect(dashboardService.assets).toEqual([]);
    });

    it('reset() should set the dashboard properties to their default  values', () => {
      dashboardService.reset();
      expect(dashboardService.queryOptions).toEqual({});
      expect(dashboardService._assets).toEqual([]);
      expect(dashboardService._queryParams).toEqual({});
      expect(dashboardService._submissionId).toEqual(-1);
      expect(dashboardService._isUnfilteredQueryExcludingSearch).toBeTruthy();
      expect(dashboardService._activeServicesFlag).toBeFalsy();
      expect(dashboardService._hasAssetsInitialised).toBeFalsy();
      expect(dashboardService._previousQuery).toEqual({});
      expect(dashboardService._forceQuery).toBeFalsy();
      expect(dashboardService._assetsSearchTerm).toEqual('');
      expect(dashboardService._searchAssetSelectedSortType).toBeNull();
      expect(dashboardService._searchAssetsCategories).toBeNull();
      expect(dashboardService._searchAssetsCategories).toBeNull();
    });

    it('should get/set queryOptions', () => {
      expect(dashboardService.queryOptions).toEqual({});

      spyOn(dashboardService, 'submitQuery').and.stub();

      // Set it twice
      dashboardService.queryOptions = { queryString: '*Hello World*' };
      dashboardService.queryOptions = { queryString: '*Hello World*' };
      expect(dashboardService.submitQuery).toHaveBeenCalledTimes(0);
      expect(dashboardService.queryOptions).toEqual({ queryString: '*Hello World*' });

      dashboardService.submitQuery.calls.reset();

      // Set it again to different query
      dashboardService.queryOptions = { queryString: '*Hello World II*' };
      expect(dashboardService.submitQuery).toHaveBeenCalledTimes(0);
      expect(dashboardService.queryOptions).toEqual({ queryString: '*Hello World II*' });
    });

    it('should get/set queryParams', () => {
      expect(dashboardService.queryParams).toEqual({});

      spyOn(dashboardService, 'submitQuery').and.stub();

      // Set it twice
      dashboardService.queryParams = { queryString: '*Hello World*' };
      dashboardService.queryParams = { queryString: '*Hello World*' };
      expect(dashboardService.submitQuery).toHaveBeenCalledTimes(0);
      expect(dashboardService.queryParams).toEqual({ queryString: '*Hello World*' });

      dashboardService.submitQuery.calls.reset();

      // Set it again to different query
      dashboardService.queryParams = { queryString: '*Hello World II*' };
      expect(dashboardService.submitQuery).toHaveBeenCalledTimes(0);
      expect(dashboardService.queryParams).toEqual({ queryString: '*Hello World II*' });
    });

    it('filterOptionsChanged() should append queryOptions', () => {
      spyOn(dashboardService, 'submitQuery').and.stub();

      expect(dashboardService.queryOptions).toEqual({});
      dashboardService.queryOptions = { queryString: '*Hello World*' };
      expect(dashboardService.queryOptions).toEqual({ queryString: '*Hello World*' });

      dashboardService.filterOptionsChanged({ anotherQueryString: '*More query*' });
      expect(dashboardService.queryOptions).toEqual({
        queryString: '*Hello World*',
        anotherQueryString: '*More query*',
      });
      expect(dashboardService.submitQuery).toHaveBeenCalledTimes(0);
    });

    it('getAssetsSearchQuery() should return the correct value', () => {
      dashboardService.searchAssetsSelectedCategory = { query: 'Foo' };
      spyOn(dashboardService, 'removeFilterOption').and.stub();

      expect(dashboardService.getAssetsSearchQuery()).toEqual({});
      expect(dashboardService.removeFilterOption).toHaveBeenCalled();

      dashboardService._canSearchAssets = true;
      dashboardService.assetsSearchTerm = 'Hello World';
      const query = 'abc';
      dashboardService.searchAssetsSelectedCategory = {
        name: 'XXX',
        query,
        type: 'foo',
        wildcard: true,
      };
      expect(dashboardService.getAssetsSearchQuery()[query]).toEqual(`*${dashboardService.assetsSearchTerm}*`);
    });

    it('previousQuery getter/setter should get and assign the right values', () => {
      expect(dashboardService.previousQuery).toEqual({});
      const previousQuery = {
        isFavourite: true,
      };
      dashboardService.previousQuery = previousQuery;
      expect(dashboardService.previousQuery).toEqual(previousQuery);
    });

    it('forceQuery getter/setter should get and assign the right values', () => {
      expect(dashboardService.forceQuery).toEqual(false);
      dashboardService.forceQuery = true;
      expect(dashboardService.forceQuery).toEqual(true);
    });

    it('forceNewQuery getter should get the right values', () => {
      expect(dashboardService.forceNewQuery).toEqual(false);

      dashboardService.forceQuery = true;
      expect(dashboardService.forceNewQuery).toEqual(false);

      dashboardService.forceQuery = false;
      dashboardService.hasAssetsInitialised = true;
      expect(dashboardService.forceNewQuery).toEqual(false);

      dashboardService.forceQuery = true;
      dashboardService.hasAssetsInitialised = true;
      expect(dashboardService.forceNewQuery).toEqual(true);
    });

    it('searchAssetsSelectedSortType getter/setter should get/set the right values', () => {
      expect(dashboardService.searchAssetsSelectedSortType)
        .toEqual(AppSettings.ASSETS_SORT_BY_CATEGORIES[0]);

      dashboardService.setSearchAssetsSelectedSortType();
      expect(dashboardService.searchAssetsSelectedSortType).toEqual({});

      dashboardService.setSearchAssetsSelectedSortType('AssetName');
      expect(dashboardService.searchAssetsSelectedSortType)
        .toEqual(AppSettings.ASSETS_SORT_BY_CATEGORIES[0]);
    });

    it('hasAssetsInitialised getter/setter should get and assign the right values', () => {
      expect(dashboardService.hasAssetsInitialised).toEqual(false);
      dashboardService.hasAssetsInitialised = true;
      expect(dashboardService.hasAssetsInitialised).toEqual(true);
    });

    it('tabSelected getter/setter should get and assign the right values', () => {
      expect(dashboardService.tabSelected).toEqual(0);
      dashboardService.tabSelected = 1;
      expect(dashboardService.tabSelected).toEqual(1);
    });

    it('isServiceSchedule getter/setter should get and assign the right values', () => {
      expect(dashboardService.isServiceSchedule).toBeUndefined();
      dashboardService.isServiceSchedule = false;
      expect(dashboardService.isServiceSchedule).toEqual(false);
      dashboardService.isServiceSchedule = true;
      expect(dashboardService.isServiceSchedule).toEqual(true);
    });

    it('canSearchAssets getter/setter should get and assign the right values', () => {
      expect(dashboardService.canSearchAssets).toEqual(false);
      dashboardService.canSearchAssets = true;
      expect(dashboardService.canSearchAssets).toEqual(true);
    });

    it('should get/set selectedCategory with the right values', () => {
      dashboardService.searchAssetsSelectedCategory = dashboardService.searchAssetsCategories[0];
      expect(dashboardService.searchAssetsSelectedCategory).toEqual({
        name: 'cd-asset-name-or-imo-number',
        query: 'search',
        type: 'string',
        wildcard: true,
      });
    });

    it('searchAssetsCategories should return the right values', () => {
      const searchOptions = _.filter(
        _.clone(AppSettings.ASSETS_SEARCH_CATEGORIES),
        category => category.name !== 'cd-former-asset-name',
      );
      expect(dashboardService.searchAssetsCategories).toEqual(searchOptions);
    });

    it('searchAssetsSelectedCategory getter/setter should get and assign the right values', () => {
      const searchOptions = _.filter(
        _.clone(AppSettings.ASSETS_SEARCH_CATEGORIES),
        category => category.name !== 'cd-former-asset-name',
      );
      expect(dashboardService.searchAssetsSelectedCategory).toEqual(searchOptions[0]);
      const searchAssetsSelectedCategory = { LRClassed: true };
      dashboardService.searchAssetsSelectedCategory = searchAssetsSelectedCategory;
      expect(dashboardService.searchAssetsSelectedCategory).toEqual(searchAssetsSelectedCategory);
    });

    it('assetsSearchTerm getter/setter should get and assign the right values', () => {
      expect(dashboardService.assetsSearchTerm).toEqual('');
      dashboardService.assetsSearchTerm = 'ACE';
      expect(dashboardService.assetsSearchTerm).toEqual('ACE');
    });

    it('activeServicesFlag should return correct value', () => {
      dashboardService.activeServicesFlag = 'true';
      expect(dashboardService.activeServicesFlag).toEqual('true');
    });

    it('resetFavourites should update add and remove values to empty', () => {
      const favourites = {
      // Using object insted of array since its easier to add/remove entry
      // e.g { LRV10: 'LRV10', LRV99: 'LRV99'}
        toAdd: {},
        toRemove: {},
      };
      dashboardService.resetFavourites();
      expect(dashboardService._favourites).toEqual(favourites);
    });
    describe('submitQuery', () => {
      it.async('it ignores the previous results that came in late', async (done) => {
        let count = -1;
        spyOn(mockAssetService, 'query').and.callFake(() => {
          count++;
          if (count === 0) {
            // First query is slow to return
            return new Promise((resolve) => {
              setTimeout(() => resolve([{ first: true }]), 500);
            });
          }
          return Promise.resolve([{ second: true }]);
        });

        dashboardService.queryOptions = { queryString: '*Hello World*' };
        dashboardService.queryParams = { sort: 'exampleCategory', order: 'desc' };

        // Previous assets result
        const firstQueryPromise = dashboardService.submitQuery();
        spyOn(dashboardService, 'removeFilterOption');
        // Change the query options and submit again
        dashboardService.queryOptions = { queryString: '*Hello World II*' };
        dashboardService.submitQuery();
        // removeFilterOption should be called with search key
        expect(dashboardService.removeFilterOption).toHaveBeenCalledWith('search');

        // Even after the slower first query returns, check that result is that of second query
        firstQueryPromise
          .then(() => {
            expect(dashboardService.assets).toEqual([{ second: true }]);
          })
          .then(done);

        expect(mockAssetService.query).not.toHaveBeenCalledWith({
          queryString: '*Hello World*',
        }, {
          sort: 'exampleCategory',
          order: 'desc',
        });
      });
    });
  });
});
