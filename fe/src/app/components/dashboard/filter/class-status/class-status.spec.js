/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service,
no-param-reassign, no-undef, no-underscore-dangle, jasmine/no-unsafe-spy */

import _ from 'lodash';
import AssetClassStatusMocks from
  'app/common/models/asset/asset-class-status/asset-class-status.mocks.json';
import AssetClassStatusModel from
  'app/common/models/asset/asset-class-status/asset-class-status';
import ClassStatusComponent from './class-status.component';
import ClassStatusController from './class-status.controller';
import ClassStatusModule from './class-status';
import ClassStatusTemplate from './class-status.pug';

describe('ClassStatus', () => {
  let $compile;
  let $rootScope;
  let makeController;
  let classStatuses;

  beforeEach(window.module(ClassStatusModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;

    classStatuses = _.clone(AssetClassStatusMocks);
    _.forEach(classStatuses, (rawObject, index) => {
      classStatuses[index] = Reflect.construct(AssetClassStatusModel, [rawObject]);
    });
    const mockReferenceDataService = {
      getAssetClassStatuses: () => Promise.resolve(classStatuses),
    };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('ReferenceDataService', mockReferenceDataService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      async (bindings = {}) => {
        const controller = $componentController('classStatus', { $dep: dep }, bindings);
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('has statuses property', async () => {
      const controller = await makeController();
      expect(controller.statuses).not.toBeNull();
    });

    it.async('should select \'All\' with all statuses selected by default', async () => {
      const controller = await makeController();
      expect(controller.isAll).toEqual(true);

      _.forEach(controller.classStatuses, (status) => {
        expect(status.selected).toEqual(true);
      });
    });

    it.async('if selectedClassStatusIds is given as empty', async () => {
      const controller = await makeController({
        selectedClassStatusIds: [],
      });
      expect(controller.isAll).toEqual(false);

      _.forEach(controller.classStatuses, (status) => {
        expect(status.selected).toEqual(false);
      });
    });

    it.async('should select all when selectedClassStatusIds is null', async () => {
      const controller = await makeController({
        selectedClassStatusIds: null,
      });
      expect(controller.isAll).toEqual(true);

      _.forEach(controller.classStatuses, (status) => {
        expect(status.selected).toEqual(true);
      });
    });

    it.async('should select all when \'All\' is selected', async () => {
      const controller = await makeController({ classStatuses });

      // Unselect all
      _.forEach(controller.classStatuses, (status, index) => {
        controller.classStatuses[index].selected = false;
      });

      // Select 'All'
      controller.isAll = true;

      // Select all
      _.forEach(controller.classStatuses, (status) => {
        expect(status.selected).toEqual(true);
      });
    });

    it.async('should unselect all when \'All\' is unselected', async () => {
      const controller = await makeController({ classStatuses });
      // Unselect 'All'
      controller.isAll = false;

      // Unselect all
      _.forEach(controller.classStatuses, (status) => {
        expect(status.selected).toEqual(false);
      });
    });

    describe('updateSelection', () => {
      it.async('should update class status objects with selection from selectedClassStatusIds', async () => {
        const controllerWithoutSelection =
          await makeController({ selectedClassStatusIds: [] });
        controllerWithoutSelection.updateSelection();
        expect(controllerWithoutSelection.isAll).toBe(false);

        const controllerWithSelection =
          await makeController({ selectedClassStatusIds: [1] });
        controllerWithSelection.updateSelection();
        expect(controllerWithoutSelection.isAll).toBe(false);
        expect(
          _.filter(controllerWithSelection.classStatuses, cs => cs.selected).map(cs => cs.id)
        ).toEqual([1]);

        const allIDs = classStatuses.map(cs => cs.id);
        const controllerWithAllSelected =
          await makeController({ selectedClassStatusIds: allIDs });
        controllerWithAllSelected.updateSelection();
        expect(controllerWithAllSelected.isAll).toBe(true);
        expect(
          _.filter(controllerWithAllSelected.classStatuses, cs => cs.selected).map(cs => cs.id)
        ).toEqual(allIDs);
      });
    });

    describe('updateIsAllStatus', () => {
      it.async('should unselect \'All\' when at least one status is unselected', async () => {
        const controller = await makeController({ classStatuses });

        expect(controller.isAll).toEqual(true);
        controller.classStatuses[0].selected = false;
        controller.updateIsAllStatus();
        expect(controller.isAll).toEqual(false);
      });

      it.async('should select \'All\' when all statuses is selected', async () => {
        const controller = await makeController({ classStatuses });

        // Like previous test, we continue with at least 1 is unselected
        controller.classStatuses[0].selected = false;
        controller.updateIsAllStatus();
        expect(controller.isAll).toEqual(false);

        // Then select back and test to be toEqual true
        controller.classStatuses[0].selected = true;
        controller.updateIsAllStatus();
        expect(controller.isAll).toEqual(true);
      });
    });

    it.async('setClassStatusValues() should pass the correct selected status ids', async () => {
      const controller = await makeController({ classStatuses, onChange: () => {} });
      spyOn(controller, 'onChange');

      controller.classStatuses[0].selected = false;
      controller.setClassStatusValues({ $valid: true });

      expect(controller.onChange).toHaveBeenCalledWith({ classStatusId: [2, 3, 4, 5, 6, 7, 8, 9] });
    });

    it.async('onSelectionChanged should call setClassStatusValues and updateIsAllStatus', async () => {
      const controller = await makeController({ classStatuses, onChange: () => {} });
      spyOn(controller, 'setClassStatusValues');
      spyOn(controller, 'updateIsAllStatus');

      const form = { $valid: true };
      controller.onSelectionChanged(form);
      expect(controller.setClassStatusValues).toHaveBeenCalledWith(form);
      expect(controller.updateIsAllStatus).toHaveBeenCalledTimes(1);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ClassStatusComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ClassStatusTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ClassStatusController);
    });
  });

  describe('Rendering', () => {
    let element;

    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<class-status class-statuses="statuses"><class-status/>');
      element = $compile(element)(scope);
      scope.statuses = classStatuses;
      scope.$apply();
    });

    it('renders correctly', () => {
      // The element does exist?
      expect(element.length).toEqual(1);

      // Does the intended information displayed?
      _.forEach(classStatuses, (statusObj) => {
        expect(element.text().indexOf(statusObj.name)).toBeGreaterThan(-1);
      });
    });
  });
});
