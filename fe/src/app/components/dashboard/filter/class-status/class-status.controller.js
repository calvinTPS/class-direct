import _ from 'lodash';
import Base from 'app/base';

export default class ClassStatusController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    ReferenceDataService,
  ) {
    super(arguments);
  }

  onSelectionChanged(form) {
    this.setClassStatusValues(form);
    this.updateIsAllStatus();
  }

  $onInit() {
    return this._ReferenceDataService.getAssetClassStatuses()
      .then((classStatuses) => {
        this.classStatuses = classStatuses;

        // If selected IDs undefined, set everything to true.
        if (this.selectedClassStatusIds == null) {
          this.isAll = true;
        }
      });
  }

  /**
   * Updates the selection based on given selected class status IDs.
   */
  updateSelection() {
    const selectedClassStatusIdsMap = _.keyBy(this._selectedClassStatusIds);

    _.forEach(this.classStatuses, (statusObj) => {
      // eslint-disable-next-line no-param-reassign
      statusObj.selected = selectedClassStatusIdsMap[statusObj.id] != null;
    });

    this.updateIsAllStatus();
  }

  get classStatuses() {
    return this._classStatuses;
  }

  set classStatuses(statuses) {
    this._classStatuses = statuses;
    this.updateSelection();
  }

  /**
   * @returns {Array}
   */
  get selectedClassStatusIds() {
    return this._selectedClassStatusIds;
  }

  /**
   * Externally set the selected class status IDs. This is used to
   * sync between the desktop filter and the mobile modal filter.
   *
   * @param ids {Array}
   */
  set selectedClassStatusIds(ids) {
    this._selectedClassStatusIds = ids;

    if (ids == null) {
      this.isAll = true;
    } else {
      this.updateSelection();
    }
  }

  /**
    * This get() will return this._isAll
    *
    * @returns {Boolean} this._isAll
    */
  get isAll() {
    return this._isAll;
  }

  /**
    * This set all() will set all this.statuses[x].selected the the same status
    * with the given parameter bool
    *
    * @param {Boolean} bool
    */
  set isAll(bool) {
    this._isAll = bool;
    _.forEach(this.classStatuses, (statusObj) => {
      statusObj.selected = bool; // eslint-disable-line no-param-reassign
    });
  }

  /**
    * Updates the all checkbox based on the child status states.
    */
  updateIsAllStatus() {
    this._isAll = _.every(_.map(this.classStatuses), statusObj => statusObj.selected);
  }

  /**
    * This setClassStatusValues function takes a form object and only executes
    * if $valid is true.
    *
    * @param {Object} form
    */
  setClassStatusValues(form) {
    const classStatusIds = [];
    if (form.$valid) {
      _.forEach(this.classStatuses, (statusObject) => {
        if (statusObject.selected) {
          classStatusIds.push(statusObject.id);
        }
      });

      // executes this.onChange given from the parent component with an array of
      // classStatusIds from this.statuses where selected === true
      this.onChange({
        classStatusId: this.classStatuses.length === classStatusIds.length ? null : classStatusIds,
      });
    }
  }
}
