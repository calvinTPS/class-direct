import angular from 'angular';
import classStatusComponent from './class-status.component';

export default angular.module('classStatus', [])
.component('classStatus', classStatusComponent)
.name;
