import './class-status.scss';
import controller from './class-status.controller';
import template from './class-status.pug';

export default {
  bindings: {
    classStatuses: '<',
    onChange: '&',
    selectedClassStatusIds: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
