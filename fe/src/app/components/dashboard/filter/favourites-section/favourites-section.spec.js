/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import FavouritesSectionComponent from './favourites-section.component';
import FavouritesSectionController from './favourites-section.controller';
import FavouritesSectionModule from './favourites-section';
import FavouritesSectionTemplate from './favourites-section.pug';

describe('FavouritesSection', () => {
  let $rootScope,
    $compile,
    $scope,
    makeController;

  beforeEach(window.module(FavouritesSectionModule));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController, $log) => {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $compile = _$compile_;

    makeController =
      (bindings = {}) => {
        const controller = $componentController(FavouritesSectionModule, {
          $log,
          $scope,
          AppSettings,
        }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('has a name property', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller.name).toBeDefined();
    });

    it('should select the correct option based on binding value', () => {
      const controller = makeController({ isFavourite: true });
      expect(controller.selection).toEqual(FavouritesSectionController.CHOICES.FAVOURITES);

      const anotherController = makeController({ isFavourite: false });
      expect(anotherController.selection).toEqual(FavouritesSectionController.CHOICES.ALL);
    });

    it('should call onChange when selection changes', () => {
      const controller = makeController({
        onChange: () => {},
      });

      spyOn(controller, 'onChange').and.callThrough();

      controller.selection = FavouritesSectionController.CHOICES.ALL;
      controller.onSelect();

      expect(controller.onChange).toHaveBeenCalledWith({ isFavourite: false });

      controller.onChange.calls.reset();

      controller.selection = FavouritesSectionController.CHOICES.FAVOURITES;
      controller.onSelect();

      expect(controller.onChange).toHaveBeenCalledWith({ isFavourite: true });
    });

    it('should change selection when isFavourite is updated', () => {
      const controller = makeController({
        onChange: () => {},
      });

      // Default
      controller.isFavourite = true;

      controller.isFavourite = false;
      expect(controller.selection).toEqual(FavouritesSectionController.CHOICES.ALL);

      controller.isFavourite = true;
      expect(controller.selection).toEqual(FavouritesSectionController.CHOICES.FAVOURITES);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = FavouritesSectionComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(FavouritesSectionTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(FavouritesSectionController);
    });
  });
});
