import './favourites-section.scss';
import controller from './favourites-section.controller';
import template from './favourites-section.pug';

export default {
  restrict: 'E',
  bindings: {
    isFavourite: '<',
    onChange: '&',
  },
  template,
  controllerAs: 'vm',
  controller,
};
