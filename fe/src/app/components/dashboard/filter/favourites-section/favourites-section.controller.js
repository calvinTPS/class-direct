import Base from 'app/base';

export default class FavouritesSectionController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $scope,
  ) {
    super(arguments);
    this.name = 'favourites-section';
  }

  onSelect() {
    this._isFavourite =
      this.selection === FavouritesSectionController.CHOICES.FAVOURITES;

    // Trigger the output binding
    this.onChange({ isFavourite: this._isFavourite });
  }

  $onInit() {
    this._setSelection();
  }

  set isFavourite(arg) {
    this._isFavourite = arg;
    this._setSelection();
  }

  _setSelection() {
    this.selection = this._isFavourite ?
      FavouritesSectionController.CHOICES.FAVOURITES :
      FavouritesSectionController.CHOICES.ALL;
  }
}

FavouritesSectionController.CHOICES = {
  ALL: 'all',
  FAVOURITES: 'favourites',
};

