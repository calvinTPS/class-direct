import angular from 'angular';
import favouritesSectionComponent from './favourites-section.component';

export default angular.module('favouritesSection', [
])
.component('favouritesSection', favouritesSectionComponent)
.name;
