import _ from 'lodash';
import Base from 'app/base';
import FilterController from '../filter.controller';
import filterModalTemplate from '../filter-modal.pug';
import searchModalTemplate from './search-assets-modal.pug';
import SortController from '../sort-assets/sort-assets.controller';
import sortModalTemplate from '../sort-assets/sort-assets.pug';

export default class SearchAssetsController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $rootScope,
    $scope,
    $state,
    $window,
    AppSettings,
    DashboardService,
    ServiceScheduleService,
    utils,
    ViewService,
  ) {
    super(arguments);

    this.isFetching = false;
    this.isDisabled = false;
  }

  onChangeSearchTerm() {
    this._DashboardService.assetsSearchTerm = this.searchTerm;
  }

  $onInit() {
    this.searchTerm = this._DashboardService.assetsSearchTerm ? this._DashboardService.assetsSearchTerm : '';
    this._canSearch = this._DashboardService.canSearchAssets;
    this._selectedCategory = this._DashboardService.searchAssetsSelectedCategory;
    // This is to be updated in other user stories
    this._sortByTypes = _.clone(this._AppSettings.ASSETS_SORT_BY_CATEGORIES);
    this._submissionId = -1;
    this.iconTabs = [
      {
        icon: 'list',
        label: 'cd-tabular-tab',
      },
      {
        icon: 'gantt',
        label: 'cd-gantt-tab',
      },
    ];

    this.serviceScheduleLocation =
      this._AppSettings.SERVICE_SCHEDULE.LOCATIONS.SERVICE_SCHEDULE_LISTING;

    this._$scope.$on(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, (event, bool) => {
      this.isDisabled = bool;
    });

    const intialQuery = _.clone(this._AppSettings.DEFAULT_PARAMS.FLEET.UNFILTERED_QUERY);
    intialQuery.isFavourite = undefined;
    this._initialQueryOptions = intialQuery;
  }

  get showSortBy() {
    return !!(this._$mdMedia('xs') || this.vesselListViewType === this._AppSettings.VIEW_TYPES.CARD ||
      this._DashboardService.isServiceSchedule);
  }

  get currentState() {
    return _.get(this._$state, 'current.name');
  }

  get vesselListViewType() {
    return this._ViewService.getVesselListViewType();
  }

  set vesselListViewType(arg) {
    this._ViewService.setVesselListViewType = arg;
  }

  get viewType() {
    return this._ServiceScheduleService.viewType;
  }

  set viewType(arg) {
    this._ServiceScheduleService.viewType = arg;
  }

  get canSearch() {
    const minimumKeyLength = _.get(this.selectedCategory, 'query') === 'yardNumber' ?
      1 : this._AppSettings.DEFAULT_PARAMS.MINIMUM_INPUT_CHARACTER;
    this._canSearch = !!this.searchTerm && (this.searchTerm.length >= minimumKeyLength);
    this._DashboardService.canSearchAssets = this._canSearch;
    return this._canSearch;
  }

  get categories() {
    return this._DashboardService.searchAssetsCategories;
  }

  get selectedCategory() {
    return this._selectedCategory;
  }

  set selectedCategory(categoryObj) {
    // used by updateModel()
    this._tempSelectedCategoryObj = categoryObj;
    if (this._$mdMedia('xs')) {
      this.updateModel();
    }
  }

  // Update the selected category only if the user intereact with the select option
  updateModel() {
    this._selectedCategory = this._tempSelectedCategoryObj;
    this._DashboardService.removeFilterOption(this._selectedCategory.query);
    this._DashboardService.searchAssetsSelectedCategory = this._selectedCategory;
  }

  get sortByTypes() {
    return this._sortByTypes;
  }

  get selectedSortType() {
    return this._DashboardService.searchAssetsSelectedSortType;
  }

  set selectedSortType(typeObj) {
    this._DashboardService.setSearchAssetsSelectedSortType(typeObj.sort);
    this._ViewService.focusedTableHeaderId = typeObj.sort;
    this.submit();
  }

  get isMobile() {
    // eslint-disable-next-line
    return this._$window.cordova || this._utils.isMobileDevice() || window.classDirect;
  }

  get isModifiedFilter() {
    const queryOptions = _.cloneDeep(this._DashboardService.queryOptions);
    queryOptions.isFavourite = undefined;
    return !_.isEqual(queryOptions, this._initialQueryOptions);
  }

  get isFavouriteIncluded() {
    return !!this._DashboardService.queryOptions.isFavourite;
  }

  get mobileFilterCTAIcon() {
    if (this.isModifiedFilter) {
      return 'filterApplied';
    }
    return 'filters';
  }

  search() {
    if (this.canSearch) {
      this.submit();
    }
  }

  showFilterModal(evt) {
    return this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: true,
      controller: FilterController,
      controllerAs: 'vm',
      fullscreen: true, // Only for -xs, -sm breakpoints.
      locals: {
        answer: this._$mdDialog.hide,
        cancel: this._$mdDialog.cancel,
        isModal: true,
      },
      onShowing: () => {
        // Keep a copy of filter options, so that it can be restored on cancellation.
        this._modalFilterOptions = _.cloneDeep(this._DashboardService.queryOptions);
      },
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: filterModalTemplate,
    })
      .then((filterOptions) => {
        // Submit query on submitting the filter modal
        // Both Submit and Clear Filters buttons(in filter-modal.pug)
        // will trigger $mdDialog to close,
        // which will in turn trigger this method.
        // We want to submit/query the server only when the Submit button was clicked, which will
        // pass in the filterOptions
        if (filterOptions) {
          this.submit();
        }
      })
      .catch(() => {
        // User cancelled filter modal, hence revert to previous query
        if (!_.isEqual(this._DashboardService.queryOptions, this._modalFilterOptions)) {
          this._DashboardService.queryOptions = this._modalFilterOptions;
        }
      });
  }

  showSearchModal(evt) {
    const previousSelectedCategory = this.selectedCategory;
    const previousSearchTerm = this.searchTerm;
    return this._$mdDialog.show({
      bindToController: true,
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true,
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: searchModalTemplate,
    })
      .then(() => {
        this.search();
      })
      .catch(() => {
        // Reset the options if modal is closed without submit
        this.selectedCategory = previousSelectedCategory;
        this.searchTerm = previousSearchTerm;
      });
  }

  showSortModal(evt) {
    return this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: true,
      controller: SortController,
      controllerAs: 'vm',
      fullscreen: true,
      locals: {
        cancel: this._$mdDialog.cancel,
        hide: this._$mdDialog.hide,
        sortByTypes: this._sortByTypes,
        selectedSortType: this._DashboardService.searchAssetsSelectedSortType,
      },
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: sortModalTemplate,
    })
      .then((sortTypeObj) => {
        this._DashboardService.setSearchAssetsSelectedSortType(sortTypeObj.sort);
        this.submit();
      });
  }

  /**
    * Submit a new query to update fleet view
    */
  async submit() {
    this.isFetching = true;
    this._DashboardService.queryParams = _.pick(this._DashboardService.searchAssetsSelectedSortType, ['sort', 'order']);
    await this._DashboardService.submitQuery();
    this.isFetching = false;
    this._$scope.$apply();
  }
}
