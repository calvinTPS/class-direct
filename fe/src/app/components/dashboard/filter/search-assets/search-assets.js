import angular from 'angular';
import searchAssetsComponent from './search-assets.component';

export default angular.module('searchAssets', [])
.component('searchAssets', searchAssetsComponent)
.name;
