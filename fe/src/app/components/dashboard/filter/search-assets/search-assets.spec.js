/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, angular/timeout-service,
no-underscore-dangle, no-unused-vars */
import _ from 'lodash';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import SearchAssetsComponent from './search-assets.component';
import SearchAssetsController from './search-assets.controller';
import SearchAssetsModule from './search-assets';
import SearchAssetsTemplate from './search-assets.pug';
import utils from 'app/common/utils/utils.factory';


describe('SearchAssets', () => {
  let $rootScope;
  let $compile;
  let $mdMedia;
  let $state;
  let makeController;
  let mockDashboardService;
  let mockViewService;

  const mockCategories = _.filter(
    _.clone(AppSettings.ASSETS_SEARCH_CATEGORIES),
    category => category.name !== 'cd-former-asset-name',
  );

  beforeEach(window.module(
    SearchAssetsModule,
    angularMaterial,
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockMdDialog = {
      show: () => { },
    };
    const mockServiceScheduleService = {};
    mockDashboardService = {
      filterOptionsChanged: () => { },
      getAssetsSearchQuery: () => { },
      removeFilterOption: (x) => { },
      submitQuery: () => { },
      queryOptions: { isFavourite: true },
      setSearchAssetsSelectedSortType: (x) => { },
      searchAssetsSelectedSortType: AppSettings.ASSETS_SORT_BY_CATEGORIES[0],
      searchAssetsCategories: mockCategories,
      searchAssetsSelectedCategory: 'FOO',
    };
    const mockState = {
      reload: () => { },
      current: {},
    };

    const mockErrorService = {
      showErrorDialog: () => { },
    };

    mockViewService = {
      getVesselListViewType: () => true,
      focusedTableHeaderId: 'Foo',
    };

    window.module(($provide) => {
      $provide.value('ErrorService', mockErrorService);
      $provide.value('$mdDialog', mockMdDialog);
      $provide.value('AppSettings', AppSettings);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('ServiceScheduleService', mockServiceScheduleService);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
      $provide.value('$state', mockState);
      $provide.value('utils', utils());
      $provide.value('ViewService', mockViewService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, _$state_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;
    $state = _$state_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('searchAssets', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('should validate currentState', () => {
      const controller = makeController();
      $state.current.name = AppSettings.STATES.VESSEL_LIST;
      expect(controller.currentState).toEqual(AppSettings.STATES.VESSEL_LIST);

      $state.current.name = AppSettings.STATES.SERVICE_SCHEDULE;
      expect(controller.currentState).toEqual(AppSettings.STATES.SERVICE_SCHEDULE);
    });

    it('should initialize sorting types', () => {
      const controller = makeController();
      expect(controller._sortByTypes).toEqual(AppSettings.ASSETS_SORT_BY_CATEGORIES);
    });

    it('should get/set searchTerm with the right values', () => {
      const controller = makeController();
      controller.searchTerm = '*hello world*';
      expect(controller.searchTerm).toEqual('*hello world*');
    });

    it('should get/set selectedSortType with the right values', () => {
      const controller = makeController();
      controller.selectedSortType = controller.sortByTypes[0];
      expect(controller.selectedSortType).toEqual({
        name: 'cd-asset-name',
        sort: 'AssetName',
        order: 'ASC',
      });
    });

    it('should get sortByTypes with the right values', () => {
      const controller = makeController();
      expect(controller.sortByTypes).toEqual(AppSettings.ASSETS_SORT_BY_CATEGORIES);
    });

    it('should get/set selectedSortByTypes with the right values', () => {
      const controller = makeController();
      controller.selectedSortType = controller.sortByTypes[0];
      expect(controller.selectedSortType).toEqual({ name: 'cd-asset-name', sort: 'AssetName', order: 'ASC' });
    });

    it('should get/set selectedCategory with the right values', () => {
      const controller = makeController();
      controller._selectedCategory = mockDashboardService.searchAssetsCategories[0];
      expect(controller.selectedCategory)
        .toEqual(mockDashboardService.searchAssetsCategories[0]);
      controller.selectedCategory = { foo: 'BAR' };
      expect(controller._tempSelectedCategoryObj).toEqual({ foo: 'BAR' });
    });

    it('updateModel() should update the searchAssetSelectedCategory ', () => {
      const controller = makeController();
      const categoryObj = { foo: 'FOO', query: 'XXX' };
      controller._tempSelectedCategoryObj = categoryObj;
      spyOn(mockDashboardService, 'removeFilterOption');
      controller.updateModel();
      expect(controller.selectedCategory).toEqual(categoryObj);
      expect(mockDashboardService.removeFilterOption).toHaveBeenCalledWith(categoryObj.query);
      expect(mockDashboardService.searchAssetsSelectedCategory).toEqual(categoryObj);
    });


    describe('canSearch', () => {
      it('canSearch should return true when searchTerm has at least 3 charactars', () => {
        const controller = makeController();
        expect(controller.canSearch).toBeFalsy();
        controller.searchTerm = '';
        expect(controller.canSearch).toBeFalsy();
        controller.searchTerm = 'ab';
        expect(controller.canSearch).toBeFalsy();
        controller.searchTerm = 'abc';
        expect(controller.canSearch).toBeTruthy();
      });

      it('canSearch should return true when searching by yard number and searchTerm has at least 1 charactar', () => {
        const controller = makeController();
        expect(controller.canSearch).toBeFalsy();
        controller.selectedCategory = {
          query: 'yardNumber',
        };
        controller.searchTerm = 'a';
        controller.updateModel();
        expect(controller.canSearch).toBeTruthy();
      });
    });

    it.async('submit() should update queryOptions and call AssetService.query', async () => {
      const controller = makeController();
      spyOn(mockDashboardService, 'submitQuery');
      await controller.submit();
      expect(mockDashboardService.submitQuery).toHaveBeenCalled();
      expect(controller.isFetching).toEqual(false);
    });

    describe('showFilterModal', () => {
      it.async('modal filter should copy current query on show, and restore & submit query if cancelled', async () => {
        const controller = makeController();
        mockDashboardService.queryOptions = { initial: 'query' };

        spyOn(controller._$mdDialog, 'show').and.callFake((dialogConfig) => {
          dialogConfig.onShowing();

          // Change query in dashboard service
          mockDashboardService.queryOptions = { foo: 'bar' };

          return Promise.reject();
        });

        await controller.showFilterModal();

        expect(mockDashboardService.queryOptions).toEqual({ initial: 'query' });
      });

      it.async('On closing filter modal should call submit() when argument/filter options are passed', async () => {
        const controller = makeController();
        const dialogSpy = spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve({ something: 'something' }));

        spyOn(controller, 'submit');

        await controller.showFilterModal();

        expect(dialogSpy).toHaveBeenCalled();
        expect(controller.submit).toHaveBeenCalled();
      });

      it.async('On closing filter modal should NOT call submit() when no argument is passed', async () => {
        const controller = makeController();
        const dialogSpy = spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve()); // Blank resolve
        spyOn(controller, 'submit');

        await controller.showFilterModal();
        expect(controller.submit).not.toHaveBeenCalled();
      });
    });

    describe('showSearchModal', () => {
      it.async('On closing search modal should call submit()', async () => {
        const controller = makeController();

        const dialogSpy = spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve());
        spyOn(controller, 'search');

        await controller.showSearchModal();
        expect(dialogSpy).toHaveBeenCalled();
        expect(controller.search).toHaveBeenCalled();
      });
    });

    describe('showSortModal', () => {
      it.async('On closing sort modal should call submit()', async () => {
        const controller = makeController();

        const dialogSpy = spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve({ sort: 'FOO' }));
        spyOn(controller, 'submit');

        await controller.showSortModal();
        expect(dialogSpy).toHaveBeenCalled();
        expect(controller.submit).toHaveBeenCalled();
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SearchAssetsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SearchAssetsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SearchAssetsController);
    });
  });

  describe('Rendering', () => {
    let controller;
    let element;

    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<search-assets foo="bar"><search-assets/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('SearchAssets');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
