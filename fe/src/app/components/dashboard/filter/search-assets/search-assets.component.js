import './search-assets.scss';
import controller from './search-assets.controller';
import template from './search-assets.pug';

export default {
  bindings: {
    assets: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
