import * as _ from 'lodash';
import Base from 'app/base';
import dialogTemplate from './service-type-modal.pug';

export default class ServiceTypeController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    ReferenceDataService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.refServiceTypes = []; // This scope is used to mantain state on canceling the modal

    if (!this.isSingleAsset) {
      this._ReferenceDataService.getServiceTypes()
        .then((serviceTypes) => {
          this.serviceTypes = this._groupServices(serviceTypes);
        });
    }
  }

  showAdvanced(ev) {
    this._$mdDialog.show({
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true,
      multiple: true,
      onShowing: () => {
        this.refServiceTypes = _.cloneDeep(this.serviceTypes);
      },
      parent: angular.element(this._$document[0].body),
      skipHide: true,
      targetEvent: ev,
      template: dialogTemplate,
    });
  }

  cancel() {
    this._$mdDialog.cancel();
  }

  submit() {
    this.serviceTypes = _.cloneDeep(this.refServiceTypes);
    this.onChange({ serviceCatalogueIds: this._buildPayload(this.serviceTypes) });
    this._$mdDialog.cancel();
  }

  /**
   * Grouped the services according to their respective product's name
   *
   * Pairs the properties array and value array using _.zipObject
   */
  _groupServices(serviceScheduleServices) {
    // - An array of services is passed in
    // I will sort all services
    // - Special note, the data comes from MAST
    const sortCollection = c => _.sortBy(c, ['id']);
    const pairsCollection = c => _.toPairs(_.groupBy(c, 'productCatalogue'));
    const mapCollection = c => _.map(c, r => _.zipObject(['productName', 'details'], r));

    return _.flow(
      sortCollection,
      pairsCollection,
      mapCollection,
    )(serviceScheduleServices);
  }

  _buildPayload(payload) {
    const filteredResults = _.filter(payload, 'details');
    const ids = [];
    _.forEach(filteredResults, (selectedResult) => {
      _.forEach(_.filter(selectedResult.details, 'isSelected'), (selectedDetail) => {
        ids.push(selectedDetail.serviceCatalogueH.id);
      });
    });
    return ids;
  }

  /**
   * Click clear all to clear all selected
   */
  clearAllSelection(model) {
    _.forEach(model, (v) => {
      if (_.has(v, 'details') && v.details.length) {
        v.isCategorySelected = false; // eslint-disable-line no-param-reassign
        this.clearAllSelection(v.details);
      }

      if (_.hasIn(v, 'isSelected')) {
        _.assign(v, { isSelected: false });
      }
    });
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
