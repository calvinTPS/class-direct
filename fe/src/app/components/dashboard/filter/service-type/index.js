import angular from 'angular';
import ServiceTypeComponent from './service-type.component';
import uiRouter from 'angular-ui-router';

export default angular.module('serviceType', [
  uiRouter,
])
.component('serviceType', ServiceTypeComponent)
.name;
