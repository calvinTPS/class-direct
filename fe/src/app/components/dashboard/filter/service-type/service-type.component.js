import './service-type.scss';
import controller from './service-type.controller';
import template from './service-type.pug';

export default {
  bindings: {
    onChange: '&',
    isSingleAsset: '<',
    serviceTypes: '=',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
