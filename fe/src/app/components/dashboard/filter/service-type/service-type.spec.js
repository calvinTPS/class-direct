/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
angular/no-service-method, no-underscore-dangle
*/
import _ from 'lodash';
import angularMaterial from 'angular-material';
import mockData from './service-type.mocks.json';
import ServiceTypeComponent from './service-type.component';
import ServiceTypeController from './service-type.controller';
import ServiceTypeModule from './';
import ServiceTypeTemplate from './service-type.pug';

describe('ServiceType', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    makeController;

  beforeEach(window.module(
    ServiceTypeModule,
    angularMaterial,
  ));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockReferenceDataService = {
      getServiceTypes: () => Promise.resolve(),
    };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('ReferenceDataService', mockReferenceDataService);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('serviceType', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('should test cancel() behaviour', () => {
      const controller = makeController();
      spyOn($mdDialog, 'cancel');
      controller.cancel();
      expect($mdDialog.cancel).toHaveBeenCalled();
    });

    it.async('showAdvanced() should popup a fullscreen mdDialog box', async() => {
      const controller = await makeController();
      const fakePromise = () => 'Promise'; // Cheating, I know, ma bad.
      spyOn($mdDialog, 'show').and.returnValue(new Promise(fakePromise));

      controller.showAdvanced();

      expect($mdDialog.show).toHaveBeenCalled();
    });

    it('submit() should behave properly', () => {
      const controller = makeController({ onChange: () => {} });
      controller.refServiceTypes = mockData;

      spyOn(controller, 'onChange').and.callThrough();
      spyOn($mdDialog, 'cancel');

      controller.submit();
      expect(controller.serviceTypes).toEqual(controller.refServiceTypes);
      expect(controller.onChange).toHaveBeenCalled();
      expect($mdDialog.cancel).toHaveBeenCalled();
    });

    it('_buildPayload() should return correct ids', () => {
      const controller = makeController();

      expect(controller._buildPayload(mockData)).toEqual([1]);
    });

    it('clearAllSelection() should clear all the models', () => {
      const controller = makeController();

      controller.clearAllSelection(mockData);
      expect(mockData[0].isCategorySelected).toBe(false);
      expect(mockData[0].details[0].isSelected).toBe(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ServiceTypeComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ServiceTypeTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ServiceTypeController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<service-type foo="bar"><service-type/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ServiceType');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
