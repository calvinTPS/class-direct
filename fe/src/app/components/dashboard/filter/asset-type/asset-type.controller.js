import _ from 'lodash';
import Base from 'app/base';
import dialogTemplate from './asset-type-modal.pug';

export default class AssetTypeController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    AppSettings,
    ReferenceDataService,
  ) {
    super(arguments);
  }

  $onInit() {
    const categoryLevel = this._AppSettings.ASSET_TYPES.CATEGORY_LEVEL_INDICATION;
    const displayedLevel = this._AppSettings.ASSET_TYPES.DISPLAYED_LEVEL_INDICATION;
    return this._ReferenceDataService.getAssetTypesAtLevel(displayedLevel)
      .then((types) => {
        // Show unique codes
        const uniqTypes = _.uniqBy(types, type => type.code);
        // Leave out types without leaf codes.
        this._types = _.filter(uniqTypes, type => this._getLeafCodes(type).length);

        // Group by category level indicator
        const levelDiff = displayedLevel - categoryLevel;
        this._groupedAssetTypes = _.groupBy(this._types, (type) => {
          // Find grandparent
          for (let i = 0; i < levelDiff; i++) {
            // eslint-disable-next-line no-param-reassign
            type = type.parent;
          }
          return type.name;
        });
      })
      .then(::this.updateSelection);
  }

  get groupedAssetTypes() {
    return this._groupedAssetTypes;
  }

  get selectedAssetTypeCodes() {
    return this._selectedAssetTypeCodes;
  }

  set selectedAssetTypeCodes(codes) {
    this._selectedAssetTypeCodes = codes || [];
    this.updateSelection();
  }

  get types() {
    return this._types;
  }

  /**
   * Restores the selection to those kept in selectedTypeCodes, in case of
   * modal filter cancellation, or non-empty selected codes on init.
   *
   * Maps asset type leaf codes to selection in displayed asset types.
   */
  updateSelection() {
    if (_.isEmpty(this.types)) {
      return;
    }

    // Map leaf codes back to higher level asset types.
    // Conveniently, codes at a certain level have length of level N.
    // e.g. level 3 asset type has code "Z11", "A36"
    const selectedAssetTypeCodesMap =
      _.groupBy(
        this._selectedAssetTypeCodes,
        code => code.substring(0, this._AppSettings.ASSET_TYPES.DISPLAYED_LEVEL_INDICATION)
      );

    _.forEach(this.types, (assetType) => {
      // eslint-disable-next-line no-param-reassign
      assetType.selected = selectedAssetTypeCodesMap[assetType.code] != null;
    });
  }

  /**
    * Modal dialog behaviours
    */
  showAdvanced(ev) {
    this._$mdDialog.show({
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true,
      multiple: true,
      parent: angular.element(this._$document[0].body),
      skipHide: true,
      targetEvent: ev,
      template: dialogTemplate,
    });
  }

  /**
    * Cancel the dialog box
    */
  cancel() {
    this._$mdDialog.cancel();
    this.updateSelection();
  }

  /**
    * Submit a new query to update fleet view
    */
  submit() {
    const selectedAssetTypes = _.filter(this.types, type => type.selected);
    this._selectedAssetTypeCodes = _.flatten(
      _.map(selectedAssetTypes, ::this._getLeafCodes)
    );

    this.onChange({ assetTypeCodes: this._selectedAssetTypeCodes });
    this._$mdDialog.cancel();
  }

  /**
   * Retrieves the leaf asset type codes.
   *
   * @param assetType
   * @returns {Array<string>} Array of leaf codes.
   * @private
   */
  _getLeafCodes(assetType) {
    if (assetType.levelIndication === this._AppSettings.ASSET_TYPES.LEAF_LEVEL_INDICATION) {
      return [assetType.code];
    }
    return _.flatten(
      _.map(assetType.children, child => this._getLeafCodes(child))
    );
  }

  /**
  * Click clear all to clear all selected
  */
  clearAllSelection() {
    _.forEach(this.types, (typeObj) => {
      typeObj.selected = false; // eslint-disable-line no-param-reassign
    });
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
