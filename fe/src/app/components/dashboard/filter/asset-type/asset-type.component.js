import './asset-type.scss';
import controller from './asset-type.controller';
import template from './asset-type.pug';

export default {
  bindings: {
    selectedAssetTypeCodes: '<',
    onChange: '&',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
