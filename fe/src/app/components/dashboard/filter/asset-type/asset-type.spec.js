/* global afterEach afterAll beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, angular/no-service-method, no-underscore-dangle */

import * as _ from 'lodash';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import AssetTypeComponent from './asset-type.component';
import AssetTypeController from './asset-type.controller';
import AssetTypeMocks from 'app/common/models/asset/asset-type/asset-type.mocks.json';
import AssetTypeModel from 'app/common/models/asset/asset-type/asset-type';
import AssetTypeModule from './asset-type';
import AssetTypeTemplate from './asset-type.pug';
import ReferenceDataService from 'app/services/reference-data.service';

describe('AssetTypeFilter', () => {
  let $compile;
  let $rootScope;
  let assetTypes;
  let makeController;

  beforeEach(window.module(
    angularMaterial,
    AssetTypeModule,
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockMdDialog = function mdDialog() {
      return {
        alert: (arg) => {},
        build: (arg) => {},
        cancel: function cancel(reason, options) {},
        confirm: (arg) => {},
        destroy: function destroyInterimElement(opts) {},
        hide: function hide(reason, options) {},
        prompt: (arg) => {},
        show: function showInterimElement(opts) {},
      };
    };
    const mockReferenceDataService = {
      getAssetTypesAtLevel: () => {
        const assetTypesCopy = _.clone(assetTypes);

        ReferenceDataService.buildAssetTypeTree(assetTypesCopy);
        return Promise.resolve(
          _.filter(assetTypesCopy, a => a.levelIndication === 3)
        );
      },
    };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.service('$mdDialog', mockMdDialog);
      $provide.value('ReferenceDataService', mockReferenceDataService);
      $provide.value('AppSettings', AppSettings);
    });

    assetTypes = _.clone(AssetTypeMocks);
    const assetTypeMapById = _.keyBy(assetTypes, 'id');
    _.forEach(assetTypes, (rawAssetTypeObj, index) => {
      const assetTypeModel = Reflect.construct(AssetTypeModel, [rawAssetTypeObj]);

      if (assetTypeModel.parentId) {
        assetTypeModel.parent = assetTypeMapById[assetTypeModel.parentId];
      }
      assetTypes[index] = assetTypeModel;
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      async (bindings = {}) => {
        const controller = $componentController('assetType', { $dep: dep }, bindings);
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('initialize with lists of asset types grouped by level 1 type indicator', async () => {
      const controller = await makeController();
      expect(controller.groupedAssetTypes['Cargo Carrying'].length).toEqual(1);
    });

    it.async('initialize with selectedAssetTypeCodes should call updateSelection and update selection', async () => {
      const controller = await makeController({
        selectedAssetTypeCodes: ['A21B2BO'],
      });

      expect(controller.selectedAssetTypeCodes).toEqual(['A21B2BO']);
      expect(
        _.filter(controller.types, type => type.selected).map(type => type.id)
      ).toEqual([35]);
    });

    it.async('clearAllSelection() should clear all selected', async () => {
      const controller = await makeController();
      controller.types[0].selected = true;

      controller.clearAllSelection();

      _.forEach(controller.types, (typeObj) => {
        expect(typeObj.selected).toEqual(false);
      });
    });

    it.async('showAdvanced() should popup a fullscreen mdDialog box', async () => {
      const controller = await makeController();
      spyOn(controller._$mdDialog, 'show');

      controller.showAdvanced();

      expect(controller._$mdDialog.show).toHaveBeenCalled();
    });

    it.async('should call updateSelection() when cancelling modal', async () => {
      const controller = await makeController({ onChange: () => {} });
      spyOn(controller._$mdDialog, 'cancel');
      spyOn(controller._$mdDialog, 'show');
      spyOn(controller, 'onChange');
      spyOn(controller, 'updateSelection');

      controller.showAdvanced();
      expect(controller._$mdDialog.show).toHaveBeenCalled();

      controller.types[0].selected = true;

      controller.submit();

      expect(controller.onChange).toHaveBeenCalledWith({
        assetTypeCodes: ['A21A2BC', 'A21A2BV', 'A21A2BG', 'A21B2BO'],
      });
      expect(controller._$mdDialog.cancel).toHaveBeenCalled();
      expect(controller.selectedAssetTypeCodes.length).toEqual(4);

      controller.showAdvanced();

      expect(controller._$mdDialog.show).toHaveBeenCalled();

      controller.types[0].selected = false;

      controller.cancel();

      expect(controller._$mdDialog.cancel).toHaveBeenCalled();
      expect(controller.updateSelection).toHaveBeenCalled();
      expect(controller.selectedAssetTypeCodes.length).toEqual(4);
    });

    it.async('cancel() should close mdDialog box', async () => {
      const controller = await makeController();
      spyOn(controller._$mdDialog, 'cancel');

      controller.cancel();

      expect(controller._$mdDialog.cancel).toHaveBeenCalled();
    });

    it.async('cancel() should clear all selection and close dialog box', async () => {
      const controller = await makeController();
      spyOn(controller._$mdDialog, 'cancel');
      spyOn(controller, 'updateSelection');

      controller.types[0].selected = true;

      controller.cancel();

      expect(controller._$mdDialog.cancel).toHaveBeenCalled();
      expect(controller.updateSelection).toHaveBeenCalled();
    });

    it.async('submit() should call onChange with array of selected asset type codes (leaf codes)', async () => {
      const controller = await makeController({ onChange: () => {} });
      spyOn(controller, 'onChange');
      spyOn(controller._$mdDialog, 'cancel');

      controller.types[0].selected = true;
      controller.submit();

      // Test call onChange function
      expect(controller.onChange).toHaveBeenCalledWith({ assetTypeCodes: ['A21A2BC', 'A21A2BV', 'A21A2BG', 'A21B2BO'] });

      // Test close dialog box
      expect(controller._$mdDialog.cancel).toHaveBeenCalled();
    });

    it.async('calls updateSelection when selectedAssetTypeCodes are set', async () => {
      const controller = await makeController();
      spyOn(controller, 'updateSelection');
      controller.selectedAssetTypeCodes = ['A21B2BO'];

      expect(controller.updateSelection).toHaveBeenCalled();
    });

    describe('_getLeafCodes', () => {
      it.async('retrieves level 5 codes', async () => {
        const controller = await makeController();
        const leafCodes = controller._getLeafCodes(
          {
            id: 1,
            children: [
              {
                id: 2,
                children: [
                  { id: 3, levelIndication: 5, code: '3' },
                  { id: 4, levelIndication: 5, code: '4' },
                ],
              },
              {
                id: 6,
                children: [
                  { id: 7, levelIndication: 5, code: '7' },
                  { id: 8, levelIndication: 5, code: '8' },
                ],
              },
            ],
          },
        );
        expect(leafCodes).toEqual(['3', '4', '7', '8']);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetTypeComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetTypeTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetTypeController);
    });
  });

  describe('Rendering', () => {
    let element;

    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<asset-type types="assetTypes"><asset-type/>');
      element = $compile(element)(scope);
      scope.types = assetTypes;
      scope.$apply();
    });

    it('renders correctly', () => {
      // The element does exist?
      expect(element.length).toEqual(1);
    });
  });
});
