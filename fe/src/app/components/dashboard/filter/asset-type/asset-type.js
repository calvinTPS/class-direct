import angular from 'angular';
import assetTypeComponent from './asset-type.component';

export default angular.module('assetType', [])
.component('assetType', assetTypeComponent)
.name;
