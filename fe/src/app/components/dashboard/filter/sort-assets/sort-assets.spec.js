/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import SortAssetsComponent from './sort-assets.component';
import SortAssetsController from './sort-assets.controller';
import SortAssetsModule from './';
import SortAssetsTemplate from './sort-assets.pug';

describe('SortAssets', () => {
  let $rootScope;
  let $compile;
  let makeController;

  beforeEach(window.module(
    SortAssetsModule,
    angularMaterial,
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('sortAssets', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SortAssetsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SortAssetsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SortAssetsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<sort-assets foo="bar"><sort-assets/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('SortAssets');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
