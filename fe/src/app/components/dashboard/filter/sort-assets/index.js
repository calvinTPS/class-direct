import angular from 'angular';
import SortAssetsComponent from './sort-assets.component';
import uiRouter from 'angular-ui-router';

export default angular.module('sortAssets', [
  uiRouter,
])
.component('sortAssets', SortAssetsComponent)
.name;
