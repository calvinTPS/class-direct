import './sort-assets.scss';
import controller from './sort-assets.controller';
import template from './sort-assets.pug';

export default {
  bindings: {
    sortByTypes: '<',
    selectedSortType: '=',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
