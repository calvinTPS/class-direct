import _ from 'lodash';
import Base from 'app/base';

export default class FilterController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $mdDialog,
    $scope,
    $state,
    AppSettings,
    DashboardService,
    ViewService,
  ) {
    super(arguments);

    // Closed by default
    this.isOpen = false;

    // Debounce the update of filter options
    this.updateFilterOptions = _.debounce(
        this.updateFilterOptions.bind(this),
        this._AppSettings.DEFAULT_PARAMS.DEBOUNCE,
      );
  }

  get currentState() {
    return _.get(this._$state, 'current.name');
  }

  get assets() {
    return this._DashboardService.assets;
  }

  get filterOptions() {
    return this._DashboardService.queryOptions;
  }

  get visibleCount() {
    const pagination = _.get(this._DashboardService, 'assets.pagination');
    return pagination.numberOfElements + ((pagination.number - 1) * pagination.size);
  }

  get totalCount() {
    return _.get(this._DashboardService, 'assets.pagination.totalElements');
  }

  async updateFilterOptions(changedOption) {
    if (changedOption) {
      _.assign(this.filterOptions, changedOption);
    }
  }

  clearFilterOptions() {
    this._$mdDialog.hide();
    this._DashboardService.queryOptions =
      _.clone(this._AppSettings.DEFAULT_PARAMS.FLEET.QUERY);
    this._DashboardService.assetsSearchTerm = '';
    const defaultSortColumn = this._AppSettings.TABLE_HEADER_SORT_AND_IDS.ASSET_NAME;
    this._ViewService.focusedTableHeaderId = defaultSortColumn;
    this._DashboardService.setSearchAssetsSelectedSortType(defaultSortColumn);
    this._DashboardService.queryParams = { sort: defaultSortColumn, order: 'ASC' };
    this._DashboardService.hasAssetsInitialised = false;
    // DashboardService does not re-query with same payload. Force it to.
    this._$state.reload();
  }

  resetServices(model) {
    _.forEach(model, (v) => {
      if (_.has(v, 'details') && v.details.length) {
        v.isCategorySelected = true; // eslint-disable-line no-param-reassign
        this.resetServices(v.details);
      }
      if (_.has(v, '_isSelected')) {
        v.isSelected = true; // eslint-disable-line no-param-reassign
      }
    });
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
