import angular from 'angular';
import AssetType from './asset-type/asset-type';
import ClassStatus from './class-status/class-status';
import CodicilType from './codicil-type';
import FavouritesSection from './favourites-section/favourites-section';
import FilterComponent from './filter.component';
import FlagFilter from './flag-filter/flag-filter';
import GrossTonnage from './gross-tonnage/gross-tonnage';
import SearchAssets from './search-assets/search-assets';
import ServiceType from './service-type';

export default angular.module('filter', [
  AssetType,
  ClassStatus,
  CodicilType,
  FavouritesSection,
  FlagFilter,
  GrossTonnage,
  SearchAssets,
  ServiceType,
])
.component('filter', FilterComponent)
.name;
