import _ from 'lodash';
import Base from 'app/base';
import dialogTemplate from './flag-filter-modal.pug';

export default class FlagFilterController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    FlagService,
  ) {
    super(arguments);
  }

  $onInit() {
    this._showMoreFlags = false;

    this._flags = [];
    this._selectedFlags = [];
    this._modalSelectedFlags = [];

    return this._populateFlagsFromService();
  }

  /**
   * Retrieves all flags from the service.
   *
   * @private
   */
  async _populateFlagsFromService() {
    this._flags = await this._FlagService.getUserFlags();

    // Create an index for easy retrieval
    this._flagCodesMap = _.keyBy(this._flags, 'flagCode');

    this._updateSelectedFlagsFromCodes(this.selectedFlagCodes);
  }

  /**
   * @returns {Array}
   */
  get selectedFlagCodes() {
    return this._selectedFlagCodes;
  }

  /**
   * When setting selected flag ids, we need to update selected flags.
   *
   * @param selectedFlagCodes {Array}
   */
  set selectedFlagCodes(selectedFlagCodes) {
    this._selectedFlagCodes = selectedFlagCodes;
    this._updateSelectedFlagsFromCodes(selectedFlagCodes);
  }

  /**
   * Updates selected flags from the given selected flag ids.
   *
   * @param selectedFlagCodes
   * @private
   */
  _updateSelectedFlagsFromCodes(selectedFlagCodes) {
    if (_.isEmpty(this._flagCodesMap)) {
      // Cannot process yet, wait for flags to load, then processing will happen.
      return;
    }

    this._selectedFlags = _.map(selectedFlagCodes, code => this._flagCodesMap[code]);
    this.loadFlagOptions();
  }

  /**
   * All flags retrieved from the backend for this user.
   *
   * @returns {Array}
   */
  get flags() {
    return this._flags;
  }

  /**
   * Retrieve the flags selected.
   *
   * @returns {Array}
   */
  get selectedFlags() {
    return this._selectedFlags;
  }

  /**
   * Set the selected flags.
   *
   * @param selectedFlags {Array}
   */
  set selectedFlags(selectedFlags) {
    this._selectedFlags = selectedFlags;
  }

  /**
   * Retrieve the selected flags in the modal.
   * This is to separate the modal's model from the currently selected flags.
   *
   * @returns {Array}
   */
  get modalSelectedFlags() {
    return this._modalSelectedFlags;
  }

  /**
   * Set the modal selected flags.
   *
   * @param modalSelectedFlags {Array}
   */
  set modalSelectedFlags(modalSelectedFlags) {
    this._modalSelectedFlags = modalSelectedFlags;
  }

  get showMoreFlags() {
    return this._showMoreFlags;
  }

  set showMoreFlags(boolean) {
    this._showMoreFlags = boolean;
  }

  get unselectedFlags() {
    return this._unselectedFlags;
  }

  /**
   * This function is used to remove a single tag based on the passed id.
   *
   * @param {int} flagCode The ID of the flag to be removed.
   */
  removeTag(flagCode) {
    _.pullAllBy(this._selectedFlags, [{ flagCode }], 'flagCode');

    // Inform filter via flag codes
    this.onChange({ flagStateCodes: this._getSelectedFlagCodes() });
  }

  /**
   * This function is used to re-order 'flagStateId' model value
   * to be an array od string IDs.
   */
  _getSelectedFlagCodes() {
    return _.map(this._selectedFlags, 'flagCode');
  }

  toggleShowMoreFlags() {
    this.showMoreFlags = !this.showMoreFlags;
  }

  /**
   * Modal dialog behaviours
   */
  showAdvanced(ev) {
    this._$mdDialog.show({
      controller: () => this,
      controllerAs: 'vm',
      focusOnOpen: false, // allow input box to take focus
      fullscreen: true,
      multiple: true,
      onShowing: () => {
        this._modalSelectedFlags = _.cloneDeep(this._selectedFlags);
        this.loadFlagOptions();
      },
      parent: angular.element(this._$document[0].body),
      skipHide: true,
      targetEvent: ev,
      template: dialogTemplate,
    });
  }

  /**
   * Provides a list of unselected flag options.
   */
  loadFlagOptions() {
    let unselectedFlags = [];

    if (this._modalSelectedFlags.length) {
      unselectedFlags = _.differenceBy(this._flags, this._modalSelectedFlags, 'flagCode');
    } else {
      unselectedFlags = this._flags;
    }

    this._unselectedFlags = unselectedFlags;
  }

  /**
   * Called by the modal when a flag is de-selected. Refreshes the options.
   */
  refresh() {
    this.loadFlagOptions();
  }

  /**
   * Called when modal Cancel button is clicked.
   */
  cancel() {
    this._modalSelectedFlags = [];
    this._$mdDialog.cancel();
  }

  /**
   * Called when modal Submit button is clicked.
   */
  submit() {
    this._showMoreFlags = false; // showing 'show more' button again

    this._selectedFlags = this._modalSelectedFlags;

    // Inform filter via flag codes
    this.onChange({ flagStateCodes: this._getSelectedFlagCodes() });

    this._modalSelectedFlags = [];
    this._$mdDialog.cancel();
  }

  /**
  * Click clear all to clear all selected
  */
  clearAllSelection() {
    this._modalSelectedFlags = [];
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
