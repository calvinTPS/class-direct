/* global, describe, beforeEach, beforeAll, afterEach, afterAll, it, expect */
/* eslint-disable one-var, no-unused-vars, one-var-declaration-per-line, sort-vars,
angular/window-service, no-undef */
import _ from 'lodash';
import angularMaterial from 'angular-material';
import AppConfig from 'app/config/project-variables.js';
import FlagFilterComponent from './flag-filter.component';
import FlagFilterController from './flag-filter.controller';
import FlagFilterModule from './flag-filter';
import FlagFilterTemplate from './flag-filter.pug';
import FlagMocks from 'app/common/models/flag/flag.mocks.json';
import FlagService from 'app/services/flag.service.js';

describe('FlagFilter', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    $q,
    flagServiceGetAllResult,
    makeController,
    scope;

  const mockTranslateFilter = value => value;
  const flagService = new FlagService({}, AppConfig);

  beforeEach(window.module(
    FlagFilterModule,
    angularMaterial,
    { translateFilter: mockTranslateFilter },
    { FlagService: flagService },
  ));

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, _$q_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;
    $q = _$q_;

    flagServiceGetAllResult = Promise.resolve(_.cloneDeep(FlagMocks));
    spyOn(flagService, 'getUserFlags').and.callFake(() => flagServiceGetAllResult);

    makeController =
      async (bindings = {}) => {
        const controller =
          $componentController('flagFilter', { FlagService: flagService }, bindings);
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('calls FlagService.getUserFlags to retrieve a list of flags', async () => {
      const controller = await makeController();
      expect(flagService.getUserFlags).toHaveBeenCalledTimes(1);
      expect(controller.flags).toEqual(FlagMocks);
    });

    describe('SET selectedFlagIds', () => {
      describe('flags have loaded', () => {
        it.async('setting selectedFlagIds should update selectedFlags', async () => {
          const controller = await makeController();

          spyOn(controller, 'loadFlagOptions').and.callThrough();

          expect(controller.selectedFlags.length).toBe(0);

          controller.selectedFlagCodes = ['GBI'];
          expect(controller.selectedFlags).toEqual([FlagMocks[0]]);

          // selected flags should have same order as given selected flag codes
          controller.selectedFlagCodes = ['IDA', 'SNP', 'GBI'];
          expect(_.map(controller.selectedFlags, 'flagCode')).toEqual(['IDA', 'SNP', 'GBI']);

          expect(controller.loadFlagOptions).toHaveBeenCalled();
        });
      });

      describe('flags have not loaded', () => {
        beforeEach(() => {
          flagServiceGetAllResult = [];
        });

        it.async('setting selectedFlagIds should not update selectedFlags', async () => {
          const controller = await makeController();
          spyOn(controller, 'loadFlagOptions').and.callThrough();

          // selected flags should have same order as given selected flag codes
          controller.selectedFlagCodes = ['IDA', 'SNP', 'GBI'];
          expect(controller.selectedFlags).toEqual([]);
          expect(controller.loadFlagOptions).not.toHaveBeenCalled();
        });
      });
    });

    it.async('should test removeTag() behaviour', async () => {
      const controller = await makeController({
        onChange: () => {},
      });

      spyOn(controller, 'onChange').and.callThrough();

      // Select all flags
      controller.selectedFlags = controller.flags;

      controller.removeTag('GBI');

      expect(controller.onChange).toHaveBeenCalledWith(
        { flagStateCodes: ['SNP', 'FIN', 'IDA', 'MAL', 'NTH', 'USA', 'DEN'] });
      expect(controller.flags[0]).toEqual(FlagMocks[1]);
    });

    it.async('showMoreFlags() should test the show more flags behaviour', async () => {
      const controller = await makeController();

      expect(controller.showMoreFlags).toBe(false);
      controller.toggleShowMoreFlags();
      expect(controller.showMoreFlags).toBe(true);
    });

    it.async('showAdvanced() should open up $mdDialog box', async () => {
      const controller = await makeController();

      spyOn($mdDialog, 'show').and.callThrough();

      const selectedFlags = [{ id: 1, name: 'United Kingdom', flagCode: 'GBI' }];
      controller.selectedFlags = selectedFlags;

      expect(controller.modalSelectedFlags).toEqual([]);

      controller.showAdvanced();
      $rootScope.$apply();

      expect($mdDialog.show).toHaveBeenCalled();
      // Selected flags is copied to modal selected flags.
      expect(controller.modalSelectedFlags).toEqual(selectedFlags);
    });

    it.async('should test cancel() behaviour', async () => {
      const controller = await makeController();
      spyOn($mdDialog, 'cancel');

      controller.cancel();

      expect($mdDialog.cancel).toHaveBeenCalled();
      expect(controller.modalSelectedFlags).toEqual([]);
    });

    it.async('should test submit() behaviour', async () => {
      const controller = await makeController({
        onChange: () => {},
      });

      spyOn(controller, 'onChange').and.callThrough();
      spyOn($mdDialog, 'cancel');

      const mockData = [{ id: 1, name: 'United Kingdom', flagCode: 'GBI' }];
      controller.modalSelectedFlags = mockData;
      controller.submit();

      expect(controller.showMoreFlags).toBe(false);
      expect(controller.selectedFlags).toEqual(mockData);
      expect(controller.modalSelectedFlags).toEqual([]);
      expect($mdDialog.cancel).toHaveBeenCalled();
      expect(controller.onChange).toHaveBeenCalledWith(
        { flagStateCodes: ['GBI'] });

      controller.onChange.calls.reset();
    });

    it.async('should test refresh() behaviour', async () => {
      const controller = await makeController();
      spyOn(controller, 'loadFlagOptions');

      controller.refresh();
      expect(controller.loadFlagOptions).toHaveBeenCalled();
    });

    it.async('should test loadFlagOptions() behaviour', async () => {
      const controller = await makeController();

      controller.modalSelectedFlags = [{ id: 1, name: 'United Kingdom', flagCode: 'GBI' }];
      controller.loadFlagOptions();

      expect(controller.unselectedFlags.length).toEqual(7);

      controller.modalSelectedFlags = [];
      controller.loadFlagOptions();

      expect(controller.unselectedFlags.length).toEqual(8);
    });

    it.async('clearAllSelection() should remove all the selected flags in the modal', async () => {
      const controller = await makeController();
      controller.clearAllSelection();
      expect(controller.modalSelectedFlags).toEqual([]);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = FlagFilterComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(FlagFilterTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(FlagFilterController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      scope = $rootScope.$new();

      element = angular.element('<flag-filter><flag-filter/>');
      element = $compile(element)(scope);

      spyOn($mdDialog, 'show').and.callFake(() => {
        const deferred = $q.defer();
        return deferred.promise;
      });

      scope.$apply();

      controller = element.controller('FlagFilter');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1);
    });

    it('should open mdDialog after flag button pressed', () => {
      const button = element.find('button');
      button.triggerHandler('click');
      expect($mdDialog.show).toHaveBeenCalled();
    });
  });
});
