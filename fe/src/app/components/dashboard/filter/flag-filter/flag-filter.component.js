import './flag-filter.scss';
import controller from './flag-filter.controller';
import template from './flag-filter.pug';

export default {
  restrict: 'E',
  bindings: {
    onChange: '&',
    selectedFlagCodes: '<',
  },
  template,
  controllerAs: 'vm',
  controller,
};
