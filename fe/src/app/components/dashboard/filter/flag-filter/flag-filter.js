import angular from 'angular';
import flagFilterComponent from './flag-filter.component';
import uiRouter from 'angular-ui-router';

export default angular.module('flagFilter', [
  uiRouter,
])
.component('flagFilter', flagFilterComponent)
.name;
