import angular from 'angular';
import CodicilTypeComponent from './codicil-type.component';
import uiRouter from 'angular-ui-router';

export default angular.module('codicilType', [
  uiRouter,
])
.component('codicilType', CodicilTypeComponent)
.name;
