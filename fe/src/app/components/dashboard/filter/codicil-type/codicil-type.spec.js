/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import CodicilTypeComponent from './codicil-type.component';
import CodicilTypeController from './codicil-type.controller';
import CodicilTypeMocks from
  'app/common/models/codicil/codicil-type/codicil-type.mocks.json';
import CodicilTypeModel from
  'app/common/models/codicil/codicil-type/codicil-type';
import CodicilTypeModule from './';
import CodicilTypeTemplate from './codicil-type.pug';

describe('CodicilType', () => {
  let $rootScope,
    $compile,
    makeController,
    mockCodicilTypes,
    mockSelectedTypes;

  beforeEach(window.module(CodicilTypeModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;

    mockCodicilTypes = [
      {
        ID: 'COC',
        NAME: 'cd-condition-of-class',
        OPEN_STATUS_ID: 14,
      },
      {
        ID: 'AI',
        NAME: 'cd-actionable-item',
        OPEN_STATUS_ID: 3,
      },
    ];

    mockSelectedTypes = ['COC', 'AI'];

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('codicilType', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('has codicil types property', async() => {
      const controller = await makeController({ codicilTypes: mockCodicilTypes });
      expect(controller.codicilTypes).not.toBeNull();
    });

    it.async('should select \'All\' with all types selected by default', async() => {
      const controller = await makeController({
        codicilTypes: mockCodicilTypes,
        selectedCodicilTypesIds: mockSelectedTypes,
      });
      controller.updateSelection();
      expect(controller._isAll).toEqual(true);
      _.forEach(controller.codicilTypes, (type) => {
        expect(type.selected).toEqual(true);
      });
    });

    it.async('should select all by default', async () => {
      const controller = await makeController({ selectedCodicilTypesIds: null });
      expect(controller.isAll).not.toEqual(true);
      expect(controller.isAll).toEqual(false);

      _.forEach(controller.codicilTypesIds, (codicilType) => {
        expect(codicilType.selected).toEqual(true);
      });
    });

    it.async('should select all when \'All\' is selected', async() => {
      const controller = await makeController({ codicilTypes: mockCodicilTypes });

      // Unselect all
      _.forEach(controller.codicilTypes, (type, index) => {
        controller.codicilTypes[index].selected = false;
      });

      // Select 'All'
      controller.isAll = true;

      // Select all
      _.forEach(controller.codicilTypes, (type) => {
        expect(type.selected).toEqual(true);
      });
    });

    it.async('should unselect all when \'All\' is unselected', async() => {
      const controller = await makeController({ codicilTypes: mockCodicilTypes });
      // Unselect 'All'
      controller.isAll = false;

      // Unselect all
      _.forEach(controller.codicilTypes, (type) => {
        expect(type.selected).toEqual(false);
      });
    });

    it.async('should unselect \'All\' when at least one type is unselected', async() => {
      const controller = await makeController({
        codicilTypes: mockCodicilTypes,
        selectedCodicilTypesIds: mockSelectedTypes,
      });
      controller.updateSelection();
      expect(controller.isAll).toEqual(true);
      controller.codicilTypes[0].selected = false;
      controller.updateIsAllStatus();
      expect(controller.isAll).toEqual(false);
    });

    it.async('should select \'All\' when all codicil types is selected', async() => {
      const controller = await makeController({ codicilTypes: mockCodicilTypes });

      // Like previous test, we continue with at least 1 is unselected
      controller.codicilTypes[0].selected = false;
      controller.updateIsAllStatus();
      expect(controller.isAll).toEqual(false);

      // Then select back and test to be toEqual true
      _.forEach(controller.codicilTypes, (type) => {
        _.assign(type, { selected: true });
      });
      controller.updateIsAllStatus();
      expect(controller.isAll).toEqual(true);
    });

    it.async('setCodicilTypesValues() should pass the correct selected status ids', async() => {
      const controller = await makeController({ codicilTypes: mockCodicilTypes,
        onChange: () => {} });
      spyOn(controller, 'onChange');

      controller.selectedCodicilTypesIds = mockSelectedTypes;
      controller.updateSelection();
      controller.codicilTypes[0].selected = false;
      controller.setCodicilTypesValues();

      expect(controller.onChange)
        .toHaveBeenCalledWith({ codicilTypeId: ['AI'] });
    });

    it.async('onSelectionChanged should call setCodicilTypesValues and updateIsAllStatus', async() => {
      const controller = await makeController({ codicilTypes: mockCodicilTypes,
        onChange: () => {} });
      spyOn(controller, 'setCodicilTypesValues');
      spyOn(controller, 'updateIsAllStatus');

      controller.onSelectionChanged();
      expect(controller.updateIsAllStatus).toHaveBeenCalledTimes(1);
    });

    it.async('updateSelection() should update the codicilTypes object based on selectedCodicilTypesIds', async() => {
      const codicilTypes = [
        { ID: 'COC', selected: false },
        { ID: 'AI', selected: false },
      ];
      const _selectedCodicilTypesIds = ['COC'];
      const controller = await makeController({ codicilTypes, onChange: () => {} });
      spyOn(controller, 'updateIsAllStatus');

      controller.selectedCodicilTypesIds = _selectedCodicilTypesIds;
      controller.updateSelection();

      expect(controller.codicilTypes[0].selected).toEqual(true);
      expect(controller.codicilTypes[1].selected).toEqual(false);
      expect(controller.updateIsAllStatus).toHaveBeenCalledTimes(1);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CodicilTypeComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CodicilTypeTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CodicilTypeController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<codicil-type foo="bar"><codicil-type/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('CodicilType');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
