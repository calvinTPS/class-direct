import * as _ from 'lodash';
import Base from 'app/base';

export default class CodicilTypeController extends Base {
  /* @ngInject */
  constructor(
    $timeout,
    AppSettings,
  ) {
    super(arguments);
    this._isAll = false;
  }

  onSelectionChanged() {
    this.setCodicilTypesValues();
    this.updateIsAllStatus();
  }

  $onInit() {
    this._codicilTypes = this.codicilTypes ? this.codicilTypes : [
      this._AppSettings.CODICILS_META.COC,
      this._AppSettings.CODICILS_META.AI,
    ];
    // Had to set timeout, if not the updateSelection() method won't applied on view
    // Still not sure why needs to apply timeout, because logically it should work without timeout
    this._$timeout(() => this.updateSelection());
  }

  /**
   * Updates the selection based on given selected class status IDs on first load
   */
  updateSelection() {
    const selectedCodicilTypesIdsMap = _.keyBy(this.selectedCodicilTypesIds);

    _.forEach(this._codicilTypes, (codicilTypesObj) => {
      _.assign(codicilTypesObj,
        { selected: selectedCodicilTypesIdsMap[codicilTypesObj.ID] != null });
    });

    this.updateIsAllStatus();
  }

  get codicilTypes() {
    return this._codicilTypes;
  }

  set codicilTypes(types) {
    this._codicilTypes = types;
  }

  get isAll() {
    return this._isAll;
  }

  /**
   * This set all() will set all this.statuses[x].selected the the same status
   * with the given parameter bool
   *
   * @param {Boolean} bool
   */
  set isAll(bool) {
    this._isAll = bool;
    _.forEach(this.codicilTypes, (statusObj) => {
      _.assign(statusObj, { selected: bool });
    });
  }

  /**
   * Updates the all checkbox based on the child status states.
   */
  updateIsAllStatus() {
    this._isAll = _.every(_.map(this.codicilTypes), statusObj => statusObj.selected);
  }

  /**
    * manipulate selected checkboxes and send the selection to execute query
    */
  setCodicilTypesValues() {
    const codicilTypesIds = [];
    _.forEach(this._codicilTypes, (codicilTypeObject) => {
      if (codicilTypeObject.selected) {
        codicilTypesIds.push(codicilTypeObject.ID);
      }
    });
    // executes this.onChange given from the parent component with an array of
    // codicilTypeIds from this.statuses where selected === true
    this.onChange({ codicilTypeId: codicilTypesIds });
  }
}
