import './codicil-type.scss';
import controller from './codicil-type.controller';
import template from './codicil-type.pug';

export default {
  bindings: {
    codicilTypes: '<',
    isSingleAsset: '<',
    onChange: '&',
    selectedCodicilTypesIds: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
