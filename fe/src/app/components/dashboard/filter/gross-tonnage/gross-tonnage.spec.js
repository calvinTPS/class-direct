/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import findIn from 'src/spec-helpers/find-in';
import GrossTonnageComponent from './gross-tonnage.component';
import GrossTonnageController from './gross-tonnage.controller';
import GrossTonnageModule from './gross-tonnage';
import GrossTonnageTemplate from './gross-tonnage.pug';
import translationStrings from 'resources/locales/en-GB.json';

describe('GrossTonnage', () => {
  let $rootScope,
    $compile,
    $interpolate,
    scope,
    makeController;

  beforeEach(window.module(GrossTonnageModule));
  beforeEach(() => {
    const mockTranslateFilter = value => $interpolate(translationStrings[value])(scope);
    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$interpolate_, $componentController) => {
    $interpolate = _$interpolate_;
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('grossTonnage', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // Controller method tests
    it('setMinMaxValue', () => {
      const controller = makeController({
        onChange: () => {},
        min: 50,
        max: 100,
      });

      spyOn(controller, 'onChange').and.callThrough();

      controller.setMinMaxValue({ $valid: true });
      expect(controller.onChange).toHaveBeenCalledWith({
        grossTonnageMin: 50,
        grossTonnageMax: 100,
      });

      controller.onChange.calls.reset();
      controller.setMinMaxValue({ $valid: false });
      expect(controller.onChange).not.toHaveBeenCalled();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = GrossTonnageComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(GrossTonnageTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(GrossTonnageController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element('<gross-tonnage min="min" max="max"><gross-tonnage/>');
      element = $compile(element)(scope);

      scope.min = undefined;
      scope.max = undefined;
      scope.$apply();

      controller = element.controller(GrossTonnageModule);
    });

    it('set initial scope', () => {
      expect(scope.min).not.toBeDefined();
      expect(scope.max).not.toBeDefined();
    });

    it('should have invalid form state when min > max', () => {
      const grossMin = element.find('input[placeholder="Min"]');
      const grossMax = element.find('input[placeholder="Max"]');

      grossMin.triggerHandler('focus');
      scope.min = 3;
      scope.$digest();
      grossMin.triggerHandler('blur');

      grossMax.triggerHandler('focus');
      scope.max = 2;
      scope.$digest();
      grossMax.triggerHandler('blur');

      expect(element.find('form').hasClass('ng-valid')).toBeFalsy();
      expect(element.find('form').hasClass('ng-invalid')).toBeTruthy();

      expect(findIn(element, '.input-max').text()).toContain("Can't be less than 3");
      expect(findIn(element, '.input-min').text()).toContain("Can't be more than 2");
    });

    it('should have a valid form state when min < max', () => {
      const grossMin = element.find('input[placeholder="Min"]');
      const grossMax = element.find('input[placeholder="Max"]');

      grossMin.triggerHandler('focus');
      scope.min = 1;
      scope.$digest();
      grossMin.triggerHandler('blur');

      grossMax.triggerHandler('focus');
      scope.max = 2;
      scope.$digest();
      grossMax.triggerHandler('blur');

      expect(element.find('form').hasClass('ng-valid')).toBeTruthy();
      expect(element.find('form').hasClass('ng-invalid')).toBeFalsy();
    });
  });
});
