import angular from 'angular';
import grossTonnageComponent from './gross-tonnage.component';

export default angular.module('grossTonnage', [
])
.component('grossTonnage', grossTonnageComponent)
.name;
