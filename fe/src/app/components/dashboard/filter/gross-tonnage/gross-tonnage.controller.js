import Base from 'app/base';

export default class GrossTonnageController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
  }

  setMinMaxValue(form) {
    if (form.$valid) {
      this.onChange({ grossTonnageMin: this.min, grossTonnageMax: this.max });
    }
  }
}
