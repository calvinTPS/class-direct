import './gross-tonnage.scss';
import controller from './gross-tonnage.controller';
import template from './gross-tonnage.pug';

export default {
  restrict: 'E',
  bindings: {
    min: '<',
    max: '<',
    onChange: '&',
  },
  template,
  controllerAs: 'vm',
  controller,
};
