import './filter.scss';
import controller from './filter.controller';
import template from './filter.pug';

export default {
  bindings: {
    expandable: '<?',
    isModal: '<?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
