/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, angular/no-service-method, no-underscore-dangle */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import componentSpyOn from 'src/spec-helpers/component-spy-on';
import FilterComponent from './filter.component';
import FilterController from './filter.controller';
import FilterModule from './filter';
import FilterTemplate from './filter.pug';
import mockServiceData from './service-type/service-type.mocks.json';

describe('Filter', () => {
  let makeController;
  let mockDashboardService;
  let mockMdDialog;
  let $state;

  const favouritesSectionSpy = componentSpyOn('favouritesSection');
  const grossTonnageSpy = componentSpyOn('grossTonnage');
  const mockFlagService = {
    getAll: () => { },
  };

  beforeEach(window.module(
    FilterModule,
    angularMaterial,
    favouritesSectionSpy,
    grossTonnageSpy,
    { AppSettings },
    { FlagService: mockFlagService },
  ));

  beforeEach(() => {
    mockMdDialog = {
      hide: () => { },
      cancel: () => { },
    };
    const mockTranslateFilter = value => value;

    mockDashboardService = {
      submitQuery: () => { },
      queryOptions: {},
      setSearchAssetsSelectedSortType: () => {},
    };

    window.module(($provide) => {
      $provide.value('$mdMedia', () => false);
      $provide.value('$mdDialog', mockMdDialog);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('ViewService', {});
    });
  });

  beforeEach(inject((_$state_, $componentController) => {
    $state = _$state_;
    makeController =
      (bindings = {}) => $componentController(FilterModule, {}, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('should validate currentState', () => {
      const controller = makeController();
      $state.current.name = AppSettings.STATES.VESSEL_LIST;
      expect(controller.currentState).toEqual(AppSettings.STATES.VESSEL_LIST);

      $state.current.name = AppSettings.STATES.SERVICE_SCHEDULE;
      expect(controller.currentState).toEqual(AppSettings.STATES.SERVICE_SCHEDULE);
    });

    describe('updateFilterOptions', () => {
      it('updates the filter options of the sidebar', () => {
        const controller = makeController();

        controller.updateFilterOptions({ isFavourite: false });
        controller.updateFilterOptions.flush(); // debounced
        expect(controller.filterOptions).toEqual({ isFavourite: false });

        controller.updateFilterOptions({ isFavourite: true });
        controller.updateFilterOptions.flush(); // debounced
        expect(controller.filterOptions).toEqual({ isFavourite: true });
      });
    });

    describe('clearFilterOptions', () => {
      it('should reload the state and close the filter', () => {
        const controller = makeController();
        controller.isOpen = true;
        spyOn($state, 'reload');
        spyOn(mockMdDialog, 'hide');

        controller.clearFilterOptions();
        expect($state.reload).toHaveBeenCalledTimes(1);
        expect(mockMdDialog.hide).toHaveBeenCalledTimes(1);
        expect(controller.isOpen).toBe(true);
      });
    });

    describe('visibleCount', () => {
      const testScenarios = [
        {
          description: 'last page less than page size',
          pagination: {
            size: 15,
            number: 2,
            numberOfElements: 10,
            totalElements: 25,
          },
          expectedVisibleCount: 25,
        },
        {
          description: 'last page same as page size',
          pagination: {
            size: 15,
            number: 2,
            numberOfElements: 15,
            totalElements: 30,
          },
          expectedVisibleCount: 30,
        },
        {
          description: 'first page less than page size',
          pagination: {
            size: 15,
            number: 1,
            numberOfElements: 10,
            totalElements: 30,
          },
          expectedVisibleCount: 10,
        },
        {
          description: 'first page same as page size',
          pagination: {
            size: 15,
            number: 1,
            numberOfElements: 15,
            totalElements: 30,
          },
          expectedVisibleCount: 15,
        },
      ];

      testScenarios.forEach((test) => {
        describe(test.description, () => {
          const assetsWithPagination = [];
          assetsWithPagination.pagination = test.pagination;

          it('shows the number of items already loaded in the page', () => {
            const controller = makeController();
            controller._DashboardService.assets = assetsWithPagination;

            expect(controller.visibleCount).toBe(test.expectedVisibleCount);
          });
        });
      });
    });
  });

  describe('Integration with components', () => {
    let controller;
    beforeEach(() => {
      controller = makeController();
    });

    it('should update DashboardService queryOptions for favourites', () => {
      controller.updateFilterOptions({ isFavourite: false });
      controller.updateFilterOptions.flush(); // debounced
      expect(mockDashboardService.queryOptions.isFavourite).toEqual(false);
    });

    it('should update DashboardService queryOptions for gross tonnage', () => {
      const grossTonnageOptions = {
        grossTonnageMin: 50,
        grossTonnageMax: 100,
      };
      controller.updateFilterOptions(grossTonnageOptions);
      controller.updateFilterOptions.flush(); // debounced
      expect(mockDashboardService.queryOptions.grossTonnageMin).toEqual(50);
      expect(mockDashboardService.queryOptions.grossTonnageMax).toEqual(100);
    });

    it('should reset the services when clear filter method is called', () => {
      controller.resetServices(mockServiceData);
      expect(mockServiceData[0].isCategorySelected).toBe(true);
      expect(mockServiceData[0].details[0].isSelected).toBe(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = FilterComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(FilterTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(FilterController);
    });
  });
});
