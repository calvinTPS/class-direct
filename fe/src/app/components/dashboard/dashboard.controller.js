import Base from 'app/base';
import RouteMixins from 'app/route.mixin';

export default class DashboardController extends RouteMixins(Base) {
  /* @ngInject */
  constructor(
    $scope,
    $state,
    AppSettings,
    DashboardService,
  ) {
    super(arguments);
  }

  $onInit() {
    this._DashboardService.currentUser = this.currentUser;
  }

  get isServiceSchedule() {
    return this._$state.current.name === this._AppSettings.STATES.SERVICE_SCHEDULE;
  }

  get isFleetDashboardView() {
    return this._$state.current.name === this._AppSettings.STATES.VESSEL_LIST ||
      this._$state.current.name === this._AppSettings.STATES.SERVICE_SCHEDULE;
  }

  get isAdministrationView() {
    return this._$state.current.name === this._AppSettings.STATES.ADMINISTRATION;
  }

  get dashboardAssets() {
    return this._DashboardService.assets;
  }
}
