/* eslint-disable no-param-reassign, angular/timeout-service */
import _ from 'lodash';

/* @ngInject */
export default function config(
  $stateProvider,
  $urlRouterProvider,
) {
  // Redirect /fleet to /fleet/asset-list
  $urlRouterProvider.when('/fleet', '/fleet/asset-list');

  const getInitialAssets = (
    $location,
    $rootScope,
    AppSettings,
    AssetService,
    DashboardService,
    dateRange,
    currentUser,
    isServiceSchedule,
    $stateParams,
  ) => {
    // if ?export=assets present in URL, show export full asset listing modal
    const isExportAssetListingMode = () =>
      $location.search().export === AppSettings.EXPORT_TYPES.ASSETS;
    // Get default payload for assets query when making first query for
    // fleet-list/service-schedule page.
    let defaultQuery = DashboardService.hasAssetsInitialised ? DashboardService.queryOptions :
      _.clone(AppSettings.DEFAULT_PARAMS.FLEET.QUERY);
    if (!DashboardService.hasAssetsInitialised) {
      DashboardService.queryParams = _.clone(AppSettings.DEFAULT_PARAMS.FLEET_SORT);
    }
    let additionalQuery = {};
    // additionalQuery for service schedule. Only add if
    // initialise/refresh service schedule page
    // going to service schedule page from fleet list
    if (isServiceSchedule &&
      (!DashboardService.hasAssetsInitialised ||
        DashboardService.isServiceSchedule !== isServiceSchedule)) {
      additionalQuery = {
        codicilDueDateMin: dateRange.lowRange,
        codicilDueDateMax: dateRange.highRange,
        isSubfleet: true,
      };
    } else if (!isServiceSchedule) {
      defaultQuery = _.omit(defaultQuery, ['codicilDueDateMin', 'codicilDueDateMax', 'isSubfleet']);
    }
    DashboardService.isServiceSchedule = isServiceSchedule;
    DashboardService.queryOptions = _.assign(defaultQuery, additionalQuery);
    // Query favourites
    if (isExportAssetListingMode()) {
      DashboardService.queryOptions.isFavourite = false;
      return DashboardService.submitQuery(true).then(() => {
        setTimeout(() => {
          $rootScope.$broadcast(AppSettings.EVENTS.ASSETS_EXPORT_INITIALIZE);
        }, 800);
      });
    }

    if (!DashboardService.hasAssetsInitialised) {
      DashboardService.queryOptions.isFavourite = true;
    }

    if (!DashboardService.forceNewQuery &&
      _.isEqual(DashboardService.queryOptions, DashboardService.previousQuery)) {
      return DashboardService.assets;
    }

    return DashboardService.submitQuery().then((favouriteAssetsResult) => {
      DashboardService.hasAssetsInitialised = true;
      DashboardService.forceQuery = false;
      if (
        favouriteAssetsResult != null &&
        favouriteAssetsResult.length &&
        !_.get($stateParams, 'skipFavourites')
      ) {
        DashboardService.assets = favouriteAssetsResult;
        return Promise.resolve();
      }
      DashboardService.queryOptions.isFavourite = false;
      return DashboardService.submitQuery();
    });
  };

  const assetList = (
    $location,
    $rootScope,
    AppSettings,
    AssetService,
    DashboardService,
    currentUser,
    $stateParams,
  ) =>
    getInitialAssets(
      $location,
      $rootScope,
      AppSettings,
      AssetService,
      DashboardService,
      null,
      currentUser,
      false,
      $stateParams,
    );

  const assetService = (
    $location,
    $rootScope,
    AppSettings,
    AssetService,
    DashboardService,
    dateRange,
  ) =>
    getInitialAssets(
      $location,
      $rootScope,
      AppSettings,
      AssetService,
      DashboardService,
      dateRange,
      null,
      true,
    );

  $stateProvider
    .state('fleet', {
      url: '/fleet',
      abstract: true,
      component: 'dashboard',
      resolve: {
        /* @ngInject */
        currentUser: UserService => UserService.getCurrentUser(),
      },
    })
    .state('fleet.vessel', {
      url: '/fleet-list',
      params: {
        skipFavourites: false,
      },
      resolve: {
        /* @ngInject */ // eslint-disable-next-line object-shorthand
        assetsToDefer: function fn(
          $location,
          $rootScope,
          AppSettings,
          AssetService,
          DashboardService,
          currentUser,
          $stateParams,
        ) {
          return assetList.bind(null, ...arguments);
        },
      },
      views: {
        'dashboard-content': {
          component: 'vesselList',
        },
      },
    })
    .state('fleet.schedule', {
      url: '/survey-schedule',
      params: {
        skipFavourites: false,
      },
      resolve: {
        /* @ngInject */
        // TODO : Modify after API has been build
        dateRange: () => {
          // For now the value is default to :
          // minrange = today -30 days
          // maxrange = minrange + 7years

          const today = new Date();
          const minRange = new Date().setMonth(today.getMonth() - 30);
          const maxRange = new Date(minRange).setYear(new Date(minRange).getFullYear() + 7);
          const dateRange = {
            lowRange: new Date(minRange),
            highRange: new Date(maxRange),
          };
          return dateRange;
        },
        /* @ngInject */ // eslint-disable-next-line object-shorthand
        assetsServiceToDefer: function fn(
          $location,
          $rootScope,
          AppSettings,
          AssetService,
          DashboardService,
          dateRange,
        ) {
          return assetService.bind(null, ...arguments);
        },
      },
      views: {
        'dashboard-content': {
          component: 'serviceSchedule',
        },
      },
    });
}
