/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, angular/timeout-service, no-underscore-dangle */

import * as _ from 'lodash';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import componentSpyOn from 'src/spec-helpers/component-spy-on';
import DashboardComponent from './dashboard.component';
import DashboardController from './dashboard.controller';
import DashboardModule from './';
import DashboardTemplate from './dashboard.pug';

describe('Dashboard', () => {
  let $rootScope;
  let $injector;
  let $scope;
  let $state;
  let makeController;
  let mockDashboardService;
  let mockUserService;

  const filterSpy = componentSpyOn('filter');

  const mockTranslateFilter = value => value;

  const mockAssetService = {
    query: () => Promise.resolve([]),
  };

  // Call invoke to inject dependencies and run function
  const getRequiredServices = objectToDefer => _.reduce(objectToDefer, (services, value) => {
    if (!_.isFunction(value)) {
      services.push($injector.get(value));
    }
    return services;
  }, []);

  beforeEach(window.module(DashboardModule,
    angularMaterial,
    filterSpy,
    { translateFilter: mockTranslateFilter }
  ));

  beforeEach(window.module(($provide) => {
    mockDashboardService = {
      queryOptions: {},
      queryParams: {},
      submitQuery: () => {},
      filterOptionsChanged: (filterOptions) => {
        _.assign(mockDashboardService.queryOptions, filterOptions);
      },
    };

    mockUserService = {
      getCurrentUser: () => Promise.resolve({ id: 1, name: 'testUser' }),
      logout: () => {},
      unsetCurrentUser: () => {},
    };

    $provide.value('AppSettings', AppSettings);
    $provide.value('AssetService', mockAssetService);
    $provide.value('DashboardService', mockDashboardService);
    $provide.value('currentUser', mockUserService);

    // resolve the dateRange value for Service Schedule routes
    $provide.value('dateRange', {
      lowRange: 'LOW-RANGE-DATE',
      highRange: 'HIGH-RANGE-DATE',
    });
  }));

  beforeEach(inject((
    _$rootScope_,
    _$compile_,
    _$injector_,
    _$state_,
    $componentController,
    $document,
    $log,
    $mdDialog,
    $window,
  ) => {
    $injector = _$injector_;
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;

    makeController =
      (bindings = {}) => $componentController(DashboardModule, {
        $document,
        $log,
        $mdDialog,
        $scope,
        $state: {
          current: {
            name: AppSettings.STATES.VESSEL_LIST,
          },
        },
        $window,
        AssetService: mockAssetService,
        AppSettings,
      }, bindings);
  }));

  describe('Routes', () => {
    describe('resolve assets, with favourite assets', () => {
      let result;
      beforeEach(() => {
        result = [{ id: 1, name: 'Ship', isFavourite: true }];
        spyOn(mockDashboardService, 'submitQuery').and.callFake(() => {
          mockDashboardService.assets = result;
          return Promise.resolve(result);
        });
      });

      describe('Vessel List state', () => {
        it('transition to fleet.vessel should respond to the right URLs', () => {
          expect($state.href(AppSettings.STATES.VESSEL_LIST)).toEqual('#!/fleet/fleet-list');
        });

        it.async('should resolve assets via asset query with the right query and params', async() => {
          // Invoke state change
          const objectToDefer =
            $state.get(AppSettings.STATES.VESSEL_LIST).resolve.assetsToDefer;
          const functionToDefer = await $injector.invoke(objectToDefer);

          // Execute the defered function
          await functionToDefer(...getRequiredServices(objectToDefer));
          expect(mockDashboardService.submitQuery).toHaveBeenCalled();
          expect(mockDashboardService.assets).toEqual(result);
          expect(mockDashboardService.queryOptions).toEqual({
            isFavourite: true,
            codicilTypeId: [],
          });
          expect(mockDashboardService.queryParams).toEqual({
            sort: 'AssetName',
            order: 'ASC',
          });
        });

        it.async('should resolve asset with isFavourite = false', async() => {
          result = [];

          // Invoke state change
          const objectToDefer =
            $state.get(AppSettings.STATES.VESSEL_LIST).resolve.assetsToDefer;
          const functionToDefer = await $injector.invoke(objectToDefer);

          // Execute the defered function
          await functionToDefer(...getRequiredServices(objectToDefer));
          expect(mockDashboardService.queryOptions).toEqual({
            isFavourite: false,
            codicilTypeId: [],
          });
          expect(mockDashboardService.queryParams).toEqual({
            sort: 'AssetName',
            order: 'ASC',
          });
        });
      });

      describe('Service Schedule state', () => {
        it('transition to fleet.schedule should respond to the right URLs', () => {
          expect($state.href(AppSettings.STATES.SERVICE_SCHEDULE)).toEqual('#!/fleet/survey-schedule');
        });

        it.async('should resolve assets via asset query with right query and params', async() => {
          // Invoke state change
          const objectToDefer =
            $state.get(AppSettings.STATES.SERVICE_SCHEDULE).resolve.assetsServiceToDefer;
          const functionToDefer = await $injector.invoke(objectToDefer);

          // Execute the defered function
          await functionToDefer(...getRequiredServices(objectToDefer));
          expect(mockDashboardService.submitQuery).toHaveBeenCalled();
          expect(mockDashboardService.assets).toEqual(result);
          expect(mockDashboardService.queryOptions).toEqual({
            isFavourite: true,
            isSubfleet: true,
            codicilTypeId: [],
            codicilDueDateMin: 'LOW-RANGE-DATE',
            codicilDueDateMax: 'HIGH-RANGE-DATE',
          });
          expect(mockDashboardService.queryParams).toEqual({
            sort: 'AssetName',
            order: 'ASC',
          });
        });

        it.async('should resolve asset with isFavourite = false', async() => {
          result = [];

          // Invoke state change
          const objectToDefer =
            $state.get(AppSettings.STATES.SERVICE_SCHEDULE).resolve.assetsServiceToDefer;
          const functionToDefer = await $injector.invoke(objectToDefer);

          // Execute the defered function
          await functionToDefer(...getRequiredServices(objectToDefer));
          expect(mockDashboardService.queryOptions).toEqual({
            isFavourite: false,
            isSubfleet: true,
            codicilTypeId: [],
            codicilDueDateMin: 'LOW-RANGE-DATE',
            codicilDueDateMax: 'HIGH-RANGE-DATE',
          });
          expect(mockDashboardService.queryParams).toEqual({
            sort: 'AssetName',
            order: 'ASC',
          });
        });
      });
    });
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('isServiceSchedule getter should get the right values', () => {
      const controller = makeController();
      expect(controller.isServiceSchedule).toEqual(false);
      controller._$state.current.name = AppSettings.STATES.SERVICE_SCHEDULE;
      expect(controller.isServiceSchedule).toEqual(true);
    });

    it('isFleetDashboardView getter should get the right values', () => {
      const controller = makeController();
      expect(controller.isFleetDashboardView).toEqual(true);
      controller._$state.current.name = AppSettings.STATES.SERVICE_SCHEDULE;
      expect(controller.isFleetDashboardView).toEqual(true);
      controller._$state.current.name = AppSettings.STATES.ADMINISTRATION;
      expect(controller.isFleetDashboardView).toEqual(false);
    });

    it('isAdministrationView getter should get the right values', () => {
      const controller = makeController();
      expect(controller.isAdministrationView).toEqual(false);
      controller._$state.current.name = AppSettings.STATES.ADMINISTRATION;
      expect(controller.isAdministrationView).toEqual(true);
    });

    it('dashboardAssets getter should get the right values', () => {
      const controller = makeController();
      mockDashboardService.assets = [
        {
          name: 'LADY K II',
          isLead: true,
          leadImo: 999999,
          imoNumber: 123123,
        },
      ];
      expect(controller.dashboardAssets).toEqual(mockDashboardService.assets);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = DashboardComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(DashboardTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(DashboardController);
    });
  });
});
