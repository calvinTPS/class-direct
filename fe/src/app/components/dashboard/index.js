import angular from 'angular';
import dashboardComponent from './dashboard.component';
import DashboardService from './dashboard.service';
import Filter from './filter/filter';
import routes from './dashboard.routes';
import ServiceSchedule from './service-schedule';
import TabsMaster from './tabs-master';
import uiRouter from 'angular-ui-router';
import VesselList from './vessel-list';

export default angular.module('dashboard', [
  Filter,
  uiRouter,
  ServiceSchedule,
  TabsMaster,
  VesselList,
])
.config(routes)
.service({
  DashboardService,
  // @insertInstance
})
.component('dashboard', dashboardComponent)
.name;
