/* global afterAll afterEach beforeAll beforeEach describe expect inject it */
/* eslint-disable angular/window-service, no-underscore-dangle */

import AppSettings from 'app/config/project-variables.js';
import TabsMasterComponent from './tabs-master.component';
import TabsMasterController from './tabs-master.controller';
import TabsMasterModule from './';
import TabsMasterTemplate from './tabs-master.pug';
import translationStrings from 'resources/locales/en-GB.json';

describe('TabsMaster', () => {
  let $rootScope;
  let $compile;
  let $interpolate;
  let $state;
  let scope;
  let makeController;
  let mockDashboardService;

  beforeEach(window.module(TabsMasterModule));
  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));
  beforeEach(() => {
    const mockTranslateFilter = value => $interpolate(translationStrings[value])(scope);
    mockDashboardService = {
      tabSelected: value => value,
    };

    window.module(($provide) => {
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
    });
  });
  beforeEach(inject((
    _$rootScope_,
    _$compile_,
    _$interpolate_,
    _$state_,
    $componentController
  ) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $interpolate = _$interpolate_;
    $state = _$state_;

    $state.$current.parent = { name: AppSettings.STATES.VESSEL_LIST.split('.')[0] };
    $state.$current.parent.name = AppSettings.STATES.VESSEL_LIST.split('.')[0];

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('tabsMaster', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        foo: 'bar',
      });
      expect(controller.foo).toEqual('bar');
    });

    it('should defined how many initialized tabs are there', () => {
      const controller = makeController({
        showAdminTab: false,
      });
      expect(controller.tabs.length).toBe(2);

      const newController = makeController({
        showAdminTab: true,
      });
      expect(newController.tabs.length).toBe(3);
    });

    it('$onDestroy() should have destroyed the right function', () => {
      const controller = makeController({
        showAdminTab: false,
      });
      spyOn(controller, '_deregisterEventHook');
      controller.$onDestroy();
      expect(controller._deregisterEventHook).toHaveBeenCalled();
    });

    it('tabSelected should set/get the right _tabSelected', () => {
      const controller = makeController();
      controller.tabSelected = 1;
      expect(controller._tabSelected).toEqual(1);

      controller._tabSelected = 2;
      expect(controller.tabSelected).toEqual(2);
    });

    it('tabs should get the right _tabs', () => {
      const controller = makeController({
        showAdminTab: false,
      });
      expect(controller.tabs).toEqual(controller._tabs);
    });

    it('revalidateTabSelection() should set _tabSelected with the right selection', () => {
      const controller = makeController();

      $state.current.name = AppSettings.STATES.SERVICE_SCHEDULE;
      $state.$current.parent = { name: AppSettings.STATES.SERVICE_SCHEDULE.split('.')[0] };
      controller.revalidateTabSelection();
      expect(controller._tabSelected).toEqual(1);
    });

    it('revalidateTabSelection() should set _tabSelected with same parent tab', () => {
      const controller = makeController({
        showAdminTab: true,
      });

      $state.current.name = 'administration.userDetails';
      $state.$current.parent = { name: AppSettings.STATES.ADMINISTRATION.split('.')[0] };
      controller.revalidateTabSelection();
      expect(controller._tabSelected).toEqual(2);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = TabsMasterComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(TabsMasterTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(TabsMasterController);
    });
  });

  describe('Rendering', () => {
    let controller; // eslint-disable-line
    let element;
    beforeEach(() => {
      $state.current.name = AppSettings.STATES.VESSEL_LIST;
      $state.$current.parent = { name: AppSettings.STATES.VESSEL_LIST.split('.')[0] };
      scope = $rootScope.$new();
      element = angular.element('<tabs-master foo="bar"><tabs-master/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('TabsMaster');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
