import './tabs-master.scss';
import controller from './tabs-master.controller';
import template from './tabs-master.pug';

export default {
  bindings: {
    showAdminTab: '<',
    tabSelected: '@?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
