import angular from 'angular';
import tabsMasterComponent from './tabs-master.component';
import uiRouter from 'angular-ui-router';

export default angular.module('tabsMaster', [
  uiRouter,
])
.component('tabsMaster', tabsMasterComponent)
.name;
