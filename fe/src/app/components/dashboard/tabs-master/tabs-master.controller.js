import * as _ from 'lodash';
import Base from 'app/base';

export default class TabsMasterController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $transitions,
    AppSettings,
    DashboardService,
  ) {
    super(arguments);
  }

  $onInit() {
    // Initial tabs
    this._tabs = [{
      class: 'tab-asset-list',
      label: 'cd-tabs-fleet-list',
      icon: 'vessel',
      uiSref: this._AppSettings.STATES.VESSEL_LIST,
    }, {
      class: 'tab-survey-schedule',
      label: 'cd-tabs-survey',
      icon: 'calendar',
      uiSref: this._AppSettings.STATES.SERVICE_SCHEDULE,
    }];

    // Add admin tab to last of array if user is admin
    if (this.showAdminTab) {
      this._tabs.push({
        class: 'tab-administration',
        label: 'cd-tabs-administration',
        icon: 'administration',
        uiSref: this._AppSettings.STATES.ADMINISTRATION,
      });
    }

    // Group tabs by parent state
    this._groupTabsByParentState = _.groupBy(this._tabs, tab => tab.uiSref.split('.')[0]);
    // Example usage: _.zipObject(['a', 'b'], [1, 2]) => { 'a': 1, 'b': 2 }
    this._stateToTabPositionIndex =
      _.zipObject(this._tabs.map(tab => tab.uiSref), this._tabs.map((tab, i) => i));

    /**
     * Revalidate tabs selection on refresh
     */
    this.revalidateTabSelection();

    /**
     * Revalidate tabs selection on state change for same parent states because
     * this constructor will not be called when changing state with same parent,
     * the rest of selection is handled by md-tab side.
     *
     * $transitions.on* returns a function deregisterEventHook() that destroys the hook
     * Reference: https://ui-router.github.io/docs/latest/interfaces/transition.ihookregistry.html#onsuccess
     */
    const parentStateName = this._$state.$current.parent.name;
    this._deregisterEventHook =
      this._$transitions.onSuccess({
        from: `${parentStateName}.**`,
        to: `${parentStateName}.**`,
      }, transition => this.revalidateTabSelection());
  }

  $onDestroy() {
    this._deregisterEventHook();
  }

  revalidateTabSelection() {
    this._tabSelected = this._stateToTabPositionIndex[this._$state.current.name];

    if (this._tabSelected == null) {
      // We are at a route that doesn't match the main tab route
      // See if we are in a child route, if so, select the tab anyway
      const firstTabWithParentState =
        this._groupTabsByParentState[this._$state.$current.parent.name] ?
        this._groupTabsByParentState[this._$state.$current.parent.name][0].uiSref :
        null;
      const newTabIndex = this._stateToTabPositionIndex[firstTabWithParentState];
      if (newTabIndex) {
        this._tabSelected = newTabIndex;
      } else {
        this._tabSelected = this._DashboardService.tabSelected;
      }
    }
  }

  get tabs() {
    return this._tabs;
  }

  get tabSelected() {
    return this._tabSelected;
  }

  set tabSelected(tabIndex) {
    this._tabSelected = tabIndex;
  }
}
