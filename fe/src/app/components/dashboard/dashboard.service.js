import _ from 'lodash';
import BaseService from 'app/services/base.service';

export default class DashboardService extends BaseService {
  /* @ngInject */
  constructor(
    $state,
    AppSettings,
    AssetService,
    UserService,
    utils,
  ) {
    super(arguments);
    this.name = 'DashboardService';

    this._assets = [];
    this._query = {};
    this._queryParams = {};
    this._submissionId = -1;
    this._isUnfilteredQueryExcludingSearch =
      this._utils.hasUnfilteredQuery(this.queryOptions, true);
    this._activeServicesFlag = false;
    this._assetsSearchTerm = '';
    this._tabSelected = 0;
    this._hasAssetsInitialised = false;
    this._previousQuery = {};
    this._forceQuery = false;
    this._previousFilteroption = '';
    this._favourites = {
      // Using object insted of array since its easier to add/remove entry
      // e.g { LRV10: 'LRV10', LRV99: 'LRV99'}
      toAdd: {},
      toRemove: {},
    };
  }

  reset() {
    this._assets = [];
    this._query = {};
    this._queryParams = {};
    this._submissionId = -1;
    this._isUnfilteredQueryExcludingSearch =
      this._utils.hasUnfilteredQuery(this.queryOptions, true);
    this._activeServicesFlag = false;
    this._hasAssetsInitialised = false;
    this._previousQuery = {};
    this._forceQuery = false;

    this._assetsSearchTerm = '';
    this._previousFilteroption = '';
    this._searchAssetSelectedSortType = null;
    this._searchAssetsCategories = null;
    this._searchAssetsSelectedCategory = null;
    this._favourites = {
      // Using object insted of array since its easier to add/remove entry
      // e.g { LRV10: 'LRV10', LRV99: 'LRV99'}
      toAdd: {},
      toRemove: {},
    };
  }

  // Use to force dashboard to make new query and not return the existing dashboard assets
  get forceNewQuery() {
    return this.hasAssetsInitialised && this.forceQuery;
  }

  get forceQuery() {
    return this._forceQuery;
  }

  set forceQuery(value) {
    this._forceQuery = value;
  }

  get previousQuery() {
    return this._previousQuery;
  }

  set previousQuery(value) {
    this._previousQuery = value;
  }

  get hasAssetsInitialised() {
    return this._hasAssetsInitialised;
  }

  set hasAssetsInitialised(value) {
    this._hasAssetsInitialised = value;
  }

  get favouriteAsset() {
    return this._favouriteAsset;
  }

  set favouriteAsset(value) {
    this._favouriteAsset = value;
  }

  resetFavourites() {
    this._favourites = {
      // Using object insted of array since its easier to add/remove entry
      // e.g { LRV10: 'LRV10', LRV99: 'LRV99'}
      toAdd: {},
      toRemove: {},
    };
  }

  get tabSelected() {
    return this._tabSelected;
  }

  set tabSelected(value) {
    this._tabSelected = value;
  }

  get isServiceSchedule() {
    return this._isServiceSchedule;
  }

  set isServiceSchedule(value) {
    this._isServiceSchedule = value;
  }

  get assets() {
    return this._assets;
  }

  set assets(arrayObject) {
    this._assets = arrayObject;
  }

  get queryOptions() {
    return this._query;
  }

  set queryOptions(options) {
    this._query = options;
  }

  get queryParams() {
    return this._queryParams;
  }

  set queryParams(params) {
    this._queryParams = params;
  }

  get activeServicesFlag() {
    return this._activeServicesFlag;
  }

  set activeServicesFlag(bool) {
    this._activeServicesFlag = bool;
  }

  get canSearchAssets() {
    return !!this._canSearchAssets;
  }

  set canSearchAssets(bool) {
    this._canSearchAssets = bool;
  }

  get searchAssetsSelectedCategory() {
    if (!this._searchAssetsSelectedCategory) {
      this._searchAssetsSelectedCategory = this.searchAssetsCategories[0];
    }
    return this._searchAssetsSelectedCategory;
  }

  set searchAssetsSelectedCategory(obj) {
    this._searchAssetsSelectedCategory = obj;
  }

  get searchAssetsCategories() {
    if (!this._searchAssetsCategories) {
      this._searchAssetsCategories = _.filter(
        _.clone(this._AppSettings.ASSETS_SEARCH_CATEGORIES),
        category => category.name !== 'cd-former-asset-name',
      );
    }
    return this._searchAssetsCategories;
  }

  get searchAssetsSelectedSortType() {
    if (this._searchAssetSelectedSortType) {
      return this._searchAssetSelectedSortType;
    }
    this._searchAssetSelectedSortType = _.cloneDeep(this._AppSettings.ASSETS_SORT_BY_CATEGORIES)[0];
    return this._searchAssetSelectedSortType;
  }

  setSearchAssetsSelectedSortType(stringParam) {
    const result = _.find(_.cloneDeep(this._AppSettings.ASSETS_SORT_BY_CATEGORIES),
      { sort: stringParam });
    this._searchAssetSelectedSortType = result || {};
  }

  get assetsSearchTerm() {
    return this._assetsSearchTerm;
  }

  set assetsSearchTerm(param) {
    this._assetsSearchTerm = param;
  }

  getAssetsSearchQuery() {
    const searchQuery = {};
    // When search term exists
    if (this._canSearchAssets) {
      if (this.searchAssetsSelectedCategory.type === 'array') {
        searchQuery[this.searchAssetsSelectedCategory.query] =
          [_.parseInt(this.assetsSearchTerm, 10)];
      } else {
        const hasWildcard = this.searchAssetsSelectedCategory.wildcard;
        searchQuery[this.searchAssetsSelectedCategory.query] =
          hasWildcard ? `*${this.assetsSearchTerm}*` : this.assetsSearchTerm;
      }
      // When search term doesn't exist
    } else {
      this.removeFilterOption(this.searchAssetsSelectedCategory.query);
    }
    return searchQuery;
  }

  /**
   * Whether or not the query is unfiltered (returns all assets).
   *
   * @returns {boolean}
   */
  get isUnfilteredQuery() {
    return this._utils.hasUnfilteredQuery(this.queryOptions);
  }

  /**
    *  Whether or not the query is unfiltered (excluding search)
    *
    * @returns {boolean}
    */
  get isUnfilteredQueryExcludingSearch() {
    return this._isUnfilteredQueryExcludingSearch;
  }

  /**
   * Whether or not the query is unfiltered favourites (returns all favourites).
   *
   * @returns {boolean}
   */
  get isUnfilteredFavouritesQuery() {
    return this._utils.hasUnfilteredFavouritesQuery(this.queryOptions);
  }

  /**
   * Adds/updates filter options to existing options.
   *
   * Triggers a query.
   *
   * @param filterOptions
   */
  filterOptionsChanged(filterOptions) {
    if (this._previousFilteroption !== '' && filterOptions !== this._previousFilteroption) {
      this.removeFilterOption(this._previousFilteroption);
    }
    _.assign(this._query, filterOptions);
  }

  /**
  * Remove the filter option. Does not trigger a query.
  *
  * @param filterKey The filter key to remove from query params.
  */
  removeFilterOption(filterKey) {
    this._query = _.omit(this._query, [filterKey]);
  }

  /**
   * Submits the query. This is to be the one place where a query is ever submitted
   * for the dashboard (do not call AssetService.query directly).
   *
   * @param {boolean} forceQuerySubmission Bypass the full query check.
   * @returns {Promise} A resolved promise once query has completed.
   */
  async submitQuery(forceQuerySubmission) {
    this._previousQuery = _.clone(this.queryOptions);
    this.filterOptionsChanged(this.getAssetsSearchQuery());
    this._previousFilteroption = this.searchAssetsSelectedCategory.query;
    // We don't want to submit nulls to the server.
    const queryToSubmit = _.omitBy(this.queryOptions, v => v == null);
    const currentUser = await this._UserService.getCurrentUser();
    this._submissionId += 1;
    const currentSubmissionId = this._submissionId;

    // Checking for the default query state
    const isTruncatedQuery = this._utils.hasUnfilteredQuery(queryToSubmit) &&
      currentUser.shouldTruncateSearchResults;

    let assets = [];
    this.forcedQuerySubmission = !!forceQuerySubmission;
    queryToSubmit.hasActiveServices = this.activeServicesFlag;
    if (!isTruncatedQuery || forceQuerySubmission) {
      assets = await this._AssetService.query(queryToSubmit, this.queryParams);
    }
    // Only set assets if results are for the latest submission.
    if (currentSubmissionId === this._submissionId) {
      this.assets = assets;
      this._isUnfilteredQueryExcludingSearch =
        this._utils.hasUnfilteredQuery(this.queryOptions, true);
    }

    return Promise.resolve(assets);
  }
}
