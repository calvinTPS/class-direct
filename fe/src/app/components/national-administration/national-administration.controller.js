import _ from 'lodash';
import Base from 'app/base';

export default class NationalAdministrationController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $scope,
    $timeout,
    AppSettings,
    DownloadService,
    FlagService,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.autoCompleteSettings = this._AppSettings.AUTO_COMPLETE;
    this.filterOptions = _.cloneDeep(this._AppSettings.HISTORICAL_DOCUMENTATION_OPTIONS);
    this.searchResult = [];
    this.searchText = '';
    this.selectedFlag = null;
    this.countryFile = null;
    this.flag = [];
    this.historicalDocs = [];
    this.fileNotFound = false;

    this._populateFlagsFromService();

    this.countryFileAction = {
      clickArea: 'icon',
      icon: 'download',
      hasGap: true,
      ngClick: () => this._DownloadService
        .downloadFile({
          token: this.countryFile.token,
          mobileDownload: {
            httpMethod: 'GET',
            fileName: this.countryFile.flag,
            fileExtension: 'pdf',
          },
        }),
    };

    this.countryFileItemTemplateLocation = this._AppSettings.RIDER_TEMPLATES.COUNTRY_FILE;

    // TODO: call the actual API
    this.historicalDocsAction = {
      clickArea: 'icon',
      hasGap: true,
      icon: 'download',
      ngClick: item => this._DownloadService.downloadFile({
        token: item.token,
        mobileDownload: {
          httpMethod: 'GET',
          fileName: item.flag,
          fileExtension: 'pdf',
        },
      }),
    };
  }

  async getCountryFile() {
    const { versionInUse, countryFiles } =
      await this._FlagService.getCountryFilesByFlag(this.selectedFlag);
    // Treat the first array as 'Version in use'
    this.countryFile = versionInUse;
    // Treat the rest as 'Historical documentation'
    this.historicalDocs = countryFiles;
  }

  callServiceToGetCountryFiles() {
    this.getCountryFile().then((files) => {
      this._$rootScope.$broadcast('UPDATE_RIDER_FILTER', this.historicalDocs);
      // if country found not found for the flag
      if (this.selectedFlag && !this.countryFile) {
        this.fileNotFound = true;
      } else {
        this.fileNotFound = false;
      }
    });
  }

  async _populateFlagsFromService() {
    this.flags = await this._FlagService.getAll();
  }

  searchFlag(searchText) {
    this.searchResult = searchText ?
      _.map(_.pickBy(this.flags, flag => _.startsWith(flag.name.toLowerCase(),
        searchText.toLowerCase()))) : '';
    this.countryFile = null;
    this.historicalDocs = [];
  }

  get showSpinner() {
    if (this.selectedFlag && this.countryFile === null) {
      if (this.fileNotFound) {
        return false;
      }
      return true;
    }
    return false;
  }

  cancel() {
    this._$state.go(this._$state.previous.name, this._$state.previous.params);
  }
}
