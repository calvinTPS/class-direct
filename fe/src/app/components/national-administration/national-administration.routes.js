/* eslint-disable no-param-reassign */

/* @ngInject */
export default function config(
  $stateProvider,
) {
  $stateProvider
    .state('nationalAdministration', {
      url: '/national-administration',
      component: 'nationalAdministration',
    });
}
