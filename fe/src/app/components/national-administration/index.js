import angular from 'angular';
import NationalAdministrationComponent from './national-administration.component';
import routes from './national-administration.routes';
import uiRouter from 'angular-ui-router';

export default angular.module('nationalAdministration', [
  uiRouter,
])
.config(routes)
.component('nationalAdministration', NationalAdministrationComponent)
.name;
