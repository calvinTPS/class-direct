/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppConfig from 'app/config/project-variables.js';
import FlagMocks from 'app/common/models/flag/flag.mocks.json';
import NationalAdministrationComponent from './national-administration.component';
import NationalAdministrationController from './national-administration.controller';
import NationalAdministrationModule from './';
import NationalAdministrationTemplate from './national-administration.pug';

describe('NationalAdministration', () => {
  let $rootScope,
    $state,
    $stateParams,
    $compile,
    flagServiceGetAllResult,
    makeController;

  const mockTranslateFilter = value => value;
  const mockFlagService = {
    getAll: () => { },
    getCountryFilesByFlag: () => { },
    getHistoricalDocsByFlag: () => { },
  };
  const mockDownloadService = {
    downloadFile: () => { },
  };

  const sampleCountryFileObj = [
    {
      id: 1,
      flag: 'Malaysia',
      token: 'eyJ0eXBlIjoiUzMiLCJzM1Rva2VuIjp7InJlZ2lvbiI6IkVVX1dFU1RfMSIsImJ1Y2tldE5hbWUiOiJsci1jbGFzc2RpcmVjdCIsImZpbGVLZXkiOiJjb3VudHJ5LWZpbGVzL01ZX3ZlcnNpb25fMS4wLnBkZiJ9fQ==',
      lastModifiedDate: new Date('2017-06-06'),
    },
    {
      id: 2,
      flag: 'London',
      token: 'eyJ0eXBlIjoiUzMiLCJzM1Rva2VuIjp7InJlZ2lvbiI6IkVVX1dFU1RfMSIsImJ1Y2tldE5hbWUiOiJsci1jbGFzc2RpcmVjdCIsImZpbGVLZXkiOiJjb3VudHJ5LWZpbGVzL01ZX3ZlcnNpb25fMS4wLnBkZiJ9fQ==',
      lastModifiedDate: new Date('2017-08-06'),
    },
  ];

  const sampleHistoricalDocs = [
    {
      id: 1,
      flagName: 'Malaysia',
      lastModified: new Date(),
    },
    {
      id: 2,
      flagName: 'Thailand',
      lastModified: new Date('2017-01-10'),
    },
  ];

  const mockViewService = { registerTemplate: (x, y) => { } };

  beforeEach(window.module(
    NationalAdministrationModule,
    { translateFilter: mockTranslateFilter },
    { AppSettings: AppConfig },
    { FlagService: mockFlagService },
    { DownloadService: mockDownloadService },
    { ViewService: mockViewService },
  ));

  beforeEach(inject((_$rootScope_, _$compile_, _$state_, _$stateParams_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $state = _$state_;
    $stateParams = _$stateParams_;

    flagServiceGetAllResult = Promise.resolve(_.cloneDeep(FlagMocks));
    spyOn(mockFlagService, 'getAll').and.callFake(() => flagServiceGetAllResult);

    const versionInUse =
      Object.assign({}, sampleCountryFileObj[0]);

    const countryFiles =
      [].concat(Object.assign({}, sampleCountryFileObj[1]));

    spyOn(mockFlagService, 'getCountryFilesByFlag').and.callFake(() =>
      Promise.resolve(
        {
          versionInUse,
          countryFiles,
        }));

    spyOn(mockFlagService, 'getHistoricalDocsByFlag').and.callFake(() => Promise.resolve([...sampleHistoricalDocs]));

    makeController =
      async (bindings = {}) => {
        const controller =
          $componentController('nationalAdministration', {}, bindings);
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Controller', () => {
    it.async('Test constructor default state', async () => {
      const controller = await makeController();
      expect(controller.autoCompleteSettings)
        .toEqual(AppConfig.AUTO_COMPLETE);
      expect(controller.countryFileItemTemplateLocation)
        .toEqual(AppConfig.RIDER_TEMPLATES.COUNTRY_FILE);
      expect(controller.searchResult).toEqual([]);
      expect(controller.searchText).toEqual('');
      expect(controller.selectedFlag).toBeNull();
      expect(controller.countryFile).toBeNull();
      expect(controller.flag).toEqual([]);
    });

    describe('countryFileAction', () => {
      it.async('calls DownloadService downloadFile', async () => {
        const controller = await makeController();
        spyOn(mockDownloadService, 'downloadFile');
        controller.countryFile = {
          token: 'TOKEN',
          flag: 'Foo',
        };
        controller.countryFileAction.ngClick();
        expect(mockDownloadService.downloadFile).toHaveBeenCalledTimes(1);
        expect(mockDownloadService.downloadFile).toHaveBeenCalledWith({
          token: 'TOKEN',
          mobileDownload: {
            httpMethod: 'GET',
            fileName: controller.countryFile.flag,
            fileExtension: 'pdf',
          },
        });
      });
    });

    describe('getCountryFileFlag', () => {
      it.async('gets the countryFile', async () => {
        const controller = await makeController();
        controller.selectedFlag = {
          id: 1,
          name: 'Malaysia',
          flagCode: 'MAL',
        };
        await controller.getCountryFile();
        expect(mockFlagService.getCountryFilesByFlag).toHaveBeenCalledWith(controller.selectedFlag);
        expect(controller.historicalDocs.length).toEqual(1);
        expect(controller.countryFile).toEqual(sampleCountryFileObj[0]);
      });
    });

    it.async('calls FlagService.getAll to retrieve a list of flags', async () => {
      const controller = await makeController();
      expect(mockFlagService.getAll).toHaveBeenCalledTimes(1);
      expect(controller.flags).toEqual(FlagMocks);
    });

    it.async('searchFlag() should return the correct data', async () => {
      const controller = await makeController();
      controller.searchFlag('Malaysia');
      expect(controller.searchResult.length).toEqual(1);
      expect(controller.searchResult[0].name).toEqual('Malaysia');
    });

    it.async('cancel() should go to previous state', async () => {
      spyOn($state, 'go');
      $state.previous = {
        name: 'some.route',
        params: 'mockParam',
      };
      const controller = await makeController();
      controller.cancel();
      expect($state.go).toHaveBeenCalledWith('some.route', 'mockParam');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = NationalAdministrationComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(NationalAdministrationTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(NationalAdministrationController);
    });
  });
});
