import './national-administration.scss';
import controller from './national-administration.controller';
import template from './national-administration.pug';

export default {
  bindings: {
    form: '=',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
