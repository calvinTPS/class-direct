import './port-finder.scss';
import controller from './port-finder.controller';
import template from './port-finder.pug';

export default {
  restrict: 'E',
  bindings: {
    asset: '<',
    portModel: '=',
    form: '=',
  },
  template,
  controllerAs: 'vm',
  controller,
};
