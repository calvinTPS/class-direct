/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import PortfinderComponent from './port-finder.component';
import PortfinderController from './port-finder.controller';
import PortfinderModule from './port-finder';
import PortfinderTemplate from './port-finder.pug';
import PortService from '../../services/port.service.js';

describe('Portfinder', () => {
  let $rootScope,
    makeController,
    portService,
    mockRestangular,
    mockPortObject,
    mockPromise;

  beforeEach(window.module(PortfinderModule));

  beforeEach(inject((_$rootScope_) => {
    mockPortObject = {
      $object: {},
    };
    mockPromise = {
      get() {
        return this;
      },
      getList() {
        return this;
      },
      customPOST() {
        return this;
      },
      customGET() {
        return this;
      },
      customGETLIST() {
        return this;
      },
      one() {
        return this;
      },
    };
    spyOn(mockPromise, 'get').and.callFake(() => mockPortObject);
    spyOn(mockPromise, 'getList').and.returnValue([mockPortObject]);
    spyOn(mockPromise, 'customPOST').and.returnValue([mockPortObject]);
    spyOn(mockPromise, 'customGET').and.returnValue(mockPortObject);
    spyOn(mockPromise, 'customGETLIST').and.returnValue(mockPortObject);
    spyOn(mockPromise, 'one').and.returnValue(mockPortObject);

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };
    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);

    $rootScope = _$rootScope_;
    const scope = $rootScope.$new();

    portService = new PortService(mockRestangular, AppSettings);
    makeController = () => {
      const controller = new PortfinderController(
        scope,
        AppSettings,
        portService,
      );
      controller.$onInit();
      return controller;
    };
  }));

  describe('Controller', () => {
    // controller specs
    it('only fetch matched ports while the search key length is larger than 1', () => {
      const controller = makeController();

      expect(controller.searchKey).toBeDefined();
      expect(controller.searchKey).toBe('');

      spyOn(controller, 'fetchMatchedPorts');

      controller.searchKey = 'A';
      expect(controller.fetchMatchedPorts).toHaveBeenCalled();

      controller.fetchMatchedPorts.calls.reset();
    });

    it.async('port search API is called properly', async() => {
      spyOn(portService, 'search').and.returnValue({
        then: () => Promise.resolve(),
      });
      const controller = makeController();

      controller.searchKey = 'ABC';
      expect(portService.search).toHaveBeenCalledWith('ABC');

      portService.search.calls.reset();
      controller.searchKey = 'AB';
      expect(portService.search).toHaveBeenCalled();
      expect(portService.search).toHaveBeenCalledWith('AB');
    });

    it.async('get port from API and return it properly', async() => {
      spyOn(portService, 'search').and.callFake(() => [1, 2, 3]);
      const controller = makeController();
      controller.searchKey = 'ABC';
      await controller.fetchMatchedPorts();
      expect(portService.search).toHaveBeenCalledWith('ABC');
      expect(controller.matchedPorts).toEqual([1, 2, 3]);
    });

    it('get port details of the selected port', () => {
      const controller = makeController();
      expect(controller.selectedPort).not.toBeDefined();
      controller.selectedPort = {
        id: 300,
      };
      controller.getPortDetails();
      expect(controller.portModel).toEqual(controller.selectedPort);
    });

    it('check if port model and selected port exist', () => {
      const controller = makeController();
      expect(controller.selectedPort).not.toBeDefined();
      expect(controller.portModel).not.toBeDefined();
      expect(controller.portExists).toBeFalsy();
      controller.selectedPort = {};
      expect(controller.portExists).toBeFalsy();
      controller.portModel = {};
      expect(controller.portExists).toBeTruthy();
      delete controller.selectedPort;
      expect(controller.portExists).toBeFalsy();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = PortfinderComponent;

    it('includes the intended template',
      () => expect(component.template).toEqual(PortfinderTemplate)
    );

    it('invokes the right controller',
      () => expect(component.controller).toEqual(PortfinderController)
    );
  });
});
