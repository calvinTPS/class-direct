import Base from 'app/base';

export default class PortfinderController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    AppSettings,
    PortService,
  ) {
    super(arguments);
  }

  $onInit() {
    this._noPort = false;
    this.name = 'port-finder';
    this.minKeyLength = this.autoCompleteSettings.minimumCharacter;
    this._searchKey = '';
    this._matchedPorts = [];
  }

  _changeSearchKey(searchKey) {
    if (searchKey !== this._searchKey) {
      this._searchKey = searchKey;
      if (searchKey && searchKey.length >= this.minKeyLength) {
        this.fetchMatchedPorts();
      }
    }
  }

  async fetchMatchedPorts() {
    this._matchedPorts = this._PortService.search(this._searchKey);
    const result = await this._matchedPorts;
    this._noPort = result.length === 0;
  }

  get noPort() {
    return this._noPort;
  }

  get matchedPorts() {
    return this._matchedPorts;
  }

  searchPort(searchKey) {
    this._changeSearchKey(searchKey);
  }

  get autoCompleteSettings() {
    return this._AppSettings.AUTO_COMPLETE;
  }

  getPortDetails() {
    this.portModel = this.selectedPort;
  }

  get searchKey() {
    return this._searchKey;
  }

  set searchKey(searchKey) {
    this._changeSearchKey(searchKey);
  }

  get portExists() {
    if (this.selectedPort && this.portModel) {
      return true;
    }
    return false;
  }

  getOnBlurUpdate() {
    this._noPort = !this.selectedPort && this._searchKey.length > 0;
  }
}
