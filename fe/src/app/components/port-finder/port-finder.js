import angular from 'angular';
import portFinderComponent from './port-finder.component';
import uiRouter from 'angular-ui-router';

export default angular.module('portFinder', [
  uiRouter,
])
.component('portFinder', portFinderComponent)
.name;
