import AcceptTermsAndConditions from './accept-terms-and-conditions';
import Administration from './administration';
import angular from 'angular';
import Asset from './assets/asset/asset';
import AssetCard from 'app/components/assets/asset/asset-card/asset-card';
import AssetClientName from './assets/asset/asset-header/asset-client-name/asset-client-name';
import AssetDetails from './assets/asset/asset-details/asset-details';
import AssetHeader from './assets/asset/asset-header/asset-header';
import AssetHome from './assets/asset/asset-home/asset-home';
import AssetHub from './assets/asset/asset-hub/asset-hub';
import Assets from './assets/assets';
import AssetServiceDetails from './assets/asset/asset-service-details';
import AssetServiceSchedule from './assets/asset/asset-service-schedule';
import AssetSisters from './assets/asset/asset-sisters/asset-sisters';
import AssetsServiceHighLevel from './assets/asset/assets-service-high-level';
import Bucket from './survey-request-form/bucket/bucket';
import CdLogo from './cd-logo';
import CertificatesAndRecords from './assets/asset/certificates-and-records';
import CodicilDetails from './assets/asset/codicils-listing/codicil-details/codicil-details';
import CodicilsListing from './assets/asset/codicils-listing/codicils-listing';
import CodicilsTabs from './assets/asset/codicils-tabs';
import Dashboard from 'app/components/dashboard';
import EorDashboard from 'app/components/eor-dashboard';
import ExecutiveHullSummaries from './assets/asset/executive-hull-summaries';
import ExportAssets from './assets/export-assets';
import HeaderMenu from 'app/components/header-menu';
import LocationAndDatesForm from
  './survey-request-form/location-and-dates-form/location-and-dates-form';
import MasqueradeBar from './masquerade-bar';
import NationalAdministration from './national-administration';
import PageError from './page-error';
import PortFinder from './port-finder/port-finder';
import ServiceHistory from './assets/asset/service-history';
import SurveyReports from './assets/asset/survey-reports/survey-reports';
import SurveyRequestForm from './survey-request-form/survey-request-form';
import SystemHeader from 'app/components/system-header';
import TableHeaderBtn from './assets/asset/table-view/table-header-btn';
import TableView from './assets/asset/table-view';
import UserAccount from './user-account';
import UserItem from './user-item';
import Users from './users';

const componentModule = angular.module('app.components', [
  AcceptTermsAndConditions,
  Administration,
  Asset,
  AssetCard,
  AssetClientName,
  AssetDetails,
  AssetHeader,
  AssetHome,
  AssetHub,
  Assets,
  AssetServiceDetails,
  AssetServiceSchedule,
  AssetSisters,
  AssetsServiceHighLevel,
  Bucket,
  CdLogo,
  CertificatesAndRecords,
  CodicilDetails,
  CodicilsListing,
  CodicilsTabs,
  Dashboard,
  EorDashboard,
  ExecutiveHullSummaries,
  ExportAssets,
  HeaderMenu,
  LocationAndDatesForm,
  MasqueradeBar,
  NationalAdministration,
  PageError,
  PortFinder,
  ServiceHistory,
  SurveyReports,
  SurveyRequestForm,
  SystemHeader,
  TableHeaderBtn,
  TableView,
  UserAccount,
  UserItem,
  Users,
])
.name;
export default componentModule;
