import Base from 'app/base';

export default class UserDetailsController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    PermissionsService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.sections = [
      {
        fields: [
          { label: 'cd-email', value: this.user.email },
          { label: 'cd-telephone', value: this.user.company.telephone },
        ],
      },
      {
        fields: [
          { label: 'cd-company', value: this.user.company.name },
          { label: 'cd-address-line-1', value: this.user.company.addressLine1 },
          { label: 'cd-address-line-2', value: this.user.company.addressLine2 },
          { label: 'cd-address-line-3', value: this.user.company.addressLine3 },
          { label: 'cd-city', value: this.user.company.city },
          { label: 'cd-state-province-region', value: this.user.company.state },
          { label: 'cd-zip-postal-code', value: this.user.company.postCode },
          { label: 'cd-country', value: this.user.company.country },
        ],
      },
      {
        fields: [
          {
            label: 'cd-edit-profile',
            link: `/${this._AppSettings.API_ENDPOINTS.ACCOUNT.EditProfile}`,
            ngIf: this._PermissionsService.canEditProfileOrPassword(this.user),
          },
        ],
      },
    ];
  }
}
