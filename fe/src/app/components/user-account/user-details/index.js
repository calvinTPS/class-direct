import angular from 'angular';
import infoCardModule from 'app/common/info-card';
import uiRouter from 'angular-ui-router';
import userDetailsComponent from './user-details.component';

export default angular.module('user-details', [
  uiRouter,
  infoCardModule,
])
.component('userDetails', userDetailsComponent)
.name;
