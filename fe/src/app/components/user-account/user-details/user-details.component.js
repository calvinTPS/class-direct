import './user-details.scss';
import controller from './user-details.controller';
import template from './user-details.pug';

export default {
  bindings: {
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
