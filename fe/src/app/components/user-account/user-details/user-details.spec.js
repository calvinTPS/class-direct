/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import User from 'app/common/models/user/user';
import UserDetailsComponent from './user-details.component';
import UserDetailsController from './user-details.controller';
import UserDetailsModule from './';
import UserDetailsTemplate from './user-details.pug';

describe('UserDetails', () => {
  let $rootScope,
    $compile,
    mockPermissionsService,
    makeController;

  const mockUser = {
    id: 1,
    userId: 101,
    equasis: true,
    thetis: false,
    flagCode: 100,
    shipBuilderCode: 1,
    clientCode: 4,
    name: 'John Doe',
    email: 'abc@bae.com',
    telephone: '07865432',
    city: 'WP',
    postCode: '50470',
    status: 1,
    company: 'Lloyds Register',
    address1: 'Address Line 1',
    address2: 'Address Line 2',
    address3: 'Address Line 3',
    country: 'UK',
  };

  const mockTranslateFilter = value => value;
  beforeEach(window.module(
    UserDetailsModule,
    { translateFilter: mockTranslateFilter },
    { safeTranslateFilter: mockTranslateFilter },
    { applyFilterFilter: input => input },
  ));
  beforeEach(() => {
    mockPermissionsService = {
      canEditProfileOrPassword: () => {},
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('PermissionsService', mockPermissionsService);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = { user: mockUser }) => {
        const controller = $componentController('userDetails', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      spyOn(mockPermissionsService, 'canEditProfileOrPassword').and.returnValue(true);

      const controller = makeController({ user: mockUser });
      expect(controller.user).toEqual(mockUser);

      // Permissions
      expect(controller.sections[2].fields[0].ngIf).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = UserDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(UserDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(UserDetailsController);
    });
  });

  describe('Rendering', () => {
    let scope,
      element,
      userObj;

    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element('<user-details user="user"></user-details>');
      userObj = {
        email: 'EMAIL',
        company: {
          name: 'COMPANY_NAME',
          telephone: 'TELEPHONE',
          addressLine1: 'ADDRESS_LINE_1',
          addressLine2: 'ADDRESS_LINE_2',
          addressLine3: 'ADDRESS_LINE_3',
          city: 'CITY',
          state: 'STATE',
          postCode: 'POSTCODE',
          country: 'COUNTRY',
        },
      };
      scope.user = new User(userObj);
      element = $compile(element)(scope);
      scope.$apply();
    });

    it('should show all this information', () => {
      const allTexts = _.values(userObj).concat(_.values(userObj.company)).filter(_.isString);
      allTexts.forEach(expectedText =>
        expect(element.text().indexOf(expectedText)).toBeGreaterThan(-1)
      );
    });
  });
});
