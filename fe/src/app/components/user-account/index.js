import accountDetailsModule from './account-details';
import adminModule from '../administration';
import angular from 'angular';
import routes from './user-account.routes';
import uiRouter from 'angular-ui-router';
import userAccountComponent from './user-account.component';
import userDetailsHeaderModule from './user-details-header';
import userDetailsModule from './user-details';

export default angular.module('userAccount', [
  accountDetailsModule,
  adminModule,
  uiRouter,
  userDetailsModule,
  userDetailsHeaderModule,
])
.config(routes)
.component('userAccount', userAccountComponent)
.name;
