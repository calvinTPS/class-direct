/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-underscore-dangle */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import UserAccountComponent from './user-account.component';
import UserAccountController from './user-account.controller';
import UserAccountModule from './';
import UserAccountTemplate from './user-account.pug';
import utils from 'app/common/utils/utils.factory';

describe('UserAccount', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    $state,
    $injector,
    makeController;

  const mockTranslateFilter = value => value;
  const mockUserService = {
    getCurrentUser: () => Promise.resolve([]),
    logout: () => {},
  };
  const mockNotificationService = {
    getAllTypes: () => Promise.resolve([]),
    setNotification: id => Promise.resolve([{ message: 'success' }]),
    removeNotification: id => Promise.resolve([]),
  };
  const mockPermissionsService = {
    canAccessMPMS: () => true,
    canAccessCertsAndCodicils: () => {},
    canAccessSistersSection: () => {},
    canAccessJobs: () => {},
    canPrintExport: user => user && !user.isEquasisThetis,
  };
  beforeEach(window.module(
    UserAccountModule,
    angularMaterial,
    { translateFilter: mockTranslateFilter },
  ));

  beforeEach(window.module(($provide) => {
    $provide.value('UserService', mockUserService);
    $provide.value('NotificationService', mockNotificationService);
    $provide.value('AppSettings', AppSettings);
    $provide.value('PermissionsService', mockPermissionsService);
    $provide.value('utils', utils());
  }));
  beforeEach(inject((
     _$rootScope_,
     _$compile_,
     _$injector_,
     _$state_,
     _$mdMedia_,
     $componentController
  ) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $injector = _$injector_;
    $mdMedia = _$mdMedia_;
    $state = _$state_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('userAccount', {
        UserService: mockUserService,
        NotificationService: mockNotificationService,
      }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('/user route', () => {
    it.async('should resolve currentUser', async() => {
      const fakeUser = { id: 1, name: 'user' };
      const fakeNotification = [
        {
          id: 1,
          name: 'Asset Listing',
        },
        {
          id: 2,
          name: 'Due Status',
        },
        {
          id: 3,
          name: 'Job Reports',
        },
      ];
      spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(fakeUser));
      spyOn(mockNotificationService, 'getAllTypes').and.returnValue(Promise.resolve(fakeNotification));
      const currentUser = await $injector.invoke($state.get('user').resolve.currentUser);
      const notificationTypes = await $injector.invoke($state.get('user').resolve.notificationTypes);
      expect(mockUserService.getCurrentUser).toHaveBeenCalled();
      expect(mockNotificationService.getAllTypes).toHaveBeenCalled();
      expect(currentUser).toEqual(fakeUser);
      expect(notificationTypes).toEqual(fakeNotification);
    });

    it.async('notificationTypes should return null if in EOR mode', async() => {
      const fakeUser = { id: 1, name: 'user' };
      const fakeNotification = [
        {
          id: 1,
          name: 'Asset Listing',
        },
        {
          id: 2,
          name: 'Due Status',
        },
        {
          id: 3,
          name: 'Job Reports',
        },
      ];
      $rootScope.role_mode = 'EOR';
      spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve(fakeUser));
      spyOn(mockNotificationService, 'getAllTypes').and.returnValue(Promise.resolve(fakeNotification));
      const currentUser = await $injector.invoke($state.get('user').resolve.currentUser);
      const notificationTypes = await $injector.invoke($state.get('user').resolve.notificationTypes);
      expect(currentUser).toEqual(fakeUser);
      expect(notificationTypes).toEqual(null);
    });
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('_getNotifications() will hydrates the notifications between full list and current users list', () => {
      const mockCurrentUser = {
        notifications: [
          {
            id: 3,
            userId: 'astring',
            notificationTypes: {
              id: 1,
              name: 'Asset Listing',
            },
            typeId: 1,
            favouritesOnly: 1,
          },
          // {
          //   id: 5,
          //   userID: 'astring',
          //   notificationTypes: {
          //     id: 2,
          //     name: 'Due Status',
          //   },
          //   typeId: 2,
          //   favouritesOnly: false,
          // },
          {
            id: 5,
            userId: 'astring',
            notificationTypes: {
              id: 3,
              name: 'Job Reports',
            },
            typeId: 3,
            favouritesOnly: 1,
          },
        ],
      };
      const mockNotifcationTypes = [
        {
          id: 1,
          name: 'Asset Listing',
          label: 'asset-listing',
          isEnabled: true,
          isFavouritesOnly: 1,
        },
        {
          id: 2,
          name: 'Due Status',
          label: 'due-status',
          isEnabled: false,
          isFavouritesOnly: 1,
        },
        {
          id: 3,
          name: 'Job Reports',
          label: 'job-reports',
          isEnabled: true,
          isFavouritesOnly: 1,
        },
      ];
      const controller = makeController(
        {
          currentUser: mockCurrentUser,
          notificationTypes: [
            {
              id: 1,
              name: 'Asset Listing',
            },
            {
              id: 2,
              name: 'Due Status',
            },
            {
              id: 3,
              name: 'Job Reports',
            },
          ],
        }
      );
      controller._getNotifications();
      expect(controller.notificationTypes).toEqual(mockNotifcationTypes);
    });

    it('updateNotification() to call either post or delete notification type api correctly', () => {
      const controller = makeController({ _NotificationService: mockNotificationService });
      spyOn(mockNotificationService, 'setNotification').and.returnValue(Promise.resolve({ message: 'success' }));
      spyOn(mockNotificationService, 'removeNotification').and.returnValue(Promise.resolve({ message: 'success' }));
      controller.updateNotification({
        id: 1,
        isEnabled: true,
      });
      expect(mockNotificationService.setNotification).toHaveBeenCalled();

      controller.updateNotification({
        id: 1,
        isEnabled: false,
      });
      expect(mockNotificationService.removeNotification).toHaveBeenCalled();
    });

    it('userRoles getter should return a correctly user roles string', () => {
      const controller = makeController({ currentUser: { roles: ['CLIENT', 'FLAG', 'EOR'] } });
      expect(controller.userRoles).toEqual('cd-role-client, cd-role-flag, cd-role-eor');
    });

    describe('logout', () => {
      it('calls logout on UserService', () => {
        spyOn(mockUserService, 'logout');

        const controller = makeController();
        controller.logout();

        expect(mockUserService.logout).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = UserAccountComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(UserAccountTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(UserAccountController);
    });
  });
});
