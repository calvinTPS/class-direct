import './user-account.scss';
import controller from './user-account.controller';
import template from './user-account.pug';

export default {
  bindings: {
    currentUser: '<',
    notificationTypes: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
