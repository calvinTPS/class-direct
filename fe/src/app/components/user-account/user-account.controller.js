import _ from 'lodash';
import Base from 'app/base';

export default class UserAccountController extends Base {
  /* @ngInject */
  constructor(
    $filter,
    $log,
    $mdMedia,
    AppSettings,
    NotificationService,
    PermissionsService,
    UserService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.notificationOptions = _.clone((this._AppSettings.NOTIFICATION_OPTIONS));
    this._getNotifications();
  }

  logout() {
    this._UserService.logout();
  }

  _getNotifications() {
    // - This merge 1 and 2 together in this function
    // - 1.A list of notifications from restangular
    // - 2.A list of user customisations to the notifications
    _.forEach(this.notificationTypes, (type) => {
      // - Check if the user has this notification saved
      const userNotificationSetting = _.find(this.currentUser.notifications,
        notification => notification.typeId === type.id);

      // - If this is defined then proceed to merge infomation
      // - This is the point to change isFavouritesOnly from boolean value to integer
      _.assign(type, {
        label: _.split(type.name, ' ').join('-').toLowerCase(),
        isEnabled: !!userNotificationSetting,
        isFavouritesOnly: userNotificationSetting ? +userNotificationSetting.favouritesOnly : 1,
      });
    });
  }

  async updateNotification(type) {
    // - Used by both the switch and the select element

    // - Action relates to the post/delete function
    const action = type.isEnabled ? 'set' : 'remove';

    // - notification.isEnabled has been automatically switch by the data-ng-model
    // - If switch is disabled, it will also reset and disable the select element
    if (type.isEnabled === false) {
      // - Disable the md-select when switch is disabled
      _.set(type, 'userOptionChanges', 1);
    }

    try {
      await this._NotificationService[`${action}Notification`](type);
    } catch (err) {
      this._$log.error(err);
    }
  }

  get userRoles() {
    const userRoles = [];
    _.forEach(this.currentUser.roles, (value) => {
      userRoles.push(this._$filter('translate')(`cd-role-${_.kebabCase(value)}`));
    });

    return userRoles.join(', ');
  }

  canViewNotifications() {
    return this._PermissionsService.canViewNotifications(this.currentUser);
  }
}
