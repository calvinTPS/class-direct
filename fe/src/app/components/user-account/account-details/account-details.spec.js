/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AccountDetailsComponent from './account-details.component';
import AccountDetailsController from './account-details.controller';
import AccountDetailsModule from './';
import AccountDetailsTemplate from './account-details.pug';
import AppSettings from 'app/config/project-variables.js';
import User from 'app/common/models/user/user';

describe('AccountDetails', () => {
  let $rootScope,
    $compile,
    mockPermissionsService,
    makeController;
  const mockUser = {
    userAccExpiryDate: '12/10/2016',
  };

  const mockTranslateFilter = value => value;
  beforeEach(window.module(
    AccountDetailsModule,
    { translateFilter: mockTranslateFilter },
  ));
  beforeEach(() => {
    mockPermissionsService = {
      canEditProfileOrPassword: () => { },
    };

    window.module(($provide) => {
      $provide.value('PermissionsService', mockPermissionsService);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = { user: mockUser }) => $componentController('accountDetails', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ user: mockUser });
      expect(controller.user).toEqual(mockUser);
    });

    describe('canEditProfileOrPassword', () => {
      it('delegates to PermissionsService', () => {
        const controller = makeController({ user: mockUser });
        spyOn(mockPermissionsService, 'canEditProfileOrPassword').and.returnValue(false);
        const res = controller.canEditProfileOrPassword;
        expect(mockPermissionsService.canEditProfileOrPassword).toHaveBeenCalledTimes(1);
        expect(res).toEqual(false);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AccountDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AccountDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AccountDetailsController);
    });
  });

  describe('Rendering', () => {
    let scope, element, userObj;
    beforeEach(() => {
      scope = $rootScope.$new();
      element = angular.element('<account-details user="user"><account-details/>');
      userObj = {
        userAccExpiryDate: 'USER_ACCOUNT_EXPIRY_DATE',
      };
      scope.user = new User(userObj);
      element = $compile(element)(scope);
      scope.$apply();
    });

    it('should show the user account expiry date', () => {
      expect(element.text().indexOf('USER_ACCOUNT_EXPIRY_DATE')).not.toEqual(-1);
    });
  });
});
