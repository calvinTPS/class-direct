import accountDetailsComponent from './account-details.component';
import angular from 'angular';
import infoCardModule from 'app/common/info-card';
import uiRouter from 'angular-ui-router';

export default angular.module('account-details', [
  uiRouter,
  infoCardModule,
])
.component('accountDetails', accountDetailsComponent)
.name;
