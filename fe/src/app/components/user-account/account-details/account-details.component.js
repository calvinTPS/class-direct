import './account-details.scss';
import controller from './account-details.controller';
import template from './account-details.pug';

export default {
  bindings: {
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
