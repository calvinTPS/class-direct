import Base from 'app/base';

export default class AccountDetailsController extends Base {
  /* @ngInject */
  constructor(
    PermissionsService,
  ) {
    super(arguments);
  }

  get canEditProfileOrPassword() {
    if (!this.user) return false;

    return this._PermissionsService.canEditProfileOrPassword(this.user);
  }
}
