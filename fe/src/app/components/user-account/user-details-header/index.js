import angular from 'angular';
import uiRouter from 'angular-ui-router';
import userDetailsHeaderComponent from './user-details-header.component';

export default angular.module('user-details-header', [
  uiRouter,
])
.component('userDetailsHeader', userDetailsHeaderComponent)
.name;
