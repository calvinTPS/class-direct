import * as _ from 'lodash';
import Base from 'app/base';
import statusConfirmDialogTemplate from './status-change-confirm.pug';
import User from 'app/common/models/user/user';

export default class UserDetailsHeaderController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    AppSettings,
    UserService,
  ) {
    super(arguments);
  }

  $onInit() {
    return this._UserService.getAccountStatuses()
      .then((accountStatuses) => {
        this.accountStatuses = accountStatuses.map(status => ({
          label: `cd-${status.name.toLowerCase()}`,
          id: status.id,
          name: status.name,
          class: `${status.name.toLowerCase()}`,
          order: status.id === this.user.status.id ? -1 :
            this._AppSettings.ACCOUNT_STATUS_ORDER[status.name],
        }));
      });
  }

  get currentStatusId() {
    return this.user.statusId;
  }

  set currentStatusId(id) {
    this.confirmChangeStatus(id).then(() => {
      const status = { id };
      this._UserService.editUser(this.user.userId, { status });
      this.user.statusId = id;

      // re-order status options list
      _.forEach(this.accountStatuses, (statusOption) => {
        statusOption.order = statusOption.id === this.user.statusId ? -1 :  // eslint-disable-line
          this._AppSettings.ACCOUNT_STATUS_ORDER[statusOption.name];
      });
    });
  }

  confirmChangeStatus(newStatusId) {
    return this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: true,
      controller: UserDetailsHeaderController,
      controllerAs: 'vm',
      locals: {
        title: 'cd-account-status',
        buttons: [
          {
            class: 'flex modal-cancel-button',
            label: 'cd-cancel',
            onClick: 'vm.cancelDialog()',
            theme: 'light',
          },
          {
            class: 'flex modal-confirm-button',
            label: 'cd-confirm',
            onClick: 'vm.hideDialog()',
            theme: 'warn',
          },
        ],
        newStatus: User.getStatus(newStatusId).name.toLowerCase(),
        oldStatus: User.getStatus(this.currentStatusId).name.toLowerCase(),
        user: this.user,
      },
      parent: this._$document.body,
      template: statusConfirmDialogTemplate,
    });
  }

  cancelDialog() {
    this._$mdDialog.cancel();
  }

  hideDialog() {
    this._$mdDialog.hide();
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
