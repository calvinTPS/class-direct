import './user-details-header.scss';
import controller from './user-details-header.controller';
import template from './user-details-header.pug';

export default {
  bindings: {
    user: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
