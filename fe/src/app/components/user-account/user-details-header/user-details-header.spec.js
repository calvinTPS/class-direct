/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import User from 'app/common/models/user/user';
import UserDetailsHeaderComponent from './user-details-header.component';
import UserDetailsHeaderController from './user-details-header.controller';
import UserDetailsHeaderModule from './';
import UserDetailsHeaderTemplate from './user-details-header.pug';

describe('UserDetailsHeader', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    $state,
    $window,
    makeController,
    UserService;

  const mockUser = new User({
    id: 1,
    userId: 101,
    equasis: true,
    thetis: false,
    flagCode: 100,
    shipBuilderCode: 1,
    clientCode: 4,
    name: 'John Doe',
    email: 'abc@bae.com',
    telephone: '07865432',
    city: 'WP',
    postCode: '50470',
    status: {
      id: 1,
      name: 'Active',
    },
    company: {
      name: 'BAE SYSTEMS APPLIED INTELLIGENCE',
      addressLine1: 'LEVEL 28',
      addressLine2: 'MENARA BINJAI',
      addressLine3: '2 JALAN BINJAI',
      city: 'Kuala Lumpur',
      state: 'WP',
      postCode: '50450',
      country: 'Malaysia',
      telephone: '321913000',
    },
  });

  beforeEach(window.module(
    UserDetailsHeaderModule,
    angularMaterial,
  ));

  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, _$window_, _$state_,
                        $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;
    $window = _$window_;
    $state = _$state_;

    UserService = {
      editUser: () => {},
      getAccountStatuses: () => Promise.resolve([
        { id: 1, name: 'Active' },
        { id: 2, name: 'Disabled' },
        { id: 3, name: 'Archived' },
      ]),
    };

    makeController =
      async (bindings = {}) => {
        const controller = $componentController('userDetailsHeader', { UserService, $state }, bindings);
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('initializes just fine', async () => {
      const controller = await makeController({ user: mockUser });
      expect(controller.user).toEqual(mockUser);
    });
  });

  describe('UserDetailsHeader.accountStatuses', () => {
    it.async('should list the account statuses in the correct order', async () => {
      const controller = await makeController({ user: mockUser });
      const statuses = controller.accountStatuses;
      expect(statuses.length).toEqual(3);
      expect(statuses[0].order).toEqual(-1); // active
      expect(statuses[1].order).toEqual(2); // disabled
      expect(statuses[2].order).toEqual(3); // archived
    });

    it.async('should have the current status as first on the list', async () => {
      mockUser.status.id = 3;
      const controller = await makeController({ user: mockUser });
      const statuses = controller.accountStatuses;
      expect(statuses[0].order).toEqual(1);  // active
      expect(statuses[1].order).toEqual(2);  // disabled
      expect(statuses[2].order).toEqual(-1); // archived
      mockUser.status.id = 1;
    });
  });

  describe('UserDetailsHeader.confirmChangeStatus()', () => {
    it.async('should $mdDialog.show', async () => {
      const controller = await makeController({ user: mockUser });
      spyOn($mdDialog, 'show');
      controller.confirmChangeStatus(3);

      expect($mdDialog.show).toHaveBeenCalled();
      expect($mdDialog.show.calls.mostRecent().args[0].locals.oldStatus).toEqual('active');
      expect($mdDialog.show.calls.mostRecent().args[0].locals.newStatus).toEqual('archived');
    });
  });

  describe('UserDetailsHeader.cancelDialog()', () => {
    it.async('should call $mdDialog.cancel', async () => {
      const controller = await makeController({ user: mockUser });
      spyOn($mdDialog, 'cancel');
      controller.cancelDialog();

      expect($mdDialog.cancel).toHaveBeenCalled();
    });
  });

  describe('UserDetailsHeader.hideDialog()', () => {
    it.async('should call $mdDialog.hide', async () => {
      const controller = await makeController({ user: mockUser });
      spyOn($mdDialog, 'hide');
      controller.hideDialog();

      expect($mdDialog.hide).toHaveBeenCalled();
    });
  });

  describe('userDetailsHeader set currentStatusId', () => {
    it.async('should call UserService.editUser with correct parameters', async () => {
      const controller = await makeController({ user: mockUser });
      spyOn(UserService, 'editUser');
      const promise = Promise.resolve();
      spyOn(controller, 'confirmChangeStatus').and.returnValue(promise);
      controller.currentStatusId = 2;
      await promise;

      expect(UserService.editUser).toHaveBeenCalledWith(101, { status: { id: 2 } });
    });

    it.async('should re-order status list so current status is first', async () => {
      const controller = await makeController({ user: mockUser });
      const promise = Promise.resolve();
      spyOn(controller, 'confirmChangeStatus').and.returnValue(promise);
      controller.currentStatusId = 2;
      await promise;

      expect(controller.accountStatuses[1].order).toEqual(-1);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = UserDetailsHeaderComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(UserDetailsHeaderTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(UserDetailsHeaderController);
    });
  });
});
