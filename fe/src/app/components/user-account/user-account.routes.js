/* eslint-disable no-param-reassign */

/* @ngInject */
export default function config(
  $stateProvider,
  $urlRouterProvider
) {
  $stateProvider
   .state('user', {
     url: '/user',
     component: 'userAccount',
     resolve: {
       /* @ngInject */
       currentUser: UserService => UserService.getCurrentUser(),

       /* @ngInject */
       notificationTypes:
        (
          AppSettings,
          NotificationService,
          $rootScope
        ) => ($rootScope.role_mode !== AppSettings.ROLES.EOR) ?
          NotificationService.getAllTypes() : null,
     },
   });
}
