/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AssetsComponent from './assets.component';
import AssetsController from './assets.controller';
import AssetsMocks from './assets.mocks.json';
import AssetsModule from './assets';
import AssetsTemplate from './assets.pug';

describe('Assets', () => {
  let $compile;
  let $rootScope;
  let makeController;
  let mockAssets;
  let mockUserService;
  let mockAssetService;
  let mockDashboardService;
  let mockRootScope;
  let mockViewService;

  beforeEach(window.module(AssetsModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    mockUserService = {
      getCurrentUser: () => Promise.resolve({}),
    };
    mockAssets = [
      { id: '0', selected: true },
      { id: '1', selected: false },
      { id: '2', selected: true },
      { id: '3', selected: false },
    ];
    mockAssets.pagination = { totalElements: 10 };

    mockAssetService = {
      updateFavourites: () => Promise.resolve({}),
    };

    mockDashboardService = {
      submitQuery: () => Promise.resolve({}),
    };

    mockViewService = {
      getVesselListViewType: () => true,
    };

    window.module(($provide) => {
      $provide.value('$mdMedia', {});
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('UserService', mockUserService);
      $provide.value('AssetService', mockAssetService);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('ViewService', mockViewService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('assets', { $dep: dep }, bindings);
        controller._$rootScope = $rootScope;
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('should get the right MODES', () => {
      const controller = makeController();
      expect(controller.MODES).toEqual({
        NORMAL: 'normal',
        SUMMARY: 'summary',
        EXPORT: 'export',
        EOR: 'eor',
      });
    });

    it('miniCardFlex getter should return the correct integer value', () => {
      const controller = makeController();
      expect(controller.miniCardFlex).toEqual(25);

      controller.mode = 'export';
      expect(controller.miniCardFlex).toEqual(33);
    });

    it('includeAll getter/setter should return with the right values and set all asset.selected = true', () => {
      const controller = makeController({ assets: mockAssets });
      controller.includeAll = true;
      _.forEach(controller.assets, (asset) => {
        expect(asset.selected).toEqual(true);
      });

      controller.includeAll = false;
      _.forEach(controller.assets, (asset) => {
        expect(asset.selected).toEqual(false);
      });

      expect(controller.selectedIds).toEqual({
        includedAssets: [],
        excludedAssets: [],
      });
    });

    it('includedCount should return the right number', () => {
      const controller = makeController({ assets: mockAssets });
      controller.includeAll = true;
      expect(controller.includedCount).toEqual(10);

      controller.assets[0].selected = false;
      expect(controller.includedCount).toEqual(9);

      controller.assets[1].selected = false;
      expect(controller.includedCount).toEqual(8);

      controller.includeAll = false;
      expect(controller.includedCount).toEqual(0);

      controller.assets[0].selected = true;
      expect(controller.includedCount).toEqual(1);

      controller.assets[1].selected = true;
      expect(controller.includedCount).toEqual(2);
    });

    it('_getIncludedIds() should return an array with the right ids', () => {
      const controller = makeController({ assets: mockAssets });
      expect(controller._getIncludedIds()).toEqual(['0', '2']);
    });

    it('_getExcludedIds() should return an array with the right ids', () => {
      const controller = makeController({ assets: mockAssets });
      expect(controller._getExcludedIds()).toEqual(['1', '3']);
    });

    it('selectedIds should return with the right object', () => {
      const controller = makeController({ assets: mockAssets });
      controller.selectedIds = undefined;

      controller.includeAll = true;
      controller.assets[1].selected = false;
      controller.assets[3].selected = false;
      expect(controller.selectedIds).toEqual({
        includedAssets: [],
        excludedAssets: ['1', '3'],
      });

      controller.includeAll = false;
      controller.assets[0].selected = true;
      controller.assets[2].selected = true;
      expect(controller.selectedIds).toEqual({
        includedAssets: ['0', '2'],
        excludedAssets: [],
      });
    });

    it('isShowActionBar(), correctly returns true/false', () => {
      const controller = makeController({ assets: mockAssets });

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isShowActionBar).toEqual(false);

      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isShowActionBar).toEqual(true);

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = { LRV1: 'LRV1' };
      expect(controller.isShowActionBar).toEqual(true);
    });

    it('isFavouriteAsset(), correctly returns whether an asset is in the favourites list', () => {
      const controller = makeController({ assets: mockAssets });

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isFavouriteAsset('LRV1')).toBeUndefined();

      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = {};
      expect(controller.isFavouriteAsset('LRV1')).toEqual(true);

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = { LRV1: 'LRV1' };
      expect(controller.isFavouriteAsset('LRV1')).toEqual(false);
    });

    it('saveFavourites(), correctly saves the selected assets as favourites', () => {
      const controller = makeController({ assets: mockAssets });
      spyOn(controller._$rootScope, '$broadcast');
      spyOn(controller._AssetService, 'updateFavourites');
      spyOn(controller._DashboardService, 'submitQuery');

      controller._DashboardService.favourites.toAdd = {};
      controller._DashboardService.favourites.toRemove = {};

      controller.saveFavourites();
      expect(controller._$rootScope.$broadcast).not.toHaveBeenCalled();
      expect(controller._AssetService.updateFavourites).not.toHaveBeenCalled();
      expect(controller._DashboardService.submitQuery).not.toHaveBeenCalled();

      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = { LRV2: 'LRV2' };

      controller.saveFavourites();
      expect(controller.isSaving).toEqual(true);
      expect(controller._$rootScope.$broadcast).toHaveBeenCalled();
      expect(controller._AssetService.updateFavourites).toHaveBeenCalled();
    });

    it('cancelSaveFavourites(), correctly reverts the favourites state to blank/empty', () => {
      const controller = makeController({ assets: mockAssets });
      spyOn(controller._$rootScope, '$broadcast');
      controller._DashboardService.favourites.toAdd = { LRV1: 'LRV1' };
      controller._DashboardService.favourites.toRemove = { LRV1: 'LRV1' };

      controller.cancelSaveFavourites();
      expect(controller._DashboardService.favourites.toAdd).toEqual({});
      expect(controller._DashboardService.favourites.toRemove).toEqual({});
      // - At this state, the app is not in favourites mode and user can switch between views
      expect(controller._$rootScope.inFavouritesMode).toBe(false);
      expect(controller._$rootScope.$broadcast).toHaveBeenCalled();
    });
  });

  describe('Rendering', () => {
    let element;

    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<assets assets-collections="assets"></assets>');
      element = $compile(element)(scope);
      scope.assets = AssetsMocks;
      scope.$apply();
    });

    it('should render the pagination directive', () => {
      expect(element.find('pagination')).toBeDefined();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetsController);
    });
  });
});
