import angular from 'angular';
import assetsComponent from './assets.component';
import uiRouter from 'angular-ui-router';

export default angular.module('assets', [
  uiRouter,
])
.component('assets', assetsComponent)
.name;
