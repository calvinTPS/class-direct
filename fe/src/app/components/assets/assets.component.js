import './assets.scss';
import controller from './assets.controller';
import template from './assets.pug';

export default {
  controller,
  controllerAs: 'vm',
  bindings: {
    assets: '<',
    includeAll: '=?',
    includedCount: '=?',
    isEditable: '<?',
    isPristine: '=?',
    mode: '@',
    notification: '@',
    selectedIds: '=?',
    showEditButton: '=?',
    showSpinner: '<?',
    showTopControl: '<?',
    theme: '@?',
  },
  restrict: 'E',
  template,
};
