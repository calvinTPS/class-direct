import angular from 'angular';
import assetComponent from './asset.component';
import MPMS from './mpms';
import RiderFilter from './rider-filter';
import routes from './asset.routes';
import SurveyPlanner from './survey-planner';
import uiRouter from 'angular-ui-router';

export default angular.module('asset', [
  MPMS,
  RiderFilter,
  SurveyPlanner,
  uiRouter,
])
.config(routes)
.component('asset', assetComponent)
.name;
