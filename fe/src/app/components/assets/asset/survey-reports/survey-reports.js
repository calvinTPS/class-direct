import angular from 'angular';
import routes from './survey-reports.routes';
import surveyReportsComponent from './survey-reports.component';
import uiRouter from 'angular-ui-router';

export default angular.module('surveyReports', [
  uiRouter,
])
.config(routes)
.component('surveyReports', surveyReportsComponent)
.name;
