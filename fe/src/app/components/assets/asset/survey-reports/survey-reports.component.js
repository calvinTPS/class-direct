import './survey-reports.scss';
import controller from './survey-reports.controller';
import template from './survey-reports.pug';

export default {
  bindings: {
    asset: '<',
    reports: '<',
  },
  controllerAs: 'vm',
  controller,
  restrict: 'E',
  template,
};
