/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import SurveyReportsComponent from './survey-reports.component';
import SurveyReportsController from './survey-reports.controller';
import SurveyReportsModule from './survey-reports';
import SurveyReportsTemplate from './survey-reports.pug';

describe('SurveyReports', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(SurveyReportsModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('surveyReports', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('has a name property', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SurveyReportsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SurveyReportsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SurveyReportsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<survey-reports foo="bar"><survey-reports/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('SurveyReports');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
