import AppSettings from 'app/config/project-variables.js';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
  .state('surveyReports', {
    url: '/assets/{assetId: string}/survey-reports',
    component: 'surveyReports',
    resolve: {
      /* @ngInject */
      asset: ($stateParams, AssetService) =>
       AssetService.getOne($stateParams.assetId),

      /* @ngInject */
      reports: ($stateParams, AssetService) =>
       AssetService.get(
         $stateParams.assetId,
         AppSettings.API_ENDPOINTS.SurveyReports,
         { size: 4 }
       ),
    },
  });
}
