/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import AssetComponent from './asset.component';
import AssetController from './asset.controller';
import AssetModule from './asset';
import AssetTemplate from './asset.pug';

describe('Asset', () => {
  let $rootScope,
    $compile,
    $injector,
    $mdMedia,
    $state,
    $stateParams,
    makeController;

  const currentUser = { model: { roles: [{ name: 'CLIENT' }] } };
  const asset = { isEORAsset: false };

  const mockPromise = {
    then: () => currentUser,
    catch: () => {},
  };

  const mockUserService = {
    getCurrentUser() {
      return mockPromise;
    },
  };

  const mockAssetService = {
    get: assetId => Promise.resolve([]),
    getOne: assetId => mockPromise,
    getDetails: assetId => Promise.resolve([]),
    getEORDetails: assetId => Promise.resolve([]),
    getEORAssetByAssetId: assetId => Promise.resolve([]),
    getServiceScheduleRange: assetId => Promise.resolve([]),
    getPMSableTasks: assetId => Promise.resolve([]),
    getSurveyReports: assetId => Promise.resolve([]),
  };

  const mockViewService = {
    isServiceScheduleOnDetailMode: false,
    isAssetHeaderVisible: false,
  };

  const mockCodicilService = {
    get: assetId => Promise.resolve([]),
    getOne: assetId => Promise.resolve([]),
  };

  const mockDefectService = {
    getOne: assetId => Promise.resolve([]),
  };

  const mockServiceService = {
    getAll: assetId => Promise.resolve([]),
  };

  const mockTermsAndConditionsService = {
    hasEquasisThetisAcceptedTC: () => {},
    setEquasisThetisAcceptedTC: () => {},
  };

  beforeEach(window.module(
    angularMaterial,
    AssetModule,
  ));
  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('UserService', mockUserService);
      $provide.value('AssetService', mockAssetService);
      $provide.value('CodicilService', mockCodicilService);
      $provide.value('DefectService', mockDefectService);
      $provide.value('ServiceService', mockServiceService);
      $provide.value('ViewService', mockViewService);
      $provide.value('currentUser', currentUser);
      $provide.value('asset', asset);
      $provide.value('TermsAndConditionsService', mockTermsAndConditionsService);
    });
  });

  beforeEach(inject(
    (
      _$rootScope_,
      _$compile_,
      _$injector_,
      _$mdMedia_,
      _$state_,
      _$stateParams_,
      $componentController
    ) => {
      $rootScope = _$rootScope_;
      $compile = _$compile_;
      $injector = _$injector_;
      $mdMedia = _$mdMedia_;
      $state = _$state_;
      $stateParams = _$stateParams_;

      makeController =
      (bindings = {}) => $componentController('asset', {
        $stateParams,
        AssetService: mockAssetService,
      }, bindings);
    }));

  describe('Routes', () => {
    it.async('base asset resolves currentUser', async() => {
      spyOn(mockUserService, 'getCurrentUser').and.callThrough();
      const currentUserObject = $state.get('asset').resolve.currentUser;
      const result = await $injector.invoke(currentUserObject);
      expect(mockUserService.getCurrentUser).toHaveBeenCalled();
    });

    it('base asset resolves asset', () => {
      spyOn(mockAssetService, 'getOne').and.returnValue(mockPromise);

      const result = $injector.invoke(
        $state.get('asset').resolve.asset
      );
      expect(mockAssetService.getOne).toHaveBeenCalled();
    });

    it.async('asset surveyRequest resolves assetServices', async() => {
      $stateParams.assetId = 1;
      spyOn(mockServiceService, 'getAll');

      await $injector.invoke(
        $state.get('asset.surveyRequest').resolve.assetServices
      );
      expect(mockServiceService.getAll).toHaveBeenCalledTimes(1);
      expect(mockServiceService.getAll).toHaveBeenCalledWith(1);
    });

    it.async('asset details resolves details', async() => {
      $stateParams.assetId = 1;
      asset.isEORAsset = false;
      spyOn(mockAssetService, 'getDetails');

      await $injector.invoke(
        $state.get('asset.details').resolve.details
      );
      expect(mockAssetService.getDetails).toHaveBeenCalledTimes(1);
      expect(mockAssetService.getDetails).toHaveBeenCalledWith(1);
    });

    it.async('eor asset details resolve details', async() => {
      $stateParams.assetId = 1;
      spyOn(mockAssetService, 'getEORDetails');

      asset.isEORAsset = true;
      await $injector.invoke(
        $state.get('asset.details').resolve.details
      );
      expect(mockAssetService.getEORDetails).toHaveBeenCalledTimes(1);
      expect(mockAssetService.getEORDetails).toHaveBeenCalledWith(1);
    });

    it.async('asset conditionsOfClassDetail resolves item', async() => {
      $stateParams.assetId = 1;
      $stateParams.codicilId = 1;
      spyOn(mockCodicilService, 'getOne');

      await $injector.invoke(
        $state.get('asset.conditionsOfClassDetail').resolve.item
      );
      expect(mockCodicilService.getOne).toHaveBeenCalledTimes(1);
      expect(mockCodicilService.getOne).toHaveBeenCalledWith(1, 'cocs', 1);
    });

    it.async('asset assetNotesDetail resolves item', async() => {
      $stateParams.assetId = 1;
      $stateParams.codicilId = 1;
      spyOn(mockCodicilService, 'getOne');

      await $injector.invoke(
        $state.get('asset.assetNotesDetail').resolve.item
      );
      expect(mockCodicilService.getOne).toHaveBeenCalledTimes(1);
      expect(mockCodicilService.getOne).toHaveBeenCalledWith(1, 'asset-notes', 1);
    });

    it.async('asset actionableItemsDetail resolves item', async() => {
      $stateParams.assetId = 1;
      $stateParams.codicilId = 1;
      spyOn(mockCodicilService, 'getOne');

      await $injector.invoke(
        $state.get('asset.actionableItemsDetail').resolve.item
      );
      expect(mockCodicilService.getOne).toHaveBeenCalledTimes(1);
      expect(mockCodicilService.getOne).toHaveBeenCalledWith(1, 'actionable-items', 1);
    });

    it.async('asset defectsDetail resolves item', async() => {
      $stateParams.assetId = 1;
      $stateParams.defectId = 1;
      spyOn(mockDefectService, 'getOne');

      await $injector.invoke(
        $state.get('asset.defectsDetail').resolve.item
      );
      expect(mockDefectService.getOne).toHaveBeenCalledTimes(1);
      expect(mockDefectService.getOne).toHaveBeenCalledWith(1, 'defects', 1);
    });

    it.async('asset statutoryDeficienciesDetail resolves item', async() => {
      $stateParams.assetId = 1;
      $stateParams.deficiencyId = 1;
      spyOn(mockDefectService, 'getOne');

      await $injector.invoke(
        $state.get('asset.statutoryDeficienciesDetail').resolve.item
      );
      expect(mockDefectService.getOne).toHaveBeenCalledTimes(1);
      expect(mockDefectService.getOne).toHaveBeenCalledWith(1, 'deficiencies', 1);
    });
  });

  describe('Controller', () => {
    it('has a name property', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('isHeaderVisible sets to true the first time', () => {
      const controller = makeController({ foo: 'bar' });
      mockViewService.isAssetHeaderVisible = false;
      controller.isHeaderVisible = false;
      expect(mockViewService.isAssetHeaderVisible).toEqual(true);
      controller.isHeaderVisible = false;
      expect(mockViewService.isAssetHeaderVisible).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetController);
    });
  });
});
