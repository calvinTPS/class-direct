import './top-services.scss';
import controller from './top-services.controller';
import template from './top-services.pug';

export default {
  bindings: {
    services: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
