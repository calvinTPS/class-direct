import _ from 'lodash';
import Base from 'app/base';

export default class JobsListController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $stateParams,
    AppSettings,
    ViewService
  ) {
    super(arguments);
  }

  $onInit() {
    this.jobItemTemplate = this._AppSettings.RIDER_TEMPLATES.JOB;
    this.searchByList = this._AppSettings.JOBS_SEARCH_BY_LIST;
    this.sortByList = this._AppSettings.JOBS_SORT_BY_LIST;
    // Mutated by rider filter.
    this.filterOptions = _.cloneDeep(this._AppSettings.JOBS_FILTER_OPTIONS);
    this.action = {
      stateName: this._AppSettings.STATES.SERVICE_HISTORY_JOB_DETAILS,
      evalParams: { jobId: 'id' },
      params: { assetId: this._$stateParams.assetId },
    };
  }
}
