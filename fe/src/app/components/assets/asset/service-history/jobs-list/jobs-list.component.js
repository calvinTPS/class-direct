import './jobs-list.scss';
import controller from './jobs-list.controller';
import template from './jobs-list.pug';

export default {
  bindings: {
    jobs: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
