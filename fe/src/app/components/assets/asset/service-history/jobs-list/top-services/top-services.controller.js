import Base from 'app/base';

export default class TopServicesController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
  }

  get shownServicesCount() {
    if (this._shownServicesCount == null) {
      this._shownServicesCount =
        Math.min(this._AppSettings.JOB.TOP_SERVICES_TO_SHOW, this.totalServicesCount);
    }
    return this._shownServicesCount;
  }

  get totalServicesCount() {
    if (this._totalServicesCount == null) {
      this._totalServicesCount = this.services ? this.services.length : 0;
    }
    return this._totalServicesCount;
  }

  get showXOfY() {
    if (this._showXOfY == null) {
      this._showXOfY = this.totalServicesCount > this._AppSettings.JOB.TOP_SERVICES_TO_SHOW;
    }
    return this._showXOfY;
  }
}
