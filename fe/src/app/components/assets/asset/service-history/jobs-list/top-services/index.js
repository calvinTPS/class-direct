import angular from 'angular';
import TopServicesComponent from './top-services.component';
import uiRouter from 'angular-ui-router';

export default angular.module('topServices', [
  uiRouter,
])
.component('topServices', TopServicesComponent)
.name;
