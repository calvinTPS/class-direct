import angular from 'angular';
import JobDetails from '../job-details';
import JobsList from './jobs-list.component';
import TopServices from './top-services';
import uiRouter from 'angular-ui-router';

export default angular.module('jobsList', [
  JobDetails,
  TopServices,
  uiRouter,
])
.component('jobsList', JobsList)
.name;
