/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import TopServicesComponent from './top-services.component';
import TopServicesController from './top-services.controller';
import TopServicesModule from './';
import TopServicesTemplate from './top-services.pug';

describe('TopServices', () => {
  let $rootScope,
    $compile,
    makeController;

  const fakeServices = [
    { id: 1, name: 'Service 1' },
    { id: 2, name: 'Service 2' },
    { id: 3, name: 'Service 3' },
    { id: 4, name: 'Service 4' },
    { id: 5, name: 'Service 5' },
  ];

  beforeEach(window.module(TopServicesModule));
  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('topServices', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    let controller2;
    let controller3;

    beforeEach(() => {
      controller = makeController({ services: _.take(fakeServices, 2) });
      controller2 = makeController({ services: _.take(fakeServices, 3) });
      controller3 = makeController({ services: _.take(fakeServices, 4) });
    });

    describe('shownServicesCount', () => {
      it('shows the number of services shown on screen', () => {
        expect(controller.shownServicesCount).toEqual(2);
        expect(controller2.shownServicesCount).toEqual(3);
        expect(controller3.shownServicesCount).toEqual(3); // limited to configurable number
      });
    });

    describe('totalServicesCount', () => {
      it('shows the total number of services', () => {
        expect(controller.totalServicesCount).toEqual(2);
        expect(controller2.totalServicesCount).toEqual(3);
        expect(controller3.totalServicesCount).toEqual(4);
      });
    });

    describe('showXOfY', () => {
      it('returns true if we should show X of Y', () => {
        expect(controller.showXOfY).toEqual(false);
        expect(controller2.showXOfY).toEqual(false);
        expect(controller3.showXOfY).toEqual(true);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = TopServicesComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(TopServicesTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(TopServicesController);
    });
  });
});
