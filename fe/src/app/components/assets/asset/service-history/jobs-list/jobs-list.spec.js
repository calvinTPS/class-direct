/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import JobsListComponent from './jobs-list.component';
import JobsListController from './jobs-list.controller';
import JobsListModule from './';
import JobsListTemplate from './jobs-list.pug';

describe('JobsList', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    makeController;
  const mockViewService = { registerTemplate: (x, y) => {} };
  beforeEach(window.module(
    JobsListModule,
    angularMaterial,
  ));
  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
    $provide.value('ViewService', mockViewService);
  }));
  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('jobsList', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = JobsListComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(JobsListTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(JobsListController);
    });
  });
});
