/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import AssetModule from '../asset';
import ServiceHistoryComponent from './service-history.component';
import ServiceHistoryController from './service-history.controller';
import ServiceHistoryModule from './';
import ServiceHistoryTemplate from './service-history.pug';

describe('ServiceHistory', () => {
  let $rootScope,
    $compile,
    $injector,
    $state,
    $stateParams,
    makeController;

  const mockAssetService = {
    getOne: () => Promise.resolve([]),
  };

  const mockJobService = {
    getJobs: () => Promise.resolve([]),
    getJob: () => Promise.resolve({}),
  };

  const fakeJobs = [{ id: 1, jobNumber: 1, leaderSurveyorName: 'surveyor1' },
                    { id: 2, jobNumber: 2, leaderSurveyorName: 'surveyor2' }];

  beforeEach(window.module(
    AssetModule,
    ServiceHistoryModule,
  ));

  beforeEach(window.module(($provide) => {
    $provide.value('AssetService', mockAssetService);
    $provide.value('JobService', mockJobService);
    $provide.value('jobs', fakeJobs);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, _$injector_, _$state_, _$stateParams_,
                     $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $injector = _$injector_;
    $state = _$state_;
    $stateParams = _$stateParams_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('serviceHistory', {
        AppSettings,
        $state,
        $stateParams,
        AssetService: mockAssetService,
        JobService: mockJobService,
      }, bindings);
  }));

  describe('serviceHistory route', () => {
    beforeEach(() => {
      $stateParams.assetId = 1;
      const fakeAsset = { id: 1, name: 'Ship' };
      spyOn(mockAssetService, 'getOne').and.returnValue(Promise.resolve(fakeAsset));
    });

    it.async('should resolve jobs for an asset', async() => {
      spyOn(mockJobService, 'getJobs').and.returnValue(Promise.resolve(fakeJobs));
      const jobs = await $injector.invoke($state.get('asset.jobsList').resolve.jobs);
      expect(mockJobService.getJobs).toHaveBeenCalledWith(1);
      expect(jobs).toEqual(fakeJobs);
    });

    it.async('should resolve a single job', async() => {
      $stateParams.jobId = 1;
      spyOn(mockJobService, 'getJob').and.returnValue(Promise.resolve(fakeJobs[0]));
      spyOn(mockJobService, 'getJobs').and.returnValue(Promise.resolve(fakeJobs));
      const job = await $injector.invoke($state.get('asset.jobDetails').resolve.job);
      expect(mockJobService.getJob).toHaveBeenCalledWith(1);
      expect(mockJobService.getJobs).not.toHaveBeenCalled();
      expect(job).toEqual(fakeJobs[0]);
    });
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ServiceHistoryComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ServiceHistoryTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ServiceHistoryController);
    });
  });
});
