import './service-history.scss';
import controller from './service-history.controller';
import template from './service-history.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
