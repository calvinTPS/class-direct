/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import JobDetailsComponent from './job-details.component';
import JobDetailsController from './job-details.controller';
import JobDetailsModule from './';
import JobDetailsTemplate from './job-details.pug';

describe('JobDetails', () => {
  let $rootScope,
    $compile,
    $mdMedia,
    makeController;

  beforeEach(window.module(
    JobDetailsModule,
    angularMaterial,
  ));

  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));

  beforeEach(inject((_$rootScope_, _$compile_, _$mdMedia_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdMedia = _$mdMedia_;

    const mockJobService = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('jobDetails', { JobService: mockJobService }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const mockReports = [1, 2, 3];
      const ehsAttachment = { id: 7, name: 'Executive Hull Summary', shortName: 'Executive Hull Summary' };
      const mockAttachments = [
        { id: 4, shortName: 'TM' },
        { id: 5, shortName: 'TM' },
        { id: 6, shortName: 'XX' },
        { id: 7, shortName: 'YY' },
        ehsAttachment,
      ];
      const controller = makeController({
        _$stateParams: { jobId: 100 },
        jobReports: mockReports,
        jobAttachments: mockAttachments,
      });

      expect(controller.jobReports).toEqual(mockReports);
      expect(controller.jobAttachments).toEqual(mockAttachments);
      expect(controller.jobEhsAttachments).toEqual([ehsAttachment]);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = JobDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(JobDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(JobDetailsController);
    });
  });
});
