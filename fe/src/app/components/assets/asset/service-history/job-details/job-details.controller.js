import * as _ from 'lodash';
import Base from 'app/base';

export default class JobDetailsController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $stateParams,
    AppSettings,
  ) {
    super(arguments);
  }

  $onInit() {
    this.jobServiceItemTpl = this._AppSettings.RIDER_TEMPLATES.JOB_DETAILS;
    this.jobEhsAttachments =
      _.filter(this.jobAttachments, a => a.name === this._AppSettings.ATTACHMENTS.EHS.Name);
  }
}
