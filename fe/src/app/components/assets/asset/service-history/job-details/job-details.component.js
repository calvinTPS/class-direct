import './job-details.scss';
import controller from './job-details.controller';
import template from './job-details.pug';

export default {
  bindings: {
    job: '<',
    jobAttachments: '<',
    jobReports: '<',
    jobServices: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
