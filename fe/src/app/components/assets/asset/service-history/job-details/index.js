import angular from 'angular';
import JobDetails from './job-details.component';
import uiRouter from 'angular-ui-router';

export default angular.module('jobDetails', [
  uiRouter,
])
.component('jobDetails', JobDetails)
.name;
