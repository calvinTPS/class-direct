import angular from 'angular';
import JobDetails from './job-details';
import JobsList from './jobs-list';
import routes from './service-history.routes';
import ServiceHistory from './service-history.component';

import uiRouter from 'angular-ui-router';

export default angular.module('serviceHistory', [
  JobDetails,
  JobsList,
  uiRouter,
])
.config(routes)
.component('serviceHistory', ServiceHistory)
.name;
