/* @ngInject */
export default function config(
  $stateProvider,
) {
  $stateProvider
    .state('asset.jobsList', {
      url: '/survey-history/jobs',
      views: {
        'asset-view': {
          component: 'jobsList',
        },
      },
      resolve: {
        /* @ngInject */
        jobs: ($stateParams, JobService) => JobService.getJobs($stateParams.assetId),
      },
    })

    .state('asset.jobDetails', {
      url: '/survey-history/jobs/:jobId',
      views: {
        'asset-view': {
          component: 'jobDetails',
        },
      },
      resolve: {
        /* @ngInject */
        job: ($stateParams, JobService) => JobService.getJob($stateParams.jobId),
        /* @ngInject */
        jobReports: ($stateParams, JobService) => JobService.getJobReports($stateParams.jobId),
        /* @ngInject */
        jobAttachments: ($stateParams, JobService) =>
          JobService.getJobAttachments($stateParams.jobId),
      },
    })

    .state('asset.executiveHullSummaries', {
      url: '/survey-history/jobs/:jobId/executive-hull-summaries',
      views: {
        'asset-view': {
          component: 'executiveHullSummaries',
        },
      },
      resolve: {
        /* @ngInject */
        jobEhsAttachments: ($stateParams, AppSettings, JobService) =>
          JobService.getJobAttachments($stateParams.jobId,
            AppSettings.ATTACHMENTS.EHS.Name),
      },
    });
}
