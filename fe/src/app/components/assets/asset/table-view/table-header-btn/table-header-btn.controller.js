import Base from 'app/base';

export default class TableHeaderBtnController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    AppSettings,
    DashboardService,
    ViewService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    if (!this.viewId) {
      throw new Error('Please provide a viewId for to the table-header-btn');
    }
  }

  async sortData() {
    if (this.isFocused &&
        this._DashboardService.queryParams.order === TableHeaderBtnController.SORT_ORDER.ASC) {
      this._sortOrder = TableHeaderBtnController.SORT_ORDER.DESC;
    } else {
      this._sortOrder = TableHeaderBtnController.SORT_ORDER.ASC;
    }

    this._ViewService.focusedTableHeaderId = this.viewId;

    this._DashboardService.queryParams = {
      sort: this.viewId,
      order: this._sortOrder,
    };
    this._DashboardService.setSearchAssetsSelectedSortType(this.viewId);
    await this._DashboardService.submitQuery();

    this._$scope.$apply();
  }

  get headerIcon() {
    if (this._DashboardService.queryParams.order === TableHeaderBtnController.SORT_ORDER.ASC) {
      return 'arrowUp';
    }
    return 'arrowDown';
  }

  get isFocused() {
    return this._ViewService.focusedTableHeaderId === this.viewId;
  }

  static SORT_ORDER = {
    ASC: 'ASC',
    DESC: 'DESC',
  };
}
