import angular from 'angular';
import TableHeaderBtnComponent from './table-header-btn.component';

export default angular.module('tableHeaderBtn', [])
.component('tableHeaderBtn', TableHeaderBtnComponent)
.name;
