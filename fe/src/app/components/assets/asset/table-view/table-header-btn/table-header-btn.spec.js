/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, max-len,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import TableHeaderBtnComponent from './table-header-btn.component';
import TableHeaderBtnController from './table-header-btn.controller';
import TableHeaderBtnModule from './';
import TableHeaderBtnTemplate from './table-header-btn.pug';

describe('TableHeaderBtn', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    $mdMedia,
    makeController,
    mockDashboardService,
    mockViewService;

  beforeEach(window.module(
    angularMaterial,
    TableHeaderBtnModule,
  ));
  beforeEach(() => {
    mockDashboardService = {
      submitQuery: () => { },
      setSearchAssetsSelectedSortType: (x) => { },
      queryParams: { sort: 'Foo', order: 'Bar' },
    };

    mockViewService = {
      focusedTableHeaderId: 'Foo',
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('ViewService', mockViewService);
      $provide.value('safeTranslateFilter', value => value);
    });
  });

  beforeEach(inject(($componentController, _$rootScope_, _$mdDialog_, _$compile_, _$mdMedia_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $mdDialog = _$mdDialog_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('tableHeaderBtn', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ viewId: 'bar' });
      expect(controller.viewId).toEqual('bar');
    });

    it.async('sortData() should update queryOptions and call DasboardService.submitQuery', async () => {
      const controller = makeController({ viewId: 'Foo' });
      spyOn(mockDashboardService, 'submitQuery');

      await controller.sortData();
      expect(controller.headerIcon).toEqual('arrowUp');

      await controller.sortData();
      expect(controller.headerIcon).toEqual('arrowDown');

      expect(controller.isFocused).toBeTruthy();
      expect(mockDashboardService.submitQuery).toHaveBeenCalled();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = TableHeaderBtnComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(TableHeaderBtnTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(TableHeaderBtnController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<table-header-btn foo="bar" view-id="foo"><table-header-btn/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('TableHeaderBtn');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
