import './table-header-btn.scss';
import controller from './table-header-btn.controller';
import template from './table-header-btn.pug';

export default {
  bindings: {
    label: '@',
    viewId: '@?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
