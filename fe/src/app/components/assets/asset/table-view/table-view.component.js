import './table-view.scss';
import controller from './table-view.controller';
import template from './table-view.pug';

export default {
  bindings: {
    assets: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  template,
};
