import angular from 'angular';
import TableViewComponent from './table-view.component';

export default angular.module('tableView', [])
.component('tableView', TableViewComponent)
.name;
