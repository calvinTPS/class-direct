/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, max-len,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import TableViewComponent from './table-view.component';
import TableViewController from './table-view.controller';
import TableViewModule from './';
import TableViewTemplate from './table-view.pug';

describe('TableView', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    $mdMedia,
    makeController;

  beforeEach(window.module(
    angularMaterial,
    TableViewModule,
  ));
  beforeEach(() => {
    window.module(($provide) => {
      const mockTranslateFilter = value => value;
    });
  });

  beforeEach(inject(($componentController, _$rootScope_, _$mdDialog_, _$compile_, _$mdMedia_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $mdDialog = _$mdDialog_;
    $mdMedia = _$mdMedia_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('tableView', { $dep: dep }, bindings);
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = TableViewComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(TableViewTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(TableViewController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<table-view foo="bar"><table-view/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('TableView');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
