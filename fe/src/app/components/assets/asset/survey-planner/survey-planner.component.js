import './survey-planner.scss';
import controller from './survey-planner.controller';
import template from './survey-planner.pug';

export default {
  bindings: {
    asset: '<',
    serviceScheduleCodicils: '<',
    serviceScheduleDateRange: '<',
    serviceScheduleServices: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
