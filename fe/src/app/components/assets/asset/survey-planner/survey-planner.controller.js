import Base from 'app/base';

export default class SurveyPlannerController extends Base {
  /* @ngInject */
  constructor(
    $state,
    AppSettings,
    ViewService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this._ViewService.showServiceScheduleDetailedMode();

    if (!this.asset.isAccessibleMASTAsset) {
      this._$state.go(this._AppSettings.STATES.ERROR, { statusCode: 401 });
    }
  }

  $onDestroy() {
    this._ViewService.dismissServiceScheduleDetailedMode();
  }
}
