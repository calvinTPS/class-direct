import angular from 'angular';
import SurveyPlannerComponent from './survey-planner.component';
import uiRouter from 'angular-ui-router';

export default angular.module('surveyPlanner', [
  uiRouter,
])
.component('surveyPlanner', SurveyPlannerComponent)
.name;
