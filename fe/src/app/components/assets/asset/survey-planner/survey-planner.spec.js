/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import SurveyPlannerComponent from './survey-planner.component';
import SurveyPlannerController from './survey-planner.controller';
import SurveyPlannerModule from './';
import SurveyPlannerTemplate from './survey-planner.pug';

describe('SurveyPlanner', () => {
  let $rootScope,
    $compile,
    makeController,
    mockViewService;

  beforeEach(window.module(SurveyPlannerModule));
  beforeEach(() => {
    mockViewService = {
      showServiceScheduleDetailedMode: () => {},
      dismissServiceScheduleDetailedMode: () => {},
    };

    window.module(($provide) => {
      $provide.value('ViewService', mockViewService);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('surveyPlanner', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ asset: {} });
      expect(controller.asset).toEqual({});
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SurveyPlannerComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SurveyPlannerTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SurveyPlannerController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<survey-planner asset="inject"><survey-planner/>');
      scope.inject = {};
      scope.$apply();
      element = $compile(element)(scope);

      controller = element.controller('SurveyPlanner');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
