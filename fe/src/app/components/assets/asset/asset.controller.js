import Base from 'app/base';

export default class AssetController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $state,
    AppSettings,
    ViewService,
  ) {
    super(arguments);
    this._ViewService.asset = this.asset;
  }

  get inViewOptions() {
    return {
      offset: -100, // make room for system header.
    };
  }

  set isHeaderVisible(inview) {
    // LRCD-2688 in-view directive fails to detect scroll
    // changes on transition to asset page.
    // The first time it sets inview as false, which is not correct.
    // Force inview to be true the first time.
    if (this._isHeaderVisibleSet == null &&
      this._$state.current.name !== this._AppSettings.STATES.ASSET_SURVEY_PLANNER) {
      this._ViewService.isAssetHeaderVisible = true;
      this._isHeaderVisibleSet = true;
    } else {
      this._ViewService.isAssetHeaderVisible = inview;
    }
  }
}
