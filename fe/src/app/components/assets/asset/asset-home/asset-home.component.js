import './asset-home.scss';
import controller from './asset-home.controller';
import template from './asset-home.pug';

export default {
  bindings: {
    asset: '<',
    currentUser: '<',
    serviceScheduleCodicils: '<',
    serviceScheduleDateRange: '<',
    serviceScheduleServices: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
