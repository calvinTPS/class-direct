import angular from 'angular';
import AssetHomeComponent from './asset-home.component';
import uiRouter from 'angular-ui-router';

export default angular.module('assetHome', [
  uiRouter,
])
.component('assetHome', AssetHomeComponent)
.name;
