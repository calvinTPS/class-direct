/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import AssetHomeComponent from './asset-home.component';
import AssetHomeController from './asset-home.controller';
import AssetHomeModule from './asset-home';
import AssetHomeTemplate from './asset-home.pug';

describe('AssetHome', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(AssetHomeModule));
  const mockViewService = {
    isServiceScheduleOnDetailMode: false,
  };

  const mockAssetService = {
    getSurveyReports: () => Promise.resolve(),
    getServiceScheduleRange: () => Promise.resolve(),
  };

  const mockCertificateService = {
    getCertificates: () => Promise.resolve(),
  };

  const mockCodicilService = {
    get: () => Promise.resolve(),
  };

  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('ViewService', mockViewService);
      $provide.value('AssetService', mockAssetService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('assetHome', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        currentUser: { id: 1, isEquasisThetis: false },
      });
      spyOn(controller, '_getData');

      controller.$onInit();
      expect(controller._getData).toHaveBeenCalled();
    });

    it('_getData, correctly calls all the api endpoints', () => {
      const controller = makeController({
        currentUser: { id: 1, isEquasisThetis: true },
      });
      spyOn(controller._AssetService, 'getServiceScheduleRange').and.returnValue({
        then: () => Promise.resolve(),
      });

      controller.currentUser.isEquasisThetis = true;
      controller.$onInit(); // This call _getData();
      expect(controller._AssetService.getServiceScheduleRange).toHaveBeenCalled();

      controller.currentUser.isEquasisThetis = false;
      controller._getData();
      expect(controller._AssetService.getServiceScheduleRange).toHaveBeenCalledTimes(2);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetHomeComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetHomeTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetHomeController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<asset-home foo="bar" current-user="{id:1, isEquasisThethis: false }"><asset-home/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('AssetHome');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
