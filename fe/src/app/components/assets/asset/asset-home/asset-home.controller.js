import * as _ from 'lodash';
import Base from 'app/base';

export default class AssetHomeController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $stateParams,
    AppSettings,
    AssetService,
    ViewService, // used by template
  ) {
    super(arguments);
  }

  $onInit() {
    this._getData();
  }

  async _getData() {
    const LOAD_EVENT = this._AppSettings.EVENTS.ASSET_HOME_ITEM_LOADED;
    const { SERVICE_SCHEDULE } = this._AppSettings.ASSET_HOME_ITEMS;

    // SERVICE SCHEDULE
    const dateRange = await this._AssetService.getServiceScheduleRange(this._$stateParams.assetId);

    if (_.isObject(dateRange) && dateRange.highRange && dateRange.lowRange) {
      const { highRange, lowRange } = dateRange;

      const codicilsPromise = this._AssetService.getByDueDate(
        this._$stateParams.assetId,
        this._AssetService.constructor.ASSET_ITEMS.Codicils,
        ['dueDateMin', 'dueDateMax'], {
          dueDateMin: lowRange,
          dueDateMax: highRange,
        });

      const servicesPromise = this._AssetService.getByDueDate(
        this._$stateParams.assetId,
        this._AssetService.constructor.ASSET_ITEMS.Services,
        ['minDueDate', 'maxDueDate'], {
          minDueDate: lowRange,
          maxDueDate: highRange,
          includeFutureDueDates: true,
        });

      const [codicils, services] = await Promise.all([codicilsPromise, servicesPromise]);

      this._$scope.$broadcast(LOAD_EVENT, {
        type: SERVICE_SCHEDULE,
        data: {
          serviceMinDate: lowRange,
          serviceMaxDate: highRange,
          codicils,
          services,
        },
      });
    }
  }
}
