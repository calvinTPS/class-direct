/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import MpmsTaskComponent from './mpms-task.component';
import MpmsTaskController from './mpms-task.controller';
import MpmsTaskModule from './';
import MpmsTaskTemplate from './mpms-task.pug';

describe('MPMSTask', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(MpmsTaskModule));

  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('mpmsTask', { $dep: dep }, bindings);
        spyOn(controller, '_setDefaultValues').and.callThrough();
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    const item = { id: 1 };

    const mockForm = {
      taskDateCredited: {
        $viewValue: '',
        $pristine: false,
        $valid: true,
        $setPristine: () => true,
      },
    };

    beforeEach(() => {
      controller = makeController({
        foo: 'bar',
        item: _.clone(item),
        taskDateCreditedForm: mockForm,
        updateParent: () => true,
      });
    });

    it('initializes just fine', () => {
      expect(controller.foo).toEqual('bar');
      expect(controller._setDefaultValues).toHaveBeenCalled();
      expect(controller._canShowErrors).toEqual(false);
      expect(controller.inEditMode).toEqual(false);
      expect(controller.dateCredited).toEqual(null);
    });

    it('edit(), expands the component to edit', () => {
      spyOn(controller, '_reset');
      spyOn(controller, '_onStateChange');

      controller._canShowErrors = true;
      controller.inEditMode = true;
      controller.edit();
      expect(controller._canShowErrors).toEqual(false);
      expect(controller._reset).toHaveBeenCalledTimes(1);
      expect(controller._onStateChange).not.toHaveBeenCalled();

      controller._canShowErrors = true;
      controller.inEditMode = false;
      controller.edit();
      expect(controller._canShowErrors).toEqual(true);
      expect(controller._reset).toHaveBeenCalledTimes(1);
      expect(controller._onStateChange).toHaveBeenCalled();
    });

    it('_reset(), properly resets to initial state', () => {
      spyOn(controller, '_onStateChange');
      spyOn(controller.taskDateCreditedForm.taskDateCredited, '$setPristine');

      controller._reset();
      expect(controller._onStateChange).toHaveBeenCalled();
      expect(controller.taskDateCreditedForm.taskDateCredited.$setPristine).toHaveBeenCalled();

      // Called first time in the "initializes just fine" test
      expect(controller._setDefaultValues).toHaveBeenCalledTimes(2);
    });

    it('_setDefaultValues(), resets the state and value back to initial state', () => {
      controller.inEditMode = true;
      controller.dateCredited = '2017-12-12';

      controller._setDefaultValues();
      expect(controller.inEditMode).toEqual(false);
      expect(controller.dateCredited).toEqual(null);
    });

    it('get isDateCreditedValid(), gets the correct value of isDateCreditedValid', () => {
      const input = controller.taskDateCreditedForm.taskDateCredited;
      const dateObj = new Date('2017-03-01');

      input.$pristine = true;
      input.$valid = true;
      controller.dateCredited = null;
      expect(controller.isDateCreditedValid).toEqual(true);

      input.$pristine = true;
      input.$valid = false;
      controller.dateCredited = dateObj;
      expect(controller.isDateCreditedValid).toEqual(true);

      input.$pristine = false;
      input.$valid = true;
      controller.dateCredited = null;
      expect(controller.isDateCreditedValid).toEqual(false);

      input.$pristine = false;
      input.$valid = false;
      controller.dateCredited = dateObj;
      expect(controller.isDateCreditedValid).toEqual(false);

      input.$pristine = false;
      input.$valid = true;
      controller.dateCredited = dateObj;
      expect(controller.isDateCreditedValid).toEqual(true);
    });

    it('get canShowNoDateSelectedError(), returns the correct value', () => {
      const input = controller.taskDateCreditedForm.taskDateCredited;

      input.$pristine = true;
      controller._canShowErrors = true;
      input.$viewValue = new Date('2017-03-01');
      expect(controller.canShowNoDateSelectedError).toEqual(false);

      input.$pristine = false;
      controller._canShowErrors = false;
      input.$viewValue = new Date('2017-03-01');
      expect(controller.canShowNoDateSelectedError).toEqual(false);

      input.$pristine = true;
      controller._canShowErrors = false;
      input.$viewValue = new Date('2017-03-01');
      expect(controller.canShowNoDateSelectedError).toEqual(false);

      input.$pristine = false;
      controller._canShowErrors = true;
      input.$viewValue = new Date('2017-03-01');
      expect(controller.canShowNoDateSelectedError).toEqual(false);

      input.$pristine = false;
      controller._canShowErrors = false;
      input.$viewValue = '';
      expect(controller.canShowNoDateSelectedError).toEqual(true);

      input.$pristine = true;
      controller._canShowErrors = true;
      input.$viewValue = '';
      expect(controller.canShowNoDateSelectedError).toEqual(true);
    });

    it('get canShowInvalidDateError(), returns the correct value', () => {
      const input = controller.taskDateCreditedForm.taskDateCredited;
      const dateObj = new Date('2017-03-01');

      // This will set controller.isDateCreditedValid to false
      input.$pristine = false;
      input.$valid = false;
      controller.dateCredited = dateObj;

      // This will set controller.canShowNoDateSelectedError to true
      input.$pristine = false;
      controller._canShowErrors = false;
      input.$viewValue = new Date('2017-03-01');

      controller.inEditMode = false;
      expect(controller.canShowInvalidDateError).toEqual(false);

      controller.inEditMode = true;
      expect(controller.canShowInvalidDateError).toEqual(true);
    });

    it('reactToDateChange(), correctly reacts to date change', () => {
      spyOn(controller, '_onStateChange');
      controller.reactToDateChange();
      expect(controller._onStateChange).toHaveBeenCalled();
    });

    it('_onStateChange(), correctly broadcasts change in state/value', () => {
      spyOn(controller._$rootScope, '$broadcast');
      controller.item.id = 1;
      controller.inEditMode = true;
      controller.dateCredited = '2017-12-12';
      controller.item.model = 'some data in object';

      controller._onStateChange();
      expect(controller._$rootScope.$broadcast)
        .toHaveBeenCalledWith(AppSettings.EVENTS.MPMS_TASK_STATE_CHANGE, {
          id: 1,
          inEditMode: true,
          isValid: controller.isDateCreditedValid,
          dateCredited: '2017-12-12',
          originalData: 'some data in object',
        });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = MpmsTaskComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(MpmsTaskTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(MpmsTaskController);
    });
  });
});
