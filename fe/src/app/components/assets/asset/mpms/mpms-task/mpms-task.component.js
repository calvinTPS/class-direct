import './mpms-task.scss';
import controller from './mpms-task.controller';
import template from './mpms-task.pug';

export default {
  bindings: {
    itemState: '<',
    item: '<',
    updateParent: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
