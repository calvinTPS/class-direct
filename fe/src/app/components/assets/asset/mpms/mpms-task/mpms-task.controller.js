import Base from 'app/base';

export default class MPMSTaskController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $scope,
    AppSettings,
  ) {
    super(arguments);
  }

  $onInit() {
    this._setDefaultValues();

    // Don't show any error before the user interacts
    this._canShowErrors = false;

    this._$scope.$on(this._AppSettings.EVENTS.SHOW_CREDITING_ERRORS, (event) => {
      if (this.inEditMode) {
        this._canShowErrors = true;
        this._$scope.$apply();
      }
    });
  }

  get isDateCreditedValid() {
    const input = this.taskDateCreditedForm.taskDateCredited;
    // If the field has NOT been edited/touched by the user
    // OR it has been touched AND the date is not valid AND a date has been picked(not empty)
    return input.$pristine || (!input.$pristine && input.$valid && !!this.dateCredited);
  }

  get canShowNoDateSelectedError() {
    const input = this.taskDateCreditedForm.taskDateCredited;
    // If the field has been edited/touched by user
    // OR has been informed by parent component to show errors
    // AND the field is empty
    return (!input.$pristine || this._canShowErrors) && !input.$viewValue;
  }

  get canShowInvalidDateError() {
    // If the field is in edit mode
    // AND the date entered is not valid OR the date field is
    // empty(and allowed to show error for that)
    return this.inEditMode && (!this.isDateCreditedValid || this.canShowNoDateSelectedError);
  }

  _reset() {
    this._setDefaultValues();
    this._onStateChange();
    this.taskDateCreditedForm.taskDateCredited.$setPristine();
  }

  _setDefaultValues() {
    this.inEditMode = false;
    this.dateCredited = null;
  }

  _onStateChange() {
    this._$rootScope.$broadcast(this._AppSettings.EVENTS.MPMS_TASK_STATE_CHANGE, {
      id: this.item.id,
      inEditMode: this.inEditMode,
      isValid: this.isDateCreditedValid,
      dateCredited: this.dateCredited,
      originalData: this.item.model,
    });
  }

  reactToDateChange() {
    this._onStateChange();
  }

  edit() {
    if (this.inEditMode) {
      this._canShowErrors = false;
      this._reset();
    } else {
      this.inEditMode = true;
      this._onStateChange();
    }
  }
}
