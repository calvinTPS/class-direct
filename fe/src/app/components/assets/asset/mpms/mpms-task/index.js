import angular from 'angular';
import MPMSTaskComponent from './mpms-task.component';

export default angular.module('mpmsTask', [])
.component('mpmsTask', MPMSTaskComponent)
.name;
