/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import fecha from 'fecha';
import MpmsComponent from './mpms.component';
import MpmsController from './mpms.controller';
import MpmsModule from './';
import MpmsTemplate from './mpms.pug';

describe('Mpms', () => {
  let $rootScope,
    $compile,
    makeController;

  const mockAssetService = {
    updateTasks: () => new Promise(),
  };

  beforeEach(window.module(MpmsModule));

  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('AssetService', mockAssetService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('mpms', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    const tasks = [
      { id: 1, model: {} },
      { id: 2, model: {} },
      { id: 3, model: {} },
    ];

    beforeEach(() => {
      controller = makeController({ foo: 'bar', tasks: _.clone(tasks) });
    });

    it('initializes just fine', () => {
      expect(controller.foo).toEqual('bar');
      expect(controller.accordionMpmsTemplate).toEqual(AppSettings.RIDER_TEMPLATES.MPMS);
      expect(controller.childrenStates).toEqual({});
    });

    it('inEditMode(), determines the edit mode correctly', () => {
      controller.childrenStates = {
        1: { id: 1, inEditMode: false, isValid: false, dateCredited: null },
        2: { id: 2, inEditMode: false, isValid: false, dateCredited: null },
        3: { id: 3, inEditMode: false, isValid: false, dateCredited: null },
      };

      expect(controller.inEditMode).toEqual(false);
      controller.childrenStates[2].inEditMode = true;
      expect(controller.inEditMode).toEqual(true);
      controller.childrenStates[2].inEditMode = false;
      expect(controller.inEditMode).toEqual(false);
    });

    it('isValid(), correctly checks whether all the components in edit mode are valid', () => {
      controller.childrenStates = {
        1: { id: 1, inEditMode: false, isValid: false, dateCredited: null },
        2: { id: 2, inEditMode: false, isValid: false, dateCredited: null },
        3: { id: 3, inEditMode: false, isValid: false, dateCredited: null },
      };

      expect(controller.isValid).toEqual(false);

      controller.childrenStates[1].inEditMode = true;
      controller.childrenStates[1].isValid = false;
      expect(controller.isValid).toEqual(false);

      controller.childrenStates[1].inEditMode = true;
      controller.childrenStates[1].isValid = true;
      expect(controller.isValid).toEqual(false);

      controller.childrenStates[1].inEditMode = true;
      controller.childrenStates[1].isValid = true;
      controller.childrenStates[1].dateCredited = '2017-12-12';
      expect(controller.isValid).toEqual(true);
    });

    it('_removeFromUI(), correctly removes the states based on the given ids', () => {
      spyOn(controller, '_removeFromHierarchy');
      controller.childrenStates = {
        1: { id: 1, inEditMode: false, isValid: false, dateCredited: null },
        2: { id: 2, inEditMode: false, isValid: false, dateCredited: null },
        3: { id: 3, inEditMode: false, isValid: false, dateCredited: null },
      };

      controller._removeFromUI([1, 2]);

      expect(Object.keys(controller.childrenStates).length).toEqual(1);
      expect(controller.childrenStates[3])
        .toEqual({ id: 3, inEditMode: false, isValid: false, dateCredited: null });
      expect(controller.childrenStates[1]).toBeUndefined();
      expect(controller.childrenStates[2]).toBeUndefined();
    });

    it('_removeFromHierarchy(), correctly removes tasks from hierarchy structure', () => {
      controller.taskItems = [
        {
          itemsH: [
            {
              name: 'Vessel',
              tasksH: [{ id: 10 }, { id: 20 }, { id: 30 }],
              itemsH: [
                {
                  name: 'Vessel 2',
                  tasksH: [{ id: 40 }, { id: 50 }],
                  itemsH: [
                    {
                      name: 'Vessel 3',
                      tasksH: [{ id: 60 }, { id: 70 }, { id: 80 }],
                    },
                    {
                      name: 'Vessel 3',
                      tasksH: [{ id: 90 }, { id: 100 }],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ];

      expect(controller.taskItems[0].itemsH[0].tasksH[2]).toEqual({ id: 30 });
      expect(controller.taskItems[0].itemsH[0].itemsH[0].itemsH[1].tasksH[1]).toEqual({ id: 100 });

      controller._removeFromHierarchy(controller.taskItems, [30, 100]);
      expect(controller.taskItems[0].itemsH[0].tasksH[2]).toBeUndefined();
      expect(controller.taskItems[0].itemsH[0].itemsH[0].itemsH[1].tasksH[1]).toBeUndefined();

      // [{ id: 10 }, { id: 20 }]
      expect(controller.taskItems[0].itemsH[0].tasksH.length).toEqual(2);
      // [{ id: 40 }, { id: 50 }]
      expect(controller.taskItems[0].itemsH[0].itemsH[0].tasksH.length).toEqual(2);
      // [{ id: 60 }, { id: 70 }, { id: 80 }]
      expect(controller.taskItems[0].itemsH[0].itemsH[0].itemsH[0].tasksH.length).toEqual(3);
      // [{ id: 90 }]
      expect(controller.taskItems[0].itemsH[0].itemsH[0].itemsH[1].tasksH.length).toEqual(1);
    });

    it('_extractDataToSubmit(), extracts the correct data to submit to server', () => {
      controller.childrenStates = {
        1: {
          id: 1,
          inEditMode: true,
          isValid: true,
          dateCredited: new Date('2017-12-12'),
          originalData: { id: 1, pmsCreditDate: null },
        },
        2: {
          id: 2,
          inEditMode: true,
          isValid: true,
          dateCredited: null,
          originalData: { id: 2, pmsCreditDate: null },
        },
        3: {
          id: 3,
          inEditMode: true,
          isValid: true,
          dateCredited: new Date('2018-12-12'),
          originalData: { id: 3, pmsCreditDate: null },
        },
      };

      const data = controller._extractDataToSubmit();
      expect(data.length).toEqual(2);

      const formattedDate = fecha.format(new Date('2017-12-12'), AppSettings.FORMATTER.LONG_DATE_SERVER);
      expect(data[0].pmsCreditDate).toEqual(formattedDate);
    });

    it.async('submit(), correctly makes submission to server', async () => {
      spyOn(controller._$rootScope, '$broadcast');
      spyOn(controller, '_extractDataToSubmit').and.callThrough();
      spyOn(controller._AssetService, 'updateTasks');
      spyOn(controller, '_removeFromUI');

      // Check for invalid state
      controller.childrenStates = {
        1: {
          id: 1,
          inEditMode: false,
          isValid: false,
          dateCredited: null,
          originalData: { id: 1, pmsCreditDate: null },
        },
        2: {
          id: 2,
          inEditMode: false,
          isValid: false,
          dateCredited: null,
          originalData: { id: 2, pmsCreditDate: null },
        },
      };

      await controller.submit();
      expect(controller._$rootScope.$broadcast)
        .toHaveBeenCalledWith(AppSettings.EVENTS.SHOW_CREDITING_ERRORS);
      expect(controller._extractDataToSubmit).not.toHaveBeenCalled();
      expect(controller._AssetService.updateTasks).not.toHaveBeenCalled();
      expect(controller._removeFromUI).not.toHaveBeenCalled();

      // Check for Valid state
      controller.childrenStates[3] = {
        id: 3,
        inEditMode: true,
        isValid: true,
        dateCredited: new Date('2017-12-12'),
        originalData: { id: 3, pmsCreditDate: null },
      };

      await controller.submit();
      expect(controller._$rootScope.$broadcast)
        .toHaveBeenCalledTimes(1); // called in the test above
      expect(controller._extractDataToSubmit).toHaveBeenCalled();
      expect(controller._AssetService.updateTasks).toHaveBeenCalledWith({
        tasks: [{ id: 3, pmsCreditDate: '2017-12-12' }],
      });
      expect(controller._removeFromUI).toHaveBeenCalledWith([3]);

      expect(controller.submissionDone).toEqual(true);
      expect(controller.submissionSuccesful).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = MpmsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(MpmsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(MpmsController);
    });
  });
});
