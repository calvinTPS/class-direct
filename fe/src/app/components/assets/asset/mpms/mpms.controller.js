import * as _ from 'lodash';
import Base from 'app/base';
import fecha from 'fecha';

export default class MPMSController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    AssetService,
    $rootScope,
    $scope,
  ) {
    super(arguments);
  }

  $onInit() {
    this.accordionMpmsTemplate = this._AppSettings.RIDER_TEMPLATES.MPMS;
    this.childrenStates = {};

    this._$scope.$on(this._AppSettings.EVENTS.MPMS_TASK_STATE_CHANGE, (events, state) => {
      this.childrenStates[state.id] = state;
    });
  }

  get inEditMode() {
    // Return true if any one of the child component is in edit mode
    return !!_.filter(this.childrenStates, state => state.inEditMode).length;
  }

  get isValid() {
    // Return true if all the child component that is in Edit mode is also valid
    const inEditModeCount = _.filter(this.childrenStates, state => state.inEditMode).length;
    const inEditModeAndValidCount = _.filter(this.childrenStates, state =>
      state.inEditMode && state.isValid && state.dateCredited).length;
    return (inEditModeCount > 0) && (inEditModeCount === inEditModeAndValidCount);
  }

  _removeFromUI(taskIds) {
    _.forEach(taskIds, (id) => {
      delete this.childrenStates[id];
      this._removeFromHierarchy(this.taskItems, taskIds);
    });
  }

  _removeFromHierarchy(items, taskIds) {
    items.forEach((item) => {
      if (!_.isEmpty(item.tasksH)) {
        // eslint-disable-next-line no-param-reassign
        item.tasksH = item.tasksH
          .map(task => taskIds.indexOf(task.id) > -1 ? null : task)
          .filter(task => task);
      }

      if (!_.isEmpty(item.itemsH)) {
        this._removeFromHierarchy(item.itemsH, taskIds);
      }
    });
  }

  _extractDataToSubmit() {
    // We need to send back all the fields given by server(state.originalData)
    // or else the PUT api will fail
    const data = [];
    _.forEach(this.childrenStates, (state, id) => {
      if (state.inEditMode && state.isValid && state.dateCredited && state.originalData) {
        // Format the date for server's accepted format
        // eslint-disable-next-line no-param-reassign
        state.originalData.pmsCreditDate = fecha.format(state.dateCredited,
          this._AppSettings.FORMATTER.LONG_DATE_SERVER);

        data.push(state.originalData);
      }
    });
    return data;
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }

  async submit() {
    if (!this.isValid) {
      // Inform child components to show error messages if not in valid state
      this._$rootScope.$broadcast(this._AppSettings.EVENTS.SHOW_CREDITING_ERRORS);
    } else {
      const tasks = this._extractDataToSubmit();
      try {
        await this._AssetService.updateTasks({ tasks });

        this._removeFromUI(_.map(tasks, 'id'));
        this.submissionDone = true;
        this.submissionSuccesful = true;
        this._$scope.$apply();
      } catch (e) {
        this.submissionDone = true;
        this.submissionSuccesful = false;

        // To check if the returned message from BE is a JSON string
        try {
          this.submissionErrorMessage = e.data && angular.fromJson(e.data.message).message;
        } catch (jsonErr) {
          this.submissionErrorMessage = e.data && e.data.message;
        }
        this._$scope.$apply();
      }
    }
  }
}
