import angular from 'angular';
import MPMSComponent from './mpms.component';
import MPMSTaskModule from './mpms-task';

export default angular.module('mpms', [
  MPMSTaskModule,
])
.component('mpms', MPMSComponent)
.name;
