import './mpms.scss';
import controller from './mpms.controller';
import template from './mpms.pug';

export default {
  bindings: {
    asset: '<',
    taskItems: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
