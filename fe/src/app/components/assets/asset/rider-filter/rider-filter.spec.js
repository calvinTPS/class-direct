/* global afterEach afterAll beforeAll beforeEach describe expect inject it */
/*
eslint-disable angular/window-service, angular/no-service-method,
no-underscore-dangle, no-unused-vars
*/
import * as _ from 'lodash';
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import fecha from 'fecha';
import RiderFilterComponent from './rider-filter.component';
import RiderFilterController from './rider-filter.controller';
import RiderFilterModule from './';
import RiderFilterTemplate from './rider-filter.pug';

describe('RiderFilter', () => {
  let $compile;
  let $document;
  let $filter;
  let $mdDialog;
  let $rootScope;
  let $state;
  let $timeout;
  let makeController;
  let mockState;

  beforeEach(window.module(
    RiderFilterModule,
    angularMaterial
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockStateParams = value => value;
    mockState = {
      reload: () => {},
    };
    const mockUtilsFactory = {
      getTabularAction: () => {},
    };

    window.module(($provide) => {
      $provide.value('$state', mockState);
      $provide.value('$stateParams', mockStateParams);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('utils', mockUtilsFactory);
      $provide.value('$mdMedia', () => true);
    });
  });
  beforeEach(
    inject((
      _$compile_,
      _$document_,
      _$filter_,
      _$mdDialog_,
      _$rootScope_,
      _$timeout_,
      $componentController
    ) => {
      $compile = _$compile_;
      $document = _$document_;
      $filter = _$filter_;
      $mdDialog = _$mdDialog_;
      $rootScope = _$rootScope_;
      $timeout = _$timeout_;

      const dep = {};
      makeController =
        (bindings = {}) => {
          const controller = $componentController('riderFilter', {
            $dep: dep,
          }, bindings);
          spyOn(controller, 'updateData').and.callThrough();
          controller.$onInit();
          return controller;
        };
    }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        foo: 'bar',
      });
      expect(controller.foo).toEqual('bar');
      expect(controller.isFilterFormValid).toBe(true);
      expect(controller.showResults).toBe(false);

      // updateData must be called at least once initially,
      // so that showResults becomes true.
      controller.updateData.flush();
      expect(controller.showResults).toEqual(true);
    });

    it('clears date values on initialization', () => {
      const controller = makeController({
        filterOptions: _.cloneDeep(AppSettings.CODICILS_FILTER_OPTIONS),
      });

      controller.filterOptions[0].maxDate = new Date();
      controller.filterOptions[0].minDate = new Date();

      controller._initializeFilterOptions();
      expect(controller.filterOptions[0].minDate).toEqual(null);
      expect(controller.filterOptions[0].maxDate).toEqual(null);
    });

    describe('initializes this.originalItems', () => {
      const fakeItems = [
        { name: 'item 1' },
        { name: 'item 2' },
      ];

      it('this.useBackend === false should not set this.originalItems', () => {
        const controller = makeController({
          items: fakeItems,
        });
        expect(controller.items).toEqual(fakeItems);
        expect(controller.originalItems).toEqual(controller.items);
      });

      it('this.useBackend === true should set this.originalItems', () => {
        const controller = makeController({
          items: fakeItems,
          backendFilteringEnabled: true,
          backendQueryFn: () => {},
        });
        expect(controller.items).toEqual(fakeItems);
        expect(controller.originalItems).toEqual(undefined);
      });
    });

    it('closeFilter should returning false', () => {
      const controller = makeController();
      expect(controller.filterOpen).toBe(false);

      controller.filterOpen = true;
      controller.closeFilter();
      expect(controller.filterOpen).toBe(false);
      expect(controller.filterOpen).not.toBe(true);
    });

    it('toggleFilter returning the correct boolean', () => {
      const controller = makeController();
      expect(controller.filterOpen).toBe(false);

      controller.toggleFilter();
      expect(controller.filterOpen).toBe(true);
    });

    it.async('showAdvanced() should popup a fullscreen mdDialog box', async() => {
      const controller = await makeController();
      const fakePromise = () => 'Promise'; // Cheating, I know, ma bad.
      spyOn($mdDialog, 'show').and.returnValue(new Promise(fakePromise));

      controller.showFilterModal();

      expect($mdDialog.show).toHaveBeenCalled();
    });

    it('should test cancel() behaviour', () => {
      const controller = makeController();
      spyOn($mdDialog, 'cancel');
      controller.cancel();
      expect($mdDialog.cancel).toHaveBeenCalled();
    });

    it('should test submit() behaviour', () => {
      const controller = makeController();
      spyOn($mdDialog, 'hide');
      controller.submit();
      expect($mdDialog.hide).toHaveBeenCalled();
    });

    it.async('On closing search modal should call submit()', async () => {
      const controller = makeController();

      const dialogSpy = spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve());
      spyOn(controller, 'submit');

      await controller.showSearchModal();
      expect(dialogSpy).toHaveBeenCalled();
    });

    it.async('On closing sort modal should call submit()', async () => {
      const controller = makeController();

      const dialogSpy = spyOn(controller._$mdDialog, 'show').and.returnValue(Promise.resolve());
      spyOn(controller, 'submit');

      await controller.showSortModal();
      expect(dialogSpy).toHaveBeenCalled();
    });

    it('updateFilterQuery() should update filter query that will be passed', () => {
      const controller = makeController({ backendQueryFn: () => {} });

      expect(controller.filterQuery).toEqual({});
      controller.updateFilterQuery('statusName', 'Open');
      expect(controller.filterQuery).not.toEqual({});
      expect(controller.filterQuery).toEqual({
        statusName: 'Open',
      });
    });

    it('toggleAllSelected() should function properly', () => {
      const controller = makeController({ backendQueryFn: () => {} });
      const checkboxObject = _.cloneDeep(AppSettings.TASK.FILTER_OPTIONS[0]);
      const checkboxOptions = checkboxObject.options = [{
        name: 'Foo',
        value: 'foo',
        selected: false,
      }];

      checkboxObject.isAll = true;
      controller.toggleAllSelected(checkboxObject);
      expect(checkboxObject.isAllSelected).toBe(true);
      expect(
        controller.filterQuery[checkboxObject.key].length
      ).toEqual(checkboxOptions.length);

      checkboxObject.isAll = false;
      controller.toggleAllSelected(checkboxObject);
      expect(checkboxObject.isAllSelected).toBe(false);
      expect(
        controller.filterQuery[checkboxObject.key].length
      ).toEqual(0);
    });

    it('toggleSelected() should function properly', () => {
      const controller = makeController({ backendQueryFn: () => {} });
      const checkboxObject = _.cloneDeep(AppSettings.TASK.FILTER_OPTIONS[0]);
      const checkboxOptions = checkboxObject.options = [{
        name: 'Foo',
        value: 'foo',
        selected: false,
      }];

      _.forEach(checkboxOptions, (item) => {
        _.assign(item, {
          selected: true,
        });
      });

      controller.toggleSelected(checkboxObject);
      expect(checkboxObject.isAllSelected).toBe(true);
      expect(
        controller.filterQuery[checkboxObject.key].length
      ).toEqual(checkboxOptions.length);

      checkboxOptions[0].selected = false;
      controller.toggleSelected(checkboxObject);
      expect(checkboxObject.isAllSelected).toBe(false);
      expect(
        controller.filterQuery[checkboxObject.key].length
      ).toEqual(0);
    });

    it('revalidateDateRange() should function properly', () => {
      const controller = makeController({ backendQueryFn: () => {} });
      const dateRangeObject = AppSettings.TASK.FILTER_OPTIONS[1];
      const dateRangeValues = {
        buildMinDate: fecha.format(new Date(), AppSettings.FORMATTER.LONG_DATE_MOMENT),
        buildMaxDate: fecha.format(new Date(), AppSettings.FORMATTER.LONG_DATE_MOMENT),
      };

      controller.revalidateDateRange(dateRangeValues, dateRangeObject);
      expect(
        controller.filterQuery[dateRangeObject.key]
      ).toEqual(dateRangeValues);
    });

    it('clearFilterOptions() should reload the state, and close filter in both desktop & mobile', () => {
      const controller = makeController({ backendQueryFn: () => {} });
      spyOn(mockState, 'reload');
      spyOn(controller, 'cancel');

      controller.clearFilterOptions();
      expect(mockState.reload).toHaveBeenCalledTimes(1);
      expect(controller.cancel).toHaveBeenCalledTimes(1);
      expect(controller.filterOpen).toBe(false);
    });

    it('clearFilterOptions() should not reload the state, and close filter in both desktop & mobile', () => {
      const controller = makeController({
        backendQueryFn: () => {},
        hasCurrentStateOnClear: true,
      });
      spyOn(controller, '_initializeFilterOptions');
      spyOn(controller, '_$filter').and.returnValue(jasmine.createSpy());
      spyOn(controller, 'submit');

      controller.clearFilterOptions();
      expect(controller._initializeFilterOptions).toHaveBeenCalledTimes(1);
      expect(controller.submit).toHaveBeenCalledTimes(1);
      expect(controller.filterOpen).toBe(false);
      expect(controller._$filter).toHaveBeenCalledWith('customRiderFilter');
      expect(controller._$filter()).toHaveBeenCalledWith(
        controller.originalItems,
        controller.filterQuery,
        controller.searchBy,
        controller.searchText
      );
    });

    it('mobileFilterButtonClass should return the correct flex percentage', () => {
      const controller = makeController();
      spyOn(angular, 'element').and.returnValue({ children: () => [1, 2, 3, 4] });
      expect(controller.mobileFilterButtonClass).toEqual({ 'flex-25': true });
      expect(angular.element).toHaveBeenCalledWith($document[0].querySelector('.filter-search-mobile-container'));
    });
  });

  describe('Controller updateData()', () => {
    let controller;
    it.async('should NOT call backendQueryFn when useBackend is false', async() => {
      controller = makeController({
        backendFilteringEnabled: false,
        backendQueryFn: jasmine.createSpy(),
        postUpdateCallback: items => items,
      });
      spyOn(controller, '_$filter').and.returnValue(jasmine.createSpy());
      spyOn(controller._$scope, '$apply');
      spyOn(controller, 'postUpdateCallback');

      // updateData is debounced.
      const updateDataPromise = controller.updateData();
      controller.updateData.flush();
      await updateDataPromise;

      expect(controller.backendQueryFn).not.toHaveBeenCalled();
      expect(controller.postUpdateCallback).toHaveBeenCalled();
      expect(controller._$filter).toHaveBeenCalledWith('customRiderFilter');
      expect(controller._$filter()).toHaveBeenCalledWith(
        controller.originalItems,
        controller.filterQuery,
        controller.searchBy,
        controller.searchText,
        controller.limit,
      );
      expect(controller._$scope.$apply).toHaveBeenCalledTimes(1);
    });

    it.async('should call backendQueryFn with correct params when useBackend is true and set returned items and filters', async() => {
      const fakeItems = [{ some: 'items' }];
      const fakeFilters = { some: 'filters' };
      const fakeParams = { some: 'params' };
      controller = makeController({
        backendFilteringEnabled: true,
        backendQueryFn: jasmine.createSpy().and.returnValue(
          { items: fakeItems, filters: fakeFilters }),
      });
      controller._queryParams = fakeParams;
      spyOn(controller._$scope, '$apply');

      // updateData is debounced
      const updateDataPromise = controller.updateData();
      controller.updateData.flush();
      await updateDataPromise;

      expect(controller.backendQueryFn).toHaveBeenCalledWith({ queryParams: fakeParams });
      expect(controller.items).toEqual(fakeItems);
      expect(controller.filters).toEqual(fakeFilters);
      expect(controller.isResolved).toBe(true);
      expect(controller.showResults).toBe(true);
      expect(controller._$scope.$apply).toHaveBeenCalledTimes(1);
    });

    it.async('should only update items and filters based on latest query', async() => {
      const fakeItems = [[{ some1: 'items1' }], [{ some2: 'items2' }]];
      const fakeFilters = { some: 'filters' };
      const fakeParams = { some: 'params' };
      let submissionId = -1;
      controller = makeController({
        backendFilteringEnabled: true,
        backendQueryFn: jasmine.createSpy().and.callFake(() => {
          submissionId++;
          return { items: fakeItems[submissionId], filters: fakeFilters };
        }),
      });
      controller._queryParams = fakeParams;

      // calling twice
      controller.updateData();
      controller.updateData.flush();

      const updateDataPromise = controller.updateData();
      controller.updateData.flush();
      await updateDataPromise;

      expect(controller.backendQueryFn).toHaveBeenCalledWith({ queryParams: fakeParams });
      expect(controller.items).toEqual(fakeItems[1]);
      expect(controller.filters).toBe(fakeFilters);
      expect(controller.isResolved).toBe(true);
    });
  });

  describe('rider-filter-controller orderBy', () => {
    let controller;
    const fakeSortByList = [{
      value: 'id2',
    }, {
      value: 'date',
      order: 'DESC',
    }, {
      customSort: 'sampleSort',
    }];

    it('should return the correct orderBy when customSort exist', () => {
      const sampleSort = () => {};
      controller = makeController({
        customSort: { sampleSort },
      });
      controller.sortBy = fakeSortByList[2];
      expect(_.isFunction(controller.orderBy)).toBe(true);
    });

    it('should not do anything when orderBy is undefined', () => {
      controller = makeController();
      expect(controller.orderBy).toEqual(undefined);
    });

    it('should return the correct orderBy param value when set to DESC', () => {
      controller = makeController({
        sortByList: fakeSortByList,
      });
      controller.sortBy = fakeSortByList[1];
      expect(controller.orderBy).toEqual('-date');
    });

    it('should return the correct orderBy param value when NOT set to DESC', () => {
      controller = makeController({
        sortByList: fakeSortByList,
      });
      controller.sortBy = fakeSortByList[0];
      expect(controller.orderBy).toEqual('id2');
    });

    it('calls updateData if rider-filter if backed by backend', () => {
      controller = makeController({
        sortByList: fakeSortByList,
        backendFilteringEnabled: true,
        backendQueryFn: () => {},
      });
      spyOn(controller, 'updateData').and.callFake(() => {});
      controller.sortBy = fakeSortByList[0];
      expect(controller.updateData).toHaveBeenCalledTimes(1);
      expect(controller.orderBy).toEqual('id2');
    });
  });

  describe('Controller when backendFilteringEnabled is true', () => {
    let controller;
    let fakeParams;

    beforeEach(() => {
      controller = makeController({
        backendFilteringEnabled: true,
        backendQueryFn: () => {},
      });
      // updateData is called once on init.
      controller.updateData.flush(); // debounced, let it through

      controller.backendQueryFn = jasmine.createSpy();
    });

    it('should call backendQueryFn with updated params when search input is cleared', () => {
      fakeParams = { some: 'searchby-data' };
      controller.searchBy = fakeParams;
      controller.searchText = '';

      controller.updateData.flush(); // debounced, let it through

      expect(controller.backendQueryFn).toHaveBeenCalledWith(
        { queryParams: { searchBy: fakeParams, searchText: '' } });
    });

    it('should NOT call backendQueryFn when searchBy change but searchText is empty', () => {
      fakeParams = { some: 'searchby-data' };
      controller.searchBy = fakeParams;

      controller.updateData.flush(); // debounced, let it through

      expect(controller.backendQueryFn).not.toHaveBeenCalled();
    });

    it('should call backendQueryFn with updated params when updateFilterQuery is called', () => {
      controller.updateFilterQuery('some', 'filter_data');
      controller.submit();
      controller.updateData.flush(); // debounced, let it through

      expect(controller.backendQueryFn).toHaveBeenCalledWith(
        { queryParams: { filterQuery: { some: 'filter_data' } } });
    });

    it('should call backendQueryFn with combined params', () => {
      fakeParams = { some: 'searchby-data' };
      controller.searchBy = fakeParams;
      controller.searchText = 'xxx';
      controller.updateFilterQuery('some', 'filter_data');
      controller.submit();
      controller.updateData.flush(); // debounced, let it through

      expect(controller.backendQueryFn).toHaveBeenCalledWith(
        { queryParams: {
          searchBy: fakeParams,
          searchText: 'xxx',
          filterQuery: { some: 'filter_data' },
        } });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = RiderFilterComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(RiderFilterTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(RiderFilterController);
    });
  });

  describe('Rendering', () => {
    let controller;
    let element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<rider-filter foo="bar"><rider-filter/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('RiderFilter');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });

  describe('Rider Filter', () => {
    let controller;

    const fakeData = [{
      id: 1,
      jobNumber: 'Test123',
      services: [{
        some: 'data1',
        name: 'test 1',
      }, {
        some: 'data2',
        name: 'Test 2',
      }],
    }, {
      id: 2,
      jobNumber: 'test456',
      services: [{
        some: 'data3',
        name: 'test 3',
      }, {
        some: 'data4',
        name: 'Test 4',
      }],
    }];

    const mockSearchBy = [
      { name: 'cd-job-number', value: 'jobNumber', selected: false },
      { name: 'cd-surveys', value: 'services.name', isArray: true, selected: true },
    ];

    it('should filter data correctly when searchBy is NOT an array', () => {
      expect($filter('customRiderFilter')(fakeData, null, mockSearchBy[0], '45')).toEqual([fakeData[1]]);
      expect($filter('customRiderFilter')(fakeData, null, mockSearchBy[0], 'test1')).toEqual([fakeData[0]]);
    });

    it('should filter data correctly when searchBy is an array', () => {
      expect($filter('customRiderFilter')(fakeData, null, mockSearchBy[1], 'test 3')).toEqual([fakeData[1]]);
      expect($filter('customRiderFilter')(fakeData, null, mockSearchBy[1], 'test 4')).toEqual([fakeData[1]]);
      expect($filter('customRiderFilter')(fakeData, null, mockSearchBy[1], 'data1')).toEqual([]);
      expect($filter('customRiderFilter')(fakeData, null, mockSearchBy[1], 'test')).toEqual(fakeData);
    });
  });
});
