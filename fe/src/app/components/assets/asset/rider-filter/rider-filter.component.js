import './rider-filter.scss';
import controller from './rider-filter.controller';
import template from './rider-filter.pug';

/**
 * This component handles filtering for: Codicils + Defects + Repairs + Services + Jobs
 *
 * @param {object} action - an object that contains either
 *                          1) state parameters,
 *                          2) $state sref for a single row,
 *                          3) ngClick function.
 *  example for 1, 2:
 *  {
 *    stateName: 'serviceHistory.jobDetails',
 *    evalParams: { jobId: 'id' },
 *    params: { assetId: 1 },
 *  }
 *  - this will be evaluated as "serviceHistory.jobDetails({assetId:1, jobId: item.id})"
 *
 *  example for 3:
 *  {
 *    clickArea: 'icon',                      // icon | full
 *    icon: 'download',                       // iconName
 *    ngClick: this.customClickFunction,
 *  }
 * @param {string} accordionMode -  Currently accepts 2 types of mode in cd-card-accordion component
 *                                  ('schedule' or 'checklist'). Default will use the standard
 *                                  cd-card-tabular component.
 * @param {boolean} backendFilteringEnabled - defaults to false. When enabled filter parameters
 *               will be passed to backendQueryFn
 * @param {function} backendQueryFn - when backendFilteringEnabled is enabled,
 *              backendQueryFn is called with filter parameters to update the data set.
 * @param {object} customData - any custom data passed to this component that will
 *                              be accessed by the item row template
 * @param {string} filterLabel - string key for filter label.
 * @param {array} filterOptions - array of objects for filter options.
 *              configureable by project-variables.js
 * @param {boolean} hideFilterOptionsLabel - hides filter options text label.
 *              It's used to hide the invisible filter label if there's no
 *              searchBy and sortBy component being shown
 *              because it broke the UI
 * @param {boolean} hideLabel - hides search text label
 * @param {array} items - array of objects to display in a table
 * @param {array} searchByList - array of objects for search options.
 *              configureable by project-variables.js
 * @param {string} searchLabel - string key for search label.
 * @param {boolean} hasCurrentStateOnClear - save current state
 *  when clear filter is executed without reloading the current state
 * @param {boolean} showAdministrationButtons - show add user and export account button
 * @param {boolean} showPagination - show pagination
 * @param {boolean} showPaginationSpinner - show pagination spinner.
 *  Only applicable if showPagination is true.
 * @param {boolean} showTopSpinner - show top spinner when results not ready.
 * @param {boolean} loadingMessage - message to show when results not ready.
 * @param {array} sortByList - array of objects for sort options.
 *              configureable by project-variables.js
 * @param {string} sortLabel - string key for sort label.
 * @param {string} template - template or a single row item in the table
 */
export default {
  bindings: {
    accordionMode: '<?',
    action: '<?',
    backendFilteringEnabled: '<?',
    backendQueryFn: '&?',
    canExpandCollapse: '<',
    customData: '<?',
    customSort: '<?',
    filterLabel: '@?',
    filterOptions: '<',
    hasCurrentStateOnClear: '<?',
    hideFilterOptionsLabel: '<?',
    hideLabel: '<',
    isAccordionExpanded: '<',
    items: '=?',
    limit: '<?',
    loadingMessage: '@?',
    postUpdateCallback: '<?',
    searchByList: '<?',
    searchLabel: '@?',
    showAdministrationButtons: '<?',
    showLoadMore: '<?',
    showPagination: '<?',
    showPaginationSpinner: '<?',
    showTopSpinner: '<?',
    sortByList: '<?',
    sortLabel: '@?',
    template: '@',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
