import angular from 'angular';
import riderFilterComponent from './rider-filter.component';
import riderFilterFilter from './rider-filter.filter';
import RiderRefresherDirective from './riders.refresher.directive.js';

export default angular.module('riderFilter', [
])
.component('riderFilter', riderFilterComponent)
.filter('customRiderFilter', riderFilterFilter)
.directive('ridersRefresher', RiderRefresherDirective)
.name;
