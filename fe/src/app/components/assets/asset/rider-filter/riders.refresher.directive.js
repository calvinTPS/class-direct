// I refresh the transcluded ng-repeat using
// a single watch - yay!
// I watch the attribute condition
// and refresh the scope
export default () => ({
  transclude: true,
  link: (
    scope,
    element,
    attrs,
    ctrl,
    transclude,
  ) => {
    let childScope;
    scope.$watch(attrs.condition, (value) => {
      element.empty();
      if (childScope) {
        childScope.$destroy();
        childScope = null;
      }

      transclude((clone, newScope) => {
        childScope = newScope;
        element.append(clone);
      });
    });
  },
});
