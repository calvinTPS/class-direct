/* eslint-disable no-param-reassign, no-unused-expressions, no-unneeded-ternary */
import * as _ from 'lodash';
import Base from 'app/base';
import filterModalTemplate from './rider-filter-modal.pug';
import searchModalTemplate from './rider-filter-search-modal.pug';
import sortModalTemplate from './rider-filter-sort-modal.pug';

export default class RiderFilterController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $filter,
    $mdDialog,
    $mdMedia,
    $scope,
    $state,
    $stateParams,
    AppSettings,
    utils,
  ) {
    super(arguments);
  }

  $onInit() {
    this._searchBy = {};
    this._searchText = '';
    this._sortBy = {};
    this.filterOpen = false;
    this.filterQuery = {};
    this.isAccordionExpandable = true;
    this.isFilterFormValid = true;
    this.useBackend = this.backendFilteringEnabled || false;
    this._submissionId = -1;
    this._queryParams = {};

    if (this.items) {
      this.postUpdateCallback =
        this.postUpdateCallback ||
        this.items.postUpdateCallback ||
        null;
    }

    // Hide results on first load to prevent flickering
    // when results are being filtered
    this.showResults = false;

    if (this.useBackend) {
      if (this.backendQueryFn == null) {
        throw new Error('Missing backend query function');
      }
    }

    // Debounce updateData
    this.updateData = _.debounce(this.updateData, this._AppSettings.DEFAULT_PARAMS.DEBOUNCE);

    this.isResolved = !this.useBackend;
    this.originalItems = this.useBackend ? undefined : _.cloneDeep(this.items);
    // this.items = [];
    this.originalLimit = this.limit;
    this._initializeFilterOptions();

    this._$scope.$on('UPDATE_RIDER_FILTER', (event, items) => {
      this.updateOriginalItems(items);
    });
    this.lastUpdated = {};
    this.lastUpdated.date = Date.now();

    // Trigger the data update at least once.
    // It is debounced, so subsequent neartime calls are debounced.
    this.updateData();
  }

  get mobileFilterButtonClass() {
    const totalChildElements = angular.element(this._$document[0].querySelector('.filter-search-mobile-container')).children().length;
    const flexPercentage = Math.floor((100 / totalChildElements) | 0); // eslint-disable-line no-bitwise, max-len
    return { [`flex-${flexPercentage}`]: flexPercentage > 0 };
  }

  get searchBy() {
    return this._searchBy;
  }

  set searchBy(arg) {
    this._searchBy = arg;
    this._queryParams.searchBy = arg;

    // Prevent filter option being applied on the fly in mobile filter modal
    if (this._searchText && this._$mdMedia('gt-xs')) {
      this.updateData();
    }
  }

  get searchText() {
    return this._searchText;
  }

  set searchText(arg) {
    this._searchText = arg;
    this._queryParams.searchText = arg;
    if (!this._searchText && this._$mdMedia('gt-xs')) {
      this.updateData();
    }
  }

  get sortBy() {
    return this._sortBy;
  }

  set sortBy(arg) {
    this._sortBy = arg;
    this._queryParams.sortBy = arg;

    // Prevent filter option being applied on the fly in mobile filter modal
    if (this._$mdMedia('gt-xs')) {
      this.updateData();
    }
  }

  get orderBy() {
    if (this.sortBy && this.sortBy.customSort) {
      return this.customSort[this.sortBy.customSort];
    }

    return this.sortBy && this.sortBy.order === 'DESC' ?
      `-${this.sortBy.value}` : this.sortBy.value;
  }

  async updateData() {
    // Backend filtering
    if (this.useBackend) {
      this.isResolved = false;
      this._submissionId += 1;
      const currentSubmissionId = this._submissionId;

      const { items, filters } = await this.backendQueryFn({ queryParams: this._queryParams });

      if (currentSubmissionId === this._submissionId) {
        this.items = items;
        this.filters = filters;
      }

      this.isResolved = true;

      // Frontend filtering
    } else {
      const filtered = this._$filter('customRiderFilter')(this.originalItems, this.filterQuery, this.searchBy, this.searchText, this.limit);
      this.items = this.postUpdateCallback ? this.postUpdateCallback(filtered) : filtered;
    }

    this.showResults = true;
    this._$scope.$apply(); // Need this to force digest in async function
    return this.items;
  }

  closeFilter() {
    this.filterOpen = false;
  }

  /**
   * Toggle expanding filter container
   */
  toggleFilter() {
    this.filterOpen = !this.filterOpen;
  }

  /**
   * This function is used to update the filter query from filter component
   *
   * @param {string} key
   * @param {string} value
   */
  updateFilterQuery(key, value) {
    this.filterQuery[key] = value;
    this._queryParams.filterQuery = this.filterQuery;
  }

  /**
   * This function is used to toggle all of the checkboxes inside filter component
   *
   * @param {object} checkboxObject
   * @param {boolean} boolean
   */
  toggleAllSelected(checkboxObject, boolean) {
    checkboxObject.isAllSelected = !_.isUndefined(boolean) ?
      boolean :
      _.cloneDeep(checkboxObject.isAll);
    _.forEach(checkboxObject.options, (d) => {
      d.selected = checkboxObject.isAllSelected;
    });

    this.updateFilterQuery(
      checkboxObject.key,
      this.rearrangeCheckboxValues(checkboxObject.options)
    );
  }

  /**
   * This function is used to validate the isAll checkbox when
   * the other single checkboxes being selected / unselected
   *
   * @param {object} checkboxObject
   */
  toggleSelected(checkboxObject) {
    checkboxObject.isAllSelected = _.every(checkboxObject.options, c => c.selected);
    this.updateFilterQuery(
      checkboxObject.key,
      this.rearrangeCheckboxValues(checkboxObject.options)
    );
  }

  /**
   * This function is used to rearrange the selected checkboxes
   * and create a new array with the values inside it
   *
   * @param {object} checkboxes
   * @return {array} of the checkboxes
   */
  rearrangeCheckboxValues(checkboxes) {
    return checkboxes
      .filter(v => v.selected)
      .map(v => v.value);
  }

  /**
   * This function is used to revalidate date range component when
   * users make selection changes
   *
   * @param {object} changedDateRange
   * @param {object} filterOption
   */
  revalidateDateRange(changedDateRange, filterOption) {
    this.updateFilterQuery(filterOption.key, changedDateRange);
  }

  /**
   * This function is used to open up the modal dialog from filter box
   * on mobile view
   *
   * @param {object} evt
   */
  showFilterModal(evt) {
    // Must deep clone because options can be deep
    const previousFilterOptions = _.cloneDeep(this.filterOptions);
    this._$mdDialog.show({
      bindToController: true,
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true, // Only for -xs, -sm breakpoints.
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: filterModalTemplate,
      onShowing: () => {
        this.isAccordionExpandable = false;
      },
    }).then(() => {
      this.isAccordionExpandable = true;
    }).catch(() => {
      this.isAccordionExpandable = true;
      this.filterOptions = previousFilterOptions;
    });
  }

  showSearchModal(evt) {
    const previousSearchBy = this.searchBy;
    const previousSearchText = this.searchText;
    this._$mdDialog.show({
      bindToController: true,
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true,
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: searchModalTemplate,
    })
      .catch(() => {
        // Reset the options if modal is closed without submit
        this.searchBy = previousSearchBy;
        this.searchText = previousSearchText;
      });
  }

  showSortModal(evt) {
    const previousSortBy = this.sortBy;
    this._$mdDialog.show({
      bindToController: true,
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true,
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: sortModalTemplate,
    })
      .catch(() => {
        // Reset the option if modal is closed without submit
        this.sortBy = previousSortBy;
      });
  }

  cancel() {
    this._$mdDialog.cancel();
  }

  submit() {
    this.updateData();
    this._$mdDialog.hide();
  }

  /**
   * This function is used to reinitiate the model value of some specific filterOptions :
   *
   * - Checkboxes
   * - Radio buttons
   * - Date ranges
   */
  _initializeFilterOptions() {
    const filterOptions = _.clone(this.filterOptions);
    _.forEach(filterOptions, (item, k) => {
      if (item.type === 'radiobuttons') {
        if (item.options.length === 1) {
          item.options[0].selected = true;
        }
        const model = _.filter(item.options, (v, e) => v.selected);

        item.model = _.get(model, '[0].value');
        item.model ? this.updateFilterQuery(item.key, item.model) : '';
      } else if (item.type === 'checkboxes' && item.isAll === true) {
        this.toggleAllSelected(item);
      } else if (item.type === 'checkboxes' && !item.isAll) {
        this.toggleSelected(item);
      } else if (item.type === 'daterange') {
        item.minDate = null;
        item.maxDate = null;
      }
    });
  }

  /*
  * I update the originalItems forcefully from (items)
  * arguement or from this.items
  */
  async updateOriginalItems(items) {
    this.showResults = false;
    this.limit = this.originalLimit;

    this.originalItems = items ? [...items] : [...this.items];

    await this.updateData();
  }

  loadMore() {
    this.limit = this.limit + 10;
    this.updateData();
  }
  /**
   * This function is used to clear the filter options.
   *
   * 1. Close all menus.
   * 2. Reload state from server.
   */
  clearFilterOptions() {
    this.closeFilter(); // Desktop

    if (this.hasCurrentStateOnClear) {
      this.filterQuery = {};
      this._initializeFilterOptions();
      const filtered = this._$filter('customRiderFilter')(this.originalItems, this.filterQuery, this.searchBy, this.searchText);
      this.items = this.postUpdateCallback ? this.postUpdateCallback(filtered) : filtered;
      this.submit(); // Submit current filter options on modal exit
    } else {
      this.cancel(); // Store previous filter options on modal exit
      this._$state.reload();
    }
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
