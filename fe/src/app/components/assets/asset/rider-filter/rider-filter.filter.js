/* eslint-disable no-mixed-operators */
import * as _ from 'lodash';
import dateHelpers from 'app/common/helpers/date-helper';

/* @ngInject */
export default $filter =>
  (items, filterQuery, searchBy, searchText, limit) => {
    let data = _.cloneDeep(items);

    /**
     * Query the filter if the query object doesn't contain empty object
     */

    if (!_.isEmpty(filterQuery)) {
      _.forEach(filterQuery, (v, k) => {
        if (_.isArray(v) && v.length > 0) {
          // Filter checkpoint if the filter value contains array which means checkboxes
          data = _.filter(data, r => _.get(r, k) ? v.indexOf(_.get(r, k)) !== -1 : false);
        } else if (
          _.isObject(v) && v.filterDateMin ||
          _.isObject(v) && v.filterDateMax
        ) {
          // Filter checkpoint if the filter value contains object which means date range
          data = _.filter(data, r => _.get(r, k) ?
            dateHelpers.inRange(_.get(r, k), v.filterDateMin, v.filterDateMax) :
            false
          );
        } else if (!_.isArray(v) && !_.isObject(v)) {
          // Filter checkpoint if the filter value contains value which means radio buttons
          data = _.filter(data, r => _.get(r, k) ? _.get(r, k) === v : false);
        }
      });
    }

    /**
     * Query the search if the search text and search by param are exist
     */

    const searchTerm = searchText ? searchText.toLowerCase() : '';

    if (searchBy && searchTerm) {
      data = _.filter(data, (item) => {
        const key = searchBy.value;
        const isArray = _.includes(key, '.') ?
          _.isArray(item[key.split('.')[0]]) :
          _.isArray(item[key]);

        if (isArray) {
          const keys = key.split('.');
          const arrValues = _.map(_.get(item, keys[0]), i => i[keys[1]]);
          for (let i = 0; i < arrValues.length; i++) {
            if (arrValues[i].toLowerCase().indexOf(searchTerm) > -1) {
              return true;
            }
          }
          return false;
        }
        return _.toString(_.get(item, key)).toLowerCase().indexOf(searchTerm) > -1;
      });
    }

    if (limit) {
      return data.slice(0, limit);
    }

    return data;
  };
