import angular from 'angular';
import ExecutiveHullSummariesComponent from './executive-hull-summaries.component';
import uiRouter from 'angular-ui-router';

export default angular.module('executiveHullSummaries', [
  uiRouter,
])
.component('executiveHullSummaries', ExecutiveHullSummariesComponent)
.name;
