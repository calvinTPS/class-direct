import './executive-hull-summaries.scss';
import controller from './executive-hull-summaries.controller';
import template from './executive-hull-summaries.pug';

export default {
  bindings: {
    jobEhsAttachments: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
