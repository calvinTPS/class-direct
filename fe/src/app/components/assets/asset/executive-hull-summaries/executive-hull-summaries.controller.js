import * as _ from 'lodash';
import Base from 'app/base';

export default class ExecutiveHullSummariesController extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    AppSettings,
    DownloadService,
    ErrorService,
    ViewService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this.jobEhsAttachments = _.orderBy(this.jobEhsAttachments, 'creationDate', 'desc');

    this.searchByList = this._AppSettings.EHS_SEARCH_BY_LIST;
    this.filterOptions = this._AppSettings.EHS_FILTER_OPTIONS;
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.EXECUTIVE_HULL_SUMMARY;
    this.isRetrievingAttachements = false;

    this.action = {
      clickArea: 'icon',
      hasGap: true,
      icon: 'download',
      ngClick: (item) => {
        if (this.isRetrievingAttachements) return;
        this.isRetrievingAttachements = true;

        this._DownloadService.downloadFile({ token: item.token })
          .then(() => {
            this.isRetrievingAttachements = false;
          }).catch((response) => {
            this.isRetrievingAttachements = false;
            this._ErrorService.showErrorDialog({ message: 'cd-failed-to-retrieve-attachment' });
          });
      },
    };
  }
}
