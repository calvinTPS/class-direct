/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import ExecutiveHullSummariesComponent from './executive-hull-summaries.component';
import ExecutiveHullSummariesController from './executive-hull-summaries.controller';
import ExecutiveHullSummariesModule from './';
import ExecutiveHullSummariesTemplate from './executive-hull-summaries.pug';

describe('ExecutiveHullSummaries', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(
    ExecutiveHullSummariesModule,
    {
      DownloadService: () => {},
      ErrorService: () => {},
      ViewService: { registerTemplate: (x, y) => {} },
    }
  ));


  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('executiveHullSummaries', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const mockJobEhsAttachments = [
        { id: 1, creationDate: '2017-03-27T00:00:00Z' },
        { id: 2, creationDate: '2017-08-27T00:00:00Z' },
        { id: 3, creationDate: '2017-02-27T00:00:00Z' },
        { id: 4, creationDate: '2017-10-27T00:00:00Z' },
      ];
      const sortedJobEhsAttachments = [
        { id: 4, creationDate: '2017-10-27T00:00:00Z' },
        { id: 2, creationDate: '2017-08-27T00:00:00Z' },
        { id: 1, creationDate: '2017-03-27T00:00:00Z' },
        { id: 3, creationDate: '2017-02-27T00:00:00Z' },
      ];
      const controller = makeController({ jobEhsAttachments: mockJobEhsAttachments });
      expect(controller.jobEhsAttachments).toEqual(sortedJobEhsAttachments);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ExecutiveHullSummariesComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ExecutiveHullSummariesTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ExecutiveHullSummariesController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<executive-hull-summaries foo="bar"><executive-hull-summaries/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ExecutiveHullSummaries');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
