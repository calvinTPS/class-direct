import './asset.scss';
import controller from './asset.controller';
import template from './asset.pug';

export default {
  bindings: {
    asset: '<',
    currentUser: '<',
  },
  controllerAs: 'vm',
  controller,
  restrict: 'E',
  template,
};
