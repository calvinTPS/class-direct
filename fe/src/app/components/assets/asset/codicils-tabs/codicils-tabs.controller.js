import * as _ from 'lodash';
import Base from 'app/base';

export default class CodicilsTabsController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $stateParams,
    $timeout,
    $transitions,
    AppSettings,
    PermissionsService,
  ) {
    super(arguments);
  }

  $onInit() {
    const STATES = _.clone(this._AppSettings.STATES);
    const EXPORTABLE_ITEMS = _.clone(this._AppSettings.EXPORTABLE_ITEMS);

    this.exportOptions = this.exportOptions || {};

    this._tabSelected = this._tabSelected || 0;
    this.assetId = this._$stateParams.assetId;
    this.tabs = [{
      class: 'tab-coc',
      label: 'cd-tabs-conditions-of-class',
      exportId: EXPORTABLE_ITEMS.CONDITIONS_OF_CLASS,
      uiSref: STATES.CONDITIONS_OF_CLASS_LIST,
    }, {
      class: 'tab-actionable-items',
      label: 'cd-tabs-actionable-items',
      exportId: EXPORTABLE_ITEMS.ACTIONABLE_ITEMS,
      uiSref: STATES.ACTIONABLE_ITEMS_LIST,
    }, {
      class: 'tab-asset-notes',
      label: 'cd-tabs-asset-notes',
      exportId: EXPORTABLE_ITEMS.ASSET_NOTES,
      uiSref: STATES.ASSET_NOTES_LIST,
    }];

    if (!this.currentUser.isEquasisThetis) {
      // Push Defect, Statutory Findings and Statutory Deficiencies tabs
      // to second position as per wireframe
      this.tabs.splice(1, 0, {
        class: 'tab-defects',
        label: 'cd-tabs-defects',
        exportId: EXPORTABLE_ITEMS.DEFECTS,
        uiSref: STATES.DEFECTS_LIST,
      }, {
        class: 'tab-statutory-findings',
        label: 'cd-tabs-statutory-findings',
        exportId: EXPORTABLE_ITEMS.STATUTORY_FINDINGS,
        uiSref: STATES.STATUTORY_FINDINGS_LIST,
      }, {
        class: 'tab-statutory-deficiencies',
        label: 'cd-tabs-statutory-deficiencies',
        exportId: EXPORTABLE_ITEMS.STATUTORY_DEFICIENCIES,
        uiSref: STATES.STATUTORY_DEFICIENCIES_LIST,
      });

      if (this._PermissionsService.canAccessMNCN(this.currentUser, this.asset)) {
        this.tabs.push({
          class: 'tab-mncn',
          label: 'cd-tabs-mncn',
          exportId: EXPORTABLE_ITEMS.MNCN,
          uiSref: STATES.MNCNS_LIST,
        });
      }
    }

    this.revalidateTabSelection();
    this._setExportCodicilType(this.tabs[this._tabSelected].exportId);
  }

  revalidateTabSelection() {
    const tabIndex = _.findIndex(this.tabs, {
      uiSref: this._$state.current.name,
    });
    if (tabIndex && tabIndex > 0) {
      this.tabSelected = tabIndex;
    }
  }

  get tabSelected() {
    return this._tabSelected;
  }

  set tabSelected(arg) {
    this._tabSelected = arg;
    this._setExportCodicilType(this.tabs[arg].exportId);
  }

  // To update the print/export options
  _setExportCodicilType(exportId) {
    _.assign(this.exportOptions, { itemType: exportId });
  }
}
