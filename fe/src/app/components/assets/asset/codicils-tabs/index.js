import angular from 'angular';
import codicilsTabsComponent from './codicils-tabs.component';
import uiRouter from 'angular-ui-router';

export default angular.module('codicilsTabs', [
  uiRouter,
])
.component('codicilsTabs', codicilsTabsComponent)
.name;
