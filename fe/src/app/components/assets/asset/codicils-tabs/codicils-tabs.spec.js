/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import CodicilsTabsComponent from './codicils-tabs.component';
import CodicilsTabsController from './codicils-tabs.controller';
import CodicilsTabsModule from './';
import CodicilsTabsTemplate from './codicils-tabs.pug';

describe('CodicilsTabs', () => {
  let $rootScope,
    $compile,
    $state,
    makeController;

  beforeEach(window.module(CodicilsTabsModule));
  beforeEach(window.module(($provide) => {
    const mockTranslateFilter = value => value;
    const mockPermissionService = { canAccessMNCN: (foo, bar) => true };
    $provide.value('AppSettings', AppSettings);
    $provide.value('PermissionsService', mockPermissionService);
    $provide.value('translateFilter', mockTranslateFilter);
  }));
  beforeEach(inject((_$rootScope_, _$compile_, _$state_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $state = _$state_;

    $state.$current.parent = { name: AppSettings.STATES.VESSEL_LIST.split('.')[0] };
    $state.$current.parent.name = AppSettings.STATES.VESSEL_LIST.split('.')[0];

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('codicilsTabs', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    beforeEach(() => {
      controller = makeController({
        foo: 'bar',
        currentUser: {
          isEquasisThetis: false,
        },
      });
      controller.$onInit();
    });

    it('initializes just fine', () => {
      controller = makeController({
        foo: 'bar',
        currentUser: {
          isEquasisThetis: false,
        },
      });
      spyOn(controller, 'revalidateTabSelection');
      spyOn(controller, '_setExportCodicilType');

      controller.$onInit();

      expect(controller.foo).toEqual('bar');
      expect(controller.tabs.length).toEqual(7);
      expect(controller.tabs[0]).toEqual({
        class: 'tab-coc',
        label: 'cd-tabs-conditions-of-class',
        exportId: AppSettings.EXPORTABLE_ITEMS.CONDITIONS_OF_CLASS,
        uiSref: AppSettings.STATES.CONDITIONS_OF_CLASS_LIST,
      });
      expect(controller.revalidateTabSelection).toHaveBeenCalled();
      expect(controller._setExportCodicilType)
        .toHaveBeenCalledWith(AppSettings.EXPORTABLE_ITEMS.CONDITIONS_OF_CLASS); // first tab
    });

    it('initializes without the Defects, Statutory Findings and Statutory Deficiencies tabs for Equasis/Thetis user', () => {
      const currentUser = { isEquasisThetis: true };
      controller = makeController({ currentUser });
      controller.$onInit();
      expect(controller.tabs.length).toEqual(3);
      expect(controller.tabs[0].label).toEqual('cd-tabs-conditions-of-class');
      expect(controller.tabs[1].label).toEqual('cd-tabs-actionable-items');
      expect(controller.tabs[2].label).toEqual('cd-tabs-asset-notes');
    });

    it('should defined how many initialized tabs are there', () => {
      $state.current.name = AppSettings.STATES.CONDITIONS_OF_CLASS_LIST;
      $state.$current.parent = { name: AppSettings.STATES.CONDITIONS_OF_CLASS_LIST.split('.')[0] };
      expect(controller.tabs.length).toBe(7);
    });

    it('revalidateTabSelection() should return the correct tabSelected value', () => {
      $state.current.name = AppSettings.STATES.CONDITIONS_OF_CLASS_LIST;
      $state.$current.parent = { name: AppSettings.STATES.CONDITIONS_OF_CLASS_LIST.split('.')[0] };

      expect(controller.tabSelected).toBe(0);

      $state.current.name = AppSettings.STATES.DEFECTS_LIST;
      $state.$current.parent = { name: AppSettings.STATES.DEFECTS_LIST.split('.')[0] };
      controller.revalidateTabSelection();
      expect(controller.tabSelected).toBe(1);

      $state.current.name = AppSettings.STATES.STATUTORY_FINDINGS_LIST;
      $state.$current.parent = { name: AppSettings.STATES.STATUTORY_FINDINGS_LIST.split('.')[0] };
      controller.revalidateTabSelection();
      expect(controller.tabSelected).toBe(2);

      $state.current.name = AppSettings.STATES.STATUTORY_DEFICIENCIES_LIST;
      $state.$current.parent = { name: AppSettings.STATES.STATUTORY_DEFICIENCIES_LIST.split('.')[0] };
      controller.revalidateTabSelection();
      expect(controller.tabSelected).toBe(3);

      $state.current.name = AppSettings.STATES.ACTIONABLE_ITEMS_LIST;
      $state.$current.parent = { name: AppSettings.STATES.ACTIONABLE_ITEMS_LIST.split('.')[0] };
      controller.revalidateTabSelection();
      expect(controller.tabSelected).toBe(4);

      $state.current.name = AppSettings.STATES.ASSET_NOTES_LIST;
      $state.$current.parent = { name: AppSettings.STATES.ASSET_NOTES_LIST.split('.')[0] };
      controller.revalidateTabSelection();
      expect(controller.tabSelected).toBe(5);
    });

    it('_setExportCodicilType(), should correctly broadcast the codicil type', () => {
      controller._setExportCodicilType('AN');
      expect(controller.exportOptions).toEqual({ itemType: 'AN' });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CodicilsTabsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CodicilsTabsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CodicilsTabsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.bar = 'baz'; // CHANGE ME
      element = angular.element('<codicils-tabs foo="bar" current-user={}><codicils-tabs/>');
      element = $compile(element)(scope);
      scope.$apply();

      controller = element.controller('CodicilsTabs');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
