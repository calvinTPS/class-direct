import './codicils-tabs.scss';
import controller from './codicils-tabs.controller';
import template from './codicils-tabs.pug';

export default {
  bindings: {
    asset: '<',
    currentUser: '<',
    exportOptions: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
