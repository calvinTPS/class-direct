import angular from 'angular';
import assetServiceScheduleComponent from './asset-service-schedule.component';
import uiRouter from 'angular-ui-router';

export default angular.module('assetServiceSchedule', [
  uiRouter,
])
.component('assetServiceSchedule', assetServiceScheduleComponent)
.name;
