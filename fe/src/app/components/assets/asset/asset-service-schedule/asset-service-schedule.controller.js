import * as _ from 'lodash';
import Base from 'app/base';
import filterModalTemplate from './asset-service-schedule-filter-modal.pug';

export default class AssetServiceScheduleController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $rootScope,
    $scope,
    $state,
    $window,
    AppSettings,
    UserService,
    utils,
    ViewService,
  ) {
    super(arguments);
    this.isLoaded = false;
  }

  onDatePickerChange(options) {
    _.set(options, 'codicilTypeId', this.codicilTypeId);
    _.set(options, 'serviceIds', this.serviceIds);
    if (!_.isDate(options.minDate)) {
      options.minDate = this.serviceMinDate; // eslint-disable-line no-param-reassign
    }

    if (!_.isDate(options.maxDate)) {
      options.maxDate = this.serviceMaxDate; // eslint-disable-line no-param-reassign
    }
    this._filterItems(options);
  }

  $onInit() {
    const LOAD_EVENT = this._AppSettings.EVENTS.ASSET_HOME_ITEM_LOADED;
    const { SERVICE_SCHEDULE } = this._AppSettings.ASSET_HOME_ITEMS;
    // for gantt chart date filter
    this._ViewService.isSurveyScheduleViewToggled = true;
    // For Survey Planner page
    if (this.serviceMinDate && this.serviceMaxDate &&
      this.serviceScheduleCodicils && this.serviceScheduleServices) {
      this._initialize();
      this.isLoaded = true;
    } else {
      // for Asset Home page
      this._$scope.$on(LOAD_EVENT, (event, payload) => {
        if (payload.type === SERVICE_SCHEDULE) {
          this.serviceMinDate = payload.data.serviceMinDate;
          this.serviceMaxDate = payload.data.serviceMaxDate;
          this.serviceScheduleCodicils = payload.data.codicils;
          this.serviceScheduleServices = payload.data.services;
          this._ViewService.isSurveyScheduleViewToggled = false;
          this._initialize();
          this.isLoaded = true;
          this._$scope.$apply();
        }
      });
    }
  }

  _initialize() {
    if (!this.asset.isAccessibleMASTAsset) {
      return;
    }
    this.isAccordionExpandable = true;
    this.assetServiceScheduleItemTpl = this._AppSettings.RIDER_TEMPLATES.ASSET_SERVICE_SCHEDULE;

    this._codicils = _.cloneDeep(this.serviceScheduleCodicils);
    this._services = _.cloneDeep(this.serviceScheduleServices);
    this._groupedServices = this._groupServices(this._services);

    this.dueDateMin = this.serviceMinDate;
    this.dueDateMax = this.serviceMaxDate;
    // Save the default date range
    this._storedDateRange = _.cloneDeep({
      minDate: this.dueDateMin,
      maxDate: this.dueDateMax,
    });
    // get codicil types from list for filter options
    this.codicilTypeId = [];
    this._getCodicilTypes();
    // get service types from list for filter options
    this.serviceIds = [];
    this._getServiceTypes();
    // set the timeline min max

    this.viewTypeOptions = _.cloneDeep(this._AppSettings.SERVICE_SCHEDULE.VIEW_TYPES);
    this.viewLocation = this._AppSettings.SERVICE_SCHEDULE.LOCATIONS.ASSET_SERVICE_SCHEDULE;

    this.viewType = this._ViewService.getServiceScheduleViewType(this.viewLocation) ||
      this.viewTypeOptions.TABULAR;

    this.filterOpen = false;

    if (this.isDetailedMode) {
      this.showDetailedMode();
    }

    // this.currentUser Need to be created as an object so the child component will be updated
    // when the object is populated with the user details
    this.currentUser = {};
    this._getCurrentUser();
  }

  async _getCurrentUser() {
    this.currentUser = await this._UserService.getCurrentUser();
    this._$scope.$apply();
  }

  _filterItems({ minDate, maxDate, codicilTypeId, serviceIds }) {
    this._codicils = this._filterByDates(
      this._filterByCodicils(this.serviceScheduleCodicils, codicilTypeId), minDate, maxDate
    );
    this._groupedServices = this._groupServices(
      this._filterByDates(
        this._filterByServiceIds(this.serviceScheduleServices, serviceIds),
        minDate,
        maxDate,
      ),
    );
  }

  _filterByDates(data, minDate, maxDate) {
    const min = _.isNumber(minDate) ? minDate : Date.parse(minDate);
    const max = _.isNumber(maxDate) ? maxDate : Date.parse(maxDate);
    return data.filter(item => item.dueDateEpoch >= min && item.dueDateEpoch <= max);
  }

  /**
  * Grouped the services according to their product family, then the respective product's name
  *
  * Pairs the properties array and value array using _.zipObject
  */
  _groupServices(serviceScheduleServices) {
    // - An array of services is passed in

    const groupIfSameCatalogue = services =>
      _.forEach(services, (service) => {
        // - Only show the primary service
        // - Especially when there are two or more services, eg:
        // - 5 annual services for the next 5 years
        // - These additional service gets
        // - assigned to the parent service attribute
        if (!service.repeatOf) {
          _.set(service, 'myRepeatServices', _.filter(services, service2 =>
            service.serviceCatalogue.id === service2.serviceCatalogue.id
          ));
        }
      });

    // - Helper function to sort Classification
    const sortClassificatonServices = c => _.sortBy(c, [
      'serviceCatalogueH.productGroupH.displayOrder',
      'dueDateEpoch',
      'serviceCatalogueH.displayOrder',
      'occurrenceNumber']
    );

    // - Helper function to sort MMS and Statutory Services
    // -'serviceCatalogueH.productGroupH.displayOrder',
    // - 'serviceCatalogueH.productGroupH.id',
    const sortOtherServices = c => _.sortBy(c, [
      'dueDateEpoch',
      'serviceCatalogueH.displayOrder',
      'occurrenceNumber']
    );

    const advanceSort = (services) => {
      const classificationServices = sortClassificatonServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.ID]));

      const statutoryServices = sortOtherServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.STATUTORY.ID]));

      const mmsServices = sortOtherServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.MMS.ID]));

      return _.concat(classificationServices, mmsServices, statutoryServices);
    };

    const removeIfNotPrimary = services =>
      // - Remove services that has this attribute
      // - Merged to primary service
      _.reject(services, service => service.repeatOf);

    const mergeCollection = c => _.map(c, service =>
      // - I will add a property called isSelected and set to true
      _.merge(service, { isSelected: true }));

    // - I will group the services into 3 buckets :
    // - Classification, Statutory, MMS
    // - Not required to display the classification label
    // -
    // - Product Groups(Classification label is not needed)
    // -    Service Groups
    // - MMS
    // -    Service Groups
    // - Statutory
    // -    Service Groups
    const pairsCollection = c => _.toPairs(_.groupBy(c, (service) => {
      if (service.productFamilyName ===
        this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.NAME) {
        return service.productGroupName;
      }
      return service.productFamilyName;
    }));

    const mapCollection = c => _.map(c, r => _.zipObject(['productName', 'details'], r));

    return _.flow(
      groupIfSameCatalogue,
      removeIfNotPrimary,
      mergeCollection,
      advanceSort,
      pairsCollection,
      mapCollection,
    )(serviceScheduleServices);
  }

  get codicils() {
    return this._codicils;
  }

  get services() {
    return this._services;
  }

  get groupedServices() {
    return this._groupedServices;
  }

  // Fetch the list of codicils to filter comparing the list with AppSettings
  _getCodicilTypes() {
    const codicilList = [];
    _.forEach(this.serviceScheduleCodicils, (codicil) => {
      codicilList.push(
          _.assign(_.get(this._AppSettings.CODICILS_META, _.get(codicil, 'codicilType')),
        { selected: true })
        );
    });
    this.codicilTypes = _.uniqWith(codicilList, _.isEqual);
    this.codicilTypeId = _.flatMap(this.codicilTypes, type => type.ID);
  }

  // Fetch the list of services to filter
  _getServiceTypes() {
    // - need to review this logic
    this.serviceTypes = this._groupServices(
      this._services
      // - _.map(this._services, service => (_.omit(service, ['dueStatusH'])))
    );
    // There may be multiple services of the same catalogue, hence unique them.
    this.serviceIds = _.uniq(
      _.map(this.serviceScheduleServices, 'serviceCatalogueH.id')
    );
  }

  _initFiltering() {
    this._filterItems({
      minDate: this.dueDateMin,
      maxDate: this.dueDateMax,
      codicilTypeId: this.codicilTypeId,
      serviceIds: this.serviceIds,
    });
  }

  closeFilter() {
    this.filterOpen = false;
  }

  /**
   * Toggle expanding filter container
   */
  toggleFilter() {
    this.filterOpen = !this.filterOpen;
  }

  /**
   * I toggle the detailed service schedule
   * view on / off
   * @param {boolean} toShow
   */
  showDetailedMode() {
    this._$state.go(this._AppSettings.STATES.ASSET_SURVEY_PLANNER);
  }

  /**
   * This function is used to open up the modal dialog from filter box
   * on mobile view
   *
   * @param {object} evt
   */
  showFilterModal(evt) {
    this._$mdDialog.show({
      bindToController: true,
      controller: () => this,
      controllerAs: 'vm',
      fullscreen: true, // Only for -xs, -sm breakpoints.
      parent: angular.element(this._$document[0].body),
      targetEvent: evt,
      template: filterModalTemplate,
      onShowing: () => {
        this.isAccordionExpandable = false;
      },
    }).then(() => {}, () => {
      this.isAccordionExpandable = true;
    });
  }

  cancel() {
    this._$mdDialog.cancel();
  }

  clearFilterOptions() {
    this._getCodicilTypes();
    this._getServiceTypes();
    this._initFiltering();
    this.filterOpen = false;
  }

  // triggers the filtering for codicil filters
  updateFilterOptions(changedOption) {
    if (changedOption.codicilTypeId) {
      this.codicilTypeId = changedOption.codicilTypeId;
    }
    if (changedOption.serviceCatalogueIds) {
      this.serviceIds = changedOption.serviceCatalogueIds;
    }

    this._initFiltering();
  }

  // method to filter codicils
  _filterByCodicils(items, filterOption) {
    return _.filter(items,
      _.conforms({ codicilType: _.partial(_.includes, filterOption) }));
  }

  // method to filter services
  _filterByServiceIds(items, serviceIds) {
    const serviceIdMap = _.keyBy(serviceIds);

    // Filter in items that have the service ID.
    return _.filter(items, i => serviceIdMap[i.serviceCatalogueH.id] != null);
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
