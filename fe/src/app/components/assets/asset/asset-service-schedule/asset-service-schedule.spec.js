/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import AssetServiceScheduleComponent from './asset-service-schedule.component';
import AssetServiceScheduleController from './asset-service-schedule.controller';
import AssetServiceScheduleModule from './';
import AssetServiceScheduleTemplate from './asset-service-schedule.pug';

describe('AssetServiceSchedule', () => {
  let $rootScope,
    $mdDialog,
    $compile,
    $timeout,
    $window,
    makeController,
    mockState;

  const serviceScheduleCodicils = [
      { title: 'COC1', dueDateEpoch: Date.parse('2014-02-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC2', dueDateEpoch: Date.parse('2015-09-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC3', dueDateEpoch: Date.parse('2016-09-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC4', dueDateEpoch: Date.parse('2016-02-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC5', dueDateEpoch: Date.parse('2016-12-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC6', dueDateEpoch: Date.parse('2017-05-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC7', dueDateEpoch: Date.parse('2017-06-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC8', dueDateEpoch: Date.parse('2018-08-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC9', dueDateEpoch: Date.parse('2018-10-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'AI1', dueDateEpoch: Date.parse('2019-02-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
      { title: 'AI2', dueDateEpoch: Date.parse('2020-05-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
      { title: 'AI3', dueDateEpoch: Date.parse('2021-08-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
      { title: 'AI4', dueDateEpoch: Date.parse('2022-10-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
  ];

  const serviceScheduleServices = [
    {
      acomment: 'I am a mock Classification service',
      productFamilyName: 'Classification',
      serviceCatalogue: { id: 1 },
      serviceCatalogueH: {
        id: 1,
        name: 'S1',
        productCatalogue: {
          name: 'P1',
          productType: {
            id: 1,
          },
        },
      },
      dueDateEpoch: Date.parse('2018-06-11T00:00:00Z'),
      displayOrder: 1,
      isSelected: true,
    },
    {
      acomment: 'I am a mock Statutory service',
      productFamilyName: 'Statutory',
      serviceCatalogue: { id: 1 },
      serviceCatalogueH: {
        id: 2,
        name: 'S1',
        productCatalogue: {
          name: 'P2',
          productType: {
            id: 1,
          },
        },
      },
      dueDateEpoch: Date.parse('2018-06-11T00:00:00Z'),
      displayOrder: 2,
      isSelected: true,
    }, // same ID
    {
      acomment: 'I am a mock Statutory service',
      productFamilyName: 'Statutory',
      serviceCatalogue: { id: 2 },
      serviceCatalogueH: {
        id: 2,
        name: 'S3',
        productCatalogue: {
          name: 'P3',
          productType: {
            id: 3,
          },
        },
      },
      dueDateEpoch: Date.parse('2020-02-11T00:00:00Z'),
      isSelected: true,
    },
    {
      acomment: 'I am a mock MMS service',
      productFamilyName: 'MMS',
      serviceCatalogue: { id: 3 },
      serviceCatalogueH: {
        id: 3,
        name: 'S2',
        productCatalogue: {
          name: 'P4',
          productType: {
            id: 2,
          },
        },
      },
      dueDateEpoch: Date.parse('2021-11-11T00:00:00Z'),
      isSelected: true,
    },
    {
      acomment: 'I am a mock MMS service',
      productFamilyName: 'MMS',
      serviceCatalogue: { id: 4 },
      serviceCatalogueH: {
        id: 4,
        name: 'S4',
        productCatalogue: {
          name: 'P5',
          productType: {
            id: 2,
          },
        },
      },
      dueDateEpoch: Date.parse('2022-03-10T00:00:00Z'),
      isSelected: true,
    },
  ];

  const mockViewService = {
    isServiceScheduleOnDetailMode: false,
    isAssetHeaderVisible: true,
    isCollapsedAssetHeaderExpandable: true,
    setServiceScheduleViewType: () => true,
    getServiceScheduleViewType: () => true,
  };

  const mockUserService = {
    getCurrentUser: () => Promise.resolve(),
  };

  const mockScope = {
    $apply: () => null,
  };

  beforeEach(window.module(AssetServiceScheduleModule, angularMaterial));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockUtilsFactory = {
      getTabularAction: () => {},
    };
    mockState = {
      go: () => {},
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('UserService', mockUserService);
      $provide.value('ViewService', mockViewService);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('utils', mockUtilsFactory);
      $provide.value('$scope', mockScope);
      $provide.value('$state', mockState);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_,
    _$timeout_, _$window_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;
    $timeout = _$timeout_;
    $window = _$window_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('assetServiceSchedule', {}, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    beforeEach(() => {
      controller = makeController(
        {
          asset: {
            isAccessibleMASTAsset: true,
          },
          serviceMinDate: new Date(),
          serviceMaxDate: new Date(),
          serviceScheduleCodicils,
          serviceScheduleServices,
        });
    });

    it('initializes just fine', () => {
      expect(controller._storedDateRange).toBeDefined();
      expect(controller.serviceScheduleCodicils).not.toBeNull();
      expect(controller.serviceScheduleServices).not.toBeNull();
      expect(controller.serviceIds).toEqual([1, 2, 3, 4]);
      expect(controller.isDetailedMode).toBe(undefined);
      expect(controller.isAccordionExpandable).toBe(true);
      expect(controller.currentUser).toEqual({});
    });

    it.async('_getCurrentUser, gets the current user from the User Service', async () => {
      spyOn(controller._UserService, 'getCurrentUser').and.returnValue({ id: 1 });
      spyOn(controller._$scope, '$apply');

      await controller._getCurrentUser();
      expect(controller._UserService.getCurrentUser).toHaveBeenCalled();
      expect(controller.currentUser).toEqual({ id: 1 });
      expect(controller._$scope.$apply).toHaveBeenCalled();
    });

    it('checks if codicils exist and return the proper value', () => {
      expect(controller.codicils[0]).toEqual(serviceScheduleCodicils[0]);
      expect(controller.codicils[9]).toEqual(serviceScheduleCodicils[9]);
      expect(controller.codicils.length).toEqual(13);
    });

    it('checks if services exist and return the proper value', () => {
      expect(controller.services[0].id).toEqual(serviceScheduleServices[0].id);
      expect(controller.services.length).toEqual(5);
    });

    it('checks if services are grouped properly', () => {
      const groupedServices = controller._groupServices(controller.services);
      expect(controller.groupedServices).toEqual(groupedServices);
      expect(controller.groupedServices.length).toEqual(groupedServices.length);
    });

    it('does the _filterByDates correctly', () => {
      const minDate = '2017-01-01';
      const maxDate = '2019-12-31';

      const filtered = controller._filterByDates(controller.codicils, minDate, maxDate);
      expect(filtered.length).toEqual(5);
      expect(filtered[0].title).toEqual(serviceScheduleCodicils[5].title);
      expect(filtered[4].title).toEqual(serviceScheduleCodicils[9].title);

      const filteredGroupedServices = controller._groupServices(
        controller._filterByDates(controller.services, minDate, maxDate));
      expect(filteredGroupedServices.length).toEqual(2);
      expect(filteredGroupedServices[0].details.length).toEqual(1);
      expect(filteredGroupedServices[0].details[0].serviceCatalogueH.name)
        .toEqual(serviceScheduleServices[0].serviceCatalogueH.name);
    });

    it('does the _filterItems correctly', () => {
      let minDate = '2017-01-01';
      let maxDate = '2019-12-31';
      controller._filterItems({
        minDate,
        maxDate,
        codicilTypeId: ['COC', 'AI'],
        serviceIds: [1, 2, 3, 4],
      });
      expect(controller.codicils.length).toEqual(5);
      expect(controller.codicils[0].title).toEqual(serviceScheduleCodicils[5].title);
      expect(controller.codicils[4].title).toEqual(serviceScheduleCodicils[9].title);
      expect(controller.groupedServices.length).toEqual(2);
      expect(controller.groupedServices[0].details.length).toEqual(1);
      expect(controller.groupedServices[0].details[0].serviceCatalogueH.name)
        .toEqual(serviceScheduleServices[0].serviceCatalogueH.name);

      minDate = '2019-01-01';
      maxDate = '2022-12-31';
      controller._filterItems({
        minDate,
        maxDate,
        codicilTypeId: ['COC', 'AI'],
        serviceIds: [1, 2, 3, 4],
      });
      expect(controller.codicils.length).toEqual(4);
      expect(controller.codicils[0].title).toEqual(serviceScheduleCodicils[9].title);
      expect(controller.codicils[3].title).toEqual(serviceScheduleCodicils[12].title);
      expect(controller.groupedServices.length).toEqual(2);
      expect(
        _.map(
          controller.groupedServices,
          groupedService => groupedService.details[0].serviceCatalogueH.name)
        )
        .toEqual(['S2', 'S3']);
    });

    it('calls _filterItems when date picker is updated', () => {
      const filters = {
        minDate: '2017-01-01',
        maxDate: '2019-12-31',
        codicilTypeId: ['COC', 'AI'],
      };

      spyOn(controller, '_filterItems');
      controller.onDatePickerChange(filters);
      expect(controller._filterItems).toHaveBeenCalledWith(filters);
    });

    it('_getCodicilTypes should give a list for filters', () => {
      const mockCodicilTypes = [
        { ID: 'COC', NAME: 'cd-condition-of-class', selected: true },
        { ID: 'AI', NAME: 'cd-actionable-item', selected: true },
      ];
      const mockCodicilTypeId = ['COC', 'AI'];

      controller._codicils = serviceScheduleCodicils;
      controller._getCodicilTypes();
      expect(controller.codicilTypes).toEqual(mockCodicilTypes);
      expect(controller.codicilTypeId).toEqual(mockCodicilTypeId);
    });

    it('_filterByCodicils should filter and return filtered list', () => {
      const mockCodicils = serviceScheduleCodicils;

      expect(controller._filterByCodicils(mockCodicils, ['COC', 'AI']).length).toEqual(13);
      expect(controller._filterByCodicils(mockCodicils, ['COC']).length).toEqual(9);
      expect(controller._filterByCodicils(mockCodicils, ['AI']).length).toEqual(4);
      expect(controller._filterByCodicils(mockCodicils, []).length).toEqual(0);
    });

    it('showDetailedMode should behave correctly', () => {
      const stateGoSpy = spyOn(mockState, 'go');
      controller.showDetailedMode();
      expect(stateGoSpy).toHaveBeenCalled();
    });

    it('_getServiceTypes should give a list for filters', () => {
      const mockServices = [
        {
          productName: 'P1',
          details: [
            {
              acomment: 'I am a mock Classification service',
              productFamilyName: 'Classification',
              serviceCatalogueH: {
                id: 1,
                name: 'S1',
                productCatalogue: {
                  name: 'P1',
                  productType: {
                    id: 1,
                  },
                },
              },
              dueDateEpoch: Date.parse('2018-06-11T00:00:00Z'),
              displayOrder: 1,
              isSelected: true,
            },
            {
              acomment: 'I am a mock Classification service',
              productFamilyName: 'Classification',
              serviceCatalogueH: {
                id: 2,
                name: 'S2',
                productCatalogue: {
                  name: 'S2',
                  productType: {
                    id: 1,
                  },
                },
              },
              dueDateEpoch: Date.parse('2018-06-11T00:00:00Z'),
              displayOrder: 2,
              isSelected: true,
            },
            {
              acomment: 'I am a mock Classification service',
              productFamilyName: 'Classification',
              serviceCatalogueH: {
                id: 4,
                name: 'S4',
                productCatalogue: {
                  name: 'P1',
                  productType: {
                    id: 1,
                  },
                },
              },
              dueDateEpoch: Date.parse('2022-03-10T00:00:00Z'),
              isSelected: true,
            },
          ],
        },
        {
          productName: 'P2',
          details: [
            {
              acomment: 'I am a mock Statutory service',
              productFamilyName: 'Statutory',
              serviceCatalogueH: {
                id: 2,
                name: 'S2',
                productCatalogue: {
                  name: 'P2',
                  productType: {
                    id: 3,
                  },
                },
              },
              dueDateEpoch: Date.parse('2020-02-11T00:00:00Z'),
              isSelected: true,
            },
          ],
        },
        {
          productName: 'P3',
          details: [
            {
              acomment: 'I am a mock MMS service',
              productFamilyName: 'MMS',
              serviceCatalogueH: {
                id: 3,
                name: 'S3',
                productCatalogue: {
                  name: 'P3',
                  productType: {
                    id: 3,
                  },
                },
              },
              dueDateEpoch: Date.parse('2021-11-11T00:00:00Z'),
              isSelected: true,
            },
          ],
        },
      ];
      controller._getServiceTypes();
      // Filters are also returning product groups
      expect(controller.serviceTypes.length).toEqual(3);
      expect(controller.serviceIds).toEqual([1, 2, 3, 4]);
    });

    it('_filterByServiceIds should filter and return filtered list', () => {
      expect(controller._filterByServiceIds(serviceScheduleServices,
        [1, 1, 2, 3, 4]).length).toEqual(5); // able to handle duplicate service IDs
      expect(controller._filterByServiceIds(serviceScheduleServices, [1, 2, 3]).length).toEqual(4);
      expect(controller._filterByServiceIds(serviceScheduleServices, [1]).length).toEqual(1);
      expect(controller._filterByServiceIds(serviceScheduleServices, []).length).toEqual(0);
    });

    it.async('showFilterModal() should popup a fullscreen mdDialog box', async() => {
      const fakePromise = () => 'Promise'; // Cheating, I know, ma bad.
      spyOn($mdDialog, 'show').and.returnValue(new Promise(fakePromise));

      controller.showFilterModal();

      expect($mdDialog.show).toHaveBeenCalled();
    });

    it('closeFilter should returning false', () => {
      expect(controller.filterOpen).toBe(false);

      controller.filterOpen = true;
      controller.closeFilter();
      expect(controller.filterOpen).toBe(false);
      expect(controller.filterOpen).not.toBe(true);
    });

    it('toggleFilter returning the correct boolean', () => {
      expect(controller.filterOpen).toBe(false);

      controller.toggleFilter();
      expect(controller.filterOpen).toBe(true);
    });

    it('updateFilterOptions calls the filterItems and update timeline method', () => {
      const mockFilterChange = {};
      mockFilterChange.codicilTypeId = ['COC'];

      spyOn(controller, '_filterItems');
      controller.dueDateMin = '2017-01-01';
      controller.dueDateMax = '2019-12-31';

      controller.updateFilterOptions(mockFilterChange);
      expect(controller.codicilTypeId).toEqual(mockFilterChange.codicilTypeId);
      expect(controller.serviceIds).toEqual([1, 2, 3, 4]);
      expect(controller._filterItems).toHaveBeenCalledWith(
        {
          minDate: '2017-01-01',
          maxDate: '2019-12-31',
          codicilTypeId: ['COC'],
          serviceIds: [1, 2, 3, 4],
        });

      mockFilterChange.serviceCatalogueIds = [1, 2];
      controller.updateFilterOptions(mockFilterChange);
      expect(controller.serviceIds).toEqual([1, 2]);
    });

    it('_initFiltering to call filterItems functions', () => {
      spyOn(controller, '_filterItems');
      controller._initFiltering();
      expect(controller._filterItems).toHaveBeenCalled();
    });

    it('clearFilterOptions resets the filtered items', () => {
      spyOn(controller, '_getCodicilTypes');
      spyOn(controller, '_getServiceTypes');
      spyOn(controller, '_initFiltering');
      controller.clearFilterOptions();
      expect(controller._getCodicilTypes).toHaveBeenCalled();
      expect(controller._getServiceTypes).toHaveBeenCalled();
      expect(controller._initFiltering).toHaveBeenCalled();
      expect(controller.filterOpen).toBe(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetServiceScheduleComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetServiceScheduleTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetServiceScheduleController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.serviceScheduleCodicils = serviceScheduleCodicils;
      scope.asset = {
        isAccessibleLRAsset: true,
      };
      scope.AppSettings = AppSettings;
      element = angular.element(
        '<asset-service-schedule service-schedule-codicils="serviceScheduleCodicils" asset="asset"><asset-service-schedule/>'); // eslint-disable-line max-len
      element = $compile(element)(scope);
      scope.$apply();

      controller = element.controller('AssetServiceSchedule');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
