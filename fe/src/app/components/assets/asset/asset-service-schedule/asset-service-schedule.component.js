import './asset-service-schedule.scss';
import controller from './asset-service-schedule.controller';
import template from './asset-service-schedule.pug';

export default {
  bindings: {
    asset: '<',
    isDetailedMode: '<?',
    serviceMinDate: '<',
    serviceMaxDate: '<',
    serviceScheduleCodicils: '<',
    serviceScheduleServices: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
