import './statutory-deficiencies.scss';
import controller from './statutory-deficiencies.controller';
import template from './statutory-deficiencies.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
