/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import StatutoryDeficienciesComponent from './statutory-deficiencies.component';
import StatutoryDeficienciesController from './statutory-deficiencies.controller';
import StatutoryDeficienciesModule from './';
import StatutoryDeficienciesTemplate from './statutory-deficiencies.pug';

describe('StatutoryDeficiencies', () => {
  let $rootScope,
    $compile,
    mockCodicilsListingFactory,
    makeController;

  const fakeAsset = {
    id: 1,
    description: 'fake asset',
  };
  const codicils = [
    {
      id: 1,
      imposedDate: '2017-06-11T00:00:00Z',
      dueDate: '2017-06-11T00:00:00Z',
    },
    {
      id: 2,
      imposedDate: '2016-01-14T00:00:00Z',
      dueDate: '2016-01-14T00:00:00Z',
    },
  ];

  beforeEach(window.module(StatutoryDeficienciesModule));
  beforeEach(() => {
    mockCodicilsListingFactory = {
      getFilterOptions: () => {},
    };
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('CodicilsListingFactory', mockCodicilsListingFactory);
      $provide.value('CodicilService', {});
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('statutoryDeficiencies', {
          AppSettings,
          CodicilsListingFactory: mockCodicilsListingFactory,
        }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ asset: fakeAsset, codicils });
      expect(controller.asset).toEqual(fakeAsset);
      expect(controller.codicils).toEqual(codicils);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = StatutoryDeficienciesComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(StatutoryDeficienciesTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(StatutoryDeficienciesController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<statutory-deficiencies foo="bar"><statutory-deficiencies/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('StatutoryDeficiencies');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
