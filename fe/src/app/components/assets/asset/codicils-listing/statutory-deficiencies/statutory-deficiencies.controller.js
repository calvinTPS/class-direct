import * as _ from 'lodash';
import CodicilBase from '../codicil-base';

export default class StatutoryDeficienciesController extends CodicilBase {
  /* @ngInject */
  constructor(
    AppSettings,
    CodicilsListingFactory,
    CodicilService,
    ViewService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.STATUTORY_DEFICIENCY;

    this.searchByList = _.cloneDeep(this._AppSettings.NOTES_AND_ACTIONS_SEARCH_PARAM
      .STATUTORY_DEFICIENCIES_SEARCH_BY_LIST);
    this.sortByList = this._AppSettings.STATUTORY_DEFICIENCIES_SORT;
    this.filterOptions = this._CodicilsListingFactory.getFilterOptions(
      this.codicils, _.cloneDeep(this._AppSettings.STATUTORY_DEFICIENCIES_FILTER_OPTIONS));
    this.action = {
      stateName: this._AppSettings.STATES.STATUTORY_DEFICIENCIES_DETAILS,
      params: {
        assetId: _.get(this.asset, 'id'),
      },
      evalParams: {
        deficiencyId: 'id',
      },
    };
  }
}
