import angular from 'angular';
import StatutoryDeficienciesComponent from './statutory-deficiencies.component';
import uiRouter from 'angular-ui-router';

export default angular.module('statutoryDeficiencies', [
  uiRouter,
])
.component('statutoryDeficiencies', StatutoryDeficienciesComponent)
.name;
