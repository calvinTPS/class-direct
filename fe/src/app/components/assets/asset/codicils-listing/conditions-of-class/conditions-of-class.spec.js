/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import CodicilsListingFactory from 'app/components/assets/asset/codicils-listing/codicils-listing.factory';
import ConditionsOfClassComponent from './conditions-of-class.component';
import ConditionsOfClassController from './conditions-of-class.controller';
import ConditionsOfClassModule from './';
import ConditionsOfClassTemplate from './conditions-of-class.pug';

describe('ConditionOfClass', () => {
  let $rootScope,
    $compile,
    $filter,
    mockCodicilsListingFactory,
    makeController;

  beforeEach(window.module(ConditionsOfClassModule));
  beforeEach(() => {
    mockCodicilsListingFactory = {
      getFilterOptions: () => {},
    };
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('CodicilsListingFactory', mockCodicilsListingFactory);
      $provide.value('$mdMedia', {});
      $provide.value('CodicilService', {});
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$filter_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $filter = _$filter_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('conditionsOfClass', {
          AppSettings,
          CodicilsListingFactory: mockCodicilsListingFactory,
        }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const fakeAsset = {
        id: 1,
        description: 'fake asset',
      };
      const codicils = [
        {
          id: 1,
          imposedDate: '2017-06-11T00:00:00Z',
          dueDate: '2017-06-11T00:00:00Z',
        },
        {
          id: 2,
          imposedDate: '2016-01-14T00:00:00Z',
          dueDate: '2016-01-14T00:00:00Z',
        },
      ];
      const controller = makeController({ asset: fakeAsset, codicils });
      expect(controller.asset).toEqual(fakeAsset);
      expect([...controller.codicils]).toEqual(
        $filter('orderBy')([
          {
            id: 1,
            imposedDate: '2017-06-11T00:00:00Z',
            dueDate: '2017-06-11T00:00:00Z',
          },
          {
            id: 2,
            imposedDate: '2016-01-14T00:00:00Z',
            dueDate: '2016-01-14T00:00:00Z',
          },
        ], 'dueDate')
      );
      expect(controller.isRevisionsShown).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ConditionsOfClassComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ConditionsOfClassTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ConditionsOfClassController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<condition-of-class foo="bar"><condition-of-class/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ConditionOfClass');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
