import angular from 'angular';
import conditionsOfClassComponent from './conditions-of-class.component';

export default angular.module('conditionsOfClass', [
])
.component('conditionsOfClass', conditionsOfClassComponent)
.name;
