import './conditions-of-class.scss';
import controller from './conditions-of-class.controller';
import template from './conditions-of-class.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
