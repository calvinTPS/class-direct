import * as _ from 'lodash';
import CodicilBase from '../codicil-base';

export default class ConditionsOfClassController extends CodicilBase {
  /* @ngInject */
  constructor(
    $filter,
    $mdMedia,
    AppSettings,
    CodicilsListingFactory,
    CodicilService,
    ViewService,
  ) {
    // Default orderBy due date ascending
    super(arguments);
  }

  $onInit() {
    this.searchByList = _.cloneDeep(this._AppSettings.NOTES_AND_ACTIONS_SEARCH_PARAM
      .COC_SEARCH_BY_LIST);
    this.filterOptions = this._CodicilsListingFactory
      .getFilterOptions(this.codicils, _.cloneDeep(this._AppSettings.CODICILS_FILTER_OPTIONS));
    this.action = {
      stateName: this._AppSettings.STATES.CONDITIONS_OF_CLASS_DETAILS,
      params: {
        assetId: this.asset.id,
      },
      evalParams: {
        codicilId: 'id',
      },
    };

    // Default orderBy due date ascending
    this.codicils = this._$filter('orderBy')(this.codicils, 'dueDate');

    this.isRevisionsShown = false;
    this.templateLocation = this._AppSettings.RIDER_TEMPLATES.CONDITION_OF_CLASS;
  }
}
