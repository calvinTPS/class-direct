import angular from 'angular';
import mncnsComponent from './mncns.component';

export default angular.module('mncns', [
])
.component('mncns', mncnsComponent)
.name;
