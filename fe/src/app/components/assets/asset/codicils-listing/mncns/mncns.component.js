import './mncns.scss';
import controller from './mncns.controller';
import template from './mncns.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
