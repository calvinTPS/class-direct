/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import MncnsComponent from './mncns.component';
import MncnsController from './mncns.controller';
import MncnsModule from './';
import MncnsTemplate from './mncns.pug';

describe('Mncns', () => {
  let $rootScope,
    $compile,
    $filter,
    mockCodicilsListingFactory,
    makeController;

  beforeEach(window.module(MncnsModule));
  beforeEach(() => {
    mockCodicilsListingFactory = {
      getFilterOptions: () => { },
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('CodicilsListingFactory', mockCodicilsListingFactory);
      $provide.value('$mdMedia', {});
      $provide.value('CodicilService', {});
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$filter_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $filter = _$filter_;

    makeController =
      (bindings = {}) => {
        const controller = $componentController('mncns',
          {
            AppSettings,
            CodicilsListingFactory: mockCodicilsListingFactory,
          }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      // const fakeAsset = {
      //   id: 1,
      //   description: 'fake asset',
      // };
      // const codicils = [
      //   {
      //     id: 1,
      //     imposedDate: '2017-06-11T00:00:00Z',
      //     dueDate: '2017-06-11T00:00:00Z',
      //   },
      //   {
      //     id: 2,
      //     imposedDate: '2016-01-14T00:00:00Z',
      //     dueDate: '2016-01-14T00:00:00Z',
      //   },
      // ];
      // const controller = makeController({ asset: fakeAsset, codicils });
      // expect(controller.asset).toEqual(fakeAsset);
      // expect([...controller.codicils]).toEqual(
      //   $filter('orderBy')([
      //     {
      //       id: 1,
      //       imposedDate: '2017-06-11T00:00:00Z',
      //       dueDate: '2017-06-11T00:00:00Z',
      //     },
      //     {
      //       id: 2,
      //       imposedDate: '2016-01-14T00:00:00Z',
      //       dueDate: '2016-01-14T00:00:00Z',
      //     },
      //   ], 'dueDate')
      // );
      // expect(controller.isRevisionsShown).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = MncnsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(MncnsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(MncnsController);
    });
  });
});
