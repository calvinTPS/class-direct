import * as _ from 'lodash';
import CodicilBase from '../codicil-base';

export default class MncnsController extends CodicilBase {
  /* @ngInject */
  constructor(
    $filter,
    AppSettings,
    CodicilsListingFactory,
    CodicilService,
    ViewService
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.MNCN;
    this.searchByList = _.cloneDeep(
      this._AppSettings.NOTES_AND_ACTIONS_SEARCH_PARAM
      .MNCNS_SEARCH_BY_LIST
    );
    this.filterOptions = _.cloneDeep(this._AppSettings.MNCNS_FILTER_OPTIONS);

    this.action = {
      stateName: this._AppSettings.STATES.MNCNS_DETAILS,
      params: { assetId: this.asset.id },
      evalParams: { codicilId: 'id' },
    };

    // Default orderBy due date ascending
    this._codicils = this._$filter('orderBy')(this._codicils, 'dueDate');
  }

}
