/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import ActionableItemsComponent from './actionable-items.component';
import ActionableItemsController from './actionable-items.controller';
import ActionableItemsModule from './';
import ActionableItemsTemplate from './actionable-items.pug';
import AppSettings from 'app/config/project-variables.js';

describe('ActionableItems', () => {
  let $rootScope,
    $compile,
    $filter,
    mockCodicilsListingFactory,
    makeController;

  beforeEach(window.module(ActionableItemsModule));
  beforeEach(() => {
    mockCodicilsListingFactory = {
      getFilterOptions: () => {},
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('CodicilsListingFactory', mockCodicilsListingFactory);
      $provide.value('$mdMedia', {});
      $provide.value('CodicilService', {});
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$filter_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $filter = _$filter_;

    makeController =
      (bindings = {}) => {
        const controller = $componentController('actionableItems',
          {
            AppSettings,
            CodicilsListingFactory: mockCodicilsListingFactory,
          }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const fakeAsset = {
        id: 1,
        description: 'fake asset',
      };
      const codicils = [
        {
          id: 1,
          imposedDate: '2017-06-11T00:00:00Z',
          dueDate: '2017-06-11T00:00:00Z',
        },
        {
          id: 2,
          imposedDate: '2016-01-14T00:00:00Z',
          dueDate: '2016-01-14T00:00:00Z',
        },
      ];
      const controller = makeController({ asset: fakeAsset, codicils });
      expect(controller.asset).toEqual(fakeAsset);
      expect([...controller.codicils]).toEqual(
        $filter('orderBy')([
          {
            id: 1,
            imposedDate: '2017-06-11T00:00:00Z',
            dueDate: '2017-06-11T00:00:00Z',
          },
          {
            id: 2,
            imposedDate: '2016-01-14T00:00:00Z',
            dueDate: '2016-01-14T00:00:00Z',
          },
        ], 'dueDate')
      );
      expect(controller.isRevisionsShown).toEqual(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ActionableItemsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ActionableItemsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ActionableItemsController);
    });
  });
});
