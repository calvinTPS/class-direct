import * as _ from 'lodash';
import CodicilBase from '../codicil-base';

export default class ActionableItemsController extends CodicilBase {
  /* @ngInject */
  constructor(
    $filter,
    $mdMedia,
    AppSettings,
    CodicilsListingFactory,
    CodicilService,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.ACTIONABLE_ITEM;
    this.searchByList = _.cloneDeep(this._AppSettings.NOTES_AND_ACTIONS_SEARCH_PARAM
      .ACTIONABLE_ITEM_SEARCH_BY_LIST);
    this.filterOptions = this._CodicilsListingFactory
      .getFilterOptions(this.codicils, _.cloneDeep(this._AppSettings.CODICILS_FILTER_OPTIONS));

    this.action = {
      stateName: this._AppSettings.STATES.ACTIONABLE_ITEMS_DETAILS,
      params: { assetId: this.asset.id },
      evalParams: { codicilId: 'id' },
    };

    // Default orderBy due date ascending
    this._codicils = this._$filter('orderBy')(this._codicils, 'dueDate');

    this.isRevisionsShown = false;
  }
}
