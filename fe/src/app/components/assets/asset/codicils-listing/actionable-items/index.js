import actionableItemsComponent from './actionable-items.component';
import angular from 'angular';

export default angular.module('actionableItems', [
])
.component('actionableItems', actionableItemsComponent)
.name;
