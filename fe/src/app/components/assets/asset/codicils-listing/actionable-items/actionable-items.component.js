import './actionable-items.scss';
import controller from './actionable-items.controller';
import template from './actionable-items.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
