/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import AssetNotesComponent from './asset-notes.component';
import AssetNotesController from './asset-notes.controller';
import AssetNotesModule from './';
import AssetNotesTemplate from './asset-notes.pug';
import CodicilsListingFactory from 'app/components/assets/asset/codicils-listing/codicils-listing.factory';

describe('AssetNotes', () => {
  let $rootScope,
    $compile,
    $filter,
    mockCodicilsListingFactory,
    makeController;

  beforeEach(window.module(AssetNotesModule));
  beforeEach(() => {
    mockCodicilsListingFactory = {
      getFilterOptions: () => {},
    };
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('CodicilsListingFactory', mockCodicilsListingFactory);
      $provide.value('CodicilService', {});
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$filter_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $filter = _$filter_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('assetNotes', {
          AppSettings,
          CodicilsListingFactory: mockCodicilsListingFactory,
        }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const fakeAsset = {
        id: 1,
        description: 'fake asset',
      };
      const codicils = [
        {
          id: 1,
          categoryH: { name: 'Class' },
          imposedDate: '2016-01-14T00:00:00Z',
        },
        {
          id: 2,
          categoryH: { name: 'Class' },
          imposedDate: '2016-06-09T00:00:00Z',
        },
      ];
      const controller = makeController({ asset: fakeAsset, codicils });
      expect(controller.asset).toEqual(fakeAsset);

      expect([...controller.codicils]).toEqual(
        $filter('orderBy')([
          {
            id: 1,
            categoryH: { name: 'Class' },
            imposedDate: '2016-01-14T00:00:00Z',
          },
          {
            id: 2,
            categoryH: { name: 'Class' },
            imposedDate: '2016-06-09T00:00:00Z',
          },
        ], 'imposedDate', true)
      );
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetNotesComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetNotesTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetNotesController);
    });
  });
});
