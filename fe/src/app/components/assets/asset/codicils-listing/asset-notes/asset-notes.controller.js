import * as _ from 'lodash';
import CodicilBase from '../codicil-base';

export default class AssetNotesController extends CodicilBase {
  /* @ngInject */
  constructor(
    $filter,
    AppSettings,
    CodicilsListingFactory,
    CodicilService,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.ASSET_NOTE;
    this.searchByList = _.cloneDeep(this._AppSettings.NOTES_AND_ACTIONS_SEARCH_PARAM
      .ASSET_NOTES_SEARCH_BY_LIST);

    const filterOptions = _.cloneDeep(this._AppSettings.CODICILS_FILTER_OPTIONS);
    // TODO: imposedDateEpoch is not available in BE yet.
    // Thus, filter by `Imposed Date` won't work for now.
    this.filterOptions = this._CodicilsListingFactory
      .getFilterOptions(this.codicils, _.filter(filterOptions, { key: 'imposedDateEpoch' }));

    // Default orderBy imposed date descending
    this._codicils = this._$filter('orderBy')(this._codicils, 'imposedDate', true);

    this.action = {
      stateName: this._AppSettings.STATES.ASSET_NOTES_DETAILS,
      params: { assetId: this.asset.id },
      evalParams: { codicilId: 'id' },
    };
  }
}
