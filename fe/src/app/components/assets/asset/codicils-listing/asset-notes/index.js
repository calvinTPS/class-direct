import angular from 'angular';
import assetNotesComponent from './asset-notes.component';

export default angular.module('assetNotes', [
])
.component('assetNotes', assetNotesComponent)
.name;
