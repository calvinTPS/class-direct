import './asset-notes.scss';
import controller from './asset-notes.controller';
import template from './asset-notes.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
