/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import AssetModule from '../asset';
import CodicilModel from 'app/common/models/codicil/codicil';
import CodicilsListingComponent from './codicils-listing.component';
import CodicilsListingController from './codicils-listing.controller';
import CodicilsListingFactory from './codicils-listing.factory';
import CodicilsListingModule from './codicils-listing';
import CodicilsListingTemplate from './codicils-listing.pug';

describe('CodicilsListing', () => {
  let $rootScope,
    $compile,
    $injector,
    $state,
    $stateParams,
    makeController;

  const mockCodicilService = {
    get: assetId => Promise.resolve([]),
  };
  const mockFilterOption = [
    {
      type: 'radiobuttons',
      title: 'cd-status',
      key: 'status',
      options: [
        {
          label: 'Open',
          value: 'Open',
          selected: true,
        },
        {
          label: 'Deleted',
          value: 'Deleted',
          selected: false,
        },
        {
          label: 'Cancelled',
          value: 'Cancelled',
          selected: false,
        },
      ],
    },
    {
      type: 'checkboxes',
      title: 'cd-category',
      key: 'category',
      isAll: true,
      options: [
        { name: 'Class', value: 'Class', selected: false },
        { name: 'Statutory', value: 'Statutory', selected: false },
      ],
    },
  ];
  const mockAdditionalFilterOption = [
    {
      type: 'radiobuttons',
      title: 'cd-status',
      key: 'status',
      options: [
        {
          label: 'Open',
          value: 'Open',
          selected: true,
        },
        {
          label: 'Deleted',
          value: 'Deleted',
          selected: false,
        },
        {
          label: 'Cancelled',
          value: 'Cancelled',
          selected: false,
        },
      ],
    },
    {
      type: 'checkboxes',
      title: 'cd-category',
      key: 'category',
      isAll: true,
      options: [
        { name: 'Class', value: 'Class', selected: false },
        { name: 'Statutory', value: 'Statutory', selected: false },
      ],
    },
    {
      type: 'daterange',
      title: 'cd-impose-date',
      key: 'imposedDateEpoch',
    },
    {
      type: 'daterange',
      title: 'cd-due-date',
      key: 'dueDateEpoch',
    },
  ];
  const mockCodicils = [
    {
      id: 1,
      codicilType: 'COC',
      categoryH: {
        id: 1,
        deleted: false,
        name: 'Class',
        description: 'ANC',
        codicilTypeId: {
          id: 3,
          _id: '3',
        },
        _id: '1',
      },
      statusH: {
        id: 14,
        deleted: false,
        name: 'Open',
        description: null,
        _id: '14',
      },
    },
    {
      id: 2,
      codicilType: 'COC',
      categoryH: {
        id: 1,
        deleted: false,
        name: 'Class',
        description: 'ANC',
        codicilTypeId: {
          id: 3,
          _id: '3',
        },
        _id: '1',
      },
      statusH: {
        id: 15,
        deleted: false,
        name: 'Deleted',
        description: null,
        _id: '15',
      },
    },
    {
      id: 3,
      codicilType: 'COC',
      categoryH: {
        id: 1,
        deleted: false,
        name: 'Class',
        description: 'ANC',
        codicilTypeId: {
          id: 3,
          _id: '3',
        },
        _id: '1',
      },
      statusH: {
        id: 16,
        deleted: false,
        name: 'Cancelled',
        description: null,
        _id: '16',
      },
    },
    {
      id: 5,
      codicilType: 'COC',
      categoryH: {
        id: 1,
        deleted: false,
        name: 'Class',
        description: 'ANC',
        codicilTypeId: {
          id: 3,
          _id: '3',
        },
        _id: '1',
      },
      statusH: {
        id: 15,
        deleted: false,
        name: 'Deleted',
        description: null,
        _id: '15',
      },
    },
    {
      id: 7,
      codicilType: 'COC',
      categoryH: {
        id: 1,
        deleted: false,
        name: 'Class',
        description: 'ANC',
        codicilTypeId: {
          id: 3,
          _id: '3',
        },
        _id: '1',
      },
      statusH: {
        id: 14,
        deleted: false,
        name: 'Open',
        description: null,
        _id: '14',
      },
    },
    {
      id: 21,
      codicilType: 'COC',
      categoryH: {
        id: 2,
        deleted: false,
        name: 'Statutory',
        description: 'ANS',
        codicilTypeId: {
          id: 3,
          _id: '3',
        },
        _id: '2',
      },
      statusH: {
        id: 14,
        deleted: false,
        name: 'Open',
        description: null,
        _id: '14',
      },
    },
  ];

  beforeEach(window.module(
    AssetModule,
    CodicilsListingModule
  ));

  beforeEach(() => {
    const mockTranslateFilter = value => value;

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('CodicilService', mockCodicilService);
    });
  });

  beforeEach(inject(
    (
      _$rootScope_,
      _$compile_,
      _$injector_,
      _$state_,
      _$stateParams_,
      $componentController
    ) => {
      $rootScope = _$rootScope_;
      $compile = _$compile_;
      $injector = _$injector_;
      $state = _$state_;
      $stateParams = _$stateParams_;

      makeController =
      (bindings = {}) => $componentController('codicilsListing', {
        $stateParams,
        AssetService: mockCodicilService,
      }, bindings);
    }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Routes', () => {
    it.async('should resolve coc via get coc', async() => {
      $stateParams.assetId = 1;
      spyOn(mockCodicilService, 'get');
      await $injector.invoke(
        $state.get('asset.codicilsListing.conditionsOfClass').resolve.codicils
      );

      expect(mockCodicilService.get).toHaveBeenCalledTimes(1);
      expect(mockCodicilService.get).toHaveBeenCalledWith(1, 'cocs');
    });

    it.async('should resolve actionable items via get ai', async() => {
      $stateParams.assetId = 1;
      spyOn(mockCodicilService, 'get');

      await $injector.invoke(
        $state.get('asset.codicilsListing.actionableItems').resolve.codicils
      );

      expect(mockCodicilService.get).toHaveBeenCalledTimes(1);
      expect(mockCodicilService.get).toHaveBeenCalledWith(1, 'actionable-items');
    });

    it.async('should resolve asset notes via get an', async() => {
      $stateParams.assetId = 1;
      spyOn(mockCodicilService, 'get');

      await $injector.invoke(
        $state.get('asset.codicilsListing.assetNotes').resolve.codicils
      );

      expect(mockCodicilService.get).toHaveBeenCalledTimes(1);
      expect(mockCodicilService.get).toHaveBeenCalledWith(1, 'asset-notes');
    });
  });

  describe('Controller', () => {
    it('has a name property', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Factory', () => {
    let codicils = [];

    beforeEach(() => {
      codicils = _.cloneDeep(mockCodicils)
        .map(codicil => Reflect.construct(CodicilModel, [codicil]));
    });

    it('getStatuses should generate the filter status radio options based on codicils', () => {
      const statusOptions = CodicilsListingFactory().getStatuses(codicils);
      expect(statusOptions).toEqual(mockFilterOption[0].options);
    });

    it('getCategories should generate the filter status radio options based on codicils', () => {
      const categoryOptions = CodicilsListingFactory().getCategories(codicils);
      expect(categoryOptions).toEqual(mockFilterOption[1].options);
    });

    it('getFilterOptions should generate the filter options based on codicils', () => {
      const filterOptions = CodicilsListingFactory().getFilterOptions(codicils);
      expect(filterOptions).toEqual(mockFilterOption);
    });

    it('getFilterOptions should generate with additionalOptions', () => {
      const additionalOptions = [
        {
          type: 'daterange',
          title: 'cd-impose-date',
          key: 'imposedDateEpoch',
        },
        {
          type: 'daterange',
          title: 'cd-due-date',
          key: 'dueDateEpoch',
        },
      ];
      const filterOptions = CodicilsListingFactory()
        .getFilterOptions(codicils, additionalOptions);
      expect(filterOptions).toEqual(mockAdditionalFilterOption);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CodicilsListingComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CodicilsListingTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CodicilsListingController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      scope.asset = { id: 1 };
      element = angular.element('<codicils-listing asset="asset"><codicils-listing/>');
      element = $compile(element)(scope);
      scope.$apply();

      controller = element.controller('CodicilsListing');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
