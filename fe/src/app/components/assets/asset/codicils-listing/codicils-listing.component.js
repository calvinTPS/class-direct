import './codicils-listing.scss';
import controller from './codicils-listing.controller';
import template from './codicils-listing.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
    currentUser: '<',
  },
  controllerAs: 'vm',
  controller,
  restrict: 'E',
  template,
};
