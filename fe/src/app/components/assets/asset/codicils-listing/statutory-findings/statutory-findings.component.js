import './statutory-findings.scss';
import controller from './statutory-findings.controller';
import template from './statutory-findings.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
