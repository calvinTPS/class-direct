import angular from 'angular';
import StatutoryFindingsComponent from './statutory-findings.component';
import uiRouter from 'angular-ui-router';

export default angular.module('statutoryFindings', [
  uiRouter,
])
.component('statutoryFindings', StatutoryFindingsComponent)
.name;
