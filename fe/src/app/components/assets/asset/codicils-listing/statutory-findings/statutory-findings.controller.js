import * as _ from 'lodash';
import CodicilBase from '../codicil-base';

export default class StatutoryFindingsController extends CodicilBase {
  /* @ngInject */
  constructor(
    AppSettings,
    CodicilsListingFactory,
    CodicilService,
    ViewService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.STATUTORY_FINDING;
    this.searchByList = _.cloneDeep(this._AppSettings.NOTES_AND_ACTIONS_SEARCH_PARAM
      .STATUTORY_FINDINGS_SEARCH);
    this.sortByList = this._AppSettings.STATUTORY_FINDINGS_SORT;
    this.filterOptions = _.concat(
      [
        {
          type: 'radiobuttons',
          title: 'cd-status',
          key: 'status',
          options: this._CodicilsListingFactory.getStatuses(this.codicils),
        },
      ],
      _.cloneDeep(this._AppSettings.STATUTORY_FINDINGS_FILTER_OPTIONS)
    );
    this.action = {
      stateName: this._AppSettings.STATES.STATUTORY_FINDINGS_DETAILS,
      params: {
        assetId: _.get(this.asset, 'id'),
      },
      evalParams: {
        codicilId: 'id',
        deficiencyId: 'deficiencyId',
      },
    };
  }
}
