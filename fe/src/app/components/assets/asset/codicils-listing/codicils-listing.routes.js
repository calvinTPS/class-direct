import AppSettings from 'app/config/project-variables.js';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
  .state('asset.codicilsListing', {
    abstract: true,
    views: {
      'asset-view': {
        component: 'codicilsListing',
      },
    },
  })
  .state('asset.codicilsListing.conditionsOfClass', {
    url: '/conditions-of-class',
    views: {
      'codicils-listing': {
        component: 'conditionsOfClass',
      },
    },
    resolve: {
      /* @ngInject */
      codicils: ($stateParams, CodicilService) =>
        CodicilService.get(
         $stateParams.assetId,
         AppSettings.API_ENDPOINTS.ASSET.Cocs,
       ),
    },
  })
  .state('asset.codicilsListing.assetNotes', {
    url: '/asset-notes',
    views: {
      'codicils-listing': {
        component: 'assetNotes',
      },
    },
    resolve: {
      /* @ngInject */
      codicils: ($stateParams, CodicilService) =>
       CodicilService.get(
         $stateParams.assetId,
         AppSettings.API_ENDPOINTS.ASSET.AssetNotes,
       ),
    },
  })
  .state('asset.codicilsListing.actionableItems', {
    url: '/actionable-items',
    views: {
      'codicils-listing': {
        component: 'actionableItems',
      },
    },
    resolve: {
      /* @ngInject */
      codicils: ($stateParams, CodicilService) =>
       CodicilService.get(
         $stateParams.assetId,
         AppSettings.API_ENDPOINTS.ASSET.ActionableItems,
       ),
    },
  })
  .state('asset.codicilsListing.defects', {
    url: '/defects',
    views: {
      'codicils-listing': {
        component: 'defects',
      },
    },
    resolve: {
      /* @ngInject */
      codicils: ($stateParams, DefectService) =>
       DefectService.query(
          $stateParams.assetId,
          AppSettings.API_ENDPOINTS.ASSET.Defects,
       ),
    },
  })
  .state('asset.codicilsListing.mncns', {
    url: '/mncns',
    views: {
      'codicils-listing': {
        component: 'mncns',
      },
    },
    resolve: {
      /* @ngInject */
      codicils: ($stateParams, CodicilService) =>
        CodicilService.get(
          $stateParams.assetId,
          AppSettings.API_ENDPOINTS.ASSET.MNCNs,
      ),
    },
  })
  .state('asset.codicilsListing.statutoryFindings', {
    url: '/statutory-findings',
    views: {
      'codicils-listing': {
        component: 'statutoryFindings',
      },
    },
    resolve: {
      /* @ngInject */
      codicils: ($stateParams, CodicilService) =>
        CodicilService.query(
          $stateParams.assetId,
          AppSettings.API_ENDPOINTS.ASSET.StatutoryFindings,
        ),
    },
  })
  .state('asset.codicilsListing.statutoryDeficiencies', {
    url: '/statutory-deficiencies',
    views: {
      'codicils-listing': {
        component: 'statutoryDeficiencies',
      },
    },
    resolve: {
      /* @ngInject */
      codicils: ($stateParams, DefectService) =>
        DefectService.query(
          $stateParams.assetId,
          AppSettings.API_ENDPOINTS.ASSET.StatutoryDeficiencies,
        ),
    },
  });
}
