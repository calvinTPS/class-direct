import './defects.scss';
import controller from './defects.controller';
import template from './defects.pug';

export default {
  bindings: {
    asset: '<',
    codicils: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
