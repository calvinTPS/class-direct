import * as _ from 'lodash';
import Base from 'app/base';

export default class DefectsController extends Base {
  /* @ngInject */
  constructor(
    $filter,
    AppSettings,
    CodicilsListingFactory,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.DEFECT;
    this.searchByList = _.cloneDeep(this._AppSettings.NOTES_AND_ACTIONS_SEARCH_PARAM
      .DEFECTS_SEARCH_BY_LIST);
    this.filterOptions = this._CodicilsListingFactory
      .getFilterOptions(this.codicils, _.cloneDeep(this._AppSettings.DEFECTS_FILTER_OPTIONS));
    this.action = {
      stateName: this._AppSettings.STATES.DEFECTS_DETAILS,
      params: { assetId: this.asset.id },
      evalParams: { defectId: 'id' },
    };
    // Default orderBy incident date descending
    this.codicils = this._$filter('orderBy')(this.codicils, 'incidentDate', true);
  }

  get displayCMRO() {
    return !!this.asset.imo && !this.currentUser.isEquasisThetis;
  }
}
