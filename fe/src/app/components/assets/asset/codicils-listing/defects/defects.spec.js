/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import CodicilsListingFactory from 'app/components/assets/asset/codicils-listing/codicils-listing.factory';
import DefectsComponent from './defects.component';
import DefectsController from './defects.controller';
import DefectsModule from './';
import DefectsTemplate from './defects.pug';

describe('Defects', () => {
  let $rootScope,
    $compile,
    $filter,
    mockCodicilsListingFactory,
    makeController;

  beforeEach(window.module(DefectsModule));
  beforeEach(() => {
    mockCodicilsListingFactory = {
      getFilterOptions: () => {},
    };
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('CodicilsListingFactory', mockCodicilsListingFactory);
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$filter_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $filter = _$filter_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('defects', {
          AppSettings,
          CodicilsListingFactory: mockCodicilsListingFactory,
        }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const fakeAsset = {
        id: 1,
        description: 'fake asset',
      };
      const codicils = [
        {
          id: 1,
          imposedDate: '2017-06-11T00:00:00Z',
          incidentDate: '2017-06-11T00:00:00Z',
        },
        {
          id: 2,
          imposedDate: '2016-01-14T00:00:00Z',
          incidentDate: '2016-01-14T00:00:00Z',
        },
      ];
      const action = {
        stateName: 'asset.defectsDetail',
        params: { assetId: 1 },
        evalParams: { defectId: 'id' },
      };

      const controller = makeController({ asset: fakeAsset, codicils });
      expect(controller.action).toEqual(action);
      expect(controller.asset).toEqual(fakeAsset);
      expect(controller.codicils).toEqual(
        $filter('orderBy')([
          {
            id: 1,
            imposedDate: '2017-06-11T00:00:00Z',
            incidentDate: '2017-06-11T00:00:00Z',
          },
          {
            id: 2,
            imposedDate: '2016-01-14T00:00:00Z',
            incidentDate: '2016-01-14T00:00:00Z',
          },
        ], 'incidentDate', true)
      );
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = DefectsComponent;
    it('includes the intended template', () => {
      expect(component.template).toEqual(DefectsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(DefectsController);
    });
  });

  it('displayCMRO() to check wheather to display CMRO link', () => {
    const fakeAsset = {
      id: 1,
      description: 'fake asset',
      imo: '123456',
    };
    const currentUser = {
      isEquasisThetis: false,
    };
    const controller = makeController({ asset: fakeAsset,
      currentUser: _.cloneDeep(currentUser) });
    expect(controller.displayCMRO).toEqual(true);
    const currentUser1 = {
      isEquasisThetis: true,
    };
    const controller1 = makeController({ asset: fakeAsset,
      currentUser: _.cloneDeep(currentUser1) });
    expect(controller1.displayCMRO).toEqual(false);
  });
});
