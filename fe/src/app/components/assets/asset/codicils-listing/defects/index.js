import angular from 'angular';
import defectsComponent from './defects.component';

export default angular.module('defects', [
])
.component('defects', defectsComponent)
.name;
