import Base from 'app/base';

export default class CodicilsListingController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    CodicilService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.exportOptions = {
      code: this.asset.id,
      service: this._CodicilService,
    };
  }

  get displayCMRO() {
    return !!this.asset.imo && !this.currentUser.isEquasisThetis;
  }
}
