import ActionableItems from './actionable-items';
import angular from 'angular';
import AssetNotes from './asset-notes';
import codicilsListingComponent from './codicils-listing.component';
import CodicilsListingFactory from './codicils-listing.factory';
import ConditionsOfClass from './conditions-of-class';
import Defects from './defects';
import Mncns from './mncns';
import routes from './codicils-listing.routes';
import StatutoryDeficiencies from './statutory-deficiencies';
import StatutoryFindings from './statutory-findings';
import uiRouter from 'angular-ui-router';

export default angular.module('codicilsListing', [
  uiRouter,
  ActionableItems,
  AssetNotes,
  ConditionsOfClass,
  Defects,
  Mncns,
  StatutoryDeficiencies,
  StatutoryFindings,
])
  .config(routes)
  .component('codicilsListing', codicilsListingComponent)
  .factory('CodicilsListingFactory', CodicilsListingFactory)
  .name;
