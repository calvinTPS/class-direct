import * as _ from 'lodash';
import Base from 'app/base';
import CodicilModel from 'app/common/models/codicil/codicil';
import DefectModel from 'app/common/models/defect/';
import GeneralModalController from 'app/common/dialogs/general-modal.controller';
import rectificationDetailModalTemplate from '../rectifications/rectification-detail-modal.pug';
import repairDetailModalTemplate from '../repairs/repair-detail-modal.pug';
import RepairModel from 'app/common/models/repair';

export default class CodicilDetailController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $scope,
    AppSettings,
    JobService,
    utils,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    this._createAddons();

    if (!this.item.isMNCN) {
      this._configureAttachments();
    }
  }

  /*
   * Get the attachments for jobs with FSR
   */
  async _configureAttachments() {
    let jobs = [];
    const attachments = [];

    // If there are jobs AND job, we use jobs.
    // Else, use job if available
    if (this.item.jobs) {
      jobs = this.item.jobs;
    } else if (this.item.job) {
      jobs = [this.item.job];
    }

    if (jobs.length > 0) {
      try {
        const reports = await Promise.all(jobs.map(job => this._JobService.getJobReports(job.id)));
        _.forEach(_.flatten(reports), (report) => {
          if (report.reportType === this._AppSettings.REPORT_TYPES.FSR) {
            attachments.push(report);
          }
        });
        this.item.attachments = attachments;
        this._$scope.$apply();
      } catch (e) {
        this.errorGettingAttachments = true;
      }
    }
  }

  /*
   * Associated codicils, defects or repairs
   */
  _createAddons() {
    this.item.addons = [];

    if (_.size(this.item.repairs) > 0) {
      this.item.addons.push({
        itemClass: 'repair-item',
        title: 'cd-associated-repairs',
        template: this._AppSettings.RIDER_TEMPLATES.REPAIR,
        data: _.forEach(this.item.repairs, (repairItem) => {
          // To use the defect's title, as repair has no title
          repairItem.title = this.item.title; // eslint-disable-line no-param-reassign
        }),
      });

      // this._ViewService.registerTemplate(this._AppSettings.RIDER_TEMPLATE_KEY.REPAIR,
        // repairItemTemplate);
    }

    if (_.size(this.item.cocs) > 0) {
      this.item.addons.push({
        itemClass: 'coc-item',
        title: 'cd-associated-conditions-of-class',
        template: this._AppSettings.RIDER_TEMPLATES.CONDITIONS_OF_CLASS,
        data: this.item.cocs,
      });
    }

    if (this.item.defect) {
      this.item.addons.push({
        itemClass: 'defect-item',
        title: 'cd-associated-class-defect',
        template: this._AppSettings.RIDER_TEMPLATES.DEFECT,
        data: [this.item.defect],
      });
    }

    if (_.size(this.item.statutoryFindings) > 0) {
      this.item.addons.push({
        itemClass: 'statutory-finding-item',
        title: 'cd-tabs-statutory-findings',
        template: this._AppSettings.RIDER_TEMPLATES.STATUTORY_FINDING,
        data: _.forEach(this.item.statutoryFindings, (statutoryFinding, index) => {
          // Reconstruct object using codicil model
          // Put conditional checking because it's messed up with @cacheable decorator
          if (!(this.item.statutoryFindings[index] instanceof CodicilModel)) {
            this.item.statutoryFindings[index] =
              Reflect.construct(CodicilModel, [statutoryFinding]);
          }
        }),
      });
    }

    if (_.size(this.item.rectificationsH) > 0) {
      this.item.addons.push({
        itemClass: 'rectification-item',
        title: 'cd-rectifications',
        template: this._AppSettings.RIDER_TEMPLATES.RECTIFICATION,
        data: _.forEach(this.item.rectificationsH, (rectification, index) => {
          // To use the deficiency(parent)'s title as the rectification title
          rectification.title = this.item.title; // eslint-disable-line no-param-reassign
          this.item.rectificationsH[index] = Reflect.construct(RepairModel, [rectification]);
        }),
      });
    }

    if (this.item.deficiencyH) {
      this.item.addons.push({
        itemClass: 'statutory-deficiency-item',
        title: 'cd-tabs-statutory-deficiencies',
        template: this._AppSettings.RIDER_TEMPLATES.DEFICIENCY,
        // Reconstruct object using defect model
        data: [Reflect.construct(DefectModel, [this.item.deficiencyH])],
      });
    }
  }

  /*
   * Options to build the sref for the associated codicils/defects
   */
  getDetailsPageSrefOptions(item) {
    // Cache result because Angular will keep asking and see different objects
    if (this._srefOptionsCache == null) {
      this._srefOptionsCache = new WeakMap();
    }

    const cachedResult = this._srefOptionsCache.get(item);
    if (cachedResult != null) {
      return cachedResult;
    }

    const { COC, SF, MNCN } = this._AppSettings.CODICILS_META;
    const { DEFECT, SD, REPAIR, RECTIFICATION } = this._AppSettings.DEFECTS_META;

    const STATES = this._AppSettings.STATES;
    let obj = null;

    if (item.type === COC.ID) {
      obj = {
        stateName: STATES.CONDITIONS_OF_CLASS_DETAILS,
        params: {
          assetId: this.asset.id,
          codicilId: item.id,
        },
      };
    } else if (item.type === SF.ID) {
      obj = {
        stateName: STATES.STATUTORY_FINDINGS_DETAILS,
        params: {
          assetId: _.get(this.asset, 'id'),
        },
        evalParams: {
          codicilId: 'id',
          deficiencyId: 'deficiencyId',
        },
      };
    } else if (item.type === DEFECT.ID) {
      obj = {
        stateName: STATES.DEFECTS_DETAILS,
        params: {
          assetId: this.asset.id,
          defectId: item.id,
        },
      };
    } else if (item.type === MNCN.ID) {
      obj = {
        stateName: STATES.MNCNS_DETAILS,
        params: {
          assetId: this.asset.id,
          codicilId: item.id,
        },
      };
    } else if (item.type === SD.ID) {
      obj = {
        stateName: STATES.STATUTORY_DEFICIENCIES_DETAILS,
        params: {
          assetId: this.asset.id,
          deficiencyId: item.id,
        },
      };
    } else if (item.type === RECTIFICATION.ID) {
      this.rectification = item;

      obj = {
        icon: 'reopen',
        ngClick: this.openRectificationDetail.bind(this),
      };
    } else if (item.type === REPAIR.ID) {
      this.repair = item;

      obj = {
        icon: 'reopen',
        ngClick: this.openRepairDetail.bind(this),
      };
    }

    this._srefOptionsCache.set(item, obj);
    return obj;
  }

  openRepairDetail(repairItem) {
    this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: true,
      controller: GeneralModalController,
      controllerAs: 'vm',
      locals: {
        item: repairItem,
      },
      parent: angular.element(this._$document[0].body),
      template: repairDetailModalTemplate,
    });
  }

  openRectificationDetail(rectification) {
    this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: true,
      controller: GeneralModalController,
      controllerAs: 'vm',
      locals: {
        item: rectification,
      },
      parent: angular.element(this._$document[0].body),
      template: rectificationDetailModalTemplate,
    });
  }

  /*
   * Show proper details template by codicil/defect type
   */
  canShowDetailsByType(type) {
    return type === this.item.type;
  }

  /*
   * Show attachments/downloads if it is not Statutory Findings nor Statutory Deficiency
   */
  canShowAttachments() {
    const { SF, MNCN } = this._AppSettings.CODICILS_META;
    const { SD } = this._AppSettings.DEFECTS_META;

    return !_.includes([SF.ID, SD.ID, MNCN], this.item.type);
  }

  get canShowAddon() {
    return !this.currentUser.isEquasisThetis && this.item.addons.length;
  }

  get isOpenMNCN() {
    return this.item.isMNCN && (this.item.status === this._AppSettings.CODICILS_STATUSES.OPEN);
  }

  get isClosedMNCN() {
    return this.item.isMNCN && (this.item.status === this._AppSettings.CODICILS_STATUSES.CLOSED);
  }
}
