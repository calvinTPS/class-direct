import './codicil-details.scss';
import controller from './codicil-details.controller';
import template from './codicil-details.pug';

export default {
  bindings: {
    asset: '<',
    item: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
