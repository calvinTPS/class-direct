/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import CodicilDetailsComponent from './codicil-details.component';
import CodicilDetailsController from './codicil-details.controller';
import CodicilDetailsModule from './codicil-details';
import CodicilDetailsTemplate from './codicil-details.pug';
import CodicilModel from 'app/common/models/codicil/codicil';
import utils from 'app/common/utils/utils.factory';

describe('CodicilDetails', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    makeController;

  const item = {
    id: 1,
    title: 'Item title',
    type: 'COC',
    codicilType: 'COC',
    defectCategory: null,
    defectStatus: null,
    repairs: [{}, {}, {}],
    cocs: [{}, {}, {}],
    defect: {},
    rectificationsH: [{
      repairActionH: {
        name: 'Rectification Title',
      },
    }],
    statutoryFindings: [{
      deficiencyH: {},
    }, {
      codicilType: 'COC',
    }],
    deficiencyH: {},
    job: { id: 1 },
    defectH: {
      title: 'Defect title',
    },
    flagStateConsulted: null,
  };

  const asset = {
    id: 'LRV1',
  };

  const jobReports = [{
    id: 1,
    reportType: AppSettings.REPORT_TYPES.FSR,
    reportVersion: null,
    name: AppSettings.REPORT_TYPES.FSR,
  }];

  const mockJobService = {
    getJobReports(id) {
      return new Promise((resolve, reject) => {
        resolve(jobReports);
      });
    },
  };

  const currentUser = {
    isEquasisThetis: false,
  };

  beforeEach(window.module(CodicilDetailsModule));

  beforeEach(() => {
    const mockMdDialog = function mdDialog() {
      return {
        alert: (arg) => {},
        build: (arg) => {},
        cancel: function cancel(reason, options) {},
        confirm: (arg) => {},
        destroy: function destroyInterimElement(opts) {},
        hide: function hide(reason, options) {},
        prompt: (arg) => {},
        show: function showInterimElement(opts) {
          return new Promise((resolve, reject) => resolve());
        },
      };
    };

    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('utils', utils());
      $provide.value('JobService', mockJobService);
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
      $provide.service('$mdDialog', mockMdDialog); // eslint-disable-line angular/no-service-method
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      async (bindings = {}) => {
        const controller =
          $componentController('codicilDetails', { $dep: dep }, bindings);
        // $onInit() returns a promise
        await controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let newItem;

    beforeEach(() => {
      newItem = {
        id: 1,
        type: 'COC',
        codicilType: 'COC',
        defectCategory: null,
        defectStatus: null,
      };
    });

    it.async('initializes just fine', async () => {
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });
      expect(controller.foo).toEqual('bar');
    });

    it.async('creates the correct Details page Sref object, getDetailsPageSrefOptions()', async () => {
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });

      const obj1 = {
        stateName: AppSettings.STATES.CONDITIONS_OF_CLASS_DETAILS,
        params: {
          assetId: 'LRV1',
          codicilId: 1,
        },
      };

      const obj2 = {
        stateName: AppSettings.STATES.DEFECTS_DETAILS,
        params: {
          assetId: 'LRV1',
          defectId: 1,
        },
      };
      expect(controller.getDetailsPageSrefOptions(newItem))
        .toEqual({
          stateName: 'asset.conditionsOfClassDetail',
          params: {
            assetId: 'LRV1',
            codicilId: 1,
          },
        });

      const defectItem = _.clone(newItem);
      defectItem.type = 'DEFECT';
      defectItem.defectCategory = 'category';
      expect(controller.getDetailsPageSrefOptions(defectItem))
        .toEqual({
          stateName: 'asset.defectsDetail',
          params: {
            assetId: 'LRV1',
            defectId: 1,
          },
        });

      const deficiencyItem = _.clone(newItem);
      deficiencyItem.type = 'SD';
      deficiencyItem.defectCategory = 'category';
      expect(controller.getDetailsPageSrefOptions(deficiencyItem))
        .toEqual({
          stateName: 'asset.statutoryDeficienciesDetail',
          params: {
            assetId: 'LRV1',
            deficiencyId: 1,
          },
        });

      const repairActionItem = _.clone(defectItem);
      repairActionItem.defectCategory = null;
      repairActionItem.repairActionH = {
        id: 1,
        name: 'PERMANENT',
        deleted: false,
        description: 'Permanent repair',
      };
      const result = controller.getDetailsPageSrefOptions(repairActionItem);
      expect(result.ngClick).not.toBeNull();
    });

    it.async('creates addons as expected, _createAddons()', async () => {
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });

      expect(controller.item.addons.length).toEqual(6);
      expect(controller.item.addons[0].itemClass).toEqual('repair-item');
      expect(controller.item.addons[0].title).toEqual('cd-associated-repairs');
      expect(controller.item.addons[0].data.length).toEqual(3);

      expect(controller.item.addons[1].itemClass).toEqual('coc-item');
      expect(controller.item.addons[1].title).toEqual('cd-associated-conditions-of-class');

      expect(controller.item.addons[2].itemClass).toEqual('defect-item');
      expect(controller.item.addons[2].title).toEqual('cd-associated-class-defect');

      expect(controller.item.addons[3].itemClass).toEqual('statutory-finding-item');
      expect(controller.item.addons[3].title).toEqual('cd-tabs-statutory-findings');
      expect(controller.item.addons[3].data[0].type)
        .toEqual(AppSettings.CODICILS_META.SF.ID);
      expect(controller.item.addons[3].data[1].type)
        .toEqual(AppSettings.CODICILS_META.COC.ID);

      expect(controller.item.addons[4].itemClass).toEqual('rectification-item');
      expect(controller.item.addons[4].title).toEqual('cd-rectifications');
      expect(controller.item.addons[4].data[0].title).toEqual('Item title');

      expect(controller.item.addons[5].itemClass).toEqual('statutory-deficiency-item');
      expect(controller.item.addons[5].title).toEqual('cd-tabs-statutory-deficiencies');
      expect(controller.item.addons[5].data[0].flagStateConsulted).toEqual('cd-no');
    });

    it.async('configures the attachments correctly for single job, _configureAttachments', async () => {
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });
      expect(controller.item.attachments).toBeDefined();
      expect(controller.item.attachments.length).toEqual(1);
      expect(controller.item.attachments[0].name).toEqual(AppSettings.REPORT_TYPES.FSR);
    });

    it.async('configures the attachments correctly for multiple jobs, _configureAttachments', async () => {
      item.jobs = [
        { id: 1 },
        { id: 2 },
        { id: 3 },
      ];
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });
      expect(controller.item.attachments).toBeDefined();
      expect(controller.item.attachments.length).toEqual(3);
      expect(controller.item.attachments[2].name).toEqual(AppSettings.REPORT_TYPES.FSR);
    });

    it.async('openRepairDetail & openRectificationDetail should open up modal popup', async () => {
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });
      spyOn($mdDialog, 'show');

      controller.openRepairDetail();
      expect($mdDialog.show).toHaveBeenCalled();
      expect($mdDialog.show.calls.count()).toEqual(1);

      controller.openRectificationDetail();
      expect($mdDialog.show.calls.count()).toEqual(2);
    });

    it.async('canShowDetailsByType() should return boolean correctly', async () => {
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });
      controller.item.type = 'COC';
      expect(controller.canShowDetailsByType('COC')).toEqual(true);
      expect(controller.canShowDetailsByType('AN')).toEqual(false);
      expect(controller.canShowDetailsByType('SD')).toEqual(false);

      controller.item.type = 'AN';
      expect(controller.canShowDetailsByType('COC')).toEqual(false);
      expect(controller.canShowDetailsByType('AN')).toEqual(true);
      expect(controller.canShowDetailsByType('SD')).toEqual(false);
    });

    it.async('canShowAttachments() should return boolean correctly', async () => {
      const controller = await makeController({ foo: 'bar', item: _.cloneDeep(item), asset });
      controller.item.type = 'SD';
      expect(controller.canShowAttachments()).toEqual(false);

      controller.item.type = 'AI';
      expect(controller.canShowAttachments()).toEqual(true);

      controller.item.type = 'SF';
      expect(controller.canShowAttachments()).toEqual(false);

      controller.item.type = 'DEFECT';
      expect(controller.canShowAttachments()).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CodicilDetailsComponent;

    it.async('includes the intended template', async () => {
      expect(component.template).toEqual(CodicilDetailsTemplate);
    });

    it.async('invokes the right controller', async () => {
      expect(component.controller).toEqual(CodicilDetailsController);
    });
  });
});
