import angular from 'angular';
import CodicilDetailsComponent from './codicil-details.component';
import uiRouter from 'angular-ui-router';

export default angular.module('codicilDetails', [
  uiRouter,
])
.component('codicilDetails', CodicilDetailsComponent)
.name;
