/* eslint-disable no-param-reassign */
import * as _ from 'lodash';

/* @ngInject */
const CodicilsListingFactory = () => {
  const getFilterOptions = (codicils, additionalOptions) => {
    let filterOptions = [];
    if (!_.isEmpty(codicils)) {
      if (!additionalOptions) {
        additionalOptions = [];
      }
      filterOptions.push({
        type: 'radiobuttons',
        title: 'cd-status',
        key: 'status',
        options: getStatuses(codicils),
      });
      filterOptions.push({
        type: 'checkboxes',
        title: 'cd-category',
        key: 'category',
        isAll: true,
        options: getCategories(codicils),
      });
      filterOptions = _.concat(filterOptions, additionalOptions);
    }
    return filterOptions;
  };

  const getStatuses = (codicils) => {
    const statusList = [];
    _.forEach(codicils, (codicil) => {
      if (codicil.status) {
        statusList.push(
          {
            label: codicil.status,
            value: codicil.status,
            selected: (codicil.status === 'Open'),
          }
        );
      }
    });

    return _.uniqWith(statusList, _.isEqual);
  };

  const getCategories = (codicils) => {
    const categoryList = [];
    _.forEach(codicils, (codicil) => {
      if (codicil.category) {
        categoryList.push(
          {
            name: codicil.category,
            value: codicil.category,
            selected: false,
          }
        );
      }
    });

    return _.uniqWith(categoryList, _.isEqual);
  };

  return {
    getCategories,
    getFilterOptions,
    getStatuses,
  };
};
export default CodicilsListingFactory;
