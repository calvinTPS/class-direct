import Base from 'app/base';

export default class CodicilBase extends Base {
  constructor(
    args,
  ) {
    super(args);

    this.updateItemsToExport = this.updateItemsToExport.bind(this);

    this.service = this._CodicilService; // Just a reference to keep things generic
    this._CodicilService.itemsToExport = [];
  }

  // I am fired by the rider filter when the results change
  updateItemsToExport(items) {
    // I store the items to export in the service
    this.service.itemsToExport = items;

    // I return the items back to the rider filter as it
    // expects me to manipulate it if I want to
    return items;
  }

  addCallback() {
    const codicilsWithPostUpdate = this._codicils;
    if (codicilsWithPostUpdate) {
      codicilsWithPostUpdate.postUpdateCallback = this.updateItemsToExport;
    }
  }

  getCodicilsCached() {
    return this._codicils;
  }

  get codicils() {
    this.addCallback();
    return this.getCodicilsCached();
  }

  set codicils(codicils) {
    this._codicils = codicils || [];
  }
}
