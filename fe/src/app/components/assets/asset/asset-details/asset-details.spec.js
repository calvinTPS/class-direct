/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import AssetDetailsComponent from './asset-details.component';
import AssetDetailsController from './asset-details.controller';
import AssetDetailsModule from './asset-details';
import AssetDetailsTemplate from './asset-details.pug';
import fieldsSchema from './fields-schema';

describe('AssetDetails', () => {
  let $rootScope,
    $compile,
    $state,
    mockPermissionsService,
    mockState,
    makeController;

  const details = {
    registryInformation: { lead: true },
    principalDimension: {},
    rulesetDetailsDto: {},
    equipmentDetails: {},
    ownership: {
      registeredOwner: null,
      groupOwner: {
        id: 1,
        name: 'Group Owner from Asset Details',
        email: 'groupOwner@lr.com',
      },
      operator: {
        id: 2,
        name: 'Ship Operator from Asset Details',
        email: 'operator@lr.com',
      },
      shipManager: null,
      technicalManager: null,
      doc: null,
    },
  };

  const currentUser = {
    isEquasisThetis: false,
  };

  const asset = {
    id: 777,
    isIHSAsset: false,
    isLRAsset: true,
    imo: 1000019,
    classNotation: 'This is the class notation',
    builder: 'Builder Name',
    yardNumber: 123,
    customersH: [
      {
        party: {
          customer: {
            name: 'Registered Owner 1 from Asset',
          },
        },
        partyRoles: { id: 4 }, // registered owner
      },
      {
        party: {
          customer: {
            name: 'Ship Operator from Asset',
          },
        },
        partyRoles: { id: 2 }, // ship operator
      },
      {
        party: {
          customer: {
            name: 'Group Owner from Asset',
          },
        },
        partyRoles: { id: 6 }, // group owner
      },
    ],
    ihsAssetDto: null,
  };

  beforeEach(window.module(AssetDetailsModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockCMROService = {
      getLinks: () => [{ URL: 'blah' }, { URL: 'blah' }],
    };

    mockState = {
      go: () => {},
    };
    mockPermissionsService = {
      canAccessSistersSection: () => { },
      canViewAssetDetailInfo: () => { },
      canPrintExport: user => user && !user.isEquasisThetis,
    };

    const mockAssetService = {
      get: () => Promise.resolve(),
      getSisterAssetsCount: () => Promise.resolve(),
    };

    const mockStateParams = {};

    window.module(($provide) => {
      $provide.value('$mdMedia', {});
      $provide.value('$mdDialog', {});
      $provide.value('$filter', () => val => `<span>${val}</span>`);
      $provide.value('$state', mockState);
      $provide.value('AppSettings', AppSettings);
      $provide.value('AssetService', mockAssetService);
      $provide.value('PermissionsService', mockPermissionsService);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('CMROService', mockCMROService);
      $provide.value('$stateParams', mockStateParams);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}, init) => {
        const controller = $componentController('assetDetails', { $dep: dep }, bindings);
        if (init !== false) {
          controller.$onInit();
        }
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    beforeEach(() => {
      controller = makeController({
        details: _.cloneDeep(details),
        currentUser: _.cloneDeep(currentUser),
        asset: _.cloneDeep(asset),
        technicalSisters: [],
        registerBookSisters: [],
      });
    });

    it('initializes just fine', () => {
      const mycontroller = makeController({
        details: _.cloneDeep(details),
        currentUser: _.cloneDeep(currentUser),
        asset: _.cloneDeep(asset),
        technicalSisters: [],
        registerBookSisters: [],
      }, false);

      spyOn(mycontroller, '_getCustomerDetails');
      mycontroller.$onInit();

      expect(mycontroller.asset).toEqual(asset);
      expect(mycontroller.currentUser).toEqual(currentUser);
      expect(_.size(mycontroller.details)).toEqual(_.size(details));
      expect(mycontroller.details.registryInformation.lead).toEqual('Yes');
      expect(mycontroller.CMROLinks).toBeDefined();
      expect(mycontroller.details.rulesetDetailsDto.hullNotation).toBeDefined();
      expect(mycontroller._getCustomerDetails).toHaveBeenCalled();
    });

    it('_getCustomerDetails(), for IHS Asset, correctly extracts the customer details', () => {
      const IHSAsset = _.cloneDeep(asset);
      IHSAsset.isIHSAsset = true;

      const newController = makeController({
        details: _.cloneDeep(details),
        currentUser: _.cloneDeep(currentUser),
        asset: IHSAsset,
        technicalSisters: [],
        registerBookSisters: [],
      });

      expect(newController.details.groupOwner).toBeDefined();
      expect(newController.details.groupOwner.name).toEqual('Group Owner from Asset Details');

      expect(newController.details.operator).toBeDefined();
      expect(newController.details.operator.name).toEqual('Ship Operator from Asset Details');

      expect(newController.details.registeredOwner).toBeUndefined();
      expect(newController.details.shipManager).toBeUndefined();
      expect(newController.details.technicalManager).toBeUndefined();
      expect(newController.details.doc).toBeUndefined();
    });

    it('_getCustomerDetails(), for NON IHS(MAST) Asset, correctly extracts the customer details', () => {
      expect(controller.details.registeredOwner).toBeDefined();
      expect(controller.details.registeredOwner.name).toEqual('Registered Owner 1 from Asset');

      expect(controller.details.groupOwner).toBeDefined();
      expect(controller.details.groupOwner.name).toEqual('Group Owner from Asset');

      expect(controller.details.operator).toBeDefined();
      expect(controller.details.operator.name).toEqual('Ship Operator from Asset');

      expect(controller.details.shipManager).toBeUndefined();
      expect(controller.details.technicalManager).toBeUndefined();
      expect(controller.details.doc).toBeUndefined();
    });

    it('_updateFieldToFrutiger, correctly add html for Frutiger fonts', () => {
      controller.details.somevalue = 'Testing Frutiger';
      controller._updateFieldToFrutiger('somevalue');

      expect(controller.details.somevalue).toEqual('<span>Testing Frutiger</span>');
    });


    it('_modifyCableLength, modify the cable length', () => {
      controller.details.equipmentDetails.cableLength = 0.0;
      controller._modifyCableLength();
      expect(controller.details.equipmentDetails.cableLength).toEqual('-');

      controller.details.equipmentDetails.cableLength = 18;
      controller._modifyCableLength();
      expect(controller.details.equipmentDetails.cableLength).toEqual('18m');

      controller.details.equipmentDetails.cableLength = undefined;
      controller._modifyCableLength();
      expect(controller.details.equipmentDetails.cableLength).toEqual('-');
    });

    it('_displayLifeCycleStatusForIHSAsset, display lifecycle status for IHS asset', () => {
      const IHSAsset = _.cloneDeep(asset);
      IHSAsset.isIHSAsset = true;

      const mycontroller = makeController({
        details: _.cloneDeep(details),
        currentUser: _.cloneDeep(currentUser),
        asset: IHSAsset,
        technicalSisters: [],
        registerBookSisters: [],
      });

      mycontroller.asset.ihsAssetDto = {
        ihsAsset: {
          id: 1000021,
          status: 'broken',
        },
      };
      mycontroller._displayLifeCycleStatusForIHSAsset();

      expect(mycontroller.details.registryInformation.assetLifeCycleStatus).toEqual('broken');
    });

    it('_getSisterAssetsCount, correctly gets the count of sister assets from api', () => {
      const mycontroller = makeController({
        details,
        currentUser,
        asset,
      }, false);

      mycontroller._$stateParams.assetId = 1;
      const assetId = 1;
      const leadImo = mycontroller.asset.leadImo;
      let canViewSisters = false;

      spyOn(mycontroller._AssetService, 'getSisterAssetsCount').and.returnValue({
        then: () => Promise.resolve(),
      });

      spyOn(mycontroller._PermissionsService, 'canAccessSistersSection')
        .and.callFake(() => canViewSisters);

      mycontroller._getSisterAssetsCount();
      expect(mycontroller._AssetService.getSisterAssetsCount).not.toHaveBeenCalled();

      canViewSisters = true;
      mycontroller._getSisterAssetsCount();
      expect(mycontroller._PermissionsService.canAccessSistersSection).toHaveBeenCalledTimes(2);

      expect(mycontroller._AssetService.getSisterAssetsCount).toHaveBeenCalledTimes(1);
      expect(mycontroller._AssetService.getSisterAssetsCount)
        .toHaveBeenCalledWith({
          assetId,
          leadImo,
          type: AppSettings.API_ENDPOINTS.TechnicalSisters,
        });

      expect(mycontroller._AssetService.getSisterAssetsCount)
        .not.toHaveBeenCalledWith({
          assetId,
          leadShip,
          type: AppSettings.API_ENDPOINTS.RegisterBookSisters,
        });

      mycontroller.asset.ihsAssetDto = {
        ihsAsset: {
          id: 1000021,
          leadShip: 1234567,
        },
      };

      const leadShip = mycontroller.asset.ihsAssetDto.ihsAsset.leadShip;
      mycontroller._getSisterAssetsCount();

      expect(mycontroller._AssetService.getSisterAssetsCount)
        .toHaveBeenCalledWith({
          assetId,
          leadShip,
          type: AppSettings.API_ENDPOINTS.RegisterBookSisters,
        });

      expect(mycontroller._AssetService.getSisterAssetsCount)
        .toHaveBeenCalledWith({
          assetId,
          leadImo,
          type: AppSettings.API_ENDPOINTS.TechnicalSisters,
        });
    });

    describe('show', () => {
      it('should delegate to PermissionsService', () => {
        spyOn(mockPermissionsService, 'canViewAssetDetailInfo').and.returnValue(true);
        const res = controller.show('KEYNAME');

        expect(mockPermissionsService.canViewAssetDetailInfo)
          .toHaveBeenCalledWith('KEYNAME', controller.asset, controller.currentUser);
        expect(res).toEqual(true);
      });
    });

    describe('canView()', () => {
      it('checks for technical/register/all sisters access', () => {
        mockPermissionsService.canAccessSistersSection = () => true;
        spyOn(mockPermissionsService, 'canAccessSistersSection').and.callThrough();
        expect(controller.canView(AssetDetailsController.SECTIONS.SISTER_VESSELS)).toBe(true);

        mockPermissionsService.canAccessSistersSection = () => false;
        expect(controller.canView(AssetDetailsController.SECTIONS.SISTER_VESSELS)).toBe(false);
      });

      it('checks for export access', () => {
        [
          { isEquasisThetis: true, isEOR: true, isRestricted: true, expectedResult: false },
          { isEquasisThetis: true, isEOR: true, isRestricted: false, expectedResult: false },
          { isEquasisThetis: true, isEOR: false, isRestricted: true, expectedResult: false },
          { isEquasisThetis: true, isEOR: false, isRestricted: false, expectedResult: false },
          { isEquasisThetis: false, isEOR: true, isRestricted: true, expectedResult: true },
          { isEquasisThetis: false, isEOR: true, isRestricted: false, expectedResult: true },
          { isEquasisThetis: false, isEOR: false, isRestricted: true, expectedResult: true },
          { isEquasisThetis: false, isEOR: false, isRestricted: false, expectedResult: true },
        ].forEach((condition) => {
          controller.currentUser = {
            isEquasisThetis: condition.isEquasisThetis,
          };
          controller.asset = {
            isEORAsset: condition.isEOR,
            isAccessibleLRAsset: !condition.isRestricted,
          };

          expect(controller.canView(AssetDetailsController.SECTIONS.EXPORT))
            .toEqual(condition.expectedResult);
        });
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetDetailsController);
    });
  });
});
