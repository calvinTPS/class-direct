import './asset-details.scss';
import controller from './asset-details.controller';
import template from './asset-details.pug';

export default {
  bindings: {
    asset: '<',
    currentUser: '<',
    details: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
