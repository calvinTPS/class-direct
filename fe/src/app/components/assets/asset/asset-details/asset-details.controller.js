import * as _ from 'lodash';
import Base from 'app/base';

export default class AssetDetailsController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $filter,
    $mdDialog,
    $mdMedia,
    $sce,
    $scope,
    $state,
    $stateParams,
    $window,
    AppSettings,
    AssetService,
    CMROService,
    PermissionsService,
  ) {
    super(arguments);
  }

  $onInit() {
    this._getCustomerDetails();
    this._getSisterAssetsCount();

    // Use classNotation from the asset api for hullNotation. That's the requirement
    _.set(this.details, 'rulesetDetailsDto.hullNotation', this.asset.classNotation);

    _.set(this.details, 'registryInformation.lead',
      _.get(this.details, 'registryInformation.lead') === true ? 'Yes' : 'No'
    );

    this._modifyCableLength();

    this._displayLifeCycleStatusForIHSAsset();

    this.CMROLinks = this._CMROService.getLinks(this.asset.imo);

    this._AppSettings.FIELDS_TO_CONVERT_TO_FRUTIGER
      .forEach(field => this._updateFieldToFrutiger(field));
  }

  _updateFieldToFrutiger(theField) {
    if (_.get(this.details, theField)) {
      _.set(this.details, theField,
        this._$filter('convertToFrutiger')(_.get(this.details, theField)
      ));
    }
  }

  _getCustomerDetails() {
    // For IHS assets from the /asset/details/ api
    if (this.asset.isIHSAsset) {
      if (!this.details.ownership) return;

      _.forEach(AssetDetailsController.CUSTOMERS_PARTY_ROLES, (role) => {
        const key = role.key;
        if (this.details.ownership[key]) {
          this.details[key] = this.details.ownership[key];
        }
      });
    }

    // For MAST assets from the /asset/ api
    if (!this.asset.isIHSAsset) {
      if (!this.asset.customersH) return;

      _.forEach(AssetDetailsController.CUSTOMERS_PARTY_ROLES, (role) => {
        const key = role.key;

        // There could be more than one entry for each role as of 15/09/2017
        // Could be fixed in the api later
        const firstFound =
            _.find(this.asset.customersH, customer =>
              _.get(customer, 'partyRoles.id') === role.id);

        if (firstFound) {
          const data = _.get(firstFound, 'party.customer');
          if (data) {
            // Concat the address
            data.address = _.compact([
              data.addressLine1,
              data.addressLine2,
              data.addressLine3,
              data.city,
              data.postcode,
              data.country,
            ]).join(', ');

            this.details[key] = data;
          }
        }
      });
    }
  }

  _getSisterAssetsCount() {
    const { leadImo } = this.asset;
    const { assetId } = this._$stateParams;
    const leadShip = _.get(this.asset, 'ihsAssetDto.ihsAsset.leadShip', null);
    if (this._PermissionsService.canAccessSistersSection(this.asset, this.currentUser)) {
      if (leadShip) {
        this._AssetService.getSisterAssetsCount({
          assetId,
          leadShip,
          type: this._AppSettings.API_ENDPOINTS.RegisterBookSisters,
        }).then((response) => {
          this.registerBookSistersCount = response;
          this._$scope.$apply();
        });
      } else {
        this.registerBookSistersCount = 0;
      }

      this._AssetService.getSisterAssetsCount({
        assetId,
        leadImo,
        type: this._AppSettings.API_ENDPOINTS.TechnicalSisters,
      }).then((response) => {
        this.technicalSistersCount = response;
        this._$scope.$apply();
      });
    }
  }

  /*
  * Hide or show. Delegates to permissions service.
  */
  show(key) {
    return this._PermissionsService.canViewAssetDetailInfo(key, this.asset, this.currentUser);
  }

  /**
   * The visibility matrix:
   *                   | Accessible | Restricted | EOR | Equasis/Thetis   | Flag user
   * Technical Sisters |      /     |      X     |  X  |       X          |     /
   * Reg Book Sisters  |      /     |      X     |  X  |       X          |     /
   * Export Asset      |      /     |      /     |  /  |       X          |     /
   *
   * @param section
   * @returns {boolean} Whether or not the user can view that section.
   */
  canView(section) {
    switch (section) {
      case AssetDetailsController.SECTIONS.SISTER_VESSELS:
        return this._PermissionsService.canAccessSistersSection(this.asset, this.currentUser);
      case AssetDetailsController.SECTIONS.EXPORT:
        return this._PermissionsService.canPrintExport(this.currentUser);
      default:
        return false;
    }
  }

  displayInModal(cmroUrl, cmroName) {
    if (!this._$window.cordova) {
      throw new Error('Cannot display in mobile browser in non-mobile platform');
    } else {
      this._$window.cordova.InAppBrowser.open(this._AppSettings.BASE_URL + cmroUrl, '_blank');
    }
  }

  _modifyCableLength() {
    if (!_.isUndefined(_.get(this.details, 'equipmentDetails'))) {
      const equipmentCableLength = _.get(this.details, 'equipmentDetails.cableLength');
      if (_.isUndefined(equipmentCableLength) || equipmentCableLength === 0.0) {
        _.set(this.details, 'equipmentDetails.cableLength', '-');
      } else {
        _.set(this.details, 'equipmentDetails.cableLength', `${equipmentCableLength}m`);
      }
    }
  }

  _displayLifeCycleStatusForIHSAsset() {
    if (this.asset.isIHSAsset) {
      const assetLifeCycleStatus = _.get(this.asset, 'ihsAssetDto.ihsAsset.status', null);
      _.set(this.details, 'registryInformation.assetLifeCycleStatus', assetLifeCycleStatus);
    }
  }

  get displayCMRO() {
    return !!this.asset.imo && !this.currentUser.isEquasisThetis;
  }

  static SECTIONS = {
    EXPORT: 'export',
    SISTER_VESSELS: 'sisterVessels',
  }

  static CUSTOMERS_PARTY_ROLES = {
    REGISTERED_OWNER: {
      key: 'registeredOwner',
      id: 4,
    },
    GROUP_OWNER: {
      key: 'groupOwner',
      id: 6,
    },
    SHIP_OPERATOR: {
      key: 'operator',
      id: 2,
    },
    SHIP_MANAGER: {
      key: 'shipManager',
      id: 3,
    },
    TECHNICAL_MANAGER: {
      key: 'technicalManager',
      id: 14,
    },
    DOC_COMPANY: {
      key: 'doc',
      id: 10,
    },
  }
}
