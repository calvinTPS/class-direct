import angular from 'angular';
import AssetDetailsComponent from './asset-details.component';

export default angular.module('assetDetails', [])
.component('assetDetails', AssetDetailsComponent)
.name;
