const section1 = [
  // Registry information
  {
    title: 'cd-registry-information',
    key: 'registryInformation',
    map: [
      [
        { label: 'cd-port-of-registry', field: 'portOfRegistry' },
        { label: 'cd-call-sign', field: 'callSign' },
      ],
      [
        { label: 'cd-official-number', field: 'officialNumber' },
        { label: 'cd-asset-type', field: 'assetTypeDetails' },
      ],
      [
        { label: 'cd-former-asset-names', field: 'formerAssetNames' },
        { label: 'cd-yard', field: 'yard' },
      ],
      [
        { label: 'cd-yard-number', field: 'yardNumber' },
        { label: 'cd-keel-laying-date', field: 'keelLayingDate' },
      ],
      [
        { label: 'cd-date-of-build', field: 'dateOfBuild' },
        { label: 'cd-country-of-build', field: 'countryOfBuild' },
      ],
      [
        { label: 'cd-flag', field: 'flag' },
        { label: 'cd-builder', field: 'builder' },
      ],
      [
        { label: 'cd-mmsi-number', field: 'mmsiNumber' },
        { label: 'cd-asset-lifecycle-status', field: 'assetLifeCycleStatus' },
      ],
      [
        { label: 'cd-lead-technical-sister', field: 'lead', key: 'leadTechnicalSister' },
      ],
    ],
  },

  // Principal dimensions
  {
    title: 'cd-principal-dimensions-m-t',
    key: 'principalDimension',
    map: [
      [
        { label: 'cd-length-overall', field: 'lengthOverall' },
        { label: 'cd-length-between-perpendiculars', field: 'lengthBetweenPerpendiculars' },
      ],
      [
        { label: 'cd-breadth-moulded', field: 'breadthMoulded' },
        { label: 'cd-breadth-extreme', field: 'breadthExtreme' },
      ],
      [
        { label: 'cd-draught-max', field: 'draughtMax' },
        { label: 'cd-depth-moulded', field: 'depthMoulded' },
      ],
      [
        { label: 'cd-gross-tonnage', field: 'grossTonnage' },
        { label: 'cd-net-tonnage', field: 'netTonnage' },
      ],
      [
        { label: 'cd-deadweight', field: 'deadWeight' },
        { label: 'cd-decks', field: 'decks' },
      ],
      [
        { label: 'cd-propulsion', field: 'propulsion' },
      ],
    ],
  },

  // Rule set
  {
    title: 'cd-class-history-notations-and-descriptive-notes',
    key: 'rulesetDetailsDto',
    map: [
      [
        { label: 'cd-machinery-notation', field: 'machineryNotation', filter: 'convertToFrutiger | safeHtml' },
        { label: 'cd-descriptive-notes', field: 'descriptiveNote' },
      ],
      [
        { label: 'cd-hull-notation', field: 'hullNotation', filter: 'convertToFrutiger | safeHtml' },
      ],
    ],
  },

  // Equipment information
  {
    title: 'cd-equipment-information-m-t',
    key: 'equipmentDetails',
    map: [
      [
        { label: 'cd-cable-length', field: 'cableLength' },
        { label: 'cd-cable-grade', field: 'cableGrade' },
      ],
      [
        { label: 'cd-cable-diameter', field: 'cableDiameter' },
        { label: 'cd-equipment-letter', field: 'equipmentLetter' },
      ],
    ],
  },
];

const section2 = [
  // Registered Owner
  {
    title: 'cd-registered-owner',
    key: 'registeredOwner',
    map: [
      [
        { label: '', field: 'name', isFullWidth: true },
      ],
      [
        { label: 'cd-email-address', field: 'emailAddress' },
        { label: 'cd-phone-number', field: 'phoneNumber' },
      ],
      [
        { label: 'cd-address', field: 'address' },
      ],
    ],
  },

  // Group Owner
  {
    title: 'cd-group-owner',
    key: 'groupOwner',
    map: [
      [
        { label: '', field: 'name', isFullWidth: true },
      ],
      [
        { label: 'cd-email-address', field: 'emailAddress' },
        { label: 'cd-phone-number', field: 'phoneNumber' },
      ],
      [
        { label: 'cd-address', field: 'address' },
      ],
    ],
  },

  // Operator
  {
    title: 'cd-operator',
    key: 'operator',
    map: [
      [
        { label: '', field: 'name', isFullWidth: true },
      ],
      [
        { label: 'cd-email-address', field: 'emailAddress' },
        { label: 'cd-phone-number', field: 'phoneNumber' },
      ],
      [
        { label: 'cd-address', field: 'address' },
      ],
    ],
  },

  // Ship Manager
  {
    title: 'cd-ship-manager',
    key: 'shipManager',
    map: [
      [
        { label: '', field: 'name', isFullWidth: true },
      ],
      [
        { label: 'cd-email-address', field: 'emailAddress' },
        { label: 'cd-phone-number', field: 'phoneNumber' },
      ],
      [
        { label: 'cd-address', field: 'address' },
      ],
    ],
  },

  // Technical Manager
  {
    title: 'cd-technical-manager',
    key: 'technicalManager',
    map: [
      [
        { label: '', field: 'name', isFullWidth: true },
      ],
      [
        { label: 'cd-email-address', field: 'emailAddress' },
        { label: 'cd-phone-number', field: 'phoneNumber' },
      ],
      [
        { label: 'cd-address', field: 'address' },
      ],
    ],
  },

  // DOC Company
  {
    title: 'cd-doc-company',
    key: 'doc',
    map: [
      [
        { label: '', field: 'name', isFullWidth: true },
      ],
      [
        { label: 'cd-email-address', field: 'emailAddress' },
        { label: 'cd-phone-number', field: 'phoneNumber' },
      ],
      [
        { label: 'cd-address', field: 'address' },
      ],
    ],
  },
];

// CFO
const cfo = {
  title: 'cd-client-service-delivery-support-team',
  key: 'cfo',
  map: [
    [
      { label: '', field: 'name', isFullWidth: true },
    ],
    [
      { label: 'cd-email-address', field: 'emailAddress' },
      { label: 'cd-phone-number', field: 'phoneNumber' },
    ],
    [
      { label: 'cd-address', field: 'address' },
    ],
  ],
};


module.exports = {
  section1,
  section2,
  cfo,
};
