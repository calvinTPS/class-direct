import './assets-service-high-level.scss';
import controller from './assets-service-high-level.controller';
import ganttTemplate from './assets-service-high-level-gantt.pug';
import template from './assets-service-high-level.pug';

export default {
  bindings: {
    asset: '<',
    currentUser: '<',
    maxDate: '<',
    minDate: '<',
    view: '@',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  /* @ngInject */
  template: $attrs => $attrs.view === 'gantt' ? ganttTemplate : template,
};
