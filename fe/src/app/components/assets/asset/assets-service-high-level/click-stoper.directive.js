import * as _ from 'lodash';

/* @ngInject */

export default ($log, $timeout, $parse) => ({
  restrict: 'A',
  link: (
    scope,
    element,
    attributes,
  ) => {
    $timeout(() => {
      const callback = () => {
        $timeout(() => {
          const fn = $parse(attributes.clickStoper);
          fn(scope);
        });
      };

      element.on('click', (e) => {
        const tagNames = ['BUTTON', 'MD-ICON'];
        const index = _.indexOf(tagNames, e.target.tagName);

        // Search for registered element,
        // if the targeted element is exist then do nothing.
        if (index !== -1) {
          // This specific condition is used to check whether the click target
          // element is fav button or not
          if (
            e.target.tagName === 'MD-ICON' &&
            angular.element(e.target).closest('icon').getAttribute('name') !== 'star'
          ) {
            callback();
          }
          return;
        }

        callback();
      });
    }, 0);
  },
});
