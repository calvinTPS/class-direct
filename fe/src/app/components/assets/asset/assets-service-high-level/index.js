import angular from 'angular';
import AssetsServiceHighLevelComponent from './assets-service-high-level.component';
import clickStoper from './click-stoper.directive';
import uiRouter from 'angular-ui-router';

export default angular.module('assetsServiceHighLevel', [
  uiRouter,
])
.component('assetsServiceHighLevel', AssetsServiceHighLevelComponent)
.directive('clickStoper', clickStoper)
.name;
