/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import AssetsServiceHighLevelComponent from './assets-service-high-level.component';
import AssetsServiceHighLevelController from './assets-service-high-level.controller';
import AssetsServiceHighLevelModule from './';
import AssetsServiceHighLevelTemplate from './assets-service-high-level.pug';
import servicesMocks from './services.mocks.json';

describe('AssetsServiceHighLevel', () => {
  let $rootScope,
    $compile,
    makeController;

  const codicilsMocks = [
      { title: 'COC1', dueDateEpoch: Date.parse('2014-02-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC2', dueDateEpoch: Date.parse('2015-09-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC3', dueDateEpoch: Date.parse('2016-09-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC4', dueDateEpoch: Date.parse('2016-02-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC5', dueDateEpoch: Date.parse('2016-12-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC6', dueDateEpoch: Date.parse('2017-05-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC7', dueDateEpoch: Date.parse('2017-06-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC8', dueDateEpoch: Date.parse('2018-08-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'COC9', dueDateEpoch: Date.parse('2018-10-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'COC' },
      { title: 'AI1', dueDateEpoch: Date.parse('2019-02-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
      { title: 'AI2', dueDateEpoch: Date.parse('2020-05-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
      { title: 'AI3', dueDateEpoch: Date.parse('2021-08-11T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
      { title: 'AI4', dueDateEpoch: Date.parse('2022-10-10T00:00:00Z'), statusH: { name: 'Open' }, codicilType: 'AI' },
  ];

  beforeEach(window.module(AssetsServiceHighLevelModule));
  const mockAssetService = {
    removeFavourite: () => new Promise((resolve, reject) => resolve()),
    setFavourite: () => new Promise((resolve, reject) => resolve()),
  };
  const mockUserService = {
    getCurrentUser: () => Promise.resolve({ id: 1, name: 'testUser' }),
    logout: () => {},
    unsetCurrentUser: () => {},
  };

  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('$mdMedia', {});
      $provide.value('AssetService', mockAssetService);
      $provide.value('AppSettings', AppSettings);
      $provide.value('UserService', mockUserService);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('assetsServiceHighLevel', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('AssetServiceHighLevel._getPayload()', () => {
    it('should return a payload with future services when getting services', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller._getPayload('Services')).not.toEqual({});
      expect(controller._getPayload('Codicils')).toEqual({});
    });
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('isExpanded should return correct behaviour', () => {
      const controller = makeController();
      const fakeRecords = ['test', 'test2'];
      spyOn(controller, '_fetchServicesAndCodicils').and.callThrough();

      expect(controller.isExpanded).toBe(false);

      controller.codicils = fakeRecords;
      controller.services = fakeRecords;

      expect(controller.codicils).not.toEqual([]);
      expect(controller.services).not.toEqual([]);

      controller.isExpanded = true;

      expect(controller.isExpanded).toBe(true);
      expect(controller._fetchServicesAndCodicils).toHaveBeenCalled();
    });

    it.async('1/4 - _fetchServicesAndCodicils should return grouped codicils', async () => {
      const controller = makeController({ view: 'tabular' });
      // - Mock fake response
      const fnc = (assetId, type) => {
        if (type === 'Codicils') {
          return codicilsMocks;
        }
        return servicesMocks.content;
      };

      controller._AssetService.getByDueDate = (assetId, type) =>
        new Promise((resolve, reject) => resolve(fnc(assetId, type)));

      controller.asset = { id: 2 };

      const codicils = await controller._getServicesCodicilsData('Codicils', 'dueDateMin', 'dueDateMax');

      expect(codicils).toEqual(codicilsMocks);

      await controller._fetchServicesAndCodicils();

      expect(controller.isFetching).toBe(false);
      expect(controller.codicils[0].productName).toEqual('cd-notes-and-actions');
    });

    it.async('2/4 _fetchServicesAndCodicils should return Classification services as its own product family', async () => {
      const controller = makeController({ view: 'tabular' });
      // - Mock fake response
      const fnc = (assetId, type) => {
        if (type === 'Codicils') {
          return codicilsMocks;
        }
        return servicesMocks.content;
      };

      controller._AssetService.getByDueDate = (assetId, type) =>
        new Promise((resolve, reject) => resolve(fnc(assetId, type)));

      controller.asset = { id: 3 };

      const services = await controller._getServicesCodicilsData('Services', 'minDueDate', 'maxDueDate');

      expect(services).toEqual(servicesMocks.content);

      await controller._fetchServicesAndCodicils();
      expect(controller.isFetching).toBe(false);
      expect(controller.services[0].productName).toEqual('Hull');
      expect(controller.services[0].productName)
      .not.toEqual(AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.NAME);
    });

    it.async('3/4 _fetchServicesAndCodicils should return Statutory services in a group', async () => {
      const controller = makeController({ view: 'tabular' });
      // - Mock fake response
      const fnc = (assetId, type) => {
        if (type === 'Codicils') {
          return codicilsMocks;
        }
        return servicesMocks.content;
      };

      controller._AssetService.getByDueDate = (assetId, type) =>
        new Promise((resolve, reject) => resolve(fnc(assetId, type)));

      controller.asset = { id: 2 };

      const services = await controller._getServicesCodicilsData('Services', 'minDueDate', 'maxDueDate');

      expect(services).toEqual(servicesMocks.content);

      await controller._fetchServicesAndCodicils();

      expect(controller.isFetching).toBe(false);
      expect(controller.services[1].productName)
      .toEqual(AppSettings.PRODUCT_FAMILY_TYPES.STATUTORY.NAME);
    });

    it.async('3/4 _fetchServicesAndCodicils should return MMS services in a group', async () => {
      const controller = makeController({ view: 'tabular' });
      // - Mock fake response
      const fnc = (assetId, type) => {
        if (type === 'Codicils') {
          return codicilsMocks;
        }
        return servicesMocks.content;
      };

      controller._AssetService.getByDueDate = (assetId, type) =>
        new Promise((resolve, reject) => resolve(fnc(assetId, type)));

      controller.asset = { id: 2 };

      const services = await controller._getServicesCodicilsData('Services', 'minDueDate', 'maxDueDate');

      expect(services).toEqual(servicesMocks.content);

      await controller._fetchServicesAndCodicils();

      expect(controller.isFetching).toBe(false);
      expect(controller.services[2].productName).toEqual(AppSettings.PRODUCT_FAMILY_TYPES.MMS.NAME);
    });

    it('toggleExpandBtn() should behave correctly', () => {
      const controller = makeController();
      controller.isExpanded = true;
      expect(controller.isExpanded).toBe(true);
      controller.toggleExpandBtn();
      expect(controller.isExpanded).toBe(false);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetsServiceHighLevelComponent;

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetsServiceHighLevelController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<assets-service-high-level foo="bar"><assets-service-high-level/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('AssetsServiceHighLevel');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
