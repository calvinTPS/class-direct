import * as _ from 'lodash';
import Base from 'app/base';

export default class AssetsServiceHighLevelController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $mdMedia,
    $stateParams,
    $rootScope,
    AppSettings,
    AssetService,
    UserService,
  ) {
    super(arguments);
  }

  $onInit() {
    this._isExpanded = false;
    this.isFetching = false;

    this.codicils = [];
    this.services = [];
  }

  get isExpanded() {
    return this._isExpanded;
  }

  set isExpanded(arg) {
    this._isExpanded = arg;

    if (this._isExpanded) {
      this.fetchData(true);
    }
  }

  /**
   * Fetches services and codicils data. Used by gantt view template to trigger data
   * loading on demand.
   */
  fetchData(bool) {
    if (bool) {
      this.isFetching = true;
      this._fetchServicesAndCodicils();
    }
  }

  /**
  * This function is used to fetch codicils and services when users expanding the
  * asset card on view level. It should keep data. Subsequent calls return data immediately.
  */
  async _fetchServicesAndCodicils() {
    if (this._fetched) {
      this.isFetching = false;
    } else {
      const codicils = await this._getServicesCodicilsData('Codicils', 'dueDateMin', 'dueDateMax');
      const services = await this._getServicesCodicilsData('Services', 'minDueDate', 'maxDueDate');

      this.codicils = this._groupCodicils(codicils);
      this.services = this.view === AssetsServiceHighLevelController.VIEWTYPE.GANTT ?
        services : this._groupServices(services);

      this.isFetching = false;
      this._fetched = true;
      this._$scope.$apply(); // need to force digest in async func.
    }
  }

  /**
   * @param  {String} type - of Rider to retrieve;
   */
  _getPayload(type) {
    return (type === 'Services') ? { includeFutureDueDates: true } : {};
  }

  /**
   * This function is a skeleton function to fetch the codicils and services from BE
   *
   * @param {string} type
   * @param {string} minDateParam
   * @param {string} maxDateParam
   */
  async _getServicesCodicilsData(type, minDateParam, maxDateParam) {
    const payload = this._getPayload(type);

    payload[minDateParam] = this.minDate;
    payload[maxDateParam] = this.maxDate;

    return await this._AssetService.getByDueDate(
      this.asset.id, type, [minDateParam, maxDateParam], payload
    );
  }

  /**
   * This function is used to group codicils so that the data could be displayed
   * on cd-card-accordion component
   *
   * @param {array} codicils
   */

  _groupCodicils(codicils) {
    if (codicils.length && this.view === AssetsServiceHighLevelController.VIEWTYPE.TABULAR) {
      return [{
        productName: 'cd-notes-and-actions',
        details: codicils,
      }];
    }

    return codicils;
  }

  /**
   * Grouped the services according to their respective product's name
   *
   * Pairs the properties array and value array using _.zipObject
   *
   * @param {object} serviceScheduleServices
   */
  _groupServices(serviceScheduleServices) {
    // - An array of services is passed in

    const groupIfSameCatalogue = services =>
      _.forEach(services, (service) => {
        // - Only show the primary service
        // - Especially when there are two or more services, eg:
        // - 5 annual services for the next 5 years
        // - These additional service gets
        // - assigned to the parent service attribute
        if (!service.repeatOf) {
          _.set(service, 'myRepeatServices', _.filter(services, service2 =>
            service.serviceCatalogue.id === service2.serviceCatalogue.id
          ));
        }
      });

    // - Helper function to sort Classification
    const sortClassificatonServices = c => _.sortBy(c, [
      'serviceCatalogueH.productGroupH.displayOrder',
      'dueDateEpoch',
      'serviceCatalogueH.displayOrder',
      'occurrenceNumber']
    );

    // - Helper function to sort MMS and Statutory Services
    const sortOtherServices = c => _.sortBy(c, [
      'dueDateEpoch',
      'serviceCatalogueH.displayOrder',
      'occurrenceNumber']
    );

    const advanceSort = (services) => {
      const classificationServices = sortClassificatonServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.ID]));

      const statutoryServices = sortOtherServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.STATUTORY.ID]));

      const mmsServices = sortOtherServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.MMS.ID]));

      return _.concat(classificationServices, mmsServices, statutoryServices);
    };

    const removeIfNotPrimary = services =>
    // - Remove services that has this attribute
    // - Merged to primary service
    _.reject(services, service => service.repeatOf);

    // I will group the services into 3 buckets :
    // Classification, Statutory, MMS
    // Not required to display the classification label
    //
    // Product Groups(Classification label is not needed)
    //    Service Groups
    // MMS
    //    Service Groups
    // Statutory
    //    Service Groups
    const pairsCollection = c => _.toPairs(_.groupBy(c, (service) => {
      if (service.productFamilyName ===
        this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.NAME) {
        return service.productGroupName;
      }
      return service.productFamilyName;
    }));

    const mapCollection = c => _.map(c, r => _.zipObject(['productName', 'details'], r));

    return _.flow(
      groupIfSameCatalogue,
      removeIfNotPrimary,
      advanceSort,
      pairsCollection,
      mapCollection,
    )(serviceScheduleServices);
  }

  toggleExpandBtn(evt) {
    this.isExpanded = !this.isExpanded;
    this._$scope.$broadcast(this._AppSettings.EVENTS.EXPAND_ACCORDION,
      { groupKey: this.servicesGroupKey, isExpanded: this.isExpanded });
  }

  get servicesGroupKey() {
    return AssetsServiceHighLevelController.ACCORDION_GROUP.SERVICES;
  }

  get notesAndActionGroupKey() {
    return AssetsServiceHighLevelController.ACCORDION_GROUP.NOTES_AND_ACTIONS;
  }

  static ACCORDION_GROUP = {
    SERVICES: 'services',
    NOTES_AND_ACTIONS: 'notesAndActions',
  }

  static VIEWTYPE = {
    TABULAR: 'tabular',
    GANTT: 'gantt',
  }
}
