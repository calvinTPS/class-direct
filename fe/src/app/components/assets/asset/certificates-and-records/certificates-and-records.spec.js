/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef, no-underscore-dangle */
import AppSettings from 'app/config/project-variables.js';
import CertificateMocks from 'app/common/models/certificate/certificate.mocks.json';
import CertificateModel from 'app/common/models/certificate/';
import CertificatesAndRecordsComponent from './certificates-and-records.component';
import CertificatesAndRecordsController from './certificates-and-records.controller';
import CertificatesAndRecordsModule from './';
import CertificatesAndRecordsTemplate from './certificates-and-records.pug';

describe('CertificatesAndRecords', () => {
  let $compile;
  let $rootScope;
  let certificateMocks;
  let mockDownloadService;
  let makeController;
  let mockErrorService;

  beforeEach(window.module(CertificatesAndRecordsModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    mockDownloadService = {
      downloadCertificate: () => {},

    };

    mockErrorService = {
      showErrorDialog: () => {},
    };

    certificateMocks = [];
    _.forEach(CertificateMocks, (certificateObject, index) => {
      certificateMocks[index] = Reflect.construct(CertificateModel, [certificateObject]);
    });

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('AppSettings', AppSettings);
      $provide.value('DownloadService', mockDownloadService);
      $provide.value('ErrorService', mockErrorService);
      $provide.value('ViewService', { registerTemplate: (x, y) => {} });
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('certificatesAndRecords', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        certificates: certificateMocks,
        currentUser: {
          isEquasisThetis: false,
        },
      });

      const filterOptions = AppSettings.CERTIFICATES_FILTER_OPTIONS;
      filterOptions[2].options = [
        { name: 'Issued', value: 'Issued', selected: false },
        { name: 'Withdrawn', value: 'Withdrawn', selected: false },
        { name: 'null', value: 'null', selected: false },
      ];

      expect(controller.filterOptions).toEqual(AppSettings.CERTIFICATES_FILTER_OPTIONS);
      expect(controller.searchByList).toEqual(AppSettings.CERTIFICATES_SEARCH_BY_LIST);
      expect(controller.sortByList).toEqual(AppSettings.CERTIFICATES_SORT_BY_LIST);
      expect(controller.itemTemplate).toEqual(AppSettings.RIDER_TEMPLATES.CERTIFICATE_AND_RECORD);
      expect(controller.action.ngClick).not.toBeNull();
      expect(_.isFunction(controller.customSort.sortState)).toEqual(true);
    });

    describe('action', () => {
      it('calls the download service to download the certificate based on user role', () => {
        spyOn(mockDownloadService, 'downloadCertificate').and.returnValue({
          then: () => ({
            catch: () => true,
          }),
        });

        let controller = makeController({
          certificates: certificateMocks,
          currentUser: {
            isEquasisThetis: false,
          },
        });

        const cert = controller.certificates[0];

        controller.isRetrievingAttachements = true;
        controller.action.ngClick(cert);
        expect(mockDownloadService.downloadCertificate).not.toHaveBeenCalled();

        controller.isRetrievingAttachements = false;
        controller.action.ngClick(cert);

        expect(mockDownloadService.downloadCertificate).toHaveBeenCalledWith({
          certId: cert.id,
          jobId: cert.job.id,
        });

        controller = makeController({
          certificates: certificateMocks,
          currentUser: {
            isEquasisThetis: true,
          },
        });
        expect(controller.action).not.toBeDefined();
      });
    });

    it('sortState() should sort status accordingly', () => {
      const controller = makeController();
      let sequence;

      sequence = controller.sortState({
        status: AppSettings.CERTIFICATE_STATUS.STATUSES.ISSUED,
      });
      expect(sequence).toEqual(1);

      sequence = controller.sortState({
        status: AppSettings.CERTIFICATE_STATUS.STATUSES.WITHDRAWN,
      });
      expect(sequence).toEqual(2);

      sequence = controller.sortState({
        status: AppSettings.CERTIFICATE_STATUS.STATUSES.EXPIRED,
      });
      expect(sequence).toEqual(3);

      sequence = controller.sortState({
        status: AppSettings.CERTIFICATE_STATUS.STATUSES.FINAL,
      });
      expect(sequence).toEqual(4);

      sequence = controller.sortState({
        status: AppSettings.CERTIFICATE_STATUS.STATUSES.DRAFT,
      });
      expect(sequence).toEqual(5);

      sequence = controller.sortState({ status: 'any other string' });
      expect(sequence).toEqual(6);

      sequence = controller.sortState({});
      expect(sequence).toEqual(6);

      sequence = controller.sortState(undefined);
      expect(sequence).toEqual(6);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = CertificatesAndRecordsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(CertificatesAndRecordsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(CertificatesAndRecordsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<certificates-and-records foo="bar"><certificates-and-records/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('CertificatesAndRecords');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
