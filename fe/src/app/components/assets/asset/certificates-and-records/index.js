import angular from 'angular';
import CertificatesAndRecordsComponent from './certificates-and-records.component';
import uiRouter from 'angular-ui-router';

export default angular.module('certificatesAndRecords', [
  uiRouter,
])
.component('certificatesAndRecords', CertificatesAndRecordsComponent)
.name;
