import './certificates-and-records.scss';
import controller from './certificates-and-records.controller';
import template from './certificates-and-records.pug';

export default {
  bindings: {
    certificates: '<',
    currentUser: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
