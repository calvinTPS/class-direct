import * as _ from 'lodash';
import Base from 'app/base';

export default class CertificatesAndRecordsController extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    AppSettings,
    DownloadService,
    ErrorService,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    // Get resolution statuses from actual data
    const filterOptions = _.cloneDeep(this._AppSettings.CERTIFICATES_FILTER_OPTIONS);
    const certStatusOption = _.find(filterOptions, opt => opt.key === 'status');
    certStatusOption.options =
      _.keys(_.groupBy(this.certificates, 'status'))
        .map(key => ({ name: key, value: key, selected: false }));

    this.filterOptions = filterOptions;

    this.searchByList = this._AppSettings.CERTIFICATES_SEARCH_BY_LIST;
    this.sortByList = this._AppSettings.CERTIFICATES_SORT_BY_LIST;
    this.customSort = {
      sortState: this.sortState.bind(this),
    };
    this.itemTemplate = this._AppSettings.RIDER_TEMPLATES.CERTIFICATE_AND_RECORD;
    this.isRetrievingAttachements = false;

    if (this.currentUser && !this.currentUser.isEquasisThetis) {
      this.action = {
        clickArea: 'icon',
        hasGap: true,
        icon: 'download',
        ngClick: (certificate) => {
          if (this.isRetrievingAttachements) return;

          this.isRetrievingAttachements = true;

          this._DownloadService.downloadCertificate({
            certId: certificate.id,
            jobId: certificate.job.id,
          }).then(() => {
            this.isRetrievingAttachements = false;
          }).catch((response) => {
            this.isRetrievingAttachements = false;
            this._ErrorService.showErrorDialog({ message: 'cd-failed-to-retrieve-attachment' });
          });
        },
      };
    }
  }

  sortState(item) {
    if (item && this._AppSettings.CERTIFICATE_STATUS.ORDER[item.status]) {
      return this._AppSettings.CERTIFICATE_STATUS.ORDER[item.status];
    }
    return _.keys(this._AppSettings.CERTIFICATE_STATUS.STATUSES).length + 1;
  }
}
