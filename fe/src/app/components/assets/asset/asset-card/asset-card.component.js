import './asset-card.scss';
import controller from './asset-card.controller';
import tableTemplate from './asset-card-table.pug';
import tabularTemplate from './asset-card-tabular.pug';
import template from './asset-card.pug';

export default {
  restrict: 'E',
  bindings: {
    asset: '<',
    currentUser: '<',
    hideFavourite: '<?',
    isExpanded: '<?',
    isFavourite: '<',
    isShowingExpandBtn: '<?',
    mode: '@',
  },
  controllerAs: 'vm',
  controller,
  /* @ngInject */
  template: ($element, $attrs) => {
    if (!$attrs.mode || $attrs.mode === 'card') {
      return template;
    } else if ($attrs.mode === 'table') {
      return tableTemplate;
    }
    return tabularTemplate;
  },
};
