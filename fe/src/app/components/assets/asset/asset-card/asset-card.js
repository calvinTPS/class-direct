import angular from 'angular';
import assetCardComponent from './asset-card.component';
import uiRouter from 'angular-ui-router';

export default angular.module('assetCard', [
  uiRouter,
])
.component('assetCard', assetCardComponent)
.name;
