import Base from 'app/base';
import generalDialogTemplate from 'app/common/dialogs/general.pug';
import GeneralModalController from 'app/common/dialogs/general-modal.controller';

export default class AssetCardController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $scope,
    $state,
    AppSettings,
    AssetService,
    PermissionsService,
    UserService,
  ) {
    super(arguments);
    this.name = 'asset-card';
  }

  onMenuItemClick(actionKey) {
    switch (actionKey) {
      case AssetCardController.ACTION_KEY.REQUEST_SURVEY:
        if (!this.hasClientSupportOffice) {
          this.showRequestASurveyModal();
        } else if (this.canRequestSurvey) {
          this._$state.go(this._AppSettings.STATES.SURVEY_REQUEST, { assetId: this.asset.id });
        }
        break;
      case AssetCardController.ACTION_KEY.ASSET_DETAIL:
        this._$state.go(this._AppSettings.STATES.ASSET_DETAILS, { assetId: this.asset.id });
        break;
      case AssetCardController.ACTION_KEY.SURVEY_SCHEDULE:
        this.goToSurveyPlanner();
        break;
      case AssetCardController.ACTION_KEY.VIEW_ASSET:
        this.goToAssetHome();
        break;
      default:
        break;
    }
  }

  $onInit() {
    if (this.mode === this._AppSettings.VIEW_TYPES.TABLE) {
      this.currentUser = this.currentUser ? this.currentUser :
        this._AssetService.getAssetCurrentUser(this.name);

      if (!this.currentUser) {
        this._UserService.getCurrentUser()
          .then((currentUser) => {
            this.currentUser = currentUser;
            this._AssetService.setAssetCurrentUser(currentUser);
            // to avoid $cope.$apply() and update the asset-card-table menu
            this._setAssetCardTableMenuItems();
          });
      }
      this._setAssetCardTableMenuItems();
    }
  }

  goToAssetHome() {
    this._$state.go(this._AppSettings.STATES.ASSET_HOME, { assetId: this.asset.id });
  }

  goToSurveyPlanner() {
    this._$state.go(this._AppSettings.STATES.ASSET_SURVEY_PLANNER, { assetId: this.asset.id });
    // if (!this.asset.isIHSAsset && !this.asset.restricted) {
    //   this._$state.go(this._AppSettings.STATES.ASSET_SURVEY_PLANNER, { assetId: this.asset.id });
    // }
  }

  get canViewSurveyPlanner() {
    return !this.asset.isIHSAsset && !this.asset.restricted;
  }

  get canViewLRInfo() {
    return !this.asset.isIHSAsset && !this.asset.restricted && this.asset.isLRAsset;
  }

  get canRequestSurvey() {
    return this._PermissionsService.canAccessSurveyRequest(this.asset, this.currentUser);
  }

  get showStar() {
    return this._PermissionsService.canFavouriteAsset(this.currentUser) && !this.hideFavourite;
  }

  get hasClientSupportOffice() {
    // - Add type cooercion as cfoOffice can return undefined
    return !!(this.asset.cfoOffice && this.asset.cfoOffice.emailAddress);
  }

  get isFavorite() {
    return this.isFavourite || this.asset.isFavourite;
  }
  showRequestASurveyModal(ev) {
    if (ev) {
      // Need to stop parent event propagation so the card
      // won't expand when user click request survey button
      ev.stopPropagation();
    }
    if (!this.hasClientSupportOffice) {
      this._$mdDialog.show({
        bindToController: true,
        clickOutsideToClose: true,
        controller: GeneralModalController,
        controllerAs: 'vm',
        // fullscreen: true,
        locals: {
          body: 'cd-request-a-survey-functionality-not-available',
          buttons: [
            {
              class: 'modal-close-button',
              label: 'cd-close',
              onClick: 'vm._$mdDialog.cancel()',
              theme: 'light',
            },
          ],
        },
        parent: angular.element(this._$document[0].body),
        targetEvent: ev,
        template: generalDialogTemplate,
      });
    }
  }

  getYearOfBuild(buildDate) {
    return new Date(buildDate).getFullYear();
  }

  get assetCardTableMenuItems() {
    return this._assetCardTableMenuItems;
  }

  _setAssetCardTableMenuItems() {
    this._assetCardTableMenuItems = [
      {
        label: 'cd-request-survey',
        actionKey: AssetCardController.ACTION_KEY.REQUEST_SURVEY,
        class: { 'visible-half': !this.hasClientSupportOffice },
        isNotRenderable: !this.canRequestSurvey,
      },
      {
        label: 'cd-view-asset',
        actionKey: AssetCardController.ACTION_KEY.VIEW_ASSET,
      },
      {
        label: 'cd-survey-schedule',
        actionKey: AssetCardController.ACTION_KEY.SURVEY_SCHEDULE,
        isNotRenderable: this.asset.isIHSAsset || this.asset.restricted,
      },
      {
        label: 'cd-asset-details',
        actionKey: AssetCardController.ACTION_KEY.ASSET_DETAIL,
      },
    ];
  }

  static ACTION_KEY = {
    ASSET_DETAIL: 'assetDetails',
    REQUEST_SURVEY: 'requestSurvey',
    SURVEY_SCHEDULE: 'surveySchedule',
    VIEW_ASSET: 'viewAsset',
  }
}
