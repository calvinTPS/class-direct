/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable */
import angularMaterial from 'angular-material';
import AssetCardComponent from './asset-card.component';
import AssetCardController from './asset-card.controller';
import AssetCardModule from './asset-card';
import AssetCardTemplate from './asset-card.pug';
import AssetModel from 'app/common/models/asset/asset';
import AppSettings from 'app/config/project-variables.js';

describe('AssetCard', () => {
  let $rootScope,
    $compile,
    $mdDialog,
    $mdMedia,
    controller,
    mockState,
    mockAssetService,
    mockPermissionsService,
    makeController;

  beforeEach(window.module(
    AssetCardModule,
    angularMaterial,
  ));
  beforeEach(() => {
    const mockTranslateFilter = value => value;
    const mockAssetService = {
      removeFavourite: () => {},
      setFavourite: () => {}
    };
    mockPermissionsService = {
      canAccessSurveyRequest: () => true,
      canFavouriteAsset: () => true,
    };

    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
      $provide.value('AssetService', mockAssetService);
      $provide.value('AppSettings', AppSettings);
      $provide.value('PermissionsService', mockPermissionsService);
      $provide.value('UserService', { getCurrentUser: () => {}});
    });
  });

  beforeEach(inject(($componentController, _$rootScope_, _$mdDialog_, _$compile_, _$mdMedia_, _$state_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $mdDialog = _$mdDialog_;
    $mdMedia = _$mdMedia_;
    mockState = _$state_;
    makeController =
      (bindings = {}) => $componentController('assetCard', {
        $mdMedia,
        $state: mockState,
      }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    beforeEach(() => {
      controller = makeController({});
      controller.asset = { id: 1 };
      spyOn(mockState, 'go');
    });

    describe('canRequestSurvey', () => {
      it('delegates to permissions service', () => {
        expect(controller.canRequestSurvey).toEqual(true);
        mockPermissionsService.canAccessSurveyRequest = () => false;
        expect(controller.canRequestSurvey).toEqual(false);
      });
    });

    it('onMenuItemClick() should fire the correct event', () => {
      const ACTION_KEY = {
        ASSET_DETAIL: 'assetDetails',
        REQUEST_SURVEY: 'requestSurvey',
        SURVEY_SCHEDULE: 'surveySchedule',
        VIEW_ASSET: 'viewAsset',
      }

      controller.onMenuItemClick(ACTION_KEY.ASSET_DETAIL);
      expect(mockState.go).toHaveBeenCalledWith(AppSettings.STATES.ASSET_DETAILS, { assetId: 1 });

      mockState.go.calls.reset();
      controller.onMenuItemClick(ACTION_KEY.SURVEY_SCHEDULE);
      expect(mockState.go).toHaveBeenCalledWith(AppSettings.STATES.ASSET_SURVEY_PLANNER, { assetId: 1 });

      mockState.go.calls.reset();
      controller.onMenuItemClick(ACTION_KEY.VIEW_ASSET);
      expect(mockState.go).toHaveBeenCalledWith(AppSettings.STATES.ASSET_HOME, { assetId: 1 });

      mockState.go.calls.reset();
      spyOn(controller, 'showRequestASurveyModal');
      controller.onMenuItemClick(ACTION_KEY.REQUEST_SURVEY);
      expect(mockState.go).not.toHaveBeenCalled();
      expect(controller.showRequestASurveyModal).toHaveBeenCalled();
    });

    describe('hasClientSupportOffice', () => {
      it('return the correct value', () => {
        controller.asset = { cfoOffice: {
          code: 'GB110',
          emailAddress: 'glass@mail.com',
        } };

        expect(controller.hasClientSupportOffice).toBeTruthy();

        controller.asset.cfoOffice.emailAddress = null;
        expect(controller.hasClientSupportOffice).toBeFalsy();

        controller.asset.cfoOffice = null;
        expect(controller.hasClientSupportOffice).toBeFalsy();
      });
    });

    describe('showRequestASurveyModal', () => {
      it.async('should open up $mdDialog', async () => {
        // controller._$document[0] = angular.element('<asset-header user="user"><asset-header/>');
        spyOn($mdDialog, 'show').and.callThrough();
        controller.showRequestASurveyModal();
        expect($mdDialog.show).toHaveBeenCalled();
      });
    });

    describe('showStar', () => {
      it('delegates to permissions service', () => {
        expect(controller.showStar).toEqual(true);
        controller.hideFavourite = true;
        expect(controller.showStar).toEqual(false);
        controller.hideFavourite = false;
        expect(controller.showStar).toEqual(true);
        mockPermissionsService.canFavouriteAsset = () => false;
        expect(controller.showStar).toEqual(false);
        controller.hideFavourite = true;
        expect(controller.showStar).toEqual(false);
      });
    });

    describe('getYearOfBuild', () => {
      it('return correct year for a date', () => {
        const controller = makeController({ foo: 'bar' });
        controller.asset = { id: 1 };
        const buildDate = '2020-10-13';
        expect(controller.getYearOfBuild(buildDate)).toEqual(2020);
      });
    });

    describe('getter/setter', () => {
      it('canViewSurveyPlanner return correct value', () => {
        controller.asset.isIHSAsset = true;
        controller.asset.restricted = true;
        expect(controller.canViewSurveyPlanner).toEqual(false);

        controller.asset.isIHSAsset = false;
        controller.asset.restricted = true;
        expect(controller.canViewSurveyPlanner).toEqual(false);

        controller.asset.isIHSAsset = true;
        controller.asset.restricted = false;
        expect(controller.canViewSurveyPlanner).toEqual(false);

        controller.asset.isIHSAsset = false;
        controller.asset.restricted = false;
        expect(controller.canViewSurveyPlanner).toEqual(true);
      });

      it('canViewLRInfo return correct value', () => {
        controller.asset.isIHSAsset = true;
        controller.asset.restricted = true;
        controller.asset.isLRAsset = true
        expect(controller.canViewLRInfo).toEqual(false);

        controller.asset.isIHSAsset = false;
        controller.asset.restricted = true;
        controller.asset.isLRAsset = true
        expect(controller.canViewLRInfo).toEqual(false);

        controller.asset.isIHSAsset = true;
        controller.asset.restricted = false;
        controller.asset.isLRAsset = true
        expect(controller.canViewLRInfo).toEqual(false);

        controller.asset.isIHSAsset = false;
        controller.asset.restricted = false;
        controller.asset.isLRAsset = true
        expect(controller.canViewLRInfo).toEqual(true);

        controller.asset.isIHSAsset = true;
        controller.asset.restricted = true;
        controller.asset.isLRAsset = false;
        expect(controller.canViewLRInfo).toEqual(false);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetCardComponent;

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetCardController);
    });
  });

  describe('Rendering', () => {
    let scope,
        element;

    beforeEach(function () {
      scope = $rootScope.$new();
      element = angular.element('<asset-card asset="asset"></asset-card>');
      element = $compile(element)(scope);
      scope.asset = new AssetModel({
        id: 678910,
        name: 'El Capitan',
        code: "LRV678910",
        ihsAsset: {
          id: '123456',
        },
        assetTypeHDto: {
          name: 'Tanker',
        },
        grossTonnage: 1000,
        flagStateDto: {
          name: 'Malaysia',
        },
        isFavourite: true,
      });
      scope.$apply();
    });

    it('should render the asset name', () => {
      expect(element.text().indexOf('El Capitan')).toBeGreaterThan(-1);
    });

    it('should render the asset type', () => {
      expect(element.text().indexOf('Tanker')).toBeGreaterThan(-1);
    });

    it('should render the asset IMO', () => {
      expect(element.text().indexOf('123456')).toBeGreaterThan(-1);
    });

    it('should render the asset flag', () => {
      expect(element.text().indexOf('Malaysia')).toBeGreaterThan(-1);
    });

    it('should render the asset gross tonnage', () => {
      expect(element.text().indexOf('1000')).toBeGreaterThan(-1);
    });
  });

  describe('UI interaction', () => {
    let controller,
        element,
        scope;

    beforeEach(function () {
      scope = $rootScope.$new();
      element = angular.element('<asset-card asset="asset"></asset-card>');
      element = $compile(element)(scope);
      scope.asset = new AssetModel({
        id: 678910,
        code: "LRV678910",
        name: 'El Capitan',
        ihsAsset: {
          id: '123456',
        },
        assetTypeHDto: {
          name: 'Tanker',
        },
        grossTonnage: 1000,
        flagStateDto: {
          name: 'Malaysia',
        },
        isFavourite: true,
      });
      scope.$apply();

      controller = element.controller('assetCard');
    });
  });
});
