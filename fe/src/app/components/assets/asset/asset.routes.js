import * as _ from 'lodash';
import { STATES } from 'app/config/project-variables.js';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('asset', {
      abstract: true,
      url: '/assets/{assetId}',
      component: 'asset',
      resolve: {
        /* @ngInject */
        currentUser: (
          $state,
          $stateParams,
          AppSettings,
          UserService,
          TermsAndConditionsService
        ) => UserService.getCurrentUser()
          .then((user) => {
            if (user.isEquasisThetis) {
              if (!TermsAndConditionsService.hasEquasisThetisAcceptedTC()) {
                $state.go(
                  AppSettings.STATES.ACCEPT_TERMS_AND_CONDITIONS,
                  { assetId: $stateParams.assetId },
                );
                return Promise.reject();
              }
            }
            return Promise.resolve(user);
          }),

        /* @ngInject */
        asset: ($state, $rootScope, $stateParams, AppSettings, AssetService, currentUser,
          ViewService) => {
          if (ViewService.asset && ViewService.asset.id === $stateParams.assetId) {
            return ViewService.asset;
          }
          const assetPromise = AssetService.getOne($stateParams.assetId);

          assetPromise.then((asset) => {
            if (asset.isEOR) {
              currentUser.role = AppSettings.ROLES.EOR; // eslint-disable-line
            }
          });

          return assetPromise;
        },
      },
    })
    .state('asset.home', {
      url: '',
      views: {
        'asset-view': {
          component: 'assetHome',
        },
      },
    })
    .state('asset.surveyPlanner', {
      url: '/survey-planner',
      resolve: {
        /* @ngInject */
        serviceScheduleDateRange: ($stateParams, AssetService) =>
          AssetService.getServiceScheduleRange($stateParams.assetId),

        /* @ngInject */
        serviceScheduleServices: ($stateParams, AssetService, serviceScheduleDateRange) => {
          if (serviceScheduleDateRange == null) {
            return null;
          }

          return AssetService.getByDueDate($stateParams.assetId,
            AssetService.constructor.ASSET_ITEMS.Services,
            ['minDueDate', 'maxDueDate'], {
              minDueDate: serviceScheduleDateRange.lowRange,
              maxDueDate: serviceScheduleDateRange.highRange,
              includeFutureDueDates: true,
            });
        },

        /* @ngInject */
        serviceScheduleCodicils: ($stateParams, AssetService, serviceScheduleDateRange) => {
          if (serviceScheduleDateRange == null) {
            return null;
          }

          return AssetService.getByDueDate($stateParams.assetId,
            AssetService.constructor.ASSET_ITEMS.Codicils,
            ['dueDateMin', 'dueDateMax'], {
              dueDateMin: serviceScheduleDateRange.lowRange,
              dueDateMax: serviceScheduleDateRange.highRange,
            });
        },
      },
      views: {
        'asset-view': {
          component: 'surveyPlanner',
        },
      },
    })
    .state('asset.details', {
      url: '/details',
      resolve: {
        /* @ngInject */
        details: ($stateParams, AssetService, asset) => {
          if (asset.isEORAsset) {
            return AssetService.getEORDetails($stateParams.assetId);
          }
          return AssetService.getDetails($stateParams.assetId);
        },
      },
      views: {
        'asset-view': {
          component: 'assetDetails',
        },
      },
    })
    .state('asset.surveyRequest', {
      url: '/survey-request',
      resolve: {
        /* @ngInject */
        assetServices: ($stateParams, ServiceService) =>
          ServiceService.getAll($stateParams.assetId),
      },
      views: {
        'asset-view': {
          component: 'surveyRequestForm',
        },
      },
    })
    .state('asset.registerBookSisters', {
      url: '/register-book-sisters',
      resolve: {
        /* @ngInject */
        currentUser: UserService => UserService.getCurrentUser(),
        /* @ngInject */
        vessels: ($stateParams, asset, AppSettings, AssetService, $state) => {
          const leadShipDetail = _.get(asset, 'ihsAssetDto.ihsAsset.leadShip', null);
          let vesselDetails;
          if (!leadShipDetail) {
            $state.go(AppSettings.STATES.ERROR, {
              statusCode: 404,
            });
          } else {
            vesselDetails = AssetService
              .get(
              $stateParams.assetId,
              AppSettings.API_ENDPOINTS.RegisterBookSisters,
              { leadShip: leadShipDetail },
            );
          }
          return vesselDetails;
        },
        type: () => 'registerBook',
      },
      views: {
        'asset-view': {
          component: 'assetSisters',
        },
      },
    })
    .state('asset.technicalSisters', {
      url: '/technical-sisters',
      resolve: {
        /* @ngInject */
        currentUser: UserService => UserService.getCurrentUser(),
        /* @ngInject */
        vessels: ($stateParams, asset, AppSettings, AssetService) =>
          AssetService.get(
            $stateParams.assetId,
            AppSettings.API_ENDPOINTS.TechnicalSisters,
            { leadImo: asset.leadImo },
          ),
        type: () => 'technical',
      },
      views: {
        'asset-view': {
          component: 'assetSisters',
        },
      },
    })
    .state('asset.conditionsOfClassDetail', {
      url: '/conditions-of-class/{codicilId: int}',
      views: {
        'asset-view': {
          component: 'codicilDetails',
        },
      },
      resolve: {
        /* @ngInject */
        item: ($stateParams, AppSettings, CodicilService) =>
          CodicilService.getOne(
            $stateParams.assetId,
            AppSettings.API_ENDPOINTS.ASSET.Cocs,
            $stateParams.codicilId,
          ),
      },
    })
    .state('asset.assetNotesDetail', {
      url: '/asset-notes/{codicilId: int}',
      views: {
        'asset-view': {
          component: 'codicilDetails',
        },
      },
      resolve: {
        /* @ngInject */
        item: ($stateParams, AppSettings, CodicilService) =>
          CodicilService.getOne(
            $stateParams.assetId,
            AppSettings.API_ENDPOINTS.ASSET.AssetNotes,
            $stateParams.codicilId,
          ),
      },
    })
    .state('asset.actionableItemsDetail', {
      url: '/actionable-items/{codicilId: int}',
      views: {
        'asset-view': {
          component: 'codicilDetails',
        },
      },
      resolve: {
        /* @ngInject */
        item: ($stateParams, AppSettings, CodicilService) =>
          CodicilService.getOne(
            $stateParams.assetId,
            AppSettings.API_ENDPOINTS.ASSET.ActionableItems,
            $stateParams.codicilId,
          ),
      },
    })
    .state(STATES.MNCNS_DETAILS, {
      url: '/mncns/{codicilId: int}',
      views: {
        'asset-view': {
          component: 'codicilDetails',
        },
      },
      resolve: {
        /* @ngInject */
        item: ($stateParams, AppSettings, CodicilService) =>
          CodicilService.getOne(
            $stateParams.assetId,
            AppSettings.API_ENDPOINTS.ASSET.MNCNs,
            $stateParams.codicilId,
            {
              attachments: 'getMNCNAttachments',
            }
          ),
      },
    })
    .state('asset.defectsDetail', {
      url: '/defects/{defectId: int}',
      views: {
        'asset-view': {
          component: 'codicilDetails',
        },
      },
      resolve: {
        /* @ngInject */
        item: ($stateParams, AppSettings, DefectService) =>
          DefectService.getOne(
            $stateParams.assetId,
            AppSettings.API_ENDPOINTS.ASSET.Defect,
            $stateParams.defectId,
          ),
      },
    })
    .state('asset.certificatesAndRecords', {
      url: '/certificates-and-records',
      views: {
        'asset-view': {
          component: 'certificatesAndRecords',
        },
      },
      resolve: {
        /* @ngInject */
        certificates: ($stateParams, CertificateService) =>
          CertificateService.getCertificates($stateParams.assetId),
      },
    })
    .state('asset.mpms', {
      url: '/mpms',
      views: {
        'asset-view': {
          component: 'mpms',
        },
      },
      resolve: {
        /* @ngInject */
        taskItems: (ServiceService, $stateParams) =>
          ServiceService.getPMSableTasks($stateParams.assetId),
      },
    })
    .state('asset.statutoryFindingsDetail', {
      url: '/statutory-deficiencies/{deficiencyId: int}/statutory-findings/{codicilId: int}',
      views: {
        'asset-view': {
          component: 'codicilDetails',
        },
      },
      resolve: {
        /* @ngInject */
        item: ($stateParams, AppSettings, CodicilService) =>
          CodicilService.getStatutoryFindings(
            $stateParams.assetId,
            $stateParams.deficiencyId,
            $stateParams.codicilId,
          ),
      },
    })
    .state('asset.statutoryDeficienciesDetail', {
      url: '/statutory-deficiencies/{deficiencyId: int}',
      views: {
        'asset-view': {
          component: 'codicilDetails',
        },
      },
      resolve: {
        /* @ngInject */
        item: ($stateParams, AppSettings, DefectService) =>
          DefectService.getOne(
            $stateParams.assetId,
            AppSettings.API_ENDPOINTS.ASSET.Deficiencies,
            $stateParams.deficiencyId,
          ),
      },
    });
}
