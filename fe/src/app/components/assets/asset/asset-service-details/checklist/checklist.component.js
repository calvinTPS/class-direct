import './checklist.scss';
import controller from './checklist.controller';
import template from './checklist.pug';

/**
 * The checklist component. Should display an accordion of checklist items.
 *
 * @param checklist {Array} The array of checklist items.
 */
export default {
  bindings: {
    asset: '<',
    checklist: '<',
    service: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
