/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import ChecklistComponent from './checklist.component';
import ChecklistController from './checklist.controller';
import ChecklistModule from './';
import ChecklistTemplate from './checklist.pug';

describe('Checklist', () => {
  let $rootScope,
    $compile,
    $stateParams,
    makeController;

  const mockTranslateFilter = value => value;

  beforeEach(window.module(
    angularMaterial,
    ChecklistModule,
  ));

  beforeEach(window.module(($provide) => {
    $provide.value('translateFilter', mockTranslateFilter);
    $provide.value('AppSettings', AppSettings);
    $provide.value('ViewService', { registerTemplate: (x, y) => {} });
  }));

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    makeController =
      (bindings = {}) => {
        const controller = $componentController('checklist', { $stateParams }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const fakeChecklist = [{ test: 'test object' }];
      const controller = makeController({ foo: 'bar', checklist: fakeChecklist });
      expect(controller.foo).toEqual('bar');
      expect(controller.checklist).toEqual(fakeChecklist);
      expect(controller.accordionChecklistTemplate).toEqual(AppSettings.RIDER_TEMPLATES.CHECKLIST);
      expect(controller.searchByList).toEqual(AppSettings.CHECKLIST_SEARCH_BY_LIST);
    });

    it('checklist should be equal to [] when checklist is undefined', () => {
      const controller = makeController();
      expect(controller.checklist).toEqual([]);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ChecklistComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ChecklistTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ChecklistController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<checklist foo="bar"><checklist/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Checklist');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
