import Base from 'app/base';

export default class ChecklistController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    this.checklist = this.checklist || [];
    this.accordionChecklistTemplate = this._AppSettings.RIDER_TEMPLATES.CHECKLIST;
    this.searchByList = this._AppSettings.CHECKLIST_SEARCH_BY_LIST;
  }
}
