import angular from 'angular';
import ChecklistComponent from './checklist.component';
import uiRouter from 'angular-ui-router';

export default angular.module('checklist', [
  uiRouter,
])
.component('checklist', ChecklistComponent)
.name;
