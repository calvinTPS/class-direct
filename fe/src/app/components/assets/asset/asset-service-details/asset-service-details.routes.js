/* @ngInject */
export default function config(
  $stateProvider,
) {
  $stateProvider
  .state('asset.serviceDetails', {
    url: '/services/{serviceId: int}',
    views: {
      'asset-view': {
        component: 'assetServiceDetails',
      },
    },
    resolve: {
      /* @ngInject */
      service: ($stateParams, ServiceService) =>
        ServiceService.getOne($stateParams.assetId, $stateParams.serviceId),
    },
  })
  .state('asset.serviceDetails.checklist', {
    url: '/checklist',
    views: {
      'asset-service-view': {
        component: 'checklist',
      },
    },
    resolve: {
      /* @ngInject */
      checklist: ($stateParams, ServiceService) =>
        ServiceService.getChecklist($stateParams.assetId, $stateParams.serviceId),
    },
  })
  .state('asset.serviceDetails.tasklist', {
    url: '/tasklist',
    views: {
      'asset-service-view': {
        component: 'tasklist',
      },
    },
    resolve: {
      /* @ngInject */
      tasklistItems: ($stateParams, ServiceService) =>
        ServiceService.getTasklist($stateParams.assetId, $stateParams.serviceId),
    },
  });
}
