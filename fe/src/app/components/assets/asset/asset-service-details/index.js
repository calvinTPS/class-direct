import angular from 'angular';
import AssetServiceDetailsComponent from './asset-service-details.component';
import Checklist from './checklist';
import routes from './asset-service-details.routes';
import Tasklist from './tasklist';
import uiRouter from 'angular-ui-router';

export default angular.module('assetServiceDetails', [
  Checklist,
  Tasklist,
  uiRouter,
])
.config(routes)
.component('assetServiceDetails', AssetServiceDetailsComponent)
.name;
