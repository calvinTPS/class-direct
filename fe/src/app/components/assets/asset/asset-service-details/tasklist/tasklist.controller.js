import _ from 'lodash';
import Base from 'app/base';

export default class TasklistController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
    ViewService,
  ) {
    super(arguments);
  }

  $onInit() {
    if (!_.isEmpty(this.tasklistItems)) {
      this.flatTasklist = this._getTasksFromItems(this.tasklistItems);
    }

    this.accordionTasklistTemplate = this._AppSettings.RIDER_TEMPLATES.TASKLIST;
    this.searchByList = _.cloneDeep(this._AppSettings.TASK.SEARCH_BY_LIST);

    // Get resolution statuses from actual data
    const filterOptions = _.cloneDeep(this._AppSettings.TASK.FILTER_OPTIONS);
    const creditStatusOption = _.find(filterOptions, opt => opt.key === 'creditStatus');
    creditStatusOption.options =
      _.keys(_.groupBy(this.flatTasklist, 'creditStatus'))
        .map(rs => ({ name: rs, value: rs, selected: false }));

    this.filterOptions = filterOptions;
    this.filterHierarchy = this.filterHierarchy.bind(this);
  }

  // Create a subset of hierarchy from the original hierarchy
  filterHierarchy(wantedTasks) {
    if (_.isEmpty(wantedTasks)) return null;
    return this._buildHierarchy(this.tasklistItems, _.map(wantedTasks, 'id'));
  }

  // Return a flat array of all the tasks found
  _getTasksFromItems(items) {
    return items.reduce((tasks, item) => {
      if (!_.isEmpty(item.tasksH)) {
        // eslint-disable-next-line no-param-reassign
        tasks = tasks.concat(item.tasksH);
      }

      if (!_.isEmpty(item.itemsH)) {
        return tasks.concat(this._getTasksFromItems(item.itemsH));
      }

      return tasks;
    }, []);
  }

  // See the spec file for the hierarchy
  // each item created: { name: 'string', id: number, items: array, tasks: array }
  // NOTE:  itemsH will be returned as items and tasksH will be returned as tasks
  _buildHierarchy(items, wantedTaskIDs) {
    return _.compact(items.map((item) => {
      const newItem = {};
      let childItems = null;
      let tasks = null;

      if (!_.isEmpty(item.tasksH)) {
        tasks = _.filter(item.tasksH, task => _.indexOf(wantedTaskIDs, task.id) > -1);
      }

      if (!_.isEmpty(tasks)) {
        newItem.tasks = tasks;
      }

      if (!_.isEmpty(item.itemsH)) {
        childItems = this._buildHierarchy(item.itemsH, wantedTaskIDs);
      }

      if (!_.isEmpty(childItems)) {
        newItem.items = childItems;
      }

      if (_.isEmpty(newItem)) {
        return null;
      }

      newItem.id = item.id;
      newItem.name = item.name;
      newItem.displayOrder = item.displayOrder;

      return newItem;
    }));
  }
}
