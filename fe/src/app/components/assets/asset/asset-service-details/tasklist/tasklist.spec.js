/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import TasklistComponent from './tasklist.component';
import TasklistController from './tasklist.controller';
import TasklistModule from './';
import TasklistTemplate from './tasklist.pug';

describe('Tasklist', () => {
  let $rootScope,
    $compile,
    $stateParams,
    makeController;

  const mockTranslateFilter = value => value;

  beforeEach(window.module(
    TasklistModule,
    angularMaterial,
  ));
  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
    $provide.value('translateFilter', mockTranslateFilter);
    $provide.value('ViewService', { registerTemplate: (x, y) => {} });
  }));
  beforeEach(inject((_$rootScope_, _$compile_, _$stateParams_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $stateParams = _$stateParams_;

    makeController =
      (bindings = {}) => {
        const controller = $componentController('tasklist', { $stateParams }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
      expect(controller.searchByList).toEqual(AppSettings.TASK.SEARCH_BY_LIST);
      expect(controller.filterHierarchy).toBeDefined();
    });

    it('filterHierarchy(), should call the required methods', () => {
      const controller = makeController();
      spyOn(controller, '_buildHierarchy');
      controller.filterHierarchy();
      expect(controller._buildHierarchy).not.toHaveBeenCalled();
      controller.filterHierarchy([]);
      expect(controller._buildHierarchy).not.toHaveBeenCalled();
      controller.filterHierarchy([{}, {}]);
      expect(controller._buildHierarchy).toHaveBeenCalled();
    });

    it('_getTasksFromItems(), correctly return the tasks in a flattened array', () => {
      const controller = makeController();
      const items = [
        {
          itemsH: [
            {
              id: 1,
              name: 'Vessel',
              tasksH: [{ id: 10 }, { id: 20 }, { id: 30 }],
              itemsH: [
                {
                  id: 2,
                  name: 'Vessel 2',
                  tasksH: [{ id: 40 }, { id: 50 }],
                  itemsH: [
                    {
                      id: 3,
                      name: 'Vessel 3',
                      tasksH: [{ id: 60 }, { id: 70 }, { id: 80 }],
                    },
                    {
                      id: 4,
                      name: 'Vessel 3',
                      tasksH: [{ id: 90 }, { id: 100 }],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ];

      const tasks = controller._getTasksFromItems(items);
      expect(tasks.length).toEqual(10);
      expect(tasks[0].id).toEqual(10);
      expect(tasks[1].id).toEqual(20);
      expect(tasks[9].id).toEqual(100);
    });

    it('_buildHierarchy(), correctly builds hierarchial structure of the tasks', () => {
      const controller = makeController();
      const initialGrouping = [
        {
          id: 0,
          name: 'Vessel 0',
          displayOrder: 1,
          itemsH: [
            {
              id: 1,
              name: 'Vessel',
              displayOrder: 2,
              tasksH: [{ id: 10 }, { id: 20 }, { id: 30 }],
              itemsH: [
                {
                  id: 2,
                  name: 'Vessel 2',
                  displayOrder: 3,
                  tasksH: [{ id: 40 }, { id: 50 }],
                  itemsH: [
                    {
                      id: 3,
                      name: 'Vessel 3',
                      displayOrder: 4,
                      tasksH: [{ id: 60 }, { id: 70 }, { id: 80 }],
                    },
                    {
                      id: 4,
                      name: 'Vessel 3',
                      displayOrder: 5,
                      tasksH: [{ id: 90 }, { id: 100 }],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ];

      const wantedTaskIDs = [20, 30, 40, 70, 80];

      // With only tasks with id in wantedTaskIDs
      // NOTE:  itemsH will be returned as items and tasksH will be returned as tasks
      const expectedGrouping = [
        {
          id: 0,
          name: 'Vessel 0',
          displayOrder: 1,
          items: [
            {
              id: 1,
              name: 'Vessel',
              displayOrder: 2,
              tasks: [{ id: 20 }, { id: 30 }],
              items: [
                {
                  id: 2,
                  name: 'Vessel 2',
                  displayOrder: 3,
                  tasks: [{ id: 40 }],
                  items: [
                    {
                      id: 3,
                      name: 'Vessel 3',
                      displayOrder: 4,
                      tasks: [{ id: 70 }, { id: 80 }],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ];

      const newGrouping = controller._buildHierarchy(initialGrouping, wantedTaskIDs);

      expect(newGrouping).toEqual(expectedGrouping);
    });

    it('should combine list of known values of creditStatuses and from the actual data', () => {
      const tasklistItems = [
        {
          id: 0,
          name: 'Vessel 0',
          itemsH: [
            {
              id: 1,
              name: 'Vessel',
              tasksH: [
                { id: 10, creditStatus: 'New Status' },
                { id: 20, creditStatus: 'Complete' },
                { id: 30, creditStatus: 'No Status' }],
            },
          ],
        },
      ];

      const controller = makeController({ tasklistItems });
      expect(controller.filterOptions[0].options).toEqual([{
        name: 'New Status',
        value: 'New Status',
        selected: false,
      }, {
        name: 'Complete',
        value: 'Complete',
        selected: false,
      }, {
        name: 'No Status',
        value: 'No Status',
        selected: false,
      }]);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = TasklistComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(TasklistTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(TasklistController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<tasklist foo="bar"><tasklist/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('Tasklist');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
