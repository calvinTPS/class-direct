import angular from 'angular';
import TasklistComponent from './tasklist.component';
import uiRouter from 'angular-ui-router';

export default angular.module('tasklist', [
  uiRouter,
])
.component('tasklist', TasklistComponent)
.name;
