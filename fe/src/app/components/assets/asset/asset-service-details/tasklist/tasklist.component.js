import './tasklist.scss';
import controller from './tasklist.controller';
import template from './tasklist.pug';

export default {
  bindings: {
    asset: '<',
    service: '<',
    tasklistItems: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
