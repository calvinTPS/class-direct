/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import AssetServiceDetailsComponent from './asset-service-details.component';
import AssetServiceDetailsController from './asset-service-details.controller';
import AssetServiceDetailsModule from './';
import AssetServiceDetailsTemplate from './asset-service-details.pug';

describe('AssetServiceDetails', () => {
  let $rootScope,
    $state,
    $stateParams,
    $compile,
    makeController;

  const mockServices = [{
    id: 1,
    serviceCatalogueH: {
      name: 'Service 1',
      productCatalogue: { name: 'Product 1' },
    },
    serviceCreditStatusH: { name: 'credit status 1' },
    serviceStatusH: { name: 'service status 1' },
    workItemType: 'TASKLIST',
  }, {
    id: 2,
    serviceCatalogueH: {
      name: 'Service 2',
      productCatalogue: { name: 'Product 2' },
    },
    serviceCreditStatusH: { name: 'credit status 2' },
    serviceStatusH: { name: 'service status 2' },
    workItemType: 'CHECKLIST',
  }];

  const mockChecklist = [{
    id: 1,
    name: 'Checklist 1',
    conditionalParent: { id: 2 },
    creditStatusH: { name: 'checklist 1 status' },
  }];

  const mockTasklist = [{
    id: 3,
    name: 'A very long task name 2',
    number: 3456712,
    dueDate: '2016-11-12',
    assignedDate: '2016-11-11',
    creditStatusH: { name: 'extended' },
    postponement: { date: '2016-01-01' },
    postponementTypeH: { name: 'xxx' },
  }, {
    id: 4,
    name: 'A very long task name 3',
    number: null,
    dueDate: '2016-11-12',
    assignedDate: '2016-11-11',
    creditStatusH: { name: 'extended' },
    postponement: { date: null },
    postponementTypeH: { name: null },
  }];

  beforeEach(window.module(AssetServiceDetailsModule));
  beforeEach(window.module(($provide) => {
    $provide.value('AppSettings', AppSettings);
  }));
  beforeEach(inject((_$rootScope_, _$compile_, _$state_, _$stateParams_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $state = _$state_;
    $stateParams = _$stateParams_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('assetServiceDetails', { $dep: dep }, bindings);
        if (controller.$onInit) {
          controller.$onInit();
        }
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({
        foo: 'bar',
      });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetServiceDetailsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetServiceDetailsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetServiceDetailsController);
    });
  });
});
