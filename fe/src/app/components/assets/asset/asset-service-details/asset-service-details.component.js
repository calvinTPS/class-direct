import './asset-service-details.scss';
import controller from './asset-service-details.controller';
import template from './asset-service-details.pug';

export default {
  bindings: {
    service: '<',
    checklist: '<',
    tasklist: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
