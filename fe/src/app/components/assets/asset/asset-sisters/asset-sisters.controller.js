import Base from 'app/base';

export default class AssetSistersController extends Base {
  /* @ngInject */
  constructor(
    AppSettings,
  ) {
    super(arguments);
  }

  get title() {
    return this.type === 'registerBook' ? 'cd-the-following-are-the-register-book-assets'
      : 'cd-the-following-are-the-technical-assets';
  }
}
