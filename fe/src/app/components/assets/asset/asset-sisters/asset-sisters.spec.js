/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AssetSistersComponent from './asset-sisters.component';
import AssetSistersController from './asset-sisters.controller';
import AssetSistersModule from './asset-sisters';
import AssetSistersTemplate from './asset-sisters.pug';

describe('AssetSisters', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(AssetSistersModule));

  beforeEach(() => {
    const mockTranslateFilter = value => value;
    window.module(($provide) => {
      $provide.value('translateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('assetSisters', { $dep: dep }, bindings);
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('has a name property', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    it('shows the correct title', () => {
      const controller = makeController({ foo: 'bar', type: 'registerBook' });
      expect(controller.title).toEqual('cd-the-following-are-the-register-book-assets');
      controller.type = 'technical';
      expect(controller.title).toEqual('cd-the-following-are-the-technical-assets');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetSistersComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetSistersTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetSistersController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<asset-sisters foo="bar"><asset-sisters/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('AssetSisters');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
