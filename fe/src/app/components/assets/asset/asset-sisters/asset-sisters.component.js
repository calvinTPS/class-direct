import './asset-sisters.scss';
import controller from './asset-sisters.controller';
import template from './asset-sisters.pug';

export default {
  bindings: {
    asset: '<',
    currentUser: '<',
    vessels: '<',
    type: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
