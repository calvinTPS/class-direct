import angular from 'angular';
import assetSistersComponent from './asset-sisters.component';

export default angular.module('assetSisters', [])
.component('assetSisters', assetSistersComponent)
.name;
