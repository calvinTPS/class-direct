import './asset-header.scss';
import controller from './asset-header.controller';
import template from './asset-header.pug';
import templateCollapsed from './asset-header-collapsed.pug';

export default {
  restrict: 'E',
  bindings: {
    asset: '<',
    currentUser: '<',
  },
  template:
    /* @ngInject */
    $attrs => ($attrs.collapsed) ?
        templateCollapsed :
        template,
  controllerAs: 'vm',
  controller,
};
