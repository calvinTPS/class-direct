/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from '../../../../config/project-variables.js';
import AssetHeaderCollapsedTemplate from './asset-header-collapsed.pug';
import AssetHeaderComponent from './asset-header.component';
import AssetHeaderController from './asset-header.controller';
import AssetHeaderModule from './asset-header';
import AssetHeaderTemplate from './asset-header.pug';
import AssetModel from 'app/common/models/asset/asset';
import AssetService from 'app/services/asset.service';

describe('AssetHeader', () => {
  let $rootScope,
    $document,
    $mdDialog,
    $mdMedia,
    $timeout,
    $window,
    makeController,
    mockPermissionsService,
    mockState,
    assetService;

  const assetData = {
    id: 123456,
    code: 'LRV123456',
    name: 'El Capitan',
    clientName: 'Captain America',
    ihsAsset: {
      id: '123456',
    },
    assetTypeHDto: {
      name: 'Tanker',
    },
    grossTonnage: 1000,
    flagStateDto: {
      name: 'Malaysia',
    },
    isFavourite: true,
  };

  const windowObj = {
    location: { href: '', protocol: 'http:', hostname: 'localhost', port: 8080 },
    history: { go: x => x },
  };

  beforeEach(window.module(($provide) => {
    const mockUtilsFactory = {
      getTabularAction: () => { },
      scrollToTop: () => { },
    };
    const mockViewService = {
      isServiceScheduleOnDetailMode: false,
    };

    $provide.value('utils', mockUtilsFactory);
    $provide.value('ViewService', mockViewService);
    $provide.value('$window', windowObj);
  }));

  beforeEach(window.module(
    angularMaterial,
  ));

  beforeEach(inject((_$rootScope_, _$document_, _$mdDialog_, _$mdMedia_, _$timeout_, _$window_,
    _$controller_) => {
    $rootScope = _$rootScope_;
    $document = _$document_;
    $mdDialog = _$mdDialog_;
    $mdMedia = _$mdMedia_;
    $timeout = _$timeout_;
    $window = _$window_;

    const scope = $rootScope.$new();
    mockPermissionsService = {
      canAccessSurveyRequest: () => true,
      canFavouriteAsset: () => true,
    };
    mockState = {
      current: {
        name: AppSettings.STATES.ASSET_DETAILS,
      },
      previous: {
        name: '',
      },
      go: () => { },
    };
    makeController = (bindings) => {
      assetService = new AssetService(AppSettings, {});
      spyOn(assetService, 'getOne');
      spyOn(assetService, 'getAll');
      const controller = _$controller_(
        AssetHeaderController, {
          $scope: scope,
          $rootScope,
          $mdMedia,
          $state: mockState,
          AppSettings,
          PermissionsService: mockPermissionsService,
          $timeout,
        }, bindings);

      controller.$onInit();
      return controller;
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    beforeEach(() => {
      controller = makeController({
        currentUser: {},
        asset: { isLRAsset: true, restricted: false },
      });
    });

    it('serviceScheduleMode should return correct value', () => {
      expect(controller.serviceScheduleMode).toBe(false);
    });

    it('goBackToAssetHome function behaves correctly', () => {
      mockState.previous.name = AppSettings.STATES.VESSEL_LIST;
      const spyScroll = spyOn(mockState, 'go');
      controller.goBackToAssetHome();
      expect(mockState.go).toHaveBeenCalledWith(AppSettings.STATES.VESSEL_LIST);
      mockState.previous.name = 'xx';
      controller.goBackToAssetHome();
      expect(mockState.go).toHaveBeenCalledWith(AppSettings.STATES.ASSET_HOME);
    });

    it('toggleFavourite', () => {
      controller.asset = { isFavourite: true };
      controller.toggleFavourite();
      expect(controller.asset.isFavourite).toBe(false);

      controller.toggleFavourite();
      expect(controller.asset.isFavourite).toBe(true);
    });

    it('should update isFavourite : onFavouriteToggle', () => {
      controller.asset = { isFavourite: true };
      controller.onFavouriteToggle({ isFavourite: true });
      expect(controller.asset.isFavourite).toBe(true);
      controller.onFavouriteToggle({ isFavourite: false });
      expect(controller.asset.isFavourite).toBe(false);
    });

    it('hide() should return correct true false value for Asset Details button', () => {
      controller.currentUser = { roles: [] };
      controller._$state.current.name = AppSettings.STATES.ASSET_DETAILS;
      expect(controller.hide('details')).toEqual(false);
      controller._$state.current.name = AppSettings.STATES.SURVEY_REQUEST;
      expect(controller.hide('details')).toEqual(true);
    });

    it('hide() should return correct true false value for Request Survey button', () => {
      controller.currentUser = {};
      controller.asset = _.clone(assetData);

      const reset = () => {
        controller._$state.current.name = AppSettings.STATES.ASSET_DETAILS;
        mockPermissionsService.canAccessSurveyRequest = () => true;
      };

      reset();
      expect(controller.hide('requestSurvey')).toEqual(true);

      reset();
      controller._$state.current.name = AppSettings.STATES.SURVEY_REQUEST;
      expect(controller.hide('requestSurvey')).toEqual(false);

      reset();
      mockPermissionsService.canAccessSurveyRequest = () => false;
      expect(controller.hide('requestSurvey')).toEqual(false);
    });

    it('goBack() should redirect page correctly', () => {
      const windowSpy = spyOn(windowObj.history, 'go');
      const stateSpy = spyOn(mockState, 'go');

      controller.goBack();
      expect(windowSpy).not.toHaveBeenCalled();
      expect(stateSpy).toHaveBeenCalledWith('asset.home');

      controller._$state.current.name = AppSettings.STATES.ASSET_TECHNICAL_VESSELS;
      controller.goBack();
      expect(windowSpy).not.toHaveBeenCalled();
      expect(stateSpy).toHaveBeenCalledWith('asset.details');

      controller._$state.previous.name = AppSettings.STATES.SURVEY_REQUEST;
      controller.goBack();
      expect(windowSpy).not.toHaveBeenCalled();
      expect(stateSpy).toHaveBeenCalledWith('asset.details');

      // Test whitelisted state
      controller._$state.current.name = AppSettings.STATES.ASSET_HOME;
      controller.goBack();
      expect(windowSpy).not.toHaveBeenCalled();
      expect(stateSpy).toHaveBeenCalledWith('fleet.vessel');

      controller._$state.previous.name = AppSettings.STATES.VESSEL_LIST;
      controller.goBack();
      expect(windowSpy).toHaveBeenCalledWith(-1);
      expect(windowSpy).toHaveBeenCalledTimes(1);

      // Test blacklisted state
      controller._$state.current.name = AppSettings.STATES.ASSET_DETAILS;
      controller._$state.previous.name = AppSettings.STATES.VESSEL_LIST;
      controller.goBack();
      expect(windowSpy).toHaveBeenCalledWith(-1);
      expect(windowSpy).toHaveBeenCalledTimes(2);

      controller._$state.previous.name = AppSettings.STATES.ASSET_TECHNICAL_VESSELS;
      controller.goBack();
      expect(stateSpy).toHaveBeenCalledWith('asset.home');

      // Test EOR user
      controller._$state.previous.name = AppSettings.STATES.ASSET_DETAILS;
      controller._$state.current.name = AppSettings.STATES.ASSET_HOME;
      controller._$rootScope.role_mode = AppSettings.ROLES.EOR;
      controller.goBack();
      expect(stateSpy).toHaveBeenCalledWith('exhibitionOfRecords.vessel');

      // Test EOR User with only EOR role default previous state
      controller._$rootScope.role_mode = null;
      controller.currentUser.isEORUserOnly = true;
      controller.goBack();
      expect(stateSpy).toHaveBeenCalledWith('exhibitionOfRecords.vessel');

      // Test Equasis User
      controller.currentUser.isEquasisThetis = true;
      controller.currentUser.roles = [AppSettings.ROLES.EQUASIS];
      controller.goBack();
      expect(windowObj.location.href).toEqual(AppSettings.EQUASIS_HOME_URL);

      // Test Thetis User
      controller.currentUser.roles = ['Foo'];
      controller.goBack();
      expect(windowObj.location.href).toEqual(AppSettings.THETHIS_HOME_URL);
    });

    it('get backButtonTooltip() should return the correct text', () => {
      expect(controller.backButtonTooltip).toEqual('cd-back');

      controller.currentUser.isEquasisThetis = true;
      controller.currentUser.roles = [AppSettings.ROLES.EQUASIS];
      expect(controller.backButtonTooltip).toEqual('cd-return-to-equasis');

      controller.currentUser.roles = [AppSettings.ROLES.THETIS];
      expect(controller.backButtonTooltip).toEqual('cd-return-to-thetis');
    });

    it('get hasClientSupportOffice() should return the correct value', () => {
      controller.asset = { cfoOffice: {
        code: 'GB110',
        emailAddress: 'glass@mail.com',
      } };

      expect(controller.hasClientSupportOffice).toBeTruthy();

      controller.asset.cfoOffice.emailAddress = null;
      expect(controller.hasClientSupportOffice).toBeFalsy();

      controller.asset.cfoOffice = null;
      expect(controller.hasClientSupportOffice).toBeFalsy();
    });

    it.async('showRequestASurveyModal() should open up $mdDialog', async () => {
      controller._$document[0] = angular.element('<asset-header user="user"><asset-header/>');
      spyOn($mdDialog, 'show').and.callThrough();
      controller.showRequestASurveyModal();
      expect($mdDialog.show).toHaveBeenCalled();
    });

    describe('showStar', () => {
      it('delegates to permissions service', () => {
        expect(controller.showStar).toEqual(true);
        mockPermissionsService.canFavouriteAsset = () => false;
        expect(controller.showStar).toEqual(false);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetHeaderComponent;

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetHeaderController);
    });
  });

  describe('UI interaction', () => {
    it('clicking on star button should update asset model', () => {
      const controller = makeController({
        asset: { isFavourite: true },
      });
      expect(controller.asset.isFavourite).toBeTruthy();

      controller.toggleFavourite();
      expect(controller.asset.isFavourite).toBeFalsy();

      controller.toggleFavourite();
      expect(controller.asset.isFavourite).toBeTruthy();
    });
  });
});
