import angular from 'angular';
import assetClientNameComponent from './asset-client-name.component';
import uiRouter from 'angular-ui-router';

export default angular.module('assetClientName', [
  uiRouter,
])
.component('assetClientName', assetClientNameComponent)
.name;
