import Base from 'app/base';

export default class AssetClientNameController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    AppSettings,
  ) {
    super(arguments);
  }

  goToVesselList() {
    if (this.currentUser.isEquasisThetis) {
      // Equasis & thetis shouldn't do anything.
      return;
    } else if (
      this._$rootScope.role_mode === this._AppSettings.ROLES.EOR ||
      this.currentUser.isEORUserOnly ||
      this.currentUser.role === this._AppSettings.ROLES.EOR
    ) {
      this._$state.go(this._AppSettings.STATES.EOR_VESSEL_LIST);
    } else {
      this._$state.go(this._AppSettings.STATES.VESSEL_LIST);
    }
  }
}
