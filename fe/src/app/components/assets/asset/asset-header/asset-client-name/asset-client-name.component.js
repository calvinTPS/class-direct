import './asset-client-name.scss';
import controller from './asset-client-name.controller';
import template from './asset-client-name.pug';

export default {
  restrict: 'E',
  bindings: {
    name: '<',
    currentUser: '<',
  },
  template,
  controllerAs: 'vm',
  controller,
};
