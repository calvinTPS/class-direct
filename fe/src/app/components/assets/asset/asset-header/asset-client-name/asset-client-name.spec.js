/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import AssetClientNameComponent from './asset-client-name.component';
import AssetClientNameController from './asset-client-name.controller';
import AssetClientNameModule from './asset-client-name';
import AssetClientNameTemplate from './asset-client-name.pug';

describe('AssetClientName', () => {
  let $rootScope,
    $state,
    $compile,
    makeController;

  const mockCurrentUser = {
    isEquasisThetis: false,
  };

  beforeEach(window.module(AssetClientNameModule));

  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('AppSettings', AppSettings);
      $provide.value('currentUser', mockCurrentUser);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$state_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $state = _$state_;

    const dep = {};
    makeController =
      (bindings = {}) => $componentController('assetClientName', { $dep: dep }, bindings);
  }));

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });

    describe('goToVesselList() should redirect user to the correct state', () => {
      it('should go to vessel list for NON EOR users', () => {
        spyOn($state, 'go');
        const controller = makeController({ currentUser: mockCurrentUser });

        $rootScope.role_mode = 'NOT EOR';
        controller.currentUser.role = 'NOT EOR';
        controller.goToVesselList();
        expect($state.go).toHaveBeenCalledWith('fleet.vessel');
        expect($state.go).not.toHaveBeenCalledWith('exhibitionOfRecords.vessel');
      });

      it('should go to EOR vessel list for EOR user (currentUser)', () => {
        spyOn($state, 'go');
        const controller = makeController({ currentUser: mockCurrentUser });

        $rootScope.role_mode = 'NOT EOR';
        controller.currentUser.role = 'EOR';
        controller.goToVesselList();
        expect($state.go).not.toHaveBeenCalledWith('fleet.vessel');
        expect($state.go).toHaveBeenCalledWith('exhibitionOfRecords.vessel');
      });

      it('should go to EOR vessel list for EOR user ($rootScope)', () => {
        spyOn($state, 'go');
        const controller = makeController({ currentUser: mockCurrentUser });

        $rootScope.role_mode = 'EOR';
        controller.currentUser.role = 'NOT EOR';
        controller.goToVesselList();
        expect($state.go).not.toHaveBeenCalledWith('fleet.vessel');
        expect($state.go).toHaveBeenCalledWith('exhibitionOfRecords.vessel');
      });

      it('should go not go to any vessel list for Equasis Thetis user', () => {
        spyOn($state, 'go');
        const controller = makeController({ currentUser: mockCurrentUser });

        controller.currentUser.isEquasisThetis = true;
        controller.goToVesselList();
        expect($state.go).not.toHaveBeenCalledWith('fleet.vessel');
        expect($state.go).not.toHaveBeenCalledWith('exhibitionOfRecords.vessel');
      });
    });
  });

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetClientNameComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetClientNameTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetClientNameController);
    });
  });
});
