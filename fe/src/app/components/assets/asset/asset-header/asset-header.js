import angular from 'angular';
import assetHeaderComponent from './asset-header.component';
import uiRouter from 'angular-ui-router';

export default angular.module('assetHeader', [
  uiRouter,
])
.component('assetHeader', assetHeaderComponent)
.name;
