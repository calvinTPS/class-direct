import * as _ from 'lodash';
import Base from 'app/base';
import generalDialogTemplate from 'app/common/dialogs/general.pug';
import GeneralModalController from 'app/common/dialogs/general-modal.controller';

export default class AssetHeaderController extends (Base) {
  /* @ngInject */
  constructor(
    $document,
    $mdDialog,
    $mdMedia,
    $rootScope,
    $scope,
    $state,
    $timeout,
    $window,
    AppSettings,
    PermissionsService,
    utils,
    ViewService,
  ) {
    super(arguments);
  }

  onFavouriteToggle({ isFavourite }) {
    this.asset.isFavourite = isFavourite;
  }

  $onInit() {
    this.name = 'AssetHeaderController';

    // - Sample, event listeners are added in init block
    // this._$window.ondeviceorientation = () => {
    //   this._$state.reload();
    // };
  }

  // - To remove after use
  // $onDestroy() {
  //   this._$window.ondeviceorientation = null;
  // }

  get serviceScheduleMode() {
    return this._ViewService.isServiceScheduleOnDetailMode;
  }

  toggleFavourite() {
    this.asset.isFavourite = !this.asset.isFavourite;
  }

  goBackToAssetHome() {
    if (this._$state.previous.name === this._AppSettings.STATES.VESSEL_LIST) {
      this._$state.go(this._AppSettings.STATES.VESSEL_LIST);
    } else {
      this._$state.go(this._AppSettings.STATES.ASSET_HOME);
    }
  }

  hide(field) {
    const {
      SURVEY_REQUEST,
      REQUEST_SUBMISSION_CONFIRMATION,
      ASSET_DETAILS,
    } = this._AppSettings.STATES;
    let can = true;

    switch (field) {
      case AssetHeaderController.FIELDS.DETAILS:
        can = !_.includes([ASSET_DETAILS], this._$state.current.name);
        break;
      case AssetHeaderController.FIELDS.REQUEST_SURVEY:
        can =
          this._PermissionsService.canAccessSurveyRequest(this.asset, this.currentUser) &&
          !_.includes(
            [SURVEY_REQUEST, REQUEST_SUBMISSION_CONFIRMATION],
            this._$state.current.name
          );
        break;
      default:
    }
    return can;
  }

  goBack() {
    if (this.currentUser.isEquasisThetis) {
      if (_.includes(this.currentUser.roles, this._AppSettings.ROLES.EQUASIS)) {
        this._$window.location.href = this._AppSettings.EQUASIS_HOME_URL;
      } else {
        this._$window.location.href = this._AppSettings.THETHIS_HOME_URL;
      }
    } else {
      const currentState = this._$state.current;
      const previousState = this._$state.previous;
      const backStates = _.clone(this._AppSettings.PREVIOUS_STATES);

      if (previousState.name && _.get(backStates[currentState.name], 'useHistoryFirst')) {
        if (
          (!backStates[currentState.name].disallowedStates &&
            !backStates[currentState.name].allowedStates) ||
          (backStates[currentState.name].disallowedStates &&
            _.indexOf(backStates[currentState.name].disallowedStates, previousState.name) === -1) ||
          (backStates[currentState.name].allowedStates &&
            _.indexOf(backStates[currentState.name].allowedStates, previousState.name) > -1)
        ) {
          this._$window.history.go(-1);
        } else {
          this._goToDefaultPreviousState(currentState, backStates);
        }
      } else if (backStates[currentState.name]) {
        this._goToDefaultPreviousState(currentState, backStates);
      } else {
        // Default redirection to asset home
        this._$state.go(this._AppSettings.STATES.ASSET_HOME);
      }
    }
  }

  get backButtonTooltip() {
    let tooltipText = 'cd-back';
    if (this.currentUser.isEquasisThetis) {
      if (_.includes(this.currentUser.roles, this._AppSettings.ROLES.EQUASIS)) {
        tooltipText = 'cd-return-to-equasis';
      } else if (_.includes(this.currentUser.roles, this._AppSettings.ROLES.THETIS)) {
        tooltipText = 'cd-return-to-thetis';
      }
    }
    return tooltipText;
  }

  /**
   * Redirects users back to the previous default state depending on role.
   */
  _goToDefaultPreviousState(currentState, backStates) {
    const defaultPreviousState = backStates[currentState.name].defaultPreviousState;
    if (
      this._$rootScope.role_mode === this._AppSettings.ROLES.EOR ||
      this.currentUser.isEORUserOnly
    ) {
      this._$state.go(this._AppSettings.STATES.EOR_VESSEL_LIST);
    } else {
      this._$state.go(defaultPreviousState);
    }
  }

  get showStar() {
    return this._PermissionsService.canFavouriteAsset(this.currentUser);
  }

  get hasClientSupportOffice() {
    return this.asset.cfoOffice && this.asset.cfoOffice.emailAddress;
  }

  showRequestASurveyModal(ev) {
    this._$mdDialog.show({
      bindToController: true,
      clickOutsideToClose: true,
      controller: GeneralModalController,
      controllerAs: 'vm',
      // fullscreen: true,
      locals: {
        body: 'cd-request-a-survey-functionality-not-available',
        buttons: [
          {
            class: 'modal-close-button',
            label: 'cd-close',
            onClick: 'vm._$mdDialog.cancel()',
            theme: 'light',
          },
        ],
      },
      parent: angular.element(this._$document[0].body),
      targetEvent: ev,
      template: generalDialogTemplate,
    });
  }

  static FIELDS = {
    DETAILS: 'details',
    REQUEST_SURVEY: 'requestSurvey',
  }
}
