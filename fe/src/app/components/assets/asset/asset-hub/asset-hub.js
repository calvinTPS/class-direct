import angular from 'angular';
import assetHubComponent from './asset-hub.component';
import uiRouter from 'angular-ui-router';

export default angular.module('assetHub', [
  uiRouter,
])
.component('assetHub', assetHubComponent)
.name;
