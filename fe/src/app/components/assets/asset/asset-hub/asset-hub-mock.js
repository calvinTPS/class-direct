export const surveyReportsMock = [
  {
    id: 1,
    job: {
      id: 1,
      lastVisitDate: '2015-12-12',
    },
    reportTypeDto: { name: 'FSR' },
  },
  {
    id: 2,
    job: {
      id: 1,
      lastVisitDate: '2017-12-12',
    },
    reportTypeDto: { name: 'FSR' },
  },
  {
    id: 3,
    job: {
      id: 1,
      lastVisitDate: '2018-09-12',
    },
    reportTypeDto: { name: 'FSR' },
  },
  {
    id: 4,
    job: {
      id: 1,
      lastVisitDate: '2015-01-12',
    },
    reportTypeDto: { name: 'FSR' },
  },
  {
    id: 5,
    job: {
      id: 1,
      lastVisitDate: '2018-12-12',
    },
    reportTypeDto: { name: 'FSR' },
  },
];

export const codicilsMock = [
  {
    id: 1,
    codicilType: 'COC',
    status: 'Open',
  },
  {
    id: 2,
    codicilType: 'AI',
    status: 'Closed',
  },
  {
    id: 3,
    codicilType: 'AN',
    status: 'Change Recommended',
  },
  {
    id: 4,
    codicilType: 'AN',
    status: 'Deleted',
  },
  {
    id: 5,
    codicilType: 'COC',
    status: 'Closed',
  },
  {
    id: 6,
    codicilType: 'AI',
    status: 'Open',
  },
  {
    id: 7,
    codicilType: 'AI',
    status: 'Closed',
  },
  {
    id: 8,
    codicilType: 'COC',
    status: 'Change Recommended',
  },
  {
    id: 9,
    codicilType: 'COC',
    status: 'Change Recommended',
  },
  {
    id: 10,
    codicilType: 'COC',
    status: 'Change Recommended',
  },
];

export const certificatesMock = [
  {
    id: 1,
  },
  {
    id: 2,
  },
  {
    id: 3,
  },
];
