/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle, max-len,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */

import { certificatesMock, codicilsMock, surveyReportsMock } from './asset-hub-mock';
import AppSettings from 'app/config/project-variables.js';
import AssetHubComponent from './asset-hub.component';
import AssetHubController from './asset-hub.controller';
import AssetHubModule from './asset-hub';
import AssetHubTemplate from './asset-hub.pug';

describe('AssetHub', () => {
  let $rootScope,
    $compile,
    mockPermissionsService,
    makeController;

  beforeEach(window.module(AssetHubModule));

  beforeEach(() => {
    mockPermissionsService = {
      canAccessMPMS: () => true,
      canAccessCertsAndCodicils: () => { },
      canAccessSistersSection: () => { },
      canAccessJobs: () => { },
      canPrintExport: user => user && !user.isEquasisThetis,
    };

    window.module(($provide) => {
      $provide.value('$mdMedia', {});
      $provide.value('AppSettings', AppSettings);
      $provide.value('PermissionsService', mockPermissionsService);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('assetHub', { $dep: dep }, bindings);
        return controller;
      };
  }));

  describe('Controller', () => {
    let controller;

    beforeEach(() => {
      controller = makeController({
        asset: { id: 'LRV1', isFromMAST: true },
        currentUser: {
          role: 'LR_ADMIN',
        },
        technicalSisters: [],
        registerBookSisters: [],
      });
    });

    describe('itemUrl()', () => {
      it('correctly returns the url for the buttons', () => {
        expect(controller.itemUrl(controller.constructor.SECTIONS.CODICILS))
          .toEqual("asset.codicilsListing.conditionsOfClass({ assetId: 'LRV1' })");
        expect(controller.itemUrl(controller.constructor.SECTIONS.JOBS))
          .toEqual("asset.jobsList({ assetId: 'LRV1' })");
        expect(controller.itemUrl(controller.constructor.SECTIONS.CERTIFICATES))
          .toEqual("asset.certificatesAndRecords({ assetId: 'LRV1' })");
        expect(controller.itemUrl(controller.constructor.SECTIONS.MPMS))
          .toEqual("asset.mpms({ assetId: 'LRV1' })");
      });
    });

    describe('canView', () => {
      it('checks for MPMS access is delegated to permissions service', () => {
        mockPermissionsService.canAccessMPMS = () => true;
        spyOn(mockPermissionsService, 'canAccessMPMS').and.callThrough();
        expect(controller.canView(AssetHubController.SECTIONS.MPMS)).toEqual(true);
        expect(mockPermissionsService.canAccessMPMS).toHaveBeenCalledTimes(1);

        mockPermissionsService.canAccessMPMS = () => false;
        expect(controller.canView(AssetHubController.SECTIONS.MPMS)).toEqual(false);
      });

      it('checks for codicils and certificates access', () => {
        mockPermissionsService.canAccessCertsAndCodicils = () => true;
        spyOn(mockPermissionsService, 'canAccessCertsAndCodicils').and.callThrough();
        expect(controller.canView(AssetHubController.SECTIONS.CODICILS)).toBe(true);
        expect(controller.canView(AssetHubController.SECTIONS.CERTIFICATES)).toBe(true);
        expect(mockPermissionsService.canAccessCertsAndCodicils).toHaveBeenCalledTimes(2);

        mockPermissionsService.canAccessCertsAndCodicils = () => false;
        expect(controller.canView(AssetHubController.SECTIONS.CODICILS)).toBe(false);
        expect(controller.canView(AssetHubController.SECTIONS.CERTIFICATES)).toBe(false);
      });

      it('checks for jobs access', () => {
        mockPermissionsService.canAccessJobs = () => true;
        spyOn(mockPermissionsService, 'canAccessJobs').and.callThrough();
        expect(controller.canView(AssetHubController.SECTIONS.JOBS)).toEqual(true);
        expect(mockPermissionsService.canAccessJobs).toHaveBeenCalledTimes(1);

        mockPermissionsService.canAccessJobs = () => false;
        expect(controller.canView(AssetHubController.SECTIONS.JOBS)).toEqual(false);
      });
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = AssetHubComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(AssetHubTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(AssetHubController);
    });
  });
});
