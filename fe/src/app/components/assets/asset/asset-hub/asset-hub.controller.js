import Base from 'app/base';

export default class AssetHubController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    AppSettings,
    PermissionsService,
  ) {
    super(arguments);
  }

  itemUrl(type) {
    const { STATES } = this._AppSettings;
    let sref = '';

    switch (type) {
      case AssetHubController.SECTIONS.CODICILS:
        sref = `${STATES.CONDITIONS_OF_CLASS_LIST}({ assetId: '${this.asset.id}' })`;
        break;
      case AssetHubController.SECTIONS.JOBS:
        sref = `${STATES.SERVICE_HISTORY_JOBS_LIST}({ assetId: '${this.asset.id}' })`;
        break;
      case AssetHubController.SECTIONS.CERTIFICATES:
        sref = `${STATES.CERTIFICATES_LIST}({ assetId: '${this.asset.id}' })`;
        break;
      case AssetHubController.SECTIONS.MPMS:
        sref = `${STATES.ASSET}.${STATES.MPMS}({ assetId: '${this.asset.id}' })`;
        break;
      default:
        sref = '';
    }

    return sref;
  }

  /**
   * The visibility matrix:
   *                   | Accessible | Restricted | EOR | Equasis/Thetis   | Flag user
   * Codicils          |      /     |      X     |  /  |       /          |     /
   * Jobs              |      /     |      X     |  /  |       X          |     x
   * Certificates      |      /     |      X     |  /  |       /          |     /
   * MPMS              |      /     |      X     |  X  |  Role-dependent. |     x
   *
   * @param section
   * @returns {boolean} Whether or not the user can view that section.
   */
  canView(section) {
    switch (section) {
      case AssetHubController.SECTIONS.CERTIFICATES:
      case AssetHubController.SECTIONS.CODICILS:
        return this._PermissionsService.canAccessCertsAndCodicils(this.asset);
      case AssetHubController.SECTIONS.JOBS:
        return this._PermissionsService.canAccessJobs(this.asset, this.currentUser);
      case AssetHubController.SECTIONS.MPMS:
        return this._PermissionsService.canAccessMPMS(this.asset, this.currentUser);
      default:
        return false;
    }
  }

  /**
   * Sections in the Asset Hub.
   */
  static SECTIONS = {
    ALL_SISTERS: 'allSisters',
    CERTIFICATES: 'certificates',
    CODICILS: 'codicils',
    EXPORT: 'export',
    JOBS: 'jobs',
    MPMS: 'mpms',
    REGISTER_BOOK_SISTERS: 'registerBookSisters',
    TECHNICAL_SISTERS: 'technicalSisters',
  }
}
