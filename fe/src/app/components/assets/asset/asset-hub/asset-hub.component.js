import './asset-hub.scss';
import controller from './asset-hub.controller';
import template from './asset-hub.pug';

export default {
  bindings: {
    asset: '<',
    certificates: '<',
    codicils: '<',
    currentUser: '<',
    surveyReports: '<',
  },
  controllerAs: 'vm',
  controller,
  restrict: 'E',
  template,
};
