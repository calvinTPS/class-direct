import * as _ from 'lodash';
import Base from 'app/base';

const MODES = {
  NORMAL: 'normal',
  SUMMARY: 'summary',
  EXPORT: 'export',
  EOR: 'eor',
};

const THEMES = {
  BLUE: 'blue',
  WHITE: 'white',
};

export default class AssetsController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    $rootScope,
    $scope,
    AppSettings,
    AssetService,
    DashboardService,
    UserService,
    ViewService,
  ) {
    super(arguments);
  }

  async $onInit() {
    this.mode = this.mode || MODES.NORMAL;
    this.theme = this.theme || THEMES.BLUE;
    this.showTopControl = this.showTopControl == null || this.showTopControl;
    this.viewTypeOptions = _.cloneDeep(this._AppSettings.VIEW_TYPES);

    this._DashboardService.favourites = {
      // Using object insted of array since its easier to add/remove entry
      // e.g { LRV10: 'LRV10', LRV99: 'LRV99'}
      toAdd: {},
      toRemove: {},
    };

    const { SET, REMOVE } = AssetsController.FAV_UPDATE_TYPE;
    this._$scope.$on(this._AppSettings.EVENTS.ON_TOGGLE_FAVOURITE, (event, payload) => {
      const { type, assetId } = payload;
      // SET means to add an asset to the favourites list
      if (type === SET) {
        // If it was added to the toRemove list earlier,
        // meaning it was a favourite asset that the user had just clicked to unfavourite/remove,
        // just remove it from the toRemove list
        if (this._DashboardService.favourites.toRemove[assetId]) {
          delete this._DashboardService.favourites.toRemove[assetId];
        // Else add it to the toAdd list
        } else {
          this._DashboardService.favourites.toAdd[assetId] = assetId;
        }
      }

      // REMOVE means to remove an asset from the favourites list
      if (type === REMOVE) {
        // If it was added to the toAdd list earlier,
        // meaning it was NOT a favourite asset that the user had just clicked to favourite/add,
        // just remove it from the toAdd list
        if (this._DashboardService.favourites.toAdd[assetId]) {
          delete this._DashboardService.favourites.toAdd[assetId];
        // Else add it to the toRemove list
        } else {
          this._DashboardService.favourites.toRemove[assetId] = assetId;
        }
      }

      this._$rootScope.inFavouritesMode = this.isShowActionBar;
    });

    this._UserService.getCurrentUser()
      .then((currentUser) => {
        this.currentUser = currentUser;
        this._AssetService.setAssetCurrentUser(currentUser);
        this._$scope.$apply();
      });
  }

  get MODES() {
    return MODES;
  }

  get viewType() {
    return this._ViewService.getVesselListViewType();
  }

  /**
   * To show 3 or 4 assets per row based on mode.
   *
   * @returns {Integer}
   */
  get miniCardFlex() {
    return this.mode === 'export' ? 33 : 25;
  }

  get includeAll() {
    return this._includeAll;
  }

  set includeAll(bool) {
    this._includeAll = bool;

    // Selects or deselects all asset object
    _.forEach(this.assets, (asset) => {
      asset.selected = bool; // eslint-disable-line no-param-reassign
    });

    // Reset selected ids
    this.selectedIds = {
      includedAssets: [],
      excludedAssets: [],
    };

    // Disable Select/Deselect All button
    this.isPristine = true;
  }

  get includedCount() {
    this._includedCount = this.includeAll ?
      _.get(this.assets, 'pagination.totalElements', 0) -
        (_.countBy(this.assets, asset => asset.selected).false || 0) :
      _.countBy(this.assets, asset => asset.selected).true || 0;

    // Shows warning message in export asset modal
    this._$rootScope.$broadcast(this._AppSettings.EVENTS.TOO_MANY_ASSETS_TO_EXPORT, {
      exceeded: this._includedCount > this._AppSettings.DEFAULT_PARAMS.MAXIMUM_ASSETS_EXPORT,
    });

    return this._includedCount;
  }

  set includedCount(integer) {
    this._includedCount = integer;
  }

  get addButtonLabelCount() {
    return this.assets.pagination.totalElements - this.includedCount;
  }

  get removeButtonLabelCount() {
    return this.includedCount;
  }

  _getIncludedIds() {
    return _.reduce(this.assets, (result, asset) => {
      if (asset.selected) {
        result.push(asset.id);
      }
      return result;
    }, []);
  }

  _getExcludedIds() {
    return _.reduce(this.assets, (result, asset) => {
      if (!asset.selected) {
        result.push(asset.id);
      }
      return result;
    }, []);
  }

  /**
   * selectedIds getter will only be called from parent controller via
   * two way data binding. So the parent controller can get included/excluded
   * ids automatically.
   *
   * @return {object} : This getter will return an object containing
   *                    {
   *                      includedAssets: ['asset_code_strings'],
   *                      excludedAssets: ['asset_code_strings'],
   *                    }
  */
  get selectedIds() {
    if (this.includeAll) { // When we select all, we will pass excluded ids
      const newExcludedIds = this._getExcludedIds();
      this._selectedIds = !_.isEqual(newExcludedIds, this._selectedIds.excludedAssets) ? {
        includedAssets: [],
        excludedAssets: newExcludedIds,
      } : this._selectedIds;
    } else { // When we remove all, we will pass included ids
      const newIncludedIds = this._getIncludedIds();
      this._selectedIds = !_.isEqual(newIncludedIds, this._selectedIds.includedAssets) ? {
        includedAssets: newIncludedIds,
        excludedAssets: [],
      } : this._selectedIds;
    }

    return this._selectedIds;
  }

  set selectedIds(userIdsObject) {
    this._selectedIds = userIdsObject || {};
  }

  get isShowActionBar() {
    return !!(_.keys(this._DashboardService.favourites.toAdd).length ||
    _.keys(this._DashboardService.favourites.toRemove).length);
  }


  /**
   * @param string assetId
   * This function will be used when rendering the asset card to figure out whether to show in
   * favourite or not favourite state.
   */
  isFavouriteAsset(assetId) {
    let val; // undefined (if undefined Star Controller will use the value from asset.isFavourite)

    // If the asset had been clicked TO ADD to the favourites list(not saved yet)
    // - actually not a favourite asset yet but temporarily in the favourite state
    if (this._DashboardService.favourites.toAdd[assetId]) {
      val = true;

    // If the asset had been clicked TO REMOVE from the favourites list(not saved yet)
    // - actually a favourite asset but temporarily NOT in the favourite state
    } else if (this._DashboardService.favourites.toRemove[assetId]) {
      val = false;
    }

    return val; // return true || false || undefined
  }

  async saveFavourites() {
    const add = _.keys(this._DashboardService.favourites.toAdd);
    const remove = _.keys(this._DashboardService.favourites.toRemove);

    if (add.length || remove.length) {
      this.isSaving = true;
      this._$rootScope.$broadcast(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, true);

      const payload = { add, remove };

      try {
        await this._AssetService.updateFavourites(payload);
        // Reload the assets so pagination continues to work correctly
        // In favourites view, once you add/remove assets, the assets list in database
        // and data in pagination becomes out of sync
        await this._DashboardService.submitQuery();
        this.isSaving = false;
        this._DashboardService.favourites.toAdd = {};
        this._DashboardService.favourites.toRemove = {};
        this._$rootScope.inFavouritesMode = this.isShowActionBar;
        this._$rootScope.$broadcast(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, false);
        this._$scope.$apply();
      } catch (err) {
        this.isSaving = false;
        this._$rootScope.$broadcast(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, false);
      }
    }
  }

  cancelSaveFavourites() {
    _.forEach(this._DashboardService.favourites, (assetIds, actionType) => {
      _.forEach(assetIds, (assetId) => {
        this._$rootScope.$broadcast(this._AppSettings.EVENTS.UPDATE_FAVOURITE, {
          assetId,
          // actionType === 'toRemove || 'toAdd', see this._favourites
          // entries in toRemove were initially a favourite,
          // and entries in toAdd were initially NOT a favourite
          isFavourite: actionType === 'toRemove',
        });
      });
    });

    this._DashboardService.favourites.toAdd = {};
    this._DashboardService.favourites.toRemove = {};

    // - This resets the events
    this._$rootScope.inFavouritesMode = this.isShowActionBar;
    this._$rootScope.$broadcast(this._AppSettings.EVENTS.DISABLE_ASSETS_SEARCH, false);
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }

  get renderAssetCard() {
    return this._$mdMedia('xs') || this.viewType === this.viewTypeOptions.CARD;
  }

  get renderAssetTable() {
    return this._$mdMedia('gt-xs') &&
      this.viewType === this.viewTypeOptions.TABLE &&
      this.assets.length > 0;
  }

  static FAV_UPDATE_TYPE = {
    SET: 'set',
    REMOVE: 'remove',
  }
}
