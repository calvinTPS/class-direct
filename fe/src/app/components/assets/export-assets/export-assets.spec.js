/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import AppSettings from 'app/config/project-variables.js';
import ExportAssetsComponent from './export-assets.component';
import ExportAssetsController from './export-assets.controller';
import ExportAssetsModule from './';
import ExportAssetsTemplate from './export-assets.pug';

describe('ExportAssets', () => {
  let $compile;
  let $mdDialog;
  let $rootScope;
  let makeController;

  const mockExportService = {
    exportAssetsPDF: payload => Promise.resolve([]),
  };

  beforeEach(window.module(ExportAssetsModule));

  beforeEach(() => {
    const mockTranslateFilter = (value) => {
      if (value === 'cd-assets') return 'assets';

      return value;
    };

    const mockMdDialog = function mdDialog() {
      return {
        alert: (arg) => {},
        build: (arg) => {},
        cancel: function cancel(reason, options) {},
        confirm: (arg) => {},
        destroy: function destroyInterimElement(opts) {},
        hide: function hide(reason, options) {},
        prompt: (arg) => {},
        show: function showInterimElement(opts) {
          return new Promise((resolve, reject) => resolve());
        },
      };
    };

    const mockDashboardService = {
      queryOptions: {},
    };

    window.module(($provide) => {
      $provide.service('$mdDialog', mockMdDialog); // eslint-disable-line angular/no-service-method
      $provide.value('$mdMedia', {});
      $provide.value('AppSettings', AppSettings);
      $provide.value('DashboardService', mockDashboardService);
      $provide.value('ExportService', mockExportService);
      $provide.value('translateFilter', mockTranslateFilter);
      $provide.value('safeTranslateFilter', mockTranslateFilter);
    });
  });

  beforeEach(inject((_$rootScope_, _$compile_, _$mdDialog_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $mdDialog = _$mdDialog_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('exportAssets', { $dep: dep }, bindings);
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const mockLocation = {
        search: () => ({
          export: AppSettings.EXPORT_TYPES.ASSETS,
        }),
      };
      const controller = makeController({ foo: 'bar', assets: [{ id: 1 }], $location: mockLocation });

      expect(controller.foo).toEqual('bar');
      expect(controller.assets.length).toEqual(1);

      // shoulf set isLoading by default
      expect(controller.isLoading).toEqual(false);

      // should set to include all by default
      expect(controller.includeAll).toEqual(true);
    });

    it('_initializeTabs() should get list of tab items and select the initial active tab correctly', () => {
      let controller = makeController();
      expect(controller.selectedTab).toEqual('information');

      controller = makeController({ showAssets: true });
      controller._initializeTabs();
      expect(controller.selectedTab).toEqual('assets');

      expect(controller.tabs).toEqual({
        assets: {
          label: 'Assets',
        },
        information: {
          label: 'cd-information',
        },
      });
    });

    it('should listen to export assets PDF event and set isLoading correctly', () => {
      const controller = makeController();
      $rootScope.$broadcast(AppSettings.EVENTS.EXPORT_ASSETS_PDF_GENERATED);
      expect(controller.isLoading).toEqual(false);
    });

    it('showModal() should popup $mdDialog modal box', () => {
      const controller = makeController();
      spyOn($mdDialog, 'show');
      controller.showModal();
      expect($mdDialog.show).toHaveBeenCalled();
    });

    it('closeModal() should close $mdDialog modal box', () => {
      const controller = makeController();
      spyOn($mdDialog, 'cancel');
      controller.closeModal();
      expect($mdDialog.cancel).toHaveBeenCalled();
    });

    it('export() should populate with the right payload', () => {
      const mockSelectedIds = {
        includedAssets: [
          'LRV1', 'LRV2',
        ],
        excludedAssets: [],
      };
      const controller = makeController();
      controller.assets = [
        { name: 'LRV1' },
        { name: 'LRV2' },
      ];
      spyOn(mockExportService, 'exportAssetsPDF');

      expect(typeof controller.export).toEqual('function');

      controller.showAssets = true;
      controller.selectedIds = mockSelectedIds;
      controller.isEOR = true;
      controller.selection.fileType = 2;
      controller.export();
      expect(controller.isLoading).toEqual(true);
      expect(mockExportService.exportAssetsPDF).toHaveBeenCalled();
      expect(mockExportService.exportAssetsPDF).toHaveBeenCalledWith(
        {
          includes: ['LRV1', 'LRV2'],
          excludes: [],
          filter: {
            isEOR: true,
          },
          fileType: 'PDF',
          info: 1,
          service: 1,
          codicils: [1, 2, 3, 5],
        }, true);
    });

    it('canExport getter should return the correct value', () => {
      const controller = makeController();
      controller.isLoading = false;
      controller.isTooManyAssets = false;
      controller.showAssets = false;
      controller.includedCount = 0;
      controller.assets = [
        { name: 'asset 1' },
      ];

      expect(controller.canExport).toEqual(true);

      controller.isLoading = true;
      expect(controller.canExport).toEqual(false);

      controller.isLoading = false;
      controller.isTooManyAssets = true;
      expect(controller.canExport).toEqual(false);

      controller.isTooManyAssets = false;
      controller.showAssets = true;
      expect(controller.canExport).toEqual(false);

      controller.includedCount = 1;
      expect(controller.canExport).toEqual(true);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ExportAssetsComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ExportAssetsTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ExportAssetsController);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<export-assets foo="bar"><export-assets/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('ExportAssets');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
