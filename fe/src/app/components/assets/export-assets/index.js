import angular from 'angular';
import ExportAssetsComponent from './export-assets.component';
import uiRouter from 'angular-ui-router';

export default angular.module('exportAssets', [
  uiRouter,
])
.component('exportAssets', ExportAssetsComponent)
.name;
