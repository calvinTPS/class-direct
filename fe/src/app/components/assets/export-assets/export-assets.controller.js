import * as _ from 'lodash';
import Base from 'app/base';
import dialogTemplate from './export-assets-modal.pug';

export default class ExportAssetsController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $filter,
    $location,
    $mdDialog,
    $mdMedia,
    $rootScope,
    $scope,
    AppSettings,
    DashboardService,
    ExportService,
  ) {
    super(arguments);

    this.INFO = {
      BASIC: 1,
      FULL: 2,
    };

    this.FILE_TYPE = {
      CSV: 1,
      PDF: 2,
    };

    this.SERVICE = {
      NONE: 1,
      BASIC: 2,
      FULL: 3,
    };

    this.CODICILS = {
      COC: {
        NAME: 'cd-condition-of-class',
        SERVER_ID: 1,
        SELECTED: true,
      },
      AI: {
        NAME: 'cd-actionable-item',
        SERVER_ID: 2,
        SELECTED: true,
      },
      AN: {
        NAME: 'cd-asset-note',
        SERVER_ID: 3,
        SELECTED: true,
      },
      SF: {
        NAME: 'cd-statutory-finding',
        SERVER_ID: 5,
        SELECTED: true,
      },
    };

    // Default selection
    this.selection = {
      info: this.INFO.BASIC,
      service: this.SERVICE.NONE,
      codicils: this.CODICILS,
      fileType: this.FILE_TYPE.CSV,
    };

    this.isLoading = false;
    this.includeAll = true;
    this.isTooManyAssets = false;

    this._initializeTabs();

    this._$scope.$on(this._AppSettings.EVENTS.ASSETS_EXPORT_DONE, () => {
      this.isLoading = false;
    });

    this._$scope.$on(this._AppSettings.EVENTS.TOO_MANY_ASSETS_TO_EXPORT, (event, { exceeded }) => {
      this.isTooManyAssets = exceeded;
    });

    this._$scope.$on(this._AppSettings.EVENTS.ASSETS_EXPORT_INITIALIZE, () => {
      this.showModal();
    });
  }

  /**
   * To get the list of tab items to display and select the initial active tab
   */
  _initializeTabs() {
    this.selectedTab = this.showAssets ? 'assets' : 'information';

    this.tabs = {
      assets: {
        label: _.startCase(this._$filter('translate')('cd-assets')),
      },
      information: {
        label: 'cd-information',
      },
    };
  }

  showModal(ev) {
    this._$mdDialog.show({
      bindToController: true,
      controller: ExportAssetsController,
      controllerAs: 'vm',
      fullscreen: true,
      locals: {
        assets: this.assets,
        currentUser: this.currentUser,
        showAssets: this.showAssets,
        isEOR: this._$rootScope.role_mode === this._AppSettings.ROLES.EOR,
      },
      parent: this._$document.body,
      template: dialogTemplate,
      onComplete: () => {
        /* eslint-disable angular/document-service */
        const dialogView = document.querySelector('.export-assets-modal md-dialog-content');
        const assetsView = document.querySelector('.export-assets-modal pagination');
        /* eslint-enable */

        if (dialogView && assetsView) {
          // Load the second page if the first page content is taking less space than
          // the available space, which would not create the scrollbar required to scroll
          // to the bottom to trigger the second page
          if (assetsView.clientHeight < dialogView.clientHeight) {
            this._$rootScope.$broadcast(this._AppSettings.EVENTS.SCROLL_TO_BOTTOM_PAGE);
          }
        }
      },
    });
  }

  closeModal() {
    this._$mdDialog.cancel();
  }

  export() {
    this.isLoading = true;

    let payload = {};
    let multipleAssets = false;
    // construct the payload of filter and asset ids
    if (this.showAssets) {
      _.set(payload, 'includes', this.selectedIds.includedAssets);
      _.set(payload, 'excludes', this.selectedIds.excludedAssets);
      payload.filter = this._DashboardService.queryOptions;
      // For multiple assets, initially both excludes and includes [] will be empty
      // When we exclude an assets, excludes [] will be populated
      // When we click Remove all button and add assets, includes [] will be populated
      // When we click to Add all button, both excludes and includes [] will be empty
      multipleAssets =
        this.assets.length > 0 && (this.assets.length - payload.excludes.length > 1) &&
        payload.includes.length !== 1;
    } else {
      payload = {
        includes: [this.assets[0].id],
        excludes: [],
        filter: {},
      };
    }

    // Is EOR or not
    payload.filter.isEOR = this.isEOR;

    // File type
    payload.fileType = this.selection.fileType === 1 ? 'CSV' : 'PDF';

    // construct the payload of selected information, services and codicils
    // export for CSV is fixed
    if (this.selection.fileType === this.FILE_TYPE.PDF) {
      payload.info = this.selection.info;
      payload.service = this.selection.service;
      payload.codicils = _.map(_.filter(this.selection.codicils, ['SELECTED', true]), 'SERVER_ID');
    }

    this._ExportService.exportAssetsPDF(payload, multipleAssets);
  }

  // Enable export button if conditions are met:
  //
  // 1) is not in the middle of exporting, and
  // 2) not exporting more than limit, and
  // 3) at least an asset is selected
  get canExport() {
    return !this.isLoading &&
      !this.isTooManyAssets &&
      (
        (this.showAssets && !!this.includedCount) ||
        (!this.showAssets && !!this.assets.length)
      );
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
