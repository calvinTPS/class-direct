import './export-assets.scss';
import controller from './export-assets.controller';
import template from './export-assets.pug';

export default {
  bindings: {
    assets: '<',
    hasMobileView: '<?',
    showAssets: '<?',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
