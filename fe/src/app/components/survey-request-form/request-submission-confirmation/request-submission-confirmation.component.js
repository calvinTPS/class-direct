import './request-submission-confirmation.scss';
import controller from './request-submission-confirmation.controller';
import template from './request-submission-confirmation.pug';

export default {
  restrict: 'E',
  bindings: {
    surveyRequest: '<',
  },
  template,
  controllerAs: 'vm',
  controller,
};
