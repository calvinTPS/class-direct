/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import RequestSubmissionConfirmationComponent from './request-submission-confirmation.component';
import RequestSubmissionConfirmationController from './request-submission-confirmation.controller';
import RequestSubmissionConfirmationModule from './request-submission-confirmation';
import RequestSubmissionConfirmationTemplate from './request-submission-confirmation.pug';

describe('RequestSubmissionConfirmation', () => {
  let $rootScope, makeController;

  beforeEach(window.module(RequestSubmissionConfirmationModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      const controller = new RequestSubmissionConfirmationController();
      controller.surveyRequest = {
        otherEmails: ['this@yahoo.com', 'that@gmail.com', 'thisandthat@hotmail.com'],
      };
      return controller;
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller.name).toBeDefined();
    });

    it('return the other emails address', () => {
      const controller = makeController();
      expect(controller.surveyRequest.otherEmails).toBeDefined();
      expect(controller.otherEmails).toEqual(controller.surveyRequest.otherEmails);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = RequestSubmissionConfirmationComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(RequestSubmissionConfirmationTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(RequestSubmissionConfirmationController);
    });
  });
});
