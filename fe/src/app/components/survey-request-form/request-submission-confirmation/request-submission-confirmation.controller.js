import Base from 'app/base';

export default class RequestSubmissionConfirmationController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);
    this.name = 'request-submission-confirmation';
  }

  get otherEmails() {
    return this.surveyRequest.otherEmails;
  }
}
