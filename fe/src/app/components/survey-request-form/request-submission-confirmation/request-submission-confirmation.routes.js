/* ngInject */
export default function config($stateProvider) {
  'ngInject';

  $stateProvider
   .state('asset.requestSubmissionConfirmation', {
     url: '/assets/{assetId: string}/survey-request/submission-confirmation',
     params: {
       surveyRequest: {},
     },
     views: {
       'asset-view': {
         component: 'requestSubmissionConfirmation',
       },
     },
     resolve: {
       /* @ngInject */
       surveyRequest: $stateParams => $stateParams.surveyRequest,
     },
   });
}
