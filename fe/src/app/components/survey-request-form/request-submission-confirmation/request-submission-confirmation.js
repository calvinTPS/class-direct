import angular from 'angular';
import requestSubmissionConfirmationComponent from './request-submission-confirmation.component';
import routes from './request-submission-confirmation.routes';
import uiRouter from 'angular-ui-router';

export default angular.module('requestSubmissionConfirmation', [
  uiRouter,
])
.config(routes)
.component('requestSubmissionConfirmation', requestSubmissionConfirmationComponent)
.name;
