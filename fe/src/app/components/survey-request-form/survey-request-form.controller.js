import * as _ from 'lodash';
import Base from 'app/base';
import dateHelper from 'app/common/helpers/date-helper';
import fecha from 'fecha';
import SurveyRequestModel from 'app/common/models/survey-request-model';

export default class SurveyRequestFormController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $mdMedia,
    $state,
    $timeout,
    AppSettings,
    SurveyService,
    utils,
    WizardHandler,
  ) {
    super(arguments);
  }

  $onInit() {
    this._defaultScrollUp();
    this._surveyDetailModel = Reflect.construct(SurveyRequestModel,
      [this._surveyDetailModel]);
    this.groupServices();
    this._visibleServicesCount = 0;
    this._payload = {};
  }

  groupServices() {
    // - An array of services is passed in
    // I will sort all services at request survey page

    // - Helper function to sort Classification
    const sortClassificatonServices = c => _.sortBy(c, [
      'serviceCatalogueH.productGroupH.displayOrder',
      'dueDateEpoch',
      'serviceCatalogueH.displayOrder',
      'occurrenceNumber']
    );

    // - Helper function to sort MMS and Statutory Services
    const sortOtherServices = c => _.sortBy(c, [
      'dueDateEpoch',
      'serviceCatalogueH.displayOrder',
      'occurrenceNumber']
    );

    const advanceSort = (services) => {
      const classificationServices = sortClassificatonServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.ID]));

      const statutoryServices = sortOtherServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.STATUTORY.ID]));

      const mmsServices = sortOtherServices(_.filter(services, [
        'serviceCatalogueH.productCatalogue.productType.id',
        this._AppSettings.PRODUCT_FAMILY_TYPES.MMS.ID]));

      return _.concat(classificationServices, mmsServices, statutoryServices);
    };

    // I will tick the services that is not due
    const mergeCollection = c => _.map(c, r =>
      _.merge(r, { isSelected:
        (_.startCase(r.dueStatus) === this._AppSettings.OVERALL_STATUS_NAMES.OVERDUE ||
        _.startCase(r.dueStatus) === this._AppSettings.OVERALL_STATUS_NAMES.IMMINENT ||
        _.startCase(r.dueStatus) === this._AppSettings.OVERALL_STATUS_NAMES.DUE_SOON),
      }));


    const results = _.flow(
      mergeCollection,
      advanceSort,
    )(this.assetServices);

    // I will group the services into 3 buckets :
    // Classification, Statutory, MMS
    // Not required to display the classification label
    //
    // Product Groups(Classification label is not needed)
    //    Service Groups
    // MMS
    //    Service Groups
    // Statutory
    //    Service Groups
    //
    // I will group the services into a product catalogue with the following structure
    // [['Hull',[Services Array], ['Anchor',[Service Array]]]
    const pairsCollection = c => _.toPairs(_.groupBy(c, (service) => {
      if (service.productFamilyName ===
        this._AppSettings.PRODUCT_FAMILY_TYPES.CLASSIFICATION.NAME) {
        return service.productGroupName;
      }
      return service.productFamilyName;
    }));

    // I will update each array to add the key
    // ['Hull',[Services Array]]
    // {'productName':'Hull', 'details':[Services Array]}
    const mergeServiceCollection = c => _.map(c, r => _.merge(_.zipObject(['productName', 'details'], r), {
      isCategorySelected: !!_.find(r[1], { isSelected: true }),
      isExpanded: false,
    })
  );

    this._surveyDetailModel.groupedServices = _.flow(
      pairsCollection,
      mergeServiceCollection,
    )(results);
  }

  /** method to get the selected services and push into survey request model */
  addSelectedServices() {
    this._surveyDetailModel.serviceIds.length = 0;
    const filteredResults = _.filter(this._surveyDetailModel.groupedServices, 'isCategorySelected');
    _.forEach(filteredResults, (selectedResult) => {
      _.forEach(_.filter(selectedResult.details, 'isSelected'), (selectedDetail) => {
        this._surveyDetailModel.serviceIds.push(selectedDetail.id);
      });
    });
  }

  _closeBucketHeader() {
    _.forEach(this._surveyDetailModel.groupedServices, (groupedService) => {
      if (groupedService.isExpanded) {
        _.set(groupedService, 'isExpanded', false);
        this._visibleServicesCount -= groupedService.details.length;
      }
    });
  }

  /**
   * Scroll up to the top using utils method.
   *
   * @private
   */
  _defaultScrollUp() {
    this._utils.scrollToTop();
  }

  /** method to return a payload from populated survey request model */
  populatePayload() {
    this._payload = {};
    _.set(this._payload, 'portId', this._surveyDetailModel.port.id);
    _.set(this._payload, 'assetId', this.asset.id);
    _.set(this._payload, 'estimatedDateOfArrival',
        this.formatDate(this._surveyDetailModel.estimatedDateOfArrival));
    _.set(this._payload, 'estimatedTimeOfArrival',
        this.formatTime(this._surveyDetailModel.estimatedTimeOfArrival));
    _.set(this._payload, 'estimatedDateOfDeparture',
        this.formatDate(this._surveyDetailModel.estimatedDateOfDeparture));
    _.set(this._payload, 'estimatedTimeOfDeparture',
        this.formatTime(this._surveyDetailModel.estimatedTimeOfDeparture));
    _.set(this._payload, 'surveyStartDate',
        this.formatDate(this._surveyDetailModel.surveyStartDate));
    if (!_.isEmpty(this._surveyDetailModel.otherEmails)) {
      _.set(this._payload, 'otherEmails', this._surveyDetailModel.otherEmails);
    }
    _.set(this._payload, 'portAgent', this._surveyDetailModel.portAgent);
    _.set(this._payload, 'berthAnchorage', this._surveyDetailModel.berthAnchorage);
    _.set(this._payload, 'shipContactDetails', this._surveyDetailModel.shipContactDetails);
    _.set(this._payload, 'additionalComments', this._surveyDetailModel.additionalComments);
    _.set(this._payload, 'serviceIds', this._surveyDetailModel.serviceIds);
  }

  /** getter for payload object */
  get payload() {
    return this._payload;
  }
  /** setter for payload object */
  set payload(payload) {
    this._payload = payload;
  }
  /** getter for surveyDetailModel */
  get surveyDetailModel() {
    return this._surveyDetailModel;
  }
  /** setter for surveyDetailModel */
  set surveyDetailModel(surveyDetailModel) {
    this._surveyDetailModel = surveyDetailModel;
  }
  /** getter for visible services when dropping accordion */
  get visibleKnownServices() {
    return this._visibleServicesCount;
  }
  /** setter for visible services when dropping accordion */
  set visibleKnownServices(knownServices) {
    this._visibleServicesCount = knownServices;
  }
  /** go to back a step */
  backStep() {
    this._defaultScrollUp();
    this._closeBucketHeader();
    this._WizardHandler.wizard().previous();
  }
  /** go to next step */
  nextStep() {
    this._defaultScrollUp();
    this._closeBucketHeader();
    this._WizardHandler.wizard().next();
  }
  /** go to a specific step number */
  goToStep(stepNumber) {
    this._defaultScrollUp();
    this._closeBucketHeader();
    this._WizardHandler.wizard().goTo(stepNumber);
  }
  /** back to previous page */
  backToPrevious() {
    if (this._$state.previous && this._$state.previous.name) {
      this._$state.go(this._$state.previous.name, this._$state.previous.params);
    } else {
      // Need this when coming to state directly via URL.
      this._$state.go(this._AppSettings.STATES.ASSET_HOME, { assetId: this.asset.id });
    }
  }
  /** validation method before moving to location & dates page */
  toLocationAndDatesValidation() {
    return !_.find(this._surveyDetailModel.groupedServices, { isCategorySelected: true });
  }
  /** validation method before moving to confirm page */
  toConfirmValidation() {
    const surveyModel = this._surveyDetailModel;
    if (surveyModel.isLocationDateValid) {
      return !surveyModel.isLocationDateValid();
    }
    return true;
  }
  /** Move to confirmation */
  toConfirmation() {
    this._defaultScrollUp();
    this._closeBucketHeader();
    const surveyModel = this._surveyDetailModel;
    if (surveyModel.isLocationDateValid && surveyModel.isLocationDateValid()) {
      this.nextStep();
    }
  }
  /** end the wizard and submit model to api */
  async completeSurvey() {
    if (!this.acceptedTnc) {
      return false;
    }

    this.addSelectedServices();
    this.populatePayload();
    try {
      await this._SurveyService.createSurvey(this.asset.id, this._payload);
      this._$state.go(this._AppSettings.STATES.REQUEST_SUBMISSION_CONFIRMATION, {
        assetId: this.asset.id,
        surveyRequest: this._surveyDetailModel,
      });
    } catch (e) {
      this._$log.error(e);
    }

    return true;
  }

  formatDate(date) {
    return _.isDate(date) && dateHelper.isValid(date) ?
      fecha.format(date, this._AppSettings.FORMATTER.LONG_DATE_SERVER) : undefined;
  }

  formatTime(time) {
    return _.isDate(time) && dateHelper.isValid(time) ?
      fecha.format(time, this._AppSettings.FORMATTER.LONG_TIME_SERVER) : time;
  }

  toggleAccordionExpansion(bool) {
    _.forEach(this.surveyDetailModel.groupedServices, (serviceList) => {
      serviceList.isExpanded = !!bool; // eslint-disable-line no-param-reassign
    });
    this._visibleServicesCount = _.find(this.surveyDetailModel.groupedServices, 'isExpanded') ?
      this.assetServices.length : 0;
  }

  get isNativeMobile() {
    // eslint-disable-next-line
    return !!window.classDirect;
  }
}
