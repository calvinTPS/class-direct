import angular from 'angular';
import Bucket from './bucket/bucket';
import LocationAndDatesForm from './location-and-dates-form/location-and-dates-form';
import RequestSubmissionConfirmation from
  './request-submission-confirmation/request-submission-confirmation';
import SurveyConfirmationForm from './survey-confirmation-form/survey-confirmation-form';
import surveyRequestFormComponent from './survey-request-form.component';

export default angular.module('surveyRequestForm', [
  Bucket,
  LocationAndDatesForm,
  RequestSubmissionConfirmation,
  SurveyConfirmationForm,
])
.component('surveyRequestForm', surveyRequestFormComponent)
.name;
