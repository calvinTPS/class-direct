import './survey-confirmation-form.scss';
import controller from './survey-confirmation-form.controller';
import template from './survey-confirmation-form.pug';

export default {
  restrict: 'E',
  bindings: {
    asset: '<',
    editStep: '&',
    surveyDetailModel: '=',
  },
  template,
  controllerAs: 'vm',
  controller,
};
