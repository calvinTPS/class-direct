import Base from 'app/base';

export default class SurveyConfirmationFormController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
  ) {
    super(arguments);
    this._otherEmailsReadOnly = true;

    this.otherEmailsOptions = [];
  }

  get otherEmailsReadOnly() {
    return this._otherEmailsReadOnly;
  }

  set otherEmailsReadOnly(otherEmailsReadOnly) {
    this._otherEmailsReadOnly = otherEmailsReadOnly;
  }

  activateShareEmail() {
    this._otherEmailsReadOnly = !this._otherEmailsReadOnly;
    if (this._otherEmailsReadOnly) {
      this.surveyDetailModel.otherEmails = [];
    }
  }

  goToEdit() {
    this.editStep();
  }
}
