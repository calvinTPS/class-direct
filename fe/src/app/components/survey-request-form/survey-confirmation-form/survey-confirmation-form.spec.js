/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import SurveyConfirmationFormComponent from './survey-confirmation-form.component';
import SurveyConfirmationFormController from './survey-confirmation-form.controller';
import SurveyConfirmationFormModule from './survey-confirmation-form';
import SurveyConfirmationFormTemplate from './survey-confirmation-form.pug';

describe('SurveyConfirmationForm', () => {
  let $rootScope, makeController;
  let controller;

  beforeEach(window.module(SurveyConfirmationFormModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => new SurveyConfirmationFormController();
  }));
  beforeEach(() => {
    controller = makeController();
  });
  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('should initialize with otherEmailsReadOnly flag', () => {
      expect(controller.otherEmailsReadOnly).toBeTruthy();
    });

    it('can otherEmailsReadOnly flag retrieve and override', () => {
      const mockOtherEmailsReadOnly = controller.otherEmailsReadOnly;

      expect(controller.otherEmailsReadOnly).toBeTruthy();
      const newMockFlag = false;
      controller.otherEmailsReadOnly = newMockFlag;
      expect(controller.otherEmailsReadOnly).toBeFalsy();
    });

    it('has a function to switch boolean otherEmailsReadOnly flag', () => {
      const mockOtherEmailsReadOnly = controller.otherEmailsReadOnly;
      const mockSurveyDetailModel = { mockOtherEmails: [] };
      controller.surveyDetailModel = mockSurveyDetailModel;

      expect(controller.otherEmailsReadOnly).toBe(true);
      controller.activateShareEmail();
      expect(controller.otherEmailsReadOnly).toBe(false);
    });

    it('has a function to remove otherEmails if true', () => {
      const mockSurveyDetailModel = { otherEmails: ['a', 'b'] };
      const newMockFlag = false;
      controller.otherEmailsReadOnly = newMockFlag;
      controller.surveyDetailModel = mockSurveyDetailModel;

      controller.activateShareEmail();
      expect(controller.surveyDetailModel.otherEmails).toEqual([]);
    });

    it('calls goToEdit function', () => {
      const mockEditFunction = () => {};
      controller.editStep = mockEditFunction;
      spyOn(controller, 'goToEdit').and.callThrough();
      controller.goToEdit();
      expect(controller.goToEdit).toHaveBeenCalled();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SurveyConfirmationFormComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SurveyConfirmationFormTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SurveyConfirmationFormController);
    });
  });
});
