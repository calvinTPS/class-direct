import angular from 'angular';
import surveyConfirmationFormComponent from './survey-confirmation-form.component';
import uiRouter from 'angular-ui-router';

export default angular.module('surveyConfirmationForm', [
  uiRouter,
])
.component('surveyConfirmationForm', surveyConfirmationFormComponent)
.name;
