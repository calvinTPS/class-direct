/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-underscore-dangle,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import * as _ from 'lodash';
import AppSettings from 'app/config/project-variables.js';
import SurveyRequestFormComponent from './survey-request-form.component';
import SurveyRequestFormController from './survey-request-form.controller';
import SurveyRequestFormMocks from './survey-request-form.mocks.json';
import SurveyRequestFormModule from './survey-request-form';
import SurveyRequestFormTemplate from './survey-request-form.pug';
import SurveyRequestModel from 'app/common/models/survey-request-model';
import SurveyService from '../../services/survey.service.js';

describe('SurveyRequestForm', () => {
  let $rootScope, makeController, mockWizardHandler, mockWizard;
  let mockSurveyObject, mockPromise, mockRestangular, mockService;
  let controller;

  const servicesJSON = SurveyRequestFormMocks;
  const mockState = {
    go: () => true,
  };

  beforeEach(window.module(SurveyRequestFormModule));
  beforeEach(inject((_$rootScope_, _$state_, $document, $timeout) => {
    $rootScope = _$rootScope_;
    mockWizard = {
      previous() {
        return this;
      },
      next() {
        return this;
      },
      goTo(stepNumber) {
        return this;
      },
    };
    mockWizardHandler = {
      wizard() {
        return mockWizard;
      },
    };
    mockSurveyObject = {};
    mockPromise = {
      get() {
        return this;
      },
      getList() {
        return this;
      },
      customPOST() {
        return this;
      },
      customGET() {
        return this;
      },
      customGETLIST() {
        return this;
      },
      one() {
        return this;
      },
    };
    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };
    spyOn(mockPromise, 'get').and.callFake(() => mockSurveyObject);
    spyOn(mockPromise, 'getList').and.returnValue(mockSurveyObject);
    spyOn(mockPromise, 'customPOST').and.returnValue(mockSurveyObject);
    spyOn(mockPromise, 'customGET').and.returnValue(mockSurveyObject);
    spyOn(mockPromise, 'customGETLIST').and.returnValue(mockSurveyObject);
    spyOn(mockPromise, 'one').and.returnValue(mockSurveyObject);

    mockRestangular = {
      name: 'mockRestangular',
      one() {
        return mockPromise;
      },
      all() {
        return mockPromise;
      },
    };
    spyOn(mockRestangular, 'one').and.returnValue(mockPromise);
    spyOn(mockRestangular, 'all').and.returnValue(mockPromise);
    mockService = new SurveyService(mockRestangular, AppSettings);

    const mockUtils = {
      scrollToTop: () => {},
    };

    const $mdMedia = {};

    makeController = () => {
      const con = new SurveyRequestFormController(
        $document,
        $mdMedia,
        mockState,
        $timeout,
        AppSettings,
        mockService,
        mockUtils,
        mockWizardHandler,
      );

      con.$onInit();
      return con;
    };
  }));
  beforeEach(() => {
    controller = makeController();
  });
  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('groups services into categories properly', () => {
      expect(typeof controller.groupServices).toBe('function');
      expect(controller.surveyDetailModel.groupedServices.length).toEqual(0);

      const products =
        Object.keys(_.groupBy(servicesJSON, 'serviceCatalogueH.productCatalogue.name'));
      controller.assetServices = servicesJSON;

      controller.groupServices();
      // - Classification Product Families (1 x Count) + Statutory Services (1) + MMS Services (1)
      expect(controller.surveyDetailModel.groupedServices.length).toEqual(products.length);
      _.forEach(controller.surveyDetailModel.groupedServices, (service) => {
        // make sure serviceCategory and details is in each service Object
        expect(_.has(service, 'productName')).toBeTruthy();
        expect(_.has(service, 'details')).toBeTruthy();
      });
    });

    it('calls groupServices function', () => {
      spyOn(controller, 'groupServices').and.callThrough();
      controller.groupServices();
      expect(controller.groupServices).toHaveBeenCalled();
    });

    it('has survey detail model object', () => {
      const mockSurveyDetailModel = controller.surveyDetailModel;
      expect(controller.surveyDetailModel).toBeDefined();
      expect(controller.surveyDetailModel.otherEmails).toBeDefined();
    });

    it('can survey detail model retrieve and override', () => {
      const mockSurveyDetailModel = controller.surveyDetailModel;

      mockSurveyDetailModel.name = 'survey details';
      expect(controller.surveyDetailModel.name).toEqual('survey details');
      const newMockModel = new SurveyRequestModel();
      newMockModel.otherEmails.push('test@test.com');
      controller.surveyDetailModel = newMockModel;
      expect(controller.surveyDetailModel.otherEmails).toEqual(['test@test.com']);
      expect(controller.surveyDetailModel).not.toEqual(mockSurveyDetailModel);
      expect(controller.surveyDetailModel).toBeDefined();
    });

    it('has method to validate if services has been selected', () => {
      controller.assetServices = servicesJSON;
      controller.groupServices();
      expect(controller.toLocationAndDatesValidation()).toBeTruthy();

      _.set(_.head(controller.surveyDetailModel.groupedServices), 'isCategorySelected', true);
      expect(controller.toLocationAndDatesValidation()).toBeFalsy();
    });

    it('toggleAccordionExpansion() should set groupedServices isExpanded key to the correct value', () => {
      let bool;
      bool = true;
      controller.toggleAccordionExpansion(bool);
      _.forEach(controller.surveyDetailModel.groupedServices, (serviceList) => {
        expect(serviceList.isExpanded).toEqual(bool);
      });

      bool = false;
      _.forEach(controller.surveyDetailModel.groupedServices, (serviceList) => {
        expect(serviceList.isExpanded).toEqual(bool);
      });
    });

    it('has method to validate by calling location and date validation passed', () => {
      const mockSurveyDetailModel = { port: {},
        isLocationDateValid() {
          return true;
        },
      };
      spyOn(mockSurveyDetailModel, 'isLocationDateValid').and.returnValue(true);
      controller.surveyDetailModel = mockSurveyDetailModel;
      expect(controller.toConfirmValidation()).toBeFalsy();
      expect(mockSurveyDetailModel.isLocationDateValid).toHaveBeenCalled();
    });

    it('has method to validate by calling location and date validation fail', () => {
      const mockSurveyDetailModel = { port: {},
        isLocationDateValid() {
          return true;
        },
      };
      spyOn(mockSurveyDetailModel, 'isLocationDateValid').and.returnValue(false);
      controller.surveyDetailModel = mockSurveyDetailModel;
      expect(controller.toConfirmValidation()).toBeTruthy();
      expect(mockSurveyDetailModel.isLocationDateValid).toHaveBeenCalled();
    });

    it('sets and gets the visible Known Services value properly', () => {
      expect(controller.visibleKnownServices).toEqual(0);
      controller.visibleKnownServices = 5;
      expect(controller.visibleKnownServices).toEqual(5);
    });

    it('should initialize with payload object', () => {
      expect(controller.payload).toEqual(jasmine.any(Object));
    });

    it('should populate mandatory values to the payload from the survey model', () => {
      const newMockModel = new SurveyRequestModel();
      const mockSurveyDetailModel = {
        port: { id: 1 },
        serviceIds: [101],
      };
      const mockAsset = { id: 1 };
      newMockModel.port.id = 1;
      newMockModel.serviceIds.push(101);
      controller.surveyDetailModel = newMockModel;
      controller.asset = mockAsset;
      expect(controller.payload).toBeDefined();
      controller.populatePayload();
      expect(controller.payload.portId).toEqual(1);
      expect(controller.payload.serviceIds[0]).toEqual(101);
      expect(controller.payload.assetId).toEqual(1);
    });

    it('should format the dates correctly', () => {
      const now = new Date('July 21, 1986');
      expect(controller.formatDate(now)).toEqual('1986-07-21');
    });

    it('should format the time correctly', () => {
      const now = new Date('July 21, 1986 01:15:00');
      expect(controller.formatTime(now)).toEqual('01:15:00');
    });

    it.async('should go to correct state', async (done) => {
      controller.asset = servicesJSON[0].asset;
      const mockSurveyDetailModel = {
        port: { id: 1 },
        serviceIds: [101],
      };
      const mockAsset = { id: 1 };
      controller.surveyDetailModel = mockSurveyDetailModel;
      controller.asset = mockAsset;
      spyOn(controller, 'completeSurvey').and.callThrough();
      spyOn(controller._$state, 'go');

      await controller.completeSurvey();
      expect(controller._$state.go).not.toHaveBeenCalled();

      controller.acceptedTnc = true;
      await controller.completeSurvey();
      expect(controller._$state.go).toHaveBeenCalledWith(
        'asset.requestSubmissionConfirmation',
        jasmine.objectContaining({
          assetId: 1,
          surveyRequest: controller.surveyDetailModel,
        }));
    });

    it('should go back to previous state', () => {
      mockState.previous = {
        name: 'asset.home',
        params: { id: 1 },
      };
      spyOn(mockState, 'go');
      controller.backToPrevious();
      expect(mockState.go).toHaveBeenCalledWith(
        'asset.home', { id: 1 }
      );
    });

    it('close the bucket header when going to other steps', () => {
      // setting up and expect initial values in controller
      controller.assetServices = servicesJSON;
      controller.groupServices();
      expect(controller.visibleKnownServices).toEqual(0);
      // if expanded is true, visible services should equal to services expanded
      controller.surveyDetailModel.groupedServices[0].isExpanded = true;
      controller.visibleKnownServices =
        controller.surveyDetailModel.groupedServices[0].details.length;
      expect(controller.visibleKnownServices)
        .toEqual(controller.surveyDetailModel.groupedServices[0].details.length);

      // expect expanded to be reset to false after going to next step
      controller.nextStep();
      expect(controller.surveyDetailModel.groupedServices[0].isExpanded).toBeFalsy();
      expect(controller.visibleKnownServices).toEqual(0);

      // set manually is expanded to true
      controller.surveyDetailModel.groupedServices[0].isExpanded = true;
      controller.visibleKnownServices =
        controller.surveyDetailModel.groupedServices[0].details.length;
      expect(controller.visibleKnownServices)
        .toEqual(controller.surveyDetailModel.groupedServices[0].details.length);

      // expect expanded to be reset to false after when back to previous step
      controller.backStep();
      expect(controller.surveyDetailModel.groupedServices[0].isExpanded).toBeFalsy();
      expect(controller.visibleKnownServices).toEqual(0);

      // set manually is expanded to true
      controller.surveyDetailModel.groupedServices[0].isExpanded = true;
      controller.visibleKnownServices =
        controller.surveyDetailModel.groupedServices[0].details.length;
      expect(controller.visibleKnownServices)
        .toEqual(controller.surveyDetailModel.groupedServices[0].details.length);

      // expect expanded to be reset to false
      controller.goToStep(1);
      expect(controller.surveyDetailModel.groupedServices[0].isExpanded).toBeFalsy();
      expect(controller.visibleKnownServices).toEqual(0);

      // set manually is expanded to true
      controller.surveyDetailModel.groupedServices[0].isExpanded = true;
      controller.visibleKnownServices =
        controller.surveyDetailModel.groupedServices[0].details.length;

      // expect expanded to be reset to false after going to step 3
      controller.toConfirmation();
      expect(controller.surveyDetailModel.groupedServices[0].isExpanded).toBeFalsy();
      expect(controller.visibleKnownServices).toEqual(0);
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = SurveyRequestFormComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(SurveyRequestFormTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(SurveyRequestFormController);
    });
  });
});
