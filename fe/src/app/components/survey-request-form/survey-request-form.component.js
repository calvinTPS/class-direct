import './survey-request-form.scss';
import controller from './survey-request-form.controller';
import template from './survey-request-form.pug';

export default {
  restrict: 'E',
  bindings: {
    asset: '<',
    assetServices: '<',
    visibleServices: '<',
  },
  template,
  controllerAs: 'vm',
  controller,
};
