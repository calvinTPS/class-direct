/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars, no-unused-expressions, arrow-body-style,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import AppSettings from 'app/config/project-variables.js';
import LocationAndDatesFormComponent from './location-and-dates-form.component';
import LocationAndDatesFormController from './location-and-dates-form.controller';
import LocationAndDatesFormModule from './location-and-dates-form';
import LocationAndDatesFormTemplate from './location-and-dates-form.pug';

describe('LocationAndDatesForm', () => {
  let $rootScope,
    $mdMedia,
    controller,
    makeController;

  beforeEach(window.module(
    LocationAndDatesFormModule,
    angularMaterial,
  ));

  beforeEach(() => {
    const mockUtilsFactory = {
      isIE: () => {},
      generateTimeData: () => {},
      isMobileDevice: () => false,
    };

    window.module(($provide) => {
      $provide.value('utils', mockUtilsFactory);
    });
  });

  beforeEach(inject((_$rootScope_, _$mdMedia_, _$controller_) => {
    $rootScope = _$rootScope_;
    $mdMedia = _$mdMedia_;

    const scope = $rootScope.$new();
    const data = {
      surveyDetailModel: {},
    };
    makeController = () => {
      const con = _$controller_(
        LocationAndDatesFormController, {
          $scope: scope,
          $rootScope,
          AppSettings,
        }, data);
      con.$onInit();
      return con;
    };
  }));

  beforeEach(() => {
    controller = makeController();
  });

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Component', () => {
    // component/directive specs
    const component = LocationAndDatesFormComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(LocationAndDatesFormTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(LocationAndDatesFormController);
    });
  });

  describe('Controller', () => {
    // controller specs
    it('test if it is valid location date', () => {
      controller.surveyDetailModel.port = undefined;
      expect(controller.surveyDetailModel.isLocationDateValid()).toBe(false);
      controller.surveyRequestForm = { $valid: false };
      expect(controller.surveyDetailModel.isLocationDateValid()).toBe(false);

      controller.surveyRequestForm = { $valid: true };
      controller.surveyDetailModel.port = { id: 1 };
      expect(controller.surveyDetailModel.isLocationDateValid()).toBe(true);
    });

    it('return the proper maximum arrival date', () => {
      expect(controller.maxArrivalDate).toBeFalsy();
      controller.surveyDetailModel.surveyStartDate = new Date();
      expect(controller.maxArrivalDate).toBe(controller.surveyDetailModel.surveyStartDate);
      const tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      controller.surveyDetailModel.estimatedDateOfDeparture = tomorrow;
      expect(controller.maxArrivalDate).toBe(controller.surveyDetailModel.surveyStartDate);
      controller.surveyDetailModel.surveyStartDate = null;
      expect(controller.maxArrivalDate).toBe(controller.surveyDetailModel.estimatedDateOfDeparture);
      controller.surveyDetailModel.estimatedDateOfDeparture = null;
      expect(controller.maxArrivalDate).toBeFalsy();
    });

    it('return the proper minimum departure date', () => {
      expect(controller.minDepartureDate).toBe(controller.now);
      controller.surveyDetailModel.surveyStartDate = new Date();
      expect(controller.minDepartureDate).toBe(controller.surveyDetailModel.surveyStartDate);
      const tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      controller.surveyDetailModel.estimatedDateOfArrival = tomorrow;
      expect(controller.minDepartureDate).toBe(controller.surveyDetailModel.surveyStartDate);
      controller.surveyDetailModel.surveyStartDate = null;
      expect(controller.minDepartureDate).toBe(controller.surveyDetailModel.estimatedDateOfArrival);
      controller.surveyDetailModel.estimatedDateOfArrival = null;
      expect(controller.minDepartureDate).toBe(controller.now);
    });

    it('get the proper minimum survey start date', () => {
      expect(controller.minSurveyStartDate).toBe(controller.now);
      controller.surveyDetailModel.estimatedDateOfArrival = new Date();
      expect(controller.minSurveyStartDate)
        .toBe(controller.surveyDetailModel.estimatedDateOfArrival);
      controller.surveyDetailModel.estimatedDateOfArrival = null;
      expect(controller.minSurveyStartDate).toBe(controller.now);
    });

    it('get the proper maximum survey start date', () => {
      expect(controller.maxSurveyStartDate).toBeFalsy();
      controller.surveyDetailModel.estimatedDateOfDeparture = new Date();
      expect(controller.maxSurveyStartDate)
        .toBe(controller.surveyDetailModel.estimatedDateOfDeparture);
      controller.surveyDetailModel.estimatedDateOfDeparture = null;
      expect(controller.maxSurveyStartDate).toBeFalsy();
    });

    it('get the proper result on queryListTimes() method', () => {
      expect(controller.queryListTimes().length).toBe(0);
    });
  });
});
