import * as _ from 'lodash';
import Base from 'app/base';

export default class LocationAndDatesFormController extends Base {
  /* @ngInject */
  constructor(
    $mdMedia,
    AppSettings,
    utils,
  ) {
    super(arguments);
  }

  $onInit() {
    this._now = new Date();
    this.surveyDetailModel.isLocationDateValid = () => {
      const isPortPresent =
        _.get(this.surveyDetailModel, 'port.id') != null;
      return isPortPresent && this.surveyRequestForm.$valid;
    };
    this.datePickerSelectedItems = {
      estimatedTimeOfArrival: null,
      estimatedTimeOfDeparture: null,
    };
  }

  queryListTimes(query) {
    const times = this._utils.generateTimeData();
    const interval = new RegExp(this._AppSettings.FORMATTER.TIME_PICKER_DEFAULT_RANGE);

    if (query) {
      return _.filter(times, v => v.indexOf(query) === 0);
    }
    return _.filter(times, v => interval.test(v));
  }

  get now() {
    return this._now;
  }

  get maxArrivalDate() {
    if (this.surveyDetailModel.surveyStartDate) {
      return this.surveyDetailModel.surveyStartDate;
    }

    if (this.surveyDetailModel.estimatedDateOfDeparture) {
      return this.surveyDetailModel.estimatedDateOfDeparture;
    }

    return null;
  }

  get minDepartureDate() {
    if (this.surveyDetailModel.surveyStartDate) {
      return this.surveyDetailModel.surveyStartDate;
    }

    if (this.surveyDetailModel.estimatedDateOfArrival) {
      return this.surveyDetailModel.estimatedDateOfArrival;
    }

    return this._now;
  }

  get minSurveyStartDate() {
    if (this.surveyDetailModel.estimatedDateOfArrival) {
      return this.surveyDetailModel.estimatedDateOfArrival;
    }

    return this._now;
  }
  get maxSurveyStartDate() {
    if (this.surveyDetailModel.estimatedDateOfDeparture) {
      return this.surveyDetailModel.estimatedDateOfDeparture;
    }

    return null;
  }
}
