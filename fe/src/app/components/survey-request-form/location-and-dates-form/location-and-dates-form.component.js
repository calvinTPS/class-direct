import './location-and-dates-form.scss';
import controller from './location-and-dates-form.controller';
import template from './location-and-dates-form.pug';

export default {
  restrict: 'E',
  bindings: {
    asset: '<',
    surveyDetailModel: '=',
  },
  template,
  controllerAs: 'vm',
  controller,
};
