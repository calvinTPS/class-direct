import angular from 'angular';
import LocationAndDatesFormComponent from './location-and-dates-form.component';
import uiRouter from 'angular-ui-router';

export default angular.module('locationAndDatesForm', [
  uiRouter,
])
.component('locationAndDatesForm', LocationAndDatesFormComponent)
.name;
