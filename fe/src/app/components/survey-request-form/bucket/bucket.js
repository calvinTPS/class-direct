import angular from 'angular';
import bucketComponent from './bucket.component';
import ListCheckbox from './list-checkbox/list-checkbox';
import uiRouter from 'angular-ui-router';

export default angular.module('bucket', [
  uiRouter,
  ListCheckbox,
])
.component('bucket', bucketComponent)
.name;
