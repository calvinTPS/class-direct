import * as _ from 'lodash';
import Base from 'app/base';

export default class BucketController extends Base {
  /* @ngInject */
  constructor(
    $log,
  ) {
    super(arguments);
  }

  $onInit() {
    this.name = 'bucket';
    this._initializeVisibleServices();
  }

  goToEdit() {
    this.editStep();
  }

  updateVisibleServices(idx) {
    this.serviceList[idx].isExpanded = !this.serviceList[idx].isExpanded;
    if (this.serviceList[idx].isExpanded) {
      this.visibleServices += this.serviceList[idx].details.length;
    } else {
      this.visibleServices -= this.serviceList[idx].details.length;
    }
  }

  _initializeVisibleServices() {
    _.forEach(this.serviceList, (serviceList) => {
      if (serviceList.isExpanded) {
        this.visibleServices += serviceList.details.length;
      }
    });
  }

  hideHeader(item) {
    return this.isReadOnly && !item.isCategorySelected;
  }

  hideContent(item) {
    return this.isReadOnly && !item.isSelected;
  }
}
