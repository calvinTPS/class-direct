/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import ListCheckBoxComponent from './list-checkbox.component';
import ListCheckBoxController from './list-checkbox.controller';
import ListCheckBoxModule from './list-checkbox';
import ListCheckBoxTemplate from './list-checkbox.pug';

describe('ListCheckBox', () => {
  let $rootScope, makeController;

  beforeEach(window.module(ListCheckBoxModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      const controller = new ListCheckBoxController();
      controller.service = {
        isCategorySelected: false,
        details: [{
          isSelected: false,
        },
        {
          isSelected: false,
        }],
      };
      controller.selected = controller.service.details[1];
      return controller;
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('toggle current item properly', () => {
      const controller = makeController();
      expect(controller.toggle).toEqual(jasmine.any(Function));
      // Current item always toggle.
      expect(controller.selected.isSelected).toBeFalsy();
      controller.toggle();
      expect(controller.selected.isSelected).toBeTruthy();
      controller.toggle();
      expect(controller.selected.isSelected).toBeFalsy();
    });

    it('toggle category when the rest item(s) is/are not selected', () => {
      const controller = makeController();
      expect(controller.selected.isSelected).toBeFalsy();
      expect(controller.service.isCategorySelected).toBeFalsy();
      controller.toggle();
      expect(controller.selected.isSelected).toBeTruthy();
      expect(controller.service.isCategorySelected).toBeTruthy();
      controller.toggle();
      expect(controller.selected.isSelected).toBeFalsy();
      expect(controller.service.isCategorySelected).toBeFalsy();
    });

    it('does not toggle category when other item is selected', () => {
      const controller = makeController();
      // Another service is selected
      controller.service.details[0].isSelected = true;
      controller.service.isCategorySelected = true;

      expect(controller.selected.isSelected).toBeFalsy();
      controller.toggle();
      expect(controller.selected.isSelected).toBeTruthy();
      expect(controller.service.isCategorySelected).toBeTruthy();
      controller.toggle();
      expect(controller.selected.isSelected).toBeFalsy();
      expect(controller.service.isCategorySelected).toBeTruthy();
    });

    it('get the selected item value', () => {
      const controller = makeController();
      // Another service is selected
      expect(controller.checked).toBeFalsy();
      controller.toggle();
      expect(controller.checked).toBeTruthy();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = ListCheckBoxComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(ListCheckBoxTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(ListCheckBoxController);
    });
  });
});
