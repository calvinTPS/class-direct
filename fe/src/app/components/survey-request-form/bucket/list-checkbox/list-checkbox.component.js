import './list-checkbox.scss';
import controller from './list-checkbox.controller';
import template from './list-checkbox.pug';

export default {
  restrict: 'E',
  bindings: {
    selected: '=',
    service: '=',
  },
  template,
  controllerAs: 'vm',
  controller,
};
