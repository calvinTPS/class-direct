import * as _ from 'lodash';
import Base from 'app/base';

export default class ListCheckBoxController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);
    this.name = 'list-checkbox';
  }

  toggle() {
    this.selected.isSelected = !this.selected.isSelected;
    if (_.find(this.service.details, { isSelected: true })) {
      this.service.isCategorySelected = true;
    } else {
      this.service.isCategorySelected = false;
    }
  }

  get checked() {
    return this.selected.isSelected;
  }
}
