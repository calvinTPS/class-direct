import angular from 'angular';
import listCheckboxComponent from './list-checkbox.component';
import uiRouter from 'angular-ui-router';

export default angular.module('listCheckbox', [
  uiRouter,
])
.component('listCheckbox', listCheckboxComponent)
.name;
