/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef,
no-underscore-dangle */
import BucketComponent from './bucket.component';
import BucketController from './bucket.controller';
import BucketModule from './bucket';
import BucketTemplate from './bucket.pug';

describe('Bucket', () => {
  let $rootScope, makeController;

  beforeEach(window.module(BucketModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      const controller = new BucketController();
      controller.$onInit();
      controller.visibleServices = 0;
      controller.editStep = () => {};
      controller.serviceList = [
        {
          id: 1,
          isExpanded: true,
          details: [
            {
              id: 1,
            },
            {
              id: 2,
            },
          ],
        },
        {
          id: 2,
          isExpanded: true,
          details: [
            {
              id: 3,
            },
          ],
        },
        {
          id: 3,
          isExpanded: false,
          details: [
            {
              id: 4,
            },
            {
              id: 2,
            },
          ],
        },
      ];
      controller._initializeVisibleServices();
      return controller;
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller.name).toBeDefined();
    });

    it('_initializeVisibleServices() should return proper rendered data', () => {
      const controller = makeController();
      expect(controller.visibleServices).toEqual(3);
    });

    it('updates the number of known visible services properly', () => {
      const controller = makeController();
      expect(controller.visibleServices).toEqual(3);
      let temp = controller.visibleServices;
      controller.updateVisibleServices(0);
      let totalKnownService = temp - controller.serviceList[0].details.length;
      expect(controller.visibleServices).toEqual(totalKnownService);
      temp = controller.visibleServices;
      controller.updateVisibleServices(0);
      totalKnownService = temp + controller.serviceList[0].details.length;
      expect(controller.visibleServices).toEqual(totalKnownService);
    });

    it('calls editStep function', () => {
      const controller = makeController();
      spyOn(controller, 'goToEdit').and.callThrough();
      controller.goToEdit();
      expect(controller.goToEdit).toHaveBeenCalled();
    });

    it('check whether bucket header should be hidden', () => {
      const controller = makeController();
      controller.isReadOnly = false;
      const item = {
        isCategorySelected: false,
      };
      expect(controller.hideHeader(item)).toBeFalsy();
      controller.isReadOnly = true;
      expect(controller.hideHeader(item)).toBeTruthy();
      item.isCategorySelected = true;
      expect(controller.hideHeader(item)).toBeFalsy();
      controller.isReadOnly = false;
      expect(controller.hideHeader(item)).toBeFalsy();
    });

    it('check whether bucket content should be hidden', () => {
      const controller = makeController();
      controller.isReadOnly = false;
      const item = {
        isSelected: false,
      };
      expect(controller.hideContent(item)).toBeFalsy();
      controller.isReadOnly = true;
      expect(controller.hideContent(item)).toBeTruthy();
      item.isSelected = true;
      expect(controller.hideContent(item)).toBeFalsy();
      controller.isReadOnly = false;
      expect(controller.hideContent(item)).toBeFalsy();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = BucketComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(BucketTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(BucketController);
    });
  });
});
