import './bucket.scss';
import controller from './bucket.controller';
import template from './bucket.pug';

export default {
  restrict: 'E',
  bindings: {
    editStep: '&',
    isHideDetails: '<?',
    isReadOnly: '<',
    mode: '@?',
    serviceList: '=',
    visibleServices: '=?',
  },
  template,
  controllerAs: 'vm',
  controller,
};
