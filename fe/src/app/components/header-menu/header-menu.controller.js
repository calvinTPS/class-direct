import Base from 'app/base';

export default class HeaderMenuController extends Base {
  /* @ngInject */
  constructor(
    $mdMenu,
    $rootScope,
    $scope,
    $state,
    AppSettings,
    UserService,
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
    this._initMenuStatus();
    this.setExternalLinks();

    this._$scope.$on(this._AppSettings.EVENTS.START_MASQUERADE, () => {
      this.setExternalLinks();
      this._$scope.$apply();
    });

    this._currentUser = {};
  }

  _initMenuStatus() {
    this.isMenuOpen = false;

    this._$scope.$on('$mdMenuOpen', () => {
      this.isMenuOpen = true;
    });

    this._$scope.$on('$mdMenuClose', () => {
      this.isMenuOpen = false;
    });
  }

  get canViewMenu() {
    return (this.externalLinks && this.externalLinks.length > 0) || this.canViewRoleSwitcher;
  }

  get canViewRoleSwitcher() {
    return this._currentUser && this._currentUser.isEORUser && !this._currentUser.isEORUserOnly;
  }

  async setExternalLinks() {
    this._currentUser = await this._UserService.getCurrentUser();
    this.externalLinks = this._AppSettings.SYSTEM_HEADER_LINKS[this._currentUser.role] ||
      this._AppSettings.SYSTEM_HEADER_LINKS.DEFAULT;
  }

  /**
   * Set the role  for user view from the switch
   *
   * @param {string} role, either EOR or CLIENT.
   */
  setViewRole(role) {
    this._$rootScope.role_mode = role;

    // Need to set user role for so that user model can do the right thing
    // for the given mode.
    this._currentUser.role = role;
    if (role === this._AppSettings.ROLES.EOR) {
      this._$state.go(this._AppSettings.STATES.EOR_VESSEL_LIST);
    } else {
      this._$state.go(this._AppSettings.STATES.VESSEL_LIST);
    }

    if (this.updateClientName) {
      this.updateClientName(this._currentUser);
    }
  }

  get viewMode() {
    if (this._currentUser.role === this._AppSettings.ROLES.EOR) {
      return {
        name: 'cd-switch-to-cd',
        action: () => {
          // Switch back to user's default role instead of CLIENT,
          // to conform with user role setter validation
          this.setViewRole(this._currentUser.roles[0]);
        },
      };
    }

    return {
      name: 'cd-switch-to-eor',
      action: () => {
        this.setViewRole(this._AppSettings.ROLES.EOR);
      },
    };
  }
}
