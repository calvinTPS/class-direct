/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-underscore-dangle, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import angularMaterial from 'angular-material';
import HeaderMenuComponent from './header-menu.component';
import HeaderMenuController from './header-menu.controller';
import HeaderMenuModule from './';
import HeaderMenuTemplate from './header-menu.pug';

describe('HeaderMenu', () => {
  let $rootScope,
    $state,
    $compile,
    $mdMenu,
    makeController,
    scope;

  const mockUrls = [
    {
      name: 'link title',
      url: 'http://some_url',
      icon: 'file',
    },
    {
      name: 'link title2',
      url: 'http://some_url_2',
      icon: 'file',
    },
  ];
  const mockAppSetting = {
    SYSTEM_HEADER_LINKS: {
      DEFAULT: [mockUrls[0]],
      SOME_ROLE: [mockUrls[1]],
    },
    EVENTS: {
      START_MASQUERADE: 'startMasquerade',
    },
    STATES: {
      EOR_VESSEL_LIST: 'EOR_VESSEL_LIST',
      VESSEL_LIST: 'VESSEL_LIST',
    },
    ROLES: {
      EOR: 'EOR',
      DUMMY_ROLE: 'DUMMY_ROLE',
    },
  };
  const mockUserService = {
    getCurrentUser: () => Promise.resolve({}),
  };

  beforeEach(window.module(
    angularMaterial,
    HeaderMenuModule,
    { translateFilter: value => value },
  ));
  beforeEach(() => {
    window.module(($provide) => {
      $provide.value('AppSettings', mockAppSetting);
      $provide.value('UserService', mockUserService);
    });
  });
  beforeEach(inject((_$rootScope_, _$compile_, _$mdMenu_, $componentController) => {
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
    $compile = _$compile_;
    $mdMenu = _$mdMenu_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('headerMenu', { $scope: scope }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it.async('initializes just fine', async () => {
      const userServiceSpy = spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve({
        role: 'SOME_ROLE',
      }));
      const controller = await makeController();
      expect(controller.isMenuOpen).toEqual(false);
      expect(controller.externalLinks).toEqual([mockUrls[1]]);
      expect(userServiceSpy).toHaveBeenCalledTimes(1);
    });

    it.async('Should using default links if role based links is not defined', async () => {
      spyOn(mockUserService, 'getCurrentUser').and.returnValue(Promise.resolve({
        role: 'SOME_OTHER_ROLE',
      }));
      const controller = await makeController();
      controller.setExternalLinks();
      expect(controller.externalLinks).toEqual([mockUrls[0]]);
    });

    it('Should update the external links on masquerade event', () => {
      const controller = makeController();
      spyOn(controller, 'setExternalLinks');
      $rootScope.$broadcast(mockAppSetting.EVENTS.START_MASQUERADE);
      expect(controller.setExternalLinks).toHaveBeenCalledTimes(1);
    });

    it('Should update the menu status on menu open/close event', () => {
      const controller = makeController();
      scope.$broadcast('$mdMenuOpen');
      expect(controller.isMenuOpen).toBeTruthy();

      scope.$broadcast('$mdMenuClose');
      expect(controller.isMenuOpen).toBeFalsy();
    });

    it('Getter canViewMenu returns the correct value', () => {
      const controller = makeController();
      controller.externalLinks = [{}];
      let result = controller.canViewMenu;
      expect(result).toBeTruthy();

      // Having external links and not EOR user.
      controller.externalLinks = [];
      result = controller.canViewMenu;
      expect(result).toBeFalsy();

      // EOR + CLIENT user.
      controller._currentUser = {
        isEORUser: true,
        isEORUserOnly: false,
      };
      result = controller.canViewMenu;
      expect(result).toBeTruthy();

      // EOR user only.
      controller._currentUser = {
        isEORUser: true,
        isEORUserOnly: true,
      };
      result = controller.canViewMenu;
      expect(result).toBeFalsy();

      // Non EOR.
      controller._currentUser = {
        isEORUser: false,
      };
      result = controller.canViewMenu;
      expect(result).toBeFalsy();
    });

    it('Getter canViewRoleSwitcher return the correct value', () => {
      const controller = makeController();

      // Not EOR user.
      controller._currentUser = {
        isEORUser: false,
      };
      let result = controller.canViewRoleSwitcher;
      expect(result).toBeFalsy();

      // EOR user only
      controller._currentUser = {
        isEORUser: true,
        isEORUserOnly: true,
      };
      result = controller.canViewRoleSwitcher;
      expect(result).toBeFalsy();

      // EOR + CLIENT
      controller._currentUser = {
        isEORUser: true,
        isEORUserOnly: false,
      };
      result = controller.canViewRoleSwitcher;
      expect(result).toBeTruthy();
    });

    it('setViewRole goes to correct state', () => {
      const mockUser = {};
      const controller = makeController();
      controller._currentUser = mockUser;
      controller.updateClientName = () => true;
      spyOn(controller, 'updateClientName');

      spyOn(controller._$state, 'go');
      controller.setViewRole('CLIENT');
      expect(controller._$state.go).toHaveBeenCalledWith(mockAppSetting.STATES.VESSEL_LIST);
      expect(mockUser.role).toEqual('CLIENT');

      controller.setViewRole('EOR');
      controller._currentUser = mockUser;
      expect(controller._$state.go).toHaveBeenCalledWith(mockAppSetting.STATES.EOR_VESSEL_LIST);
      expect(mockUser.role).toEqual('EOR');
      expect(controller.updateClientName).toHaveBeenCalled();
    });

    it('Getter viewMode returns the correct object', () => {
      const controller = makeController();
      controller._currentUser.role = 'EOR';
      let result = controller.viewMode;
      expect(result.name).toEqual('cd-switch-to-cd');
      expect(result.action).toBeDefined();

      controller._currentUser.role = 'CLIENT';
      result = controller.viewMode;
      expect(result.name).toEqual('cd-switch-to-eor');
      expect(result.action).toBeDefined();
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = HeaderMenuComponent;

    it('includes the intended template', () => {
      expect(component.template).toEqual(HeaderMenuTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(HeaderMenuController);
    });
  });
});
