import './header-menu.scss';
import controller from './header-menu.controller';
import template from './header-menu.pug';

export default {
  bindings: {
    updateClientName: '<',
  },
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
