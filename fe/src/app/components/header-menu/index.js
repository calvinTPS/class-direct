import angular from 'angular';
import HeaderMenuComponent from './header-menu.component';
import uiRouter from 'angular-ui-router';

export default angular.module('headerMenu', [
  uiRouter,
])
.component('headerMenu', HeaderMenuComponent)
.name;
