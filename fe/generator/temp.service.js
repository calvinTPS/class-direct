import Base from 'app/base';

export default class <%= upCaseName %>Service extends Base {
  /* @ngInject */
  constructor(
    Restangular,
  ) {
    super(arguments);
  }

  async getOne(<%= name %>Id) {
    return this._Restangular.one('<%= name %>s', <%= name %>Id).get().$object;
  }

  async getAll() {
    return this._Restangular.all('<%= name %>s').getList().$object;
  }
}
