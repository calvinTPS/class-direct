import './<%= hyphenName %>.scss';
import controller from './<%= hyphenName %>.controller';
import template from './<%= hyphenName %>.pug';

export default {
  bindings: {},
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  template,
};
