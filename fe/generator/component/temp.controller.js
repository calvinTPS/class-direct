import Base from 'app/base';

export default class <%= upCaseName %>Controller extends Base {
  /* @ngInject */
  constructor(
  ) {
    super(arguments);
  }

  /**
   * Access injected variables here.
   */
  $onInit() {
  }
}
