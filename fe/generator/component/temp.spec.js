/* global describe beforeEach beforeAll afterEach afterAll it expect */
/* eslint-disable one-var, no-unused-vars,
one-var-declaration-per-line, sort-vars, angular/window-service, no-undef */
import <%= upCaseName %>Component from './<%= hyphenName %>.component';
import <%= upCaseName %>Controller from './<%= hyphenName %>.controller';
import <%= upCaseName %>Module from './';
import <%= upCaseName %>Template from './<%= hyphenName %>.pug';

describe('<%= upCaseName %>', () => {
  let $rootScope,
    $compile,
    makeController;

  beforeEach(window.module(<%= upCaseName %>Module));
  beforeEach(inject((_$rootScope_, _$compile_, $componentController) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    const dep = {};
    makeController =
      (bindings = {}) => {
        const controller = $componentController('<%= camelCaseName %>', { $dep: dep }, bindings);
        controller.$onInit();
        return controller;
      };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    it('initializes just fine', () => {
      const controller = makeController({ foo: 'bar' });
      expect(controller.foo).toEqual('bar');
    });
  });

  describe('Component', () => {
    // component/directive specs
    const component = <%= upCaseName %>Component;

    it('includes the intended template', () => {
      expect(component.template).toEqual(<%= upCaseName %>Template);
    });

    it('invokes the right controller', () => {
      expect(component.controller).toEqual(<%= upCaseName %>Controller);
    });
  });

  describe('Rendering', () => {
    let controller, element;
    beforeEach(() => {
      const scope = $rootScope.$new();
      element = angular.element('<<%= hyphenName %> foo="bar"><<%= hyphenName %>/>');
      element = $compile(element)(scope);
      scope.bar = 'baz'; // CHANGE ME
      scope.$apply();

      controller = element.controller('<%= upCaseName %>');
    });

    it('renders correctly', () => {
      expect(element.length).toEqual(1); // CHANGE ME
    });
  });
});
