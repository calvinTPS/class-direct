import angular from 'angular';
import <%= upCaseName %>Component from './<%= hyphenName %>.component';
import uiRouter from 'angular-ui-router';

export default angular.module('<%= name %>', [
  uiRouter,
])
.component('<%= name %>', <%= upCaseName %>Component)
.name;
