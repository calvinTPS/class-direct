#!/bin/sh

[[ ! -f ~/.aws/credentials ]] &&  echo "Must have AWS CLI and credentials installed." && exit 1

aws s3 cp s3://lr-classdirect/deploy-mobile.sh deploy-mobile.sh
chmod +x deploy-mobile.sh
