/* eslint-disable */
const seleniumJar = require('selenium-server-standalone-jar');

exports.config = {
  seleniumPort: 4444,
  seleniumServerJar: seleniumJar.path,
  specs: [
    'e2e/*.spec.js',
  ],
  framework: "jasmine2",
  onPrepare: () => {
    const jasmineReporters = require('jasmine-reporters');
    jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      savePath: 'test-results',
      filePrefix: 'e2e'
    }));
  },
  capabilities: {
    'browserName': 'phantomjs',
    /*
     * Can be used to specify the phantomjs binary path.
     * This can generally be ommitted if you installed phantomjs globally.
     */
    'phantomjs.binary.path': require('phantomjs-prebuilt').path,
    /*
     * Command line args to pass to ghostdriver, phantomjs's browser driver.
     * See https://github.com/detro/ghostdriver#faq
     */
    'phantomjs.ghostdriver.cli.args': ['--loglevel=DEBUG']
  }
};
