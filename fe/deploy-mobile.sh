#!/bin/bash -e

#
# Builds iOS and Android apps and pushes it to iTunes Connect from the command line.
# -s skip download
# -p ios/android
# -e Demo/Production
#

#
# Prerequisites:
# 1. Run `cordova prepare`
#

#
# Dependencies (IMPORTANT):
# 1. NPM
# 2. OpenSSL (latest): brew update && brew upgrade openssl
# 3. Install newer Ruby 2.3.1:
#   brew install rbenv
#   brew install ruby-build
#   rbenv install 2.3.1
#   rbenv init (follow instructions and add to .bash_profile)
# 4. Fastlane: brew cask install fastlane
# 5. Android setup: https://engineering/confluence/pages/viewpage.action?pageId=450769673
# 6. Additional XCode setup: https://developer.apple.com/library/content/qa/qa1827/_index.html

#
# CI Authentication:
# Please read https://docs.fastlane.tools/best-practices/continuous-integration
# and set up those environment variables:
# 1. FASTLANE_APPLE_APPLICATION_SPECIFIC_PASSWORD
# 2. FASTLANE_SESSION
#

#
# S3 Authentication:
# Install AWS CLI: http://docs.aws.amazon.com/cli/latest/userguide/cli-install-macos.html
# You will need a key and secret key from lr-classdirect bucket owner.
# Read http://docs.aws.amazon.com/cli/latest/reference/s3/ on how to use AWS-CLI
#

[[ ! -f ~/.aws/credentials ]] &&  echo "Must have AWS CLI and credentials installed." && exit 1

downloadBuild=1
env=Demo

while getopts ":se:p:" opt; do
  case $opt in
    s)
      echo "-s option: Skipping download and building with existing files..."
      downloadBuild=0
      ;;
    e)
      echo "-e option: Building for $OPTARG environment..."
      env=$OPTARG
      ;;
    p)
      echo "-p option: Building for platform $OPTARG..."
      platform=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ "$env" != "Demo" ] && [ "$env" != "Test" ] && [ "$env" != "Production" ]; then
    echo "Provide Demo/Production/Test as 1st argument."
    exit 1
fi
echo "Initializing build for $env environment..."

buildAndroid=0
buildIOS=0
if [ -z "$platform" ]; then
    buildAndroid=1
    buildIOS=1
    echo "No platform argument specified. Proceeding to build for both ios and android..."
elif [ "$platform" = "android" ]; then
    buildAndroid=1
    echo "Proceeding to build for android..."
elif [ "$platform" = "ios" ]; then
    buildIOS=1
    echo "Proceeding to build for ios..."
else
    echo "Invalid platform argument. Provide ios/android or leave empty to build both."
    exit 1
fi

BUILD_FILE_NAME="app-build.tar.gz"
BUILD_URL="s3://lr-classdirect/$env/artifacts/$BUILD_FILE_NAME"
BUILD_DIR="build"

IOS_DIR="platforms/ios"
IOS_FASTLANE_DIR="$IOS_DIR/fastlane"
PROJECT_NAME="LR Class Direct"
APP_DELEGATE_FILE="$IOS_DIR/$PROJECT_NAME/Classes/AppDelegate.m"

ANDROID_DIR="platforms/android"
ANDROID_FASTLANE_DIR="$ANDROID_DIR/fastlane"
ANDROID_DEBUG_APK_PATH="platforms/android/build/outputs/apk/android-debug.apk"

mkdir -p $BUILD_DIR
pushd $BUILD_DIR
cat << 'EOF' > Appfile
json_key_file "../../../play-serviceaccount.json"
EOF
    #
    # GENERIC SETUP, Application to all Platforms
    #

    # Remove old files
    if [ $downloadBuild -eq 1 ]; then
        rm -rf www icon* dev-assets config.xml Fastfile APP_VERSION $BUILD_FILE_NAME

        aws s3 cp $BUILD_URL $BUILD_FILE_NAME
        tar -xvzf $BUILD_FILE_NAME
    fi

    APPVERSION=$(cat APP_VERSION)

    # Different bundle ID for demo.
    BUNDLE_ID="org.lr.cdf"
    if [ "$env" = "Demo" ]; then
        BUNDLE_ID="org.lr.cdf-demo"
    fi

    #
    # IOS DEPLOY
    #
    if [ $buildIOS -eq 1 ]; then
        cordova prepare ios

        mkdir -p $IOS_FASTLANE_DIR
        cp Fastfile $IOS_FASTLANE_DIR

        if [ "$env" = "Demo" ] || [ "$env" = "Test" ]; then
            if grep -q "allowsAnyHTTPSCertificateForHost" "$APP_DELEGATE_FILE"; then
                # Nothing to do
                echo "Already injected self-signed SSL code. Nothing to do."
            else
                echo "Injecting self-signed SSL code..."
                # Append self-signed SSL bypass
cat <<EOF >> "$APP_DELEGATE_FILE"
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end
EOF
            fi
        else
            if grep -q "allowsAnyHTTPSCertificateForHost" "$APP_DELEGATE_FILE"; then
                # Delete the self-signed SSL bypass
                sed -i '' '/@implementation NSURLRequest(DataController)/,$ d' "$APP_DELEGATE_FILE"
            fi
        fi

        pushd $IOS_DIR
            fastlane beta version:"$APPVERSION" username:"$FASTLANE_USER" bundle_id:"$BUNDLE_ID"
        popd
    fi

    #
    # ANDROID DEPLOY
    #
    if [ $buildAndroid -eq 1 ]; then
        mkdir -p $ANDROID_FASTLANE_DIR
        cp Appfile $ANDROID_FASTLANE_DIR
        cp Fastfile $ANDROID_FASTLANE_DIR

        # Copies www files into android folders
        cordova prepare android

        # OLD WAY TO PUSH DEBUG APK TO S3
        # cordova build android
        # aws s3 cp "$ANDROID_DEBUG_APK_PATH" "s3://lr-classdirect/$1/artifacts/android-unreleased.apk" --metadata "version=$APPVERSION"

        pushd $ANDROID_DIR
            fastlane android beta bundle_id:"$BUNDLE_ID"
        popd
    fi
popd
