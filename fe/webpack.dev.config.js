/* eslint-disable */
const webpack = require('webpack');
const path = require('path');
const config  = require('./webpack.config');
const autoCleanBuildPlugin = require('webpack-auto-clean-build-plugin');

const publicPath = (process.env && process.env.CDN_URL) ? process.env.CDN_URL : '/';

console.log(`CDN_URL is ${publicPath}`);
config.output = {
  filename: '[name].bundle.js',
  publicPath,
  path: path.resolve(__dirname, 'www'),
};

config.devtool = 'inline-source-map';


config.plugins = config.plugins.concat([
  // Adds webpack HMR support. It act's like livereload,
  // reloading page after webpack rebuilt modules.
  // It also updates stylesheets and inline assets without page reloading.
  new webpack.HotModuleReplacementPlugin(),
  new autoCleanBuildPlugin(),
]);

module.exports = config;
