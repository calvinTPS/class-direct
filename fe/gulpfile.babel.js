/* eslint-disable */
import gulp from 'gulp';

import './gulp/clean';
import './gulp/component';
import './gulp/create-service';
import './gulp/e2e';
import './gulp/serve';
import './gulp/service';
import './gulp/set-env';
import './gulp/watch';
import './gulp/webpack';

gulp.task('default', ['watch']);
