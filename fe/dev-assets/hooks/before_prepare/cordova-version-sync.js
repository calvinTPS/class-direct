// my-version-hook.js
const version = require('cordova-version');
console.log(`Building version ${require('../../../package.json').version}`);
module.exports = () => version.update();
