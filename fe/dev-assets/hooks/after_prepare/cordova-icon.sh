#!/bin/bash

# Note, cordova-icon takes the icon-***.png from the root folder and generates the App icons
# This is a pre-build hook which will run every time the app is built

# This script also generates splash screens from splash.png in root.

function program_is_installed {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  type $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  echo "$return_"
}

if [ ! $(program_is_installed convert) ]; then
  echo "Please install imagemagick. Refer to https://github.com/AlexDisler/cordova-icon"
else
  echo "Generating icons START"
  if [ $(program_is_installed cordova-icon) ]; then
    cordova-icon
    echo "Generating icons... DONE"
  elif [ $(program_is_installed ./node_modules/cordova-icon/bin/cordova-icon) ]; then
    ./node_modules/cordova-icon/bin/cordova-icon
    echo "Generating icons... DONE"
  else
    echo "Could not generate icons. Install imagemagick via Homebrew, and cordova-icon via NPM."
  fi

  echo "Generating splash screen START"
  if [ $(program_is_installed cordova-splash) ]; then
    cordova-splash
    echo "Generating splash screen... DONE"
  elif [ $(program_is_installed ./node_modules/cordova-splash/bin/cordova-splash) ]; then
    ./node_modules/cordova-splash/bin/cordova-splash
    echo "Generating splash screen... DONE"
  else
    echo "Could not generate splash screen. Install imagemagick via Homebrew, and cordova-splash via NPM."
  fi
fi
