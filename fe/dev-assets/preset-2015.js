const presets = require('babel-preset-es2015').buildPreset;

const modules = process.env.RUN_MODE === 'es' ? false : 'commonjs';

module.exports = {
  presets: [
    [
      presets,
      {
        modules,
      }
    ],
  ],
};
